const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');
const mf = require('@angular-architects/module-federation/webpack');
const path = require('path');
const share = mf.share;
const deps = require('../../package.json').dependencies;

/**
 * We use the NX_TSCONFIG_PATH environment variable when using the @nrwl/angular:webpack-browser
 * builder as it will generate a temporary tsconfig file which contains any required remappings of
 * shared libraries.
 * A remapping will occur when a library is buildable, as webpack needs to know the location of the
 * built files for the buildable library.
 * This NX_TSCONFIG_PATH environment variable is set by the @nrwl/angular:webpack-browser and it contains
 * the location of the generated temporary tsconfig file.
 */
const tsConfigPath =
  process.env.NX_TSCONFIG_PATH ??
  path.join(__dirname, '../../tsconfig.base.json');

const workspaceRootPath = path.join(__dirname, '../../');
const sharedMappings = new mf.SharedMappings();
sharedMappings.register(
  tsConfigPath,
  [
    /* mapped paths to share */
    '@hcm-mfe/shared/common/base-service',
    '@hcm-mfe/shared/common/translate',
    '@hcm-mfe/shared/common/store',
  ],
  workspaceRootPath
);

module.exports = {
  output: {
    uniqueName: 'shell',
    publicPath: 'auto',
  },
  mode: 'production',
  optimization: {
    runtimeChunk: false,
  },
  experiments: {
    outputModule: true,
  },
  resolve: {
    alias: {
      ...sharedMappings.getAliases(),
    },
  },
  plugins: [
    new ModuleFederationPlugin({
      remotes: {
        'dashboard-manager': 'http://localhost:4201/remoteEntry.js',
        system: 'http://localhost:4202/remoteEntry.js',
        'staff-manager': 'http://localhost:4203/remoteEntry.js',
        'staff-abs': 'http://localhost:4204/remoteEntry.js',
        'model-organization': 'http://localhost:4205/remoteEntry.js',
        'goal-management': 'http://localhost:4206/remoteEntry.js',
        'learn-development': 'http://localhost:4207/remoteEntry.js',
        'personal-tax': 'http://localhost:4208/remoteEntry.js',
        'policy-management': 'http://localhost:4209/remoteEntry.js',
        'self-service': 'http://localhost:4010/remoteEntry.js',
        partnership: 'http://localhost:4211/remoteEntry.js',
        reports: 'http://localhost:4212/remoteEntry.js',
        'angle-it': 'http://localhost:4213/remoteEntry.js',
        'insurance-management': 'http://localhost:4214/remoteEntry.js',
      },
      shared: share({
        '@angular/core': {
          singleton: true,
          strictVersion: true,
          requiredVersion: deps['@angular/core'],
          includeSecondaries: true,
        },
        '@angular/common': {
          singleton: true,
          strictVersion: true,
          requiredVersion: deps['@angular/common'],
          includeSecondaries: true,
        },
        '@angular/common/http': {
          singleton: true,
          strictVersion: true,
          requiredVersion: deps['@angular/common/http'],
          includeSecondaries: true,
        },
        '@angular/router': {
          singleton: true,
          strictVersion: true,
          requiredVersion: deps['@angular/router'],
          includeSecondaries: true,
        },
        rxjs: {
          singleton: true,
          strictVersion: true,
          requiredVersion: deps['rxjs'],
          includeSecondaries: true,
        },
        '@angular/form': {
          singleton: true,
          strictVersion: true,
          requiredVersion: deps['@angular/form'],
          includeSecondaries: true,
        },
        '@angular/platform-browser': {
          singleton: true,
          strictVersion: true,
          requiredVersion: deps['@angular/platform-browser'],
          includeSecondaries: true,
        },
        '@angular/animations': {
          singleton: true,
          strictVersion: true,
          requiredVersion: deps['@angular/animations'],
          includeSecondaries: true,
        },
        'rxjs/operators': {
          singleton: true,
          strictVersion: true,
          requiredVersion: deps['rxjs'],
          includeSecondaries: true,
        },
        '@larscom/ngx-translate-module-loader': {
          singleton: true,
          strictVersion: true,
          requiredVersion: deps['@larscom/ngx-translate-module-loader'],
          includeSecondaries: true,
        },
        '@ngx-translate/core': {
          singleton: true,
          strictVersion: true,
          requiredVersion: deps['@ngx-translate/core'],
          includeSecondaries: true,
        },
        '@ngx-translate/http-loader': {
          singleton: true,
          strictVersion: true,
          requiredVersion: deps['@ngx-translate/http-loader'],
          includeSecondaries: true,
        },
        'keycloak-angular': {
          singleton: true,
          strictVersion: true,
          requiredVersion: deps['keycloak-angular'],
          includeSecondaries: true,
        },
        'ngx-toastr': {
          singleton: true,
          strictVersion: true,
          requiredVersion: deps['ngx-toastr'],
          includeSecondaries: true,
        },
        ...sharedMappings.getDescriptors(),
      }),
      library: {
        type: 'module',
      },
    }),
    sharedMappings.getPlugin(),
  ],
};
