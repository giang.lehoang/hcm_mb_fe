import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NZ_CONFIG } from 'ng-zorro-antd/core/config';
import { AppRoutingModule } from './app-routing.module';
import { SharedFeatureAppModule } from '@hcm-mfe/shared/feature/app';
import { ShellFeatureLayoutModule } from '@hcm-mfe/shell/feature/layout';
import { AppComponent } from './app.component';
import { CustomHttpIntercepter } from '@hcm-mfe/shared/common/init';
import { NZ_I18N, vi_VN } from 'ng-zorro-antd/i18n';
import { SharedCoreModule } from '@hcm-mfe/shared/core';
import { SharedCommonKeycloakModule } from '@hcm-mfe/shared/common/keycloak';
import { HttpClient } from '@angular/common/http';
import { IModuleTranslationOptions, ModuleTranslateLoader } from '@larscom/ngx-translate-module-loader';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { DecimalPipe } from '@angular/common';
import * as AllIcons from "@ant-design/icons-angular/icons";
import {IconDefinition} from "@ant-design/icons-angular";
import {NZ_ICONS} from "ng-zorro-antd/icon";

export function ModuleHttpLoaderFactory(http: HttpClient) {
  const baseTranslateUrl =  '/assets/i18n';
  const options: IModuleTranslationOptions = {
    modules: [
      { baseTranslateUrl },
      { baseTranslateUrl, moduleName: 'shared', namespace: 'shared' },
    ]
  };
  return new ModuleTranslateLoader(http, options);
}

const antDesignIcons = AllIcons as {
  [key: string]: IconDefinition;
};
const icons: IconDefinition[] = Object.keys(antDesignIcons).map(key => antDesignIcons[key]);

@NgModule({
  declarations: [AppComponent],
  imports: [
    SharedCommonKeycloakModule,
    BrowserModule,
    SharedCoreModule,
    AppRoutingModule,
    SharedFeatureAppModule,
    ShellFeatureLayoutModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: ModuleHttpLoaderFactory,
        deps: [HttpClient],
      }
    }),
  ],
  providers: [
    DecimalPipe,
    {provide: NZ_ICONS, useValue: icons},
    {provide: NZ_I18N, useValue: vi_VN},
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CustomHttpIntercepter,
      multi: true,
    },
    {
      provide: NZ_CONFIG,
      useValue: {
        collapse: {
          nzGhost: true,
        },
        collapsePanel: {
          nzShowArrow: false,
        },
      },
    }
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
