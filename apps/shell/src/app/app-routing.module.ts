import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from '@hcm-mfe/shell/feature/layout';
import { AppInitService, AuthGuard, LoginGuard } from '@hcm-mfe/shared/core';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () =>
      import('@hcm-mfe/shared/feature/login').then(
        (m) => m.SharedFeatureLoginModule
      ),
    canActivate: [LoginGuard],
  },
  {
    path: 'access-denied/',
    loadChildren: () =>
      import('@hcm-mfe/shared/feature/access-denied').then(
        (m) => m.SharedFeatureAccessDeniedModule
      ),
    canActivate: [AuthGuard],
  },
  {
    path: 'access-import/',
    loadChildren: () =>
      import('@hcm-mfe/shared/feature/access-import').then(
        (m) => m.SharedFeatureAccessImportModule
      ),
    canActivate: [AuthGuard],
  },

  {
    path: '',
    component: LayoutComponent,
    canActivate: [AppInitService],
    children: [
      {
        path: '',
        loadChildren: () =>
          import('dashboard-manager/Module').then((m) => m.RemoteEntryModule),
      },
      {
        path: 'system',
        loadChildren: () =>
          import('system/Module').then((m) => m.RemoteEntryModule),
        data: {
          pageName: 'system.pageName.systemManager',
          breadcrumb: 'system.breadcrumb.systemManager'
        }
      },
      {
        path: 'staff',
        loadChildren: () =>
          import('staff-manager/Module').then((m) => m.RemoteEntryModule),
        data: {
          pageName: 'staffManager.pageName.staffManager',
          breadcrumb: 'staffManager.breadcrumb.staffManager'
        }
      },
      {
        path: 'staff-abs',
        loadChildren: () =>
          import('staff-abs/Module').then((m) => m.RemoteEntryModule),
        data: {
          pageName: 'staffAbs.breadcrumb.staffAbs',
          breadcrumb: 'staffAbs.breadcrumb.staffAbs'
        }
      },
      {
        path: 'organization',

        loadChildren: () =>
          import('model-organization/Module').then(
            (m) => m.RemoteEntryModule
          ),
        data: {
          breadcrumb: 'modelOrganization.breadcrumb.modelPlan'
        }
      },
      {
        path: 'development',
        loadChildren: () =>
          import('learn-development/Module').then((m) => m.RemoteEntryModule),
        data: {
          pageName: 'development.pageName.development',
          breadcrumb: 'development.breadcrumb.development'
        }
      },
      {
        path: 'goal-management',
        data: {
          breadcrumb: 'targerManager.breadcrumb.targerManager'
        },
        loadChildren: () =>
          import('goal-management/Module').then((m) => m.RemoteEntryModule),
      },
      {
        path: 'personal-tax',
        loadChildren: () => import('personal-tax/Module').then((m) => m.RemoteEntryModule),
        data: {
          pageName: 'personalTax.pageName.personalTax',
          breadcrumb: 'personalTax.breadcrumb.personalTax'
        }
      },
      {
        path: 'ss',
        loadChildren: () => import('self-service/Module').then((m) => m.RemoteEntryModule),
        data: {
          pageName: '',
        }
      },
      {
        path: 'policy-management',
        data: {
          breadcrumb: 'polManagement.breadcrumb.policyManagement'
        },
        loadChildren: () => import('policy-management/Module').then((m) => m.RemoteEntryModule),
      },
      {
        path: 'partnership',
        data: {
          pageName: 'partnership.pageName.partnership',
          breadcrumb: 'partnership.breadcrumb.partnership'
        },
        loadChildren: () => import('partnership/Module').then((m) => m.RemoteEntryModule),
      },
      {
        path: 'reports',
        data: {
          pageName: 'reports.pageName.reportsManager',
          breadcrumb: 'reports.breadcrumb.reportsManager'
        },
        loadChildren: () => import('reports/Module').then((m) => m.RemoteEntryModule),
      },
      {
        path: 'goc-it',
        data: {
          pageName: 'angleIt.pageName.angleIt',
          breadcrumb: 'angleIt.breadcrumb.angleIt'
        },
        loadChildren: () => import('angle-it/Module').then((m) => m.RemoteEntryModule),
      },
      {
        path: 'insurance-management',
        data: {
          pageName: 'insurance.pageName',
          breadcrumb: 'insurance.breadcrumb',
        },
        loadChildren: () =>
          import('insurance-management/Module').then(
            (m) => m.RemoteEntryModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
