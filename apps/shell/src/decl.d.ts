declare module 'dashboard-manager/Module';

declare module 'goal-management/Module';

declare module 'learn-development/Module';

declare module 'model-organization/Module';

declare module 'staff-abs/Module';

declare module 'staff-manager/Module';

declare module 'system/Module';

declare module 'personal-tax/Module';

declare module 'policy-management/Module';

declare module 'self-service/Module';

declare module 'partnership/Module';

declare module 'reports/Module';

declare module 'insurance-management/Module';

declare module 'angle-it/Module';
