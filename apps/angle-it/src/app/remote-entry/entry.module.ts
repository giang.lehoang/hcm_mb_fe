import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { RemoteEntryComponent } from './entry.component';
import { NZ_DATE_LOCALE, NzI18nModule, NzI18nService } from 'ng-zorro-antd/i18n';
import { vi } from 'date-fns/locale';
import {TranslateLoader, TranslateModule, TranslateService} from "@ngx-translate/core";
import { HttpClient, HttpClientModule } from '@angular/common/http';
import {IModuleTranslationOptions, ModuleTranslateLoader} from "@larscom/ngx-translate-module-loader";
import {TranslateCommon} from "@hcm-mfe/shared/common/utils";
import { environment } from '@hcm-mfe/shared/environment';

export function ModuleHttpLoaderFactory(http: HttpClient) {
  const baseTranslateUrl = `${environment.translateUrl.angleIt}/assets/i18n`;
  const options: IModuleTranslationOptions = {
    modules: [
      { baseTranslateUrl, moduleName: 'angle-it', namespace: 'angleIt' },
    ]
  };
  return new ModuleTranslateLoader(http, options);
}

@NgModule({
  declarations: [RemoteEntryComponent],
  imports: [
    CommonModule,
    NzI18nModule,
    RouterModule.forChild([
      {
        path: '',
        component: RemoteEntryComponent,
        loadChildren: () => import('@hcm-mfe/angle-it/feature/shell').then((m) => m.AngleItFeatureShellModule)
      },
    ]),
    HttpClientModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: ModuleHttpLoaderFactory,
        deps: [HttpClient],
      },
      isolate: true,
      extend: true
    }),
  ],
  providers: [
    { provide: NZ_DATE_LOCALE, useValue: vi },
  ],
})
export class RemoteEntryModule extends TranslateCommon {
  constructor(translate: TranslateService,
              i18n: NzI18nService
  ) {
    super(i18n ,translate, 'angleIt');
  }
}
