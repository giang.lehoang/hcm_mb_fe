import { Component } from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import { NzI18nService } from 'ng-zorro-antd/i18n';
import { en_US_ext, vi_VN_ext } from '@hcm-mfe/shared/common/constants';
import { enUS, vi } from 'date-fns/locale';

@Component({
  selector: 'hcm-mfe-angle-it-entry',
  template: `
    <div>
      <router-outlet></router-outlet>
    </div>`,
})
export class RemoteEntryComponent {
  constructor(translate: TranslateService, i18n: NzI18nService) {
    translate.use(translate.store.currentLang ?? 'vn');
    if(translate.store.currentLang === 'vn') {
      i18n.setLocale(vi_VN_ext);
      i18n.setDateLocale(vi);
    } else {
      i18n.setLocale(en_US_ext);
      i18n.setDateLocale(enUS);
    }
  }
}
