import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { RemoteEntryComponent } from './entry.component';
import { SharedCommonKeycloakModule } from '@hcm-mfe/shared/common/keycloak';
import { NzI18nService, NZ_DATE_LOCALE } from 'ng-zorro-antd/i18n';
import { vi } from 'date-fns/locale';
import { TranslateCommon } from '@hcm-mfe/shared/common/utils';
import {
  TranslateLoader,
  TranslateModule,
  TranslateService,
} from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import {
  IModuleTranslationOptions,
  ModuleTranslateLoader,
} from '@larscom/ngx-translate-module-loader';
import { environment } from '@hcm-mfe/shared/environment';

export function ModuleHttpLoaderFactory(http: HttpClient) {
  const baseTranslateUrl = `${environment.translateUrl.insuranceManagement}/assets/i18n`;
  const options: IModuleTranslationOptions = {
    modules: [
      { baseTranslateUrl, moduleName: 'insurance', namespace: 'insurance' },
    ],
  };
  return new ModuleTranslateLoader(http, options);
}

@NgModule({
  declarations: [RemoteEntryComponent],
  imports: [
    CommonModule,
    SharedCommonKeycloakModule,
    RouterModule.forChild([
      {
        path: '',
        component: RemoteEntryComponent,
        loadChildren: () =>
          import('@hcm-mfe/insurance-management/feature/shell').then(
            (m) => m.InsuranceManagementFeatureShellModule
          ),
      },
    ]),
    HttpClientModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: ModuleHttpLoaderFactory,
        deps: [HttpClient],
      },
      isolate: true,
      extend: true,
    }),
  ],
  providers: [{ provide: NZ_DATE_LOCALE, useValue: vi }],
})
export class RemoteEntryModule extends TranslateCommon {
  constructor(translate: TranslateService, i18n: NzI18nService) {
    super(i18n, translate, 'insurance');
  }
}
