import { Component } from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import {NzI18nService, vi_VN} from "ng-zorro-antd/i18n";
import {vi} from "date-fns/locale";

@Component({
  selector: 'hcm-mfe-insurance-management-entry',
  template: `<div>
    <router-outlet></router-outlet>
  </div>`
})
export class RemoteEntryComponent {
  constructor(translate: TranslateService, i18n: NzI18nService) {
    i18n.setLocale(vi_VN);
    i18n.setDateLocale(vi);
     translate.use(translate.store.currentLang ?? 'vn');
  }
}

