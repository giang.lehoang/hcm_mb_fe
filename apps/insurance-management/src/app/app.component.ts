import { Component } from '@angular/core';

@Component({
  selector: 'hcm-mfe-root',
  templateUrl: './app.component.html',
})
export class AppComponent {}
