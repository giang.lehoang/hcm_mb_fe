const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');
const mf = require('@angular-architects/module-federation/webpack');
const path = require('path');
const {dependencies: deps} = require("../../package.json");
const share = mf.share;

/**
 * We use the NX_TSCONFIG_PATH environment variable when using the @nrwl/angular:webpack-browser
 * builder as it will generate a temporary tsconfig file which contains any required remappings of
 * shared libraries.
 * A remapping will occur when a library is buildable, as webpack needs to know the location of the
 * built files for the buildable library.
 * This NX_TSCONFIG_PATH environment variable is set by the @nrwl/angular:webpack-browser and it contains
 * the location of the generated temporary tsconfig file.
 */
const tsConfigPath =
  process.env.NX_TSCONFIG_PATH ??
  path.join(__dirname, '../../tsconfig.base.json');

const workspaceRootPath = path.join(__dirname, '../../');
const sharedMappings = new mf.SharedMappings();
sharedMappings.register(
  tsConfigPath,
  [
    /* mapped paths to share */
    '@hcm-mfe/shared/common/base-service',
    '@hcm-mfe/shared/common/translate',
    '@hcm-mfe/shared/common/store',
  ],
  workspaceRootPath
);

module.exports = {
  output: {
    uniqueName: 'partnership',
    publicPath: 'auto',
  },
  mode: 'production',
  optimization: {
    runtimeChunk: false,
  },
  experiments: {
    outputModule: true,
  },
  resolve: {
    alias: {
      ...sharedMappings.getAliases(),
    },
  },
  plugins: [
    new ModuleFederationPlugin({
      name: 'partnership',
      filename: 'remoteEntry.js',
      exposes: {
        './Module': 'apps/partnership/src/app/remote-entry/entry.module.ts',
      },
      shared: share({
        '@angular/core': {
          singleton: true,
          strictVersion: true,
          requiredVersion: deps['@angular/core'],
          includeSecondaries: true,
        },
        '@angular/common': {
          singleton: true,
          strictVersion: true,
          requiredVersion: deps['@angular/common'],
          includeSecondaries: true,
        },
        '@angular/common/http': {
          singleton: true,
          strictVersion: true,
          requiredVersion: deps['@angular/common/http'],
          includeSecondaries: true,
        },
        '@angular/router': {
          singleton: true,
          strictVersion: true,
          requiredVersion: deps['@angular/router'],
          includeSecondaries: true,
        },
        'rxjs': {
          singleton: true,
          strictVersion: true,
          requiredVersion: deps['rxjs'],
          includeSecondaries: true,
        },
        '@angular/form': {
          singleton: true,
          strictVersion: true,
          requiredVersion: deps['@angular/form'],
          includeSecondaries: true,
        },
        '@angular/platform-browser': {
          singleton: true,
          strictVersion: true,
          requiredVersion: deps['@angular/platform-browser'],
          includeSecondaries: true,
        },
        '@angular/animations': {
          singleton: true,
          strictVersion: true,
          requiredVersion: deps['@angular/animations'],
          includeSecondaries: true,
        },
        'rxjs/operators': {
          singleton: true,
          strictVersion: true,
          requiredVersion: deps['rxjs'],
          includeSecondaries: true,
        },
        '@larscom/ngx-translate-module-loader': {
          singleton: true,
          strictVersion: true,
          requiredVersion: deps['@larscom/ngx-translate-module-loader'],
          includeSecondaries: true,
        },
        '@ngx-translate/core': {
          singleton: true,
          strictVersion: true,
          requiredVersion: deps['@ngx-translate/core'],
          includeSecondaries: true,
        },
        '@ngx-translate/http-loader': {
          singleton: true,
          strictVersion: true,
          requiredVersion: deps['@ngx-translate/http-loader'],
          includeSecondaries: true,
        },
        'keycloak-angular': {
          singleton: true,
          strictVersion: true,
          requiredVersion: deps['keycloak-angular'],
          includeSecondaries: true,
        },
        'ngx-toastr': {
          singleton: true,
          strictVersion: true,
          requiredVersion: deps['ngx-toastr'],
          includeSecondaries: true,
        },
        ...sharedMappings.getDescriptors(),
      }),
      library: {
        type: 'module',
      },
    }),
    sharedMappings.getPlugin(),
  ],
};
