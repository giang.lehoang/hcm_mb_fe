

# MfApp
BPM-MFE Monorepo
# Tech Stack
- Angular 13.2.0
- NX 13.8.8
- NgRx 12.4.0
- Webpack 5 & Module Federation
## Adding capabilities to your workspace

Nx supports many plugins which add capabilities for developing different types of applications and different tools.

These capabilities include generating applications, libraries, etc as well as the devtools to test, and build projects as well.

Below are our core plugins:

- [Angular](https://angular.io)
  - `npm install --save-dev @nrwl/angular`

There are also many [community plugins](https://nx.dev/community) you could add.

## Generate an application

Run `npx nx g @nrwl/angular:application my-app` to generate an application.

> You can use any of the plugins above to generate applications as well.

When using Nx, you can create multiple applications and libraries in the same workspace.

## Generate a library

Run `npx nx g @nrwl/angular:lib my-lib` to generate a library.

> You can also use any of the plugins above to generate libraries as well.

Libraries are shareable across libraries and applications. They can be imported from `@mf-app/mylib`.

## Development server
Run `npm start` for a dev server. Navigate to http://localhost:4200/.

Run `npx nx serve my-app` for a dev server. Navigate to http://localhost:port/. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `npx nx generate @schematics/angular:component my-component --project=my-app` to generate a new component.

## Build

Run `npx nx build my-app` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `npx nx test my-app` to execute the unit tests via [Jest](https://jestjs.io).

Run `npx nx affected:test` to execute the unit tests affected by a change.

## Running end-to-end tests

Run `ng e2e my-app` to execute the end-to-end tests via [Cypress](https://www.cypress.io).

Run `npx nx affected:e2e` to execute the end-to-end tests affected by a change.

## Understand your workspace

Run `npx nx dep-graph` to see a diagram of the dependencies of your projects.

## CLI generated
Run `npx nx generate @schematics/angular:module --name=myName --project=projectName --routing` tạo module

>eg: `npx nx generate @schematics/angular:module --name=remote-entry/features --project=vktd --routing`



Run `npx nx g @schematics/angular:service serviceName` tạo Service

>eg: `npx nx g @nrwl/angular:service toast`


Run `npx nx g @nrwl/angular:app projectName --mfe --mfeType=remote --port=5000 --host=hostName --routing=true` tạo application mfe

>eg: `npx nx g @nrwl/angular:app gallery --mfe --mfeType=remote --port=5000 --host=shell --routing=true`


Run `npx nx g @nrwl/angular:lib urlLib/LibName --tags=TageName` tạo Library

>eg: `npx nx g @nrwl/angular:lib dashboard/feature/home --tags=scope:dashboard,type:feature`


Run `npx nx generate @schematics/angular:component --name=myName --project=projectName --style=scss` tạo Component

>eg: `npx nx generate @schematics/angular:component --name=remote-entry/features/components/branch-info --project=vktd --style=scss`



Run `npx nx g @nrwl/workspace:remove myLibName` Remove Library


Run `npx nx g @nrwl/workspace:move --project my-feature-lib my-feature-lib` Move Library

## Lint All project

Run `npx nx affected --target=lint --all=true --exclude=shared-assets`
