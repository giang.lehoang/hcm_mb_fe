import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalaryCommitmentInitComponent } from './salary-commitment-init.component';

describe('SalaryCommitmentInitComponent', () => {
  let component: SalaryCommitmentInitComponent;
  let fixture: ComponentFixture<SalaryCommitmentInitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SalaryCommitmentInitComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SalaryCommitmentInitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
