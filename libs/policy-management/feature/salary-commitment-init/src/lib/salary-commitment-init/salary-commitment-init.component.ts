import {Component, Injector, OnDestroy, OnInit} from '@angular/core';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {
  ApproveReq,
  CommitmentRequest, EMPLOYEE_STATUS, EmployeeDetail, LookupCode, REGEX_CURRENCY,
  SalaryCommitmentInit,
  SalaryParams
} from '@hcm-mfe/policy-management/data-access/models';
import {SalaryCommitmentService, SalaryEvolutionService} from "@hcm-mfe/policy-management/data-access/service";
import * as moment from "moment/moment";
import {MapPipe} from "@hcm-mfe/shared/pipes/map";
import {formatToCurrency} from "@hcm-mfe/policy-management/helper";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";

@Component({
  selector: 'app-salary-commitment-init',
  templateUrl: './salary-commitment-init.component.html',
  styleUrls: ['./salary-commitment-init.component.scss']
})
export class SalaryCommitmentInitComponent extends BaseComponent implements OnInit, OnDestroy {
  salaryForm:SalaryCommitmentInit;
  isEdit: boolean;
  paramSearch: SalaryParams;
  employeeId:SalaryParams | undefined;
  isSubmit:boolean;
  loading:boolean;
  empStatus = EMPLOYEE_STATUS;
  fileName:string;
  docId:string;
  commitMentId: number | undefined;
  isApprove: boolean;
  description: string;
  hasFile:boolean | undefined;
  error:boolean;
  file: File | undefined;
  isVisible: boolean;
  pdfSrc: string;
  listCurrency: LookupCode[] = [];
  mapPipe:MapPipe;
  isBackdate: boolean;
  selected:any = {
    empCode:'',
    fullName:''
  };
  functionCode: string = FunctionCode.POL_SALARY_COMMITMENT;

  constructor(injector:Injector, readonly commitmentService:SalaryCommitmentService,
              readonly salaryEvolutionService:SalaryEvolutionService) {
    super(injector);
    this.mapPipe = new MapPipe();
    this.salaryForm = new SalaryCommitmentInit();
    this.isEdit = false;
    this.isSubmit = false;
    this.loading = false;
    this.fileName = '';
    this.error = false;
    this.paramSearch = new SalaryParams();
    this.isApprove = false;
    this.isVisible = false;
    this.pdfSrc = '';
    this.description = '';
    this.docId = '';
    this.isBackdate = false;
  }

  ngOnInit(): void {
    this.isApprove = this.route.snapshot.data['mode'] === 'approve';
    this.isEdit = this.route.snapshot.data['edit'];
    this.isBackdate = this.route.snapshot.data['mode'] === "backdate";
    const empInfo = JSON.parse(localStorage.getItem("employeeInfo"));
    this.checkEmpl(empInfo);
    if(this.isEdit || this.isApprove){
      const id = Number(localStorage.getItem('idCommitment'));
      this.commitMentId = id;
      this.loading = true;
      this.commitmentService.getDetailCommitment(id).subscribe(data => {
        if(data.data){
          this.loading = false;
          const commit = data.data;
          this.paramSearch.employeeId = commit.employeeId;
          this.getEmpInfo();
          this.paramSearch.positionName = commit.positionName;
          this.paramSearch.fullName = commit.fullName;
          this.paramSearch.orgName = commit.orgName;
          this.paramSearch.jobName = commit.jobName;
          this.salaryForm.fromDate = commit.fromDate;
          this.paramSearch.level = commit.levelName;
          this.salaryForm.toDate = commit.toDate;
          this.salaryForm.isBonus = commit.isBonus;
          this.salaryForm.curCode = commit.curCode;
          this.salaryForm.commitmentLevelCur = commit.amount;
          this.salaryForm.commitmentLevel = formatToCurrency(this.salaryForm.commitmentLevelCur, "currencyNumber", this.mapPipe);
          this.salaryForm.note = commit.note;
          this.fileName = commit.fileName;
          this.docId = commit.docId;
        }
        if(this.fileName) this.hasFile = true;
      } ,error => {
        this.loading = false;
        this.toastrCustom.error(error.message);
      })
    };
    this.getCurrencyData();
  }

  override ngOnDestroy() {
    super.ngOnDestroy();
    localStorage.removeItem("employeeInfoCommit");
  }

  onChange(event: any):void {
    if(event && event.length !== 0){
      this.setParam(event, true);
      this.getProcessEpl();
    } else {
      this.paramSearch = new SalaryParams();
      this.selected = null;
    }
  }

  setParam(emp:EmployeeDetail,isFullData?:boolean):void{
    if(isFullData) {
      this.paramSearch.fullName = emp.fullName;
      this.paramSearch.positionName = emp.positionName;
      this.paramSearch.orgName = emp.orgName;
      this.paramSearch.jobName = emp.jobName;
      this.paramSearch.level = emp.levelName?''+emp.levelName:'';
    }
    this.paramSearch.employeeCode = emp.empCode;
    this.paramSearch.employeeId = emp.empId;
    this.paramSearch.statusEmployee = Number(emp.flagStatus);
  }

  getEmpInfo():void{
    this.loading = true;
    this.salaryEvolutionService.getInfoEmpl(Number(this.paramSearch.employeeId)).subscribe(data => {
      const emp = (data.data.listData[0] as EmployeeDetail);
      this.setParam(emp);
      this.loading = false;
    }, error => {
      this.loading = false;
      this.toastrCustom.error(error.message);
    })
  }

  onCancel():void {
    this.router.navigate(['/policy-management/salary-process/salary-commitment']);
  }

  selectFile(file:{file:File, fileName:string}){
    if(file){
      this.salaryForm.file = file.file;
      this.file = file.file;
      this.fileName = file.fileName;
    } else {
      this.salaryForm.file = undefined;
      this.fileName = '';
    }
  }

  saveData():void{
    if(!this.paramSearch.employeeId){
      this.toastrCustom.error(this.translate.instant('polManagement.message.employeeSelect'));
      return;
    }
    this.isSubmit = true;
    if(!this.salaryForm.commitmentLevel || !this.salaryForm.fromDate) return;
    this.salaryForm.note = this.salaryForm.note.trim();
    const request:CommitmentRequest = {
      employeeId: this.paramSearch.employeeId,
      amount: this.formatToNumber(this.salaryForm.commitmentLevelCur),
      note: this.salaryForm.note,
      fromDate: moment(this.salaryForm.fromDate).format('YYYY-MM-DD'),
      toDate: this.salaryForm.toDate ? moment(this.salaryForm.toDate).format('YYYY-MM-DD') : '',
      bonus: this.salaryForm.isBonus,
      curCode: this.salaryForm.curCode
    }

    if(this.isEdit && this.commitMentId){
      request.salaryCommitmentId = this.commitMentId;
    }

    if(this.isEdit){
      if(!this.fileName && this.hasFile){
        request.deleteFile = true;
      } else {
        request.deleteFile = false;
      }
    }
    console.log(request);

    const formData = new FormData();
    formData.append("request", new Blob([JSON.stringify(request)], {
      type: 'application/json',
    }));


    if(this.salaryForm.file){
      formData.append('file',this.salaryForm.file);
    }
    this.loading = true;
    this.commitmentService.initCommitment(formData).subscribe(data => {
      this.toastrCustom.success();
      this.onCancel();
      this.loading = false;
    }, error => {
      this.toastrCustom.error(error.message);
      this.loading = false;
    })
  }

  getProcessEpl():void{
    const params = {
      employeeCode: this.paramSearch.employeeCode,
      startRecord: 0,
      pageSize:1000
    }

    this.salaryEvolutionService.getListProcessEmp(params).subscribe(
      (data) => {
        if(data.data?.listData && data.data?.listData.length > 0){
          const wp = data.data.listData.sort((a:any,b:any):number => {
            const dateA = moment(a.fromDate,'DD-MM-YYYY').toDate();
            const dateB = moment(b.fromDate,'DD-MM-YYYY').toDate()
            if(dateA > dateB){
              return -1;
            } else if(dateA < dateB){
              return 1;
            } else {
              return 0;
            }
          });
          this.salaryForm.fromDate = ''+ moment(wp[0]?.fromDate,'DD-MM-YYYY').toDate();
        }
      },
      (error) => {
        this.toastrCustom.error(error?.message);
      }
    );
  }

  buildApproveRequest(action: 'APPROVE' | 'REJECT'):ApproveReq {

    const request:ApproveReq = {
      action: action,
      description: this.description,
      ids: []
    }
    request.ids.push(Number(this.commitMentId));
    return request;
  }

  approveSalaryCommit(isApprove:boolean):void {
    this.isSubmit = true;
    if (!isApprove && !this.description) {
      return;
    }
    this.loading = true;
    const action = isApprove ? "APPROVE" : "REJECT";
    const request = this.buildApproveRequest(action);
    this.commitmentService.approveData(request,this.isBackdate).subscribe(data => {
      this.toastrCustom.success();
      this.loading = false;
      this.router.navigate(['/policy-management/salary-process/salary-commit-approve']);
    }, error => {
      this.toastrCustom.error(error.message);
    });
  }

  onChangeDate(isFromDate:boolean):void {
    this.error = false;
    if(this.salaryForm.fromDate && this.salaryForm.toDate){
      const fromDate = new Date(this.salaryForm.fromDate).setHours(0, 0, 0, 0);
      const toDate = new Date(this.salaryForm.toDate).setHours(0, 0, 0, 0);

      if(fromDate >= toDate){
        this.toastrCustom.error(this.translate.instant('polManagement.message.toDate'));
        this.clearValue(isFromDate ? 'fromDate' : 'toDate');
      }
    }
  }

  clearValue(filed: 'fromDate' | 'toDate'): void{
    setTimeout(() => {
      this.salaryForm[filed] = '';
    }, 500)
  }

  downloadFile(docId:string, docName:string){
    this.salaryEvolutionService.downloadFile(docId).subscribe((data) => {
      const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.pdf' });
      this.file = new File([blob], docName);
      this.onFileSelected();
    });
  }

  handleCancel():void {
    this.isVisible = false;
  }

  onFileSelected():void {
    const reader = new FileReader();
    reader.onload = (e: any) => {
      this.pdfSrc = e.target.result;
    };
    if(this.file) reader.readAsArrayBuffer(this.file);
    this.isVisible = true;
  }

  viewFile():void {
    if(this.fileName){
      if(!this.file) {
        this.downloadFile(this.docId, this.fileName);
      } else this.onFileSelected();
    }
  }

  getCurrencyData():void {
    const params = {
      lookupCode: 'LOAI_TIEN_TE'
    }
    this.salaryEvolutionService.getListCurency(params).subscribe(
      (data) => {
        this.listCurrency = data.data;
      },
      (error) => {
        this.toastrCustom.error(error?.error?.message);
      }
    );
  }

  onKeyPressLCB(event:KeyboardEvent){
    const pattern = REGEX_CURRENCY;
    if (!pattern.test(event.key)) {
      event.preventDefault();
      return;
    }
  }


  convertToCur():void{
    this.salaryForm.commitmentLevelCur = this.salaryForm.commitmentLevel;
    this.salaryForm.commitmentLevel = formatToCurrency(this.formatToNumber(this.salaryForm.commitmentLevelCur), "currencyNumber", this.mapPipe);
  }

  convertToNumber():void{
    this.salaryForm.commitmentLevel = this.salaryForm.commitmentLevelCur;
  }
  formatToNumber(value:any){
    value = String(value);
    if(value && value.includes(",")){
      value = value.replaceAll(",","")
    }
    return +value;
  }
  checkEmpl(empInfo: any){
    if(empInfo){
      this.setParam(empInfo,true);
      this.selected.empCode = empInfo.empCode;
      this.selected.fullName = empInfo.fullName;
      if(this.route.snapshot.data['mode'] === "create"){
        this.getProcessEpl();
      }
    } else {
      this.selected = null;
    }
  }
  findStatus(key: number|string){
    if(key){
      return this.empStatus.find(item => item.key === key)?.value;
    } else {
      return '';
    }
  }
}
