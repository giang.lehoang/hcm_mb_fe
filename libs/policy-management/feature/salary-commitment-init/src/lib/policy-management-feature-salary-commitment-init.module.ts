import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SalaryCommitmentInitComponent} from "./salary-commitment-init/salary-commitment-init.component";
import {NzGridModule} from "ng-zorro-antd/grid";
import {NzCardModule} from "ng-zorro-antd/card";
import {SharedUiMbEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/mb-employee-data-picker";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {TranslateModule} from "@ngx-translate/core";
import {RouterModule} from "@angular/router";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {SharedUiMbInputFileModule} from "@hcm-mfe/shared/ui/mb-input-file";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {PdfViewerModule} from "ng2-pdf-viewer";
import {NzModalModule} from "ng-zorro-antd/modal";
import {FormsModule} from "@angular/forms";
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";

@NgModule({
  imports: [CommonModule, PdfViewerModule,
    RouterModule.forChild([
      {
        path: '',
        component: SalaryCommitmentInitComponent
      }
    ]), SharedUiMbInputFileModule, NzModalModule,
    NzGridModule, NzCardModule, SharedUiMbEmployeeDataPickerModule, SharedUiMbInputTextModule,
    TranslateModule, SharedUiMbDatePickerModule, SharedUiMbButtonModule, SharedUiLoadingModule, SharedUiMbSelectModule, FormsModule, NzCheckboxModule],
  declarations: [SalaryCommitmentInitComponent],
  exports: [SalaryCommitmentInitComponent]
})
export class PolicyManagementFeatureSalaryCommitmentInitModule {}
