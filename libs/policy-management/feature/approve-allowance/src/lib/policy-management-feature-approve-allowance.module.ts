import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {TranslateModule} from "@ngx-translate/core";
import {ApproveAllowanceComponent} from "./approve-allowance/approve-allowance.component";
import {RouterModule} from "@angular/router";
import {SharedUiMbEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/mb-employee-data-picker";
import {NzCardModule} from "ng-zorro-antd/card";
import {FormsModule} from "@angular/forms";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {NzGridModule} from "ng-zorro-antd/grid";
import {NzIconModule} from "ng-zorro-antd/icon";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {NzTagModule} from "ng-zorro-antd/tag";

@NgModule({
  imports: [CommonModule, RouterModule.forChild([
    {
      path: '',
      component: ApproveAllowanceComponent,
    },
  ]), FormsModule, SharedUiLoadingModule, SharedUiMbButtonModule, TranslateModule, SharedUiMbEmployeeDataPickerModule, NzCardModule, SharedUiMbInputTextModule, NzGridModule, NzIconModule, SharedUiMbTableWrapModule, SharedUiMbTableModule, NzTagModule],
  declarations: [ApproveAllowanceComponent],
  exports: [ApproveAllowanceComponent]
})
export class PolicyManagementFeatureApproveAllowanceModule {}
