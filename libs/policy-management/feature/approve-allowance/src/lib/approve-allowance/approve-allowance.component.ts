import {Component, Injector, OnInit} from "@angular/core";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {AllowanceDevelopmentService} from "@hcm-mfe/policy-management/data-access/service";
import {FormModalShowTreeUnitComponent} from "@hcm-mfe/shared/ui/form-modal-show-tree-unit";
import {
  IListAllowanceApprove,
  IParamAllowanceApprove,
  IParamApprove
} from "@hcm-mfe/policy-management/data-access/models";
import {MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";

@Component({
  selector: 'app-approve-allowance',
  templateUrl: './approve-allowance.component.html',
  styleUrls: ['./approve-allowance.component.scss'],
})
export class ApproveAllowanceComponent extends BaseComponent implements OnInit {
  employee: any = "";
  orgName: any = "";
  description: any = "";
  isSubmit: boolean = false;
  isBackDate: boolean = false;
  heightTable = { x: '100vw', y: '25em'};
  tableConfig: MBTableConfig = new MBTableConfig();
  pagination = new Pagination();
  functionCode: string = '';
  checked: NzSafeAny[] = [];
  dataTable: IListAllowanceApprove[] = [];
  params: IParamAllowanceApprove = {
    empId: "",
    organizationId: "",
    createdBy: "",
    size: this.pagination.pageSize,
    pageNumber: 0,
    flagStatuses: [2]
  };
  constructor(
    injector : Injector,
    private readonly allowanceDevService: AllowanceDevelopmentService
  ) {
    super(injector);
    this.isBackDate = this.route.snapshot.data['mode'] === 'BD';
    this.functionCode = !this.isBackDate ? FunctionCode.ALLOWANCE_DEVELOPMENTS_APPROVE : FunctionCode.ALLOWANCE_DEVELOPMENTS_APPROVE_BD;
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${ !this.isBackDate ? FunctionCode.ALLOWANCE_DEVELOPMENTS_APPROVE : FunctionCode.ALLOWANCE_DEVELOPMENTS_APPROVE_BD}`);
    console.log(this.objFunction);
  }
  private initTable(): void {

    this.tableConfig = {
      headers: [
        {
          title: 'polManagement.table.employeeCode',
          field: 'employeeCode',
          width: 120,
          thClassList: ['text-center'],
          fixed: window.innerWidth > 1024,
          fixedDir: 'left',
        },
        {
          title: 'polManagement.table.fullName',
          field: 'fullName',
          thClassList: ['text-center'],
          width: 110,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left',
        },
        {
          title: 'polManagement.table.orgName',
          field: 'orgName',
          thClassList: ['text-center'],
          width: 120,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'polManagement.table.jobName',
          field: 'jobName',
          thClassList: ['text-center'],
          width: 110,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'polManagement.table.fromDate',
          field: 'fromDate',
          thClassList: ['text-center'],
          pipe: 'date',
          width: 90,
        },
        {
          title: 'polManagement.table.toDate',
          field: 'toDate',
          thClassList: ['text-center'],
          width: 90,
          fixed: window.innerWidth > 1024,
          pipe: 'date',
        },
        {
          title: 'polManagement.table.allowanceTypeName',
          field: 'allowanceTypeName',
          thClassList: ['text-center'],
          width: 110,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'polManagement.table.amount',
          field: 'amount',
          thClassList: ['text-center'],
          tdClassList: ['text-right'],
          width: 80,
          pipe: 'currencyNumber',
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'polManagement.table.curCode',
          field: 'currency',
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          width: 50,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'polManagement.table.note',
          field: 'note',
          thClassList: ['text-center'],
          width: 130,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'polManagement.table.level',
          field: 'levelName',
          thClassList: ['text-center'],
          width: 80,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'polManagement.table.positionName',
          field: 'posName',
          thClassList: ['text-center'],
          width: 150,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'polManagement.table.createdBy',
          field: 'createdBy',
          thClassList: ['text-center'],
          width: 100,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'polManagement.table.salaryRange',
          field: 'salaryRange',
          thClassList: ['text-center'],
          width: 60,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'polManagement.table.regionName',
          field: 'regionName',
          thClassList: ['text-center'],
          width: 100,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'polManagement.table.organizationScale',
          field: 'organizationScale',
          thClassList: ['text-center'],
          width: 130,
          fixed: window.innerWidth > 1024,
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: 10,
      pageIndex: 1,
      showCheckbox: true,
      showFrontPagination: false
    };
  }
  ngOnInit(): void {
    this.initTable();
    this.initData();
  }
  approveAll() {
    if (!this.dataTable.length) {
      return;
    }
    this.isLoading = true;
    this.params.organizationId = this.params.organizationId ? this.params.organizationId : null;
    this.params.empId = this.params.empId ? this.params.empId : null;
    this.allowanceDevService.approveAllowanceProcessAll(this.params, this.isBackDate).subscribe(() => {
      this.toastrCustom.success(this.translate.instant('common.notification.isApprove'));
      this.initData();
      this.isLoading = false;
    }, (err) => {
      if(err.status === 400) {
        this.toastrCustom.error(err.message);
      } else {
        this.toastrCustom.error(this.translate.instant('common.notification.error'));
      }
      this.isLoading = false;
    })
  }

  initData() {
    this.isLoading = true;
    this.params.organizationId = this.params.organizationId ? this.params.organizationId : '';
    this.params.empId = this.params.empId ? this.params.empId : '';
    this.allowanceDevService.getListAllowanceProcessApprove(this.params, this.isBackDate).subscribe((res) => {
      if(res.data.data.length) {
        this.dataTable = res.data.data;
        this.tableConfig.total = res.data.totalElements;
      } else {
        this.dataTable = [];
      }
      this.isLoading = false;
    }, () => {
      this.isLoading = false;
    })
  }
  showModalOrg(event: any): void {
    const modal = this.modal.create({
      nzWidth: '85vw',
      nzTitle: '',
      nzContent: FormModalShowTreeUnitComponent,
      nzFooter: null,
    });
    modal.afterClose.subscribe((result) => {
      if (result) {
        this.orgName = result.orgName;
        this.params.organizationId = result.orgId;
      }
    });
  }
  clearOrgInput() {
    this.orgName = '';
    this.params.organizationId = '';
  }
  search(event?: any) {
    if (event) {
      this.pagination.pageNumber = event - 1;
      this.params.pageNumber = event - 1;
    }
    this.initData();
  }

  approveOrRejectAllowanceProcess(isApprove: boolean) {
    if(this.checked?.length === 0){
      this.toastrCustom.error(this.translate.instant("polManagement.message.plSelectRecord"));
      return;
    }
    if(!isApprove && !this.description){
      this.isSubmit = true;
      return;
    }

    const action = isApprove ? "APPROVE" : "REJECT";
    const request = this.buildApproveRequest(action);
    this.isLoading = true;
    this.allowanceDevService.approveAllowanceProcess(request, this.isBackDate).subscribe(() => {
      this.toastrCustom.success( isApprove ? this.translate.instant('common.notification.isApprove') : this.translate.instant('common.notification.reject'));
      this.search();
      this.checked = [];
      this.description = "";
      this.isSubmit = false;
      this.isLoading = false;
    }, (err) => {
      if(err.status === 400) {
        this.toastrCustom.error(err.message);
      } else {
        this.toastrCustom.error(this.translate.instant('common.notification.error'));
      }
      this.isLoading = false;
    })
  }
  buildApproveRequest(action: string):IParamApprove {
    const request:IParamApprove = {
      action: action,
      reason: this.description,
      prAllowanceProcessIds: []
    }

    this.checked?.forEach((item: IListAllowanceApprove) => {
      request.prAllowanceProcessIds.push(item.prAllowanceProcessId);
    });

    return request;
  }
  onChange(event: any) {
    event ? this.params.empId = event.employeeId : this.params.empId = '';
    if(event){
      this.search();
    }
  }

  onClickItem(allowanceInfo: IListAllowanceApprove) {
    this.router.navigate(['/policy-management/approve-allowance/detail']);
    localStorage.setItem("objectAllowance", JSON.stringify(allowanceInfo));
    if(this.isBackDate) {
      localStorage.setItem("mode", JSON.stringify(this.isBackDate));
    }
  }
  override triggerSearchEvent(): void {
    this.search();
  }
}
