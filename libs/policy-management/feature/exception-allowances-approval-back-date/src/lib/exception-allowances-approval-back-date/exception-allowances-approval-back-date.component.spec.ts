import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExceptionAllowancesApprovalBackDateComponent } from './exception-allowances-approval-back-date.component';

describe('ExceptionAllowancesApprovalBackDateComponent', () => {
  let component: ExceptionAllowancesApprovalBackDateComponent;
  let fixture: ComponentFixture<ExceptionAllowancesApprovalBackDateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExceptionAllowancesApprovalBackDateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExceptionAllowancesApprovalBackDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
