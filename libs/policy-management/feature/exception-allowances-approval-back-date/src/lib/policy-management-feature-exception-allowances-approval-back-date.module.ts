import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExceptionAllowancesApprovalBackDateComponent } from './exception-allowances-approval-back-date/exception-allowances-approval-back-date.component';
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: ExceptionAllowancesApprovalBackDateComponent,
      },
    ]),
  ],
  declarations: [
    ExceptionAllowancesApprovalBackDateComponent
  ],
})
export class PolicyManagementFeatureExceptionAllowancesApprovalBackDateModule {}
