import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListReceivingExceptionAllowancesComponent } from './list-receiving-exeption-allowances/list-receiving-exception-allowances.component';
import {RouterModule} from "@angular/router";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzGridModule} from "ng-zorro-antd/grid";
import {TranslateModule} from "@ngx-translate/core";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzResizableModule} from "ng-zorro-antd/resizable";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {NzTabsModule} from "ng-zorro-antd/tabs";
import {FormsModule} from "@angular/forms";
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {NzTagModule} from "ng-zorro-antd/tag";
import {SharedPipesFormatDateModule} from "@hcm-mfe/shared/pipes/format-date";
import {NzPopconfirmModule} from "ng-zorro-antd/popconfirm";
import {SharedUiMbEmployeeInfoModule} from "@hcm-mfe/shared/ui/mb-employee-info";
import {NzCardModule} from "ng-zorro-antd/card";
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: ListReceivingExceptionAllowancesComponent
      }
    ]),
    SharedUiLoadingModule,
    SharedUiMbButtonModule,
    NzGridModule,
    TranslateModule,
    NzTableModule,
    NzResizableModule,
    NzDropDownModule,
    NzTabsModule,
    FormsModule,
    // NzTransitionPatchModule,
    NzCheckboxModule,
    SharedUiMbTableWrapModule,
    SharedUiMbTableModule,
    NzTagModule,
    SharedPipesFormatDateModule,
    NzPopconfirmModule,
    SharedUiMbEmployeeInfoModule,
    NzCardModule,
    NzToolTipModule
  ],
  declarations: [
    ListReceivingExceptionAllowancesComponent
  ],
  exports: [
    ListReceivingExceptionAllowancesComponent
  ]

})
export class PolicyManagementFeatureListReceivingExeptionAllowancesModule {}
