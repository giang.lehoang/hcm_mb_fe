import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListReceivingExceptionAllowancesComponent } from './list-receiving-exception-allowances.component';

describe('ListReceivingExeptionAllowancesComponent', () => {
  let component: ListReceivingExceptionAllowancesComponent;
  let fixture: ComponentFixture<ListReceivingExceptionAllowancesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListReceivingExceptionAllowancesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListReceivingExceptionAllowancesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
