import {Component, Injector, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {HTTP_STATUS_CODE, Mode, userConfig} from "@hcm-mfe/shared/common/constants";
import {NzModalRef} from "ng-zorro-antd/modal";
import {getInnerWidth, renderFlagStatus} from "@hcm-mfe/policy-management/helper";
import {
  ModalAddFormReceivingExceptionAllowancesComponent,
  ModalDocumentReceivingExceptionAllowancesComponent
} from "@hcm-mfe/policy-management/ui/modal-attack";
import {
  ConstantColor,
  flagStatus,
  IListReceivingExceptionAllowances,
  IParamSearchListReceivingException
} from "@hcm-mfe/policy-management/data-access/models";
import {ListReceivingExceptionAllowancesService, SharedService} from "@hcm-mfe/policy-management/data-access/service";
import {Subscription} from "rxjs";
import {BaseResponse, MBTableConfig} from "@hcm-mfe/shared/data-access/models";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";

@Component({
  selector: 'hcm-mfe-list-receiving-exception-allowances',
  templateUrl: './list-receiving-exception-allowances.component.html',
  styleUrls: ['./list-receiving-exception-allowances.component.scss']
})
export class ListReceivingExceptionAllowancesComponent extends BaseComponent implements OnInit {

  tableConfig!: MBTableConfig;
  @ViewChild('actionTmpl', {static: true}) action!: TemplateRef<NzSafeAny>;
  @ViewChild('actionFlagStatus', {static: true}) actionFlagStatus!: TemplateRef<NzSafeAny>;
  @ViewChild('tplTranFormDate', {static: true}) tplTranFormDate!: TemplateRef<NzSafeAny>;
  @ViewChild('tplTranToDate', {static: true}) tplTranToDate!: TemplateRef<NzSafeAny>;
  renderFlagStatus = renderFlagStatus;
  heightTable = { x: '100vw', y: '25em'};
  functionCode: string;
  typeShowDetail = false;

  paramSearch: IParamSearchListReceivingException = {
    employeeCode: null,
    fullName: null,
    orgId: null,
    prAllowanceType: null,
    flagStatus: null,
    page: 0,
    size: userConfig.pageSize,
  }

  searchResult: IListReceivingExceptionAllowances[] | undefined;
  override isLoading = false;
  modalRef: NzModalRef | undefined;
  private readonly subs: Subscription[] = [];
  isSubmitted = false;

  constructor(
    injector: Injector,
    private sharedService: SharedService,
    private readonly listReceivingExceptionAllowancesService: ListReceivingExceptionAllowancesService
  ) {
    super(injector);
    this.modalRef = undefined;
    this.tableConfig = new MBTableConfig();
    this.objFunction = this.sessionService.getSessionData(
      `FUNCTION_${FunctionCode.RECEIVING_EXCEPTION_ALLOWANCE}`
    );
    this.functionCode = FunctionCode.RECEIVING_EXCEPTION_ALLOWANCE;
  }

  // showModalUploadFile(footerTmpl: TemplateRef<any>) {
  //   this.modalRef = this.modal.create({
  //     nzWidth: getInnerWidth(3.5),
  //     nzTitle: this.translate.instant('polManagement.targetCategory.label.atribute.UploadFileExel'),
  //     nzContent: ModalDocumentReceivingExceptionAllowancesComponent,
  //     nzComponentParams: {
  //       mode: Mode.ADD,
  //       url: 'urlTarget',
  //       fileName: 'TEMPLATE_CHI_TIEU'
  //     },
  //     nzFooter: footerTmpl,
  //   });
  //   // this.modalRef.afterClose.subscribe((result) => (result?.refresh ? this.getListTargetCategory(this.objectSearch) : ''));
  // }

  search(param: IParamSearchListReceivingException) {
    this.isLoading = true;
    const listDataResponse = this.listReceivingExceptionAllowancesService.getListReceivingExceptionAllowances(param)
      .subscribe((res: BaseResponse) => {
        if (res.status === HTTP_STATUS_CODE.OK) {
          this.searchResult = res.data.content || [];
          this.isLoading = false;
        }
      }, (err) => {
        this.isLoading = false;
        this.toastrCustom.error(err.message);
      });
    this.subs.push(listDataResponse);
  }

  doSearch(pageIndex: number) {
    this.isLoading = true;
    this.tableConfig.pageIndex = pageIndex;
    this.paramSearch.page = pageIndex ? pageIndex - 1 : this.paramSearch.page;

    const doSearchData = this.listReceivingExceptionAllowancesService.getListReceivingExceptionAllowances(this.paramSearch)
      .subscribe((res: BaseResponse )=> {
      if (res.data.content) {
        this.searchResult = res.data.content;
        this.tableConfig.needScroll = true;
        this.tableConfig.total = res.data.totalElements;
      }
    }, (err ) => {
        this.toastrCustom.error(this.translate.instant("polManagement.message.showMessageError"));
        this.isLoading = false;

      }, () => {
        this.isLoading = false;
      });

    this.subs.push(doSearchData);
  }


  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'polManagement.table.employeeCode',
          field: 'employeeCode',
          width: 120,
        },
        {
          title: 'polManagement.table.fullName',
          field: 'fullName',
        },
        {
          title: 'polManagement.table.orgName',
          field: 'orgName',
        },
        {
          title: 'polManagement.table.jobName',
          field: 'jobName',
        },
        {
          title: 'polManagement.table.positionName',
          field: 'posName',
        },
        {
          title: 'polManagement.table.fromDate',
          field: 'fromDate',
          tdTemplate: this.tplTranFormDate,
          width: 100,
        },
        {
          title: 'polManagement.table.toDate',
          field: 'toDate',
          tdTemplate: this.tplTranToDate,
          width: 100,
        },
        {
          title: 'polManagement.table.prAllowanceTypeName',
          field: 'prAllowanceTypeName',
        },
        {
          title: 'polManagement.table.status',
          field: 'flagStatus',
          tdTemplate: this.actionFlagStatus,
          width: 120,
        },
        {
          title: '',
          tdTemplate: this.action,
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          width: 120,
          fixedDir: 'right',
          fixed: window.innerWidth > 1024,
        },
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.paramSearch.size,
      pageIndex: 1,
      widthIndex: '70px'
    };
  }

  ngOnInit(): void {
    this.initTable();
    this.doSearch(1);
  }

  deleteItem(id: number, fromDate: string): void {
    // const currentDay = new Date();
    // if(moment(fromDate) >= moment([currentDay.getFullYear(), currentDay.getMonth() - 1, 26])){
      this.deletePopup.showModal(() => {
        const deleteItem = this.listReceivingExceptionAllowancesService.deleteReceivingExceptionAllowancesItem(id).subscribe(res => {
          if(res){
            this.toastrCustom.success(this.translate.instant('polManagement.validate.notificationDeleteSuccess'));
            this.doSearch(1);
          }
        }, err => this.toastrCustom.error(err.message))
        this.subs.push(deleteItem);
      });
    // }else {
    //   this.toastrCustom.error(this.translate.instant('polManagement.validate.errPeriodSaveOrDelete'));
    // }

  }

  onDoubleClickView(event: IListReceivingExceptionAllowances, footerTmpl: TemplateRef<any> ){
    this.typeShowDetail = true;
    this.showModalUpdate(event.prAllowanceExtendListId, footerTmpl, undefined, true);
  }

  showModalUpdate(id: number | undefined, footerTmpl: TemplateRef<any>, fromDate: string | undefined, typeShowInfo: boolean) {
    const currentDay = new Date();
    // if(moment(fromDate) >= moment([currentDay.getFullYear(), currentDay.getMonth() - 1, 26])){
      this.modalRef = this.modal.create({
        nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
        nzTitle: this.translate.instant(this.translate.instant('polManagement.table.detailAllowanceType')),
        nzContent: ModalAddFormReceivingExceptionAllowancesComponent,
        nzFooter: footerTmpl,
        nzComponentParams: {
          mode: Mode.EDIT,
          prAllowanceExtendListId: id,
          typeShowInfo
        },
      });
      this.modalRef.afterClose.subscribe(result => {
        if(result?.refresh){
          this.doSearch(1);
        }
        this.typeShowDetail = false;
      });
    // }else {
    //   this.toastrCustom.error(this.translate.instant('polManagement.validate.errPeriodSaveOrDelete'));
    // }

  }

  showModalAdd(footerTmpl: TemplateRef<any>, mode?: Mode) {
    this.modalRef = this.modal.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
      nzTitle: this.translate.instant(this.translate.instant('polManagement.table.detailAllowanceType')),
      nzComponentParams: {
        mode: Mode.ADD,
      },
      nzContent: ModalAddFormReceivingExceptionAllowancesComponent,
      nzFooter: footerTmpl
    });
    this.modalRef.afterClose.subscribe(result => {
      if(result?.refresh) {
        this.doSearch(1);
      }
      this.typeShowDetail = false;
    });
  }

  onReSearch($event: IParamSearchListReceivingException) {
    const { employeeCode, orgId, prAllowanceType} = $event;
    this.paramSearch = {
      flagStatus: $event.statusRecord,
      employeeCode,
      orgId,
      prAllowanceType,
      page: 0,
      size: this.paramSearch.size,
    }
    this.doSearch(1);
  }

  getFlagStatusWithKey(key: string) {
    return flagStatus.STATUS_LIST.find(el => Number(el.key) === Number(key))?.value;
  }

  override ngOnDestroy() {
    super.ngOnDestroy();
    this.modalRef?.destroy();
    this.subs.forEach((s) => s.unsubscribe());
    this.sharedService.userToUpdateEmployee = undefined;
  }
}
