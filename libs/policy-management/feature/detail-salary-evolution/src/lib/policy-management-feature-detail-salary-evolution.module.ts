import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { NzTableModule } from 'ng-zorro-antd/table';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzFormModule } from 'ng-zorro-antd/form';
import { SharedUiMbEmployeeInfoModule } from '@hcm-mfe/shared/ui/mb-employee-info';
import { SharedUiMbTableWrapModule } from '@hcm-mfe/shared/ui/mb-table-wrap';
import { SharedUiMbTableModule } from '@hcm-mfe/shared/ui/mb-table';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { DetailSalaryEvolutionComponent } from './detail-salary-evolution/detail-salary-evolution.component';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { SharedUiMbDatePickerModule } from '@hcm-mfe/shared/ui/mb-date-picker';
import { SharedUiMbEmployeeDataPickerModule } from '@hcm-mfe/shared/ui/mb-employee-data-picker';
import { MatChipsModule } from '@angular/material/chips';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { MapPipe } from '@hcm-mfe/shared/pipes/map';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
@NgModule({
  imports: [CommonModule, FormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: DetailSalaryEvolutionComponent
      }
    ]),
    TranslateModule, NzToolTipModule, SharedUiMbButtonModule, NzTableModule, SharedUiMbInputTextModule, SharedUiMbSelectModule, NzCardModule, NzFormModule, SharedUiMbEmployeeInfoModule, SharedUiMbTableWrapModule, SharedUiMbTableModule, NzTagModule, NzRadioModule, SharedUiMbDatePickerModule, SharedUiMbEmployeeDataPickerModule, MatChipsModule, NzIconModule, NzModalModule, PdfViewerModule, SharedUiLoadingModule],
  declarations: [DetailSalaryEvolutionComponent],
  exports: [DetailSalaryEvolutionComponent],
  providers:[MapPipe]
})
export class PolicyManagementFeatureDetailSalaryEvolutionModule {}
