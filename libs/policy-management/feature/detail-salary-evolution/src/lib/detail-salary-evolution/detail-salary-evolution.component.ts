import { ChangeDetectorRef, Component, Injector, OnInit } from '@angular/core';
import {
  ApproveResquest, EMPLOYEE_STATUS, EMPLOYEE_TYPE, EmployeeInfo,
  GradeLCBModel,
  groupSalary,
  InitSalaryModel, LookupCode,
  QCLModel,
  RankLCBModel, REGEX_CURRENCY,
  SalaryParams
} from '@hcm-mfe/policy-management/data-access/models';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { SalaryEvolutionService } from '@hcm-mfe/policy-management/data-access/service';
import * as moment from 'moment';
import { MapPipe } from '@hcm-mfe/shared/pipes/map';
import {FunctionCode} from "@hcm-mfe/shared/common/enums";

@Component({
  selector: 'app-detail-salary-evolution',
  templateUrl: './detail-salary-evolution.component.html',
  styleUrls: ['./detail-salary-evolution.component.scss']
})
export class DetailSalaryEvolutionComponent extends BaseComponent implements OnInit {
  employeeId: string = '';
  paramSearch: SalaryParams = new SalaryParams();
  dataObject: InitSalaryModel = new InitSalaryModel();
  isLoadingPage: boolean = false;
  listGroupSalary = groupSalary.LIST_GROUP_SALARY;
  listQCL: QCLModel[] = [];
  listRankLCB: RankLCBModel[] | undefined = [];
  listGradeLCB: GradeLCBModel[] = [];
  listDataQCL: any = [];
  fileName: string = '';
  isEdit: boolean = false;
  isSubmit: boolean = false;
  approve: boolean;
  description:string;
  isCall: boolean = false;
  employeeType = EMPLOYEE_TYPE;
  empStatus = EMPLOYEE_STATUS;
  isAddNew: boolean = false;
  file: any;
  isVisible: boolean = false;
  pdfSrc: string = '';
  backdate: boolean;
  listCurrency: LookupCode[] = [];
  selected:any = {
    empCode:'',
    fullName:''
  };
  functionCode:string = FunctionCode.POL_SALARY_EVOLUTION;
  diffMonth: number = 0;
  constructor(
    private salaryEvolutionService: SalaryEvolutionService,
    private mapPipe: MapPipe = new MapPipe(),
  private cdr: ChangeDetectorRef,
    injector: Injector
  ) {
    super(injector);
    this.approve = false;
    this.description = "";
    this.backdate = false;
    this.objFunction = JSON.parse(localStorage.getItem("objFunction"));
    this.approve = this.route.snapshot.data['mode'] === "approve";
    this.isAddNew = this.route.snapshot.data['mode'] === "create";
  }
  ngOnInit(): void {
    this.getDataQCL();
    this.getCurrencyData();
    const employeeInfo = JSON.parse(localStorage.getItem("employeeInfo"));
    if(this.approve){
      this.backdate = localStorage.getItem("salaryBackdate") === 'true' ? true : false;
    }
    this.checkEmplInfo(employeeInfo);
  }
  ngOnDestroy() {
    localStorage.removeItem("employeeInfo");
    localStorage.removeItem("objFunction");
    localStorage.removeItem("salaryBackdate");
  }

  onChangeType(value: any) {
    this.dataObject.employeeType = value;
    this.dataObject.salaryType= 'LHQ';
  }
  onChange(event: any) {
    if(event && event?.length !== 0){
      this.paramSearch = event;
      this.paramSearch.levelName = event?.levelName;
      this.paramSearch.statusEmployee = event?.flagStatus;
      this.paramSearch.employeeCode= event.empCode?event.empCode:this.paramSearch.employeeCode;
      this.paramSearch.employeeId  = event?.empId?event?.empId:this.paramSearch.employeeId;
       this.diffMonth = this.diffEmpDate(event);
      if(event.joinCompanyDate){
        if(this.diffMonth > 6 ) {
          this.getScored();
        } else {
          this.dataObject.periodCurFactor = '1';
          this.dataObject.periodCurPoint = '';
        }
        if(this.isAddNew){
          this.getProcessEpl();
        }
      }
    } else{
      this.paramSearch = new SalaryParams();
      this.dataObject =  new InitSalaryModel();
      this.isCall = false;
      this.fileName = '';
      this.selected = null;
    }
  }
  getDataQCL() {
    this.salaryEvolutionService.getQCL().subscribe(
      (data) => {
        this.listQCL = data.data;
        this.listQCL.forEach(item => {
          item.salaryVersionNameTmp = item.flagEnabled === "N"?item.salaryVersionName+' (KSD)':item.salaryVersionName;
        });
        this.dataObject.salaryVersionId = data.data[0]?.salaryVersionId;
        this.getLCBInfo();
      },
      (error) => {
        this.toastrCustom.error(error?.error?.message);
      }
    );
  }
  getCurrencyData() {
    const params = {
      lookupCode: 'LOAI_TIEN_TE'
    }
    this.salaryEvolutionService.getListCurency(params).subscribe(
      (data) => {
        this.listCurrency = data.data;
      },
      (error) => {
        this.toastrCustom.error(error?.error?.message);
      }
    );
  }
  getLCBInfo() {
    const params = {
      salaryVersionId: this.dataObject.salaryVersionId
    }
    this.salaryEvolutionService.getRankLCB(params).subscribe(
      (data) => {
        this.listDataQCL = data.data;
        this.listGradeLCB = this.listDataQCL[0]?.salaryGradeOfMatrixResponses;
          this.onChangeGrade(true);
      },
      (error) => {
        this.toastrCustom.error(error?.error?.message);
      }
    );
  }
  onChangeQCL(event: any){
    if(event){
      this.getLCBInfo();
    } else {
      this.listGradeLCB = [];
      this.dataObject.salaryGradeId = 0;
      this.dataObject.salaryRankId = 0;
      this.dataObject.salaryAmount = 0;
    }
  }
  onChangeGrade(event: any) {
    if(event){
      this.listRankLCB = this.listGradeLCB.find(item => item.salaryGradeId === this.dataObject.salaryGradeId)?.salaryRankOfMatrixResponses;
      this.dataObject.performanceFactor =this.listRankLCB?.find(item => item.salaryGradeId === this.dataObject.salaryGradeId)?.performCoefficient || 0;
      this.onChangeRank(this.dataObject.salaryRankId);
    } else {
      this.listRankLCB = [];
      this.dataObject.salaryRankId = 0;
      this.dataObject.salaryAmount = 0;
    }
  }
  onChangeRank(event: any,isChange?: boolean){
    if(isChange){
      this.dataObject.salaryAmount = this.listRankLCB?.find(item => item.salaryRankId === event)?.basicAmount || 0;
      this.caclRealLCB(this.dataObject);
    }
  }
  saveData(){
    this.isSubmit = true;
    if(this.checkValid()){
      return;
    }
    this.isLoadingPage = true;
    this.salaryEvolutionService.initSalayEvolution(this.buildRequest()).subscribe(data => {
      this.toastrCustom.success();
      this.isAddNew = false;
      this.isEdit = false;
      this.isLoadingPage = false;
      this.onCancel();
    }, error => {
      this.toastrCustom.error(error?.message);
      this.isLoadingPage = false;
    })
  }
  getDetails(){
    this.isLoadingPage = true;
    this.salaryEvolutionService.getDetail(this.paramSearch.salaryProcessId).subscribe(
      (data) => {
        this.dataObject= data.data;
        this.paramSearch= data.data;
        this.paramSearch.statusEmployee= data.data.empFlagStatus;
        this.fileName = data.data.docName;
        this.dataObject.approveDate = this.dataObject.approveDate? moment(this.dataObject.approveDate).format('DD/MM/YYYY') : '';
        this.dataObject.lastUpdateDate = this.dataObject.lastUpdateDate? moment(this.dataObject.lastUpdateDate).format('DD/MM/YYYY'): '';
        if(this.dataObject.employeeType === "1" || this.dataObject.employeeType === "4"){
          this.getLCBInfo();
          this.caclRealLCB(this.dataObject);
        }
        this.isLoadingPage  = false;
      },
      (error) => {
        this.toastrCustom.error(error?.error?.message);
        this.isLoadingPage = false;
      }
    );
  }
  buildRequest(){
    const formData: FormData = new FormData();
    const data = {
      id: this.isEdit?this.dataObject.salaryProcessId:'',
      employeeId: this.paramSearch.employeeId,
      salaryProcessDetailObjectRequest: {
        salaryVersionId: this.dataObject.salaryVersionId?this.dataObject.salaryVersionId:'',
        employeeType: this.dataObject.employeeType?this.dataObject.employeeType:'',
        salaryType: this.dataObject.salaryType?this.dataObject.salaryType:'',
        fromDate: moment(this.dataObject.fromDate).format('DD/MM/YYYY'),
        toDate: this.dataObject.toDate? moment(this.dataObject.toDate).format('DD/MM/YYYY'):'',
        note: this.dataObject.note?this.dataObject.note:'',
        docNo: this.dataObject.docNo?this.dataObject.docNo:'',
        packAmount: this.formatToNumber(this.dataObject.packAmount?this.dataObject.packAmount:''),
        curCode: this.dataObject.curCode?this.dataObject.curCode:'',
        salaryGroupOfProcessDetailRequest: {
          salaryGradeId: this.dataObject.salaryGradeId?this.dataObject.salaryGradeId:'',
          salaryRankId: this.dataObject.salaryRankId?this.dataObject.salaryRankId:'',
          salaryAmount: this.formatToNumber(this.dataObject.salaryAmount),
          salaryPercent: this.dataObject.salaryPercent?this.dataObject.salaryPercent:'',
          performanceFactor: this.dataObject.performanceFactor?this.dataObject.performanceFactor:'',
          performancePercent: this.dataObject.performancePercent?this.dataObject.performancePercent:'',
          periodCurPoint: this.dataObject.periodCurPoint?this.dataObject.periodCurPoint:'',
          periodCurRate: this.dataObject.periodCurRate?this.dataObject.periodCurRate:'',
          periodCurFactor: this.dataObject.periodCurFactor?this.dataObject.periodCurFactor:'',
          performanceAmount: this.formatToNumber(this.dataObject.levelTHS)
        }
      },
    }
    formData.append(
      'request',
      new Blob([JSON.stringify(data)], {
        type: 'application/json',
      })
    );
    if(this.dataObject.file){
      formData.append('file',this.dataObject.file);
    }
    return formData;
  }
  caclRealLCB(dataObject: any){
    this.dataObject.packAmount = this.formatToCurrency(this.formatToNumber(dataObject.packAmount),'currencyNumber');
    this.dataObject.salaryAmount = this.formatToCurrency(this.formatToNumber(dataObject.salaryAmount),"currencyNumber");
    const salaryReal= this.formatToNumber(dataObject.salaryAmount) * (this.formatToNumber(dataObject.salaryPercent))/100
    this.dataObject.salaryReal = this.formatToCurrency(Math.round(salaryReal),"currencyNumber");
    const levelTHS = this.formatToNumber(dataObject.salaryAmount)  *  (this.formatToNumber(dataObject.performanceFactor));
    this.dataObject.levelTHS = this.formatToCurrency(Math.round(levelTHS),"currencyNumber");
    const realTHS= this.formatToNumber(dataObject.levelTHS) * (this.formatToNumber(dataObject.performancePercent)
      * this.formatToNumber(dataObject.periodCurFactor))/100;
    this.dataObject.realTHS  = this.formatToCurrency(Math.round(realTHS),"currencyNumber");
  }
  handleFileInput(event:any) {
    this.dataObject.file = event.target.files[0];
    this.fileName = event.target.files[0].name;
  }
  removeChip(){
    this.fileName = '';
    this.dataObject.file = '';
  }
  checkValid():boolean{
    if (!this.paramSearch.employeeCode) {
      this.toastrCustom.error(this.translate.instant("polManagement.validate.emtyEmp"));
      return true
    }
    if(+this.dataObject.performancePercent > 100 || +this.dataObject.salaryPercent > 100 ){
      return true;
    }
    if (this.dataObject.fromDate && this.dataObject.toDate) {
      const diffDate = moment(new Date(this.dataObject.fromDate)).diff(moment(new Date(this.dataObject.toDate)),'days');
      if (diffDate > 0) {
        this.toastrCustom.error(this.translate.instant("polManagement.validate.toDate"));
        return true
      }
    }
    if(this.dataObject.employeeType === '1' || this.dataObject.employeeType === '4'){
      return !this.dataObject.fromDate || !this.dataObject.salaryVersionId || !this.dataObject.salaryGradeId
        || !this.dataObject.salaryRankId || !this.dataObject.salaryAmount || !this.dataObject.salaryReal;
    } else {
      return !this.dataObject.packAmount;
    }
  }
  getScored() {
    this.isCall = true;
    const params = {
      empCode: this.paramSearch.employeeCode,
      numPeriod: 1
    }
    this.salaryEvolutionService.getScoredData(params).subscribe(
      (data) => {
          this.dataObject.periodCurRate= data.data?.listScore[0].ratingAfter;
          this.dataObject.periodCurPoint = String(data.data?.listScore[0].score);
          this.getTHSPointData();
      },
      (error) => {
          this.dataObject.periodCurPoint = '0';
          this.getTHSPointData();
      }
    );
  }
  getTHSPointData(){
    const params = {
      ratePoint : this.dataObject.periodCurPoint,
    }
    this.salaryEvolutionService.getTHSPoint(params).subscribe(
      (data) => {
        this.dataObject.periodCurFactor= data.data.coefficient;
      },
      (error) => {
        this.dataObject.periodCurFactor = '1';
      }
    );
  }
  approveSalaryProcess(isApprove:boolean):void {
    this.isSubmit = true;
    if(!isApprove && !this.description){
      return;
    }
    this.isLoadingPage= true;
    const action = isApprove ? "PD" : "TC";
    const request = this.buildApproveRequest(action);
    this.salaryEvolutionService.approveSalaryProcess(request, this.backdate).subscribe(data => {
      this.toastrCustom.success();
      this.isLoadingPage = false;
      this.description = "";
      this.router.navigate(['/policy-management/salary-process/salary-process-approve']);
      this.isSubmit = false;
    }, error => {
      this.toastrCustom.error(error.message);
      this.isLoadingPage = false;
    });
  }
  buildApproveRequest(action: 'PD' | 'TC'):ApproveResquest {

    const request:ApproveResquest = {
      action: action,
      description: this.description,
      ids: [],
    }
    request.ids.push(this.dataObject.salaryProcessId);
    return request;
  }
  onCancel(){
    this.router.navigate(['/policy-management/salary-process/salary-evolution']);
  }
  getProcessEpl(){
    const params = {
      employeeCode: this.paramSearch.employeeCode,
      startRecord: 0,
      pageSize:1000
    }
    this.salaryEvolutionService.getListProcessEmp(params).subscribe(
      (data) => {
        if(data.data?.listData && data.data?.listData.length > 0){
          const wp = data.data.listData.sort((a:any,b:any):number => {
            const dateA = moment(a.fromDate,'DD-MM-YYYY').toDate();
            const dateB = moment(b.fromDate,'DD-MM-YYYY').toDate()
            if(dateA > dateB){
              return -1;
            } else if(dateA < dateB){
              return 1;
            } else {
              return 0;
            }
          });
          this.dataObject.fromDate = ''+ moment(wp[0]?.fromDate,'DD-MM-YYYY').toDate();
        }
      },
      (error) => {
      }
    );
  }
  onChangeSalaryType(event:any){
    if(event !== 'LHQ'){
      this.dataObject.performanceFactor = 0;
      this.caclRealLCB(this.dataObject);
    }
  }
  downloadFile(){
    if(this.isAddNew){
      const blob = new Blob([this.dataObject.file], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.pdf' });
      this.file = new File([blob], this.dataObject.docName);
      this.onFileSelected();
    } else {
      this.salaryEvolutionService.downloadFile(this.dataObject.docId).subscribe((data) => {
        const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.pdf' });
        this.file = new File([blob], this.dataObject.docName);
        this.onFileSelected();
      });
    }
  }
  onFileSelected() {
    const reader = new FileReader();
    reader.onload = (e: any) => {
      this.pdfSrc = e.target.result;
    };
    reader.readAsArrayBuffer(this.file);
    this.isVisible = true;
  }
  handleCancel(): void {
    this.isVisible = false;
  }
  formatToCurrency(value:any,param:any){
    let currency = this.mapPipe.transform(value, param);
    return !currency || currency === '-' ? "": currency;
  }
  formatToNumber(value:any){
    value = String(value);
    if(value && value.includes(",")){
       value = value.replaceAll(",","")
    }
    return +value;
  }
  onKeyPressLCB(event:any){
    const pattern = REGEX_CURRENCY;
    if (!pattern.test(event.key)) {
      event.preventDefault();
      return;
    }
  }
  onChangePerLCB(event:any){
    if(event){
      this.dataObject.performancePercent = event.target.value;
    }
  }
  checkEmplInfo(employeeInfo: any){
    if(employeeInfo && employeeInfo.employeeCode) {
      this.isEdit = true;
      this.onChange(employeeInfo);
      this.paramSearch = employeeInfo;
      if(employeeInfo.employeeCode){
        this.selected.empCode = employeeInfo.employeeCode;
        this.selected.fullName = employeeInfo.fullName;
      }
      if(!this.isAddNew){
        this.getDetails();
      }
    } else {
      this.selected = null;
    }
  }
  diffEmpDate(empInfo:any){
    if(empInfo.joinCompanyDate){
      const emplDate = moment(empInfo.joinCompanyDate,'YYYY-MM-DD').toDate();
      const diffDate = moment(new Date()).diff(emplDate,'months');
      return diffDate;
    } else {
      return 0;
    }
  }
  findStatus(key: number|string){
    if(key){
      return this.empStatus.find(item => item.key === key)?.value;
    } else {
      return '';
    }
  }
}
