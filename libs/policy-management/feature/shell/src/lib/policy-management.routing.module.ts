import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {RoleGuardService} from "@hcm-mfe/shared/core";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";

const router = [
  {
    path: 'salary-process',
    data: {
      breadcrumb: 'polManagement.breadcrumb.salary-process'
    },
    children: [
      {
        path: 'salary-process-approve',
        data: {
          pageName: 'polManagement.pageName.salary-process-approve',
          breadcrumb: 'polManagement.breadcrumb.salary-process-approve'
        },
        loadChildren:() =>
          import(
            '@hcm-mfe/policy-management/feature/salary-process-approve'
            ).then(
            (m) =>
              m.PolicyManagementFeatureSalaryProcessApproveModule
          ),
      },
      {
        path: 'salary-evolution',
        data: {
          pageName: 'polManagement.pageName.salary-evolution',
          breadcrumb: 'polManagement.pageName.salary-evolution'
        },
        loadChildren: () =>
          import(
            '@hcm-mfe/policy-management/feature/salary-evolution'
            ).then(
            (m) =>
              m.PolicyManagementFeatureSalaryEvolutionModule
          ),
      },
      {
        path: 'create-salary-evoltuion',
        data: {
          mode: 'create',
          pageName:
            'polManagement.pageName.create-salary-evolution',
          breadcrumb:
            'polManagement.breadcrumb.create-salary-evolution',
        },
        loadChildren: () =>
          import(
            '@hcm-mfe/policy-management/feature/detail-salary-evolution'
            ).then(
            (m) =>
              m.PolicyManagementFeatureDetailSalaryEvolutionModule
          ),
      },
      {
        path: 'detail',
        data: {
          pageName: 'polManagement.pageName.detail-salary-evolution',
          breadcrumb: 'polManagement.pageName.detail-salary-evolution'
        },
        loadChildren: () =>
          import(
            '@hcm-mfe/policy-management/feature/detail-salary-evolution'
            ).then(
            (m) =>
              m.PolicyManagementFeatureDetailSalaryEvolutionModule
          ),
      },
      {
        path: 'detail-approve',
        data: {
          mode: 'approve',
          pageName: 'polManagement.pageName.detail-salary-process-approve',
          breadcrumb: 'polManagement.breadcrumb.detail-salary-process-approve'
        },
        loadChildren: () =>
          import(
            '@hcm-mfe/policy-management/feature/detail-salary-evolution'
            ).then(
            (m) =>
              m.PolicyManagementFeatureDetailSalaryEvolutionModule
          ),
      },
      {
        path: 'salary-commitment',
        data: {
          pageName: 'polManagement.pageName.salaryCommitmentManage',
          breadcrumb: 'polManagement.breadcrumb.salaryCommitmentManage'
        },
        loadChildren: () =>
          import(
            '@hcm-mfe/policy-management/feature/salary-commitment'
            ).then(
            (m) =>
              m.PolicyManagementFeatureSalaryCommitmentModule
          ),
      },
      {
        path: 'salary-commit-approve',
        data: {
          pageName: 'polManagement.pageName.salary-commit-approve',
          breadcrumb: 'polManagement.pageName.salary-commit-approve'
        },
        loadChildren: () =>
          import(
            '@hcm-mfe/policy-management/feature/salary-commit-approve'
            ).then(
            (m) =>
              m.PolicyManagementFeatureSalaryCommitApproveModule
          ),
      },
      {
        path: 'salary-commitment-init',
        data: {
          mode:'create',
          pageName: 'polManagement.pageName.salaryCommitmentInit',
          breadcrumb: 'polManagement.breadcrumb.salaryCommitmentInit'
        },
        loadChildren: () =>
          import(
            '@hcm-mfe/policy-management/feature/salary-commitment-init'
            ).then(
            (m) =>
              m.PolicyManagementFeatureSalaryCommitmentInitModule
          ),
      },
      {
        path: 'salary-commitment-edit',
        data: {
          edit: true,
          pageName: 'polManagement.pageName.salaryCommitmentEdit',
          breadcrumb: 'polManagement.breadcrumb.salaryCommitmentEdit'
        },
        loadChildren: () =>
          import(
            '@hcm-mfe/policy-management/feature/salary-commitment-init'
            ).then(
            (m) =>
              m.PolicyManagementFeatureSalaryCommitmentInitModule
          ),
      },
      {
        path: 'detail-commit-approve',
        data: {
          mode: 'approve',
          pageName: 'polManagement.pageName.salaryCommitmentDetail',
          breadcrumb: 'polManagement.breadcrumb.salaryCommitmentDetail'
        },
        loadChildren: () =>
          import(
            '@hcm-mfe/policy-management/feature/salary-commitment-init'
            ).then(
            (m) =>
              m.PolicyManagementFeatureSalaryCommitmentInitModule
          ),
      },
      {
        path: 'salary-process-approve-backdate',
        data: {
          pageName: 'polManagement.pageName.salary-process-approve',
          breadcrumb: 'polManagement.breadcrumb.salary-process-approve',
          backdate: true
        },
        loadChildren:() =>
          import(
            '@hcm-mfe/policy-management/feature/salary-process-approve'
            ).then(
            (m) =>
              m.PolicyManagementFeatureSalaryProcessApproveModule
          ),
      },
      {
        path: 'salary-commit-approve-backdate',
        data: {
          pageName: 'polManagement.pageName.salary-commit-approve',
          breadcrumb: 'polManagement.breadcrumb.salary-commit-approve',
          mode: 'backdate',
        },
        loadChildren:() =>
          import(
            '@hcm-mfe/policy-management/feature/salary-commit-approve'
            ).then(
            (m) =>
              m.PolicyManagementFeatureSalaryCommitApproveModule
          ),
      },
    ]
  },
  {
    path: 'allowance-development',
    canActivateChild: [RoleGuardService],
    data: {
      code: FunctionCode.ALLOWANCE_DEVELOPMENTS_LIST,
      pageName:
        'polManagement.pageName.allowanceDevelopment',
      breadcrumb:
        'polManagement.breadcrumb.allowanceDevelopment',
    },
    children: [
      {
        path: '',
        loadChildren: () =>
          import(
            '@hcm-mfe/policy-management/feature/allowance-development'
            ).then(
            (m) =>
              m.PolicyManagementFeatureAllowanceDevelopmentModule
          ),
      },
      {
        path: 'create',
        data: {
          pageName:
            'polManagement.pageName.createAllowanceDevelopment',
          breadcrumb:
            'polManagement.breadcrumb.createAllowanceDevelopment',
        },
        loadChildren: () =>
          import(
            '@hcm-mfe/policy-management/feature/detail-allowance'
            ).then(
            (m) =>
              m.PolicyManagementFeatureDetailAllowanceModule
          ),
      },
      {
        path: 'edit',
        data: {
          pageName:
            'polManagement.pageName.detailAllowanceDevelopment',
          breadcrumb:
            'polManagement.breadcrumb.detailAllowanceDevelopment',
        },
        loadChildren: () =>
          import(
            '@hcm-mfe/policy-management/feature/detail-allowance'
            ).then(
            (m) =>
              m.PolicyManagementFeatureDetailAllowanceModule
          ),
      },
      {
        path: 'detail',
        data: {
          mode: 'detail-allowance',
          pageName:
            'polManagement.pageName.detailAllowanceDevelopment',
          breadcrumb:
            'polManagement.breadcrumb.detailAllowanceDevelopment',
        },
        loadChildren: () =>
          import(
            '@hcm-mfe/policy-management/feature/detail-allowance'
            ).then(
            (m) =>
              m.PolicyManagementFeatureDetailAllowanceModule
          ),
      }
    ],
  },
  {
    path: 'list-receiving-exception-allowances',
    canActivateChild: [RoleGuardService],
    data: {
      code: FunctionCode.RECEIVING_EXCEPTION_ALLOWANCE,
      pageName:
        'polManagement.pageName.list-receiving-exception-allowances',
      breadcrumb:
        'polManagement.pageName.list-receiving-exception-allowances',
    },
    children: [
      {
        path: '',
        loadChildren: () =>
          import(
            '@hcm-mfe/policy-management/feature/list-receiving-exeption-allowances'
            ).then(
            (m) =>
              m.PolicyManagementFeatureListReceivingExeptionAllowancesModule
          ),
      },
    ],
  },
  {
    path: 'exception-allowance-approval',
    canActivateChild: [RoleGuardService],
    data: {
      code: FunctionCode.RECEIVING_EXCEPTION_ALLOWANCE_APPROVE,
      pageName:
        'polManagement.pageName.exception-allowances-approval',
      breadcrumb:
        'polManagement.pageName.exception-allowances-approval',
    },
    children: [
      {
        path: '',
        loadChildren: () =>
          import(
            '@hcm-mfe/policy-management/feature/exception-allowances-approval'
            ).then(
            (m) =>
              m.PolicyManagementFeatureExceptionAllowancesApprovalModule
          ),
      },
    ],
  },
  {
    path: 'exception-allowance-approval-back-date',
    // canActivateChild: [RoleGuardService],
    data: {
      mode: 'BD',
      // code: FunctionCode.RECEIVING_EXCEPTION_ALLOWANCE_APPROVE_BACK_DATE,
      pageName:
        'polManagement.pageName.exception-allowances-approval-back-date',
      breadcrumb:
        'polManagement.pageName.exception-allowances-approval-back-date',
    },
    children: [
      {
        path: '',
        loadChildren: () =>
          import(
            '@hcm-mfe/policy-management/feature/exception-allowances-approval'
            ).then(
            (m) =>
              m.PolicyManagementFeatureExceptionAllowancesApprovalModule
          ),
      },
    ],
  },
  {
    path: 'approve-allowance',
    canActivateChild: [RoleGuardService],
    data: {
      code: FunctionCode.ALLOWANCE_DEVELOPMENTS_APPROVE,
      pageName:
        'polManagement.pageName.approveAllowanceDevelopment',
      breadcrumb:
        'polManagement.breadcrumb.approveAllowanceDevelopment',
    },
    children: [
      {
        path: '',
        loadChildren: () =>
          import(
            '@hcm-mfe/policy-management/feature/approve-allowance'
            ).then(
            (m) =>
              m.PolicyManagementFeatureApproveAllowanceModule
          ),
      },
      {
        path: 'detail',
        data: {
          mode: 'detail',
          pageName:
            'polManagement.pageName.detailAllowanceDevelopment',
          breadcrumb:
            'polManagement.breadcrumb.detailAllowanceDevelopment',
        },
        loadChildren: () =>
          import(
            '@hcm-mfe/policy-management/feature/detail-allowance'
            ).then(
            (m) =>
              m.PolicyManagementFeatureDetailAllowanceModule
          ),
      },
    ],
  },
  {
    path: 'approve-allowance-backdate',
    canActivateChild: [RoleGuardService],
    data: {
      mode: 'BD',
      code: FunctionCode.ALLOWANCE_DEVELOPMENTS_APPROVE_BD,
      pageName:
        'polManagement.pageName.approveAllowanceDevelopmentBD',
      breadcrumb:
        'polManagement.breadcrumb.approveAllowanceDevelopmentBD',
    },
    children: [
      {
        path: '',
        loadChildren: () =>
          import(
            '@hcm-mfe/policy-management/feature/approve-allowance'
            ).then(
            (m) =>
              m.PolicyManagementFeatureApproveAllowanceModule
          ),
      },
      {
        path: 'detail',
        data: {
          mode: 'detail',
          pageName:
            'polManagement.pageName.detailAllowanceDevelopment',
          breadcrumb:
            'polManagement.breadcrumb.detailAllowanceDevelopment',
        },
        loadChildren: () =>
          import(
            '@hcm-mfe/policy-management/feature/detail-allowance'
            ).then(
            (m) =>
              m.PolicyManagementFeatureDetailAllowanceModule
          ),
      },
    ],
  },
  {
    path: 'achievement-bonus',
    data: {
      breadcrumb: 'polManagement.pageName.achievementBonus'
    },
    children: [
      {
        path: '',
        loadChildren: () =>
          import(
            '@hcm-mfe/policy-management/feature/achievement-bonus'
            ).then(
            (m) =>
              m.PolicyManagementFeatureAchievementBonusModule
          ),
      },
      {
        path: 'approve',
        data: {
          pageName: 'polManagement.pageName.abApprove',
          breadcrumb: 'polManagement.breadcrumb.abApprove'
        },
        loadChildren: () =>
          import(
            '@hcm-mfe/policy-management/feature/ab-approve'
            ).then(
            (m) =>
              m.PolicyManagementFeatureAbApproveModule
          ),
      },
      {
        path: 'input-achievement-bonus',
        data: {
          pageName: 'polManagement.pageName.inputArchBonus',
          breadcrumb: 'polManagement.pageName.inputArchBonus'
        },
        loadChildren:() =>
          import(
            '@hcm-mfe/policy-management/feature/achievement-bonus'
            ).then(
            (m) =>
              m.PolicyManagementFeatureAchievementBonusModule
          ),
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(router)],
  exports: [RouterModule],
})
export class PolicyManagemenRoutingModule {
}
