import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NzMessageModule} from "ng-zorro-antd/message";
import {NzModalModule} from "ng-zorro-antd/modal";
import {SharedUiPopupModule} from "@hcm-mfe/shared/ui/popup";
import {OverlayModule} from "@angular/cdk/overlay";
import {ToastrModule} from "ngx-toastr";
import {FormatCurrencyPipe} from "@hcm-mfe/shared/pipes/format-currency";
import {PolicyManagemenRoutingModule} from "./policy-management.routing.module";

@NgModule({
  imports: [CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NzMessageModule,
    NzModalModule,
    SharedUiPopupModule,
    OverlayModule,
    PolicyManagemenRoutingModule,
    ToastrModule.forRoot({})],
  providers: [DatePipe, FormatCurrencyPipe],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class PolicyManagementFeatureShellModule {}
