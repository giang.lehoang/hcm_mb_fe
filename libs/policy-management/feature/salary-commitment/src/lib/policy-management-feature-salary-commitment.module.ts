import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SalaryCommitmentComponent} from "./salary-commitment/salary-commitment.component";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/mb-employee-data-picker";
import {NzGridModule} from "ng-zorro-antd/grid";
import {TranslateModule} from "@ngx-translate/core";
import {FormsModule} from "@angular/forms";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {RouterModule} from "@angular/router";
import {NzTagModule} from "ng-zorro-antd/tag";
import {NzIconModule} from "ng-zorro-antd/icon";
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';

@NgModule({
  imports: [CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: SalaryCommitmentComponent
      }
    ]), FormsModule, SharedUiMbButtonModule, SharedUiMbTableWrapModule, SharedUiMbTableModule, SharedUiLoadingModule,
    SharedUiMbEmployeeDataPickerModule, NzGridModule, TranslateModule, SharedUiMbInputTextModule, NzTagModule, NzIconModule, NzToolTipModule],
  declarations: [SalaryCommitmentComponent],
  exports: [SalaryCommitmentComponent]
})
export class PolicyManagementFeatureSalaryCommitmentModule {}
