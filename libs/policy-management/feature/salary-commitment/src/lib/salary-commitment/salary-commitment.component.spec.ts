import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalaryCommitmentComponent } from './salary-commitment.component';

describe('SalaryCommitmentComponent', () => {
  let component: SalaryCommitmentComponent;
  let fixture: ComponentFixture<SalaryCommitmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SalaryCommitmentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SalaryCommitmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
