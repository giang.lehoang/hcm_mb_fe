import {ChangeDetectorRef, Component, Injector, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {
  BONUS,
  SalaryCommitmentModel,
  SalaryCommitmentSearch,
  SalaryParams,
} from "@hcm-mfe/policy-management/data-access/models";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {FormModalShowTreeUnitComponent} from "@hcm-mfe/shared/ui/form-modal-show-tree-unit";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {MBTableConfig} from "@hcm-mfe/shared/data-access/models";
import {SalaryCommitmentService} from "@hcm-mfe/policy-management/data-access/service";
import {checkLastM, renderFlagStatus} from "@hcm-mfe/policy-management/helper";
import * as moment from "moment";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";

@Component({
  selector: 'app-salary-commitment',
  templateUrl: './salary-commitment.component.html',
  styleUrls: ['./salary-commitment.component.scss']
})
export class SalaryCommitmentComponent extends BaseComponent implements OnInit {
  @ViewChild('actionTmpl', {static: true}) action!: TemplateRef<NzSafeAny>;
  @ViewChild('flagStatusTmpl', {static: true}) flagStatus!: TemplateRef<NzSafeAny>;
  objectSearch:SalaryCommitmentSearch;
  tableConfig:MBTableConfig;
  params:SalaryCommitmentSearch;
  loading:boolean;
  orgName:string;
  fullName:string;
  jobName:string | undefined;
  dataTable:SalaryCommitmentModel[];
  renderFlagStatus = renderFlagStatus;
  heightTable = { x: '100vw', y: '50vh'};
  functionCode = FunctionCode.POL_SALARY_COMMITMENT;

  constructor(injector:Injector, readonly salaryCommitmentService:SalaryCommitmentService, readonly cdr: ChangeDetectorRef) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.POL_SALARY_COMMITMENT}`);
    console.log(this.objFunction);
    this.loading = false;
    this.tableConfig = new MBTableConfig();
    this.objectSearch = new SalaryCommitmentSearch();
    this.orgName = '';
    this.fullName = '';
    this.jobName = '';
    this.dataTable = [];
    this.params = {...this.objectSearch};
  }

  ngOnInit(): void {
    const empl =  localStorage.getItem("employeeInfo");
    if(empl){
      localStorage.removeItem("employeeInfo");
    }
    this.tableConfig.showFrontPagination = true;
    this.initTable();
    this.search(1);
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        // {
        //   title: 'polManagement.table.employeeCode',
        //   field: 'employeeCode',
        //   width: 80,
        //   fixed: window.innerWidth > 1024,
        //   fixedDir: 'left',
        // },
        // {
        //   title: 'polManagement.table.fullName',
        //   field: 'fullName',
        //   width: 100,
        //   fixed: window.innerWidth > 1024,
        //   fixedDir: 'left',
        // },
        // {
        //   title: 'polManagement.table.org',
        //   field: 'lastName',
        //   needEllipsis:true,
        //   fixed: window.innerWidth > 1024,
        //   width: 150,
        // },
        // {
        //   title: 'polManagement.table.jobName',
        //   field: 'jobName',
        //   fixed: window.innerWidth > 1024,
        //   width: 100
        // },
        {
          title: 'polManagement.table.fromDate',
          field: 'fromDate',
          pipe:'date',
          fixed: window.innerWidth > 1024,
          width: 80,
        },
        {
          title: 'polManagement.table.toDate',
          field: 'toDate',
          pipe:'date',
          fixed: window.innerWidth > 1024,
          width: 80,
        },
        {
          title: 'polManagement.table.salaryCommitmentLevel',
          field: 'amount',
          pipe:'currencyNumber',
          width: 100,
          fixed: window.innerWidth > 1024,
          tdClassList: ['text-right'],
        },
        {
          title: 'polManagement.table.curCode',
          field: 'curCode',
          fixed: window.innerWidth > 1024,
          width: 60,
        },
        {
          title: 'polManagement.table.bonus',
          field: 'bonusName',
          fixed: window.innerWidth > 1024,
          width: 60,
        },
        {
          title: 'polManagement.table.status',
          field: 'flagStatus',
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          tdTemplate: this.flagStatus,
          fixed: window.innerWidth > 1024,
          width: 100,
        },
        {
          title: ' ',
          tdTemplate: this.action,
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          width: 40,
          fixed: window.innerWidth > 1024,
          fixedDir: 'right',
        }
      ],
      total: 0,
      loading: false,
      size: 'small',
      pageSize: this.params.size,
      pageIndex: 1,
      widthIndex: '30px'
    };
  }

  search(page?:number){
    this.loading = true;
    this.setParam();
    this.params.page = page ? page - 1 : this.params.page;
    this.salaryCommitmentService.getListCommitment(this.params).subscribe(data => {
      if(data.data?.content){
        this.dataTable = data.data.content;
        if(data.data.totalElements) this.tableConfig.total = data.data.totalElements;
        this.dataTable?.forEach(item => {
          item.bonusName = item.isBonus === 'Y' ? BONUS.Y : BONUS.N;
        })
      }
      this.loading = false;
    }, error => {
      this.loading = false;
      this.toastrCustom.error(error.message);
    });
  }

  setParam(){
    this.params.orgId = this.objectSearch.orgId;
    this.params.employeeCode = this.objectSearch.employeeCode ?this.objectSearch.employeeCode:'-1';
  }

  showPopup() {
    const modal = this.modal.create({
      nzWidth: '80%',
      nzTitle: ' ',
      nzContent: FormModalShowTreeUnitComponent,
      nzFooter: null,
      nzComponentParams: {
        isShowChkOrg: true
      },
    });

    modal.afterClose.subscribe((result) => {
      if (result?.orgName) {
        this.orgName = result?.orgName;
        this.objectSearch.orgId = result?.orgId;
      }
    });
  }

  onChange(event: SalaryParams){
    if(event){
      this.objectSearch.employeeCode = event.employeeCode;
      this.fullName = event.fullName;
      this.jobName = event.jobName;
      this.search(0);
    } else {
      this.objectSearch.employeeCode = '';
      this.fullName = '';
      this.jobName = '';
    }
    localStorage.setItem("employeeInfo",JSON.stringify(event));
  }

  deleteCommitment(id:number, event:any):void {
    event.stopPropagation();
    const del = () => {
      this.loading = true;
      this.salaryCommitmentService.deleteCommitment(id).subscribe(data => {
        this.loading = false;
        this.search();
        this.toastrCustom.success();
      }, error => {
        this.loading = false;
        this.toastrCustom.error(error.message);
      });
    }
    this.deletePopup.showModal(del);
  }

  createCommitment():void {
    this.router.navigate(['/policy-management/salary-process/salary-commitment-init']);
  }

  editCommitment(id:number):void {
    localStorage.setItem('idCommitment', String(id));
    this.router.navigate(['/policy-management/salary-process/salary-commitment-edit']);
  }

  clearOrgInput():void {
    this.orgName = "";
    this.objectSearch.orgId = "";
  }

  override triggerSearchEvent(): void {
    this.search(1);
  }
}
