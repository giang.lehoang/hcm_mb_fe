import { Component, Injector, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import {
  AchBonusResponse, CoefficientSystem,
  EMPLOYEE_STATUS,
  flagStatus,
  ParamBonus, RankGroupResponse,
  RatingResponse,
  REGEX_NUMBER,
  SALARY_PERIOD, TYPE_BONUS
} from '@hcm-mfe/policy-management/data-access/models';
import { MBTableConfig, RegionCategory } from '@hcm-mfe/shared/data-access/models';
import { FunctionCode, maxInt32 } from '@hcm-mfe/shared/common/enums';
import { FormModalShowTreeUnitComponent } from '@hcm-mfe/shared/ui/form-modal-show-tree-unit';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { AchievementBonusService } from '../../../../../data-access/service/src/lib/achievement-bonus.service';
import {renderFlagStatus} from "@hcm-mfe/policy-management/helper";
import * as FileSaver from 'file-saver';
import * as moment from 'moment';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { ActionDocumentAttachedFileComponent } from '@hcm-mfe/goal-management/ui/form-input';
import { MICRO_SERVICE, Mode } from '@hcm-mfe/shared/common/constants';

@Component({
  selector: 'app-achievement-bonus',
  templateUrl: './achievement-bonus.component.html',
  styleUrls: ['./achievement-bonus.component.scss']
})
export class AchievementBonusComponent extends BaseComponent implements OnInit {

  @ViewChild('flagStatusTmpl', {static: true}) flagStatus!: TemplateRef<NzSafeAny>;
  @ViewChild('coefficientTmpl', {static: true}) coefficientTmpl!: TemplateRef<NzSafeAny>;
  @ViewChild('fromDateTmpl', {static: true}) fromDateTmpl!: TemplateRef<NzSafeAny>;
  @ViewChild('toDateTmpl', {static: true}) toDateTmpl!: TemplateRef<NzSafeAny>;
  @ViewChild('ratingTmpl', {static: true}) ratingTmpl!: TemplateRef<NzSafeAny>;
  @ViewChild('tccvTmpl', {static: true}) tccvTmpl!: TemplateRef<NzSafeAny>;
  @ViewChild('actionTmpl', {static: true}) action!: TemplateRef<NzSafeAny>;
  @ViewChild('rankGroupTmpl', {static: true}) rankGroupTmpl!: TemplateRef<NzSafeAny>;


  objectSearch:ParamBonus;
  params:ParamBonus;
  tableConfig:MBTableConfig;
  dataTable:AchBonusResponse[];
  listRating:RatingResponse[] = [];
  listTCCV:RegionCategory[] = [];
  checked:NzSafeAny[];
  listSystemCoe:CoefficientSystem[] = [];
  description:string;
  submit:boolean;
  orgName:string;
  fullName:string;
  loading:boolean;
  isApprove:boolean = false;
  heightTable = { x: '100vw', y: '35vh'};
  isBackdate: boolean= false;
  functionCode: string;
  empStatus = EMPLOYEE_STATUS;
  listStatus = flagStatus.STATUS_LIST;
  listTypeBonus = TYPE_BONUS;
  selectTab: number = 0;
  listPeriod = [];
  listEmpStatus = [];
  listRankGroup: RankGroupResponse[] = [];
  type = 'TH';
  displayTitle = 'HSTH';
  modalRef: NzModalRef | undefined;
  typeImport = '';
  fileName:string = '';
  renderFlagStatus = renderFlagStatus;


  constructor(injector:Injector ,readonly achievementBonusService:AchievementBonusService) {
    super(injector);
    this.loading = false;
    this.objectSearch = new ParamBonus();
    this.params = {...this.objectSearch};
    this.fullName = '';
    this.orgName = '';
    this.description = '';
    this.submit = false;
    this.objectSearch.type='TH'

    this.tableConfig = new MBTableConfig();
    this.checked = [];
    this.dataTable = [];
    this.isBackdate = this.route.snapshot.data['mode'] === "backdate";
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.POL_SALARY_ACHIEVEMENT_BONUS}`);
    console.log(this.objFunction);
    this.functionCode = FunctionCode.POL_SALARY_ACHIEVEMENT_BONUS;
    this.getPeriodDate();
    console.log(this.objFunction);
  }

  ngOnInit(): void {
    this.tableConfig.showFrontPagination= true;
    this.initTable('TH');
  }
  checkParams(){
    this.params.employeeId = this.objectSearch.employeeId?this.objectSearch.employeeId:'';
    this.params.orgId = this.objectSearch.orgId?this.objectSearch.orgId:'';
    this.params.periodDate = this.objectSearch.periodDate?this.objectSearch.periodDate:'';
    this.params.flagStatus = this.objectSearch.flagStatus?this.objectSearch.flagStatus:'';
    this.params.empStatus = this.objectSearch.empStatus?this.objectSearch.empStatus:'';
    this.params.type = this.type?this.type:'TH';
    this.params.isSpecial = this.objectSearch.isSpecial?this.objectSearch.isSpecial:'';
  }

  private initTable(type:string): void {
    this.tableConfig = {
      headers: [
        {
          title: 'polManagement.table.employeeCode',
          field: 'employeeCode',
          width: 150,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left',
          show: true
        },
        {
          title: 'polManagement.table.fullName',
          field: 'fullName',
          width: 200,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left',
          show: true
        },
        {
          title: 'polManagement.table.org',
          field: 'orgFullName',
          width: 400,
          fixedDir: 'left',
          show: true
        },
        {
          title: 'polManagement.table.jobName',
          field: 'jobName',
          width: 250,
          show: true
        },
        {
          title: 'Level',
          field: 'levelValue',
          width: 150,
          show: type !== 'CN'
        },
        {
          title: 'polManagement.table.periodCurRate',
          field: 'dataObj',
          needEllipsis:true,
          tdTemplate:this.ratingTmpl,
          width: 100,
          show: type === 'XL'
        },
        {
          title: 'polManagement.table.scale',
          width: 200,
          field: 'scaleName',
          show: type === 'CN'
        },
        {
          title: 'polManagement.table.rankGroup',
          field: 'rankOfGroupName',
          width: 250,
          show: type === 'TH'|| type ===  'TCCV'
        },
        {
          title: 'polManagement.table.rankGroup',
          field: 'dataObj',
          tdTemplate:this.rankGroupTmpl,
          width: 250,
          show: type === 'CB'
        },
        {
          title: 'TCCV',
          field: 'rankOfGroupName',
          width: 150,
          tdTemplate: this.tccvTmpl,
          show: type ===  'TCCV'
        },
        {
          title: 'HĐLĐ',
          field: 'typeOfContractName',
          width: 250,
          show: type === 'TH'
        },
        {
          title: this.displayTitle +' '+ this.translate.instant('polManagement.table.system'),
          field: 'systemCoefficient',
          width: 100,
          show: true
        },
        {
          title:  this.displayTitle,
          field: 'realCoefficient',
          tdTemplate: this.coefficientTmpl,
          width: 100,
          show: true
        },
        {
          title: 'polManagement.table.fromDate',
          field: 'dataObj',
          tdTemplate: this.fromDateTmpl,
          width: 150,
          fixed: window.innerWidth > 1024,
          show: true
        },
        {
          title: 'polManagement.table.toDate',
          field: 'dataObj',
          tdTemplate: this.toDateTmpl,
          width: 150,
          fixed: window.innerWidth > 1024,
          show: true
        },
        {
          title: 'polManagement.table.typeBonus',
          field: 'typeBonus',
          width: 250,
          show: true
        },
        {
          title: 'polManagement.table.status',
          field: 'flagStatus',
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          tdTemplate:this.flagStatus,
          width: 150,
          fixed: window.innerWidth > 1024,
          show: true
        },
        {
          title: 'polManagement.table.createBy',
          field: 'createdBy',
          width: 150,
          fixed: window.innerWidth > 1024,
          show: true
        },
        {
          title: 'polManagement.table.comments',
          field: 'contents',
          width: 200,
          fixed: window.innerWidth > 1024,
          show: true
        },
        {
          title: ' ',
          field: 'dataObj',
          tdTemplate:this.action,
          width: 120,
          fixedDir: 'right',
          fixed: window.innerWidth > 1024,
          show: true
        },
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: 10,
      pageIndex: 1,
      showCheckbox: true,
      showFrontPagination: false
    };
  }
  changePage(page:number){
    this.params.page = page - 1;
    this.search();
  }
  search():void {
    this.loading = true;
    this.dataTable = [];
    this.checked = [];
    this.checkParams();
    this.achievementBonusService.getListData(this.params).subscribe(data => {
      this.dataTable = data.data?.listData || [];
      this.dataTable.forEach( (item,index) => {
        item.index = index;
        item.typeBonus = item.isSpecial === "Y"?this.translate.instant("polManagement.label.bonusSpecial")
          :this.translate.instant("polManagement.label.bonus");
        item.dataObj = this.setData(item);
        item.dataObj.isValid = true;
        item.contents = item.flagStatus !== 2 ? item.contents:'';
        item.dataObj.realCoefficient= String(item.realCoefficient);
      });
      this.tableConfig.total = data.data?.count;
      this.loading = false;
    }, error => {
      this.loading = false;
      this.toastrCustom.error(error.message);
    });
  }
  onSearch(){
      this.params.page = 0;
      this.tableConfig.pageIndex = 1;
      this.search();
  }
  setData(item: AchBonusResponse){
    const obj = {...item};
    delete obj.dataObj;
    return obj;
  }
  convertDate(item: AchBonusResponse){
    const obj = {...item};
    obj.fromDate =  moment(new Date(obj.fromDate)).format('DD/MM/YYYY');
    obj.toDate = moment(new Date (obj.toDate)).format('DD/MM/YYYY');
    return obj;
  }

  getPeriodDate():void {
    this.achievementBonusService.getListPeriodDate().subscribe(data => {
      this.objectSearch.periodDate = data.data[0];
      data.data.forEach((item:string) => {
        this.listPeriod.push({
          value: item
        });
      })
      this.search();
    }, error => {
      this.toastrCustom.error(error.message);
    });
  }
  getRatingData():void {
    this.achievementBonusService.getRating().subscribe(data => {
      this.listRating = data.data ||[];
    }, error => {
      this.toastrCustom.error(error.message);
    });
  }
  getLookupCode():void {
    this.achievementBonusService.getLookupCode('TCCV').subscribe(data => {
      this.listTCCV = data.data ||[];
    }, error => {
      this.toastrCustom.error(error.message);
    });
  }
  getListRankGroup():void {
    const params = {
      id: "",
      name: "",
      type: "AB",
      page: 0,
      size: maxInt32
    }
    this.achievementBonusService.getRankGroup(params).subscribe(data => {
      this.listRankGroup = data.data.content ||[];
    }, error => {
      this.toastrCustom.error(error.message);
    });
  }
  buildApproveRequest() {
    return {
      type: this.type,
      achievementBonusDTOS:  this.dataTable.filter(item => item._checked).map(value => {
        return this.convertDate(value.dataObj);
      })
    };
  }

  onChange(event: ParamBonus){
    if(event){
      this.objectSearch.employeeId = event.employeeId
      this.onSearch();
    } else {
      this.objectSearch.employeeId = '';
    }

  }

  showPopup() {
    const modal = this.modal.create({
      nzWidth: '80%',
      nzTitle: ' ',
      nzContent: FormModalShowTreeUnitComponent,
      nzFooter: null,
    });

    modal.afterClose.subscribe((result) => {
      if (result?.orgName) {
        this.orgName = result?.orgName;
        this.objectSearch.orgId = result?.orgId;
      }
    });
  }

  clearOrgInput():void {
    this.orgName = "";
    this.objectSearch.orgId = "";
  }

  override triggerSearchEvent(): void {
    this.search();
  }
  changeTab(event:any){
    switch (event) {
      case 0:
        this.type = 'TH';
        this.displayTitle = 'HSTH';
        this.initTable('TH');
        break;
      case 1:
        this.type = 'CB';
        this.displayTitle = 'HSCB';
        this.getListSystemCoe();
        this.getListRankGroup();
        this.initTable('CB');
        break;
      case 2:
        this.type = 'XL';
        this.displayTitle = 'HSXL';
        this.getListSystemCoe();
        this.getRatingData();
        this.initTable('XL');
        break;
      case 3:
        this.type = 'TCCV';
        this.displayTitle = 'HSTCCV';
        this.getListSystemCoe();
        this.getLookupCode();
        this.initTable('TCCV');
        break;
      case 4:
        this.type = 'CN';
        this.displayTitle = 'HSCN';
        this.initTable('CN');
        break;
      default:
        this.displayTitle = 'HSTH';
        this.initTable('TH');
        break;
    }
    this.onSearch();
  }
  exportExcel() {
    this.checkParams();
    this.loading =  true;
    this.achievementBonusService.exportExcel(this.params).subscribe(
      (data) => {
        const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
        FileSaver.saveAs(blob, 'Template.xlsx');
        this.loading = false;
      },
      (error) => {
        this.toastrCustom.error(error.error.message);
      }
    );
  }
  waitingAproved():void {
    this.submit = true;
    if(!this.validateParams()){
      return;
    }
    const request = this.buildApproveRequest();
    this.loading = true;
    this.achievementBonusService.waitingApprove(request).subscribe(data => {
      this.submit = false;
      this.toastrCustom.success();
      this.loading = false;
      this.onSearch();
      this.checked = [];
      this.description = "";
    }, error => {
      this.loading = false;
      this.toastrCustom.error(error.message);
    });
  }
  onClickTypeImport(typeImport?:string,footerTmpl?: TemplateRef<any>){
    if(typeImport){
      this.typeImport = typeImport;
      this.fileName = 'DAC_BIET'
    } else {
      this.typeImport = this.type;
      this.fileName = this.type;
    }
    this.showUpload(footerTmpl);

  }
  showUpload(footerTmpl?: TemplateRef<any>) {
    this.modalRef = this.modal.create({
      nzWidth: this.getInnerWidth(3.5),
      nzTitle: this.translate.instant('common.label.uploadFileExel'),
      nzContent: ActionDocumentAttachedFileComponent,
      nzComponentParams: {
        mode: Mode.ADD,
        url: this.achievementBonusService.importExcel(),
        microService: MICRO_SERVICE.POL,
        fileName: 'TEMPLATE_THUONG_THANH_TICH_'+this.fileName,
        module: 'SALARY_EVOLUTION',
        urlDownload:this.achievementBonusService.getDownloadUrl(),
        param: this.typeImport,
        requestBlod:{type:this.typeImport},
      },
      nzFooter: footerTmpl,
    });
    this.modalRef.afterClose.subscribe((result) => (result?.refresh ? this.search() : ''));
  }
  getInnerWidth(point: number): number {
    if (window.innerWidth > 767) {
      return window.innerWidth / point > 1100 ? 1100 : window.innerWidth / point;
    }
    return window.innerWidth;
  }
  addNew(index:number){
    this.submit = false;
    const obj = JSON.parse(JSON.stringify(this.dataTable[index]));
    obj.dataObj._checked = true;
    obj._checked = true;
    obj.dataObj.index = index+1;
    obj.index = index+1;
    this.setEmptyData(obj);
    this.dataTable.splice(index+1,0,obj);
    this.setIndex();
  }
  deleteRecord(index:number,objTmp:AchBonusResponse){
    const params = {
      type: this.type,
      objectId: objTmp.objectId
    }
    this.deletePopup.showModal(() => {
      this.loading = true;
      if(objTmp.objectId){
        this.achievementBonusService.deleteBonus(params).subscribe(data => {
          this.toastrCustom.success();
          this.loading = false;
          this.onSearch();
        }, error => {
          this.loading = false;
          this.toastrCustom.error(error.message)
        })
      } else {
        this.dataTable.splice(index,1);
        this.dataTable = [...this.dataTable];
        this.loading = false;
      }
    })
    this.setIndex();
  }
  validateParams(){
    let isValid = true;
     this.dataTable.filter(item => item._checked).forEach(value => {
      value.dataObj._checked = value._checked;
       value.dataObj.isValid = true;
       value.dataObj.isValidNumber = true;
       if(moment(new Date(value.dataObj.fromDate)) > moment(new Date(value.dataObj.toDate))){
          value.dataObj.isValid = false;
          isValid = false;
        } else if(+value.dataObj.realCoefficient !== 0 && !Number(value.dataObj.realCoefficient)) {
         value.dataObj.isValidNumber = false;
         isValid = false;
       } else {
         value.dataObj.isValid = true;
         value.dataObj.isValidNumber = true;
       }
    });
    this.dataTable.filter(item => !item._checked).forEach(value => {
      value.dataObj._checked = false;
    });
    if(this.dataTable.filter(item => item._checked).some(value => !value.dataObj.fromDate || !value.dataObj.toDate || !value.dataObj.realCoefficient)){
        isValid = false;
        this.toastrCustom.error(this.translate.instant("polManagement.validate.requiredParams"));
        return;
    }
    if(this.dataTable.filter(item => item._checked).some(value => !value.dataObj.isValidNumber)){
      isValid = false;
      this.toastrCustom.error(this.translate.instant("polManagement.validate.isValidCoe"));
      return;
    }
    if(this.dataTable.filter(item => item._checked).some(value => !value.dataObj.isValid)){
      isValid = false;
      this.toastrCustom.error(this.translate.instant("polManagement.validate.toDate"));
      return;
    }

    if(this.dataTable.filter(item => item._checked).length === 0) {
      this.toastrCustom.error(this.translate.instant("polManagement.message.plSelectRecord"));
      isValid = false;
    }
    return isValid;
  }
  onKeyPress(event:any){
    const pattern = REGEX_NUMBER;
    if (!pattern.test(event.key)) {
      event.preventDefault();
      return;
    }
  }
  disabledEndtDate = (endValue: Date): boolean => {
    const objDate = this.caclDate();
    if (!endValue) {
      return false;
    }
    return new Date(objDate.dateFrom).getTime() >= endValue.getTime() || endValue.getTime() >= new Date(objDate.dateTo).getTime();
  };
  caclDate(){
    const preMonth = Number(this.listPeriod[0].value?.split("/")[0]-1);
    const nextMonth = Number(this.listPeriod[0].value?.split("/")[0]);
    const year = Number(this.listPeriod[0].value?.split("/")[1]);
    const dateFrom = year+'-'+preMonth+'-'+SALARY_PERIOD.END_DATE;
    const dateTo = year+'-'+nextMonth+'-'+SALARY_PERIOD.START_DATE;
    return {
      preMonth: preMonth,
      nextMonth: nextMonth,
      year: year,
      dateFrom: dateFrom,
      dateTo: dateTo
    }
  }
  onSelectStatus(e:any){
    if(e){
      this.objectSearch.empStatus = [...e.listOfSelected].join(",")
    } else {
      this.objectSearch.empStatus = '';
    }
  }
  setIndex(){
    this.dataTable.forEach((item,index) => {
      item.index = index;
      item.dataObj.index= index;
    })
  }
  setEmptyData(obj:any){
    obj.objectId = '';
    obj.contents = '';
    obj.flagStatus = '';
    obj.dataObj.objectId = '';
    obj.dataObj.realCoefficient = '';
    obj.dataObj.fromDate = '';
    obj.dataObj.toDate = '';
    obj.dataObj.flagStatus ='';
    obj.dataObj.contents = '';
    obj.createdBy = '';
    obj.dataObj.createdBy = '';
    return obj;
  }
  onPaste(event:any) {
    const value = event.clipboardData.getData('text');
    if (+value !== 0 && !Number(value) || value.includes("-")) {
      event.preventDefault();
    }
  }
  changeSelect(event:number,dataObj:AchBonusResponse) {
    if (this.type === 'CB') {
      dataObj.realCoefficient = this.listSystemCoe.find(item => item.headRow === event && (dataObj.alLevels === null || item.alLevels === dataObj.alLevels))?.coefficient || '';
    } else {
      dataObj.realCoefficient = this.listSystemCoe.find(item => item.headColumn === event && item.headRow === dataObj.positionGroupId
        && (dataObj.alLevels === null || item.alLevels === dataObj.alLevels))?.coefficient || '';
    }
    this.dataTable = [...this.dataTable];
  }
  getListSystemCoe():void {
    const params = {
      type: this.type,
    }
    this.achievementBonusService.getSystemCoefficient(params).subscribe(data => {
     this.listSystemCoe = data.data;
    }, error => {
      this.loading = false;
      this.toastrCustom.error(error.message);
    });
  }
  showUploadSumary(footerTmpl?: TemplateRef<any>) {
    this.modalRef = this.modal.create({
      nzWidth: this.getInnerWidth(3.5),
      nzTitle: this.translate.instant('common.label.uploadFileExel'),
      nzContent: ActionDocumentAttachedFileComponent,
      nzComponentParams: {
        mode: Mode.ADD,
        url: this.achievementBonusService.importExcel(),
        microService: MICRO_SERVICE.POL,
        fileName: 'TEMPLATE_THUONG_THANH_TICH_'+this.fileName,
        module: 'SALARY_EVOLUTION',
        param: this.typeImport,
        requestBlod:{type:this.typeImport},
        isHideDownload:true
      },
      nzFooter: footerTmpl,
    });
    this.modalRef.afterClose.subscribe((result) => (result?.refresh ? this.search() : ''));
  }
  onClickSumary(type?:string){
    const params = {
      type: type?type:this.type,
      periodName:this.params.periodDate
    }
    this.achievementBonusService.sumaryAb(params).subscribe(data => {
      this.toastrCustom.success(data.data.message);
    }, error => {
      this.loading = false;
      this.toastrCustom.error(error.message);
    });
  }
}
