import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AchievementBonusComponent } from './achievement-bonus.component';

describe('AchievementBonusComponent', () => {
  let component: AchievementBonusComponent;
  let fixture: ComponentFixture<AchievementBonusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AchievementBonusComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AchievementBonusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
