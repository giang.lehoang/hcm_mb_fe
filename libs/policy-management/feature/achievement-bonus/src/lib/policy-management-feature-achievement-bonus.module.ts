import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { NzCardModule } from 'ng-zorro-antd/card';
import { PolicyManagemenRoutingModule } from '../../../shell/src/lib/policy-management.routing.module';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { SharedUiMbEmployeeDataPickerModule } from '@hcm-mfe/shared/ui/mb-employee-data-picker';
import { SharedUiMbEmployeeInfoModule } from '@hcm-mfe/shared/ui/mb-employee-info';
import { SharedUiMbTableWrapModule } from '@hcm-mfe/shared/ui/mb-table-wrap';
import { SharedUiMbTableModule } from '@hcm-mfe/shared/ui/mb-table';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { AchievementBonusComponent } from './achievement-bonus/achievement-bonus.component';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { SharedUiMbDatePickerModule } from '@hcm-mfe/shared/ui/mb-date-picker';
import { SharedUiMbSelectCheckAbleModule } from '@hcm-mfe/shared/ui/mb-select-check-able';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
@NgModule({
  imports: [CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: AchievementBonusComponent
      }
    ]), TranslateModule, FormsModule, SharedUiMbButtonModule, NzCardModule, PolicyManagemenRoutingModule, NzGridModule, ReactiveFormsModule, SharedUiMbSelectModule, SharedUiMbInputTextModule, NzIconModule, SharedUiMbEmployeeDataPickerModule, SharedUiMbEmployeeInfoModule, SharedUiMbTableWrapModule, SharedUiMbTableModule, NzTagModule, SharedUiLoadingModule, NzTabsModule, SharedUiMbDatePickerModule, SharedUiMbSelectCheckAbleModule, NzDropDownModule, NzToolTipModule],
  declarations: [AchievementBonusComponent],
  exports: [AchievementBonusComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
})
export class PolicyManagementFeatureAchievementBonusModule {}
