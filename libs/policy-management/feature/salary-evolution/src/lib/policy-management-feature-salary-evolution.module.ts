import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SalaryEvolutionComponentComponent } from './salary-evolution-component/salary-evolution-component.component';
import { TranslateModule } from '@ngx-translate/core';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { NzTableModule } from 'ng-zorro-antd/table';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzFormModule } from 'ng-zorro-antd/form';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { SharedUiMbEmployeeInfoModule } from '@hcm-mfe/shared/ui/mb-employee-info';
import { SharedUiMbTableWrapModule } from '@hcm-mfe/shared/ui/mb-table-wrap';
import { SharedUiMbTableModule } from '@hcm-mfe/shared/ui/mb-table';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { SharedUiMbTableMergeCellWrapModule } from '@hcm-mfe/shared/ui/mb-table-merge-cell-wrap';
import { SharedUiMbTableMergeCellModule } from '@hcm-mfe/shared/ui/mb-table-merge-cell';
import { MapPipe } from '@hcm-mfe/shared/pipes/map';

@NgModule({
  imports: [CommonModule, FormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: SalaryEvolutionComponentComponent
      }
    ]),
    TranslateModule, NzToolTipModule, SharedUiMbButtonModule, NzTableModule, SharedUiMbInputTextModule, SharedUiMbSelectModule, NzCardModule, NzFormModule, SharedUiMbEmployeeInfoModule, SharedUiMbTableWrapModule, SharedUiMbTableModule, NzTagModule, SharedUiLoadingModule, SharedUiMbTableMergeCellWrapModule, SharedUiMbTableMergeCellModule],
  declarations: [SalaryEvolutionComponentComponent],
  exports: [SalaryEvolutionComponentComponent],
  providers: [MapPipe]
})
export class PolicyManagementFeatureSalaryEvolutionModule {}
