import { ChangeDetectorRef, Component, Injector, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import {Pagination, TableConfig } from '@hcm-mfe/shared/data-access/models';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { EMPLOYEE_TYPE, flagStatus, SalaryModel, SalaryParams } from '@hcm-mfe/policy-management/data-access/models';
import { SalaryEvolutionService, UrlConstant } from '@hcm-mfe/policy-management/data-access/service';
import {renderFlagStatus} from "@hcm-mfe/policy-management/helper";
import { UploadFileService } from '@hcm-mfe/model-organization/data-access/services';
import { FunctionCode } from '@hcm-mfe/shared/common/enums';
import { MICRO_SERVICE, Mode } from '@hcm-mfe/shared/common/constants';
import { ActionDocumentAttachedFileComponent } from '@hcm-mfe/goal-management/ui/form-input';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { MapPipe } from '@hcm-mfe/shared/pipes/map';
@Component({
  selector: 'app-salary-evolution-component',
  templateUrl: './salary-evolution-component.component.html',
  styleUrls: ['./salary-evolution-component.component.scss'],
  providers: [
    UploadFileService,
  ],
})
export class SalaryEvolutionComponentComponent extends BaseComponent implements OnInit {
  @ViewChild('actionTmpl', {static: true}) dataTmp!: TemplateRef<NzSafeAny>;
  @ViewChild('actionFactorTmpl', {static: true}) actionFactor!: TemplateRef<NzSafeAny>;
  @ViewChild('flagStatusTmpl', {static: true}) flagStatus!: TemplateRef<NzSafeAny>;
  nzWidthConfig: string[] = ['50px','150px','100px','120px','50px','50px','100px','100px','100px',
    '100px','100px','100px','100px',
    '100px','100px','100px','150px','100px','120px','120px'
  ]

  dataTable: SalaryModel[] = [];
  tableConfig: TableConfig = new TableConfig();
  paramSearch: SalaryParams = new SalaryParams();
  listStatus: any = [];
  pagination = new Pagination();
  isLoadingPage: boolean = false;
  modalRef: NzModalRef | undefined;
  renderFlagStatus = renderFlagStatus;
  functionCode: string;
  constructor(
    private salaryEvolutionService: SalaryEvolutionService,
    private cdr: ChangeDetectorRef,
    public uploadFile: UploadFileService,
    private mapPipe: MapPipe = new MapPipe(),
    injector: Injector
  ) {
    super(injector);
    this.functionCode = FunctionCode.POL_SALARY_EVOLUTION;
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.POL_SALARY_EVOLUTION}`);
  }
  ngOnInit(): void {
    const empl =  localStorage.getItem("employeeInfo");
    if(empl){
      localStorage.removeItem("employeeInfo");
    }
    this.tableConfig.showFrontPagination = true;
    this.listStatus = flagStatus.STATUS_LIST;
    this.initTable();
    this.onSearch();
  }
  onChange(event: any){
    this.paramSearch = event;
    this.getData();
  }
  getData() {
    const request = {
      employeeCode: this.paramSearch.employeeCode ? this.paramSearch.employeeCode:'-1',
      flagStatus: this.paramSearch.statusRecord,
      page: {...this.pagination}.pageNumber - 1,
      pageSize: 10
    }
    this.isLoadingPage = true;
    this.cdr.detectChanges();
    this.salaryEvolutionService.getListData(request).subscribe(
      (data) => {
        this.dataTable = data.data.content;
        this.isLoadingPage = false;
        this.tableConfig.total = data.data.totalElements;
        this.dataTable.forEach(sl => {
          const type = Number(sl.employeeType);
          if(type){
            sl.employeeTypeName = EMPLOYEE_TYPE[type - 1]
          }
          sl.dataTmp = sl;
          sl.curCode = sl.curCode === 'VND'? 'VNĐ': sl.curCode;
          sl.fromDate = this.mapPipe.transform(sl.fromDate,'date');
          sl.toDate = this.mapPipe.transform(sl.toDate,'date');
          sl.salaryAmount = this.mapPipe.transform(sl.salaryAmount,'currencyNumber');
          sl.salaryReal = this.mapPipe.transform(sl.salaryReal,'currencyNumber');
          sl.packAmount = this.mapPipe.transform(sl.packAmount,'currencyNumber');
          sl.performanceAmount = this.mapPipe.transform(sl.performanceAmount,'currencyNumber');
          sl.performanceReal=this.mapPipe.transform(sl.performanceReal,'currencyNumber');
        })
        this.cdr.detectChanges();
      },
      (error) => {
        this.isLoadingPage = false;
        this.toastrCustom.error(error?.error?.message);
        this.cdr.detectChanges()
      }
    );
  }
  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: "STT",
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          rowspan: 2,
          fixed: window.innerWidth > 1024,
          width: 50,
          show: true,
          fixedDir: 'left',
        },
        {
          title: 'polManagement.table.employeeCode',
          field: 'employeeCode',
          thClassList: ['text-center'],
          width: 150,
          fixed: window.innerWidth > 1024,
          rowspan: 2,
          show: false,
          fixedDir: 'left',
        },
        {
          title: 'polManagement.table.fromDate',
          field: 'fromDate',
          thClassList: ['text-center'],
          width: 100,
          fixed: window.innerWidth > 1024,
          rowspan: 2,
          show: true,
        },
        {
          title: 'polManagement.table.toDate',
          field: 'toDate',
          thClassList: ['text-center'],
          width: 100,
          fixed: window.innerWidth > 1024,
          rowspan: 2,
          show: true
        },
        {
          title: 'LCB',
          colspan: 4,
          width:400,
          child: [
            {
              title: 'polManagement.table.salaryGradeName',
              field: 'salaryGradeName',
              tdClassList: ['text-right'],
              thClassList: ['text-center'],
              rowspan: 2,
              fixed: window.innerWidth > 1024,
              show: true,
            },
            {
              title: 'polManagement.table.salaryRankName',
              field: 'salaryRankName',
              tdClassList: ['text-right'],
              thClassList: ['text-center'],
              rowspan: 2,
              fixed: window.innerWidth > 1024,
              show: true
            },
            {
              title: 'polManagement.table.salaryAmount',
              field: 'salaryAmount',
              thClassList: ['text-center'],
              tdClassList: ['text-right'],
              fixed: window.innerWidth > 1024,
              show: true
            },
            {
              title: 'polManagement.table.salaryPercent',
              field: 'salaryPercent',
              thClassList: ['text-center'],
              tdClassList: ['text-right'],
              fixed: window.innerWidth > 1024,
              show: true
            },
            // {
            //   title: 'polManagement.table.salaryReal',
            //   field: 'salaryReal',
            //   tdClassList: ['text-right'],
            //   fixed: window.innerWidth > 1024,
            //   show: false
            // },

          ]
        },
        {
          title: 'THS',
          colspan: 4,
          width:400,
          child: [
            {
              title: 'polManagement.table.performanceFactor',
              field: 'performanceFactor',
              thClassList: ['text-center'],
              tdClassList: ['text-right'],
              fixed: window.innerWidth > 1024,
              show: true
            },
            {
              title: 'polManagement.table.performanceAmount',
              field: 'performanceAmount',
              thClassList: ['text-center'],
              tdClassList: ['text-right'],
              fixed: window.innerWidth > 1024,
              show: true
            },
            {
              title: 'polManagement.table.performancePercent',
              field: 'performancePercent',
              thClassList: ['text-center'],
              tdClassList: ['text-right'],
              fixed: window.innerWidth > 1024,
              show: true
            },
            {
              title: 'polManagement.table.periodCurFactor',
              field: 'periodCurFactor',
              thClassList: ['text-center'],
              fixed: window.innerWidth > 1024,
              tdClassList: ['text-right'],
              show: true
            },
            // {
            //   title: 'polManagement.table.performanceReal',
            //   field: 'performanceReal',
            //   fixed: window.innerWidth > 1024,
            //   tdClassList: ['text-right'],
            //   show: false
            // },
          ]
        },
        {
          title: 'polManagement.table.packAmount',
          field: 'packAmount',
          rowspan: 2,
          thClassList: ['text-center'],
          tdClassList: ['text-right'],
          width: 100,
          fixed: window.innerWidth > 1024,
          show: true
        },
        {
          title: 'polManagement.table.periodCurPoint',
          field: 'periodCurPoint',
          thClassList: ['text-center'],
          rowspan: 2,
          width: 100,
          fixed: window.innerWidth > 1024,
          tdClassList: ['text-right'],
          show: true
        },
        {
          title: 'polManagement.table.salaryType',
          field: 'salaryType',
          thClassList: ['text-center'],
          rowspan: 2,
          width: 100,
          fixed: window.innerWidth > 1024,
          show: true
        },
        {
          title: 'polManagement.table.salaryVersionName',
          field: 'salaryVersionName',
          thClassList: ['text-center'],
          rowspan: 2,
          width: 120,
          fixed: window.innerWidth > 1024,
          show: true
        },
        {
          title: 'polManagement.table.employeeTypeName',
          field: 'employeeTypeName',
          thClassList: ['text-center'],
          fixed: window.innerWidth > 1024,
          width: 150,
          rowspan: 2,
          show: true
        },
        {
          title: 'polManagement.table.curCode',
          field: 'curCode',
          thClassList: ['text-center'],
          width:50,
          rowspan: 2,
          show: false
        },
        {
          title: 'polManagement.table.status',
          field: 'flagStatus',
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          tdTemplate: this.flagStatus,
          width: 150,
          fixed: window.innerWidth > 1024,
          rowspan: 2,
        },
        {
          title: ' ',
          field: 'salaryProcessId',
          tdTemplate: this.dataTmp,
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          width: 120,
          fixed: window.innerWidth > 1024,
          fixedDir: 'right',
          rowspan: 2,
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: 10,
      pageIndex: 1
    };
  }
  search(pageNumber: number) {
    this.pagination.pageNumber = pageNumber;
    this.getData();
  }
  onSearch() {
    this.tableConfig.pageIndex = 1;
    this.search(1);
  }

  deleteRecord(dataTmp: SalaryModel,event:any){
    event.stopPropagation();
    this.deletePopup.showModal(() => {
      this.isLoadingPage = true;
      this.salaryEvolutionService.deleteSalaryEvolution(dataTmp.salaryProcessId).subscribe(data => {
        this.isLoadingPage = false;
        this.toastrCustom.success();
        this.getData();
      }, (error: any) => {
        this.isLoadingPage = false;
        this.toastrCustom.error(error.message)
      })
    })
  }
  onClickItem(employee: SalaryModel) {
    const employeeTmp = {...employee};
    delete employeeTmp.dataTmp;
    localStorage.setItem("employeeInfo", JSON.stringify(employeeTmp));
    localStorage.setItem("objFunction",JSON.stringify(this.objFunction));
    this.router.navigate(['/policy-management/salary-process/detail'], { state: { data: employeeTmp } });
  }
  onClickNew(){
    localStorage.setItem("objFunction",JSON.stringify(this.objFunction));
    this.router.navigate(['/policy-management/salary-process/create-salary-evoltuion']);
  }
  getInnerWidth(point: number): number {
    if (window.innerWidth > 767) {
      return window.innerWidth / point > 1100 ? 1100 : window.innerWidth / point;
    }
    return window.innerWidth;
  }
  showUpload(footerTmpl: TemplateRef<any>) {
    this.modalRef = this.modal.create({
      nzWidth: this.getInnerWidth(3.5),
      nzTitle: this.translate.instant('common.label.uploadFileExel'),
      nzContent: ActionDocumentAttachedFileComponent,
      nzComponentParams: {
        mode: Mode.ADD,
        url: this.salaryEvolutionService.importExcel(),
        microService: MICRO_SERVICE.POL,
        fileName: 'TEMPLATE_DIEN_BIEN_LUONG',
        module: 'SALARY_EVOLUTION',
        urlDownload:this.salaryEvolutionService.getDownloadUrl()
      },
      nzFooter: footerTmpl,
    });
    this.modalRef.afterClose.subscribe((result) => (result?.refresh ? this.onSearch() : ''));
  }
}
