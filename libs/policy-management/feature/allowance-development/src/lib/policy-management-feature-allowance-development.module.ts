import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {AllowanceDevelopmentComponent} from "./allowance-development/allowance-development.component";
import {RouterModule} from "@angular/router";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzCardModule} from "ng-zorro-antd/card";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {NzGridModule} from "ng-zorro-antd/grid";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {PolicyManagemenRoutingModule} from "../../../shell/src/lib/policy-management.routing.module";
import {NzIconModule} from "ng-zorro-antd/icon";
import {SharedUiMbEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/mb-employee-data-picker";
import {SharedUiMbEmployeeInfoModule} from "@hcm-mfe/shared/ui/mb-employee-info";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {NzTagModule} from "ng-zorro-antd/tag";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';

@NgModule({
  imports: [CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: AllowanceDevelopmentComponent
      }
    ]), TranslateModule, FormsModule, SharedUiMbButtonModule, NzCardModule, PolicyManagemenRoutingModule, NzGridModule, ReactiveFormsModule, SharedUiMbSelectModule, SharedUiMbInputTextModule, NzIconModule, SharedUiMbEmployeeDataPickerModule, SharedUiMbEmployeeInfoModule, SharedUiMbTableWrapModule, SharedUiMbTableModule, NzTagModule, SharedUiLoadingModule, NzToolTipModule],
  declarations: [AllowanceDevelopmentComponent],
  exports: [AllowanceDevelopmentComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class PolicyManagementFeatureAllowanceDevelopmentModule {}
