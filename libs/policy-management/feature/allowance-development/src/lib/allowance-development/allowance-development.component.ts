import {Component, Injector, Input, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {ActionDocumentAttachedFileComponent} from "@hcm-mfe/goal-management/ui/form-input";
import { Mode } from "@hcm-mfe/shared/common/constants";
import {NzModalRef} from "ng-zorro-antd/modal";
import {FormModalShowTreeUnitComponent} from "@hcm-mfe/shared/ui/form-modal-show-tree-unit";
import {Pagination} from "@hcm-mfe/shared/data-access/models";
import {MBTableConfig} from "@hcm-mfe/shared/data-access/models";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {AllowanceDevelopmentService, SalaryEvolutionService} from "@hcm-mfe/policy-management/data-access/service";
import {
  flagStatus,
  IAllowanceType,
  IBaseResponse, IInfoEmployee,
  IListAllowanceProcess
} from "@hcm-mfe/policy-management/data-access/models";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {renderFlagStatusAllowance} from "@hcm-mfe/policy-management/helper";

@Component({
  selector: 'app-allowance-development',
  templateUrl: './allowance-development.component.html',
  styleUrls: ['./allowance-development.component.scss'],
})
export class AllowanceDevelopmentComponent extends BaseComponent implements OnInit {
  modalRef: NzModalRef | undefined;
  employee: any;
  orgName: string = "";
  dataTable: IListAllowanceProcess[] = [];
  @ViewChild('actionTmpl', {static: true}) action!: TemplateRef<NzSafeAny>;
  @ViewChild('flagStatusTmpl', {static: true}) isAprrove!: TemplateRef<NzSafeAny>;
  renderFlagStatus = renderFlagStatusAllowance;
  functionCode = FunctionCode.ALLOWANCE_DEVELOPMENTS_LIST;
  listStatus = flagStatus.STATUS_LIST;
  heightTable = { x: '80vw', y: '27em'};
  pagination = new Pagination();
  tableConfig: MBTableConfig = new MBTableConfig();
  infoEmployee: IInfoEmployee = {
    employeeCode: '',
    fullName: '',
    orgName: '',
    positionName: ''
  };
  listAllowanceType: IAllowanceType[] = [];
  params = {
    orgId: "",
    payOffId: "",
    allowanceTypeId: "",
    regionId: "",
    empId: "",
    flagStatuses: "",
    size: this.pagination.pageSize,
    pageNumber: {...this.pagination}.pageNumber - 1
  }
  constructor(
    injector : Injector,
    private allowanceDevService: AllowanceDevelopmentService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ALLOWANCE_DEVELOPMENTS_LIST}`);
  }

  ngOnInit(): void {
    this.fetchDataDropDown();
    this.tableConfig.showFrontPagination = true;
    this.initTable();
  }
  initData() {
    this.convertParamToEmptyString(this.params);
    this.allowanceDevService.getListAllowanceProcess(this.params).subscribe(res => {
      if(res.data.data.length) {
        this.dataTable = res.data.data;
        this.dataTable.forEach(data => {
          const dataTemp = data;
          data['temp'] = dataTemp;
        })
      } else {
        this.dataTable = [];
      }
      this.tableConfig.total = res.data.totalElements;
      this.isLoading = false;
    }, () => {
      this.isLoading = false;
    })
  }
  convertParamToEmptyString(params: any){
    params.orgId = params.orgId ? params.orgId : '';
    params.payOffId = params.payOffId ? params.payOffId : '';
    params.allowanceTypeId = params.allowanceTypeId ? params.allowanceTypeId : '';
    params.regionId = params.regionId ? params.regionId : '';
    params.empId = params.empId ? params.empId : '';
  }
  fetchDataDropDown() {
    this.allowanceDevService.getAllowanceType().subscribe((data: IBaseResponse<IAllowanceType[]>) => {
          this.listAllowanceType = data.data;
    }, (error) => {
          this.toastrCustom.error(error.error?.message);
    })
  }
  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'polManagement.table.fromDate',
          field: 'fromDate',
          thClassList: ['text-center'],
          pipe: 'date',
          width: 60,
        },
        {
          title: 'polManagement.table.toDate',
          field: 'toDate',
          thClassList: ['text-center'],
          width: 70,
          fixed: window.innerWidth > 1024,
          pipe: 'date',
        },
        {
          title: 'polManagement.table.allowanceTypeName',
          field: 'allowanceTypeName',
          thClassList: ['text-center'],
          width: 110,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'polManagement.table.money',
          field: 'amount',
          thClassList: ['text-center'],
          tdClassList: ['text-right'],
          width: 60,
          fixed: window.innerWidth > 1024,
          pipe: 'currencyNumber'
        },
        {
          title: 'polManagement.table.curCode',
          thClassList: ['text-center'],
          field: 'currency',
          width: 50,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'polManagement.table.note',
          thClassList: ['text-center'],
          field: 'note',
          width: 110,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'polManagement.table.status',
          field: 'isApprove',
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          tdTemplate: this.isAprrove,
          width: 80,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'polManagement.table.action',
          tdTemplate: this.action,
          field: 'temp',
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          width: 60,
          fixed: window.innerWidth > 1024,
          fixedDir: 'right',
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1,

    };
  }

  getInnerWidth(point: number): number {
    if (window.innerWidth > 767) {
      return window.innerWidth / point > 1100 ? 1100 : window.innerWidth / point;
    }
    return window.innerWidth;
  }
  clearOrgInput() {
    this.orgName = '';
    this.params.orgId = '';
  }
  summarize() {
    this.isLoading = true;
    this.allowanceDevService.summarizeAllowance().subscribe(() => {
      this.isLoading = false;
      this.initData();
    }, (err) => {
      this.isLoading = false;
      if (err?.status === 400 && err?.message) {
        this.toastrCustom.error(err.message);
      } else {
        this.toastrCustom.error(this.translate.instant('common.notification.error'));
      }
    })
  }
  showDeleteConfirm(id: number, $event: Event): void {
    $event.stopPropagation();
    this.deletePopup.showModal(() => this.deleteAllowanceItem(id));
  }
  deleteAllowanceItem(id: number){
    this.isLoading = true;
    this.allowanceDevService.deleteAllowance(id).subscribe(
      ()=>{
          this.toastrCustom.success(this.translate.instant('common.notification.success'));
          this.initData();
          this.isLoading = false;
      }, (err) => {
        this.isLoading = false;
        if (err?.status === 400 && err?.message) {
          this.toastrCustom.error(err.message);
        } else {
          this.toastrCustom.error(this.translate.instant('common.notification.error'));
        }
      }
    )
  }

  onKeyUp(event: any) {
    if(event.key === 'Enter') {
      this.search();
    }
  }
  onChange(event: any){
    if (event) {
      this.infoEmployee = {...event};
      this.params.empId = event?.empId;
      this.search();
    } else {
      this.infoEmployee = {fullName: "", orgName: "", positionName: ""};
      this.params.empId = '';
    }
    localStorage.setItem("employeeInfoA", JSON.stringify(event));
  }
  onClickItem(employee: any) {
    const employeeTmp = {id: employee.prAllowanceProcessId};
    this.router.navigate(['/policy-management/allowance-development/detail']);
    localStorage.setItem("objectAllowance", JSON.stringify(employeeTmp));
  }

  showModalOrg(event: any): void {
    const modal = this.modal.create({
      nzWidth: '85vw',
      nzTitle: '',
      nzContent: FormModalShowTreeUnitComponent,
      nzFooter: null,
    });
    modal.afterClose.subscribe((result) => {
      if (result) {
        this.orgName = result.orgName;
        this.params.orgId = result.orgId
      }
    });
  }

  search(pageNumber?: number) {
    if (!this.params.empId) {
      this.isLoading = true;
      this.params.empId = '-1';
      this.initData();
      return;
    }
    if (pageNumber) {
      this.pagination.pageNumber = pageNumber - 1 ;
      this.params.pageNumber = pageNumber - 1;
    }
    this.isLoading = true;
    this.initData();
  }
  redirectToCreate() {
    this.router.navigateByUrl('/policy-management/allowance-development/create');
  }
  redirectToEdit(id: number) {
    const object = {
      isEdit: true,
      id: id
    }
    localStorage.setItem('objectAllowance', JSON.stringify(object));
    this.router.navigateByUrl('/policy-management/allowance-development/edit');
  }

  showModalUploadFile(footerTmpl: TemplateRef<any>) {
    this.modalRef = this.modal.create({
      nzWidth: this.getInnerWidth(3.5),
      nzTitle: this.translate.instant('common.label.uploadFileExel'),
      nzContent: ActionDocumentAttachedFileComponent,
      nzComponentParams: {
        mode: Mode.ADD,
        url: 'urlEndPointAllowanceProcess',
        endpoint: 'END_POINT_POLICY_MANAGEMENT',
        microService: 'POL',
        serviceName: 'POLICY_MANAGEMENT',
        fileName: 'TEMPLATE_DIEN_BIEN_PHU_CAP'
      },
      nzFooter: footerTmpl,
    });
    this.modalRef.afterClose.subscribe((result) => (result?.refresh ? this.initData() : ''));
  }
  override triggerSearchEvent(): void {
    this.search();
  }
}
