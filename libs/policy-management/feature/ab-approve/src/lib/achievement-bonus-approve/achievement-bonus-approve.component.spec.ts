import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AchievementBonusApproveComponent } from './achievement-bonus-approve.component';

describe('AchievementBonusComponent', () => {
  let component: AchievementBonusApproveComponent;
  let fixture: ComponentFixture<AchievementBonusApproveComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AchievementBonusApproveComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AchievementBonusApproveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
