import { Component, Injector, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import {
  ABApprove, ABComment, ABType, ApproveABResquest, TYPE_BONUS,
  EMPLOYEE_STATUS, flagStatus, ObjectReject, ParamApprove, ParamBonus, RejectABResquest,
} from '@hcm-mfe/policy-management/data-access/models';
import { MBTableConfig } from '@hcm-mfe/shared/data-access/models';
import { FunctionCode } from '@hcm-mfe/shared/common/enums';
import { FormModalShowTreeUnitComponent } from '@hcm-mfe/shared/ui/form-modal-show-tree-unit';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { AchievementBonusService } from '../../../../../data-access/service/src/lib/achievement-bonus.service';
import {renderFlagStatus} from "@hcm-mfe/policy-management/helper";
import { NzModalRef } from 'ng-zorro-antd/modal';

@Component({
  selector: 'app-achievement-bonus-approve',
  templateUrl: './achievement-bonus-approve.component.html',
  styleUrls: ['./achievement-bonus-approve.component.scss']
})
export class AchievementBonusApproveComponent extends BaseComponent implements OnInit {

  @ViewChild('commentTmpl', {static: true}) comment!: TemplateRef<NzSafeAny>;
  @ViewChild('flagStatusTmpl', {static: true}) flagStatusTmpl!: TemplateRef<NzSafeAny>;
  objectSearch:ParamApprove;
  params:ParamApprove;
  tableConfig:MBTableConfig;
  dataTable:ABApprove[];
  checked:NzSafeAny[];
  description:string;
  orgName:string;
  fullName:string;
  loading:boolean;
  isApprove:boolean = false;
  heightTable = { x: '100vw', y: '50vh'};
  functionCode: string;
  empStatus = EMPLOYEE_STATUS;
  listStatus = flagStatus.STATUS_LIST;
  listPeriod:{value:string}[] = [];
  modalRef: NzModalRef | undefined;
  id:number;
  renderFlagStatus = renderFlagStatus;
  listTypeBonus = TYPE_BONUS;


  constructor(injector:Injector ,readonly achievementBonusService:AchievementBonusService) {
    super(injector);
    this.loading = false;
    this.objectSearch = new ParamApprove();
    this.params = {...this.objectSearch};
    this.fullName = '';
    this.orgName = '';
    this.description = '';
    this.id = 0;

    this.tableConfig = new MBTableConfig();
    this.checked = [];
    this.dataTable = [];
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.POL_SALARY_ACHIEVEMENT_BONUS_APPROVED}`);
    this.functionCode = FunctionCode.POL_SALARY_ACHIEVEMENT_BONUS_APPROVED;
    this.getPeriodDate();
  }

  ngOnInit(): void {
    this.initTable();
  }
  checkParams(){
    this.params.employeeId = this.objectSearch.employeeId?this.objectSearch.employeeId:'';
    this.params.orgId = this.objectSearch.orgId?this.objectSearch.orgId:'';
    this.params.periodDate = this.objectSearch.periodDate?this.objectSearch.periodDate:'';
    this.params.isSpecial = this.objectSearch.isSpecial ? this.objectSearch.isSpecial : '';
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'polManagement.table.employeeCode',
          field: 'employeeCode',
          width: 150,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left',
          show: true
        },
        {
          title: 'polManagement.table.fullName',
          field: 'fullName',
          width: 200,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left',
          show: true
        },
        {
          title: 'polManagement.table.org',
          field: 'orgFullName',
          needEllipsis:true,
          width: 300,
          fixedDir: 'left',
          show: true
        },
        {
          title: 'polManagement.table.jobName',
          field: 'jobName',
          width: 250,
          show: true
        },
        {
          title: 'Level',
          field: 'alLevels',
          width: 150,
          show: true
        },
        {
          title: 'polManagement.table.rankGroup',
          field: 'rankOfGroupName',
          width: 250,
          show: true
        },
        {
          title: 'HSCB' + ' ' + this.translate.instant('polManagement.table.system'),
          field: 'CBS',
          width: 150,
          show: true
        },
        {
          title: 'HSCB',
          field: 'CB',
          width: 100,
          show: true
        },
        {
          title: 'HĐLĐ',
          field: 'typeOfContractName',
          width: 250,
          show: true
        },
        {
          title: 'HSTH' + ' ' + this.translate.instant('polManagement.table.system'),
          field: 'THS',
          width: 150,
          show: true
        },
        {
          title:  'HSTH',
          field: 'TH',
          width: 100,
          show: true
        },

        {
          title: 'polManagement.table.periodCurRate',
          field: 'cldName',
          width: 150,
          show: true
        },
        {
          title: 'HSXL' + ' ' + this.translate.instant('polManagement.table.system'),
          field: 'XLS',
          width: 150,
          show: true
        },
        {
          title:  'HSXL',
          field: 'XL',
          width: 100,
          show: true
        },
        {
          title: 'TCCV',
          field: 'complexMean',
          width: 250,
          show: true
        },
        {
          title: 'HSTCCV' + ' ' + this.translate.instant('polManagement.table.system'),
          field: 'TCCVS',
          width: 150,
          show: true
        },
        {
          title: 'HSTCCV',
          field: 'TCCV',
          width: 100,
          show: true
        },
        {
          title: 'polManagement.table.scale',
          field: 'scaleName',
          width: 250,
          show: true
        },
        {
          title: 'HSCN' + ' ' + this.translate.instant('polManagement.table.system'),
          field: 'CNS',
          width: 150,
          show: true
        },
        {
          title: 'HSCN',
          field: 'CN',
          width: 100,
          show: true
        },
        {
          title: 'polManagement.table.fromDate',
          field: 'fromDate',
          pipe: 'date',
          width: 150,
          fixed: window.innerWidth > 1024,
          show: true
        },
        {
          title: 'polManagement.table.toDate',
          field: 'toDate',
          pipe: 'date',
          width: 150,
          fixed: window.innerWidth > 1024,
          show: true
        },
        {
          title: 'polManagement.table.typeBonus',
          field: 'isSpecial',
          width: 200,
          fixed: window.innerWidth > 1024,
          show: true
        },
        {
          title: 'polManagement.table.comments',
          field: 'contents',
          tdTemplate: this.comment,
          width: 290,
          fixed: window.innerWidth > 1024,
          show: true
        },
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: 10,
      pageIndex: 1,
      showCheckbox: true,
      showFrontPagination: false
    };
  }
  changePage(page:number){
    this.search();
  }

  checkDuplicate(ab:ABApprove, map:Map<string, ABApprove>, type:string, cm:string):ABApprove|undefined{
    const key = `${ab.employeeId}${ab.fromDate}${ab.toDate}${ab.isSpecial}`;

    ab.isSpecial = ab.isSpecial === 'Y' ? this.translate.instant('polManagement.table.abSpecial')
      : this.translate.instant('polManagement.table.ab');
    if(map.has(key)){
      const item = map.get(key);
      item?.comment.push({key:type, value:cm});
      item?.ids.push({objectId:ab.objectId, empId:ab.employeeId, type:type, periodDate:ab.periodDate});
      return item;
    } else {
      ab.ids = [{objectId:ab.objectId, empId:ab.employeeId, type:type, periodDate:ab.periodDate}];
      ab.comment = [{key:type, value:cm}];
      ab.commentSelect = [];
      ab.error = false;
      ab.id = this.id;
      this.id++;
     map.set(key, ab);
     return undefined;
    }
  }

  search():void {
    this.loading = true;
    this.checkParams();
    this.checked = [];
    this.achievementBonusService.getListApprove(this.params).subscribe(data => {
      this.dataTable = [];
      const mapAchive:Map<string, ABApprove> = new Map();
      data.data?.lstComplexes?.forEach(item => {
        const checkItem = this.checkDuplicate(item, mapAchive, ABType.TCCV, ABComment.TCCV);
        if(!checkItem){
          item.TCCVS = item.systemCoefficient;
          item.TCCV = item.realCoefficient;
          this.dataTable.push(item);
        } else {
          checkItem.complexMean = item.complexMean;
          checkItem.TCCVS = item.systemCoefficient;
          checkItem.TCCV = item.realCoefficient;
        }
      });
      data.data?.lstRanks?.forEach(item => {
        const checkItem = this.checkDuplicate(item, mapAchive, ABType.CB, ABComment.CB);
        if(!checkItem){
          item.CBS = item.systemCoefficient;
          item.CB = item.realCoefficient;
          this.dataTable.push(item);
        } else {
          checkItem.rankOfGroupName = item.rankOfGroupName;
          checkItem.CBS = item.systemCoefficient;
          checkItem.CB = item.realCoefficient;
        }
      });
      data.data?.lstPeriods?.forEach(item => {
        const checkItem = this.checkDuplicate(item, mapAchive, ABType.TH, ABComment.TH);
        if(!checkItem){
          item.THS = item.systemCoefficient;
          item.TH = item.realCoefficient;
          this.dataTable.push(item);
        } else {
          checkItem.typeOfContractName = item.typeOfContractName;
          checkItem.THS = item.systemCoefficient;
          checkItem.TH = item.realCoefficient;
        }
      });
      data.data?.lstRatings?.forEach(item => {

        const checkItem = this.checkDuplicate(item, mapAchive, ABType.XL, ABComment.XL);
        if(!checkItem){
          item.XLS = item.systemCoefficient;
          item.XL = item.realCoefficient;
          this.dataTable.push(item);
        } else {
          checkItem.cldName = item.cldName;
          checkItem.XLS = item.systemCoefficient;
          checkItem.XL = item.realCoefficient;
        }
      });
      data.data?.lstUnitSizes?.forEach(item => {
        const checkItem = this.checkDuplicate(item, mapAchive, ABType.CN, ABComment.CN);
        if(!checkItem){
          item.CNS = item.systemCoefficient;
          item.CN = item.realCoefficient;
          this.dataTable.push(item);
        } else {
          checkItem.scaleName = item.scaleName;
          checkItem.CNS = item.systemCoefficient;
          checkItem.CN = item.realCoefficient;
        }
      });
      this.loading = false;
    }, error => {
      this.loading = false;
      this.toastrCustom.error(error.message);
    });
  }
  onSearch(){
      this.tableConfig.pageIndex = 1;
      this.search();
  }


  getPeriodDate():void {
    this.achievementBonusService.getListPeriodDate().subscribe(data => {
      this.objectSearch.periodDate = data.data[0];
      data.data.forEach((item:string) => {
        this.listPeriod.push({
          value: item
        });
      })
      this.search();
    }, error => {
      this.toastrCustom.error(error.message);
    });
  }

  onChange(event: ParamBonus){
    if(event){
      this.objectSearch.employeeId = event.employeeId
      this.fullName = event.fullName;
      this.onSearch();
    } else {
      this.objectSearch.employeeId = '';
      this.fullName = '';
    }

  }

  showPopup() {
    const modal = this.modal.create({
      nzWidth: '80%',
      nzTitle: ' ',
      nzContent: FormModalShowTreeUnitComponent,
      nzFooter: null,
    });

    modal.afterClose.subscribe((result) => {
      if (result?.orgName) {
        this.orgName = result?.orgName;
        this.objectSearch.orgId = result?.orgId;
      }
    });
  }

  clearOrgInput():void {
    this.orgName = "";
    this.objectSearch.orgId = "";
  }

  override triggerSearchEvent(): void {
    this.search();
  }

  waitingAproved():void {
    if(!this.checked.length){
      this.toastrCustom.error(this.translate.instant('polManagement.message.plSelectRecord'));
      return;
    }
    const request:ApproveABResquest = {
      object: [],
      description: []
    };

    this.checked.forEach(item => {
      request.object.push(...item.ids);
    });

    this.loading = true;
    this.achievementBonusService.abApprove(request).subscribe(data => {
      this.toastrCustom.success();
      this.checked = [];
      this.search();
      this.loading = false;
    }, error => {
      this.loading = false;
      this.toastrCustom.error(error.message);
    });
  }

  getInnerWidth(point: number): number {
    if (window.innerWidth > 767) {
      return window.innerWidth / point > 1100 ? 1100 : window.innerWidth / point;
    }
    return window.innerWidth;
  }

  approveAll():void {
    this.deletePopup.showApprove(() => {
      this.loading = true;
      this.achievementBonusService.abApproveAll(this.params).subscribe(data => {
        this.toastrCustom.success();
        this.checked = [];
        this.search();
        this.loading = false;
      }, error => {
        this.loading = false;
        this.toastrCustom.error(error.message);
      });
    });
  };

  reject():void {
    if(!this.checked.length){
      this.toastrCustom.error(this.translate.instant('polManagement.message.plSelectRecord'));
      return;
    }
    const request:RejectABResquest = {
      lstSubRequest: [],
    };

    let checkCm = false;
    for (const item of this.checked){
      if(!item.commentSelect.length){
        item.error = true;
        checkCm =true;
      } else {
        item.error = false;
      }
      const setType = new Map();
      item.commentSelect.forEach((com:{key:string, value:string}) => {
        setType.set(com.key, com.value);
      });
      item.ids.forEach((id:ObjectReject) => {
        if(setType.has(id.type)){
          id.description = setType.get(id.type);
        } else {
          id.description = '';
        }
        request.lstSubRequest.push(id)
      });
    };

    if(checkCm){
      this.toastrCustom.error("Vui lòng chọn ý kiến từ chối");
      return;
    }
    this.loading = true;
    this.achievementBonusService.abReject(request).subscribe(data => {
      this.toastrCustom.success();
      this.checked = [];
      this.search();
      this.loading = false;
    }, error => {
      this.loading = false;
      this.toastrCustom.error(error.message);
    });
  }

}
