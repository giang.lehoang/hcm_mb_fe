import {Component, Injector, OnInit} from "@angular/core";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {
  IAllowanceType,
  IInfoAllowanceEmployee,
  IParamApprove, REGEX_CURRENCY
} from "@hcm-mfe/policy-management/data-access/models";
import * as moment from 'moment';
import {AllowanceDevelopmentService} from "@hcm-mfe/policy-management/data-access/service";
import {IBaseResponse} from "@hcm-mfe/system/data-access/models";
import {MapPipe} from "@hcm-mfe/shared/pipes/map";
import {renderStatusEmployee} from "@hcm-mfe/policy-management/helper";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";


@Component({
  selector: 'app-detail-allowance',
  templateUrl: './detail-allowance.component.html',
  styleUrls: ['./detail-allowance.component.scss'],
})
export class DetailAllowanceComponent extends BaseComponent implements OnInit {
  orgName: string = '';
  employee: any;
  isSubmitted: boolean = false;
  invalidDate: boolean = false;
  description: string = '';
  objectData: any;
  isSubmit: boolean = false;
  isDetailAprrove: boolean = false;
  isDetailList: boolean = false;
  listTypeAllowance: IAllowanceType[] = [];
  listTypeCurrency = [];
  functionCode: string = FunctionCode.ALLOWANCE_DEVELOPMENTS_LIST;
  renderStatusEmployee = renderStatusEmployee;
  fromDate: string = '';
  toDate: string = '';
  isBackDate: boolean = false;
  tmpAmount = '';
  selected:any = {
    empCode:'',
    fullName:''
  };
  bodyRequest: IInfoAllowanceEmployee = {
    employeeId: null,
    fromDate: '',
    toDate: '',
    amount: null,
    note: '',
    prAllowanceId: null
  }
  infoEmployee = {
    employeeCode: "",
    orgName: "",
    jobName: "",
    levelName: "",
    positionName: "",
    salaryRange: "",
    contractTypeName: "",
    organizationScale: "",
    zoneName: "",
    regionName: "",
    flagStatus: "",
    fullName: "",
    approver: "",
    approveDate: "",
    reason: "",
    lastUpdatedBy: "",
    lastUpdatedDate: "",
  };
  constructor(injector : Injector, private allowanceDevService: AllowanceDevelopmentService, private mapPipe: MapPipe = new MapPipe(),) {
    super(injector);
    this.isDetailAprrove = this.route.snapshot.data['mode'] === "detail";
    this.isDetailList = this.route.snapshot.data['mode'] === "detail-allowance";
    const employeeInfo = localStorage.getItem('employeeInfoA') === 'null' ? null : localStorage.getItem('employeeInfoA');
    if(employeeInfo) {
      this.infoEmployee = {...JSON.parse(employeeInfo)};
      this.infoEmployee.contractTypeName = '';
      this.infoEmployee.flagStatus = this.renderStatusEmployee(this.infoEmployee.flagStatus);
      this.selected.empCode = JSON.parse(employeeInfo).empCode;
      this.bodyRequest.employeeId = JSON.parse(employeeInfo).empId;
    } else {
      this.selected = null;
    }
  }

  ngOnInit(): void {
    this.getAllowanceType();
    this.getCurrencyType();
    const objectTemp = localStorage.getItem('objectAllowance');
    if (objectTemp){
      this.isLoading = true;
      this.objectData = JSON.parse(objectTemp);
      const id = this.isDetailAprrove ? this.objectData.prAllowanceProcessId : this.objectData.id;
      this.allowanceDevService.getDetailAllowance(id).subscribe((res: IBaseResponse<IInfoAllowanceEmployee>) => {
        this.isLoading = false;
        res.data.fromDate ? this.fromDate = res.data.fromDate : this.fromDate = '';
        res.data.toDate ? this.toDate = res.data.toDate : this.toDate = '';
        this.bodyRequest.employeeId = res.data.employeeId;
        this.bodyRequest.amount = this.formatToCurrency(res.data.amount, 'currencyNumber');
        this.bodyRequest.toDate = res.data.toDate;
        this.bodyRequest.fromDate = res.data.fromDate;
        this.bodyRequest.prAllowanceId = res.data.prAllowanceId;
        this.bodyRequest.note = res.data.note;
        // @ts-ignore
        this.infoEmployee = {...res.data};
        this.infoEmployee.flagStatus = this.renderStatusEmployee(this.infoEmployee.flagStatus);
      }, () => {
        this.isLoading = false;
      });
    }
  }

  save() {
    debugger;
    this.isSubmitted = true;
    this.fromDate ? this.bodyRequest.fromDate = moment(this.fromDate).format('YYYY-MM-DD') : this.bodyRequest.fromDate = null;
    this.toDate ? this.bodyRequest.toDate = moment(this.toDate).format('YYYY-MM-DD') : this.bodyRequest.toDate = null;
     this.tmpAmount = String(this.bodyRequest.amount);
    this.bodyRequest.amount = this.formatToNumber(this.bodyRequest.amount);
    if (this.tmpAmount && this.bodyRequest.employeeId && this.bodyRequest.prAllowanceId && this.bodyRequest.fromDate && this.bodyRequest.currency) {
      this.isLoading = true;
      this.objectData?.isEdit ? this.updateAllowance() : this.createAllowance();
    }
  }
  createAllowance() {
    this.allowanceDevService.createInfoAllowance(this.bodyRequest).subscribe(() => {
      this.isLoading = false;
      this.toastrCustom.success('Thêm mới thành công');
      this.backToList();
    }, (err) => {
      this.isLoading = false;
      this.formatToCurrency(this.formatToNumber(this.bodyRequest.amount), 'currencyNumber');
      if(err?.status === 400 && err?.message) {
        this.toastrCustom.error(err.message);
      } else {
        this.toastrCustom.error('Thêm mới thất bại');
      }
    })
  }

  approveOrRejectAllowanceProcess(isApprove: boolean) {
    if(!isApprove && !this.description){
      this.isSubmit = true;
      return;
    }
    const request:IParamApprove = {
      action: isApprove ? "APPROVE" : "REJECT",
      reason: '',
      prAllowanceProcessIds: []
    }
    request.prAllowanceProcessIds.push(this.objectData.prAllowanceProcessId);
    this.isLoading = true;
    if(localStorage.getItem('mode')) {
      this.isBackDate = true;
    }

    this.allowanceDevService.approveAllowanceProcess(request, this.isBackDate).subscribe(() => {
      this.toastrCustom.success( isApprove ? 'Phê duyệt thành công' : 'Từ chối thành công');
      this.back();
      this.isLoading = false;
    }, (err) => {
      if(err.status === 400) {
        this.toastrCustom.error(err.message);
      } else {
        this.toastrCustom.error( isApprove ? 'Phê duyệt thất bại' : 'Từ chối thất bại');
      }
      this.isLoading = false;
    })
  }
  updateAllowance() {
    this.allowanceDevService.updateAllowance(this.objectData.id, this.bodyRequest).subscribe(() => {
      this.isLoading = false;
      this.toastrCustom.success('Sửa thành công');
      this.backToList();
    }, (err) => {
      this.isLoading = false;
      this.formatToCurrency(this.formatToNumber(this.bodyRequest.amount), 'currencyNumber');
      if(err?.status === 400 && err?.message) {
        this.toastrCustom.error(err.message);
      } else {
        this.toastrCustom.error('Sửa thất bại');
      }
    })
  }

  backToList() {
    this.router.navigate(['/policy-management/allowance-development']);
  }
  getAllowanceType() {
    this.isLoading = true;
    const params = { flagEnabled: 'Y' };
    this.allowanceDevService.getAllowanceType(params).subscribe((data) => {
      this.listTypeAllowance = data.data;
      this.isLoading = false;
    }, () => {
      this.isLoading = false;
    })
  }
  getCurrencyType() {
    this.isLoading = true;
    const params = { lookupCode: 'LOAI_TIEN_TE' };
    this.allowanceDevService.getCurrencyType(params).subscribe((data) => {
      this.listTypeCurrency = data.data;
      this.bodyRequest.currency = "VND";
      this.isLoading = false;
    }, () => {
      this.isLoading = false;
    })
  }
  onChange(event: any) {
    this.bodyRequest.employeeId = event?.empId;
    this.infoEmployee = {...event};
    this.infoEmployee.contractTypeName = '';
    if (this.infoEmployee.flagStatus) {
      this.infoEmployee.flagStatus = this.renderStatusEmployee(this.infoEmployee.flagStatus);
    } else {
      this.selected = null;
    }
  }
  onChangeSelectDate(event: any) {
    if (this.toDate && this.fromDate) {
      const fromDate = moment(this.fromDate, 'YYYY-MM-DD').toDate();
      const toDate = moment(this.toDate, 'YYYY-MM-DD').toDate();
      const checkDate = toDate.getTime() - fromDate.getTime();
      checkDate < 0 ? this.invalidDate = true : this.invalidDate = false;
    }
  }

  formatToCurrency(value:any,param:any){
    let currency = this.mapPipe.transform(value, param);
    this.bodyRequest.amount = currency;
    return !currency || currency === '-' ? "": currency;
  }
  formatToNumber(value:any){
    value = String(value);
    if(value && value.includes(",")){
      value = value.replaceAll(",","")
    }
    return +value;
  }

  onKeyPress(event:any){
    const pattern = REGEX_CURRENCY;
    if (!pattern.test(event.key)) {
      event.preventDefault();
      return;
    }
  }
  override ngOnDestroy() {
    super.ngOnDestroy();
    localStorage.removeItem('objectAllowance');
    localStorage.removeItem('allowanceInfo');
    localStorage.removeItem('mode');
    localStorage.removeItem("employeeInfoA");

  }
}
