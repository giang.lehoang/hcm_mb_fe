import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DetailAllowanceComponent} from "./detail-allowance/detail-allowance.component";
import {NzCardModule} from "ng-zorro-antd/card";
import {NzGridModule} from "ng-zorro-antd/grid";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiMbEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/mb-employee-data-picker";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {NzIconModule} from "ng-zorro-antd/icon";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {MapPipe} from "@hcm-mfe/shared/pipes/map";

@NgModule({
  imports: [CommonModule, RouterModule.forChild([
    {
      path: '',
      component: DetailAllowanceComponent,
    },
  ]), FormsModule, ReactiveFormsModule, NzCardModule, NzGridModule, TranslateModule, SharedUiMbInputTextModule, SharedUiMbEmployeeDataPickerModule, SharedUiMbButtonModule, SharedUiMbDatePickerModule, SharedUiMbSelectModule, NzIconModule, SharedUiLoadingModule],
  exports: [DetailAllowanceComponent],
  declarations: [DetailAllowanceComponent],
  providers:[MapPipe]
})
export class PolicyManagementFeatureDetailAllowanceModule {}
