import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExeptionAllowancesApprovalComponent } from './exeption-allowances-approval/exeption-allowances-approval.component';
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/mb-employee-data-picker";
import {NzGridModule} from "ng-zorro-antd/grid";
import {RouterModule} from "@angular/router";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedPipesDateFormatModule} from "@hcm-mfe/shared/pipes/date-format";
import {NzTagModule} from "ng-zorro-antd/tag";
import {NzIconModule} from "ng-zorro-antd/icon";


@NgModule({
    imports: [
        CommonModule,
        SharedUiLoadingModule,
        ReactiveFormsModule,
        TranslateModule,
        SharedUiMbEmployeeDataPickerModule,
        NzGridModule,
        RouterModule.forChild([
            {
                path: '',
                component: ExeptionAllowancesApprovalComponent,
            },
        ]),
        SharedUiMbInputTextModule,
        FormsModule,
        SharedUiMbSelectModule,
        SharedUiMbButtonModule,
        SharedUiMbTableModule,
        SharedUiMbTableWrapModule,
        SharedPipesDateFormatModule,
        NzTagModule,
        NzIconModule,
    ],
  declarations: [
    ExeptionAllowancesApprovalComponent
  ],
  exports: [
    ExeptionAllowancesApprovalComponent
  ]
})
export class PolicyManagementFeatureExceptionAllowancesApprovalModule {}
