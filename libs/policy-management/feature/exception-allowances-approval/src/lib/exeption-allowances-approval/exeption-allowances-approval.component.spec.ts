import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExeptionAllowancesApprovalComponent } from './exeption-allowances-approval.component';

describe('ExeptionAllowancesApprovalComponent', () => {
  let component: ExeptionAllowancesApprovalComponent;
  let fixture: ComponentFixture<ExeptionAllowancesApprovalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExeptionAllowancesApprovalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExeptionAllowancesApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
