import {Component, Injector, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {FormGroup} from "@angular/forms";
import {FormModalShowTreeUnitComponent} from "@hcm-mfe/shared/ui/form-modal-show-tree-unit";
import {
  CONFIRM,
  ConstantColor,
  flagStatus,
  IAllowanceExtendListUserInfo,
  IListReceivingExceptionAllowances,
  SalaryParams
} from "@hcm-mfe/policy-management/data-access/models";
import {NzModalRef} from "ng-zorro-antd/modal";
import {Subscription} from "rxjs";
import {
  ExceptionAllowancesApprovalService,
  ListReceivingExceptionAllowancesService
} from "@hcm-mfe/policy-management/data-access/service";
import * as _ from 'lodash';
import {MBTableConfig} from "@hcm-mfe/shared/data-access/models";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import * as events from "events";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {renderFlagStatus} from "@hcm-mfe/policy-management/helper";


@Component({
  selector: 'hcm-mfe-exeption-allowances-approval',
  templateUrl: './exeption-allowances-approval.component.html',
  styleUrls: ['./exeption-allowances-approval.component.scss']
})
export class ExeptionAllowancesApprovalComponent extends BaseComponent implements OnInit {

  private readonly subs: Subscription[] = [];
  isLoadingPage = false;
  isSubmitted = false;
  form: FormGroup;

  get f() {
    return this.form.controls;
  }

  tagGroupNameColor: string = ConstantColor.TAG.groupName;
  modalRef: NzModalRef | undefined;
  dataResponseListUserInput: IAllowanceExtendListUserInfo[] | undefined = undefined;
  reasonApproval = '';
  searchResult: IListReceivingExceptionAllowances[] | undefined;
  tableConfig!: MBTableConfig;
  listRegisterId: number[] = [];
  actionUser = {
    APPROVE: CONFIRM.APPROVE,
    REJECT: CONFIRM.REJECT,
  }
  flagStatus = false;
  renderFlagStatus = renderFlagStatus;
  isBackDate = false;
  heightTable = { x: '100vw', y: '32vh'};

  @ViewChild('selectTmpl', {static: true}) select!: TemplateRef<NzSafeAny>;
  @ViewChild('actionFlagStatus', {static: true}) actionFlagStatus!: TemplateRef<NzSafeAny>;
  @ViewChild('tplTranFormDate', {static: true}) tplTranFormDate!: TemplateRef<NzSafeAny>;
  @ViewChild('tplTranToDate', {static: true}) tplTranToDate!: TemplateRef<NzSafeAny>;
  functionCode: string;

  constructor(
    injector: Injector,
    private readonly listReceivingExceptionAllowancesService: ListReceivingExceptionAllowancesService,
    private readonly exceptionAllowancesApprovalService: ExceptionAllowancesApprovalService
  ) {
    super(injector);
    this.isBackDate = this.route.snapshot.data['mode'] === 'BD';
    this.form = this.fb.group({
      employeeId: [null],
      employeeCode: [null],
      fullName: [null],
      orgId: [null],
      orgName: [null],
      createdBy: [null],
    });
    this.objFunction = this.sessionService.getSessionData(
      this.isBackDate
        ? `FUNCTION_${FunctionCode.RECEIVING_EXCEPTION_ALLOWANCE_APPROVE_BACK_DATE}`
        : `FUNCTION_${FunctionCode.RECEIVING_EXCEPTION_ALLOWANCE_APPROVE}`
    );
    this.functionCode = this.isBackDate ? FunctionCode.RECEIVING_EXCEPTION_ALLOWANCE_APPROVE_BACK_DATE : FunctionCode.RECEIVING_EXCEPTION_ALLOWANCE_APPROVE
  }


  showModalOrg(event: any): void {
    this.modalRef = this.modal.create({
      nzWidth: '85vw',
      nzTitle: '',
      nzContent: FormModalShowTreeUnitComponent,
      nzFooter: null,
    });
    this.modalRef.afterClose.subscribe(result => {
      if (result?.orgName) {
        this.form.patchValue({
          orgId: Number(result?.orgId),
          orgName: result?.orgName,
        });
      }
    });
  }

  clearOrgInput() {
    this.form.patchValue({
      orgId: undefined,
      orgName: '',
    });
  }


  ngOnInit(): void {
    this.initTable();

    const userInfoDropdownList = this.exceptionAllowancesApprovalService.getExceptionAllowancesApprovalUserInfo().subscribe((res) => {
      if (res.data) {
        this.dataResponseListUserInput = res.data;
      }
    }, (err) => this.toastrCustom.error(err.message));

    this.doSearch(1);

    this.subs.push(userInfoDropdownList);
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'polManagement.table.select',
          field: 'select',
          width: 60,
          tdTemplate: this.select,
          show: true,
          thClassList: ['text-nowrap', 'text-center'],
          tdClassList: ['text-nowrap', 'text-center'],
          fixed: window.innerWidth > 1024,
          fixedDir: 'left'
        },
        {
          title: 'polManagement.table.employeeCode',
          field: 'employeeCode',
          width: 120,
        },
        {
          title: 'polManagement.table.fullName',
          field: 'fullName',
          width: 200,
        },
        {
          title: 'polManagement.table.orgName',
          field: 'orgName',
          width: 210,
        },

        {
          title: 'polManagement.table.jobName',
          field: 'jobName',
        },
        {
          title: 'polManagement.table.positionName',
          field: 'posName',
        },

        {
          title: 'polManagement.table.fromDate',
          field: 'fromDate',
          tdTemplate: this.tplTranFormDate,
          width: 100,
        },
        {
          title: 'polManagement.table.toDate',
          field: 'toDate',
          tdTemplate: this.tplTranToDate,
          width: 100,
        },
        {
          title: 'polManagement.table.prAllowanceTypeName',
          field: 'prAllowanceTypeName',
          width: 200,
        },
        {
          title: 'polManagement.table.createdBy',
          field: 'createdBy',
          width: 200,
        },
        // {
        //   title: 'polManagement.table.status',
        //   field: 'flagStatus',
        //   tdTemplate: this.actionFlagStatus
        // },
      ],
      total: 0,
      loading: false,
      size: 'small',
      pageSize: 10,
      pageIndex: 1
    };
  }

  onChange(event:any){
    if(event){
      this.doSearch(1)
    }
  }
  approvalItem(action: CONFIRM) {
    if (this.listRegisterId?.length === 0) {
      this.toastrCustom.error( this.translate.instant('polManagement.validate.requiredApproval'));
      return;
    }
    if (action !== CONFIRM.APPROVE && this.reasonApproval.trim().length <= 0) {
      this.flagStatus = true;
      return;
    } else {
      this.flagStatus = false;
    }
    this.extractedConfirmOrReject(action);
  }

  approvalAllItem(action: CONFIRM){
      this.listRegisterId = this.searchResult?.length ? this.searchResult.map((el: IListReceivingExceptionAllowances) => Number(el.prAllowanceExtendListId)) : [];
    this.extractedConfirmOrReject(action);
  }


  private extractedConfirmOrReject(action: CONFIRM) {
    const approvalItem = this.exceptionAllowancesApprovalService.actionConfirmExceptionAllowances({
      prAllowanceExtendListId: this.listRegisterId,
      action: action === CONFIRM.APPROVE ? CONFIRM.APPROVE : CONFIRM.REJECT,
      contents: this.reasonApproval
    }, this.isBackDate).subscribe(res => {
      if (res) {
        this.toastrCustom.success(action === CONFIRM.APPROVE ?
          this.translate.instant("polManagement.validate.approvalSuccess")
          : this.translate.instant("polManagement.validate.rejectSuccess"));
        this.reasonApproval = '';
        this.doSearch(1);
      }
    }, err => this.toastrCustom.error(err?.message))
    this.subs.push(approvalItem);
  }

  doSearch(pageIndex?: number) {
    this.isSubmitted = true;
    if (this.form.valid) {
      this.isLoading = true;
      this.isLoadingPage = true;
      const formData = _.cloneDeep(this.form.value);
      const getItemExceptionAllowances = this.listReceivingExceptionAllowancesService.getListReceivingExceptionAllowancesBackDate({
        employeeCode: formData.employeeId?.employeeCode,
        orgId: formData.orgId,
        page: null,
        size: null,
        createdBy: formData.createdBy,
        // flagStatus: 2
      }, this.isBackDate).subscribe(res => {
          if (res?.data?.content) {
            this.searchResult = res.data.content;
            this.tableConfig.needScroll = true;
            this.listRegisterId = [];
          }
        }, err => {
          this.isLoadingPage = false;
          this.toastrCustom.error(this.translate.instant("polManagement.message.showMessageError"));
          this.isLoading = false;
        }, () => {
          this.isLoadingPage = false;
          this.isLoading = false;
        }
      )
      this.subs.push(getItemExceptionAllowances);
    }
  }

  onCheckChange(event: events, status: number, employeeCode: string) {
    const id = parseInt(event?.target?.value, 10);
    if (event.target.checked) {
      this.listRegisterId.push(id);
    } else {
      this.listRegisterId.splice(this.listRegisterId.indexOf(id), 1);
    }
  }

  getFlagStatusWithKey(key: string) {
    return flagStatus.STATUS_LIST.find(el => Number(el.key) === Number(key))?.value;
  }

  override ngOnDestroy() {
    this.subs.forEach((s) => s.unsubscribe());
    this.modalRef?.destroy();
  }

}
