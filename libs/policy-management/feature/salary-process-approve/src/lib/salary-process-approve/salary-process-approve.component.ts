import {Component, Injector, OnInit, TemplateRef, ViewChild} from '@angular/core';
import { MBTableConfig, TableConfig } from '@hcm-mfe/shared/data-access/models';
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {
  ApproveAllRequest,
  ApproveResquest, EMPLOYEE_TYPE,
  SalaryModel, SalaryParams,
  SearchApprove
} from "@hcm-mfe/policy-management/data-access/models";
import {SalaryEvolutionService} from "@hcm-mfe/policy-management/data-access/service";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {FormModalShowTreeUnitComponent} from "@hcm-mfe/shared/ui/form-modal-show-tree-unit";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import { MapPipe } from '@hcm-mfe/shared/pipes/map';

@Component({
  selector: 'hcm-mfe-salary-process-approve',
  templateUrl: './salary-process-approve.component.html',
  styleUrls: ['./salary-process-approve.component.scss']
})
export class SalaryProcessApproveComponent extends BaseComponent implements OnInit {
  @ViewChild('flagStatusTmpl', {static: true}) flagStatus!: TemplateRef<NzSafeAny>;
  objectSearch:SearchApprove;
  params:SearchApprove;
  tableConfig:TableConfig = new TableConfig;
  dataTable:SalaryModel[];
  checked:NzSafeAny[];
  description:string;
  setOfCheckedId = new Set<number>();
  submit:boolean;
  orgName:string;
  fullName:string;
  loading:boolean;
  backdate:boolean;
  emp:number | null | undefined;
  nzWidthConfig: string[] = ['50px','50px','50px','150px','200px','400px','100px','100px','50px','50px','100px','100px',
    '100px','100px','100px','100px',
    '100px','100px','100px','120px','150px','120px','150px'
  ];
  functionCode: string;
  isApprove:boolean = false;
  constructor(injector:Injector ,readonly salaryProcessService:SalaryEvolutionService,
              private mapPipe: MapPipe = new MapPipe()) {
    super(injector);
    this.backdate = this.route.snapshot.data['backdate'];
    if(this.backdate){
      this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.POL_SALARY_PROCESS_APPROVE_B}`);
      this.functionCode = FunctionCode.POL_SALARY_PROCESS_APPROVE_B;
    } else {
      this.functionCode = FunctionCode.POL_SALARY_PROCESS_APPROVE;
      this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.POL_SALARY_PROCESS_APPROVE}`);
    }
    console.log(this.objFunction);

    this.loading = false;
    this.objectSearch = {
      createBy: '',
      orgId: '',
      employeeCode: ''
    }

    this.params = {...this.objectSearch};
    this.fullName = '';
    this.orgName = '';
    this.description = '';
    this.submit = false;

    this.tableConfig = new MBTableConfig();
    this.checked = [];
    this.dataTable = [];
  }

  ngOnInit(): void {
    this.search();
    this.initTable();
  }
  private initTable(): void {

    this.tableConfig = {
      headers: [
        {
          title: "STT",
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          rowspan: 2,
          fixed: window.innerWidth > 1024,
          width: 50,
          show: true,
          fixedDir: 'left',
        },
        {
          title: 'polManagement.table.employeeCode',
          field: 'employeeCode',
          thClassList: ['text-center'],
          width: 150,
          fixed: window.innerWidth > 1024,
          rowspan: 2,
          show: true,
          fixedDir: 'left',
        },
        {
          title: 'polManagement.table.fullName',
          field: 'fullName',
          thClassList: ['text-center'],
          width: 150,
          fixed: window.innerWidth > 1024,
          rowspan: 2,
          show: true,
          fixedDir: 'left',
        },
        {
          title: 'polManagement.table.orgName',
          field: 'orgName',
          thClassList: ['text-center'],
          fixed: window.innerWidth > 1024,
          rowspan: 2,
          show: true,
          fixedDir: 'left'
        },
        {
          title: 'polManagement.table.fromDate',
          field: 'fromDate',
          thClassList: ['text-center'],
          width: 100,
          fixed: window.innerWidth > 1024,
          rowspan: 2,
          show: true,
        },
        {
          title: 'polManagement.table.toDate',
          field: 'toDate',
          width: 100,
          fixed: window.innerWidth > 1024,
          rowspan: 2,
          show: true
        },
        {
          title: 'LCB',
          colspan: 4,
          width:400,
          child: [
            {
              title: 'polManagement.table.salaryGradeName',
              field: 'salaryGradeName',
              tdClassList: ['text-right'],
              thClassList: ['text-center'],
              rowspan: 2,
              fixed: window.innerWidth > 1024,
              show: true,
            },
            {
              title: 'polManagement.table.salaryRankName',
              field: 'salaryRankName',
              tdClassList: ['text-right'],
              thClassList: ['text-center'],
              rowspan: 2,
              fixed: window.innerWidth > 1024,
              show: true
            },
            {
              title: 'polManagement.table.salaryAmount',
              field: 'salaryAmount',
              tdClassList: ['text-right'],
              thClassList: ['text-center'],
              fixed: window.innerWidth > 1024,
              show: true
            },
            {
              title: 'polManagement.table.salaryPercent',
              field: 'salaryPercent',
              thClassList: ['text-center'],
              tdClassList: ['text-right'],
              fixed: window.innerWidth > 1024,
              show: true
            },
            // {
            //   title: 'polManagement.table.salaryReal',
            //   field: 'salaryReal',
            //   tdClassList: ['text-right'],
            //   fixed: window.innerWidth > 1024,
            //   show: false
            // },

          ]
        },
        {
          title: 'THS',
          colspan: 4,
          width:400,
          child: [
            {
              title: 'polManagement.table.performanceFactor',
              field: 'performanceFactor',
              tdClassList: ['text-right'],
              thClassList: ['text-center'],
              fixed: window.innerWidth > 1024,
              show: true
            },
            {
              title: 'polManagement.table.performanceAmount',
              field: 'performanceAmount',
              tdClassList: ['text-right'],
              thClassList: ['text-center'],
              fixed: window.innerWidth > 1024,
              show: true
            },
            {
              title: 'polManagement.table.performancePercent',
              field: 'performancePercent',
              tdClassList: ['text-right'],
              thClassList: ['text-center'],
              fixed: window.innerWidth > 1024,
              show: true
            },
            {
              title: 'polManagement.table.periodCurFactor',
              field: 'periodCurFactor',
              fixed: window.innerWidth > 1024,
              tdClassList: ['text-right'],
              thClassList: ['text-center'],
              show: true
            },
            // {
            //   title: 'polManagement.table.performanceReal',
            //   field: 'performanceReal',
            //   fixed: window.innerWidth > 1024,
            //   tdClassList: ['text-right'],
            //   show: false
            // },
          ]
        },
        {
          title: 'polManagement.table.packAmount',
          field: 'packAmount',
          rowspan: 2,
          tdClassList: ['text-right'],
          thClassList: ['text-center'],
          width: 100,
          fixed: window.innerWidth > 1024,
          show: true
        },
        {
          title: 'polManagement.table.periodCurPoint',
          field: 'periodCurPoint',
          rowspan: 2,
          width: 100,
          fixed: window.innerWidth > 1024,
          thClassList: ['text-center'],
          tdClassList: ['text-right'],
          show: true
        },
        {
          title: 'polManagement.table.salaryType',
          field: 'salaryType',
          rowspan: 2,
          width: 100,
          fixed: window.innerWidth > 1024,
          thClassList: ['text-center'],
          show: true
        },
        {
          title: 'polManagement.table.salaryVersionName',
          field: 'salaryVersionName',
          rowspan: 2,
          width: 120,
          fixed: window.innerWidth > 1024,
          thClassList: ['text-center'],
          show: true
        },
        {
          title: 'polManagement.table.employeeTypeName',
          field: 'employeeTypeName',
          fixed: window.innerWidth > 1024,
          thClassList: ['text-center'],
          width: 150,
          rowspan: 2,
          show: true
        },
        {
          title: 'polManagement.table.curCode',
          field: 'curCode',
          thClassList: ['text-center'],
          width:50,
          rowspan: 2,
          show: false
        },
        {
          title: 'polManagement.table.createdBy',
          field: 'createdBy',
          thClassList: ['text-center'],
          rowspan: 2,
          width: 150,
          fixed: window.innerWidth > 1024,
          show: true
        },
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageIndex: 1,
      pageSize: 10,
      showFrontPagination: false,
      showSelect: true
    };
  }

  search():void {
    this.loading = true;
    this.setParams();
    this.salaryProcessService.getListApprove(this.params, this.backdate).subscribe(data => {
      if(data.data){
        this.dataTable = data.data;
        this.dataTable.forEach(sl => {
          sl.id = sl.salaryProcessId;
          const type = Number(sl.employeeType);
          this.tableConfig.total = data.data.totalElements;
          if(type){
            sl.employeeTypeName = EMPLOYEE_TYPE[type - 1];
            sl.curCode = sl.curCode === 'VND'? 'VNĐ': sl.curCode;
            sl.fromDate = this.mapPipe.transform(sl.fromDate,'date');
            sl.toDate = this.mapPipe.transform(sl.toDate,'date');
            sl.salaryAmount = this.mapPipe.transform(sl.salaryAmount,'currencyNumber');
            sl.salaryReal = this.mapPipe.transform(sl.salaryReal,'currencyNumber');
            sl.packAmount = this.mapPipe.transform(sl.packAmount,'currencyNumber');
            sl.performanceAmount = this.mapPipe.transform(sl.performanceAmount,'currencyNumber');
            sl.performanceReal=this.mapPipe.transform(sl.performanceReal,'currencyNumber');          }
        })
      }
      this.loading = false;
    }, error => {
      this.loading = false;
      this.toastrCustom.error(error.message);

    })
  }

  setParams():void {
    this.params.employeeCode = this.objectSearch.employeeCode ? this.objectSearch.employeeCode : '';
    this.params.orgId = this.objectSearch.orgId ? this.objectSearch.orgId : '';
    this.params.createBy = this.objectSearch.createBy ? this.objectSearch.createBy.trim() : '';
  }

  approveSalaryProcess(isApprove:boolean):void {
    if(this.setOfCheckedId.size === 0){
      this.toastrCustom.error(this.translate.instant("polManagement.message.plSelectRecord"));
      return;
    }
    this.submit = true;
    this.isApprove = isApprove;
    if(!isApprove && !this.description){
      return;
    }
    const action = isApprove ? "PD" : "TC";
    const request = this.buildApproveRequest(action);
    this.loading = true;
    this.salaryProcessService.approveSalaryProcess(request, this.backdate).subscribe(data => {
      this.toastrCustom.success();
      this.setOfCheckedId.clear();
      this.search();
      this.checked = [];
      this.description = "";
      this.submit = false;
      this.loading = false;
    }, error => {
      this.loading = false;
      this.toastrCustom.error(error.message);
    });
  }

  approveAllSalaryProcess():void {
    if(!this.dataTable?.length){
      this.toastrCustom.error(this.translate.instant('polManagement.message.recordApproveNotFound'));
      return;
    }
    const approve = () => {
      this.loading = true;
      const request = this.buildApproveAllRequest();
      this.salaryProcessService.approveSalaryProcessAll(request, this.backdate).subscribe(data => {
        this.toastrCustom.success();
        this.setOfCheckedId.clear();
        this.description = "";
        this.clearOrgInput();
        this.emp = null;
        this.objectSearch.employeeCode = '';
        this.fullName = '';
        this.checked = [];
        this.search();
        this.submit = false;
        this.loading = false;
      }, error => {
        this.loading = false;
        this.toastrCustom.error(error.message);
      });
    }
    this.deletePopup.showApprove(approve);
  }

  buildApproveRequest(action: 'PD' | 'TC'):ApproveResquest {

    const request:ApproveResquest = {
      action: action,
      description: this.description,
      ids: []
    }

    this.setOfCheckedId.forEach((value:number) => {
      request.ids.push(String(value));
    });
    return request;
  }

  buildApproveAllRequest():ApproveAllRequest {
    this.setParams();
    const request:ApproveAllRequest = {
      ...this.params,
      description: this.description,
      isBackdate: this.backdate ? true : false
    }
    return request;
  }

  onChange(event: SalaryParams):void{
    if(event){
      this.objectSearch.employeeCode = event.employeeCode;
      this.fullName = event.fullName;
      this.search();
    } else {
      this.objectSearch.employeeCode = '';
      this.fullName = '';
    }
  }

  showPopup():void {
    const modal = this.modal.create({
      nzWidth: '80%',
      nzTitle: ' ',
      nzContent: FormModalShowTreeUnitComponent,
      nzFooter: null,
    });

    modal.afterClose.subscribe((result) => {
      if (result?.orgName) {
        this.orgName = result?.orgName;
        this.objectSearch.orgId = result?.orgId;
      }
    });
  }

  clearOrgInput():void {
    this.orgName = "";
    this.objectSearch.orgId = "";
  }

  onClickItem(employee: SalaryModel):void {
    if(!this.objFunction.approve){
      return;
    }
    this.router.navigate(['/policy-management/salary-process/detail-approve'], { state: { data: employee } });
    localStorage.setItem("employeeInfo", JSON.stringify(employee));
    localStorage.setItem("salaryBackdate", JSON.stringify(this.backdate ? true : false));
    localStorage.setItem("objFunction",JSON.stringify(this.objFunction));
  }

  override triggerSearchEvent() {
    this.search();
  }

}
