import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SalaryProcessApproveComponent} from "./salary-process-approve/salary-process-approve.component";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {NzGridModule} from "ng-zorro-antd/grid";
import {TranslateModule} from "@ngx-translate/core";
import {FormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {NzTagModule} from "ng-zorro-antd/tag";
import {SharedUiFormModalShowTreeUnitModule} from "@hcm-mfe/shared/ui/form-modal-show-tree-unit";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/mb-employee-data-picker";
import {NzIconModule} from "ng-zorro-antd/icon";
import { SharedUiMbTableMergeCellWrapModule } from '@hcm-mfe/shared/ui/mb-table-merge-cell-wrap';
import { SharedUiMbTableMergeCellModule } from '@hcm-mfe/shared/ui/mb-table-merge-cell';
import { MapPipe } from '@hcm-mfe/shared/pipes/map';

@NgModule({
  imports: [CommonModule, TranslateModule, NzTagModule, SharedUiMbTableWrapModule, SharedUiMbButtonModule, NzIconModule,
    SharedUiMbTableModule, SharedUiMbInputTextModule, SharedUiMbEmployeeDataPickerModule,
    NzGridModule, FormsModule, SharedUiFormModalShowTreeUnitModule,
    RouterModule.forChild([
      {
        path: '',
        component: SalaryProcessApproveComponent
      }
    ]), SharedUiLoadingModule, SharedUiMbTableMergeCellWrapModule, SharedUiMbTableMergeCellModule],
  declarations: [SalaryProcessApproveComponent],
  exports: [SalaryProcessApproveComponent],
  providers: [MapPipe]
})
export class PolicyManagementFeatureSalaryProcessApproveModule {}
