import { Component, Injector, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import {
  ApproveAllReq, ApproveReq, BONUS, SalaryCommitmentModel, SalaryCommitmentSearch,
  SalaryModel, SalaryParams
} from '@hcm-mfe/policy-management/data-access/models';
import {MBTableConfig} from "@hcm-mfe/shared/data-access/models";
import { SalaryCommitmentService } from '@hcm-mfe/policy-management/data-access/service';
import { FormModalShowTreeUnitComponent } from '@hcm-mfe/shared/ui/form-modal-show-tree-unit';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { FunctionCode } from '@hcm-mfe/shared/common/enums';

@Component({
  selector: 'app-salary-commit-approve',
  templateUrl: './salary-commit-approve.component.html',
  styleUrls: ['./salary-commit-approve.component.scss']
})
export class SalaryCommitApproveComponent extends BaseComponent implements OnInit {

  @ViewChild('flagStatusTmpl', {static: true}) flagStatus!: TemplateRef<NzSafeAny>;
  objectSearch:SalaryCommitmentSearch;
  params:SalaryCommitmentSearch;
  tableConfig:MBTableConfig;
  dataTable:SalaryCommitmentModel[];
  checked:NzSafeAny[];
  description:string;
  submit:boolean;
  orgName:string;
  fullName:string;
  loading:boolean;
  isApprove:boolean = false;
  heightTable = { x: '100vw', y: '40vh'};
  isBackdate: boolean= false;
  functionCode: string;

  constructor(injector:Injector ,readonly salaryCommitService:SalaryCommitmentService) {
    super(injector);
    this.loading = false;
    this.objectSearch = new SalaryCommitmentSearch();
    this.params = {...this.objectSearch};
    this.fullName = '';
    this.orgName = '';
    this.description = '';
    this.submit = false;

    this.tableConfig = new MBTableConfig();
    this.checked = [];
    this.dataTable = [];
    this.isBackdate = this.route.snapshot.data['mode'] === "backdate";

    if(this.isBackdate){
      this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.POL_SALARY_COMMITMENT_APPROVE_B}`);
      this.functionCode = FunctionCode.POL_SALARY_COMMITMENT_APPROVE_B;
    } else {
      this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.POL_SALARY_COMMITMENT_APPROVE}`);
      this.functionCode = FunctionCode.POL_SALARY_COMMITMENT_APPROVE;
    }
    console.log(this.objFunction);
  }

  ngOnInit(): void {
    this.search();
    this.initTable();
  }

  private initTable(): void {

    this.tableConfig = {
      headers: [
        {
          title: 'polManagement.table.employeeCode',
          field: 'employeeCode',
          width: 150,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left',
        },
        {
          title: 'polManagement.table.fullName',
          field: 'fullName',
          width: 150,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left',
        },
        {
          title: 'polManagement.table.org',
          field: 'lastName',
          needEllipsis:true,
          width: 300,
          fixedDir: 'left',
        },
        {
          title: 'polManagement.table.jobName',
          field: 'jobName',
          width: 250,
        },
        {
          title: 'polManagement.table.fromDate',
          field: 'fromDate',
          pipe:'date',
          width: 150,
        },
        {
          title: 'polManagement.table.toDate',
          field: 'toDate',
          pipe:'date',
          width: 150,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'polManagement.table.amountCommit',
          field: 'amount',
          pipe: 'currencyNumber',
          tdClassList: ['text-right'],
          width: 150
        },
        {
          title: 'polManagement.table.curCode',
          field: 'curCode',
          width: 60,
        },
        {
          title: 'polManagement.table.isBonus',
          field: 'bonusName',
          width: 150
        },
        {
          title: 'polManagement.table.createdBy',
          field: 'createdBy',
          width: 150
        },
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: 10,
      pageIndex: 1,
      showCheckbox: true,
      showFrontPagination: false
    };
  }

  search():void {
    this.loading = true;
    this.setParams();
    this.salaryCommitService.getListApprove(this.params,this.isBackdate).subscribe(data => {
      if(data.data){
        this.dataTable = data.data;
        this.dataTable?.forEach(item => {
          item.bonusName = item.isBonus === 'Y' ? BONUS.Y : BONUS.N;
          item.curCode = item.curCode === 'VND'?'VNĐ':item.curCode;
        })
      }
      this.loading = false;
    }, error => {
      this.loading = false;
      this.toastrCustom.error(error.message);
    })
  }

  setParams():void {
    this.params.employeeCode = this.objectSearch.employeeCode ? this.objectSearch.employeeCode : '';
    this.params.orgId = this.objectSearch.orgId ? this.objectSearch.orgId : '';
    this.params.createBy = this.objectSearch.createBy ? this.objectSearch.createBy.trim() : '';
    this.params.flagStatus = 2;
    this.params.page = 0;
    this.params.size = 1000;
  }

  approveSalaryCommit(isApprove:boolean):void {
    this.isApprove = isApprove;
    if(this.checked.length === 0){
      this.toastrCustom.error(this.translate.instant("polManagement.message.plSelectRecord"));
      return;
    }
    this.submit = true;
    if(!isApprove && !this.description){
      return;
    }

    const action = isApprove ? "APPROVE" : "REJECT";
    const request = this.buildApproveRequest(action);
    this.loading = true;
    this.salaryCommitService.approveData(request,this.isBackdate).subscribe(data => {
      this.submit = false;
      this.toastrCustom.success();
      this.search();
      this.checked = [];
      this.description = "";
      this.loading = false;
    }, error => {
      this.loading = false;
      this.toastrCustom.error(error.message);
    });
  }

  approveAllSalaryCommit():void {
    this.loading = true;
    this.isApprove = true;
    const request = this.buildApproveAllRequest();
    this.salaryCommitService.approveAll(request, this.isBackdate).subscribe(data => {
      this.submit = false;
      this.toastrCustom.success();
      this.description = "";
      this.search();
      this.loading = false;
    }, error => {
      this.loading = false;
      this.toastrCustom.error(error.message);
    });

  }

  buildApproveRequest(action: 'APPROVE' | 'REJECT'):ApproveReq {

    const request:ApproveReq = {
      action: action,
      description: this.description,
      ids: []
    }

    this.checked?.forEach((item:SalaryCommitmentModel) => {
      request.ids.push(item.hrSalaryCommitmentId);
    });

    return request;
  }

  buildApproveAllRequest():ApproveAllReq {
    this.setParams();
    const request: ApproveAllReq = {
      ...this.params,
      description: this.description
    }

    return request;
  }

  onChange(event: SalaryParams){
    if(event){
      this.objectSearch.employeeCode = event.employeeCode;
      this.fullName = event.fullName;
      this.search();
    } else {
      this.objectSearch.employeeCode = '';
      this.fullName = '';
    }
  }

  showPopup() {
    const modal = this.modal.create({
      nzWidth: '80%',
      nzTitle: ' ',
      nzContent: FormModalShowTreeUnitComponent,
      nzFooter: null,
    });

    modal.afterClose.subscribe((result) => {
      if (result?.orgName) {
        this.orgName = result?.orgName;
        this.objectSearch.orgId = result?.orgId;
      }
    });
  }

  clearOrgInput():void {
    this.orgName = "";
    this.objectSearch.orgId = "";
  }

  onClickItem(commit:SalaryCommitmentModel ) {
    localStorage.setItem('idCommitment', String(commit.hrSalaryCommitmentId));
    this.router.navigate(['/policy-management/salary-process/detail-commit-approve']);
  }
  override triggerSearchEvent(): void {
    this.search();
  }
}
