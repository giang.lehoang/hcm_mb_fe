import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { SharedUiMbTableWrapModule } from '@hcm-mfe/shared/ui/mb-table-wrap';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { SharedUiMbTableModule } from '@hcm-mfe/shared/ui/mb-table';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { SharedUiMbEmployeeDataPickerModule } from '@hcm-mfe/shared/ui/mb-employee-data-picker';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { FormsModule } from '@angular/forms';
import { SharedUiFormModalShowTreeUnitModule } from '@hcm-mfe/shared/ui/form-modal-show-tree-unit';
import { RouterModule } from '@angular/router';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { SalaryCommitApproveComponent } from './salary-commit-approve/salary-commit-approve.component';

@NgModule({
  imports: [CommonModule, TranslateModule, NzTagModule, SharedUiMbTableWrapModule, SharedUiMbButtonModule, NzIconModule,
    SharedUiMbTableModule, SharedUiMbInputTextModule, SharedUiMbEmployeeDataPickerModule,
    NzGridModule, FormsModule, SharedUiFormModalShowTreeUnitModule,
    RouterModule.forChild([
      {
        path: '',
        component: SalaryCommitApproveComponent
      }
    ]), SharedUiLoadingModule,],
  declarations: [SalaryCommitApproveComponent],
  exports: [SalaryCommitApproveComponent]
})
export class PolicyManagementFeatureSalaryCommitApproveModule {}
