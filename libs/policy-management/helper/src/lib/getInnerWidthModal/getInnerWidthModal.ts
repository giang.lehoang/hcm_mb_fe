export const getInnerWidth = (point: number): number => {
  if (window.innerWidth > 767) {
    return window.innerWidth / point > 1100 ? 1100 : window.innerWidth / point;
  }
  return window.innerWidth;
}
