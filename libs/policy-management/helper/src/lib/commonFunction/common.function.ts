import {MapPipe} from "@hcm-mfe/shared/pipes/map";

export const renderFlagStatus = (status: number) => {
  switch (status) {
    case 2:
      return {
        text: 'Chờ duyệt',
        class: 'orange',
      };
    case 3:
      return {
        text: 'Từ chối',
        class: 'red',
      };

    case 4:
      return {
        text: 'Phê duyệt',
        class: 'green',
      };
    default:
  }
  return {
    text: 'Đang hoạt động',
    class: 'green',
  };
}
export const renderFlagStatusAllowance = (status: string | null) => {
  switch (status) {
    case 'Y':
      return {
        text: 'Phê duyệt',
        class: 'green',
      };
    case 'N':
      return {
        text: 'Từ chối',
        class: 'red',
      };

    case null:
      return {
        text: 'Chờ duyệt',
        class: 'orange',
      };
    default:
  }
  return {
    text: ''
  };
}
export const renderStatusEmployee = (status: number | string): string => {
  switch (status) {
    case 1:
      return "Hiện diện";
    case 2:
      return "Tạm hoãn";
    case 3:
      return "Đã nghỉ việc";
    default:
      return "";
  }
}
export const checkLastM = (dateCheck:Date | null):boolean => {
  if(!dateCheck){
    return false;
  }
  const now = new Date();
  if(dateCheck.getFullYear() > now.getFullYear()){
    return false;
  }

  if(dateCheck.getFullYear() === now.getFullYear()){
    if(dateCheck.getMonth() < now.getMonth() - 1){
      return true;
    }

    if(now.getDate() < 26) {
      if((dateCheck.getMonth() === now.getMonth() - 1) && dateCheck.getDate() < 26) {
        return true;
      }
    } else {
      if((dateCheck.getMonth() === now.getMonth() - 1)) {
        return true;
      }

      if((dateCheck.getMonth() === now.getMonth()) && dateCheck.getDate() < 26) {
        return true;
      }
    }
    return false;
  }

  if(dateCheck.getFullYear() < now.getFullYear()){
    if(dateCheck.getFullYear() !== now.getFullYear() -1){
      return true;
    }
    if(now.getMonth() !== 0 || dateCheck.getMonth() !== 11) return true;
    if(dateCheck.getDate() < 26) return true;
    return false;
  }
  return false;
}

export const  formatToCurrency = (value:number|string,param:string, mapPipe:MapPipe) => {
  const currency = mapPipe.transform(value, param);
  return !currency || currency === '-' ? "": currency;
}

