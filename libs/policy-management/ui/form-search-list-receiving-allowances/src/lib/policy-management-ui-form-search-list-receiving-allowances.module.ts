import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormSearchListReceivingAllowancesComponent } from './form-search-list-receiving-allowances/form-search-list-receiving-allowances.component';

@NgModule({
  imports: [CommonModule],
  declarations: [
    FormSearchListReceivingAllowancesComponent
  ],
  exports: [
    FormSearchListReceivingAllowancesComponent
  ]
})
export class PolicyManagementUiFormSearchListReceivingAllowancesModule {}
