import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormSearchListReceivingAllowancesComponent } from './form-search-list-receiving-allowances.component';

describe('FormSearchListReceivingAllowancesComponent', () => {
  let component: FormSearchListReceivingAllowancesComponent;
  let fixture: ComponentFixture<FormSearchListReceivingAllowancesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormSearchListReceivingAllowancesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormSearchListReceivingAllowancesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
