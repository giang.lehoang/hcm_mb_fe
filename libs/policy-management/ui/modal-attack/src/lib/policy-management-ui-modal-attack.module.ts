import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  ModalDocumentReceivingExceptionAllowancesComponent
} from "./modal-document-receiving-exception-allowances/action-document-attached-file.component";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {NzGridModule} from "ng-zorro-antd/grid";
import {NzUploadModule} from "ng-zorro-antd/upload";
import {NzListModule} from "ng-zorro-antd/list";
import {NzAlertModule} from "ng-zorro-antd/alert";
import {TranslateModule} from "@ngx-translate/core";
import { ModalAddFormReceivingExceptionAllowancesComponent } from './modal-add-form-receiving-exception-allowances/modal-add-form-receiving-exception-allowances.component';
import {ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/mb-employee-data-picker";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {NzFormModule} from "ng-zorro-antd/form";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {SharedUiMbInputFileModule} from "@hcm-mfe/shared/ui/mb-input-file";
import {NzModalModule} from "ng-zorro-antd/modal";
import {PdfViewerModule} from "ng2-pdf-viewer";

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, NzGridModule, NzUploadModule, NzListModule, NzAlertModule, TranslateModule,
    ReactiveFormsModule, SharedUiMbEmployeeDataPickerModule, SharedUiMbInputTextModule, NzFormModule, SharedUiMbSelectModule,
    SharedUiMbDatePickerModule, SharedUiMbInputFileModule, NzModalModule, PdfViewerModule],
  exports: [
    ModalDocumentReceivingExceptionAllowancesComponent
  ],
  declarations: [
    ModalDocumentReceivingExceptionAllowancesComponent,
    ModalAddFormReceivingExceptionAllowancesComponent
  ]
})
export class PolicyManagementUiModalAttackModule {}
