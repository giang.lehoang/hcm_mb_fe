import {Component, Injector, OnInit} from '@angular/core';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {FormGroup, Validators} from "@angular/forms";
import {HTTP_STATUS_CODE, Mode, SYSTEM_FORMAT_DATA} from "@hcm-mfe/shared/common/constants";
import {debounceTime, Subscription} from "rxjs";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";
import {
  ListReceivingExceptionAllowancesService,
  SalaryEvolutionService,
  SharedService
} from "@hcm-mfe/policy-management/data-access/service";
import {IListReceivingExceptionAllowances} from "@hcm-mfe/policy-management/data-access/models";
import {DateValidator} from "@hcm-mfe/shared/common/validators";
import * as moment from "moment";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import _ from 'lodash';
import {NzModalRef} from "ng-zorro-antd/modal";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";

@Component({
  selector: 'hcm-mfe-modal-add-form-receiving-exception-allowances',
  templateUrl: './modal-add-form-receiving-exception-allowances.component.html',
  styleUrls: ['./modal-add-form-receiving-exception-allowances.component.scss']
})
export class ModalAddFormReceivingExceptionAllowancesComponent extends BaseComponent implements OnInit {

  mode = Mode.ADD;
  typeShowInfo = false;

  selectedEmployee: {
    empCode: string | undefined | null,
    fullName: string | undefined | null
  } = {
    empCode: null,
    fullName: null
  }
  employeeId = '';
  form: FormGroup;
  isSubmitted = false;
  private readonly subs: Subscription[] = [];
  dataResponseKindOfReward: Pick<IListReceivingExceptionAllowances, 'prAllowanceType' | 'prAllowanceType'>[] = [];
  startDate: Date | undefined = undefined;
  endDate: Date | undefined = undefined;
  override isLoading = false;
  prAllowanceExtendListId: number | undefined = undefined;

  //upload file
  fileName:string;
  file: File | undefined;
  docId: string;
  pdfSrc: string | undefined;
  isVisible: boolean;
  isApprove: boolean;
  hasFile: boolean | undefined;
  functionCode: string;

  constructor(
    injector: Injector,
    private modalRef: NzModalRef,
    private readonly listReceivingExceptionAllowancesService: ListReceivingExceptionAllowancesService,
    private sharedService: SharedService,
    readonly salaryEvolutionService:SalaryEvolutionService
  ) {
    super(injector);
    this.fileName = '';
    this.docId = '';
    this.isVisible = false;
    this.isApprove = false;


    this.form = this.fb.group({
      fullName: [null],
      prAllowanceExtendListId: [null],
      employeeId: [null, Validators.required],
      employeeCode: [null],
      orgName: [null],
      jobName: [null],
      levelName: [null],
      posName: [null],
      prAllowanceType: [null, Validators.required],
      fromDate: [null, Validators.required],
      toDate: [null],
      contents: [null],
      note: [null],
      flagStatus: [null],
      approver: [null],
      approveDate: [null],
      lastUpdatedBy: [null],
      lastUpdateDate: [null],
      file: [null],
      fileName: [null],
    }, {
      validators: [
        DateValidator.validateSpecificRangeDate('fromDate', 'toDate', 'rangeDateError1', true),
      ]

    });

    this.objFunction = this.sessionService.getSessionData(
      `FUNCTION_${FunctionCode.RECEIVING_EXCEPTION_ALLOWANCE}`
    );
    this.functionCode = FunctionCode.RECEIVING_EXCEPTION_ALLOWANCE;
  }

  get f() {
    return this.form.controls;
  }

  ngOnInit(): void {
    // get before month and date 26
    const currentDay = new Date();
    this.startDate = moment([currentDay.getFullYear(), currentDay.getMonth() - 1, 26]).toDate();
    this.endDate = moment([currentDay.getFullYear(), currentDay.getMonth(), 25]).toDate();

    //

    this.selectedEmployee = {
      fullName: this.sharedService.userToUpdateEmployee?.fullName,
      empCode: this.sharedService.userToUpdateEmployee?.empCode
    }

    const getKindOfReward = this.listReceivingExceptionAllowancesService.getKindOfReward()
      .subscribe((res: BaseResponse) => {
        if (res.data) {

          // remove ResponseKindOfReward with type getAll is 00
          this.dataResponseKindOfReward = res.data
            .filter((el: Pick<IListReceivingExceptionAllowances, 'prAllowanceType' | 'prAllowanceType'>) => {
              return el.prAllowanceType !== '00';
            });
        }
      }, (err) => console.log(err));

    if (this.mode === Mode.EDIT) {
      this.getDetailReceivingExeptionAllowances();
    }else if(this.mode === Mode.ADD){
      if(this.sharedService.userToUpdateEmployee?.empCode){
        this.form.patchValue({
          employeeId: this.sharedService.userToUpdateEmployee,
          orgName: this.sharedService.userToUpdateEmployee?.orgPathName,
          levelName: this.sharedService.userToUpdateEmployee?.levelName,
          posName: this.sharedService.userToUpdateEmployee?.posName,
          jobName: this.sharedService.userToUpdateEmployee?.jobName,
        });

        // this.onChanges();
      }else{
        this.form.patchValue({
          employeeId: null
        });
        this.onChanges();
      }
    }

    this.subs.push(getKindOfReward);
  }

  getDetailReceivingExeptionAllowances(){
    const detailTargetAssesmentSub = this.listReceivingExceptionAllowancesService.
    getDetailReceivingExceptionAllowances(this.prAllowanceExtendListId).subscribe(
      ((res: BaseResponse) => {
        if (res.status === HTTP_STATUS_CODE.OK) {
          this.form.patchValue(res.data);
          this.isLoading = false;
          this.form.patchValue({
            fullName: `${res?.data?.fullName}-${res?.data?.employeeCode}`,
          })
          this.fileName = res.data?.fileName;
          this.docId = res.data?.docId;
          if(this.fileName) this.hasFile = true;
        }
      }), (err) => {
        this.toastrCustom.error(`${this.translate.instant('common.notification.error')} : ${err.message}`);
        this.isLoading = false;
        this.modalRef.close();
      }
    )
    this.subs.push(detailTargetAssesmentSub);
  }
  onChange(value:any){
    this.employeeId = value.employeeId ? value.employeeId : '';
  }
  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      const formData = _.cloneDeep(this.form.value);
      formData.fromDate = formData.fromDate ? moment(formData.fromDate).format(SYSTEM_FORMAT_DATA.DATE_FORMAT_SCORD) : null;
      formData.toDate = formData.toDate ? moment(formData.toDate).format(SYSTEM_FORMAT_DATA.DATE_FORMAT_SCORD) : null;

      // save file pdf
      const request: Partial<IListReceivingExceptionAllowances> = this.mode === Mode.ADD ? {
        employeeId: this.employeeId ? this.employeeId : (formData.employeeId.employeeId ? formData.employeeId.employeeId : formData.employeeId.empId),
        prAllowanceType: formData?.prAllowanceType,
        fromDate: formData?.fromDate,
        toDate: formData?.toDate,
        note: formData?.note,
      } : {
        prAllowanceExtendListId: formData?.prAllowanceExtendListId,
        prAllowanceType: formData?.prAllowanceType,
        employeeId: this.employeeId ? this.employeeId : (formData.employeeId.employeeId ? formData.employeeId.employeeId : formData.employeeId.empId),
        fromDate: formData?.fromDate,
        toDate: formData?.toDate,
        note: formData?.note,

      };

      if(this.mode === Mode.EDIT){
        if(!this.fileName && this.hasFile){
          request.deleteFile = true;
        } else {
          request.deleteFile = false;
        }
      }
      const formDataParamAttachFile = new FormData();
      formDataParamAttachFile.append("request", new Blob([JSON.stringify(request)], {
        type: 'application/json',
      }));


      if(formData.file){
        formDataParamAttachFile.append('file', formData.file);
      }

      const updateItemExceptionAllowances = this.listReceivingExceptionAllowancesService.postOrPutReceivingExceptionAllowances(
        formDataParamAttachFile
        , this.mode === Mode.ADD ? Mode.ADD : Mode.EDIT
      )
        .subscribe((res: BaseResponse) => {
          if (res.data) {
            this.toastrCustom.success(this.translate.instant(
              this.mode === Mode.EDIT ? 'common.notification.updateSuccess' : 'common.notification.addSuccess')
            );
            this.modalRef.close({refresh: true});
          }
          }, (err) => this.toastrCustom.error(`${this.translate.instant('common.notification.error')} : ${err.message}`)
        );

      this.subs.push(updateItemExceptionAllowances);
    }
  }

  onChanges(): void {
    const formChanges = this.form.valueChanges.pipe(debounceTime(2000)).subscribe((values) => {
      if (values?.employeeId) {
        // this.getDetailInfoEmployee(values.employeeId.empId);
        this.form.patchValue({
          orgName: values.employeeId?.orgName,
          jobName: values.employeeId?.jobName,
          levelName: values.employeeId?.levelName,
          posName: values.employeeId?.positionName,
        }, {emitEvent: false});
      } else {
        this.form.patchValue({
          orgName: null,
          jobName: null,
          levelName: null,
          posName: null,
        }, {emitEvent: false});
      }
    });
    this.subs.push(formChanges);
  }



  disabledDateFrom = (current: Date): boolean => {
   // return current && current < this.startDate ||current && current > this.endDate;
    return current && current < this.startDate;
  };

  // download and attackFile

  downloadFile(docId:string, docName:string){
    this.salaryEvolutionService.downloadFile(docId).subscribe((data) => {
      const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.pdf' });
      this.file = new File([blob], docName);
      this.onFileSelected();
    });
  }

  viewFile():void {
   this.fileName && !this.file ? this.downloadFile(this.docId, this.fileName) : this.onFileSelected();
  }

  onFileSelected():void {
    const reader = new FileReader();
    reader.onload = (e: any) => {
      this.pdfSrc = e.target.result;
    };
    if(this.file) reader.readAsArrayBuffer(this.file);
    this.isVisible = true;
  }

  selectFile(file:{file:File, fileName:string}){
    if(file){
      this.form.patchValue({
        file: file.file
      });
      this.file = file.file;
      this.fileName = file.fileName;
    } else {
      this.form.patchValue({
        file: undefined
      });
      this.fileName = '';
    }
  }

  handleCancel():void {
    this.isVisible = false;
  }

  // end download and attackFile

  override ngOnDestroy() {
    super.ngOnDestroy();
    this.subs.forEach((s) => s.unsubscribe());
  }
}
