import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAddFormReceivingExceptionAllowancesComponent } from './modal-add-form-receiving-exception-allowances.component';

describe('ModalAddFormReceivingExceptionAllowancesComponent', () => {
  let component: ModalAddFormReceivingExceptionAllowancesComponent;
  let fixture: ComponentFixture<ModalAddFormReceivingExceptionAllowancesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalAddFormReceivingExceptionAllowancesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAddFormReceivingExceptionAllowancesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
