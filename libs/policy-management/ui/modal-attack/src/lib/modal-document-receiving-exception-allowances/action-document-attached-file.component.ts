import {Component, Injector, Input} from '@angular/core';
import { FormBuilder } from "@angular/forms";
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { ToastrService } from 'ngx-toastr';
import {Observable, Subscription} from 'rxjs';
import {Mode} from "@hcm-mfe/shared/common/constants";
import {TargetAssessmentService} from "@hcm-mfe/goal-management/data-access/services";
import * as FileSaver from 'file-saver';
import {NzUploadFile} from "ng-zorro-antd/upload";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";

@Component({
  selector: 'hcm-mfe-modal-document-receiving-exception-allowances',
  templateUrl: './action-document-attached-file.component.html',
  styleUrls: ['./action-document-attached-file.component.scss'],
})
export class ModalDocumentReceivingExceptionAllowancesComponent extends BaseComponent{
  mode = Mode.ADD;
  endPoint: object | undefined;
  microService: string | undefined;
  @Input() url: string | undefined;
  @Input() fileName: string | undefined;
  error: string | undefined;
  isSubmitted = false;
  fileList: NzUploadFile[] = [];
  private readonly subs: Subscription[] = [];
  employeeId = 1;
  isLoadingFile = false;
  constructor(
    private readonly modalRef: NzModalRef,
    injector: Injector,
    private readonly targetAssessmentService: TargetAssessmentService,
  ) {
  super(injector);
  }

  dataResponseUploadFile: string[] = [];

  save() {
    if (!this.fileList.length) {
      this.toastrCustom.error(this.translate.instant('common.notification.emptyFile'));
      return;
    }
    this.isLoadingFile = true;
    this.isSubmitted = true;
    const formData = new FormData();
    this.fileList.forEach((nzFile: NzUploadFile) => {
      formData.append('file', nzFile as any)
    });

    // const uplioadFileExel = this.targetAssessmentService.getUrlUploadFile(formData, this.url).subscribe(res => {
    //   if (res && res.status === 200) {
    //     this.dataResponseUploadFile = res.data;
    //     if(!this.dataResponseUploadFile[0]){
    //       this.toastrCustom.success(this.translate.instant('common.notification.updateSuccess'));
    //       this.modalRef.destroy({ refresh: true });
    //       this.isLoadingFile = false;
    //     } else {
    //       this.isLoadingFile = false;
    //       this.toastrCustom.error(this.translate.instant('polManagement.common.notification.uploadFileError'))
    //     }
    //   }
    // }, error => {
    //   this.isLoadingFile = false;
    //   this.toastrCustom.error(this.translate.instant('common.notification.updateError'));
    // });
    //
    // this.subs.push(uplioadFileExel);
  }

  dowloadFileSample(){
    const dowLoadFileSample = this.targetAssessmentService.getDowloadFileExel(this.url, this.endPoint, this.microService).subscribe(data => {
        this.toastrCustom.success(this.translate.instant('polManagement.common.notification.dowloadFile'));
        const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
        FileSaver.saveAs(blob, `${this.fileName}.xlsx`);
    }, error => {
      this.toastrCustom.error(this.translate.instant('polManagement.common.notification.dowloadFileError'));
    });
    this.subs.push(dowLoadFileSample);
  }

  beforeUpload = (file: NzUploadFile): boolean | Observable<boolean> => {
    if (file.type !== 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' && file.type !== 'application/vnd.ms-excel') {
      this.toastrCustom.error(this.translate.instant('polManagement.common.notification.invalidFileType'));
      return false;
    }

    if (file.size && file.size >= 3000000) {
      this.toastrCustom.error(this.translate.instant('polManagement.common.notification.MaximumfileUpload'));
      return false;
    }

    this.fileList = this.fileList.concat(file);
    this.fileList = [this.fileList[this.fileList.length -1]];
    this.dataResponseUploadFile = [];
    return false;
  };

  override ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subs.forEach((s) => s.unsubscribe());
  }

}
