import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'pol-checkbox-select-input',
  templateUrl: './checkbox-select-input.component.html',
  styleUrls: ['./checkbox-select-input.component.scss']
})
export class CheckboxSelectInputComponent implements OnInit {

  data:string = '';
  disable:boolean = false;
  fontSize = '14px'
  isVisible:boolean;
  @Input('listComment') orgListFilter: { key:number|string,  value:number|string}[] = [];
  @Input('listCommentSelect') listSelect:{ key:number|string,  value:number|string}[] = [];
  @Input() error:boolean;
  setSelect:Set<number> = new Set();
  selectAll:boolean;
  height = '32px';

  constructor() {
    this.isVisible = false;
    this.selectAll = false;
    this.error = false;
  }

  ngOnInit(): void {
  }


  onSelect(item: any) {
    if(this.setSelect.has(item)){
      this.setSelect.delete(item);
    } else {
      this.setSelect.add(item);
    }
  }

  change($event: any) {
    let index = 0;
    this.orgListFilter.forEach(item => {
      if($event){
        this.setSelect.add(index);
      } else {
        this.selectAll = false;
        this.setSelect.delete(index);
      }
      index++;
    });
  }

  done() {
    this.unSelectAll();
    this.setSelect.forEach(value => {
      this.listSelect.push(this.orgListFilter[value]);
    });
    this.isVisible = false;
  }

  unSelectAll() {
    while (this.listSelect.length){
      this.listSelect.splice(0, 1);
    }
  }

  removeItem(i: number, event: MouseEvent) {
    event.stopPropagation();
    this.listSelect.splice(i, 1);
    this.setSelect.delete(i);
  }
}
