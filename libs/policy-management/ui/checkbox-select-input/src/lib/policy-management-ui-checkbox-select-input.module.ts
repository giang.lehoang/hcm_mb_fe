import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CheckboxSelectInputComponent} from "./checkbox-select-input/checkbox-select-input.component";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {TranslateModule} from "@ngx-translate/core";
import {NzSwitchModule} from "ng-zorro-antd/switch";
import {FormsModule} from "@angular/forms";
import {NzEmptyModule} from "ng-zorro-antd/empty";
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";
import {NzTagModule} from "ng-zorro-antd/tag";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzButtonModule} from "ng-zorro-antd/button";

@NgModule({
  declarations: [CheckboxSelectInputComponent],
  exports: [CheckboxSelectInputComponent],
  imports: [CommonModule, SharedUiMbInputTextModule, NzDropDownModule, TranslateModule, NzSwitchModule, FormsModule, NzCheckboxModule
    , NzEmptyModule, NzTagModule, NzIconModule, NzButtonModule],
})
export class PolicyManagementUiCheckboxSelectInputModule {}
