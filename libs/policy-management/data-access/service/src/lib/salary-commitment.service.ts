import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {Injectable} from "@angular/core";
import {
  ApproveAllReq,ApproveReq,
  SalaryCommitmentSearch,
  CommitmentRequest, CommitmentResponse,
  ResponseEntity, ResponseEntityData, SalaryCommitmentModel,
} from '@hcm-mfe/policy-management/data-access/models';
import {Observable} from "rxjs";
import {MICRO_SERVICE, SERVICE_NAME} from "@hcm-mfe/shared/common/constants";
import {UrlConstant} from "@hcm-mfe/policy-management/data-access/service";

@Injectable({
  providedIn: 'root',
})
export class SalaryCommitmentService extends BaseService {

  getListCommitment(params:SalaryCommitmentSearch):Observable<any>{
    return this.get(SERVICE_NAME.POLICY_MANAGEMENT + UrlConstant.END_POINT_SALARY_COMMITMENT.listSalaryCommitment,
      { params: params }, MICRO_SERVICE.POL);
  }

  getListApprove(params:SalaryCommitmentSearch,isBackdate: boolean):Observable<any>{
    const urlEndpoint =  isBackdate ? UrlConstant.END_POINT_SALARY_COMMITMENT.listSalaryApproveBackdate: UrlConstant.END_POINT_SALARY_COMMITMENT.listSalaryApprove;
    return this.get(SERVICE_NAME.POLICY_MANAGEMENT + urlEndpoint,
      { params: params }, MICRO_SERVICE.POL);
  }

  deleteCommitment(id:number){
    return this.delete(SERVICE_NAME.POLICY_MANAGEMENT + UrlConstant.END_POINT_SALARY_COMMITMENT.deleteSalaryCommitment + id,
      {}, MICRO_SERVICE.POL);
  }

  approveData(body:ApproveReq,isBackdate: boolean){
    const urlEndpoint =  isBackdate?UrlConstant.END_POINT_SALARY_COMMITMENT.approveBackdate:UrlConstant.END_POINT_SALARY_COMMITMENT.approved;
    return this.post(SERVICE_NAME.POLICY_MANAGEMENT + urlEndpoint, body, {}, MICRO_SERVICE.POL);
  }
  approveAll(body:ApproveAllReq,isBackdate:boolean){
    const urlEndpoint =  isBackdate?UrlConstant.END_POINT_SALARY_COMMITMENT.approveAllBackDate: UrlConstant.END_POINT_SALARY_COMMITMENT.approvedAll;
    return this.post(SERVICE_NAME.POLICY_MANAGEMENT +urlEndpoint, body,{},MICRO_SERVICE.POL)
  }
  initCommitment(body:FormData){
    return this.post(SERVICE_NAME.POLICY_MANAGEMENT + UrlConstant.END_POINT_SALARY_COMMITMENT.initSalaryCommitment, body,
      {}, MICRO_SERVICE.POL);
  }

  getDetailCommitment(id:number):Observable<ResponseEntity<CommitmentResponse>>{
    return this.get(SERVICE_NAME.POLICY_MANAGEMENT + UrlConstant.END_POINT_SALARY_COMMITMENT.initSalaryCommitment + `/${id}`,
      {}, MICRO_SERVICE.POL);
  }
}
