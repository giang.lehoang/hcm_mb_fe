import { Injectable } from "@angular/core";
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { MICRO_SERVICE, SERVICE_NAME } from '@hcm-mfe/shared/common/constants';
import { UrlConstant } from './constant/url.class';
import {ResponseApprove, ResponseEntity} from "@hcm-mfe/policy-management/data-access/models";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root',
})
export class AchievementBonusService extends BaseService {

  getListData(params: any) {
    return this.get(SERVICE_NAME.POLICY_MANAGEMENT + UrlConstant.END_POINT_ACHIEVEMENT_BONUS.listBonus, { params: params }, MICRO_SERVICE.POL);
  }
  getListPeriodDate() {
    return this.get(SERVICE_NAME.POLICY_MANAGEMENT + UrlConstant.END_POINT_ACHIEVEMENT_BONUS.listPeriodDate, { }, MICRO_SERVICE.POL);
  }
  exportExcel(params:any){
    return this.get(SERVICE_NAME.POLICY_MANAGEMENT + UrlConstant.END_POINT_ACHIEVEMENT_BONUS.exportExcel, {responseType: 'arraybuffer',params:params }, MICRO_SERVICE.POL);
  }
  waitingApprove(body:any){
    return this.post(SERVICE_NAME.POLICY_MANAGEMENT + UrlConstant.END_POINT_ACHIEVEMENT_BONUS.waitingApprove,body, { }, MICRO_SERVICE.POL);
  }
  getDownloadUrl() {
    return `${SERVICE_NAME.POLICY_MANAGEMENT + UrlConstant.END_POINT_ACHIEVEMENT_BONUS.downloadTemp}`;
  }
  getRating(){
    const params = {
      claType:'CN'
    }
    return this.get(UrlConstant.END_POINT_ACHIEVEMENT_BONUS.ratting, { params: params }, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  getListApprove(params: any):Observable<ResponseEntity<ResponseApprove>> {
    return this.get(SERVICE_NAME.POLICY_MANAGEMENT + UrlConstant.END_POINT_ACHIEVEMENT_BONUS.listApprove, { params: params }, MICRO_SERVICE.POL);
  }

  abApprove(body:any){
    return this.post(SERVICE_NAME.POLICY_MANAGEMENT + UrlConstant.END_POINT_ACHIEVEMENT_BONUS.abApprove,body, { }, MICRO_SERVICE.POL);
  }

  getLookupCode(lookupCode:string){
    const params = {
      lookupCode: lookupCode
    }
    return this.get(SERVICE_NAME.POLICY_MANAGEMENT+UrlConstant.END_POINT_ACHIEVEMENT_BONUS.lookupValue, { params: params }, MICRO_SERVICE.POL);
  }
  importExcel() {
    return `${SERVICE_NAME.POLICY_MANAGEMENT}${UrlConstant.END_POINT_ACHIEVEMENT_BONUS.importExcel}`;
  }

  abApproveAll(params:any){
    return this.post(SERVICE_NAME.POLICY_MANAGEMENT + UrlConstant.END_POINT_ACHIEVEMENT_BONUS.abApproveAll, params,{}, MICRO_SERVICE.POL);
  }

  abReject(params:any){
    return this.post(SERVICE_NAME.POLICY_MANAGEMENT + UrlConstant.END_POINT_ACHIEVEMENT_BONUS.abReject,  params, {}, MICRO_SERVICE.POL);
  }
  getRankGroup(params:any){
    return this.post(SERVICE_NAME.MODEL_PLAN + UrlConstant.END_POINT_ACHIEVEMENT_BONUS.getRankGroup,  params, {}, MICRO_SERVICE.MODEL_PLAN);
  }
  deleteBonus(params:any){
    return this.delete(SERVICE_NAME.POLICY_MANAGEMENT + UrlConstant.END_POINT_ACHIEVEMENT_BONUS.deleteBonus+'/'+`${params.type}`+'/'+`${params.objectId}`,  {},MICRO_SERVICE.POL);
  }
  getSystemCoefficient(params: any) {
    return this.get(SERVICE_NAME.POLICY_MANAGEMENT + UrlConstant.END_POINT_ACHIEVEMENT_BONUS.systemCoefficient, { params: params }, MICRO_SERVICE.POL);
  }
  sumaryAb(params:any){
    return this.post(SERVICE_NAME.POLICY_MANAGEMENT + UrlConstant.END_POINT_ACHIEVEMENT_BONUS.synthetic, params,{}, MICRO_SERVICE.POL);
  }
}
