import {Injectable} from "@angular/core";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {UrlConstant} from "@hcm-mfe/goal-management/data-access/services";
import {MICRO_SERVICE, SERVICE_NAME} from "@hcm-mfe/shared/common/constants";
import {
  IInfoAllowanceEmployee,
  IParamAllowanceApprove,
  IParamAllowanceProcess, IParamApprove
} from "@hcm-mfe/policy-management/data-access/models";

@Injectable({
  providedIn: 'root'
})
export class AllowanceDevelopmentService extends BaseService {
  public createInfoAllowance(params: IInfoAllowanceEmployee) {
    return this.post(`${SERVICE_NAME.POLICY_MANAGEMENT + '/'}${UrlConstant.END_POINT_POLICY_MANAGEMENT.urlEndPointAllowanceProcess}`, params,
      undefined, MICRO_SERVICE.POL);
  }
  public summarizeAllowance() {
    return this.post(`${SERVICE_NAME.POLICY_MANAGEMENT + '/'}${UrlConstant.END_POINT_POLICY_MANAGEMENT.urlEndPointAllowanceProcess}/summarize`, undefined,
      undefined, MICRO_SERVICE.POL);
  }
  public getAllowanceType(params?: any) {
    return this.get(`${SERVICE_NAME.POLICY_MANAGEMENT+ '/'}${UrlConstant.END_POINT_POLICY_MANAGEMENT.urlEndPointAllowanceType}`,
      {params: params}, MICRO_SERVICE.POL);
  }
  public getCurrencyType(params?: any) {
    return this.get(`${SERVICE_NAME.POLICY_MANAGEMENT+ '/'}${UrlConstant.END_POINT_POLICY_MANAGEMENT.urlEndPointLookupCode}`,
      {params: params}, MICRO_SERVICE.POL);
  }
  public getSalaryPeriod() {
    return this.get(`${SERVICE_NAME.POLICY_MANAGEMENT+ '/'}${UrlConstant.END_POINT_POLICY_MANAGEMENT.urlEndPointAllowanceProcess}/mpr-periods`,
      undefined, MICRO_SERVICE.POL);
  }
  public getListAllowanceProcess(params: IParamAllowanceProcess) {
    return this.get(`${SERVICE_NAME.POLICY_MANAGEMENT+ '/'}${UrlConstant.END_POINT_POLICY_MANAGEMENT.urlEndPointAllowanceProcess}/employee`,
      {params: params}, MICRO_SERVICE.POL);
  }
  public getListAllowanceProcessApprove(params: IParamAllowanceApprove, isBackDate: boolean) {
    return this.get(`${SERVICE_NAME.POLICY_MANAGEMENT+ '/'}${UrlConstant.END_POINT_POLICY_MANAGEMENT.urlEndPointAllowanceProcess}/approve${isBackDate ? '/back-date' : ''}`,
      {params: params}, MICRO_SERVICE.POL);
  }

  public approveAllowanceProcess(params: IParamApprove, isBackDate: boolean) {
    return this.put(`${SERVICE_NAME.POLICY_MANAGEMENT+ '/'}${UrlConstant.END_POINT_POLICY_MANAGEMENT.urlEndPointAllowanceProcess}/approve${isBackDate ? '/back-date' : ''}`,
      params, MICRO_SERVICE.POL);
  }

  public approveAllowanceProcessAll(params: IParamAllowanceApprove, isBackDate: boolean) {
    return this.put(`${SERVICE_NAME.POLICY_MANAGEMENT+ '/'}${UrlConstant.END_POINT_POLICY_MANAGEMENT.urlEndPointAllowanceProcess}/approve/all${isBackDate ? '/back-date' : ''}`,
      params, MICRO_SERVICE.POL);
  }

  public deleteAllowance(id: number) {
    return this.delete(`${SERVICE_NAME.POLICY_MANAGEMENT+ '/'}${UrlConstant.END_POINT_POLICY_MANAGEMENT.urlEndPointAllowanceProcess}/${id}`,
      undefined, MICRO_SERVICE.POL);
  }
  public updateAllowance(id:number, params: IInfoAllowanceEmployee) {
    return this.put(`${SERVICE_NAME.POLICY_MANAGEMENT+ '/'}${UrlConstant.END_POINT_POLICY_MANAGEMENT.urlEndPointAllowanceProcess}/${id}`,
      params, MICRO_SERVICE.POL);
  }
  public getDetailAllowance(id: number) {
    return this.get(`${SERVICE_NAME.POLICY_MANAGEMENT+ '/'}${UrlConstant.END_POINT_POLICY_MANAGEMENT.urlEndPointAllowanceProcess}/${id}`,
      undefined, MICRO_SERVICE.POL);
  }
  public getRegionCategoryList(lookupCode: string, flagStatus?:number){
    let params
    if(flagStatus){
      params = {
        lookupCode: lookupCode,
        flagStatus: flagStatus
      }
    } else {
      params = {
        lookupCode: lookupCode,
      }
    }
    return this.get(`${UrlConstant.END_POINT_MODEL_PLAN.urlLookupValues}`,
      {params: params}, MICRO_SERVICE.MODEL_PLAN);
  }
}
