export class UrlConstant {
  public static readonly END_POINT_POLICY_MANAGEMENT = {
    urlListReceivingExceptionAllowances: `pol/v1.0/allowance-extend-list`,
  }
  public static readonly END_POINT_SALARY_EVOLUTION = {
    listSalaryEvolution: '/v1.0/salary-process/list',
    downloadTemplate: '/v1.0/salary-process/download',
    importTemplate:'/v1.0/salary-process/import',
    deleteSalaryEvolution: '/v1.0/salary-process/delete',
    getQCL: '/v1.0/salary-version',
    getLCB: '/v1.0/matrix/salary-version',
    listApproveSalaryProcess: '/v1.0/salary-process/approval/list',
    approveSalaryProcess: '/v1.0/salary-process/approval',
    approveSalaryProcessAll: '/v1.0/salary-process/approval/all',
    getRankLCB: '/v1.0/matrix/salary-version',
    initSalaryEvoltuion:'/v1.0/salary-process/initialize',
    getDetail: '/v1.0/salary-process',
    getScoredEmpoyee: 'v1.0/share/score_employee',
    getTHSPoint: '/v1.0/performance-rule/coefficient',
    getListProcessEmp: '/v1.0/work-processes',
    getEmpInfo: '/v1.0/employees/get-detail',
    downloadFile: '/v1.0/files/pol/download',
    listApproveSalaryProcessBd: '/v1.0/salary-process/backdate/list',
    approveSalaryProcessBd: '/v1.0/salary-process/approval/backdate',
    approveSalaryProcessAllBd: '/v1.0/salary-process/approval/backdate/all',
    getListCurrency: '/v1.0/lookup-values'
  }

  public static readonly END_POINT_SALARY_COMMITMENT = {
    listSalaryCommitment: '/v1.0/salary-commitment/list',
    listSalaryApprove: '/v1.0/salary-commitment/approval/list',
    listSalaryApproveBackdate: '/v1.0/salary-commitment/backdate/list',
    approveBackdate: '/v1.0/salary-commitment/approved/backdate',
    approveAllBackDate: '/v1.0/salary-commitment/approved/backdate/all',
    deleteSalaryCommitment: '/v1.0/salary-commitment/delete/',
    approved: '/v1.0/salary-commitment/approved',
    approvedAll :'/v1.0/salary-commitment/approved/all',
    initSalaryCommitment: '/v1.0/salary-commitment'
  }
  public static readonly END_POINT_ACHIEVEMENT_BONUS = {
    listBonus: '/v1.0/achievement-bonus/list',
    listPeriodDate:'/v1.0/period/period-date',
    downloadTemp:'/v1.0/achievement-bonus/download-template',
    exportExcel:'/v1.0/achievement-bonus/list/export-excel',
    waitingApprove: '/v1.0/achievement-bonus/waiting-approval',
    ratting:'goa/v1.0/classification',
    listApprove: '/v1.0/achievement-bonus/list-pending',
    abApprove: '/v1.0/achievement-bonus/approved',
    lookupValue: '/v1.0/lookup-values',
    importExcel:'/v1.0/achievement-bonus/import-excel',
    abApproveAll: '/v1.0/achievement-bonus/approve-all',
    abReject: '/v1.0/achievement-bonus/reject',
    getRankGroup: '/v1.0/position/group/title',
    deleteBonus:'/v1.0/achievement-bonus',
    systemCoefficient:'/v1.0/achievement-matrix',
    synthetic:'/v1.0/achievement-bonus/synthetic'
  }
}
