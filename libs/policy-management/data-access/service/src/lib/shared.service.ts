import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {
  IListReceivingExceptionAllowances,
  IParamSearchListReceivingException
} from "@hcm-mfe/policy-management/data-access/models";


@Injectable({
  providedIn: 'root',
})

export class SharedService extends BaseService{

  private data: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  data$: Observable<any> = this.data.asObservable();
  private userInfoEmployee : IParamSearchListReceivingException | undefined;

  setData(newData: BehaviorSubject<any> ) {
    this.data.next(newData);
  }

  set userToUpdateEmployee(userInfoEmployee) {
    this.userInfoEmployee = userInfoEmployee;
  }

  get userToUpdateEmployee() {
    return this.userInfoEmployee;
  }

}
