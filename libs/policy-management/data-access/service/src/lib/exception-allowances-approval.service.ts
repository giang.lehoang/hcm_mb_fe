import {Injectable} from "@angular/core";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {
  IActionConfirmExceptionAllowances,
  IAllowanceExtendListUserInfo,
} from "@hcm-mfe/policy-management/data-access/models";
import {UrlConstant} from "@hcm-mfe/policy-management/data-access/service";
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";

@Injectable({
  providedIn: 'root',
})

export class ExceptionAllowancesApprovalService extends BaseService {
  getExceptionAllowancesApprovalUserInfo() {
    const url = `${UrlConstant.END_POINT_POLICY_MANAGEMENT.urlListReceivingExceptionAllowances}/user-info`;
    return this.get(url,undefined, `${MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT}`);
  }

  actionConfirmExceptionAllowances(params: IActionConfirmExceptionAllowances, isBackDate: boolean){
    let url = `${UrlConstant.END_POINT_POLICY_MANAGEMENT.urlListReceivingExceptionAllowances}/approve/current-date`;
    if(isBackDate){
      url = `${UrlConstant.END_POINT_POLICY_MANAGEMENT.urlListReceivingExceptionAllowances}/approve/back-date`;
    }
    return this.post(url, params, undefined, `${MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT}`);
  }
}
