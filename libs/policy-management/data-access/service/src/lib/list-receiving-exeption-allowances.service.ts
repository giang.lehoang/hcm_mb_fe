import { Injectable } from '@angular/core';
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {MICRO_SERVICE, Mode,  UrlConstant as URLCommon} from "@hcm-mfe/shared/common/constants";
import {
  IListReceivingExceptionAllowances,
  IParamSearchListReceivingException
} from "@hcm-mfe/policy-management/data-access/models";
import {UrlConstant} from "./constant/url.class";

@Injectable({
  providedIn: 'root',
})
export class ListReceivingExceptionAllowancesService extends BaseService {
  getListReceivingExceptionAllowances(params: Partial<IParamSearchListReceivingException>) {
    const url = `${UrlConstant.END_POINT_POLICY_MANAGEMENT.urlListReceivingExceptionAllowances}/search`;

    return this.post(url, params,undefined, `${MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT}`);
  }

  getListReceivingExceptionAllowancesBackDate(params: Partial<IParamSearchListReceivingException>, isBackDate?: boolean) {
    let url = `${UrlConstant.END_POINT_POLICY_MANAGEMENT.urlListReceivingExceptionAllowances}/search/current-date`;
    if(isBackDate){
      url = `${UrlConstant.END_POINT_POLICY_MANAGEMENT.urlListReceivingExceptionAllowances}/search/back-date`;
    }

    return this.post(url, params,undefined, `${MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT}`);
  }

  getKindOfReward() {
    const url = `${UrlConstant.END_POINT_POLICY_MANAGEMENT.urlListReceivingExceptionAllowances}?type=PC_DIEN_THOAI`;
    return this.get(url, undefined, `${MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT}`);
  }

  postOrPutReceivingExceptionAllowances(params: FormData, type: Mode){
    const url = `${UrlConstant.END_POINT_POLICY_MANAGEMENT.urlListReceivingExceptionAllowances}`;
    if(type === Mode.ADD){
      return this.post(url, params, undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
    }
    return this.put(url, params, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  getDetailReceivingExceptionAllowances(id: number | undefined){
    const url = `${UrlConstant.END_POINT_POLICY_MANAGEMENT.urlListReceivingExceptionAllowances}/${id}`;
    return this.get(url, undefined, `${MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT}`);
  }
  deleteReceivingExceptionAllowancesItem(id: number){
    const url = `${UrlConstant.END_POINT_POLICY_MANAGEMENT.urlListReceivingExceptionAllowances}/${id}`;
    return this.delete(url, undefined, `${MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT}`);
  }

  getDetailEmployeeInfo(id: number){
    // hcm-person-info/v1.0/employees/get-detail/auth?empId
    const url =`pol${URLCommon.API_VERSION}/employee/${id}`;
    return this.get(url, undefined, `${MICRO_SERVICE.POL}`);
  }

}
