import { Injectable } from "@angular/core";
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { MICRO_SERVICE, SERVICE_NAME } from '@hcm-mfe/shared/common/constants';
import { environment } from '@hcm-mfe/shared/environment';
import { UrlConstant } from './constant/url.class';
import {
  ApproveAllRequest,
  ApproveResquest,
  ResponseEntity,
  SalaryModel,
  SalaryParams,
  SearchApprove
} from "@hcm-mfe/policy-management/data-access/models";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root',
})
export class SalaryEvolutionService extends BaseService {

  getListData(params: any) {
    return this.get(SERVICE_NAME.POLICY_MANAGEMENT + UrlConstant.END_POINT_SALARY_EVOLUTION.listSalaryEvolution, { params: params }, MICRO_SERVICE.POL);
  }
  getDownloadUrl() {
    return `${SERVICE_NAME.POLICY_MANAGEMENT + UrlConstant.END_POINT_SALARY_EVOLUTION.downloadTemplate}`;
  }
  importExcel() {
    return `${SERVICE_NAME.POLICY_MANAGEMENT}${UrlConstant.END_POINT_SALARY_EVOLUTION.importTemplate}`;
  }
  deleteSalaryEvolution(id: string) {
    return this.delete(SERVICE_NAME.POLICY_MANAGEMENT + UrlConstant.END_POINT_SALARY_EVOLUTION.deleteSalaryEvolution +'/'+`${id}`,{}, MICRO_SERVICE.POL);
  }
  getQCL() {
    return this.get(SERVICE_NAME.POLICY_MANAGEMENT + UrlConstant.END_POINT_SALARY_EVOLUTION.getQCL, {}, MICRO_SERVICE.POL);
  }
  getListApprove(params:SearchApprove, backdate:boolean):Observable<any>{
    const url = backdate ? UrlConstant.END_POINT_SALARY_EVOLUTION.listApproveSalaryProcessBd : UrlConstant.END_POINT_SALARY_EVOLUTION.listApproveSalaryProcess;
    return this.get(SERVICE_NAME.POLICY_MANAGEMENT + url, { params: params }, MICRO_SERVICE.POL);
  }

  approveSalaryProcess(body:ApproveResquest, backdate?:boolean){
    const url = backdate ? UrlConstant.END_POINT_SALARY_EVOLUTION.approveSalaryProcessBd : UrlConstant.END_POINT_SALARY_EVOLUTION.approveSalaryProcess;
    return this.post(SERVICE_NAME.POLICY_MANAGEMENT + url, body,
      {}, MICRO_SERVICE.POL);
  }

  approveSalaryProcessAll(body:ApproveAllRequest, backdate?:boolean){
    const url = backdate ? UrlConstant.END_POINT_SALARY_EVOLUTION.approveSalaryProcessAllBd : UrlConstant.END_POINT_SALARY_EVOLUTION.approveSalaryProcessAll;
    return this.post(SERVICE_NAME.POLICY_MANAGEMENT + url, body,
      {}, MICRO_SERVICE.POL);
  }
  getRankLCB(params: any) {
    return this.get(SERVICE_NAME.POLICY_MANAGEMENT + UrlConstant.END_POINT_SALARY_EVOLUTION.getRankLCB, {params: params}, MICRO_SERVICE.POL);
  }
  initSalayEvolution(body: any) {
    return this.post(SERVICE_NAME.POLICY_MANAGEMENT + UrlConstant.END_POINT_SALARY_EVOLUTION.initSalaryEvoltuion,body, {}, MICRO_SERVICE.POL);
  }
  getDetail(id: string){
    return this.get(SERVICE_NAME.POLICY_MANAGEMENT + UrlConstant.END_POINT_SALARY_EVOLUTION.getDetail+'/'+`${id}`,{} , MICRO_SERVICE.POL);
  }
  getScoredData(params: any){
    return this.get(`${'goa'+ '/'}${UrlConstant.END_POINT_SALARY_EVOLUTION.getScoredEmpoyee}`,
      {params: params}, MICRO_SERVICE.POL);
  }
  getTHSPoint(params: any){
    return this.get(SERVICE_NAME.POLICY_MANAGEMENT + UrlConstant.END_POINT_SALARY_EVOLUTION.getTHSPoint,{params:params} , MICRO_SERVICE.POL);
  }
  getListProcessEmp(params: any){
    return this.get('hcm-person-info'+'/' + UrlConstant.END_POINT_SALARY_EVOLUTION.getListProcessEmp,{params:params} , MICRO_SERVICE.POL);
  }

  getInfoEmpl(empId:number){
    const params = {
      empIds: empId
    }
    return this.get('hcm-person-info'+'/' + UrlConstant.END_POINT_SALARY_EVOLUTION.getEmpInfo,{params:params} , MICRO_SERVICE.POL);
  };

  downloadFile(docId: string){
    return this.get(SERVICE_NAME.POLICY_MANAGEMENT + UrlConstant.END_POINT_SALARY_EVOLUTION.downloadFile+`/${docId}`,{responseType: 'arraybuffer'}, MICRO_SERVICE.POL);
  }
  getListCurency(params: any){
    return this.get(SERVICE_NAME.POLICY_MANAGEMENT + UrlConstant.END_POINT_SALARY_EVOLUTION.getListCurrency,{params:params} , MICRO_SERVICE.POL);
  }
}
