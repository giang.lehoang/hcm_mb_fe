export enum ABType {
  TH = 'TH',
  CB = 'CB',
  XL = 'XL',
  TCCV = 'TCCV',
  CN = 'CN'
}

export const ABComment = {
  TH :'Từ chối HSTH',
  CB :'Từ chối HSCB',
  XL : 'Từ chối HSXL',
  TCCV : 'Từ chối TCCV',
  CN : 'Từ chối CN'
}
