import {userConfig} from "@hcm-mfe/shared/common/constants";

export interface IInfoAllowanceEmployee {
  "employeeId": number | null,
  "prAllowanceId": number | null,
  "fromDate": string | null,
  "toDate": string | null,
  "amount": number | null,
  "note": string,
  currency?: string
}
export interface IListAllowanceProcess {
  temp: IListAllowanceProcess;
  prAllowanceProcessId: number,
  empCode: string,
  empName: string,
  fromDate: string,
  toDate: string,
  allowanceTypeName: string,
  amount: number,
  isAprrove: string | null,
  isDisAble: boolean
}
export interface IInfoEmployee {
  employeeCode?: string
  employeeId?: number | null
  fullName: string
  orgName: string
  positionName: string
  contractTypeName?: string
  flagStatus?: number | string
  jobName?: string
  levelName?: string
  organizationScale?: string
  regionName?: string
  salaryRange?: string
  zoneName?: string
}
export interface ISalaryPeriod {
  prId: number,
  prName: string
}
export interface IAllowanceType {
  prAllowanceId: number,
  prAllowanceName: string
}
export interface IRegion {
  lvaId: number
  lvaMean: string
}
export interface IParamAllowanceProcess {
  orgId: string,
  payOffId: number | string,
  allowanceTypeId: number | string,
  regionId: number | string,
  empId: number | string,
  size: number,
  pageNumber: number
}
export interface IParamAllowanceApprove {
  empId: string | null,
  organizationId: string | null,
  createdBy: string,
  size?: number,
  pageNumber?: number,
  flagStatuses?: number[]
}
export interface IParamApprove {
  action: string,
  reason: string,
  prAllowanceProcessIds: string[];
}

export interface  IListAllowanceApprove {
  allowanceTypeName: string
  amount: number
  employeeCode: string
  fromDate: string
  fullName: string
  isApprove: string
  jobName: string
  levelName: string
  orgName: string
  organizationScale: string
  posName: string
  prAllowanceProcessId: string
  regionName: string
  salaryRange: string
  toDate: string
}
export interface IBaseResponse<T> {
  data: T
}
