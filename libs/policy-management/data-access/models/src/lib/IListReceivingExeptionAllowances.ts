export interface IListReceivingExceptionAllowances {
  prAllowanceExtendListId?: number;
  employeeId: number | string;
  employeeCode: string | null;
  jobName: string;
  posName: string;
  orgName: string;
  fromDate: string;
  toDate: string;
  prAllowanceTypeName: string;
  flagStatus: number;
  prAllowanceType?: string;
  note: string;
  deleteFile?: boolean;
}

export interface IParamSearchListReceivingException{
  employeeCode?: string | null | undefined;
  fullName?: string | null | undefined;
  orgId: number | null;
  prAllowanceType: string | null;
  page?: number | null;
  size?: number | undefined;
  createdBy?: string;
  flagStatus?: number | null;
  statusRecord?: number | null;
  empCode?: string | undefined;
  empId?: string | undefined;
  orgName?: string | undefined;
  levelName?:string | undefined;
  posName?: string | undefined;
  orgPathName?: string | undefined;
  jobName?: string | undefined | null;
}




