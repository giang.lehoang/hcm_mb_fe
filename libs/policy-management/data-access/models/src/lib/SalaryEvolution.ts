export class SalaryParams {
  employeeCode: string = '';
  employeeId: number | undefined | string = undefined;
  fullName: string = '';
  orgName: string = '';
  positionName: string = '';
  level: string|number = '';
  levelName: string = '';
  statusEmployee: string | number = '';
  statusRecord: string = '';
  orgId: number | undefined = undefined;
  prAllowanceType: null | string = null;
  salaryProcessId: string = '';
  jobName?: string;
  joinCompanyDate: string = '';
}
export class SalaryModel {
  id: string = ''
  employeeCode: string = '';
  employeeId: number | undefined | string = undefined;
  fromDate: string = '';
  toDate: string = '';
  salaryGradeName: string = '';
  salaryRankName: string = '';
  salaryAmount: number = 0;
  salaryPercent: number = 100;
  performanceFactor: number = 0;
  performancePercent: number = 100;
  hsTHSDiem: number = 0;
  salaryObject: string = '';
  salaryGroupName: string = '';
  salaryVersionName: string = '';
  flagStatus: number = 0;
  isApprove: string = '';
  salaryProcessId: string = '';
  salaryReal:number = 0;
  employeeType:string = '1';
  employeeTypeName:string = '';
  edit:boolean = false;
  dataTmp?: any;
  curCode: string = 'VND';
  packAmount: string = '';
  performanceAmount: string = '';
  performanceReal: string = '';
  checked: boolean = false;
  orgName: string = '';
}

export interface SearchApprove {
  employeeCode: string,
  orgId:string,
  createBy:string
}

export interface ApproveResquest {
  ids:string[];
  description:string;
  action: 'PD' | 'TC';
}

export interface ApproveAllRequest extends SearchApprove {
  description:string;
  isBackdate: boolean;
}

export const EMPLOYEE_TYPE:string[] = ["Chính thức", "Người đại diện chuyên trách", "Người đại diện kiêm nhiệm", "Trưởng ban kiểm soát, kiểm soát viên"];


export interface QCLModel {
  salaryVersionId: number;
  salaryVersionName: string;
  salaryVersionNameTmp: string;
  flagEnabled: string;
}
export interface RankLCBModel {
  salaryRankId: number;
  salaryRankName: string;
  basicAmount?: number;
  performCoefficient: number;
  salaryGradeId: number;
}
export interface GradeLCBModel {
  salaryGradeId: number;
  salaryGradeName: string;
  salaryRankOfMatrixResponses: RankLCBModel[];
}
export class InitSalaryModel extends SalaryModel{
  salaryVersionId: string ='';
  salaryType: string = 'LHQ';// Nhóm lương
  note:string = ''; // Ghi chú
  packAmount: string = '';
  docId: string = '';
  salaryGradeId: number = 0;
  salaryRankId: number = 0;
  periodCurPoint: string = '';
  periodCurRate: string = ''
  docNo: string = '';
  periodCurFactor: string = '';
  levelTHS: number = 0;
  realTHS: number = 0;
  file:any;
  approver: string = '';
  approveDate: string = '';
  createdBy: string = '';
  docName: string  = '';
  description: string = '';
  lastUpdatedBy: string = '';
  lastUpdateDate: string = '';
}

export interface EmployeeInfo {
  orgId: number,
  empId: number,
  empCode: string,
  fullName: string,
  email: string,
  posId: number,
  posName: string,
  jobId: number,
  jobName: string,
  orgPathId: string,
  orgName: string,
  status: number,
  joinCompanyDate: string,
  dateOfBirth: string,
  posFromDate: string,
  posSeniority: number,
  seniority: string,
  orgPathName: string,
  mobileNumber: string
}

export interface EmployeeDetail {
  dateOfBirth: string;
  email: string;
  empCode: string;
  empId: number;
  fullName: string;
  gender: string;
  jobId: number;
  jobName: string;
  joinCompanyDate: string;
  mobileNumber: string;
  orgId: number;
  orgName: string;
  orgPathId: string;
  orgPathName: string;
  posFromDate: string;
  posId: number;
  posName: string;
  posSeniority: number;
  seniority: number;
  status: number;
  positionLevel: string;
  flagStatus?: number;
  levelName?: string;
  positionName:string;
}



