export class ParamBonus {
  employeeId: number | undefined | string = '';
  orgName: string = '';
  fullName: string = '';
  periodDate: string = '';
  flagStatus: string|number = '';
  empStatus: string = '';
  type: string | number = '';
  orgId: number | undefined|string = '';
  complexValue?:string ='';
  page: number = 0;
  size: number  = 10;
  isSpecial?: string = ''
}

export class ParamApprove {
  employeeId: number | string | undefined;
  orgId: number | string;
  periodDate: string;
  isSpecial: string;

  constructor() {
    this.employeeId = '';
    this.orgId = '';
    this.periodDate = '';
    this.isSpecial = '';
  }

}

export interface AchBonusResponse {
  employeeCode:string;
  flagStatus:number;
  fromDate:string;
  fullName:string;
  jobName:string;
  orgFullName:string;
  rankOfGroupName:string;
  realCoefficient:number|string;
  systemCoefficient:number|string;
  toDate:string;
  typeOfContractName:string;
  dataObj: AchBonusResponse;
  _checked:boolean;
  objectId: string | number;
  claCode:string;
  isValidNumber?: boolean;
  complexValue?:string;
  isValid?:boolean;
  index?:number;
  isSpecial?:string;
  typeBonus?:string;
  positionGroupId: string | number;
  contents:string;
  alLevels:string;
}

export interface ABApprove {
  id:number,
  objectId: number,
  employeeId: number,
  employeeCode: string,
  fullName: string,
  orgFullName: string,
  complexMean:string,
  jobName: string,
  rankOfGroupName: string,
  typeOfContractName: string,
  systemCoefficient: number,
  realCoefficient: number,
  fromDate: string,
  toDate: string,
  flagStatus: number,
  cldName: string,
  scaleName: string,
  CBS: number,
  CB: number,
  THS: number,
  TH: number,
  XLS: number,
  XL: number,
  TCCVS: number,
  TCCV: number,
  CNS:number,
  CN: number,
  ids:{objectId:number, empId: number, type:string, periodDate:string}[],
  isSpecial: 'Y' | 'N';
  comment: { key:number|string,  value:number|string}[];
  commentSelect:{ key:number|string,  value:number|string}[];
  error:boolean;
  periodDate:string;
}

export interface ResponseApprove {
  lstPeriods: ABApprove[],
  lstRanks: ABApprove[],
  lstUnitSizes: ABApprove[],
  lstRatings: ABApprove[],
  lstComplexes: ABApprove[]
}

export interface RatingResponse {
  claCode:string;
  claId:number;
  cldName:string;
  flagStatus:string;
}

export interface ApproveABResquest {
  object:   {
    objectId: number,
    empId: number,
    periodDate: string
  }[],
  "description": string[]
}

export interface RejectABResquest {
  lstSubRequest:  ObjectReject[],
}

export interface ObjectApprove {
  objectId: number,
  empId: number,
  periodDate: string
}

export interface ObjectReject extends ObjectApprove {
  type:string,
  description:string
}
export interface RankGroupResponse {
  name:string;
  id:number;
}
export interface CoefficientSystem {
  coefficient:number;
  headColumn:number;
  headRow:number;
  type:string;
  alLevels:string;
}
