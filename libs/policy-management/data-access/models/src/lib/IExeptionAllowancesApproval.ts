import {CONFIRM} from "./ICommon";

export interface IAllowanceExtendListUserInfo {
  prAllowanceExtendListId: string;
  createBy: string;
}

export type IActionConfirmExceptionAllowances =
  {
    prAllowanceExtendListId: number[],
    action: CONFIRM,
    contents: string
  }



