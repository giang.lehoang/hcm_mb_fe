import {userConfig} from "@hcm-mfe/shared/common/constants";

export interface SalaryCommitmentModel {
  hrSalaryCommitmentId:number;
  employeeId:number;
  fullName:string;
  employeeCode:string;
  posId:number;
  orgId:number;
  lastName:string;
  jobName:string;
  fromDate:string;
  toDate:string;
  amount:number;
  isBonus:string;
  flagStatus:number;
  bonusName?:string;
  curCode?: string;
}

export class SalaryCommitmentSearch {
  employeeCode: string;
  orgId?: number | string;
  createBy: string;
  page: number;
  size: number;
  flagStatus?:number;


  constructor() {
    this.employeeCode = '';
    this.orgId = '';
    this.createBy = '';
    this.page = 0;
    this.size = userConfig.pageSize;
  }
}
export interface ApproveReq {
  ids:number[];
  description:string;
  action: 'REJECT'|'APPROVE';
}

export interface ApproveAllReq extends SalaryCommitmentSearch {
  description: string;
}
export class SalaryCommitmentInit {
  employeeCode: string;
  fromDate: string;
  toDate: string;
  commitmentLevelCur: string | number;
  commitmentLevel: string | number;
  file: File | undefined;
  isBonus: boolean;
  note: string;
  curCode:string;

  constructor() {
    this.employeeCode = '';
    this.fromDate = '';
    this.toDate = '';
    this.commitmentLevel = '';
    this.commitmentLevelCur = '';
    this.isBonus = true;
    this.note = '';
    this.file = undefined;
    this.curCode = 'VND';
  }
}

export interface CommitmentRequest {
  salaryCommitmentId?: number | string,
  employeeId: number | string,
  fromDate: string,
  toDate: string,
  amount: string | number,
  bonus: boolean,
  note: string,
  deleteFile?:boolean;
  curCode:string
}

export interface CommitmentResponse {
  salaryCommitmentId: number,
  employeeId: string | number,
  employeeCode: string,
  fullName: string,
  fromDate: string,
  toDate: string,
  organizationId: number,
  jobId: number,
  positionId: number,
  levelName: string,
  orgName: string,
  positionName: string,
  jobName: string,
  amount: number | string,
  isBonus: boolean,
  docId: string,
  fileName: string,
  note: string,
  curCode:string
}

export const BONUS = {
  'N': "Không",
  'Y' : 'Có'
}
