export interface IListOfColumn {
  key: number;
  name: string;
  type: string;
  expression: string;
  show: boolean;
  width: string;
}

export class ConstantColor {
  public static readonly TAG = {
    groupName: '#108ee9',
    factor: '#f50'
  };
}

export const flagStatus = {
  STATUS_LIST: [
    {key: '', value: 'Tất cả'},
    {key: '2', value: 'Chờ duyệt'},
    {key: '3', value: 'Từ chối'},
    {key: '4', value: 'Phê duyệt'},
  ],
}
export const groupSalary = {
  LIST_GROUP_SALARY: [
    {key: 'LHQ', value: 'LHQ'},
    {key: 'NSLD', value: 'NSLD'},
  ]
}

export interface ResponseEntity<T> {
  data?: T;
  content?: T;
  size?: number;
  totalElements?: number;
  totalPages?: number;
  numberOfElements?: number;
  number?: number;
  pageable?: {
    pageNumber: number;
  };
}

export interface ResponseEntityData<T> {
  data?: ResponseEntity<T>;
}

export interface SelectObject<T, V> {
  key: T,
  value: V
}

export const EMPLOYEE_STATUS: SelectObject<number, string>[] = [
  {
  key: 1,
  value: "Hiện diện"
  },
  {
    key: 2,
    value: "Tạm hoãn"
  },
  {
    key: 3,
    value: "Đã nghỉ việc"
  }
]

export enum CONFIRM {
  REJECT = "REJECT",
  APPROVE = "APPROVE",
}
export const REGEX_CURRENCY = /^[0-9,]+/;
export const REGEX_NUMBER = /^[0-9.]+/;
export interface LookupCode {
  lvaValue: string;
  lvaMean: string;
}
export const SALARY_PERIOD = {
 START_DATE: '26',
 END_DATE: '25',
}
export const TYPE_BONUS: SelectObject<string, string>[] = [
  {
    key: "Y",
    value: "Thưởng thành tích đặc biệt"
  },
  {
    key: "N",
    value: "Thưởng thành tích"
  },
]
