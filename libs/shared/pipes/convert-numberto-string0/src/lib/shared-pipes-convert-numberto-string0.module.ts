import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CovertNumberString0} from "./covert-number-string-0";

@NgModule({
  imports: [CommonModule],
  declarations: [
    CovertNumberString0
  ],
  exports: [
    CovertNumberString0
  ],
})
export class SharedPipesConvertNumbertoString0Module {}
