import { Pipe, PipeTransform } from "@angular/core";

@Pipe({name: 'covertNumberString0'})
export class CovertNumberString0 implements PipeTransform {
  transform(value: number | null | string): string | number {
    if(value === 0){
      return '0';
    }
    return Number(value);
  }
}
