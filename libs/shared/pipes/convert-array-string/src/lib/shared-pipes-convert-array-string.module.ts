import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConvertArrayStringPipe } from './convert-array-string.pipe';

@NgModule({
  imports: [CommonModule],
  declarations: [ConvertArrayStringPipe],
  exports: [ConvertArrayStringPipe],
})
export class SharedPipesConvertArrayStringModule {}
