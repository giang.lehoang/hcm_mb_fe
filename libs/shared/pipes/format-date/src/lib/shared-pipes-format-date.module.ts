import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DateFormatPipe } from './format-date.pipe';

@NgModule({
  imports: [CommonModule],
  declarations: [DateFormatPipe],
  exports: [DateFormatPipe],
})
export class SharedPipesFormatDateModule {}
