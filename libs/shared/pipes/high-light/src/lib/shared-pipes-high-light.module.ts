import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HighlightSearch } from './high-light.pipe';

@NgModule({
  imports: [CommonModule],
  declarations: [HighlightSearch],
  exports: [HighlightSearch],
})
export class SharedPipesHighLightModule {}
