import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SumValuePipe } from './sum-value.pipe';

@NgModule({
  imports: [CommonModule],
  declarations: [
    SumValuePipe
  ],
  exports: [
    SumValuePipe
  ],
})
export class SharedPipesSumValueModule {}
