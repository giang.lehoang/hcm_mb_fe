import { CurrencyPipe, DatePipe, DecimalPipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sumValue'
})
export class SumValuePipe implements PipeTransform {

  constructor(
    private datePipe: DatePipe,
    private currencyPipe: CurrencyPipe,
    private decimalPipe: DecimalPipe,
  ) { }

  transform(column: any, data: any[]): any {
    return 0;
  }
}
