import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SplitPipe} from "./split.pipe";

@NgModule({
  imports: [CommonModule],
  exports: [SplitPipe],
  declarations: [SplitPipe]
})
export class SharedPipesSplitModule {}
