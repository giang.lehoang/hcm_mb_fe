import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableFiledPipe } from './table-filed.pipe';

@NgModule({
  imports: [CommonModule],
  declarations: [TableFiledPipe],
  exports: [TableFiledPipe],
})
export class SharedPipesTableFiledModule {}
