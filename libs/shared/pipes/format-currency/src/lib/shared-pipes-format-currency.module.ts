import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormatCurrencyPipe } from './format-currency.pipe';

@NgModule({
  imports: [CommonModule],
  declarations: [FormatCurrencyPipe],
  exports: [FormatCurrencyPipe],
})
export class SharedPipesFormatCurrencyModule {}
