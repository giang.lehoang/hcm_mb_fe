import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NumberFilterPipe } from './number-filter.pipe';

@NgModule({
  imports: [CommonModule],
  declarations: [NumberFilterPipe],
  exports: [NumberFilterPipe],
})
export class SharedPipesNumberFilterModule {}
