import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'formatScore' })
export class FormatScorePipe implements PipeTransform {
    transform(value: any): string {
        let result: any = null;
        if (value) {
            result = (Math.round(parseFloat(value) * 100) / 100).toFixed(2)
        } else {
            result = value;
        }
        return result;
    }
}
