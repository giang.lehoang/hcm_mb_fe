import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormatScorePipe } from './format-score.pipe';

@NgModule({
  imports: [CommonModule],
  declarations: [FormatScorePipe],
  exports: [FormatScorePipe],
})
export class SharedPipesFormatScoreModule {}
