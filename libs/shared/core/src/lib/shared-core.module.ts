import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebSocketService } from './service/websocket.service';
import { UserService } from './service/user.service';
import { RoleGuardService } from './guard/role-guard.service';
import { CustomToastrService } from './service/custom-toastr.service';
import { ToastrModule } from 'ngx-toastr';
import { ConfirmService } from './service/confirm.service';
import { LoginGuard } from './guard/login.guard';
import { AuthGuard } from './guard/auth.guard';

@NgModule({
  imports: [CommonModule, ToastrModule.forRoot({})],
  exports: [ToastrModule],
  providers: [WebSocketService, UserService, WebSocketService, RoleGuardService , CustomToastrService, ConfirmService, LoginGuard, AuthGuard]
})
export class SharedCoreModule {}
