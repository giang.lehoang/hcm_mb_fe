import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild, UrlTree } from '@angular/router';
import { KeycloakService, KeycloakAuthGuard } from 'keycloak-angular';
import { Observable } from 'rxjs';
import { SessionService } from "@hcm-mfe/shared/common/store";
import { Scopes, SessionKey } from '@hcm-mfe/shared/common/enums';
import { AppFunction } from '@hcm-mfe/shared/data-access/models';
import { UserService } from '../service/user.service';
import { HttpClient } from '@angular/common/http';

type responseType = boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree>;
@Injectable({
  providedIn: "any"
})
export class RoleGuardService extends KeycloakAuthGuard implements CanActivateChild {
  isRedirectTo = true;
  data: { resourceId: string; };
  isTracing: string;

  constructor(
    protected readonly router: Router,
    protected readonly keycloak: KeycloakService,
    private readonly sessionService: SessionService,
    private readonly http: HttpClient,
    private readonly userService: UserService,
  ) {
    super(router, keycloak);
    if(!localStorage.getItem(SessionKey.CURRENCY_USER)) {
      this.userService.getCurrentUser().toPromise().then(res => {
        if(res && res.data) {
          localStorage.setItem(SessionKey.CURRENCY_USER, JSON.stringify(res.data));
          this.isTracing = res.data.isTracing;
        }
      })
    } else {
      this.isTracing = JSON.parse(localStorage.getItem(SessionKey.CURRENCY_USER))?.isTracing;
    }
  }

  isAccessAllowed(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    return new Promise(async (resolve) => {
      if (!this.authenticated) {
        localStorage.clear();
        await this.keycloakAngular.login();
        return;
      }
      const objFunction: AppFunction = this.sessionService.getSessionData(`FUNCTION_${route?.data?.code}`);
      if (objFunction) {
        localStorage.setItem('FUNCTION_SCOPE', route?.data?.scope || Scopes.VIEW);
        const currCode = localStorage.getItem('FUNCTION_CODE');
        if (currCode !== objFunction.rsCode && this.isTracing === 'Y') {
          this.data = {
            resourceId: objFunction.rsId
          }
          localStorage.setItem('FUNCTION_CODE', objFunction.rsCode);
          this.save();
        }
        resolve(true);
      } else {
        await this.router.navigateByUrl('/');
        resolve(false);
      }
    });
  }

  canActivateChild(
    childRoute: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): responseType {
    const objFunction: AppFunction = this.sessionService.getSessionData(`FUNCTION_${childRoute?.data?.code}`);
    if ((objFunction && !childRoute?.data?.scope) || objFunction?.scopes?.includes(childRoute?.data?.scope)) {
      localStorage.setItem('FUNCTION_SCOPE', childRoute?.data?.scope || Scopes.VIEW);
      const currCode = localStorage.getItem('FUNCTION_CODE');
      if (currCode !== objFunction.rsCode && this.isTracing === 'Y') {
        this.data = {
          resourceId: objFunction.rsId
        }
        localStorage.setItem('FUNCTION_CODE', objFunction.rsCode);
        this.save();
      }
      return true;
    }
    this.router.navigateByUrl('/');
    return false;
  }

  save() {
    this.userService.sessionDetail(this.data).subscribe();
  }
}
