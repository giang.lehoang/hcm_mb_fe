import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import { AuthService } from '@hcm-mfe/shared/data-access/services';
import { StorageService } from '@hcm-mfe/shared/common/store';
import { STORAGE_NAME } from '@hcm-mfe/shared/common/constants';
import { environment } from '@hcm-mfe/shared/environment';

@Injectable({
  providedIn: "any"
})
export class LoginGuard implements CanActivate {
  constructor(
    private authService: AuthService,
  private router: Router
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    if (!this.authService.isAuthenticated() && StorageService.get(STORAGE_NAME.APP) === environment.app.HCM_ESS_WEB) {
      return true;
    } else {
      this.router.navigate(['']);
      return false;
    }
  }
}
