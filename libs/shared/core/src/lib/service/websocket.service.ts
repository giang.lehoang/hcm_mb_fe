import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { environment } from '@hcm-mfe/shared/environment';

declare const io: any;

@Injectable({
  providedIn: 'root'
})
export class WebSocketService {
  token: string;
  url: string;
  socket: any;
  message$: any;
  constructor(){
    this.message$ = new Subject<any>();
  }
  disconnect() {
    if (this.socket) {
      this.socket.disconnect();
      this.socket = null;
    }
  }

  connect(token: string) {
    this.disconnect();
    this.token = token;
    this.url = `${environment.webSocketUrl}?token=${this.token}`;
    this.socket = new io.connect(this.url, { transports: ["websocket", "polling"], forceNew: true });

    this.socket.on('message', (data) => {
      this.message$.next(data);
    });
  }

  completeMessage() {
    if (this.message$) {
      this.message$.complete();
    }
  }
}
