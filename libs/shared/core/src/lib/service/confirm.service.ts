import { Injectable, TemplateRef } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { TranslateService } from '@ngx-translate/core';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { ConfirmType } from '@hcm-mfe/shared/common/enums';

@Injectable({
  providedIn: 'any',
})
export class ConfirmService {
  constructor(private readonly modalService: NzModalService, private readonly translate: TranslateService ) {}

  success(message?: string | TemplateRef<NzSafeAny>): Promise<any> {
    return this.openModal(ConfirmType.Success, message);
  }

  error(message?: string | TemplateRef<NzSafeAny>): Promise<boolean> {
    return this.openModal(ConfirmType.CusError, message);
  }

  confirm(message?: string | TemplateRef<NzSafeAny>): Promise<any> {
    return this.openModal(ConfirmType.Confirm, message);
  }

  warn(message?: string | TemplateRef<NzSafeAny>): Promise<any> {
    return this.openModal(ConfirmType.Warning, message);
  }

  openModal(confirmType: ConfirmType, message?: string | TemplateRef<NzSafeAny>): Promise<any> {
    return new Promise((resolve) => {
      this.modalService.confirm(
        {
          nzContent: message || this.translate.instant('common.label.confirmMessage'),
          nzTitle: this.translate.instant('common.label.confirmTitle'),
          nzCancelText: this.translate.instant('common.button.cancel'),
          nzOkText: this.translate.instant('common.button.continue'),
          nzClassName: 'ld-confirm',
          nzOnOk: () => {
            resolve(true);
          },
          nzOnCancel: () => {
            resolve(false);
          }
        },
        confirmType
      );
    });
  }
}
