import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { KeycloakService } from 'keycloak-angular';
import { STORAGE_NAME, url } from '@hcm-mfe/shared/common/constants';
import { IdleUserCheckService, WebSocketService } from '@hcm-mfe/shared/core';
import { NotificationService } from '@hcm-mfe/shell/data-access/services';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { StorageService } from "@hcm-mfe/shared/common/store";
import { environment } from '@hcm-mfe/shared/environment';
import { AuthService } from '@hcm-mfe/shared/data-access/services';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class UserService extends BaseService {

  constructor(private readonly http: HttpClient,
              private readonly keyCloakService: KeycloakService,
              private readonly webSocketService: WebSocketService,
              private readonly notificationService: NotificationService,
              private authService: AuthService,
              private router: Router
              ) {
    super(http);
  }
  getCurrentUser() : Observable<any>{
    const uri = `${url.url_endpoint_admin_current_user}`;
    return this.get(uri, undefined, 'URL_ENDPOIN_CATEGORY');

  }

  sessionDetail(data: any) : Observable<any>{
    const uri = `${url.url_endpoint_admin_sessions_detail}`;
    return this.post(uri, data, undefined, 'URL_ENDPOIN_CATEGORY');
  }

  logout(): void {
    const isInternal = StorageService.get(STORAGE_NAME.APP) === environment.app.HCM_WEB;
    IdleUserCheckService.runTimer = false;
    StorageService.clear();
    localStorage.clear();
    this.webSocketService.disconnect();
    this.webSocketService.completeMessage();
    this.notificationService.countNotification$.complete();
    if (isInternal) {
      if (this.keyCloakService) {
        localStorage.setItem(STORAGE_NAME.USER_NAME, this.keyCloakService.getUsername());
        this.keyCloakService.logout();
      } else {
        window.location.reload();
      }
    } else {
      this.authService.logout();
      window.location.href = `${location.origin}/login`
    }
  }
}
