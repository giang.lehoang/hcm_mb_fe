import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {UrlConstant} from "@hcm-mfe/shared/common/constants";
import {BaseResponse, CurrentPage} from "@hcm-mfe/shared/data-access/models";

@Injectable({
  providedIn: "root"
})
export class StaffInfoService extends BaseService {

  public getCatalog(typeCode: string, parentCode?: number){
    const url = UrlConstant.API_VERSION + UrlConstant.CATALOGS.PREFIX;
    return this.get(url, {params: parentCode ? {typeCode, parentCode} : {typeCode}});
  }

  public getBanks(){
    const url = UrlConstant.API_VERSION + UrlConstant.BANKS;
    return this.get(url);
  }

  public getEmployeeData(param: CurrentPage): Observable<BaseResponse>{
    const url = UrlConstant.API_VERSION + UrlConstant.EMPLOYEES.PREFIX + UrlConstant.EMPLOYEES.DATA_PICKER;
    return this.get(url, {params:param})
  }

  public getListSchool(){
    const url = UrlConstant.API_VERSION + UrlConstant.SCHOOLS;
    return this.get(url);
  }

  public getListFaculty(){
    const url = UrlConstant.API_VERSION + UrlConstant.FACULTIES;
    return this.get(url);
  }

  public getListMajorLevel(){
    const url = UrlConstant.API_VERSION + UrlConstant.MAJOR_LEVELS;
    return this.get(url);
  }
}
