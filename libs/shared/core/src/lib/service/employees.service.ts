import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {environment} from "@hcm-mfe/shared/environment";

@Injectable({
  providedIn: 'root',
})
export class EmployeesService {

  constructor(private http: HttpClient) {}

  fullTextSearchEmployees(keyword: string): Observable<any> {
    const params = {
      keyword: keyword
    }
    return this.http.get(`${environment.baseUrl}v1.0/employees`, {params: params});
  }

  public getAvatar(employeeId: number): Observable<any> {
    return this.http.get(`${environment.baseUrl}v1.0/employees/${employeeId}/avatar`);
  }

  public getQrCode(employeeId: number, width: number, height: number): Observable<any> {
    const body = {
      width: width,
      height: height,
    };
    return this.http.post(`${environment.baseUrl}v1.0/employees/${employeeId}/qrcode`, body);
  }
}
