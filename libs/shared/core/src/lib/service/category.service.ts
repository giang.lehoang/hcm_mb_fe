import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { maxInt32 } from '@hcm-mfe/shared/common/enums';
import { environment } from '@hcm-mfe/shared/environment';
import { STORAGE_NAME, url } from '@hcm-mfe/shared/common/constants';
import { StorageService } from '@hcm-mfe/shared/common/store';

@Injectable({
  providedIn: 'root',
})
export class CategoryService {
  constructor(private http: HttpClient) {}

  getCategory() {
    const params = {
      page: 0,
      size: maxInt32,
    };
    return this.http.get(`${environment.adminUrl}function`, { params: params });
  }

  getMenuByUser() {
    const params = {
      appCode: StorageService.get(STORAGE_NAME.APP) ?? 'HCM-WEB',
    };
    return this.http.get(`${environment.adminUrl}${url.url_endpoint_admin_menu}`, { params: params });
    // return this.http.get(`http://10.215.254.8:10433/hcm-admin/${url.url_endpoint_admin_menu}`, { params: params });
  }
}
