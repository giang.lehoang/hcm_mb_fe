import {Injectable} from "@angular/core";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {Observable} from "rxjs";
import {maxInt32} from "@hcm-mfe/shared/common/enums";
import {MICRO_SERVICE, SERVICE_NAME} from "@hcm-mfe/shared/common/constants";
import {UrlConstant} from "@hcm-mfe/goal-management/data-access/services";

@Injectable({
  providedIn: 'root',
})
export class ShareService extends BaseService {
  getJobsByType(jobType?: any, page?: any, size?: any): Observable<any> {
    const params = {
      page: page ? page : 0,
      size: size ? size : maxInt32,
      jobType: jobType ? jobType : 'ORG'
    }
    return this.get(SERVICE_NAME.MODEL_PLAN + '/v1.0/jobs/type', {params: params}, MICRO_SERVICE.MODEL_PLAN);
  }

  getOrganizationTree(params?) {
    return this.get(`${UrlConstant.END_POINT_MODEL_PLAN.urlEndPointOrgTree}/getAll`,
      {params}, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
}
