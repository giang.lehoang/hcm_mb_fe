import { ToastrService } from 'ngx-toastr';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class CustomToastrService {
  constructor(private toastr: ToastrService) {}

  success(message?: string) {
    this.toastr.success(
      `<div class="toast-content"> <img src="assets/images/icon/icon-alert-success.svg"> <span class="toast-content__text"> ${
        message ? message : 'Thành công'
      }  </span> </div>`,
      '',
      { enableHtml: true },
    );
  }


  error(message?: string) {
    this.toastr.error(
      `<div toast-content> <img src="assets/images/icon/icon-alert-error.svg"> <span class="toast-content__text"> ${
        message ? message : 'Đã có lỗi xảy ra'
      } </span> </div>`,
      '',
      { enableHtml: true }
    );
  }

  warning(message?: string) {
    this.toastr.warning(
      `<div toast-content> <img src="assets/icon/toastr/warning.svg"> <span class="toast-content__text"> ${
        message ? message : 'Đã có lỗi xảy ra'
      }  </span> </div>`,
      '',
      { enableHtml: true }
    );
  }

  info(message?: string) {
    this.toastr.info(
      `<div toast-content><span class="toast-content__text"> ${
        message ? message : 'Thông báo'
      }  </span> </div>`,
      '',
      { enableHtml: true }
    );
  }
}
