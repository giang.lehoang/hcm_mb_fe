import { Injectable } from '@angular/core';
import {
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivate,
  UrlTree,
} from '@angular/router';
import { KeycloakService } from 'keycloak-angular';
import { Observable } from 'rxjs';
import { Scopes, SessionKey } from '@hcm-mfe/shared/common/enums';
import { AppFunction, UserLogin } from '@hcm-mfe/shared/data-access/models';
import { STORAGE_NAME } from '@hcm-mfe/shared/common/constants';
import { CategoryService } from './category.service';
import { WebSocketService } from './websocket.service';
import { UserService } from './user.service';
import {SessionService, StorageService} from "@hcm-mfe/shared/common/store";
import { environment } from '@hcm-mfe/shared/environment';
import { AuthService } from '@hcm-mfe/shared/data-access/services';

type responseType = boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree>;
@Injectable({
  providedIn: 'root',
})
export class AppInitService implements CanActivate {
  isLoaded = false;

  constructor(
    protected readonly router: Router,
    private readonly sessionService: SessionService,
    private readonly keycloakService: KeycloakService,
    private readonly userService: UserService,
    private readonly categoryService: CategoryService,
    private readonly webSocketService: WebSocketService,
    private authService: AuthService
  ) {
    // let permission = false;
    // this.router.events.subscribe((res) => {
    //   permission = this.sessionService.getSessionData(SessionKey.IS_PERMISSION);
    //   if (res instanceof NavigationEnd && res.url === functionUri.access_denied && permission) {
    //     this.router.navigateByUrl('/');
    //   }
    // });
    this.token().then();
  }

  token = async () => {
    let token = '';
    if (StorageService.get(STORAGE_NAME.APP) === environment.app.HCM_WEB) {
     const accessToken = await this.keycloakService?.getToken();
        token = accessToken;
    } else {
      token = StorageService.get(STORAGE_NAME.ACCESS_TOKEN);
    }
    if (token) {
      const currSession = this.parseJwtSessionState(token);
      const sessionState = localStorage.getItem(SessionKey.SESSION_STATE);
      if (currSession?.session_state !== sessionState) {
        const obj = this.keycloakService.getKeycloakInstance();
        localStorage.removeItem(SessionKey.FUNCTION_CODE);
        localStorage.setItem(SessionKey.SESSION_STATE, currSession?.session_state);
        localStorage.removeItem(SessionKey.CURRENCY_USER);
        const response: any = await this.userService.getCurrentUser().toPromise();
        if (response && response.data) {
          localStorage.setItem(SessionKey.CURRENCY_USER, JSON.stringify(response.data));
        }
        if (obj) {
          const userLogin = new UserLogin();
          userLogin.id = obj.profile?.id;
          userLogin.username = obj.profile?.username;
          userLogin.firstName = response?.data?.fullName;
          userLogin.employeeId = response?.data?.empId;
          userLogin.employeeCode = response?.data?.empCode;
          StorageService.set(STORAGE_NAME.USER_LOGIN, userLogin);
          StorageService.set(STORAGE_NAME.ACCESS_TOKEN, obj.token);
          StorageService.set(STORAGE_NAME.REFRESH_TOKEN, obj.refreshToken);
        }
      }
      this.webSocketService.connect(token);
    }
  }

  parseJwtSessionState(token) {
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    const jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
    return JSON.parse(jsonPayload);
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): responseType {
    if (!this.authService.isAuthenticated() && StorageService.get(STORAGE_NAME.APP) === environment.app.HCM_ESS_WEB) {
      this.router.navigate(['/login']);
      return false;
    }
    return new Promise( async (resolve, reject) => {
      try {
        const menus = this.sessionService.getSessionData(SessionKey.MENU)
        if(menus){
          resolve(true);
          return;
        }
        this.isLoaded = true;
        const functionByUser: any = await this.categoryService.getMenuByUser().toPromise();

        const menuAll = [];
        const menu = [];
        for (const item of functionByUser.data) {
          const childs: Array<any> = functionByUser.data.filter(el => el.parentId === item.id && el.isMenu !== 'Y');
          if(childs && childs.length > 0) {
            childs.forEach(child => {
              if(!item.scopes) {
                item.scopes = [];
              }
              if(item.scopes) {
                item.scopes.push(...child.scopes);
                item.scopes = [...new Set(item.scopes)];
              }
            });
          }
        }
        let isPermissionView = false;
        for (const item of functionByUser.data) {
          if (!isPermissionView && item && item?.scopes?.includes(Scopes.VIEW)) {
            isPermissionView = true;
          }
          this.sessionService.setSessionData(item.id, item)
          const objFunction: AppFunction = {
            rsId: item?.id,
            rsUri: item?.uri,
            rsCode: item?.code,
            rsName: item?.name,
            scopes: item?.scopes,
            view: item.scopes?.indexOf(Scopes.VIEW) > -1,
            create: item.scopes?.indexOf(Scopes.CREATE) > -1,
            edit: item.scopes?.indexOf(Scopes.EDIT) > -1,
            delete: item.scopes?.indexOf(Scopes.DELETE) > -1,
            approve: item.scopes?.indexOf(Scopes.APPROVE) > -1,
            import: item.scopes?.indexOf(Scopes.IMPORT) > -1,
            upload: item.scopes?.indexOf(Scopes.UPLOAD) > -1,
            download: item.scopes?.indexOf(Scopes.DOWNLOAD) > -1,
            adjusted: item.scopes?.indexOf(Scopes.ADJUSTED) > -1,
            correction: item.scopes?.indexOf(Scopes.CORRECTION) > -1,
            cancel: item.scopes?.indexOf(Scopes.CANCEL) > -1,
            export: item.scopes?.indexOf(Scopes.EXPORT) > -1,
            generate: item.scopes?.indexOf(Scopes.GENERATE) > -1
          }
          this.sessionService.setSessionData(`FUNCTION_${item.code}`, objFunction);
          item.subs = [];
          menuAll.push(item)
        }
        // this.sessionService.setSessionData(SessionKey.IS_PERMISSION, isPermissionView);
        //add subs menu to menu
        for (const item of menuAll) {
          if(item.isMenu === 'N'){
            continue
          }

          if (item.parentId) {
            const parent = this.sessionService.getSessionData(item.parentId);
            if(!parent){
              continue
            }
            parent.subs.push(item);
          } else {
            menu.push(item)
          }
        }
        this.sessionService.setSessionData(SessionKey.MENU, menu)
        this.sessionService.setSessionData(SessionKey.MENU_ALL, menuAll)
        resolve(true);
    } catch (err) {
      reject(err);
    }
    });
  }
}
