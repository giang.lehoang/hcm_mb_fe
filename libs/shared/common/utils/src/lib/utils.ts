import { Observable } from 'rxjs';
import {FileConstant, HTTP_STATUS_CODE} from '@hcm-mfe/shared/common/constants';
import {NzUploadFile} from "ng-zorro-antd/upload";
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";
import {StringUtils} from "./string-utils.class";
import * as moment from "moment";
import * as _ from "lodash";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";
import {AbstractControl, FormControl, FormGroup} from "@angular/forms";
import { NzSafeAny } from 'ng-zorro-antd/core/types';

export class Utils {
  private static async preresolve(promises, value, key?: string) {
    if (key === 'callback' || key === 'socket' || key === 'listener') {
      promises.push(value);
      return;
    }
    if (value instanceof Function) {
      this.preresolve(promises, value());
    } else if (value instanceof Array) {
      const inpromises = [];
      value.forEach((v) => {
        this.preresolve(inpromises, v);
      });
      promises.push(Promise.all(inpromises));
    } else if (value instanceof Observable) {
      promises.push(value.toPromise());
    } else if (value instanceof Promise) {
      promises.push(value);
    } else if (value instanceof Object) {
      promises.push(this.resolve(value));
    } else {
      promises.push(value);
    }
  }

  static async resolve(value: Object) {
    const keys = [];
    const promises = [];
    for (const i in value) {
      if (value.hasOwnProperty(i)) {
        keys.push(i);
        this.preresolve(promises, value[i], i);
      }
    }
    const values = await Promise.all(promises);
    const raw = {};
    keys.forEach((key: string, index: number) => {
      raw[key] = values[index];
    });
    return raw;
  }

  // static isEmailValid(email: string): boolean {
  //   const regex = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.+[A-Z]{2,4}/gim;
  //   return regex.test(email);
  // }

  static isNotNull(obj: any): boolean {
    return !this.isNull(obj);
  }

  static isNull(obj: any): boolean {
    return obj === undefined || obj === null || obj === {};
  }

  static isEmpty(obj: any): boolean {
    return _.isEmpty(obj);
  }

  static isNotEmpty(obj: any): boolean {
    return !this.isEmpty(obj);
  }

  static isFunction(obj: any): boolean {
    return this.isNotNull(obj) && obj instanceof Function;
  }

  static isNotFunction(obj: any): boolean {
    return !this.isFunction(obj);
  }

  static isStringEmpty(obj: string): boolean {
    return this.isNull(obj) || obj === '';
  }

  static isStringNotEmpty(obj: string): boolean {
    return !this.isStringEmpty(obj);
  }

  static isArrayEmpty(obj: any[]): boolean {
    return this.isNull(obj) || obj.length === 0;
  }

  static isArrayNotEmpty(obj: any[]): boolean {
    return !Utils.isArrayEmpty(obj);
  }

  static isHtmlNotEmpty(text: string): boolean {
    return !Utils.isHtmlEmpty(text);
  }

  static subString(text: string) {
    return text?.substr(0, 1) || '';
  }

  static isHtmlEmpty(text: string): boolean {
    if (Utils.isNull(text)) {
      return true;
    }
    text = text.replace(/(<([^>]+)>)/gi, '');
    text = text.trim();
    return Utils.isStringEmpty(text);
  }

  static number(obj: any, defaultValue?: number) {
    if (Utils.isNull(obj) || Utils.isStringEmpty(obj) || isNaN(obj)) {
      return defaultValue;
    }
    return Number(obj);
  }

  static shorten(text: string, numOfCharacters: number = 300): string {
    if (Utils.isStringNotEmpty(text)) {
      text = text.replace(/(<([^>]+)>)/gi, '');
      text = text.replace(/\s+/g, ' ');
      if (text.length > numOfCharacters) {
        text = text.substr(0, numOfCharacters) + '...';
      }
    }
    return text;
  }

  static stripHtmlTags(text: string): string {
    return text ? text.replace(/\<.*?\>/gi, '') : undefined;
  }

  static parseToEnglish(text: string): string {
    if (Utils.isNull(text)) {
      return '';
    }
    let value = text.trim().toLowerCase();
    value = value.replace(/[áàảãạâấầẩẫậăắằẳẵặ]/gi, 'a');
    value = value.replace(/[éèẻẽẹêếềểễệ]/gi, 'e');
    value = value.replace(/[iíìỉĩị]/gi, 'i');
    value = value.replace(/[óòỏõọơớờởỡợôốồổỗộ]/gi, 'o');
    value = value.replace(/[úùủũụưứừửữự]/gi, 'u');
    value = value.replace(/[yýỳỷỹỵ]/gi, 'y');
    value = value.replace(/[đ]/gi, 'd');
    return value;
  }

  static trim(text: string) {
    return text.trim();
  }

  static trimToNull(value: string) {
    if (this.isNull(value)) {
      return null;
    } else {
      return value.trim();
    }
  }

  static trimNullToEmpty(value: string) {
    if (this.isNull(value)) {
      return '';
    } else {
      return value.trim();
    }
  }

  static numberWithCommas(x) {
    return x?.toString().replace(/\B(?=(\d\d\d)+(?!\d))/g, ',');
  }

  static parseCode(value: string) {
    if (Utils.isStringEmpty(value)) {
      return '';
    }
    const i = value.lastIndexOf('-');
    if (i >= 0) {
      return value.substr(i + 1);
    }
    return value;
  }

  static truncate(value: string, limit = 30, completeWords = false, ellipsis = '...') {
    if (value.length < limit) return `${value.substr(0, limit)}`;

    if (completeWords) {
      limit = value.substr(0, limit).lastIndexOf(' ');
    }
    return `${value.substr(0, limit)}${ellipsis}`;
  }

  static orderBy(data: any, key: string | string[]) {
    return _.orderBy(data, key);
  }

  static processResponse(responseFromServer: BaseResponse, toastrService: ToastrService, translateService: TranslateService) {
    switch (responseFromServer.code) {
      case HTTP_STATUS_CODE.OK:
        return responseFromServer?.data;
      case HTTP_STATUS_CODE.CREATED:
        toastrService.error(responseFromServer?.message);
        break;
      default:
        toastrService.error(translateService.instant('shared.notification.errorServer'));
        break;
    }

    return null;
  }

// Neu be tra ve Date voi dinh dang DD/MM/YYYY kieu string.
  static convertDateToFillForm(dateFromServer: string | NzSafeAny) {
    if (StringUtils.isNullOrEmpty(dateFromServer)) {
      return null;
    }

    return moment(dateFromServer, "DD/MM/YYYY").toDate();
  }

  static fullResetFormControl(formControls: AbstractControl[]) {
    if (formControls && formControls.length > 0) {
      for (const fc of formControls) {
        fc.reset();
        fc.setValidators(null);
        fc.setErrors(null);
        fc.updateValueAndValidity();
      }
    }
  }

  static addDays(dateFromServer: string | NzSafeAny, days: number) {
    if (StringUtils.isNullOrEmpty(dateFromServer)) {
      return null;
    }
    return moment(dateFromServer, "DD/MM/YYYY").add(days, 'days');
  }

  static subtractDays(dateFromServer: string | NzSafeAny, days: number) {
    if (StringUtils.isNullOrEmpty(dateFromServer)) {
      return null;
    }
    return moment(dateFromServer, "DD/MM/YYYY").subtract(days, 'days');
  }

  static convertDateToSendServer(date: NzSafeAny) {
    if (date) {
      return moment(date).format('DD/MM/YYYY')
    }
    return null;
  }

  static convertDateTimeToSendServer(date: NzSafeAny) {
    if (date) {
      return moment(date).format('DD/MM/YYYY HH:mm:ss')
    }
    return null;
  }

  static processTrimFormControlsBeforeSend(form: FormGroup, ...formControlsName) {
    const controls = form.controls;
    for(const controlName of formControlsName) {
      const value = controls[controlName]?.value;
      controls[controlName].setValue(value?.trim());
    }
  }

  static getFirstDayOrLastDayOfCurrentMonth(firstDay: boolean) {
    const now = new Date();
    if (!firstDay) { // ngay cuoi thang
      return moment(new Date(now.getFullYear(), now.getMonth() + 1, 0), "DD/MM/YYYY").toDate();
    }
    return moment(new Date(now.getFullYear(), now.getMonth(), 1), "DD/MM/YYYY").toDate();
  }

  static getFirstDayOrLastDayOfCurrentMonthString(firstDay: boolean, month?: number, year?: number): string {
    const now = new Date();
    if (!firstDay) { // ngay cuoi thang
      return moment(new Date(year ? year : now.getFullYear(), month ? month : now.getMonth() + 1, 0), "DD/MM/YYYY").format('DD/MM/YYYY');
    }
    return moment(new Date(year ? year : now.getFullYear(), month ? month : now.getMonth(), 1), "DD/MM/YYYY").format('DD/MM/YYYY');
  }
}

export const detectBrowserVersion = () => {
  const userAgent = navigator.userAgent
  let tem,matchTest = userAgent.match(/(edg|opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];

  if (/trident/i.test(matchTest[1])) {
    tem = /\brv[ :]+(\d+)/g.exec(userAgent) || [];
    return 'IE ' + (tem[1] || '');
  }

  if (matchTest[1] === 'Chrome') {
    tem = userAgent.match(/\b(OPR|Edge)\/(\d+)/);
    if (tem != null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
  }

  matchTest = matchTest[2] ? [matchTest[1], matchTest[2]] : [navigator.appName, navigator.appVersion, '-?'];

  if ((tem = userAgent.match(/version\/(\d+)/i)) != null) matchTest.splice(1, 1, tem[1]);
  return matchTest.join(' ');
};

export function serialize(obj = {}) {
  const arr = [];
  for (const k of Object.keys(obj)) {
    arr.push(
      `${k}=${encodeURIComponent(
        typeof obj[k] === 'string'
          ? String.prototype.trim.call(obj[k])
          : obj[k] === null
            ? ''
            : obj[k]
      )}`
    );
  }
  return arr.join('&');
}

export function delEmptyKey(obj: {}) {
  const objCpy = {};
  if (obj === null || obj === undefined || obj === '') {
    return objCpy;
  }
  for (const key in obj) {
    if (obj[key] !== null && typeof obj[key] === 'object') {
      objCpy[key] = this.delEmptyKey(obj[key]);
    } else if (obj[key] !== null && obj[key] !== undefined && obj[key] !== '') {
      objCpy[key] = obj[key];
    }
  }
  return objCpy;
}

export function isEmptyObject(obj: {}) {
  let name: any;
  // tslint:disable-next-line: forin
  for (name in obj) {
    return false;
  }
  return true;
}

export function isValidDate(date: Date) {
  return date instanceof Date && !isNaN(date.getTime());
}

export function obj2Str(obj: any) {
  const p = {};
  for (const key of Object.keys(obj)) {
    if (obj[key] || obj[key] === 0) {
      if (obj[key].toString() !== '') {
        p[key] = obj[key].toString();
      }
    }
  }
  return p;
}

export function str2arr(str: string) {
  return str.replace(/[\r\n\s]/g, '').split(',');
}

export function getScrollbarWidth() {
  const scrollDiv = document.createElement('div');
  scrollDiv.style.cssText =
    'width: 99px; height: 99px; overflow: scroll; position: absolute; top: -9999px;';
  document.body.appendChild(scrollDiv);
  const scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;
  document.body.removeChild(scrollDiv);

  return scrollbarWidth;
}
export function getTypeExport(format: string | any): string {
  let type: string;
  switch (format) {
    case FileConstant.EXCEL_TYPE_1:
      type = FileConstant.EXCEL_MIME_1;
      break;
    case FileConstant.EXCEL_TYPE_2:
      type = FileConstant.EXCEL_MIME_2;
      break;
    case FileConstant.PDF_TYPE:
      type = FileConstant.PDF_MIME;
      break;
    case FileConstant.DOC_TYPE:
      type = FileConstant.DOC_MIME;
      break;
    case FileConstant.DOCX_TYPE:
      type = FileConstant.DOCX_MIME;
      break;
    default:
      type = null;
      break;
  }
  return type;
}

export function convertToType(typevar, input) {
  return {
    'string': String.bind(null, input),
    'number': Number.bind(null, input)
  }[typeof typevar]();
}

export function checkFileExtension(fileName: string, ...fileExtensions) {
  if (fileName) {
    return fileExtensions.some((item) => fileName.toLowerCase().endsWith(item.toLowerCase()));
  }
  return false;
}

export function beforeUploadFile(file: File | NzUploadFile, fileList, fileSize: number,toastrService : ToastrService, translate: TranslateService, error: string | null, ...fileExtensions) {
  let check = true;
  if (fileExtensions.length > 0) {
    check = fileExtensions.some((item) => file?.name.toLowerCase().endsWith(item.toLowerCase()))
  }
  if (file.size > fileSize || !check) {
    if (error) {
      toastrService.error(translate.instant(error));
    }
    return fileList;
  }
  fileList = fileList.concat(file);
  return fileList;
}
