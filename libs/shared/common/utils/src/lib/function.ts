import { FormControl, FormGroup } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import * as _ from 'lodash';
import { Roles, Scopes } from '@hcm-mfe/shared/common/enums';

export function getRole(roles: string[]): string {
  if (roles) {
    if (roles.indexOf(Roles.TL) !== -1) {
      return Roles.TL;
    } else if (roles.indexOf(Roles.SeRM) !== -1) {
      return Roles.SeRM;
    } else if (roles.indexOf(Roles.RM) !== -1) {
      return Roles.RM;
    } else if (roles.indexOf(Roles.RGM) !== -1) {
      return Roles.RGM;
    } else if (roles.indexOf(Roles.RDM) !== -1) {
      return Roles.RDM;
    } else if (roles.indexOf(Roles.OP) !== -1) {
      return Roles.OP;
    } else if (roles.indexOf(Roles.ED) !== -1) {
      return Roles.ED;
    } else if (roles.indexOf(Roles.BODS) !== -1) {
      return Roles.BODS;
    } else if (roles.indexOf(Roles.BOD) !== -1) {
      return Roles.BOD;
    } else if (roles.indexOf(Roles.BM) !== -1) {
      return Roles.BM;
    } else if (roles.indexOf(Roles.BDM) !== -1) {
      return Roles.BDM;
    } else if (roles.indexOf(Roles.ADM) !== -1) {
      return Roles.ADM;
    }
  }
  return null;
}

export function generateAvatar(name: string, isUser?: boolean) {
  const nameArr = name?.trim().split(' ');
  if (nameArr.length > 1) {
    if (isUser) {
      return nameArr[nameArr.length - 2] + ' ' + nameArr[nameArr.length - 1];
    } else {
      return nameArr[0] + ' ' + nameArr[1];
    }
  }
  return name;
}
export function buildRSQL(objSearch, query: string) {
  if (!query) {
    query = '';
  }
  if (objSearch) {
    const condition = objSearch.condition === 'and' ? ';' : ',';
    objSearch.rules.forEach((rule) => {
      if (rule.condition) {
        query += '(';
        query = buildRSQL(rule, query);
        query += ')';
        query += condition;
      } else {
        if (rule.value && rule.value.toString().length > 0) {
          query += rule.field;
          switch (rule.operator) {
            case '=':
              query += '==';
              query += rule.value;
              break;
            case 'like':
              query += '==';
              query += '*' + rule.value + '*';
              break;
            case 'in':
              query += '=in=';
              query += '(' + rule.value + ')';
              break;
            case 'out':
              query += '=out=';
              query += '(' + rule.value + ')';
              break;
            default:
              query += rule.operator;
              query += rule.value;
              break;
          }
          query = query + condition;
        }
      }
    });
  }
  query = query && query.length > 0 ? query.substr(0, query.length - 1) : query;
  return query;
}

export function buildBaseQuery(objSearch: any, query: string) {
  if (!query) {
    query = '';
  }
  if (objSearch) {
    Object.keys(objSearch).forEach((key) => {
      if (objSearch[key] && objSearch[key].toString().trim().length > 0) {
        query += key;
        query += '==';
        query += objSearch[key];
        query += ';';
      }
    });
  }
  return query.length > 0 ? query.substr(0, query.length - 1) : query;
}

export function sortTaskKanban(array: any[]) {
  return array.sort((a, b) => {
    if (!b.index) {
      return -1;
    } else if (b.index && a.index && a.index !== b.index) {
      return a.index - b.index; // asc
    }
    if (!b.timeIndex) {
      return -1;
    } else if (b.timeIndex && a.timeIndex && a.timeIndex !== b.timeIndex) {
      return b.timeIndex - a.timeIndex; // asc
    }
    return  -1;
  });
}

export function validateAllFormFields(formGroup: FormGroup) {
  Object.keys(formGroup.controls).forEach((field) => {
    const control = formGroup.get(field);
    if (control instanceof FormControl) {
      control.markAsTouched({ onlySelf: true });
    } else if (control instanceof FormGroup) {
      validateAllFormFields(control);
    }
  });
}

export function cleanDataForm(formGroup: FormGroup) {
  const data = cleanData(formGroup.getRawValue());
  formGroup.patchValue(data, { emitEvent: false });
  return data;
}

export function cleanData(data: any) {
  if (_.isArray(data)) {
    for (let index = 0; index < data?.length; index++) {
      if (_.isString(data[index])) {
        data[index] = _.trim(data[index]);
      } else if (_.isNull(data[index]) || _.isUndefined(data[index]) || _.isNaN(data[index])) {
        delete data[index];
      } else if (_.isArray(data[index]) || _.isObject(data[index])) {
        cleanData(data[index]);
      }
    }
  } else {
    Object.keys(data).forEach((key) => {
      if (_.isString(data[key])) {
        data[key] = _.trim(data[key]);
      } else if (_.isNull(data[key]) || _.isUndefined(data[key]) || _.isNaN(data[key])) {
        delete data[key];
      } else if (_.isArray(data[key]) || _.isObject(data[key])) {
        cleanData(data[key]);
      }
    });
  }
  return data;
}

export function checkScopeForScreen(scope, objFunction) {
  if (!objFunction) {
    return false;
  }
  switch (scope) {
    case Scopes.CREATE:
      return objFunction.create;
    case Scopes.EDIT:
      return objFunction.edit;
    case Scopes.VIEW:
      return objFunction.view;
    case Scopes.DELETE:
      return objFunction.delete;
    case Scopes.DOWNLOAD:
      return objFunction.download;
    case Scopes.IMPORT:
      return objFunction.import;
    default:
      return false;
  }
}

export function percentage(value: number, total: number) {
  if (value === 0 && total === 0) {
    return value.toFixed(2);
  }
  return ((100 * value) / total)?.toFixed(2);
}

export function checkRespondSuccess(respond) {
  return !(respond instanceof HttpErrorResponse);
}

export function getTextWidth(text: string) {
  const canvas = document.createElement('canvas');
  const context = canvas.getContext('2d');

  // context.font = getComputedStyle(document.body).font;

  return context.measureText(text).width;
}

export function arrayBufferToBase64(buffer) {
  var binary = '';
  var bytes = new Uint8Array(buffer);
  var len = bytes.byteLength;
  for (var i = 0; i < len; i++) {
    binary += String.fromCharCode(bytes[i]);
  }
  return window.btoa(binary);
}

export function clearParamSearch(params: any) {
  const newParams: any = {};
  Object.keys(params).forEach((key) => {
    if ((!_.isNull(params[key]) && !_.isUndefined(params[key])) || _.isNumber(params[key])) {
      newParams[key] = params[key];
    }
  });
  return newParams;
}



export function getInitializedDate(createDate, now, translate) {
  createDate = new Date(createDate);
  now = new Date(now);
  const timediff = now - createDate;
  if (isNaN(timediff)) {
    return '';
  }
  if (createDate) {
    const seconds = Math.floor((timediff) / 1000);
    if (seconds < 1000) // less than 30 seconds ago will show as 'Just now'
      return translate.instant('notify.label.now');
    const intervals: { [key: string]: number } = {
      'year': 31536000,
      'month': 2592000,
      'week': 604800,
      'days': 86400,
      'hours': 3600,
      'minute': 60,
      'now': 1
    };
    let counter;
    for (const i in intervals) {
      counter = Math.floor(seconds / intervals[i]);
      if (counter > 0)
          return counter + translate.instant(`notify.label.${i}Before`);
    }
  }
  return createDate;
}

export function toLowerCaseNonAccentVietnamese(str) {
  str = str.toLowerCase();
    str = str.replace(/\u00E0|\u00E1|\u1EA1|\u1EA3|\u00E3|\u00E2|\u1EA7|\u1EA5|\u1EAD|\u1EA9|\u1EAB|\u0103|\u1EB1|\u1EAF|\u1EB7|\u1EB3|\u1EB5/g, "a");
    str = str.replace(/\u00E8|\u00E9|\u1EB9|\u1EBB|\u1EBD|\u00EA|\u1EC1|\u1EBF|\u1EC7|\u1EC3|\u1EC5/g, "e");
    str = str.replace(/\u00EC|\u00ED|\u1ECB|\u1EC9|\u0129/g, "i");
    str = str.replace(/\u00F2|\u00F3|\u1ECD|\u1ECF|\u00F5|\u00F4|\u1ED3|\u1ED1|\u1ED9|\u1ED5|\u1ED7|\u01A1|\u1EDD|\u1EDB|\u1EE3|\u1EDF|\u1EE1/g, "o");
    str = str.replace(/\u00F9|\u00FA|\u1EE5|\u1EE7|\u0169|\u01B0|\u1EEB|\u1EE9|\u1EF1|\u1EED|\u1EEF/g, "u");
    str = str.replace(/\u1EF3|\u00FD|\u1EF5|\u1EF7|\u1EF9/g, "y");
    str = str.replace(/\u0111/g, "d");
  str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, "");
  str = str.replace(/\u02C6|\u0306|\u031B/g, "");
  return str;
}
