import {LangChangeEvent, TranslateService} from "@ngx-translate/core";
import {NzI18nService} from "ng-zorro-antd/i18n";
import {en_US_ext, vi_VN_ext} from "@hcm-mfe/shared/common/constants";
import {enUS, vi} from "date-fns/locale";

export class TranslateCommon {
  constructor(i18n: NzI18nService, translate: TranslateService, namespace: string) {
    translate.store.onLangChange.subscribe(
      (lang: LangChangeEvent) => {
        if(lang.lang === 'vn') {
          i18n.setLocale(vi_VN_ext);
          i18n.setDateLocale(vi);
        } else {
          i18n.setLocale(en_US_ext);
          i18n.setDateLocale(enUS);
        }
        translate.use(lang.lang);
        for (const [key, value] of Object.entries(translate.store.translations)) {
          translate.setTranslation(key, value, true);
        }
        if (translate.store.translations[lang.lang] && !translate.store.translations[lang.lang][namespace]) {
          translate.getTranslation(lang.lang).subscribe(res => {
            if (!translate.store.translations[lang.lang]) {
              translate.store.translations[lang.lang] = {};
            }
            translate.store.translations[lang.lang] = {...translate.store.translations[lang.lang], ...res};
          });
        }
      }
    );
    translate.getTranslation(translate.store.currentLang).subscribe(res => {
      if (!translate.store.translations[translate.store.currentLang]) {
        translate.store.translations[translate.store.currentLang] = {};
      }
      translate.store.translations[translate.store.currentLang] = {...translate.store.translations[translate.store.currentLang], ...res};
    });
    for (const [key, value] of Object.entries(translate.store.translations)) {
      translate.setTranslation(key, value, true);
    }
  }
}
