import { AbstractControl, ValidatorFn } from '@angular/forms';

export function validateRangeNumber(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    if ( 0 > parseFloat(control.value) ||
    parseFloat(control.value) > 100 ||
    parseFloat(control.value) === 0 || parseFloat(control.value) === 1) {
        return { rangeNumber: true };
    }
  };
}

export function validateNotAllowSpace(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    let value = control.value;
    if (value === " ") {
       return value = value.replace(/\s/g,'')
    }
  };
}