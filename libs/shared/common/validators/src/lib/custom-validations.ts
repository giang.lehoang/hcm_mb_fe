import {AbstractControl, FormControl, ValidationErrors, ValidatorFn} from '@angular/forms';
import * as moment from "moment";

const regexPhoneMobileVN = new RegExp(/84|0[35789]([0-9]{8})/g);
const regexEmail = new RegExp(/^([\w+-.%]+@[\w-.]+\.[A-Za-z]{2,6},?)+$/);
const regexOnlyNumber = new RegExp(/^[0-9]*$/g);
const regexCode = new RegExp(/^[0-9A-Z-_]*$/g);
const regexApi = new RegExp(/^[0-9a-zA-Z-_/.*?=&{}]*$/g);
const regexUrl = new RegExp(/^[0-9a-zA-Z-_/.?=:&]*$/g);
const regexCodeKpi = new RegExp(/^[a-zA-Z0-9]*$/g);
// ([1-9]|0(?=,[1-9])) -> Bat dau boi 1-9 hoac 0 nhung theo sau boi , va 1-9
const regexRate = new RegExp(/^([1-9]|0(?=,[1-9]))(\d*)(,\d)?$/g);
// 2 number decimal
const regexStandardKpi = new RegExp(/^(\d+)(,\d{1,2})?$/g);
const regexValueByTitle = new RegExp(/^(\d+)(,\d{1,2})?$/g);
const valueFactor = new RegExp(/^(\d+)(,\d{1,2})?$/g);
const regexRmTime = new RegExp(/^(\d+)(,\d{1,2})?$/g);
const regexSpecial = '^[A-Za-z0-9ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ ]*'
const regexSpecialForLine = new RegExp(/[!@#$%^&*()+\=\[\]{};':"\\|,<>\/?~]/);
export class CustomValidators {
  static required(control: AbstractControl): ValidationErrors | null {
    if (
      (!control.value && control.value !== 0 && control.value !== false) ||
      control.value?.toString()?.trim()?.length === 0
    ) {
      return { required: true };
    }
    return null;
  }

  static code(control: AbstractControl): ValidationErrors | null {
    regexCode.lastIndex = 0;
    if (control.value && !regexCode.test(control.value?.toString())) {
      return { code: true };
    }
    return null;
  }

  static api(control: AbstractControl): ValidationErrors | null {
    regexApi.lastIndex = 0;
    if (control.value && !regexApi.test(control.value?.toString())) {
      return { api: true };
    }
    return null;
  }

  static url(control: AbstractControl): ValidationErrors | null {
    regexUrl.lastIndex = 0;
    if (control.value && !regexUrl.test(control.value?.toString())) {
      return { url: true };
    }
    return null;
  }

  static codeKpi(control: AbstractControl): ValidationErrors | null {
    regexCodeKpi.lastIndex = 0;
    if (control.value && !regexCodeKpi.test(control.value?.toString())) {
      return { code: true };
    }
    return null;
  }

  static valueFactor(control: AbstractControl): ValidationErrors | null {
    valueFactor.lastIndex = 0;
    let factor = parseFloat(control.value?.toString()?.replace(',', '.'));
    let validFactor = factor > 0;
    if (control.value && (!valueFactor.test(control.value?.toString()) || !validFactor)) {
      return { value: true };
    }
    return null;
  }

  static rate(control: AbstractControl): ValidationErrors | null {
    regexRate.lastIndex = 0;
    let rate = parseFloat(control.value?.toString()?.replace(',', '.'));
    let validRate = rate > 0 && rate <= 100;
    if (control.value && (!regexRate.test(control.value?.toString()) || !validRate)) {
      return { value: true };
    }
    return null;
  }

  static standardKpi(control: AbstractControl): ValidationErrors | null {
    regexStandardKpi.lastIndex = 0;
    let standardKpi = parseFloat(control.value?.toString()?.replace(',', '.'));
    let validStandardKpi = standardKpi >= 0;
    if (control.value && (!regexStandardKpi.test(control.value?.toString()) || !validStandardKpi)) {
      return { value: true };
    }
    return null;
  }

  static assignKpi(control: AbstractControl): ValidationErrors | null {
    regexStandardKpi.lastIndex = 0;
    let assignKpi = parseFloat(control.value?.toString()?.replace(',', '.'));
    let validAssignKpi = assignKpi >= 0;
    if (control.value && (!regexStandardKpi.test(control.value?.toString()) || !validAssignKpi)) {
      return { value: true };
    }
    return null;
  }

  static valueRmTime(control: AbstractControl): ValidationErrors | null {
    regexRmTime.lastIndex = 0;
    let timeRate = parseFloat(control.value?.toString()?.replace(',', '.'));
    let validTimeRate = timeRate > 0 && timeRate <= 1;
    if (control.value && (!regexRmTime.test(control.value?.toString()) || !validTimeRate)) {
      return { value: true };
    }
    return null;
  }

  static valueByTitle(control: AbstractControl): ValidationErrors | null {
    regexValueByTitle.lastIndex = 0;
    let valueByTitle = parseFloat(control.value?.toString()?.replace(',', '.'));
    let validValueByTitle = valueByTitle >= 0;
    if (control.value && (!regexValueByTitle.test(control.value?.toString()) || !validValueByTitle)) {
      return { value: true };
    }
    return null;
  }

  static noSpace(control: AbstractControl): ValidationErrors | null {
    if (control.value && control.value.toString().indexOf(' ') >= 0) {
      return { noSpace: true };
    }
    return null;
  }

  static phoneMobileVN(control: AbstractControl): ValidationErrors | null {
    regexPhoneMobileVN.lastIndex = 0;
    if (control.value && !regexPhoneMobileVN.test(control.value.toString())) {
      return { phoneMobile: true };
    }
    return null;
  }

  static onlyNumber(control: AbstractControl): ValidationErrors | null {
    regexOnlyNumber.lastIndex = 0;
    if (control.value && !regexOnlyNumber.test(control.value.toString())) {
      return { onlyNumber: true };
    }
    return null;
  }

  static imageSizeValidator(maxSize: number) {
    return function (input: FormControl) {
      if (input.value) {
        return input.value.size > maxSize ? {maxSize: true} : null;
      }
      return null;
    };
  }
  static imageExtensionValidator(whiteListImageExtension: Array<string>) {
    return function(input: FormControl) {
      if (input.value) {
        return whiteListImageExtension.includes(input.value.type) ? null : {extension: true};
      }
      return null;
    };
  }
  static mailValidator(control: AbstractControl): ValidationErrors | null {
    regexEmail.lastIndex = 0;
    if (control.value && !regexEmail.test(control.value.toString().replaceAll(/\s/g,','))) {
      return { mailValidator: true };
    }
    return null;
  }

  static toDateAfterFromDate(fromDateField: string, toDateField: string) {
    return (formGroup: AbstractControl): { [key: string]: boolean } | null => {
      let fromDate = formGroup.get(fromDateField).value
      let toDate = formGroup.get(toDateField).value
      if ((fromDate !== null && toDate !== null)) {
        fromDate = moment(formGroup.get(fromDateField).value).startOf('day').toDate();
        toDate = moment(formGroup.get(toDateField).value).startOf('day').toDate();
        if (fromDate > toDate) {
          formGroup.get(toDateField).setErrors({ toDateNotAfterFromDate: true });
          return { toDateNotAfterFromDate: true };
        } else {
          formGroup.get(toDateField).setErrors(null);
        }
      }
      return null;
    };
  }

    static validateOnly9Or12Digit(str: string, errorName: string = 'onlyAccept9Or12Digit'): ValidatorFn {
        return (formGroup: AbstractControl): { [key: string]: boolean } | null => {
            const str1 = formGroup.get(str).value;
            const only9or12DigitPattern = new RegExp('^(\\d{9}|\\d{12})$');

            if (str1) {
                if (!only9or12DigitPattern.test(str1)) {
                    return {[errorName]: true};
                }
            }
            return null;
        };
    }

}
