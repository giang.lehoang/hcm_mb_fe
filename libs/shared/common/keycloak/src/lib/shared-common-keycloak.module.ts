import { APP_INITIALIZER, NgModule } from '@angular/core';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { initializer } from '@hcm-mfe/shared/common/init';
import { HttpClient } from '@angular/common/http';
import {Router} from "@angular/router";

@NgModule({
  imports: [KeycloakAngularModule],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initializer,
      deps: [KeycloakService, HttpClient, Router],
      multi: true,
    }
  ]
})
export class SharedCommonKeycloakModule {}
