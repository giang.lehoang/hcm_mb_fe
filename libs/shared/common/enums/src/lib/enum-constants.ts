export enum Actions {
  CREATE,
  UPDATE,
  VIEW,
  DELETE,
  SWITCH,
  CHECKBOX,
  LOGOUT,
  CHOOSE_ITEM,
}

export enum REGEX {
  SPECIAL_TEXT = '^[A-Za-z0-9ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêắấớốếứìíòóôõùýúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ ]*',
  SPECIAL_TEXT_SHIFT = '^[A-Za-z0-9\\-ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêắấớốếứìíòóôõùýúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ ]*',
  SPECIAL_TEXT_CODE = '^[A-Za-z0-9.ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêắấớốếứìíòóôõùýúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ ]*',
}

export enum ProcessType {
  LEAD = 'LEAD',
  OPPORTUNITY = 'OPPORTUNITY',
}

export enum TaskType {
  LEAD = 'LEAD',
  OPPORTUNITY = 'OPPORTUNITY',
  CAMPAIGN = 'CAMPAIGN',
  ACCOUNT = 'ACCOUNT',
  CONTACT = 'CONTACT',
  CUSTOMER = 'CUSTOMER',
  PG_KH = 'PG_KH',
  TODO = 'TD',
}

export enum CalendarType {
  CALENDAR = 'CALENDAR',
  TODO = 'TD',
  ACTIVITY = 'HD',
}

export enum StatusTask {
  IMPORT_PENDING = 'IMPORT_PENDING',
  IMPORT_SUCCESS = 'IMPORT_SUCCESS',
  WAITING_APPROVE = 'WAITING_APPROVE',
  WAITING_ASSIGN = 'WAITING_ASSIGN',
  ASSIGN_BRANCH = 'ASSIGN_BRANCH',
  ASSIGN_PGD = 'ASSIGN_PGD',
  ASSIGN_TEAM = 'ASSIGN_TEAM',
  ASSIGN_RM = 'ASSIGN_RM',
  ABORT = 'ABORT',
  STARTED = 'STARTED',
  COMPLETED = 'COMPLETED',
  DONE = 'DONE',
  PENDING = 'PENDING',
  NEW = 'NEW',
  REJECTED = 'REJECTED',
  OPEN = 'OPEN',
}

export enum StatusLead {
  NEW = 'NEW',
  CONTACTED = 'CONTACTED',
  WORKING = 'WORKING',
  UNQUALIFIED = 'UNQUALIFIED',
  QUALIFIED = 'QUALIFIED',
  CONVERTED = 'CONVERTED',
}

export const leadType = {
  assigned: 'ASSIGN',
  followUp: 'FOLLOWUP',
  support: 'SUPPORT',
};

export enum StatusTaskOpportunity {
  NEW = 'NEW',
  MEETING = 'MEETING',
  PRESENTATION = 'PRESENTATION',
  PROPOSAL = 'PROPOSAL',
  NEGOTIATION = 'NEGOTIATION',
  SENT_APPROVE = 'SENT_APPROVE',
  WAITING_APPROVE = 'WAITING_APPROVE',
  LOST = 'LOST',
  WON = 'WON',
  REJECT = 'REJECT',
}

export enum Roles {
  TL = 'TL',
  SeRM = 'SeRM',
  RM = 'RM',
  RGM = 'RGM',
  RDM = 'RDM',
  OP = 'OP',
  ED = 'ED',
  BODS = 'BODS',
  BOD = 'BOD',
  BM = 'BM',
  BDM = 'BDM',
  ADM = 'ADM',
}

export enum ServiceType {
  SALE,
  WORKFLOW,
  CATEGORY,
  ACTIVITY,
}

export enum SocketClientState {
  ATTEMPTING,
  CONNECTED,
}

export const FieldType = {
  textbox: 'textbox',
  textarea: 'textarea',
  date: 'date',
  checkbox: 'checkbox',
  radio: 'radio',
  select: 'select',
};

export enum FieldStyle {
  BLOCK,
  INLINE,
}

export enum DateType {
  DATE,
  HAS_HOUR,
}

export const DescriptionTypeTask = {
  campaign: 'Chiến dịch',
  lead: 'Khách hàng',
  task: 'Việc cần làm',
};

export const DescriptionStatusTask = {
  success: 'Import thành công',
  pending: 'Đang xử lý',
  abort: 'Import lỗi',
  assign: 'Chờ phân công',
  started: 'Cần thực hiện',
  completed: 'Đã thực hiện',
  done: 'Đã hoàn thành',
};

export enum CampaignStatus {
  InProgress = 'IN_PROGRESS',
  Activated = 'ACTIVATED',
}

export const productStatus = {
  active: {
    description: 'Active',
    code: '1',
  },
  inActive: {
    description: 'InActive',
    code: '0',
  },
};

export const ProgressBarClass = ['progress--blue', 'progress--yellow', 'progress--red', 'progress--green'];

export enum RoomType {
  Channel = 'c',
  Group = 'p',
  Direct = 'd',
}

export enum RoomPath {
  Channel = 'channel',
  Group = 'group',
  Direct = 'direct',
}

export enum ActivityType {
  Call = 'CALL',
  Meeting = 'MEETING',
  SMS = 'SMS',
  Email = 'EMAIL',
}

export const dataLaneLead = [
  { id: 'new', name: 'Chưa liên hệ', index: 0, isDisabled: false, isFilter: false },
  { id: 'waiting', name: 'Đang liên hệ', index: 1, isDisabled: false, isFilter: false },
  { id: 'unqualified', name: 'Không có nhu cầu', index: 2, isDisabled: false, isFilter: false },
  { id: 'qualified', name: 'Có nhu cầu', index: 3, isDisabled: true, isFilter: false },
];

export const gender = {
  male: {
    code: 'MALE',
    name: 'Nam',
  },
  female: {
    code: 'FEMALE',
    name: 'Nữ',
  },
};

export const maxInt32 = 2147483647;
export const defaultExportExcel = 200000;
export const typeExcel = [
  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  'application/vnd.ms-excel',
];

export const componentDashboards = {
  leadStatistic: {
    component: 'leadStatistic',
    cols: 2,
    rows: 1,
    image: 'lead_report.jpg',
    type: 'lead',
  },
  activityCallSuccess: {
    component: 'activityCallSuccess',
    cols: 1,
    rows: 2,
    image: 'activity_call_report.jpg',
    type: 'lead',
  },
  opportunityWonReport: {
    component: 'opportunityWonReport',
    cols: 1,
    rows: 2,
    image: 'opportunity_report.jpg',
    type: 'lead',
  },
  activityWeeklyReport: {
    component: 'activityWeeklyReport',
    cols: 2,
    rows: 3,
    image: 'activity_chart_report.jpg',
    type: 'lead',
  },
  activityUserRank: {
    component: 'activityUserRank',
    cols: 2,
    rows: 3,
    image: 'team_rate_report.jpg',
    type: 'lead',
  },
};

export enum HttpRespondCode {
  EXIST_CODE = '001',
}

export enum Scopes {
  VIEW = '00_VIEW',
  CREATE = '01_CREATE',
  EDIT = '02_EDIT',
  DELETE = '03_DELETE',
  APPROVE = '04_APPROVE',
  IMPORT = '05_IMPORT',
  UPLOAD = '06_UPLOAD',
  DOWNLOAD = '07_DOWNLOAD',
  ADJUSTED = '08_ADJUSTED',
  CORRECTION = '09_CORRECTION',
  CANCEL = '10_CANCEL',
  EXPORT = '11_EXPORT',
  GENERATE = '12_GENERATE'
}

export enum FunctionCode {
  GOA_HANDLE_CLASSIFICATION = 'D-02-009',
  GOA_SETUP_SYSTEM = 'D-02-011',
  PERSONNEL_EXCEPTION_RATINGS = 'D-02-010',
  SYSTEM_ALLOCATION = 'D-02-002',
  CLASSIFICATION_OF_BLOCKS_AND_LINES = 'D-02-003',
  ALLOCATION_LINE_BRANCH = 'D-02-004',
  TARGET_ASSESSMENT_RESULT = 'E-01-004',
  PERSONAL_TARGET_LIST = 'E-01-002',
  BALANCED_SCORECARD_MANAGEMENT = 'D-01-004',
  SET_OF_INDICATORS = 'D-01-002',
  SETUP_ASSESSMENT = 'D-01-003',
  EVALUETION_APPROVAL_SET_OF_INDICATOR = 'E-01-003',
  HR_ACCEPTANCE_RECORD = "HR_ACCEPTANCE_RECORD", // Tuyển dụng
  HR_EMPLOYES = "HR_EMPLOYES",    //1Quản lý thông tin nhân viên
  HR_RESEARCH = "HR_RESEARCH",    //1.1Tra cứu
  HR_RECRUITMENT = "HR-MENU-RECRUITMENT",    //1.1Tra cứu
  PROPOSAL_CANDIDATE = "PROPOSAL-CANDIDATE",// Đề xuất tuyển dụng
  APPROVAL_CANDIDATE = "APPROVAL-CANDIDATE",// Phế duyệt tuyển dụng
  HR_PERSONAL_INFO = "HR_PERSONAL_INFO",    //1.1.1Thông tin cơ bản
  HR_ACCOUNT = "HR_ACCOUNT",    //1.1.2Thông tin tài khoản
  HR_IDENTITY = "HR_IDENTITY",    //1.1.3Thông tin định danh
  HR_FAMILY_RELATIONSHIP = "HR_FAMILY_RELATIONSHIP",    //1.1.4Thông tin thân nhân
  HR_DEPENDENT_PERSON = "HR_DEPENDENT_PERSON",    //1.1.5Thông tin giảm trừ gia cảnh
  HR_WORK_OUTSIDE = "HR_WORK_OUTSIDE",    //1.1.6Quá trình công tác trước khi vào MB
  HR_WORK_PROCESS = "HR_WORK_PROCESS",    //1.1.7Quá trình công tác
  HR_CONCURRENT_PROCESS = "HR_CONCURRENT_PROCESS",
  HR_PROJECT_MEMBER = "HR_PROJECT_MEMBER",    //1.1.8Quá trình tham gia dự án
  HR_CONTRACT_PROCESS = "HR_CONTRACT_PROCESS",    //1.1.9Quá trình hợp đồng
  HR_SALARY_PROCESS = "HR_SALARY_PROCESS",    //1.1.10Quá trình hệ số lương
  HR_ALLOWANCE_PROCESS = "HR_ALLOWANCE_PROCESS",    //1.1.11Quá trình phụ cấp
  HR_EDU_DEGREE = "HR_EDU_DEGREE",    //1.1.12Thông tin bằng cấp/chứng chỉ
  HR_EDU_PROCESS = "HR_EDU_PROCESS",    //1.1.13Thông tin quá trình đào tạo
  HR_REWARD = "HR_REWARD",    //1.1.14Thông tin khen thưởng
  HR_DECPLINE = "HR_DECPLINE",    //1.1.15Thông tin kỷ luật
  HR_PROFILE = "HR_PROFILE",    //1.1.16 FILE đính
  HR_MANAGER_PROCESS = "HR_MANAGER_PROCESS",    //1.1.17 Thông tin người quản ký
  HR_MODIFICATION = "HR_MODIFICATION",    //1.2Hiệu chỉnh thông tin
  HR_MODIFY_IDENTITY = "HR_MODIFY_IDENTITY",    //1.2.1Hiệu chỉnh Thông tin định danh
  HR_MODIFY_ACCOUNT = "HR_MODIFY_ACCOUNT",    //1.2.2Hiệu chỉnh Thông tin tài khoản
  HR_MODIFY_DEPENDENT = "HR_MODIFY_DEPENDENT",    //1.2.3Hiệu chỉnh Thông tin giảm trừ gia cảnh
  HR_MODIFY_WORK_PROCESS = "HR_MODIFY_WORK_PROCESS",    //1.2.4Hiệu chỉnh Thông tin công tác
  HR_MODIFY_PROJECT_MEMBER = "HR_MODIFY_PROJECT_MEMBER",    //1.2.5Hiệu chỉnh Thông tin tham gia dự án
  HR_MODIFY_CONTRACT_PROCESS = "HR_MODIFY_CONTRACT_PROCESS",    //1.2.6Hiệu chỉnh Thông tin hợp đồng
  HR_MODIFY_MANAGER = "HR_MODIFY_MANAGER",    //1.2.7Hiệu chỉnh Thông tin người quản lý trực tiếp
  HR_MODIFY_SALARY_PROCESS = "HR_MODIFY_SALARY_PROCESS",    //1.2.8Hiệu chỉnh Thông tin diễn biến lương
  HR_MODIFY_ALLOWANCE_PROCESS = "HR_MODIFY_ALLOWANCE_PROCESS",    //1.2.9Hiệu chỉnh Thông tin phụ cấp
  HR_MODIFY_DEGREE = "HR_MODIFY_DEGREE",    //1.2.10Hiệu chỉnh Thông tin bằng cấp/chứng chỉ
  HR_MODIFY_EDU_PROCESS = "HR_MODIFY_EDU_PROCESS",    //1.2.11Hiệu chỉnh Thông tin quá trình đào tạo
  HR_MODIFY_REWARD = "HR_MODIFY_REWARD",    //1.2.12Hiệu chỉnh Thông tin khen thưởng
  HR_MODIFY_DECIPLINE = "HR_MODIFY_DECIPLINE",    //1.2.13Hiệu chỉnh Thông tin kỷ luật
  HR_DRAFT_CONCURRENT_PROCESS = "HR_DRAFT_CONCURRENT_PROCESS", //1.2.14Hiệu chỉnh Thông tin quá trình kiêm nhiệm
  EMPLOYEE_TARGET_LIST = 'E-01-001',
  ASSIGN_TARGET_TO_ALL_EMPLOYEES = 'E-01-001-03',
  ORGANIZATION_MANGEMENT = 'HCM-MPL-01-001',
  ORGANIZATION_ORG_TEMPLATE = 'HCM-MPL-08-005',
  ORGANIZATION_ORG_TEMPLATE_INIT = 'HCM-MPL-08-006',
  ORGANIZATION_MANGEMENT_INIT = 'HCM-MPL-01-002',
  ORGANIZATION_INFO = 'HCM-MPL-01-007',
  ORGANIZATION_LIST_INFO = 'HCM-MPL-01-006',
  ADMIN_APP = 'DANH-MUC-UNG-DUNG',
  ADMIN_FULL_TEXT_SEARCH = 'FULL_TEXT_SEARCH',
  ADMIN_SYSTEM = 'THAM-SO-HE-THONG',
  ADMIN_SESSIONS = 'QUAN-LY-PHIEN-LAM-VIEC',
  ADMIN_SESSIONS_HISTORY = 'LICH-SU-PHIEN-LAM-VIEC',
  ADMIN_COMMON = 'ADMIN_COMMON',
  ADMIN_BRANCH = 'ADMIN_BRANCH',
  ADMIN_BLOCK = 'ADMIN_BLOCK',
  ADMIN_FUNCTION = 'DANH-MUC-CHUC-NANG',
  ADMIN_SCOPES = 'DANH-MUC-THAO-TAC',
  ADMIN_USER = 'QUAN-LY-NGUOI-DUNG',
  ADMIN_ROLE = 'DANH-MUC-VAI-TRO',
  ADMIN_USER_ROLE = 'GAN_VAI_TRO_CHO_NGUOI_DUNG',
  ADMIN_PERMISSION = 'QUAN-LY-QUYEN',
  ADMIN_USER_PERMISSION = 'QUAN-LY-DATA-PHAN-QUYEN',
  ADMIN_LOG = 'ADMIN_LOG',
  ADMIN_DATA_DOMAIN = 'ADMIN_DATA_DOMAIN',
  ADMIN_PERMISSION_ROLE = 'GAN_QUYEN_CHO_VAI_TRO',
  ADMIN_USER_PROFILE = 'USER_PROFILE',
  ADMIN_SESSION_MANAGEMENT = 'QUAN_LY_PHIEN',
  GAN_TU_DONG_VAI_TRO = 'GAN-TU-DONG-VAI-TRO',
  ADMIN_SEARCH_USER_LOG = 'SEARCH-USER-LOG',
  ADMIN_SEARCH_PERMISSION = 'TIM-KIEM-NGUOI-DUNG-THONG-TIN-NGUOI-DUNG',
  ADMIN_TEMPLATE_MANAGEMENT = 'DANH_MUC_BIEU_MAU',
  DEVELOPMENT_CAP_BAN_HANH = 'CAP_BAN_HANH',
  DEVELOPMENT_APPROVE_CONFIG = 'CAU_HINH_KY_DUYET_LD',
  DEVELOPMENT_FORM_LD = 'HINH_THUC_LD',
  DEVELOPMENT_PROPOSE = 'HCM-LND-02',
  DEVELOPMENT_PROPOSE_LIST = 'HCM-LND-04-002',
  DEVELOPMENT_APPROVE_PROPOSE = 'HCM-LND-03',
  DEVELOPMENT_APPROVEMENT_PROPOSE = 'HCM-LND-04-006',
  DEVELOPMENT_APPROVE_STAFF = 'HCM-MPL-07-004',
  ORGANIZATION_APPROVE_ORG = 'HCM-MPL-01-005',
  ORGANIZATION_REGION_CATEGORY = 'HCM-MPL-08-002',
  ORGANIZATION_BRANCH_CATEGORY = 'HCM-MPL-08-001',
  ORGANIZATION_SUB_REGION_CATEGORY = 'HCM-MPL-08-003',
  ORGANIZATION_CATEGORY = 'HCM-MPL-08-011',
  ORGANIZATION_ISSUED_LIST = "HCM-MPL-06-001",
  ORGANIZATION_ISSUED_TRANFER ='HCM-MPL-06-002',
  GIVE_CRITERIA = "E-01-001",
  PERSONAL_PLAN_TARGET = "E-01-002-01",
  TARGET_CATEGORY = "D-01-001",
  ORGANIZATION_ADJUSTED_ORG = 'HCM-MPL-01-003',
  ORGANIZATION_CORRECTION_ORG = 'HCM-MPL-01-004',
  ORGANIZATION_STAFF_PLAN = "HCM-MPL-07-001", // danh mục kế hoạch nhân sự năm
  ORGANIZATION_STAFF_PLAN_APPROVE = "HCM-MPL-07-006",
  ORGANIZATION_STAFF_PLAN_INIT = "HCM-MPL-07-002",
  ORGANIZATION_STAFF_PLAN_ADJUST = "HCM-MPL-07-003",
  ORGANIZATION_POSITION_LIST = 'HCM-MPL-04-004',
  ORGANIZATION_POSITION_GROUP = 'HCM-MPL-08-009',
  ORGANIZATION_POSITION_ASSIGN = 'HCM-MPL-08-010',
  ORGANIZATION_POSITION_UNASSIGN = 'HCM-MPL-04-005',
  ORGANIZATION_STAFF_PLAN_CORRECTION = "HCM-MPL-07-004",
  ORGANIZATION_SORT_TREE = "HCM-MPL-01-008",
  ORGANIZATION_JD_INIT = 'HCM-MPL-05-002',
  ORGANIZATION_JD_APPROVE = 'HCM-MPL-05-004',
  // EVALUATE_EMPLOYEE_RESULTS = 'E-01-003-01',
  EVALUATE_EMPLOYEE_RESULTS = 'E-01-003-01',
  EVALUATE_EMPLOYEE_RESULT = 'E-01-003-03',
  DEPLOY_JOB = 'HCM-MPL-04-001',
  ASSIGN_JOB_FOR_UNIT = 'HCM-MPL-04-002',
  LIST_JOB = 'HCM-MPL-08-008', // Danh mục Job (Mô hình kế hoạch)
  APPROVE_ASSIGN_JOB = 'HCM-MPL-04-003', // Phê duyệt triển khai Job (Mô hình kế hoạch)
  LIST_LINE = 'HCM-MPL-03-005', // Danh sách line (Mô hình kế hoạch)
  MANAGE_LINE = 'HCM-MPL-03-001', // Quản lý line (Mô hình kế hoạch)
  INIT_LINE = 'HCM-MPL-03-002', // Thêm mới, edit line (Mô hình kế hoạch)
  APPROVE_LINE = 'HCM-MPL-03-003', // Phê duyệt line (Mô hình kế hoạch)
  APPROVED_STAFF = 'HCM-MPL-07-006', // Phê kế hoạch nhân sự (Mô hình kế hoạch)
  INIT_STAFF_PLAN = 'HCM-MPL-07-002', // Khởi tạo kế hoạch năm (Mô hình kế hoạch)
  ADJUSTED_STAFF = 'HCM-MPL-07-003', // Điều chỉnh kế hoạch năm (Mô hình kế hoạch)
  PROJECT_CRITERIA_CATEGORY = 'HCM-MPL-08-007', // Danh mục tiêu chí dự án (Mô hình kế hoạch)
  INIT_PROJECT = 'HCM-MPL-02-002', // Thêm mới, chỉnh sửa dự án (Mô hình kế hoạch)
  MANAGE_PROJECT = 'HCM-MPL-02-001',
  APPROVE_PROJECT = 'HCM-MPL-02-003',
  IMPLEMENT_PROJECT = 'HCM-MPL-02-005',
  APPROVE_PROJECT_DETAIL = 'HCM-MPL-02-004',
  CONFIRM_STAFF = 'HCM-MPL-07-005',
  CORRECTION_STAFF = 'HCM-MPL-07-004', // Hiệu chỉnh kế hoạch năm (Mô hình kế hoạch)
  JOB_DESCRIPTION = 'HCM-MPL-05-001',
  JOB_DESCRIPTION_APPROVE = 'HCM-MPL-05-004',
  ABS_TIMEKEEPINGS = "ABS_TIMEKEEPING", //3. quan ly cham cong
  ABS_ANNUAL_LEAVES = "ABS_NUMBER_OF_LEAVE", //3. quan ly số ngày phép
  PTX_TAX_SEARCH = 'PTX-TAX-SEARCH',
  ALLOWANCE_DEVELOPMENTS = 'HCM-POL-02',
  RECEIVING_EXCEPTION_ALLOWANCE= 'HCM-POL-02-001',
  RECEIVING_EXCEPTION_ALLOWANCE_APPROVE = 'HCM-POL-02-002',
  RECEIVING_EXCEPTION_ALLOWANCE_APPROVE_BACK_DATE = 'HCM-POL-02-005',
  ALLOWANCE_DEVELOPMENTS_LIST = 'HCM-POL-02-003',
  ALLOWANCE_DEVELOPMENTS_APPROVE = 'HCM-POL-02-004',
  ALLOWANCE_DEVELOPMENTS_APPROVE_BD = 'HCM-POL-02-006',
  POL_SALARY_EVOLUTION = 'HCM-POL-01-001',
  POL_SALARY_PROCESS_APPROVE = 'HCM-POL-01-002',
  POL_SALARY_PROCESS_APPROVE_B = 'HCM-POL-01-005',
  POL_SALARY_COMMITMENT = 'HCM-POL-01-003',
  POL_SALARY_COMMITMENT_APPROVE = 'HCM-POL-01-004',
  POL_SALARY_COMMITMENT_APPROVE_B = 'HCM-POL-01-006',
  POL_SALARY_ACHIEVEMENT_BONUS = 'HCM-POL-03-001',
  POL_SALARY_ACHIEVEMENT_BONUS_APPROVED = 'HCM-POL-03-002',
  RATING_CATEGORY = 'D-02-001',
  LIST_EMP_ALLOCATED = 'D-02-005',
  APPROVE_EMP_ALLOCATED = 'D-02-006',
  KPI_COMPLIANCE = 'D-02-007',
  LIST_OF_INDICATOR = 'E-01-005',
  LIST_PERSONNEL_ALLOCATION = 'D-02-008',
  DETAIL_CLASSIFICATION = 'D-02-012',
  CRM_LIST_EMP = 'E-01-006',

  PTX_TAX_REGISTER = 'TAX-REGISTER',
  PTX_TAX_CHANGE = 'TAX-CHANGE',
  PTX_DEPENDENT_REGISTERS = 'DEPENDENT-REGISTER',
  PTX_REPORT_DEPENDENT = 'PTX_REPORT_DEPENDENT',
  PTX_DEPENDENT_DEDUCTION = 'PTX_DEPENDENT_DEDUCTION',
  PTX_DECLARATION_REGISTERS = 'DECLARATION-REGISTER',
  PTX_RECEIVE_INVOICE = 'RECEIVE-INVOICE',
  PTX_CONFIRM_PROVIDE = 'CONFIRM-PROVIDE-INFO',
  PTX_SALARY_ACCOUNT_REQS = 'TAX-DK-MO-TAI-KHOAN-LUONG',

  MANAGER_DIREECT = 'HCM-ADM-06'

}

export const functionUri = {
  access_denied: '/access-denied/',
  access_import: '/access-import/',
  page_not_pound: '/page-not-pound',
  template_init: '/organization/model-organization-template/init-template',
  project_implement_list: '/organization/project/project-implement',
  dashboard: '/dashboard',
  organization_init_screen: '/organization/init/init-screen',
  organization_mangement: '/organization/init/mangement',
  organization_approve_detail: '/organization/init/unit-approve/detail',
  organization_assign_position: '/organization/job/position-group/position-assignment',
  JD_list: '/organization/job-description/job',
  JD_detail: '/organization/job-description/jd-detail/',
  project_assign_staff: '/organization/project/employee-assignment',
  admin_branch: '/system/branches-category',
  admin_block: '/system/blocks-category',
  admin_function: '/system/resources-category',
  admin_scopes: '/system/manipulation-category',
  admin_role: '/system/role-category',
  admin_user_role: '/system/user-role-config',
  admin_log: '/system/log-history',
  admin_permission: '/system/permission-config',
  admin_data_domain: '/system/user-permission-config',
  process_management: '/system/process-management',
  rm_360_manager: '/rm360/rm-manager',
  rm_block: '/rm360/rm-block',
  rm_title: '/rm360/title-category',
  rm_level: '/rm360/level-category',
  rm_group: '/rm360/rm-group',
  customer_360_manager: '/customer360/customer-manager',
  customer_360_operation_block_manager: '/customer360/operation-block',
  customer_360_assignment: '/customer360/customer-assignment',
  customer_360_assignment_approved: '/customer360/customer-assignment-approve',
  customer_360_dashboard: '/customer360/dashboard',
  customer_360_dashboard2: '/customer360/dashboard2',
  rm_fluctuate: '/rm360/rm-fluctuate',
  task: '/tasks',
  calendar: '/calendar',
  lead_management: '/lead/lead-management',
  product: '/products',
  campaign: '/campaigns/campaign-management',
  campaign_rm: '/campaigns/campaign-rm',
  device_rm: 'rm360/device',
  tickets_manager: 'tickets/ticket-management',
  kpi_config: '/kpis/kpi-management/kpi-config/',
  rm_group_arm: '/rm360/arm',
  revenue_share: '/customer360/revenue-sharing',
  propose_list: '/development/propose/propose-list',
  organization_position_category: '/organization/job/position-group',
  organization_jd_list: '/organization/job-description/job-description',
  organization_pos_list_unassign: '/organization/job/list-position-unassigned',
  organization_pos_unassign: '/organization/job/list-position-unassigned/position-unassigned',
  organization_jd_init: '/organization/job-description/jd-init',
  system_manager_direct: '/staff/manager-direct',
  system_manager_direct_assign: '/staff/manager-direct/assign-manager'
};

export enum AgeGroup {
  AGE_LESS_18 = 'AGE_LESS_18',
  AGE_BETWEEN_18_25 = 'AGE_BETWEEN_18_25',
  AGE_BETWEEN_26_35 = 'AGE_BETWEEN_26_35',
  AGE_BETWEEN_36_50 = 'AGE_BETWEEN_36_50',
  AGE_BETWEEN_51_60 = 'AGE_BETWEEN_51_60',
  AGE_BETWEEN_OVER_60 = 'AGE_BETWEEN_OVER_60',
}

export enum RespondCodeError {
  CDRM002 = 'CDRM002',
  CDRM003 = 'CDRM003',
  LVRM001 = 'LVRM001',
  LVRM002 = 'LVRM002',
}

export enum ConfirmType {
  Success = 'success',
  Warning = 'warning',
  Confirm = 'confirm',
  CusError = 'error',
}

export enum PriorityTaskOrCalendar {
  High = 'HIGH',
  Medium = 'MEDIUM',
  Low = 'LOW',
}

export enum RmType {
  RM = 'RM',
  aRM = 'aRM',
}

export enum Format {
  CusDate = 'dd/MM/yyyy',
  DateUp = 'DD/MM/YYYY',
  DateTime = 'HH:mm dd/MM/yyyy',
  DateTimeUp = 'HH:mm DD/MM/YYYY',
}

export enum CustomerType {
  INDIV = 'INDIV',
  NHS = 'NHS',
  COR = 'COR',
  SME = 'SME',
  CIB = 'CIB',
}

export enum ChartJsType {
  Pie = 'pie',
  Bar = 'bar',
  Line = 'line',
  Doughnut = 'doughnut',
}

export enum EChartType {
  Pie = 'pie',
  Bar = 'bar',
  Line = 'line',
  Doughnut = 'doughnut',
}

export enum ScreenType {
  Create = 'CREATE',
  Update = 'UPDATE',
  Detail = 'DETAIL',
}

export enum ServiceProductType {
  Credit = 'Credit',
  Mobilization = 'Mobilization',
  Card = 'Card',
  Digital = 'Digital',
  Group = 'Group',
}

export enum CreditProductGroup {
  Housing = '1_Housing',
  HousingProject = '2_Housing_Project',
  Stock = '3_Stock',
  Car = '4_Car',
  LetterValue = '5_Letter_Value',
  Coll = '6_Consumer_Coll',
  TDKC_TSBD = '7_Consumer_Non_Coll',
  SXKD = '8_Consumer_Bussiness',
  Salary = '9_Salary',
  Overdraft = '10_Overdraff',
  Other = '11_Others',
}

export enum MobilizationChart {
  CasaTD = '100',
  CasaBQ = 'CASA_BQ',
  TKCKHTD = '101',
}

export enum DigitalChart {
  EMB = '141',
  Sms = '142',
  App = '143',
}

export enum ComprodChart {
  MBAL = '171',
  SPDT = '172',
}

export enum CardChart {
  ActivePlus = 'ACTIVEPLUS',
  Debit = 'DEBIT',
  Jcb = 'JCB',
  VisaCredit = 'VISA_CREDIT',
  VisaBank = 'VISA_BANK_PLUS',
  VisaVin = 'VISA_VIN_GRP',
  // VisaVlin = '',
  // VisaViettel = '',
  // ActivePlus = '',
  // Debit = '',
}

export const rolesDefault = ['offline_access', 'uma_authorization'];
export const regexSpecialForLine = /[!@#$%^&*()+\=\[\]{};':"\\|,<>\/?~]/;

export const CommonCategory = {
  CAMPAIGN_TYPE: 'CAMPAIGN_TYPE',
  CAMPAIGN_FORM: 'CAMPAIGN_FORM',
  CONFIG_ACTIVITY_RESULT: 'CONFIG_ACTIVITY_RESULT',
  CONFIG_TYPE_ACTIVITY: 'CONFIG_TYPE_ACTIVITY',
  CONFIG_BACK_DATE: 'CONFIG_BACK_DATE',
  STATUS_CUSTOMER_CONFIG: 'STATUS_CUSTOMER_CONFIG',
  SEGMENT_CUSTOMER_CONFIG: 'SEGMENT_CUSTOMER_CONFIG',
  CONFIG_EXPORT_CUSTOMER_LIMIT_COUNT: 'CONFIG_EXPORT_CUSTOMER_LIMIT_COUNT',
  CUSTOMER_OBJECT_CONFIG: 'CUSTOMER_OBJECT_CONFIG',
  KH_PHONE_NO_CONFIG: 'KH_PHONE_NO_CONFIG',
  KH_EMAIL_CONFIG: 'KH_EMAIL_CONFIG',
  ACCOUNT_GROUP: 'ACCOUNT_GROUP',
  QUICKSEARCH_TYPE: 'QUICKSEARCH_TYPE',
  EMPLOYEE_CHANGE: 'EMPLOYEE_CHANGE',
  KPI_SYS_ORG_TYPE: 'KPI_SYS_ORG_TYPE',
  KPI_LOCATION: 'KPI_LOCATION',
  ACTIVITY_NEXT_TYPE: 'ACTIVITY_NEXT_TYPE',
  ASSET_TYPE: 'ASSET_TYPE',
  ASSET_GROUP: 'ASSET_GROUP',
  AGE_GROUP: 'AGE_GROUP',
  PRIVATE_PRIORITY_CONFIG: 'PRIVATE_PRIORITY_CONFIG',
  EXT_CAMPAIGNS_CHANNELS: 'EXT_CAMPAIGNS_CHANNELS',
  CAMPAIGN_DIVISION: 'CAMPAIGN_DIVISION',
  SOURCE_DATA: 'SOURCE_DATA',
  KPI_YEARS: 'KPI_YEARS',
  SPDV_THE: 'CARDTYPE_SPDV_CONFIG',
  PROCESS_MANAGEMENT_STATUS: 'PROCESS_MANAGEMENT_STATUS',
  APP_MANAGEMENT: 'APP_MANAGEMENT',
  REALM_ROLE_MANAGEMENT: 'REALM_ROLE_MANAGEMENT',
  EXPORT_CUSTOMER: 'EXPORT_CUSTOMER',
  CURRENCY_UNIT_REPORTS: 'CURRENCY_UNIT_REPORTS',
  REPORTS_YEARS: 'REPORTS_YEARS',
  BAOCAO_PL_RM_ROLEADMIN: 'BAOCAO_PL_RM_ROLEADMIN',
  TERM_TYPE: 'TERM_TYPE ',
  CURRENCY_TYPE: 'CURRENCY_TYPE',
  REPORTS_MAX_LENGTH_RM: 'REPORTS_MAX_LENGTH_RM',
  DASHBOARD_LIST: 'DASHBOARD_LIST',
  SALEKIT_APPROVE_STATUS: 'SALEKIT_APPROVE_STATUS',
  KHOI_PRIORITY: 'KHOI_PRIORITY',
  //f-marketing
  SMART_CHAT_MARKET_TYPE: 'SMART_CHAT_MARKET_TYPE',
  SMART_CHAT_MARKET_STATUS: 'SMART_CHAT_MARKET_STATUS',
  SMART_CHAT_MARKET_CATEGORY: 'SMART_CHAT_MARKET_CATEGORY',
  SMART_CHAT_MARKET_CUSTOMER_CATEGORY: 'SMART_CHAT_MARKET_CUSTOMER_CATEGORY',
  //f-marketing
  SHARE_REVENUE_LINE: 'SHARE_REVENUE_LINE',
  SHARE_ITEM: 'SHARE_ITEM',
  ACC_REVENUE_SHARING_ST: 'ACC_REVENUE_SHARING_ST',
  SHARE_REVENUE_ROLE_INPUT: 'SHARE_REVENUE_ROLE_INPUT',
  MAX_RM_REPORT_CREDIT: 'MAX_RM',
  MAX_RM_REPORT_CUSTOMER: 'BAOCAO_PTKH',
  SHARE_REVENUE_ROLE_APPROVE_EMAIL: 'SHARE_REVENUE_ROLE_APPROVE_EMAIL',
  MAX_CUSTOMER_REPORT: 'MAX_CUSTOMER',
  LEAD_STATUS: 'LEAD_STATUS',
  LEAD_LEVEL: 'LEAD_LEVEL',
  LEAD_FILTER: 'LEAD_FILTER',
  LEAD_SOURCE: 'LEAD_SOURCE',
  LEAD_TYPE: 'LEAD_TYPE',
  LEAD_ORG_TYPE: 'LEAD_ORG_TYPE',
};

export const ConfigBackDate = {
  BACK_DATE_ACTIVITY: 'BACK_DATE_ACTIVITY',
};

export const TimeShowNotify = 10000;

export const LeadProsessName = 'mb_crm_lead_process';

export enum ProductLevel {
  Lvl1 = '1',
  Lvl2 = '2',
  Lvl3 = '3',
  Lvl4 = '4',
  Lvl5 = '5',
}

export enum CampaignProcessType {
  HO = 'HO',
  MB247 = 'MB247',
  MANAGER = 'MANAGER',
  RM = 'RM',
}

export const BRANCH_HO = 'VN0010001';

export enum ConstantEnum {
  LIST_FUNCTION = 'LIST_FUNCTION',
  LIST_FUNCTION_USER = 'LIST_FUNCTION_USER',
}

export enum CampaignSize {
  TOAN_HANG = 'TOAN_HANG',
  CHI_NHANH = 'CHI_NHANH',
}

export enum KyRM {
  MONTH = '0',
  DAY = '1',
}

export enum CustomerImportType {
  KH360 = 'KH360',
  TN = 'TN',
}

export enum SessionKey {
  SYNC_USER = 'SYNC_USER',
  SYNC_PERMISSION = 'SYNC_PERMISSION',
  SYNC_USER_ROLE = 'SYNC_USER_ROLE',
  SYNC_AUTOMATIC = 'SYNC_AUTOMATIC',
  CURRENCY_USER = 'CURRENCY_USER',
  CUSTOMER_TARGETS = 'CUSTOMER_TARGETS',
  DIVISION_OF_RM = 'DIVISION_OF_RM',
  USER_INFO = 'USER_INFO',
  TOKEN = 'TOKEN',
  PRODUCT_DETAIL_CUSTOMER = 'PRODUCT_DETAIL_CUSTOMER',
  CURRENT_LINK_ACTIVE = 'CURRENT_LINK_ACTIVE',
  MENU = 'MENU',
  MENU_ALL = 'MENU_ALL',
  SESSION_STATE = 'SESSION_STATE',
  FUNCTION_CODE = 'FUNCTION_CODE',
  IS_PERMISSION = 'IS_PERMISSION',
  LIMIT_EXPORT = 'LIMIT_EXPORT',
  COMMON_DATA_CUSTOMER_DETAILS = 'COMMON_DATA_CUSTOMER_DETAILS',
  PL_REPORTS = 'PL_REPORTS',
  TOI_REPORTS = 'TOI_REPORTS',
  TIME_REFRESH_TOKEN = 'TIME_REFRESH_TOKEN',
  REPORT_MOBILIZATION = 'REPORT_MOBILIZATION',
  REPORT_CREDIT = 'REPORT_CREDIT',
  REPORT_PT_KH = 'REPORT_PT_KH',
  LIST_CUSTOMER_OF_RM = 'LIST_CUSTOMER_OF_RM',
  SHARE_REVENUE_ROLE_INPUT = 'SHARE_REVENUE_ROLE_INPUT',
  LIST_INDUSTRY = 'LIST_INDUSTRY',
  KHOI_PRIORITY = 'KHOI_PRIORITY',
  URL_PROPOSE_ELEMENT = 'URL_PROPOSE_ELEMENT',
}

export enum ExportKey {
  Customer = 'EX_CUSTOMER',
}

export enum ComponentType {
  SinglePage,
  Modal,
}

export enum CodeApp {
  MOBILE = 'APP',
}

export enum RequestMethod {
  Get = 'GET',
  Post = 'POST',
  Put = 'PUT',
  Delete = 'DELETE',
}

export enum NotifyType {
  Success = 'success',
  CusError = 'error',
  Info = 'info',
  Warning = 'warn',
}

export enum Division {
  INDIV = 'INDIV',
  CIB = 'CIB',
  DVC = 'DVC',
  SME = 'SME',
  FI = 'FI',
}

export enum SaleKit {
  SALE_KIT = 'SALE_KIT',
}

export enum TypeFieldProduct {
  STRING = 'STRING',
  CURRENCY = 'CURRENCY',
  DATE = 'DATE',
}
export const types = [
  { id: 'KT', name: 'Tạo mới' },
  { id: 'DC', name: 'Điều chỉnh' },
  { id: 'HC', name: 'Hiệu chỉnh' },
];

export const typesObject = {
  KT:'KT',
  DC:'DC',
  HC:'HC'
};

export const listStatus = [
  { id: 7, value: 'Chưa tạo' },
  { id: 5, value: 'Tạo mới' },
  { id: 2, value: 'Chờ duyệt' },
  { id: 3, value: 'Từ chối' },
  { id: 1, value: 'Đang hoạt động' },
  { id: 0, value: 'Dừng hoạt động' },
  { id: 6, value: 'Xác nhận' },
];

export const listStatusPosition = [
  { id: 7, value: 'Chưa tạo' },
  { id: 5, value: 'Tạo mới' },
  { id: 2, value: 'Chờ duyệt' },
  { id: 3, value: 'Từ chối' },
  { id: 4, value: 'Phê duyệt' },
  { id: 1, value: 'Đang hoạt động' },
  { id: 0, value: 'Không hoạt động' },
  { id: 6, value: 'Xác nhận' },
];

export const listStatusJob = [
  { id: 1, value: 'Sử dụng' },
  { id: 0, value: 'Không sử dụng' },
];
export const typePosition = [
  { id: 'POSITION', value: 'Position' },
  { id: 'POSITION DETAIL', value: 'Position details' },
];

export const orgStatus = {
  CT : 6,
  TM : 5,
  CD : 2,
  TC : 3,
  HD : 1,
  DHD : 0
}

export const orgGroup = [
  { id: 'HO', name: 'HO' },
  { id: 'CN', name: 'CN' },
];

export const typeRule = {
  noSub: 0,
  noSimilarSub: 1,
  similarSub: 2
}
export const targetType = {
  left: 'MY02',
  right: 'MY01'
}
export const scoringRule = {
  rate: 'SR01',
  passOrNot: 'SR02',
  combine: 'SR03'
}
