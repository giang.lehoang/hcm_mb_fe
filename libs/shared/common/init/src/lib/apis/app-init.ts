import { HttpClient, HttpResponse } from '@angular/common/http';
import { KeycloakEventType, KeycloakService } from 'keycloak-angular';
import { environment } from '@hcm-mfe/shared/environment';
import { StorageService } from '@hcm-mfe/shared/common/store';
import {STORAGE_NAME} from '@hcm-mfe/shared/common/constants';
import {Router} from "@angular/router";
import {functionUri} from "@hcm-mfe/shared/common/enums";
import {detectBrowserVersion} from "@hcm-mfe/shared/common/utils";

export function initializer(keycloak: KeycloakService, http: HttpClient, router: Router): () => Promise<any> {
  return (): Promise<any> => {
    return new Promise(async (resolve, reject) => {
      try {
        const response: HttpResponse<any> = await http.get(environment.url.urlCheckEVN, {observe: 'response'}).toPromise();
        StorageService.set(STORAGE_NAME.APP, response.headers.get('appId'));
        if (StorageService.get(STORAGE_NAME.APP) === environment.app.HCM_WEB) {
          keycloak.keycloakEvents$.subscribe(event => {
            if(event.type === KeycloakEventType.OnAuthSuccess) {
              keycloak.loadUserProfile().then(profile => {
                if (localStorage.getItem(STORAGE_NAME.USER_NAME) !== profile.username) {
                  localStorage.setItem(STORAGE_NAME.USER_NAME, profile.username);
                  router.navigateByUrl('/');
                }
              })
              if(window.location.href.includes(functionUri.access_denied)) {
                router.navigateByUrl('/');
              }
              const data = {
                deviceName: detectBrowserVersion()
              }
              http.post(`${environment.adminUrl}sessions-history`, data).subscribe(() => {
                console.log('Insert log');
              });
            }
          });
          await keycloak.init({
            config: {
              url: environment.keycloak.issuer,
              realm: environment.keycloak.realm,
              clientId: environment.keycloak.client,
            },
            loadUserProfileAtStartUp: true,
            initOptions: {
              onLoad: 'login-required',
              checkLoginIframe: false,
              // checkLoginIframeInterval: 5,
            },
            bearerPrefix: 'Bearer',
          });
        }
        resolve(true);
      } catch (error) {
        reject(error);
      }
    });
  };
}
