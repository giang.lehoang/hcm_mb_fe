import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { KeycloakEventType, KeycloakService } from 'keycloak-angular';
import { Observable, tap, throwError} from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { NzI18nService } from 'ng-zorro-antd/i18n';
import { v4 as uuidv4 } from 'uuid';
import { STORAGE_NAME, url } from '@hcm-mfe/shared/common/constants';
import { functionUri, SessionKey } from '@hcm-mfe/shared/common/enums';
import { UserService, WebSocketService } from '@hcm-mfe/shared/core';
import { SessionService, StorageService } from '@hcm-mfe/shared/common/store';
import { environment } from '@hcm-mfe/shared/environment';
import {SpinnerService} from "@hcm-mfe/shared/common/base-service";
const TOKEN_HEADER_KEY = 'Authorization';

@Injectable()
export class CustomHttpIntercepter implements HttpInterceptor {
  constructor(
    private readonly keyCloakService: KeycloakService,
    private readonly router: Router,
    private readonly i18n: NzI18nService,
    private readonly sessionService: SessionService,
    private readonly userService: UserService,
    private readonly webSocketService: WebSocketService,
    private spinnerService: SpinnerService
  ) {
    this.keyCloakService?.keycloakEvents$.subscribe((res) => {
      if (res?.type === KeycloakEventType.OnAuthRefreshSuccess) {
        this.connectWebSocket();
      }
      if (res?.type === KeycloakEventType.OnAuthRefreshError) {
        this.userService.logout();
      } else if (res?.type === KeycloakEventType.OnTokenExpired) {
        this.keyCloakService?.updateToken();
      }
    });
  }

  connectWebSocket() {
    this.keyCloakService?.getToken().then(token => {
      this.webSocketService.connect(token);
    })
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!req.url.includes(url.url_endpoint_env) && StorageService.get(STORAGE_NAME.APP) === environment.app.HCM_WEB) {
      if (this.keyCloakService.isTokenExpired()) {
        this.keyCloakService.updateToken();
      }
    }
    const urlCurrent = req.url.toLowerCase();
    const isAppSSV: boolean = StorageService.get(STORAGE_NAME.APP) === environment.app.HCM_ESS_WEB;
    if(isAppSSV) {
      const accessToken = StorageService.get(STORAGE_NAME.ACCESS_TOKEN);
      if (accessToken && !urlCurrent.includes('login') && !urlCurrent.includes('refresh-token') && !urlCurrent.includes(environment.url.urlCheckEVN)) {
        req = this.addTokenHeader(req, accessToken);
      }
    } else if (urlCurrent.includes(environment.url.urlCheckEVN)) {
        req = this.clearTokenHeader(req);
    }

    req = req.clone({ headers: req.headers.set('Accept-Language', this.i18n.getLocale().locale) });
    req = req.clone({ headers: req.headers.set('clientMessageId', uuidv4()) });
    let img = null;
    this.spinnerService.requestStart();
    return next.handle(req).pipe(
      tap(res => {
        if (res instanceof HttpResponse) {
          this.spinnerService.requestEnd();
        }
      }),
      catchError((error: HttpErrorResponse) => {
        this.spinnerService.requestEnd();
        if (error.status === 403) {
          if(error.url.includes(url.url_endpoint_get_count_notify)) {
            this.sessionService.setSessionData(SessionKey.IS_PERMISSION, false);
          }
          const role = !isAppSSV ? this.keyCloakService?.isUserInRole('INIT-KEYCLOAK', 'hcm-service') : null;
          if(role) {
            this.router.navigateByUrl(functionUri.access_import);
          } else {
            if (!img) {
              img = new Image();
              img.onload = () => {
                this.router.navigateByUrl(functionUri.access_denied);
              };
              img.src = 'assets/images/access-denied.svg';
            }
          }
        }
        if (error.status === 401 && !error.url.includes(url.url_endpoint_login)) {
          this.userService.logout();
        }
        return throwError(error);
      })
    );
  }

  private addTokenHeader(request: HttpRequest<any>, accessToken: string) {
    /* for Spring Boot back-end */
    return request.clone({ headers: request.headers.set(TOKEN_HEADER_KEY, 'Bearer ' + accessToken) });
  }

  private clearTokenHeader(request: HttpRequest<any>) {
    /* for Spring Boot back-end */
    return request.clone({ headers: request.headers.delete(TOKEN_HEADER_KEY) });
  }
}
