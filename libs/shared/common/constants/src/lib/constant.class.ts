export class Constant {
  public static readonly EXCEL_TYPE_1 = 'xls';
  public static readonly EXCEL_MIME_1 = 'application/vnd.ms-excel';

  public static readonly EXCEL_TYPE_2 = 'xlsx';
  public static readonly EXCEL_MIME_2 = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';

  public static readonly PDF_TYPE = 'pdf';
  public static readonly PDF_MIME = 'application/pdf';

  public static readonly CATALOGS = {
    GIOI_TINH: 'GIOI_TINH',
    DAN_TOC: 'DAN_TOC',
    TON_GIAO: 'TON_GIAO',
    TINH_TRANG_HON_NHAN: 'TINH_TRANG_HON_NHAN',
    TINH: 'TINH',
    HUYEN: 'HUYEN',
    XA: 'XA',
    LOAI_TAI_KHOAN: 'LOAI_TAI_KHOAN',
    LOAI_GIAY_TO: 'LOAI_GIAY_TO',
    MA_VUNG_DIEN_THOAI: 'MA_VUNG_DIEN_THOAI',
    QUOC_GIA: 'QUOC_GIA',
    DOI_TUONG_CV: 'DOI_TUONG_CV',
    CAP_BAC_QUAN_HAM: 'CAP_BAC_QUAN_HAM',
    LEVEL_NV: 'LEVEL_NV',
    PHAN_LOAI_HOP_DONG: 'PHAN_LOAI_HOP_DONG',
    LOAI_HINH_DT: 'LOAI_HINH_DT',
    LOAI_BANG_CAP: 'LOAI_BANG_CAP',
    HE_DT: 'HE_DT',
    HINHTHUC_DAOTAO: 'HINHTHUC_DAOTAO',
    XEP_LOAI_DT: 'XEP_LOAI_DT',
    XEP_LOAI_TOT_NGHIEP: 'XEP_LOAI_TOT_NGHIEP',
    MOI_QUAN_HE_NT: 'MOI_QUAN_HE_NT',
    TINH_TRANG_NT: 'TINH_TRANG_NT',
    DOITUONG_CHINHSACH: 'DOITUONG_CHINHSACH',
    CAP_QD_KHENTHUONG: 'CAP_QD_KHENTHUONG',
    HINHTHUC_KHENTHUONG: 'HINHTHUC_KHENTHUONG',
    LOAI_KHENTHUONG: 'LOAI_KHENTHUONG',
    HINHTHUC_KYLUAT: 'HINHTHUC_KYLUAT',
    LOAI_HO_SO: 'LOAI_HO_SO',
    LOAI_PHU_CAP: 'LOAI_PHU_CAP',
    CAP_QD_KYLUAT: 'CAP_QD_KYLUAT',
    LOAI_HINH_CHI_NHANH: 'LOAI_HINH',
    KHU_VUC: 'KHU_VUC'
  }

}
