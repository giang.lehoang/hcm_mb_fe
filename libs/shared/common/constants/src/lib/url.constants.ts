export const STATUS = {
  ACTIVE: 1,
  INACTIVE: 0,
};

export enum Mode {
  ADD,
  EDIT,
  View,
}

export const TABLE_CONFIG_DEFAULT = {
  pageSize: 15
}

export class FileConstant {
  // EXCEL
  public static readonly EXCEL_TYPE_1 = 'xls';
  public static readonly EXCEL_MIME_1 = 'application/vnd.ms-excel';

  public static readonly EXCEL_TYPE_2 = 'xlsx';
  public static readonly EXCEL_MIME_2 = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
  public static readonly REFRESH_TOTP_SUCCESS = 'Tạo lại thành công TOTP';

// PDF
  public static readonly PDF_TYPE = 'pdf';
  public static readonly PDF_MIME = 'application/pdf';

  // WORD
  public static readonly DOC_TYPE = 'doc';
  public static readonly DOC_MIME = 'application/msword';

  public static readonly DOCX_TYPE = 'docx';
  public static readonly DOCX_MIME = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';

}

