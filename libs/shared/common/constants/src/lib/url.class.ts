export class UrlConstant {
  public static readonly API_VERSION = '/v1.0';

  public static readonly LOGIN = '/login';

  public static readonly LOGOUT = '/logout';

  public static readonly REFRESH_TOKEN = '/refresh-token';

  public static readonly BANKS = '/cat-banks';

  public static readonly SCHOOLS = '/schools';

  public static readonly FACULTIES = '/faculties';

  public static readonly MAJOR_LEVELS = '/major_levels';

  public static readonly API_PERSON ={
    PERSON_INFO: 'hcm-person-info',
  }

  public static readonly ORG_INFO = {
    PREFIX: '/org-tree',
    SEARCH_NODE: '/search-node',
    GET_BY_PARENT: '/get-by-parent/{parentId}',
    GET_BY_LEVEL: '/get-org-by-level'
  }

  public static readonly INFO_CHANGE = {
    GET_INFO_CHANGE: '/employees/{employeeId}/info-changes',
  }

  public static readonly CATALOGS = {
    PREFIX: '/lookup-values',
    CONTRACT_TYPES: '/contract-types',
    JOBS: '/mp-jobs',
    PROJECTS: '/mp-projects',
    SALARY_GRADES: '/salary-grades',
    SALARY_RANKS: '/salary-ranks',
    DOCUMENT_TYPES: '/document-types',
    MB_POSITIONS: '/mp-positions/org/{orgId}'
  }

  public static readonly EMPLOYEES = {
    PREFIX: '/employees',
    DATA_PICKER: '/data-picker',
    PERSONAL_INFO: '/personal-information',
    IDENTITIES: '/identities',
    CONTACT: '/contact-info',
    BANK: '/bank-accounts',
    PARTY: '/party-army',
    AVATAR: '/avatar',
  }

  public static readonly PERSONAL = {
    PREFIX: '/personal',
    DATA_PICKER: '/data-picker',
    PERSONAL_INFO: '/personal-information',
    IDENTITIES: '/identities',
    CONTACT: '/contact-info',
    BANK: '/bank-accounts',
    PARTY: '/party-army',
    AVATAR: '/avatar',
  }

  public static DASHBOARD = {
    BANNER: '/banner/active',
    ARTICLE: '/news/hight-light'
  }

  public static readonly API_VERSION_V1 = '/v1.0';

  public static readonly ORGANIZATION_TREE = {
    PREFIX: '/org-tree',
    ROOT_NODE: '/root-node',
    NODE_BY_PARENT_NODE: '/lazy-load-node',
    SEARCH_BY_PARENT_NODE: '/get-by-parent/',
    SEARCH_BY: '/search-node'
  }

  public static readonly WORK_EARLY = {
    PREFIX: '/timekeeping-management/work-early',
    SEARCH: '/search',
  }

  public static readonly CALENDAR = {
    PREFIX: '/time-keepings/personal'
  }

  public static readonly END_POINT_MODEL_PLAN = {
    urlEndPointOrgTree: `hcm-model-plan/organization-tree`,
    urlLookupValues: `hcm-model-plan/v1.0/lookup-values`,
    urlEndPointOrgTreeId: `hcm-model-plan/v1.0/organization-tree`,
    urlEndpointJob:`hcm-model-plan/v1.0/jobs/type`,
    urlEnpointLookupValues:`hcm-model-plan/v1.0/lookup-values`,
    url_enpoint_organization_tree_get_by_id: 'hcm-model-plan/v1.0/organization-tree/findTreeById',
    url_enpoint_organization_tree_get_by_name: 'hcm-model-plan/organization-tree/searchTreeByName',
    SUB_TREE_ACTIVE: '/v1.0/organization-tree/sub-branch/list-active',
    SUB_TREE_INACTIVE: '/v1.0/organizations/sub-branch/list-active-and-inactive',
    GET_ORG_BY_LEVEL:'hcm-model-plan/organization-tree/getOrgByLevel',
    GET_ORG_POPUP:'hcm-model-plan/organization-tree/searchTreeOrg'
  }
}
