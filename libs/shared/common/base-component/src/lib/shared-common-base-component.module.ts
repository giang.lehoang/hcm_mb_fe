import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseComponent } from './base.component';
import { CustomToastrService, SharedCoreModule } from '@hcm-mfe/shared/core';

@NgModule({
  imports: [CommonModule, SharedCoreModule],
  declarations: [BaseComponent],
  exports: [BaseComponent],
  providers: [CustomToastrService]
})
export class SharedCommonBaseComponentModule {}
