import { Location } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import {
  ChangeDetectorRef,
  Component,
  HostBinding,
  HostListener,
  Injector,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  defaultExportExcel,
  ScreenType,
  SessionKey,
} from '@hcm-mfe/shared/common/enums';
import { SessionService } from '@hcm-mfe/shared/common/store';
import {
  CommunicateService,
  ConfirmService,
  CustomToastrService,
} from '@hcm-mfe/shared/core';
import { AppFunction } from '@hcm-mfe/shared/data-access/models';
import { PopupService } from '@hcm-mfe/shared/ui/popup';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { Subscription } from 'rxjs';
@Component({
  template: `<ng-content></ng-content>`,
})
export class BaseComponent implements OnDestroy {
  public prop: any;
  public fields: any;
  public labelTitle: any;
  public notificationMessage: any;
  public confirmMessage: any;
  public screenType = {
    create: ScreenType.Create,
    update: ScreenType.Update,
    detail: ScreenType.Detail,
  };
  public objFunction: AppFunction;
  public maxExportExcel = defaultExportExcel;
  public isLoading = false;
  public currUser: any;
  public isDownLoading = false;
  public readonly SCROLL_CONFIG = '1100px';
  public readonly WIDTH_COLUMN = {
    CHECKED: '80px',
    SMALL: '100px',
    MEDIUM: '200px',
    LARGE: '300px',
    VERY_LARGE: '500px',
  };
  public readonly TABLE_SIZE = 'small';

  protected configModal: NgbModalConfig;
  protected modalService: NgbModal;
  protected fb: FormBuilder;
  protected router: Router;
  protected route: ActivatedRoute;
  protected location: Location;
  protected ref: ChangeDetectorRef;
  protected message: NzMessageService;
  protected modal: NzModalService;
  protected deletePopup: PopupService;
  protected state: any;
  protected translate: TranslateService;
  protected sessionService: SessionService;
  protected confirmService: ConfirmService;
  protected communicateService: CommunicateService;
  protected toastrCustom: CustomToastrService;

  subscription: Subscription;
  subscriptions: Subscription[] = [];
  @ViewChild('table') table: DatatableComponent;

  @HostBinding('class.app__right-content') appRightContent = true;

  constructor(private readonly injector: Injector) {
    this.init();
    this.configModal.backdrop = 'static';
    this.configModal.keyboard = false;
    this.state = this.router.getCurrentNavigation()?.extras?.state;
    this.currUser = this.sessionService.getSessionData(SessionKey.USER_INFO);
    this.subscriptions.push(
      this.communicateService.request$.subscribe((req) => {
        if (req?.name === 'export-completed' && req?.key) {
          this.isDownLoading = false;
          this.sessionService.setSessionData(req?.key, false);
        }
      })
    );
  }

  init() {
    this.configModal = this.injector.get(NgbModalConfig);
    this.modalService = this.injector.get(NgbModal);
    this.fb = this.injector.get(FormBuilder);
    this.router = this.injector.get(Router);
    this.route = this.injector.get(ActivatedRoute);
    this.location = this.injector.get(Location);
    this.ref = this.injector.get(ChangeDetectorRef);
    this.message = this.injector.get(NzMessageService);
    this.modal = this.injector.get(NzModalService);
    this.deletePopup = this.injector.get(PopupService);
    this.translate = this.injector.get(TranslateService);
    this.sessionService = this.injector.get(SessionService);
    this.confirmService = this.injector.get(ConfirmService);
    this.communicateService = this.injector.get(CommunicateService);
    this.toastrCustom = this.injector.get(CustomToastrService);
  }

  @HostListener('window:keydown.enter', ['$event'])
  handleKeyDown(event: KeyboardEvent) {
    this.triggerSearchEvent();
  }

  triggerSearchEvent() {}

  back() {
    this.location.back();
  }

  getMessageError(errorResponse: HttpErrorResponse) {
    if (errorResponse?.error?.message) {
      this.message.error(errorResponse?.error?.message);
    } else {
      this.message.error(this.translate.instant('shared.error.errorCode500'));
    }
  }

  handleDestroy() {
    // override thay cho ngOnDestroy
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
    this.subscriptions?.forEach((sub) => {
      sub.unsubscribe();
    });
    this.handleDestroy();
  }
}
