import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { MICRO_SERVICE } from '@hcm-mfe/shared/common/constants';
import { environment } from '@hcm-mfe/shared/environment';

@Injectable({
  providedIn: 'root'
})
export class BaseService {

  constructor(private httpClient: HttpClient) {}

  /**
   * handleError
   */
  public handleError(error: any) {
    return throwError(error.error);
  }

  /**
   * make get request
   */
  public get(endpointUrl: string, options?: any, serviceName?: string): Observable<any> {
    let urlPath = this.getBaseUrl(serviceName) + endpointUrl;
    return this.httpClient.get(urlPath, options).pipe(
      catchError(this.handleError.bind(this))
    );
  }

  /**
   * make get request with async
   */
  public async getWithAsync(endpointUrl: string, options?: any, serviceName?: string) {
    let urlPath = this.getBaseUrl(serviceName) + endpointUrl;
    return await this.httpClient.get(urlPath, options).toPromise();
  }


  /**
   * make post request
   */
  public post(endpointUrl: string, data?: any, option?: any, serviceName?: string): Observable<any> {
    let urlPath = this.getBaseUrl(serviceName) + endpointUrl;
    return this.httpClient.post(urlPath, data, option).pipe(
      catchError(err => {
        return throwError(err.error);
      })
    );
  }

  /**
   * make put request
   */
  public put(endpointUrl: string, data?: any, serviceName?: string): Observable<any> {
    const urlPath = this.getBaseUrl(serviceName) + endpointUrl;
    return this.httpClient.put(urlPath, data).pipe(
      catchError(this.handleError)
    );
  }

  /**
   * make delete request
   */
  public delete(endpointUrl: string, options?: any, serviceName?: string): Observable<any> {
    const urlPath = this.getBaseUrl(serviceName) + endpointUrl;
    return this.httpClient.delete(urlPath, options).pipe(
      catchError(this.handleError)
    );
  }

  /**
   * make post request for file
   */
  public postRequestFile(endpointUrl: string, data?: any, serviceName?: string): Observable<any> {
    const urlPath = this.getBaseUrl(serviceName) + endpointUrl;
    return this.httpClient.post(urlPath, data, {observe: 'response', responseType: 'blob' }).pipe(
      catchError(this.handleError)
    );
  }

  /**
   * d2t make get request to get file
   * @param endpointUrl
   * @param options
   * @param serviceName
   * @returns
   */
  public getRequestFileD2T(endpointUrl: string, options?: any, serviceName?: string): Observable<any> {
    let urlPath = this.getBaseUrl(serviceName) + endpointUrl;
    return this.httpClient.get(urlPath, { ...options, observe: 'response', responseType: 'blob' }).pipe(
      catchError(this.handleError)
    );
  }

  public getRequestFile(endpointUrl: string, options?: any, baseUrl?: string): Observable<any> {
    let urlPath = baseUrl != null ? baseUrl : environment.backend.employeeServiceBackend + endpointUrl;
    return this.httpClient.get(urlPath, { ...options, observe: 'response', responseType: 'blob' }).pipe(
      catchError(this.handleError)
    );
  }

  public getRequestAbsFile(endpointUrl: string, options?: any, baseUrl?: string): Observable<any> {
    let urlPath = baseUrl != null ? baseUrl : environment.backend.absServiceBackend + endpointUrl;
    return this.httpClient.get(urlPath, {...options, responseType: 'blob'}).pipe(
      catchError(this.handleError)
    );
  }

  private getBaseUrl(serviceName?: string): string {
    let baseUrl = environment.backend.employeeServiceBackend;
    switch (serviceName) {
      case MICRO_SERVICE.DEFAULT:
        baseUrl = environment.backend.baseUrl;
        break;
      case MICRO_SERVICE.EMPLOYEE_MANAGEMENT:
        baseUrl = environment.backend.employeeServiceBackend;
        break;
      case MICRO_SERVICE.ABS_MANAGEMENT:
        baseUrl = environment.backend.absServiceBackend;
        break;
      case MICRO_SERVICE.URL_ENDPOIN_CATEGORY:
        baseUrl = environment.adminUrl;
        break;
      case MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT:
        baseUrl = environment.baseUrlTarget;
        break;
      case MICRO_SERVICE.URL_ENDPOIN_UTILITIES:
        baseUrl = environment.utilitiesUrl;
        break;
      case MICRO_SERVICE.MODEL_PLAN:
        baseUrl = environment.baseUrl;
        break;
      case MICRO_SERVICE.TAX_SERVICE:
        baseUrl = environment.backend.taxServiceBackend;
        break;
      case MICRO_SERVICE.POL:
        baseUrl = environment.policyManagement;
        break;
      case MICRO_SERVICE.SELF_SERVICE:
        baseUrl = environment.backend.selfServiceBackend;
        break;
      case MICRO_SERVICE.CONTRACT:
        baseUrl = environment.backend.contractBackend;
        break;
      case MICRO_SERVICE.REPORT:
        baseUrl = environment.reportUrl;
        break;
      case MICRO_SERVICE.ANGLE_IT:
        baseUrl = environment.angleItUrl;
        break;
    }
    return baseUrl;
  }

  protected toParams(objParams): HttpParams {
    const params = { data: new HttpParams() };
    for (const l1PropertyName in objParams) {
      if (objParams.hasOwnProperty(l1PropertyName) && objParams[l1PropertyName.toString()] != null) {
        const l1Property = objParams[l1PropertyName.toString()];
        if (typeof l1Property === 'object') {
          if (Array.isArray(l1Property)) {
            this.toParamsAppend(params, l1PropertyName, l1Property)
          } else {
            this.toParamsChild(params, l1PropertyName, l1Property);
          }
        } else {
          this.toParamsIfHasValue(params, l1PropertyName, l1Property);
        }
      }
    }
    return params.data;
  }

  private toParamsAppend(params, l1PropertyName, l1Property) {
    for (const item of l1Property) {
      params.data = params.data.append(l1PropertyName, item);
    }
  }

  private toParamsChild(params, l1PropertyName, l1Property) {
    for (const l2PropertyName in l1Property) {
      if (l1Property.hasOwnProperty(l2PropertyName) && l1Property[l2PropertyName.toString()] != null) {
        const level2Property = l1Property[l2PropertyName.toString()];
        params.data = params.data.set(`${l1PropertyName}.${l2PropertyName}`, level2Property);
      }
    }
  }

  private toParamsIfHasValue(params, l1PropertyName, l1Property) {
    if (l1Property !== '' && l1Property !== null && l1Property !== undefined) {
      params.data = params.data.set(l1PropertyName, l1Property);
    }
  }

}
