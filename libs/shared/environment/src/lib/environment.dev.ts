export const environment = {
  production: false,
  isMbBank: true,
  keycloak: {
    // Url of the Identity Provider
    issuer: 'http://10.1.27.43:10191/auth/',
    // Realm
    realm: 'internal',
    // The SPA's id.
    // The SPA is registerd with this id at the auth-serverß
    client: 'hcm-frontend',
    relationShip: 'hcm-service',
    realmManagement: 'realm-management',
  },
  app: {
    HCM_ESS_WEB: 'HCM-ESS-WEB',
    HCM_WEB : 'HCM-WEB'
  },
  backend: {
    baseUrl: 'http://10.215.254.20:10440',
    // employeeServiceBackend: 'http://10.215.254.20/hcm-person-info',
    selfServiceBackend: 'http://10.215.254.20:10440/hcm-ssv',
    employeeServiceBackend: 'http://10.215.254.20:10440/hcm-person-info',
    absServiceBackend: 'http://10.215.254.20:10440/hcm-abs',
    taxServiceBackend: 'http://10.215.254.20:10440/hcm-ptx-info',
    contractBackend: 'http://10.215.254.20:10440/hcm-pns-info'
  },

  baseUrl: 'http://10.215.254.20:10440/',
  adminUrl: 'http://10.215.254.20:10440/hcm-admin/',
  reportUrl: 'http://10.215.254.20:10440/hcm-report/',
  angleItUrl: 'http://10.215.254.20:10440/hcm-gocit/',
  utilitiesUrl: 'http://10.215.254.20:10440/hcm_utilities/',
  baseUrlTarget: 'http://10.215.254.20:10440/',
  webSocketUrl: 'http://10.215.254.20:10440',
  // webSocketUrl: 'http://10.215.254.8:10433',
  policyManagement: 'http://10.215.254.20:10440/',

  url: {
    urlPersonalInfo : 'ss/staff/personal-info',
    urlUserProfile : 'system/user-profile',
    urlRequestsManager : 'ss/requests-manager/create',
    urlChangePassword: 'https://doimatkhau.mbbank.com.vn',
    urlPayroll: 'http://10.1.27.154:9502/ords/f?p=115:1:17421164982037:::::',
    // urlCheckEVN: 'http://10.215.254.8:10433/hcm-ssv/public/env'
    urlCheckEVN: 'http://10.215.254.20:10440/hcm-ssv/public/env',
    urlRecruitment: 'http://10.1.27.154:9502/ords/f?p=132',
    urlTraining: 'http://10.1.27.154:9502/ords/f?p=161',
    urlCareerPath: 'http://10.1.27.154:9502/ords/restapi/r/ptnl/home',
    urlHRInsider: 'https://confluence.mbbank.com.vn/display/TCNS/HR+INSIDER'
  },

  translateUrl: {
    dashboard: 'dashboard-manager-view',
    system: 'system-view',
    staffManager: 'staff-manager-view',
    staffAbs: 'staff-abs-view',
    modelOrganization: 'model-organization-view',
    goalManagement: 'goal-management-view',
    learnDevelopment: 'learn-development-view',
    personalTax: 'personal-tax-view',
    policyManagement: 'policy-management-view',
    selfService: 'self-service-view',
    partnership: 'partnership-view',
    reports: 'reports-view',
    insuranceManagement: 'insurance-management-view',
    angleIt: 'angle-it-view'
  }
};
