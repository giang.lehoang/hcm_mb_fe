import {Injectable} from "@angular/core";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {MICRO_SERVICE, UrlConstant} from "@hcm-mfe/shared/common/constants";
import {Observable} from "rxjs";
import {BaseResponse, CurrentPage} from "@hcm-mfe/shared/data-access/models";

@Injectable({
  providedIn: 'root'
})
export class StaffService extends BaseService{
  public getCatalog(typeCode: string, parentCode?: number){
    const url = UrlConstant.API_VERSION + UrlConstant.CATALOGS.PREFIX;
    return this.get(url, {params: parentCode ? {typeCode, parentCode} : {typeCode}});
  }

  public getEmployeeData(param: CurrentPage): Observable<BaseResponse>{
    const url = UrlConstant.API_VERSION + UrlConstant.EMPLOYEES.PREFIX + UrlConstant.EMPLOYEES.DATA_PICKER;
    return this.get(url, {params:param})
  }

  public getEmployeeDataPolicyManagement(params): Observable<BaseResponse>{
    // http://10.215.254.20:10440/hcm-person-info/v1.0/employees/search/auth?startRecord=0&pageSize=10&keySearch=tienvv
    const url = `${UrlConstant.API_PERSON.PERSON_INFO}${UrlConstant.API_VERSION}${UrlConstant.EMPLOYEES.PREFIX}/search/auth`;
    return this.get(url, {params}, `${MICRO_SERVICE.POL}`)
  }
}
