import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {UrlConstant} from "@hcm-mfe/shared/common/constants";
import {HttpParams} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class OrgInfoService extends BaseService {
  readonly baseUrl = UrlConstant.API_VERSION + UrlConstant.ORG_INFO.PREFIX;

  public doSearchOrg(param: any): Observable<any> {
    const url = this.baseUrl + UrlConstant.ORG_INFO.SEARCH_NODE;
    return this.get(url, {params: param});
  }

  public getByParent(parentId: number, params: any): Observable<any> {
    const url = this.baseUrl + UrlConstant.ORG_INFO.GET_BY_PARENT.replace('{parentId}', parentId? parentId.toString() : '0');
    return this.get(url, {params: params});
  }

  public getOrgByLevel(params: HttpParams) {
    const url = this.baseUrl + UrlConstant.ORG_INFO.GET_BY_LEVEL;
    return this.get(url, {params: params});
  }

  getListValue(urlEndppoint: string, params: HttpParams) {
    const url = UrlConstant.API_VERSION + urlEndppoint;
    return this.get(url, {params: params});
  }
}
