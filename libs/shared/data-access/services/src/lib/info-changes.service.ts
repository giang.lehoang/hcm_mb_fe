import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";
import {environment} from "@hcm-mfe/shared/environment";
import {UrlConstant} from "@hcm-mfe/shared/common/constants";

@Injectable({
    providedIn: 'root'
})
export class InfoChangesService {

    constructor(private http: HttpClient) {
    }

    getInfoChange(employeeId: number, params: HttpParams): Observable<BaseResponse> {
        return this.http.get<BaseResponse>(`${environment.backend.employeeServiceBackend}` + UrlConstant.API_VERSION + UrlConstant.INFO_CHANGE.GET_INFO_CHANGE.replace('{employeeId}', employeeId?.toString()), { params: params });
    }

}
