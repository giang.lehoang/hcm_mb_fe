import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import { UrlConstant } from '@hcm-mfe/shared/common/constants';
import { BaseService } from '@hcm-mfe/shared/common/base-service';

@Injectable({
  providedIn: 'root'
})
export class TreeService extends BaseService {

  public doGetRootNode(param : any): Observable<any> {
    return this.get(UrlConstant.API_VERSION_V1 + UrlConstant.ORGANIZATION_TREE.PREFIX + UrlConstant.ORGANIZATION_TREE.ROOT_NODE, {params:param});
  }

  public doGetByParentNode(parentId: string): Observable<any> {
    return this.get(UrlConstant.API_VERSION_V1 + UrlConstant.ORGANIZATION_TREE.PREFIX + UrlConstant.ORGANIZATION_TREE.NODE_BY_PARENT_NODE, {params: {parentId: parentId}});
  }

  public doSearchNode(keyword: string): Observable<any> {
    return this.get('/org/v1/tree/search-node', {params: {keyword: keyword}});
  }
}
