import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import { HttpClient, HttpParams } from '@angular/common/http';
import {tap} from "rxjs/operators";
import jwt_decode from "jwt-decode";
import { MICRO_SERVICE, STORAGE_NAME, UrlConstant } from '@hcm-mfe/shared/common/constants';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { environment } from '@hcm-mfe/shared/environment';
import { StorageService } from '@hcm-mfe/shared/common/store';
import { UserLogin } from '@hcm-mfe/shared/data-access/models';
import {detectBrowserVersion} from "@hcm-mfe/shared/common/utils";

@Injectable({
  providedIn: 'root'
})
export class AuthService extends BaseService {
  constructor(private http: HttpClient) {
    super(http);
  }
  private decodedAccessToken: any;
  private refreshTokenTimeout;
  readonly baseUrl = UrlConstant.API_VERSION;

  // Đăng nhập hệ thống
  public login(param: any): Observable<any> {
    const url = this.baseUrl + UrlConstant.LOGIN;
    return this.post(url, param, {}, MICRO_SERVICE.SELF_SERVICE);
  }

  // Đăng xuất hệ thống
  public logout() {
    this.stopRefreshTokenTimer();
    const param = new HttpParams({
      fromObject: {
        clientId: environment.keycloak.client,
        refreshToken: StorageService.get(STORAGE_NAME.REFRESH_TOKEN),
      },
    });
    const url = this.baseUrl + UrlConstant.LOGOUT;
    return this.post(url, param, {}, MICRO_SERVICE.SELF_SERVICE);
  }

  // Refresh token
  public refreshToken() {
    const param = new HttpParams({
      fromObject: {
        clientId: environment.keycloak.client,
        refreshToken: StorageService.get(STORAGE_NAME.REFRESH_TOKEN),
      },
    });
    // const url = 'http://10.215.254.8:10433/hcm-ssv' + this.baseUrl + UrlConstant.REFRESH_TOKEN;
    // return this.http.post(url, param).pipe(
    const url = this.baseUrl + UrlConstant.REFRESH_TOKEN;
    return this.post(url, param, {}, MICRO_SERVICE.SELF_SERVICE).pipe(
      tap(event => {
        if (event) {
          this.saveStorage(event);
          this.saveSessionHistory();
        }
      }, error => {
        if ([401, 403].includes(error?.status)) {
          this.logout();
        }
      })
    );
  }

  //
  public isAuthenticated() {
    const accessToken = StorageService.get(STORAGE_NAME.ACCESS_TOKEN);
    if (accessToken) {
      this.decodedAccessToken = jwt_decode(accessToken);
      return accessToken && (this.decodedAccessToken.exp * 1000 - new Date().getTime() > 0);
    }
  }

  // Luu thong tin sau khi login
  public saveStorage(data: any) {
    if (data && data.code == 200) {
      this.decodedAccessToken = jwt_decode(data.data.accessToken);
      const userLogin = new UserLogin();
      userLogin.id = this.decodedAccessToken.sub;
      userLogin.username = this.decodedAccessToken.preferred_username;
      userLogin.firstName = data.data.fullName;
      userLogin.positionName = data.data.positionName;
      userLogin.employeeId = data.data.employeeId;
      StorageService.set(STORAGE_NAME.USER_LOGIN, userLogin);
      StorageService.set(STORAGE_NAME.ACCESS_TOKEN, data.data.accessToken);
      StorageService.set(STORAGE_NAME.REFRESH_TOKEN, data.data.refreshToken);
      StorageService.set(STORAGE_NAME.NUMBER_LOGIN_FAILED, 0);
      this.startRefreshTokenTimer();
    }
  }

  // start timer refresh token
  private startRefreshTokenTimer() {
    // parse json object from base64 encoded jwt token
    const accessToken = StorageService.get(STORAGE_NAME.ACCESS_TOKEN);
    if (accessToken) {
      this.decodedAccessToken = jwt_decode(accessToken);
      // set a timeout to refresh the token a minute before it expires
      const expires = new Date(this.decodedAccessToken.exp * 1000);
      const timeout = expires.getTime() - Date.now() - (30 * 1000);
      this.refreshTokenTimeout = setTimeout(() => this.refreshToken().subscribe(), timeout);
    }
  }

  // stop timer refresh token
  private stopRefreshTokenTimer() {
    clearTimeout(this.refreshTokenTimeout);
  }

  saveSessionHistory() {
    const data = {
      deviceName: detectBrowserVersion()
    }
    this.http.post(`${environment.adminUrl}sessions-history`, data).subscribe(() => {
      console.log('Insert log');
    });
  }

}
