import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import { UrlConstant } from '@hcm-mfe/shared/common/constants';
import { BaseService } from '@hcm-mfe/shared/common/base-service';

@Injectable({
  providedIn: 'root'
})
export class OrgService extends BaseService {

  public doGetByParentNode(nodeId: any, pageNumber?: number): Observable<any> {
    let index = pageNumber != undefined ? pageNumber : 0;
    return this.get(UrlConstant.API_VERSION_V1 + UrlConstant.ORGANIZATION_TREE.PREFIX + UrlConstant.ORGANIZATION_TREE.SEARCH_BY_PARENT_NODE + nodeId, {params: {startRecord: index}});
  }
}
