import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import { BaseResponse } from '@hcm-mfe/shared/data-access/models';
import { MICRO_SERVICE, SERVICE_NAME, UrlConstant } from '@hcm-mfe/shared/common/constants';
import { BaseService } from '@hcm-mfe/shared/common/base-service';

@Injectable({
  providedIn: 'root'
})
export class ImportFormService extends BaseService {

  public doImport(endpoint: string, data: any): Observable<BaseResponse> {
    const url = UrlConstant.API_VERSION + endpoint;
    return this.post(url, data);
  }

  public doDownloadFileByName(fileName: any) {
    const url = UrlConstant.API_VERSION + '/download/temp-file?fileName=' + fileName;
    return this.getRequestFile(url);
  }

  public doDownloadFileByNameFromABS(fileName: string) {
    const url = UrlConstant.API_VERSION + '/download/get-file-by-name?fileName=' + fileName;
    return this.getRequestFileD2T(url, {}, MICRO_SERVICE.ABS_MANAGEMENT);
  }

  public downloadTemplate(urlEndpoint: string) {
    const url = urlEndpoint;
    return this.getRequestFileD2T(url, undefined, MICRO_SERVICE.URL_ENDPOIN_CATEGORY);
  }

  public doDownloadFileByNameAdmin(fileName: string) {
    const url = 'download/temp-file?fileName=' + fileName;
    return this.getRequestFileD2T(url, undefined, MICRO_SERVICE.URL_ENDPOIN_CATEGORY);
  }

  public doImportAdmin(endpoint: string, data: FormData): Observable<BaseResponse> {
    const url = endpoint;
    return this.post(url, data, undefined, MICRO_SERVICE.URL_ENDPOIN_CATEGORY);
  }
  public importFile(endpoint: string, data: FormData,service){
    const url = endpoint;
    return this.post(url, data, undefined,service);
  }
  public exportFile(endpoint: string, body,service){
    const url = endpoint;
    return this.post(url, body, {responseType: 'arraybuffer'},service);
  }
}
