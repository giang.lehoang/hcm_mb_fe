import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { Observable } from 'rxjs/internal/Observable';
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import { MICRO_SERVICE, SERVICE_NAME, UrlConstant } from '@hcm-mfe/shared/common/constants';
import {
  IDataResponse,
  ITreeOrganization, OrganizationNode,
  ResponseEntity
} from "@hcm-mfe/shared/data-access/models";
import { maxInt32 } from '@hcm-mfe/shared/common/enums';

@Injectable({
  providedIn: 'root',
})
export class DataService extends BaseService {

  token: string;

  constructor(private keycloak: KeycloakService, private http: HttpClient) {
    super(http);
  }

  getOrganizationTree(params?) {
    // return this.http.get(environment.baseUrl + url.url_enpoint_organization_tree,{params: params});
    return this.get(`${UrlConstant.END_POINT_MODEL_PLAN.urlEndPointOrgTree}/getAll`,
      {params}, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  getOrganizationTreeByName(name) {
    const params = {
      name: name,
      page: 0,
      size: 10
    }
    // return this.http.get(environment.baseUrl + url.url_enpoint_organization_tree_get_by_name, {params:params});
    return this.get(`${UrlConstant.END_POINT_MODEL_PLAN.urlEndPointOrgTree}/searchTreeByName`,
      {params}, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  getOrganizationTreeById(params): Observable<IDataResponse<ITreeOrganization[]>> {
    // return this.http.get<IDataResponse<ITreeOrganization[]>>(environment.baseUrl + url.url_enpoint_organization_tree_get_by_id, {params:params});
    return this.get(`${UrlConstant.END_POINT_MODEL_PLAN.urlEndPointOrgTreeId}/findTreeById`,
      {params}, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  getSubTree(active:boolean, scope:string, resourceCode:string, is_author:boolean, orgId?:number|string,listStatus?:string):Observable<ResponseEntity<OrganizationNode>>{
    const params = {
      orgId: orgId ? orgId : '',
      scope: scope,
      resourceCode: resourceCode,
      is_author: is_author,
      listStatus:listStatus?listStatus:''
    }
    let url:string;
    if(active){
      url = UrlConstant.END_POINT_MODEL_PLAN.SUB_TREE_ACTIVE
    } else {
      url = UrlConstant.END_POINT_MODEL_PLAN.SUB_TREE_INACTIVE
    }
    return this.get(SERVICE_NAME.MODEL_PLAN + url, {params: params}, MICRO_SERVICE.MODEL_PLAN);
  }

  getJobsByType(jobType?, page?, size?,jobTypes?):Observable<any>{
    const params = {
      page: page ? page : 0,
      size: size ? size : maxInt32,
      jobType: jobType ? jobType : 'ORG',
      jobTypes:jobTypes ? jobTypes :''
    }
    return this.get(`${UrlConstant.END_POINT_MODEL_PLAN.urlEndpointJob}`,
      {params}, MICRO_SERVICE.MODEL_PLAN);
  }

  getOrganizationTreeByNamePopup(obj) {
    const params = {
      name: obj.orgName,
      page: obj.page,
      size: obj.size,
      is_author: obj.isAuthor
    }
    // return this.http.get(environment.baseUrl + url.url_enpoint_organization_tree_get_by_name, {params:params});
    return this.get(`${UrlConstant.END_POINT_MODEL_PLAN.urlEndPointOrgTree}/searchTreeByName`,
      {params}, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
  getRegionCategoryList(lookupCode, flagStatus?:number){
    let params
    if(flagStatus){
      params = {
        lookupCode: lookupCode,
        flagStatus: flagStatus
      }
    } else {
      params = {
        lookupCode: lookupCode,
      }
    }
    return this.get(`${UrlConstant.END_POINT_MODEL_PLAN.urlEnpointLookupValues}`, {params: params},MICRO_SERVICE.MODEL_PLAN)
  }


  getOrganizationTreeByNameL(obj) {
    if (!obj.is_author) {
      delete obj.is_author;
    }
    return this.get(`${UrlConstant.END_POINT_MODEL_PLAN.url_enpoint_organization_tree_get_by_name}`, {params: obj}, MICRO_SERVICE.MODEL_PLAN);
  }

  getBranchType() {
    return this.get(UrlConstant.END_POINT_MODEL_PLAN.urlEnpointLookupValues,{},MICRO_SERVICE.MODEL_PLAN);
  }

  getOrgByLevel(params) {
    return this.get(UrlConstant.END_POINT_MODEL_PLAN.GET_ORG_BY_LEVEL,{params:params},MICRO_SERVICE.MODEL_PLAN);
  }
  getOrgPopup(params) {
    return this.get(UrlConstant.END_POINT_MODEL_PLAN.GET_ORG_POPUP,{params:params},MICRO_SERVICE.MODEL_PLAN);
  }
  getOrgPopupMulti(body) {
    return this.post(UrlConstant.END_POINT_MODEL_PLAN.GET_ORG_POPUP,body,{},MICRO_SERVICE.MODEL_PLAN);
  }
}
