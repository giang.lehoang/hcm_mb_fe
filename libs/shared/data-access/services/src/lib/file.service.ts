import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseService } from '@hcm-mfe/shared/common/base-service';

@Injectable({
  providedIn: 'root',
})
export class FileService extends BaseService {
  deleteFile(url: string, params, serviceName?: string) : Observable<any> {
    return this.delete(url, {params}, serviceName);
  }

  doDownloadAttachFileWithSecurity(url: string, params ,serviceName?: string) {
    return this.getRequestFileD2T(url, {params: params}, serviceName);
  }

  downloadFileWithNoSecurity(id: number, url: string, serviceName: string): Observable<any> {
    url = url.replace("{id}", id.toString());
    return this.get(url, {responseType: 'blob' }, serviceName);
  }
}
