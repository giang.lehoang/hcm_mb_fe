import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import jwt_decode from "jwt-decode";
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { MICRO_SERVICE, STORAGE_NAME, UrlConstant } from '@hcm-mfe/shared/common/constants';
import { StorageService } from '@hcm-mfe/shared/common/store';

@Injectable({
  providedIn: 'root'
})
export class LoginService extends BaseService {
  private decodedAccessToken: any;
  readonly baseUrl = UrlConstant.API_VERSION;

  // Đăng nhập hệ thống
  public login(param: any): Observable<any> {
    const url = this.baseUrl + UrlConstant.LOGIN;
    return this.post(url, param, {}, MICRO_SERVICE.SELF_SERVICE);
  }

  // Đăng xuất hệ thống
  public logout(param: any) {
    const url = this.baseUrl + UrlConstant.LOGOUT;
    return this.post(url, param, {}, MICRO_SERVICE.SELF_SERVICE);
  }

  public isAuthenticated() {
    let accessToken = StorageService.get(STORAGE_NAME.ACCESS_TOKEN);
    if(accessToken) {
      this.decodedAccessToken = jwt_decode(accessToken);
      return accessToken && (this.decodedAccessToken.exp * 1000 - new Date().getTime() > 0);
    }
  }
}
