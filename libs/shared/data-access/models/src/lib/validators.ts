export interface ValidatorInfo {
  errorName: string;
  errorDescription: string;
  type?: 'danger' | 'warning' | 'success';
  icon?: string;
}
