export class InfoChange {
    infoChangeId?: number;
    employeeId?: number;
    actionType?: string;
    dataType?: string;
    dataBefore?: string;
    dataAfter?: string;
    createdBy?: string;
    createDate?: string;
}
