import { TemplateRef } from '@angular/core';

export interface TableHeader {
  title?: string;
  titleTrans?: string;
  isCheckBox?: boolean;
  field?: string;  // Trường dữ liệu map với backend
  rowspan?: number;
  colspan?: number;
  fixed?: boolean;
  remove?: boolean;
  fixedDir?: 'left' | 'right';
  width?: number;
  show?: boolean;
  needEllipsis?: boolean;
  needBreakword?: boolean;
  tdClassList?: string[];           // class thẻ TD
  thClassList?: string[];           // class thẻ TH
  thTemplate?: TemplateRef<any>;    // th TemplateRef
  tdTemplate?: TemplateRef<any>;    // td TemplateRef
  child?: TableHeader[];
  pipe?: string;
  isChecked?: boolean;
}

export class TableConfig {
  headers: TableHeader[];
  pageIndex?: number;
  pageSize?: number;
  total?: number;
  loading?: boolean;
  needScroll?: boolean;
  size?: 'small'| 'middle' | 'default' = 'default';
  showFrontPagination?: boolean = false;
  showPagination?: boolean = true;
  showSelect?: boolean = false;
  footer?: TemplateRef<any>;
}

export abstract class MBTableMergeCellComponentToken {
  tableConfig!: TableConfig;
  tableHeader!: Array<Array<TableHeader>>;
  setOfCheckedId: Set<number>;

  abstract tableChangeDetection(): void;
}
