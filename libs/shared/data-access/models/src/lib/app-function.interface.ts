export interface AppFunction {
  rsId: string,
  rsUri: string,
  rsCode: string,
  rsName: string,
  scopes: Array<string>,
  view: boolean, //Xem Thông tin
  create: boolean, //Tạo mới
  edit: boolean, //Sửa
  delete: boolean, //Xóa
  approve: boolean; //Phê duyệt - Từ chôi
  import: boolean, //Import
  upload: boolean, //Upload Fil
  download: boolean, //Download File
  adjusted: boolean; //Điều chỉnh
  correction: boolean; //Hiệu chỉnh
  cancel: boolean; //Hủy
  export: boolean; //Export
  generate: boolean // Tổng hợp
}
