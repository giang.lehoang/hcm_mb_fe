export interface RequestInfo {
  file: File;
  url: string;
}
