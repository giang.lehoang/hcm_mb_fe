export class ImportError {
  column: string;
  row: string;
  columnLabel: string;
  description: string;
  content: any;
}
