export interface CurrentPage {
  startRecord?: number;
  pageSize?: number;
}
