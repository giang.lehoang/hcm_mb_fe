export class UserLogin {
  id: string;
  username: string;
  firstName: string;
  employeeId?: number;
  employeeCode?: string;
  positionName?: string;
}

export interface User {
  userId: string;
  username: string;
  fullName: string;
  empId: number;
  empCode: string;
  dateOfBirth: string;
  genderName: string;
  phoneNumber: string;
  email: string;
  address: string;
  orgName: string;
  jobName: string;
  posName: string;
  isLocked: string;
  reasonLock: string;
  flagStatus: number | boolean;
  isTracing: string | boolean;
}
