export interface OrganizationByName {
  flagStatus: number;
  isExpanded: boolean;
  orgGroup: string;
  orgId: number;
  orgName: string;
  orgType: string;
  parentId: number;
  pathId: string;
  pathName: string;
  rootId: number;
  tmpInputType: string;
  tmpStatus: string;
  treeLevel: number;
}


