export class SelectModal {
  action: 'SUBMIT' | 'CANCEL' | 'NG_MODEL_CHANGE';
  isCheckAll = false;
  listOfSelected?: any[] = [];
  itemChecked?: any;
  itemSelected?: any;

  constructor(action?: 'SUBMIT' | 'CANCEL' | 'NG_MODEL_CHANGE', listOfSelected: any = null) {
    this.action = action;
    this.listOfSelected = listOfSelected;
  }
}
