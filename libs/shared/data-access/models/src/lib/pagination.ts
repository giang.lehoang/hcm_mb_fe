import { TABLE_CONFIG_DEFAULT } from '@hcm-mfe/shared/common/constants';


export class Pagination {
  pageNumber: number = 1;
  pageSize: number = TABLE_CONFIG_DEFAULT.pageSize;

  getCurrentPage() {
    return {startRecord: (this.pageNumber-1) * this.pageSize , pageSize: this.pageSize}
  }
}
