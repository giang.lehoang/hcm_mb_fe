export interface BaseResponse<T = any> {
  code?: number;
  message?: string;
  timestamp?: string;
  clientMessageId?: string;
  transactionId?: string;
  path?: string;
  status?: number;
  data?: T;
}
