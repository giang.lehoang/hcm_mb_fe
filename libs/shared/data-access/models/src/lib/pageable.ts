export class Pageable {
  totalPages?: number;
  totalElements?: number;
  currentPage?: number;
  size?: number;
  prevPage?: boolean;
  nextPage?: boolean;
  offset?: number;
  numberOfElements?: number;
}
