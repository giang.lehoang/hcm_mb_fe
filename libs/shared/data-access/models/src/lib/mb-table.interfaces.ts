import {TemplateRef} from "@angular/core";
import {NzTableSize} from "ng-zorro-antd/table";

export class MBTableHeader {
  title: string;                   // Tiêu đề
  titleTrans?: string;             // Tiêu đề được dịch
  field?: string;                  // Tên trường dữ liệu
  pipe?: string;                    // PIPE
  // showSort?: boolean;               // Hiển thị sort
  // sortDir?: undefined | 'asc' | 'desc';  // Loại sort
  width?: number;                    // Width
  thTemplate?: TemplateRef<any>;    // th TemplateRef
  tdTemplate?: TemplateRef<any>;    // td TemplateRef
  fixed?: boolean;                  // fixed
  fixedDir?: 'left' | 'right';        // fixed Bên trái hoặc phải (liên tục)
  needEllipsis?: boolean;           // truncate
  needBreakword?: boolean;           // truncate
  tdClassList?: string[];           // class thẻ TD
  thClassList?: string[];           // class thẻ TH
  show?: boolean;                   // hiển thị cột mặc định là true
}

export class MBTableConfig {
  needScroll?: boolean;
  size?: 'small'| 'middle' | 'default' = 'default';
  showFrontPagination?: boolean = false;
  showCheckbox?: boolean = false;           // Mặc định false, [checkCashArrayFromComment] = "cashArray" - cần có thuộc tính ID
  pageIndex: number;
  pageSize: number | undefined;
  total: number;
  loading: boolean;
  widthIndex?:string;
  headers: MBTableHeader[];
  showCheckboxCustom?:boolean = false;
}

export abstract class MBTableComponentToken {
  tableSize!: NzTableSize;
  tableConfig!: MBTableConfig;

  abstract tableChangeDetection(): void;
}

export interface SortFile {
  fileName: string;
  sortDir: undefined | 'desc' | 'asc'
}
