import {NzTreeNodeOptions} from "ng-zorro-antd/tree";

export interface TreeNode {
  code?: string;
  displayName?: string;
  id?: string;
  indexNumber?: number;
  isMenu?: string;
  name?: string;
  type?: string;
  uri?: string;
  children?: Array<TreeNode>;
  level?: number;
  parent?: TreeNode;
  description?: string;
  dataId?: number;
  dataType?: string;
  key?: string;
  permissionType?: string;
  parentId?: string;
  checked?: boolean;
  parentChecked?: boolean;
  originId?: string;
  expand?: boolean,
  expandCustom?: boolean,
  isChecked?:boolean
}
export interface ITreeOrganization {
  orgName: string;
  pathName: string;
  orgId: number;
  expanded: boolean;
  pageNumber?: number;
  treeLevel: number;
  pathId: string;
  flagStatus?:number
}

export class OrganizationNode implements NzTreeNodeOptions {
  title: string;
  key: string | any;
  icon?: string;
  isLeaf?: boolean;
  checked?: boolean;
  selected?: boolean;
  selectable?: boolean;
  disabled?: boolean;
  disableCheckbox?: boolean;
  children?: OrganizationNode[];
  orgId: number;
  orgName: string;
  subs?: OrganizationNode[];
  expanded: boolean;
  tmpInputType?: string;
  tmpStatus?: number;
  treeLevel: number;
  loadChild: boolean;
  id?: number;
  isExpanded?: boolean;
  name?: string;
  level?: number;
  flagStatus?:number;
}
export interface InitScreenSearch {
  orgId: string;
  flagStatus: string;
  inputType: string;
  page: number;
  size: number;
  jobId?: number;
  year?: number;
  date?: string;
  region?:string;
  orgGroup?:string;
  brachType?:string;
  treeLevel?:number|string;
  orgManager?:string;
  orgIdTT1?:string;
  orgIdTT2?:string;
  orgIdTT3?:string;

}
export interface IDataResponse<T> {
  content?: T;
  number: number;
  numberOfElements: number;
  size: number;
  totalElements: number;
  totalPages: number;
  pageable?: {
    pageNumber: number;
  };
}


export interface TreeRoot<T> {
  key:string;
  title?:string;
  children: Array<T>;
  level: number;
  expand?: boolean;
  parent?: T;

}
export class ParamTreeMulti {
  orgId? ='';
  name? =''
  orgGroup? ='';
  region? = '';
  brachType = '';
  isAuthor = false;
  scope = '';
  resourceCode = ''
  listStatus? = '';
  isSelected = false;
  isMulti =false;
  orgIds?: number[] = [];
  page = 0;
  size = 10;
}

