export class Category {
  value?: string | number;
  label?: string;
  id?: string;
  disabled?:boolean = false;
  code?: string | number;
  name?: string;
  description?: string;
  orderNum?: number;
  parentCode?: string;
  statusType?: number;
}

export interface RegionCategory {
  attribute: string;
  criteria: string;
  lcoCode: string;
  lvaId: number;
  lvaMean: string;
  lvaValue: string;
  parentValue: string;
  regionMean:string;
  flagStatus:number;
}
