export class ScrollTabOption {
  icon?: string;
  title?: string;
  disabled?: boolean = false;
  selected?: boolean = true;
  scrollTo?: string;
  children?: ScrollTabOption[];
  active?: boolean;
}
