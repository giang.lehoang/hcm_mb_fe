export class Node {
  nodeId: number;
  code: string;
  name: string;
  parentId: number;
  childrens: Node[] = [];
}

export class OrgInfo {
  orgId: number;
  orgName: string;
  path: string;
  parentId: number;
  parentName: string;
}
