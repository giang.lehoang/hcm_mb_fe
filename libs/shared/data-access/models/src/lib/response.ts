export interface ResponseEntity<T> {
  data?: Array<T>;
  content?: [];
  size?: number;
  totalElements?: number;
  totalPages?: number;
  numberOfElements?: number;
  pageable?: {
    pageNumber: number;
  };
}

export interface ResponseEntityModel<T> {
  data?: T;
}

export interface ResponseEntityData<T> {
  data?: ResponseEntity<T>;
  content?: [];
  size?: number;
  totalElements?: number;
  pageable?: {
    pageNumber: number;
  };
}
