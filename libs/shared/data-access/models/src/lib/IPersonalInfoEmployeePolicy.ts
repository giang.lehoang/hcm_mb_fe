export interface IPersonalInfoEmployeePolicy {
  dateOfBirth?: string;
  email?: string;
  empCode: string;
  employeeCode?: string;
  empId?: number;
  employeeId?: number;
  fullName?: string;
  gender?: string;
  jobId?: number;
  jobName?: string;
  joinCompanyDate?: string;
  mobileNumber?: string;
  orgId?: number;
  orgName?: string;
  orgPathId?: string;
  orgPathName?: string;
  posFromDate?: string;
  posId?: number;
  posName?: string;
  positionName?: string;
  posSeniority?: number;
  seniority?: number;
  status?: number;
}
