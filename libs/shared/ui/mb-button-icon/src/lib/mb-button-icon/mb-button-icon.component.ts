import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'mb-button-icon',
  templateUrl: './mb-button-icon.component.html',
  styleUrls: ['./mb-button-icon.component.scss'],
})
export class MbButtonIconComponent implements OnInit {
  @Input() mbButtonType: 'DELETE' | 'EDIT' | 'ADD' | 'COPY' | 'RESET'| 'EYE';
  @Input() width = 22;
  @Input() height = 22;
  iconUrl = null;
  tooltipContent = '';

  constructor() {}
  ngOnInit(): void {
    if(this.mbButtonType === 'DELETE'){
      this.iconUrl = 'assets/images/icon/delete_outline.svg';
      this.tooltipContent = 'common.button.delete';
    }
    if(this.mbButtonType === 'RESET'){
      this.iconUrl = 'assets/images/icon/reset.svg';
      this.tooltipContent = 'common.button.reset';
    }
    if(this.mbButtonType === 'EDIT'){
      this.width = 16;
      this.height = 16;
      this.iconUrl = 'assets/images/icon/edit.png';
      this.tooltipContent = 'common.button.editS';
    }
    if(this.mbButtonType === 'COPY'){
      this.width = 16;
      this.height = 16;
      this.iconUrl = 'assets/images/icon/copy-org.svg';
      this.tooltipContent = 'common.button.copy';
    }
    if(this.mbButtonType === 'EYE'){
      this.width = 22;
      this.height = 16;
      this.iconUrl = 'assets/images/icon/eye.png';
      this.tooltipContent = 'common.button.view';
    }
  }
}
