import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MbButtonIconComponent } from './mb-button-icon/mb-button-icon.component';
import { TranslateModule } from '@ngx-translate/core';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';

@NgModule({
  imports: [CommonModule, TranslateModule, NzToolTipModule],
  declarations: [MbButtonIconComponent],
  exports: [MbButtonIconComponent],
})
export class SharedUiMbButtonIconModule {}
