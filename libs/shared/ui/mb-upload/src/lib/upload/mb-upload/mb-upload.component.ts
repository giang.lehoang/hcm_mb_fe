import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { saveAs } from 'file-saver';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { InputBoolean } from 'ng-zorro-antd/core/util';
import {FileService} from "@hcm-mfe/shared/data-access/services";

export class ModelUpload {
  type?: 'default' | 'warning' | 'error' | 'success' = 'default';
  labelText?: string;
  textMessageValue?: string;
  showIcon?: boolean = true;
  description?: string = '';
  disable?: boolean;
  autofocus?: 'autofocus' | null; // Chưa hoạt động
  showFlexEnd?: boolean = true;
  showError?: boolean = false;
  multiple?: boolean = true;
  viewDetail?: boolean = false;
  viewDetailStr?: string = '';
  loadMore?: boolean = false;
}

export class EmitData {
  fileList: NzUploadFile[];
  docIdsDelete: number[]
}

@Component({
    selector: 'mb-upload',
    templateUrl: './mb-upload.component.html',
    styleUrls: ['./mb-upload.component.scss'],
})
export class MbUploadComponent implements OnInit, OnChanges {
  @Input() mbConfig: ModelUpload = new ModelUpload();
  @Input() mbLabelText: string;
  @Input() mbDescriptionText = '';
  @Input() @InputBoolean() mbRequired = false;
  @Input() mbTypeFileAvailable?: Array<string> = ['docx', 'doc', 'xlsx', 'xls', 'pdf', 'dwg', 'jpg', 'png', 'jpeg' ,'svg', 'zip'];
  @Input() mbShowContentAlert?: boolean = false;
  @Input() mbMaxFile: number = 100;
  @Input() mbMaxSize: number = 300;
  @Input() mbShowFlexEnd = false;
  @Input() mbIcon: string = 'inbox';
  @Input() mbType: 'default' | 'warning' | 'error' | 'success';
  @Input() @InputBoolean() mbDisable = false;
  @Input() mbShowIconMessage = true;
  @Input() mbShowError = false;
  @Input() mbMultiple = true;

  @Input() isDownloadFileWithNoSecurity = false;

  @Input() mbUrl: string = '/v1.0/download/file';
  @Input() mbServiceName: string;

  @Input() fileList: NzUploadFile[] = [];
  @Output() fileListChange = new EventEmitter<NzUploadFile[]>();

  @Input() docIdsDelete: number[] = [];
  @Output() docIdsDeleteChange = new EventEmitter<number[]>();

  @Output() onFileChangeAction?: EventEmitter<EmitData> = new EventEmitter();
  isShowError: boolean;

  inputGroupClass = 'input__group';
  inputGroupGroupClass = 'input__group--group';
  inputTextClass = 'input__text';
  inputMessageClass = 'input__message';

  classGroup = {
    noIcon: 'input__group--no-icon',
    icon: 'input__group--icon'
  };

  classGroupGroup = {
    default: 'input__group--group--default',
    warning: 'input__group--group--warning',
    error: 'input__group--group--error',
    success: 'input__group--group--success',
  };

  classInput = {
    default: 'input__text--default',
    warning: 'input__text--warning',
    error: 'input__text--error',
    success: 'input__text--success'
  };

  classMessage = {
    default: 'input__message--default',
    warning: 'input__message--warning',
    error: 'input__message--error',
    success: 'input__message--success'
  };

  classIcon = {
    warning: 'warning',
    error: 'close-circle',
    success: 'check-circle',
  };

  iconType: string;

  constructor(private toastrService: ToastrService,
              private translate: TranslateService,
              private fileService: FileService,
  ) {}

  ngOnInit(): void {
    if(!this.fileList) {
      this.fileList = [];
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.checkShowError();
  }

  checkShowError() {
    if ((this.mbShowError && this.mbRequired) && (this.fileList && this.fileList.length==0)) {
      this.mbType = 'error';
      this.setErrorMessage(this.translate.instant('File bắt buộc nhập'));
    } else {
      this.mbType = 'default';
    }
    this.inputGroupClass = 'input__group' + ' ';
    this.inputGroupGroupClass = 'input__group--group' + ' ';
    this.inputTextClass = 'input__text' + ' ';
    this.inputMessageClass = 'input__message ' + ' ';
    this.setMbConfig();
    this.configInput();
  }

  setMbConfig() {
    if (!this.mbShowFlexEnd) {
      this.mbConfig.showFlexEnd = this.mbShowFlexEnd;
    }
    if (this.mbType) {
      this.mbConfig.type = this.mbType;
    }
    if (this.mbLabelText) {
      this.mbConfig.labelText = this.mbLabelText;
    }
    if (this.mbDescriptionText) {
      this.mbConfig.description = this.mbDescriptionText;
    }
    this.mbConfig.disable = this.mbDisable;
    this.mbConfig.showError = this.mbShowError;
    this.mbConfig.showIcon = this.mbShowIconMessage;
    this.mbConfig.multiple = this.mbMultiple;
  }

  configInput() {
    this.inputGroupClass += this.classGroup.noIcon;
    switch (this.mbConfig?.type) {
      case 'default':
        this.inputGroupGroupClass += this.classGroupGroup.default;
        this.inputTextClass += this.classInput.default;
        this.inputMessageClass += this.classMessage.default;
        break;
      case 'warning':
        this.inputGroupGroupClass += this.classGroupGroup.warning;
        this.inputTextClass += this.classInput.warning;
        this.inputMessageClass += this.classMessage.warning;
        this.iconType = this.classIcon.warning;
        break;
      case 'error':
        this.inputGroupGroupClass += this.classGroupGroup.error;
        this.inputTextClass += this.classInput.error;
        this.inputMessageClass += this.classMessage.error;
        this.iconType = this.classIcon.error;
        break;
      case 'success':
        this.inputGroupGroupClass += this.classGroupGroup.success;
        this.inputTextClass += this.classInput.success;
        this.inputMessageClass += this.classMessage.success;
        this.iconType = this.classIcon.success;
        break;
      default:
        this.inputGroupGroupClass += this.classGroupGroup.default;
        this.inputTextClass += this.classInput.default;
        this.inputMessageClass += this.classMessage.default;
        break;
    }
  }

  setErrorMessage(errorDescription: string) {
    this.mbConfig.textMessageValue = errorDescription;
  }

  public getListAccept(): string {
    return this.mbTypeFileAvailable.map(item => '.' + item).join(', ');
  }

  beforeUpload = (file: NzUploadFile): boolean => {
    this.isShowError = false;
    this.setErrorMessage(null);
    if (this.checkFileExists(file, this.fileList)) {
      let fileUploadIsAvailable: NzUploadFile;
      if (!file.type && file.name) {
        const tempArr = file.name.split('.');
        const ext = tempArr[tempArr.length - 1];
        if (this.mbTypeFileAvailable.includes(ext.toLowerCase()))
          fileUploadIsAvailable = file
      }
      if (file.type && this.checkFileUploadIsAvailable(file.type)) {
        this.mbShowContentAlert = false
        fileUploadIsAvailable = file
      } else {
        this.mbShowContentAlert = true
      }
      if (fileUploadIsAvailable) {
        let tempListFileOld = Array.isArray(this.fileList) ? Array.from(this.fileList) : [];
        if (this.mbMaxFile >= tempListFileOld.length + 1) {
            if (!this.checkSizeAvailable(fileUploadIsAvailable)) {
              tempListFileOld = [
                ...tempListFileOld,
                fileUploadIsAvailable
              ];
              this.fileList = tempListFileOld;
              this.fileListChange.emit(this.fileList);
              this.onFileChangeAction.emit({fileList: this.fileList, docIdsDelete: this.docIdsDelete});
            } else {
              this.isShowError = true;
              this.setErrorMessage(this.translate.instant("Dung lượng file không quá: ") + this.mbMaxSize + 'M');
            }
        } else {
          this.isShowError = true;
          this.setErrorMessage(this.translate.instant("Số lượng file không được quá: ") + this.mbMaxFile);
        }
      }
    } else {
      this.isShowError = true;
      this.setErrorMessage(this.translate.instant("File đã được tải lên: ") + file.name);
    }
    return false;
  }

  private checkSizeAvailable(itemFile: NzUploadFile) {
    return itemFile.size >= this.mbMaxSize * 1024 * 1024;
  }

  private checkFileUploadIsAvailable(fileType: string) {
    const ext = this.getTypeFileUpload(fileType);
    return Array.isArray(this.mbTypeFileAvailable) && this.mbTypeFileAvailable.includes(ext);
  }

  private getTypeFileUpload(typeFile: string) {
    let ext;
    switch (typeFile) {
      case TYPE_FILE_UPLOAD.DOC:
        ext = 'doc';
        break;
      case TYPE_FILE_UPLOAD.DOCX:
        ext = 'docx';
        break;
      case  TYPE_FILE_UPLOAD.XLS:
        ext = 'xls';
        break;
      case TYPE_FILE_UPLOAD.XLSX:
        ext = 'xlsx';
        break;
      case TYPE_FILE_UPLOAD.PDF:
        ext = 'pdf';
        break;
      case TYPE_FILE_UPLOAD.DWG:
        ext = 'dwg';
        break;
      case TYPE_FILE_UPLOAD.PNG:
        ext = 'png';
        break;
      case TYPE_FILE_UPLOAD.JPG:
        ext = 'jpg';
        break;
      case TYPE_FILE_UPLOAD.JPEG:
        ext = 'jpeg';
        break;
      case TYPE_FILE_UPLOAD.ZIP:
        ext = 'zip';
        break;
      case TYPE_FILE_UPLOAD.ZIP_NZ:
        ext = 'zip';
        break;
      case TYPE_FILE_UPLOAD.SVG:
        ext = 'svg';
        break;
      case TYPE_FILE_UPLOAD.RAR:
        ext = 'rar';
        break;
      default:
        ext = undefined;
        break;
    }
    return ext;
  }

  private checkFileExists(fileInput: any, listFileResponse: Array<any>) {
    if (!listFileResponse || (Array.isArray(listFileResponse) && listFileResponse.length === 0)) {
      return true;
    }
    if (Array.isArray(listFileResponse)) {
      const res = listFileResponse.filter((item) => item.name === fileInput.name)
      if (res.length > 0) {
        return false;
      }
    }
    return true;
  }

  public getTypeFileAvailable() {
    return this.translate.instant('Định đạng hỗ trợ: ') + this.mbTypeFileAvailable.join(', ');
  }

  downloadFile = (file: NzUploadFile) => {
    if (this.isDownloadFileWithNoSecurity) {
      this.fileService.downloadFileWithNoSecurity(~~file.uid, this.mbUrl, this.mbServiceName).subscribe({
        next: (res) => {
          const reportFile = new Blob([res], { type: this.getTypeExtension(file.name.split('.').pop()) });
          saveAs(reportFile, file.name);
        },
        error: () => {

        }
      });
    } else {
      if (file?.uid) {
        const param = {
          docId: file.uid,
          security: file.url ? file.url : file.thumbUrl
        }
        this.fileService.doDownloadAttachFileWithSecurity(this.mbUrl, param, this.mbServiceName).subscribe(res => {
          const reportFile = new Blob([res.body], { type: this.getTypeExtension(file.name.split('.').pop()) });
          saveAs(reportFile, file.name);
        });
      }
    }
  }

  removeFile = (file: NzUploadFile) => {
    this.isShowError = false;
    this.setErrorMessage(null);

    this.docIdsDelete.push(Number(file.uid));
    const index = this.fileList.indexOf(file);
    this.fileList.splice(index, 1);
    this.fileListChange.emit(this.fileList);
    this.onFileChangeAction.emit({fileList: this.fileList, docIdsDelete: this.docIdsDelete});
    if (Number(file.uid)) {
      this.docIdsDeleteChange.emit(this.docIdsDelete);
    }

    this.checkShowError();
    return true;
  }

  getTypeExtension(format: string | any): string {
    let type: string;
    switch (format) {
      case TYPE_FILE_UPLOAD.DOC:
        type = TYPE_EXTENSION.DOC;
        break;
      case TYPE_FILE_UPLOAD.DOCX:
        type = TYPE_EXTENSION.DOCX;
        break;
      case TYPE_FILE_UPLOAD.XLS:
        type = TYPE_EXTENSION.XLS;
        break;
      case TYPE_FILE_UPLOAD.XLSX:
        type = TYPE_EXTENSION.XLSX;
        break;
      case TYPE_FILE_UPLOAD.PDF:
        type = TYPE_EXTENSION.PDF;
        break;
      case TYPE_FILE_UPLOAD.DWG:
        type = TYPE_EXTENSION.DWG;
        break;
      case TYPE_FILE_UPLOAD.PNG:
        type = TYPE_EXTENSION.PNG;
        break;
      case TYPE_FILE_UPLOAD.JPG:
        type = TYPE_EXTENSION.JPG;
        break;
      case TYPE_FILE_UPLOAD.JPEG:
        type = TYPE_EXTENSION.JPEG;
        break;
      case TYPE_FILE_UPLOAD.ZIP:
        type = TYPE_EXTENSION.ZIP;
        break;
      case TYPE_FILE_UPLOAD.ZIP_NZ:
        type = TYPE_EXTENSION.ZIP_NZ;
        break;
      case TYPE_FILE_UPLOAD.SVG:
        type = TYPE_EXTENSION.SVG;
        break;
      case TYPE_FILE_UPLOAD.RAR:
        type = TYPE_EXTENSION.RAR;
        break;

      default:
        type = null;
        break;
    }
    return type;
  }
}

export const TYPE_FILE_UPLOAD = {
  XLSX: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  XLS: 'application/vnd.ms-excel',
  PDF: 'application/pdf',
  DOC: 'application/msword',
  DOCX: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  DWG: 'application/dwg',
  PNG: 'image/png',
  JPG: 'image/jpg',
  JPEG: 'image/jpeg',
  ZIP: 'application/x-zip-compressed',
  ZIP_NZ: 'application/zip',
  SVG: 'image/svg+xml',
  RAR: 'application/x-rar-compressed',
}

export  const TYPE_EXTENSION = {
  DOC: 'doc',
  DOCX: 'docx',
  XLS: 'xls',
  XLSX: 'xlsx',
  PDF: 'pdf',
  DWG: 'dwg',
  PNG: 'png',
  JPG: 'jpg',
  JPEG: 'jpeg',
  ZIP: 'zip',
  ZIP_NZ: 'zip',
  SVG: 'svg',
  RAR: 'rar',
}
