import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MbUploadComponent } from './upload/mb-upload/mb-upload.component';
import { MbUploadFileComponent } from './upload/mb-upload-file/mb-upload.component';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { NzInputModule } from 'ng-zorro-antd/input';

@NgModule({
  imports: [CommonModule, NzUploadModule, NzFormModule, NzIconModule, TranslateModule, SharedUiMbButtonModule, NzInputModule],
  declarations: [MbUploadComponent, MbUploadFileComponent],
  exports: [MbUploadComponent, MbUploadFileComponent],
})
export class SharedUiMbUploadModule {}
