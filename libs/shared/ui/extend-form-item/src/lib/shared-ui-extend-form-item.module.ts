import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ExtendFormItemComponent} from "./extend-form-item/extend-form-item.component";
import {NzIconModule} from "ng-zorro-antd/icon";
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  imports: [CommonModule, NzIconModule, TranslateModule],
  declarations: [ExtendFormItemComponent],
  exports: [ExtendFormItemComponent],
})
export class SharedUiExtendFormItemModule {}
