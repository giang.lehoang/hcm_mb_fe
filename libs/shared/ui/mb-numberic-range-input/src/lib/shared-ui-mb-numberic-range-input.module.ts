import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MbNumbericRangeInputComponent } from './mb-numberic-range-input/mb-numberic-range-input.component';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { SharedDirectivesNumbericModule } from '@hcm-mfe/shared/directives/numberic';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
  imports: [CommonModule, NzIconModule, SharedUiMbInputTextModule, SharedDirectivesNumbericModule, FormsModule, ReactiveFormsModule],
  declarations: [MbNumbericRangeInputComponent],
  exports: [MbNumbericRangeInputComponent],
})
export class SharedUiMbNumbericRangeInputModule {}
