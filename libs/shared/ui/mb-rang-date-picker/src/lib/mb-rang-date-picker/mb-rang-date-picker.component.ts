import { Component, EventEmitter, forwardRef, Injector, Input, OnInit, Output, TemplateRef } from '@angular/core';
import { NG_VALUE_ACCESSOR, NgControl } from '@angular/forms';
import {
  CompatibleDate,
  DisabledDateFn,
  DisabledTimeFn,
  NzDateMode,
  SupportTimeOptions
} from 'ng-zorro-antd/date-picker';
import { FunctionProp, NzSafeAny } from 'ng-zorro-antd/core/types';
import { toBoolean } from 'ng-zorro-antd/core/util';
import { noop } from 'rxjs';

@Component({
  selector: 'mb-rang-date-picker',
  templateUrl: './mb-rang-date-picker.component.html',
  styleUrls: ['./mb-rang-date-picker.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => MbRangDatePickerComponent),
    }
  ]
})
export class MbRangDatePickerComponent implements OnInit {

  @Input() mbLabelText: string;
  @Input() mbAutofocus: boolean = false;
  @Input() mbDatePickerIcon: 'search' | 'down' | string;
  @Input() mbDisabled: boolean = false;
  @Input() mbInputReadOnly: boolean = false;
  @Input() mbInline: boolean = false;
  @Input() mbMode: NzDateMode = 'date';
  @Input() mbPlaceholderFromdate: string = null;
  @Input() mbPlaceholderTodate: string = null;
  @Input() mbSuffixIcon: string | TemplateRef<NzSafeAny> = 'calendar';
  @Input() mbDisabledDate?: (d: Date) => boolean;
  @Input() mbShowToday: boolean;
  @Input() mbDisabledTime?: DisabledTimeFn;
  @Input() mbDateRender?: TemplateRef<NzSafeAny> | string | FunctionProp<TemplateRef<Date> | string>;
  @Input() mbRenderExtraFooter?: TemplateRef<NzSafeAny> | string | FunctionProp<TemplateRef<NzSafeAny> | string>;
  private  showTime: SupportTimeOptions | boolean = false;

  @Output() readonly mbOnOpenChange = new EventEmitter<boolean>();
  @Output() readonly mbOnPanelChange = new EventEmitter<NzDateMode | NzDateMode[] | string | string[]>();
  @Output() readonly mbOnCalendarChange = new EventEmitter<Array<Date | null>>();
  @Output() readonly mbOnOk = new EventEmitter<CompatibleDate | null>();

  @Input() mbErrorDefs: {errorName: string, errorDescription: string}[];
  @Input() mbShowIconMessage = true;
  @Input() mbShowError = false;
  @Input() mbErrors: any;
  @Input() mbType: 'default' | 'warning' | 'error' | 'success' = 'default';

  @Input() get mbShowTime(): SupportTimeOptions | boolean {
    return this.showTime;
  }

  set mbShowTime(value: SupportTimeOptions | boolean) {
    this.showTime = typeof value === 'object' ? value : toBoolean(value);
  }

  value: any[] = [null, null];
  onTouched: () => void = noop;
  onChange: (_: any) => void = noop;
  ngControl?: NgControl;
  inputMessageClass = 'input__message';
  textMessageValue: any;
  showIcon: any;
  iconType: any;
  inputGroupGroupClass = 'datepicker__group--group';

  classGroupGroup = {
    default: 'datepicker__group--group--default',
    warning: 'datepicker__group--group--warning',
    error: 'datepicker__group--group--error',
    success: 'datepicker__group--group--success',
  };

  classMessage = {
    default: 'datepicker__message--default',
    warning: 'datepicker__message--warning',
    error: 'datepicker__message--error',
    success: 'datepicker__message--success'
  };

  classIcon = {
    warning: 'warning',
    error: 'warning',
    success: 'check-circle'
  };

  constructor(private inj: Injector) { }

  ngOnChanges() {
    this.configInput();
    this.setErrorMessage();
  }

  ngOnInit(): void {
    this.ngControl = this.inj.get(NgControl);
  }

  writeValue(obj: any) {
    this.value = obj ?? [null, null];
    this.onChange(this.value);
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.mbDisabled = isDisabled;
  }

  openChange($event, type: 'START'| 'END') {
    this.mbOnOpenChange.emit($event)
  }

  onOk($event, type: 'START'| 'END') {
    switch (type) {
      case 'END':
        this.value[1] = $event;
        break;
      case 'START':
        this.value[0] = $event;
        break;
      default:
        return
    }
    this.mbOnOk.emit($event)
    this.writeValue(this.value)
  }

  onInputChange($event: any,  type: 'START'| 'END') {
    switch (type) {
      case 'END':
        this.value[1] = $event;
        break;
      case 'START':
        this.value[0] = $event;
        break;
      default:
        return
    }
    this.onChange($event);
    this.writeValue(this.value)
  }

  configInput() {
    this.inputGroupGroupClass = 'datepicker__group--group' + ' ';
    this.inputMessageClass = 'datepicker__message ' + ' ';
    switch (this.mbType) {
      case 'default':
        this.inputGroupGroupClass += this.classGroupGroup.default;
        this.inputMessageClass += this.classMessage.default;
        break;
      case 'warning':
        this.inputGroupGroupClass += this.classGroupGroup.warning;
        this.inputMessageClass += this.classMessage.warning;
        this.iconType = this.classIcon.warning;
        break;
      case 'error':
        this.inputGroupGroupClass += this.classGroupGroup.error;
        this.inputMessageClass += this.classMessage.error;
        this.iconType = this.classIcon.error;
        break;
      case 'success':
        this.inputGroupGroupClass += this.classGroupGroup.success;
        this.inputMessageClass += this.classMessage.success;
        this.iconType = this.classIcon.success;
        break;
      default:
        this.inputGroupGroupClass += this.classGroupGroup.default;
        this.inputMessageClass += this.classMessage.default;
        break;
    }
  }

  setErrorMessage() {
    if (this.mbErrors) {
      for(let error of this.mbErrorDefs) {
        let key = error.errorName;
        if (this.mbErrors[key]) {
          this.textMessageValue = error.errorDescription;
        }
      }
    }
  }

}
