import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MbRangDatePickerComponent } from './mb-rang-date-picker/mb-rang-date-picker.component';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { SharedDirectivesMaskDateModule } from '@hcm-mfe/shared/directives/mask-date';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
    imports: [CommonModule, NzIconModule, NzDatePickerModule, SharedDirectivesMaskDateModule, FormsModule, ReactiveFormsModule],
  declarations: [MbRangDatePickerComponent],
  exports: [MbRangDatePickerComponent],
})
export class SharedUiMbRangDatePickerModule {}
