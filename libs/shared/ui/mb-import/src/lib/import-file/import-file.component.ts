import { Component, EventEmitter, Input, Output } from '@angular/core';
import { saveAs } from 'file-saver';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { ImportError } from '@hcm-mfe/shared/data-access/models';
import { Constant } from '@hcm-mfe/shared/common/constants';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import {getTypeExport} from "@hcm-mfe/shared/common/utils";
import {ImportFormService} from "@hcm-mfe/shared/data-access/services";
@Component({
    selector: 'mb-import',
    templateUrl: './import-file.component.html',
    styleUrls: ['./import-file.component.scss']
})
export class ImportFileComponent {

  @Input() showContent = false;
  @Input() closeModalWhenClick = true;
  @Input() urlApiImport: string = '';
  @Input() urlApiDownloadTemp: string = '';
  @Output() onCloseModal: EventEmitter<boolean> = new EventEmitter<boolean>();

  isModalError: boolean = false;
  isImport: boolean = false;
  isDownloadTemplate: boolean = false;
  errorList: ImportError[] = [];
  fileList: NzUploadFile[] = [];
  fileName: string;
  fileImportName: string = '';
  fileImportSize: string;

  isExistFileImport: boolean = false;
  nzWidth: number;

  constructor(private readonly importFormService: ImportFormService,
              private readonly toastrService: ToastrService, public translate: TranslateService) {
    this.nzWidth = window.innerWidth > 767 ? window.innerWidth / 3 : window.innerWidth / 2.5;
  }

  doDownloadTemplate() {
    this.isDownloadTemplate = true;
    this.importFormService.downloadTemplate(this.urlApiDownloadTemp).subscribe(res => {
      this.isDownloadTemplate = false;
      const arr = res.headers.get("Content-Disposition")?.split(';');
      const fileName:string = arr[arr.length - 1].replace('filename=','').trim();
      const reportFile = new Blob([res.body], { type: getTypeExport('xlsx') });
      saveAs(reportFile, fileName);
    }, error => {
      this.isDownloadTemplate = false;
      this.toastrService.error(this.translate.instant('common.notification.downloadFileError') + `: ${error?.message}`);
    });
  }

  doDownloadFile() {
    this.importFormService.doDownloadFileByNameAdmin(this.fileName).subscribe(res => {
      const reportFile = new Blob([res.body], { type: getTypeExport('xlsx') });
      saveAs(reportFile, 'file_error_information.xlsx');
    }, error => {
      this.toastrService.error(error.message);
    });
  }

  beforeUpload = (file: NzUploadFile): boolean => {
    if (file.size >= 3000000 || !checkTypeExport(file.type)) {
      if(!checkTypeExport(file.type)) {
        this.toastrService.error(this.translate.instant('common.upload.fileError'));
      }
      if(file.size >= 3000000) {
        this.toastrService.error(this.translate.instant('common.notification.limitSize'));
      }
    } else {
      this.fileList = [];
      this.fileList = this.fileList.concat(file);
      this.isExistFileImport = true;
      this.fileImportName = file.name;
      this.fileImportSize = (file.size/1000000).toFixed(2);
    }
    return false;
  };

  doImportFile() {
    this.isImport = true;
    const formData = new FormData();
    this.fileList.forEach((file: NzUploadFile) => {
      formData.append('file', file as any);
    });
    this.importFormService.doImportAdmin(this.urlApiImport, formData).subscribe(res => {
      if (res != null && res.data?.errorList) {
        this.errorList = res.data.errorList;
        if (this.errorList != null && this.errorList.length > 0) {
          this.isModalError = true;
          this.fileName = res.data.errorFile;
          this.toastrService.error(this.translate.instant('common.notification.fileError'));
          this.isImport = false;
        } else {
          this.isModalError = false;
          this.errorList = [];
          this.fileName = undefined;
          this.toastrService.error(res.message);
          this.isImport = false;
        }
      } else {
        this.isModalError = false;
        this.errorList = [];
        this.fileName = undefined;
        this.doClose(true);
        this.toastrService.success(this.translate.instant('common.notification.fileSuccess'));
        this.isImport = false;
      }
    }, error => {
      this.toastrService.error(error?.message ?? this.translate.instant('common.notification.fileError'));
      this.isImport = false;
    });
  }

  doRemoveFile = (): boolean => {
    this.fileList = [];
    this.isExistFileImport = false;
    return true;
  };

  doCloseModal() {
    this.isModalError = false;
  }

  doClose(isSearch: boolean) {
    this.showContent = false;
    this.onCloseModal.emit(isSearch);
    this.fileImportName = '';
    this.isExistFileImport = false;
  }
}

export function checkTypeExport(typeFile: string): boolean {
  let type: boolean;
  switch (typeFile) {
    case Constant.EXCEL_MIME_1:
      type = true;
      break;
    case Constant.EXCEL_MIME_2:
      type = true;
      break;
    default:
      type = false;
      break;
  }
  return type;
}
