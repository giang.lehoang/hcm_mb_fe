import { Component, EventEmitter, Input, OnInit, Output, OnDestroy } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { CustomToastrService } from '@hcm-mfe/shared/core';
import { SUCCESS } from '@hcm-mfe/model-organization/data-access/models';
import { ImportFormService } from '@hcm-mfe/shared/data-access/services';

export interface IFileUploadInfo {
  fileName: string;
  fileSize: any;
}
@Component({
  selector: 'app-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.scss'],
})
export class UploadFileComponent implements OnInit, OnDestroy {
  fileUploadInfo: any;
  url: string = '';
  error: string = '';
  file: any;
  loading: boolean = false;
  listError: Array<string> = [];
  isError: boolean = false;
  @Input() isData: boolean = false;
  @Input() fileType: string[] | undefined;
  @Input() request: any;
  @Output() eventClose: EventEmitter<unknown> = new EventEmitter<unknown>();

  constructor(private translate: TranslateService, private importFormService: ImportFormService, private toast: CustomToastrService) {}

  ngOnDestroy(): void {
    this.eventClose.complete();
  }

  ngOnInit(): void {
    this.loading = false;
  }

  handleCancel(): void {
    this.eventClose.emit(false);
  }

  beforeUpload = (file: any): boolean => {
    const fileExtend = file.name?.split('.')?.pop()?.toUpperCase();
    const contain = this.fileType?.find((element) => element.toUpperCase() == fileExtend);
    this.error = contain ? '' : this.translate.instant('shared.uploadFile.error.format');

    const fileSize = file.size / (1024 * 1024);
    this.error = fileSize > 3 ? this.translate.instant('shared.uploadFile.error.maxSize') : this.error;

    this.file = file;
    if (!this.error) {
      this.fileUploadInfo = {
        fileName: this.file.name,
        fileSize: fileSize,
      };
    }
    return false;
  };

  handleUpload() {
    this.checkFile().then(data => {
      if(data){
        const message = this.translate.instant('common.upload.fileNotFound')
        this.toast.error(message);
        this.fileUploadInfo = null;
      } else {
        this.callUpload();
      }
    })
  }

  callUpload(){
    this.loading = true;
    const formData: FormData = new FormData();
    formData.append('file', this.file);

    this.importFormService.importFile(this.request?.url, formData,this.request?.service)
    .subscribe(
      (data: any) => {
        this.loading = false;
        if (this.isData) {
          if (data.data.errors?.length) {
            this.listError = data.data.errors;
          } else {
            this.eventClose.emit(data);
            return;
          }
        } else {
          this.listError = data.data;
        }
        if (this.listError[0] === SUCCESS) {
          this.isError = false;
          this.eventClose.emit(true);
        } else {
          this.isError = true;
          this.toast.error(this.translate.instant('common.upload.uploadFail'));
        }
      },
      (error) => {
        this.loading = false;
        this.error = error.error.message;
        this.listError = [this.error];
        this.isError = true;
        this.toast.error(this.error);
      }
    );
  }


  checkFile(){
    return new Promise((resolve) => {
      let errorMessage = 0;
      const reader = new FileReader();
      // Wait till complete
      reader.onloadend = function(event) {
        let error = event.target?.error;
        console.log(error, event);
        if(error && error.code === 8){
          errorMessage = 1
          resolve(errorMessage);
        } else {
          resolve(errorMessage);
        }

      };
      reader.readAsText(this.file);
    });
  }
}


