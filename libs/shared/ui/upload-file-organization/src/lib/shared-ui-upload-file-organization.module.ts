import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { UploadFileComponent } from './upload-file/upload-file.component';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { NzAlertModule } from 'ng-zorro-antd/alert';
@NgModule({
  imports: [CommonModule, TranslateModule, NzButtonModule, NzSpinModule, NzUploadModule, NzAlertModule],
  declarations: [UploadFileComponent],
  exports: [UploadFileComponent],
})
export class SharedUiUploadFileOrganizationModule {}
