import {ChangeDetectorRef, Component, HostBinding, OnInit, ViewEncapsulation} from '@angular/core';
import {SpinnerService} from "@hcm-mfe/shared/common/base-service";

@Component({
  selector: 'loading-page',
  templateUrl: './loading-page.component.html',
  styleUrls: ['./loading-page.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class LoadingPageComponent implements OnInit{
  @HostBinding('class') hostClass = 'app-loading';
  showLoadPage = false;

  constructor(private spinnerService: SpinnerService,
              private cdRef: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.init();
  }

  init() {
    this.spinnerService.getSpinnerObserver().subscribe(status => {
      this.showLoadPage = status === 'start';
      this.cdRef.detectChanges();
    })
  }
}
