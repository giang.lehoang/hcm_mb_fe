import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MbInputCompactComponent } from './mb-input-compact/mb-input-compact.component';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzIconModule } from 'ng-zorro-antd/icon';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedDirectivesNumberInputModule} from "@hcm-mfe/shared/directives/number-input";

@NgModule({
  imports: [CommonModule, NzInputModule, NzSelectModule, NzRadioModule, NzIconModule, SharedUiMbSelectModule, SharedUiMbButtonModule,
    ReactiveFormsModule, FormsModule, SharedDirectivesNumberInputModule],
  declarations: [MbInputCompactComponent],
  exports: [MbInputCompactComponent]
})
export class SharedUiMbInputCompactModule {}
