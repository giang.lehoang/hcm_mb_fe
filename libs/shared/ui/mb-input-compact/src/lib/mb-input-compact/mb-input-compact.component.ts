import {Component, EventEmitter, forwardRef, Injector, Input, OnChanges, OnInit, Output} from '@angular/core';
import {
  ControlValueAccessor,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  NG_VALUE_ACCESSOR,
  NgControl,
  ValidatorFn,
  Validators
} from '@angular/forms';
import {noop} from 'rxjs';
import {SelectModal} from "@hcm-mfe/shared/data-access/models";

export class ModelInput {
  groupType?: 'noIcon' | 'icon' | 'textarea' = 'noIcon';
  type?: 'default' | 'warning' | 'error' | 'success' = 'default';
  labelText?: string;
  textMessageValue?: string;
  showIcon?: boolean = true;
  placeholder?: string = '';
  suffixIcon?: 'search' | 'down' | string = null;
  prefixIcon?: 'search' | 'down' | string = null;
  autoSize?: { [key: string]: number } = {minRows: 4, maxRows: 4};
  rows?: number;
  disable?: boolean;
  autofocus?: 'autofocus' | null;
  showFlexEnd?: boolean = true;
  isDatepicker?: boolean = false;
  dateConfig?: any;
  showError?: boolean = false;
}

@Component({
  selector: 'mb-input-compact',
  templateUrl: './mb-input-compact.component.html',
  styleUrls: ['./mb-input-compact.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => MbInputCompactComponent),
    }
  ]
})
export class MbInputCompactComponent implements OnInit, ControlValueAccessor, OnChanges { // , Validator {
  @Input() mbConfig: ModelInput = new ModelInput();
  @Input() mbLabelText: string;
  @Input() mbPlaceholderText = '';
  @Input() mbDisable = false;
  @Input() mbAutofocus = false;
  @Input() mbType: 'default' | 'warning' | 'error' | 'success';
  @Input() mbShowFlexEnd = true;
  @Input() mbIsDatepicker = false;
  @Input() mbDatepickerConfig: any;
  @Input() mbErrorDefs: { errorName: string, errorDescription: string }[];
  @Input() mbShowIconMessage = true;
  @Input() mbShowError = false;
  @Input() mbErrors: any;
  @Input() mbDataSelects: any[] = [];
  @Input() mbKeyLabel: string = 'label';
  @Input() mbKeyValue: string = 'value';
  @Input() mbOptionHeightPx = 56;
  @Input() mbOptionOverflowSize = 5;
  @Input() mbShowRadio = false;
  @Input() mbShowFormArray = true;
  @Input() mbGroupName = 'phoneNumbers';
  @Input() mbSelectControlName = 'regionCode';
  @Input() mbInputControlName = 'phoneNumber';
  @Input() mbInputMainControlName = 'phoneNumberMain';
  @Input() mbValidInput: ValidatorFn[] = [Validators.required];
  @Input() mbSelectDefaultValue = '+84';
  @Output() mbEventEmit: EventEmitter<SelectModal> = new EventEmitter<SelectModal>();

  listOfSelectedValue: any;
  radioValue: any;
  formInput: FormGroup;
  validateObjs = [];
  inputGroupGroupClasses = [];

  inputGroupClass = 'input__group';
  inputGroupGroupClass = 'input__group--group';
  inputTextClass = 'input__text';
  inputMessageClass = 'input__message';

  classGroup = {
    noIcon: 'input__group--no-icon',
    icon: 'input__group--icon'
  };

  classGroupGroup = {
    default: 'input__group--group--default',
    warning: 'input__group--group--warning',
    error: 'input__group--group--error',
    success: 'input__group--group--success',
  };

  classInput = {
    default: 'input__text--default',
    warning: 'input__text--warning',
    error: 'input__text--error',
    success: 'input__text--success'
  };

  classMessage = {
    default: 'input__message--default',
    warning: 'input__message--warning',
    error: 'input__message--error',
    success: 'input__message--success'
  };

  classIcon = {
    warning: 'warning',
    error: 'warning',
    success: 'check-circle'
  };

  onTouched: () => void = noop;
  onChange: (_: any) => void = noop;
  ngControl?: NgControl;
  iconType: string;
  value: any[];
  bsDatepicker = 'bsDatepicker';

  constructor(
    private inj: Injector,
    private fb: FormBuilder
  ) {
    this.inputGroupClass = 'input__group' + ' ';
    this.inputGroupGroupClass = 'input__group--group' + ' ';
    this.inputTextClass = 'input__text' + ' ';
    this.inputMessageClass = 'input__message ' + ' ';
  }

  ngOnInit() {
    let validateObj: { [key: string]: any } = {};
    validateObj.textMessageValue = 'Số điện thoại không được để trống';
    validateObj.showTextMessageValue = false;
    validateObj.showIconMessage = true;
    this.validateObjs = [validateObj];
    this.inputGroupGroupClasses = ['input__group--group input__group--group--default'];
    this.ngControl = this.inj.get(NgControl);
    let formControl = new FormGroup({});
    formControl.addControl(this.mbGroupName, this.fb.array([this.createPhoneNumbersFormGroup(null, null)]));
    this.formInput = this.fb.group(formControl.controls);
    this.formInput.valueChanges.subscribe(() => {
      let value = null;
      let isValid = true;
      for (let i = 0; i < (this.formInput.get(this.mbGroupName) as FormArray).controls.length; i++) {
        if (!(this.formInput.get(this.mbGroupName) as FormArray).controls[i].valid) {
          isValid = false;
          break;
        }
      }
      if (isValid) {
        this.configValue();
        value = this.formInput.value[this.mbGroupName];
      }
      this.onChange(value);
    });
  }

  ngOnChanges() {
    this.inputGroupClass = 'input__group' + ' ';
    this.inputGroupGroupClass = 'input__group--group' + ' ';
    this.inputTextClass = 'input__text' + ' ';
    this.inputMessageClass = 'input__message ' + ' ';
    this.setMbConfig();
    this.configInput();
    this.setErrorMessage();
  }

  createPhoneNumbersFormGroup(item: any, index: number): FormGroup {
    let formControl = new FormGroup({});
    formControl.addControl(this.mbSelectControlName, new FormControl(item ? item[this.mbSelectControlName] : this.mbSelectDefaultValue, Validators.required));
    formControl.addControl(this.mbInputControlName, new FormControl(item ? item[this.mbInputControlName] : null, this.mbValidInput));
    formControl.addControl(this.mbInputMainControlName, new FormControl(item ? item[this.mbInputMainControlName] : null));
    formControl.addControl('index', new FormControl(index ? index : null));
    return new FormGroup(formControl.controls);
  }

  addPhoneNumbersFormGroup() {
    let isValid = true;
    let maxI = 0;
    const idx = this.formInput.value[this.mbGroupName][0]?.index;
    this.radioValue = idx;
    for (let i = 0; i < (this.formInput.get(this.mbGroupName) as FormArray).controls.length; i++) {
      maxI += 1;
      let arrayErrors = ((this.formInput?.get(this.mbGroupName) as FormArray)?.controls[i]['controls'][this.mbInputControlName] as FormControl).errors;
      if (!(this.formInput.get(this.mbGroupName) as FormArray).controls[i].valid) {
        for (let error of this.mbErrorDefs) {
          let key = error.errorName;
          if (arrayErrors!= null && arrayErrors[key]) {
            this.validateObjs[i].textMessageValue = error.errorDescription;
          }
        }
        isValid = false;
        this.validateObjs[i].showTextMessageValue = true;
        this.inputGroupGroupClasses[i] = 'input__group--group input__group--group--error';
      } else {
        this.validateObjs[i].showTextMessageValue = false;
        this.inputGroupGroupClasses[i] = 'input__group--group input__group--group--default';
      }
    }
    if (isValid) {
      let validateObj: { [key: string]: any } = {};
      validateObj.textMessageValue = 'Số điện thoại không được để trống';
      validateObj.showTextMessageValue = false;
      validateObj.showIconMessage = true;
      if (this.mbShowError) {
        this.inputGroupGroupClasses[maxI] = 'input__group--group input__group--group--error';
      } else {
        this.inputGroupGroupClasses[maxI] = 'input__group--group input__group--group--default';
      }
      this.validateObjs = [...this.validateObjs, validateObj];
      const phoneNumber = this.formInput.get(this.mbGroupName) as FormArray;
      phoneNumber.push(this.createPhoneNumbersFormGroup(null, idx));
    }
  }

  removeOrClearPhoneNumbers(i: number) {
    const phoneNumber = this.formInput.get(this.mbGroupName) as FormArray;
    const idx = this.formInput.value[this.mbGroupName][0]?.index;
    this.radioValue = idx;
    (this.formInput.value[this.mbGroupName] as Array<any>).forEach((item, j) => {
      if (i < item.index) {
        item.index = idx - 1;
      }
    });
    if (phoneNumber.length > 1) {
      this.validateObjs.splice(i, 1);
      this.inputGroupGroupClasses.splice(i, 1);
      phoneNumber.removeAt(i);
    } else {
      let validateObj: { [key: string]: any } = {};
      validateObj.textMessageValue = 'Số điện thoại không được để trống';
      validateObj.showTextMessageValue = false;
      validateObj.showIconMessage = true;
      this.validateObjs = [validateObj];
      this.inputGroupGroupClasses[i] = 'input__group--group input__group--group--default';
      phoneNumber.reset();
    }
  }

  setMbConfig() {
    if (this.mbType) {
      this.mbConfig.type = this.mbType;
    }
    if (this.mbLabelText) {
      this.mbConfig.labelText = this.mbLabelText;
    }
    if (this.mbPlaceholderText) {
      this.mbConfig.placeholder = this.mbPlaceholderText;
    }
    if (this.mbDisable) {
      this.mbConfig.disable = this.mbDisable;
    }
    this.mbConfig.groupType = 'icon';
    if (this.mbAutofocus) {
      this.mbConfig.autofocus = 'autofocus';
    }
    if (!this.mbShowFlexEnd) {
      this.mbConfig.showFlexEnd = this.mbShowFlexEnd;
    }
    if (this.mbIsDatepicker) {
      this.mbConfig.isDatepicker = this.mbIsDatepicker;
    }
    this.mbConfig.showError = this.mbShowError;
    this.mbConfig.showIcon = this.mbShowIconMessage;
    if (this.mbDatepickerConfig) {
      this.mbConfig.dateConfig = this.mbDatepickerConfig;
    } else {
      this.mbConfig.dateConfig = {
        dateInputFormat: 'DD/MM/YYYY', returnFocusToInput: true
      }
    }
  }

  configInput() {
    if (this.mbConfig?.groupType === 'icon') {
      this.inputGroupClass += this.classGroup.icon;
    } else {
      this.inputGroupClass += this.classGroup.noIcon;
    }
    switch (this.mbConfig?.type) {
      case 'default':
        this.inputGroupGroupClass += this.classGroupGroup.default;
        this.inputTextClass += this.classInput.default;
        this.inputMessageClass += this.classMessage.default;
        break;
      case 'warning':
        this.inputGroupGroupClass += this.classGroupGroup.warning;
        this.inputTextClass += this.classInput.warning;
        this.inputMessageClass += this.classMessage.warning;
        this.iconType = this.classIcon.warning;
        break;
      case 'error':
        this.inputGroupGroupClass += this.classGroupGroup.error;
        this.inputTextClass += this.classInput.error;
        this.inputMessageClass += this.classMessage.error;
        this.iconType = this.classIcon.error;
        break;
      case 'success':
        this.inputGroupGroupClass += this.classGroupGroup.success;
        this.inputTextClass += this.classInput.success;
        this.inputMessageClass += this.classMessage.success;
        this.iconType = this.classIcon.success;
        break;
      default:
        this.inputGroupGroupClass += this.classGroupGroup.default;
        this.inputTextClass += this.classInput.default;
        this.inputMessageClass += this.classMessage.default;
        break;
    }
  }

  setErrorMessage() {
    if (this.mbErrors) {
      for (let i = 0; i < (this.formInput?.get(this.mbGroupName) as FormArray)?.controls.length; i++) {
        let arrayErrors = ((this.formInput?.get(this.mbGroupName) as FormArray)?.controls[i]['controls'][this.mbInputControlName] as FormControl).errors;
        if (!(this.formInput?.get(this.mbGroupName) as FormArray)?.controls[i].valid) {
          for (let error of this.mbErrorDefs) {
            let key = error.errorName;
            if (arrayErrors!= null && arrayErrors[key]) {
              this.validateObjs[i].showTextMessageValue = this.mbShowError;
              this.validateObjs[i].textMessageValue = error.errorDescription;
              this.inputGroupGroupClasses[i] = this.mbShowError ? 'input__group--group input__group--group--error' : 'input__group--group input__group--group--default';
            }
          }
        } else {
          this.validateObjs[i].showTextMessageValue = false;
          this.inputGroupGroupClasses[i] = 'input__group--group input__group--group--default';
        }
      }
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.mbConfig.disable = isDisabled;
  }

  writeValue(obj: any[]): void {
    this.value = obj;
    if (this.value != null && this.value.length > 0) {
      const form = this.formInput.get(this.mbGroupName) as FormArray;
      form.clear();
      let idx;
      this.value.forEach((item, index) => {
        let validateObj: { [key: string]: any } = {};
        validateObj.textMessageValue = 'Số điện thoại không được để trống';
        validateObj.showTextMessageValue = false;
        validateObj.showIconMessage = true;
        this.validateObjs = [...this.validateObjs, validateObj];
        if (item[this.mbInputMainControlName] === 1) {
          idx = index;
          this.radioValue = idx;
        }
        form.push(this.createPhoneNumbersFormGroup(item, idx));
      });
    }
  }

  /*registerOnValidatorChange(fn: () => void): void {
    this.onChange(fn);
  }

  validate(control: FormControl) {
    return null;
  }*/

  configValue() {
    (this.formInput.value[this.mbGroupName] as Array<any>).forEach((item, j) => {
      if (j === item.index) {
        item[this.mbInputMainControlName] = 1;
      } else {
        item[this.mbInputMainControlName] = 0;
      }
    });
  }

  getShowError(i): boolean {
    if ((this.formInput.get(this.mbGroupName) as FormArray).controls[i].valid) {
      this.validateObjs[i].showTextMessageValue = false;
    }
    return this.validateObjs[i].showTextMessageValue;
  }

  getShowIcon(i): boolean {
    return this.validateObjs[i].showIconMessage;
  }

  getMessageValue(i): string {
    return this.validateObjs[i].textMessageValue;
  }

  getInputGroupGroupClass(i): string {
    if ((this.formInput.get(this.mbGroupName) as FormArray).controls[i].valid) {
      this.inputGroupGroupClasses[i] = 'input__group--group input__group--group--default';
    }
    return this.inputGroupGroupClasses[i];
  }
}
