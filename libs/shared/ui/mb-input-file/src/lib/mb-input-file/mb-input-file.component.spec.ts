import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MbInputFileComponent } from './mb-input-file.component';

describe('MbInputFileComponent', () => {
  let component: MbInputFileComponent;
  let fixture: ComponentFixture<MbInputFileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MbInputFileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MbInputFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
