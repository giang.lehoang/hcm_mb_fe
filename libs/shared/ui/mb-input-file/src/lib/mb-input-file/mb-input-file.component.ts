import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-mb-input-file',
  templateUrl: './mb-input-file.component.html',
  styleUrls: ['./mb-input-file.component.scss']
})
export class MbInputFileComponent {
  file: File | undefined | null;
  @Input() fileName: string | undefined;
  @Input() disable: boolean;
  @Input() labelText:string = "File đính kèm";
  @Input() required: boolean = false;
  @Output() fileEvent = new EventEmitter<{
    file:File,
    fileName:string
    }>();
  @Output() clickPreview = new EventEmitter<boolean>();

  constructor() {
    this.disable = false;
  }

  handleFileInput(event:Event):void  {
    this.file = (event.target as HTMLInputElement).files?.item(0);
    this.fileName = (event.target as HTMLInputElement).files?.item(0)?.name;
    if(this.file && this.fileName) this.fileEvent.emit({file: this.file, fileName: this.fileName});
  }

  removeChip():void{
    if(this.disable) return;
    this.fileName = '';
    this.file = undefined;
    this.fileEvent.emit();
  }

  previewFile():void {
    this.clickPreview.emit(true);
  }

}
