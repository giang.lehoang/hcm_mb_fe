import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MbInputFileComponent} from "./mb-input-file/mb-input-file.component";
import {MatChipsModule} from "@angular/material/chips";
import {NzIconModule} from "ng-zorro-antd/icon";

@NgModule({
  imports: [CommonModule, MatChipsModule, NzIconModule],
  declarations: [MbInputFileComponent],
  exports: [MbInputFileComponent]
})
export class SharedUiMbInputFileModule {}
