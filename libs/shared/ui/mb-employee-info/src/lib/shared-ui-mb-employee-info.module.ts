import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzTableModule } from 'ng-zorro-antd/table';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzFormModule } from 'ng-zorro-antd/form';
import { SharedUiMbEmployeeDataPickerModule } from '@hcm-mfe/shared/ui/mb-employee-data-picker';
import { MbEmployeeInfoComponent } from './mb-employee-info/mb-employee-info.component';
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzIconModule} from "ng-zorro-antd/icon";

@NgModule({
    imports: [CommonModule, FormsModule,
        TranslateModule, NzToolTipModule, NzTableModule, SharedUiMbInputTextModule, SharedUiMbSelectModule, NzCardModule, NzFormModule, SharedUiMbEmployeeDataPickerModule, SharedUiMbButtonModule, NzIconModule],
  declarations: [MbEmployeeInfoComponent],
  exports: [MbEmployeeInfoComponent]
})
export class SharedUiMbEmployeeInfoModule {}
