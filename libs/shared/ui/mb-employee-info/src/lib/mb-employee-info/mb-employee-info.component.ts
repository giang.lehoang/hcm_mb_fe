import {Component, EventEmitter, Injector, Input, OnInit, Output} from '@angular/core';
import {
  IListReceivingExceptionAllowances,
  SalaryParams,
  flagStatus, EmployeeInfo, EMPLOYEE_STATUS
} from '@hcm-mfe/policy-management/data-access/models';
import {FormModalShowTreeUnitComponent} from "@hcm-mfe/shared/ui/form-modal-show-tree-unit";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {
  ListReceivingExceptionAllowancesService,
  SalaryEvolutionService, SharedService
} from '@hcm-mfe/policy-management/data-access/service';
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";


@Component({
  selector: 'app-mb-employee-info',
  templateUrl: './mb-employee-info.component.html',
  styleUrls: ['./mb-employee-info.component.scss']
})
export class MbEmployeeInfoComponent extends BaseComponent implements OnInit {
  @Input() type = 'SALARY';
  @Output() selectEmployee: EventEmitter<any> = new EventEmitter<any>();
  @Input() isDisableListReceiving = true;
  @Input() isSaveToService = false;
  @Input() functionCode:string = '';

  listStatus = flagStatus.STATUS_LIST;
  empStatus = EMPLOYEE_STATUS;
  paramSearch: SalaryParams = new SalaryParams();
  employeeId = '';
  dataResponseKindOfReward:Pick<IListReceivingExceptionAllowances, 'prAllowanceTypeName' | 'prAllowanceType'>[] = [];
  constructor(
    injector : Injector,
    private readonly listReceivingExceptionAllowancesService: ListReceivingExceptionAllowancesService,
    private sharedService: SharedService
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.listReceivingExceptionAllowancesService.getKindOfReward()
      .subscribe((res: BaseResponse )=> {
      if (res.data) {
        this.dataResponseKindOfReward = res.data;
      }
    }, (err ) => console.log(err));
  }
  onChange(event: any){
    if(event){
      this.paramSearch = event;
      this.paramSearch.statusRecord = '';
      this.paramSearch.levelName = event.levelName;
      this.paramSearch.statusEmployee = event.flagStatus;
      if(!this.isDisableListReceiving){
        this.paramSearch.prAllowanceType = null;
        this.paramSearch.orgName = '';
        this.paramSearch.orgId = undefined;
      }
      this.selectEmployee.emit(this.paramSearch);
    } else {
      this.paramSearch = new SalaryParams();
    }
    // dont use localStorage pass data, need refactor codes
    localStorage.setItem("employeeInfo", JSON.stringify(this.paramSearch));

    if(this.isSaveToService) this.sharedService.userToUpdateEmployee = event;

  }

  showModalOrg(event: any): void {
    const modal = this.modal.create({
      nzWidth: '85vw',
      nzTitle: '',
      nzContent: FormModalShowTreeUnitComponent,
      nzFooter: null,
    });
    modal.afterClose.subscribe((result) => {
      if (result?.orgName) {
        this.paramSearch.orgName = result?.orgName;
        this.paramSearch.orgId = Number(result?.orgId);
      }
    });
  }

  clearOrgInput() {
    this.paramSearch.orgName = '';
    this.paramSearch.orgId = undefined;
  }

  getDataResult(){
    this.selectEmployee.emit(this.paramSearch);
  }

  onChangeAllowanceTypeName(event){
    this.paramSearch.prAllowanceType = event.itemSelected.prAllowanceType;
  }

  onChangeStatus(event: any){
    this.paramSearch.statusRecord = event.itemSelected.key;
  }

  override triggerSearchEvent(): void {
    this.selectEmployee.emit(this.paramSearch);
  }

}
