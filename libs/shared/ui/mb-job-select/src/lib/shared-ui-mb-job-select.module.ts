import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzGridModule} from "ng-zorro-antd/grid";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
import {NzTagModule} from "ng-zorro-antd/tag";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzSwitchModule} from "ng-zorro-antd/switch";
import {TranslateModule} from "@ngx-translate/core";
import {MbJobSelectComponent} from "./mb-job-select/mb-job-select.component";

@NgModule({
  imports: [CommonModule, FormsModule, NzDropDownModule, SharedUiMbButtonModule, NzCheckboxModule, NzIconModule, NzGridModule,
    ReactiveFormsModule, SharedUiMbInputTextModule, NzToolTipModule, NzTagModule, NzButtonModule, NzSwitchModule, TranslateModule],
  declarations: [MbJobSelectComponent],
  exports: [MbJobSelectComponent]
})
export class SharedUiMbJobSelectModule {}
