import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MbHeaderFilterComponent } from './mb-header-filter.component';

describe('MbHeaderFilterComponent', () => {
  let component: MbHeaderFilterComponent;
  let fixture: ComponentFixture<MbHeaderFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MbHeaderFilterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MbHeaderFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
