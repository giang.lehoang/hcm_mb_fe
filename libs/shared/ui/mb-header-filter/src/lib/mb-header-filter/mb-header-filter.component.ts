import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TableHeader } from '@hcm-mfe/shared/data-access/models';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'mb-header-filter',
  templateUrl: './mb-header-filter.component.html',
  styleUrls: ['./mb-header-filter.component.scss']
})
export class MbHeaderFilterComponent implements OnInit {
  @Input() tableHeaders: TableHeader[] = [];
  @Output() headersEmit: EventEmitter<any> = new EventEmitter<any>();

  copyHeader: TableHeader[] = [];
  initHeaders: TableHeader[] = [];
  columnSearch: string;
  tableConfigVisible = false;

  allTableFieldChecked = false;
  allTableFieldIndeterminate = false;

  constructor(
    private translate: TranslateService,
  ) {
  }

  ngOnInit(): void {
    this.tableHeaders.forEach(item => item.isChecked = item.show);
    this.copyHeader = [...this.tableHeaders];
    this.initHeaders = JSON.parse(JSON.stringify(this.tableHeaders));
    this.checkStatusAll();
  }

  traverseToSetTitleTrans(item: TableHeader) {

  }

  changeSignalCheck(e: boolean, item: TableHeader): void {
    this.checkStatusAll();
    this.headersEmit.emit(this.copyHeader);
  }


  checkStatusAll (){
    if(this.copyHeader.every(item => item.show && item.isChecked)){
      this.allTableFieldChecked  = true;
      this.allTableFieldIndeterminate = false;
      this.allTableFieldIndeterminate = false;
    }else if(this.copyHeader.every(item => item.show && !item.isChecked)) {
      this.allTableFieldChecked  = false;
      this.allTableFieldIndeterminate = false;
    }  else{
      this.allTableFieldIndeterminate = true;
      this.allTableFieldChecked  = false;
    }
  }


  reset(): void {
    this.columnSearch = '';
    this.copyHeader = JSON.parse(JSON.stringify(this.initHeaders));
    this.checkStatusAll();
    this.headersEmit.emit(this.copyHeader);
  }

  changeAllTableTableConfigShow(e: boolean): void {
    this.copyHeader.map(item => item.isChecked = e);
    this.checkStatusAll();
    this.headersEmit.emit(this.copyHeader);
  }

  searchColumn(event:any) {
    if(event.target.value) {
      this.initHeaders.forEach((item,index) => {
        if (String(item?.title).toLowerCase().includes(String(event.target.value).toLowerCase())) {
          this.copyHeader[index].show = true;
        } else {
          this.copyHeader[index].show = false;
        }
      })
    }  else {
      this.copyHeader.forEach(item => item.show= true);
    }
    this.checkStatusAll();
  }

}
