import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NzCardModule } from 'ng-zorro-antd/card';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { FormsModule } from '@angular/forms';
import { NzSpaceModule } from 'ng-zorro-antd/space';
import { MbHeaderFilterComponent } from './mb-header-filter/mb-header-filter.component';

@NgModule({
  imports: [CommonModule, NzCardModule, SharedUiMbButtonModule, NzPopoverModule, DragDropModule, NzCheckboxModule,
    TranslateModule, SharedUiMbInputTextModule, FormsModule, NzSpaceModule],
  declarations: [MbHeaderFilterComponent],
  exports: [MbHeaderFilterComponent],
})
export class SharedUiMbHeaderFilterModule {}
