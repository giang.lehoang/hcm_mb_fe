import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import {
  Component, ElementRef, EventEmitter, HostListener, Input,
  OnDestroy,
  OnInit, Output, Renderer2, ViewChild
} from '@angular/core';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, takeUntil } from 'rxjs/operators';

import { NzMessageService } from 'ng-zorro-antd/message';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import {TargetAssessmentService} from "@hcm-mfe/goal-management/data-access/services";

interface ItemData {
  gender: string;
  name: Name;
  email: string;
}

interface Name {
  title: string;
  first: string;
  last: string;
}

@Component({
  selector: 'mb-drop-dow-list-lazy-search',
  templateUrl: './mb-drop-dow-list-lazy-search.component.html',
  styleUrls: ['./mb-drop-dow-list-lazy-search.component.scss']
})
export class MbDropDowListLazySearchComponent implements OnInit, OnDestroy {
  @ViewChild(CdkVirtualScrollViewport) virtualScroll: CdkVirtualScrollViewport | undefined;

  private destroy$ = new Subject();
  public userInputSearchOrigin = { value: '', hold: false };
  public userInputSearch = '';
  public idSelectItem: any;
  public showPopupSelect = false;
  public userInputUpdateSearch = new Subject<string>();
  public ds;

  @Input() params: any;
  @Input() urlService: string | undefined;
  @Input() accessDataSelect: any;
  @Input() labelText: string | undefined;
  @Input() placeholderText: string | undefined;
  @Input() isDropDownList: boolean | undefined;
  @Input() multiple: boolean | undefined;
  @Input() error: boolean | undefined;

  @Output() mbEventSelectItem: EventEmitter<any> = new EventEmitter<(any)>();

  ngOnInit(): void {

    this.ds.params = this.params;
    this.ds.urlService = this.urlService;
    this.ds.accessDataSelect = this.accessDataSelect;
    this.ds
      .completed()
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.nzMessage.warning('Infinite List loaded all');
      });

    // Debounce search.
    this.userInputUpdateSearch.pipe(
      debounceTime(500),
      distinctUntilChanged())
      .subscribe(value => {
        this.onRetrivedData(this.params);
      });


  }

  @HostListener('document:click', ['$event'])
  clickout(event: any) {
    if (!this.eRef.nativeElement.contains(event.target)) { // clicked outside
      this.showPopupSelect = false;
      if (this.multiple) {
        this.userInputSearch = '';
      }
    }
  }

  constructor(
    public eRef: ElementRef,
    public nzMessage: NzMessageService,
    public renderer: Renderer2,
    public targetAssessmentService: TargetAssessmentService) {
    this.ds = new ShowListPosition(this.targetAssessmentService);
  }

  onRetrivedData(paramSetup: any) {
    this.ds = new ShowListPosition(this.targetAssessmentService);
    this.ds.urlService = this.urlService;
    this.ds.accessDataSelect = this.accessDataSelect;
    this.ds.params = this.params;
    this.ds.params = { ...paramSetup, name: String(this.userInputSearch) };
    this.ds
      .completed()
      .pipe(takeUntil(this.destroy$))
      .subscribe();
  }

  selectItem(event: any, item: any) {
    if (!this.multiple) {
      this.userInputSearch = item[this.accessDataSelect.name];
      this.showPopupSelect = false;
    }
    this.mbEventSelectItem.emit(item);
    this.idSelectItem = item;
    if (this.isDropDownList) {
      this.userInputSearchOrigin.value = this.userInputSearch;
      this.userInputSearchOrigin.hold = false;
    }
  }

  clearDataSelect() {
    this.userInputSearch = '';
    this.idSelectItem = undefined;
    this.mbEventSelectItem.emit({});
    this.onRetrivedData(this.params);
  }

  showModal() {
    this.showPopupSelect = true;
    if (this.isDropDownList && !this.userInputSearchOrigin.hold) {
      this.userInputSearchOrigin.value = this.userInputSearch;
      this.userInputSearchOrigin.hold = true;
    }
  }

  ngModelChangeInput(event: any) {
    this.showModal();
    this.userInputUpdateSearch.next(event);
  }

  changeInput(event: any) {
    if (this.isDropDownList && !this.multiple) {
      this.userInputSearch = this.userInputSearchOrigin.value;
      this.userInputSearchOrigin.hold = false;
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next(null);
    this.destroy$.complete();
    this.userInputUpdateSearch.next('');
    this.userInputUpdateSearch.complete();
  }
}

class ShowListPosition extends DataSource<ItemData> {
  private pageSize = 20;
  private cachedData: ItemData[] = [];
  private fetchedPages = new Set<number>();
  private dataStream = new BehaviorSubject<ItemData[]>(this.cachedData);
  private complete$ = new Subject<void>();
  private disconnect$ = new Subject<void>();
  private totalPage$ = 0;
  private isLoading: boolean | undefined;
  public params: any;
  public urlService: string | undefined;
  public accessDataSelect: { dataRes: string | number; } | undefined;
  heightCustomRender = '';


  constructor(private targetAssessmentService: TargetAssessmentService) {
    super();
  }

  completed(): Observable<void> {
    return this.complete$.asObservable();
  }

  connect(collectionViewer: CollectionViewer): Observable<ItemData[]> {
    this.setup(collectionViewer);
    return this.dataStream;
  }

  disconnect(): void {
    this.disconnect$.next();
    this.disconnect$.complete();
  }

  private setup(collectionViewer: CollectionViewer): void {
    this.fetchPage(0);
    collectionViewer.viewChange
      .pipe(takeUntil(this.complete$), takeUntil(this.disconnect$))
      .subscribe((range) => {
        if (this.getPageForIndex(range.end) > this.totalPage$) {
          this.complete$.next();
          this.complete$.complete();
        } else {
          const endPage = this.getPageForIndex(range.end);
          this.fetchPage(endPage + 1);
        }
      });
  }

  private getPageForIndex(index: number): number {
    return Math.floor(index / this.pageSize);
  }

  private fetchPage(page: number): void {
    if (this.fetchedPages.has(page)) {
      if (page === 0) {
        this.dataStream.next(this.cachedData);
      }
      return;
    }
    this.isLoading = true;
    this.fetchedPages.add(page);

    this.targetAssessmentService.getLazyLoadDataDropDownList({ ...this.params, page: page, size: 20 }, this.urlService)
      .pipe(catchError(() => of({ results: [] })))
      .subscribe((res) => {
        if (res.data) {
          this.totalPage$ = res?.data?.totalPages;
          this.cachedData?.splice(
            page * this.pageSize,
            this.pageSize,
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            ...res?.data[this.accessDataSelect.dataRes]
          );
          this.heightCustomRender = this.cachedData.length < 6 ? `${this.cachedData.length * 5}em` : '250px'
          this.dataStream.next(this.cachedData);
        }
        this.isLoading = false;
      }, () => {
        this.isLoading = false;
      });
  }
}
