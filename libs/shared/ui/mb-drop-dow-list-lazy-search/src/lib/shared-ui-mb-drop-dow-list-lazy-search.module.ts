import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MbDropDowListLazySearchComponent} from "./mb-drop-dow-list-lazy-search/mb-drop-dow-list-lazy-search.component";
import {NzInputModule} from "ng-zorro-antd/input";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {ScrollingModule} from "@angular/cdk/scrolling";
import {NzListModule} from "ng-zorro-antd/list";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NzSkeletonModule} from "ng-zorro-antd/skeleton";
import {NzIconModule} from "ng-zorro-antd/icon";

@NgModule({
  imports: [CommonModule, NzInputModule, SharedUiMbButtonModule, ScrollingModule, NzListModule, FormsModule, ReactiveFormsModule, NzSkeletonModule, NzIconModule],
  declarations: [MbDropDowListLazySearchComponent],
  exports: [MbDropDowListLazySearchComponent],
})
export class SharedUiMbDropDowListLazySearchModule {}
