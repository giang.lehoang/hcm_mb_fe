import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { NzIconModule } from 'ng-zorro-antd/icon';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NzTagModule} from "ng-zorro-antd/tag";
import {NzInputModule} from "ng-zorro-antd/input";
import {NzAutocompleteModule} from "ng-zorro-antd/auto-complete";
import {NzFormModule} from "ng-zorro-antd/form";
import { MbMultiDataPickerComponent } from './mb-multi-data-picker/mb-multi-data-picker.component';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';

@NgModule({
  imports: [CommonModule, TranslateModule, NzIconModule, SharedUiMbInputTextModule, FormsModule, ReactiveFormsModule, NzTagModule, NzInputModule, NzAutocompleteModule, NzFormModule, NzToolTipModule],
  declarations: [MbMultiDataPickerComponent],
  exports: [MbMultiDataPickerComponent],
})
export class SharedUiMbMultiDataPickerModule {}
