import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { InputBoolean } from 'ng-zorro-antd/core/util';
import { NzAutocompleteComponent } from 'ng-zorro-antd/auto-complete';
import { MbInputTextComponent } from '@hcm-mfe/shared/ui/mb-input-text';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { CommonUtils } from '@hcm-mfe/shared/core';

// Component chỉ sử dụng để custom
@Component({
  selector: 'mb-multi-data-picker',
  templateUrl: './mb-multi-data-picker.component.html',
  styleUrls: ['./mb-multi-data-picker.component.scss']
})
export class MbMultiDataPickerComponent implements OnInit, OnChanges {
  @Input() mbDisableButtonSearch = false;
  @Input() mbLabelText: string;
  @Input() mbData: any[] = [];
  @Input() @InputBoolean() mbCanText = false;
  @Input() mbLabel: string;
  @Input() mbPlaceholder: string = 'common.label.search'
  @Input() @InputBoolean() mbDisable = false;
  @Input() @InputBoolean() isMultiSelect = false;
  @Input() mbLabelCustom: string;
  @Input() selected!: any;
  @Output() selectedChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() inputChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() oldInputValue: EventEmitter<any> = new EventEmitter<any>();
  @Output() afterCloseModal: EventEmitter<any> = new EventEmitter<any>();
  @Input() mbResultTpl: TemplateRef<any> = null;
  @Input() mbResultHeader: TemplateRef<any> = null;
  @Input() mbResultFooter: TemplateRef<any> = null;
  @Input() mbModalWidth: number = window.innerWidth > 767 ? window.innerWidth / 2 : window.innerWidth;

  @Input() mbErrorDefs: { errorName: string, errorDescription: string }[];
  @Input() mbShowError = false;
  @Input() mbErrors: any;
  @Input() mbType: 'default' | 'warning' | 'error' | 'success' = 'default';

  @Input() mbAutocomplete: NzAutocompleteComponent;

  textMessageValue?: string;

  public modalRef: NzModalRef;

  constructor(
    private modalService: NzModalService,
  ) {
  }

  @ViewChild('searchInput') searchInput: MbInputTextComponent;
  @ViewChild('inputElement') inputElement: ElementRef;

  inputSearch: string;
  inputVisible: boolean = false;

  ngOnInit(): void {
    this.inputSearch = this.mbLabelCustom ? this.mbLabelCustom : (this.mbLabel ? this.selected[this.mbLabel] : '');
  }

  clearResult() {
    if(this.mbDisable) {
      return;
    }
    this.inputSearch = null;
    this.inputChange.emit(null)
    this.selected = null;
    this.selectedChange.emit(null)
  }

  selectItem(item) {
    this.selected = item;
    this.selectedChange.emit(item);
    if (!this.isMultiSelect) {
      this.inputSearch = this.mbLabelCustom ? this.mbLabelCustom : this.selected[this.mbLabel];
      if (this.modalRef)
        this.modalRef.destroy(true)
    } else {
      this.mbAutocomplete.setVisibility();
      setTimeout(()=>{
        this.inputSearch = null;
        this.inputChange.emit(null)
      },0);
    }
  }

  openResult() {
    if(this.mbDisable || this.mbDisableButtonSearch) {
      return;
    }
    this.modalRef = this.modalService.create({
      nzWidth: this.mbModalWidth,
      nzClosable: true,
      nzBodyStyle: {padding: '0'},
      nzTitle: this.mbResultHeader,
      nzContent: this.mbResultTpl,
      nzFooter: this.mbResultFooter,
    });

    this.modalRef.afterClose.subscribe(res => {
      this.afterCloseModal.emit();
      if (res) {
        this.inputSearch = this.mbLabelCustom ? this.mbLabelCustom : this.selected[this.mbLabel];
      } else {
        this.inputSearch = !this.mbCanText ?
          this.inputSearch :
          this.mbLabelCustom ? this.mbLabelCustom :
            this.selected ? (this.mbLabel ? this.selected[this.mbLabel] : '') : '';
      }
    });
  }

  onBlur() {
    if(this.isMultiSelect) {
      this.oldInputValue.emit(this.inputSearch);
    } else {
      if (this.searchInput.mbAutocomplete?.isOpen) {
        this.oldInputValue.emit(this.inputSearch);
      }
    }
    // if (this.selected) {
    //     this.inputSearch = this.mbLabelCustom ? this.mbLabelCustom : this.selected[this.mbLabel];
    // }
    // else this.inputSearch = '';
  }

  onInputChange($event) {
    this.inputChange.emit($event);
  }

  closeModal() {
    if (this.selected) {
      this.inputSearch = '';
      this.inputSearch = this.mbLabelCustom ? this.mbLabelCustom : this.selected[this.mbLabel];
    } else this.inputSearch = '';
    this.modalRef.destroy()
  }

  handleClose(item: NzSafeAny) {
    this.selected = [...new Set(this.selected?.filter(el => el.employeeId !== item.employeeId))];
    this.selectedChange.emit(this.selected);
  }

  showInput() {
    // this.inputVisible = true;
    // setTimeout(()=>{ // this will make the execution after the above boolean has changed
    //   this.inputElement.nativeElement.focus();
    // },0);
  }

  ngOnChanges() {
    this.setErrorMessage();
  }

  setErrorMessage() {
    if (this.mbErrors) {
      for (const error of this.mbErrorDefs) {
        const key = error.errorName;
        if (this.mbErrors[key]) {
          this.textMessageValue = error.errorDescription;
        }
      }
    }
  }
}
