import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NzModalApproveCustomComponent } from './popup/approve-popup-component';
import { NzModalDeleteCustomComponent } from './popup/delete-popup-component';
import { NzModalConfirmCustomComponent } from './popup/confirm-popup-component';
import { TranslateModule } from '@ngx-translate/core';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { PopupService } from './popup/popup.service';

@NgModule({
  imports: [CommonModule, TranslateModule, NzButtonModule],
  declarations: [NzModalApproveCustomComponent, NzModalConfirmCustomComponent, NzModalDeleteCustomComponent],
  exports: [NzModalApproveCustomComponent, NzModalConfirmCustomComponent, NzModalDeleteCustomComponent],
  providers: [PopupService]
})
export class SharedUiPopupModule {}
