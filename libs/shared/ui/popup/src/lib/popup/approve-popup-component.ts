import { Component } from '@angular/core';
import { PopupService } from './popup.service';

@Component({
  selector: 'nz-modal-custom-approve-component',
  template: `
    <div class="main">
      <img alt='' src="assets/src/images/icon/tranfer.svg" />
      <div class="title">
        <span>{{'shared.popupConfirm.approve.title' | translate}}</span>
      </div>
      <div class="footer">
        <button nz-button nzType="primary" class="cancel" (click)="clickButton(false)">{{'shared.popupConfirm.approve.cancel' | translate}}</button>
        <button nz-button nzType="primary" class="ok-approve" (click)="clickButton(true)">{{'shared.popupConfirm.approve.ok' | translate}}</button>
      </div>
    </div>
  `,
  styleUrls: ['./popup.custom.component.scss'],
})
export class NzModalApproveCustomComponent {
  constructor(private popupService: PopupService) {}

  clickButton(value: boolean) {
    if (value) {
      this.popupService.modalDelete.triggerOk();
    } else {
      this.popupService.modalDelete.destroy();
    }
  }
}
