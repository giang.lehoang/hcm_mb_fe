import { Component} from '@angular/core';
import { PopupService } from './popup.service';

@Component({
  selector: 'nz-modal-custom-component',
  template: `
    <div class="main">
      <img alt='' src='assets/images/icon/popup-delete.svg' />
      <div class="title">
        <span>{{'shared.popupConfirm.delete.title' | translate}}</span>
      </div>
      <div class="footer">
        <button nz-button nzType="primary" class="cancel" (click)="clickButton(false)">{{'shared.popupConfirm.delete.cancel' | translate}}</button>
        <button nz-button nzType="primary" class="ok" (click)="clickButton(true)">{{'shared.popupConfirm.delete.delete' | translate}}</button>
      </div>
    </div>
  `,
  styleUrls: ['./popup.custom.component.scss']
})
export class NzModalDeleteCustomComponent {
  constructor(private popupService: PopupService) {}

  clickButton(value: boolean) {
    if (value) {
      this.popupService.modalDelete.triggerOk();
    } else {
      this.popupService.modalDelete.destroy();
    }
  }
}
