import { EventEmitter, Injectable, OnDestroy } from '@angular/core';
import { NzModalService, NzModalRef } from 'ng-zorro-antd/modal';
import {
  NzModalApproveCustomComponent,
  NzModalConfirmCustomComponent,
  NzModalDeleteCustomComponent
} from '@hcm-mfe/shared/ui/popup';
@Injectable()
export class PopupService implements OnDestroy {
  onClick: EventEmitter<boolean> = new EventEmitter<boolean>();
  modalDelete: NzModalRef;
  constructor(private modal: NzModalService) {}
  ngOnDestroy(): void {
    this.onClick.unsubscribe();
  }

  showModal(onOK) {
    this.modalDelete = this.modal.create({
      nzTitle: null,
      nzStyle: {
        width: '414px',
        height: '321px',
      },
      nzWrapClassName: 'delete-popup-container',
      nzContent: NzModalDeleteCustomComponent,
      nzBodyStyle: {
        padding: '0px',
        border: 'none',
      },
      nzOnOk: onOK,
      nzFooter: null,
    });
  }

  showApprove(onOk){
    this.modalDelete = this.modal.create({
      nzTitle: null,
      nzStyle: {
        width: '414px',
        height: '223px',
      },
      nzWrapClassName: 'delete-popup-container',
      nzContent: NzModalApproveCustomComponent,
      nzBodyStyle: {
        padding: '0px',
        border: 'none',
      },
      nzOnOk: onOk,
      nzFooter: null,
    });
  }

  showConfirm(onOk, data){
    this.modalDelete = this.modal.create({
      nzTitle: null,
      nzStyle: {
        width: '414px',
        height: '223px',
      },
      nzWrapClassName: 'delete-popup-container',
      nzContent: NzModalConfirmCustomComponent,
      nzBodyStyle: {
        padding: '0px',
        border: 'none',
      },
      nzComponentParams: {
        InputData: data
      },
      nzOnOk: onOk,
      nzFooter: null,
    });
  }
}
