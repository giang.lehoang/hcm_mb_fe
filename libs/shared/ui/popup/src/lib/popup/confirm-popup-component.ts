import { Component, Input } from '@angular/core';
import { PopupService } from './popup.service';

@Component({
  selector: 'nz-modal-custom-approve-component',
  template: `
    <div class="main">
      <div class="title">
        <span>{{'shared.popupConfirm.confirm.title' | translate}}</span>
      </div>
      <div class="body">
        <p>Bạn xác nhận giao bộ chỉ tiêu <span style="font-weight: bold">{{ InputData.sotName }}</span> cho nhân viên <span style="font-weight: bold">{{ InputData.fullName }}</span> ?</p>
      </div>
      <div class="footer">
        <button nz-button nzType="primary" class="ok-approve" (click)="clickButton(true)">{{'shared.popupConfirm.confirm.confirm' | translate}}</button>
        <button nz-button nzType="primary" class="cancel" (click)="clickButton(false)">{{'shared.popupConfirm.confirm.cancel' | translate}}</button>
      </div>
    </div>
  `,
  styleUrls: ['./popup.custom.component.scss'],
})
export class NzModalConfirmCustomComponent {
  @Input() InputData: any;
  constructor(private popupService: PopupService) {
  }

  clickButton(value: boolean) {
    if (value) {
      this.popupService.modalDelete.triggerOk();
    } else {
      this.popupService.modalDelete.destroy();
    }
  }
}
