import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EmployeeDataPickerComponent} from "./employee-data-picker/employee-data-picker.component";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbDataPickerModule} from "@hcm-mfe/shared/ui/mb-data-picker";
import {NzAutocompleteModule} from "ng-zorro-antd/auto-complete";
import {NzListModule} from "ng-zorro-antd/list";
import {NzAvatarModule} from "ng-zorro-antd/avatar";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzFormModule} from "ng-zorro-antd/form";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {NzSpinModule} from "ng-zorro-antd/spin";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzEmptyModule} from "ng-zorro-antd/empty";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MultiEmployeeDataPickerComponent} from "./multi-employee-data-picker/multi-employee-data-picker.component";
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";
import { SharedUiMbMultiDataPickerModule } from '@hcm-mfe/shared/ui/mb-multi-data-picker';

@NgModule({
  imports: [CommonModule, SharedUiMbSelectModule, TranslateModule, SharedUiMbDataPickerModule, NzAutocompleteModule,
    NzListModule, NzAvatarModule, SharedUiMbButtonModule, NzFormModule, SharedUiMbInputTextModule, NzSpinModule,
    NzIconModule, NzEmptyModule, FormsModule, ReactiveFormsModule, NzCheckboxModule, SharedUiMbMultiDataPickerModule],
  declarations: [EmployeeDataPickerComponent, MultiEmployeeDataPickerComponent],
  exports: [EmployeeDataPickerComponent, MultiEmployeeDataPickerComponent],
})
export class SharedUiEmployeeDataPickerModule {}
