import {
  Component,
  EventEmitter,
  forwardRef,
  Injector,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import { InputBoolean } from 'ng-zorro-antd/core/util';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, NgControl } from '@angular/forms';
import { noop, Observable, of, Subscription } from 'rxjs';
import * as _ from 'lodash';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { catchError, map } from 'rxjs/operators';
import {BaseResponse, Category, EmployeeDetail, Pagination} from "@hcm-mfe/shared/data-access/models";
import {MbDataPickerComponent} from "@hcm-mfe/shared/ui/mb-data-picker";
import {StaffInfoService} from "@hcm-mfe/shared/core";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {Constant} from "@hcm-mfe/personal-tax/data-access/common";
import {PersonalInfoService} from "@hcm-mfe/dashboard-manager/data-access/services";
import {PersonalInfo} from "@hcm-mfe/personal-tax/data-access/models";
import {StringUtils} from "@hcm-mfe/shared/common/utils";
import {NzSafeAny} from "ng-zorro-antd/core/types";

@Component({
  selector: 'employee-data-picker',
  templateUrl: './employee-data-picker.component.html',
  styleUrls: ['./employee-data-picker.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => EmployeeDataPickerComponent)
    }
    /*,
      {
        provide: NG_VALIDATORS,
        useExisting: forwardRef(() => MbInputTextComponent),
        multi: true
      }*/
  ]
})
export class EmployeeDataPickerComponent implements OnInit, ControlValueAccessor, OnDestroy {
  @Input() mbLabelText: string | undefined;

  @Input() mbDisable = false;
  @Input() selected: EmployeeDetail | undefined;
  @Input() @InputBoolean() mbCanText = false;
  @Input() @InputBoolean() showEmpTypeSelect = false;
  @Output() selectedChange: EventEmitter<NzSafeAny> = new EventEmitter<NzSafeAny>();

  @Input() mbErrorDefs: { errorName: string, errorDescription: string }[] | undefined;
  @Input() mbShowError = false;
  @Input() mbErrors: NzSafeAny;
  @Input() mbType: 'default' | 'warning' | 'error' | 'success' = 'default';
  @Input() status: 'ALL' | 'ACTIVE' | 'WORKING' | undefined;
  @Input() empTypes: string[] = [];
  @Input() scope: string | undefined;
  @Input() functionCode: string | undefined;
  @Input() getEmail = false;


  @Input() @InputBoolean() isCheckEmployeeHasTaxNumber = false;
  @Input() @InputBoolean() isCheckEmployeeNotHaveTaxNumber = false;
  @Input() @InputBoolean() isRemoveHireOutsideEmpType = false;

  empTypeCode: string | undefined = undefined;
  empTypeCodeList: Category[] = [];

  data: EmployeeDetail[] = [];

  count = 0;
  loading = false;
  onTouched: () => void = noop;
  onChange: (_: NzSafeAny) => void = noop;
  ngControl?: NgControl;
  avtBase64Default = 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';
  avtBase64 = 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';
  subs: Subscription[] = [];
  listAvatar: NzSafeAny[] = [];
  listIsLoading: boolean[] = [];

  oldInputSearch: string | undefined = undefined;
  keyWordSearch: string | undefined = undefined;

  pagination = new Pagination();

  @ViewChild('dataPicker') dataPicker: MbDataPickerComponent | undefined;

  constructor(
    private inj: Injector,
    private staffService: StaffInfoService,
    private personalInfoService: PersonalInfoService,
    private toastrService: ToastrService,
    private translateService: TranslateService
  ) {
    this.getEmpTypeCodes();
  }

  ngOnInit(): void {
    this.ngControl = this.inj.get(NgControl);
    this.onInputChange = _.debounce(this.onInputChange, 200);
  }

  ngOnDestroy(): void {
    this.listAvatar = [];
    this.listIsLoading = [];
    this.subs.forEach(sub => sub.unsubscribe());
  }

  getEmployeeData(keyword: string) {
    let param: NzSafeAny = this.pagination.getCurrentPage();
    if (keyword) param = { ...param, ...{ keyword: keyword } };
    if (this.empTypeCode) param = { ...param, ...{ empTypeCode: this.empTypeCode } };
    param.status = this.status ? this.status : 'ALL';
    param.scope = this.scope ? this.scope : '';
    param.empTypes = this.empTypes;
    param.functionCode = this.functionCode ? this.functionCode : '';
    if(this.getEmail){
      param.getEmail = true;
    }
    this.loading = true;
    this.listAvatar = [];
    this.listIsLoading = [];
    this.staffService.getEmployeeData(param).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.loading = false;
        this.data = [...this.data, ...res.data.listData];
        this.count = res.data.count;
        this.getListAvatar();
        this.pagination.pageNumber++;
      } else this.data = [];
    }, () => {
      this.loading = false;
      this.data = [];
    });
  }

  onInputChange(value: string): void {
    this.resetDataSearch();
    if (value && value.length > 1)
      this.getEmployeeData(value);
  }

  keyUpEnterSelectItem($event: NzSafeAny, item: NzSafeAny) {
    if ($event.source.selected) {
      this.selectItem(item);
    }
  }

  getEmpTypeCodes() {
    this.staffService.getCatalog(Constant.CATALOGS.DOI_TUONG_CV).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        if (this.isRemoveHireOutsideEmpType) {
          this.empTypeCodeList = res.data.filter((item: NzSafeAny) => item.label.toLowerCase() !== 'Thuê ngoài'.toLowerCase());
        } else {
          this.empTypeCodeList = res.data;
        }
        if(this.empTypes && this.empTypes.length > 0){
          this.empTypeCodeList = this.empTypeCodeList.filter((item: NzSafeAny) => this.empTypes.includes(item.value));
        }
      }
    });
  }

  change(personalInfo: EmployeeDetail) {
    this.selectedChange.emit(personalInfo);
    this.writeValue(personalInfo);
    this.onChange(personalInfo);
  }

  selectItem(personalInfo: EmployeeDetail) {
    // check xem nhân viên đã có mst hay chưa
    if (this.isCheckEmployeeHasTaxNumber) {
      this.validateEmployeeBeforeChoose(personalInfo).subscribe((res) => {
        if (res) {  // nv chua co mst
          return;
        } else {
          this.doSelect(personalInfo);
        }
      })
    } else if(this.isCheckEmployeeNotHaveTaxNumber) { // nếu nv chưa có mst thì cho phép đăng ký cấp mới mst
      this.validateEmployeeBeforeChoose(personalInfo).subscribe((res) => {
        if (res) {  // nv chua co mst
          this.doSelect(personalInfo);
        }
      })
    } else {
      this.doSelect(personalInfo);
    }
  }

  doSelect(personalInfo: EmployeeDetail) {
    this.dataPicker?.selectItem(personalInfo);
    this.writeValue(personalInfo);
    this.onChange(personalInfo);
  }

  registerOnChange(fn: NzSafeAny): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: NzSafeAny): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.mbDisable = isDisabled;
  }

  writeValue(obj: NzSafeAny): void {
    this.selected = obj;
    if (this.dataPicker) {
      this.dataPicker.inputSearch = this.selected ? this.selected.fullName + (this.selected.employeeCode ? ('-' + this.selected.employeeCode) : '') : '';
    }
  }

  getListAvatar() {
    this.data.forEach((item, index) => {
      this.getAvatar(item.employeeId, index);
    });
  }

  getAvatar(employeeId: number, index: number) {
    this.listIsLoading[index] = true;
    this.subs.push(
      this.personalInfoService.getAvatar(employeeId).subscribe(res => {
        this.listAvatar[index] = res?.data ? 'data:image/jpg;base64,' + res?.data : null;
        this.listIsLoading[index] = false;
      }, () => {
        this.listAvatar[index] = this.avtBase64Default;
        this.listIsLoading[index] = false;
      })
    );
  }

  showModal() {
    this.resetDataSearch();
    this.keyWordSearch = this.oldInputSearch + '';
    this.getEmployeeData(this.keyWordSearch);
    this.dataPicker?.openResult();
  }

  getEmployeeDataInModal() {
    this.getEmployeeData(this.keyWordSearch ?? '');
  }

  public resetDataSearch() {
    this.data = [];
    this.count = 0;
    this.pagination = new Pagination();
    this.pagination.pageNumber = 1;
  }

  private validateEmployeeBeforeChoose(personalInfo: EmployeeDetail): Observable<boolean> {
    const employeeId = personalInfo?.employeeId;
    return this.personalInfoService.getPersonalInfo(employeeId).pipe(
      map((res) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          const personalInfo: PersonalInfo = res?.data;
          if (!!personalInfo && StringUtils.isNullOrEmpty(personalInfo?.taxNo ?? '')) {
            if (this.isCheckEmployeeHasTaxNumber) {
              this.toastrService.warning(this.translateService.instant('shared.notification.employeeNoTax'));
            }
            return true;
          }
          if (this.isCheckEmployeeNotHaveTaxNumber) {
            this.toastrService.warning(this.translateService.instant('shared.notification.employeeHasTax'));
          }
          return false;
        } else {
          this.toastrService.error(res?.message);
          return true;
        }
      }),
      catchError((err) => {
        this.toastrService.error(err?.message);
        return of(true);
      })
    )
  }

}
