import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MbTextLabelComponent } from './mb-text-label/mb-text-label.component';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzFormModule } from 'ng-zorro-antd/form';

@NgModule({
  imports: [CommonModule, SharedUiMbInputTextModule, FormsModule, ReactiveFormsModule, NzFormModule],
  declarations: [MbTextLabelComponent],
  exports: [MbTextLabelComponent],
})
export class SharedUiMbTextLabelModule {}
