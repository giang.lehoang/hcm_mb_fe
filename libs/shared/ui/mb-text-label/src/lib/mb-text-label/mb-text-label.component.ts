import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {DatePipe} from "@angular/common";
import * as moment from 'moment';
import { FormatCurrencyPipe } from '@hcm-mfe/shared/pipes/format-currency';

@Component({
  selector: 'mb-text-label',
  templateUrl: './mb-text-label.component.html',
  styleUrls: ['./mb-text-label.component.scss']
})
export class MbTextLabelComponent implements OnInit, OnChanges {
  @Input() mbValueType: 'string' | 'number' | 'date' | 'currency' | any = 'string';
  @Input() mbLabelText: any;
  @Input() mbLabelValue: any;
  @Input() mbShowEdit = false;
  @Input() mbDateFormat: string = '';
  @Input() mbShowLabelText = true;
  @Input() mbShowLabelValue = true;
  @Input() mbPlaceholderText = '';
  @Input() mbPrefixIcon: 'search' | 'calendar' | 'user';
  @Input() mbSuffixIcon: 'down' | 'eye' | 'key';

  valueShow: any;
  dateFormatShow: any;

  constructor(
    private datePipe: DatePipe,
    private currencyFormat: FormatCurrencyPipe
  ) {
  }

  ngOnInit(): void {

  }

  ngOnChanges() {
    this.configValue();
  }

  configValue() {
    this.dateFormatShow = this.mbDateFormat;
    if (this.mbDateFormat === '') {
      this.dateFormatShow = this.mbShowEdit ? 'DD/MM/YYYY' : 'dd/MM/yyyy';
    }
    this.valueShow = this.mbLabelValue;
    if (this.mbValueType === 'date') {
      this.valueShow = this.mbShowEdit ? moment(this.mbLabelValue, this.dateFormatShow).toDate() : this.mbLabelValue;
    }
    if (this.mbValueType === 'currency') {
      this.valueShow = this.mbShowEdit ? this.mbLabelValue : this.currencyFormat.transform(this.mbLabelValue);
    }
  }

}
