import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MbCollapseComponent } from './mb-collapse/mb-collapse.component';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { NzSpaceModule } from 'ng-zorro-antd/space';
import { TranslateModule } from '@ngx-translate/core';
import { PortalModule } from '@angular/cdk/portal';
import {NzIconModule} from "ng-zorro-antd/icon";

@NgModule({
  imports: [CommonModule, NzCollapseModule, NzSpaceModule, TranslateModule, PortalModule, NzIconModule],
  declarations: [MbCollapseComponent],
  exports: [MbCollapseComponent]
})
export class SharedUiMbCollapseModule {}
