import {
  ApplicationRef,
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  EventEmitter,
  Injector,
  Input,
  OnInit,
  Output,
  QueryList,
  TemplateRef,
  ViewChildren
} from '@angular/core';
import { ComponentPortal } from '@angular/cdk/portal';
import { SessionService } from "@hcm-mfe/shared/common/store";
import {PanelOption} from "@hcm-mfe/shared/data-access/models";

@Component({
  selector: 'mb-collapse',
  templateUrl: './mb-collapse.component.html',
  styleUrls: ['./mb-collapse.component.scss']
})
export class MbCollapseComponent implements OnInit {
  @Input() mbPanels: PanelOption[] = []; // multiPanels = true
  @Input() mbExtra?: string | TemplateRef<void> | any;
  @Output() mbActiveChange: EventEmitter<any> = new EventEmitter<any>();

  @ViewChildren("panel") panels :QueryList<any>;

  constructor(
    private cfr: ComponentFactoryResolver,
    private appRef: ApplicationRef,
    private injector: Injector,
    private sessionService: SessionService,
  ) {
  }

  ngOnInit(): void {
    this.mbPanels = [...this.mbPanels.filter(element => !element.code || this.sessionService.getSessionData(`FUNCTION_${element.code}`)?.view)];
    this.mbPanels.forEach(panel => panel.panelComponentPortal = new ComponentPortal(panel.panelComponent));
  }

  setReference(ref: ComponentRef<any> | any, id: any) {
    let panel = this.mbPanels.find(panel => panel.id === id);
    panel.instance = ref.instance;
  }

  activeChange($event) {
    this.mbActiveChange.emit($event)
  }

  getInstance(panel: PanelOption) {
    return panel.instance;
  }

}
