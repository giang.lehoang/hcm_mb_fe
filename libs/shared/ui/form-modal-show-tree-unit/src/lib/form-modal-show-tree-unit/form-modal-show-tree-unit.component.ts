import {Component, Injector, Input, OnInit} from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { NzResizeEvent } from 'ng-zorro-antd/resizable';
import {NzFormatEmitEvent, NzTreeNodeOptions} from 'ng-zorro-antd/tree';
import {IDataResponse, InitScreenSearch, ITreeOrganization, OrganizationNode} from "@hcm-mfe/shared/data-access/models";
import {userConfig} from "@hcm-mfe/shared/common/constants";
import {DataService} from "@hcm-mfe/shared/data-access/services";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {NzTreeNode} from "ng-zorro-antd/core/tree";
import { forkJoin } from 'rxjs';
import {
  Area,
  Job,
  ListLegalBranch,
  LookupValue,
  RegionType,
  ResponseEntity, ResponseEntityData, Type
} from '@hcm-mfe/model-organization/data-access/models';
import { maxInt32 } from '@hcm-mfe/shared/common/enums';


@Component({
  selector: 'app-form-modal-show-tree-unit',
  templateUrl: './form-modal-show-tree-unit.component.html',
  styleUrls: ['./form-modal-show-tree-unit.component.scss']
})
export class FormModalShowTreeUnitComponent extends BaseComponent implements OnInit {
  @Input() isShowCheckbox: boolean | undefined;
  @Input() mapChecked: Map<number, ITreeOrganization> = new Map<number, ITreeOrganization>();
  @Input() isAuthor: boolean = false;
  @Input() scope = '';
  @Input() resourceCode = '';
  @Input() isShowChkOrg = false;
  isLoading: boolean = false;
  col: number = 6;
  id: number = -1;
  activeNode: string | null = '';
  orgNameModal: string = '';
  orgName: string = '';
  pathResult: string = '';
  pathResult2: string = ''
  orgMap: Map<number, ITreeOrganization> = new Map<number, ITreeOrganization>();
  checked: boolean | undefined = false;
  listTree: OrganizationNode[] = [];
  nodeRoot: OrganizationNode;
  treeOrg: OrganizationNode[] = [];
  isSearchAdv= false;
  areas: Area[] = [];
  types: Type[] = [];
  listOrgGroup = [
    {key:"CN",value:"CN"},
    {key:"HO",value:"HO"},
  ];
  listOrgManager:ITreeOrganization[]=[];
  listOrgTT1:ITreeOrganization[]=[];
  listOrgTT2:ITreeOrganization[]=[];
  listOrgTT3:ITreeOrganization[]=[];
  isShowAll = false;
  flagChk = false;

  objectSearch: InitScreenSearch = {
    orgId: '',
    flagStatus: '',
    inputType: '',
    page: 0,
    size: 15,
  };

  paginationModal = {
    size: 0,
    total: 0,
    pageIndex: 0,
    numberOfElements: 0,
  };

  params = {
    id: '',
    scope: '',
    resourceCode: '',
    page: 0,
    size: 10,
    is_author: false,
    listStatus:'',
  };

  listSelectedOrg: ITreeOrganization[] | undefined = [];
  constructor(readonly dataService: DataService, readonly modalRef: NzModalRef, injector:Injector) {
    super(injector);
  }


  selectedNodeId(id: any, name?: any): void {
    this.checked = false;
    if (this.mapChecked && this.mapChecked.size <= 0) {
      this.orgMap.clear();
    }
    this.params.id = id ? id : '';
    this.params.scope = this.scope;
    this.params.resourceCode = this.resourceCode;
    this.orgNameModal = name;
    this.params.is_author = this.isAuthor;
    this.params.listStatus= this.checkStatus();
    this.dataService.getOrganizationTreeById(this.params).subscribe((data: IDataResponse<ITreeOrganization[]>) => {
      this.listSelectedOrg = data.content;
      this.listSelectedOrg?.forEach(org => {
        org.pageNumber = data.pageable?.pageNumber;
      })
      this.setDataPagination(data);
      this.mapCheckbox();
    });
  }

  chooseOrg(index: any,data?:any): void {
    if (this.listSelectedOrg) {
      this.orgName = this.listSelectedOrg[index].orgName;
      const orgLevel = this.listSelectedOrg[index].treeLevel
      const lastIndex = this.listSelectedOrg[index].pathName ? this.listSelectedOrg[index].pathName.replace(' ', '').split(' -').length - 1 : '';
      const pathID = this.listSelectedOrg[index].pathId;
      this.pathResult2 = this.listSelectedOrg[index].pathName;
      if (!lastIndex) {
        this.pathResult = this.listSelectedOrg[index].pathName;
      } else {
        this.pathResult =
          this.listSelectedOrg[index].pathName.replace(' ', '').split(' -')[lastIndex] +
          '-' + this.listSelectedOrg[index].orgName;
      }
      this.objectSearch.orgId = '' + this.listSelectedOrg[index].orgId;
      this.activeNode = null;
      this.orgNameModal = '';
      this.modalRef.destroy({ orgName: this.orgName, orgId: this.objectSearch.orgId, pathId: pathID, pathResult: this.pathResult2, orgLevel: orgLevel });
    }
  }

  searchTreeOrg(event: any): void {
    this.checked = false;
    this.orgMap.clear();
    const params = {
      orgName: this.orgNameModal ? this.orgNameModal : "",
      page: 0,
      size: userConfig.pageSizePopup,
    };
    if (event.key === 'Enter' || event.type === 'click') {
      this.isLoading = true;
      if(this.flagChk != this.isShowAll && this.isShowChkOrg){
        this.getAllTree();
      }
      this.getDataTable(params);
    }
  }

  ngOnInit(): void {
    if (this.mapChecked && this.mapChecked.size > 0) {
      this.orgMap = new Map(JSON.parse(JSON.stringify([...this.mapChecked])));
    }
    this.fetchData();
    this.getAllTree();
  }

  getAllTree(): void {
    const listStatus = this.checkStatus();
    this.dataService.getSubTree(true, this.scope, this.resourceCode, this.isAuthor,'',listStatus).subscribe(
      (data) => {
        if(data?.data?.length){
          const rootItem = data.data[0];
          this.params.id = String(rootItem.orgId);
          this.selectedNodeId(String(rootItem.orgId), rootItem.orgName);
          this.nodeRoot = rootItem;
          data.data.forEach(org => {
            org.key = '0';
            org.title = org.orgName;
            org.id = org.orgId;
          });
          this.treeOrg = data.data;
        }
        this.flagChk = this.isShowAll
      },
      (error) => {
        this.toastrCustom.error(error.message)
      }
    );
  }


  addKey(data: OrganizationNode[] | undefined, keyParent?: any) {
    if (!data) {
      return
    }
    for (let i = 0; i < data.length; i++) {
      data[i].title = data[i].orgName;
      data[i].id = data[i].orgId;
      data[i].expanded = false;
      data[i].key = keyParent + '-' + i;
      this.addKey(data[i].subs, data[i].key)
      data[i].children = data[i].subs;
    }
  }

  updateCheckedSet(item: ITreeOrganization, checked: boolean): void {
    if (checked) {
      this.orgMap.set(item.orgId, item);
    } else {
      this.orgMap.delete(item.orgId);
    }
  }
  refreshCheckedStatus(): void {
    this.checked = this.listSelectedOrg?.every(({ orgId }) => this.orgMap.has(orgId));
  }

  onItemChecked(item: ITreeOrganization, checked: boolean): void {
    this.updateCheckedSet(item, checked);
    this.refreshCheckedStatus();
  }

  onAllChecked(checked: boolean): void {
    this.listSelectedOrg?.forEach((item) => this.updateCheckedSet(item, checked));
    this.refreshCheckedStatus();
  }
  changePage(value: any): void {
    const params = {
      orgName: this.orgNameModal ? this.orgNameModal : "",
      page: value - 1,
      size: userConfig.pageSizePopup,
    };
    this.getDataTable(params);
  }

  onResize({ col }: NzResizeEvent): void {
    cancelAnimationFrame(this.id);
    this.id = requestAnimationFrame(() => {
      this.col = col!;
    });
  }

  setDataPagination(data: any) {
    this.paginationModal.size = data.size;
    this.paginationModal.total = data.totalElements;
    this.paginationModal.pageIndex = data.pageable.pageNumber;
    this.paginationModal.numberOfElements = data.numberOfElements;
  }
  applyChecked() {
    this.modalRef.destroy({ orgList: this.orgMap });
  }
  mapCheckbox() {
    if (this.isShowCheckbox) {
      const arrChecked: any = [];
      this.orgMap.forEach((value, key) => {
        arrChecked.push(value);
      });
      this.listSelectedOrg = this.listSelectedOrg?.filter(item => Number(item.orgId) !== 1 && Number(item.orgId) !== 2);
      if (arrChecked.filter((item: any) => item.pageNumber === this.paginationModal.pageIndex).length === this.listSelectedOrg?.length) {
        this.checked = true;
      } else {
        this.checked = false;
      }
      this.listSelectedOrg?.forEach((item, index) => {
        if (!this.orgMap.has(item.orgId)) {
          this.checked = false;
        } else {
          this.orgMap.set(item.orgId, <ITreeOrganization>this.orgMap.get(item.orgId));
        }
      });
    }
  }

  loadChild(node: NzTreeNode) {
    const listStatus = this.checkStatus();
    const children: NzTreeNodeOptions[] = [];
    let index = 0;
    this.dataService.getSubTree(true, this.scope, this.resourceCode, this.isAuthor ,node.origin['id'],listStatus).subscribe(
      (data) => {
        data?.data?.forEach((item) => {
          const org = {
            id: item.orgId,
            ...item
          };
          org.title = item.orgName;
          org.key = `${node.key}-${index}`;
          children.push(org);
          index++;
        });
        node.addChildren(children);
      },
      (error) => {
        this.toastrCustom.error(error?.message)
        this.isLoading = false;
        node.addChildren(children);
      }
    );
  }

  nzEvent(event: NzFormatEmitEvent): void {
    // load child async
    if (event.eventName === 'click' && event.node) {
      this.selectedNodeId(event.node.origin['id'], event.node.origin.title);
    }

    if (event.eventName === 'expand') {
      const node = event.node;
      if (node?.getChildren().length === 0 && node?.isExpanded) {
        this.loadChild(node);
      }
    }
  }
  showAdvSearch(){
    this.isSearchAdv = !this.isSearchAdv;
    if(this.isSearchAdv){
      this.objectSearch.treeLevel=2;
      this.getOrgByLevel();
    }
  }
  fetchData() {
    forkJoin([
      this.dataService.getRegionCategoryList(RegionType.KV, 1),
      this.dataService.getRegionCategoryList(RegionType.LH, 1),
    ]).subscribe(
      ([areas, types]: [
        ResponseEntity<LookupValue[]>,
        ResponseEntity<LookupValue[]>,
      ]) => {
        this.areas = areas.data;
        this.types = types.data;
      },
      (error) => {
        this.toastrCustom.error(error.error?.message);
      }
    );
  }
  getOrgByLevel() {
    this.isLoading = true;
    const params = {
      isAuthor: this.isAuthor,
      scope: this.scope,
      resourceCode: this.resourceCode,
      orgId: this.objectSearch.orgId,
      treeLevel: this.objectSearch.treeLevel,
      page: 0,
      size: maxInt32
    }
    this.dataService.getOrgByLevel(params).subscribe((data) => {
      if(!this.objectSearch.orgId){
        this.listOrgManager = data.data.listOrgManager;
      } else {
        this.listOrgTT1 = data.data.listTT1;
        this.listOrgTT2 = data.data.listTT2;
        this.listOrgTT3 = data.data.listTT3;
      }
      this.objectSearch.orgIdTT1 = '';
      this.objectSearch.orgIdTT2 = '';
      this.objectSearch.orgIdTT3 = '';
      this.isLoading = false;
    }, (error) => {
      this.toastrCustom.error(error.error?.message);
      this.isLoading = false;
    });
  }
  onChangeManager(event:any){
    if(event){
      this.objectSearch.orgId = event;
      this.objectSearch.treeLevel = '';
      this.getOrgByLevel();
    } else{
      this.objectSearch.orgManager = '';
      this.objectSearch.orgIdTT1 = '';
      this.objectSearch.orgIdTT2 = '';
      this.objectSearch.orgIdTT3 = '';
    }
  }
  checkOrg(){
    if(!this.objectSearch.orgIdTT1){
      this.objectSearch.orgIdTT2 = '';
      this.objectSearch.orgIdTT3 = '';
    }else if(!this.objectSearch.orgIdTT2){
      this.objectSearch.orgIdTT3 = '';
    }
  }
  getDataTable(params:any) {
    this.isLoading = true;
    const request = this.checkRequest(params);
    this.dataService.getOrgPopup(request).subscribe((data: IDataResponse<ITreeOrganization[]>) => {
      this.listSelectedOrg = data.content;
      this.listSelectedOrg?.forEach(org => {
        org.pageNumber = data.pageable?.pageNumber;
      })
      this.setDataPagination(data);
      this.mapCheckbox();
      this.isLoading = false;
    }, (error) => {
      this.toastrCustom.error(error.error?.message);
      this.isLoading = false;
    });
  }
  checkRequest(params:any){
    if(this.objectSearch.orgIdTT2){
      this.objectSearch.orgId = this.objectSearch.orgIdTT2;
    } else if(!this.objectSearch.orgIdTT2 && this.objectSearch.orgIdTT1){
      this.objectSearch.orgId = this.objectSearch.orgIdTT1;
    } else if(this.objectSearch.orgManager && !this.objectSearch.orgIdTT1){
      this.objectSearch.orgId = this.objectSearch.orgManager;
    }else {
      this.objectSearch.orgId = '';
    }
    return {
      orgId: this.objectSearch.orgId,
      name: params.orgName,
      orgGroup:this.objectSearch.orgGroup?this.objectSearch.orgGroup:'',
      region:this.objectSearch.region?this.objectSearch.region:'',
      brachType:this.objectSearch.brachType?this.objectSearch.brachType:'',
      isAuthor: this.isAuthor,
      scope: this.scope,
      resourceCode:this.resourceCode,
      listStatus: this.checkStatus(),
      page: params.page,
      size:params.size,
    }
  }
  checkStatus(){
    let listStatus = "";
    if(this.isShowAll){
      listStatus="0,1"
    } else{
      listStatus=""
    }
    return listStatus;
  }
}
