import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormModalShowTreeUnitComponent} from "./form-modal-show-tree-unit/form-modal-show-tree-unit.component";
import {NzResizableModule} from "ng-zorro-antd/resizable";
import {NzGridModule} from "ng-zorro-antd/grid";
import {NzTreeModule} from "ng-zorro-antd/tree";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {TranslateModule} from "@ngx-translate/core";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';

@NgModule({
  imports: [CommonModule, NzResizableModule, FormsModule, ReactiveFormsModule,
    NzGridModule, NzTreeModule, SharedUiLoadingModule, SharedUiMbInputTextModule, SharedUiMbButtonModule, TranslateModule,
    NzTableModule, NzToolTipModule, NzPaginationModule, SharedUiMbSelectModule, NzCheckboxModule],
  exports: [FormModalShowTreeUnitComponent],
  declarations: [FormModalShowTreeUnitComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class SharedUiFormModalShowTreeUnitModule {}
