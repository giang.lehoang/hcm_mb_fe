import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';

@Component({
  selector: 'mb-button',
  templateUrl: './mb-button.component.html',
  styleUrls: ['./mb-button.component.scss'],
})
export class MbButtonComponent implements OnInit, OnChanges{
  @Input() mbButtonType: 'BORDER' | 'NO_BORDER' | 'ONLY_ICON' = 'BORDER';
  @Input() mbButtonSize: 'LARGE' | 'MEDIUM' | 'SMALL' | 'VERY_LARGE' = 'MEDIUM';
  @Input() mbType: 'SUBMIT' | 'BUTTON' = 'BUTTON';
  @Input() mbButtonBgColor = '#fff';
  @Input() mbButtonBorderColor = '#A0A3BD';
  @Input() mbButtonTextColor = '#6E7191';
  @Input() mbButtonShowVerticalDash = false;
  @Input() mbButtonVerticalDashColor = '#A0A3BD';
  @Input() mbIsDisable = false;
  @Input() mbPrefixIcon: null | string;
  @Input() mbButtonText: string;
  @Input() mbSuffixIcon: null | string;
  @Input() mbButtonAvailableStyle: 'PRIMARY' | 'DANGER' | 'DEFAULT' | 'BORDER' | 'CONTENT' | 'LOADING'
    | 'CUSTOM' | 'BtnCancelHasBorder'|'GRAY'| 'SEARCH' | 'DISABLE' | 'INFO' | 'IMPORT' | 'ADD_NEW' | 'CANCEL';
  @Input() mbButtonIsLoading = false;
  @Input() mbButtonIconType: 'ZORRO' | 'AWESOME' = 'ZORRO';
  @Input() customStyle = false;
  @Input() customStyleDefault = false;
  @Input() buttonColorPink = false;
  @Input() buttonColorWhite = false;
  @Input() customWidth = '';
  mbButtonClass = 'button__class';
  mbStyle: any;
  styleDash: any;
  borderRadius: string;
  padding: string;
  minHeight: string;
  marginLeft: string;
  lineHeight: string;
  fontSize: string;
  colorCustom: string;
  customBorderColor: string;
  fontWeight: string;
  constructor() {}

  ngOnInit(): void {
    this.mbButtonClass = 'button__class';
    if (this.mbButtonType === 'BORDER') {
      this.mbButtonClass += ' ' + 'button__class--border';
    } else {
      this.mbButtonClass += ' ' + 'button__class--no-border';
    }
    if (this.mbButtonShowVerticalDash) {
      this.mbButtonClass += ' ' + 'button__class--show-dash';
    }
    if (
      this.mbButtonAvailableStyle === 'PRIMARY' ||
      this.mbButtonAvailableStyle === 'LOADING' ||
      this.mbButtonAvailableStyle === 'DANGER' ||
      this.mbButtonAvailableStyle === 'DEFAULT' ||
      this.mbButtonAvailableStyle === 'BORDER'
    ) {
      this.mbButtonClass = 'button__class button__class--no-border';
      this.mbButtonTextColor = '#fff';
      this.mbButtonShowVerticalDash = false;
      this.mbButtonSize = 'MEDIUM';
    }
    if (this.mbButtonAvailableStyle === 'PRIMARY' || this.mbButtonAvailableStyle === 'LOADING') {
      this.mbButtonBgColor = '#141ED2';
    }
    if (this.mbButtonAvailableStyle === 'SEARCH') {
      this.mbButtonClass = 'button__class button__class--no-border';
      this.mbButtonBgColor = 'rgb(232, 234, 246)';
      this.mbButtonTextColor = '#141ED2';
      this.mbButtonShowVerticalDash = false;
      this.mbButtonSize = 'LARGE';
      this.borderRadius = '8px';
    }
    if (this.mbButtonAvailableStyle === 'DANGER') {
      this.mbButtonBgColor = '#C30052';
    }

    if(this.mbButtonAvailableStyle === 'DISABLE'){
      this.mbButtonBgColor = '#A0A3BD';
      this.mbButtonTextColor = '#FFFFFF';
    }

    if(this.mbButtonAvailableStyle === 'CUSTOM'){
      this.mbButtonBgColor = '#fff';
      this.colorCustom = '#141ED2';
      this.mbButtonBgColor = 'rgb(232, 234, 246)';
      this.borderRadius = '4px';
      this.mbButtonTextColor = '#141ED2';
      this.fontWeight = '600';
      this.mbButtonBorderColor = 'white';
      this.minHeight = "40px"
    }

    if(this.mbButtonAvailableStyle === 'BtnCancelHasBorder'){
      this.mbButtonBgColor = '#FFFFFF';
      this.colorCustom = '#141ED2';
      this.borderRadius = '4px';
      this.mbButtonTextColor = '#141ED2';
      this.fontWeight = '600';
      this.mbButtonBorderColor = '#D4D7E7';
    }

    if (this.mbButtonAvailableStyle === 'DEFAULT') {
      this.mbButtonClass = 'button__class button__class--border';
      this.mbButtonBgColor = '#f5f5f5';
      this.mbButtonBorderColor = '#A0A3BD';
      this.mbButtonTextColor = '#6E7191';
    }

    if(this.mbButtonAvailableStyle === 'BORDER'){
      this.mbButtonBorderColor = '#141ED2'
      this.mbButtonTextColor = '#141ED2'
      this.mbButtonClass = 'button__class button__class--border';
      this.fontWeight = '600';
    }

    if(this.mbButtonAvailableStyle === 'CONTENT'){
      this.mbButtonClass = 'button__class button__class--no-border';
      this.mbButtonBgColor = "none";
      this.marginLeft = '0px';
    }

    if (this.mbButtonAvailableStyle === 'INFO') {
      this.mbButtonClass = 'button__class button__class--no-border';
      this.mbButtonTextColor = '#141ED2';
      this.mbButtonBgColor = '#E8EAF6';
      this.mbButtonShowVerticalDash = false;
      this.mbButtonSize = 'MEDIUM';
    }

    if (this.customStyleDefault) {
      this.mbButtonBgColor = 'white';
      this.fontSize = '15px';
      this.colorCustom = '#6E7191';
      this.borderRadius = '8px';
      this.padding = '10px 24px';
      this.minHeight = '40px';
      this.marginLeft = '16px';
      this.lineHeight = '20px';
      this.customBorderColor = 'white';
    }
    if (this.buttonColorWhite) {
      this.mbButtonBgColor = 'white';
      this.fontSize = '14px';
      this.colorCustom = '#141ED2';
      this.borderRadius = '8px';
      this.padding = '10px 24px';
      this.minHeight = '40px';
      this.marginLeft = '16px';
      this.lineHeight = '20px';
      this.customBorderColor = 'white';
      this.fontWeight = '600'
    }

    if (this.buttonColorPink) {
      this.mbButtonBgColor = '#E8EAF6';
      this.fontSize = '14px';
      this.colorCustom = '#141ED2';
      this.borderRadius = '8px';
      this.padding = '10px 24px';
      this.minHeight = '40px';
      this.marginLeft = '16px';
      this.lineHeight = '20px';
      this.customBorderColor = 'white';
      this.fontWeight = '600'
    }

    if (this.customStyle) {
      this.borderRadius = '8px';
      this.padding = '10px 24px';
      this.minHeight = '40px';
      this.marginLeft = '16px';
      this.lineHeight = '20px';
      this.fontSize = '14px';
    }
    if(this.mbButtonAvailableStyle === 'GRAY'){
      this.colorCustom = '#141ED2';
      this.mbButtonBgColor = 'rgb(232, 234, 246)';
      this.borderRadius = '4px';
      this.mbButtonTextColor = '#141ED2';
      this.fontWeight = '600';
      this.mbButtonBorderColor = 'white';
    }
    if (this.mbButtonAvailableStyle === 'IMPORT') {
      // this.width = '139px';
      // this.height = '40px';
      this.colorCustom = '#FFFFFF';
      this.mbButtonBgColor = '#FFFFFF';
      // this.borderRadius = '8px';
      this.mbButtonTextColor = '#141ED2';
      this.fontWeight = '100';
      this.mbButtonBorderColor = '#D4D7E7';
    }
    if (this.mbButtonAvailableStyle === 'ADD_NEW') {
      // this.width = '139px';
      // this.height = '40px';
      this.colorCustom = '#FFFFFF';
      this.mbButtonBgColor = '#141ED2';
      // this.borderRadius = '8px';
      this.mbButtonTextColor = '#FFFFFF';
      this.fontWeight = '100';
      this.mbButtonBorderColor = '#141ED2';
    }
    if(this.mbButtonAvailableStyle === 'CANCEL'){
      this.mbButtonBgColor = '#FFFFFF';
      this.colorCustom = '#141ED2';
      this.borderRadius = '4px';
      this.mbButtonTextColor = '#141ED2';
      this.mbButtonBorderColor = '#D4D7E7';
    }
    this.mbButtonClass += ' ' + 'button__class--' + this.mbButtonSize.toLowerCase();
    const bgColor = `${this.mbButtonBgColor}!important`;
    const borderColor = `${this.mbButtonBorderColor}!important`;
    const textColor = `${this.mbButtonTextColor}!important`;
    const verticalDash = `${this.mbButtonVerticalDashColor}!important`;
    const heightVertical = this.mbButtonSize === 'LARGE' ? '40px' : this.mbButtonSize === 'MEDIUM' ? '32px' : '24px';
    this.mbStyle = {
      background: bgColor,
      'border-color': this.customStyleDefault || this.buttonColorWhite|| this.buttonColorPink ? this.customBorderColor   : borderColor,
      color: this.customStyleDefault || this.buttonColorWhite || this.buttonColorPink ? this.colorCustom   : textColor,
      'border-radius': this.borderRadius,
      padding: this.padding,
      'min-height': this.minHeight,
      'margin-left': this.marginLeft,
      'line-height': this.lineHeight,
      'font-size': this.fontSize,
      'font-weight': this.fontWeight,
      'min-width': this.customWidth,
    };
    this.styleDash = { 'border-color': verticalDash, height: heightVertical };
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.mbButtonAvailableStyle === 'LOADING') {
      if (changes['mbButtonIsLoading']?.currentValue ) {
        this.mbButtonClass = 'button__class button__class--no-border';
        this.mbButtonBgColor = '#d1d2e6';
        this.mbButtonTextColor = '#141ED2';
        this.fontWeight = '600';
        this.mbIsDisable = true;
      } else {
        this.mbButtonBgColor = '#141ED2';
        this.mbButtonClass = 'button__class--medium button__class--no-border';
        this.mbButtonTextColor = '#fff';
        this.mbButtonShowVerticalDash = false;
        this.mbIsDisable = false;
        this.fontWeight = '100';
      }
      const bgColor = `${this.mbButtonBgColor}!important`;
      const borderColor = `${this.mbButtonBorderColor}!important`;
      const textColor = `${this.mbButtonTextColor}!important`;
      const verticalDash = `${this.mbButtonVerticalDashColor}!important`;
      const heightVertical = this.mbButtonSize === 'LARGE' ? '40px' : this.mbButtonSize === 'MEDIUM' ? '32px' : '24px';
      this.mbStyle = {
        background: bgColor,
        'border-color': this.customStyleDefault || this.buttonColorWhite|| this.buttonColorPink ? this.customBorderColor   : borderColor,
        color: this.customStyleDefault || this.buttonColorWhite || this.buttonColorPink ? this.colorCustom   : textColor,
        'border-radius': this.borderRadius,
        padding: this.padding,
        'min-height': this.minHeight,
        'margin-left': this.marginLeft,
        'line-height': this.lineHeight,
        'font-size': this.fontSize,
        'font-weight': this.fontWeight,
        'min-width': this.customWidth,
      };
      this.styleDash = { 'border-color': verticalDash, height: heightVertical };
    }
  }
}
