import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MbButtonComponent } from './mb-button/mb-button.component';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzButtonModule } from 'ng-zorro-antd/button';

@NgModule({
  imports: [CommonModule,
    NzIconModule,
    NzButtonModule],
  declarations: [MbButtonComponent],
  exports: [MbButtonComponent]
})
export class SharedUiMbButtonModule {}
