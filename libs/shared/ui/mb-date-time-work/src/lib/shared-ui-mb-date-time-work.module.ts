import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MbDateTimeWorkComponent } from './mb-date-time-work/mb-date-time-work.component';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { NzIconModule } from 'ng-zorro-antd/icon';
import {FormsModule} from "@angular/forms";
import {SharedDirectivesMaskDateModule} from "@hcm-mfe/shared/directives/mask-date";

@NgModule({
  imports: [CommonModule, NzDatePickerModule, NzIconModule, SharedUiMbSelectModule, FormsModule, SharedDirectivesMaskDateModule],
  declarations: [MbDateTimeWorkComponent],
  exports: [MbDateTimeWorkComponent],
})
export class SharedUiMbDateTimeWorkModule {}
