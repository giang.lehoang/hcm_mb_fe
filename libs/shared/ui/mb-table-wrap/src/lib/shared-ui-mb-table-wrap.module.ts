import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MbTableWrapComponent } from './mb-table-wrap/mb-table-wrap.component';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { NzSpaceModule } from 'ng-zorro-antd/space';
import { TranslateModule } from '@ngx-translate/core';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DragDropModule} from "@angular/cdk/drag-drop";

@NgModule({
  imports: [CommonModule, NzCardModule, NzCheckboxModule, NzPopoverModule, NzSpaceModule, TranslateModule,
    SharedUiMbButtonModule, SharedUiMbInputTextModule, FormsModule, ReactiveFormsModule, DragDropModule],
  declarations: [MbTableWrapComponent],
  exports: [MbTableWrapComponent],
})
export class SharedUiMbTableWrapModule {}
