import { AfterContentInit, ChangeDetectionStrategy, Component, ContentChild, Input, OnInit } from '@angular/core';
import { moveItemInArray } from '@angular/cdk/drag-drop';
import { TranslateService } from '@ngx-translate/core';
import { StringUtils } from '@hcm-mfe/shared/common/utils';
import {MBTableComponentToken, MBTableHeader} from "@hcm-mfe/shared/data-access/models";

@Component({
  selector: 'mb-table-wrap',
  templateUrl: './mb-table-wrap.component.html',
  styleUrls: ['./mb-table-wrap.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MbTableWrapComponent implements OnInit, AfterContentInit {
  @Input() mbTitle;
  @Input() nonBg: boolean;
  @Input() extra = true;

  columnSearch: string ;
  tableConfigVisible = false;
  @ContentChild(MBTableComponentToken) antTableComponent!: MBTableComponentToken;
  currentTableComponent!: MBTableComponentToken ;

  tableHeaders: MBTableHeader[] = [];
  tableHeadersSource: MBTableHeader[] = [];
  copyHeader: MBTableHeader[] = [];
  allTableFieldChecked = false;
  allTableFieldIndeterminate = false;

  constructor(private translate: TranslateService ) {
    this.nonBg = false;
  }

  ngOnInit(): void {
    this.mbTitle = this.mbTitle ?? this.translate.instant("common.label.searchResult");
    this.translate.onLangChange.subscribe(res => this.configTableHeader())
  }

  configTableHeader() {
    this.tableHeaders.forEach(header => header.titleTrans = header.title ? this.translate.instant(header.title) : '')
    this.copyHeader.length = 0;
    this.tableHeaders.forEach(item => {
      this.copyHeader.push({...item});
    })
    this.tableHeadersSource = [...this.tableHeaders];
  }

  dropTableConfig(event: any) {
    moveItemInArray(this.tableHeaders, event.previousIndex, event.currentIndex);
    this.changeTableConfigShow();
  }

  changeTableConfigShow(): void {
    const tempArray = [...this.tableHeaders];
    const fixedLeftArray: MBTableHeader[] = [];
    const fixedRightArray: MBTableHeader[] = [];
    const noFixedArray: MBTableHeader[] = [];
    tempArray.forEach(item => {
      if (item.fixed) {
        if (item.fixedDir === "left") {
          fixedLeftArray.push(item);
        } else {
          fixedRightArray.push(item)
        }
      } else {
        noFixedArray.push(item)
      }
    });
    this.currentTableComponent.tableConfig.headers = [...fixedLeftArray, ...noFixedArray, ...fixedRightArray];
    this.tableChangeDetection();
  }

  tableChangeDetection() {
    this.currentTableComponent.tableChangeDetection();
  }

  changeSignalCheck(e: boolean, item: MBTableHeader): void {
    item.show = e;
    this.judgeAllChecked();
    this.tableChangeDetection();
  }

  judgeAllChecked(): void {
    this.allTableFieldChecked = this.tableHeaders.every(item => item.show === true);
    const allUnChecked = this.tableHeaders.every(item => !item.show);
    this.allTableFieldIndeterminate = !this.allTableFieldChecked && !allUnChecked;
  }

  reset(): void {
    this.columnSearch = null;
    this.tableHeaders = [];
    this.copyHeader.forEach(item => {
      this.tableHeaders.push({...item});
    })
    this.tableHeadersSource = [...this.tableHeaders];
    this.currentTableComponent.tableConfig.headers = [...this.tableHeaders];
    this.tableChangeDetection();
  }

  ngAfterContentInit(): void {
    this.currentTableComponent = this.antTableComponent;

    // if (this.isNormalTable) {
      this.tableHeaders = [...this.currentTableComponent.tableConfig.headers];
      this.tableHeaders.forEach(item => {
        if (item.show === undefined) {
          item.show = true;
        }
      })
    this.configTableHeader();
    this.judgeAllChecked();
    // }
  }

  changeAllTableTableConfigShow(e: boolean): void {
    if (e) {
      this.allTableFieldChecked = e;
      this.allTableFieldIndeterminate = false;
    }
    this.tableHeaders.forEach(item => item.show = e);
    this.tableHeadersSource = [...this.tableHeaders];
    this.tableChangeDetection();
  }

  searchColumn() {
    if (!StringUtils.isNullOrEmpty(this.columnSearch))
      this.tableHeaders = this.tableHeadersSource.filter(item => item.titleTrans.toLowerCase().startsWith(this.columnSearch.toLowerCase()))
    else
      this.tableHeaders = [...this.tableHeadersSource];
  }
}
