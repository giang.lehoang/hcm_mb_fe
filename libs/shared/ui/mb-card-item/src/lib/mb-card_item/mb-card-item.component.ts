import { Component, Input, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'mb-card-item',
  templateUrl: './mb-card-item.component.html',
  styleUrls: ['./mb-card-item.component.scss'],
})
export class MbCardItemComponent implements OnInit {
  @Input() mbRouterLink: string;
  @Input() mbTitle: string;
  @Input() mbIconPath: string = '/assets/img/icon_card/mau_bieu.png'
  @Input() mbIsDisable: boolean = false;
  @Input() mbIsOpenNewTab: boolean = false;

  constructor(private readonly router: Router ) {
  }

  ngOnInit(): void {

  }

  change(event, mbRouterLink: string) {
    if (mbRouterLink === "#") {
      // event.defaultPrevented();
      return;
    } else {
      if (this.mbIsOpenNewTab) {
        window.open(mbRouterLink);
      } else {
        this.router.navigateByUrl(mbRouterLink);
      }
    }
  }
}
