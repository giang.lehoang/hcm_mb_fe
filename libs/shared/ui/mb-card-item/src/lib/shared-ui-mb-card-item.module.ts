import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MbCardItemComponent } from './mb-card_item/mb-card-item.component';

@NgModule({
  imports: [CommonModule],
  declarations: [MbCardItemComponent],
  exports: [MbCardItemComponent],
})
export class SharedUiMbCardItemModule {}
