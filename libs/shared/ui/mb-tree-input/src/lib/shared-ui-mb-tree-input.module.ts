import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MbTreeInputComponent } from './mb-tree-input/mb-tree-input.component';
import { NzTreeSelectModule } from 'ng-zorro-antd/tree-select';
import { NzIconModule } from 'ng-zorro-antd/icon';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
  imports: [CommonModule, NzTreeSelectModule, NzIconModule, FormsModule, ReactiveFormsModule],
  declarations: [MbTreeInputComponent],
  exports: [MbTreeInputComponent]
})
export class SharedUiMbTreeInputModule {}
