import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'mb-tree-input',
  templateUrl: './mb-tree-input.component.html',
  styleUrls: ['./mb-tree-input.component.scss']
})
export class MbTreeInputComponent implements OnInit {

  @Input() mbLabelText: string;
  @Input() mbPlaceholderText = '';
  @Input() mbDataTree: any = [];
  @Input() expandKeys: any = [];
  @Input() mbMultiple: boolean = false;
  @Input() mbCheckable: boolean = false;
  @Input() mbShowSearch: boolean = false;

  value?: string;

  constructor() {
  }

  ngOnInit(): void {
  }


}
