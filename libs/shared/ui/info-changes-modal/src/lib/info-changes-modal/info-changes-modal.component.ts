import { Component, OnInit } from '@angular/core';
import {InfoChange, MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {ShareDataService} from "@hcm-mfe/staff-manager/data-access/services";
import {InfoChangesService} from "@hcm-mfe/shared/data-access/services";
import {HttpParams} from "@angular/common/http";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";


@Component({
    selector: 'app-info-changes-modal',
    templateUrl: './info-changes-modal.component.html',
    styleUrls: ['./info-changes-modal.component.scss']
})
export class InfoChangesModalComponent implements OnInit {

    infoChangeData: InfoChange[] = [];
    employeeId: number | any;
    tableConfig!: MBTableConfig;
    pagination = new Pagination();


    constructor(
        private infoChangeService: InfoChangesService,
        private shareDataService: ShareDataService) {
    }

    ngOnInit(): void {
        this.shareDataService.employee$.subscribe(employee => {
            this.employeeId = employee.employeeId;
        });
        this.initTable();
        this.doSearch(1);
    }

    doSearch(pageIndex: number) {
        this.pagination.pageNumber = pageIndex;
        this.tableConfig.loading = true;

        const infoChangeApi = this.infoChangeService;
        let params = new HttpParams();
        params = params.appendAll(this.pagination.getCurrentPage());
        infoChangeApi.getInfoChange(this?.employeeId, params).subscribe({
            next: (res) => {
                if (res.code == HTTP_STATUS_CODE.OK) {
                    this.infoChangeData = res?.data?.listData;
                    this.tableConfig.pageIndex = pageIndex;
                    this.tableConfig.total = res.data.count;
                }
                this.tableConfig.loading = false;
            },
            error: () => this.tableConfig.loading = true
        });
    }

    private initTable(): void {
        this.tableConfig = {
            headers: [
                {
                    title: 'common.table.infoChange.actionType',
                    field: 'actionType',
                    tdClassList: ['text-center'], thClassList: ['text-center'],
                    width: 100
                },
                {
                    title: 'common.table.infoChange.dataType',
                    field: 'dataType',
                    tdClassList: ['text-center'], thClassList: ['text-center'],
                    width: 100
                },
                {
                    title: 'common.table.infoChange.dataBefore',
                    field: 'dataBefore',
                    tdClassList: ['text-center'], thClassList: ['text-center'],
                    width: 300,
                },
                {
                    title: 'common.table.infoChange.dataAfter',
                    field: 'dataAfter',
                    tdClassList: ['text-center'], thClassList: ['text-center'],
                    width: 300
                },
                {
                    title: 'common.table.infoChange.createdBy',
                    field: 'createdBy',
                    tdClassList: ['text-center'], thClassList: ['text-center'],
                    width: 100
                },
                {
                    title: 'common.table.infoChange.createDate',
                    field: 'createDate',
                    tdClassList: ['text-center'], thClassList: ['text-center'],
                    width: 100
                },
            ],
            total: 0,
            loading: false,
            size: 'small',
            pageSize: this.pagination.pageSize,
            pageIndex: 1,
            needScroll: true,
        };
    }

}
