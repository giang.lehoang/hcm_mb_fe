import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {InfoChangesModalComponent} from "./info-changes-modal/info-changes-modal.component";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";

@NgModule({
  imports: [CommonModule, SharedUiMbTableWrapModule, SharedUiMbTableModule],
  declarations: [InfoChangesModalComponent],
  exports: [InfoChangesModalComponent],
})
export class SharedUiInfoChangesModalModule {}
