import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MbDatePickerComponent } from './mb-date-picker/mb-date-picker.component';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedDirectivesMaskDateModule } from '@hcm-mfe/shared/directives/mask-date';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, NzDatePickerModule, NzIconModule, SharedDirectivesMaskDateModule],
  declarations: [MbDatePickerComponent],
  exports: [MbDatePickerComponent]
})
export class SharedUiMbDatePickerModule {}
