import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input, OnChanges,
  OnInit,
  Output, SimpleChanges,
  TemplateRef
} from '@angular/core';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { InputBoolean } from 'ng-zorro-antd/core/util';
import {MBTableMergeCellComponentToken, TableConfig, TableHeader} from "@hcm-mfe/shared/data-access/models";
import {ValidateService} from "@hcm-mfe/shared/core";

@Component({
  selector: 'mb-table-merge-cell',
  templateUrl: './mb-table-merge-cell.component.html',
  styleUrls: ['./mb-table-merge-cell.component.scss'],
  providers: [
    { provide: MBTableMergeCellComponentToken, useExisting: MbTableMergeCellComponent }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MbTableMergeCellComponent implements OnInit, OnChanges {

  @Input() tableConfig?: TableConfig;
  @Input() dataList: readonly NzSafeAny[] = [];
  @Input() nzWidthConfig: any = [];
  @Input() nzScrollHeight: string = '35vh';
  @Input() isScrollHeight: boolean = false;
  @Input() @InputBoolean() bordered: boolean = true;
  @Input() @InputBoolean() isShowSelectRow: boolean = false;
  @Input() conditionField: string = '';
  @Input() selectValue: any;

  tableHeader: Array<Array<TableHeader>> = [];
  tableHeaderMappingWithBe: Array<TableHeader> = [];
  nzScrollWidth: number | undefined;
  nzWidthConfigTmp: NzSafeAny[] = [];
  currentPageIndex = 1;
  currentDataTable!: readonly NzSafeAny[];

  @Input() setOfCheckedId = new Set<number>();
  checkboxAllValueMap: Map<number, boolean> = new Map();
  checkboxAllIndeterminateMap: Map<number, boolean> = new Map();

  @Input() @InputBoolean() showSizeChanger = false;
  @Input() pageSizeOptions: number[] = [];
  @Input() @InputBoolean() showQuickJumper = false;
  @Input() rangeTotalTemplate?: TemplateRef<NzSafeAny>;
  @Output() changePageNum = new EventEmitter<number>();
  @Output() changePageSize = new EventEmitter<number>();
  @Output() amountSelectedRowsChange = new EventEmitter<Set<number>>();
  @Output() dblClickRow = new EventEmitter<NzSafeAny>();
  @Output() clickRow = new EventEmitter<NzSafeAny>();

  constructor(
    private cdRef: ChangeDetectorRef,
    public validateService: ValidateService
  ) {
  }

  ngOnInit(): void {
    this.preprocessTableHeader();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['tableConfig']) {
      this.preprocessTableHeader();
    }
  }

  resetAllCheckBoxFn() {
    this.checkboxAllValueMap.clear();
    this.checkboxAllIndeterminateMap.clear();
    this.setOfCheckedId.clear();

    this.cdRef.markForCheck();
  }

  tableChangeDetection(): void {
    this.dataList = [...this.dataList];
    this.preprocessTableHeader();
    this.cdRef.markForCheck();
  }

  private preprocessTableHeader() {
    const tree = {
      title: null,
      child: this.tableConfig?.headers.filter(item => item.remove !== true)
    };

    let result = this.walkTree(tree);

    result.shift();

    this.tableHeader = result;
    this.tableHeaderMappingWithBe = this.getTableHeaderMappingWithBe([tree]);


    if (this.tableConfig?.showSelect) {
      const rowspan = this.tableHeader[0][0].rowspan;  // lay row span cua stt
      this.tableHeader[0].unshift({rowspan: rowspan, width: 60, isCheckBox: true});
      this.tableHeaderMappingWithBe.unshift({});
    }

    if(this.nzWidthConfig.length > 0){
      this.nzWidthConfigTmp = [...this.nzWidthConfig];
      if(this.tableHeaderMappingWithBe?.filter(value => !value.show && value).length > 0){
        this.tableHeaderMappingWithBe.forEach((item,index) => {
          if(!item.show){
            this.nzWidthConfigTmp.splice(index,1);
          }
        });
      } else {
        this.nzWidthConfigTmp = [...this.nzWidthConfig];
      }
    }

    this.setScrollWidth();
  }

  private walkTree(tree: NzSafeAny, level = 0, collection: NzSafeAny = []) {

    const c = tree.child;
    const v = tree;

    if (!Array.isArray(collection[level])) {
      collection[level] = [];
    }

    collection[level].push(v);

    for (const subTree of c || []) {
      collection = this.walkTree(subTree, level + 1, collection);
    }

    return collection;
  }

  private getTableHeaderMappingWithBe(nodes: NzSafeAny, result: NzSafeAny = []) {
    for (let i = 0, length = nodes.length; i < length; i++) {
      if (!nodes[i].child || nodes[i].child.length === 0) {
        result.push(nodes[i]);
      } else {
        result = this.getTableHeaderMappingWithBe(nodes[i].child, result);
      }
    }
    return result;
  }

  onPageIndexChange(event: number): void {
    this.changePageNum.emit(event);
    this.currentPageIndex = event;
  }

  onPageSizeChange($event: number): void {
    this.changePageSize.emit($event);
  }

  setScrollWidth() {
    this.nzScrollWidth = 0;
    for (let item of this.tableHeaderMappingWithBe) {
      if ((item.show === undefined || item.show === true) && item.width) {
        this.nzScrollWidth += item.width;
      }
    }
  }

  onItemChecked(id: number, event: boolean) {
    if (event) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
    this.refreshStatus();
  }

  refreshStatus() {
    const validData = this.currentDataTable.filter(item => !item.disabled).map(item => item.id);
    const allCheck = validData.every(id => this.setOfCheckedId.has(id));
    const allUnCheck = validData.every(id => !this.setOfCheckedId.has(id));
    const indeterminate = !allCheck && !allUnCheck;
    this.checkboxAllValueMap.set(this.currentPageIndex, allCheck);
    this.checkboxAllIndeterminateMap.set(this.currentPageIndex, indeterminate);

    this.onAmountSelectedRowsChange();

    // const validData = this.dataList.filter(value => !value.disabled);
    // const allChecked = validData.length > 0 && validData.every(value => value.checked === true);
    // const allUnChecked = validData.every(value => !value.checked);
    // this.allChecked = allChecked;
    // this.indeterminate = !allChecked && !allUnChecked;
  }

  onAmountSelectedRowsChange() {
    this.amountSelectedRowsChange.emit(this.setOfCheckedId);
  }

  onAllChecked(value: boolean) {
    const validData = this.currentDataTable.filter(item => !item.disabled).map(item => item.id);
    if (value) {
      this.checkboxAllValueMap.set(this.currentPageIndex, true);
      validData.forEach(id => this.setOfCheckedId.add(id));
    } else {
      this.checkboxAllValueMap.delete(this.currentPageIndex);
      validData.forEach(id => this.setOfCheckedId.delete(id));
    }
    this.refreshStatus();
  }

  currentPageDataChange(event: readonly NzSafeAny[]): void {
    this.currentDataTable = event;
  }


  dblClick(row:NzSafeAny){
    this.dblClickRow.emit(row);
  }

  click(row:NzSafeAny){
    if (this.isShowSelectRow) {
      this.selectValue = row[this.conditionField];
    }
    this.clickRow.emit(row);
  }

  getRow(row: any, rowIndex: number): NzSafeAny {
    return {...row, index: rowIndex};
  }
}
