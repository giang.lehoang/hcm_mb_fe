import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MbDataPickerComponent } from './mb-data-picker/mb-data-picker.component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { NzIconModule } from 'ng-zorro-antd/icon';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
  imports: [CommonModule, TranslateModule, NzIconModule, SharedUiMbInputTextModule, FormsModule, ReactiveFormsModule],
  declarations: [MbDataPickerComponent],
  exports: [MbDataPickerComponent],
})
export class SharedUiMbDataPickerModule {}
