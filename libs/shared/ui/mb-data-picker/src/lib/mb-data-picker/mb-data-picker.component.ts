import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { InputBoolean } from 'ng-zorro-antd/core/util';
import { NzAutocompleteComponent } from 'ng-zorro-antd/auto-complete';
import { MbInputTextComponent } from '@hcm-mfe/shared/ui/mb-input-text';

// Component chỉ sử dụng để custom
@Component({
  selector: 'mb-data-picker',
  templateUrl: './mb-data-picker.component.html',
  styleUrls: ['./mb-data-picker.component.scss']
})
export class MbDataPickerComponent implements OnInit {
  @Input() mbDisableButtonSearch = false;
  @Input() mbLabelText: string;
  @Input() mbData: any[] = [];
  @Input() @InputBoolean() mbCanText = false;
  @Input() mbLabel: string;
  @Input() mbPlaceholder: string = 'common.label.search'
  @Input() @InputBoolean() mbDisable = false;
  @Input() mbLabelCustom: string;
  @Input() selected!: any;
  @Output() selectedChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() inputChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() oldInputValue: EventEmitter<any> = new EventEmitter<any>();
  @Output() afterCloseModal: EventEmitter<any> = new EventEmitter<any>();
  @Input() mbResultTpl: TemplateRef<any> = null;
  @Input() mbResultHeader: TemplateRef<any> = null;
  @Input() mbResultFooter: TemplateRef<any> = null;
  @Input() mbModalWidth: number = window.innerWidth > 767 ? window.innerWidth / 1.2 : window.innerWidth;

  @Input() mbErrorDefs: { errorName: string, errorDescription: string }[];
  @Input() mbShowError = false;
  @Input() mbErrors: any;
  @Input() mbType: 'default' | 'warning' | 'error' | 'success' = 'default';

  @Input() mbAutocomplete: NzAutocompleteComponent;

  public modalRef: NzModalRef;

  constructor(
    private modalService: NzModalService,
  ) {
  }

  @ViewChild('searchInput') searchInput: MbInputTextComponent;

  inputSearch: string;

  ngOnInit(): void {
    this.inputSearch = this.mbLabelCustom ? this.mbLabelCustom : (this.mbLabel ? this.selected[this.mbLabel] : '');
  }

  clearResult() {
    if(this.mbDisable) {
      return;
    }
    this.inputSearch = null;
    this.inputChange.emit(null)
    this.selected = null;
    this.selectedChange.emit(null)
  }

  selectItem(item) {
    this.selected = item;
    this.selectedChange.emit(item);
    this.inputSearch = this.mbLabelCustom ? this.mbLabelCustom : this.selected[this.mbLabel];
    if (this.modalRef)
      this.modalRef.destroy(true)
  }

  openResult() {
    if(this.mbDisable || this.mbDisableButtonSearch) {
      return;
    }
    this.modalRef = this.modalService.create({
      nzWidth: this.mbModalWidth,
      nzClosable: null,
      nzBodyStyle: {padding: '0'},
      nzTitle: this.mbResultHeader,
      nzContent: this.mbResultTpl,
      nzFooter: this.mbResultFooter,
    });

    this.modalRef.afterClose.subscribe(res => {
      this.afterCloseModal.emit();
      if (res) {
        this.inputSearch = this.mbLabelCustom ? this.mbLabelCustom : this.selected[this.mbLabel];
      } else {
        this.inputSearch = !this.mbCanText ?
          this.inputSearch :
          this.mbLabelCustom ? this.mbLabelCustom :
            this.selected ? (this.mbLabel ? this.selected[this.mbLabel] : '') : '';
      }
    });
  }

  onBlur() {
    if (this.searchInput.mbAutocomplete?.isOpen) {
        this.oldInputValue.emit(this.inputSearch);
    }
    // if (this.selected) {
    //     this.inputSearch = this.mbLabelCustom ? this.mbLabelCustom : this.selected[this.mbLabel];
    // }
    // else this.inputSearch = '';
  }

  onInputChange($event) {
    this.inputChange.emit($event);
  }

  closeModal() {
    if (this.selected) {
      this.inputSearch = this.mbLabelCustom ? this.mbLabelCustom : this.selected[this.mbLabel];
    } else this.inputSearch = '';
    this.modalRef.destroy()
  }

}
