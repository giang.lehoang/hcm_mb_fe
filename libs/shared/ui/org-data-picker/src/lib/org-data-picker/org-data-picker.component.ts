import {Component, EventEmitter, forwardRef, Injector, Input, OnInit, Output, ViewChild} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR, NgControl} from "@angular/forms";
import {noop} from "rxjs";


import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";
import {InputBoolean} from "ng-zorro-antd/core/util";
import {
  BaseResponse,
  CatalogModel,
  InitScreenSearch,
  ITreeOrganization,
  Node,
  OrgInfo,
  Pagination
} from "@hcm-mfe/shared/data-access/models";
import {ValidateService} from "@hcm-mfe/shared/core";
import {MbDataPickerComponent} from "@hcm-mfe/shared/ui/mb-data-picker";
import {Constant, HTTP_STATUS_CODE, UrlConstant} from "@hcm-mfe/shared/common/constants";
import {OrgInfoService} from "@hcm-mfe/shared/data-access/services";
import {HttpParams} from "@angular/common/http";
import {NzSafeAny} from "ng-zorro-antd/core/types";

@Component({
  selector: 'org-data-picker',
  templateUrl: './org-data-picker.component.html',
  styleUrls: ['./org-data-picker.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => OrgDataPickerComponent),
    }/*,
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => MbInputTextComponent),
      multi: true
    }*/
  ]})
export class OrgDataPickerComponent implements OnInit,ControlValueAccessor{
  @Input() mbLabelText: string;
  @Input() mbPlacholder: string;
  @Input() scope: string;
  @Input() functionCode: string;
  @Input() isAuthor = false;

  @Input() mbDisable: boolean = false;
  @Input() selected: any;
  @Output() selectedChange: EventEmitter<any> = new EventEmitter<any>();
  @Input() @InputBoolean() mbCanText = false;
  @Input() mbErrorDefs: {errorName: string, errorDescription: string}[];
  @Input() mbShowError = false;
  @Input() mbErrors: any;
  @Input() mbType: 'default' | 'warning' | 'error' | 'success' = 'default';

  @ViewChild("dataPicker") dataPicker: MbDataPickerComponent;
  data: any[] = [];
  count: number = 0;

  loading: boolean = false;
  searchOrgValue: string;

  selectedNode: Node = null;

  onTouched: () => void = noop;
  onChange: (_: any) => void = noop;
  ngControl?: NgControl;

  pagination = new Pagination();
  isSearchAdv= false;
  listOrgManager:ITreeOrganization[]=[];
  listOrgTT1:ITreeOrganization[]=[];
  listOrgTT2:ITreeOrganization[]=[];
  listOrgTT3:ITreeOrganization[]=[];

  objectSearch: InitScreenSearch = {
    orgId: '',
    flagStatus: '',
    inputType: '',
    page: 0,
    size: 15,
  };

  areas: CatalogModel[] = [];
  types: CatalogModel[] = [];
  listOrgGroup = [
    {key:"CN",value:"CN"},
    {key:"HO",value:"HO"},
  ];
  isLoading = false;

  constructor(
    private inj: Injector,
    private orgService: OrgInfoService,
    public validateService: ValidateService,
    public toastr: ToastrService,
    public translate: TranslateService
  ) { }

  ngOnInit(): void {
    this.ngControl = this.inj.get(NgControl);
    this.getListBranchTypes();
    this.getListRegions()
    this.getByParent(1);
  }

  change(org: OrgInfo) {
    this.selectedChange.emit(org)
    this.writeValue(org)
    this.onChange(org);
  }

  doSelectOrg(org: OrgInfo) {
    this.dataPicker.selectItem(org);
    this.writeValue(org)
    this.onChange(org);
  }

  doSearchOrg() {
    this.getByParent(1);
  }

  selectNode(node: Node) {
    this.pagination.pageNumber = 1;
    this.selectedNode = node;
    this.searchOrgValue = node?.name;
    this.getByParent(1);
  }

  parseOptions(): NzSafeAny {
    let param: NzSafeAny = this.pagination.getCurrentPage();
    if (this.searchOrgValue) param.orgName = this.searchOrgValue;
    if (this.objectSearch.region) param.region = this.objectSearch.region;
    if (this.objectSearch.brachType) param.brachType = this.objectSearch.brachType;
    if (this.objectSearch.orgGroup) param.ogrGroup = this.objectSearch.orgGroup;
    if (this.objectSearch.orgManager) param.orgManagerId = this.objectSearch.orgManager;
    if (this.objectSearch.orgIdTT1) param.orgIdTT1 = this.objectSearch.orgIdTT1;
    if (this.objectSearch.orgIdTT2) param.orgIdTT2 = this.objectSearch.orgIdTT2;
    if (this.objectSearch.orgIdTT3) param.orgIdTT3 = this.objectSearch.orgIdTT3;

    param.scope = this.scope ? this.scope : '';
    param.functionCode = this.functionCode ? this.functionCode : '';
    return param;
  }


  getByParent(pageIndex: number) {
    this.isLoading = true;
    this.pagination.pageNumber = pageIndex;
    const param = this.parseOptions();
    this.orgService.getByParent(this.selectedNode?.nodeId, param ).subscribe((res: BaseResponse) => {
      if(res.code === HTTP_STATUS_CODE.OK) {
        this.data = res.data.listData;
        this.count = res.data.count;
      }
      else {
        this.data = [];
        this.count = 0;
      }
      this.isLoading = false;

    }, error => {
      this.data = [];
      this.count = 0;
      this.isLoading = false
    })
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.mbDisable = isDisabled;
  }

  writeValue(obj: any): void {
    this.selected = obj;
    if (this.dataPicker) {
      this.dataPicker.inputSearch = this.selected ? this.selected.orgName + (this.selected.parentName ? ('-' + this.selected.parentName) :'') : '';
    }
  }

  closeModal() {
    this.dataPicker.closeModal();
  }

  onChangeOrgByLevel(event: NzSafeAny, orgLevel: number) {
    if (event) {
      this.objectSearch.orgId = event;
      this.objectSearch.treeLevel = '';
      this.getOrgByLevel(orgLevel);
    } else {
      this.clearOrgId(orgLevel);
    }
  }

  clearOrgId(orgLevel: number) {
    if (orgLevel === 1) {
      this.objectSearch.orgIdTT1 = '';
      this.objectSearch.orgIdTT2 = '';
      this.objectSearch.orgIdTT3 = '';
    } else if (orgLevel === 2) {
      this.objectSearch.orgIdTT2 = '';
      this.objectSearch.orgIdTT3 = '';
    } else {
      this.objectSearch.orgIdTT3 = '';
    }
  }

  showAdvSearch(){
    this.isSearchAdv = !this.isSearchAdv;
    if (this.isSearchAdv){
      this.objectSearch.treeLevel = 2;
      this.getOrgByLevel();
    }
  }

  getOrgByLevel(orgLevel?: number): NzSafeAny {
    let params = new HttpParams();
    params =  params.append('scope', this.scope);
    params =  params.append('functionCode', this.functionCode);
    params =  params.append('orgId', this.objectSearch.orgId);
    params =  params.append('orgLevel', this.objectSearch.treeLevel);
    this.orgService.getOrgByLevel(params).subscribe((res) => {
      if(!this.objectSearch.orgId){
        this.listOrgManager = res.data;
        this.listOrgTT1 = [];
        this.listOrgTT2 = [];
        this.listOrgTT3 = [];
      } else {
        if (orgLevel === 1) { // change don vi quan ly truc tiep
          this.listOrgTT1 = res.data;
          this.listOrgTT2 = [];
          this.listOrgTT3 = [];
        } else if (orgLevel === 2) {  // change don vi TT1
          this.listOrgTT2 = res.data;
          this.listOrgTT3 = [];
        } else {
          this.listOrgTT3 = res.data; // change don vi TT2
        }
        this.clearOrgId(orgLevel);
      }
    }, (error) => {
      this.toastr.error(error.error?.message);
    });
  }

  getListBranchTypes() {
    const params = new HttpParams().set('typeCode', Constant.CATALOGS.LOAI_HINH_CHI_NHANH);
    this.orgService.getListValue(UrlConstant.CATALOGS.PREFIX, params).subscribe(res => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.types = res.data;
      }
    }, error => {
      this.toastr.error(error.error?.message);
    })
  }

  getListRegions() {
    const params = new HttpParams().set('typeCode', Constant.CATALOGS.KHU_VUC);
    this.orgService.getListValue(UrlConstant.CATALOGS.PREFIX, params).subscribe(res => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.areas = res.data;
      }
    }, error => {
      this.toastr.error(error.error?.message);
    })
  }
}
