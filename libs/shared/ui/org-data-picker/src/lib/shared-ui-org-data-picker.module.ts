import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {OrgDataPickerComponent} from "./org-data-picker/org-data-picker.component";
import {SharedUiMbDataPickerModule} from "@hcm-mfe/shared/ui/mb-data-picker";
import {TranslateModule} from "@ngx-translate/core";
import {NzFormModule} from "ng-zorro-antd/form";
import {NzCardModule} from "ng-zorro-antd/card";
import {SharedUiMbOrgTreeModule} from "@hcm-mfe/shared/ui/mb-org-tree";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzTableModule} from "ng-zorro-antd/table";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
  imports: [CommonModule, SharedUiMbDataPickerModule, TranslateModule, NzFormModule, NzCardModule, SharedUiMbOrgTreeModule,
    SharedUiMbInputTextModule, SharedUiMbButtonModule, NzTableModule, FormsModule, ReactiveFormsModule, SharedUiMbSelectModule, NzPaginationModule, SharedUiLoadingModule],
  declarations: [OrgDataPickerComponent],
  exports: [OrgDataPickerComponent],
})
export class SharedUiOrgDataPickerModule {}
