import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from './core.module';

@NgModule({
  imports: [CommonModule, CoreModule],
  exports: [ CoreModule ]
})
export class SharedUiCoreModule {}
