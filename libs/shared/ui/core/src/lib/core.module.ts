import { Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatMenuModule } from '@angular/material/menu';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { AntDesignModule } from './ant-design.module';

const THIRD_MODULES = [AntDesignModule, TranslateModule, ScrollingModule,];
// const COMPONENTS = [TopProgressBarComponent, HeaderComponent, Layout2Component, UnauthorizedComponent, NotFoundComponent];
// const DIRECTIVE = [ScrollSpyDirective];

@NgModule({
  // declarations: [...COMPONENTS,  ...DIRECTIVE, MenuSearchComponent, NotificationComponent, ListNotifyComponent],
  imports: [
    CommonModule,
    DragDropModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatSidenavModule,
    MatButtonModule,
    MatIconModule,
    MatDividerModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatMenuModule,
    ...THIRD_MODULES
  ],
  exports: [
    ...THIRD_MODULES, FormsModule, ReactiveFormsModule,
    // ...DIRECTIVE
  ],
  providers: [
    Title,
  ],

  //schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CoreModule {}
