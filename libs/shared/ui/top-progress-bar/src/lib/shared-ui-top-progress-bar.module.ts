import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopProgressBarComponent } from './top-progress-bar/top-progress-bar.component';

@NgModule({
  imports: [CommonModule],
  declarations: [TopProgressBarComponent],
  exports: [TopProgressBarComponent],
})
export class SharedUiTopProgressBarModule {}
