import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MbTableComponent } from './mb-table/mb-table.component';
import { NzTableModule } from 'ng-zorro-antd/table';
import { TranslateModule } from '@ngx-translate/core';
import { SharedPipesTableFiledModule } from '@hcm-mfe/shared/pipes/table-filed';
import { SharedPipesMapModule } from '@hcm-mfe/shared/pipes/map';

@NgModule({
  imports: [CommonModule, NzTableModule, TranslateModule, SharedPipesTableFiledModule, SharedPipesMapModule],
  declarations: [MbTableComponent],
  exports: [MbTableComponent],
})
export class SharedUiMbTableModule {}
