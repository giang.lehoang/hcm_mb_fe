import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  TemplateRef
} from '@angular/core';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { NzTableSize } from 'ng-zorro-antd/table';
import { NzResizeEvent } from 'ng-zorro-antd/resizable';
import { InputBoolean } from 'ng-zorro-antd/core/util';
import { ValidateService } from '@hcm-mfe/shared/core';
import {MBTableComponentToken, MBTableConfig, MBTableHeader} from "@hcm-mfe/shared/data-access/models";

@Component({
  selector: 'mb-table',
  templateUrl: './mb-table.component.html',
  styleUrls: ['./mb-table.component.scss'],
  providers: [
    { provide: MBTableComponentToken, useExisting: MbTableComponent }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MbTableComponent implements OnInit, OnChanges {
  _dataList!: NzSafeAny[];
  _tableSize: NzTableSize = 'default';
  @Input() heightTable = {x: '100vw'};
  @Input() isUseTitle = false;
  @Input() tableConfig!: MBTableConfig;
  @Input() keyCheck: string = 'id';
  @Input() rangeTotalTemplate: TemplateRef<any>;
  @Output() onClickNode: EventEmitter<any> = new EventEmitter<any>();
  @Output() onDblClickNode: EventEmitter<any> = new EventEmitter<any>();
  // Nếu checkbox = true
  @Input() checkedCashArrayFromComment: NzSafeAny[] = [];
  @Input() nzShowPagination = true;
  @Input() isCheckPeriod = false;
  @Input() closeDate = false;
  @Input() finishDate = false;

  @Input()
  set tableData(value: NzSafeAny[]) {
    this._dataList = value;
    if (this.tableConfig.showCheckbox) {
      this._dataList.forEach((item) => {
        if(item['_checked']){
          item['_checked'] = true
        } else {
          item['_checked'] = false
        }
      });
    }
  }

  get tableData(): NzSafeAny[] {
    return this._dataList;
  }

  @Output() changePageNum = new EventEmitter<number>();
  @Output() changePageSize = new EventEmitter<number>();
  @Output() selectedChange: EventEmitter<NzSafeAny[]> = new EventEmitter<NzSafeAny[]>();
  // @Output() sortFn: EventEmitter<SortFile> = new EventEmitter<SortFile>();
  @Input() @InputBoolean() showSizeChanger = false;
  @Input() @InputBoolean() showQuickJumper = false;
  @Input() checkboxTH:  TemplateRef<any>;
  @Input() checkboxTD:  TemplateRef<any>;
  // High light dòng thêm mới trong table
  @Input() isShowSeletedRow = false;
  indeterminate: boolean = false;
  allChecked: boolean = false;

  constructor(private cdr: ChangeDetectorRef, public validateService: ValidateService) {
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['checkedCashArrayFromComment']) {
      this.refreshStatus();
    }
  }

  tableChangeDetection(): void {
    this._dataList = [...this._dataList];
    this.cdr.markForCheck();
  }

  refreshStatus(): void {
    this._dataList.forEach((item) => {
      const index = this.checkedCashArrayFromComment.findIndex((cashItem) => {
        return item[this.keyCheck] === cashItem[this.keyCheck];
      });
      if (index !== -1) {
        item['_checked'] = true;
      }
    });
    const allChecked = this._dataList.length > 0 && this._dataList.every((item) => {
      return item['_checked'] === true;
    });
    const allUnChecked = this._dataList.every(item => item['_checked'] !== true);
    this.allChecked = allChecked;
    this.indeterminate = !allChecked && !allUnChecked;
  }

  checkFn(dataItem: NzSafeAny, isChecked: boolean): void {
    dataItem['_checked'] = isChecked;
    const index = this.checkedCashArrayFromComment.findIndex((cashItem) => cashItem[this.keyCheck] === dataItem[this.keyCheck]);
    if (isChecked) {
      if (index === -1) {
        this.checkedCashArrayFromComment.push(dataItem);
      }
    } else {
      if (index !== -1) {
        this.checkedCashArrayFromComment.splice(index, 1);
      }
    }
  }

  onAllChecked(isChecked: boolean): void {
    this._dataList.forEach((item) => {
      this.checkFn(item, isChecked);
    });
    this.selectedChange.emit(this.checkedCashArrayFromComment);
    this.refreshStatus();
  }

  public checkRowSingle(isChecked: boolean, selectIndex: number): void {
    this.checkFn(this._dataList[selectIndex], isChecked);
    this.selectedChange.emit(this.checkedCashArrayFromComment);
    this.refreshStatus();
  }

  onResize({ width }: NzResizeEvent, col: string): void {
    this.tableConfig.headers = (this.tableConfig.headers.map(e => (e.title === col ? {
      ...e,
      width: +`${width}`
    } : e))) as MBTableHeader[];
  }

  onPageSizeChange($event: number): void {
    // console.log($event);
    this.changePageSize.emit($event);
  }

  onPageIndexChange($event: number): void {
    // console.log($event);
    this.changePageNum.emit($event);
  }

  // onQueryParamsChange(tableQueryParams: NzTableQueryParams): void {
  //   this.changePageNum.emit(tableQueryParams.pageSize);
  // }

  public trackByTableHead(index: number, item: NzSafeAny): NzSafeAny {
    return item;
  }

  public trackByTableBody(index: number, item: NzSafeAny): NzSafeAny {
    return item;
  }

  // Sort
  // changeSort(tableHeader: MBTableHeader): void {
  //   this.tableConfig.headers.forEach(item => {
  //     if (item.field !== tableHeader.field) {
  //       item.sortDir = undefined;
  //     }
  //   })
  //   const sortDicArray: [undefined, 'asc', 'desc'] = [undefined, 'asc', 'desc'];
  //   const index = sortDicArray.findIndex((item) => item === tableHeader.sortDir);
  //   tableHeader.sortDir = (index === sortDicArray.length - 1) ? sortDicArray[0] : sortDicArray[index + 1];
  //   this.sortFn.emit({fileName: tableHeader.field!, sortDir: tableHeader.sortDir})
  // }
  doClickItem(row: any) {
    this.onClickNode.emit(row);
  }
  doDblClickItem(row: any) {
    this.onDblClickNode.emit(row);
  }
}
