import {
  Component,
  EventEmitter,
  forwardRef,
  Injector,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {InputBoolean} from "ng-zorro-antd/core/util";
import {BaseResponse, Category, EmployeeDetail, Pagination} from "@hcm-mfe/shared/data-access/models";
import {debounceTime, noop, Subject, Subscription, takeUntil} from "rxjs";
import {ControlValueAccessor, NG_VALUE_ACCESSOR, NgControl} from "@angular/forms";
import * as _ from 'lodash';
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {StaffService} from "@hcm-mfe/shared/data-access/services";
import {MbDataPickerComponent} from "@hcm-mfe/shared/ui/mb-data-picker";
import {IPersonalInfoEmployeePolicy} from "../../../../../data-access/models/src/lib/IPersonalInfoEmployeePolicy";
import {ListReceivingExceptionAllowancesService} from "@hcm-mfe/policy-management/data-access/service";



@Component({
  selector: 'hcm-mfe-employee-data-search-policy',
  templateUrl: './employee-data-search-policy.component.html',
  styleUrls: ['./employee-data-search-policy.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => EmployeeDataSearchPolicyComponent)
    }
  ]
})
export class EmployeeDataSearchPolicyComponent implements OnInit, ControlValueAccessor, OnDestroy {

  @Input() mbLabelText: string;
  @Input() isHideFullName = false;
  @Input() @InputBoolean() multiple = false;
  @Input() mbDisable = false;
  @Input() selected: any;
  @Input() @InputBoolean() mbCanText = false;
  @Input() @InputBoolean() showEmpTypeSelect = false;
  @Output() selectedChange: EventEmitter<any> = new EventEmitter<any>();

  @Input() mbErrorDefs: { errorName: string, errorDescription: string }[];
  @Input() mbShowError = false;
  @Input() mbErrors: any;
  @Input() mbType: 'default' | 'warning' | 'error' | 'success' = 'default';
  @Input() status: 'ALL' | 'ACTIVE' | 'WORKING';
  @Input() scope: string;
  @Input() functionCode: string;

  @Input() @InputBoolean() isCheckEmployeeHasTaxNumber = false;
  empTypeCode: string = null;
  empTypeCodeList: Category[] = [];

  protected ngUnsubscribe: Subject<void> = new Subject<void>();

  data: IPersonalInfoEmployeePolicy[] = [];

  count = 0;
  loading = false;
  onTouched: () => void = noop;
  onChange: (_: any) => void = noop;
  ngControl?: NgControl;
  subs: Subscription[] = [];
  listAvatar: any[] = [];
  listIsLoading: boolean[] = [];

  oldInputSearch: string = null;

  pagination = new Pagination();
  isCall: boolean = false;
  @ViewChild('dataPicker') dataPicker: MbDataPickerComponent;

  constructor(
    private inj: Injector,
    private staffService: StaffService,
    private readonly listReceivingExceptionAllowancesService: ListReceivingExceptionAllowancesService
  ) {
  }


  ngOnInit(): void {
    this.ngControl = this.inj.get(NgControl);
    this.onInputChange = _.debounce(this.onInputChange, 1200);
  }

  onInputChange(value: string): void {
    this.resetDataSearch();
    // This aborts all HTTP requests.
    this.ngUnsubscribe.next();
    // This completes the subject properlly.
    this.ngUnsubscribe.complete();
    if (value && value.length > 1)
      this.getEmployeeData(value);
  }

  getDetailInfoEmployee(personalInfo: IPersonalInfoEmployeePolicy){
    this.isCall = true;
    const infoDetail = this.listReceivingExceptionAllowancesService.getDetailEmployeeInfo(personalInfo.employeeId).subscribe(res => {
      if(res) {
        this.onChange({ ...res.data, employeeId : personalInfo.empId });
        const data = {...personalInfo, ...res.data};
        data.empCode = personalInfo.employeeCode;
        data.empId = personalInfo.employeeId;
        this.selectedChange.emit(data);
      }
    });


  }

  getEmployeeData(keyword: string) {
    let param: any = this.pagination.getCurrentPage();
    param.status = this.status ? this.status : 'ALL';
    if (keyword) param = {
      // ...param, ...{ keySearch, startRecord: 0}
      ...param, ...{ keyword, startRecord: 0}, ...{scope: '00_VIEW', functionCode: this.functionCode}
    };
    this.loading = true;
    this.listAvatar = [];
    this.listIsLoading = [];
    this.resetDataSearch();
    this.staffService.getEmployeeData(param).pipe( takeUntil(this.ngUnsubscribe) )
      .subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.loading = false;
        this.data = [...this.data, ...res.data.listData];
        this.count = res.data.count;
        this.getListAvatar();
        this.pagination.pageNumber++;
      } else this.data = [];
    }, () => {
      this.loading = false;
      this.data = [];
    });
  }

  public resetDataSearch() {
    this.data = [];
    this.count = 0;
    this.pagination = new Pagination();
    this.pagination.pageNumber = 1;
  }

  getListAvatar() {
    // this.data.forEach((item, index) => {
    //   this.getAvatar(item.employeeId, index);
    // });
  }

  selectItem(personalInfo: IPersonalInfoEmployeePolicy) {
    // this.dataPicker.selectItem(personalInfo);
    this.dataPicker.selectItem(personalInfo);
    this.writeValue(personalInfo);
    if(!this.isCall){
      this.getDetailInfoEmployee(personalInfo)
    }

  }

  change(personalInfo: IPersonalInfoEmployeePolicy) {
    this.writeValue(personalInfo);
    if (!personalInfo) {
      this.isCall = false;
      this.selectedChange.emit(null);
      this.selected = null;
      return;
    }
    if(!this.isCall){
      this.getDetailInfoEmployee(personalInfo);
    }
    this.onChange(personalInfo);
    // this.selectedChange.emit(personalInfo);
  }

  writeValue(obj: IPersonalInfoEmployeePolicy): void {
    this.selected = obj ? obj : this.selected;
    if (this.dataPicker && !this.isHideFullName) {
      this.dataPicker.inputSearch = this.selected ? this.selected.fullName + (this.selected.employeeCode ? ('-' + this.selected.employeeCode) : '') : '';
    } else if(this.dataPicker && this.isHideFullName){
      this.dataPicker.inputSearch = this.selected && this.selected.employeeCode ?  this.selected.employeeCode : '' ;
    }
  }

  keyUpEnterSelectItem($event, item) {
    if ($event.source.selected) {
      this.selectItem(item);
    }
  }


  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  ngOnDestroy(): void {
    this.listAvatar = [];
    this.listIsLoading = [];
    this.subs.forEach(sub => sub.unsubscribe());
    // This aborts all HTTP requests.
    this.ngUnsubscribe.next();
    // This completes the subject properlly.
    this.ngUnsubscribe.complete();
  }


}
