import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeDataSearchPolicyComponent } from './employee-data-search-policy.component';

describe('EmployeeDataSearchPolicyComponent', () => {
  let component: EmployeeDataSearchPolicyComponent;
  let fixture: ComponentFixture<EmployeeDataSearchPolicyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmployeeDataSearchPolicyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeDataSearchPolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
