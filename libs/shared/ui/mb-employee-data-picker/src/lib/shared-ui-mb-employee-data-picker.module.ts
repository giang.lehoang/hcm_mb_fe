import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {EmployeeDataPickerComponent} from "./employee-data-picker/employee-data-picker.component";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiMbDataPickerModule} from "@hcm-mfe/shared/ui/mb-data-picker";
import {FormsModule} from "@angular/forms";
import {TranslateModule} from "@ngx-translate/core";
import {NzAutocompleteModule} from "ng-zorro-antd/auto-complete";
import {NzListModule} from "ng-zorro-antd/list";
import {NzAvatarModule} from "ng-zorro-antd/avatar";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzGridModule} from "ng-zorro-antd/grid";
import {NzSpinModule} from "ng-zorro-antd/spin";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {NzEmptyModule} from "ng-zorro-antd/empty";
import { EmployeeDataSearchPolicyComponent } from './employee-data-search-policy/employee-data-search-policy.component';
import {SharedPipesSplitModule} from "@hcm-mfe/shared/pipes/split";

@NgModule({
    imports: [CommonModule, SharedUiMbSelectModule, NzGridModule, SharedUiMbDataPickerModule, FormsModule, TranslateModule,
        NzAutocompleteModule, NzListModule, NzAvatarModule, SharedUiMbButtonModule, NzSpinModule, SharedUiMbInputTextModule, NzEmptyModule, SharedPipesSplitModule],
  declarations: [EmployeeDataPickerComponent, EmployeeDataSearchPolicyComponent],
  exports: [EmployeeDataPickerComponent, EmployeeDataSearchPolicyComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SharedUiMbEmployeeDataPickerModule {}
