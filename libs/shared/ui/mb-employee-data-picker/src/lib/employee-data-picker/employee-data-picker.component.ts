import {
  Component,
  EventEmitter,
  forwardRef,
  Injector,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import { InputBoolean } from 'ng-zorro-antd/core/util';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, NgControl } from '@angular/forms';
import { noop, Observable, of, Subscription } from 'rxjs';
import * as _ from 'lodash';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { catchError, map } from 'rxjs/operators';
import {BaseResponse, Category, EmployeeDetail, Pagination, PersonalInfo} from "@hcm-mfe/shared/data-access/models";
import {MbDataPickerComponent} from "@hcm-mfe/shared/ui/mb-data-picker";
import {PersonalInfoService} from "@hcm-mfe/dashboard-manager/data-access/services";
import {Constant, HTTP_STATUS_CODE} from '@hcm-mfe/shared/common/constants';
import {StringUtils} from "@hcm-mfe/shared/common/utils";
import {StaffService} from "@hcm-mfe/shared/data-access/services";

@Component({
  selector: 'employee-data-picker',
  templateUrl: './employee-data-picker.component.html',
  styleUrls: ['./employee-data-picker.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => EmployeeDataPickerComponent)
    }
    /*,
      {
        provide: NG_VALIDATORS,
        useExisting: forwardRef(() => MbInputTextComponent),
        multi: true
      }*/
  ]
})
export class EmployeeDataPickerComponent implements OnInit, ControlValueAccessor, OnDestroy {
  @Input() mbLabelText: string;
  @Input() isHideFullName = false;
  @Input() @InputBoolean() multiple = false;
  @Input() mbDisable = false;
  @Input() selected: EmployeeDetail;
  @Input() @InputBoolean() mbCanText = false;
  @Input() @InputBoolean() showEmpTypeSelect = false;
  @Output() selectedChange: EventEmitter<any> = new EventEmitter<any>();

  @Input() mbErrorDefs: { errorName: string, errorDescription: string }[];
  @Input() mbShowError = false;
  @Input() mbErrors: any;
  @Input() mbType: 'default' | 'warning' | 'error' | 'success' = 'default';
  @Input() status: 'ALL' | 'ACTIVE' | 'WORKING' | 'OUT';
  @Input() empTypeCode: string = null;
  @Input() scope: string;
  @Input() functionCode: string;

  @Input() @InputBoolean() isCheckEmployeeHasTaxNumber = false;

  empTypeCodeList: Category[] = [];

  data: EmployeeDetail[] = [];

  count = 0;
  loading = false;
  onTouched: () => void = noop;
  onChange: (_: any) => void = noop;
  ngControl?: NgControl;
  avtBase64Default = 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';
  avtBase64 = 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';
  subs: Subscription[] = [];
  listAvatar: any[] = [];
  listIsLoading: boolean[] = [];

  oldInputSearch: string = null;
  keyWordSearch: string = null;

  pagination = new Pagination();

  @ViewChild('dataPicker') dataPicker: MbDataPickerComponent;

  constructor(
    private inj: Injector,
    private staffService: StaffService,
    private personalInfoService: PersonalInfoService,
    private toastrService: ToastrService,
    private translateService: TranslateService
  ) {
    this.getEmpTypeCodes();
  }

  ngOnInit(): void {
    this.ngControl = this.inj.get(NgControl);
    this.onInputChange = _.debounce(this.onInputChange, 200);
  }

  ngOnDestroy(): void {
    this.listAvatar = [];
    this.listIsLoading = [];
    this.subs.forEach(sub => sub.unsubscribe());
  }

  getEmployeeData(keyword: string) {
    let param: any = this.pagination.getCurrentPage();
    if (keyword) param = { ...param, ...{ keyword: keyword } };
    if (this.empTypeCode) param = { ...param, ...{ empTypeCode: this.empTypeCode } };
    param.status = this.status ? this.status : 'ALL';
    param.scope = this.scope ? this.scope : '';
    param.functionCode = this.functionCode ? this.functionCode : '';
    this.loading = true;
    this.listAvatar = [];
    this.listIsLoading = [];
    this.staffService.getEmployeeData(param).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) {

        this.loading = false;
        this.data = [...this.data, ...res.data.listData];
        this.count = res.data.count;
        this.getListAvatar();
        this.pagination.pageNumber++;
      } else this.data = [];
    }, () => {
      this.loading = false;
      this.data = [];
    });
  }

  onInputChange(value: string): void {
    this.resetDataSearch();
    if (value && value.length > 1)
      this.getEmployeeData(value);
  }

  keyUpEnterSelectItem($event, item) {
    if ($event.source.selected) {
      this.selectItem(item);
    }
  }

  getEmpTypeCodes() {
    this.staffService.getCatalog(Constant.CATALOGS.DOI_TUONG_CV).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        if (this.empTypeCode) {
            this.empTypeCodeList = res.data.filter(item => item.value === this.empTypeCode);
        } else {
            this.empTypeCodeList = res.data;
        }
      };
    });
  }

  change(personalInfo: EmployeeDetail) {
    this.selectedChange.emit(personalInfo);
    this.writeValue(personalInfo);
    this.onChange(personalInfo);
  }

  selectItem(personalInfo: EmployeeDetail) {
    // console.log(personalInfo);
    // if(this.multiple){
    //   this.selectedChange.emit(personalInfo)
    //   this.writeValue('');
    //   if(this.dataPicker.modalRef){
    //     this.dataPicker.modalRef.destroy(false);
    //   }
    //   return;
    // }
    // check xem nh�n vi�n d� c� mst hay chua
    if (this.isCheckEmployeeHasTaxNumber) {
      this.validateEmployeeBeforeChoose(personalInfo).subscribe((res) => {
        if (res) {  // nv chua co mst
          return;
        } else {
          this.dataPicker.selectItem(personalInfo);
          this.writeValue(personalInfo);
          this.onChange(personalInfo);
        }
      })
    } else {
      this.dataPicker.selectItem(personalInfo);
      this.writeValue(personalInfo);
      this.onChange(personalInfo);
    }

  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.mbDisable = isDisabled;
  }

  writeValue(obj: any): void {
    this.selected = obj;
    if (this.dataPicker && !this.isHideFullName) {
      this.dataPicker.inputSearch = this.selected ? this.selected.fullName + (this.selected.employeeCode ? ('-' + this.selected.employeeCode) : '') : '';
    } else if(this.dataPicker && this.isHideFullName){
      this.dataPicker.inputSearch = this.selected && this.selected.employeeCode ?  this.selected.employeeCode : '' ;
    }
  }

  getListAvatar() {
    this.data.forEach((item, index) => {
      this.getAvatar(item.employeeId, index);
    });
  }

  getAvatar(employeeId: number, index: number) {
    this.listIsLoading[index] = true;
    this.subs.push(
      this.personalInfoService.getAvatar(employeeId).subscribe(res => {
        this.listAvatar[index] = res?.data ? 'data:image/jpg;base64,' + res?.data : null;
        this.listIsLoading[index] = false;
      }, () => {
        this.listAvatar[index] = this.avtBase64Default;
        this.listIsLoading[index] = false;
      })
    );
  }

  showModal() {
      this.resetDataSearch();
      this.keyWordSearch = this.oldInputSearch + '';
      this.getEmployeeData(this.keyWordSearch);
      this.dataPicker.openResult();
      console.log('modal')
  }

  getEmployeeDataInModal() {
    this.getEmployeeData(this.keyWordSearch);
  }

  public resetDataSearch(check?: boolean) {
    if (check) {
      this.keyWordSearch = "";
    }
    this.data = [];
    this.count = 0;
    this.pagination = new Pagination();
    this.pagination.pageNumber = 1;
  }

  private validateEmployeeBeforeChoose(personalInfo: EmployeeDetail): Observable<boolean> {
    const employeeId = personalInfo?.employeeId;
    return this.personalInfoService.getPersonalInfo(employeeId).pipe(
      map((res:any) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          const personalInfo: PersonalInfo = res?.data;
          if (!!personalInfo && StringUtils.isNullOrEmpty(personalInfo?.taxNo)) {
            this.toastrService.warning(this.translateService.instant('shared.notification.employeeNoTax'));
            return true;
          }
          return false;
        } else {
          this.toastrService.error(res?.message);
          return true;
        }
      }),
      catchError((err) => {
        this.toastrService.error(err?.message);
        return of(true);
      })
    )
  }

}
