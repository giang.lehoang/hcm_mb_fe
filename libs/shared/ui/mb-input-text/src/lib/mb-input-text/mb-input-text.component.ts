import {
  Component,
  EventEmitter,
  forwardRef,
  Injector,
  Input,
  OnChanges,
  OnInit,
  Output,
  TemplateRef,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, NgControl } from '@angular/forms';
import { noop } from 'rxjs';
import { InputBoolean } from 'ng-zorro-antd/core/util';
import { NzAutocompleteComponent } from 'ng-zorro-antd/auto-complete';
import {AutoSizeType} from "ng-zorro-antd/input";
import { DecimalPipe } from '@angular/common';
import { SpecialInputDirective } from '@hcm-mfe/shared/directives/special-input';

export class ModelInput {
  groupType?: 'noIcon' | 'icon' | 'textareaInputSpecial' | 'textarea' = 'noIcon';
  type?: 'default' | 'warning' | 'error' | 'success' = 'default';
  labelText?: string;
  textMessageValue?: string;
  showIcon?: boolean = true;
  placeholder?: string = '';
  suffixIcon?: 'search' | 'down' | string = null;
  prefixIcon?: 'search' | 'down' | string = null;
  prefixTemplate?: string | TemplateRef<any>;
  suffixTemplate?: string | TemplateRef<any>;
  autoSize?: AutoSizeType | boolean = { minRows: 3, maxRows: 4 };
  rows?: number;
  disable?: boolean;
  autofocus?: 'autofocus' | null; // Chưa hoạt động
  showFlexEnd?: boolean = true;
  isDatepicker?: boolean = false;
  dateConfig?: any;
  showError?: boolean = false;
}

@Component({
  selector: 'mb-input-text',
  templateUrl: './mb-input-text.component.html',
  styleUrls: ['./mb-input-text.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => MbInputTextComponent),
    } /*,
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => MbInputTextComponent),
      multi: true
    }*/,
  ],
})
export class MbInputTextComponent implements OnInit, ControlValueAccessor, OnChanges {
  // , Validator {
  @Input() mbConfig: ModelInput = new ModelInput();
  @Input() mbLabelText: string;
  @Input() mbPrefixIcon: 'search' | 'calendar' | 'user';
  @Input() mbSuffixIcon: 'down' | 'eye' | 'key' | 'search';
  @Input() mbPrefixTemplate: string | TemplateRef<any>;
  @Input() mbSuffixTemplate: string | TemplateRef<any>;
  @Input() mbPlaceholderText = '';
  @Input() @InputBoolean() mbDisable = false;
  @Input() mbAutofocus = false;
  @Input() mbType: 'default' | 'warning' | 'error' | 'success';
  @Input() mbInputType: 'text' | 'number' = 'text';
  @Input() @InputBoolean() mbIsTextAre = false;
  @Input() @InputBoolean() mbIsTextAreInputSpecial = false;
  @Input() @InputBoolean() mbIsTextAreAutoResize = false;
  @Input() mbShowFlexEnd = true;
  @Input() @InputBoolean() mbIsDatepicker = false;
  @Input() mbDatepickerConfig: any;
  @Input() mbErrorDefs: { errorName: string; errorDescription: string }[];
  @Input() mbShowIconMessage = true;
  @Input() mbShowError = false;
  @Input() mbErrors: any;
  @Input() mbMaxLength: number = 10000000000000;
  @Input() mnMinLength: number = 0;
  @Input() @InputBoolean() mbRequired = false;
  @Input() @InputBoolean() mbReadOnly = false;
  @Input() mbAutocomplete: NzAutocompleteComponent;
  @Input() removeMinHeight: boolean = false;
  @Input() @InputBoolean() defaultInput?: boolean = false;
  @Input() minNumber: string = '';
  @Input() maxNumber: number = 10000000000000;
  @Input() isFormatterCurrency = false;
  @Input() isSpecial = false;
  @Input() isFocus = false;

  @Output() mbBlur: EventEmitter<any> = new EventEmitter<any>();
  @Output() mbKeyup: EventEmitter<any> = new EventEmitter<any>();
  @Output() mbChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() mbClick: EventEmitter<any> = new EventEmitter<any>();
  @Output() mbFocus: EventEmitter<any> = new EventEmitter<any>();

  inputGroupClass = 'input__group';
  inputGroupGroupClass = 'input__group--group';
  inputTextClass = 'input__text';
  inputMessageClass = 'input__message';
  inputAreaClass = 'input__area';

  classGroup = {
    noIcon: 'input__group--no-icon',
    icon: 'input__group--icon',
  };

  classGroupGroup = {
    default: 'input__group--group--default',
    warning: 'input__group--group--warning',
    error: 'input__group--group--error',
    success: 'input__group--group--success',
  };

  classInput = {
    default: 'input__text--default',
    warning: 'input__text--warning',
    error: 'input__text--error',
    success: 'input__text--success',
  };

  classMessage = {
    default: 'input__message--default',
    warning: 'input__message--warning',
    error: 'input__message--error',
    success: 'input__message--success',
  };

  classIcon = {
    warning: 'warning',
    error: 'close-circle',
    success: 'check-circle',
  };

  onTouched: () => void = noop;
  onChange: (_: any) => void = noop;
  ngControl?: NgControl;
  iconType: string;
  value: any;
  bsDatepicker = 'bsDatepicker';

  constructor(private inj: Injector, private decimalPipe: DecimalPipe) {
    this.inputGroupClass = 'input__group' + ' ';
    this.inputGroupGroupClass = 'input__group--group' + ' ';
    this.inputTextClass = 'input__text' + ' ';
    this.inputMessageClass = 'input__message ' + ' ';
    this.inputAreaClass = 'input__area ' + ' ';
  }

  ngOnInit() {
    this.ngControl = this.inj.get(NgControl);
  }

  ngOnChanges() {
    this.inputGroupClass = 'input__group' + ' ';
    this.inputGroupGroupClass = 'input__group--group' + ' ';
    this.inputTextClass = 'input__text' + ' ';
    this.inputMessageClass = 'input__message ' + ' ';
    this.inputAreaClass = 'input__area ' + ' ';
    this.setMbConfig();
    this.configInput();
    this.setErrorMessage();
  }

  setMbConfig() {
    if (this.mbType) {
      this.mbConfig.type = this.mbType;
    }
    if (this.mbLabelText) {
      this.mbConfig.labelText = this.mbLabelText;
    }
    if (this.mbPlaceholderText) {
      this.mbConfig.placeholder = this.mbPlaceholderText;
    }
    this.mbConfig.disable = this.mbDisable;

    if (this.mbPrefixIcon) {
      this.mbConfig.groupType = 'icon';
      this.mbConfig.prefixIcon = this.mbPrefixIcon;
    }
    if (this.mbPrefixTemplate) {
      this.mbConfig.groupType = 'icon';
      this.mbConfig.prefixTemplate = this.mbPrefixTemplate;
    }
    if (this.mbSuffixIcon) {
      this.mbConfig.groupType = 'icon';
      this.mbConfig.suffixIcon = this.mbSuffixIcon;
    }
    if (this.mbSuffixTemplate) {
      this.mbConfig.groupType = 'icon';
      this.mbConfig.suffixTemplate = this.mbSuffixTemplate;
    }
    if (this.mbAutofocus) {
      this.mbConfig.autofocus = 'autofocus';
    }
    if (this.mbIsTextAre) {
      this.mbConfig.groupType = 'textarea';
    }
    if (this.mbIsTextAreInputSpecial) {
      this.mbConfig.groupType = 'textareaInputSpecial';
    }
    if (!this.mbShowFlexEnd) {
      this.mbConfig.showFlexEnd = this.mbShowFlexEnd;
    }
    if (this.mbIsDatepicker) {
      this.mbConfig.isDatepicker = this.mbIsDatepicker;
    }
    this.mbConfig.showError = this.mbShowError;
    this.mbConfig.showIcon = this.mbShowIconMessage;
    if (this.mbDatepickerConfig) {
      this.mbConfig.dateConfig = this.mbDatepickerConfig;
    } else {
      this.mbConfig.dateConfig = {
        dateInputFormat: 'DD/MM/YYYY',
        returnFocusToInput: true,
      };
    }
  }

  configInput() {
    if (this.mbConfig?.groupType === 'icon') {
      this.inputGroupClass += this.classGroup.icon;
    } else {
      this.inputGroupClass += this.classGroup.noIcon;
    }
    switch (this.mbConfig?.type) {
      case 'default':
        this.inputGroupGroupClass += this.classGroupGroup.default;
        this.inputTextClass += this.classInput.default;
        this.inputMessageClass += this.classMessage.default;
        this.inputAreaClass += this.classInput.default;
        break;
      case 'warning':
        this.inputGroupGroupClass += this.classGroupGroup.warning;
        this.inputTextClass += this.classInput.warning;
        this.inputMessageClass += this.classMessage.warning;
        this.iconType = this.classIcon.warning;
        this.inputAreaClass += this.classInput.warning;
        break;
      case 'error':
        this.inputGroupGroupClass += this.classGroupGroup.error;
        this.inputTextClass += this.classInput.error;
        this.inputMessageClass += this.classMessage.error;
        this.iconType = this.classIcon.error;
        this.inputAreaClass += this.classInput.error;
        break;
      case 'success':
        this.inputGroupGroupClass += this.classGroupGroup.success;
        this.inputTextClass += this.classInput.success;
        this.inputMessageClass += this.classMessage.success;
        this.iconType = this.classIcon.success;
        this.inputAreaClass += this.classInput.success;
        break;
      default:
        this.inputGroupGroupClass += this.classGroupGroup.default;
        this.inputTextClass += this.classInput.default;
        this.inputMessageClass += this.classMessage.default;
        this.inputAreaClass += this.classInput.default;
        break;
    }
  }

  setErrorMessage() {
    if (this.mbErrors) {
      for (const error of this.mbErrorDefs) {
        const key = error.errorName;
        if (this.mbErrors[key]) {
          this.mbConfig.textMessageValue = error.errorDescription;
        }
      }
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.mbDisable = isDisabled;
    this.mbConfig.disable = isDisabled;
  }

  writeValue(obj: any): void {
    this.value = obj ? obj : ''; // fix ô textarea hiện chứ undefined
    if (this.mbIsDatepicker && this.value) {
      this.value = new Date(this.value);
    }
    this.onChange(this.value);
  }

  onInputChange($event: any) {
    this.onChange($event);
    this.mbChange.emit($event);
  }

  inputBlur($event: any) {
    this.onTouched();
    this.mbBlur.emit($event);
  }

  inputClick($event: any, value: string) {
    this.mbClick.emit($event);
    this.onChange(this.value);
  }

  inputFill($event: any) {
    this.mbKeyup.emit($event);
  }
  preventTypeSpecialSymbol(event) {
    if (+this.minNumber >= 0 && (event.key === '-' || event.key === '+')) {
      event.preventDefault();
      return;
    }
  }
  onPaste(event) {
    const clipboardData = event.clipboardData;
    const pastedText = clipboardData.getData('text');
    if(+this.minNumber >= 0 && (pastedText.includes('-') || pastedText.includes('+'))) {
      event.preventDefault();
      return;
    }
  }
  /*registerOnValidatorChange(fn: () => void): void {
    this.onChange(fn);
  }

  validate(control: FormControl) {
    return null;
  }*/
  inputFocus($event: FocusEvent) {
    this.mbFocus.emit($event);
  }

  formatterCurrency = (value: number): string => {
    if (!Number.isInteger(value)) {
      return '';
    }
    return this.decimalPipe.transform(value, '1.0-2')!;
  };

  parserCurrency = (value: string): string => {
    if (!value) {
      return '';
    }
    return value.replace(/\D+/g, '');
  };
}
