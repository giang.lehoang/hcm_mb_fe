import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MbInputTextComponent } from './mb-input-text/mb-input-text.component';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzAutocompleteModule } from 'ng-zorro-antd/auto-complete';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { SharedDirectivesSpecialInputModule } from '@hcm-mfe/shared/directives/special-input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { SharedDirectivesAutofocusModule } from '@hcm-mfe/shared/directives/autofocus';

@NgModule({
  imports: [CommonModule, NzInputModule, NzAutocompleteModule, BsDatepickerModule, SharedDirectivesSpecialInputModule,
    FormsModule, NzIconModule, ReactiveFormsModule, NzInputNumberModule,SharedDirectivesAutofocusModule],
  declarations: [MbInputTextComponent],
  exports: [MbInputTextComponent]
})
export class SharedUiMbInputTextModule {}
