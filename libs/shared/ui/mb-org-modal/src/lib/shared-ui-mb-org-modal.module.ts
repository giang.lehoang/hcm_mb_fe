import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MbOrgModalComponent } from './mb-org-modal/mb-org-modal.component';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzCardModule } from 'ng-zorro-antd/card';
import { TranslateModule } from '@ngx-translate/core';
import { NzTableModule } from 'ng-zorro-antd/table';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { SharedUiMbOrgTreeModule } from '@hcm-mfe/shared/ui/mb-org-tree';
import { SharedPipesTruncateModule } from '@hcm-mfe/shared/pipes/truncate';
import {FormsModule} from "@angular/forms";

@NgModule({
  imports: [CommonModule, FormsModule,NzModalModule, NzCardModule, TranslateModule, NzTableModule, NzIconModule, NzPaginationModule,
    SharedUiMbInputTextModule, SharedUiMbButtonModule, SharedUiMbOrgTreeModule, SharedPipesTruncateModule],
  declarations: [MbOrgModalComponent],
  exports: [MbOrgModalComponent],
})
export class SharedUiMbOrgModalModule {}
