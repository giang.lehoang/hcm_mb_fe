import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { HTTP_STATUS_CODE } from '@hcm-mfe/shared/common/constants';
import { ValidateService } from '@hcm-mfe/shared/core';
import {OrgService} from "@hcm-mfe/shared/data-access/services";

@Component({
  selector: 'app-mb-org-modal',
  templateUrl: './mb-org-modal.component.html',
  styleUrls: ['./mb-org-modal.component.scss']
})
export class MbOrgModalComponent implements OnInit {

  @Input() showContent: boolean = false;
  @Input() closeModalWhenClick: boolean = true;
  @Output() onClickNode: EventEmitter<any> = new EventEmitter<any>();
  @Output() onClickItem: EventEmitter<any> = new EventEmitter<any>();
  @Output() onCloseModal: EventEmitter<any> = new EventEmitter<any>();

  nzWidth: number;
  searchOrgValue: string;
  data: any[] = [];
  totalData: number = 0;
  parentId: number;
  loading: boolean = false;
  pageSize: number = 10;

  constructor(private orgService: OrgService,
              public validateService: ValidateService) {
    this.nzWidth = window.innerWidth > 767 ? window.innerWidth / 2 : window.innerWidth;
  }

  ngOnInit(): void {
  }

  doSelectOrg(data: any) {
    if (data != undefined) {
      this.onClickNode.emit(data);
      if (this.closeModalWhenClick) {
        this.showContent = false;
      }
    }
  }

  doSelectTreeNode(data: any, index?: number) {
    if (data != undefined) {
      this.parentId = data.nodeId;
      this.doGetByParentNode(data.nodeId, index);
    }
  }

  doGetByParentNode(parentId: number, index?: number) {
    this.loading = true;
    let pageNumber = 0;
    if (index != undefined && index >= 1) {
      pageNumber = (index - 1) * this.pageSize;
    }
    this.orgService.doGetByParentNode(parentId, pageNumber).subscribe(res => {
      if (res.code == HTTP_STATUS_CODE.OK) {
        this.data = res.data.listData;
        this.totalData = res.data.count;
        this.loading = false;
      }
    }, error => {
      this.data = [];
      this.totalData = 0;
      this.loading = false;
    });
  }

  doSearchOrg() {
    // let param: any = {};
    // this.loading = true;
    // if (this.parentId != undefined)
    //   param.parentId = this.parentId;
    // if (this.searchOrgValue != undefined && this.searchOrgValue !== '')
    //   param.name = this.searchOrgValue;
    // this.orgService.doSearchOrg(param).subscribe(res => {
    //   this.data = res.data;
    //   this.loading = false;
    // }, error => {
    //   this.loading = false;
    // })
  }

  closeModal() {
    this.showContent = false;
    this.onCloseModal.emit();
  }
}
