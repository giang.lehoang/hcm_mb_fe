import {
  Component,
  DoCheck,
  EventEmitter,
  forwardRef,
  Injector,
  Input,
  OnChanges,
  OnInit,
  Output
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, NgControl } from '@angular/forms';
import { noop } from 'rxjs';
import { InputBoolean } from 'ng-zorro-antd/core/util';

export class SelectCheckAbleModal {
  action?: 'SUBMIT' | 'CANCEL' | 'NG_MODEL_CHANGE';
  isCheckAll?= false;
  listOfSelected?: any[] = [];
  itemChecked?: any;
  listItemSelected?: any[] = [];

  constructor(action?: 'SUBMIT' | 'CANCEL' | 'NG_MODEL_CHANGE', isCheckAll: boolean = false, listOfSelected: any[] = [], itemChecked?: any) {
    this.action = action;
    this.isCheckAll = isCheckAll;
    this.listOfSelected = listOfSelected;
    this.itemChecked = itemChecked;
  }
}

@Component({
  selector: 'mb-select-check-able',
  templateUrl: './select-able.component.html',
  styleUrls: ['./select-able.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => SelectAbleComponent),
    }
  ]
})
export class SelectAbleComponent implements OnInit, ControlValueAccessor, OnChanges, DoCheck {
  @Input() mbDataSelects: any[] = [];
  @Input() mbShowCheckAll = true;
  @Input() mbShowAction = true;
  @Input() mbShowSearch = true;
  @Input() mbCheckAllKey = 'ALL';
  @Input() mbPlaceholder = '';
  @Input() mbSelectIcon: 'search' | 'down' | string;
  @Input() mbMaxTagCount = 3;
  @Input() mbOptionHeightPx = 56;
  @Input() mbOptionOverflowSize = 5;
  @Input() mbLabelText: string;
  @Input() mbKeyLabel: string = 'label';
  @Input() mbKeyValue: string = 'value';
  @Input() @InputBoolean() mbKeyValueStringType: boolean = false;
  @Input() mbErrorDefs: { errorName: string, errorDescription: string }[];
  @Input() mbShowIconMessage = true;
  @Input() mbShowError = false;
  @Input() mbErrors: any;
  @Input() mbType: 'default' | 'warning' | 'error' | 'success' = 'default';
  @Input() mbIsLoading = false;
  @Input() mbIsOpenOptions = false;
  @Input() mbTranslate = false;
  @Input() hideDropdown = false;
  @Input() listResultSelected = [];
  @Output() mbChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() mbEventEmit: EventEmitter<SelectCheckAbleModal> = new EventEmitter<SelectCheckAbleModal>();
  @Output() mbOnSearch: EventEmitter<any> = new EventEmitter<any>();
  @Input() listOfSelectedValue: any[] = [];
  @Input() mbDisabled: boolean = false;




  onTouched: () => void = noop;
  onChange: (_: any) => void = noop;
  ngControl?: NgControl;
  inputMessageClass = 'input__message';
  textMessageValue: any;
  showIcon: any;
  iconType: any;
  inputGroupGroupClass = 'select__group--group';

  classGroupGroup = {
    default: 'select__group--group--default',
    warning: 'select__group--group--warning',
    error: 'select__group--group--error',
    success: 'select__group--group--success',
  };

  classMessage = {
    default: 'select__message--default',
    warning: 'select__message--warning',
    error: 'select__message--error',
    success: 'select__message--success'
  };

  classIcon = {
    warning: 'warning',
    error: 'close-circle',
    success: 'check-circle'
  };

  constructor(
    private inj: Injector,
  ) {
  }

  ngDoCheck(): void {
    if(this.listOfSelectedValue?.length > this.mbMaxTagCount) {
      document.getElementsByClassName('ant-select-selection-item ant-select-selection-item-disabled ng-star-inserted')[0]?.removeAttribute('title');
    }
  }

  ngOnInit(): void {
    this.ngControl = this.inj.get(NgControl);

  }

  ngOnChanges() {
    this.configInput();
    this.setErrorMessage();
    if (this.listOfSelectedValue) {
      const emit = new SelectCheckAbleModal('SUBMIT', this.listOfSelectedValue?.length === this.mbDataSelects?.length, this.listOfSelectedValue);
      this.listOfSelectedValue.forEach(item => {
        let it = this.mbDataSelects ? this.mbDataSelects[this.mbDataSelects.findIndex(i => i[this.mbKeyValue] === item)] : {};
        emit.listItemSelected.push(it);
      });
      this.mbEventEmit.emit(emit);
    }
  }

  configInput() {
    this.inputGroupGroupClass = 'select__group--group' + ' ';
    this.inputMessageClass = 'select__message ' + ' ';
    switch (this.mbType) {
      case 'default':
        this.inputGroupGroupClass += this.classGroupGroup.default;
        this.inputMessageClass += this.classMessage.default;
        break;
      case 'warning':
        this.inputGroupGroupClass += this.classGroupGroup.warning;
        this.inputMessageClass += this.classMessage.warning;
        this.iconType = this.classIcon.warning;
        break;
      case 'error':
        this.inputGroupGroupClass += this.classGroupGroup.error;
        this.inputMessageClass += this.classMessage.error;
        this.iconType = this.classIcon.error;
        break;
      case 'success':
        this.inputGroupGroupClass += this.classGroupGroup.success;
        this.inputMessageClass += this.classMessage.success;
        this.iconType = this.classIcon.success;
        break;
      default:
        this.inputGroupGroupClass += this.classGroupGroup.default;
        this.inputMessageClass += this.classMessage.default;
        break;
    }
  }

  setErrorMessage() {
    if (this.mbErrors) {
      for (let error of this.mbErrorDefs) {
        let key = error.errorName;
        if (this.mbErrors[key]) {
          this.textMessageValue = error.errorDescription;
        }
      }
    }
  }

  checkAll($event: boolean) {
    if ($event) {
      this.listOfSelectedValue = this.mbDataSelects?.filter(item => item.disable !== true).map((item) => {
        return item[this.mbKeyValue];
      });
    } else {
      this.listOfSelectedValue = [];
    }
    const emit = new SelectCheckAbleModal('NG_MODEL_CHANGE', $event, this.listOfSelectedValue, this.mbCheckAllKey);
    this.mbEventEmit.emit(emit);
    this.onChange(this.listOfSelectedValue);
  }

  cancel() {
    this.mbIsOpenOptions = false;
    this.listOfSelectedValue = [];
    const emit = new SelectCheckAbleModal('CANCEL');
    this.mbEventEmit.emit(emit);
  }

  submit() {
    this.mbIsOpenOptions = false;
    const emit = new SelectCheckAbleModal('SUBMIT', this.listOfSelectedValue?.length === this.mbDataSelects.length, this.listOfSelectedValue);
    this.listOfSelectedValue.forEach(item => {
      let it = this.mbDataSelects ? this.mbDataSelects[this.mbDataSelects.findIndex(i => i[this.mbKeyValue] === item)] : {};
      emit.listItemSelected.push(it);
    });
    this.mbEventEmit.emit(emit);
  }

  checkItem($event: boolean, value: any) {
    let dataSelected = Object.assign([], this.listOfSelectedValue);
    if ($event) {
      dataSelected.push(value);
    } else {
      dataSelected = dataSelected.filter(item => item !== value);
    }
    this.listOfSelectedValue = dataSelected;
    const emit = new SelectCheckAbleModal('NG_MODEL_CHANGE', this.listOfSelectedValue?.length === this.mbDataSelects.length, this.listOfSelectedValue, value + '');
    this.mbEventEmit.emit(emit);
    this.onChange(this.listOfSelectedValue);
  }

  selectItem($event: any) {
    this.onChange($event);
    const emit = new SelectCheckAbleModal('NG_MODEL_CHANGE', this.listOfSelectedValue?.length === this.mbDataSelects.length, this.listOfSelectedValue, 'UNDEFINED');
    this.listOfSelectedValue.forEach(item => {
      let it = this.mbDataSelects ? this.mbDataSelects[this.mbDataSelects.findIndex(i => i[this.mbKeyValue] === item)] : {};
      emit.listItemSelected.push(it);
    });
    this.mbEventEmit.emit(emit);
    this.mbChange.emit($event)
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
  }

  writeValue(obj: any): void {
    this.listOfSelectedValue = obj;
    this.onChange(this.listOfSelectedValue);
  }

  getListName(selectedList: any) {
    return this.mbDataSelects.filter(el => selectedList.includes(el[this.mbKeyValue])).map(el => el[this.mbKeyLabel]).join(', ');
  }

  onSearch($event: string) {
    this.mbOnSearch.emit($event);
  }
}
