import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectAbleComponent } from './select-able/select-able.component';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { FormsModule } from '@angular/forms';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';

@NgModule({
  imports: [CommonModule, NzIconModule, NzSelectModule, NzCheckboxModule, NzButtonModule, FormsModule, NzToolTipModule],
  declarations: [SelectAbleComponent],
  exports: [SelectAbleComponent],
})
export class SharedUiMbSelectCheckAbleModule {}
