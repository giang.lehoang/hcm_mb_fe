import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TreeSelectComponent} from "./tree-select/tree-select.component";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzTreeSelectModule} from "ng-zorro-antd/tree-select";
import {FormsModule} from "@angular/forms";
import {NzSelectModule} from "ng-zorro-antd/select";

@NgModule({
  imports: [CommonModule, NzIconModule, NzTreeSelectModule, FormsModule, NzSelectModule],
  declarations: [TreeSelectComponent],
  exports: [TreeSelectComponent],
})
export class SharedUiMbTreeSelectModule {}
