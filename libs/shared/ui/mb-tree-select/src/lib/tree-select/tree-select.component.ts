import {
  AfterViewInit,
  ChangeDetectionStrategy, ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  Injector,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, NgControl } from '@angular/forms';
import { noop, of, Subject, Subscription, takeUntil } from 'rxjs';
import { InputBoolean } from 'ng-zorro-antd/core/util';
import { OrganizationNode, ResponseEntityModel, SelectModal } from '@hcm-mfe/shared/data-access/models';
import { CommonUtils, CustomToastrService, ShareService } from '@hcm-mfe/shared/core';
import { NzTreeSelectComponent } from 'ng-zorro-antd/tree-select';
import * as _ from 'lodash';

@Component({
  selector: 'mb-tree-select',
  templateUrl: './tree-select.component.html',
  styleUrls: ['./tree-select.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => TreeSelectComponent),
    }
  ]
})
export class TreeSelectComponent implements OnInit, ControlValueAccessor, OnChanges, OnDestroy, AfterViewInit {
  @Input() mbShowSearch = true;
  @Input() @InputBoolean() mbDisable = false;
  @Input() @InputBoolean() mbServerSearch = false;
  @Input() mbPlaceholder = '';
  @Input() mbDropdownClassName = 'dropdown-tree-select';
  @Input() mbSelectIcon?: 'search' | 'down' | string;
  @Input() mbLabelText: string = '';
  @Input() mbDisabled: string = 'disabled';
  @Input() mbErrorDefs: {errorName: string, errorDescription: string}[] = [];
  @Input() mbShowIconMessage = true;
  @Input() mbShowError = false;
  @Input() mbErrors: any;
  @Input() mbType: 'default' | 'warning' | 'error' | 'success' = 'default';
  @Input() mbIsLoading = false;
  @Input() mbShowClear = true;
  @Input() mbIsMultiple = false;
  @Output() mbEventEmit: EventEmitter<SelectModal> = new EventEmitter<SelectModal>();
  @Output() mbSelect: EventEmitter<any> = new EventEmitter<any>();
  @Output() mbOnSearch: EventEmitter<any> = new EventEmitter<any>();
  itemSelectedValue: any = null;


  @Input() type: string = '';
  @Input() param: any;
  @Input() objectSearch: any;
  @Input() orgName: string = '';
  @Input() functionCode?: string;

  subs: Subscription[] = [];
  mapOfExpanded: { [key: string]: any[] } = {};
  mapOfOrgId: { [key: string]: any[] } = {};

  isOpenOptions = false;
  onTouched: () => void = noop;
  onChange: (_: any) => void = noop;
  ngControl?: NgControl;
  inputMessageClass = 'input__message';
  textMessageValue: any;
  isDefaultExpandAll = false;
  showIcon: any;
  iconType: any;
  inputGroupGroupClass = 'select__group--group';

  classGroupGroup = {
    default: 'select__group--group--default',
    warning: 'select__group--group--warning',
    error: 'select__group--group--error',
    success: 'select__group--group--success',
  };

  classMessage = {
    default: 'select__message--default',
    warning: 'select__message--warning',
    error: 'select__message--error',
    success: 'select__message--success'
  };

  classIcon = {
    warning: 'warning',
    error: 'close-circle',
    success: 'check-circle'
  };
  @ViewChild(NzTreeSelectComponent) treeSelectComponent!: NzTreeSelectComponent;
  @ViewChild("elementTreeSelect") elementTreeSelect!: ElementRef;

  protected ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(
    private inj: Injector,
    private shareService: ShareService,
    private toastrCustom: CustomToastrService,
    private cdr: ChangeDetectorRef,
  ) {
  }

  ngOnInit(): void {
    this.ngControl = this.inj.get(NgControl);
    this.getAllTree(this.param, this.objectSearch, this.orgName);
    this.onInputChange = _.debounce(this.onInputChange, 100);
  }

  ngOnChanges() {
    this.configInput();
    this.setErrorMessage();
    if (this.itemSelectedValue) {
      const emit = new SelectModal('NG_MODEL_CHANGE',this.itemSelectedValue);
      this.mbEventEmit.emit(emit);
    }
  }

  configInput() {
    this.inputGroupGroupClass = 'select__group--group' + ' ';
    this.inputMessageClass = 'select__message ' + ' ';
    switch (this.mbType) {
      case 'default':
        this.inputGroupGroupClass += this.classGroupGroup.default;
        this.inputMessageClass += this.classMessage.default;
        break;
      case 'warning':
        this.inputGroupGroupClass += this.classGroupGroup.warning;
        this.inputMessageClass += this.classMessage.warning;
        this.iconType = this.classIcon.warning;
        break;
      case 'error':
        this.inputGroupGroupClass += this.classGroupGroup.error;
        this.inputMessageClass += this.classMessage.error;
        this.iconType = this.classIcon.error;
        break;
      case 'success':
        this.inputGroupGroupClass += this.classGroupGroup.success;
        this.inputMessageClass += this.classMessage.success;
        this.iconType = this.classIcon.success;
        break;
      default:
        this.inputGroupGroupClass += this.classGroupGroup.default;
        this.inputMessageClass += this.classMessage.default;
        break;
    }
  }

  setErrorMessage() {
    if (this.mbErrors) {
      for(let error of this.mbErrorDefs) {
        let key = error.errorName;
        if (this.mbErrors[key]) {
          this.textMessageValue = error.errorDescription;
        }
      }
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.mbDisable = isDisabled;
  }

  writeValue(obj: any): void {
    this.itemSelectedValue = obj;
    this.onChange(this.itemSelectedValue);
  }

  expandKeys: number[] | any = [];

  nodeAll: OrganizationNode| any;
  treeOrg: OrganizationNode[] | any = [{ title: 'Loading...', key: ' ', icon: 'loading' }];
  treeOrgTerm: OrganizationNode[] | any = [{ title: 'Loading...', key: ' ', icon: 'loading' }];

  getAllTree(param?: any, objectSearch?: any, orgName?: any): void {
    const params = {
      is_author: true
    };

    const sub = this.shareService.getOrganizationTree(this.type === "PY" ? params : "").subscribe(
      (data: ResponseEntityModel<OrganizationNode> | any) => {
        this.nodeAll = data.data;
        this.treeOrg = [data.data];
        this.addKey(this.treeOrg, '');
        this.treeOrg.forEach((el:  OrganizationNode) => this.expandKeys.push(el.key));
        this.treeOrgTerm = JSON.parse(JSON.stringify(this.treeOrg));
        if (param)
        param.id = '' + data.data.orgId ?? '';
        orgName = data.data.orgName;
        if (objectSearch) {
          objectSearch.orgId = String(data.data?.orgId);
          objectSearch.orgName = data.data?.orgName;
        }
        this.cdr.markForCheck();
      },
      (error) => {
        this.toastrCustom.error(error.message)
      }
    );
    this.subs.push(sub);
  }


  addKey(data: OrganizationNode[] | undefined, keyParent?: any, listParent?: any[]) {
    if (!data) {
      return
    }
    for (let i = 0; i < data.length; i++) {
      data[i].title = data[i].orgName;
      data[i].id = data[i].orgId;
      data[i].expanded = false;
      // data[i].key = keyParent + '-' + i;
      data[i].key = data[i].orgId;
      let listKey: any[] = [];
      listKey = [...listParent ?? [], data[i].key]
      this.mapOfExpanded[data[i].key.toString()] = [...listParent ?? [], data[i].key];
      if (!data[i].subs || data[i].subs?.length === 0) {
        data[i].expanded = true;
      } else {
        this.mapOfOrgId[data[i].key.toString()] = data[i].subs?.map(e => e.orgId) ?? [];
      }
      this.addKey(data[i].subs, data[i].key, listKey)
      data[i].children = data[i].subs;
    }
  }

  openPopup(event: boolean) {
    this.isDefaultExpandAll = false;
    if (CommonUtils.isNullOrEmpty(this.treeSelectComponent.nzSelectSearchComponent.value)) {
      this.treeOrg = JSON.parse(JSON.stringify(this.treeOrgTerm));
      this.cdr.markForCheck();
    }
    if (this.itemSelectedValue && this.itemSelectedValue != '') {
      if(event) {
        this.expandKeys = this.mapOfExpanded[this.itemSelectedValue.toString()].filter(e => e !== this.itemSelectedValue);
        let index = 0;
        this.mapOfExpanded[this.itemSelectedValue.toString()].forEach(el => {
          if(this.mapOfOrgId[el.toString()]) {
            index = index + this.mapOfOrgId[el.toString()].indexOf(el) + 1;
          }
        })
        for(let  i = 1; i < this.mapOfExpanded[this.itemSelectedValue.toString()].length + 1; i++) {
          const parentKey = this.mapOfExpanded[this.itemSelectedValue.toString()][i-1];
          const key = this.mapOfExpanded[this.itemSelectedValue.toString()][i];
          if(this.mapOfOrgId[parentKey.toString()]) {
            index = index + this.mapOfOrgId[parentKey.toString()].indexOf(key) + 1;
          }
          if(i === this.mapOfExpanded[this.itemSelectedValue.toString()].length) {
            this.scrollToIndex(index);
          }
        }
      } else {
        this.expandKeys = this.mapOfExpanded[this.itemSelectedValue.toString()].filter(e => e !== this.itemSelectedValue);
      }
    }
  }

  scrollToIndex(index: number) {
    setTimeout(() => {
      this.treeSelectComponent.treeRef.cdkVirtualScrollViewport.scrollToIndex(index);
      this.cdr.markForCheck();
    }, 20)
  }

  ngOnDestroy(): void {
    this.subs.forEach(e => e.unsubscribe());
  }

  ngAfterViewInit(): void {
    this.treeSelectComponent.nzSelectSearchComponent.valueChange.subscribe(value => {
      this.treeSelectComponent.isNotFound = true;
      this.cdr.markForCheck();
      this.onInputChange(value);
    });

  }

  onInputChange(value: string): void {
    // This aborts all HTTP requests.
    this.ngUnsubscribe.next();
    // This completes the subject properlly.
    this.ngUnsubscribe.complete();
    if (value && value.length > 1) {
      this.filterTree(value);
    }
  }

  filterTree(filterText: string) {
    const treeOrgTerm = JSON.parse(JSON.stringify(this.treeOrgTerm));
    // use filter input text, return filtered TREE_DATA, use the 'name' object value
    of(this.filterRecursive(filterText, treeOrgTerm, 'title')).pipe(takeUntil(this.ngUnsubscribe)).subscribe(data => {
      this.expandResult(data)
      this.treeOrg = data;
      this.isDefaultExpandAll = true;
      this.treeSelectComponent.isNotFound = false;
      if (data.length === 0) {
        this.treeSelectComponent.isNotFound = true;
      }
      this.cdr.markForCheck();
    });
  }

  expandResult(data: OrganizationNode[]) {
    data.forEach(el => {
      el.expanded = true;
      if(el.children) {
        this.expandResult(el.children);
      }
    })
  }

  filterRecursive(filterText: string, array: OrganizationNode[], property: string) {
    let filteredData;
    //make a copy of the data so we don't mutate the original
    function copy(obj: any) {
      return Object.assign({}, obj);
    }

    // has string
    if (filterText) {
      // need the string to match the property value
      filterText = filterText.toLowerCase();
      // copy obj so we don't mutate it and filter
      filteredData = array.map(copy).filter(function isTextIncludeProperty(item) {
        if (item[property]?.toLowerCase()?.includes(filterText)) {
          return true;
        }
        // if children match
        if (item.children) {
          return (item.children = item.children.map(copy).filter(isTextIncludeProperty)).length;
        }
      });
      // no string, return whole array
    } else {
      filteredData = array;
    }
    return filteredData;
  }
}
