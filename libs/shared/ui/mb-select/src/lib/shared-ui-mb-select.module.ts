import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { SelectComponent } from './select/select.component';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [CommonModule, NzSelectModule, NzIconModule, FormsModule],
  declarations: [SelectComponent],
  exports: [SelectComponent]
})
export class SharedUiMbSelectModule {}
