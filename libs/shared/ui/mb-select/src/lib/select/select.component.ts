import {Component, EventEmitter, forwardRef, Injector, Input, OnChanges, OnInit, Output, ViewChild} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR, NgControl} from '@angular/forms';
import {noop} from 'rxjs';
import {InputBoolean} from "ng-zorro-antd/core/util";
import { NzSelectComponent } from 'ng-zorro-antd/select';
import {SelectModal} from "@hcm-mfe/shared/data-access/models";
import {NzSafeAny} from "ng-zorro-antd/core/types";

@Component({
  selector: 'mb-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => SelectComponent),
    }
  ]
})
export class SelectComponent implements OnInit, ControlValueAccessor, OnChanges {
  @Input() mbDataSelects: NzSafeAny[] = [];
  @Input() mbShowSearch = true;
  @Input() @InputBoolean() mbDisable = false;
  @Input() @InputBoolean() mbServerSearch = false;
  @Input() mbPlaceholder = '';
  @Input() mbDropdownClassName = '';
  @Input() mbSelectIcon: 'search' | 'down' | string;
  @Input() mbOptionHeightPx = 56;
  @Input() mbOptionOverflowSize = 5;
  @Input() mbLabelText: string;
  @Input() mbKeyLabel = 'label';
  @Input() mbKeyValue = 'value';
  @Input() mbDisabled = 'disabled';
  @Input() @InputBoolean() mbKeyValueStringType = false;
  @Input() mbErrorDefs: {errorName: string, errorDescription: string}[];
  @Input() mbShowIconMessage = true;
  @Input() mbShowError = false;
  @Input() mbErrors: NzSafeAny;
  @Input() mbType: 'default' | 'warning' | 'error' | 'success' = 'default';
  @Input() mbIsLoading = false;
  @Input() mbShowClear = true;
  @Input() mbMode: 'multiple' | 'default' = 'default'
  @Output() mbEventEmit: EventEmitter<SelectModal> = new EventEmitter<SelectModal>();
  @Output() mbEventSelect: EventEmitter<SelectModal> = new EventEmitter<SelectModal>();
  @Output() mbSelect: EventEmitter<NzSafeAny> = new EventEmitter<NzSafeAny>();
  @Output() mbOnSearch: EventEmitter<NzSafeAny> = new EventEmitter<NzSafeAny>();
  itemSelectedValue: NzSafeAny = null;

  isOpenOptions = false;
  onTouched: () => void = noop;
  onChange: (_: NzSafeAny) => void = noop;
  ngControl?: NgControl;
  inputMessageClass = 'input__message';
  textMessageValue: NzSafeAny;
  showIcon: NzSafeAny;
  iconType: NzSafeAny;
  inputGroupGroupClass = 'select__group--group';

  classGroupGroup = {
    default: 'select__group--group--default',
    warning: 'select__group--group--warning',
    error: 'select__group--group--error',
    success: 'select__group--group--success',
  };

  classMessage = {
    default: 'select__message--default',
    warning: 'select__message--warning',
    error: 'select__message--error',
    success: 'select__message--success'
  };

  classIcon = {
    warning: 'warning',
    error: 'close-circle',
    success: 'check-circle'
  };
  @ViewChild(NzSelectComponent) nzSelectComponent: NzSelectComponent;
  constructor(
    private inj: Injector,
  ) {
  }

  ngOnInit(): void {
    this.ngControl = this.inj.get(NgControl);
  }

  ngOnChanges() {
    this.configInput();
    this.setErrorMessage();
    if (this.itemSelectedValue) {
      const emit = new SelectModal('NG_MODEL_CHANGE',this.itemSelectedValue);
      emit.itemSelected = this.mbDataSelects ? this.mbDataSelects[this.mbDataSelects.findIndex(item => item[this.mbKeyValue] === this.itemSelectedValue)] : {};
      this.mbEventEmit.emit(emit);
    }
  }

  configInput() {
    this.inputGroupGroupClass = 'select__group--group' + ' ';
    this.inputMessageClass = 'select__message ' + ' ';
    switch (this.mbType) {
      case 'default':
        this.inputGroupGroupClass += this.classGroupGroup.default;
        this.inputMessageClass += this.classMessage.default;
        break;
      case 'warning':
        this.inputGroupGroupClass += this.classGroupGroup.warning;
        this.inputMessageClass += this.classMessage.warning;
        this.iconType = this.classIcon.warning;
        break;
      case 'error':
        this.inputGroupGroupClass += this.classGroupGroup.error;
        this.inputMessageClass += this.classMessage.error;
        this.iconType = this.classIcon.error;
        break;
      case 'success':
        this.inputGroupGroupClass += this.classGroupGroup.success;
        this.inputMessageClass += this.classMessage.success;
        this.iconType = this.classIcon.success;
        break;
      default:
        this.inputGroupGroupClass += this.classGroupGroup.default;
        this.inputMessageClass += this.classMessage.default;
        break;
    }
  }

  setErrorMessage() {
    if (this.mbErrors) {
      for(const error of this.mbErrorDefs) {
        const key = error.errorName;
        if (this.mbErrors[key]) {
          this.textMessageValue = error.errorDescription;
        }
      }
    }
  }

  selectItem($event: NzSafeAny) {
    this.onChange($event);
    const emit = new SelectModal('NG_MODEL_CHANGE',this.itemSelectedValue);
    emit.itemSelected = this.mbDataSelects[this.mbDataSelects?.findIndex(item => item[this.mbKeyValue] === $event)];
    this.mbEventEmit.emit(emit);
    this.mbEventSelect.emit(emit);
    this.mbSelect.emit($event)
  }

  registerOnChange(fn: NzSafeAny): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: NzSafeAny): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.mbDisable = isDisabled;
  }

  writeValue(obj: NzSafeAny): void {
    this.itemSelectedValue = obj;
    this.onChange(this.itemSelectedValue);
  }

  clearValue() {
    this.nzSelectComponent.clearInput()
  }

  onSearch($event: string) {
    this.mbOnSearch.emit($event);
  }
}
