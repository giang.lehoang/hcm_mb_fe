import {
  AfterContentChecked,
  AfterContentInit,
  ChangeDetectionStrategy, ChangeDetectorRef,
  Component,
  ContentChild,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges
} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import {MBTableMergeCellComponentToken, TableHeader} from "@hcm-mfe/shared/data-access/models";
import {StringUtils} from "@hcm-mfe/shared/common/utils";

@Component({
    selector: 'mb-table-merge-cell-wrap',
    templateUrl: './mb-table-merge-cell-wrap.component.html',
    styleUrls: ['./mb-table-merge-cell-wrap.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MbTableMergeCellWrapComponent implements OnInit, AfterContentInit, OnChanges, AfterContentChecked {

    @Input() mbTitle: any;
    @Input() tableConfig: any;
    @Input() nonBg: boolean;
    @Input() extra = true;


  @ContentChild(MBTableMergeCellComponentToken) antTableComponent!: MBTableMergeCellComponentToken;
    currentTableComponent!: MBTableMergeCellComponentToken;

    tableHeaders: TableHeader[] = [];
    tableHeadersSource: TableHeader[] = [];
    copyHeader: TableHeader[] = [];


    curAmountSelectedRows: number;
    columnSearch: string;
    tableConfigVisible = false;

    allTableFieldChecked = false;
    allTableFieldIndeterminate = false;

    constructor(
        private translate: TranslateService,
        private cdRef: ChangeDetectorRef
    ) {
    }

    ngOnInit(): void {
        this.mbTitle = this.mbTitle ?? this.translate.instant('common.label.searchResult');
        this.translate.onLangChange.subscribe(res => this.configTableHeader());
    }

    ngAfterContentChecked() {
        this.curAmountSelectedRows = this.antTableComponent.setOfCheckedId.size;
        this.cdRef.markForCheck();
    }

    ngOnChanges(changes: SimpleChanges) {
      if (changes['tableConfig']) {
        this.tableHeaders = this.tableConfig.headers.filter((item: any) => item.remove !== true);
        this.copyHeader = [];
        this.tableHeaders.forEach(item => {
          this.copyHeader.push({ ...item });
        });
        this.tableHeaders.forEach(item => {
          if (item.show === undefined) {
            item.show = true;
          }
        });

        this.configTableHeader();
        this.judgeAllChecked();
      }
    }

    ngAfterContentInit(): void {
        this.currentTableComponent = this.antTableComponent;
        this.tableHeaders = this.currentTableComponent.tableConfig.headers.filter(item => item.remove !== true);

        this.tableHeaders.forEach(item => {
            if (item.show === undefined) {
                item.show = true;
            }
        });

        this.configTableHeader();
        this.judgeAllChecked();
    }

    traverseToSetTitleTrans(item: TableHeader) {
        item.titleTrans = this.translate.instant(item.title ?? '');
        for (const node of item.child || []) {
            this.traverseToSetTitleTrans(node);
        }
        return null;
    }

    configTableHeader() {
        for (let i = 0; i < this.tableHeaders.length; i++) {
            const header = this.tableHeaders[i];
            this.traverseToSetTitleTrans(header);
        }
        this.copyHeader.length = 0;
        this.tableHeaders.forEach(item => {
            this.copyHeader.push({ ...item });
        });
        this.tableHeadersSource = [...this.tableHeaders];
    }

    changeSignalCheck(e: boolean, item: TableHeader): void {
        if (item?.child && item?.child?.length > 0) {
            this.traverseToHideOrUnHideColumn(item, e);
        } else {
            item.show = e;
        }

        this.judgeAllChecked();
        this.tableChangeDetection();
    }

    traverseToHideOrUnHideColumn(item: TableHeader, isShow: boolean) {
        item.show = isShow;
        for (const node of item.child || []) {
            this.traverseToHideOrUnHideColumn(node, isShow);
        }
        return null;
    }

    tableChangeDetection() {
        this.currentTableComponent.tableChangeDetection();
    }

    judgeAllChecked(): void {
        this.allTableFieldChecked = this.tableHeaders.every(item => item.show === true);
        const allUnChecked = this.tableHeaders.every(item => !item.show);
        this.allTableFieldIndeterminate = !this.allTableFieldChecked && !allUnChecked;
    }

    reset(): void {
        this.columnSearch = undefined;
        this.tableHeaders = [];
        this.copyHeader.forEach(item => {
            item.child?.forEach(child => {
              if (item.show === true || item.show === undefined) {
                child.show = true;
              }
            })
            this.tableHeaders.push({ ...item });
        });

        this.tableHeadersSource = [...this.tableHeaders];
        this.currentTableComponent.tableConfig.headers = [...this.tableHeaders];
        this.tableChangeDetection();
    }

    changeAllTableTableConfigShow(e: boolean): void {
        if (e) {
            this.allTableFieldChecked = e;
            this.allTableFieldIndeterminate = false;
        }
        this.tableHeaders.forEach(item => this.traverseToHideOrUnHideColumn(item, e));
        this.tableHeadersSource = [...this.tableHeaders];
        this.tableChangeDetection();
    }

    searchColumn() {
        if (!StringUtils.isNullOrEmpty(this.columnSearch ?? ''))
            this.tableHeaders = this.tableHeadersSource.filter(item => item?.titleTrans?.toLowerCase().includes(this.columnSearch?.toLowerCase() ?? ''));
        else
            this.tableHeaders = [...this.tableHeadersSource];
    }

}
