import {Component, Input, OnInit} from '@angular/core';
import {saveAs} from 'file-saver';
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";
import { Constant, HTTP_STATUS_CODE } from '@hcm-mfe/shared/common/constants';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import {ImportFormService} from "@hcm-mfe/shared/data-access/services";

@Component({
  selector: 'mb-import-excel',
  templateUrl: './mb-import-excel.component.html',
  styleUrls: ['./mb-import-excel.component.scss']
})
export class MbImportExcelComponent implements OnInit {

  @Input() mbLabelText: string;
  @Input() apiImport: string;
  isModalError: boolean = false;
  errorList: any = [];
  fileList: NzUploadFile[] = [];
  fileName: string;

  constructor(private importFormService: ImportFormService,
              private toastrService: ToastrService,
              private translate: TranslateService) {
  }

  ngOnInit(): void {
  }

  beforeUpload = (file: NzUploadFile): boolean => {
    if (file.size >= 3000000) {
      this.toastrService.error(this.translate.instant('shared.notification.upload.limitSize'));
      return false;
    }
    // if(this.fileList!=undefined && this.fileList.length>0){
    //   this.toastrService.error(this.translate.instant('shared.notification.upload.limitLenght'));
    //   return false;
    // }
    this.fileList = this.fileList.concat(file);
    return false;
  };

  doCloseModal() {
    this.isModalError = false;
  }

  handleImportFile() {
    let formData = new FormData();
    this.fileList.forEach((file: NzUploadFile) => {
      formData.append('file', file.originFileObj);
    });
    this.importFormService.doImport(this.apiImport, formData).subscribe(res => {
      if (res != undefined && res.code != HTTP_STATUS_CODE.OK) {
        this.errorList = res.data.errorList;
        if (this.errorList != undefined && this.errorList.length > 0) {
          this.isModalError = true;
          this.fileName = res.data.errorFile;
          this.toastrService.error(this.translate.instant('shared.notification.upload.error'));
        } else {
          this.isModalError = false;
          this.errorList = [];
          this.fileName = undefined;
          this.toastrService.error(res.message);
        }
      } else {
        this.isModalError = false;
        this.errorList = [];
        this.fileName = undefined;
        this.toastrService.success(this.translate.instant('shared.notification.upload.success'));
      }
    });
  }

  doDownloadFile() {
    this.importFormService.doDownloadFileByName(this.fileName).subscribe(res => {
      const reportFile = new Blob([res], {type: getTypeExport('xlsx')});
      saveAs(reportFile, 'file_error_information.xlsx');
    });
  }
}

export function getTypeExport(format: string): string {
  let type: string;
  switch (format) {
    case Constant.EXCEL_TYPE_1:
      type = Constant.EXCEL_MIME_1;
      break;
    case Constant.EXCEL_TYPE_2:
      type = Constant.EXCEL_MIME_2;
      break;
    case Constant.PDF_TYPE:
      type = Constant.PDF_MIME;
      break;
    default:
      type = null;
      break;
  }
  return type;
}

