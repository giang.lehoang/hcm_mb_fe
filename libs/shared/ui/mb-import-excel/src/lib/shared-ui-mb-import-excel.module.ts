import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MbImportExcelComponent } from './mb-import-excel/mb-import-excel.component';
import { TranslateModule } from '@ngx-translate/core';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzTableModule } from 'ng-zorro-antd/table';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';

@NgModule({
  imports: [CommonModule, TranslateModule, NzUploadModule, NzModalModule, NzTableModule, SharedUiMbButtonModule],
  declarations: [MbImportExcelComponent],
  exports: [MbImportExcelComponent]
})
export class SharedUiMbImportExcelModule {}
