import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MbOrgTreeComponent } from './mb-org-tree/mb-org-tree.component';
import { NzTreeModule } from 'ng-zorro-antd/tree';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NzFormModule} from "ng-zorro-antd/form";

@NgModule({
    imports: [CommonModule, NzTreeModule, NzIconModule, NzEmptyModule, NzSpinModule, FormsModule, ReactiveFormsModule, NzFormModule],
  declarations: [MbOrgTreeComponent],
  exports: [MbOrgTreeComponent]
})
export class SharedUiMbOrgTreeModule {}
