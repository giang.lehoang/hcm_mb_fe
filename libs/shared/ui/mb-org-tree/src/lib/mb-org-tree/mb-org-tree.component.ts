import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {NzFormatEmitEvent} from 'ng-zorro-antd/tree';
import { ModelInput } from '@hcm-mfe/shared/ui/mb-input-text';
import {TreeService} from "@hcm-mfe/shared/data-access/services";

export class ConfigTree {
  asyncData?: boolean = false;
  isCheckbox?: boolean = false;
}

export class OrgInfo {
  nodeId: number;
  code: string;
  name: string;
  parentId: number;
  childrens: OrgInfo[] = [];
}

export class NodeInfo {
  key: string;
  code: string;
  title: string;
  expanded: boolean = false;
  isLoading: boolean = false;
  isLeaf: boolean = false;
  children: NodeInfo[] = [];
}

@Component({
  selector: 'mb-org-tree',
  templateUrl: './mb-org-tree.component.html',
  styleUrls: ['./mb-org-tree.component.scss']
})
export class MbOrgTreeComponent implements OnInit {

  @Input() searchText: string = undefined;
  @Input() mbConfig: ConfigTree = new ConfigTree();
  @Input() scope: string;
  @Input() functionCode: string;
  @Output() onClickNode: EventEmitter<any> = new EventEmitter<any>();
  @Output() onCheckedNode: EventEmitter<any> = new EventEmitter<any>();

  inputConfig: ModelInput = new ModelInput();

  isLoading: boolean = true;
  orgModelTree: NodeInfo[] = [];
  allOrg: OrgInfo[] = [];
  searchValue: string = '';

  constructor(private http: HttpClient, private treeService: TreeService) {
  }

  ngOnInit() {
    this.doGetRootNode({scope:this.scope ? this.scope: '', functionCode: this.functionCode? this.functionCode : ''});
  }

  async doGetNodeChild(event: NzFormatEmitEvent) {
    event.node.isLoading = true;
    if (event.eventName === 'expand') {
      const node = event.node;
      //node.origin.isLoading = true;
      if (node?.getChildren().length === 0 && node?.isExpanded) {
        const node = event.node;
        let nodeChild = await this.searchChildNodeFromAll(node.key);
        if (nodeChild != undefined && nodeChild.length > 0)
          node.addChildren(nodeChild);
        else node.origin.isLeaf = true;
      }
      event.node.isLoading = false;
    } else {
      event.node.isLoading = false;
    }
  }

  doClickNode(data: any) {
    if (data.node != undefined && data.node.origin != undefined) {
      let orgInfo = new OrgInfo();
      orgInfo.nodeId = data.node.origin.key;
      orgInfo.code = data.node.origin.code;
      orgInfo.name = data.node.origin.title;
      this.onClickNode.emit(orgInfo);
    }
  }

  doCheckedNode(data: any) {
    if (data != undefined && data.keys.length > 0) {
      this.onCheckedNode.emit(data.keys);
    }
  }

  doGetRootNode(param: any) {
    this.treeService.doGetRootNode(param).subscribe(res => {
      this.allOrg = res.data;
      this.orgModelTree = this.parseDataToNode(this.allOrg);
      this.isLoading = false;
    }, error => {
      this.isLoading = false;
    });
  }

  async doGetByParentNode(parentId: string): Promise<NodeInfo[]> {
    let childNode = [];
    let data = await this.treeService.doGetByParentNode(parentId).toPromise();
    childNode = this.parseDataToNode(data.data);
    return childNode;
  }

  parseDataToNode(raw: OrgInfo[], expaned?: boolean): NodeInfo[] {
    const listFileOld: NodeInfo[] = [];
    if (raw != undefined && raw.length > 0)
      raw.forEach(item => {
        let node = new NodeInfo();
        node.key = item.nodeId.toString();
        node.title = item.name;
        node.code = item.code;
        node.expanded = expaned != undefined ? expaned : false;
        if (item.childrens != undefined && item.childrens.length > 0) {
          node.children = this.parseDataToNode(item.childrens, expaned);
        } else node.isLeaf = false;
        listFileOld.push(node);
      })
    return listFileOld;
  }

  async searchChildNodeFromAll(key: string): Promise<NodeInfo[]> {
    let childNode: NodeInfo[];
    childNode = await this.doGetByParentNode(key);
    return childNode;
  }

}
