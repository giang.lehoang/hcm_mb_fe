import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MbTimePickerComponent } from './mb-time-picker/mb-time-picker.component';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzTimePickerModule } from 'ng-zorro-antd/time-picker';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
    imports: [CommonModule, NzIconModule, NzTimePickerModule, FormsModule, ReactiveFormsModule],
  declarations: [MbTimePickerComponent],
  exports: [MbTimePickerComponent],
})
export class SharedUiMbTimePickerModule {}
