import { Component, EventEmitter, forwardRef, Injector, Input, OnChanges, OnInit, Output, TemplateRef } from '@angular/core';
import { CompatibleDate, DisabledDateFn, NzDateMode, SupportTimeOptions } from 'ng-zorro-antd/date-picker';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, NgControl } from '@angular/forms';
import { FunctionProp, NzSafeAny } from 'ng-zorro-antd/core/types';
import { noop } from 'rxjs';
import { toBoolean } from 'ng-zorro-antd/core/util';

@Component({
  selector: 'mb-time-picker',
  templateUrl: './mb-time-picker.component.html',
  styleUrls: ['./mb-time-picker.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => MbTimePickerComponent),
    }
  ]
})
export class MbTimePickerComponent implements OnInit, ControlValueAccessor, OnChanges {
  @Input() mbLabelText: string;
  @Input() mbAutofocus: boolean = false;
  @Input() mbDatePickerIcon: 'search' | 'down' | string;
  @Input() mbMode: NzDateMode = 'date';
  @Input() mbPlaceholderText: string = '';
  @Input() required: boolean;
  @Input() mbDisabled: boolean = false;
  @Input() mbFormat: string = 'HH:mm:ss';
  @Input() mbPopupClassName: string = 'mbPopupClassName';

  @Input() mbErrorDefs: { errorName: string, errorDescription: string }[];
  @Input() mbShowIconMessage = true;
  @Input() mbShowError = false;
  @Input() mbErrors: any;
  @Input() mbType: 'default' | 'warning' | 'error' | 'success' = 'default';

  @Output() readonly mbOnOpenChange = new EventEmitter<boolean>();

  value: any;
  onTouched: () => void = noop;
  onChange: (_: any) => void = noop;
  ngControl?: NgControl;
  inputMessageClass = 'input__message';
  textMessageValue: any;
  showIcon: any;
  iconType: any;
  inputGroupGroupClass = 'datepicker__group--group';

  classGroupGroup = {
    default: 'datepicker__group--group--default',
    warning: 'datepicker__group--group--warning',
    error: 'datepicker__group--group--error',
    success: 'datepicker__group--group--success',
  };

  classMessage = {
    default: 'datepicker__message--default',
    warning: 'datepicker__message--warning',
    error: 'datepicker__message--error',
    success: 'datepicker__message--success'
  };

  classIcon = {
    warning: 'warning',
    error: 'close-circle',
    success: 'check-circle'
  };

  constructor(private inj: Injector) { }

  ngOnChanges() {
    this.configInput();
    this.setErrorMessage();
  }

  ngOnInit(): void {
    this.ngControl = this.inj.get(NgControl);
  }

  writeValue(obj: any) {
    this.value = obj;
    this.onChange(this.value);
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.mbDisabled = isDisabled;
  }

  openChange($event) {
    this.mbOnOpenChange.emit($event)
  }

  onInputChange($event: any) {
    this.onChange($event);
  }

  configInput() {
    this.inputGroupGroupClass = 'datepicker__group--group' + ' ';
    this.inputMessageClass = 'datepicker__message ' + ' ';
    switch (this.mbType) {
      case 'default':
        this.inputGroupGroupClass += this.classGroupGroup.default;
        this.inputMessageClass += this.classMessage.default;
        break;
      case 'warning':
        this.inputGroupGroupClass += this.classGroupGroup.warning;
        this.inputMessageClass += this.classMessage.warning;
        this.iconType = this.classIcon.warning;
        break;
      case 'error':
        this.inputGroupGroupClass += this.classGroupGroup.error;
        this.inputMessageClass += this.classMessage.error;
        this.iconType = this.classIcon.error;
        break;
      case 'success':
        this.inputGroupGroupClass += this.classGroupGroup.success;
        this.inputMessageClass += this.classMessage.success;
        this.iconType = this.classIcon.success;
        break;
      default:
        this.inputGroupGroupClass += this.classGroupGroup.default;
        this.inputMessageClass += this.classMessage.default;
        break;
    }
  }

  setErrorMessage() {
    if (this.mbErrors) {
      for (let error of this.mbErrorDefs) {
        let key = error.errorName;
        if (this.mbErrors[key]) {
          this.textMessageValue = error.errorDescription;
        }
      }
    }
  }

}
