import { Component, EventEmitter, Injector, Input, Output } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { InputBoolean } from 'ng-zorro-antd/core/util';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {CustomValidator} from "@hcm-mfe/shared/common/validators";

@Component({
  selector: 'mb-modal-confirm',
  templateUrl: './modal-confirm.component.html',
  providers: [],
})
export class ModalConfirmComponent extends BaseComponent {
  constructor(
    injector : Injector,
  ) {
    super(injector);
    this.formSubmit = this.fb.group(
      {
        content: [null, [Validators.required, Validators.maxLength(250)]],
      },
      {
        validators: [CustomValidator.noWhitespaceValidator('content', 'noAllowSpace')],
      }
    );
  }

  formSubmit: FormGroup;
  isLoadingCorrectionPlan = false;
  isSubmitted = false;
  @Input() @InputBoolean() isVisibleCorrectionDialog: boolean | undefined;
  @Input() modalHeader: string | undefined;
  @Input() titleInputText = '';
  @Output() sendApprovedValid: EventEmitter<string> = new EventEmitter<string>();
  @Input() mbPlaceholderText = '';
  @Input() mbErrorDefs: { errorName: string; errorDescription: string; }[] | undefined;

  cancelDialog(): void {
    this.isVisibleCorrectionDialog = false;
    this.formSubmit.patchValue({
      content: null,
    });
    this.isSubmitted = false;
  }

  _submit() {
    this.isSubmitted = true;
    if(this.formSubmit.valid){
      this.sendApprovedValid.emit(this.formSubmit.value?.content);
    }
  }

}
