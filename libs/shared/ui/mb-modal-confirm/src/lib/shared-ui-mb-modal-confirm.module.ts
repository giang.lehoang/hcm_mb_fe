import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ModalConfirmComponent} from "./mb-modal-confirm/modal-confirm.component";
import {NzModalModule} from "ng-zorro-antd/modal";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {TranslateModule} from "@ngx-translate/core";
import {NzButtonModule} from "ng-zorro-antd/button";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
  imports: [CommonModule, NzModalModule, SharedUiMbInputTextModule, TranslateModule, NzButtonModule, FormsModule, ReactiveFormsModule],
  declarations: [ModalConfirmComponent],
  exports: [ModalConfirmComponent],
})
export class SharedUiMbModalConfirmModule {}
