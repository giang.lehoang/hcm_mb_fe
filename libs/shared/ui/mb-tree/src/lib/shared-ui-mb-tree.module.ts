import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MbTreeComponent} from "./mb-tree/mb-tree.component";
import {TranslateModule} from "@ngx-translate/core";
import {NzInputModule} from "ng-zorro-antd/input";
import {MatTreeModule} from "@angular/material/tree";
import {MatMenuModule} from "@angular/material/menu";
import {MatDividerModule} from "@angular/material/divider";
import {MatIconModule} from "@angular/material/icon";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";
import {NzGridModule} from "ng-zorro-antd/grid";
import {NzTreeModule} from "ng-zorro-antd/tree";
import {NzMenuModule} from "ng-zorro-antd/menu";
import {NzIconModule} from "ng-zorro-antd/icon";

@NgModule({
  imports: [CommonModule, TranslateModule, NzInputModule, MatTreeModule, NzMenuModule, NzIconModule,
    MatMenuModule, MatDividerModule, MatIconModule,
    NzDropDownModule, NzCheckboxModule, NzGridModule, NzTreeModule],
  declarations: [MbTreeComponent],
  exports: [MbTreeComponent]
})
export class SharedUiMbTreeModule {}
