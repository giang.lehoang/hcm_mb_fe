import {
  AfterContentChecked,
  AfterContentInit,
  AfterViewInit,
  Component,
  ElementRef,
  Injector,
  Input,
  OnInit,
  ViewChild
} from '@angular/core';
import { fromEvent } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, tap } from 'rxjs/operators';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {
  OrganizationByName,OrganizationNode
} from "@hcm-mfe/shared/data-access/models";
import {DataService} from "@hcm-mfe/shared/data-access/services";
import {NzFormatEmitEvent, NzTreeComponent, NzTreeNodeOptions} from "ng-zorro-antd/tree";
import {NzTreeNode} from "ng-zorro-antd/core/tree";

@Component({
  selector: 'app-mb-tree',
  templateUrl: './mb-tree.component.html',
  styleUrls: ['./mb-tree.component.scss'],
})
export class MbTreeComponent extends BaseComponent implements OnInit, AfterViewInit {
  @Input() param: any;
  @Input() objectSearch: any;
  @Input() orgName: string;
  @Input() type: string;
  @Input() objFunction: any;
  @Input() scope = '';
  @Input() resourceCode = '';
  @Input() is_author = false;
  @ViewChild('input', { static: true }) input: ElementRef | undefined;
  @ViewChild('tree', { static: true }) tree:NzTreeComponent;
  dataDropdown: OrganizationByName[] = [];
  hideListDropdown: boolean;
  nodeRoot: OrganizationNode;
  treeOrg: OrganizationNode[] = [];

  constructor(injector: Injector,
    readonly dataService: DataService,
  ) {
    super(injector);
  }

  ngAfterViewInit() {
    fromEvent(this.input.nativeElement, 'keyup')
      .pipe(
        filter(Boolean),
        debounceTime(500),
        distinctUntilChanged(),
        tap((event: KeyboardEvent) => {
          if (this.input.nativeElement.value !== '') {
            this.hideListDropdown = false;
              const params = {
                name: this.input.nativeElement.value,
                is_author: this.type === 'PY' || this.is_author ? true : false,
                scope: this.scope,
                resourceCode: this.resourceCode
              }
            this.dataService.getOrganizationTreeByNameL(params).subscribe((data: any) => {
              this.dataDropdown = data.content;
              this.dataDropdown.forEach((item) => {
                const arrPath = item.pathName?.split('-');
                if(arrPath){
                  item.orgName = arrPath.pop() + ' - ' + item.orgName;
                }
              });
            });
          } else {
            this.hideListDropdown = true;
            this.treeOrg = [this.nodeRoot]
          }
        })
      )
      .subscribe();
  }
  ngOnInit(): void {
    this.getAllTree();
  }

  getAllTree(): void {
    this.is_author = this.type === 'PY' || this.is_author;
    this.isLoading = true;
    this.dataService.getSubTree(true, this.scope, this.resourceCode, this.is_author).subscribe(
      (data) => {
        const rootItem = data.data[0];
        this.nodeRoot = rootItem;
        data.data.forEach(org => {
          org.key = '0';
          org.title = org.orgName;
          org.id = org.orgId;
        });
        this.treeOrg = data.data;
        this.isLoading = false;
      },
      (error) => {
        this.isLoading = false;
        this.toastrCustom.error(error.message)
      }
    );
  }

  clickInit(value:NzTreeNode): void {
    const org = value.origin
    org.orgId = null;
    org.name  =  org.orgName;
    org.level = org.treeLevel;
    delete org.children;
    delete org.subs;
    localStorage.setItem('org_selected', JSON.stringify(org));
    if (this.type === "PY") {
      this.router.navigateByUrl('organization/staff-plan/plan-year/init-staff');
    } else {
      this.router.navigateByUrl('organization/init/mangement');
      localStorage.removeItem('viewDetail');
      localStorage.removeItem('viewEdit');
      localStorage.removeItem('viewInit');
    }
  }

  selectedNodeTree(item): void {
    item.key = '0';
    item.title = item.orgName;
    item.id = item.orgId;
    item.expanded = true;
    const children:OrganizationNode[] = [];
    let index = 0;
    this.is_author = this.type === 'PY' || this.is_author;
    this.isLoading = true;
    this.dataService.getSubTree(true, this.scope, this.resourceCode, this.is_author ,item.orgId).subscribe(
      (data) => {
        data.data.forEach((item) => {
          index++;
          item.id = item.orgId;
          item.title = item.orgName;
          item.key = `${item.key}-${index}`;
          children.push(item);
        });
        item.children = children;
        this.treeOrg = [item];
        this.isLoading = false;
      },
      (error) => {
        this.toastrCustom.error(error?.message)
        this.isLoading = false;
      }
    );
  }

  redrirectToAdjusted(value:NzTreeNode) {
    const org = value.origin
    org.name  =  org.orgName;
    org.level = org.treeLevel;
    org.orgId = org.id;
    delete org.children;
    delete org.subs;
    localStorage.setItem('org_selected', JSON.stringify(org));
    if (this.type === "PY") {
      this.router.navigateByUrl(`organization/staff-plan/plan-year/adjusted-staff`);
    } else {
      this.router.navigateByUrl(`/organization/init/correction/DC`);
      localStorage.removeItem('viewDetail');
    }
  }

  redrirectTocorrection(data:NzTreeNode) {
    const org = data.origin
    org.name  =  org.orgName;
    org.level = org.treeLevel;
    org.orgId = org.id;
    delete org.children;
    delete org.subs;
    localStorage.setItem('org_selected', JSON.stringify(org));
    if (this.type === "PY") {
      this.router.navigateByUrl(`organization/staff-plan/plan-year/correction-staff`);
    } else {
      this.router.navigateByUrl(`/organization/init/correction/HC`);
      localStorage.removeItem('viewDetail');
    }
  }
  redirectToDetail(id: number): void {
    localStorage.setItem('selectedId', JSON.stringify(id));
    this.router.navigateByUrl(`/organization/init/info`);
  }

  loadChild(node: NzTreeNode) {

    const children: NzTreeNodeOptions[] = [];
    let index = 0;
    this.is_author = this.type === 'PY' || this.is_author;
    this.dataService.getSubTree(true, this.scope, this.resourceCode, this.is_author ,node.origin.id).subscribe(
      (data) => {
        data.data.forEach((item) => {
          children.push({
            id: item.orgId,
            title: item.orgName,
            key: `${node.key}-${index}`,
            ...item
          });
          index++;
        });
        node.addChildren(children);
      },
      (error) => {
        this.toastrCustom.error(error?.message)
        this.isLoading = false;
        node.addChildren(children);
      }
    );
  }

  nzEvent(event: NzFormatEmitEvent): void {
    // load child async
    if (event.eventName === 'expand') {
      const node = event.node;
      if (node?.getChildren().length === 0 && node?.isExpanded) {
        this.loadChild(node);
      }
    }
  }

}
