import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MbScrollTabsComponent } from './mb-scroll-tabs/mb-scroll-tabs.component';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { TranslateModule } from '@ngx-translate/core';
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";

@NgModule({
    imports: [CommonModule, NzIconModule, NzMenuModule, TranslateModule, SharedUiMbButtonModule],
  declarations: [MbScrollTabsComponent],
  exports: [MbScrollTabsComponent],
})
export class SharedUiMbScrollTabsModule {}
