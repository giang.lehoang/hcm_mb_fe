import { InputBoolean } from 'ng-zorro-antd/core/util';
import { Component, HostBinding, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import {ScrollTabOption} from "@hcm-mfe/shared/data-access/models";
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { TranslateService } from '@ngx-translate/core';
import {InfoChangesModalComponent} from "@hcm-mfe/shared/ui/info-changes-modal";

@Component({
    selector: 'mb-scroll-tabs',
    templateUrl: './mb-scroll-tabs.component.html',
    styleUrls: ['./mb-scroll-tabs.component.scss']
})
export class MbScrollTabsComponent {
    @Input() tabs: ScrollTabOption[] = [];
    @Input() count: number = 0;
    @Input() isShowInfoChange: boolean;

    @ViewChild('footerTmpl') footerTmpl: TemplateRef<{}>;
    @HostBinding('class.scroll__tab--sticky') @Input() @InputBoolean() sticky: boolean = false;

    infoChangeModal: NzModalRef;

    constructor(
        private modalService: NzModalService,
        private translate: TranslateService
    ) {
    }

    scrollTo(tab: ScrollTabOption) {
        let el = document.getElementById(tab.scrollTo);
        el.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
    }

    onShowInfoChangeClick() {
        this.infoChangeModal = this.modalService.create({
            nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
            nzContent: InfoChangesModalComponent,
            nzTitle: this.translate.instant('common.table.infoChange.titleHeader'),
            nzFooter: this.footerTmpl
        });
    }

    cancel() {
        this.infoChangeModal.destroy();
    }

}
