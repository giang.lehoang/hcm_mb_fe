import { Component, Injector, Input, OnInit } from '@angular/core';
import {
  IDataResponse,
  InitScreenSearch,
  ITreeOrganization, MBTableConfig,
  OrganizationNode, ParamTreeMulti
} from '@hcm-mfe/shared/data-access/models';
import { Area, LookupValue, RegionType, ResponseEntity, Type } from '@hcm-mfe/model-organization/data-access/models';
import { DataService } from '@hcm-mfe/shared/data-access/services';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { userConfig } from '@hcm-mfe/shared/common/constants';
import { NzResizeEvent } from 'ng-zorro-antd/resizable';
import { NzTreeNode } from 'ng-zorro-antd/core/tree';
import { NzFormatEmitEvent, NzTreeNodeOptions } from 'ng-zorro-antd/tree';
import { forkJoin } from 'rxjs';
import { maxInt32 } from '@hcm-mfe/shared/common/enums';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { NzSafeAny } from 'ng-zorro-antd/core/types';

@Component({
  selector: 'app-multi-select-unit-popup',
  templateUrl: './multi-select-unit-popup.component.html',
  styleUrls: ['./multi-select-unit-popup.component.scss']
})
export class MultiSelectUnitPopupComponent extends BaseComponent implements OnInit {

  @Input() isAuthor: boolean = false;
  @Input() scope = '';
  @Input() resourceCode = '';
  @Input() isShowChkOrg = false;
  @Input() orgIds:number[] = [];
  isLoading: boolean = false;
  col: number = 6;
  id: number = -1;
  activeNode: string | null = '';
  orgNameModal: string = '';
  orgName: string = '';
  pathResult: string = '';
  pathResult2: string = ''
  checked:NzSafeAny[] | ITreeOrganization[]= [];
  listTree: OrganizationNode[] = [];
  nodeRoot: OrganizationNode;
  treeOrg: OrganizationNode[] = [];
  isSearchAdv= false;
  areas: Area[] = [];
  types: Type[] = [];
  listOrgGroup = [
    {key:"CN",value:"CN"},
    {key:"HO",value:"HO"},
  ];
  listOrgManager:ITreeOrganization[]=[];
  listOrgTT1:ITreeOrganization[]=[];
  listOrgTT2:ITreeOrganization[]=[];
  listOrgTT3:ITreeOrganization[]=[];
  isShowAll = false;
  flagChk = false;

  objectSearch: InitScreenSearch = {
    orgId: '',
    flagStatus: '',
    inputType: '',
    page: 0,
    size: 15,
  };

  paginationModal = {
    size: 0,
    total: 0,
    pageIndex: 0,
    numberOfElements: 0,
  };

  params = {
    id: '',
    scope: '',
    resourceCode: '',
    page: 0,
    size: 10,
    is_author: false,
    listStatus:'',
    orgName:''
  };
  paramSeach:ParamTreeMulti = new ParamTreeMulti();
  selectTab: number = 0;
  heightTable = { x: '40vw', y: '30vh'};
  tableConfig:MBTableConfig;

  listOrgOrigin: ITreeOrganization[] | undefined = [];
  listOrg: ITreeOrganization[] | undefined = [];
  listSelectIds: number[] = [];
  constructor(readonly dataService: DataService, readonly modalRef: NzModalRef, injector:Injector) {
    super(injector);
    this.tableConfig = new MBTableConfig();
  }


  selectedNodeId(id: any, name?: any): void {
    if (this.orgIds && this.orgIds.length > 0) {
      this.listSelectIds  = this.orgIds;
    }
    this.checked = [];
    this.params.id = id ? id : '';
    this.params.scope = this.scope;
    this.params.resourceCode = this.resourceCode;
    this.orgNameModal = name;
    this.params.is_author = this.isAuthor;
    this.params.orgName= name;
    this.objectSearch.orgId = this.params.id;
    this.tableConfig.pageIndex = 1;
    this.getDataTable(this.params);
  }

  chooseOrg(index: any,data?:any): void {
    if (this.listOrg) {
      this.orgName = this.listOrg[index].orgName;
      const orgLevel = this.listOrg[index].treeLevel
      const lastIndex = this.listOrg[index].pathName ? this.listOrg[index].pathName.replace(' ', '').split(' -').length - 1 : '';
      const pathID = this.listOrg[index].pathId;
      this.pathResult2 = this.listOrg[index].pathName;
      if (!lastIndex) {
        this.pathResult = this.listOrg[index].pathName;
      } else {
        this.pathResult =
          this.listOrg[index].pathName.replace(' ', '').split(' -')[lastIndex] +
          '-' + this.listOrg[index].orgName;
      }
      this.objectSearch.orgId = '' + this.listOrg[index].orgId;
      this.activeNode = null;
      this.orgNameModal = '';
      this.modalRef.destroy({ orgName: this.orgName, orgId: this.objectSearch.orgId, pathId: pathID, pathResult: this.pathResult2, orgLevel: orgLevel });
    }
  }

  searchTreeOrg(event?: any): void {
    this.checked = [];
    this.tableConfig.pageIndex = 1;
    const params = {
      orgName: this.orgNameModal ? this.orgNameModal : "",
      page: 0,
      size: userConfig.pageSizePopup,
    };
      this.isLoading = true;
      if(this.flagChk != this.isShowAll && this.isShowChkOrg){
        this.getAllTree();
      }
      this.getDataTable(params);

  }

  ngOnInit(): void {
    this.initTable();
    this.fetchData();
    this.getAllTree();
  }

  getAllTree(): void {
    this.dataService.getSubTree(true, this.scope, this.resourceCode, this.isAuthor,'').subscribe(
      (data) => {
        if(data?.data?.length){
          const rootItem = data.data[0];
          this.params.id = String(rootItem.orgId);
          this.selectedNodeId(String(rootItem.orgId), rootItem.orgName);
          this.nodeRoot = rootItem;
          data.data.forEach(org => {
            org.key = '0';
            org.title = org.orgName;
            org.id = org.orgId;
          });
          this.treeOrg = data.data;
        }
        this.flagChk = this.isShowAll
      },
      (error) => {
        this.toastrCustom.error(error.message)
      }
    );
  }


  addKey(data: OrganizationNode[] | undefined, keyParent?: any) {
    if (!data) {
      return
    }
    for (let i = 0; i < data.length; i++) {
      data[i].title = data[i].orgName;
      data[i].id = data[i].orgId;
      data[i].expanded = false;
      data[i].key = keyParent + '-' + i;
      this.addKey(data[i].subs, data[i].key)
      data[i].children = data[i].subs;
    }
  }
  changePage(value: any): void {
    const params = {
      orgName: this.orgNameModal ? this.orgNameModal : "",
      page: value - 1,
      size: userConfig.pageSizePopup,
    };
    this.getDataTable(params);
  }

  onResize({ col }: NzResizeEvent): void {
    cancelAnimationFrame(this.id);
    this.id = requestAnimationFrame(() => {
      this.col = col!;
    });
  }

  setDataPagination(data: any) {
    this.paginationModal.size = data.size;
    this.paginationModal.total = data.totalElements;
    this.paginationModal.pageIndex = data.pageable.pageNumber;
    this.paginationModal.numberOfElements = data.numberOfElements;
  }
  applyChecked() {
    const params = {
      orgIds: this.listSelectIds,
      isAuthor:this.isAuthor,
      scope:this.scope,
      resourceCode:this.resourceCode,
      isMulti: true,
      isSelected:true
    }
    this.modalRef.destroy(params);
  }

  loadChild(node: NzTreeNode) {
    const children: NzTreeNodeOptions[] = [];
    let index = 0;
    this.dataService.getSubTree(true, this.scope, this.resourceCode, this.isAuthor ,node.origin['id']).subscribe(
      (data) => {
        data?.data?.forEach((item) => {
          const org = {
            id: item.orgId,
            ...item
          };
          org.title = item.orgName;
          org.key = `${node.key}-${index}`;
          children.push(org);
          index++;
        });
        node.addChildren(children);
      },
      (error) => {
        this.toastrCustom.error(error?.message)
        this.isLoading = false;
        node.addChildren(children);
      }
    );
  }

  nzEvent(event: NzFormatEmitEvent): void {
    // load child async
    if (event.eventName === 'click' && event.node) {
      this.selectedNodeId(event.node.origin['id'], event.node.origin.title);
    }

    if (event.eventName === 'expand') {
      const node = event.node;
      if (node?.getChildren().length === 0 && node?.isExpanded) {
        this.loadChild(node);
      }
    }
  }
  showAdvSearch(){
    this.isSearchAdv = !this.isSearchAdv;
    if(this.isSearchAdv) {
      this.objectSearch.treeLevel = 2;
      this.getOrgByLevel();
      this.heightTable = { x: '40vw', y: '25vh' };
    } else {
      this.heightTable = { x: '40vw', y: '30vh' };
    }
  }
  fetchData() {
    forkJoin([
      this.dataService.getRegionCategoryList(RegionType.KV, 1),
      this.dataService.getRegionCategoryList(RegionType.LH, 1),
    ]).subscribe(
      ([areas, types]: [
        ResponseEntity<LookupValue[]>,
        ResponseEntity<LookupValue[]>,
      ]) => {
        this.areas = areas.data;
        this.types = types.data;
      },
      (error) => {
        this.toastrCustom.error(error.error?.message);
      }
    );
  }
  getOrgByLevel() {
    this.isLoading = true;
    const params = {
      isAuthor: this.isAuthor,
      scope: this.scope,
      resourceCode: this.resourceCode,
      orgId: this.objectSearch.orgId,
      treeLevel: this.objectSearch.treeLevel,
      page: 0,
      size: maxInt32
    }
    this.dataService.getOrgByLevel(params).subscribe((data) => {
      if(!this.objectSearch.orgId){
        this.listOrgManager = data.data.listOrgManager;
      } else {
        this.listOrgTT1 = data.data.listTT1;
        this.listOrgTT2 = data.data.listTT2;
        this.listOrgTT3 = data.data.listTT3;
      }
      this.objectSearch.orgIdTT1 = '';
      this.objectSearch.orgIdTT2 = '';
      this.objectSearch.orgIdTT3 = '';
      this.isLoading = false;
    }, (error) => {
      this.toastrCustom.error(error.error?.message);
      this.isLoading = false;
    });
  }
  onChangeManager(event:any){
    if(event){
      this.objectSearch.orgId = event;
      this.objectSearch.treeLevel = '';
      this.getOrgByLevel();
    } else{
      this.objectSearch.orgManager = '';
      this.objectSearch.orgIdTT1 = '';
      this.objectSearch.orgIdTT2 = '';
      this.objectSearch.orgIdTT3 = '';
    }
  }
  checkOrg(){
    if(!this.objectSearch.orgIdTT1){
      this.objectSearch.orgIdTT2 = '';
      this.objectSearch.orgIdTT3 = '';
    }else if(!this.objectSearch.orgIdTT2){
      this.objectSearch.orgIdTT3 = '';
    }
  }
  getDataTable(params:any) {
    this.isLoading = true;
    const request = this.checkRequest(params);
    if(!request.orgIds){
      delete request.orgIds
    }
    if(!request.listStatus){
      delete request.listStatus
    }
    this.dataService.getOrgPopupMulti(request).subscribe((data: IDataResponse<ITreeOrganization[]>) => {
      this.listOrg = JSON.parse(JSON.stringify(data.content));
      this.listOrgOrigin = JSON.parse(JSON.stringify(data.content));
      this.setDataPagination(data);
      this.tableConfig.total = data.totalElements;
      if(this.selectTab === 1){
        this.checked = data.content;
      } else {
        this.checked = [];
      }
      this.isLoading = false;
    }, (error) => {
      this.toastrCustom.error(error.error?.message);
      this.isLoading = false;
    });
  }
  checkRequest(params:any){
    if(this.objectSearch.orgIdTT2){
      this.objectSearch.orgId = this.objectSearch.orgIdTT2;
    } else if(!this.objectSearch.orgIdTT2 && this.objectSearch.orgIdTT1){
      this.objectSearch.orgId = this.objectSearch.orgIdTT1;
    } else if(this.objectSearch.orgManager && !this.objectSearch.orgIdTT1){
      this.objectSearch.orgId = this.objectSearch.orgManager;
    }else {
      this.objectSearch.orgId = '';
    }
    this.paramSeach.orgId = this.objectSearch.orgId;
    this.paramSeach.name = params.orgName;
    this.paramSeach.orgGroup = this.objectSearch.orgGroup?this.objectSearch.orgGroup:'';
    this.paramSeach.region=this.objectSearch.region?this.objectSearch.region:'';
    this.paramSeach.brachType=this.objectSearch.brachType?this.objectSearch.brachType:'';
    this.paramSeach.isAuthor=this.isAuthor;
    this.paramSeach.scope=this.scope;
    this.paramSeach.resourceCode=this.resourceCode;
    this.paramSeach.isMulti=true;
    this.paramSeach.orgIds= this.listSelectIds;
    this.paramSeach.isSelected= this.selectTab === 1?true:false;
    this.paramSeach.page= params.page;
    this.paramSeach.size= params.size;
    return this.paramSeach;
  }
  changeTab(event:any){
    this.selectTab = event;
    if(this.selectTab === 1 && this.listSelectIds.length === 0 ){
      this.listOrg = [];
      return;
    }
    this.searchTreeOrg();
  }
  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'common.label.org',
          field: 'orgName',
          width: 300,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left',
          show: true
        },
        {
          title: 'common.label.orgManage',
          field: 'pathName',
          width: 400,
          show: true
        },
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: 10,
      pageIndex: 1,
      showCheckbox: true,
      showFrontPagination: false
    };
  }
  changeSelected(event:any){
    if(this.selectTab === 0){
      this.listSelectIds = [...new Set([...this.listSelectIds,...event.map(item => item.orgId)])];
    } else {
      const listChk = event.map(value=> value.orgId);
      const originIds = this.listOrgOrigin?.map(item => item.orgId);
      const differences = originIds?.filter(x => listChk.indexOf(x) === -1);
      differences?.forEach(value => {
        const index =  this.listSelectIds.findIndex(item => item === value);
        this.listSelectIds.splice(index,1);
      })
    }
    this.searchTreeOrg();

  }
}
