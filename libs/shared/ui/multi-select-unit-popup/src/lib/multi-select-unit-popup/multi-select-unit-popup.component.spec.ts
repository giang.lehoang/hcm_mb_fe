import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiSelectUnitPopupComponent } from './multi-select-unit-popup.component';

describe('MultiSelectUnitPopupComponent', () => {
  let component: MultiSelectUnitPopupComponent;
  let fixture: ComponentFixture<MultiSelectUnitPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MultiSelectUnitPopupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiSelectUnitPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
