import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NzResizableModule } from 'ng-zorro-antd/resizable';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzTreeModule } from 'ng-zorro-antd/tree';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { TranslateModule } from '@ngx-translate/core';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { MultiSelectUnitPopupComponent } from './multi-select-unit-popup/multi-select-unit-popup.component';
import { SharedUiMbSelectCheckAbleModule } from '@hcm-mfe/shared/ui/mb-select-check-able';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { SharedUiMbTableWrapModule } from '@hcm-mfe/shared/ui/mb-table-wrap';
import { SharedUiMbTableModule } from '@hcm-mfe/shared/ui/mb-table';

@NgModule({
  imports: [CommonModule, NzResizableModule, FormsModule, ReactiveFormsModule,
    NzGridModule, NzTreeModule, SharedUiLoadingModule, SharedUiMbInputTextModule, SharedUiMbButtonModule, TranslateModule,
    NzTableModule, NzToolTipModule, NzPaginationModule, SharedUiMbSelectModule, NzCheckboxModule, SharedUiMbSelectCheckAbleModule, NzTabsModule, SharedUiMbTableWrapModule, SharedUiMbTableModule],
  exports: [MultiSelectUnitPopupComponent],
  declarations: [MultiSelectUnitPopupComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class SharedUiMultiSelectUnitPopupModule {}
