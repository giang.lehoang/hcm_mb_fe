import {Component, OnInit, ViewChild} from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {RecaptchaComponent, RecaptchaErrorParameters} from 'ng-recaptcha';
import { StorageService } from '@hcm-mfe/shared/common/store';
import { STORAGE_NAME } from '@hcm-mfe/shared/common/constants';
import { AuthService } from '@hcm-mfe/shared/data-access/services';
import { environment } from '@hcm-mfe/shared/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  passwordVisible = false;
  isLoading = false;
  inValidCaptcha = false;
  inValidLogin = false;
  captchaResponse: any;
  numberLoginFailed = StorageService.get(STORAGE_NAME.NUMBER_LOGIN_FAILED) ? 0 : StorageService.get(STORAGE_NAME.NUMBER_LOGIN_FAILED);
  validateForm: FormGroup = this.fb.group([]);
  mobileWidth = 993;
  isMobile: any;
  messageError?: string;
  @ViewChild('captchaElem') captchaElem!: RecaptchaComponent;
  constructor(
    private router: Router,
    private authService: AuthService,
    private toastrService: ToastrService,
    private translate: TranslateService,
    private fb: FormBuilder,
    private http: HttpClient
  ) {
    if (window.innerWidth < this.mobileWidth) {
      this.isMobile = true;
    }
  }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      username: [null, [Validators.required]],
      password: [null, [Validators.required]],
      remember: [true],
      recaptcha:['']
    });
    this.checkShowRecaptcha();
  }

  submitForm(): void {
    if (this.validateForm.valid) {
      this.login();
    } else {
      Object.values(this.validateForm.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }
  }

  public resolved(captchaResponse: string): void {
    this.captchaResponse = captchaResponse;
  }

  public onError(errorDetails: RecaptchaErrorParameters): void {
    console.log(`reCAPTCHA error encountered; details:`, errorDetails);
  }

  async login() {
    const response: HttpResponse<any> | undefined = await this.http.get(environment.url.urlCheckEVN, {observe: 'response'}).toPromise();
    if (response) {
      if (response.headers.get('appId') === environment.app.HCM_WEB) {
        location.reload();
      }
      StorageService.set(STORAGE_NAME.APP, response.headers.get('appId'));
    }
    this.isLoading = true;
    this.inValidLogin = false;
    this.messageError = '';
    const params = new HttpParams({
      fromObject: {
        clientId: environment.keycloak.client,
        username: this.validateForm.get('username')?.value,
        password: this.validateForm.get('password')?.value,
        captchaResponse: this.captchaResponse ? this.captchaResponse : ''
      }
    });
    this.authService.login(params).subscribe(res => {
      if (res && res.code == 200) {
        this.authService.saveStorage(res);
        this.authService.saveSessionHistory();
        this.router.navigate(['/']);
      } else {
        this.messageError = res?.message;
        this.toastrService.error(res?.message);
        this.numberLoginFailed += 1;
        this.checkShowRecaptcha();
      }
    }, error =>  {
      this.messageError = error?.message;
      this.toastrService.error(error?.message);
      this.numberLoginFailed += 1;
      this.checkShowRecaptcha();
    });
  }

  checkShowRecaptcha() {
    this.inValidLogin = true;
    this.inValidCaptcha = false;
    this.isLoading = false;
    StorageService.set(STORAGE_NAME.NUMBER_LOGIN_FAILED, this.numberLoginFailed);
    if (this.numberLoginFailed > 4) {
      this.validateForm.controls['recaptcha'].addValidators(Validators.required);
      this.captchaElem?.reset();
    }
  }
}
