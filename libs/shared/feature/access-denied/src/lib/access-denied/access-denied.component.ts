import { Component, OnInit, HostBinding } from '@angular/core';
import { UserService } from '@hcm-mfe/shared/core';

@Component({
  selector: 'app-access-denied',
  templateUrl: './access-denied.component.html',
  styleUrls: ['./access-denied.component.scss'],
})
export class AccessDeniedComponent implements OnInit {
  @HostBinding('class.app__right-content') appRightContent = true;
  constructor(private userService: UserService) {}

  ngOnInit(): void {}

  logout() {
    this.userService.logout();
  }
}
