import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccessDeniedComponent } from './access-denied/access-denied.component';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [CommonModule, TranslateModule,
    RouterModule.forChild([
      {
        path: '',
        component: AccessDeniedComponent
      }
    ])
  ],
  declarations: [AccessDeniedComponent],
  exports: [AccessDeniedComponent],
})
export class SharedFeatureAccessDeniedModule {}
