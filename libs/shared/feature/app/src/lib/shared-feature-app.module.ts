import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppBaseComponent } from './components/app.component';
import { TranslateService } from '@ngx-translate/core';
import {NzModalModule} from "ng-zorro-antd/modal";

@NgModule({
  imports: [CommonModule, NzModalModule],
  declarations: [AppBaseComponent],
  exports: [AppBaseComponent],
  providers: [TranslateService]
})
export class SharedFeatureAppModule {}
