import { Component, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { KeycloakService } from 'keycloak-angular';
import { StorageService } from "@hcm-mfe/shared/common/store";
import { STORAGE_NAME } from '@hcm-mfe/shared/common/constants';
import { UserLogin } from '@hcm-mfe/shared/data-access/models';
import { IdleUserCheckService, UserService, WebSocketService } from '@hcm-mfe/shared/core';
import { environment } from '@hcm-mfe/shared/environment';
import { fromEvent, map, merge, of, Subscription } from 'rxjs';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { addWarning } from '@angular-devkit/build-angular/src/utils/webpack-diagnostics';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppBaseComponent {

  networkStatus$: Subscription = Subscription.EMPTY;

  constructor(private readonly translate: TranslateService,
              private readonly keycloakService: KeycloakService,
              private readonly router: Router,
              private readonly idleUserCheckService: IdleUserCheckService,
              private readonly webSocketService: WebSocketService,
              private readonly http: HttpClient,
              private readonly userService: UserService) {
     // translate.use(translate.store.currentLang ?? 'vn');
  }

  ngOnInit(): void {
    this.initTimer();
    this.keycloakService.isLoggedIn().then(loggedIn => {
      if (loggedIn) {
        this.keycloakService.loadUserProfile()
          .then(profile => {
            const userLogin = new UserLogin();
            userLogin.id = profile.id;
            userLogin.username = profile.username;
            userLogin.firstName = profile.firstName;
            StorageService.set(STORAGE_NAME.USER_LOGIN, userLogin);
          });
      } else if (StorageService.get(STORAGE_NAME.APP) === environment.app.HCM_WEB) {
        this.userService.logout();
      }
    });
    this.checkNetworkStatus();
  }

  @HostListener('window:unload', [ '$event' ])
  unloadHandler(event) {
    this.webSocketService.disconnect();
  }

  @HostListener('window:beforeunload', [ '$event' ])
  beforeUnloadHandler(event) {
    this.webSocketService.disconnect();
  }

  checkNetworkStatus() {
    this.networkStatus$ = merge(
      of(null),
      fromEvent(window, 'online'),
      fromEvent(window, 'offline')
    )
      .pipe(map(() => navigator.onLine))
      .subscribe(async (status) => {
        if (status) {
          const response: HttpResponse<any> = await this.http.get(environment.url.urlCheckEVN, {observe: 'response'}).toPromise();
          const appCode = response.headers.get('appId');
          if (StorageService.get(STORAGE_NAME.APP) !== appCode) {
            StorageService.set(STORAGE_NAME.APP, appCode);
            this.userService.logout();
          }
        }
      });
  }

  initTimer() {
    // thời gian rảnh: 60 phút
    this.idleUserCheckService.USER_IDLE_TIMER_VALUE_IN_MIN = 30;
    this.idleUserCheckService.initilizeSessionTimeout();
    this.idleUserCheckService.userIdlenessChecker.subscribe((status: string) => {
      this.initiateFirstTimer(status);
    });
  }
  initiateFirstTimer = (status: string) => {
    switch (status) {
      case 'INITIATE_TIMER':
        break;
      case 'RESET_TIMER':
        break;
      case 'STOPPED_TIMER':
        IdleUserCheckService.runTimer = false;
        StorageService.clear();
        this.userService.logout();
        break;
      default:
        break;
    }
  }
}
