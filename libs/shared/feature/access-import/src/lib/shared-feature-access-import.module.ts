import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccessImportComponent } from './access-import/access-import.component';
import { TranslateModule } from '@ngx-translate/core';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { NzFormModule } from 'ng-zorro-antd/form';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [CommonModule, TranslateModule, NzUploadModule, SharedUiMbButtonModule, NzFormModule,
    RouterModule.forChild([
      {
        path: '',
        component: AccessImportComponent
      }
    ])
  ],
  declarations: [AccessImportComponent],
  exports: [AccessImportComponent],
})
export class SharedFeatureAccessImportModule {}
