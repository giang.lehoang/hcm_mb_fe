import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { ImportFormService } from '@hcm-mfe/shared/data-access/services';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { UserService } from '@hcm-mfe/shared/core';
import { HTTP_STATUS_CODE } from '@hcm-mfe/shared/common/constants';

@Component({
  selector: 'app-access-import',
  templateUrl: './access-import.component.html',
  styleUrls: ['./access-import.component.scss'],
})
export class AccessImportComponent implements OnInit {
  readonly urlApiImport = 'keycloak/import-data-init';

  fileList: NzUploadFile[] = [];
  fileName?: string;
  fileImportName: string = '';
  fileImportSize?: string;

  resources = 0;
  policies = 0;
  scopes = 0;
  clientRoles = 0;
  static readonly TYPE_JSON = 'application/json';

  isExistFileImport: boolean = false;
  constructor(
    private readonly importFormService: ImportFormService,
    private readonly toastrService: ToastrService,
    private readonly userService: UserService,
    public translate: TranslateService
  ) {}

  ngOnInit(): void {}

  beforeUpload = (file: NzUploadFile): boolean => {
    if (file.type !== AccessImportComponent.TYPE_JSON) {
      this.toastrService.error(this.translate.instant('common.upload.fileError'));
      return false;
    }
    this.fileList = [];
    this.fileList = this.fileList.concat(file);
    this.isExistFileImport = true;
    this.fileImportName = file.name;
    this.fileImportSize = (file.size ?? 0 / 1048576).toFixed(2);
    const currentFile: File = file as any;
    currentFile.text().then(res => {
      const dataJson = JSON.parse(res);
      this.resources = dataJson.resources.length;
      this.policies = dataJson.policies.length;
      this.scopes = dataJson.scopes.length;
      this.clientRoles = dataJson.clientRoles.length;
    });
    return false;
  };

  doImportFile() {
    const formData = new FormData();
    this.fileList.forEach((file: NzUploadFile) => {
      formData.append('file', file as any);
    });
    this.importFormService.doImport(this.urlApiImport, formData).subscribe(
      (res) => {
        if (res != null && res.code !== HTTP_STATUS_CODE.OK) {
          this.toastrService.success(this.translate.instant('shared.notification.upload.success'));
        }
      },
      (error) => {
        this.toastrService.error(error.message);
      }
    );
  }

  doRemoveFile = (): boolean => {
    this.fileList = [];
    this.isExistFileImport = false;
    return true;
  };

  logout() {
    this.userService.logout();
  }
}
