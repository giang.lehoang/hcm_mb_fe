import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessImportComponent } from './access-import.component';

describe('AccessImportComponent', () => {
  let component: AccessImportComponent;
  let fixture: ComponentFixture<AccessImportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccessImportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccessImportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
