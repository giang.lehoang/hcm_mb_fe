import { Validator, NG_VALIDATORS, FormControl } from '@angular/forms'
import { Directive } from '@angular/core';


@Directive({
    selector: '[noAllowSpace]',
    providers: [
        { provide: NG_VALIDATORS, useExisting: NoAllowSpaceValidator, multi: true }
    ]
})
export class NoAllowSpaceValidator implements Validator {
    validate(c: FormControl) {
        const value = c.value;
        if (!value || !value.trim()) {
            return { noAllowSpace: true };
        }
        return null;
    }
}
