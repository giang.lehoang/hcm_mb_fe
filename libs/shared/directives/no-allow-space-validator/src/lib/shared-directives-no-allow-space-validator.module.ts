import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NoAllowSpaceValidator } from './noAllowSpaceValidator';

@NgModule({
  imports: [CommonModule],
  declarations: [NoAllowSpaceValidator],
  exports: [NoAllowSpaceValidator],
})
export class SharedDirectivesNoAllowSpaceValidatorModule {}
