import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpecialApiInputDirective } from './special-api-input.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [SpecialApiInputDirective],
  exports: [SpecialApiInputDirective],
})
export class SharedDirectivesSpecialApiInputModule {}
