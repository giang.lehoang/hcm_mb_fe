import { Directive, ElementRef, Input } from "@angular/core";

@Directive({
  selector: "[autoFocus]"
})
export class AutofocusDirective
{
  private focus = true;

  constructor(private el: ElementRef)
  {
  }

  ngOnInit()
  {
    if (this.focus)
    {
      window.setTimeout(() =>
      {
        this.el.nativeElement.focus();
      },500);
    }
  }

  @Input() set autoFocus(condition: boolean)
  {
    this.focus = condition !== false;
  }
}
