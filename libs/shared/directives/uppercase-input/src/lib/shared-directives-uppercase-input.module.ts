import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UppercaseInputDirective } from './uppercase-input.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [UppercaseInputDirective],
  exports: [UppercaseInputDirective],
})
export class SharedDirectivesUppercaseInputModule {}
