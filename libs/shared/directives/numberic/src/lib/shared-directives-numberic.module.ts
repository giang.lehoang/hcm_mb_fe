import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NumbericDirective } from './numberic.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [NumbericDirective],
  exports: [NumbericDirective],
})
export class SharedDirectivesNumbericModule {}
