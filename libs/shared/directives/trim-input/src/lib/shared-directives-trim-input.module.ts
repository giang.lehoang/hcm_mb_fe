import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TrimDirective } from './trim-input.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [TrimDirective],
  exports: [TrimDirective]
})
export class SharedDirectivesTrimInputModule {}
