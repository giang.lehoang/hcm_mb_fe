import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VndFormatDirective } from './vnd-format.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [VndFormatDirective],
  exports: [VndFormatDirective],
})
export class SharedDirectivesVndFormatModule {}
