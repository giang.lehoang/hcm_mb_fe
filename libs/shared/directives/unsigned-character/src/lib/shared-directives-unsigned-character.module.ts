import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UnsignedCharacterDirective } from './unsigned-character.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [UnsignedCharacterDirective],
  exports: [UnsignedCharacterDirective],
})
export class SharedDirectivesUnsignedCharacterModule {}
