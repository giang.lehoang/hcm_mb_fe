import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaskRangeDateDirective } from './mask-range-date.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [MaskRangeDateDirective],
  exports: [MaskRangeDateDirective],
})
export class SharedDirectivesMaskRangeDateModule {}
