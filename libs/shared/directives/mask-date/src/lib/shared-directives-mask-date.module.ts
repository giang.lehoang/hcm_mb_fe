import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaskDateDirective } from './mask-date.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [MaskDateDirective],
  exports: [MaskDateDirective],
})
export class SharedDirectivesMaskDateModule {}
