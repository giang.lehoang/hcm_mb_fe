import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputTrimDirective } from './input-trim.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [InputTrimDirective],
  exports: [InputTrimDirective],
})
export class SharedDirectivesInputTrimModule {}
