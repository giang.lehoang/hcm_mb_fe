import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalDraggableDirective } from './modal-drag.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [ModalDraggableDirective],
  exports: [ModalDraggableDirective],
})
export class SharedDirectivesModalDragModule {}
