import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpecialInputDirective } from './special-input.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [SpecialInputDirective],
  exports: [SpecialInputDirective],
})
export class SharedDirectivesSpecialInputModule {}
