import {AfterViewInit, Directive, ElementRef, HostListener, Inject, Input, Renderer2} from '@angular/core';
import {DOCUMENT} from '@angular/common';
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {ActivatedRoute} from "@angular/router";

@Directive({
  selector: '[scroll-spy]'
})
export class ScrollSpyDirective implements AfterViewInit {

  private elements = [];
  private currentActiveLink;
  private directNavigation = false;

  @Input() option: NzSafeAny;

  constructor(
    @Inject(DOCUMENT) private document: NzSafeAny,
    private activatedRoute: ActivatedRoute,
    private el: ElementRef,
    private renderer: Renderer2
  ) {}

  ngAfterViewInit(): void {
    this.collectIds();
  }

  private collectIds() {
    this.elements = [];
    const elements = this.el.nativeElement.querySelectorAll('li');
    for (let i = 0; i < elements.length; i++) {
      const elem = elements.item(i);

      const id = ScrollSpyDirective.getId(elem);
      if (!id)
        continue;

      const destination = this._getPeerElement(id);

      if (!destination)
        continue;

      this.elements.push({
        id,
        link: elem,
        destination
      })
    }
  }

  private _getPeerElement(id) {

    const destination = this.document.getElementById(id);

    if (!destination)
      return null;

    return destination;
  }

  private static getId(elem) {
    const id = elem.id;
    if (!id)
      return null;

    return id.replace('#', '');
  }

  @HostListener("scroll", ['$event'])
  onScroll(event: Event & { target: HTMLInputElement }) {
    let divider;
    let pageName;
    if (!this.option.isSelfService) {
      divider = this.el.nativeElement.querySelectorAll('.staffManager__staffInfo--divider');
      pageName = this.document.getElementById('staffManager__pageName.personalInfo');
    } else {
      divider = this.el.nativeElement.querySelectorAll('.selfService__selfInfo--divider');
      pageName = this.document.getElementById('selfService__pageName.personalInfo');
    }
    if (divider[0].getBoundingClientRect().top <= 185) {
      pageName.innerText = this.option.empCode + '-' + this.option.empName;
    } else {
      pageName.innerText = this.option.pageName;
    }

    if (this.directNavigation)
      return;
    for (let i = 0; i < this.elements.length; i++) {
      const top = this.elements[i].destination.offsetTop - 100;
      const bottom = i !== (this.elements.length - 1) ? this.elements[i + 1].destination.offsetTop : (top + this.elements[i].destination.offsetHeight)
      if (this.el.nativeElement.scrollTop >= top && this.el.nativeElement.scrollTop <= bottom) {
        this._cleanCurrentLink();
        this._setCurrentLink(this.elements[i].link);
      } else {
        this.renderer.removeClass(this.elements[i].link, 'ant-menu-item-selected');
      }
    }
  }

  private _cleanCurrentLink() {
    if (!this.currentActiveLink)
      return;

    this.renderer.removeClass(this.currentActiveLink, 'ant-menu-item-selected');
  }

  private _setCurrentLink(elem) {
    this.currentActiveLink = elem;
    this.renderer.addClass(this.currentActiveLink, 'ant-menu-item-selected');
  }

}
