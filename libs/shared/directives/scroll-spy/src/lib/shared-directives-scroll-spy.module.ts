import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScrollSpyDirective } from './scroll-spy.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [ScrollSpyDirective],
  exports: [ScrollSpyDirective],
})
export class SharedDirectivesScrollSpyModule {}
