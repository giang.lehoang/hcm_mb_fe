import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlphabeticInputDirective } from './alphabetic-input.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [AlphabeticInputDirective],
  exports: [AlphabeticInputDirective],
})
export class SharedDirectivesAlphabeticInputModule {}
