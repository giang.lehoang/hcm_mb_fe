import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SwitchCasesDirective } from './switch-case.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [SwitchCasesDirective],
  exports: [SwitchCasesDirective],
})
export class SharedDirectivesSwitchCaseModule {}
