import { Directive, ElementRef, Input, OnInit } from "@angular/core";

@Directive({
    selector:'[paddingOrg]'
})
export class PaddingDirective implements OnInit {
    @Input() padding: any

    constructor(readonly rel:ElementRef){
    }

    ngOnInit(): void {
        this.rel.nativeElement.style['padding-left'] = this.padding + '%'
    }
}
