import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaddingDirective } from './directive/padding-directive';

@NgModule({
  imports: [CommonModule],
  declarations: [PaddingDirective],
  exports: [PaddingDirective],
})
export class SharedDirectivesPaddingDirectivesModule {}
