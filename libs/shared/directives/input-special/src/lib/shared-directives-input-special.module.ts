import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputSpecialDirective } from './input-special.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [InputSpecialDirective],
  exports: [InputSpecialDirective],
})
export class SharedDirectivesInputSpecialModule {}
