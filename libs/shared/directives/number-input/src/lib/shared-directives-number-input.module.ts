import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NumberInputDirective } from './number-input.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [NumberInputDirective],
  exports: [NumberInputDirective],
})
export class SharedDirectivesNumberInputModule {}
