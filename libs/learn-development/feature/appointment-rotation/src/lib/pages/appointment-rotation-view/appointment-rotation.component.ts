import { Component, Injector, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Constants, maxInt32, userConfig } from "@hcm-mfe/learn-development/data-access/common";
import {
  DataCommon, DataModelIssuedDecision, ListTitle, ProposeReason, TableAppointmentRotation
} from "@hcm-mfe/learn-development/data-access/models";
import { AppointmentRotationService, ProposedUnitService, ProposeMultipleEmployeeService } from "@hcm-mfe/learn-development/data-access/services";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import { cleanData } from "@hcm-mfe/shared/common/utils";
import { BaseResponse, MBTableConfig, Pagination } from '@hcm-mfe/shared/data-access/models';
import * as moment from 'moment';
import { NzSafeAny } from "ng-zorro-antd/core/types";
import { NzModalRef } from "ng-zorro-antd/modal";
import { catchError, forkJoin, Observable, of, Subscription } from "rxjs";
import { ReasonForExtensionComponent } from '../reason-for-extension/reason-for-extension.component';
import * as FileSaver from 'file-saver';



@Component({
  selector: 'app-appointment-rotation',
  templateUrl: './appointment-rotation.component.html',
  styleUrls: ['./appointment-rotation.component.scss'],
})
export class AppointmentRotationComponent
  extends BaseComponent
  implements OnInit
{
  @ViewChild('actionTmpl', { static: true })
  actionTmpl!: TemplateRef<NzSafeAny>;
  @ViewChild('lastReminderDateTmpl', { static: true })
  lastReminderDate!: TemplateRef<NzSafeAny>;
  @ViewChild('effectiveDateTmpl', { static: true })
  effectiveDate!: TemplateRef<NzSafeAny>;
  @ViewChild('planToDate1Tmpl', { static: true })
  planToDate1!: TemplateRef<NzSafeAny>;
  @ViewChild('planToDate2Tmpl', { static: true })
  planToDate2!: TemplateRef<NzSafeAny>;
  

  params = {
    employeeCode: null, // mã nhân viên
    employeeName: null, // họ tên
    unit: null, //đơn vị hiện tại
    title: null, //chức danh hiện tại
    timeCreateIssued: null, // chức danh đề xuất

    formGroup: null, // nhosm hình thức
    form: null, // Hình thức
    issueLevel: null, // Cấp phê duyệt
    position: null,
    autoPrompted: null, //  Đã nhắc tự động
    proposeStatus: null,
    extendReason: null,
    page: 0,
    size: userConfig.pageSize,
  };

  modalRef?: NzModalRef;
  subs: Subscription[] = [];
  isExpand = false;
  iconStatus = Constants.IconStatus.DOWN;
  dataTable: TableAppointmentRotation[] = [];
  dataCommon: DataCommon = {};
  tableConfig!: MBTableConfig;
  pagination = new Pagination();
  heightTable = { x: '100vw', y: '25em' };
  listSubmittedProposed = [
    { code: this.translate.instant('development.proposed-unit.table.sent'), name: this.translate.instant('development.proposed-unit.table.sent') },
    { code: this.translate.instant('development.proposed-unit.table.unsent'), name: this.translate.instant('development.proposed-unit.table.unsent') },
  ];
  listApprovalLevel = [
    { code: this.translate.instant('development.proposed-unit.table.remendered'), name: this.translate.instant('development.proposed-unit.table.remendered') },
    { code: this.translate.instant('development.proposed-unit.table.notReminded'), name: this.translate.instant('development.proposed-unit.table.notReminded') },
  ];
  listExtendReason = [];
  listPosition = [];
  screenCode = '';
  rolesOfCurrentUser: string[] = [];
  isRoleHrdv = false;
  isRolePtnl = false;
  isRoleKsptnl = false;
  unsentStatus = this.translate.instant('development.proposed-unit.table.unsent');



  constructor(
    injector: Injector,
    private readonly service: AppointmentRotationService,
    private readonly proposedUnitService: ProposedUnitService,
    private readonly multipleEmployeeService: ProposeMultipleEmployeeService,
  ) {
    super(injector);
  }

  ngOnInit() {
    this.isLoading = true;
    this.initTable();
    const paramPosition = {
      status: 1,
      type: Constants.TYPE_SEARCH_POSITION,
      page: 0,
      size: maxInt32,
    };


    const subs = forkJoin([
      this.service.getAllFormGroup().pipe(catchError(() => of(undefined))),
      this.service.getExtendReason(Constants.HCM_LD_REASON_EXTEND).pipe(catchError(() => of(undefined))),
      this.proposedUnitService.getPositionGroup().pipe(catchError(() => of(undefined))),
      this.proposedUnitService.getRolesByUsername().pipe(catchError(() => of(undefined))),
      this.service.getExtendReason(Constants.RoleAppointmentRotation.HRDV).pipe(catchError(() => of(undefined))),
      this.service.getExtendReason(Constants.RoleAppointmentRotation.PTNL).pipe(catchError(() => of(undefined))),
      this.service.getExtendReason(Constants.RoleAppointmentRotation.KSPTNL).pipe(catchError(() => of(undefined))),

    ]).subscribe(
      ([dataFormGroup ,dataExtendReason, listPosition, roleUser, listRoleHrdv, listRolePtnl, listRoleKsptnl]) => {
        this.rolesOfCurrentUser = roleUser;
        this.isRoleHrdv = this.checkRoleOfUser(listRoleHrdv?.data);
        this.isRolePtnl = this.checkRoleOfUser(listRolePtnl?.data);
        this.isRoleKsptnl = this.checkRoleOfUser(listRoleKsptnl?.data);

        this.listExtendReason = dataExtendReason?.data?.map((item: string) => { return {
          code: item,
          name: item
        } });
        this.listPosition = listPosition?.data?.content;
        if (this.isRoleHrdv) {
          this.screenCode = Constants.ScreenCode.HRDV
        }
        if (this.isRolePtnl) {
          this.screenCode = Constants.ScreenCode.PTNL
        }
        if (this.isRoleKsptnl) {
          this.screenCode = Constants.ScreenCode.KSPTNL
        }
        this.service.getState({
             screenCode: this.screenCode,
           }).subscribe(
          (state) => {
            this.dataCommon = state ? state : {};
            this.dataCommon.formGroupMultipleList = dataFormGroup?.data?.content ?? [];
            this.search(1);
          },
          (e) => {
            this.isLoading = false;
            this.getMessageError(e);
          })

      },
    (e) => {
      this.getMessageError(e);
      this.isLoading = false;
    });

  }

  checkRoleOfUser(listRoleFunction: string[]) {
    return listRoleFunction?.some(el => this.rolesOfCurrentUser?.includes(el));
  }

  search(pageIndex: number) {
    this.isLoading = true;
    this.params.page = pageIndex - 1;
    const searchParams = { ...cleanData(this.params) };
    if (searchParams?.timeCreateIssued) {
      searchParams.dueDateFrom =
      this.params.timeCreateIssued && this.params.timeCreateIssued[0] ? moment(searchParams.timeCreateIssued[0]).format(Constants.FORMAT_DATE.YYYY_MM_DD) : null;
      searchParams.dueDateTo =
      this.params.timeCreateIssued && this.params.timeCreateIssued[1] ? moment(searchParams.timeCreateIssued[1]).format(Constants.FORMAT_DATE.YYYY_MM_DD) : null;
    }
    searchParams.unit = searchParams?.unit?.orgId || null;
    delete searchParams?.timeCreateIssued;
    this.service.searchAppointmentRotation(searchParams).subscribe(
      (res) => {
        this.isLoading = false;
        this.dataTable = res?.data?.content || [];
        this.tableConfig.total = res?.data?.totalElements || 0;
        this.tableConfig.pageIndex = pageIndex;
        this.isLoading = false;
      },
      (e) => {
        this.isLoading = false;
        this.getMessageError(e)
      }
    );
  }

  collapseExpand() {
    this.isExpand = !this.isExpand;
    this.isExpand
      ? (this.iconStatus = Constants.IconStatus.UP)
      : (this.iconStatus = Constants.IconStatus.DOWN);
  }

  exportExcel() {
    if (this.dataTable?.length === 0) {
      this.toastrCustom.warning(
        this.translate.instant(
          'development.proposed-unit.messages.dataTableEmpty'
        )
      );
      return;
    }
    this.isLoading = true;
    const searchParams = { ...cleanData(this.params) };
    if (searchParams?.timeCreateIssued) {
      searchParams.dueDateFrom =
      this.params.timeCreateIssued && this.params.timeCreateIssued[0] ? moment(searchParams.timeCreateIssued[0]).format(Constants.FORMAT_DATE.YYYY_MM_DD) : null;
      searchParams.dueDateTo =
      this.params.timeCreateIssued && this.params.timeCreateIssued[1] ? moment(searchParams.timeCreateIssued[1]).format(Constants.FORMAT_DATE.YYYY_MM_DD) : null;
    }
    searchParams.unit = searchParams?.unit?.orgId || null;
    delete searchParams?.timeCreateIssued;
    this.service.exportExcelAppointmentRotation(searchParams).subscribe(
      (data) => {
        let urlExcel = 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,';
        urlExcel = urlExcel + data.data;
        FileSaver.saveAs(urlExcel, this.translate.instant('development.promulgate.message.nameFileExcelAppointmentRotation'));
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

  getFormByFormGroup(e: number) {
    let api: Observable<BaseResponse>;
    if (e) {
      api = this.service.formGroupProposeMultiple(e);
    } else {
      api = this.service.getAllFormGroup();
    }
    api.subscribe((res) => {
      this.dataCommon.formGroupMultipleList = (e ? res?.data : res?.data?.content) || [];
    }, (e) => {
      this.dataCommon.formGroupMultipleList = [];
    })

  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'development.promulgate.table.empCode',
          field: 'employeeCode',
          width: 110,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.promulgate.table.empName',
          field: 'employeeName',
          width: 150,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.promulgate.table.formCode',
          field: 'formName',
          width: 100,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.promulgate.search.effectiveDate',
          field: 'effectiveDate',
          width: 120,
          tdTemplate: this.effectiveDate,
        },
        {
          title: 'development.proposed-unit.table.formGroup',
          field: 'formGroupName',
          width: 120,
        },
        {
          title: 'development.proposed-unit.table.approveLevel',
          field: 'approvalLevel',
          width: 120,
        },
        {
          title: 'development.proposed-unit.table.unit',
          field: 'unitPathName',
          width: 150,
        },
        {
          title: 'development.proposed-unit.table.title',
          field: 'titleName',
          width: 100,
        },
        {
          title: 'development.proposed-unit.table.level',
          field: 'levelName',
          width: 150,
        },
        {
          title: 'development.proposed-unit.table.position',
          field: 'positionName',
          width: 150,
        },
        {
          title: 'development.proposed-unit.table.dayRotationMinimum',
          field: 'planToDate1',
          width: 150,
          tdTemplate: this.planToDate1,
        },
        {
          title: 'development.proposed-unit.table.dayRotationMaximum',
          field: 'planToDate2',
          width: 150,
          tdTemplate: this.planToDate2,
        },
        {
          title: 'development.proposed-unit.table.originalUnit',
          field: 'originalUnitPathName',
          width: 150,
        },
        {
          title: 'development.proposed-unit.table.originalTitle',
          field: 'originalTitleName',
          width: 120,
        },
        {
          title: 'development.proposed-unit.detail.employee.T2',
          field: 'performanceResultT2',
          width: 120,
        },
        {
          title: 'development.proposed-unit.detail.employee.T1',
          field: 'performanceResultT1',
          width: 120,
        },
        {
          title: 'development.proposed-unit.table.statusSendPropose',
          field: 'proposeStatus',
          width: 180,
        },
        {
          title: 'development.proposed-unit.table.statusApprove',
          field: 'flowStatusName',
          width: 100,
        },
        {
          title: 'development.proposed-unit.table.reasonExtend',
          field: 'extendReason',
          width: 120,
        },
        {
          title: 'development.proposed-unit.table.reasonDetail',
          field: 'reasonDetail',
          width: 150,
        },
        {
          title: 'development.proposed-unit.table.nearestRemindDate',
          field: 'lastReminderDate',
          width: 150,
          tdTemplate: this.lastReminderDate
        },
        {
          title: 'development.approvement-propose.search.handlePerson',
          field: 'processedBy',
          width: 150,
        },
        {
          title: '',
          field: '',
          tdTemplate: this.actionTmpl,
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 60,
          fixed: true,
          fixedDir: 'right',
        },
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1,
    };
  }

  showModalReasonOrCreate(isCreate: boolean, workProcessId: number) {
    const itemWorkProcess = this.dataTable.find((i: TableAppointmentRotation) => i.workProcessId === workProcessId);
    this.modalRef = this.modal.create({
      nzTitle: isCreate ? this.translate.instant(
        'development.proposed-unit.button.createProposal'
      ) : this.translate.instant(
        'development.proposed-unit.table.extendReason'
      ),
      nzWidth: '40vw',
      nzContent: ReasonForExtensionComponent,
      nzComponentParams: {
        formGroupList: this.dataCommon.formGroupList,
        isCreate: isCreate,
        listExtendReason: this.listExtendReason
      },
      nzFooter: null,
    });
    this.modalRef.componentInstance.saveInfo.subscribe(
      (data: ProposeReason) => {
        this.isLoading = true;
        if (isCreate) {
          this.multipleEmployeeService.getProposeId().subscribe((result) => {
            // const proposeId = result?.data;
            const paramAddEmp = {
              sortOrder: null,
              proposeId: result?.data,
              employeeCodes: [itemWorkProcess ? itemWorkProcess?.employeeCode : null],
              proposeUnitRequest: null,
              formGroup: data.formGroupList,
              proposeUnitRequestName: null,
            };

            this.multipleEmployeeService.addEmployee(paramAddEmp, Constants.ScreenCode.PTNL).subscribe((res) => {
              this.isLoading = false;
              this.toastrCustom.success(this.translate.instant('development.commonMessage.success'));
            },(e) => {
              this.isLoading = false;
              this.getMessageError(e)
            })
          })

        } else {
          this.service.saveProposalExtension(itemWorkProcess?.workProcessId ?? 0, data.reasonExtension, data.detailReason).subscribe((result) => {
            this.isLoading = false;
            this.toastrCustom.success(this.translate.instant('development.commonMessage.success'));
          },(e) => {
            this.isLoading = false;
            this.getMessageError(e)
          })
        }

        this.modalRef?.destroy();
      }
    );

    // this.modalRef?.afterClose.subscribe((result: boolean) => {
    //   if (!result) {
    //   }
    // });
  }

  getInnerWidth(): number {
    if (window.innerWidth > 767) {
      return window.innerWidth / 1.5 > 1368 ? 1368 : window.innerWidth / 1.3;
    }
    return window.innerWidth;
  }
}
