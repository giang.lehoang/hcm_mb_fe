import {AppointmentRotationComponent} from "./appointment-rotation-view/appointment-rotation.component";
import { ReasonForExtensionComponent } from "./reason-for-extension/reason-for-extension.component";

export const pages = [AppointmentRotationComponent, ReasonForExtensionComponent];

export * from './appointment-rotation-view/appointment-rotation.component';
export * from './reason-for-extension/reason-for-extension.component';
