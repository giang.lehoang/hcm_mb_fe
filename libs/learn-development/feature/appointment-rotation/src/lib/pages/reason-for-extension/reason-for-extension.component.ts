import { Component, EventEmitter, Injector, Input, OnInit, Output } from '@angular/core';
import { EmployeeAppointment, FormGroupList, ProposeReason } from '@hcm-mfe/learn-development/data-access/models';
import { ProposeMultipleEmployeeService } from "@hcm-mfe/learn-development/data-access/services";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import { NzModalRef } from 'ng-zorro-antd/modal';



@Component({
  selector: 'app-reason-for-extension.component',
  templateUrl: './reason-for-extension.component.html',
  styleUrls: ['./reason-for-extension.component.scss'],
})
export class ReasonForExtensionComponent extends BaseComponent implements OnInit {

  @Output() saveInfo = new EventEmitter<ProposeReason>();
  @Output() closeEvent = new EventEmitter<boolean>();
  @Input() formGroupList: FormGroupList[] = [];
  @Input() isCreate = false;
  @Input() listExtendReason = [];
  isSubmitted = false;


  params = {
    formGroupList: null,
    reasonExtension: null,
    detailReason: null,
  }
  reasonExtension = [];


  constructor(injector: Injector, private readonly modalRef: NzModalRef) {
    super(injector);
  }



  ngOnInit(): void {
  }

  closeModal() {
    this.modalRef?.destroy();
  }

  save() {
    this.isSubmitted = true;
    if ((this.isCreate && !this.params.formGroupList) || (!this.isCreate && (!this.params.reasonExtension || !this.params.detailReason) )) {
      return
    }
    this.saveInfo.emit(this.params);

  }


}
