import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {components} from "./components";
import {PopupService} from "@hcm-mfe/shared/ui/popup";
import {pages} from "./pages";
import {LearnDevelopmentFeatureShellModule} from "@hcm-mfe/learn-development/feature/shell";
import {SharedUiMbSelectCheckAbleModule} from "@hcm-mfe/shared/ui/mb-select-check-able";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {
  LearnDevelopmentFeatureAppointmentRotationRoutingModule
} from "./learn-development-feature-appointment-rotation-routing.module";

@NgModule({
  declarations: [...pages, ...components],
  exports: [...pages, ...components],
  imports: [LearnDevelopmentFeatureShellModule,
    SharedUiMbSelectCheckAbleModule,
    NzDropDownModule,
    LearnDevelopmentFeatureAppointmentRotationRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
  providers: [PopupService]
})
export class LearnDevelopmentFeatureAppointmentRotationModule {}
