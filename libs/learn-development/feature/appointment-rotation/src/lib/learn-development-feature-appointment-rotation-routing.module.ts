import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AppointmentRotationComponent} from "./pages";


const routes: Routes = [
  { path: '', component: AppointmentRotationComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LearnDevelopmentFeatureAppointmentRotationRoutingModule {}
