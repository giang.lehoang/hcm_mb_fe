import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FunctionCode, ScreenType } from '@hcm-mfe/learn-development/data-access/common';
import { ProposedAppointmentRenewalCreateComponent, ProposedUnitHandleComponent, ProposeTGCVComponent } from '@hcm-mfe/learn-development/feature/propose-unit';
import { ListChangeIssueSearchComponent } from './pages';

const routes: Routes = [
  {
    path: '',
    component: ListChangeIssueSearchComponent,
  },
  {
    path: 'detail',
    component: ProposedUnitHandleComponent,
    data: {
      pageName: 'development.pageName.issue-transfer-detail',
      breadcrumb: 'development.breadcrumb.issue-transfer-detail',
      screenType: ScreenType.Detail,
      handlePropose: true,
    },
  },
  {
    path: 'detail-tgcv',
    component: ProposeTGCVComponent,
    data: {
      pageName: 'development.pageName.issue-transfer-detail',
      breadcrumb: 'development.breadcrumb.issue-transfer-detail',
      screenType: ScreenType.Detail,
      handlePropose: true,
    },
  },
  {
    path: 'detail-appointment-renewal',
    component: ProposedAppointmentRenewalCreateComponent,
    data: {
      pageName: 'development.pageName.issue-transfer-detail',
      breadcrumb: 'development.breadcrumb.issue-transfer-detail',
      screenType: ScreenType.Detail,
      handlePropose: true,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListChangeIssueRoutingModule {}
