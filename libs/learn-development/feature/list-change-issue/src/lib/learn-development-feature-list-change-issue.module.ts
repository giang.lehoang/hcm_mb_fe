import {
  CUSTOM_ELEMENTS_SCHEMA,
  NgModule,
  NO_ERRORS_SCHEMA,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { pages } from './pages';
import { components } from './components';
import { LearnDevelopmentFeatureShellModule } from '@hcm-mfe/learn-development/feature/shell';
import { PopupService } from '@hcm-mfe/shared/ui/popup';
import { ListChangeIssueRoutingModule } from './list-change-issue.routing.module';

@NgModule({
  declarations: [...pages, ...components],
  exports: [...pages, ...components],
  imports: [CommonModule, LearnDevelopmentFeatureShellModule, ListChangeIssueRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
  providers: [PopupService],
})
export class LearnDevelopmentFeatureListChangeIssueModule {}
