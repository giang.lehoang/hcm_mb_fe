import {
  Component,
  EventEmitter,
  Injector, Input, OnInit, Output
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {
  Constants
} from '@hcm-mfe/learn-development/data-access/common';
import {
  ApplyProposePublish
} from '@hcm-mfe/learn-development/data-access/models';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';

@Component({
  selector: 'app-apply-issue-info',
  templateUrl: './apply-issue-info.component.html',
  styleUrls: ['./apply-issue-info.component.scss'],
})
export class ApplyIssueInfoComponent extends BaseComponent implements OnInit {
  form: FormGroup;
  constantsTimeType = Constants.TimeTypeCode;
  listTypeMaximumTime = Constants.TypeMaximumTime;
  isSubmitted = false;
  @Input() proposeDetailIds: number[] = [];
  @Output() closeEvent = new EventEmitter<boolean>();
  @Output() applyProposeBody = new EventEmitter<ApplyProposePublish>();

  constructor(injector: Injector) {
    super(injector);
    this.form = new FormGroup({
      effectiveDate: new FormControl(null, Validators.required),
      sameDecision: new FormControl(false),
      minimumTimePublish: new FormControl(null),
      issueEffectiveEndDate1: new FormControl(null),
      maximumTimePublish: new FormControl(null),
      timeType: new FormControl(null),
      issueEffectiveEndDate2: new FormControl(null),
      checkWorkProcess: new FormControl(true),
    });
  }

  ngOnInit() {
    this.form.get('timeType')?.setValue(Constants.TimeTypeCode.MONTH);
  }

  submit() {
    this.isSubmitted = true;
    if (this.form.get('sameDecision')?.value) {
      this.form.get('effectiveDate')?.clearValidators();
      this.form.get('effectiveDate')?.updateValueAndValidity();
    }
    if ((this.form.get('timeType')?.value === Constants.TimeTypeCode.MONTH
      && +this.form.get('minimumTimePublish')?.value >= +this.form.get('maximumTimePublish')?.value)
      || (this.form.get('timeType')?.value === Constants.TimeTypeCode.YEAR
        && +this.form.get('minimumTimePublish')?.value >= +this.form.get('maximumTimePublish')?.value * 12)) {
          this.toastrCustom.warning(this.translate.instant('development.list-change-issue.applyIssue.validateTimeType'));
          return;
    }
    if (this.form.valid) {
      const body = {
        proposeDetailIds: this.proposeDetailIds,
        effectiveDate: this.form.get('effectiveDate')?.value,
        minimumTimePublish: this.form.get('minimumTimePublish')?.value,
        issueEffectiveEndDate1: this.form.get('issueEffectiveEndDate1')?.value,
        maximumTimePublish: this.form.get('maximumTimePublish')?.value,
        timeType: this.form.get('timeType')?.value,
        issueEffectiveEndDate2: this.form.get('issueEffectiveEndDate2')?.value,
        checkWorkProcess: this.form.get('checkWorkProcess')?.value
      }
      this.closeEvent.emit(true);
      this.applyProposeBody.emit(body);
    }
  }

  closeModal() {
    this.closeEvent.emit(false);
  }

  ngAfterViewInit(): void {
    this.form.get('sameDecision')?.valueChanges.subscribe((sameDecision: boolean) => {
      if (sameDecision) {
        this.form.get('effectiveDate')?.setValue(null);
        this.form.get('issueEffectiveEndDate1')?.setValue(null);
        this.form.get('issueEffectiveEndDate2')?.setValue(null);
      }
    })
    this.form.get('effectiveDate')?.valueChanges.subscribe(() => {
      this.getIssueEffectiveEndDate1();
      this.getIssueEffectiveEndDate2();
    });
    this.form.get('minimumTimePublish')?.valueChanges.subscribe(() => {
      this.getIssueEffectiveEndDate1();
    });
    this.form.get('maximumTimePublish')?.valueChanges.subscribe(() => {
      this.getIssueEffectiveEndDate2();
    });
    this.form.get('timeType')?.valueChanges.subscribe(() => {
      this.getIssueEffectiveEndDate2();
    });
  }

  //check Ngày đến hạn tối thiểu
  getIssueEffectiveEndDate1() {
    let rangeTime = 0;
    if (this.form.get('minimumTimePublish')?.value) {
      rangeTime = +this.form.get('minimumTimePublish')?.value;
    } else if (!this.form.get('minimumTimePublish')?.value || !this.form.get('effectiveDate')?.value){
      rangeTime = 0;
      this.form.get('issueEffectiveEndDate1')?.setValue(null);
    }
    //tính ngày kết thúc
    if (this.form.get('effectiveDate')?.value && rangeTime > 0) {
      const oldDate = new Date(this.form.get('effectiveDate')?.value);
      const date1 = oldDate.getDate();
      const month1 = oldDate.getMonth() + 1;
      let year1 = oldDate.getFullYear();
      let newMonth = 0;
      newMonth = month1 + rangeTime;
      if (rangeTime <= 12) {
        if (newMonth > 12) {
          year1 = year1 + 1;
          newMonth = newMonth - 12;
        }
      } else {
        const yearBonus = Math.floor(rangeTime / 12);
        const monthBalance = rangeTime - yearBonus * 12;
        newMonth = month1 + monthBalance;
        if (rangeTime > 12) {
          year1 = year1 + yearBonus;
          if (newMonth > 12) {
            newMonth = newMonth - 12;
            year1 = year1 + 1;
          }
        } else {
          year1 = year1 + 1;
        }
      }
      const newDate = new Date();
      newDate.setDate(date1);
      newDate.setMonth(newMonth - 1);
      newDate.setFullYear(year1);
      this.form.get('issueEffectiveEndDate1')?.setValue(newDate);
    }
  }

  //check ngày đến hạn tối đa
  getIssueEffectiveEndDate2() {
    let rangeTime = 0;
    if (this.form.get('maximumTimePublish')?.value) {
      if (
        this.form.get('timeType')?.value ===
        Constants.TypeMaximumValue.MONTH
      ) {
        rangeTime = +this.form.get('maximumTimePublish')?.value;
      } else if (
        this.form.get('timeType')?.value ===
        Constants.TypeMaximumValue.YEAR
      ) {
        rangeTime = +this.form.get('maximumTimePublish')?.value * 12;
      }
    } else if (!this.form.get('maximumTimePublish')?.value || !this.form.get('effectiveDate')?.value){
      rangeTime = 0;
      this.form.get('issueEffectiveEndDate2')?.setValue(null);
    }
    //tính ngày kết thúc
    if (this.form.get('effectiveDate')?.value && rangeTime > 0) {
      const oldDate = new Date(this.form.get('effectiveDate')?.value);
      const date1 = oldDate.getDate();
      const month1 = oldDate.getMonth() + 1;
      let year1 = oldDate.getFullYear();
      let newMonth = 0;
      newMonth = month1 + rangeTime;
      if (rangeTime <= 12) {
        if (newMonth > 12) {
          year1 = year1 + 1;
          newMonth = newMonth - 12;
        }
      } else {
        const yearBonus = Math.floor(rangeTime / 12);
        const monthBalance = rangeTime - yearBonus * 12;
        newMonth = month1 + monthBalance;
        if (rangeTime > 12) {
          year1 = year1 + yearBonus;
          if (newMonth > 12) {
            newMonth = newMonth - 12;
            year1 = year1 + 1;
          }
        } else {
          year1 = year1 + 1;
        }
      }
      const newDate = new Date();
      newDate.setDate(date1);
      newDate.setMonth(newMonth - 1);
      newDate.setFullYear(year1);
      this.form.get('issueEffectiveEndDate2')?.setValue(newDate);
    }
  } 
}
