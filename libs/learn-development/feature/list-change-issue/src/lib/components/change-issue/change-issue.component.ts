import {
  Component,
  EventEmitter,
  Injector,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { IssueTransferPopup } from '@hcm-mfe/learn-development/data-access/models';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';

@Component({
  selector: 'app-change-issue',
  templateUrl: './change-issue.component.html',
  styleUrls: ['./change-issue.component.scss'],
})
export class ChangeIssueComponent extends BaseComponent implements OnInit {
  form: FormGroup;
  @Input() proposeDetailIds: number[] = [];
  @Output() closeEvent = new EventEmitter<boolean>();
  @Output() issueTransferBody = new EventEmitter<IssueTransferPopup>();

  constructor(injector: Injector) {
    super(injector);
    this.form = new FormGroup({
      commentPublish: new FormControl(null),
    });
  }

  ngOnInit() {}

  submit() {
    const body = {
      proposeDetailIds: this.proposeDetailIds,
      commentPublish: this.form.get('commentPublish')?.value,
    };
    this.closeEvent.emit(true);
    this.issueTransferBody.emit(body);
  }

  closeModal() {
    this.closeEvent.emit(false);
  }
}
