import { ApplyIssueInfoComponent } from './apply-issue-info/apply-issue-info.component';
import { ChangeIssueComponent } from './change-issue/change-issue.component';
export const components = [ApplyIssueInfoComponent, ChangeIssueComponent];
export * from './apply-issue-info/apply-issue-info.component';
export * from './change-issue/change-issue.component';