import {
  Component,
  Injector,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import {
  Constants,
  userConfig,
} from '@hcm-mfe/learn-development/data-access/common';
import {
  ConditionViolationList,
  IssueLevelList,
  ListIssueTransferTable,
  StatusList,
} from '@hcm-mfe/learn-development/data-access/models';
import {
  ListIssueTransferService,
  ProposedUnitService,
} from '@hcm-mfe/learn-development/data-access/services';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { cleanData } from '@hcm-mfe/shared/common/utils';
import { Category, MBTableConfig } from '@hcm-mfe/shared/data-access/models';
import * as FileSaver from 'file-saver';
import {
  ApplyProposePublish,
  IssueTransferPopup,
} from 'libs/learn-development/data-access/models/src/lib/list-issue-transfer.model';
import * as moment from 'moment';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { forkJoin, Subscription } from 'rxjs';
import {
  ApplyIssueInfoComponent,
  ChangeIssueComponent,
} from '../../components';

@Component({
  selector: 'app-list-change-issue-search',
  templateUrl: './list-change-issue-search.component.html',
  styleUrls: ['./list-change-issue-search.component.scss'],
})
export class ListChangeIssueSearchComponent
  extends BaseComponent
  implements OnInit
{
  screenCodeConst = Constants.ScreenCode;
  params = {
    proposeCode: null,
    empCode: null,
    empName: null,
    currentUnit: null,
    currentTitle: null,
    proposeUnit: null,
    contentTitle: null,
    formGroup: null,
    formCode: null,
    hasEffectiveDate: true,
    noEffectiveDate: true,
    issueLevelCode: null,
    signerName: null,
    approvalName: null,
    agreeDateFrom: null,
    agreeDateTo: null,
    rangeDate: null,
    size: userConfig.pageSize,
    page: 0,
  };

  dataCommon = {
    statusList: <Category[]>[],
    jobDTOS: <Category[]>[],
    typeDTOS: <Category[]>[],
    flowStatusApproveConfigList: <StatusList[]>[],
    flowStatusPublishConfigList: <StatusList[]>[],
    issueLevelList: <IssueLevelList[]>[],
    formGroupList: [], // nhóm hình thức,
    consultationStatus: [],
    interviewStatus: [],
    conditionViolationList: <ConditionViolationList[]>[],
    formDisplaySearchDTOS: []
  };

  isExpand = true;
  iconStatus = Constants.IconStatus.DOWN;
  tableConfig!: MBTableConfig;
  dataTable: ListIssueTransferTable[] = [];
  modalRef?: NzModalRef;
  checked = false;
  rolesOfCurrentUser = [];
  quantityMap: Map<number, ListIssueTransferTable> = new Map<
    number,
    ListIssueTransferTable
  >();
  subs: Subscription[] = [];
  preParam = this.params;

  @ViewChild('groupCBTmpl', { static: true })
  groupCheckbox!: TemplateRef<NzSafeAny>;
  @ViewChild('groupCBHeaderTmpl', { static: true })
  groupCheckboxHeader!: TemplateRef<NzSafeAny>;
  @ViewChild('effectiveDateTmpl', { static: true })
  effectiveDate!: TemplateRef<NzSafeAny>;
  heightTable = { x: '100%', y: '25em' };
  @ViewChild('effectiveEndDate1Tmpl', { static: true })
  effectiveEndDate1!: TemplateRef<NzSafeAny>;
  @ViewChild('effectiveEndDate2Tmpl', { static: true })
  effectiveEndDate2!: TemplateRef<NzSafeAny>;
  @ViewChild('sameDecisionTmpl', { static: true })
  sameDecision!: TemplateRef<NzSafeAny>;
  @ViewChild('minimumTimeTmpl', { static: true })
  minimumTime!: TemplateRef<NzSafeAny>;
  @ViewChild('maximumTimeTmpl', { static: true })
  maximumTime!: TemplateRef<NzSafeAny>;

  constructor(
    injector: Injector,
    private readonly proposeService: ProposedUnitService,
    private readonly service: ListIssueTransferService
  ) {
    super(injector);
  }

  ngOnInit() {
    this.initTable();
    const srcCodeParam = {
      screenCode: Constants.ScreenCode.PTNL,
    };
    this.isLoading = true;
    const sub = forkJoin([
      this.proposeService.getRolesByUsername(),
      this.proposeService.getState(srcCodeParam),
    ]).subscribe(
      ([roles, data]) => {
        this.dataCommon = data;
        this.rolesOfCurrentUser = roles;
        this.search(1);
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }

  expandSearch() {
    this.isExpand = !this.isExpand;
    if (this.isExpand) {
      this.iconStatus = Constants.IconStatus.DOWN;
    } else {
      this.iconStatus = Constants.IconStatus.UP;
    }
  }

  search(firstPage: number) {
    this.isLoading = true;
    this.params.page = firstPage - 1;
    const newParams = { ...cleanData(this.params) };
    newParams.agreeDateFrom =
      this.params.rangeDate && this.params.rangeDate[0]
        ? moment(newParams.rangeDate[0]).format(
            Constants.FORMAT_DATE.YYYY_MM_DD
          )
        : null;
    newParams.agreeDateTo =
      this.params.rangeDate && this.params.rangeDate[1]
        ? moment(this.params.rangeDate[1]).format(
            Constants.FORMAT_DATE.YYYY_MM_DD
          )
        : null;
    delete newParams.rangeDate;
    const currentUnitID = newParams?.currentUnit?.orgId || '';
    newParams.currentUnit = currentUnitID;
    const proposeUnitID = newParams?.proposeUnit?.orgId || '';
    newParams.proposeUnit = proposeUnitID;
    newParams.screenCode = Constants.ScreenCode.PTNL;
    this.isLoading = true;
    const sub = this.service.search(newParams).subscribe(
      (res) => {
        this.dataTable = res?.data?.content || [];
        this.checked = true;
        this.dataTable?.forEach((item) => {
          if (item?.proposeDetailId) {
            if (!this.quantityMap.has(item.proposeDetailId)) {
              this.checked = false;
            } else {
              this.quantityMap.set(item.proposeDetailId, item);
            }
          }
        });
        if (!this.dataTable?.length) {
          this.checked = false;
        }
        this.tableConfig.total = res?.data?.totalElements || 0;
        this.tableConfig.pageIndex = firstPage;
        this.preParam = newParams;
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }

  initTable() {
    this.tableConfig = {
      headers: [
        {
          title: '',
          field: 'select',
          width: 40,
          thClassList: [
            Constants.TABLE_CONFIG.TEXT_NOWRAP,
            Constants.TABLE_CONFIG.TEXT_CENTER,
          ],
          tdClassList: [
            Constants.TABLE_CONFIG.TEXT_NOWRAP,
            Constants.TABLE_CONFIG.TEXT_CENTER,
          ],
          fixed: true,
          thTemplate: this.groupCheckboxHeader,
          tdTemplate: this.groupCheckbox,
          fixedDir: 'left',
        },
        {
          title: 'development.promulgate.search.proposeCode',
          field: 'proposeCode',
          thClassList: [
            Constants.TABLE_CONFIG.TEXT_NOWRAP,
            Constants.TABLE_CONFIG.TEXT_CENTER,
          ],
          tdClassList: [
            Constants.TABLE_CONFIG.TEXT_NOWRAP,
            Constants.TABLE_CONFIG.TEXT_CENTER,
          ],
          width: 100,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.proposed-unit.table.empCode',
          field: 'employeeCode',
          width: 110,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.proposed-unit.table.empName',
          field: 'employeeName',
          width: 150,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.proposed-unit.table.form',
          field: 'formRotationName',
          width: 120,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.proposed-unit.table.currentUnit',
          field: 'employeeUnitPathName',
          fixed: true,
          fixedDir: 'left',
          width: 200,
        },
        {
          title: 'development.proposed-unit.table.currentTitle',
          field: 'employeeTitleName',
          width: 150,
        },
        {
          title: 'development.proposed-unit.table.proposeUnit',
          field: 'contentUnitPathName',
          width: 200,
        },
        {
          title: 'development.proposed-unit.table.proposeTitle',
          field: 'contentTitleName',
          width: 150,
        },
        {
          title: 'development.promulgate.search.issueLevelCode',
          field: 'issueLevelName',
          width: 100,
        },
        {
          title: 'development.approvement-propose.search.approvePerson',
          field: 'approvalFullName',
          width: 170,
        },
        {
          title: 'development.promulgate.search.signerCode',
          field: 'signerFullName',
          width: 170,
        },
        {
          title: 'development.proposed-unit.table.effectiveDate',
          field: 'effectiveDate',
          tdTemplate: this.effectiveDate,
          width: 120,
        },
        {
          title: 'development.proposed-unit.table.followQD',
          field: 'sameDecision',
          tdTemplate: this.sameDecision,
          width: 100,
        },
        {
          title: 'development.list-change-issue.search.minDeadline',
          field: 'minimumTime',
          width: 100,
          tdTemplate: this.minimumTime,
        },
        {
          title: 'development.list-change-issue.search.minDeadlineDate',
          field: 'effectiveEndDate1',
          tdTemplate: this.effectiveEndDate1,
          width: 120,
        },
        {
          title: 'development.list-change-issue.search.maxDeadline',
          field: 'maximumTime',
          width: 100,
          tdTemplate: this.maximumTime,
        },
        {
          title: 'development.list-change-issue.search.maxDeadlineDate',
          field: 'effectiveEndDate2',
          tdTemplate: this.effectiveEndDate2,
          width: 120,
        },
        {
          title: 'development.list-change-issue.search.impactWorkingProcess',
          field: 'impactWorkProcess',
          width: 150,
        },
        {
          title: 'development.proposed-unit.table.noteWithdraw',
          field: 'withdrawNote',
          width: 200,
        },
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: userConfig.pageSize,
      pageIndex: 1,
    };
  }

  //hiển thị modal áp thông tin ban hành
  showApplyIssueModal() {
    if (!this.quantityMap.size) {
      this.toastrCustom.error(
        this.translate.instant('development.commonMessage.chooseMin')
      );
      return;
    }
    this.modalRef = this.modal.create({
      nzTitle: this.translate.instant(
        'development.list-change-issue.search.applyIssueInfor'
      ),
      nzWidth: '75vw',
      nzContent: ApplyIssueInfoComponent,
      nzComponentParams: {
        proposeDetailIds: this.getListOfID(),
      },
      nzFooter: null,
    });
    this.modalRef.componentInstance.closeEvent.subscribe((data: boolean) => {
      if (data) {
        this.isLoading = true;
        this.modalRef?.componentInstance.applyProposeBody.subscribe(
          (body: ApplyProposePublish) => {
            const sub = this.service.applyProposePublish(body).subscribe(
              () => {
                this.toastrCustom.success(
                  this.translate.instant(
                    'development.list-change-issue.applyIssue.applyIssueSuccess'
                  )
                );
                this.search(1);
                this.quantityMap.clear();
              },
              (e) => {
                this.getMessageError(e);
                this.isLoading = false;
              }
            );
            this.subs.push(sub);
          }
        );
      }
      this.modalRef?.destroy();
    });
  }

  //hiển thị popup chuyển ban hành
  issueTransfer() {
    if (!this.quantityMap.size) {
      this.toastrCustom.error(
        this.translate.instant('development.commonMessage.chooseMin')
      );
      return;
    }
    this.modalRef = this.modal.create({
      nzTitle: this.translate.instant(
        'development.promulgate.section.changeIssue'
      ),
      nzWidth: '75vw',
      nzContent: ChangeIssueComponent,
      nzComponentParams: {
        proposeDetailIds: this.getListOfID(),
      },
      nzFooter: null,
    });
    this.modalRef.componentInstance.closeEvent.subscribe((data: boolean) => {
      if (data) {
        this.isLoading = true;
        this.modalRef?.componentInstance.issueTransferBody.subscribe(
          (body: IssueTransferPopup) => {
            const sub = this.service.issueTransfer(body).subscribe(
              (res) => {
                this.toastrCustom.success(res?.data);
                this.search(1);
                this.quantityMap.clear();
              },
              (e) => {
                this.getMessageError(e);
                this.isLoading = false;
              }
            );
            this.subs.push(sub);
            this.modalRef?.destroy();
          }
        );
      } else {
        this.modalRef?.destroy();
      }
    });
  }

  updateCheckedSet(item: ListIssueTransferTable, checked: boolean): void {
    if (item?.proposeDetailId) {
      if (checked) {
        this.quantityMap.set(item.proposeDetailId, item);
      } else {
        this.quantityMap.delete(item.proposeDetailId);
      }
    }
  }

  onItemChecked(proposeDetailId: number, checked: boolean): void {
    const item = this.dataTable.find(
      (itemTable) => itemTable.proposeDetailId === proposeDetailId
    );
    if (item) {
      this.updateCheckedSet(item, checked);
    }
    this.refreshCheckedStatus();
  }

  onAllChecked(value: boolean): void {
    this.dataTable.forEach((item) => this.updateCheckedSet(item, value));
    this.refreshCheckedStatus();
  }

  refreshCheckedStatus(): void {
    this.checked = this.dataTable.every(({ proposeDetailId }) =>
      this.quantityMap.has(proposeDetailId ? proposeDetailId : 0)
    );
  }

  getListOfID() {
    const idList: number[] = [];
    this.quantityMap.forEach((values) => {
      if (values.proposeDetailId) {
        idList.push(values.proposeDetailId);
      }
    });
    return idList;
  }

  detail(item: ListIssueTransferTable) {
    const queryParams = {
      proposeDetailId: item.proposeDetailId,
      proposeCategory: Constants.ProposeCategory.ELEMENT,
      flowStatusCode: item.flowStatusCode,
      withdrawNoteCode: item.withdrawNoteCode,
      relationType: false,
      screenCode: Constants.ScreenCode.PTNL,
    };
    if (item.formGroup) {
      if (
        +item.formGroup === Constants.FormGroupList.TGCV &&
        !item?.personalCode
      ) {
        this.router.navigate([this.router.url, Constants.SubRouterLink.DETAILTGCV], {
          queryParams: queryParams,
          skipLocationChange: true,
        });
      } else if (
        (+item.formGroup === Constants.FormGroupList.TGCV ||
          +item.formGroup === Constants.FormGroupList.DCDD) &&
        item?.personalCode
      ) {
        this.router.navigate(
          [Constants.KeyCommons.DEVELOPMENT_RECOMMEND_INDIVIDUAL_DETAIL],
          {
            queryParams: {
              proposeDetailId: item?.proposeDetailId,
              proposeCategory: Constants.ProposeCategory.ELEMENT,
            },
            state: { backUrl: this.router.url },
            skipLocationChange: true,
          }
        );
      } else if (+item?.formGroup === Constants.FormGroupList.BO_NHIEM) {
        this.router.navigate(
          [this.router.url, Constants.TypeScreenHandle.DETAIL_APPOINTMENT],
          {
            queryParams: queryParams,
            skipLocationChange: true,
          }
        );
      } else {
        this.router.navigate([this.router.url, Constants.TypeScreenHandle.DETAIL], {
          queryParams: queryParams,
          skipLocationChange: true,
        });
      }
    } else {
      this.router.navigate([this.router.url, Constants.TypeScreenHandle.DETAIL], {
        queryParams: queryParams,
        skipLocationChange: true,
      });
    }
  }

  exportExcel() {
    if (this.dataTable?.length === 0) {
      this.toastrCustom.warning(
        this.translate.instant(
          'development.proposed-unit.messages.dataTableEmpty'
        )
      );
      return;
    }
    this.isLoading = true;
    const newParams = { ...cleanData(this.preParam) };
    this.service.exportExcel(newParams).subscribe(
      (data) => {
        let urlExcel =
          'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,';
        urlExcel = urlExcel + data.data;
        FileSaver.saveAs(
          urlExcel,
          this.translate.instant(
            'development.proposed-unit.messages.excelIssueTransfer'
          )
        );
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }
}
