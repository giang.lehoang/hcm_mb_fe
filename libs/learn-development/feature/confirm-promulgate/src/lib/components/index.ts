import { CommentPublishComponent } from './comment-publish/comment-publish.component';
import { SubmitGroupComponent } from './submit-group/submit-group.component';
export const components = [CommentPublishComponent, SubmitGroupComponent];
export * from './comment-publish/comment-publish.component';
export * from './submit-group/submit-group.component';
