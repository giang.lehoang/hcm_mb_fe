import {
  Component,
  EventEmitter,
  Injector,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Constants } from '@hcm-mfe/learn-development/data-access/common';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';

@Component({
  selector: 'app-submit-group',
  templateUrl: './submit-group.component.html',
  styleUrls: ['./submit-group.component.scss'],
})
export class SubmitGroupComponent extends BaseComponent implements OnInit {
  isSubmitted = false;
  form: FormGroup;
  @Input() buttonName: string = '';
  @Output() ideaSubmit = new EventEmitter<string>();
  @Output() closeEvent = new EventEmitter<boolean>();
  header1 = '';
  headerText = '';
  placeholder = '';

  constructor(injector: Injector) {
    super(injector);
    this.form = new FormGroup({
      idea: new FormControl(null, Validators.required),
    });
  }

  ngOnInit() {
    this.header1 = this.translate.instant(
      'development.confirmIssueGroup.header1',
      {
        buttonName: this.buttonName,
      }
    );
    this.headerText = this.translate.instant(
      'development.confirmIssueGroup.ideaHeader',
      {
        buttonName: this.buttonName,
      }
    );
    this.placeholder = this.translate.instant(
      'development.confirmIssueGroup.ideaPlaceholder',
      {
        buttonName: this.buttonName,
      }
    );
  }

  submit() {
    if (this.buttonName === Constants.ButtonConfirmIssueName2.CONFIRMCANCEL
      || this.buttonName === Constants.ButtonConfirmIssueName2.CANCELREJECTCONFIRM) {
      this.isSubmitted = true;
      if (this.form.valid) {
        this.closeEvent.emit(true);
        this.ideaSubmit.emit(this.form.get('idea')?.value);
      }
    } else {
      this.closeEvent.emit(true);
      this.ideaSubmit.emit(this.form.get('idea')?.value);
    }
  }

  closeModal() {
    this.closeEvent.emit(false);
  }
}
