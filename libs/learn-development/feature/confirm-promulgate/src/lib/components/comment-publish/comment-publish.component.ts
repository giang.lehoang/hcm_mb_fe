import {
  Component,
  EventEmitter,
  Injector,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Constants } from '@hcm-mfe/learn-development/data-access/common';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';

@Component({
  selector: 'app-comment-publish',
  templateUrl: './comment-publish.component.html',
  styleUrls: ['./comment-publish.component.scss'],
})
export class CommentPublishComponent extends BaseComponent implements OnInit {
  @Input() commentConfirmPublish: string = '';

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {}
}
