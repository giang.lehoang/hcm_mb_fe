import { HttpErrorResponse } from '@angular/common/http';
import { Component, Injector, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Constants, ScreenType, userConfig } from "@hcm-mfe/learn-development/data-access/common";
import {
  DataEmployee,
  FileDetail,
  FlowStatusPublishConfigList,
  ParamPromulgate, StateConfirmPromulgate
} from "@hcm-mfe/learn-development/data-access/models";
import { ConfirmPromulgateService, PromulgateLdService } from "@hcm-mfe/learn-development/data-access/services";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import { MBTableConfig, Pagination } from "@hcm-mfe/shared/data-access/models";
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { forkJoin, Observable, of, Subscription } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { CommentPublishComponent } from '../../components/comment-publish/comment-publish.component';
import { SubmitGroupComponent } from '../../components/submit-group/submit-group.component';

@Component({
  selector: 'app-confirm-promulgate-action',
  templateUrl: './confirm-promulgate-action.component.html',
  styleUrls: ['./confirm-promulgate-action.component.scss'],
})
export class ConfirmPromulgateActionComponent extends BaseComponent implements OnInit {
  title?: string;
  type?: string;
  dataTable: DataEmployee[] = [];
  params: ParamPromulgate = {};
  proposeType?: string;
  proposeID?: string | number;
  relationType?: number;
  statusPublish = '';
  paramStatus = '';
  isCheckDecision = false;
  isIncomeAnnouncement = false;
  isModeInsurance = false;
  form?: FormGroup;
  listFile = [];
  listFileDecision?: FileDetail;
  listFileIncomeAnnouncement?: FileDetail;
  listFileModeInsurance?: FileDetail;
  modalRef?: NzModalRef;
  dataReason = {
    viewDetailReasonOrCorrection: null,
    reasonOrCorrection: null,
    commentsIssueTransfer: null,
  };
  dataDropdown = Constants.DataDropdown;
  typeAction?: ScreenType;
  proposeCategory = '';
  categoryConst = Constants.ProposeCategory;

  proposeLogs = [];
  statePromulgate?: StateConfirmPromulgate;
  tableConfig!: MBTableConfig;
  pagination = new Pagination();
  publishCode = '';
  withdrawNoteName = '';
  publishContent = '';
  statusPublishName: string | undefined;
  ProposeCategoryConst = Constants.ProposeCategory;
  StatusPublishConst = Constants.StatusPublishCode;
  heightTable = { x: '100%', y: '25em'};
  quantityMap: Map<number, DataEmployee> = new Map<number, DataEmployee>();
  checked = false;
  tableLogs!: MBTableConfig;
  proposeDetailIds: number[] = [];
  commentConfirmPublishItem: number = 0;
  action = '';
  statePublish = '';
  proposeTypeID: number = 0;
  id = 0;

  @ViewChild('groupCBTmpl', {static: true}) groupCheckbox!: TemplateRef<NzSafeAny>;
  @ViewChild('groupCBHeaderTmpl', {static: true}) groupCheckboxHeader!: TemplateRef<NzSafeAny>;
  @ViewChild('joinDateTmpl', {static: true}) joinDate!: TemplateRef<NzSafeAny>;
  @ViewChild('effectiveDateTmpl', {static: true}) effectiveDate!: TemplateRef<NzSafeAny>;
  @ViewChild('endDateTmpl', {static: true}) endDate!: TemplateRef<NzSafeAny>;
  @ViewChild('reportApproveTmpl', {static: true}) reportApprove!: TemplateRef<NzSafeAny>;
  @ViewChild('maximumTimeTmpl', {static: true}) maximumTime!: TemplateRef<NzSafeAny>;
  @ViewChild('ideaConfirmIssueTmpl', {static: true}) ideaConfirmIssue!: TemplateRef<NzSafeAny>;
  @ViewChild('logsDateTimeTmpl', {static: true}) logsDateTime!: TemplateRef<NzSafeAny>;

  constructor(injector: Injector,
      private readonly service: ConfirmPromulgateService,
      private readonly ldService: PromulgateLdService) {
    super(injector);
    this.initForm();
    this.paramStatus = this.route.snapshot.queryParams['state'];
    this.proposeCategory = this.route.snapshot.queryParams['proposeCategory'];
    this.action = this.route.snapshot.queryParams['action'];
    this.statePublish = this.route.snapshot.queryParams['state'];
    this.id = this.route.snapshot.queryParams['id'];
  }
  comment = ''; // ý kiến chi tiết
  flowStatusPublishConfigList: FlowStatusPublishConfigList[] = [];
  listTypeMaximumTime = Constants.TypeMaximumTime;
  typeMaximumTime = Constants.TypeMaximumValue.MONTH;
  ConfirmQuesConst = Constants.ConfirmQuesConfirmIssue;
  TypeStateConst = Constants.TypeStateConfirmIssue;
  submitGroupName1: string = '';
  submitGroupName2: string = '';
  subs: Subscription[] = [];
  panels = [
    {
      active: false,
      disabled: false,
    },
  ];
  pageNumber = 0;
  pageSize =  userConfig.pageSize;
  iconStatus = Constants.IconStatus.UP;
  isExpand = false;
  @ViewChild('dateLog', {static: true}) dateLog!: TemplateRef<NzSafeAny>;

  isViewDetail() {
    return Constants.StatusButton.includes(this.paramStatus);
  }

  checkApiDetail() {
    const checkApiDetail = this.isViewDetail();
    if (!checkApiDetail) {
      return this.service.getDetail(this.params!).pipe(catchError(() => of(undefined)));
    } else {
      return this.service.getPromulgate(this.params!).pipe(catchError(() => of(undefined)));
    }
  }

  ngOnInit() {
    this.initTable();
    this.initLogs();
    this.isLoading = true;

    this.title = this.route.snapshot?.data['pageName'];
    this.typeAction = this.route?.snapshot?.data['screenType'];
    const srcParams = {
      screenCode: Constants.ScreenCode.GDNS,
    };
    this.params = {
      id: this.id,
      proposeCategory: this.proposeCategory,
      state: this.paramStatus,
      action: this.action, // xác nhận ban hành
      screenCode: Constants.ScreenCode.GDNS,
    };
    switch (this.proposeCategory) {
      case Constants.ProposeCategory.ELEMENT:
        this.proposeTypeID = Constants.ProposeTypeIDIssue.ELEMENT;
        break;
      case Constants.ProposeCategory.GROUP:
        this.proposeTypeID = Constants.ProposeTypeIDIssue.GROUP;
        break;
    }
    forkJoin({
      state: this.service.getState(srcParams),
      listFile: this.service.getFile(this.params).pipe(catchError(() => of(undefined))),
      data: this.checkApiDetail(),
      log: this.ldService.getLogsPage(this.id, this.proposeTypeID, Constants.ScreenCode.GDNS, this.pageNumber, this.pageSize)
    }).subscribe(
      (res) => {
        this.isLoading = false;
        this.flowStatusPublishConfigList = res?.state?.flowStatusPublishConfigList || [];
        this.dataTable = res.data?.data?.employeeList || [];
        this.dataTable?.forEach((item: DataEmployee) => {
          if (item.proposeProcessFile) {
            item.processFileId = item.proposeProcessFile.docId;
            item.processFileName = item.proposeProcessFile.fileName;
          }
        });
        //map trạng thái ban hành của bảng nhân sự
        this.dataTable?.forEach((item) => {
          item.statusPublishName = this.flowStatusPublishConfigList?.find(
            (res) => res.key === item.statusPublishCode)?.role;
        });

        this.proposeType = res.data?.data?.proposeType || '';
        this.proposeID = this.dataTable[0]?.proposeCode || '';
        this.relationType = res.data?.data?.relationType?.toString();
        this.listFile = res.listFile?.data;
        this.statusPublish = res.data?.data?.statusPublish;
        this.publishCode = res.data?.data?.publishCode || '';
        //map trạng thái ban hành của đề xuất to
        this.statusPublishName = this.flowStatusPublishConfigList?.find(
          (item: FlowStatusPublishConfigList) => item.key === this.statusPublish)?.role;
        this.withdrawNoteName = res?.data?.data?.withdrawNoteName || '';
        this.publishContent = res.data?.data?.publishContent || '';
        this.isIncomeAnnouncement = res.data?.data?.employeeList[0]?.isIncome;
        this.isModeInsurance = res.data?.data?.employeeList[0]?.isInsurance;
        this.isCheckDecision = this.proposeCategory === this.categoryConst.GROUP
          ? true
          : res.data?.data?.employeeList[0]?.isDecision;

        this.comment = res.data?.data?.commentConfirm;
        this.form?.patchValue(res.data?.data?.proposeDecisionDTO);
        this.form?.get('signerCode')?.patchValue(res.data?.data?.signerName);
        this.form?.get('effectiveDate')?.patchValue(this.dataTable[0].issueEffectiveStartDate);
        this.form?.get('effectiveDateOld')?.patchValue(res.data?.data?.proposeCorrectionDTO?.effectiveDateOld);
        this.form?.get('comment')?.patchValue(res.data?.data?.proposeDecisionDTO?.comments || '');
        this.getState(this.statusPublish);
        this.dataReason = {
          viewDetailReasonOrCorrection: res.data?.data?.proposeCorrectionDTO?.viewDetailReasonOrCorrection,
          reasonOrCorrection: res.data?.data?.proposeCorrectionDTO?.reasonOrCorrection,
          commentsIssueTransfer: res.data?.data?.proposeCorrectionDTO?.commentsIssueTransfer,
        };
        this.listFile.forEach((item: FileDetail) => {
          switch (item.documentType) {
            case Constants.DocumentTypeIssue.DECISION:
              this.listFileDecision = item;
              break;
            case Constants.DocumentTypeIssue.TBINCOME:
              this.listFileIncomeAnnouncement = item;
              break;
            case Constants.DocumentTypeIssue.TBINSURANCE:
              this.listFileModeInsurance = item;
              break;
          }
        });
        this.commentConfirmPublishItem = this.dataTable.filter((item) => item?.commentConfirmPublish)?.length;
        this.proposeLogs = res?.log?.data?.content || [];
        this.tableLogs.total = res?.log?.data?.totalElements || 0;
        this.tableLogs.pageIndex = 1;
      },
      (err) => {
        this.isLoading = false;
        this.getMessageError(err);
      }
    );
  }

  get getFormEffectiveDateOld() {
    return this.form?.get('effectiveDateOld');
  }

  checkProposeCategory(){
    if (this.proposeCategory === Constants.ProposeCategory.GROUP) {
      return this.translate.instant('development.promulgate.table.commonDecision');
    } else {
      return this.translate.instant('development.promulgate.table.decision');
    }
  }

  get checkDisableDateSign() {
    const listState = ['10A1', '10A3', '11.1A', '11.1BA', '12A1', '11.2BA'];
    const check = listState.indexOf(this.params?.state!);
    let state;
    check === -1 ? (state = true) : (state = false);
    return state;
  }

  initForm() {
    this.form = this.fb.group({
      effectiveDate: [null],
      effectiveDateOld: [null],
      signedDate: [null],
      signerCode: [null],
      comment: [null],
      decisionNumber: [null],
    });
  }

  getState(code: string) {
    for (let i = 0; i < Constants.stateConfirmPromulgate.length; i++) {
      const found = Constants.stateConfirmPromulgate[i].list?.find((name) => name === code);
      if (found) {
        this.statePromulgate = Constants.stateConfirmPromulgate[i];
        break;
      }
    }
  }

  //folow const valiable confirm promulgate
  submit(typeState: string, action: boolean, confirmQues: string) {
    const body = {
      ...this.params,
      action: action,
      comment: this.comment,
      proposeDetailIds: this.proposeDetailIds,
    };
    //nếu trạng thái của lô gộp là chờ xác nhận thì mới required checkbox
    if (this.proposeDetailIds.length === 0
      && this.proposeCategory === Constants.ProposeCategory.GROUP
      && Constants.ConfirmButtonConfirmIssue.indexOf(this.statePublish) > -1) {
      this.toastrCustom.error(this.translate.instant('development.commonMessage.validateSelectedEmployee'));
      return;
    }
    if (this.proposeCategory === Constants.ProposeCategory.GROUP) {
      switch (confirmQues) {
        case Constants.ConfirmQuesConfirmIssue.CONFIRM:
          this.submitGroupName1 = Constants.ButtonConfirmIssueName1.CONFIRM;
          this.submitGroupName2 = Constants.ButtonConfirmIssueName2.CONFIRM;
          break;
        case Constants.ConfirmQuesConfirmIssue.CONFIRMCANCEL:
          this.submitGroupName1 = Constants.ButtonConfirmIssueName1.CONFIRMCANCEL;
          this.submitGroupName2 = Constants.ButtonConfirmIssueName2.CONFIRMCANCEL;
          break;
        case Constants.ConfirmQuesConfirmIssue.REJECTCONFIRM:
          this.submitGroupName1 = Constants.ButtonConfirmIssueName1.REJECTCONFIRM;
          this.submitGroupName2 = Constants.ButtonConfirmIssueName2.REJECTCONFIRM;
          break;
        case Constants.ConfirmQuesConfirmIssue.CANCELREJECTCONFIRM:
          this.submitGroupName1 = Constants.ButtonConfirmIssueName1.CANCELREJECTCONFIRM;
          this.submitGroupName2 = Constants.ButtonConfirmIssueName2.CANCELREJECTCONFIRM;
          break;
      }
      this.showModalSubmitGroup(typeState, action);
    } else {
      let api: Observable<any>;
      if (typeState === Constants.TypeStateConfirmIssue.CONFIRM) {
        if (this.isCheckDecision) {
          if (!body.comment && !body.action) {
            this.scrollToValidationSection('reasonDetail', 'development.commonMessage.approveIdeaRequiredPromulgate');
            return;
          }
        }
        api = this.service.confirmAndReject(body);
      } else if (typeState === Constants.TypeStateConfirmIssue.CANCEL) {
        if (this.isCheckDecision) {
          if (!body.comment && !body.action) {
            this.scrollToValidationSection('reasonDetail', 'development.commonMessage.approveIdeaRequiredPromulgate');
            return;
          }
        }
        api = this.service.cancelConfirmAndReject(body);
      }
      this.modalRef = this.modal.confirm({
        nzTitle: confirmQues,
        nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
        nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
        nzClassName: Constants.ClassName.LD_CONFIRM,
        nzOnOk: () => {
          this.isLoading = true;
          api.subscribe(
            () => {
              if (typeState === Constants.TypeStateConfirmIssue.CONFIRM) {
                action
                  ? this.toastrCustom.success(this.translate.instant('development.promulgate.section.confirmSuccess'))
                  : this.toastrCustom.success(this.translate.instant('development.promulgate.section.rejectConfirmSuccess'));
              } else if (typeState === Constants.TypeStateConfirmIssue.CANCEL) {
                action
                  ? this.toastrCustom.success(this.translate.instant('development.promulgate.section.cancelConfirmSuccess'))
                  : this.toastrCustom.success(
                      this.translate.instant('development.promulgate.section.rejectcancelConfirmSuccess')
                    );
              }
              this.router.navigateByUrl('/development/confirm-promulgate');
              this.isLoading = false;
            },
            (e: HttpErrorResponse) => {
              this.getMessageError(e);
              this.isLoading = false;
            }
          );
        },
      });
    }
  }

  downloadFile(id: number, name: string) {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;
    this.service.downloadFile(id, name).subscribe(
      () => {
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }



  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: '',
          field: 'select',
          width: 40,
          thClassList: [Constants.TABLE_CONFIG.TEXT_NOWRAP, Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [Constants.TABLE_CONFIG.TEXT_NOWRAP, Constants.TABLE_CONFIG.TEXT_CENTER],
          fixed: true,
          thTemplate: this.groupCheckboxHeader,
          tdTemplate: this.groupCheckbox,
          fixedDir: 'left',
          show: this.proposeCategory === Constants.ProposeCategory.GROUP
            && Constants.ConfirmButtonConfirmIssue.indexOf(this.statePublish) > -1,
        },
        {
          title: 'development.promulgate.search.form',
          field: 'formName',
          width: 110,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.proposed-unit.table.empCode',
          field: 'empCode',
          width: 110,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.proposed-unit.table.empName',
          field: 'fullName',
          width: 180,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.proposed-unit.detail.employee.joinCompanyDate',
          field: 'joinCompanyDate',
          width: 120,
          tdTemplate: this.joinDate
        },
        {
          title: 'development.proposed-unit.table.currentUnit',
          field: 'orgPathName',
          width: 200,
        },
        {
          title: 'development.proposed-unit.table.currentTitle',
          field: 'jobName',
          width: 150,
        },
        {
          title: 'development.proposed-unit.table.currentLevel',
          field: 'employeeLevel',
          width: 110,
        },
        {
          title: 'development.proposed-unit.table.proposeUnit',
          field: 'contentUnitPathName',
          width: 200,
        },
        {
          title: 'development.proposed-unit.table.proposeTitle',
          field: 'contentTitleName',
          width: 150,
        },
        {
          title: 'development.proposed-unit.table.proposeLevel',
          field: 'contentLevel',
          width: 110,
        },
        {
          title: 'development.proposed-unit.table.noteWithdraw',
          field: 'withdrawNoteName',
          width: 160,
        },
        {
          title: 'development.proposed-unit.table.statusPublish',
          field: 'statusPublishName',
          width: 160,
          show: this.proposeCategory === Constants.ProposeCategory.GROUP,
        },
        {
          title: 'development.promulgate.search.effectiveDate',
          field: 'issueEffectiveStartDate',
          width: 120,
          tdTemplate: this.effectiveDate
        },
        {
          title: 'development.promulgate.table.minimumTime',
          field: 'minimumTime',
          width: 110,
        },
        {
          title: 'development.proposed-unit.formRotation.maximumTimeTGCV',
          field: 'maximumTime',
          width: 150,
          tdTemplate: this.maximumTime,
        },
        {
          title: 'development.promulgate.table.endDate',
          field: 'issueEffectiveEndDate1',
          width: 120,
          tdTemplate: this.endDate
        },
        {
          title: 'development.promulgate.table.reportApprove',
          field: '',
          width: 150,
          tdTemplate: this.reportApprove
        },
        {
          title: '',
          field: '',
          width: 50,
          tdTemplate: this.ideaConfirmIssue,
          fixed: true,
          fixedDir: 'right',
          show: this.proposeCategory === Constants.ProposeCategory.GROUP && this.commentConfirmPublishItem > 0,
        },
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: userConfig.pageSize,
      pageIndex: 1,
    };
  }

  updateCheckedSet(item: DataEmployee, checked: boolean): void {
    if (item?.id){
      if (checked) {
        this.quantityMap.set(item.id, item);
      } else {
        this.quantityMap.delete(item.id);
      }
    }
    this.getListOfIDSelected();
  }

  onItemChecked(id: number, checked: boolean): void {
    const item = this.dataTable.find((itemTable: DataEmployee) => itemTable.id === id);
    if (item) {
      this.updateCheckedSet(item, checked);
    }
    this.refreshCheckedStatus();
  }

  onAllChecked(value: boolean): void {
    const list = this.dataTable.filter((item) => Constants.StatusCheckboxDetailConfirmIssue.indexOf(item.statusPublishCode!) > -1)
    list.forEach((item) => this.updateCheckedSet(item, value));
    this.refreshCheckedStatus();
  }

  refreshCheckedStatus(): void {
    this.checked = this.dataTable.every(({ id }) => this.quantityMap.has(id ? id : 0));
  }

  //download tờ trình bản duyệt
  downloadReportApprove(id: string, name: string) {
    this.isLoading = true;
    this.service.downloadFile(+id, name).subscribe(
      () => {
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

  showCheckboxEmployeeList(statusPublishCode: string) {
    return Constants.StatusCheckboxDetailConfirmIssue.indexOf(statusPublishCode) > -1;
  }

  initLogs() {
    this.tableLogs = {
      headers: [
        {
          title: 'development.proposed-unit.proposeLogsTable.time',
          field: 'actionDate',
          width: 160,
          tdTemplate: this.dateLog,
        },
        {
          title: 'development.proposed-unit.proposeLogsTable.personAction',
          field: 'userName',
          width: 150,
        },
        {
          title: 'development.proposed-unit.proposeLogsTable.action',
          field: 'action',
          width: 130,
        },
        {
          title: 'development.proposed-unit.proposeLogsTable.description',
          field: 'comment',
          width: 500,
        },
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: userConfig.pageSize,
      pageIndex: 1,
    };
  }

  getListOfIDSelected() {
    this.proposeDetailIds = [];
    this.quantityMap.forEach((values) => {
      if (values.id) {
        this.proposeDetailIds.push(values.id);
      }
    });
    return this.proposeDetailIds;
  }

  showCommentConfirmPublish(commentConfirmPublish: string) {
    this.modalRef = this.modal.create({
      nzTitle: this.translate.instant('development.promulgate.section.ideaConfirmIssue'),
      nzWidth: '30vw',
      nzContent: CommentPublishComponent,
      nzComponentParams: {
        commentConfirmPublish: commentConfirmPublish,
      },
      nzFooter: null,
    });
  }

  scrollToValidationSection(section: string, messsage: string){
    this.toastrCustom.error(this.translate.instant(messsage));
    let el = document.getElementById(section);
    el?.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
  }

  //disable ý kiến phê duyệt
  disableIdeaConfirm() {
    return Constants.StatusButtonConfirmIssue.indexOf(this.statusPublish) <= -1;
  }

  //hiển thị 2 nút xác nhận
  checkConfirmButton() {
    return Constants.ConfirmButtonConfirmIssue.indexOf(this.statusPublish) > -1;
  }

  //hiển thị 2 nút từ chối xác nhận
  checkRejectConfirmButton() {
    return Constants.RejectConfirmButtonConfirmIssue.indexOf(this.statusPublish) > -1;
  }

  showModalSubmitGroup(typeState: string, action: boolean) {
    this.modalRef = this.modal.create({
      nzTitle: this.translate.instant('development.confirmIssueGroup.header', {
        buttonName: this.submitGroupName1
      }),
      nzWidth: '50vw',
      nzContent: SubmitGroupComponent,
      nzComponentParams: {
        buttonName: this.submitGroupName2,
      },
      nzFooter: null,
    });
    this.modalRef.componentInstance.closeEvent.subscribe((data:boolean) => {
      if (data) {
        let api: Observable<any>;
        this.isLoading = true;
        this.modalRef?.componentInstance.ideaSubmit.subscribe((comment: string) => {
          const bodyGroup = {
            ...this.params,
            action: action,
            comment: comment,
            proposeDetailIds: this.proposeDetailIds,
          }
          if (typeState === Constants.TypeStateConfirmIssue.CONFIRM) {
            api = this.service.confirmAndReject(bodyGroup);
          } else if (typeState === Constants.TypeStateConfirmIssue.CANCEL) {
            api = this.service.cancelConfirmAndReject(bodyGroup);
          }
          const sub = api.subscribe(
            () => {
              if (typeState === Constants.TypeStateConfirmIssue.CONFIRM) {
                action
                  ? this.toastrCustom.success(this.translate.instant('development.promulgate.section.confirmSuccess'))
                  : this.toastrCustom.success(this.translate.instant('development.promulgate.section.rejectConfirmSuccess'));
              } else if (typeState === Constants.TypeStateConfirmIssue.CANCEL) {
                action
                  ? this.toastrCustom.success(this.translate.instant('development.promulgate.section.cancelConfirmSuccess'))
                  : this.toastrCustom.success(
                      this.translate.instant('development.promulgate.section.rejectcancelConfirmSuccess')
                    );
              }
              this.router.navigateByUrl('/development/confirm-promulgate');
              this.isLoading = false;
            },
            (e) => {
              this.getMessageError(e);
              this.isLoading = false;
            }
          );
          this.subs.push(sub);
          this.modalRef?.destroy();
        });
      } else {
        this.modalRef?.destroy();
      }
    });
  }

  expandLog() {
    this.isExpand = !this.isExpand;
    if (this.isExpand) {
      this.iconStatus = Constants.IconStatus.DOWN;
    } else {
      this.iconStatus = Constants.IconStatus.UP;
    }
  }

  // lấy log phân trang
  getLog(pageIndex: number){
    this.pageNumber = pageIndex - 1;
    this.isLoading = true;
    const log = this.ldService.getLogsPage(this.id, this.proposeTypeID, Constants.ScreenCode.GDNS, this.pageNumber, this.pageSize)
      .subscribe((log) => {
        this.proposeLogs = log?.data?.content || [];
        this.tableLogs.total = log?.data?.totalElements || 0;
        this.tableLogs.pageIndex = pageIndex;
        this.isLoading = false;
      }, (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      })
    this.subs.push(log);
  }
}
