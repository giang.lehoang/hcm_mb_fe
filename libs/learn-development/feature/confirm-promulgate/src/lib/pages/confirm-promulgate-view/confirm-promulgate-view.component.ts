import { Component, Injector, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Constants, userConfig, Utils } from "@hcm-mfe/learn-development/data-access/common";
import {
  DataCommon,
  DataTable, StatusList
} from "@hcm-mfe/learn-development/data-access/models";
import { ConfirmPromulgateService } from "@hcm-mfe/learn-development/data-access/services";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import { cleanData } from "@hcm-mfe/shared/common/utils";
import { MBTableConfig, Pageable, Pagination } from '@hcm-mfe/shared/data-access/models';
import { saveAs } from 'file-saver';
import * as moment from 'moment';
import { NzSafeAny } from "ng-zorro-antd/core/types";

@Component({
  selector: 'app-confirm-promulgate-view',
  templateUrl: './confirm-promulgate-view.component.html',
  styleUrls: ['./confirm-promulgate-view.component.scss']
})
export class ConfirmPromulgateViewComponent  extends BaseComponent implements OnInit {
  isExpand = false;
  iconStatus = Constants.IconStatus.DOWN;
  dataTable: DataTable[] = [];
  dataCommon: DataCommon = {};
  pageName?: string;
  tableConfig!: MBTableConfig;
  pagination = new Pagination();
  @ViewChild('tag', {static: true}) tag!: TemplateRef<NzSafeAny>;
  @ViewChild('effectiveDateTmpl', {static: true}) effectiveDate!: TemplateRef<NzSafeAny>;
  @ViewChild('releaseDate', {static: true}) releaseDate!: TemplateRef<NzSafeAny>;

  heightTable = { x: '100vw', y: '25em'};
  params = {
    proposeCode: null, //Mã đề xuất
    formCode: null, // loại đề xuất
    decisionNumber: null, // số quyêst định
    publishCode: null, // mã ban hành
    publishContent: null, // nội dung ban hành
    timeApprove: null, // thời gian phê duyệt
    releaseFromDate: null,
    releaseToDate: null,
    approveStartDate: null,
    approveEndDate: null,
    issueLevel:null, // cấp ban hành or cấp phê duyệt
    statusPublish: <string | null>null, // trạng thái ban hành
    employeeCode: null, // mã nhân viên
    employeeName: null, // họ tên
    effectiveDate: null, //ngày hiệu lực
    effectiveStartDate: null, //ngày hiệu lực
    effectiveEndDate: null, //ngày hiệu lực
    signerFullName: null, // người ký
    currentUnit: null, // đơn vị  hiện tại
    currentTitle: null, // chức danh hiện tại
    proposeUnit: null, // đơn vị đề xuất
    proposeTitle: null, // chức danh đề xuất
    // proposeOrigin: false, // Đề xuất gốc - Default
    // proposeItem: true, //	Đề xuất thành phần - Default
    // proposeGroup: true, //	Đề xuất gop - Default
    screenCode: 'HCM-LND-GDNS', // Default
    page: 0,
    size: userConfig.pageSize,
  };
  pageable: Pageable = {
    totalElements: 0,
    currentPage: 0,
    size: userConfig.pageSize,
  };
  processStatusPublish = Constants.ProcessStatusPublishDescription;
  constructor(injector: Injector, private readonly service: ConfirmPromulgateService) {
    super(injector);
  }

  displayStatusApprove(statusDisplay: string) {
    return Utils.getColorFlowStatusApporveOrPublish(statusDisplay);
  }

  ngOnInit() {
    this.initTable();
    this.pageName = this.route.snapshot?.data['pageName'];

    const params = {
      screenCode: 'HCM-LND-GDNS'
    }
    this.isLoading = true;
    this.service.getState(params).subscribe((data) => {
      if (data) {
        this.dataCommon = data;
        this.dataCommon.flowStatusPublishConfigList = this.convertStatus(data?.flowStatusPublishConfigList);
        if (this.dataCommon.flowStatusPublishConfigList){
          const listPendingApprove = this.dataCommon.flowStatusPublishConfigList.find(
            (item) => item.role === Constants.FlowCodeName.CHOXACNHAN);
          this.params.statusPublish = listPendingApprove?.key!;
        }
      //end gom trạng thái ban thành
      }
      this.search(1);
    });

  }

  convertStatus(valueStatus: StatusList[]) {
    const arrayStatus: StatusList [] = [];
    if (valueStatus) {
      const textStatusConsultation = [...new Set(valueStatus.map((item: StatusList) => { return  item.role}))];
      textStatusConsultation.forEach((item) =>{
        const filterValueStatus = valueStatus.filter((i) => i.role === item)?.map((a) => { return a.key});
        arrayStatus.push({key: filterValueStatus.toString(), role: item});
      })
    }
    return arrayStatus;
  }

  search(firstPage: number) {
    this.isLoading = true;
    this.params.page = firstPage - 1;
    const newParams = { ...cleanData(this.params) };
    newParams.currentUnit = newParams.currentUnit ? newParams.currentUnit.orgId : null;
    newParams.proposeUnit = newParams.proposeUnit ? newParams.proposeUnit.orgId : null;
    newParams.releaseFromDate =
      this.params.timeApprove && this.params.timeApprove[0]
        ? moment(newParams.timeApprove[0]).format(Constants.FORMAT_DATE.YYYY_MM_DD)
        : null;
    newParams.releaseToDate =
      this.params.timeApprove && this.params.timeApprove[1]
        ? moment(this.params.timeApprove[1]).format(Constants.FORMAT_DATE.YYYY_MM_DD)
        : null;
    newParams.effectiveStartDate =
      this.params.effectiveDate && this.params.effectiveDate[0]
        ? moment(newParams.effectiveDate[0]).format(Constants.FORMAT_DATE.YYYY_MM_DD)
        : null;
    newParams.effectiveEndDate =
      this.params.effectiveDate && this.params.effectiveDate[1]
        ? moment(this.params.effectiveDate[1]).format(Constants.FORMAT_DATE.YYYY_MM_DD)
        : null;
    newParams.effectiveDate = null;
    newParams.timeApprove = null;
    this.service.getAndSearch(newParams).subscribe(
      (res) => {
        this.dataTable = res.data?.datas?.content;
        // // thêm trường proposeCode: Mã đề xuất
        // this.dataTable.forEach((item) => {
        //   if (item.proposeCategory === 'ELEMENT') {
        //     item.proposeCode = item.proposeDetailId;
        //   }
        //   if (item.proposeCategory === 'GROUP' || item.proposeCategory === 'ORIGINAL') {
        //     item.proposeCode = item.proposeId;
        //   }
        // })
        this.tableConfig.total = res?.data?.datas?.totalElements || 0;
        this.tableConfig.pageIndex = firstPage;

        this.isLoading = false;
      },
      (e) => {
        this.isLoading = false;
        this.getMessageError(e)
        this.dataTable = [];
      }
    );
  }

  collapseExpand() {
    this.isExpand = !this.isExpand;
    if (this.isExpand) {
      this.iconStatus = Constants.IconStatus.UP;
    } else {
      this.iconStatus = Constants.IconStatus.DOWN;
    }
  }

  exportExcel() {
    if (this.dataTable?.length === 0) {
      this.toastrCustom.warning(
        this.translate.instant(
          'development.proposed-unit.messages.dataTableEmpty'
        )
      );
      return;
    }
    this.isLoading = true;
    const newParams = { ...cleanData(this.params) };
    delete newParams.page;
    delete newParams.size;
    newParams.currentUnit = newParams.currentUnit ? newParams.currentUnit.orgId : null;
    newParams.proposeUnit = newParams.proposeUnit ? newParams.proposeUnit.orgId : null;
    newParams.releaseFromDate =
      this.params.timeApprove && this.params.timeApprove[0]
        ? moment(newParams.timeApprove[0]).format(Constants.FORMAT_DATE.YYYY_MM_DD)
        : null;
    newParams.releaseToDate =
      this.params.timeApprove && this.params.timeApprove[1]
        ? moment(this.params.timeApprove[1]).format(Constants.FORMAT_DATE.YYYY_MM_DD)
        : null;
    newParams.effectiveStartDate =
      this.params.effectiveDate && this.params.effectiveDate[0]
        ? moment(newParams.effectiveDate[0]).format(Constants.FORMAT_DATE.YYYY_MM_DD)
        : null;
    newParams.effectiveEndDate =
      this.params.effectiveDate && this.params.effectiveDate[1]
        ? moment(this.params.effectiveDate[1]).format(Constants.FORMAT_DATE.YYYY_MM_DD)
        : null;
    newParams.effectiveDate = null;
    newParams.timeApprove = null;
    this.service.export(newParams).subscribe(
      (data) => {
        let urlExcel = 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,';
        urlExcel = urlExcel + data.data;
        saveAs(urlExcel, this.translate.instant('development.promulgate.section.fileConfirmPromulgate'));
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }


  showBtnAction(status: number){
    return Constants.StatusButton.includes(status.toString());
  }

  detail(item: DataTable) {
    let queryParams = {};
    let action = '';
    if (item.publishStatusCode) {
      if (Constants.ConfirmButtonConfirmIssue.indexOf(item.publishStatusCode) > -1) {
        action = Constants.ActionIssueScreen.XN;
      } else if (Constants.RejectConfirmButtonConfirmIssue.indexOf(item.publishStatusCode) > -1) {
        action = Constants.ActionIssueScreen.XNH;
      }
    }
    queryParams = item.proposeCategory === Constants.ProposeCategory.ELEMENT
      ? {
          id: item?.proposeDetailId,
          proposeCategory: item?.proposeCategory,
          state: item?.publishStatusCode,
          action: action,
        }
      : {
          id: item?.proposeId,
          proposeCategory: item?.proposeCategory,
          state: item?.publishStatusCode,
          action: action,
        }

    this.router.navigate([this.router.url, 'view'], {queryParams});
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'development.proposed-unit.reportCard.decisionNumber',
          field: 'decisionNumber',
          thClassList: [Constants.TABLE_CONFIG.TEXT_NOWRAP, Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [Constants.TABLE_CONFIG.TEXT_NOWRAP, Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 110,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.promulgate.search.issueCode',
          field: 'publishCode',
          width: 110,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.promulgate.table.proposeCode',
          field: 'proposeCode',
          thClassList: [Constants.TABLE_CONFIG.TEXT_NOWRAP, Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [Constants.TABLE_CONFIG.TEXT_NOWRAP, Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 100,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.promulgate.search.issueContent',
          field: 'publishContent',
          width: 150,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.approvement-issue.search.form',
          field: 'formRotationName',
          width: 140,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.promulgate.table.statusPublish',
          width: 150,
          field: 'tag',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_NOWRAP,Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_NOWRAP,Constants.TABLE_CONFIG.TEXT_CENTER],
          tdTemplate: this.tag,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.promulgate.table.empCode',
          field: 'employeeCode',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.promulgate.table.empName',
          field: 'employeeName',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
          fixed: true,
          fixedDir: 'left',

        },
        {
          title: 'development.promulgate.table.unit',
          field: 'employeeUnitPathName',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
        },
        {
          title: 'development.promulgate.table.title',
          field: 'employeeTitleName',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
        },
        {
          title: 'development.promulgate.table.proposeUnit',
          field: 'contentUnitPathName',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
        },
        {
          title: 'development.promulgate.table.proposeTitle',
          field: 'contentTitleName',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
        },
        {
          title: 'development.promulgate.search.effectiveDate',
          field: '',
          width: 150,
          tdTemplate: this.effectiveDate
        },
        {
          title: 'development.promulgate.table.issueLevel',
          field: 'issueLevelName',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
        },
        {
          title: 'development.promulgate.table.signerCode',
          field: 'signerFullName',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
        },
        {
          title: 'development.proposed-unit.table.noteWithdraw',
          field: 'withdrawNote',
          width: 200,
        },
        {
          title: 'development.promulgate.table.changeIssuePerson',
          field: 'createdBy',
          width: 150,
        },
        {
          title: 'development.promulgate.search.changeIssueDate',
          field: 'releaseDate',
          width: 150,
          tdTemplate: this.releaseDate
        }

      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1,
    };
  }



}
