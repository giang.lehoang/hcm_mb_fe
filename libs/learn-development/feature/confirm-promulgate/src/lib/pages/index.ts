import { ConfirmPromulgateActionComponent } from "./confirm-promulgate-action/confirm-promulgate-action.component";
import { ConfirmPromulgateViewComponent } from "./confirm-promulgate-view/confirm-promulgate-view.component";

export const pages = [ConfirmPromulgateViewComponent, ConfirmPromulgateActionComponent] ;

export * from "./confirm-promulgate-view/confirm-promulgate-view.component";
export * from  "./confirm-promulgate-action/confirm-promulgate-action.component";

