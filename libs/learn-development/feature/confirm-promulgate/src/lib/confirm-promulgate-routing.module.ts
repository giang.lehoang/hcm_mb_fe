import { Routes, RouterModule } from '@angular/router';
import {ScreenType} from "@hcm-mfe/learn-development/data-access/common";
import { ConfirmPromulgateActionComponent, ConfirmPromulgateViewComponent } from './pages';
import {NgModule} from "@angular/core";

const routes: Routes = [
  { path: '', component: ConfirmPromulgateViewComponent },
  {
    path: 'view',
    component: ConfirmPromulgateActionComponent,
    data: {
      pageName: 'development.pageName.confirm-promulgate-detail',
      breadcrumb: 'development.breadcrumb.confirm-promulgate-detail',
      screenType: ScreenType.Detail,
    },
  },
  {
    path: 'action',
    component: ConfirmPromulgateActionComponent,
    data: {
      pageName: 'development.pageName.confirm-promulgate-action',
      breadcrumb: 'development.breadcrumb.confirm-promulgate-action',
      screenType: ScreenType.Update,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfirmPromulgateRoutingModule {}

// export const ConfirmPromulgateRoutes = RouterModule.forChild(routes);
