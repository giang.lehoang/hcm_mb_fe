import { HttpResponse } from "@angular/common/http";
import { Component, Injector, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Constants, userConfig, Utils } from "@hcm-mfe/learn-development/data-access/common";
import { DataTempalte, Plan } from "@hcm-mfe/learn-development/data-access/models";
import { ListPlanService } from "@hcm-mfe/learn-development/data-access/services";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import { cleanData } from "@hcm-mfe/shared/common/utils";
import { MBTableConfig, Pageable } from "@hcm-mfe/shared/data-access/models";
import * as FileSaver from 'file-saver';
import * as moment from 'moment';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { forkJoin, Observable, of, Subscription } from 'rxjs';
import { catchError } from "rxjs/operators";

@Component({
  selector: 'app-list-plan-view',
  templateUrl: './list-plan-view.component.html',
  styleUrls: ['./list-plan-view.component.scss'],
})
export class ListPlanViewComponent extends BaseComponent implements OnInit {
  scrollY = '';
  isExpand = false;
  iconStatus = Constants.IconStatus.DOWN;
  dataTable: Plan[] = [];
  salaryStatus = Constants.SalaryStatus;
  screenCode = this.translate.instant('development.screenCode.PTNL');
  dataCommon: any = {
    typeDTOS: [],
    issueLevelList: [],
    consultationStatus: [],
    jobDTOS: [],
    interviewStatus: [],
    salaryStatus: [],
    flowStatusApproveConfigList: [],
  };
  title = '';
  params: any = {
    proposeType: null,
    proposeCode: null,
    sentDate: null,
    rangeDate: null,
    flowStatusCode: null,
    employeeCode: null,
    employeeName: null,
    issueLevelCode: null,
    approvalFullName: null,
    employeeUnits: null,
    employeeLines: null,
    employeeUnit: null,
    employeeTitleId: null,
    proposeUnit: null,
    proposeTitle: null,
    interviewStatus: null,
    page: 0,
    size: userConfig.pageSize,
    toDate: null,
    salaryStatus: null,
    consultationStatus: null,
  };

  inforParams = {
    screenCode: null,
  };

  pageable: Pageable = {
    totalElements: 0,
    currentPage: 0,
    size: userConfig.pageSize,
  };

  modalRef?: NzModalRef;
  quantityMapInterview = new Map<number, Plan>();
  quantityMapSalary = new Map<number, Plan>();
  checkedInterview = false;
  checkedSalary = false;
  indeterminateInterview = false;
  indeterminateSalary = false;
  disableSendInterview = true;
  disableSendSalary = true;
  keyConstansts = Constants.KeyConstants;
  preParam = this.params;
  tableConfig!: MBTableConfig;
  @ViewChild('groupInterviewCheckboxTmpl', {static: true}) groupInterviewCheckbox!: TemplateRef<NzSafeAny>;
  @ViewChild('groupCheckboxInterviewHeaderTmpl', {static: true}) groupCheckboxInterviewHeader!: TemplateRef<NzSafeAny>;
  @ViewChild('groupCheckboxSalaryHeaderTmpl', {static: true}) groupCheckboxSalaryHeader!: TemplateRef<NzSafeAny>;
  @ViewChild('groupSalaryCheckboxTmpl', {static: true}) groupSalaryCheckbox!: TemplateRef<NzSafeAny>;


  @ViewChild('flowStatusDescriptionTmpl', {static: true}) flowStatusDescription!: TemplateRef<NzSafeAny>;
  @ViewChild('interviewStatusDescriptionTmpl', {static: true}) interviewStatusDescription!: TemplateRef<NzSafeAny>;
  @ViewChild('consultationStatusDescriptionTmpl', {static: true}) consultationStatusDescription!: TemplateRef<NzSafeAny>;
  @ViewChild('salaryStatusDescriptionTmpl', {static: true}) salaryStatusDescription!: TemplateRef<NzSafeAny>;
  @ViewChild('actionPlanTmpl', {static: true}) actionPlan!: TemplateRef<NzSafeAny>;
  @ViewChild('statusPublishView', {static: true}) statusPublishView!: TemplateRef<NzSafeAny>;
  @ViewChild('sentDateTmpl', {static: true}) sentDate!: TemplateRef<NzSafeAny>;

  heightTable = { x: '100%', y: '25em'};
  flowCodeConstants = Constants.FlowCodeConstants;
  dataTemplateGet: DataTempalte[] = [];
  private readonly subs: Subscription[] = [];



  constructor(injector: Injector, private readonly service: ListPlanService) {
    super(injector);
  }

  checkSalaryStatus(status: string) {
    return Constants.SalaryStatus.find((item) => item.description === status)?.color;
  }

  checkConsulationStatus(status: string) {
    return Constants.ConsultationStatus.find((item) => item.description === status)?.color;
  }

  checkInterviewStatus(status: string) {
    return Constants.InterviewStatus.find((item) => item.description === status)?.color;
  }

  displayStatusApprove(statusDisplay: string) {
    return Utils.getColorFlowStatusApporveOrPublish(statusDisplay);
  }

  listCheckedInterview = [];
  listCheckedSalary = [];

  updateCheckedSetInterview(item: Plan, checked: boolean): void {
    if (checked) {
      this.quantityMapInterview.set(item.id!, item);
    } else {
      this.quantityMapInterview.delete(item.id!);
    }
  }

  updateCheckedSetSalary(item: Plan, checked: boolean) {
    if (checked) {
      this.quantityMapSalary.set(item.id!, item);
    } else {
      this.quantityMapSalary.delete(item.id!);
    }
  }

  onItemCheckedInterview(id: number, checked: boolean): void {
    const item = this.dataTable.find((i) => i.id === id);
    this.updateCheckedSetInterview(item!, checked);
    this.refreshCheckedStatusInterview();
    this.checkSendInterviewSalary();

  }

  onItemCheckedSalary(id: number, checked: boolean): void {
    const item = this.dataTable.find((i) => i.id === id);
    this.updateCheckedSetSalary(item!, checked);
    this.refreshCheckedStatusSalary();
    this.checkSendInterviewSalary();
  }

  returnInterviewCheck() {
    return this.dataTable.filter((i) => i.isInterviewed);
  }

  onAllCheckedInterview(checked: boolean) {
    const list = this.returnInterviewCheck();
    list.forEach((item) => this.updateCheckedSetInterview(item, checked));
    this.refreshCheckedStatusInterview();
    this.checkSendInterviewSalary();
  }

  returnSalaryCheck() {
    return this.dataTable.filter(
      (i) => i.salaryStatusDescription === this.keyConstansts.key2 && this.checkStatusCB(i.flowStatusCode)
    );
  }

  onAllCheckedSalary(checked: boolean) {
    const list = this.returnSalaryCheck();
    list.forEach((item) => this.updateCheckedSetSalary(item, checked));
    this.refreshCheckedStatusSalary();
    this.checkSendInterviewSalary();
  }

  refreshCheckedStatusInterview() {
    const list = this.returnInterviewCheck();
    this.checkedInterview = list.every((item) => this.quantityMapInterview.has(item.id!));
    this.indeterminateInterview = list.some((item) => this.quantityMapInterview.has(item.id!)) && !this.checkedInterview;
  }

  refreshCheckedStatusSalary() {
    const list = this.returnSalaryCheck();
    this.checkedSalary = list.every((item) => this.quantityMapSalary.has(item.id!));
    this.indeterminateSalary = list.some((item) => this.quantityMapSalary.has(item.id!)) && !this.checkedSalary;
  }

  ngOnInit() {
    this.initTable();
    this.isLoading = true;
    this.title = this.route.snapshot?.data['pageName'];
    const newParams = { ...cleanData(this.inforParams) };
    newParams.screenCode = this.screenCode;
    const sub = this.service.getState(newParams).subscribe(
      (res) => {
        if (res) {
          this.dataCommon = res;
          this.dataCommon.flowStatusApproveConfigList = this.convertStatus(res?.flowStatusApproveConfigList);
        }
        this.search(1);
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }

  convertStatus(valueStatus: any[]) {
    let arrayStatus: any[] = [];
    if (valueStatus) {
      const textStatusConsultation = [
        ...new Set(
          valueStatus.map((item) => {
            return item.role;
          })
        ),
      ];
      textStatusConsultation.forEach((item) => {
        let filterValueStatus = valueStatus
          .filter((i) => i.role === item)
          ?.map((a) => {
            return a.key;
          });
        arrayStatus.push({ key: filterValueStatus, role: item });
      });
    }
    return arrayStatus;
  }

  search(pageIndex: number) {
    this.isLoading = true;
    this.params.page = pageIndex - 1;
    const newParams = { ...this.params };
    newParams.employeeUnit = newParams.employeeUnit ? newParams.employeeUnit.orgId : null;
    newParams.proposeUnit = newParams.proposeUnit ? newParams.proposeUnit.orgId : null;
    newParams.sentDate =
      this.params.rangeDate && this.params.rangeDate[0] ? moment(newParams.rangeDate[0]).format(Constants.FORMAT_DATE.DD_MM_YYYY) : null;
    newParams.toDate =
      this.params.rangeDate && this.params.rangeDate[1] ? moment(newParams.rangeDate[1]).format(Constants.FORMAT_DATE.DD_MM_YYYY) : null;

    const sub = forkJoin([
      this.service.getList(newParams).pipe(catchError(() => of(undefined))),
      this.service.getExportTemplateListPlan(newParams).pipe(catchError(() => of(undefined))),
      ]).subscribe(
      ([res , listFile]) => {
        this.dataTable = res.data.datas.content || [];
        this.tableConfig.total = res?.data?.datas.totalElements || 0;
        this.tableConfig.pageIndex = pageIndex;

        this.preParam = newParams;
        this.isLoading = false;
        this.dataTemplateGet = listFile?.data;
      },
      (e) => {
        this.isLoading = false;
        this.getMessageError(e);
        this.dataTable = [];
      }
    );
    this.subs.push(sub);
  }

  collapseExpand() {
    this.isExpand = !this.isExpand;
    this.isExpand ? (this.iconStatus = Constants.IconStatus.UP) : (this.iconStatus = Constants.IconStatus.DOWN);
    setTimeout(() => {
      this.scrollY = Utils.setScrollYTable('table', this.pageable.totalElements! > 15 ? Constants.HeightPaginator : 0);
    }, 100);
  }

  handlePlan(id: number, proposeDetailId: number) {
    this.isLoading = true;
    const item = this.dataTable.find((i) => i.id === id);
    this.service.handlePlan(proposeDetailId).subscribe(
      (res) => {
        this.router.navigate(['development/propose/propose-list', 'handle'], {
          queryParams: {
            screenCode: this.screenCode,
            id: item?.proposeId,
            proposeDetailId: item?.proposeDetailId,
            proposeCategory: 'ELEMENT',
          },
        });
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

  detailPlan(item: Plan) {
    this.router.navigate([Constants.KeyCommons.DEVELOPMENT_PROPOSE_LIST, 'detail'], {
      queryParams: {
        screenCode: this.screenCode,
        id: item?.proposeId,
        proposeDetailId: item?.proposeDetailId,
        proposeCategory: Constants.ProposeCategory.ELEMENT,
        flowStatusCode: item.flowStatusCode,
        withdrawNoteCode: item.withdrawNoteCode,
        relationType: item?.relationType && +item?.relationType === Constants.RelationType.ONE,
      },
      state: {backUrl: this.router.url},
      skipLocationChange: true,
    });
  }

  exportCommon(fileName: string) {
    if (this.dataTable.length === 0) {
      this.toastrCustom.warning(this.translate.instant('development.proposed-unit.messages.dataTableEmpty'));
      return;
    }
    this.isLoading = true;
    let api: Observable<any>;
    if (fileName === 'fileNameInterview') {
      api = this.service.exportExcelInterview(this.preParam);
    } else if (fileName === 'fileNameSalary') {
      api = this.service.exportExcelSalary(this.preParam);
    } else if (fileName === 'fileNameConsultationCV') {
      api = this.service.exportExcelConsultationCV(this.preParam);
    } else if (fileName === 'fileNameOption') {
      api = this.service.exportExcelOption(this.preParam);
    }
    api!.subscribe(
      (data) => {
        let urlExcel = 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,';
        urlExcel = urlExcel + data.data;
        FileSaver.saveAs(urlExcel, this.translate.instant(`development.list-plan.search.${fileName}`));
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

  checkSendInterviewSalary() {
    this.disableSendInterview = this.quantityMapInterview.size <= 0;
    this.disableSendSalary = this.quantityMapSalary.size <= 0;
  }

  sendInterviewOrSalary() {
    if (this.quantityMapInterview.size === 0  && this.quantityMapSalary.size === 0) {
      this.toastrCustom.warning(this.translate.instant('development.salary-list.messages.greaterThanOneFile'));
    } else if (this.quantityMapInterview.size > 0 && this.quantityMapSalary.size === 0) {
      const itemInterview: number[] = [];
      const body = {
        ids: itemInterview,
      }
      this.quantityMapInterview.forEach((item) => {
        if (item.id) {
          itemInterview.push(item.id);
        }
      });
      this.modalRef = this.modal.confirm({
        nzTitle: this.translate.instant('development.list-plan.messages.sendInterview'),
        nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
        nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
        nzClassName: Constants.ClassName.LD_CONFIRM,
        nzOnOk: () => {
          this.isLoading = true;
          const sub = this.service.sendInterview(body).subscribe(
            () => {
              this.toastrCustom.success(this.translate.instant('development.list-plan.messages.sendInterviewSuccess'));
              this.search(1);
              this.refreshCheckedStatusInterview();
              this.quantityMapInterview.clear();
            },
            (e) => {
              this.getMessageError(e);
              this.isLoading = false;
            }
          );
          this.subs.push(sub);
        },
      });
    } else if (this.quantityMapSalary.size > 0 && this.quantityMapInterview.size === 0) {
      this.disableSendSalary = false;
      const itemSalary: number[] = [];
      this.quantityMapSalary.forEach((item) => {
        itemSalary.push(item.id!);
      });
      this.modalRef = this.modal.confirm({
        nzTitle: this.translate.instant('development.list-plan.messages.sendSalary'),
        nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
        nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
        nzClassName: Constants.ClassName.LD_CONFIRM,
        nzOnOk: () => {
          this.isLoading = true;
          const sub = this.service.sendSalary(itemSalary).subscribe(
            () => {
              this.toastrCustom.success(this.translate.instant('development.list-plan.messages.sendSalarySuccess'));
              this.search(1);
              this.refreshCheckedStatusSalary();
              this.quantityMapSalary.clear();
            },
            (e) => {
              this.getMessageError(e);
              this.isLoading = false;
            }
          );
          this.subs.push(sub);
        },
      });
    } else if (this.quantityMapSalary.size > 0 && this.quantityMapInterview.size > 0) {
      const itemSalaryNew: number[] = [];
      const itemInterviewNew: number[] = [];
      const bodyInterview = {
        ids: itemInterviewNew,
      }
      this.quantityMapSalary.forEach((item) => {
        if (item.id) {
          itemSalaryNew.push(item.id);
        }
      });
      this.quantityMapInterview.forEach((item) => {
        if (item.id) {
          itemInterviewNew.push(item.id);
        }
      });
      this.modalRef = this.modal.confirm({
        nzTitle: this.translate.instant('development.list-plan.messages.sendSalaryAndInterview'),
        nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
        nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
        nzClassName: Constants.ClassName.LD_CONFIRM,
        nzOnOk: () => {
          this.isLoading = true;
          const sub = forkJoin([
            this.service.sendSalary(itemSalaryNew),
            this.service.sendInterview(bodyInterview),
          ]).subscribe(
            () => {
              this.toastrCustom.success(this.translate.instant('development.list-plan.messages.sendSalaryAndInterviewSuccess'));
              this.search(1);
              this.refreshCheckedStatusSalary();
              this.refreshCheckedStatusInterview();
              this.quantityMapSalary.clear();
              this.quantityMapInterview.clear();
            },
            (e) => {
              this.getMessageError(e);
              this.isLoading = false;
            }
          );
          this.subs.push(sub);
        },
      });
    }
  }

  sendSalary() {
    if (this.quantityMapSalary.size === 0) {
      this.toastrCustom.warning(this.translate.instant('development.salary-list.messages.greaterThanOneFile'));
    } else {
      this.disableSendSalary = false;
      let itemSalary: number[] = [];
      this.quantityMapSalary.forEach((item) => {
        itemSalary.push(item.id!);
      });
      this.modalRef = this.modal.confirm({
        nzTitle: this.translate.instant('development.list-plan.messages.sendSalary'),
        nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
        nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
        nzClassName: Constants.ClassName.LD_CONFIRM,
        nzOnOk: () => {
          this.isLoading = true;
          this.service.sendSalary(itemSalary).subscribe(
            () => {
              this.toastrCustom.success(this.translate.instant('development.list-plan.messages.sendSalarySuccess'));
              this.search(1);
              this.refreshCheckedStatusSalary();
              this.quantityMapSalary.clear();
            },
            (e) => {
              this.getMessageError(e);
              this.isLoading = false;
            }
          );
        },
      });
    }
  }

  checkStatusCB(status: string) {
    return Constants.StatusCB.indexOf(status) > -1;
  }

  exportTemplateListPlan () {
    this.isLoading = true;
    if(this.dataTemplateGet) {
      const dataEx = this.dataTemplateGet[0];
      const dataDc = this.dataTemplateGet[1];
        const sub = forkJoin([
          this.service.exportTemplate(dataEx),
          this.service.exportTemplate(dataDc)
        ])
          .subscribe(([data, datafile]) => {
              this.convertFile(data)
              this.convertFile(datafile)
              this.isLoading = false;
            },
            (e) => {
              this.getMessageError(e);
              this.isLoading = false;
            }
          );
        this.subs.push(sub)

    }
  }

  convertFile(data: HttpResponse<Blob>){
    const contentDisposition = data?.headers.get('content-disposition');
    const filename = contentDisposition?.split(';')[1]?.split('filename')[1]?.split('=')[1]?.trim();
    if(data?.body) {
      const blob = new Blob([data?.body], {
        type: 'application/octet-stream',
      });
      FileSaver.saveAs(blob, filename);
    }
  }

  override ngOnDestroy() {
    this.subs.forEach((s) => s.unsubscribe());
    this.modalRef?.destroy();
  }



  initTable() {
    this.tableConfig = {
      headers: [
        {
          title: 'development.list-plan.table.2',
          field: 'select',
          width: 60,
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          fixed: true,
          thTemplate: this.groupCheckboxInterviewHeader,
          tdTemplate: this.groupInterviewCheckbox,
          fixedDir: 'left',
        },
        {
          title: 'development.list-plan.table.3',
          field: 'select',
          width: 60,
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          fixed: true,
          thTemplate: this.groupCheckboxSalaryHeader,
          tdTemplate: this.groupSalaryCheckbox,
          fixedDir: 'left',
        },
        {
          title: 'development.list-plan.table.20',
          field: 'formName',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 130,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.list-plan.search.planCode',
          field: 'optionCode',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 100,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.list-plan.table.8',
          field: 'employeeCode',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 120,
        },
        {
          title: 'development.list-plan.table.9',
          field: 'employeeName',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 180,
        },
        {
          title: 'development.list-plan.table.11',
          field: 'formRotationName',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 130,
        },
        {
          title: 'development.list-plan.table.16',
          field: 'employeeUnitPathName',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 200,
        },
        {
          title: 'development.list-plan.table.17',
          field: 'employeeTitleName',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 200,
        },
        {
          title: 'development.list-plan.table.proposeUnit',
          field: 'proposeUnitPathName',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 200,
        },
        {
          title: 'development.list-plan.table.proposeTitle',
          field: 'proposeTitleName',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 200,
        },

        {
          title: 'development.list-plan.table.5',
          field: 'flowStatusDescription',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          tdTemplate: this.flowStatusDescription,
          width: 170,
        },
        {
          title: 'development.list-plan.table.21',
          field: 'withdrawNoteValue',
          width: 170,
        },
        {
          title: 'development.list-plan.table.6',
          field: 'sender',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 110,
        },
        {
          title: 'development.list-plan.table.7',
          field: 'sentDate',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 130,
          tdTemplate: this.sentDate,
        },
        {
          title: 'development.list-plan.table.14',
          field: 'issueLevelName',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 130,
        },
        {
          title: 'development.list-plan.table.15',
          field: 'approvalName',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 180,
        },
        {
          title: 'development.list-plan.table.22',
          field: 'signerName',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 180,
        },
        {
          title: 'development.list-plan.search.statusInterview',
          field: 'interviewStatusDescription',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          tdTemplate: this.interviewStatusDescription,
          width: 180,
        },
        {
          title: 'development.list-plan.search.consultationStatus',
          field: 'consultationStatusDescription',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 180,
          tdTemplate: this.consultationStatusDescription,

        },
        {
          title: 'development.list-plan.table.23',
          field: 'salaryStatusDescription',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 180,
          tdTemplate: this.salaryStatusDescription,
        },
        {
          title: 'development.proposed-unit.table.statusPublish',
          field: 'statusPublish',
          width: 170,
          tdTemplate: this.statusPublishView,
        },

        {
          title: ' ',
          field: 'action',
          width: 100,
          tdTemplate: this.actionPlan,
          fixed: window.innerWidth > 1024,
          fixedDir: 'right',
        }


      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: userConfig.pageSize,
      pageIndex: 1,
    };
  }



}
