import { Routes, RouterModule } from '@angular/router';
import {ListPlanViewComponent} from "./list-plan-view/list-plan-view.component";
import {NgModule} from "@angular/core";


const routes: Routes = [
  { path: '', component: ListPlanViewComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class listPlanRoutingModule {}
