import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import {LearnDevelopmentFeatureShellModule} from "@hcm-mfe/learn-development/feature/shell";
import {ListPlanViewComponent} from "./list-plan-view/list-plan-view.component";
import {listPlanRoutingModule} from "./list-plan.routing.module";
import {PopupService} from "@hcm-mfe/shared/ui/popup";

@NgModule({
  declarations: [ListPlanViewComponent],
  exports: [ListPlanViewComponent],
  imports: [CommonModule, LearnDevelopmentFeatureShellModule, listPlanRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
  providers: [PopupService]
})
export class LearnDevelopmentFeatureListPlanModule {}
