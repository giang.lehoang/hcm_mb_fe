import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {pages, RecommendIndividualUpdateComponent} from "./pages";
import {LearnDevelopmentFeatureShellModule} from "@hcm-mfe/learn-development/feature/shell";
import {PopupService} from "@hcm-mfe/shared/ui/popup";
import {RecommendIndividualRoutingModule} from "./recommend-individual-routing.module";
import {CommonModule} from "@angular/common";

@NgModule({
    declarations: [...pages],
    imports: [RecommendIndividualRoutingModule,
        LearnDevelopmentFeatureShellModule, CommonModule],
    schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    providers: [PopupService],
    exports: [
        RecommendIndividualUpdateComponent
    ]
})
export class LearnDevelopmentFeatureRecommendIndividualModule {}
