import {RecommendIndividualAddComponent} from "./recommend-individual-add/recommend-individual-add.component";
import {RecommendIndividualUpdateComponent} from "./recommend-individual-update/recommend-individual-update.component";
export const pages = [RecommendIndividualAddComponent, RecommendIndividualUpdateComponent];
export * from './recommend-individual-add/recommend-individual-add.component';
export * from './recommend-individual-update/recommend-individual-update.component';
