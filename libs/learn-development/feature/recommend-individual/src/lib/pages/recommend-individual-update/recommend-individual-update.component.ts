import {Component, Injector, Input, OnInit, TemplateRef, ViewChild} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {
  Constants,
  maxInt32,
  ScreenType,
  SessionKey,
  userConfig,
  Utils
} from "@hcm-mfe/learn-development/data-access/common";
import {
  AttachFile,
  ConditionViolationList,
  EmployeeRecommendIndividual, FileDTOList,
  FormGroupList,
  FormGroupMultipleList,
  JobDTOS,
  ListFileTemplate,
  ListTitle,
  OrgItem,
  ParamSearchPosition, ProposedError,
  ProposeStatementDTO,
  RecommendIndividualComment,
  RecommendIndividualModel, ReturnPropose,
  StatusList,
  WithdrawNoteConfigList, WithdrawPropose
} from '@hcm-mfe/learn-development/data-access/models';
import { ProposedUnitService, ProposeMultipleEmployeeService } from "@hcm-mfe/learn-development/data-access/services";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import * as FileSaver from 'file-saver';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { catchError, forkJoin, of, Subscription } from 'rxjs';
import {MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {NzUploadFile} from "ng-zorro-antd/upload";
import {ReturnProposeComponent, WithdrawProposeComponent} from "@hcm-mfe/learn-development/feature/propose-unit";
import {User} from "@hcm-mfe/system/data-access/models";
import {cleanDataForm} from "@hcm-mfe/shared/common/utils";
import {NavigationExtras} from "@angular/router";


@Component({
  selector: 'app-recommend-individual-update',
  templateUrl: './recommend-individual-update.component.html',
  styleUrls: ['./recommend-individual-update.component.scss'],
})
export class RecommendIndividualUpdateComponent extends BaseComponent implements OnInit {

  // thông tin nhân viên
  employeeForm = this.fb.group({
    concurrentTitleId: null,
    concurrentTitleName: null,
    concurrentUnitId: null,
    concurrentUnitName: null,
    concurrentUnitPathId: null,
    concurrentUnitPathName: null,
    concurrentTitleId2: null,
    concurrentTitleName2: null,
    concurrentUnitId2: null,
    concurrentUnitName2: null,
    concurrentUnitPathId2: null,
    concurrentUnitPathName2: null,
    empIdp: null,
    orgId: null,
    empId: null,
    empCode: null,
    fullName: null,
    email: null,
    posId: null,
    posName: null,
    jobId: null,
    jobName: null,
    orgPathId: null,
    orgPathName: null,
    orgName: null,
    majorLevelId: null,
    majorLevelName: null,
    faculityId: null,
    faculityName: null,
    schoolId: null,
    schoolName: null,
    positionLevel: null,
    gender: null,
    joinCompanyDate: null,
    dateOfBirth: null,
    partyDate: null,
    partyOfficialDate: null,
    posSeniority: null,
    seniority: null,
    qualificationEnglish: null,
    facultyName: null,
    semesterGradeT1: null,
    semesterRankT1: null,
    semesterGradeT2: null,
    semesterRankT2: null,
    ratedCapacity: null,
    nearestDiscipline: null,
    endOfDisciplinaryDuration: null,
    userToken: null,
    treeLevel: null,
    posSeniorityName: null,
    seniorityName: null,
    levelName: null,
    formGroup: null,
    periodNameT1: null,
    periodNameT2: null,
  });

  isExpand = false;
  isSeeMore = false;
  modalRef?: NzModalRef;
  typeAction?: ScreenType;
  proposeUnitApprove = false;
  screenCodeConst = Constants.ScreenCode;

  dataCommon = {
    jobDTOS: [], // vị trí
    formGroupList: <FormGroupList[]>[],
    formGroupMultipleList: <FormGroupMultipleList[]>[],
    conditionViolationList: <ConditionViolationList[]>[],
  };
  formGroupConstants = Constants.FormGroupList;
  screenCode = Constants.ScreenCode.HRDV;
  proposeId = 0;
  proposeUnitForm = this.fb.group({
    id: null,
    formGroup: null,
    flowStatusCode: null,
    externalApprove: false,
    sentDate: null,
  });


  private readonly subs: Subscription[] = [];
  formReportElement = new FormGroup({
    statementNumber: new FormControl(null, [Validators.required]),
    submissionDate: new FormControl(null, [Validators.required]),
    idea: new FormControl(null, [Validators.required]),
  });

  formReportElementEdit = new FormGroup({
    statementNumber: new FormControl(null, [Validators.required]),
    submissionDate: new FormControl(null, [Validators.required]),
    idea: new FormControl(null, [Validators.required]),
  });

  isSubmitFormReportElement = false;
  isSubmitFormReportElementEdit = false;

  isSubmitFormSuggestionsPersonal = false;
  externalApprove = false;

  formSuggestionsPersonal = new FormGroup({
    proposeUnit: new FormControl(null, [Validators.required]),
    proposeTitle: new FormControl(null, [Validators.required]),
    proposeReason: new FormControl(null, [Validators.required]),
    commitment: new FormControl(null, [Validators.required]),
  });

  proposeDetailId = 0;
  jobDTOS: JobDTOS[] = [];
  checkDisplay: string | number | undefined;
  id: number | undefined;
  checkDisplayScreen = false;
  checkScreen = false;
  dataTableEmp: EmployeeRecommendIndividual | undefined;
  dataTableEmpDetail: EmployeeRecommendIndividual | undefined;
  dataproposeComment: RecommendIndividualComment | undefined;
  dataproposeStatement: ProposeStatementDTO[] = [];
  dataproposeStatementEdit: ProposeStatementDTO | undefined;
  conditionViolations = [];
  violationCode = 0;
  checkDisplayCode: number | undefined;
  checkRouterScreen: number | undefined;
  formGroupDetail ='';
  formGroupNameDetail: string | undefined;
  approveNoteDetail: string | undefined;
  approveNoteDetailEdit: string | undefined;
  iconStatus = Constants.IconStatus.UP;
  heightTable = { x: '50vw', y: '16em'};
  tableConfig!: MBTableConfig;
  pagination = new Pagination();
  @ViewChild('dateLog', {static: true}) dateLog!: TemplateRef<NzSafeAny>;
  proposeLogs = [];
  idDetail: number | undefined;
  contentUnitNameData = '';
  contentTitleNameData = '';
  formGroup: number | undefined;
  checkBtn = false;
  fileListSelectTTElement: NzUploadFile[] = [];
  fileListSelectTTElementEdit: NzUploadFile[] = [];
  fileElement: AttachFile = {};
  fileElementEdit: AttachFile = {};
  idAction: number | undefined;
  contentUnit?: OrgItem | null;
  flowStatusCodeDisplay = '';
  flowStatusCode = '';
  createdBy = '';
  statusPublishCodeDisplay = '';
  flowStatusName = '';
  statusPublishName = '';
  withdrawNoteName = '';
  ConstantsFormGroup = Constants.FormGroupList
  ConstantsFlowStatusCode = Constants.FlowCodeConstants
  functionCode: string;
  rolesOfCurrentUser: string[] = [];
  sentDate: Date | undefined;
  idCreate = 0;
  note = '';
  flowCodeConst = Constants.FlowCodeConstants;
  withdrawNote = '';
  checkWithdrawType: number | undefined;
  withdrawReason = '';
  comment = '';
  screenCodeRouting = '';
  currentUser: User | undefined;
  constantsVoditionsCode = Constants.ViolationCode
  @Input() personalCode = '';
  proposeCategoryView = '';
  proposeIdDetailView = 0;
  stateUrl: NavigationExtras | undefined;
  pageNumber = 0;
  pageSize =  userConfig.pageSize;
  personalCodeDetail = '';
  isAcceptPropose = false;
  // trừờng check ô ý kiến lãnh đạo của tờ trình đơn vị
  hasAgreeUnit = false;

  constructor(injector: Injector,
              private readonly service: ProposedUnitService,
              private readonly proposeMultipleService: ProposeMultipleEmployeeService

  ) {
    super(injector);
    this.typeAction = this.route.snapshot.data['screenType'];
    // this.formGroup = +this.route.snapshot?.queryParams['formGroup'];
    this.checkScreen = this.route.snapshot?.queryParams['checkFormGroup'];
    if(!this.route.snapshot?.queryParams['idCreate']) {
      this.idCreate = this.route.snapshot?.queryParams['id']
    } else {
      this.idCreate = this.route.snapshot?.queryParams['idCreate']
    }
    this.checkDisplayCode = this.route.snapshot?.queryParams['checkRouterScreen'];
    this.screenCodeRouting = this.route.snapshot?.queryParams['screenCode'];
    //lấy proposeCategory và proposeDetailId từ màn danh sách
    this.proposeCategoryView = this.route.snapshot?.queryParams['proposeCategory'];
    this.proposeIdDetailView = this.route.snapshot?.queryParams['proposeDetailId'];
    this.functionCode = this.route.snapshot.data['code'];
    this.stateUrl = this.router?.getCurrentNavigation()?.extras;
  }




  ngOnInit() {
    if (localStorage.getItem(SessionKey.CURRENCY_USER)) {
      this.currentUser = JSON.parse(localStorage.getItem(SessionKey.CURRENCY_USER) ?? '');
    }
    this.proposeUnitForm.controls['formGroup']?.setValue(this.formGroup);
    this.initTable();
    this.isLoading = true;
    this.checkDisplayScreen = true;
    const proposeCategory = Constants.ProposeCategory.ORIGINAL
    const screenCode = this.screenCodeRouting ? this.screenCodeRouting : this.screenCode
    const sub = forkJoin(
      [
        this.service.getState({screenCode: this.screenCode}).pipe(catchError(() => of(undefined))),
        this.proposeMultipleService.getProposeId().pipe(catchError(() => of(undefined))),
        //check nếu là thành phần thì call viewDetail, còn lại thì call proposeids
        this.proposeCategoryView === Constants.ProposeCategory.ELEMENT
          ? this.service.getDetailElementRecommendIndividual(this.proposeIdDetailView, screenCode).pipe(catchError(() => of(undefined)))
          : this.service.getDetailRecommendIndividual(this.idCreate, screenCode).pipe(catchError(() => of(undefined))),
        this.service.getRolesByUsername().pipe(catchError(() => of(undefined))),
        this.service.getLogsPage(this.idCreate, Constants.ProposeCategory.ORIGINAL, this.screenCode, this.pageNumber, this.pageSize)
          .pipe(catchError(() => of(undefined))),
      ]
    ).subscribe(
      ([state, result, res, roles, log]) => {
        this.isLoading = false;
        this.proposeId = result?.data;
        this.rolesOfCurrentUser = roles;
        this.proposeUnitForm.get('id')?.setValue(this.proposeId);
        if (state) {
          this.dataCommon = state;
        }
        // this.checkDisplayCode = display?.data?.checkDisplay;
        this.dataTableEmpDetail = res?.data?.proposeDTO[0]?.proposeDetails[0]?.employee;
        this.dataproposeComment = res?.data?.proposeDTO[0]?.proposeDetails[0]?.proposeComment;
        this.conditionViolations = res?.data?.proposeDTO[0]?.proposeDetails[0]?.conditionViolations;
        this.violationCode = res?.data?.proposeDTO[0]?.proposeDetails[0]?.conditionViolationCode;
        this.idDetail = res?.data?.proposeDTO[0]?.proposeDetails[0]?.id;
        this.flowStatusCode = res?.data?.proposeDTO[0]?.flowStatusCode;
        this.personalCodeDetail = res?.data?.proposeDTO[0]?.personalCode;
        this.createdBy = res?.data?.proposeDTO[0]?.createdBy
        this.flowStatusCodeDisplay = res?.data?.proposeDTO[0]?.flowStatusCodeDisplay;
        this.checkWithdrawType = res?.data?.proposeDTO[0]?.withdrawType;
        this.withdrawNote = res?.data?.proposeDTO[0]?.proposeDetails[0]?.withdrawNote;
        this.hasAgreeUnit = res?.data?.proposeDTO[0]?.proposeDetails[0]?.hasAgreeUnit;
        this.statusPublishCodeDisplay = res?.data?.proposeDTO[0]?.flowStatusPublishCode;
        this.withdrawReason = res?.data?.proposeDTO[0]?.withdrawReason;
        this.dataproposeStatement = res?.data?.proposeDTO[0]?.proposeStatementList?.find((item: ProposeStatementDTO) => !item?.typeStatement);
        this.dataproposeStatementEdit = res?.data?.proposeDTO[0]?.proposeDetails[0]?.proposeStatement?.find((item: ProposeStatementDTO) => item.typeStatement === Constants.EmployeeInfoMultipleFlowStatus.flowStatus2)
        this.externalApprove = res?.data?.proposeDTO[0]?.externalApprove;
        this.id = res?.data?.proposeDTO[0]?.id;
        this.sentDate = res?.data?.proposeDTO[0]?.sentDate;
        this.approveNoteDetail = res?.data?.proposeDTO[0]?.approveNote;
        this.approveNoteDetailEdit = res?.data?.proposeDTO[0]?.proposeDetails[0]?.approveNote;
        this.formReportElement.controls['idea']?.setValue(this.approveNoteDetail)
        this.formReportElementEdit.controls['idea']?.setValue(this.approveNoteDetailEdit)
        this.formGroupDetail = res?.data?.proposeDTO[0]?.formGroup;
        const flowCodeManyObject = res?.data?.flowStatusApproveConfigList?.find(
          (item: WithdrawNoteConfigList) => item.key === this.flowStatusCode
        );
        this.flowStatusName = flowCodeManyObject?.role
        if(this.formGroupDetail){
          this.formGroupNameDetail = this.dataCommon.formGroupList.find((item) => item?.id === +this.formGroupDetail)?.name
        }
        if (this.dataTableEmpDetail) {
          if (this.dataTableEmpDetail.posSeniority) {
            const years = Math.floor(
              this.dataTableEmpDetail.posSeniority / 12
            );
            const months = Math.floor(
              this.dataTableEmpDetail.posSeniority % 12
            );
            if (months === 0) {
              this.dataTableEmpDetail.posSeniorityName =
                years < 1 ? `` : `${years} năm`;
            } else {
              this.dataTableEmpDetail.posSeniorityName =
                years < 1
                  ? `${months} tháng`
                  : `${years} năm ${months} tháng`;
            }
          }

          if (this.dataTableEmpDetail.seniority) {
            const years = Math.floor(
              this.dataTableEmpDetail.seniority / 12
            );
            const months = Math.floor(
              this.dataTableEmpDetail.seniority % 12
            );
            if (months === 0) {
              this.dataTableEmpDetail.seniorityName =
                years < 1 ? `` : `${years} năm`;
            } else {
              this.dataTableEmpDetail.seniorityName =
                years < 1
                  ? `${months} tháng`
                  : `${years} năm ${months} tháng`;
            }
          }
          this.employeeForm.patchValue(this.dataTableEmpDetail);
        }
        if (this.dataproposeComment) {
          this.formSuggestionsPersonal.patchValue(this.dataproposeComment);
        }
        if (this.dataproposeStatement) {
          this.formReportElement.patchValue(this.dataproposeStatement);
        }
        if (this.dataproposeStatementEdit) {
          this.formReportElementEdit.patchValue(this.dataproposeStatementEdit);
        }
        this.proposeUnitForm.controls['formGroup']?.setValue(+this.formGroupDetail)
        this.contentUnitNameData = res?.data?.proposeDTO[0]?.proposeDetails[0]?.contentUnitCode;
        this.contentTitleNameData = res?.data?.proposeDTO[0]?.proposeDetails[0]?.contentTitleCode;
        // this.formSuggestionsPersonal.controls['proposeUnit']?.setValue(+this.contentUnitNameData);
        if(this.contentUnitNameData && this.contentTitleNameData) {
          this.contentUnit = {
            orgId: res?.data?.proposeDTO[0]?.proposeDetails[0]?.contentUnitCode,
            orgName: res?.data?.proposeDTO[0]?.proposeDetails[0]?.contentUnitName,
            pathId: res?.data?.proposeDTO[0]?.proposeDetails[0]?.contentUnitPathId,
            pathResult: res?.data?.proposeDTO[0]?.proposeDetails[0]?.contentUnitPathName,
            orgLevel: res?.data?.proposeDTO[0]?.proposeDetails[0]?.contentUnitTreeLevel,
          };
        }
        this.formSuggestionsPersonal.controls['proposeUnit']?.setValue(this.contentUnit);
        if(this.contentTitleNameData && this.contentUnitNameData){
          this.onChangeUnit(res?.data?.proposeDTO[0]?.proposeDetails[0]?.contentUnitCode?.toString(),
            res?.data?.proposeDTO[0]?.proposeDetails[0]?.contentTitleCode)
        }
        const dataNotifyDecision = this.proposeCategoryView === Constants.ProposeCategory.ELEMENT ?
          res?.data?.proposeDTO[0]?.proposeDetails[0].fileDTOList?.find((item: FileDTOList) => item.type === Constants.TypeStatementFile.STATEMENT) :
          res?.data?.proposeDTO[0]?.proposeDetails[0].fileDTOS?.find((item: FileDTOList) => item.type === Constants.TypeStatementFile.STATEMENT);
        const dataNotifyDecisionEdit = this.proposeCategoryView === Constants.ProposeCategory.ELEMENT ?
          res?.data?.proposeDTO[0]?.proposeDetails[0].fileDTOList?.find((item: FileDTOList) => item.type === Constants.TypeStatementFile.STATEMENTEDIT) :
          res?.data?.proposeDTO[0]?.proposeDetails[0].fileDTOS?.find((item: FileDTOList) => item.type === Constants.TypeStatementFile.STATEMENTEDIT);
        if(dataNotifyDecision){
          this.fileListSelectTTElement = [
            {
              id: dataNotifyDecision?.id,
              uid: dataNotifyDecision?.docId,
              name: dataNotifyDecision?.fileName,
              status: 'done',
            },
          ];
        }
        if(dataNotifyDecisionEdit){
          this.fileListSelectTTElementEdit = [
            {
              id: dataNotifyDecisionEdit?.id,
              uid: dataNotifyDecisionEdit?.docId,
              name: dataNotifyDecisionEdit?.fileName,
              status: 'done',
            },
          ];
        }
        this.fileElementEdit.uid = dataNotifyDecisionEdit?.docId;
        this.fileElementEdit.fileName = dataNotifyDecisionEdit?.fileName;
        this.fileElement.uid = dataNotifyDecision?.docId;
        this.fileElement.fileName = dataNotifyDecision?.fileName;
        const statusPublishCodeManyObject = res?.data?.flowStatusPublishConfigList?.find(
          (item: WithdrawNoteConfigList) => item.key === this.statusPublishCodeDisplay
        );
        this.statusPublishName = statusPublishCodeManyObject?.role

        const withdrawNote = res?.data?.withdrawNoteConfigList?.find(
          (i: StatusList) => i.key  === this.withdrawNote?.toString());
        this.withdrawNoteName = withdrawNote?.role;
        //logs
        this.proposeLogs = log?.data?.content || [];
        this.tableConfig.total = log?.data?.totalElements || 0;
        this.tableConfig.pageIndex = 1;
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }

  expandInfoEmp() {
    this.isSeeMore = !this.isSeeMore;
  }

  expandInfoEmpText() {
    const infoEmpText = !this.isSeeMore
      ? this.translate.instant('development.proposed-unit.button.down')
      : this.translate.instant('development.proposed-unit.button.up')
    return infoEmpText;
  }


  override ngOnDestroy() {
    this.subs.forEach((s) => s.unsubscribe());
  }



  getInnerWidth(): number {
    if (window.innerWidth > 767) {
      return window.innerWidth / 1.5 > 1368 ? 1368 : window.innerWidth / 1.3;
    }
    return window.innerWidth;
  }

  checkReportStatus(e: boolean) {
    this.checkBtn = e;
  }

  dowloadStatement(action: number) {
    const params = {
      groupProposeType: Constants.ProposeTypeID.ORIGINAL,
      action: action,
      screenCode: Constants.ScreenCode.HRDV,
    };
    this.isLoading = true;
    let listFile: ListFileTemplate[] = [];
    if (this.id) {
      const sub = this.service.exportReportDecision(this.id, params).subscribe(
        (res) => {
          listFile = res?.data;
          if (!listFile || listFile?.length === 0) {
            this.toastrCustom.error('development.list-plan.messages.noFile');
            this.isLoading = false;
          } else {
            listFile?.forEach((item) => {
              this.service.callBaseUrl(item.linkTemplate).subscribe((res) => {
                  const contentDisposition = res?.headers.get('content-disposition');
                  const filename = contentDisposition?.split(';')[1]?.split('filename')[1]?.split('=')[1]?.trim();
                  const blob = new Blob([res?.body], {
                    type: 'application/octet-stream',
                  });
                  FileSaver.saveAs(blob, filename);
                  this.isLoading = false;
                },
                (e) => {
                  this.getMessageError(e);
                  this.isLoading = false;
                })
            })
          }
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        }
      );
      this.subs.push(sub);
    }
  }

  // Lay cd theo don vi
  onChangeUnit(orgId: string, titleUnit?: number) {
    this.jobDTOS = [];
    this.formSuggestionsPersonal.controls['proposeTitle']?.setValue(null);
    if (orgId) {
      // this.isLoading = true;
      const paramPosition: ParamSearchPosition = {
        orgId: orgId,
        jobId: '',
        status: 1,
        type: Constants.TYPE_SEARCH_POSITION,
        page: 0,
        size: maxInt32,
      };
      const sub = this.service.getPositionByUnit(paramPosition).subscribe(
        (res) => {
          const listPositionParent = res?.data?.content;
          this.jobDTOS = listPositionParent.map((item: ListTitle) => {
            return { jobId: item.jobId, jobName: item.jobName };
          });
          if (titleUnit) {
            this.formSuggestionsPersonal.controls['proposeTitle']?.setValue(titleUnit);
          }
          // this.isLoading = false;
        },
        (e) => {
          this.isLoading = false;
          this.getMessageError(e);
        }
      );
      this.subs.push(sub);
    }
  }


  save() {
    this.isSubmitFormSuggestionsPersonal = false;
    this.isSubmitFormReportElement = false;
    this.isSubmitFormReportElementEdit = false;
    const data : RecommendIndividualModel = { ...cleanDataForm(this.proposeUnitForm) };
    const proposeUnit = this.formSuggestionsPersonal.controls['proposeUnit']?.value;
    const jobId = this.formSuggestionsPersonal.controls['proposeTitle']?.value
    const proposeTitleNameRecommend = this.jobDTOS.find((item) => jobId === item?.jobId )?.jobName;
    const formSuggestionsPersonal = cleanDataForm(this.formSuggestionsPersonal)
    data.proposeDetails = [
      {
        employee: this.checkDisplayCode === 1 ? this.dataTableEmp : this.dataTableEmpDetail,
        proposeComment: {
          proposeReason: formSuggestionsPersonal.proposeReason,
          commitment: formSuggestionsPersonal.commitment,
          strengthsPtnl: null,
          weaknessesPtnl: null,

        },
        sameDecision: null,
        isIncome: null,
        isInsurance: null,
        isDecision: null,
        issuedLater: null,
        contentUnitCode: proposeUnit?.orgId?.toString(),
        contentUnitName: proposeUnit?.orgName,
        contentUnitPathId: proposeUnit?.pathId,
        contentUnitPathName: proposeUnit?.pathResult,
        contentUnitTreeLevel: proposeUnit?.orgLevel,
        contentTitleName: proposeTitleNameRecommend,
        contentTitleCode: jobId,
        id: this.idDetail,
        flowStatusCode: this.flowStatusCode,
        conditionViolationCode: this.violationCode,
        conditionViolations: this.conditionViolations,
        formGroup: this.proposeUnitForm.get('formGroup')?.value,

      }
    ];
    data.flowStatusCode = this.flowStatusCode
    data.formGroup = this.proposeUnitForm.get('formGroup')?.value;
    if(this.id){
      data.id = this.id;
    }
    data.externalApprove = this.externalApprove;
    data.sentDate = this.sentDate;
    data.personalCode = this.personalCodeDetail;
    const paramSrcCode = Constants.ScreenCode.HRDV
    this.isLoading = true;
    const sub = this.service.saveRecommendIndividual([data], paramSrcCode)
      .subscribe(
      () => {
        this.isLoading = true;
        if(!this.externalApprove){
          if(!this.checkDisplayReportEdit()){
            this.saveReportElement()
          } else {
            this.saveReportEditElement()
          }
        }
        this.getLog(1);
        this.isLoading = false;
        this.toastrCustom.success(this.translate.instant('development.proposed-unit.messages.updateProposeSuccess'));
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    this.subs.push(sub)
  };


  delete() {
    const groupProposeType = Constants.ProposeTypeID.ORIGINAL;
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.proposed-unit.messages.delete'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        if(this.id){
          this.isLoading = true;
          this.service.deleteProposeListLD(this.id, groupProposeType).subscribe(
            () => {
              this.toastrCustom.success(this.translate.instant('development.proposed-unit.messages.deleteSuccess'));
              this.router.navigate([Constants.KeyCommons.DEVELOPMENT_RECOMMEND_INDIVIDUAL]);
              this.modalRef?.destroy();
            },
            (e) => {
              this.getMessageError(e);
              this.isLoading = false;
            }
          );
        }
      },
    });
  }

  callApiAcceptPropose() {
    const params = {
      proposeType: Constants.ProposeTypeID.ORIGINAL,
      note: this.checkDisplayReportEdit() ? this.formReportElementEdit.controls['idea'].value : this.formReportElement.controls['idea'].value,
    }
    this.isLoading = true;
    const sub = this.service
      .agreeUnitNew(this.idCreate, this.payloadSubmitForm(false), params)
      .subscribe(
        (res) => {
          if (res?.data && res?.data?.length > 0 ) {
            this.toastrCustom.error(res?.data.map((item: ProposedError) => item.errorMessage).join(', '));
          } else {
            this.toastrCustom.success(
              this.translate.instant(
                'development.proposed-unit.messages.acceptProposeSuccess'
              )
            );
            this.router.navigate(
              [Constants.KeyCommons.DEVELOPMENT_RECOMMEND_INDIVIDUAL_DETAIL],
              {
                queryParams: {
                  idCreate: this.id,
                  checkRouterScreen: this.checkDisplayCode,
                },
                skipLocationChange: true,
              }
            );
          }
          this.isLoading = false;
          this.modalRef?.destroy();
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        }
      );
    this.subs.push(sub);
  }

  //ghi nhận đồng ý
  acceptPropose() {
    this.isSubmitFormSuggestionsPersonal = true;
    if (this.formSuggestionsPersonal.invalid) {
      this.isSubmitFormSuggestionsPersonal = true;
      let scrollToTab  = document.getElementById('tab1');
      scrollToTab?.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
      this.toastrCustom.warning(this.translate.instant('development.approvement-config.messages.validate3'));
      return;
    }
    if (!this.externalApprove) {
      this.isSubmitFormReportElement = true;
      if((this.formReportElement.invalid && !this.checkDisplayReportEdit()) || this.fileListSelectTTElement.length === 0) {
        this.isSubmitFormReportElement = true;
        let scrollToTab  = document.getElementById('tab3crm');
        scrollToTab?.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
        this.toastrCustom.warning(this.translate.instant('development.approvement-config.messages.validate4'));
        return;
      }
      if(this.formReportElementEdit.invalid && this.checkDisplayReportEdit()) {
        this.isSubmitFormReportElementEdit = true;
        let scrollToTab  = document.getElementById('tab4crm');
        scrollToTab?.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
        this.toastrCustom.warning(this.translate.instant('development.approvement-config.messages.validate4'));
        return;
      }
      if(this.fileListSelectTTElementEdit.length === 0 && !this.formReportElementEdit.invalid && this.checkDisplayReportEdit()) {
        let scrollToTab  = document.getElementById('tab4crm');
        scrollToTab?.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
        this.toastrCustom.warning(this.translate.instant('development.approvement-config.messages.validate4'));
        return;
      }
    }
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant(
        'development.proposed-unit.messages.acceptPropose'
      ),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        if (this.violationCode === Constants.ViolationCode.REQUIRED) {
          this.isLoading = false;
          this.modalRef = this.modal.confirm({
            nzTitle: this.translate.instant(
              'development.proposed-unit.messages.sendProposeWarning2'
            ),
            nzCancelText: this.translate.instant(
              'development.proposed-unit.button.cancelText'
            ),
            nzOkText: this.translate.instant(
              'development.proposed-unit.button.okText'
            ),
            nzClassName: 'ld-confirm',
            nzOnOk: () => {
              this.isLoading = true;
              this.isAcceptPropose = true;
              if(this.checkDisplayReportEdit()){
                this.saveReportEditElement()
              } else {
                this.saveReportElement();
              }
            },
          });
        } else {
          this.isAcceptPropose = true;
          if(this.checkDisplayReportEdit()){
            this.saveReportEditElement()
          } else {
            this.saveReportElement();
          }
        }
      },
    });
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'development.proposed-unit.proposeLogsTable.time',
          field: 'actionDate',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
          tdTemplate: this.dateLog
        },
        {
          title: 'development.proposed-unit.proposeLogsTable.personAction',
          field: 'userName',
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
        },
        {
          title: 'development.proposed-unit.proposeLogsTable.action',
          field: 'action',
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
        },
        {
          title: 'development.proposed-unit.proposeLogsTable.description',
          field: 'comment',
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 200,
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1,
    };
  }

  expandLog() {
    this.isExpand = !this.isExpand;
    if (this.isExpand) {
      this.iconStatus = Constants.IconStatus.DOWN;
    } else {
      this.iconStatus = Constants.IconStatus.UP;
    }
  }

  downloadFile = (file: NzUploadFile) => {
    this.service.downloadFile(+file?.uid, file?.name).subscribe(
      () => {
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    )
  }

  beforeUploadTT = (file: File | NzUploadFile): boolean => {
    this.isLoading = true;
    const formData = new FormData();
    formData.append('files', file as File);
    formData.append('proposeDetailId', new Blob([JSON.stringify(this.idCreate)], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('type', new Blob(['statement'], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('groupProposeTypeId', new Blob(['1'], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('isIn', new Blob(['true'], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    this.service.uploadFile(formData).subscribe(
      (res) => {
        this.fileListSelectTTElement = [
          ...this.fileListSelectTTElement,
          {
            uid: res.data?.docId,
            name: res.data?.fileName,
            status: 'done',
            id: res.data?.id,
          },
        ];
        this.validApproveButton();
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    return false;
  };

  doRemoveFileTT = (file: NzUploadFile): boolean => {
    this.isLoading = true;
    if (this.fileListSelectTTElement.find((item) => item.uid === file.uid)){
      const idDelete = this.fileListSelectTTElement.find((item) => item.uid === file.uid)?.['id'];
      this.service.deleteUploadFile(+idDelete).subscribe(
        (res) => {
          this.fileListSelectTTElement = this.fileListSelectTTElement.filter((item) => item['id'] !== idDelete);
          this.validApproveButton();
          this.isLoading = false;
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        }
      );
    }
    return false;
  };

  //validate điền thông tin cho nút ghi nhận đồng ý
  validApproveButton(){
    return this.formReportElement.get('statementNumber')?.value
      && this.formReportElement.get('submissionDate')?.value
      && this.formReportElement.get('idea')?.value
      && this.fileListSelectTTElement.length === 1
  }

  checkBtnSendToBrower() {
    return Constants.StatusApproveSendProposeMany.includes(this.flowStatusCode)
  }

  override back() {
    if (this.stateUrl?.state?.['backUrl']) {
      this.router
        .navigateByUrl(Utils.getUrlByFunctionCode(this.functionCode))
        .then(() => {
          this.router.navigateByUrl(this.stateUrl?.state?.['backUrl'], {
            skipLocationChange: true,
          });
        });
    } else {
      this.router.navigateByUrl(Utils.getUrlByFunctionCode(this.functionCode));
    }
  }

    showModalWithdraw(typeButton: number) {
    //0 là rút đề xuất, 1 là rút thành phần
    //ids là id của cha
    const ids: number[] = [];
    if(this.id){
      ids.push(this.id);
    }
    this.modalRef = this.modal.create({
      nzTitle: this.translate.instant('development.proposed-unit.proposeMultiple.withdrawPropose'),
      nzWidth: '40vw',
      nzContent: WithdrawProposeComponent,
      nzComponentParams: {
        typeWithdraw: typeButton,
        ids: ids,
        isApproveHCMBefore: this.externalApprove,
      },
      nzFooter: null,
    });
    this.modalRef.componentInstance.closeEvent.subscribe((data:boolean) => {
      if (data) {
        this.isLoading = true;
        this.modalRef?.componentInstance.sendBody.subscribe((body: WithdrawPropose) => {
          const sub = this.service.withdrawPropose(body).subscribe(() => {
            const detail = this.service.getValueRecommendIndividual().subscribe(( state) => {
              const checkScreenWithDraw = state?.data?.checkDisplay;
              if(checkScreenWithDraw === Constants.RecommendIndividualCode.UPDATE) {
                this.router.navigate([Constants.KeyCommons.DEVELOPMENT_RECOMMEND_INDIVIDUAL_ACTION], {
                    queryParams: {
                      idCreate: this.id,
                      checkRouterScreen: this.checkDisplayCode,
                    },
                    skipLocationChange: true,
                  }
                );
              } else {
                this.isLoading = true;
                const check = this.service.getDetailRecommendIndividual(this.idCreate, this.screenCode).subscribe((res) => {
                  this.flowStatusCode = res?.data?.proposeDTO[0]?.flowStatusCode;
                  this.createdBy = res?.data?.proposeDTO[0]?.createdBy
                  this.withdrawNote = res?.data?.proposeDTO[0]?.withdrawNote;
                  const withdrawNote = res?.data?.withdrawNoteConfigList?.find(
                    (i: StatusList) => i.key  === this.withdrawNote?.toString());
                  this.withdrawNoteName = withdrawNote?.role
                  this.checkBtnShowModalWithdraw()
                  this.isLoading = false;
                }, (e) => {
                  this.getMessageError(e);
                  this.isLoading = false;
                });
                this.subs.push(check)
              }
              this.isLoading = false;
              this.toastrCustom.success(
                this.translate.instant('development.withdrawPropose.withdrawProposeSuccess'));
            }, (e) => {
              this.getMessageError(e);
              this.isLoading = false;
            });
            this.subs.push(detail);
            }, (e) => {
              this.getMessageError(e);
              this.isLoading = false;
            });
          this.subs.push(sub);
          this.modalRef?.destroy();
        });
      } else {
        this.modalRef?.destroy();
      }
    });
  }

  checkBtnShowModalWithdraw() {
    return Constants.StatusButtonWithdraw.includes(this.flowStatusCode) &&
      this.createdBy === this.currentUser?.username && !this.withdrawNote
  }


  // duyệt đồng ý LDDV
  agreeLeader() {
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.proposed-unit.messages.agreeLeader'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL2_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.CONTINUE_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        const proposeType = Constants.ProposeTypeID.ORIGINAL
        this.service.agreeLeader(this.id!, this.note, proposeType).subscribe(
          () => {
            this.toastrCustom.success(this.translate.instant(Constants.KeyCommons.MESSAGES_SUCCESS2));
            this.router.navigateByUrl(Constants.KeyCommons.DEVELOPMENT_PROPOSE_UNIT_APPROVE);
            this.isLoading = false;
            this.modalRef?.destroy();
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
      },
    });
  }

  // duyệt từ chối LDDV
  rejectLeader() {
    if (!this.note) {
      this.toastrCustom.error(this.translate.instant('development.commonMessage.approveIdeaRequired'));
      let el = document.getElementById('idea');
      el?.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
      return;
    }
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.proposed-unit.messages.rejectLeader'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        this.service.rejectLeader(this.id!, this.note).subscribe(
          () => {
            this.toastrCustom.success(this.translate.instant(Constants.KeyCommons.MESSAGES_SUCCESS2));
            this.router.navigateByUrl(Constants.KeyCommons.DEVELOPMENT_PROPOSE_UNIT_APPROVE);
            this.isLoading = false;
            this.modalRef?.destroy();
          },
          (e) => {
            this.toastrCustom.error(this.translate.instant('development.proposed-unit.messages.errorMes'));
            this.isLoading = false;
          }
        );
      },
    });
  }

  // duyệt ý kiến khác LDDV
  otherIdea() {
    if (!this.note) {
      this.toastrCustom.error(this.translate.instant('development.commonMessage.approveIdeaRequired'));
      let el = document.getElementById('idea');
      el?.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
      return;
    }
    const proposeType = Constants.ProposeTypeID.ORIGINAL
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.proposed-unit.messages.otherIdea'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        this.service.otherIdea(this.id!, this.note, proposeType).subscribe(
          (res) => {
            this.toastrCustom.success(this.translate.instant(Constants.KeyCommons.MESSAGES_SUCCESS2));
            this.router.navigateByUrl(Constants.KeyCommons.DEVELOPMENT_PROPOSE_UNIT_APPROVE);
            this.isLoading = false;
            this.modalRef?.destroy();
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
      },
    });
  }

  //check hiển thị 3 nút duyệt của LDDV và ý kiến phê duyệt
  checkApproveLDDV(){
    return (this.flowStatusCode === this.flowCodeConst.SENDAPPROVE2A ||
      this.flowStatusCode === this.flowCodeConst.CHODUYET2)
      && this.screenCodeRouting === this.screenCodeConst.LDDV;
  }

  // duyệt rút đề xuất
  agreeWithdrawProposal( ideaType: number) {
    const checkProposeType = Constants.ProposeTypeID.ORIGINAL;
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.proposed-unit.messages.agreeLeader2'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        if(this.id && checkProposeType){
          this.service.approveWithdrawPropose(this.id, checkProposeType, this.comment, ideaType, this.screenCodeRouting).subscribe(
            () => {
              this.toastrCustom.success(this.translate.instant(Constants.KeyCommons.MESSAGES_SUCCESS2));
              this.router.navigateByUrl(Constants.KeyCommons.DEVELOPMENT_PROPOSE_UNIT_APPROVE);
              this.isLoading = false;
              this.modalRef?.destroy();
            },
            (e) => {
              this.getMessageError(e);
              this.isLoading = false;
            }
          );
        }
      },
    });
  }

  //check nút duyệt rút
  checkWithdrawBtn(){
    const listWithdrawBtn = Constants.StatusButtonWithdraw;
    const listNoteCodeWithdraw = Constants.noteWithdraw;
    if(this.withdrawNote) {
      return listWithdrawBtn.indexOf(this.flowStatusCode) > -1
        && listNoteCodeWithdraw.indexOf(this.withdrawNote?.toString()) > -1
        && (this.screenCodeRouting === this.screenCodeConst.LDDV);
    }
    return false;
  }

  checkBtnWithdrawLDDV() {
    const listNoteCodeWithdraw = Constants.noteWithdraw;
    return Constants.ChangeStatusWithdraw.indexOf(this.flowStatusCode) === -1
      && listNoteCodeWithdraw.indexOf(this.withdrawNote?.toString()) > -1
      && (this.screenCodeRouting === this.screenCodeConst.LDDV);
  }

  checkWithdrawTypebtn() {
    return (this.checkWithdrawType === 0 || this.checkWithdrawType === 1)
      && (this.screenCodeRouting === this.screenCodeConst.LDDV);
  }

  displayConditionViolation() {
    const violationObj = this.dataCommon.conditionViolationList?.find(
      (item) => +item.code === this.violationCode);
    switch (this.violationCode) {
      case 0:
        return { color: Constants.ConditionViolationColor.REQUIRED, label: violationObj?.name };
      case 1:
        return { color: Constants.ConditionViolationColor.VIOLATE, label: violationObj?.name };
      case 2:
        return { color: Constants.ConditionViolationColor.PASSED, label: violationObj?.name };
      default:
        return null;
    }
  }

  checkDisabledHandleSingle() {
   return this.personalCode ? true : false;
  }

  checkReturnProposeButton(){
    return this.flowStatusCode === Constants.FlowCodeConstants.HANDLING5B
  }

  showModalReturnPropose() {
    this.modalRef = this.modal.create({
      nzTitle: this.translate.instant('development.proposed-unit.section.returnPropose'),
      nzWidth: '75vw',
      nzContent: ReturnProposeComponent,
      nzComponentParams: {
        proposeDetailId: this.idDetail,
      },
      nzFooter: null,
    });
    this.modalRef.componentInstance.closeEvent.subscribe((data:boolean) => {
      if (data) {
        this.isLoading = true;
        this.modalRef?.componentInstance.returnPropose.subscribe((body: ReturnPropose) => {
          const sub = this.service.returnPropose(body).subscribe((res) => {
              this.toastrCustom.success(this.translate.instant(Constants.KeyCommons.MESSAGES_SUCCESS2));
              this.router.navigateByUrl(Utils.getUrlByFunctionCode(this.functionCode));
              this.isLoading = false;
            },
            (e) => {
              this.getMessageError(e);
              this.isLoading = false;
            });
          this.subs.push(sub);
          this.modalRef?.destroy();
        })
      } else {
        this.modalRef?.destroy();
      }
    });
  }

  // //check hiển thị ý kiến phê duyệt của LDDV
  //check ý kiến phê duyệt LDDV ban đầu
  checkApproveNoteOrigin() {
    return !this.externalApprove && Constants.EditIdeaOriginReport.includes(this.flowStatusCode) && !this.hasAgreeUnit;
  }

  disabledEndDateSubmission = (endDate: Date): boolean => {
    return endDate >=  new Date();
  };

  // lấy log phân trang
  getLog(pageIndex: number){
    this.pageNumber = pageIndex - 1;
    this.isLoading = true;
    const log = this.service.getLogsPage(this.idCreate, Constants.ProposeCategory.ORIGINAL, this.screenCode, this.pageNumber, this.pageSize)
      .subscribe((log) => {
      this.proposeLogs = log?.data?.content || [];
      this.tableConfig.total = log?.data?.totalElements || 0;
      this.tableConfig.pageIndex = pageIndex;
      this.isLoading = false;
    }, (e) => {
      this.getMessageError(e);
      this.isLoading = false;
    })
    this.subs.push(log);
  }

  //gửi duyệt mới
  sendPropose() {
    this.isSubmitFormSuggestionsPersonal = true;
    if (this.formSuggestionsPersonal.invalid) {
      this.isSubmitFormSuggestionsPersonal = true;
      let scrollToTab  = document.getElementById('tab1');
      scrollToTab?.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
      this.toastrCustom.warning(this.translate.instant('development.approvement-config.messages.validate3'));
      return;
    }
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant(
        'development.proposed-unit.messages.sendPropose'
      ),
      nzCancelText: this.translate.instant(
        'development.proposed-unit.button.cancel'
      ),
      nzOkText: this.translate.instant(
        'development.proposed-unit.button.continue'
      ),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        if (this.violationCode === Constants.ViolationCode.REQUIRED) {
          this.isLoading = false;
          this.modalRef = this.modal.confirm({
            nzTitle: this.translate.instant(
              'development.propose-tgcv.confirmSendApprove'
            ),
            nzCancelText: this.translate.instant(
              'development.proposed-unit.button.cancel'
            ),
            nzOkText: this.translate.instant(
              'development.proposed-unit.button.continue'
            ),
            nzClassName: Constants.ClassName.LD_CONFIRM,
            nzOnOk: () => {
              this.isLoading = true;
              this.callApiSendPropose();
              this.modalRef?.destroy();
            },
          });
        } else {
          this.callApiSendPropose();
        }
      },
    });
  }

  callApiSendPropose() {
    const params = {
      proposeType: Constants.ProposeTypeID.ORIGINAL,
    }
    const send = this.service
      .sendProposeNew(this.idCreate, this.payloadSubmitForm(false), params)
      .subscribe(
        (res) => {
          if (res?.data && res?.data?.length > 0 ) {
            this.toastrCustom.error(res?.data.map((item: ProposedError) => item.errorMessage).join(', '));
          } else {
            this.toastrCustom.success(
              this.translate.instant(
                'development.proposed-unit.messages.sendProposeSuccess'
              )
            );
            this.router.navigate(
              [Constants.KeyCommons.DEVELOPMENT_RECOMMEND_INDIVIDUAL_DETAIL],
              {
                queryParams: {
                  idCreate: this.id,
                  checkRouterScreen: this.checkDisplayCode,
                },
                skipLocationChange: true,
              }
            );
          }

          this.isLoading = false;
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        }
      );
    this.subs.push(send);
  }

  //body lưu đề xuất
  payloadSubmitForm(isSave: boolean) {
    const data = { ...cleanDataForm(this.proposeUnitForm) };
    const proposeUnit = this.formSuggestionsPersonal.controls['proposeUnit']?.value;
    const jobId = this.formSuggestionsPersonal.controls['proposeTitle']?.value
    const proposeTitleNameRecommend = this.jobDTOS.find((item) => jobId === item?.jobId )?.jobName;
    const formSuggestionsPersonal = cleanDataForm(this.formSuggestionsPersonal)
    data.proposeDetails = [
      {
        employee: this.checkDisplayCode === 1 ? this.dataTableEmp : this.dataTableEmpDetail,
        proposeComment: {
          proposeReason: formSuggestionsPersonal.proposeReason,
          commitment: formSuggestionsPersonal.commitment,
          strengthsPtnl: null,
          weaknessesPtnl: null,

        },
        sameDecision: null,
        isIncome: null,
        isInsurance: null,
        isDecision: null,
        issuedLater: null,
        contentUnitCode: proposeUnit?.orgId?.toString(),
        contentUnitName: proposeUnit?.orgName,
        contentUnitPathId: proposeUnit?.pathId,
        contentUnitPathName: proposeUnit?.pathResult,
        contentUnitTreeLevel: proposeUnit?.orgLevel,
        contentTitleName: proposeTitleNameRecommend,
        contentTitleCode: jobId,
        id: this.idDetail,
        flowStatusCode: this.flowStatusCode,
        conditionViolationCode: this.violationCode,
        conditionViolations: this.conditionViolations,
        formGroup: this.proposeUnitForm.get('formGroup')?.value,

      }
    ];
    data.flowStatusCode = this.flowStatusCode
    data.formGroup = this.proposeUnitForm.get('formGroup')?.value;
    if(this.id){
      data.id = this.id;
    }
    data.externalApprove = this.externalApprove;
    data.sentDate = this.sentDate;
    data.personalCode = this.personalCodeDetail;
    data.approveNote = this.formReportElement.controls['idea'].value;
    return isSave ? data : [data];
  }

  //lưu tờ trình của thành phần
  saveReportElement() {
    const dataStatement = {
      statementNumber: this.formReportElement.controls['statementNumber'].value,
      submissionDate: this.formReportElement.controls['submissionDate'].value,
      approveNote: this.formReportElement.controls['idea'].value,
      externalApprove: this.externalApprove,
      id: this.id
    }
    const sub = this.service.saveStatementDecision(dataStatement).subscribe(
      () => {
        if (this.isAcceptPropose) {
          this.callApiAcceptPropose();
        }
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }

  //lưu tờ trình chỉnh sửa của đơn vị
  saveReportEditElement() {
    const dataStatement = {
      statementNumber: this.formReportElementEdit.controls['statementNumber'].value,
      submissionDate: this.formReportElementEdit.controls['submissionDate'].value,
      approveNote: this.formReportElementEdit.controls['idea'].value,
      externalApprove: this.externalApprove,
      id: +this.idCreate,
      typeStatement: Constants.ProposeTypeID.ELEMENT
    }
    this.isLoading = true;
    const sub = this.service.saveStatementDecision(dataStatement).subscribe(
      () => {
        this.isLoading = false;
        if (this.isAcceptPropose) {
          this.callApiAcceptPropose();
        }
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }

  // file đính kèm tờ trình chỉnh sửa
  beforeUploadTTEdit = (file: File | NzUploadFile): boolean => {
    this.isLoading = true;
    const formData = new FormData();
    formData.append('files', file as File);
    formData.append('proposeDetailId', new Blob([JSON.stringify(this.idCreate)], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('type', new Blob(['statementEdit'], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('groupProposeTypeId', new Blob(['1'], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('isIn', new Blob(['true'], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    this.service.uploadFile(formData).subscribe(
      (res) => {
        this.fileListSelectTTElementEdit = [
          ...this.fileListSelectTTElementEdit,
          {
            uid: res.data?.docId,
            name: res.data?.fileName,
            status: 'done',
            id: res.data?.id,
          },
        ];
        this.validApproveButton();
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    return false;
  };

  // xóa file đính kèm tờ trình chỉnh sửa
  doRemoveFileTTEdit = (file: NzUploadFile): boolean => {
    this.isLoading = true;
    if (this.fileListSelectTTElementEdit.find((item) => item.uid === file.uid)){
      const idDelete = this.fileListSelectTTElementEdit.find((item) => item.uid === file.uid)?.['id'];
      this.service.deleteUploadFile(+idDelete).subscribe(
        (res) => {
          this.fileListSelectTTElementEdit = this.fileListSelectTTElementEdit.filter((item) => item['id'] !== idDelete);
          this.validApproveButton();
          this.isLoading = false;
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        }
      );
    }
    return false;
  };

  //dowload file tờ trình chỉnh sửa
  downloadFile2(uid: string | number, name: string) {
    this.isLoading = true;
    this.service.downloadFile(+uid, name).subscribe(
      () => {
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    )
  }

  //check hiển thị cục tờ trình chỉnh sửa của đơn vị(tờ trình riêng của màn thành phần)
  // hiển thị ở trạng thái khác 1 ở HRDV(màn update) duyệt ngoài của cha,
  // có dữ liệu(màn view) hoặc ở trên PTNL nhưng phải có số tờ trình
  checkDisplayReportEdit() {
    return !this.externalApprove &&
      (
        (!Constants.EditOriginReport.includes(this.flowStatusCode)
          && this.screenCode === Constants.ScreenCode.HRDV
          && this.typeAction === this.screenType.update && !!this.dataproposeStatement) ||
        (this.checkDataReportEdit() &&
          (this.screenCodeRouting === Constants.ScreenCode.PTNL || this.screenCodeRouting === Constants.ScreenCode.KSPTNL ||
            (this.screenCode === Constants.ScreenCode.HRDV && (this.typeAction === this.screenType.detail || this.typeAction === this.screenType.update))))
      );
  }

  //check xem tờ trình chỉnh sửa có dữ liệu không
  checkDataReportEdit() {
    return this.dataproposeStatementEdit?.statementNumber
      || this.dataproposeStatementEdit?.submissionDate
      || this.dataproposeStatementEdit?.approveNote
      || this.fileListSelectTTElementEdit.length === 1
  }

  //check view đính kèm tờ trình chỉnh sửa của đơn vị
  checkViewFileElement() {
    return (this.screenCode === Constants.ScreenCode.HRDV && (this.fileElementEdit?.uid && this.fileElementEdit?.fileName)
        && this.typeAction === this.screenType.detail)
      || ((this.screenCodeRouting === Constants.ScreenCode.PTNL || this.screenCodeRouting === Constants.ScreenCode.KSPTNL)
        && this.checkDataReportEdit());
  }

  //check hiển thị ý kiến phê duyệt của LDDV (tờ trình chỉnh sửa)
  //check hiển thị ý kiến phê duyệt của LDDV
  checkApproveNote() {
    return !this.externalApprove
      && ((this.screenCode === Constants.ScreenCode.HRDV && this.typeAction === this.screenType.update)
        || (this.screenCode === Constants.ScreenCode.HRDV && this.typeAction === this.screenType.detail
          && this.formReportElementEdit.get('idea')?.value))
      && this.flowStatusCode !== this.flowCodeConst.HANDLING5B
      && this.flowStatusCode !== this.flowCodeConst.CANCELPROPOSE;
  }

  //check hiển thị nút xử lý của PTNL
  checkHandleButton() {
    return Constants.HandleButtonPTNL.indexOf(this.flowStatusCode) > -1 && this.screenCodeRouting === this.screenCodeConst.PTNL;
  }

  //xử lý
  handle() {
    this.isLoading = true;
    const param = {
      id: this.idDetail,
      proposeType: Constants.ProposeTypeID.ELEMENT,
    };
    this.service.proposalProcessing(param).subscribe(
      (res) => {
        this.isLoading = false;
        this.toastrCustom.success(this.translate.instant('development.commonMessage.success'));
        const queryParams = {
          screenCode: Constants.ScreenCode.PTNL,
          id: this.id,
          proposeDetailId: this.idDetail,
          proposeCategory: Constants.ProposeCategory.ELEMENT,
          relationType: 0,
        };
        this.router.navigate([Constants.KeyCommons.DEVELOPMENT_PROPOSE_LIST, 'handle'], {
          queryParams: queryParams,
          state: { backUrl: this.router.url },
          skipLocationChange: true,
        });
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }
}
