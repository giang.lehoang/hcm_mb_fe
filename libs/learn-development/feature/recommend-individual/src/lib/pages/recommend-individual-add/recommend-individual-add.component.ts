import {Component, Injector, OnInit} from '@angular/core';
import {Constants, ScreenType} from "@hcm-mfe/learn-development/data-access/common";
import {
  EmployeeRecommendIndividual,
  FormGroupList,
  FormGroupMultipleList, RecommendIndividualModel
} from '@hcm-mfe/learn-development/data-access/models';
import { ProposedUnitService } from "@hcm-mfe/learn-development/data-access/services";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import {catchError, forkJoin, of, Subscription} from 'rxjs';
import {cleanDataForm} from "@hcm-mfe/shared/common/utils";



@Component({
  selector: 'app-recommend-individual-add',
  templateUrl: './recommend-individual-add.component.html',
  styleUrls: ['./recommend-individual-add.component.scss'],
})
export class RecommendIndividualAddComponent extends BaseComponent implements OnInit {
  proposeUnitForm = this.fb.group({
    id: null,
    formGroup: null,
    externalApprove: false,
  });

  private readonly subs: Subscription[] = [];
  externalApprove = false;
  formGroup: number | undefined;
  formGroupConstants = Constants.FormGroupList;
  dataCommon = {
    jobDTOS: [], // vị trí
    formGroupConfig: <FormGroupList[]>[],
    formGroupMultipleList: <FormGroupMultipleList[]>[],
  };
  screenCode = '';
  typeAction?: ScreenType;
  checkFormGroup = false;
  checkRouterScreen: number | undefined;
  id = 0;
  dataTableEmp: EmployeeRecommendIndividual | undefined;
  sentDate: Date | undefined;


  constructor(injector: Injector,
              private readonly service: ProposedUnitService,
  ) {
    super(injector);
    this.typeAction = this.route.snapshot.data['screenType'];

  }


  ngOnInit() {
    this.isLoading = true;
    const sub =
      this.service.getValueRecommendIndividual().subscribe(
      (res) => {
        this.checkRouterScreen = res?.data?.checkDisplay;
        this.externalApprove = res?.data?.externalApprove;
        this.sentDate = res?.data?.sentDate
        this.dataCommon = res?.data;
        if(this.checkRouterScreen === Constants.RecommendIndividualCode.UPDATE) {
          const ids = res?.data?.proposeDTO[0]?.id;
          this.router.navigate([Constants.KeyCommons.DEVELOPMENT_RECOMMEND_INDIVIDUAL_ACTION], {
              queryParams: {
                formGroup: this.proposeUnitForm.controls['formGroup'].value,
                checkFormGroup: this.checkFormGroup,
                idCreate: ids,
                checkRouterScreen: this.checkRouterScreen
              },
              skipLocationChange: true,
            }
          );
        } else if (this.checkRouterScreen === Constants.RecommendIndividualCode.DETAIL) {
          const ids = res?.data?.proposeDTO[0]?.id;
          this.router.navigate([Constants.KeyCommons.DEVELOPMENT_RECOMMEND_INDIVIDUAL_DETAIL], {
              queryParams: {
                formGroup: this.proposeUnitForm.controls['formGroup'].value,
                checkFormGroup: this.checkFormGroup,
                idCreate: ids,
                checkRouterScreen: this.checkRouterScreen
              },
              skipLocationChange: true,
            }
          );
        } else {
          this.id = res?.data?.id;
          this.dataTableEmp = res?.data?.proposeDetails[0]?.employee;
        }

        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }

  onChangeFormGroup(e: number) {
    if (e === this.formGroupConstants.DCDD || e === this.formGroupConstants.TGCV) {
      if (e === this.formGroupConstants.TGCV) {
        this.checkFormGroup = true
      }
      const parramIndividual = {
        id: this.id,
        formGroup: e,
        externalApprove: this.externalApprove,
        sentDate: this.sentDate,
        proposeDetails: [{
          employee:this.dataTableEmp,
          formGroup: e
        }]
      }
      this.isLoading = true;
      this.service.saveRecommendIndividual([parramIndividual], Constants.ScreenCode.HRDV).subscribe((res) => {
        this.isLoading = false;
        this.router.navigate([Constants.KeyCommons.DEVELOPMENT_RECOMMEND_INDIVIDUAL_ACTION], {
            queryParams: {
              formGroup: this.proposeUnitForm.controls['formGroup'].value,
              checkFormGroup: this.checkFormGroup,
              idCreate: this.id,
            },
            skipLocationChange: true,
          }
        );
        this.toastrCustom.success(this.translate.instant('development.proposed-unit.messages.createProposeSuccess'));
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      })
    }
  }
}
