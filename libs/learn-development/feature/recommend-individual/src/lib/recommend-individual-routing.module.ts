import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ScreenType} from "@hcm-mfe/learn-development/data-access/common";
import {RecommendIndividualAddComponent} from "./pages/recommend-individual-add/recommend-individual-add.component";
import {
  RecommendIndividualUpdateComponent
} from "./pages/recommend-individual-update/recommend-individual-update.component";



const routes: Routes = [
  { path: 'add',
    component: RecommendIndividualAddComponent,
    data: {
      pageName: 'development.pageName.add-personal-recommendation',
      breadcrumb: 'development.breadcrumb.add-personal-recommendation',
      screenType: ScreenType.Create,
    },
  },
  {
    path: 'action-recommend-individual',
    component: RecommendIndividualUpdateComponent,
    data: {
      pageName: 'development.pageName.update-personal-recommendation',
      breadcrumb: 'development.breadcrumb.update-personal-recommendation',
      screenType: ScreenType.Update,
    },
  },
  {
    path: 'detail-recommend-individual',
    component: RecommendIndividualUpdateComponent,
    data: {
      pageName: 'development.pageName.detail-personal-recommendation',
      breadcrumb: 'development.breadcrumb.detail-personal-recommendation',
      screenType: ScreenType.Detail,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RecommendIndividualRoutingModule {}
