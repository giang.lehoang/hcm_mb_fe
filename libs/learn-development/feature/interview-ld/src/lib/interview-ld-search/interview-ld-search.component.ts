import { Component, Injector, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Constants, userConfig } from "@hcm-mfe/learn-development/data-access/common";
import { CategoryModel, DataInterview, InterviewStatus, ParamSearchInterview } from "@hcm-mfe/learn-development/data-access/models";
import { InterviewService } from "@hcm-mfe/learn-development/data-access/services";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import { MBTableConfig, Pageable } from "@hcm-mfe/shared/data-access/models";
import { saveAs } from 'file-saver';
import * as moment from 'moment';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { NzModalRef } from "ng-zorro-antd/modal";

@Component({
  selector: 'app-interview-ld-search',
  templateUrl: './interview-ld-search.component.html',
  styleUrls: ['./interview-ld-search.component.scss'],
})
export class InterviewLdSearchComponent extends BaseComponent implements OnInit {
  dataTable: DataInterview[] = [];
  title?: string;
  modalRef?: NzModalRef;
  params: ParamSearchInterview = {
    code: null,
    proposeCode: null,
    fromDate: null,
    toDate: null,
    rangeDate: null,
    employeeCode: null,
    employeeName: null,
    currentUnit: null,
    currentTitle: null,
    proposeUnit: null,
    proposeTitle: null,
    interviewStatus: null,
    page: 0,
    size: 15,
  };

  dataCommonBranch = {
    jobDTOS: <CategoryModel[]>[],
    listStatus: [],
    interviewStatusInInterviewsScreen: [],
  };
  pageable: Pageable = {
    totalElements: 0,
    currentPage: 0,
    size: userConfig.pageSize,
  };

  //Checkbox
  checked = false;
  indeterminate = false;
  isExpand = true;
  iconStatus = Constants.IconStatus.DOWN;
  quantityMap: Map<number, DataInterview> = new Map<number, DataInterview>();
  tableConfig!: MBTableConfig;
  @ViewChild('groupCBTmpl', {static: true}) groupCheckbox!: TemplateRef<NzSafeAny>;
  @ViewChild('groupCBHeaderTmpl', {static: true}) groupCheckboxHeader!: TemplateRef<NzSafeAny>;
  @ViewChild('interviewResultDescriptionTmpl', {static: true}) interviewResultDescription!: TemplateRef<NzSafeAny>;
  @ViewChild('birthdayTmpl', {static: true}) birthday!: TemplateRef<NzSafeAny>;
  @ViewChild('interviewSentDateTmpl', {static: true}) interviewSentDate!: TemplateRef<NzSafeAny>;


  heightTable = { x: '100%', y: '25em'};



  constructor(injector: Injector, private readonly interviewService: InterviewService) {
    super(injector);
  }

  ngOnInit() {
    this.initTable();
    this.title = this.route.snapshot?.data['pageName'];
    this.isLoading = true;
    this.interviewService.getState().subscribe(
      (state) => {
        if (state) {
          this.dataCommonBranch.jobDTOS = state?.jobDTOS;
          this.dataCommonBranch.interviewStatusInInterviewsScreen = state?.interviewStatusInInterviewsScreen;
          this.dataCommonBranch.listStatus = state?.interviewStatusInInterviewsScreen
            ?.filter((item: InterviewStatus) => +item.code >= +Constants.InterviewHandleStatus.DGKQ || item.code === Constants.InterviewHandleStatus.CGKQ)
            ?.map((item: InterviewStatus) => {
              return {
                code: item.code,
                name: item.name,
              };
            });
        }
        this.search(1);
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

  disabledStartDate = (startValue: Date): boolean => {
    if (!startValue || !this.params.toDate) {
      return false;
    }
    return startValue.getTime() > moment(this.params.toDate).toDate().getTime();
  };

  disabledEndDate = (endValue: Date): boolean => {
    if (!endValue || !this.params.fromDate) {
      return false;
    }
    return endValue.getTime() <= moment(this.params.fromDate).toDate().getTime();
  };

  displayInterviewResult(result: string){
    return Constants.DisplayInterviewResult.find((item) => item.description === result)?.color;
  }

  onAllChecked(checked: boolean): void {
    this.dataTable.forEach((item) => this.updateCheckedSet(item, checked));
    this.refreshCheckedStatus();
  }

  refreshCheckedStatus(): void {
    this.checked = this.dataTable.every(({ interviewId }) => this.quantityMap.has(interviewId));
  }

  updateCheckedSet(item: DataInterview, checked: boolean): void {
    if(item?.interviewStatus){
      if (checked && Constants.ViewCheckBoxInterview.includes(item?.interviewStatus)) {
        this.quantityMap.set(item.interviewId, item);
      } else {
        this.quantityMap.delete(item.interviewId);
      }
    }
  }

  onItemChecked(interviewId: number, checked: boolean): void {
    const item = this.dataTable.find((i: DataInterview) => i.interviewId === interviewId);
    if (item) {
      this.updateCheckedSet(item, checked);
    }
    this.refreshCheckedStatus();
  }

  search(firstPage: number) {
    this.isLoading = true;
    this.params.page = firstPage - 1;
    const searchParams = { ...this.params };
    const currentUnitId = searchParams?.currentUnit?.orgId || '';
    searchParams.currentUnit = currentUnitId;
    const proposeUnitId = searchParams?.proposeUnit?.orgId || '';
    searchParams.proposeUnit = proposeUnitId;
    if (searchParams?.rangeDate) {
      searchParams.fromDate =
      this.params.rangeDate && this.params.rangeDate[0] ? moment(searchParams.rangeDate[0]).format(Constants.FORMAT_DATE.YYYY_MM_DD) : null;
      searchParams.toDate =
      this.params.rangeDate && this.params.rangeDate[1] ? moment(searchParams.rangeDate[1]).format(Constants.FORMAT_DATE.YYYY_MM_DD) : null;
    }
    delete searchParams?.rangeDate;
    this.interviewService.searchInterview(searchParams).subscribe(
      (res) => {
        this.dataTable = res.data?.content;
        this.checked = true;
        this.dataTable?.forEach((item: DataInterview) => {
          if (!this.quantityMap.has(item?.interviewId)) {
            this.checked = false;
          } else {
            this.quantityMap.set(item.interviewId, item);
          }
        });
        if (!this.dataTable?.length) {
          this.checked = false;
        }
        this.tableConfig.total = res?.data?.totalElements || 0;
        this.tableConfig.pageIndex = firstPage;
        this.isLoading = false;
      },
      (e) => {
        this.isLoading = false;
        this.getMessageError(e);
        this.dataTable = [];
      }
    );
  }

  expandSearch() {
    this.isExpand = !this.isExpand;
    if (this.isExpand) {
      this.iconStatus = Constants.IconStatus.DOWN;
    } else {
      this.iconStatus = Constants.IconStatus.UP;
    }
  }

  screenUpdate() {
    if (!this.quantityMap.size) {
      this.toastrCustom.error(this.translate.instant('modelOrganization.planYear.chooseMin'));
      return;
    }
    const itemEdit: DataInterview[] = [];
    this.quantityMap.forEach((value) => {
      itemEdit.push(value);
    });

    this.router.navigate([this.router.url, 'update'], {
      skipLocationChange: true,
      queryParams: {
        dataInterview: JSON.stringify(itemEdit),
      },
    });
  }

  //xem chi tiết phỏng vấn
  detail(item: DataInterview){
    const itemEdit = [];
    itemEdit.push(item);
    this.router.navigate([this.router.url, 'detail'], {
      skipLocationChange: true,
      queryParams: {
        dataInterview: JSON.stringify(itemEdit),
      },
    });
  }

  sendResultInterview() {
    if (!this.quantityMap.size) {
      this.toastrCustom.error(this.translate.instant('modelOrganization.planYear.chooseMin'));
      return;
    }
    const itemEdit: number[] = [];
    this.quantityMap.forEach((item) => {
      itemEdit.push(item?.interviewId);
    });
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.interview-config.notificationMessage.sendResultInterview'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        this.interviewService.sendInterviewSearch(itemEdit).subscribe(
          () => {
            this.toastrCustom.success(
              this.translate.instant('development.interview-config.notificationMessage.sendReusltSuccess')
            );
            this.quantityMap.clear();
            this.search(1);
            this.isLoading = false;
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
      },
    });
  }

  exportExcel(isExcelInterview: boolean) {
    if (this.dataTable?.length === 0) {
      this.toastrCustom.warning(
        this.translate.instant(
          'development.proposed-unit.messages.dataTableEmpty'
        )
      );
      return;
    }
    this.isLoading = true;
    const searchParams = { ...this.params };
    const currentUnitId = searchParams?.currentUnit?.orgId || '';
    searchParams.currentUnit = currentUnitId;
    const proposeUnitId = searchParams?.proposeUnit?.orgId || '';
    searchParams.proposeUnit = proposeUnitId;
    if (searchParams?.rangeDate) {
      searchParams.fromDate =
      this.params.rangeDate && this.params.rangeDate[0] ? moment(searchParams.rangeDate[0]).format(Constants.FORMAT_DATE.YYYY_MM_DD) : null;
      searchParams.toDate =
      this.params.rangeDate && this.params.rangeDate[1] ? moment(searchParams.rangeDate[1]).format(Constants.FORMAT_DATE.YYYY_MM_DD) : null;
    }
    searchParams.isExportInterview = isExcelInterview;
    delete searchParams?.rangeDate;



    this.interviewService.exportExcelInterview(searchParams).subscribe(
      (data) => {
        let urlExcel = 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,';
        urlExcel = urlExcel + data.data;
        saveAs(urlExcel, 'Danh sách phỏng vấn.xlsx');
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

  initTable() {
    this.tableConfig = {
      headers: [
          {
            title: '',
            field: 'select',
            width: 60,
            thClassList: [Constants.TABLE_CONFIG.TEXT_NOWRAP, Constants.TABLE_CONFIG.TEXT_CENTER],
            tdClassList: [Constants.TABLE_CONFIG.TEXT_NOWRAP, Constants.TABLE_CONFIG.TEXT_CENTER],
            fixed: true,
            thTemplate: this.groupCheckboxHeader,
            tdTemplate: this.groupCheckbox,
            fixedDir: 'left',
          },

        {
          title: 'development.interview-config.colTable.planCode',
          field: 'proposeOptionCode',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 100,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.interview-config.colTable.codeEmployee',
          field: 'employeeCode',
          width: 120,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.interview-config.colTable.nameEmployee',
          field: 'employeeName',
          width: 200,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.interview-config.colTable.dateOfRequest',
          field: 'interviewSentDate',
          width: 150,
          tdTemplate: this.interviewSentDate,
        },
        {
          title: 'development.interview-config.colTable.withdrawNoteName',
          field: 'withdrawNoteName',
          width: 150,
        },
        {
          title: 'development.interview-config.colTable.withdrawReason',
          field: 'withdrawReason',
          width: 150,
        },

        {
          title: 'development.interview-config.colTable.interviewProcessingStatus',
          field: 'interviewStatusDescription',
          width: 160,
        },
        {
          title: 'development.interview-config.colTable.dateOfSendingInterviewResults',
          field: 'receivedResultDate',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
        },
        {
          title: 'development.interview-config.colTable.interviewResults',
          field: 'interviewResultDescription',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          tdTemplate: this.interviewResultDescription,
          width: 180,
        },
        {
          title: 'development.interview-config.colTable.typeoffer',
          field: 'formGroup',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          // tdTemplate: this.showMaxTime,
          width: 150,
        },
        {
          title: 'development.proposed-unit.detail.employee.birthday',
          field: 'birthDay',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          tdTemplate: this.birthday,
          width: 130,
        },
        {
          title: 'development.interview-config.colTable.dateIn',
          field: 'joinDate',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          // tdTemplate: this.showSeniority,
          width: 130,
        },
        {
          title: 'development.interview-config.colTable.unit',
          field: 'employeeUnit',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          // tdTemplate: this.showSeniority,
          width: 200,
        },
        {
          title: 'development.interview-config.colTable.title',
          field: 'employeeTitleName',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          // tdTemplate: this.showSeniority,
          width: 200,
        },
        {
          title: 'development.interview-config.colTable.location',
          field: 'employeePosition',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          // tdTemplate: this.showSeniority,
          width: 200,
        },
        {
          title: 'development.interview-config.colTable.proposeUnit',
          field: 'contentUnit',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          // tdTemplate: this.showSeniority,
          width: 200,
        },
        {
          title: 'development.interview-config.colTable.proposeTitle',
          field: 'contentTitleName',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          // tdTemplate: this.showSeniority,
          width: 200,
        },
        {
          title: 'development.interview-config.colTable.proposeLocation',
          field: 'contentPositionName',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          // tdTemplate: this.showSeniority,
          width: 200,
        },
        {
          title: 'development.interview-config.colTable.ykgpv',
          field: 'interviewNote',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          // tdTemplate: this.showSeniority,
          width: 200,
        },


      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: userConfig.pageSize,
      pageIndex: 1,
    };
  }

  viewBoxInterview(statusInterview: number) {
    return Constants.ViewCheckBoxInterview.includes(statusInterview);
  }

  importFile(){
    this.router.navigate([this.router.url, 'import'], {
      skipLocationChange: true,
    });
  }

}
