import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LearnDevelopmentFeatureShellModule} from "@hcm-mfe/learn-development/feature/shell";
import {InterviewLdSearchComponent} from "./interview-ld-search/interview-ld-search.component";
import {InterviewLdUpdateComponent} from "./interview-ld-update/interview-ld-update.component";
import {InterviewLdRoutingModule} from "./interview-ld.routing.module";
import {PopupService} from "@hcm-mfe/shared/ui/popup";
import {InterviewLdImportComponent} from "./interview-ld-import/interview-ld-import.component";

@NgModule({
  declarations: [InterviewLdSearchComponent, InterviewLdUpdateComponent, InterviewLdImportComponent],
  exports: [InterviewLdSearchComponent, InterviewLdUpdateComponent, InterviewLdImportComponent],
  imports: [CommonModule, LearnDevelopmentFeatureShellModule, InterviewLdRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
  providers: [PopupService]
})
export class LearnDevelopmentFeatureInterviewLdModule {}
