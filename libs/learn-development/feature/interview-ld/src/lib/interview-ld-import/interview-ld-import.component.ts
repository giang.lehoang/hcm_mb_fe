import { Component, Injector, OnInit } from '@angular/core';
import { Constants, userConfig } from "@hcm-mfe/learn-development/data-access/common";
import { ParamErrorSuccess, TableErrorImport, TableSuccessImport } from "@hcm-mfe/learn-development/data-access/models";
import { InterviewService, ProposedUnitService } from "@hcm-mfe/learn-development/data-access/services";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import { MBTableConfig } from "@hcm-mfe/shared/data-access/models";
import { NzUploadFile } from "ng-zorro-antd/upload";
import { saveAs } from 'file-saver';


@Component({
  selector: 'app-interview-ld-import',
  templateUrl: './interview-ld-import.component.html',
  styleUrls: ['./interview-ld-import.component.scss'],
})
export class InterviewLdImportComponent extends BaseComponent implements OnInit {
  dataTable: TableSuccessImport[] = [];
  dataTableError: TableErrorImport[] = [];

  params: ParamErrorSuccess = {
    importKey: '',
    dataType: 1,
    page: 0,
    size: 15,
  };

  paramsError: ParamErrorSuccess = {
    importKey: '',
    dataType: 0,
    page: 0,
    size: 15,
  };

  //Checkbox
  tableConfig!: MBTableConfig;
  tableConfigError!: MBTableConfig;
  heightTable = { x: '100%', y: '30em'};
  fileImport: NzUploadFile[] = [];
  idMessage = '';

  constructor(injector: Injector, private readonly interviewService: InterviewService, private readonly servicePropose: ProposedUnitService) {
    super(injector);
  }

  ngOnInit() {
    this.initTable();
    this.initTableError();
  }

  searchSuccess(firstPage: number) {
    this.isLoading = true;
    this.params.page = firstPage - 1;

    this.interviewService.getErrorSuccessFile(this.params).subscribe(
      (res) => {
        this.dataTable = res.data?.content?.map((item: TableErrorImport) => { return item.importSuccessData });;
        this.tableConfig.total = res?.data?.totalElements || 0;
        this.tableConfig.pageIndex = firstPage;
        this.isLoading = false;
      },
      (e) => {
        this.isLoading = false;
        this.getMessageError(e);
        this.dataTable = [];
      }
    );
  }

  searchError(firstPage: number) {
    this.isLoading = true;
    this.paramsError.page = firstPage - 1;
    this.interviewService.getErrorSuccessFile(this.paramsError).subscribe(
      (res) => {
        this.dataTableError = res.data?.content?.map((item: TableErrorImport) => { return item.importFailedData });
        this.tableConfigError.total = res?.data?.totalElements || 0;
        this.tableConfigError.pageIndex = firstPage;
        this.isLoading = false;
      },
      (e) => {
        this.isLoading = false;
        this.getMessageError(e);
        this.dataTableError = [];
      }
    );
  }


  exportExcel() {
    if (this.dataTableError?.length === 0) {
      this.toastrCustom.warning(
        this.translate.instant(
          'development.proposed-unit.messages.dataTableEmpty'
        )
      );
      return;
    }
    this.isLoading = true;
    this.interviewService.exportExcelFile(this.idMessage).subscribe(
      (data) => {
        let urlExcel = 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,';
        urlExcel = urlExcel + data.data;
        saveAs(urlExcel, this.translate.instant(
          'development.salaryInterview.listErrorData'
        ));
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );

  }

  downloadFile = (file: NzUploadFile) => {
    this.servicePropose.downloadFile(+file?.uid, file?.name).subscribe(
      () => {
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    )
  }

  beforeUpload = (file: File | NzUploadFile): boolean => {
    this.isLoading = true;
    const formData = new FormData();
    formData.append('file', file as File);
    this.interviewService.importFileInterview(formData).subscribe(
      (res) => {
        this.idMessage = res.data;
        this.params.importKey = res.data;
        this.paramsError.importKey = res.data;
        this.getStatusImport(res.data);
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    this.fileImport = [
      ...this.fileImport,
      {
        uid: '',
        name: file.name,
        status: 'removed',
        id: 0,
      },
    ];
    return false;
  };


  getStatusImport(idMessage: string) {
    let countInterval = 0;
    const interval = setInterval(() => {
      countInterval++;
      this.interviewService.getStatusFileInterview(idMessage).subscribe((res) => {
        if (res.data !== Constants.StatusImportInterview.pending) {
          clearInterval(interval);
          if (res.data === Constants.StatusImportInterview.error) {
            this.isLoading = false;
            this.toastrCustom.error(this.translate.instant('development.commonMessage.executionFailed'));
          } else {
            this.searchSuccess(1);
            this.searchError(1);
          }
        }
      }, (e) => {
        this.isLoading = false;
        clearInterval(interval);
      });

      if (countInterval > 50) {
        clearInterval(interval);
        this.toastrCustom.error(this.translate.instant('development.commonMessage.executionFailed'));
        this.isLoading = false;
      }
    }, 5000);
  }

  doRemoveFile = (file: NzUploadFile): boolean => {
    this.fileImport = [];
    this.dataTableError = [];
    this.dataTable = [];
    return false;
  };

  initTable() {
    this.tableConfig = {
      headers: [
        {
          title: 'development.interview-config.colTable.planCode',
          field: 'optionCode',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 100,
        },
        {
          title: 'development.interview-config.colTable.codeEmployee',
          field: 'employeeCode',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 120,
        },
        {
          title: 'development.interview-config.colTable.nameEmployee',
          field: 'employeeName',
          width: 200,
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
        },
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: userConfig.pageSize,
      pageIndex: 1,
    };
  }

  initTableError() {
    this.tableConfigError = {
      headers: [
        {
          title: 'development.interview-config.colTable.tdError',
          field: 'rowNumber',
          width: 30,
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
        },
        {
          title: 'development.interview-config.colTable.thError',
          field: 'colName',
          width: 85,
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
        },
        {
          title: 'development.interview-config.colTable.errorDescription',
          field: 'description',
          width: 110,
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
        },
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: userConfig.pageSize,
      pageIndex: 1,
    };
  }

  saveTableSuccess() {
    if (this.dataTable.length > 0) {
      this.interviewService.saveDataTableSuccess(this.idMessage).subscribe((res) => {
        this.isLoading = false;
        this.toastrCustom.success(this.translate.instant('development.commonMessage.success'));
        this.back();
      }, (e) => {
        this.isLoading = false;
        this.getMessageError(e);
      })
    }
  }

  dowloadTemplate() {
    this.isLoading = true;
    this.interviewService.dowloadTemplate().subscribe(
      (data) => {
        let urlExcel = 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,';
        urlExcel = urlExcel + data.data;
        saveAs(urlExcel, this.translate.instant(
          'development.salaryInterview.templateInterview'
        ));
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

}
