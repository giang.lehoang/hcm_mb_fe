import { Component, Injector, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Constants, ScreenType } from "@hcm-mfe/learn-development/data-access/common";
import { DataInterview, TitleEmpInterview } from '@hcm-mfe/learn-development/data-access/models';
import { InterviewService } from "@hcm-mfe/learn-development/data-access/services";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import * as moment from 'moment';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { Observable, Subscription } from 'rxjs';



@Component({
  selector: 'app-interview-ld-update',
  templateUrl: './interview-ld-update.component.html',
  styleUrls: ['./interview-ld-update.component.scss'],
})
export class InterviewLdUpdateComponent extends BaseComponent implements OnInit {
  subs: Subscription[] = [];
  typeAction: ScreenType;
  title?: string;
  listInterviewResult = [
    {
      code: 0,
      name: 'Không đạt',
    },
    {
      code: 1,
      name: 'Đạt',
    },
    {
      code: 2,
      name: 'Phù hợp với vị trí khác',
    },
  ];
  dataInterview:DataInterview[] = [];

  formInterview: FormGroup;
  isSubmitted = false;
  fileListInterview: NzUploadFile[] = [];
  listIdInterview: number[] = [];
  fileNameInterview = '';
  // titleEmpInterview: TitleEmpInterview[]= [];
  isValidTitle = false;
  contentPosition: number[] = [];
  constructor(injector: Injector, private interviewService: InterviewService) {
    super(injector);
    this.typeAction = this.route.snapshot.data['screenType'];
    this.formInterview = new FormGroup({
      optionGroups: this.fb.array([]),
      idmo: new FormControl(null, [Validators.required]),
    });
  }

  createFormGroups(data: DataInterview[], dataEdit: DataInterview[]) {
    data?.forEach((item: DataInterview, index) => {
      const itemInterview = { ...item, ...dataEdit?.find((i: DataInterview) => i.interviewId === item.interviewId) };
      const form = this.fb.group({
        proposeOptionCode: itemInterview?.proposeOptionCode,
        employeeCode: itemInterview?.employeeCode,
        employeeName: itemInterview?.employeeName,
        birthDay: itemInterview?.birthDay,
        joinDate: itemInterview?.joinDate,
        employeeTitleName: itemInterview?.employeeTitleName,
        employeeUnit: itemInterview?.employeeUnit,
        contentTitleName: itemInterview?.contentTitleName,
        contentUnit: itemInterview?.contentUnit,
        interviewNote: itemInterview?.interviewNote,
        optionId: [itemInterview?.optionId || null, Validators.required],
        interviewId: [itemInterview?.interviewId || null, Validators.required],
        interviewDate: [itemInterview?.interviewDate || new Date(), Validators.required],
        recruitment: [itemInterview?.recruitment, Validators.required],
        setup: [itemInterview?.setup || null, Validators.required],
        boardCode: [itemInterview?.boardCode || null, Validators.required],
        boardMembers: [itemInterview?.boardMembers || null, Validators.required],
        interviewPoint: [itemInterview?.interviewPoint || null],
        result: [itemInterview?.result, Validators.required],
        suitablePosition: [itemInterview?.suitablePosition || null, Validators.required],
        note: [itemInterview?.note || null],
        fileNameInterview: itemInterview?.processFile ? itemInterview?.processFile.fileName : null,
        fileDocIDInterview: itemInterview?.processFile ? itemInterview?.processFile.docId : null,
        idMO: itemInterview?.idMO,
        reasonForRefusing: itemInterview?.refuseReason,
      });
      if (this.typeAction === this.screenType.detail){
        form.disable();
      }
      (this.formInterview.controls['optionGroups'] as FormArray).push(form);
      if (itemInterview?.result !== undefined && itemInterview?.result !== null) {
        this.onChangeInterviewResults(+itemInterview?.result, index);
      }
    });
  }

  get optionGroups(): NzSafeAny {
    if (this.formInterview.get('optionGroups')) {
      return this.formInterview.get('optionGroups') as FormArray;
    }
    return null;
  }

  ngOnInit() {
    this.dataInterview = JSON.parse(this.route.snapshot.queryParams['dataInterview']);
    this.contentPosition = this.dataInterview?.map((item: DataInterview) => item?.contentPosition ?? 0);
    this.getInfoInterview(this.contentPosition, this.dataInterview);
    this.title = this.route.snapshot?.data['pageName'];
  }

  saveInterview(isSave: boolean) {
    const paramPointInterview: DataInterview[] = this.formInterview?.get('optionGroups')?.value.filter((item: DataInterview) => item.result === this.listInterviewResult[2].code && !item.suitablePosition);
    if (paramPointInterview?.length > 0) {
      this.toastrCustom.warning(this.translate.instant('development.commonMessage.E009'));
      return;
    }
    let checkValid = this.formInterview.invalid && isSave;
    const listInterviewPoint = this.formInterview?.get('optionGroups')?.value.filter((item: any) => {
      return !item.interviewPoint || item.interviewPoint <= 0 || item.interviewPoint > 10;
    });
    const paramInterviewResults = this.formInterview?.get('optionGroups')?.value.filter((item: DataInterview) => !item.result && item.result !== 0);
    if ((listInterviewPoint?.length > 0 || paramInterviewResults?.length > 0) && isSave) {
      this.toastrCustom.warning(listInterviewPoint?.length > 0 ? this.translate.instant('development.commonMessage.E007') : this.translate.instant('development.commonMessage.E017'));
      return;
    }
    const paramIdmo: DataInterview[] = this.formInterview?.get('optionGroups')?.value.filter((item: DataInterview) => !item.idMO);
    const paramIdmoIsSave: DataInterview[] = this.formInterview?.get('optionGroups')?.value.filter((item: DataInterview) => item.idMO);

    if (checkValid && paramIdmo.length > 0 && this.isValidTitle) {
      this.isSubmitted = true;
      this.isLoading = false;
      return;
    }
    const idmo = this.formInterview.get('idmo')?.value;
    if ((!idmo || !this.fileNameInterview) && this.isValidTitle && paramIdmoIsSave.length === 0) {
      this.toastrCustom.warning(this.translate.instant('development.commonMessage.E008'));
      return;
    }
    const paramSave = this.formInterview?.get('optionGroups')?.value.map((item: any) => {
      return {
        optionId: item?.optionId,
        interviewId: item?.interviewId,
        interviewDate: moment(item?.interviewDate).format('YYYY-MM-DD'),
        recruitment: item?.recruitment,
        setup: item?.setup,
        boardCode: item?.boardCode,
        boardMember: item?.boardMembers,
        interviewPoint: parseFloat((+item?.interviewPoint).toFixed(1)),
        result: item?.result,
        suitablePosition: item?.suitablePosition,
        note: item?.note || '',
        idMO: !isSave ? (idmo || item.idMO ) : item.idMO,
        fileName: !isSave ? (this.fileNameInterview || item.fileNameInterview) : item.fileNameInterview,

      };
    });

    this.isLoading = true;
    let api: Observable<any>;
    if(isSave){
      api = this.interviewService.sendInterview(paramSave, isSave);
    }else{
      api = this.interviewService.updateInterview(paramSave, isSave);
    }
    api.subscribe({
      next: (res) => {
        if (!isSave) {
          this.optionGroups.clear();
          this.getInfoInterview(this.contentPosition, this.dataInterview);
          this.toastrCustom.success(
            this.translate.instant('development.interview-config.notificationMessage.updateSuccess')
          );
        } else {
          this.toastrCustom.success(
            this.translate.instant('development.interview-config.notificationMessage.sendReusltSuccess')
          );
        }
        this.isLoading = false;
        if (isSave) {
          this.router.navigateByUrl(Constants.KeyCommons.DEVELOPMENT_INTERVIEW);
        }
      },
      error: (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }}
    );
  }


  beforeUploadInterview = (file: File | NzUploadFile): boolean => {
    this.isLoading = true;
    let formData = new FormData();
    formData.append('ids',  new Blob([JSON.stringify( this.listIdInterview)], { type: 'application/json' }));
    formData.append('files', file as File);
    formData.append('type', new Blob(['interview'], { type: 'application/json' }));
    const sub = this.interviewService.uploadFileInterview(formData).subscribe(
      (res) => {
        if (res?.data && res?.data?.length > 0) {
          this.fileNameInterview = res?.data[0].fileName;
          this.fileListInterview = [
            {
              uid: res?.data[0].docId,
              name: res?.data[0].fileName,
              status: 'removed',
              id: res?.data[0].id,
            },
          ];

        }
        this.isLoading = false;
      },
      (e) => {
        this.fileNameInterview = '';
        this.isLoading = false;
        this.getMessageError(e);
      }
    );
    this.subs.push(sub);
    return false;
  };

  downloadFile(id: number, name: string) {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;
    this.interviewService.downloadFile(id, name).subscribe(
      () => {
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

  removeFile = (file: NzUploadFile) => {
    this.fileListInterview = [];
    return true;
  }

  onChangeInterviewResults(interviewResult: number, index: number) {
    if (this.optionGroups) {
      if (interviewResult === Constants.SUITABLEPOSITION) {
        this.optionGroups.at(index).get('suitablePosition')?.addValidators(Validators.required);
      } else {
        this.optionGroups.at(index).get('suitablePosition')?.clearValidators();
      }
      this.optionGroups.at(index).get('suitablePosition')?.updateValueAndValidity();
    }
  }

  override ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }

  getInfoInterview(contentPosition: number[], param: DataInterview[]) {
    this.isLoading = true;
    this.interviewService.findTitleInterview(Constants.POSITION_CODE_INTERVIEW).subscribe(
      (res) => {
        const titleEmpInterview: TitleEmpInterview[] = res.data || [];
        if (titleEmpInterview.length > 0) {
          this.isValidTitle = titleEmpInterview.filter((item) => contentPosition?.includes(+item.code)).length > 0;
        }
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    param?.forEach((item: DataInterview) => {
      this.listIdInterview.push(item?.interviewId!);
    });
    this.interviewService.detailSendInterview(this.listIdInterview).subscribe(
      (res) => {
        this.isLoading = false;
        const dataEdit = res?.data || [];
        this.createFormGroups(param, dataEdit);
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

}
