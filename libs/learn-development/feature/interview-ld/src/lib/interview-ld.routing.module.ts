import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {InterviewLdSearchComponent} from "./interview-ld-search/interview-ld-search.component";
import {InterviewLdUpdateComponent} from "./interview-ld-update/interview-ld-update.component";
import {ScreenType} from "@hcm-mfe/learn-development/data-access/common";
import {InterviewLdImportComponent} from "./interview-ld-import/interview-ld-import.component";

const routes: Routes = [
  {
    path: '',
    component: InterviewLdSearchComponent
  },
  {
    path: 'update',
    component: InterviewLdUpdateComponent,
    data: {
      pageName: 'development.pageName.interview-ld-update',
      breadcrumb: 'development.breadcrumb.interview-ld-update',
      screenType: ScreenType.Update,
    },
  },
  {
    path: 'detail',
    component: InterviewLdUpdateComponent,
    data: {
      pageName: 'development.pageName.interview-ld-detail',
      breadcrumb: 'development.breadcrumb.interview-ld-detail',
      screenType: ScreenType.Detail,
    },
  },
  {
    path: 'import',
    component: InterviewLdImportComponent,
    data: {
      pageName: 'development.pageName.interview-ld-import',
      breadcrumb: 'development.breadcrumb.interview-ld-import',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InterviewLdRoutingModule {}
