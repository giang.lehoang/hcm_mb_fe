import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import {LearnDevelopmentFeatureShellModule} from "@hcm-mfe/learn-development/feature/shell";
import {pages} from "./pages";
import {LearnDevelopmentPipesModule} from "@hcm-mfe/learn-development/pipes";
import {PromulgateLDRoutingModule} from "./promulgate-ld-routing.module";
import {components} from "./components";
import {PopupService} from "@hcm-mfe/shared/ui/popup";

@NgModule({
  declarations: [...pages],
  exports: [...pages],
  imports: [CommonModule, LearnDevelopmentFeatureShellModule, PromulgateLDRoutingModule, LearnDevelopmentPipesModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
  providers: [PopupService]
})

export class LearnDevelopmentFeaturePromulgateLdModule {}
