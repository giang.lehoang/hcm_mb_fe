import { Component, Injector, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Constants, userConfig } from "@hcm-mfe/learn-development/data-access/common";
import { DataCommon, DataTable } from "@hcm-mfe/learn-development/data-access/models";
import { PromulgateLdService } from "@hcm-mfe/learn-development/data-access/services";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import { cleanData } from "@hcm-mfe/shared/common/utils";
import { MBTableConfig, Pageable } from "@hcm-mfe/shared/data-access/models";
import * as FileSaver from 'file-saver';
import * as moment from 'moment';
import { NzSafeAny } from 'ng-zorro-antd/core/types';


@Component({
  selector: 'app-promulgate-ld-view',
  templateUrl: './promulgate-ld-view.component.html',
  styleUrls: ['./promulgate-ld-view.component.scss'],
})
export class PromulgateLdViewComponent extends BaseComponent implements OnInit {
  isExpand = false;
  iconStatus = Constants.IconStatus.DOWN;
  dataTable: DataTable[] = [];
  dataCommon: DataCommon = {};
  params = {
    proposeCode: null, //Mã đề xuất
    formCode: null, // loại đề xuất
    timeApprove: null, // thời gian phê duyệt
    approveStartDate: null,
    releaseFromDate: null,
    releaseToDate: null,
    approveEndDate: null,
    issueLevel: null, // cấp ban hành or cấp phê duyệt
    statusPublish: <string | null>null, // trạng thái ban hành
    employeeCode: null, // mã nhân viên
    employeeName: null, // họ tên
    effectiveDate: null, //ngày hiệu lực
    effectiveStartDate: null, //ngày hiệu lực
    effectiveEndDate: null, //ngày hiệu lực
    signerFullName: null, // người ký
    currentUnit: null, // đơn vị  hiện tại
    currentTitle: null, // chức danh hiện tại
    proposeUnit: null, // đơn vị đề xuất
    proposeTitle: null, // chức danh đề xuất
    // proposeOrigin: false, // Đề xuất gốc - Default
    // proposeItem: true, //	Đề xuất thành phần - Default
    // proposeGroup: true, //	Đề xuất gop - Default
    screenCode: Constants.ScreenCode.DVNS, // Default
    decisionNumber: null,
    issueContent: null,
    publishCode: null,
    issueCode: null,
    publishContent: null,
    page: 0,
    size: userConfig.pageSize,
  };
  title = '';
  statusPublishColor = Constants.statusPublishColor;
  tableConfig!: MBTableConfig;
  heightTable = { x: '100%', y: '25em'};
  @ViewChild('statusPublishDisplayTmpl', {static: true}) statusPublishDisplay!: TemplateRef<NzSafeAny>;
  @ViewChild('effectiveDateTmpl', {static: true}) effectiveDate!: TemplateRef<NzSafeAny>;
  @ViewChild('releaseDate', {static: true}) releaseDate!: TemplateRef<NzSafeAny>;

  statePromulgate = Constants.statePromulgate;
  pageable: Pageable = {
    totalElements: 0,
    currentPage: 0,
    size: userConfig.pageSize,
  };
  constructor(injector: Injector, private readonly service: PromulgateLdService) {
    super(injector);
  }

  ngOnInit() {

    this.title = this.route.snapshot?.data['pageName'];
    this.initTable();
    const params = {
      screenCode: Constants.ScreenCode.DVNS,
    };
    this.isLoading = true;
    this.service.getState(params).subscribe((data) => {
      if (data) {
        this.dataCommon = data;
        this.dataCommon.flowStatusPublishConfigList = this.convertStatus(data?.flowStatusPublishConfigList);
        if (this.dataCommon.flowStatusPublishConfigList){
          const listPendingApprove = this.dataCommon.flowStatusPublishConfigList.find(
            (item) => item.role === Constants.FlowCodeName.CHUABANHANH);
          this.params.statusPublish = listPendingApprove?.key!;
        }
      //end gom trạng thái ban thành
      }
      this.search(1);
    });
  }

  convertStatus(valueStatus: any) {
    let arrayStatus: any [] =  [];
    if (valueStatus) {
      const textStatusConsultation = [...new Set(valueStatus.map((item: any) => { return  item.role}))];
      textStatusConsultation.forEach((item) =>{
        let filterValueStatus = valueStatus.filter((i: any) => i.role === item)?.map((a: any) => { return a.key});
        arrayStatus.push({key: filterValueStatus.toString(), role: item});
      })
    }
    return arrayStatus;
  }

  search(firstPage: number) {
    this.isLoading = true;
    this.params.page = firstPage - 1;
    const newParams = { ...cleanData(this.params) };
    newParams.currentUnit = newParams.currentUnit ? newParams.currentUnit.orgId : null;
    newParams.proposeUnit = newParams.proposeUnit ? newParams.proposeUnit.orgId : null;
    newParams.releaseFromDate = newParams.timeApprove?.length > 0 ? moment(newParams.timeApprove[0]).format(Constants.FORMAT_DATE.YYYY_MM_DD) : null;
    newParams.releaseToDate = newParams.timeApprove?.length > 0 ? moment(newParams.timeApprove[1]).format(Constants.FORMAT_DATE.YYYY_MM_DD) : null;
    newParams.effectiveStartDate = newParams.effectiveDate?.length > 0
      ? moment(newParams.effectiveDate[0]).format(Constants.FORMAT_DATE.YYYY_MM_DD)
      : null;
    newParams.effectiveEndDate = newParams.effectiveDate?.length > 0
      ? moment(newParams.effectiveDate[1]).format(Constants.FORMAT_DATE.YYYY_MM_DD)
      : null;

    this.service.getAndSearch(newParams).subscribe(
      (res) => {
        let indexFix = 1 + this.params.page * this.params.size;
          this.dataTable = res.data?.datas?.content?.map((item: DataTable) => {
            return {
              ...item,
              indexFix: indexFix++,
            };
          }) || [];
        // this.dataTable = res.data?.datas?.content || [];
        // // thêm trường proposeCode: Mã đề xuất
        // this.dataTable.forEach((item) => {
        //   if (item.proposeCategory === Constants.ProposeCategory.ELEMENT) {
        //     item.proposeCode = item.proposeDetailId;
        //   }
        //   if (item.proposeCategory === Constants.ProposeCategory.GROUP ||
        //     item.proposeCategory === Constants.ProposeCategory.ORIGINAL) {
        //     item.proposeCode = item.proposeId;
        //   }
        // });
        this.tableConfig.total = res?.data?.datas?.totalElements || 0;
        this.tableConfig.pageIndex = firstPage;
        //check nếu dataTable = [] thì giảm page đi 1 tránh hiện thì trống ở  page >= 1
        this.isLoading = false;
      },
      (e) => {
        this.isLoading = false;
        this.getMessageError(e);
        this.dataTable = [];
      }
    );
  }

  collapseExpand() {
    this.isExpand = !this.isExpand;
    this.isExpand ? (this.iconStatus = Constants.IconStatus.UP) : (this.iconStatus = Constants.IconStatus.DOWN);
  }

  detail(item: DataTable) {
    //check type
    let type;
    let action = '';
    for (let i = 0; i < Constants.statePromulgate.length; i++) {
      const found = Constants.statePromulgate[i].list.find((name) => name === item.publishStatusCode);
      if (found) {
        type = Constants.statePromulgate[i].type;
        action = Constants.statePromulgate[i]?.action;
        break;
      }
    }
    if (item?.proposeCategory === Constants.ProposeCategory.ELEMENT) {
      return this.router.navigate([this.router.url, 'view'], {
        queryParams: {
          id: item?.proposeDetailId,
          proposeCategory: item?.proposeCategory,
          type: type,
          state: item?.publishStatusCode,
          action: action,
        },
      });
    }
    return this.router.navigate([this.router.url, 'view'], {
      queryParams: {
        id: item?.proposeId,
        proposeCategory: item?.proposeCategory,
        type: type,
        state: item?.publishStatusCode,
        action: action,
      },
    });
  }

  isViewDetail(code: any): boolean {
    return !Constants.ButtonStatus.includes(code);
  }

  exportExcel() {
    if (this.dataTable?.length === 0) {
      this.toastrCustom.warning(
        this.translate.instant(
          'development.proposed-unit.messages.dataTableEmpty'
        )
      );
      return;
    }
    this.isLoading = true;
    const newParams = { ...cleanData(this.params) };
    delete newParams.page;
    delete newParams.size;
    newParams.currentUnit = newParams.currentUnit ? newParams.currentUnit.orgId : null;
    newParams.proposeUnit = newParams.proposeUnit ? newParams.proposeUnit.orgId : null;
    newParams.releaseFromDate = newParams.timeApprove?.length > 0 ? moment(newParams.timeApprove[0]).format(Constants.FORMAT_DATE.YYYY_MM_DD) : null;
    newParams.releaseToDate = newParams.timeApprove?.length > 0 ? moment(newParams.timeApprove[1]).format(Constants.FORMAT_DATE.YYYY_MM_DD) : null;
    newParams.effectiveStartDate = newParams.effectiveDate?.length > 0
      ? moment(newParams.effectiveDate[0]).format(Constants.FORMAT_DATE.YYYY_MM_DD)
      : null;
    newParams.effectiveEndDate = newParams.effectiveDate?.length > 0
      ? moment(newParams.effectiveDate[1]).format(Constants.FORMAT_DATE.YYYY_MM_DD)
      : null;
    this.service.exportExcel(newParams).subscribe(
      (data) => {
        let urlExcel = 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,';
        urlExcel = urlExcel + data.data;
        FileSaver.saveAs(urlExcel, this.translate.instant('Danh sách ban hành'));
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

  getColorStatusPublish(status: any):string {
    let color: any;
    for(let i = 0; i < Constants.statusPublishColor.length; i++){
      const found = this.statusPublishColor[i].name.find(item => item === status)
      if(found){
        color = Constants.statusPublishColor[i]['color'];
        break;
      }
    }
    return color;
  }



  initTable(){
    this.tableConfig = {
      headers: [
        {
          title: 'development.proposed-unit.reportCard.decisionNumber',
          field: 'decisionNumber',
          width: 110,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.promulgate.search.issueCode',
          field: 'publishCode',
          width: 110,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.promulgate.search.proposeCode',
          field: 'proposeCode',
          width: 100,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.promulgate.search.issueContent',
          field: 'publishContent',
          width: 150,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.promulgate.search.form',
          field: 'formRotationName',
          width: 110,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.proposed-unit.table.statusPublish',
          field: 'statusPublishDisplay',
          width: 170,
          fixed: true,
          fixedDir: 'left',
          tdTemplate: this.statusPublishDisplay
        },
        {
          title: 'development.proposed-unit.table.empCode',
          field: 'employeeCode',
          width: 110,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.proposed-unit.table.empName',
          field: 'employeeName',
          width: 180,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.proposed-unit.table.currentUnit',
          field: 'employeeUnitPathName',
          width: 200,
        },
        {
          title: 'development.proposed-unit.table.currentTitle',
          field: 'employeeTitleName',
          width: 150,
        },
        {
          title: 'development.proposed-unit.table.proposeUnit',
          field: 'contentUnitPathName',
          width: 200,
        },
        {
          title: 'development.proposed-unit.table.proposeTitle',
          field: 'contentTitleName',
          width: 150,
        },
        {
          title: 'development.promulgate.search.effectiveDate',
          field: '',
          width: 150,
          tdTemplate: this.effectiveDate
        },
        {
          title: 'development.promulgate.search.issueLevelCode',
          field: 'issueLevelName',
          width: 120,
        },
        {
          title: 'development.promulgate.search.signerCode',
          field: 'signerFullName',
          width: 170,
        },
        {
          title: 'development.proposed-unit.table.noteWithdraw',
          field: 'withdrawNote',
          width: 200,
        },
        {
          title: 'development.promulgate.table.changeIssuePerson',
          field: 'createdBy',
          width: 150,
        },
        {
          title: 'development.promulgate.search.changeIssueDate',
          field: 'releaseDate',
          width: 150,
          tdTemplate: this.releaseDate
        },
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: userConfig.pageSize,
      pageIndex: 1,
    };
  }
}
