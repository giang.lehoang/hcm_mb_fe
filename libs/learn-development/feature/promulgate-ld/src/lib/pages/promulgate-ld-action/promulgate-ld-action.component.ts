import { Component, Injector, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { Constants, ScreenType, userConfig } from "@hcm-mfe/learn-development/data-access/common";
import {
  DataEmployee,
  FileDetail,
  FlowStatusPublishConfigList,
  ParamPromulgate,
  ProposePublishFiles
} from "@hcm-mfe/learn-development/data-access/models";
import { PromulgateLdService, ProposedUnitService } from "@hcm-mfe/learn-development/data-access/services";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import { cleanDataForm } from "@hcm-mfe/shared/common/utils";
import {MBTableConfig, Pagination} from '@hcm-mfe/shared/data-access/models';
import { ProposeUploadFileComponent } from 'libs/learn-development/feature/propose-unit/src/lib/components';
import * as moment from 'moment';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { NzUploadFile } from "ng-zorro-antd/upload";
import { forkJoin, of, Subscription } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'app-promulgate-ld-action',
  templateUrl: './promulgate-ld-action.component.html',
  styleUrls: ['./promulgate-ld-action.component.scss'],
})
export class PromulgateLdActionComponent extends BaseComponent implements OnInit {
  title?: string;
  type?: string;
  typeAction: ScreenType;
  dataTable: DataEmployee[] = [];
  params: ParamPromulgate = {};
  proposeType?: string;
  proposeID?: string | number;
  relationType?: number;
  statusPublish = '';
  statusPublishName: string | undefined;
  isCheckDecision = false;
  isCheckHasDecision = false;
  isIncomeAnnouncement = false;
  isModeInsurance = false;
  form?: FormGroup;
  listFile = [];
  listFileDecision?: FileDetail;
  listFileIncomeAnnouncement?: FileDetail;
  listFileModeInsurance?: FileDetail;
  isSubmitted = false;
  fileUpload1: NzUploadFile[] = [];
  fileUpload2: NzUploadFile[] = [];
  fileUpload3: NzUploadFile[] = [];
  statusPublishCode?: string;
  typeStatusPublish?: string;
  modalRef?: NzModalRef;
  dataReason = {
    viewDetailReasonOrCorrection: null,
    reasonOrCorrection: null,
    commentsIssueTransfer: null,
  };
  dataDropdown = Constants.DataDropdown;
  proposeCategory = '';
  categoryConst = Constants.ProposeCategory;
  publishCode = '';
  withdrawNoteName = '';
  checked = false;
  publishContent = '';
  TypeStatusPublishConst = Constants.TypeStatusPublish;
  StatusPublishConst = Constants.StatusPublishCode;
  DocumentTypeConst = Constants.DocumentTypeIssue;
  UploadPopupName = Constants.UploadFileEmployeeIssue;
  fileTypeIssue = Constants.FileTypeIssue;
  proposePublishFiles: ProposePublishFiles[] = [];
  subs: Subscription[] = [];
  action = '';
  id = 0;

  proposeLogs = [];
  stateSwitch = false;
  panels = [
    {
      active: false,
      disabled: false,
    },
  ];

  constructor(injector: Injector, private readonly service: PromulgateLdService, private readonly serviceProposed: ProposedUnitService) {
    super(injector);
    this.initForm();
    this.state = this.route.snapshot.queryParams['state'];
    this.proposeCategory = this.route.snapshot.queryParams['proposeCategory'];
    this.typeAction = this.route.snapshot.data['screenType'];
    this.typeStatusPublish = this.route?.snapshot?.queryParams['type'];
    this.action = this.route?.snapshot?.queryParams['action'];
    this.id = this.route.snapshot.queryParams['id'];
  }

  flowStatusPublishConfigList: FlowStatusPublishConfigList[] = [];
  tableConfig!: MBTableConfig;
  heightTable = { x: '100%', y: '25em'};
  @ViewChild('groupCBTmpl', {static: true}) groupCheckbox!: TemplateRef<NzSafeAny>;
  @ViewChild('groupCBHeaderTmpl', {static: true}) groupCheckboxHeader!: TemplateRef<NzSafeAny>;
  @ViewChild('joinDateTmpl', {static: true}) joinDate!: TemplateRef<NzSafeAny>;
  @ViewChild('effectiveDateTmpl', {static: true}) effectiveDate!: TemplateRef<NzSafeAny>;
  @ViewChild('endDateTmpl', {static: true}) endDate!: TemplateRef<NzSafeAny>;
  @ViewChild('reportApproveTmpl', {static: true}) reportApprove!: TemplateRef<NzSafeAny>;
  @ViewChild('TBIncomeTmpl', {static: true}) TBIncome!: TemplateRef<NzSafeAny>;
  @ViewChild('TBInsuranceTmpl', {static: true}) TBInsurance!: TemplateRef<NzSafeAny>;
  @ViewChild('maximumTimeTmpl', {static: true}) maximumTime!: TemplateRef<NzSafeAny>;

  listTypeMaximumTime = Constants.TypeMaximumTime;
  typeMaximumTime = Constants.TypeMaximumValue.MONTH;
  quantityMap: Map<number, DataEmployee> = new Map<number, DataEmployee>();

  TBIncomeList: NzUploadFile[] = [];
  TBInsuranceList: NzUploadFile[] = [];
  ProposeCategoryConst = Constants.ProposeCategory;
  proposeTypeID: number = 0;
  pageNumber = 0;
  pageSize =  userConfig.pageSize;
  heightTableLogs = { x: '50vw', y: '16em'};
  tableLogs!: MBTableConfig;
  pagination = new Pagination();
  @ViewChild('dateLog', {static: true}) dateLog!: TemplateRef<NzSafeAny>;
  iconStatus = Constants.IconStatus.UP;
  isExpand = false;

  isViewDetail(): boolean {
    return Constants.ButtonStatus.includes(this.state);
  }

  isViewBtn(code: any): boolean {
    return Constants.ButtonStatus.includes(code);
  }

  checkApiView() {
    const checkApiDetail = this.isViewDetail();
    if (checkApiDetail) {
      return this.service.getDetail(this.params!).pipe(catchError(() => of(undefined)));
    } else {
      return this.service.getPromulgate(this.params!).pipe(catchError(() => of(undefined)));
    }
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.title = this.route.snapshot?.data['pageName'];
    this.type = this.route?.snapshot?.data['screenType'] || ScreenType.Create;
    this.initTable();
    this.initLogs();
    //Thêm property để call đúng api duyệt ban hành
    this.params = {
      id: this.id,
      proposeCategory: this.proposeCategory,
      type: this.typeStatusPublish,
      state: this.state,
      action: this.action,
      screenCode: Constants.ScreenCode.DVNS,
    };
    switch (this.proposeCategory) {
      case Constants.ProposeCategory.ELEMENT:
        this.proposeTypeID = Constants.ProposeTypeIDIssue.ELEMENT;
        break;
      case Constants.ProposeCategory.GROUP:
        this.proposeTypeID = Constants.ProposeTypeIDIssue.GROUP;
        break;
    }
    forkJoin({
      listFile: this.service.getFile(this.params).pipe(catchError(() => of(undefined))),
      data: this.checkApiView(),
      log: this.service.getLogsPage(this.id, this.proposeTypeID, Constants.ScreenCode.DVNS, this.pageNumber, this.pageSize).pipe(catchError(() => of(undefined))),
    }).subscribe(
      (res) => {
        this.isLoading = false;
        this.proposeLogs = res?.log?.data?.content || [];
        this.tableLogs.total = res?.log?.data?.totalElements || 0;
        this.tableLogs.pageIndex = 1;
        this.dataTable = res.data?.data?.employeeList || [];
        this.mapFileEmployeeList();
        this.mapReportApprove();
        this.publishCode = res.data?.data?.publishCode || '';
        this.publishContent = res.data?.data?.publishContent || '';
        this.proposeType = res.data?.data?.proposeType || '';
        this.proposeID = this.dataTable[0]?.proposeCode || '';
        this.relationType = res.data?.data?.relationType?.toString();
        this.statusPublish = res.data?.data?.statusPublish;
        this.statusPublishCode = this.statusPublish;
        this.flowStatusPublishConfigList = res.data?.data?.flowStatusPublishConfigList;
        this.withdrawNoteName = res?.data?.data?.withdrawNoteName || '';
        this.statusPublishName = this.flowStatusPublishConfigList.find(
          (item: FlowStatusPublishConfigList) => item.key === res.data?.data?.statusPublish)?.role;
        this.listFile = res.listFile?.data;
        this.isCheckDecision = this.proposeCategory === this.categoryConst.GROUP
          ? true
          : res.data?.data?.employeeList[0]?.isDecision;
        this.isCheckHasDecision = res.data?.data?.employeeList[0]?.hasDecision;
        this.isIncomeAnnouncement = res.data?.data?.employeeList[0]?.isIncome;
        this.isModeInsurance = res.data?.data?.employeeList[0]?.isInsurance;
        this.listFile = res.listFile?.data;
        this.form?.patchValue(res.data?.data?.proposeDecisionDTO);
        this.form?.get('signerCode')?.patchValue(res.data?.data?.signerName);
        this.form?.get('effectiveDate')?.patchValue(res.data?.data?.proposeCorrectionDTO?.effectiveDate);
        this.form?.get('effectiveDateOld')?.patchValue(res.data?.data?.proposeCorrectionDTO?.effectiveDateOld);
        this.form?.get('comment')?.patchValue(res.data?.data?.proposeDecisionDTO?.comments || '');
        this.form?.get('decisionNumber')?.patchValue(res.data?.data?.proposeDecisionDTO?.decisionNumber || '');
        this.form?.get('signedDate')?.patchValue(res.data?.data?.proposeDecisionDTO?.signedDate || '');
        this.dataReason = {
          viewDetailReasonOrCorrection: res.data?.data?.proposeCorrectionDTO?.viewDetailReasonOrCorrection,
          reasonOrCorrection: res.data?.data?.proposeCorrectionDTO?.reasonOrCorrection,
          commentsIssueTransfer: res.data?.data?.proposeCorrectionDTO?.commentsIssueTransfer,
        };
        this.listFile.forEach((item: FileDetail) => {
          switch (item?.documentType) {
            case Constants.DocumentTypeIssue.DECISION:
              this.listFileDecision = item;
              if (item.decisionPublishId) {
                this.fileUpload1 = [...this.fileUpload1,
                  {
                    uid: item?.decisionPublishId?.toString(),
                    name: item?.decisionPublishName,
                    status: 'done',
                    id: item?.proposeDetailId,
                  }];
              }
              break;
            case Constants.DocumentTypeIssue.TBINCOME:
              this.listFileIncomeAnnouncement = item;
              this.isIncomeAnnouncement = true;
              if (item.decisionPublishId) {
                this.fileUpload2 = [...this.fileUpload2,
                  {
                    uid: item?.decisionPublishId?.toString(),
                    name: item?.decisionPublishName,
                    status: 'done',
                    id: item?.proposeDetailId,
                  }];
              }
              break;
            case Constants.DocumentTypeIssue.TBINSURANCE:
              this.listFileModeInsurance = item;
              if (item.decisionPublishId) {
                this.fileUpload3 = [...this.fileUpload3, {
                  uid: item?.decisionPublishId?.toString(),
                  name: item?.decisionPublishName,
                  status: 'done',
                  id: item?.proposeDetailId,
                }];
              }
              break;
          }
        });
      },
      (err) => {
        this.isLoading = false;
        this.getMessageError(err);
      }
    );
  }

  checkProposeCategory() {
    if (this.proposeCategory === Constants.ProposeCategory.GROUP) {
      return this.translate.instant(Constants.KeyCommons.COMMON_DECISION);
    } else {
      return this.translate.instant(Constants.KeyCommons.DECISION);
    }
  }

  get getFormEffectiveDateOld() {
    return this.form?.get('effectiveDateOld');
  }

  //các trạng thái bắt buộc nhập số quyết định và ngày ký
  get canEditDateSign(): boolean {
    return Constants.EditSignDateIssue.includes(this.statusPublish);
  }

  get cancelPromulgate(): boolean {
    const listState = ['12.1AA', '12.2AA', '12.1BA', '12.2BA', '12.1CA', '12.2CA'];
    return listState.includes(this.statusPublish);
  }

  initForm() {
    this.form = this.fb.group({
      effectiveDate: [null],
      effectiveDateOld: [null],
      signedDate: [null, Validators.required],
      signerCode: [null],
      comment: [null],
      decisionNumber: [null, Validators.required],
    });
  }

  //ban hành
  submit() {
    if (this.canEditDateSign) {
      this.isSubmitted = true;
      if (this.form?.valid) {
        this.showSubmitIssuePopup();
      } else {
        this.toastrCustom.error(this.translate.instant(Constants.KeyCommons.NUMBER_DECISION_SIGN_DATE_VALID));
      }
    } else {
      this.showSubmitIssuePopup();
    }
  }

  showSubmitIssuePopup() {
    let data = { ...cleanDataForm(this.form!) };
    data['effectiveDate'] = data.effectiveDate ? moment(data.effectiveDate).format(Constants.FORMAT_DATE.YYYY_MM_DD) : data.effectiveDate;
    data['signedDate'] = data.signedDate ? moment(data.signedDate).format(Constants.FORMAT_DATE.YYYY_MM_DD) : data.signedDate;
    data['id'] = this.params?.id;
    data['proposeCategory'] = this.params?.proposeCategory;
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.promulgate.section.promulgateConfirm'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        this.service.handPublish(data).subscribe(
          (res) => {
            this.isLoading = false;
            this.toastrCustom.success(this.translate.instant('development.promulgate.section.promulgateSuccess'));
            this.router.navigateByUrl(Constants.KeyCommons.DEVELOPMENT_PROMULGATE);
          },
          (err) => {
            this.getMessageError(err);
            this.isLoading = false;
          }
        );
      },
    });
  }

  rePromulgate() {
    if (this.canEditDateSign) {
      this.isSubmitted = true;
      if (this.form?.valid) {
        this.showRePromulgatePopup();
      } else {
        this.toastrCustom.error(this.translate.instant(Constants.KeyCommons.NUMBER_DECISION_SIGN_DATE_VALID));
      }
    } else {
      this.showRePromulgatePopup();
    }
  }

  showRePromulgatePopup() {
    let data = { ...cleanDataForm(this.form!) };
    data['effectiveDate'] = data.effectiveDate ? moment(data.effectiveDate).format(Constants.FORMAT_DATE.YYYY_MM_DD) : data.effectiveDate;
    data['signedDate'] = data.signedDate ? moment(data.signedDate).format(Constants.FORMAT_DATE.YYYY_MM_DD) : data.signedDate;
    data['id'] = this.params?.id;
    data['proposeCategory'] = this.params?.proposeCategory;

    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.promulgate.section.rePromulgateConfirm'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        this.service.rehHandPublish(data).subscribe(
          () => {
            this.isLoading = false;
            this.toastrCustom.success(this.translate.instant('development.promulgate.section.promulgateSuccess'));
            this.router.navigateByUrl('/development/promulgate');
          },
          (err) => {
            this.getMessageError(err);
            this.isLoading = false;
          }
        );
      },
    });
  }

  //hủy ban hành
  cancel() {
    if (this.canEditDateSign) {
      this.isSubmitted = true;
      if (this.form?.valid) {
        this.showCancelIssuePopup();
      } else {
        this.toastrCustom.error(this.translate.instant('development.commonMessage.numberDecisionSignDateValid'));
      }
    } else {
      this.showCancelIssuePopup();
    }
  }

  showCancelIssuePopup() {
    const body = { ...this.params, comment: this.form?.get('comment')?.value };
    delete body.action;
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.promulgate.section.cancelPromulgateConfirm'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        this.service.cancelPublish(body).subscribe(
          (res) => {
            this.isLoading = false;
            this.toastrCustom.success(this.translate.instant('development.promulgate.section.cancelPromulgate'));
            this.router.navigateByUrl(Constants.KeyCommons.DEVELOPMENT_PROMULGATE);
          },
          (err) => {
            this.isLoading = false;
            this.getMessageError(err);
          }
        );
      },
    });
  }

  beforeUpload1 = (file: File | NzUploadFile): boolean => {
    this.isLoading = true;
    const formData = new FormData();
    formData.append('proposeCategory', this.proposeCategory);
    if (this.params?.id) {
      formData.append('id', this.params?.id?.toString());
    }
    formData.append('type', Constants.FileTypeIssue.DECISION_PUBLISH);
    formData.append('documentType', Constants.DocumentTypeIssue.DECISION.toString());
    formData.append('files', file as File);
    this.service.upLoadFile(formData).subscribe(
      (res) => {
        this.fileUpload1 = [
          ...this.fileUpload1,
          {
            uid: res.data[0]?.decisionPublishId,
            name: res.data[0]?.decisionPublishName,
            status: 'done',
            id: res.data[0]?.id,
          },
        ];
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      });
    return false;
  };

  beforeUpload2 = (file: File | NzUploadFile) => {
    this.isLoading = true;
    const formData = new FormData();
    if (this.params?.proposeCategory && this.params.id) {
      formData.append('proposeCategory', this.params?.proposeCategory);
      formData.append('id', this.params?.id?.toString());
    }
    formData.append('type', Constants.FileTypeIssue.DECISION_PUBLISH);
    formData.append('documentType', Constants.DocumentTypeIssue.TBINCOME.toString());
    formData.append('files', file as File);
    this.service.upLoadFile(formData).subscribe(
      (res) => {
        this.fileUpload2 = [
          ...this.fileUpload2,
          {
            uid: res.data[0]?.decisionPublishId,
            name: res.data[0]?.decisionPublishName,
            status: 'done',
            id: res.data[0]?.id,
          },
        ];
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      });
    return false;
  };

  beforeUpload3 = (file: File | NzUploadFile) => {
    this.isLoading = true;
    const formData = new FormData();
    if (this.params?.proposeCategory && this.params.id) {
      formData.append('proposeCategory', this.params?.proposeCategory);
      formData.append('id', this.params?.id?.toString());
    }
    formData.append('type', Constants.FileTypeIssue.DECISION_PUBLISH);
    formData.append('documentType', Constants.DocumentTypeIssue.TBINSURANCE.toString());
    formData.append('files', file as File);
    this.service.upLoadFile(formData).subscribe(
      (res) => {
        this.fileUpload3 = [
          ...this.fileUpload3,
          {
            uid: res.data[0]?.decisionPublishId,
            name: res.data[0]?.decisionPublishName,
            status: 'done',
            id: res.data[0]?.id,
          },
        ];
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      });
    return false;
  };

  disabledEffectiveDate = (current: Date) => {
    const today = new Date().getTime();
    return current.getTime() <= today;
  };

  disabledDateSign = (current: Date) => {
    const today = new Date().getTime();
    let effectiveDate = this.form?.controls['effectiveDate']?.value || null;
    if (!effectiveDate) {
      return current.getTime() >= today;
    }
    effectiveDate = new Date(effectiveDate).getTime();
    if (today > effectiveDate) {
      return current.getTime() >= effectiveDate;
    }
    return current.getTime() >= today;
  };

  downloadFile(id: number, name: string) {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;
    this.service.downloadFile(id, name).subscribe(
      () => {
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

  downloadFilePromulgate = (file: NzUploadFile) => {
    this.serviceProposed.downloadFile(+file?.uid, file?.name).subscribe(
      () => {
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }



  initTable() {
    this.tableConfig = {
      headers: [
        {
          title: 'development.promulgate.search.form',
          field: 'formName',
          width: 110,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.proposed-unit.table.empCode',
          field: 'empCode',
          width: 110,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.proposed-unit.table.empName',
          field: 'fullName',
          width: 180,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.proposed-unit.detail.employee.joinCompanyDate',
          field: 'joinCompanyDate',
          width: 120,
          tdTemplate: this.joinDate
        },
        {
          title: 'development.proposed-unit.table.currentUnit',
          field: 'orgPathName',
          width: 200,
        },
        {
          title: 'development.proposed-unit.table.currentTitle',
          field: 'jobName',
          width: 150,
        },
        {
          title: 'development.proposed-unit.table.currentLevel',
          field: 'employeeLevel',
          width: 110,
        },
        {
          title: 'development.proposed-unit.table.proposeUnit',
          field: 'contentUnitPathName',
          width: 200,
        },
        {
          title: 'development.proposed-unit.table.proposeTitle',
          field: 'contentTitleName',
          width: 150,
        },
        {
          title: 'development.proposed-unit.table.proposeLevel',
          field: 'contentLevel',
          width: 110,
        },
        {
          title: 'development.proposed-unit.table.noteWithdraw',
          field: 'withdrawNoteName',
          width: 160,
        },
        {
          title: 'development.promulgate.search.effectiveDate',
          field: 'issueEffectiveStartDate',
          width: 120,
          tdTemplate: this.effectiveDate
        },
        {
          title: 'development.promulgate.table.minimumTime',
          field: 'minimumTime',
          width: 110,
        },
        {
          title: 'development.proposed-unit.formRotation.maximumTimeTGCV',
          field: 'maximumTime',
          width: 150,
          tdTemplate: this.maximumTime,
        },
        {
          title: 'development.promulgate.table.endDate',
          field: 'issueEffectiveEndDate1',
          width: 120,
          tdTemplate: this.endDate
        },
        {
          title: 'development.promulgate.table.reportApprove',
          field: '',
          width: 150,
          tdTemplate: this.reportApprove
        },
        {
          title: 'development.promulgate.table.TBIncome',
          field: '',
          width: 220,
          tdTemplate: this.TBIncome,
          show: this.proposeCategory === Constants.ProposeCategory.GROUP
        },
        {
          title: 'development.promulgate.table.TBInsurance',
          field: '',
          width: 150,
          tdTemplate: this.TBInsurance,
          show: this.proposeCategory === Constants.ProposeCategory.GROUP
        },
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: userConfig.pageSize,
      pageIndex: 1,
    };
  }

  //upload file trong bảng danh sách nhân sự
  openUploadPopup(title: string, id: number, documentType: number, typeIssue: string) {
    this.modalRef = this.modal.create({
      nzTitle: title,
      nzWidth: '30vw',
      nzContent: ProposeUploadFileComponent,
      nzComponentParams: {
        id: id,
        documentType: documentType,
        typeIssue: typeIssue,
      },
      nzFooter: null,
    });
    this.modalRef.componentInstance.closeEvent.subscribe(() => {
      this.modalRef?.destroy();
      this.isLoading = true;
      this.checkApiView().subscribe((res) => {
        this.dataTable = res?.data?.employeeList;
        this.mapFileEmployeeList();
        this.mapReportApprove();
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      });
    });
  }

  //tải file trong bảng danh sách nhân sự
  downloadFileEmployeeList(uid: number, fileName: string) {
    this.isLoading = true;
    this.serviceProposed.downloadFile(+uid, fileName).subscribe(
      () => {
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

  mapFileEmployeeList() {
    this.dataTable.forEach((item) => {
      this.proposePublishFiles = item?.proposePublishFiles || [];
      if (this.proposePublishFiles.length > 0) {
        const incomeObj = this.proposePublishFiles.find(
          (files) => files.documentType === Constants.DocumentTypeIssue.TBINCOME);
        if (incomeObj) {
          item.idIncome = incomeObj?.decisionSignId;
          item.nameIncome = incomeObj?.decisionSignName;
        }
        const insuranceObj = this.proposePublishFiles.find(
          (file) => file.documentType === Constants.DocumentTypeIssue.TBINSURANCE);
        if (insuranceObj) {
          item.idInsurance = insuranceObj?.decisionPublishId;
          item.nameInsurance = insuranceObj?.decisionPublishName;
        }
      }
    });
  }

  //map tờ trình bản duyệt
  mapReportApprove() {
    this.dataTable.forEach((item: DataEmployee) => {
      if (item.proposeProcessFile) {
        item.processFileId = item.proposeProcessFile.docId;
        item.processFileName = item.proposeProcessFile.fileName;
      }
    })
  }

  //download tờ trình bản duyệt
  downloadReportApprove(id: string, name: string) {
    this.isLoading = true;
    this.service.downloadFile(+id, name).subscribe(
      () => {
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

  doRemoveFileDecision = (file: NzUploadFile): boolean => {
    this.isLoading = true;
    const paramDelete = {
      id: this.params?.id?.toString(),
      proposeCategory: this.proposeCategory,
      documentType: Constants.DocumentTypeIssue.DECISION,
      type: Constants.FileTypeIssue.DECISION_PUBLISH,
      docId: file.uid,
      fileName: file.name,
    };
    this.serviceProposed.removeFileNotify(paramDelete).subscribe(
      () => {
        this.fileUpload1 = [];
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    return false;
  };

  doRemoveFileIncome = (file: NzUploadFile): boolean => {
    this.isLoading = true;
    const paramDelete = {
      id: this.params?.id?.toString(),
      proposeCategory: this.proposeCategory,
      documentType: Constants.DocumentTypeIssue.TBINCOME,
      type: Constants.FileTypeIssue.DECISION_PUBLISH,
      docId: file.uid,
      fileName: file.name,
    };
    this.serviceProposed.removeFileNotify(paramDelete).subscribe(
      () => {
        this.fileUpload2 = [];
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    return false;
  };

  doRemoveFileInsurance = (file: NzUploadFile): boolean => {
    this.isLoading = true;
    const paramDelete = {
      id: this.params?.id?.toString(),
      proposeCategory: this.proposeCategory,
      documentType: Constants.DocumentTypeIssue.TBINSURANCE,
      type: Constants.FileTypeIssue.DECISION_PUBLISH,
      docId: file.uid,
      fileName: file.name,
    };
    this.serviceProposed.removeFileNotify(paramDelete).subscribe(
      () => {
        this.fileUpload3 = [];
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    return false;
  };

  //xóa file TB thu nhập, TB bảo hiểm
  removeFileEmployeeList(id: number, documentType: number, typeIssue: string) {
    this.isLoading = true;
    const paramDelete = {
      id: id,
      proposeCategory: Constants.ProposeCategory.ELEMENT,
      documentType: documentType,
      type: typeIssue,
    };
    this.serviceProposed.removeFileNotify(paramDelete).subscribe(
      () => {
        this.checkApiView().subscribe((res) => {
          this.dataTable = res?.data?.employeeList;
          this.mapFileEmployeeList();
          this.mapReportApprove();
          this.isLoading = false;
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        })
      },
      (e) => {
        this.isLoading = false;
        this.getMessageError(e);
      }
    );
  };

  expandLog() {
    this.isExpand = !this.isExpand;
    if (this.isExpand) {
      this.iconStatus = Constants.IconStatus.DOWN;
    } else {
      this.iconStatus = Constants.IconStatus.UP;
    }
  }

  private initLogs(): void {
    this.tableLogs = {
      headers: [
        {
          title: 'development.proposed-unit.proposeLogsTable.time',
          field: 'actionDate',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
          tdTemplate: this.dateLog
        },
        {
          title: 'development.proposed-unit.proposeLogsTable.personAction',
          field: 'userName',
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
        },
        {
          title: 'development.proposed-unit.proposeLogsTable.action',
          field: 'action',
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
        },
        {
          title: 'development.proposed-unit.proposeLogsTable.description',
          field: 'comment',
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 200,
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1,
    };
  }

  // lấy log phân trang
  getLog(pageIndex: number){
    this.pageNumber = pageIndex - 1;
    this.isLoading = true;
    const log = this.service.getLogsPage(this.id, this.proposeTypeID, Constants.ScreenCode.DVNS, this.pageNumber, this.pageSize)
      .subscribe((log) => {
        this.proposeLogs = log?.data?.content || [];
        this.tableLogs.total = log?.data?.totalElements || 0;
        this.tableLogs.pageIndex = pageIndex;
        this.isLoading = false;
      }, (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      })
    this.subs.push(log);
  }
}
