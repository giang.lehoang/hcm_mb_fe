import { PromulgateLdActionComponent } from './promulgate-ld-action/promulgate-ld-action.component';
import { PromulgateLdViewComponent } from './promulgate-ld-view/promulgate-ld-view.component';

export const pages = [PromulgateLdViewComponent, PromulgateLdActionComponent];



export * from './promulgate-ld-view/promulgate-ld-view.component';
export * from './promulgate-ld-action/promulgate-ld-action.component';
