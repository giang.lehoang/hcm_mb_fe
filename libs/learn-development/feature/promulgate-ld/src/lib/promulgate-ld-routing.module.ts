import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PromulgateLdActionComponent, PromulgateLdViewComponent } from './pages';
import {ScreenType} from "@hcm-mfe/learn-development/data-access/common";

const routes: Routes = [
  {
    path: '',
    component: PromulgateLdViewComponent,
  },
  {
    path: 'view',
    component: PromulgateLdActionComponent,
    data: {
      pageName: 'development.pageName.promulgate-detail',
      breadcrumb: 'development.breadcrumb.promulgate-detail',
      screenType: ScreenType.Detail,
    },
  },
  {
    path: 'action',
    component: PromulgateLdActionComponent,
    data: {
      pageName: 'development.pageName.promulgate-action',
      breadcrumb: 'development.breadcrumb.promulgate-action',
      screenType: ScreenType.Update,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PromulgateLDRoutingModule {}
