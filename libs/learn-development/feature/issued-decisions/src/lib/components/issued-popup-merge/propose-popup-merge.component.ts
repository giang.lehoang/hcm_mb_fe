import {Component, Injector, Input} from '@angular/core';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {DataModelIssuedDecision} from "@hcm-mfe/learn-development/data-access/models";

@Component({
  selector: 'app-issued-popup-merge',
  templateUrl: './propose-popup-merge.component.html',
  styleUrls: ['./propose-popup-merge.component.scss'],
})
export class IssuedPopupMergeComponent extends BaseComponent {
  @Input() empName: string | undefined;
  @Input() empCode: string | undefined;
  @Input() publishCode: string | undefined;
  @Input() listEmployeeInfoTotal: number | undefined;

  constructor(injector: Injector) {
    super(injector);
  }


}
