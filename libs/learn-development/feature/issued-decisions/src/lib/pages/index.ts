import {IssuedDecisionsComponent} from "./issued-decisions-view/issued-decisions.component";
import {IssuedDecisionsActionComponent} from "./issued-decisions-action/issued-decisions-action.component";
export const pages = [IssuedDecisionsComponent, IssuedDecisionsActionComponent];
export * from './issued-decisions-view/issued-decisions.component';
export * from './issued-decisions-action/issued-decisions-action.component';
