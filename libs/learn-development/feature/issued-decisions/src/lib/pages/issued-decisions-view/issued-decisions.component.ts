import { Component, Injector, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Constants, maxInt32, SessionKey, userConfig, Utils } from "@hcm-mfe/learn-development/data-access/common";
import {
  DataCommon, DataModelIssuedDecision, DataModelMergeIssuedDecision
} from "@hcm-mfe/learn-development/data-access/models";
import { IssuedDecisionsService } from "@hcm-mfe/learn-development/data-access/services";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import { cleanData } from "@hcm-mfe/shared/common/utils";
import { MBTableConfig, Pageable, Pagination } from '@hcm-mfe/shared/data-access/models';
import { User } from "@hcm-mfe/system/data-access/models";
import * as FileSaver from 'file-saver';
import * as moment from 'moment';
import { NzSafeAny } from "ng-zorro-antd/core/types";
import { NzModalRef } from "ng-zorro-antd/modal";
import { Subscription } from "rxjs";
import {
  ProposePopupMergeComponent
} from "../../../../../propose-unit/src/lib/components/propose-popup-merge/propose-popup-merge.component";

@Component({
  selector: 'app-issued-decisions',
  templateUrl: './issued-decisions.component.html',
  styleUrls: ['./issued-decisions.component.scss']
})
export class IssuedDecisionsComponent extends BaseComponent implements OnInit {
  isExpand = false;
  iconStatus = Constants.IconStatus.DOWN;
  dataTable: DataModelIssuedDecision[] = [];
  dataCommon: DataCommon = {};
  pageName?: string;
  tableConfig!: MBTableConfig;
  pagination = new Pagination();
  @ViewChild('handle', {static: true}) action!: TemplateRef<NzSafeAny>;
  @ViewChild('tag', {static: true}) tag!: TemplateRef<NzSafeAny>;
  @ViewChild('checkboxHeader', {static: true}) checkboxHeader!: TemplateRef<NzSafeAny>;
  @ViewChild('labelSameDecision', {static: true}) labelSameDecision!: TemplateRef<NzSafeAny>;
  @ViewChild('checkbox', {static: true}) checkbox!: TemplateRef<NzSafeAny>;
  @ViewChild('actionListEmpIssued', {static: true}) actionListEmpIssued!: TemplateRef<NzSafeAny>;
  @ViewChild('releaseDate', {static: true}) releaseDate!: TemplateRef<NzSafeAny>;
  @ViewChild('publishCodeReleaseDate', {static: true}) publishCodeReleaseDate!: TemplateRef<NzSafeAny>;
  @ViewChild('effectiveDate', {static: true}) effectiveDate!: TemplateRef<NzSafeAny>;
  @ViewChild('effectiveEndDate', {static: true}) effectiveEndDate!: TemplateRef<NzSafeAny>;

  quantityMap: Map<number, DataModelIssuedDecision> = new Map();
  checked = false;
  heightTable = { x: '100vw', y: '25em'};
  params = {
    publishContent: null, //nội dung ban hành
    publishCode: null, //mã ban hành
    publishCodeReleaseFrom: null, //ngày tạo mã ban hành từ
    publishCodeReleaseTo: null, //ngày tạo mã ban hành đến
    publishReleaseFrom: null, //ngày chuyển ban hành từ
    publishReleaseTo: null, //ngày chuyển ban hành đến
    publishStatus: null, //trạng thái ban hành
    signerName: null, //tên người kí
    empCode: null, // mã nhân viên
    empName: null, // họ tên
    currentUnit: null, //đơn vị hiện tại
    currentTitle: null, //chức danh hiện tại
    proposeUnit: null, //đơn vị đề xuất
    contentTitle: null, // chức danh đề xuất
    timeApprove: null, // chức danh đề xuất
    timeCreateIssued: null, // chức danh đề xuất
    dayApproveIssued: null, // chức danh đề xuất
    page: 0,
    size: userConfig.pageSize,
  };

  pageable: Pageable = {
    totalElements: 0,
    currentPage: 0,
    size: userConfig.pageSize,
  };
  modalRef?: NzModalRef;
  flagGroup = false;
  currentUser?: User;
  subs: Subscription[] = [];
  flowCodeConstants = Constants.FlowCodeConstants
  user = '';
  ListViewIssueStatusConstans = Constants.ListViewIssueStatus

  constructor(injector: Injector, private readonly service: IssuedDecisionsService) {
    super(injector);
    if (localStorage.getItem(SessionKey.CURRENCY_USER)) {
      this.currentUser = JSON.parse(localStorage.getItem(SessionKey.CURRENCY_USER)!);
    }
  }

  displayStatusApprove(statusDisplay: string) {
    return Utils.getColorFlowStatusApporveOrPublish(statusDisplay);
  };

  ngOnInit() {
    if(this.currentUser?.username){
      this.user = this.currentUser?.username;
    }
    this.initTable();

    const params = {
      screenCode: Constants.ScreenCode.PTNL
    }
    this.isLoading = true;
    this.service.getState(params).subscribe((data) => {
      if (data) {
        this.dataCommon = data;
        this.dataCommon.flowStatusPublishConfigList = this.convertStatus(data?.flowStatusPublishConfigList);
      }
      this.search(1);
    });
  }

  convertStatus(valueStatus: any) {
    let arrayStatus: any = [];
    if (valueStatus) {
      const textStatusConsultation = [
        ...new Set(
          valueStatus.map((item: any) => {
            return item.role;
          })
        ),
      ];
      textStatusConsultation.forEach((item) => {
        let filterValueStatus = valueStatus
          .filter((i: any) => i.role === item)
          ?.map((a: any) => {
            return a.key;
          });
        arrayStatus.push({ key: filterValueStatus.toString(), role: item });
      });
    }
    return arrayStatus;
  }

  search(pageIndex: number) {
    this.isLoading = true;
    this.params.page = pageIndex - 1;
    const newParams = { ...cleanData(this.params) };
    newParams.currentUnit = newParams.currentUnit ? newParams.currentUnit.orgId : null;
    newParams.proposeUnit = newParams.proposeUnit ? newParams.proposeUnit.orgId : null;
    newParams.publishCodeReleaseFrom =
      this.params.timeCreateIssued && this.params.timeCreateIssued[0]
        ? moment(newParams.timeCreateIssued[0]).format(Constants.FORMAT_DATE.YYYY_MM_DD)
        : null;
    newParams.publishCodeReleaseTo =
      this.params.timeCreateIssued && this.params.timeCreateIssued[1]
        ? moment(this.params.timeCreateIssued[1]).format(Constants.FORMAT_DATE.YYYY_MM_DD)
        : null;
    newParams.publishReleaseFrom =
      this.params.dayApproveIssued && this.params.dayApproveIssued[0]
        ? moment(newParams.dayApproveIssued[0]).format(Constants.FORMAT_DATE.YYYY_MM_DD)
        : null;
    newParams.publishReleaseTo =
      this.params.dayApproveIssued && this.params.dayApproveIssued[1]
        ? moment(this.params.dayApproveIssued[1]).format(Constants.FORMAT_DATE.YYYY_MM_DD)
        : null;
    newParams.timeCreateIssued = null;
    newParams.dayApproveIssued = null;
    if (this.params.publishStatus && this.dataCommon.flowStatusPublishConfigList) {
      const publishSingle = this.dataCommon.flowStatusPublishConfigList.find(
        (i) => i.key === this.params.publishStatus
      );
      const role = publishSingle?.role;
      const publishMulti = this.dataCommon.flowStatusPublishConfigList.filter((i) => i.role === role);
      newParams.publishStatus = publishMulti.map((i) => i.key).toString();
    } else {
      newParams.publishStatus = null;
    }
    this.service.search(newParams).subscribe(
      (res) => {
        this.isLoading = false;
        this.dataTable = res?.data?.content;
        let indexFix = 1 + this.params.page * this.params.size;

        this.dataTable = this.dataTable.map((item) => {
          return {
            ...item,
            indexFix: this.dataTable.length === 1 && this.quantityMap.has(1) ? maxInt32 : indexFix++,
          };
        });
        this.checked = true;
        this.dataTable?.forEach((item) => {
         if (item.indexFix) {
           if (!this.quantityMap.has(item?.indexFix)) {
             this.checked = false;
           } else {
             this.quantityMap.set(item?.indexFix, item);
           }
         }
        });
        if (!this.dataTable?.length) {
          this.checked = false;
        }
        this.pageable.totalElements = res?.data?.totalElements || 0;
        this.tableConfig.total = res?.data?.totalElements || 0;
        this.pageable.currentPage = this.params.page;
        this.tableConfig.pageIndex = pageIndex;
        this.isLoading = false;
      },
      (e) => {
        this.isLoading = false;
        this.getMessageError(e)
        this.dataTable = [];
      }
    );
  }

  collapseExpand() {
    this.isExpand = !this.isExpand;
    this.isExpand ? (this.iconStatus = Constants.IconStatus.UP) : (this.iconStatus = Constants.IconStatus.DOWN);
  }

  exportExcel() {
    if (this.dataTable?.length === 0) {
      this.toastrCustom.warning(
        this.translate.instant(
          'development.proposed-unit.messages.dataTableEmpty'
        )
      );
      return;
    }
    this.isLoading = true;
    const newParams = { ...cleanData(this.params) };
    delete newParams.page;
    delete newParams.size;
    newParams.currentUnit = newParams.currentUnit ? newParams.currentUnit.orgId : null;
    newParams.proposeUnit = newParams.proposeUnit ? newParams.proposeUnit.orgId : null;
    newParams.publishCodeReleaseFrom =
      this.params.timeCreateIssued && this.params.timeCreateIssued[0]
        ? moment(newParams.timeCreateIssued[0]).format(Constants.FORMAT_DATE.YYYY_MM_DD)
        : null;
    newParams.publishCodeReleaseTo =
      this.params.timeCreateIssued && this.params.timeCreateIssued[1]
        ? moment(this.params.timeCreateIssued[1]).format(Constants.FORMAT_DATE.YYYY_MM_DD)
        : null;
    newParams.publishReleaseFrom =
      this.params.dayApproveIssued && this.params.dayApproveIssued[0]
        ? moment(newParams.dayApproveIssued[0]).format(Constants.FORMAT_DATE.YYYY_MM_DD)
        : null;
    newParams.publishReleaseTo =
      this.params.dayApproveIssued && this.params.dayApproveIssued[1]
        ? moment(this.params.dayApproveIssued[1]).format(Constants.FORMAT_DATE.YYYY_MM_DD)
        : null;
    newParams.timeCreateIssued = null;
    newParams.dayApproveIssued = null;
    this.service.exportExcel(newParams).subscribe(
      (data) => {
        let urlExcel = 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,';
        urlExcel = urlExcel + data.data;
        FileSaver.saveAs(urlExcel, this.translate.instant('development.promulgate.section.fileConfirmPromulgate'));
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

  detail(item: DataModelIssuedDecision) {
    if(item?.publishStatus) {
      if(item.groupProposeTypeId === Constants.ProposeTypeID.GROUP && this.ListViewIssueStatusConstans.indexOf(item?.publishStatus) > -1) {
        this.router.navigate([this.router.url, 'action'], {
          queryParams: {
            id: item.proposeId,
            publishCodeId: item.publishCode,
            proposeDetailId: item.proposeDetailId,
            proposeCategory: item.proposeCategory,
            publishStatus: item.publishStatus,
            groupProposeTypeId: item.groupProposeTypeId,
          },
          skipLocationChange: true,
        });
      } else if (item.groupProposeTypeId === Constants.ProposeTypeID.GROUP) {
        this.router.navigate([this.router.url, 'detail'], {
          queryParams: {
            id: item.proposeId,
            publishCodeId: item.publishCode,
            proposeDetailId: item.proposeDetailId,
            proposeCategory: item.proposeCategory,
            publishStatus: item.publishStatus,
            groupProposeTypeId: item.groupProposeTypeId,
          },
          skipLocationChange: true,
        });
      }
    }
  }

  updateCheckedSet(item: DataModelIssuedDecision, checked: boolean): void {
    if (item.indexFix) {
      if (checked) {
        this.quantityMap.set(item.indexFix, item);
      } else {
        this.quantityMap.delete(item.indexFix);
      }
    }
  }

  onItemChecked(indexFix: number, checked: boolean): void {
    const item = this.dataTable.find((itemTable: DataModelIssuedDecision) => itemTable.indexFix === indexFix);
    if (item) {
      this.updateCheckedSet(item, checked);
    }
    this.refreshCheckedStatus();
  }

  onAllChecked(value: boolean): void {
    const list = this.dataTable?.filter((item) => item?.flagGroup);
    list.forEach((item) => this.updateCheckedSet(item, value));
    this.refreshCheckedStatus();
  }

  onItemChecked2(indexFix: number, checked: boolean): void {
    const item = this.dataTable.find((item: DataModelIssuedDecision) => item.indexFix === indexFix);
    if (item) {
      this.updateCheckedSet(item, checked);
    }
    this.refreshCheckedStatus();
  }

  refreshCheckedStatus(): void {
    const list = this.dataTable?.filter((item) => item?.flagGroup);
    this.checked = list.every(({ indexFix }) => this.quantityMap.has(indexFix));
  }

  checkNumMerge() {
    if (this.quantityMap.size > 1) {
      return false;
    } else {
      return true;
    }
  }

  showCheckBox(indexFix: number) {
    const item = this.dataTable.find((itemTable: DataModelIssuedDecision) => itemTable.indexFix === indexFix);
    return item?.flagGroup;
  }

  mergePropose() {
    if (this.quantityMap.size === 0) {
      this.toastrCustom.error(this.translate.instant('development.commonMessage.chooseMin'));
      return;
    }
    let listPropose: DataModelIssuedDecision[] = [];
    this.quantityMap.forEach((item) => {
      if (item.groupProposeTypeId === Constants.ProposeTypeID.ORIGINAL || item.groupProposeTypeId === Constants.ProposeTypeID.GROUP) {
        delete item.proposeDetailId;
      } else {
        delete item.proposeId;
      }
      listPropose.push(item);
    });
    const listIssuedDecision: DataModelMergeIssuedDecision[] = listPropose?.map((item) => {
      return {
        proposeTypeCode: item?.formGroup,
        groupProposeTypeId: item?.groupProposeTypeId,
        flow: item?.publishStatus,
        proposeId: item?.proposeId,
        proposeDetailId: item?.proposeDetailId,
        createdBy: item?.createdBy
      };
    });
    this.modalRef = this.modal.create({
      nzTitle: this.translate.instant('development.proposed-unit.messages.mergeConfirm'),
      nzContent: ProposePopupMergeComponent,
      nzFooter: null,
    });
    this.modalRef.componentInstance.eventoptionNotes.subscribe((optionNotes: string) => {
      const screeenCode = Constants.ScreenCode.PTNL
      this.service.mergePropose(listIssuedDecision, screeenCode, optionNotes).subscribe(
        () => {
          this.toastrCustom.success(this.translate.instant('development.proposed-unit.messages.mergeSuccess'));
          this.quantityMap.clear();
          this.search(1);
          this.modalRef?.destroy();
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
          this.modalRef?.destroy();
        }
      );
    });
  }

  deleteIssuedGroup(indexFix: number){
    const item = this.dataTable.find((itemTable: DataModelIssuedDecision) => itemTable.indexFix === indexFix);
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.proposed-unit.messages.deleteIssue'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        if (item?.proposeId) {
          const sub = this.service.deleteIssuedGroup(item?.proposeId).subscribe(
            () => {
              this.isLoading = false;
              this.modalRef?.destroy();
              this.toastrCustom.success(this.translate.instant('development.proposed-unit.messages.deleteIssueSuccess'));
              this.search(1)
            },
            (e) => {
              this.isLoading = false;
              this.modalRef?.destroy();
              this.getMessageError(e);
            }
          );
          this.subs.push(sub)
        }
      },
    });
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'development.approvement-propose.search.merge',
          field: 'formCode',
          fixed: true,
          fixedDir: 'left',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 40,
          thTemplate: this.checkboxHeader,
          tdTemplate: this.checkbox,
        },
        {
          title: 'development.proposed-unit.table.issueCode',
          field: 'publishCode',
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 100,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.promulgate.search.issueContent',
          field: 'publishContent',
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.promulgate.table.proposeCode',
          field: 'proposeCode',
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 100,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.promulgate.table.formCode',
          field: 'formRotationName',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 100,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.promulgate.table.empCode',
          field: 'employeeCode',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 110,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.promulgate.table.empName',
          field: 'employeeName',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
          fixed: true,
          fixedDir: 'left',

        },
        {
          title: 'development.promulgate.table.unit',
          field: 'employeeUnitPathName',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
        },
        {
          title: 'development.promulgate.table.title',
          field: 'employeeTitleName',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
        },
        {
          title: 'development.promulgate.table.proposeUnit',
          field: 'contentUnitPathName',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
        },
        {
          title: 'development.promulgate.table.proposeTitle',
          field: 'contentTitleName',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
        },
        {
          title: 'development.promulgate.table.signerCode',
          field: 'signerFullName',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
        },
        {
          title: 'development.proposed-unit.table.noteWithdraw',
          field: 'withdrawNote',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
        },
        {
          title: 'development.proposed-unit.table.issuer',
          field: 'createdBy',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 120,
        },
        {
          title: 'development.proposed-unit.table.releaseDate',
          field: 'releaseDate',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 120,
          tdTemplate: this.releaseDate,
        },
        {
          title: 'development.proposed-unit.table.timeCreateIssued',
          field: 'publishCodeReleaseDate',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 120,
          tdTemplate: this.publishCodeReleaseDate,
        },
        {
          title: 'development.approvement-issue.search.statusPublish',
          field: 'publishStatusDisplay',
          tdTemplate: this.tag,
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 180,
        },
        {
          title: 'development.promulgate.search.effectiveDate',
          field: 'effectiveDate',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 120,
          tdTemplate: this.effectiveDate,
        },
        {
          title: 'development.proposed-unit.table.followQD',
          field: 'sameDecision',
          tdTemplate: this.labelSameDecision,
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 100,
        },
        {
          title: 'development.proposed-unit.table.timeMax',
          field: 'effectivePeriod',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 120,
        },
        {
          title: 'development.proposed-unit.table.endDate',
          field: 'effectiveEndDate',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
          tdTemplate: this.effectiveEndDate,
        },
        {
          title: '',
          field: '',
          tdTemplate: this.actionListEmpIssued,
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 50,
          fixed: true,
          fixedDir: 'right',
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1,
    };
  }



}
