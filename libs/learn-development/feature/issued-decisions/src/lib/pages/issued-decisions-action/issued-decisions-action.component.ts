import { Component, EventEmitter, Injector, Input, Output, TemplateRef, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import {
  Constants,
  maxInt32,
  ScreenType,
  SessionKey,
  userConfig,
  Utils
} from "@hcm-mfe/learn-development/data-access/common";
import {
  DataModelIssuedDecision, IssueTransfer, JobDTOS,
  ListFileTemplate, RequestCancelPropose, StatusList
} from "@hcm-mfe/learn-development/data-access/models";
import {
  IssuedDecisionsService,
  ProposedUnitService
} from "@hcm-mfe/learn-development/data-access/services";
import { CancelIssueComponent, ProposeTemplateComponent } from "@hcm-mfe/learn-development/feature/propose-unit";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import { MBTableConfig, Pagination } from "@hcm-mfe/shared/data-access/models";
import { User } from "@hcm-mfe/system/data-access/models";
import * as FileSaver from 'file-saver';
import * as moment from 'moment';
import { NzSafeAny } from "ng-zorro-antd/core/types";
import { NzModalRef } from 'ng-zorro-antd/modal';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { forkJoin, of, Subscription } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { IssuedPopupMergeComponent } from "../../components";

@Component({
  selector: 'app-issued-decisions-action',
  templateUrl: './issued-decisions-action.component.html',
  styleUrls: ['./issued-decisions-action.component.scss'],
})
export class IssuedDecisionsActionComponent extends BaseComponent {
  @Input() dataForm: any;
  @Input() proposeCategory = '';

  @ViewChild('actionListEmpIssued', {static: true}) actionListEmpIssued!: TemplateRef<NzSafeAny>;
  @ViewChild('dateLog', {static: true}) dateLog!: TemplateRef<NzSafeAny>;
  @ViewChild('labelSameDecision', {static: true}) labelSameDecision!: TemplateRef<NzSafeAny>;
  @ViewChild('releaseDateTmpl', {static: true}) releaseDate!: TemplateRef<NzSafeAny>;
  @ViewChild('issueEffectiveStartDateTmpl', {static: true}) issueEffectiveStartDate!: TemplateRef<NzSafeAny>;
  @ViewChild('issueEffectiveEndDateTmpl', {static: true}) issueEffectiveEndDate1!: TemplateRef<NzSafeAny>;

  @Output() onLoading = new EventEmitter<boolean>();

  type?: ScreenType;
  isVisible = false;
  titleModal = '';
  commentsIssueTransfer = '';
  submissionDate = new FormControl(null);
  decisionNumber = new FormControl(null);
  signedDate = new FormControl(null);
  formGroup = 0;
  isSubmitted = false;
  modalRef?: NzModalRef;
  proposeId: number | undefined;
  proposeDetailId: number | undefined;
  publishStatus: string | undefined;
  groupProposeTypeId: number | undefined;
  createdBy: string | undefined;
  currentUser?: User;
  proposeLogs = [];
  listFileNotifyDecisionBH: NzUploadFile[] = [];
  listFileNotifyDecisionQD: NzUploadFile[] = [];
  dataCommon = {
    flowStatusApproveConfigList: <StatusList[]>[],
    flowStatusPublishConfigList: <StatusList[]>[],
  };

  note = '';
  functionCode = '';
  iconStatus = Constants.IconStatus.UP;
  isExpand = false;
  screenCodeConst = Constants.ScreenCode;
  listEmployeeInfoTotal: DataModelIssuedDecision[] = [];
  listIssuedDTO: DataModelIssuedDecision[] = [];
  listIssuedId: number | undefined;
  quantityMap: Map<number, DataModelIssuedDecision> = new Map();
  pubLishCode = '';
  statusPublish = '';
  statusPublishName: string | undefined;
  publishContent = '';
  withdrawNoteName = '';
  effectiveDate = '';
  initialized = false;
  listDate: Date[] = [];
  minEffectiveDate: Date | string | undefined;
  maxEffectiveDate: Date | string | undefined;
  minEffectiveDateFomat: string | null | undefined;
  jobDTOS: JobDTOS[] = [];
  subs: Subscription[] = [];
  form = this.fb.group({
    decisionDate: [null, Validators.required],
    ghiChuChuyenBanHanh: [null, Validators.required],
    effectiveDate: [null, Validators.required],
    isSameUnit: null,
    decisionNumber: null,
    signedDate: null,
    signerFullName: null,
    minEffectiveDate: null,
    commentConfirm: null,
  });
  heightTable = { x: '50vw', y: '16em'};
  heightTableIssued = { x: '100vw', y: '25em'};
  tableConfig!: MBTableConfig;
  tableConfigIssued!: MBTableConfig;
  pagination = new Pagination();
  publishCodeId: string | number | undefined;
  screenCode = Constants.ScreenCode.PTNL
  statusPublishCode = '';
  isRemoveGroup = false;
  signerFullName: string | undefined;
  params = {
    page: 0,
    size: userConfig.pageSize,
  };
  paramDetail = {
    ghiChuChuyenBanHanh: null,
    ngayKyHieuLuc: null,
    ngayHieuLucDaChuyen: null,
  };
  publicStatusCode = Constants.PublicStatusCodeConstants
  FlowCodeConstants = Constants.FlowCodeConstants
  typeAction: ScreenType;
  pageNumber = 0;
  pageSize =  userConfig.pageSize;
  @ViewChild('tag', {static: true}) tag!: TemplateRef<NzSafeAny>;


  constructor(
    injector: Injector,
    private readonly service: ProposedUnitService,
    private readonly issuedDecisionService: IssuedDecisionsService,
  ) {
    super(injector);
    this.functionCode = this.route.snapshot?.data['code'];
    this.typeAction = this.route.snapshot.data['screenType'];
    if (localStorage.getItem(SessionKey.CURRENCY_USER)) {
      this.currentUser = JSON.parse(localStorage.getItem(SessionKey.CURRENCY_USER)!);
    }
  }




  ngOnInit(): void {
    this.isLoading = true;
    this.publishCodeId = this.route.snapshot?.queryParams['publishCodeId'];
    this.proposeDetailId = this.route.snapshot?.queryParams['proposeDetailId'];
    this.proposeId = this.route.snapshot?.queryParams['id'];
    this.publishStatus = this.route.snapshot?.queryParams['publishStatus'];
    this.groupProposeTypeId = +this.route.snapshot?.queryParams['groupProposeTypeId'];
    this.initTable();
    this.initTableListIssued();
    const id = this.groupProposeTypeId === 0 ? this.proposeId : this.proposeDetailId
    const proposeCategory = this.groupProposeTypeId === 0 ? Constants.ProposeCategory.GROUP : Constants.ProposeCategory.ELEMENT
    const proposeType = this.publishCodeId ? Constants.ProposeTypeID.ISSUE.toString() : ''
    const action = Constants.ActionIssueDecisionScreen.CBH
      if (id) {
        const sub = forkJoin([
          this.typeAction === this.screenType.update ? this.issuedDecisionService.actionIssueDecision(id, this.screenCode, proposeCategory, action)
            .pipe(catchError(() => of(undefined))) :
            this.issuedDecisionService.viewDetail(id, this.screenCode, proposeCategory, action)
            .pipe(catchError(() => of(undefined))),
          this.service.viewDetailFile(id, proposeCategory)
            .pipe(catchError(() => of(undefined))),
          this.service.getLogsPage(id, proposeType, this.screenCode, this.pageNumber, this.pageSize).pipe(catchError(() => of(undefined))),
        ]).subscribe(
          ([res, listNotifyDecision, log]) => {
            this.isLoading = false;
            // GET LIST EMP IN LIST ISSUED
            this.listEmployeeInfoTotal = res?.data?.employeeList
            // Check btn gỡ
            this.isRemoveGroup = this.listEmployeeInfoTotal?.filter((item) => item?.isRemoveGroup === true).length > 0
            // Người kí
            this.signerFullName = this.listEmployeeInfoTotal?.find((item) => item?.signerFullName)?.signerFullName

            this.listIssuedDTO = res?.data
            this.dataCommon = res?.data
            this.pubLishCode = res?.data?.publishCode
            this.statusPublish = res?.data?.statusPublish
            this.publishContent = res?.data?.publishContent
            this.withdrawNoteName = res?.data?.withdrawNoteName
            this.createdBy = res?.data?.createdBy
            this.listIssuedId = res?.data?.id
            this.commentsIssueTransfer = res?.data?.proposeCorrectionDTO?.commentsIssueTransfer
            this.signedDate = res?.data?.proposeDecisionDTO?.signedDate
            this.decisionNumber = res?.data?.proposeDecisionDTO?.decisionNumber
            //SET INDEX
            let indexFix = 1 + this.params.page * this.params.size;
            this.listEmployeeInfoTotal = this.listEmployeeInfoTotal.map((item) => {
              return {
                ...item,
                indexFix: this.listEmployeeInfoTotal.length === 1 && this.quantityMap.has(1) ? maxInt32 : indexFix++,
              };
            });
            // SET DATE
            this.listEmployeeInfoTotal.forEach((item) => {
              if(item.issueEffectiveStartDate){
                this.listDate.push(new Date(item.issueEffectiveStartDate));
              }
            })
            // Lấy file
            const dataNotifyDecision = listNotifyDecision?.data;
            if (dataNotifyDecision?.length > 0) {
              this.listFileNotifyDecisionQD = dataNotifyDecision[0]?.decisionSignId
                ? [
                  {
                    id: dataNotifyDecision[0].id,
                    uid: dataNotifyDecision[0].decisionSignId,
                    name: dataNotifyDecision[0].decisionSignName,
                    status: 'done',
                  },
                ]
                : [];
              this.listFileNotifyDecisionBH = dataNotifyDecision[0]?.decisionPublishId
                ? [
                  {
                    id: dataNotifyDecision[0].id,
                    uid: dataNotifyDecision[0].decisionPublishId,
                    name: dataNotifyDecision[0].decisionPublishName,
                    status: 'done',
                  },
                ]
                : [];
            }
            // lấy trạng thái ban hành theo code
            const statusPubishObject = this.dataCommon?.flowStatusPublishConfigList.find(
              (item) => item.key === this.statusPublish
            );
            this.statusPublishName = statusPubishObject?.role
            this.minDate()
            this.minEffectiveDateFomat = moment(this.minEffectiveDate).format('YYYY/MM/DD') || null;
            this.form.controls['minEffectiveDate'].setValue(this.minEffectiveDateFomat);
            this.form.controls['commentConfirm'].setValue(this.commentsIssueTransfer);
            this.form.controls['signedDate'].setValue(this.signedDate);
            this.form.controls['decisionNumber'].setValue(this.decisionNumber);
            this.listEmployeeInfoTotal = this.listEmployeeInfoTotal.map((emp) => {
              return {
                ...emp,
                statusPublishNameEmp: this.dataCommon?.flowStatusPublishConfigList.find(
                  (item) => item.key === emp.statusPublishCode
                )?.role,
              }
            });
          // log
            this.proposeLogs = log?.data?.content || [];
            this.tableConfig.total = log?.data?.totalElements || 0;
            this.tableConfig.pageIndex = 1;
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
        this.subs.push(sub)
      }
  }

  // Lấy ngày kí quyết định bé nhất trong list ngày kí quyết định của list thành phần
  minDate() {
    let minDate = this.listDate[0];
    let minDateObj = this.listDate[0];
    this.listDate.forEach((item) =>
    {
      if ( item < minDateObj)
      {
        minDate = item;
        minDateObj = item;
      }
    });
    return this.minEffectiveDate = minDate;
  }

  disabledDateIssued = (dateVal: Date) => {
    if(this.minEffectiveDate){
      return dateVal.valueOf() >= this.minEffectiveDate.valueOf();
    }
    return false;
  };

  downloadFile = (file: NzUploadFile) => {
    this.service.downloadFile(+file?.uid, file?.name).subscribe(
      () => {
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  };

  showModal() {
    this.isVisible = true;
    this.titleModal = this.translate.instant('development.proposed-unit.proposeMultiple.addModal');
  }

  checkBtnDeleteIssued () {
    return this.isRemoveGroup;
  }

  checkViewEffectiveDate () {
    if(this.publishStatus){
      return Constants.ListPublicStatusCodeConstants.indexOf(this.publishStatus) > -1 && this.currentUser?.username === this.createdBy;
    }
    return false;
  }

  checkBtnChangeIssueDQ () {
    if (this.publishStatus){
      return Constants.ListPublicStatusCodeConstants.indexOf(this.publishStatus) > -1  && this.currentUser?.username === this.createdBy;
    }
    return false;
  }

  checkNoteIssue() {
    if (this.publishStatus) {
      return Constants.PublicStatusCodeNoteIssueConstants.indexOf(this.publishStatus) > -1
    }
    return false;
  }

  checkUploadDowloadQd () {
    const listIssuedDecisions = Constants.ListIssuedDecision
    return listIssuedDecisions.includes(this.publishStatus!) && this.currentUser?.username === this.createdBy;
  }

  showModalTemplate(listFileTemplate: ListFileTemplate[]) {
    this.isLoading = false;
    const listDecision = listFileTemplate?.filter((item) =>
      item?.typeCode === Constants.TemplateType.QD && (item?.subTypeCode !== Constants.TemplateType.PLDCDD && item?.subTypeCode !== Constants.TemplateType.TDLV));
    const listNotDecision = listFileTemplate?.filter((item) =>
      item?.typeCode !== Constants.TemplateType.QD ||
      (item?.typeCode === Constants.TemplateType.QD && (item?.subTypeCode === Constants.TemplateType.PLDCDD || item?.subTypeCode === Constants.TemplateType.TDLV)));
    if (listDecision?.length > 1) {
      this.modalRef = this.modal.create({
        nzTitle: this.translate.instant('development.proposed-unit.messages.pickFileExport'),
        nzWidth: 800,
        nzContent: ProposeTemplateComponent,
        nzComponentParams: {
          listDecision: listDecision
        },
        nzFooter: null,
      });
      this.modalRef.componentInstance.eventIdFile.subscribe((idFile: number) => {

        this.modalRef?.destroy();
        this.isLoading = true;
        const itemFileDecision = listDecision.find((item) => item?.id === +idFile)
        const listNewFileDecision =
          [...listNotDecision,
            itemFileDecision];
        this.dowloadAllFile(listNewFileDecision as ListFileTemplate[]);
        if (this.modalRef) {
          this.modalRef.componentInstance.closeEvent.subscribe((data:boolean) => {
            if (!data) {
              this.modalRef?.destroy();
            }
          })
        }
      })
    }else {
      this.isLoading = true;
      this.dowloadAllFile(listFileTemplate);
    }
  }

  dowloadAllFile (listNewFileDecision: ListFileTemplate[]) {
    const listNewTemplateCode = listNewFileDecision.map((item) => {
      return item.templateCode
    });
    this.service.updateTemplateUsed(listNewTemplateCode).subscribe();
    listNewFileDecision?.forEach((item ) => {
      this.service.callBaseUrl(item?.linkTemplate!).subscribe(
        (data) => {
          const contentDisposition = data?.headers.get('content-disposition');
          const filename = contentDisposition?.split(';')[1]?.split('filename')[1]?.split('=')[1]?.trim();
          const blob = new Blob([data.body], {
            type: 'application/octet-stream',
          });
          FileSaver.saveAs(blob, filename);
          this.isLoading = false;
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        }
      );
    });
  }

  // Xuất quyết định.
  dowloadStatement(action: number) {
    const params = {
      groupProposeType: Constants.ProposeTypeID.ISSUE,
      action: action,
      screenCode: this.screenCode,
    };
    let listFile = [];
    if(this.listIssuedId) {
      this.isLoading = true;
      const sub = this.service.exportReportDecision(this.listIssuedId, params).subscribe(
        (res) => {
          listFile = res?.data;
          if (!listFile || listFile?.length === 0) {
            this.toastrCustom.error(this.translate.instant('development.proposed-unit.messages.noFile'));
            this.isLoading = false;
          } else {
            this.showModalTemplate(listFile as ListFileTemplate[])
          }
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        }
      );
      this.subs.push(sub);
    }
  }


  expandLog() {
    this.isExpand = !this.isExpand;
    if (this.isExpand) {
      this.iconStatus = Constants.IconStatus.DOWN;
    } else {
      this.iconStatus = Constants.IconStatus.UP;
    }
  }

  override ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
    this.modalRef?.destroy();
  }

  beforeUploadSign = (file: File | NzUploadFile): boolean => {
    const id = this.groupProposeTypeId === 0 ? this.proposeId : this.proposeDetailId
    const proposeCategory = this.groupProposeTypeId === 0 ? Constants.ProposeCategory.GROUP : Constants.ProposeCategory.ELEMENT
    if (id) {
      const formData = new FormData();
      formData.append('proposeCategory', proposeCategory);
      formData.append('files', file as File);
      formData.append('id', id.toString());
      formData.append('type', 'DECISION_SIGN');
      formData.append('documentType', '1');

      this.service.uploadFileHandle(formData).subscribe(
        (res) => {
          this.listFileNotifyDecisionQD = [
            ...this.listFileNotifyDecisionQD,
            {
              uid: res.data[0]?.decisionSignId,
              name: res.data[0]?.decisionSignName,
              status: 'done',
              id: res.data[0]?.id,
            },
          ];
        },
        (e) => {
          this.getMessageError(e);
        }
      );
    }
    return false;
  };

  doRemoveFileSign = (file: NzUploadFile): boolean => {
    this.isLoading = true;
    const paramDelete = {
      id: this.proposeId,
      proposeCategory: this.proposeCategory,
      documentType: 1,
      type: 'DECISION_SIGN',
      docId: file.uid,
      fileName: file.name,
    };
    const sub = this.service.removeFileNotify(paramDelete).subscribe(
      (res) => {
        this.listFileNotifyDecisionQD = [];
        this.isLoading = false;
      },
      (e) => {
        this.isLoading = false;
        this.getMessageError(e);
      }
    );
    this.subs.push(sub);
    return false;
  };

  deleteElementInIssued(indexFix: number) {
    const item = this.listEmployeeInfoTotal.find((itemTable: DataModelIssuedDecision) => itemTable.indexFix === indexFix);
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.proposed-unit.messages.deleteIssuedDecision'),
      nzWidth: 800,
      nzContent: IssuedPopupMergeComponent,
      nzComponentParams: {
        empName: item?.fullName,
        empCode: item?.empCode,
        publishCode: item?.publishCode,
        listEmployeeInfoTotal: this.listEmployeeInfoTotal.length,
      },
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL2_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.CONTINUE_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        const proposeDetailId = item?.id;
        if (this.listIssuedId && proposeDetailId) {
          const sub = this.issuedDecisionService.deletePropose(this.listIssuedId, proposeDetailId).subscribe(
            () => {
              this.toastrCustom.success(this.translate.instant('development.proposed-unit.messages.issuedSuccess'));
              this.isLoading = false;
              this.modalRef?.destroy();
              if(this.listEmployeeInfoTotal.length === 2) {
                this.router.navigateByUrl(Constants.KeyCommons.DEVELOPMENT_ISSUED_DECISIONS);
              } else if (this.listEmployeeInfoTotal.length > 2) {
                if(this.proposeId) {
                  this.issuedDecisionService.actionIssueDecision(this.proposeId, this.screenCode, Constants.ProposeCategory.GROUP, Constants.ActionIssueDecisionScreen.CBH).subscribe((res) =>
                      {
                        // GET LIST EMP IN LIST ISSUED
                        this.listEmployeeInfoTotal = res?.data?.employeeList
                      });
                }
              }
            },
            (e) => {
              this.getMessageError(e);
              this.isLoading = false;
            }
          );
          this.subs.push(sub);
        }
      }
    });
  }

  deleteIssuedGroup(){
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.proposed-unit.messages.deleteIssue'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        if (this.listIssuedId) {
          const sub = this.issuedDecisionService.deleteIssuedGroup(this.listIssuedId).subscribe(
            () => {
              this.toastrCustom.success(this.translate.instant('development.proposed-unit.messages.deleteIssueSuccess'));
              this.router.navigateByUrl(Constants.KeyCommons.DEVELOPMENT_ISSUED_DECISIONS);
            },
            (e) => {
              this.getMessageError(e);
            }
          );
          this.subs.push(sub);
        }
      },
    });
  }

  // chuyển ban hành
  transferToIssue() {
    if (this.publishStatus === (Constants.FlowCodeConstants.DongY10A ||
      Constants.PublicStatusCodeConstants.issueTransfer12dot4 ||
      Constants.PublicStatusCodeConstants.issueTransfer12dot3A)) {
      if (!this.form.controls['minEffectiveDate'].value) {
        this.toastrCustom.error(this.translate.instant('development.commonMessage.effectiveDateValidate'));
        return;
      }
    } else if (!this.paramDetail.ghiChuChuyenBanHanh || !this.form.controls['minEffectiveDate'].value){
      this.toastrCustom.error(this.translate.instant('development.commonMessage.changeIssueManyRequired'));
      return;
    } else if (!this.paramDetail.ghiChuChuyenBanHanh && this.publishStatus === (
      Constants.PublicStatusCodeConstants.issueTransfer12dot4 ||
      Constants.PublicStatusCodeConstants.issueTransfer12dot3A)) {
      this.toastrCustom.error(this.translate.instant('development.proposed-unit.messages.noteIssueRequired'));
      return;
    }
    const params: IssueTransfer = {
      id: <number>this.listIssuedId,
      proposeCategory: this.groupProposeTypeId === 0 ? Constants.ProposeCategory.GROUP : Constants.ProposeCategory.ELEMENT,
      screenCode: this.screenCode,
    };
    const body = {
      effectiveDate: moment(this.form.controls['minEffectiveDate'].value).format('yyyy-MM-DD'),
      commentsIssueTransfer: this.paramDetail.ghiChuChuyenBanHanh,
      publishContent: this.publishContent,
    };
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.proposed-unit.messages.changeIssue'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL2_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.CONTINUE_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        const sub = this.issuedDecisionService.transferToIssue(body, params).subscribe(
          () => {
            this.toastrCustom.success(this.translate.instant('development.proposed-unit.messages.changeIssueSuccess'));
            this.router.navigateByUrl(Constants.KeyCommons.DEVELOPMENT_ISSUED_DECISIONS);
            this.isLoading = false;
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
        this.subs.push(sub);
      },
    });
  }

  //hủy ban hành
  requestCancelation() {
    const params: IssueTransfer = {
      id: <number>this.listIssuedId,
      proposeCategory: this.groupProposeTypeId === 0 ? Constants.ProposeCategory.GROUP : Constants.ProposeCategory.ELEMENT,
      screenCode: this.screenCode,
    };
    this.modalRef = this.modal.create({
      nzTitle: this.translate.instant('development.promulgate.section.requestCancelIssue'),
      nzWidth: '40vw',
      nzContent: CancelIssueComponent,
      nzComponentParams: {
        pubLishCode: this.pubLishCode,
      },
      nzFooter: null,
    });
    this.modalRef.componentInstance.closeEvent.subscribe((data:boolean) => {
      if (data) {
        this.isLoading = true;
        this.modalRef?.componentInstance.cancelIssueBody.subscribe((body: RequestCancelPropose) => {
          const sub = this.service.requestCancelation(params, body).subscribe(
            () => {
              this.isLoading = false;
              this.toastrCustom.success(this.translate.instant('development.cancelIssue.cancelIssueSuccess'));
              this.router.navigateByUrl(Constants.KeyCommons.DEVELOPMENT_ISSUED_DECISIONS);
            },
            (e) => {
              this.isLoading = false;
              this.getMessageError(e);
            }
          );
          this.subs.push(sub);
        });
      }
      this.modalRef?.destroy();
    });
  }

  checkBtnrequestCancelation () {
    if (this.publishStatus){
      return Constants.ListButtonDeleteIssuedCodeConstants.indexOf(this.publishStatus) <= -1  && this.currentUser?.username === this.createdBy;
    }
    return false;
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'development.proposed-unit.proposeLogsTable.time',
          field: 'actionDate',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
          tdTemplate: this.dateLog
        },
        {
          title: 'development.proposed-unit.proposeLogsTable.personAction',
          field: 'userName',
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
        },
        {
          title: 'development.proposed-unit.proposeLogsTable.action',
          field: 'action',
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
        },
        {
          title: 'development.proposed-unit.proposeLogsTable.description',
          field: 'comment',
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 200,
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1,
    };
  }

  private initTableListIssued(): void {
    this.tableConfigIssued = {
      headers: [
        {
          title: 'development.proposed-unit.table.handleCode',
          field: 'handleCode',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 80,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.proposed-unit.table.proposeCode',
          field: 'proposeCode',
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 100,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.formLD.table.formName',
          field: 'formName',
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 80,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.proposed-unit.table.statusPublish',
          field: 'statusPublishNameEmp',
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdTemplate: this.tag,
          width: 150,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.proposed-unit.table.empCode',
          field: 'empCode',
          width: 105,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.proposed-unit.table.empName',
          field: 'fullName',
          width: 150,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.proposed-unit.table.currentUnit',
          field: 'orgPathName',
          width: 100,
        },
        {
          title: 'development.proposed-unit.table.currentTitle',
          field: 'jobName',
          width: 100,
        },
        {
          title: 'development.proposed-unit.table.proposeUnit',
          field: 'contentUnitPathName',
          width: 100,
        },
        {
          title: 'development.proposed-unit.table.proposeTitle',
          field: 'contentTitleName',
          width: 100,
        },
        {
          title: 'development.proposed-unit.table.noteWithdraw',
          field: 'withdrawNoteName',
          width: 100,
        },
        {
          title: 'development.proposed-unit.table.issuer',
          field: 'issuer',
          width: 100,
        },
        {
          title: 'development.proposed-unit.table.releaseDate',
          field: 'releaseDate',
          width: 120,
          tdTemplate: this.releaseDate
        },
        {
          title: 'development.proposed-unit.table.effectiveDate',
          field: 'issueEffectiveStartDate',
          width: 120,
          tdTemplate: this.issueEffectiveStartDate
        },
        {
          title: 'development.proposed-unit.table.followQD',
          field: 'hasDecision',
          tdTemplate: this.labelSameDecision,
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 100,
        },
        {
          title: 'development.proposed-unit.table.timeMax',
          field: 'maximumTime',
          width: 120,
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
        },
        {
          title: 'development.proposed-unit.table.endDate',
          field: 'issueEffectiveEndDate1',
          tdTemplate: this.issueEffectiveEndDate1,
          width: 120,
        },
        {
          title: '',
          field: '',
          tdTemplate: this.actionListEmpIssued,
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 50,
          fixed: true,
          fixedDir: 'right',
          show: !this.checkBtnDeleteIssued()
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1,
    };
  }

  // lấy log phân trang
  getLog(pageIndex: number){
    this.pageNumber = pageIndex - 1;
    const id = this.groupProposeTypeId === 0 ? this.proposeId : this.proposeDetailId
    const proposeType = this.publishCodeId ? Constants.ProposeTypeID.ISSUE.toString() : ''
    this.isLoading = true;
    if (id) {
      const log = this.service.getLogsPage(id, proposeType, this.screenCode, this.pageNumber, this.pageSize)
        .subscribe((log) => {
          this.proposeLogs = log?.data?.content || [];
          this.tableConfig.total = log?.data?.totalElements || 0;
          this.tableConfig.pageIndex = pageIndex;
          this.isLoading = false;
        }, (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        })
      this.subs.push(log);
    }
  }

  //tag trạng thái ban hành
  displayStatusApprove(statusDisplay: string) {
    return Utils.getColorFlowStatusApporveOrPublish(statusDisplay);
  }

}
