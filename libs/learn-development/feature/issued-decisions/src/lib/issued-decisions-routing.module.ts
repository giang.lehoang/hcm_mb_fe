import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {IssuedDecisionsComponent} from './pages';
import {ScreenType} from "@hcm-mfe/learn-development/data-access/common";
import {IssuedDecisionsActionComponent} from "./pages/issued-decisions-action/issued-decisions-action.component";


const routes: Routes = [
  { path: '', component: IssuedDecisionsComponent },
  {
    path: 'action',
    component: IssuedDecisionsActionComponent,
    data: {
      pageName: 'development.pageName.issued-decisions-action',
      breadcrumb: 'development.breadcrumb.issued-decisions-action',
      screenType: ScreenType.Update,
    },
  },
  {
    path: 'detail',
    component: IssuedDecisionsActionComponent,
    data: {
      pageName: 'development.pageName.issued-decisions-action',
      breadcrumb: 'development.breadcrumb.issued-decisions-action',
      screenType: ScreenType.Detail,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IssuedDecisionsRoutingModule {}
