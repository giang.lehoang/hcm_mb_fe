import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {IssuedDecisionsRoutingModule} from "./issued-decisions-routing.module";
import {LearnDevelopmentFeatureShellModule} from "@hcm-mfe/learn-development/feature/shell";
import {pages} from "./pages";
import {PopupService} from "@hcm-mfe/shared/ui/popup";
import {components, IssuedPopupMergeComponent} from "./components";


@NgModule({
  declarations: [...pages, ...components],
  imports: [IssuedDecisionsRoutingModule,
    LearnDevelopmentFeatureShellModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
  providers: [PopupService],
})
export class LearnDevelopmentFeatureIssuedDecisionsModule {}
