import { DecimalPipe } from '@angular/common';
import { Component, Injector, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormGroup, Validators } from '@angular/forms';
import { Constants, ScreenType } from "@hcm-mfe/learn-development/data-access/common";
import { Salary } from "@hcm-mfe/learn-development/data-access/models";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import { cleanDataForm } from "@hcm-mfe/shared/common/utils";
import { NzModalRef } from 'ng-zorro-antd/modal';
import { Subscription } from 'rxjs';
import { SalaryService } from "../../../../../../data-access/services/src/lib/salary.service";

@Component({
  selector: 'app-salary-list-action',
  templateUrl: './salary-list-action.component.html',
  styleUrls: ['./salary-list-action.component.scss'],
  providers: [DecimalPipe],
})
export class SalaryListActionComponent extends BaseComponent implements OnInit {
  title?: string;
  dataSalary: Salary[] = [];
  subs: Subscription[] = [];
  form: FormGroup;
  typeAction: ScreenType;
  modalRef?: NzModalRef;
  constructor(injector: Injector, private readonly service: SalaryService, private readonly decimalPipe: DecimalPipe) {
    super(injector);
    this.typeAction = this.route.snapshot.data['screenType'];
    this.form = this.fb.group({
      editSalary: this.fb.array([]),
    });
  }

  ngOnInit() {
    this.title = this.route.snapshot?.data['pageName'];
    this.dataSalary = JSON.parse(this.route.snapshot.queryParams['dataSalary']);
    this.dataSalary.forEach((item, index) => {
      this.pushForm();
      item['isCreate'] = false;
      this.editSalaryControl[index].patchValue(item);
    });
  }

  get editSalaryControl() {
    return (this.form.get('editSalary') as FormArray).controls;
  }

  pushForm() {
    const form = this.fb.group({
      id: [],
      currentBasicRange: ['', Validators.required], //dải lương cơ bản
      currentBasicRank: [null], //bậc lương cơ bản
      currentBasicLevel: [null, [Validators.required, Validators.pattern('^[0-9]*$')]], // mức lương cơ bản
      currentBasicBenefitRate: [null, [Validators.required, Validators.pattern('^[0-9]*$')]], // Tỷ lệ hưởng luowg cơ bản
      currentBasicBenefitIncentiveCoefficient: [null, [Validators.required]], //LCB thưởng HS
      currentBasicBenefitIncentiveLevel: [null, [Validators.required, Validators.pattern('^[0-9]*$')]], // mức thưởng HS
      currentBasicBenefitIncentiveRate: [null, [Validators.required, Validators.pattern('^[0-9]*$')]], // tỷ lê hưởng thưởng HS
      currentMonthSalary: [null, Validators.pattern('^[0-9]*$')], //thu nhập tháng hiện tại
      proposeBasicRange: ['', Validators.required],
      proposeBasicRank: [null, Validators.pattern('^[0-9]*$')],
      proposeBasicLevel: [null, [Validators.required, Validators.pattern('^[0-9]*$')]], // Mức 30
      proposeBasicBenefitRate: [null, [Validators.required, Validators.pattern('^[0-9]*$')]], //31
      proposeBasicIncentiveCoefficient: [null, [Validators.required]],
      proposeBasicIncentiveLevel: [null, [Validators.required, Validators.pattern('^[0-9]*$')]], //33
      proposeBasicIncentiveRate: [null, [Validators.required, Validators.pattern('^[0-9]*$')]], //34
      proposeMonthSalary: [null, Validators.pattern('^[0-9]*$')],
      differentRate: [null, Validators.required], // tỷ lệ (chênh lệch)
      differentLevel: [null, Validators.pattern('^[0-9]*$')], //  mức (chênh lệch)
      minPerformanceSalary: [null, Validators.pattern('^[0-9]*$')],
      salaryBaseComment: [null, Validators.required],
      bonusTravel: [null, Validators.pattern('^[0-9]*$')],
      bonusTransport: [null, Validators.pattern('^[0-9]*$')],
      bonusHouseRent: [null, Validators.pattern('^[0-9]*$')],
      bonusLifeAssurance: [null, Validators.pattern('^[0-9]*$')],
      bonus1: [null, Validators.pattern('^[0-9]*$')],
      bonus2: [null, Validators.pattern('^[0-9]*$')],
      bonus3: [null, Validators.pattern('^[0-9]*$')],
      bonus4: [null, Validators.pattern('^[0-9]*$')],
      bonus5: [null, Validators.pattern('^[0-9]*$')],
      optionId: [null],
      isCreate: [false],
    });
    if (this.typeAction === this.screenType.detail) {
      form.disable();
    }
    this.editSalaryControl.push(form);
  }

  get validateFormArray() {
    const isInValid = this.editSalaryControl.find((item) => item.status === 'INVALID');
    return !!isInValid;
  }

  saveSalary() {
    const action = 'SAVE';
    if (this.validateFormArray) {
      Object.values(this.editSalaryControl).forEach((control: any) => {
        if (control.invalid) {
          for (let key in control['controls']) {
            control['controls'][key].markAsDirty();
            control['controls'][key].updateValueAndValidity();
          }
        }
      });
      return this.toastrCustom.warning(this.translate.instant('development.salary-list.messages.validate1'));
    }
    const body = cleanDataForm(this.form).editSalary; // this.form.control.editSalary is AbstractControl
    this.isLoading = true;
    let sub;
    sub = this.service.wrapperActionSalary(body, action).subscribe(
      () => {
        this.isLoading = false;
        this.toastrCustom.success(this.translate.instant('development.salary-list.messages.saveSalarySuccess'));
      },
      (err) => {
        this.getMessageError(err);
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }

  saveAndSendSalary() {
    const action = 'SEND_SALARY';
    if (this.validateFormArray) {
      Object.values(this.editSalaryControl).forEach((control: any) => {
        if (control.invalid) {
          for (let key in control['controls']) {
            control['controls'][key].markAsDirty();
            control['controls'][key].updateValueAndValidity();
          }
        }
      });
      return this.toastrCustom.warning(this.translate.instant('development.salary-list.messages.validate1'));
    }
    const body = cleanDataForm(this.form).editSalary; // this.form.control.editSalary is AbstractControl
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.salary-list.messages.sendSalaryConfirm'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        this.service.wrapperActionSalary(body, action).subscribe(
          (res) => {
            this.isLoading = true;
            this.toastrCustom.success(this.translate.instant('development.salary-list.messages.saveSendSalarySuccess'));
            this.router.navigateByUrl('/development/salary-list');
          },
          (err) => {
            this.getMessageError(err);
            this.isLoading = false;
          }
        );
      },
    });
  }

  addForm(item: any): AbstractControl {
    return this.fb.group({
      id: [null],
      currentBasicRange: [item?.value?.currentBasicRange || '', Validators.required],
      currentBasicRank: [item?.value?.currentBasicRank || null],
      currentBasicLevel: [item?.value?.currentBasicLevel || null, Validators.required],
      currentBasicBenefitRate: [item?.value?.currentBasicBenefitRate || null, Validators.required],
      currentBasicBenefitIncentiveCoefficient: [
        item?.value?.currentBasicBenefitIncentiveCoefficient || null,
        Validators.required,
      ],
      currentBasicBenefitIncentiveLevel: [item?.value?.currentBasicBenefitIncentiveLevel || null, Validators.required],
      currentBasicBenefitIncentiveRate: [item?.value?.currentBasicBenefitIncentiveRate || null, Validators.required],
      currentMonthSalary: [item?.value?.currentMonthSalary || null],
      proposeBasicRange: ['', Validators.required],
      proposeBasicRank: [null],
      proposeBasicLevel: [null, Validators.required], // Mức 30
      proposeBasicBenefitRate: [null, Validators.required], // 31
      proposeBasicIncentiveCoefficient: [null, Validators.required],
      proposeBasicIncentiveLevel: [null, Validators.required], //33
      proposeBasicIncentiveRate: [null, Validators.required], //34
      proposeMonthSalary: [null],
      differentRate: [null, Validators.required],
      differentLevel: [null],
      minPerformanceSalary: [null],
      salaryBaseComment: [null, Validators.required],
      bonusTravel: [null],
      bonusTransport: [null],
      bonusHouseRent: [null],
      bonusLifeAssurance: [null],
      bonus1: [null],
      bonus2: [null],
      bonus3: [null],
      bonus4: [null],
      bonus5: [null],
      optionId: [item?.value?.optionId || null], //optionId
      isCreate: [true],
    });
  }

  addPlanSalary(item: Salary, index: number) {
    const cloneItem = { ...item };
    const control = this.form.controls['editSalary'] as FormArray;
    control.insert(index + 1, this.addForm(control.controls[index]));
    this.dataSalary.splice(index + 1, 0, cloneItem);
    this.dataSalary[index + 1].id = null;
    this.dataSalary[index + 1].isCreate = true;
  }

  removePlanSalary(index: number) {
    const control = this.form.controls['editSalary'] as FormArray;
    control.removeAt(index);
    this.dataSalary.splice(index, 1);
  }

  formatterCurrency = (value: number): string => {
    if (!Number.isInteger(value)) {
      return '';
    }
    return this.decimalPipe.transform(value, '1.0-2')!;
  };

  parserCurrency = (value: string): string => {
    if (!value) {
      return '';
    }
    return value.replace(/\D+/g, '');
  };
}
