import { SalaryListViewComponent } from './salary-list-view/salary-list-view.component';
import { SalaryListActionComponent } from './salary-list-action/salary-list-action.component';
export const pages = [SalaryListViewComponent, SalaryListActionComponent];
export * from './salary-list-view/salary-list-view.component';
export * from './salary-list-action/salary-list-action.component';
