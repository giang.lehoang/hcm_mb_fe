import { Component, Injector, OnInit } from '@angular/core';
import { Constants, userConfig, Utils } from "@hcm-mfe/learn-development/data-access/common";
import { DataCommonSalary, Salary } from "@hcm-mfe/learn-development/data-access/models";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import { Pageable } from "@hcm-mfe/shared/data-access/models";
import * as FileSaver from 'file-saver';
import * as moment from 'moment';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { SalaryService } from "../../../../../../data-access/services/src/lib/salary.service";

@Component({
  selector: 'app-salary-list-view',
  templateUrl: './salary-list-view.component.html',
  styleUrls: ['./salary-list-view.component.scss'],
})
export class SalaryListViewComponent extends BaseComponent implements OnInit {
  scrollY = '';
  dataCommon: DataCommonSalary = {};
  dataTable: Salary[] = [];
  title?: string;
  isExpand = false;
  iconStatus = Constants.IconStatus.DOWN;
  modalRef?: NzModalRef;
  paginationText!: string;

  params: Salary = {
    sentSalaryFrom: null,
    sentSalaryTo: null,
    statusSalaryArrangement: null,
    proposeDetailId: null,
    employeeCode: null,
    employeeName: null,
    employeeUnit: null,
    employeeTitle: null,
    contentUnit: null,
    contentTitle: null,
    isFirstRequest: true,
    isRecommendAgain: true,
    time: null,
    page: 0,
    size: userConfig.pageSize,
  };
  pageable: Pageable = {
    totalElements: 0,
    currentPage: 0,
    size: userConfig.pageSize,
  };

  checked = false;
  indeterminate = false;
  quantityMap: Map<number, Salary> = new Map<number, Salary>();
  preParam: Salary = this.params;

  constructor(injector: Injector, private readonly service: SalaryService) {
    super(injector);
  }

  ngOnInit() {
    this.title = this.route.snapshot?.data['pageName'];
    this.search(true);
  }

  collapseExpand() {
    this.isExpand = !this.isExpand;
    this.isExpand ? (this.iconStatus = Constants.IconStatus.UP) : (this.iconStatus = Constants.IconStatus.DOWN);
    setTimeout(() => {
      this.scrollY = Utils.setScrollYTable('table', this.pageable.totalElements! > 15 ? Constants.HeightPaginator : 0);
    }, 100);
  }

  search(firstPage?: boolean) {
    this.isLoading = true;
    if (firstPage) {
      this.params.page = 0;
    }
    const newParams = { ...this.params };
    if (newParams?.time) {
      newParams.sentSalaryFrom = newParams?.time.length > 0 ? moment(newParams?.time[0]).format(Constants.FORMAT_DATE.YYYY_MM_DD) : '';
      newParams.sentSalaryTo = newParams?.time.length > 0 ? moment(newParams?.time[1]).format(Constants.FORMAT_DATE.YYYY_MM_DD) : '';
    }
    if (newParams?.statusSalaryArrangement) {
      newParams.statusSalaryArrangement = newParams?.statusSalaryArrangement === 'CGL' ? '0' : '1';
    }
    newParams.employeeUnit = newParams.employeeUnit ? newParams.employeeUnit?.orgId : newParams.employeeUnit;
    newParams.contentUnit = newParams.contentUnit ? newParams.contentUnit?.orgId : newParams.contentUnit;
    this.service.getAndSearch(newParams).subscribe(
      (res) => {
        this.dataCommon = res.data;
        this.dataTable = res.data?.salaryMapperDTOPage?.content;
        this.preParam = newParams;
        this.dataTable.forEach((item) => {
          if (item.requestType) {
            item.requestType = item.requestType === '0' ? 'Lần đầu' : 'Kiến nghị';
          }
          if (item?.salaryStatus) {
            item.salaryStatus = item.salaryStatus === '0' ? 'Chưa gửi lương' : 'Đã gửi lương';
          }
        });
        this.pageable.totalElements = res?.data?.salaryMapperDTOPage?.totalElements || 0;
        this.paginationText = this.translate.instant('development.commonMessage.pagination', {
          totalElements: this.pageable.totalElements,
          from: this.params.page! * this.pageable?.size! + 1,
            to: this.params.page! * this.pageable?.size! + this.dataTable.length,
        });
        this.isLoading = false;
        this.scrollY = Utils.setScrollYTable('table', this.pageable?.totalElements! > 15 ? Constants.HeightPaginator : 0);
      },
      (e) => {
        this.isLoading = false;
        this.getMessageError(e);
        this.dataTable = [];
      }
    );
  }

  sendSalary() {
    const action = 'SEND_SALARY_DS';
    if (this.quantityMap.size < 1) {
      return this.toastrCustom.error(this.translate.instant('development.salary-list.messages.greaterThanOneFile'));
    }
    let data: Salary[] = [];
    this.quantityMap.forEach((item) => data.push(item));
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.salary-list.messages.sendSalaryConfirm'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL2_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.CONTINUE_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        this.service.wrapperActionSalary(data, action).subscribe(
          (res) => {
            this.onAllChecked(false);
            this.isLoading = false;
            this.toastrCustom.success(this.translate.instant('development.salary-list.messages.sendSuccess'));
            this.search();
          },
          (err) => {
            this.onAllChecked(false);
            this.isLoading = false;
            this.toastrCustom.error(this.translate.instant('development.salary-list.messages.err'));
          }
        );
      },
    });
  }

  handleSalary() {
    if (this.quantityMap.size < 1) {
      return this.toastrCustom.error(this.translate.instant('development.salary-list.messages.greaterThanOneFile'));
    }
    let data: Salary[] = [];
    this.quantityMap.forEach((item) => data.push(item));
    this.router.navigate([this.router.url, 'update'], {
      skipLocationChange: true,
      queryParams: {
        dataSalary: JSON.stringify(data),
      },
    });
  }

  detail(item: Salary){
    let data = [];
    data.push(item);
    this.router.navigate([this.router.url, 'detail'], {
      skipLocationChange: true,
      queryParams: {
        dataSalary: JSON.stringify(data),
      },
    });
  }

  pageChange(page: number) {
    if (this.isLoading) {
      return;
    }
    this.params.page = page - 1;
    this.search();
  }

  onAllChecked(checked: boolean): void {
    this.dataTable
      .filter((item) => {
        return item.salaryStatus !== 'Đã gửi lương';
      })
      .forEach((item) => this.updateCheckedSet(item, checked));
    this.refreshCheckedStatus();
  }

  onItemChecked(item: Salary, checked: boolean): void {
    this.updateCheckedSet(item, checked);
    this.refreshCheckedStatus();
  }

  updateCheckedSet(item: Salary, checked: boolean): void {
    if (checked) {
      this.quantityMap.set(item.id!, item);
    } else {
      this.quantityMap.delete(item.id!);
    }
  }

  refreshCheckedStatus(): void {
    const listOfEnabledData: Salary[] = this.dataTable.filter((item) => {
      return item.salaryStatus !== 'Đã gửi lương';
    });
    this.checked = listOfEnabledData.every(({ id }) => this.quantityMap.has(id!));
    this.indeterminate = listOfEnabledData.some(({ id }) => this.quantityMap.has(id!)) && !this.checked;
  }

  onCurrentPageDataChange() {
    this.refreshCheckedStatus();
  }

  exportExcel() {
    if (this.dataTable?.length === 0) {
      this.toastrCustom.warning(
        this.translate.instant(
          'development.proposed-unit.messages.dataTableEmpty'
        )
      );
      return;
    }
    this.isLoading = true;
    this.service.exportExcel(this.preParam).subscribe(
      (data) => {
        let urlExcel = 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,';
        urlExcel = urlExcel + data.data;
        FileSaver.saveAs(urlExcel, this.translate.instant('development.salary-list.messages.excelFileName'));
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }
}
