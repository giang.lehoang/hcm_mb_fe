import { Component, Injector, OnInit } from '@angular/core';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";

@Component({
  selector: 'app-salary-upload',
  templateUrl: './salary-upload.component.html',
  styleUrls: ['./salary-upload.component.scss']
})
export class SalaryUploadComponent extends BaseComponent {
  fileList: NzUploadFile[] = [];
  docIdsDelete: Number[] = [];
  constructor(injector: Injector) {
    super(injector);
   }


  beforeUpload = (file: File | NzUploadFile): boolean => {
    this.fileList = this.fileList.concat(file as NzUploadFile);
    return false;
  }

  removeFile = (file: NzUploadFile) => {
    this.docIdsDelete.push(Number(file.uid));
    const index = this.fileList.indexOf(file);
    this.fileList.splice(index, 1);
    return true;
  }


}
