import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import {SalaryListRoutingModule} from "./salary-list.routing.module";
import {LearnDevelopmentFeatureShellModule} from "@hcm-mfe/learn-development/feature/shell";
import {pages} from "./pages";
import {dialogs} from "./dialog";
import {PopupService} from "@hcm-mfe/shared/ui/popup";

@NgModule({
  declarations: [...pages, ...dialogs],
  exports: [...pages, ...dialogs],
  imports: [CommonModule, SalaryListRoutingModule, LearnDevelopmentFeatureShellModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
  providers: [PopupService]
})
export class LearnDevelopmentFeatureSalaryModule {}
