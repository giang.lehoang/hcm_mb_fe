import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SalaryListViewComponent, SalaryListActionComponent } from './pages';
import {ScreenType} from "@hcm-mfe/learn-development/data-access/common";

const routes: Routes = [
  {
    path: '',
    component: SalaryListViewComponent,
  },
  {
    path: 'update',
    component: SalaryListActionComponent,
    data: {
      pageName: 'development.pageName.salary-list-update',
      breadcrumb: 'development.breadcrumb.salary-list-update',
      screenType: ScreenType.Update,
    },
  },
  {
    path: 'detail',
    component: SalaryListActionComponent,
    data: {
      pageName: 'development.pageName.salary-list-detail',
      breadcrumb: 'development.breadcrumb.salary-list-detail',
      screenType: ScreenType.Detail,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SalaryListRoutingModule {}
