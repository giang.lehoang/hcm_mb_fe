import { Component, Injector, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { FormLDService } from '@hcm-mfe/learn-development/data-access/services';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { ArraySection, FormLDCommon, FormLDContent } from '@hcm-mfe/learn-development/data-access/models';
import { cleanDataForm } from '@hcm-mfe/shared/common/utils';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-form-ld-add',
  templateUrl: './form-ld-add.component.html',
  styleUrls: ['./form-ld-add.component.scss'],
})
export class FormLDAddComponent extends BaseComponent implements OnInit {
  public form = this.fb.group({
    formCode: [null, Validators.required],
    formName: ['', Validators.required],
    sortOrder: [null, Validators.min(1)],
    formType: [null, Validators.required],
    formGroup: [[], Validators.required],
    showMinTime: false,
    showMaxTime: false,
    showSeniority: false,
    status: [null, Validators.required],
    documentTypeId: null,
  });
  dataCommon: FormLDCommon = {
    mapFormGroup: [],
    mapFormStatus: [],
    mapFormType: [],
  };
  isSubmitted = false;
  title = '';
  subs: Subscription[] = [];
  constructor(injector: Injector, private service: FormLDService) {
    super(injector);

  }
  ngOnInit(): void {
    this.title = this.route.snapshot?.data['pageName'];
    this.isLoading = true;
    const sub = this.service.getState().subscribe((res) => {
      this.dataCommon = res;
      this.dataCommon.mapFormGroup = this.convertStatus(res?.mapFormGroup);
      this.dataCommon.mapFormStatus = this.convertStatus(res?.mapFormStatus);
      this.dataCommon.mapFormType = this.convertStatus(res?.mapFormType);
      this.isLoading = false;
    });
    this.subs.push(sub);
    this.form.get('status')?.setValue(1);
  }

  changeFill1(e: boolean) {
    if (e) {
      this.form.get('resign')?.setValue(false, { emitEvent: false });
      this.form.get('resign')?.disable();
      this.form
        .get('newExternalRecruitment')
        ?.setValue(false, { emitEvent: false });
      this.form.get('newExternalRecruitment')?.disable();
    } else if (!this.form.get('resignExternal')?.value) {
      this.form.get('resign')?.enable();
      this.form.get('newExternalRecruitment')?.enable();
    }
  }

  changeFill2(e: boolean) {
    if (e) {
      this.form.get('newRecruitment')?.setValue(false, { emitEvent: false });
      this.form.get('newRecruitment')?.disable();
      this.form.get('resignExternal')?.setValue(false, { emitEvent: false });
      this.form.get('resignExternal')?.disable();
    } else if (!this.form.get('newExternalRecruitment')?.value) {
      this.form.get('newRecruitment')?.enable();
      this.form.get('resignExternal')?.enable();
    }
  }

  changeFill3(e: boolean) {
    if (e) {
      this.form.get('newRecruitment')?.setValue(false, { emitEvent: false });
      this.form.get('newRecruitment')?.disable();
      this.form.get('resignExternal')?.setValue(false, { emitEvent: false });
      this.form.get('resignExternal')?.disable();
    } else if (!this.form.get('resign')) {
      this.form.get('newRecruitment')?.enable();
      this.form.get('resignExternal')?.enable();
    }
  }

  changeFill4(e: boolean) {
    if (e) {
      this.form.get('resign')?.setValue(false, { emitEvent: false });
      this.form.get('resign')?.disable();
      this.form
        .get('newExternalRecruitment')
        ?.setValue(false, { emitEvent: false });
      this.form.get('newExternalRecruitment')?.disable();
    } else if (!this.form.get('newRecruitment')?.value) {
      this.form.get('resign')?.enable();
      this.form.get('newExternalRecruitment')?.enable();
    }
  }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      this.isLoading = true;
      const request: FormLDContent = cleanDataForm(this.form);
      const sub = this.service.addFormLD(request).subscribe(
        () => {
          this.toastrCustom.success(
            this.translate.instant('development.formLD.messages.addSuccess')
          );
          this.location.back();
          this.isLoading = false;
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        }
      );
      this.subs.push(sub);
    }
  }

  convertStatus(valueStatus: ArraySection) {
    let arrayStatus: ArraySection[] = [];
    if (valueStatus) {
      const textStatusConsultation = [...new Set(Object.values(valueStatus))];
      const arrayStatusConsultation = Object.entries(valueStatus);
      textStatusConsultation.forEach((item) => {
        let filterValueStatus = arrayStatusConsultation
          .filter((i) => i[1] === item)
          ?.map((a) => {
            return a[0];
          });
        arrayStatus.push({ code: +filterValueStatus.toString(), name: item });
      });
    }
    return arrayStatus;
  }



  override ngOnDestroy() {
    this.subs.forEach((s) => s.unsubscribe());
  }
}
