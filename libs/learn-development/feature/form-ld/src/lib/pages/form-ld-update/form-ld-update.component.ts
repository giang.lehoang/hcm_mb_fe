import { Component, Injector, Input, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { forkJoin, Subscription } from 'rxjs';
import { FormLDService } from '@hcm-mfe/learn-development/data-access/services';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import {
  ArraySection,
  FormLDCommon,
  FormLDContent,
} from '@hcm-mfe/learn-development/data-access/models';

import { cleanDataForm } from '@hcm-mfe/shared/common/utils';
import { CustomValidators } from '@hcm-mfe/shared/common/validators';

@Component({
  selector: 'app-form-ld-update',
  templateUrl: './form-ld-update.component.html',
  styleUrls: ['./form-ld-update.component.scss'],
})
export class FormLDUpdateComponent extends BaseComponent implements OnInit {
  @Input() detail: any;
  subs: Subscription[] = [];
  formContent?: FormLDContent;
  public form = this.fb.group({
    formCode: [null, Validators.required],
    formName: '',
    sortOrder: [null, Validators.min(1)],
    formType: null,
    formGroup: [null, Validators.required],
    showMinTime: false,
    showMaxTime: false,
    showSeniority: false,
    status: null,
    code: null,
    documentTypeId: null,
  });
  isSubmitted = false;
  code?: number;
  title = '';

  dataCommon: FormLDCommon = {
    mapFormGroup: [],
    mapFormStatus: [],
    mapFormType: [],
  };

  constructor(
    injector: Injector,
    private activatedRoute: ActivatedRoute,
    private service: FormLDService
  ) {
    super(injector);

  }

  ngOnInit(): void {
    this.title = this.route.snapshot?.data['pageName'];
    const subRoute = this.activatedRoute.queryParams.subscribe((params) => {
      this.code = params['code'];
    });
    this.subs.push(subRoute);
    if (this.code) {
      this.isLoading = true;
      const sub = forkJoin([
        this.service.getState(),
        this.service.getFormLDByCode(this.code),
      ]).subscribe(
        ([res, detail]) => {
          this.dataCommon = res;
          this.dataCommon.mapFormGroup = this.convertStatus(res?.mapFormGroup);
          this.dataCommon.mapFormStatus = this.convertStatus(
            res?.mapFormStatus
          );
          this.dataCommon.mapFormType = this.convertStatus(res?.mapFormType);
          this.formContent = detail;
          this.formContent.formGroup = this.formContent.formGroup?.map((item) => +item);
          this.form.patchValue(this.formContent || {});
          this.isLoading = false;
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        }
      );
      this.subs.push(sub);
    }
  }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      const request: FormLDContent = cleanDataForm(this.form);
      this.isLoading = true;
      const sub = this.service.updateFormLD(request).subscribe(
        () => {
          this.toastrCustom.success(
            this.translate.instant('development.formLD.messages.updateSuccess')
          );
          this.location.back();
          this.isLoading = false;
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        }
      );
      this.subs.push(sub);
    }
  }



  changeFill1(e: boolean) {
    if (e) {
      this.form.get('resign')?.setValue(false, { emitEvent: false });
      this.form.get('resign')?.disable();
      this.form
        .get('newExternalRecruitment')
        ?.setValue(false, { emitEvent: false });
      this.form.get('newExternalRecruitment')?.disable();
    } else if (!this.form.get('resignExternal')?.value) {
      this.form.get('resign')?.enable();
      this.form.get('newExternalRecruitment')?.enable();
    }
  }

  changeFill2(e: boolean) {
    if (e) {
      this.form.get('newRecruitment')?.setValue(false, { emitEvent: false });
      this.form.get('newRecruitment')?.disable();
      this.form.get('resignExternal')?.setValue(false, { emitEvent: false });
      this.form.get('resignExternal')?.disable();
    } else if (!this.form.get('newExternalRecruitment')?.value) {
      this.form.get('newRecruitment')?.enable();
      this.form.get('resignExternal')?.enable();
    }
  }

  changeFill3(e: boolean) {
    if (e) {
      this.form.get('newRecruitment')?.setValue(false, { emitEvent: false });
      this.form.get('newRecruitment')?.disable();
      this.form.get('resignExternal')?.setValue(false, { emitEvent: false });
      this.form.get('resignExternal')?.disable();
    } else if (!this.form.get('resign')) {
      this.form.get('newRecruitment')?.enable();
      this.form.get('resignExternal')?.enable();
    }
  }

  changeFill4(e: boolean) {
    if (e) {
      this.form.get('resign')?.setValue(false, { emitEvent: false });
      this.form.get('resign')?.disable();
      this.form
        .get('newExternalRecruitment')
        ?.setValue(false, { emitEvent: false });
      this.form.get('newExternalRecruitment')?.disable();
    } else if (!this.form.get('newRecruitment')?.value) {
      this.form.get('resign')?.enable();
      this.form.get('newExternalRecruitment')?.enable();
    }
  }

  convertStatus(valueStatus: ArraySection) {
    let arrayStatus: ArraySection[] = [];
    if (valueStatus) {
      const textStatusConsultation = [...new Set(Object.values(valueStatus))];
      const arrayStatusConsultation = Object.entries(valueStatus);
      textStatusConsultation.forEach((item) => {
        let filterValueStatus = arrayStatusConsultation
          .filter((i) => i[1] === item)
          ?.map((a) => {
            return a[0];
          });
        arrayStatus.push({ code: +filterValueStatus.toString(), name: item });
      });
    }
    return arrayStatus;
  }

  override ngOnDestroy() {
    this.subs.forEach((s) => s.unsubscribe());
  }
}
