import { Component, Injector, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Constants, userConfig, Utils } from "@hcm-mfe/learn-development/data-access/common";
import { ArraySection, BodyExcel, FormLDCommon, FormLDContent } from "@hcm-mfe/learn-development/data-access/models";
import { FormLDService } from "@hcm-mfe/learn-development/data-access/services";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import { cleanData } from "@hcm-mfe/shared/common/utils";
import { MBTableConfig, Pageable, Pagination } from "@hcm-mfe/shared/data-access/models";
import * as FileSaver from 'file-saver';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-form-ld-view',
  templateUrl: './form-ld.component.html',
  styleUrls: ['./form-ld.component.scss'],
})
export class FormLDComponent extends BaseComponent implements OnInit {
  dataTable: FormLDContent[] = [];
  scrollY = '';
  limit = userConfig.pageSize;
  title = '';
  pageable: Pageable = {
    totalElements: 0,
    currentPage: 0,
    size: userConfig.pageSize,
  };
  params: BodyExcel = {
    formCode: null,
    formName: null,
    sortOrder: null,
    formType: null,
    formGroup: null,
    status: null,
    page: 0,
    size: userConfig.pageSize,
  };

  prevParams = this.params;
  isSubmitted = false;
  modalRef: NzModalRef | undefined;
  private readonly subs: Subscription[] = [];
  dataCommon: FormLDCommon = {
    mapFormGroup: [],
    mapFormStatus: [],
    mapFormType: [],
  };
  tableConfig!: MBTableConfig;
  @ViewChild('actionTmpl', {static: true}) action!: TemplateRef<NzSafeAny>;
  @ViewChild('minTime', {static: true}) showMinTime!: TemplateRef<NzSafeAny>;
  @ViewChild('maxTime', {static: true}) showMaxTime!: TemplateRef<NzSafeAny>;
  @ViewChild('seniority', {static: true}) showSeniority!: TemplateRef<NzSafeAny>;
  pagination = new Pagination();
  heightTable = { x: '100%', y: '25em'};

  constructor(injector: Injector, private formLDService: FormLDService, private toastr: ToastrService) {
    super(injector);
  }

  ngOnInit() {
    this.initTable();
    this.isLoading = true;
    this.title = this.route.snapshot?.data['pageName'];
    this.formLDService.getState().subscribe((res) => {
      this.dataCommon = res;
      this.dataCommon.mapFormGroup = this.convertStatus(res?.mapFormGroup);
      this.dataCommon.mapFormStatus = this.convertStatus(res?.mapFormStatus);
      this.dataCommon.mapFormType = this.convertStatus(res?.mapFormType);
    });
    this.search(true);
  }

  create() {
    this.router.navigate([this.router.url, 'add']);
  }

  update(code: number) {
    this.router.navigate([this.router.url, 'update'], { queryParams: { code: code } });
  }

  pageChange(page: number) {
    if (this.isLoading) {
      return;
    }
    this.params.page = page - 1;
    this.search();
  }

  search(firstPage?: boolean) {
    if (firstPage) {
      this.params.page = 0;
    }
    this.isLoading = true;
    const newParams = { ...cleanData(this.params) };
    this.formLDService.getAllFormLD(newParams).subscribe(
      (dataFormLD) => {
        this.dataTable = dataFormLD?.data?.content || [];
        this.tableConfig.total = dataFormLD?.data?.totalElements || 0;
        this.prevParams = newParams;
        this.isLoading = false;
        this.scrollY = Utils.setScrollYTable('table', this.tableConfig.total > 15 ? Constants.HeightPaginator : 0);
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
        this.dataTable = [];
      }
    );
  }

  delete(code: string) {
    this.modalRef = this.modal.warning({
      nzTitle: this.translate.instant('development.formLD.messages.deleteTitle'),
      nzCancelText: this.translate.instant('development.consultation-config.messages.no'),
      nzOkText: this.translate.instant('development.consultation-config.messages.yes'),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        this.formLDService.deleteFormLD(code).subscribe(
          () => {
            this.toastrCustom.success(this.translate.instant('development.formLD.messages.deleteSuccess'));
            this.search();
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
      },
    });
  }

  exportExcel() {
    if (this.dataTable?.length === 0) {
      this.toastrCustom.warning(
        this.translate.instant(
          'development.proposed-unit.messages.dataTableEmpty'
        )
      );
      return;
    }
    this.isLoading = true;
    this.formLDService.exportExcel(this.prevParams).subscribe(
      (data) => {
        let urlExcel = 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,';
        urlExcel = urlExcel + data.data;
        FileSaver.saveAs(urlExcel, this.translate.instant('development.formLD.messages.excelFileName'));
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

  convertStatus(valueStatus: ArraySection) {
    let arrayStatus: ArraySection[] = [];
    if (valueStatus) {
      const textStatusConsultation = [...new Set(Object.values(valueStatus))];
      const arrayStatusConsultation = Object.entries(valueStatus);
      textStatusConsultation.forEach((item) => {
        let filterValueStatus = arrayStatusConsultation
          .filter((i) => i[1] === item)
          ?.map((a) => {
            return a[0];
          });
        arrayStatus.push({ code: +filterValueStatus.toString(), name: item });
      });
    }
    return arrayStatus;
  }

  override handleDestroy(): void {
    this.modalRef?.destroy();
  }

  initTable() {
    this.tableConfig = {
      headers: [
        {
          title: 'development.formLD.table.formCode',
          field: 'formCode',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 110,
        },
        {
          title: 'development.formLD.table.formName',
          field: 'formName',
          width: 150,
        },
        {
          title: 'development.formLD.table.formGroup',
          field: 'formGroup',
          width: 170,
        },
        {
          title: 'development.formLD.table.formType',
          field: 'formType',
          width: 150,
        },
        {
          title: 'development.formLD.table.status',
          field: 'status',
          width: 125,
        },
        {
          title: 'development.formLD.table.sortOrder',
          field: 'sortOrder',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 120,
        },
        {
          title: 'development.formLD.table.showMinTime',
          field: 'showMinTime',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          tdTemplate: this.showMinTime,
          width: 180,
        },
        {
          title: 'development.formLD.table.showMaxTime',
          field: 'showMaxTime',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          tdTemplate: this.showMaxTime,
          width: 180,
        },
        {
          title: 'development.formLD.table.showSeniority',
          field: 'showSeniority',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          tdTemplate: this.showSeniority,
          width: 180,
        },
        {
          title: '',
          field: 'code',
          tdClassList: ['text-nowrap','text-center'],
          thClassList: ['text-nowrap','text-center'],
          width: 90,
          tdTemplate: this.action,
          fixed: window.innerWidth > 1024,
          fixedDir: 'right',
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: userConfig.pageSize,
      pageIndex: 1,
    };
  }
}
