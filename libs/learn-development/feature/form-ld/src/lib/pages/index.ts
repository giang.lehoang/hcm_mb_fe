import { FormLDComponent } from './form-ld-view/form-ld.component';
import { FormLDUpdateComponent } from './form-ld-update/form-ld-update.component';
import { FormLDAddComponent } from './form-ld-add/form-ld-add.component';
export const pages = [FormLDComponent, FormLDUpdateComponent, FormLDAddComponent];
export * from "./form-ld-view/form-ld.component";
export * from "./form-ld-update/form-ld-update.component";
export * from "./form-ld-add/form-ld-add.component";
