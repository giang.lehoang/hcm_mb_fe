import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormLDUpdateComponent, FormLDAddComponent, FormLDComponent } from './pages';

const routes: Routes = [
  { path: '', component: FormLDComponent },
  {
    path: 'add',
    component: FormLDAddComponent,
    data: {
      pageName: 'development.pageName.form-ld-add',
      breadcrumb: 'development.breadcrumb.form-ld-add',
    },
  },
  {
    path: 'update',
    component: FormLDUpdateComponent,
    data: {
      pageName: 'development.pageName.form-ld-update',
      breadcrumb: 'development.breadcrumb.form-ld-update',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FormLDRoutingModule {}
