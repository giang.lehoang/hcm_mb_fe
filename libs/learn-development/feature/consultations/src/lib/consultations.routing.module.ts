import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ConsultationsMoveCreateComponent, ConsultationsMoveSearchComponent} from './pages';
import {ScreenType} from "@hcm-mfe/learn-development/data-access/common";

const routes: Routes = [
  { path: '', component: ConsultationsMoveSearchComponent },

  {
    path: 'create',
    component: ConsultationsMoveCreateComponent,
    data: {
      pageName: 'development.pageName.consultations-unit',
      breadcrumb: 'development.breadcrumb.consultations-unit',
      screenType: ScreenType.Update,
    },
  },
  {
    path: 'detail',
    component: ConsultationsMoveCreateComponent,
    data: {
      pageName: 'development.pageName.consultations-move-detail',
      breadcrumb: 'development.breadcrumb.consultations-move-detail',
      screenType: ScreenType.Detail,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConsultationsRoutingModule {}
