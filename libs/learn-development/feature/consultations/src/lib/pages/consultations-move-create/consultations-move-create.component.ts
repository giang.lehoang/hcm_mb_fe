import { Component, Injector, OnInit, TemplateRef } from '@angular/core';
import { Constants, Mode, ScreenType, userConfig } from "@hcm-mfe/learn-development/data-access/common";
import { CategoryModel, Consultion } from "@hcm-mfe/learn-development/data-access/models";
import { ConsultationService } from "@hcm-mfe/learn-development/data-access/services";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import { Pageable } from "@hcm-mfe/shared/data-access/models";
import { NzModalRef } from 'ng-zorro-antd/modal';
import { ConsultationsMoveFormComponent } from '../consultations-move-form/consultations-move-form.component';

@Component({
  selector: 'app-consultations-move-create',
  templateUrl: './consultations-move-create.component.html',
  styleUrls: ['./consultations-move-create.component.scss'],
})
export class ConsultationsMoveCreateComponent extends BaseComponent implements OnInit {
  title?: string;
  typeAction: ScreenType;
  dataCommon: any = {
    jobDTOS: <CategoryModel[]>[],
    listStatus: [],
    listTypeOffer: [],
    listStatusAppore: [],
    listStatusIssued: [],
    listStatusConsultation: [],
  };
  pageable: Pageable = {
    totalElements: 0,
    currentPage: 0,
    size: userConfig.pageSize,
  };

  iconStatus = Constants.IconStatus.DOWN;
  isExpand = true;
  dataTable = [];
  modalRef?: NzModalRef;
  itemConsulations?: Consultion;
  proposeDetailId?: number;
  listStatus: any[] = [];
  addConsult = this.translate.instant('development.list-consultations.message.addConsult');
  updateConsult = this.translate.instant('development.list-consultations.message.updateConsult');

  constructor(injector: Injector, private consultationsMoveService: ConsultationService) {
    super(injector);
    this.typeAction = this.route.snapshot.data['screenType'];
  }

  ngOnInit() {
    this.itemConsulations = JSON.parse(this.route.snapshot.queryParams['dataConsultation']);
    this.proposeDetailId = this.itemConsulations?.proposeDetailId;
    this.title = this.route.snapshot?.data['pageName'];
    this.viewConsultation(this.proposeDetailId!);
  }

  viewConsultation(id: number) {
    this.isLoading = true;
    this.consultationsMoveService.displayConsultation(id).subscribe(
      (res) => {
        this.dataTable = res.data.consultationListDTOS.content || [];
        this.listStatus =
          Object.keys(res.data.status || {}).map((key) => {
            return {
              code: key,
              name: res.data.status[key],
            };
          }) || [];
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

  sendConsultation() {
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.interview-config.notificationMessage.sendConsultationConfirm'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        this.consultationsMoveService.sendConsultation(this.proposeDetailId!).subscribe(
          () => {
            this.toastrCustom.success(
              this.translate.instant('development.interview-config.notificationMessage.sendConsultationSuccess')
            );
            this.router.navigateByUrl('development/list-consultations');
            this.isLoading = false;
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
      },
    });
  }

  showModalBalanced(footerTmpl: TemplateRef<any>, mode: string, data?: any) {
    this.modalRef = this.modal.create({
      nzTitle: mode === 'ADD' ? this.addConsult : this.updateConsult,
      nzWidth: this.getInnerWidth(1.5),
      nzContent: ConsultationsMoveFormComponent,
      nzComponentParams: {
        mode: mode === 'ADD' ? Mode.ADD : Mode.EDIT,
        data: data,
        listStatus: this.listStatus,
        proposeDetailId: this.proposeDetailId,
      },
      nzFooter: footerTmpl,
    });
    this.modalRef.afterClose.subscribe((result) =>
      result?.refresh ? this.viewConsultation(this.proposeDetailId!) : ''
    );
  }

  getInnerWidth(point: number): number {
    if (window.innerWidth > 767) {
      return window.innerWidth / point > 1100 ? 1100 : window.innerWidth / point;
    }
    return window.innerWidth;
  }

  deleteInfoConsultations(id: number) {
    this.isLoading = true;
    this.consultationsMoveService.deleteConsultation(id).subscribe(
      (res) => {
        this.viewConsultation(this.proposeDetailId!);
        this.toastrCustom.success(this.translate.instant('development.commonMessage.success'));
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }
}
