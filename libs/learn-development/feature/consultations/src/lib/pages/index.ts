import { ConsultationsMoveCreateComponent } from './consultations-move-create/consultations-move-create.component';
import { ConsultationsMoveFormComponent } from './consultations-move-form/consultations-move-form.component';
import { ConsultationsMoveSearchComponent } from './consultations-move-search/consultations-move-search.component';

export const pages = [ConsultationsMoveSearchComponent, ConsultationsMoveCreateComponent, ConsultationsMoveFormComponent];

export * from './consultations-move-create/consultations-move-create.component';
export * from './consultations-move-form/consultations-move-form.component';
export * from './consultations-move-search/consultations-move-search.component';
