import { HttpResponse } from "@angular/common/http";
import { Component, Injector, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Constants, userConfig, Utils } from "@hcm-mfe/learn-development/data-access/common";
import { CategoryModel, Consultion, DataTempalte } from "@hcm-mfe/learn-development/data-access/models";
import { ConsultationService } from "@hcm-mfe/learn-development/data-access/services";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import { cleanData } from "@hcm-mfe/shared/common/utils";
import { MBTableConfig, Pageable, Pagination } from "@hcm-mfe/shared/data-access/models";
import * as FileSaver from 'file-saver';
import * as moment from 'moment';
import { NzSafeAny } from "ng-zorro-antd/core/types";
import { forkJoin, of, Subscription } from "rxjs";
import { catchError } from "rxjs/operators";

@Component({
  selector: 'app-consultations-move-search',
  templateUrl: './consultations-move-search.component.html',
  styleUrls: ['./consultations-move-search.component.scss'],
})
export class ConsultationsMoveSearchComponent extends BaseComponent implements OnInit {
  dataTable: Consultion[] = [];
  scrollY = '';
  dataCommon: any = {
    jobDTOS: <CategoryModel[]>[],
    listStatus: [],
    listTypeOffer: [],
    listStatusAppore: [],
    listStatusPublish: [],
    listStatusConsultation: [],
  };

  pageable: Pageable = {
    totalElements: 0,
    currentPage: 0,
    size: userConfig.pageSize,
  };

  iconStatus = Constants.IconStatus.DOWN;
  isExpand = true;
  titlePage?: string;
  limit: number = userConfig.pageSize;
  params = {
    proposeDetailId: null,
    proposeType: null,
    employeeCode: null,
    employeeTitle: null,
    rangeDate: null,
    employeeName: null,
    statusApproval: null,
    statusConsultation: null,
    statusPublish: null,
    proposeTitle: null,
    searchContentUnit: null,
    searchCurrentUnit: null,
    formGroup: null,
    page: 0,
    size: userConfig.pageSize,
  };
  keyConstansts = Constants.KeyConstants;
  tableConfig!: MBTableConfig;
  pagination = new Pagination();
  heightTable = { x: '100%', y: '25em'};
  checkStatusConsultation = '';
  @ViewChild('statusApproveView', {static: true}) statusApproveView!: TemplateRef<NzSafeAny>;
  @ViewChild('sentDateView', {static: true}) sentDateView!: TemplateRef<NzSafeAny>;
  @ViewChild('statusPublishView', {static: true}) statusPublishView!: TemplateRef<NzSafeAny>;
  @ViewChild('statusConsultationView', {static: true}) statusConsultationView!: TemplateRef<NzSafeAny>;
  dataTemplateGet: DataTempalte[] = [];
  private readonly subs: Subscription[] = [];

  constructor(injector: Injector, private readonly consultationsMoveService: ConsultationService) {
    super(injector);
  }

  displayStatusApprove(statusDisplay: string) {
    if (statusDisplay.includes('Chờ') || statusDisplay.includes('Đang')) {
      return 'yellow';
    } else if (statusDisplay.includes('Đã')) {
      if (statusDisplay.includes('hủy')) {
        return 'red';
      } else {
        return 'green';
      }
    } else if (statusDisplay.includes('Hủy') || statusDisplay.includes('Từ chối')) {
      return 'red';
    } else if (statusDisplay.includes('Đồng ý') || statusDisplay.includes('đồng ý')) {
      if (statusDisplay.includes('Không') || statusDisplay.includes('không')) {
        return 'red';
      } else {
        return 'green';
      }
    } else {
      return 'gray';
    }
  }

  displayConsultationStatus(status: string) {
    if (status === 'Chưa gửi') {
      return 'gray';
    } else {
      return 'green';
    }
  }

  ngOnInit() {
    this.initTable();
    this.isLoading = true;
    this.titlePage = this.route.snapshot?.data['pageName'];
    const paramCategory = { ...cleanData(this.params) };
    const sub = this.consultationsMoveService.getState(paramCategory).subscribe((state) => {
      if (state) {
        this.dataCommon = state;
        this.dataCommon.jobDTOS = state?.jobDTOList;
        this.dataCommon.listTypeOffer = state?.proposeTypeList;
        this.dataCommon.listStatusAppore = this.convertStatus(state?.statusApproveList);
        this.dataCommon.listStatusPublish = this.convertStatus(state?.statusPublishList);
        this.dataCommon.listStatusConsultation = this.convertStatus(state?.statusMap);
      }
      this.search(1);
    });
    this.subs.push(sub);
  }

  convertStatus(valueStatus: any) {
    let arrayStatus: any[] = [];
    if (valueStatus) {
      const textStatusConsultation = [...new Set(Object.values(valueStatus))];
      const arrayStatusConsultation = Object.entries(valueStatus);
      textStatusConsultation.forEach((item) => {
        let filterValueStatus = arrayStatusConsultation
          .filter((i) => i[1] === item)
          ?.map((a) => {
            return a[0];
          });
        arrayStatus.push({ code: filterValueStatus.toString(), name: item });
      });
    }
    return arrayStatus;
  }

  expandSearch() {
    this.isExpand = !this.isExpand;
    if (this.isExpand) {
      this.iconStatus = Constants.IconStatus.DOWN;
    } else {
      this.iconStatus = Constants.IconStatus.UP;
    }
    setTimeout(() => {
      this.scrollY = Utils.setScrollYTable('table', this.pageable.totalElements! > 15 ? Constants.HeightPaginator : 0);
    }, 100);
  }

  exportExcel() {
    if (this.dataTable?.length === 0) {
      this.toastrCustom.warning(
        this.translate.instant(
          'development.proposed-unit.messages.dataTableEmpty'
        )
      );
      return;
    }
    this.isLoading = true;
    const searchParams = { ...cleanData(this.params) };
    const currentUnitId = searchParams?.searchCurrentUnit?.orgId || '';
    if (currentUnitId === '') {
      searchParams.searchCurrentUnit = currentUnitId;
    } else {
      searchParams.searchCurrentUnit = +currentUnitId;
    }
    const contentUnitId = searchParams?.searchContentUnit?.orgId || '';
    if (contentUnitId === '') {
      searchParams.searchContentUnit = contentUnitId;
    } else {
      searchParams.searchContentUnit = +contentUnitId;
    }
    searchParams.startDate =
      this.params.rangeDate && this.params.rangeDate[0] ? moment(searchParams.rangeDate[0]).format(Constants.FORMAT_DATE.YYYY_MM_DD) : null;
    searchParams.endDate =
      this.params.rangeDate && this.params.rangeDate[1] ? moment(searchParams.rangeDate[1]).format(Constants.FORMAT_DATE.YYYY_MM_DD) : null;

    searchParams.statusApproval = searchParams?.statusApproval ? searchParams?.statusApproval.split(',') : null;
    searchParams.statusPublish = searchParams?.statusPublish ? searchParams?.statusPublish.split(',') : null;
    delete searchParams.rangeDate;
    this.consultationsMoveService.exportExcel(searchParams).subscribe(
      (data) => {
        let urlExcel = 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,';
        urlExcel = urlExcel + data.data;
        FileSaver.saveAs(urlExcel, this.translate.instant('development.list-consultations.message.excelFile'));
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

  search(pageIndex: number) {
    this.params.page = pageIndex - 1;
    this.isLoading = true;
    const searchParams = { ...cleanData(this.params) };
    const currentUnitId = searchParams?.searchCurrentUnit?.orgId || '';
    if (currentUnitId === '') {
      searchParams.searchCurrentUnit = currentUnitId;
    } else {
      searchParams.searchCurrentUnit = +currentUnitId;
    }
    const contentUnitId = searchParams?.searchContentUnit?.orgId || '';
    if (contentUnitId === '') {
      searchParams.searchContentUnit = contentUnitId;
    } else {
      searchParams.searchContentUnit = +contentUnitId;
    }
    searchParams.startDate =
      this.params.rangeDate && this.params.rangeDate[0] ? moment(searchParams.rangeDate[0]).format('YYYY-MM-DD') : null;
    searchParams.endDate =
      this.params.rangeDate && this.params.rangeDate[1] ? moment(searchParams.rangeDate[1]).format('YYYY-MM-DD') : null;

    searchParams.statusApproval = searchParams?.statusApproval ? searchParams?.statusApproval.split(',') : null;
    searchParams.statusPublish = searchParams?.statusPublish ? searchParams?.statusPublish.split(',') : null;
    delete searchParams?.rangeDate;
    const sub = forkJoin([
      this.consultationsMoveService.searchConsultation(searchParams).pipe(catchError(() => of(undefined))),
      this.consultationsMoveService.getExportTemplate(searchParams).pipe(catchError(() => of(undefined))),
    ]).subscribe(
      ([res, dataTemplate]) => {
        this.dataTable = res.data?.consultationProposeDisplayDTOPage?.content;
        this.tableConfig.total = res?.data?.consultationProposeDisplayDTOPage?.totalElements || 0;
        this.tableConfig.pageIndex = pageIndex;
        this.dataTemplateGet = dataTemplate?.data;
        this.isLoading = false;
        this.scrollY = Utils.setScrollYTable('table', this.pageable.totalElements! > 15 ? Constants.HeightPaginator : 0);
      },
      () => {
        this.isLoading = false;
        this.toastrCustom.error(this.translate.instant('development.interview-config.notificationMessage.errorApi'));
        this.dataTable = [];
      }
    );
  }

  viewConsultation(data: number) {
    const detailId = this.dataTable.find((item: Consultion) => item.proposeDetailId === data)
    this.router.navigate([this.router.url, 'create'], {
      skipLocationChange: true,
      queryParams: {
        dataConsultation: JSON.stringify(detailId),
      },
    });
  }


  //xem chi tiết tham vấn
  detail(data: Consultion){
    if(data.statusConsultation === this.keyConstansts.key11){
      this.router.navigate([this.router.url, 'create'], {
        skipLocationChange: true,
        queryParams: {
          dataConsultation: JSON.stringify(data),
        },
      });
    } else {
      this.router.navigate([this.router.url, 'detail'], {
        skipLocationChange: true,
        queryParams: {
          dataConsultation: JSON.stringify(data),
        },
      });
    }
  }

  exportTemplate () {
    this.isLoading = true;
    if(this.dataTemplateGet) {
      const dataEx = this.dataTemplateGet[0];
      const dataDc = this.dataTemplateGet[1];
      const sub = forkJoin([
        this.consultationsMoveService.exportTemplate(dataEx),
        this.consultationsMoveService.exportTemplate(dataDc)
      ])
        .subscribe(([data, datafile]) => {
            this.convertFile(data)
            this.convertFile(datafile)
            this.isLoading = false;
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
      this.subs.push(sub)

    }
  }

  convertFile(data: HttpResponse<Blob>){
    const contentDisposition = data?.headers.get('content-disposition');
    const filename = contentDisposition?.split(';')[1]?.split('filename')[1]?.split('=')[1]?.trim();
    if(data?.body) {
      const blob = new Blob([data?.body], {
        type: 'application/octet-stream',
      });
      FileSaver.saveAs(blob, filename);
    }
  }

  override ngOnDestroy() {
    this.subs.forEach((s) => s.unsubscribe());
  }

  initTable() {
    this.tableConfig = {
      headers: [
        {
          title: 'development.promulgate.search.proposeCode',
          field: 'proposeCode',
          width: 120,
          fixed: true,
          fixedDir: 'left',
          tdClassList: ['text-center'],
        },
        {
          title: 'development.proposed-unit.table.empCode',
          field: 'employeeCode',
          width: 120,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.proposed-unit.table.empName',
          field: 'employeeName',
          width: 170,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.approvement-issue.search.form',
          field: 'formName',
          width: 170,
        },
        {
          title: 'development.proposed-unit.table.currentUnit',
          field: 'currentUnit',
          width: 170,
        },
        {
          title: 'development.proposed-unit.table.currentTitle',
          field: 'currentTitle',
          width: 170,
        },
        {
          title: 'development.list-consultations.content-consultations.location',
          field: 'currentPosition',
          width: 170,
        },
        {
          title: 'development.list-consultations.content-consultations.level',
          field: 'currentLevelTitle',
          width: 170,
        },
        {
          title: 'development.list-consultations.content-consultations.verticalDivison',
          field: 'currentBusinessLine',
          width: 170,
        },
        {
          title: 'development.proposed-unit.table.proposeUnit',
          field: 'proposeUnit',
          width: 170,
        },
        {
          title: 'development.proposed-unit.table.proposeTitle',
          field: 'proposeTitle',
          width: 170,
        },
        {
          title: 'development.list-consultations.content-consultations.proposeLocation',
          field: 'proposePosition',
          width: 170,
        },
        {
          title: 'development.list-consultations.content-consultations.proposeLevel',
          field: 'proposeLevelTitle',
          width: 170,
        },
        {
          title: 'development.list-consultations.content-consultations.proposeVerticalDivison',
          field: 'proposeBusinessLine',
          width: 170,
        },
        {
          title: 'development.proposed-unit.table.statusApprove',
          field: 'statusApprove',
          width: 170,
          tdTemplate: this.statusApproveView,
        },
        {
          title: 'development.list-consultations.content-consultations.withdrawNote',
          field: 'withdrawNoteValue',
          width: 170,
        },
        {
          title: 'development.list-consultations.content-consultations.formGroup',
          field: 'formGroup',
          width: 170,
        },
        {
          title: 'development.proposed-unit.table.sentDate',
          field: 'sentDate',
          width: 170,
          tdTemplate: this.sentDateView,
        },
        {
          title: 'development.proposed-unit.table.statusPublish',
          field: 'statusPublish',
          width: 170,
          tdTemplate: this.statusPublishView,
        },
        {
          title: 'development.list-consultations.content-consultations.consultationStatus',
          field: 'statusConsultation',
          width: 150,
          tdTemplate: this.statusConsultationView,
        },
        {
          title: 'development.list-consultations.content-consultations.issueLevel',
          field: 'issueLevelName',
          width: 150,
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: userConfig.pageSize,
      pageIndex: 1,
    };
  }

}
