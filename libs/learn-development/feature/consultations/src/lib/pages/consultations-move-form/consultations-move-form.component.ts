import { Component, Injector, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NzModalRef } from 'ng-zorro-antd/modal';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {Mode} from "@hcm-mfe/learn-development/data-access/common"
import {ConsultionCreate} from "@hcm-mfe/learn-development/data-access/models";
import {ConsultationService} from "@hcm-mfe/learn-development/data-access/services";

@Component({
  selector: 'app-consultations-move-form',
  templateUrl: './consultations-move-form.component.html',
  styleUrls: ['./consultations-move-form.component.scss'],
})
export class ConsultationsMoveFormComponent extends BaseComponent implements OnInit {
  mode = Mode.ADD;
  consultationsMoveForm: FormGroup;
  listStatus: any[] = [];
  data?: ConsultionCreate;
  proposeDetailId?: number;
  typeConsultation = 1;
  isSubmitted = false;
  constructor(
    private consultationsMoveService: ConsultationService,
    injector: Injector,
    readonly modalRef: NzModalRef
  ) {
    super(injector);
    this.consultationsMoveForm = new FormGroup({
      consultDate: new FormControl('', Validators.required),
      consultName: new FormControl('', Validators.required),
      status: new FormControl('', Validators.required),
      note: new FormControl(''),
      show: new FormControl(false),
      consultUnit: new FormControl(''),
      showOpinion: new FormControl(false),
    });
  }

  ngOnInit(): void {
    if (this.mode === Mode.EDIT) {
      const codeStatus = this.listStatus.find((item) => item.name === this.data?.status)?.code;
      const consultUnit = { orgId: this.data?.consultUnit, orgName: this.data?.consultUnitName };
      this.consultationsMoveForm.controls['consultDate'].setValue(this.data?.consultDate);
      this.consultationsMoveForm.controls['consultName'].setValue(this.data?.consultName);
      this.consultationsMoveForm.controls['consultUnit'].setValue(consultUnit);
      this.consultationsMoveForm.controls['status'].setValue(codeStatus);
      this.consultationsMoveForm.controls['note'].setValue(this.data?.note);
      this.consultationsMoveForm.controls['show'].setValue(this.data?.show);
      this.consultationsMoveForm.controls['showOpinion'].setValue(this.data?.showOpinion);
    }
  }

  disabledConsultDate = (dateVal: Date) => {
    return dateVal.valueOf() > Date.now();
  };

  addData() {
    this.isSubmitted = true;
    if (this.consultationsMoveForm.valid) {
      const data = this.consultationsMoveForm.getRawValue();
      data.consultUnitName = data.consultUnit?.orgName || '';
      data.consultUnit = data.consultUnit?.orgId || '';
      data.proposeDetailId = this.proposeDetailId;
      if (this.mode === Mode.ADD) {
        this.consultationsMoveService.saveConsultation(this.typeConsultation, data).subscribe(
          (res) => {
            this.toastrCustom.success(this.translate.instant('development.list-consultations.message.createSuccess'));
            this.modalRef.close({ refresh: true });
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
      } else {
        data.id = this.data?.id;
        this.consultationsMoveService.updateConsultation(this.typeConsultation, data).subscribe(
          (res) => {
            this.toastrCustom.success(this.translate.instant('development.list-consultations.message.updateSuccess'));
            this.modalRef.close({ refresh: true });
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
      }
    }
  }
}
