import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {LearnDevelopmentFeatureShellModule} from "@hcm-mfe/learn-development/feature/shell";
import {ConsultationsRoutingModule} from "./consultations.routing.module";
import {pages} from "./pages";
import {PopupService} from "@hcm-mfe/shared/ui/popup";
import {FormatCurrencyPipe} from "@hcm-mfe/shared/pipes/format-currency";

@NgModule({
  declarations: [...pages],
  exports: [...pages],
  imports: [CommonModule, LearnDevelopmentFeatureShellModule, ConsultationsRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
  providers: [PopupService, DatePipe, FormatCurrencyPipe ]
})
export class LearnDevelopmentFeatureConsultationsModule {}
