import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ScreenType } from '@hcm-mfe/learn-development/data-access/common';
import { ApprovementIssueComponent, ApprovementIssueActionComponent } from './pages';

const routes: Routes = [
  { path: '', component: ApprovementIssueComponent },
  {
    path: 'view',
    component: ApprovementIssueActionComponent,
    data: {
      pageName: 'development.pageName.appprovement-issue-detail',
      breadcrumb: 'development.breadcrumb.appprovement-issue-detail',
      screenType: ScreenType.Detail,
    },
  },
  {
    path: 'action',
    component: ApprovementIssueActionComponent,
    data: {
      pageName: 'development.pageName.appprovement-issue-action',
      breadcrumb: 'development.breadcrumb.appprovement-issue-action',
      screenType: ScreenType.Update,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ApprovementIssueRoutingModule {}
