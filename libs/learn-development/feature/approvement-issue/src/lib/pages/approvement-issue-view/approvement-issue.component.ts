import { Component, Injector, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Constants, userConfig, Utils } from '@hcm-mfe/learn-development/data-access/common';
import {
  CategoryModel,
  DataCommon,
  Proposed,
  SearchApprovementIssue,
  StateApprovement,
  StatusList
} from "@hcm-mfe/learn-development/data-access/models";
import { ApprovementIssueService, ProposedUnitService } from "@hcm-mfe/learn-development/data-access/services";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import { cleanData } from "@hcm-mfe/shared/common/utils";
import { MBTableConfig, Pageable } from "@hcm-mfe/shared/data-access/models";
import { saveAs } from 'file-saver';
import * as moment from 'moment';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
@Component({
  selector: 'appprovement-issue-view',
  templateUrl: './approvement-issue.component.html',
  styleUrls: ['./approvement-issue.component.scss'],
})
export class ApprovementIssueComponent extends BaseComponent implements OnInit {
  pageName = '';
  isExpand = false;
  iconStatus = Constants.IconStatus.DOWN;
  dataTable: Proposed[] = [];
  statusApproval: CategoryModel[] = [];
  statusPublish: CategoryModel[] = [];
  params: SearchApprovementIssue = {
    proposeCode: '', //Mã đề xuất
    formCode: '', // loại đề xuất
    timeApprove: '', // thời gian phê duyệt
    releaseFromDate: null,
    releaseToDate: null,
    issueLevel: '', // cấp ban hành or cấp phê duyệt
    statusPublish: <string | null>null, // trạng thái ban hành
    employeeCode: '', // mã nhân viên
    employeeName: '', // họ tên
    effectiveDate: '', //ngày hiệu lực
    effectiveStartDate: null, //ngày hiệu lực
    effectiveEndDate: null, //ngày hiệu lực
    signerFullName: '', // người ký
    currentUnit: '', // đơn vị  hiện tại
    currentTitle: '', // chức danh hiện tại
    proposeUnit: '', // đơn vị đề xuất
    proposeTitle: '', // chức danh đề xuất
    // proposeOrigin: false, // Đề xuất gốc - Default
    // proposeItem: true, //	Đề xuất thành phần - Default
    // proposeGroup: true, //	Đề xuất gop - Default
    screenCode: Constants.ScreenCode.KSDVNS, // Default
    decisionNumber: null,
    publishContent: '',
    publishCode: '',
    issueContent: null,
    issueCode: null,
    page: 0,
    size: userConfig.pageSize,
  };

  newParams = { ...this.params };
  //Luồng ban hành dùng chung interface DataCommon promulgate
  dataCommon: DataCommon = {};

  statusPublishColor = Constants.statusPublishColor;
  tableConfig!: MBTableConfig;
  heightTable = { x: '100vw', y: '25em'};
  @ViewChild('actionTmpl', {static: true}) action!: TemplateRef<NzSafeAny>;
  @ViewChild('apppoveDate', {static: true}) apppoveDate!: TemplateRef<NzSafeAny>;
  @ViewChild('statusPublishDisplay', {static: true}) statusPublishDisplay!: TemplateRef<NzSafeAny>;
  @ViewChild('effectiveDateTmpl', {static: true}) effectiveDate!: TemplateRef<NzSafeAny>;
  @ViewChild('releaseDate', {static: true}) releaseDate!: TemplateRef<NzSafeAny>;


  pageable: Pageable = {
    totalElements: 0,
    currentPage: 0,
    size: userConfig.pageSize,
  };

  constructor(injector: Injector, private readonly service: ApprovementIssueService, private readonly pUnitService: ProposedUnitService) {
    super(injector);

  }

  ngOnInit(): void {
    this.pageName = this.route.snapshot?.data['pageName'];
    this.initTable();
    this.isLoading = true;
    const params = {
      screenCode: 'HCM-LND-KSDVNS',
    };
    this.service.getState(params).subscribe((data) => {
      if (data) {
        this.dataCommon = data;
        this.dataCommon.flowStatusPublishConfigList = this.convertStatus(data?.flowStatusPublishConfigList);
        if (this.dataCommon.flowStatusPublishConfigList){
          const listPendingApprove = this.dataCommon.flowStatusPublishConfigList.find(
            (item) => item.role === Constants.FlowCodeName.CDBANHANH);
          this.params.statusPublish = listPendingApprove?.key!;
        }
      }
      this.search(1);
    });
  }

  convertStatus(valueStatus: StatusList[]) {
    const arrayStatus: StatusList [] = [];
    if (valueStatus) {
      const textStatusConsultation = [...new Set(valueStatus.map((item: StatusList) => { return  item.role}))];
      textStatusConsultation.forEach((item) =>{
        const filterValueStatus = valueStatus.filter((i) => i.role === item)?.map((a) => { return a.key});
        arrayStatus.push({key: filterValueStatus.toString(), role: item});
      })
    }
    return arrayStatus;
  }


  collapseExpand() {
    this.isExpand = !this.isExpand;
    if (this.isExpand) {
      this.iconStatus = Constants.IconStatus.UP;
    } else {
      this.iconStatus = Constants.IconStatus.DOWN;
    }
  }

  search(firstPage: number) {
    this.isLoading = true;
    this.params.page = firstPage - 1;
    const newParams = { ...cleanData(this.params) };
    newParams.releaseFromDate =
      this.params.timeApprove && this.params.timeApprove[0]
        ? moment(newParams.timeApprove[0]).format(Constants.FORMAT_DATE.YYYY_MM_DD)
        : null;
    newParams.releaseToDate =
      this.params.timeApprove && this.params.timeApprove[1]
        ? moment(this.params.timeApprove[1]).format(Constants.FORMAT_DATE.YYYY_MM_DD)
        : null;
    newParams.effectiveStartDate =
      this.params.effectiveDate && this.params.effectiveDate[0]
        ? moment(newParams.effectiveDate[0]).format(Constants.FORMAT_DATE.YYYY_MM_DD)
        : null;
    newParams.effectiveEndDate =
      this.params.effectiveDate && this.params.effectiveDate[1]
        ? moment(this.params.effectiveDate[1]).format(Constants.FORMAT_DATE.YYYY_MM_DD)
        : null;
    newParams.timeApprove = null;
    newParams.effectiveDate = null;
    const currentUnitID = newParams?.currentUnit?.orgId || '';
    newParams.currentUnit = currentUnitID;
    const proposeUnitID = newParams?.proposeUnit?.orgId || '';
    newParams.proposeUnit = proposeUnitID;

    this.service.search(newParams).subscribe(
      (approveData) => {
        this.dataTable = approveData?.data?.datas?.content || [];
        this.tableConfig.total = approveData?.data?.datas?.totalElements || 0;
        this.tableConfig.pageIndex = firstPage;
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

  detail(item: Proposed) {
    let queryParams = {};
    let action = '';
    if (item.publishStatusCode) {
      if (Constants.ApprovementIssueButton.includes(item.publishStatusCode)) {
        action = Constants.ActionIssueScreen.DBH;
      } else if (item.publishStatusCode === Constants.StatusPublishCode.PENDINGAPPROVEISSUEAGAIN) {
        action = Constants.ActionIssueScreen.DBHL;
      } else if (item.publishStatusCode === Constants.StatusPublishCode.PENDINGAPPROVECANCELISSUE) {
        action = Constants.ActionIssueScreen.DHBH;
      }
    }
    queryParams =
      item.proposeCategory === Constants.ProposeCategory.ELEMENT
        ? { id: item?.proposeDetailId, proposeCategory: item?.proposeCategory, action: action }
        : { id: item?.proposeId, proposeCategory: item?.proposeCategory, action: action };

    this.router.navigate([this.router.url, 'view'], { queryParams });
  }

  exportExcel() {
    if (this.dataTable?.length === 0) {
      this.toastrCustom.warning(
        this.translate.instant(
          'development.proposed-unit.messages.dataTableEmpty'
        )
      );
      return;
    }
    this.isLoading = true;
    const newParams = { ...cleanData(this.params) };
    delete newParams.page;
    delete newParams.size;
    newParams.releaseFromDate =
      this.params.timeApprove && this.params.timeApprove[0]
        ? moment(newParams.timeApprove[0]).format(Constants.FORMAT_DATE.YYYY_MM_DD)
        : null;
    newParams.releaseToDate =
      this.params.timeApprove && this.params.timeApprove[1]
        ? moment(this.params.timeApprove[1]).format(Constants.FORMAT_DATE.YYYY_MM_DD)
        : null;
    newParams.effectiveStartDate =
      this.params.effectiveDate && this.params.effectiveDate[0]
        ? moment(newParams.effectiveDate[0]).format(Constants.FORMAT_DATE.YYYY_MM_DD)
        : null;
    newParams.effectiveEndDate =
      this.params.effectiveDate && this.params.effectiveDate[1]
        ? moment(this.params.effectiveDate[1]).format('YYYY-MM-DD')
        : null;
    newParams.timeApprove = null;
    newParams.effectiveDate = null;
    const currentUnitID = newParams?.currentUnit?.orgId || '';
    newParams.currentUnit = currentUnitID;
    const proposeUnitID = newParams?.proposeUnit?.orgId || '';
    newParams.proposeUnit = proposeUnitID;
    this.service.export(newParams).subscribe(
      (data) => {
        let urlExcel = 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,';
        urlExcel = urlExcel + data.data;
        saveAs(urlExcel, this.translate.instant('development.promulgate.section.fileApprovementIssue'));
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

  actionNavigate(proposeCode: number) {
    const item = this.dataTable.find((i) => i.proposeCode === proposeCode);
    let state: StateApprovement = {};
    for (let stateApprovementItem of Constants.stateApprovement) {
      const found = stateApprovementItem?.list?.find((name) => name === item?.publishStatusCode);
      if (found) {
        state = stateApprovementItem;
        break;
      }
    }

    let queryParams = {};
    queryParams =
      item?.proposeCategory === 'ELEMENT'
        ? { id: item?.proposeDetailId, proposeCategory: item?.proposeCategory }
        : { id: item?.proposeId, proposeCategory: item?.proposeCategory };

    this.router.navigate([this.router.url, state.screenAction], { queryParams });
  }

  isStateButtonView(code: any): boolean {
    const state = ['11.2', '11.3', '12.2CB', '12.4', '12.5']; // state is view publish
    return state.includes(code);
  }

  getColorStatusPublish(status: any): string {
    let color: any;
    for (let i = 0; i < Constants.statusPublishColor.length; i++) {
      const found = this.statusPublishColor[i].name.find((item) => item === status);
      if (found) {
        color = Constants.statusPublishColor[i]['color'];
        break;
      }
    }
    return color;
  }

  getStatusDisplay(status: string) {
    return Utils.getColorFlowStatusApporveOrPublish(status);
  }



  initTable() {
    this.tableConfig = {
      headers: [
        {
          title: 'development.proposed-unit.reportCard.decisionNumber',
          field: 'decisionNumber',
          thClassList: [Constants.TABLE_CONFIG.TEXT_NOWRAP, Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [Constants.TABLE_CONFIG.TEXT_NOWRAP, Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 110,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.promulgate.search.issueCode',
          field: 'publishCode',
          width: 110,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.promulgate.table.proposeCode',
          field: 'proposeCode',
          thClassList: [Constants.TABLE_CONFIG.TEXT_NOWRAP, Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [Constants.TABLE_CONFIG.TEXT_NOWRAP, Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 100,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.promulgate.search.issueContent',
          field: 'publishContent',
          width: 150,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.approvement-issue.search.form',
          field: 'formRotationName',
          width: 140,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.approvement-issue.search.statusPublish',
          field: 'statusPublishDisplay',
          tdTemplate: this.statusPublishDisplay,
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 170,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.approvement-issue.search.empCode',
          field: 'employeeCode',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 125,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.approvement-issue.search.empName',
          field: 'employeeName',
          width: 160,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.approvement-issue.search.unit',
          field: 'employeeUnitPathName',
          width: 200,
        },
        {
          title: 'development.approvement-issue.search.title',
          field: 'employeeTitleName',
          width: 200,
        },
        {
          title: 'development.promulgate.table.proposeUnit',
          field: 'contentUnitPathName',
          width: 200,
        },
        {
          title: 'development.promulgate.table.proposeTitle',
          field: 'contentTitleName',
          width: 200,
        },
        {
          title: 'development.promulgate.search.effectiveDate',
          field: '',
          width: 150,
          tdTemplate: this.effectiveDate
        },
        {
          title: 'development.approvement-issue.search.issueLevel',
          field: 'issueLevelName',
          width: 150,
        },
        {
          title: 'development.approvement-issue.search.signer',
          field: 'signerFullName',
          width: 180,
        },
        {
          title: 'development.proposed-unit.table.noteWithdraw',
          field: 'withdrawNote',
          width: 200,
        },
        {
          title: 'development.promulgate.table.changeIssuePerson',
          field: 'createdBy',
          width: 150,
        },
        {
          title: 'development.promulgate.search.changeIssueDate',
          field: 'releaseDate',
          width: 150,
          tdTemplate: this.releaseDate
        },
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pageable.size,
      pageIndex: 1,
    };
  }
}
