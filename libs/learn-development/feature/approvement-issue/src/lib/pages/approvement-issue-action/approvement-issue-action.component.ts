import { Component, Injector, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Constants, ScreenType, userConfig } from "@hcm-mfe/learn-development/data-access/common";
import {
  DataEmployee, DataReason,
  DropdownReasonCancel, FileDetail, FlowStatusPublishConfigList, PanelsCollapse, ParamPromulgate, ProposeLogs, ProposePublishFiles, StateApprovement
} from "@hcm-mfe/learn-development/data-access/models";
import { ApprovementIssueService, ProposedUnitService } from "@hcm-mfe/learn-development/data-access/services";
import { ProposeUploadFileComponent } from '@hcm-mfe/learn-development/feature/propose-unit';
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import {MBTableConfig, Pagination} from '@hcm-mfe/shared/data-access/models';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import {forkJoin, of, Subscription} from 'rxjs';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'appprovement-issue-action',
  templateUrl: './approvement-issue-action.component.html',
  styleUrls: ['./approvement-issue-action.component.scss'],
})
export class ApprovementIssueActionComponent extends BaseComponent implements OnInit {
  title?: string;
  type? : string;
  dataTable: DataEmployee[] = [];
  params: ParamPromulgate = {};
  proposeType?: string;
  proposeID?: string | number;
  relationType?: number;
  statusPublish = '';
  isCheckDecision = false;
  isIncomeAnnouncement = false;
  isModeInsurance = false;
  form?: FormGroup;
  modalRef?: NzModalRef;
  listFile = [];
  listFileDecision?: FileDetail;
  listFileIncomeAnnouncement?: FileDetail;
  listFileModeInsurance?: FileDetail;
  dataReason: DataReason = {
    viewDetailReasonOrCorrection: '',
    reasonOrCorrection: undefined,
    cancellationReason: '',
    commentsIssueTransfer: '',
  };
  dataDropdown: DropdownReasonCancel[] = Constants.DataDropdown;
  proposeCategory = '';
  categoryConst = Constants.ProposeCategory;

  proposeLogs: ProposeLogs[] = [];
  stateSwitch = false;
  stateApprovement: StateApprovement | undefined;
  panels: PanelsCollapse[] = [
    {
      active: false,
      disabled: false,
    },
  ];

  listProposeCategory = Constants.ListProposeCategory;
  flowStatusPublishConfigList: FlowStatusPublishConfigList[] = [];
  tableConfig!: MBTableConfig;
  @ViewChild('joinDateTmpl', {static: true}) joinDate!: TemplateRef<NzSafeAny>;
  @ViewChild('effectiveDateTmpl', {static: true}) effectiveDate!: TemplateRef<NzSafeAny>;
  @ViewChild('endDateTmpl', {static: true}) endDate!: TemplateRef<NzSafeAny>;
  @ViewChild('reportApproveTmpl', {static: true}) reportApprove!: TemplateRef<NzSafeAny>;
  @ViewChild('maximumTimeTmpl', {static: true}) maximumTime!: TemplateRef<NzSafeAny>;
  @ViewChild('TBIncomeTmpl', {static: true}) TBIncome!: TemplateRef<NzSafeAny>;
  @ViewChild('TBInsuranceTmpl', {static: true}) TBInsurance!: TemplateRef<NzSafeAny>;
  heightTable = { x: '100%', y: '25em'};

  TBIncomeList: NzUploadFile[] = [];
  TBInsuranceList: NzUploadFile[] = [];

  publishCode = '';
  withdrawNoteName = '';
  publishContent = '';
  statusPublishName: string | undefined;
  ProposeCategoryConst = Constants.ProposeCategory;
  StatusPublishConst = Constants.StatusPublishCode;
  proposePublishFiles: ProposePublishFiles[] = [];
  DocumentTypeConst = Constants.DocumentTypeIssue;
  UploadPopupName = Constants.UploadFileEmployeeIssue;
  fileTypeIssue = Constants.FileTypeIssue;
  listTypeMaximumTime = Constants.TypeMaximumTime;
  typeMaximumTime = Constants.TypeMaximumValue.MONTH;
  proposeTypeID: number = 0;
  action = '';
  id = 0;
  pageNumber = 0;
  pageSize =  userConfig.pageSize;
  iconStatus = Constants.IconStatus.UP;
  isExpand = false;
  @ViewChild('dateLog', {static: true}) dateLog!: TemplateRef<NzSafeAny>;
  pagination = new Pagination();
  tableLogs!: MBTableConfig;
  private readonly subs: Subscription[] = [];

  constructor(injector: Injector, private readonly service: ApprovementIssueService,
    private readonly serviceProposed: ProposedUnitService) {
    super(injector);
    this.initForm();
    this.proposeCategory = this.route.snapshot.queryParams['proposeCategory'];
    this.action = this.route.snapshot.queryParams['action'];
    this.id = this.route.snapshot.queryParams['id'];
  }

  ngOnInit() {
    this.isLoading = true;
    this.title = this.route.snapshot?.data['pageName'];

    this.type = this.route?.snapshot?.data['screenType'] || ScreenType.Create;
    this.initTable();
    this.initTableLog();
    //Thêm property để call đúng api duyệt ban hành
    this.params = {
      id: this.id,
      proposeCategory: this.proposeCategory,
      action: this.action,
      screenCode: Constants.ScreenCode.KSDVNS,
    };
    switch (this.proposeCategory) {
      case Constants.ProposeCategory.ELEMENT:
        this.proposeTypeID = Constants.ProposeTypeIDIssue.ELEMENT;
        break;
      case Constants.ProposeCategory.GROUP:
        this.proposeTypeID = Constants.ProposeTypeIDIssue.GROUP;
        break;
    }
    const sub = forkJoin({
      listFile: this.service.getFile(this.params).pipe(catchError(() => of(undefined))),
      data: this.service.getListDetail(this.params).pipe(catchError(() => of(undefined))),
      log: this.service.getLogs(this.id, this.proposeTypeID,
        Constants.ScreenCode.KSDVNS, this.pageNumber, this.pageSize).pipe(catchError(() => of(undefined))),
    }).subscribe(
      (res) => {
        this.proposeLogs = res?.log?.data?.content || [];
        this.tableLogs.total = res?.log?.data?.totalElements || 0;
        this.tableLogs.pageIndex = 1;
        this.isLoading = false;
        this.dataTable = res.data?.data?.employeeList;
        this.mapFileEmployeeList();
        this.mapReportApprove();
        this.proposeType = res.data?.data?.proposeType || '';
        this.proposeID = this.dataTable[0]?.proposeCode || '';
        this.relationType = res.data?.data?.relationType?.toString();
        this.listFile = res.listFile?.data;
        this.statusPublish = res?.data?.data?.statusPublish || '';
        this.publishCode = res.data?.data?.publishCode || '';
        this.flowStatusPublishConfigList = res.data?.data?.flowStatusPublishConfigList;
        this.statusPublishName = this.flowStatusPublishConfigList.find(
          (item: FlowStatusPublishConfigList) => item.key === this.statusPublish)?.role;
        this.withdrawNoteName = res?.data?.data?.withdrawNoteName || '';
        this.publishContent = res.data?.data?.publishContent || '';
        this.isIncomeAnnouncement = res.data?.data?.employeeList[0]?.isIncome;
        this.isModeInsurance = res.data?.data?.employeeList[0]?.isInsurance;
        this.isCheckDecision = this.proposeCategory === this.categoryConst.GROUP
          ? true
          : res.data?.data?.employeeList[0]?.isDecision;
        this.listFile = res.listFile?.data;
        this.form?.get('signerCode')?.patchValue(res.data?.data?.signerName);
        this.form?.get('effectiveDate')?.patchValue(this.dataTable[0]?.issueEffectiveStartDate);
        this.form?.get('effectiveDateOld')?.patchValue(res.data?.data?.proposeCorrectionDTO?.effectiveDateOld);
        this.form?.get('comment')?.patchValue(res.data?.data?.proposeDecisionDTO?.comments || '');
        this.form?.patchValue(res.data?.data?.proposeDecisionDTO);
        this.getState(res.data?.data?.flowStatusPublishCode);
        this.dataReason = {
          viewDetailReasonOrCorrection: res.data?.data?.proposeCorrectionDTO?.viewDetailReasonOrCorrection,
          reasonOrCorrection: res.data?.data?.proposeCorrectionDTO?.reasonOrCorrection,
          cancellationReason: res.data?.data?.proposeCorrectionDTO?.cancellationReason,
          commentsIssueTransfer: res.data?.data?.proposeCorrectionDTO?.commentsIssueTransfer,
        };
        this.listFile.forEach((item: FileDetail) => {
          switch (item.documentType) {
            case Constants.DocumentTypeIssue.DECISION:
              this.listFileDecision = item;
              break;
            case Constants.DocumentTypeIssue.TBINCOME:
              this.listFileIncomeAnnouncement = item;
              break;
            case Constants.DocumentTypeIssue.TBINSURANCE:
              this.listFileModeInsurance = item;
              break;
          }
        });
      },
      (err) => {
        this.isLoading = false;
        this.getMessageError(err);
      }
    );
    this.subs.push(sub);
  }

  checkProposeCategory() {
    if (this.proposeCategory === Constants.ProposeCategory.GROUP) {
      return this.translate.instant('development.promulgate.table.commonDecision');
    } else {
      return this.translate.instant('development.promulgate.table.decision');
    }
  }

  checkCommentsTransfer(){
    const list = ['12.3A', '12.4'];
    return list.indexOf(this.statusPublish) > -1;
  }

  get getFormEffectiveDateOld() {
    return this.form?.get('effectiveDateOld')?.value;
  }

  get decisionNum(){
    return this.form?.get('decisionNumber')?.value;
  }

  initForm() {
    this.form = this.fb.group({
      effectiveDate: [null],
      effectiveDateOld: [null],
      signedDate: [null],
      signerCode: [null],
      comment: [null],
      decisionNumber: [null],
    });
  }

  getState(code: any) {
    for (let i = 0; i < Constants.stateApprovement.length; i++) {
      const found = Constants.stateApprovement[i].list?.find((name) => name === code);
      if (found) {
        this.stateApprovement = Constants.stateApprovement[i];
        break;
      }
    }
  }

  //folow const valiable confirm promulgate
  submit(action: number) {
    let groupProposeType = this.listProposeCategory.find((item) => item.name === this.params?.proposeCategory)?.id;
    let title = '';
    switch (action) {
      case Constants.ApprovementIssueButtonName.ISSUEAPPROVE:
        title = this.translate.instant('development.interview-config.notificationMessage.approveIssueConfirm');
        break;
      case Constants.ApprovementIssueButtonName.CANCELAPPROVE:
        title = this.translate.instant('development.interview-config.notificationMessage.cancelApproveIssueConfirm');
        break;
      case Constants.ApprovementIssueButtonName.ISSUEAPPROVEAGAIN:
        title = this.translate.instant('development.interview-config.notificationMessage.approveIssueConfirmAgain');
        break;
    }
    this.modalRef = this.modal.confirm({
      nzTitle: title,
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        this.service.handleApprovement(this.params?.id!, groupProposeType!).subscribe(
          (res) => {
            if (res?.data) {
              this.toastrCustom.success(res.data);
            } else {
              this.toastrCustom.success(this.translate.instant('development.interview-config.notificationMessage.success'));
            }
            this.router.navigateByUrl('/development/approvement-issue');
            this.isLoading = false;
          },
          (err) => {
            this.isLoading = false;
            this.getMessageError(err);
          }
        );
      },
    });
  }

  downloadFile(id: number, name: string) {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;
    this.service.downloadFile(id, name).subscribe(
      () => {
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }



  initTable() {
    this.tableConfig = {
      headers: [
        {
          title: 'development.promulgate.search.form',
          field: 'formName',
          width: 110,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.proposed-unit.table.empCode',
          field: 'empCode',
          width: 110,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.proposed-unit.table.empName',
          field: 'fullName',
          width: 180,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.proposed-unit.detail.employee.joinCompanyDate',
          field: 'joinCompanyDate',
          width: 120,
          tdTemplate: this.joinDate
        },
        {
          title: 'development.proposed-unit.table.currentUnit',
          field: 'orgPathName',
          width: 200,
        },
        {
          title: 'development.proposed-unit.table.currentTitle',
          field: 'jobName',
          width: 150,
        },
        {
          title: 'development.proposed-unit.table.currentLevel',
          field: 'employeeLevel',
          width: 110,
        },
        {
          title: 'development.proposed-unit.table.proposeUnit',
          field: 'contentUnitPathName',
          width: 200,
        },
        {
          title: 'development.proposed-unit.table.proposeTitle',
          field: 'contentTitleName',
          width: 150,
        },
        {
          title: 'development.proposed-unit.table.proposeLevel',
          field: 'contentLevel',
          width: 110,
        },
        {
          title: 'development.proposed-unit.table.noteWithdraw',
          field: 'withdrawNoteName',
          width: 160,
        },
        {
          title: 'development.promulgate.search.effectiveDate',
          field: 'issueEffectiveStartDate',
          width: 120,
          tdTemplate: this.effectiveDate
        },
        {
          title: 'development.promulgate.table.minimumTime',
          field: 'minimumTime',
          width: 110,
        },
        {
          title: 'development.proposed-unit.formRotation.maximumTimeTGCV',
          field: 'maximumTime',
          width: 150,
          tdTemplate: this.maximumTime,
        },
        {
          title: 'development.promulgate.table.endDate',
          field: 'issueEffectiveEndDate1',
          width: 120,
          tdTemplate: this.endDate
        },
        {
          title: 'development.promulgate.table.reportApprove',
          field: '',
          width: 150,
          tdTemplate: this.reportApprove
        },
        {
          title: 'development.promulgate.table.TBIncome',
          field: '',
          width: 150,
          tdTemplate: this.TBIncome,
          show: this.proposeCategory === Constants.ProposeCategory.GROUP
        },
        {
          title: 'development.promulgate.table.TBInsurance',
          field: '',
          width: 150,
          tdTemplate: this.TBInsurance,
          show: this.proposeCategory === Constants.ProposeCategory.GROUP
        },
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: userConfig.pageSize,
      pageIndex: 1,
    };
  }

  //check hiển thị nút duyệt ban hành
  checkApproveIssueButton() {
    return Constants.ApprovementIssueButton.indexOf(this.statusPublish) > -1;
  }

  //các trạng thái bắt buộc nhập số quyết định và ngày ký
  get canEditDateSign(): boolean {
    return Constants.editSignDate.includes(this.statusPublish);
  }

  disabledEffectiveDate = (current: Date) => {
    const today = new Date().getTime();
    return current.getTime() <= today;
  };

  openUploadPopup(title: string, id: number, documentType: number, typeIssue: string) {
    this.modalRef = this.modal.create({
      nzTitle: title,
      nzWidth: '30vw',
      nzContent: ProposeUploadFileComponent,
      nzComponentParams: {
        id: id,
        documentType: documentType,
        typeIssue: typeIssue,
      },
      nzFooter: null,
    });
    this.modalRef.componentInstance.closeEvent.subscribe(() => {
      this.params = {
        ...this.params,
        action: Constants.ActionIssueScreen.DBH,
        screenCode: Constants.ScreenCode.KSDVNS,
      };
      this.modalRef?.destroy();
      this.isLoading = true;
      this.service.getListDetail(this.params).subscribe((res) => {
        this.dataTable = res?.data?.employeeList;
        this.mapFileEmployeeList();
        this.mapReportApprove();
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      });
    });
  }

  //map thông báo thu nhập và bảo hiểm
  mapFileEmployeeList() {
    this.dataTable.forEach((item) => {
      this.proposePublishFiles = item?.proposePublishFiles || [];
      if (this.proposePublishFiles.length > 0) {
        const incomeObj = this.proposePublishFiles.find(
          (files) => files.documentType === Constants.DocumentTypeIssue.TBINCOME);
        if (incomeObj) {
          item.idIncome = incomeObj?.decisionSignId;
          item.nameIncome = incomeObj?.decisionSignName;
        }
        const insuranceObj = this.proposePublishFiles.find(
          (file) => file.documentType === Constants.DocumentTypeIssue.TBINSURANCE);
        if (insuranceObj) {
          item.idInsurance = insuranceObj?.decisionPublishId;
          item.nameInsurance = insuranceObj?.decisionPublishName;
        }
      }
    });
  }

  //map tờ trình bản duyệt
  mapReportApprove() {
    this.dataTable.forEach((item: DataEmployee) => {
      if (item.proposeProcessFile) {
        item.processFileId = item.proposeProcessFile.docId;
        item.processFileName = item.proposeProcessFile.fileName;
      }
    })
  }

  //tải file trong bảng danh sách nhân sự
  downloadFileEmployeeList(uid: number, fileName: string) {
    this.isLoading = true;
    this.serviceProposed.downloadFile(+uid, fileName).subscribe(
      () => {
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

  //xóa file TB thu nhập, TB bảo hiểm
  removeFileEmployeeList(id: number, documentType: number, typeIssue: string) {
    this.isLoading = true;
    const paramDelete = {
      id: id,
      proposeCategory: Constants.ProposeCategory.ELEMENT,
      documentType: documentType,
      type: typeIssue,
    };
    this.serviceProposed.removeFileNotify(paramDelete).subscribe(
      () => {
        this.service.getListDetail(this.params).subscribe((res) => {
          this.dataTable = res?.data?.employeeList;
          this.mapFileEmployeeList();
          this.mapReportApprove();
          this.isLoading = false;
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        })
      },
      (e) => {
        this.isLoading = false;
        this.getMessageError(e);
      }
    );
  };

  //download tờ trình bản duyệt
  downloadReportApprove(id: string, name: string) {
    this.isLoading = true;
    this.service.downloadFile(+id, name).subscribe(
      () => {
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

  private initTableLog(): void {
    this.tableLogs = {
      headers: [
        {
          title: 'development.proposed-unit.proposeLogsTable.time',
          field: 'actionDate',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
          tdTemplate: this.dateLog
        },
        {
          title: 'development.proposed-unit.proposeLogsTable.personAction',
          field: 'userName',
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
        },
        {
          title: 'development.proposed-unit.proposeLogsTable.action',
          field: 'action',
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
        },
        {
          title: 'development.proposed-unit.proposeLogsTable.description',
          field: 'comment',
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 200,
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1,
    };
  }

  expandLog() {
    this.isExpand = !this.isExpand;
    if (this.isExpand) {
      this.iconStatus = Constants.IconStatus.DOWN;
    } else {
      this.iconStatus = Constants.IconStatus.UP;
    }
  }

  // lấy log phân trang
  getLog(pageIndex: number){
    this.pageNumber = pageIndex - 1;
    this.isLoading = true;
    const log = this.service.getLogs(this.id, this.proposeTypeID,
      Constants.ScreenCode.KSDVNS, this.pageNumber, this.pageSize)
      .subscribe((log) => {
        this.proposeLogs = log?.data?.content || [];
        this.tableLogs.total = log?.data?.totalElements || 0;
        this.tableLogs.pageIndex = pageIndex;
        this.isLoading = false;
      }, (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      })
    this.subs.push(log);
  }

  override ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
    this.modalRef?.destroy();
  }
}
