import { ApprovementIssueComponent } from './approvement-issue-view/approvement-issue.component';
import { ApprovementIssueActionComponent } from './approvement-issue-action/approvement-issue-action.component';
export const pages = [ApprovementIssueComponent, ApprovementIssueActionComponent];
export * from './approvement-issue-view/approvement-issue.component';
export * from './approvement-issue-action/approvement-issue-action.component';
