import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { Constants, ScreenType } from "@hcm-mfe/learn-development/data-access/common";
import { DataCommonProvisoLd, ProvisoModel } from "@hcm-mfe/learn-development/data-access/models";
import { ProvisoLdService } from "@hcm-mfe/learn-development/data-access/services";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import { cleanDataForm, Utils } from "@hcm-mfe/shared/common/utils";
import { SelectCheckAbleModal } from "@hcm-mfe/shared/ui/mb-select-check-able";
import * as moment from 'moment';
import { forkJoin, of, Subscription } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'app-proviso-action',
  templateUrl: './proviso-action.component.html',
  styleUrls: ['./proviso-action.component.scss'],
})
export class ProvisoActionComponent extends BaseComponent implements OnInit {
  subs: Subscription[] = [];
  title?: string;
  form?: FormGroup;
  isSubmitted = false;
  type?: string;
  id?: string;
  dataCommon: DataCommonProvisoLd = {};
  constructor(injector: Injector, private readonly service: ProvisoLdService) {
    super(injector);
    this.initForm();
  }

  ngOnInit() {
    this.type = this.route.snapshot?.data['screenType'] || ScreenType.Create;
    this.id = this.route.snapshot?.queryParams['id'];
    this.title = this.route.snapshot?.data['pageName'];

    const sub = forkJoin([this.service.getLevel().pipe(catchError(() => of(undefined))),
      this.service.getSpecialized().pipe(catchError(() => of(undefined))),
      this.service.getState()]).subscribe(
      ([major,educationDegree,res]) => {
        this.dataCommon = res;
        this.dataCommon.major = major?.data;
        this.dataCommon.major?.unshift({code: "999", name: "Theo yêu cầu JD"})
        this.dataCommon.educationDegree = educationDegree?.data;
        this.dataCommon.educationDegree?.unshift({code: "999", name: "Theo yêu cầu JD"})
      },
      () => {
        this.isLoading = false;
      }
    );
    this.subs.push(sub);


    if (this.type === 'UPDATE') {
      this.service.findById(this.id!).subscribe((res) => {
        this.form?.patchValue(res.data);
      });
    }
  }

  initForm() {
    this.form = this.fb.group({
      formLD: [[], Validators.required],
      conditionType: [null, Validators.required],
      effectiveFromDate: [null, Validators.required],
      effectiveToDate: [null],
      semesterGradeT2: [null],
      semesterRankT2: [[]],
      semesterGradeT1: [null],
      semesterRankT1: [[]],
      seniority: [null],
      positionSeniority: [null],
      educationDegree: [[]],
      major: [[]],
      endOfDisciplinaryDurationDismiss: [null],
      endOfDeciplines: [null],
      titleLevel: [[]],
      ratedCapacity: [null],
      interviewResult: [[]],
    });
  }

  disabledStartDate = (startValue: Date): boolean => {
    let endValue = null;
    if (this.form?.get('effectiveToDate')?.value) {
      endValue = new Date(this.form?.get('effectiveToDate')?.value);
    }
    if (!startValue || !endValue) {
      return false;
    }
    return startValue.getTime() > endValue.getTime();
  };

  disabledEndDate = (endValue: Date): boolean => {
    let startValue = null;
    if (this.form?.get('effectiveFromDate')?.value) {
      startValue = new Date(this.form?.get('effectiveFromDate')?.value);
    }
    if (!endValue || !startValue) {
      return false;
    }
    return endValue.getTime() <= startValue.getTime();
  };

  save() {
    this.confirmService.error().then((res) => {
      if (res) {
        this.isSubmitted = true;
        const data: ProvisoModel = { ...cleanDataForm(this.form!) };
        if (this.form?.invalid || this.checkValidate() || this.isLoading) {
          return;
        }
        //convert date về YYYYMMDD
        data.effectiveFromDate = data.effectiveFromDate
          ? moment(data.effectiveFromDate).format(Constants.FORMAT_DATE.YYYY_MM_DD)
          : data.effectiveFromDate;
        data.effectiveToDate = data.effectiveToDate
          ? moment(data.effectiveToDate).format(Constants.FORMAT_DATE.YYYY_MM_DD)
          : data.effectiveToDate;

        let api:any;
        if (this.type === ScreenType.Create) {
          api = this.service.createProviso(data);
        } else if (this.type === ScreenType.Update) {
          data.id = parseInt(this.id!);
          api = this.service.updateProviso(data);
        }
        api.subscribe(
          () => {
            if (this.type === ScreenType.Create) {
              this.toastrCustom.success('Thêm mới thành công');
            } else {
              this.toastrCustom.success('Cập nhật thành công');
            }
            this.isLoading = false;
            this.router.navigateByUrl('/development/proviso');
          },
          (e:any) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
      }
    });
  }

  checkValidate() {
    const data = this.form?.getRawValue();
    if (
      Utils.isEmpty(data.semesterGradeT2) &&
      Utils.isEmpty(data.semesterRankT2) &&
      Utils.isEmpty(data.semesterGradeT1) &&
      Utils.isEmpty(data.semesterRankT1) &&
      Utils.isEmpty(data.seniority) &&
      Utils.isEmpty(data.positionSeniorrity) &&
      Utils.isEmpty(data.educationDegree) &&
      Utils.isEmpty(data.major) &&
      Utils.isEmpty(data.endOfDisciplinaryDurationDismiss) &&
      Utils.isEmpty(data.endOfDeciplines) &&
      Utils.isEmpty(data.titleLevel) &&
      Utils.isEmpty(data.ratedCapacity) &&
      Utils.isEmpty(data.interviewResult)
    ) {
      this.toastrCustom.warning(this.translate.instant('development.proviso-config.messages.validate1'));
    }
    return false;
  }

  emitValue(item: SelectCheckAbleModal, field: string) {
    this.form?.controls[field].setValue(item.listOfSelected);
  }



  override ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }

}
