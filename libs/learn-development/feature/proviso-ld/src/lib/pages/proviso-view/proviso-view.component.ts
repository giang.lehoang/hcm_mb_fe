import { Component, Injector, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Constants, userConfig } from "@hcm-mfe/learn-development/data-access/common";
import { DataCommonProvisoLd, ProvisoModel } from "@hcm-mfe/learn-development/data-access/models";
import { ProvisoLdService } from "@hcm-mfe/learn-development/data-access/services";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import { cleanData } from "@hcm-mfe/shared/common/utils";
import { MBTableConfig, Pageable, Pagination } from "@hcm-mfe/shared/data-access/models";
import * as FileSaver from 'file-saver';
import * as moment from 'moment';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { Subscription } from 'rxjs';





@Component({
  selector: 'app-proviso-view',
  templateUrl: './proviso-view.component.html',
  styleUrls: ['./proviso-view.component.scss'],
})
export class ProvisoViewComponent extends BaseComponent implements OnInit{
  dataTable = [];
  scrollY = '';
  dataCommon: DataCommonProvisoLd = {};
  title?: string;
  params: ProvisoModel = {
    effectiveFromDate: null,
    effectiveToDate: null,
    formLD: null,
    page: 0,
    size: userConfig.pageSize,
  };
  prevParams: ProvisoModel = this.params;
  pageable: Pageable = {
    totalElements: 0,
    currentPage: 0,
    size: userConfig.pageSize,
  };
  modalRef?: NzModalRef;
  private readonly subs: Subscription[] = [];
  tableConfig!: MBTableConfig;
  pagination = new Pagination();
  @ViewChild('actionTmpl', {static: true}) action!: TemplateRef<NzSafeAny>;
  heightTable = { x: '100vw', y: '25em'};

  constructor(injector: Injector, private service: ProvisoLdService) {
    super(injector);
  }

  ngOnInit() {
    this.isLoading = true;
    this.initTable();

    this.title = this.route.snapshot?.data['pageName'];
    this.service.getState().subscribe((res) => {
      this.dataCommon.formDTOs = res?.formDTOs;
      this.search(true);
    });
  }

  disabledStartDate = (startValue: Date): boolean => {
    if (!startValue || !this.params.effectiveToDate) {
      return false;
    }
    return startValue.getTime() > (this.params.effectiveToDate as Date)?.getTime();
  };

  disabledEndDate = (endValue: Date): boolean => {
    if (!endValue || !this.params?.effectiveFromDate) {
      return false;
    }
    return endValue.getTime() <= (this.params?.effectiveFromDate as Date)?.getTime();
  };

  search(firstPage?: boolean) {
    this.isLoading = true;
    if (firstPage) {
      this.params.page = 0;
    }
    const newParams = { ...cleanData(this.params) };
    newParams.effectiveFromDate = newParams.effectiveFromDate
      ? moment(newParams.effectiveFromDate).format(Constants.FORMAT_DATE.YYYY_MM_DD)
      : newParams.effectiveFromDate;
    newParams.effectiveToDate = newParams.effectiveToDate
      ? moment(newParams.effectiveToDate).format(Constants.FORMAT_DATE.YYYY_MM_DD)
      : newParams.effectiveToDate;
    this.service.getAndSearch(newParams).subscribe(
      (res) => {
        this.dataTable = res.data?.content;
        // this.pageable.totalElements = res?.data?.totalElements || 0;
        this.tableConfig.total = res?.data?.totalElements || 0;
        this.prevParams = newParams;
        //check nếu dataTable = [] thì giảm page đi 1 tránh hiện thì trống ở  page >= 1
        if (this.dataTable.length === 0 && this.params.page! > 0) {
          this.params.page = this.params.page! - 1;
          return this.search();
        }
        this.isLoading = false;
        // this.scrollY = Utils.setScrollYTable('table', this.pageable.totalElements! > 15 ? Constants.HeightPaginator : 0);

      },
      () => {
        this.isLoading = false;
        this.toastrCustom.error('Có lỗi xảy ra, vui lòng  thử lại  sau');
        this.dataTable = [];
      }
    );
  }

  detail(id: string) {
    this.router.navigate([this.router.url, 'update'], {
      queryParams: {
        id: id,
      },
      skipLocationChange: true,
    });
  }

  deleteItem(id: string) {
    if (this.isLoading) {
      return;
    }
    this.modalRef = this.modal.warning({
      nzTitle: this.translate.instant('development.consultation-config.messages.isDelete'),
      nzCancelText: this.translate.instant('development.consultation-config.messages.no'),
      nzOkText: this.translate.instant('development.consultation-config.messages.yes'),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        this.service.deleteById(id).subscribe(
          () => {
            this.toastrCustom.success(this.translate.instant('development.consultation-config.messages.deleteSuccess'));
            this.search();
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
      },
    });
  }

  pageChange(page: number) {
    if (this.isLoading) {
      return;
    }
    this.params.page = page - 1;
    this.search();
  }

  exportExcel() {
    if (this.dataTable?.length === 0) {
      this.toastrCustom.warning(
        this.translate.instant(
          'development.proposed-unit.messages.dataTableEmpty'
        )
      );
      return;
    }
    this.isLoading = true;
    this.service.exportExcel(this.prevParams).subscribe(
      (data) => {
        let urlExcel = 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,';
        urlExcel = urlExcel + data.data;
        FileSaver.saveAs(urlExcel, this.translate.instant('development.proviso.messages.excelFileName'));
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }



  override ngOnDestroy() {
    this.modalRef?.destroy();
    this.subs.forEach((s) => s.unsubscribe());
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'development.proviso.table.formLD',
          field: 'formLD',
          thClassList: ['text-nowrap', 'text-center'],
          tdClassList: ['text-nowrap', 'text-center'],
          width: 100,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.proviso.table.conditionType',
          field: 'conditionType',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 150,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.proviso.table.effectiveFromDate',
          field: 'effectiveFromDate',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 150,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.proviso.table.effectiveToDate',
          field: 'effectiveToDate',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 150,

        },
        {
          title: 'development.proviso.table.semesterGradeT2',
          field: 'semesterGradeT2',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 150,
        },
        {
          title: 'development.proviso.table.semesterRankT2',
          field: 'semesterRankT2',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 150,
        },
        {
          title: 'development.proviso.table.semesterGradeT1',
          field: 'semesterGradeT1',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 150,
        },
        {
          title: 'development.proviso.table.semesterRankT1',
          field: 'semesterRankT1',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 150,
        },
        {
          title: 'development.proviso.table.seniority',
          field: 'seniority',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 150,
        },
        {
          title: 'development.proviso.table.positionSeniority',
          field: 'positionSeniority',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 150,
        },
        {
          title: 'development.proviso.table.educationDegree',
          field: 'educationDegree',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 150,
        },
        {
          title: 'development.proviso.table.major',
          field: 'major',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 150,
        },
        {
          title: 'development.proviso.table.endOfDisciplinaryDurationDismiss',
          field: 'endOfDisciplinaryDurationDismiss',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 150,
        },
        {
          title: 'development.proviso.table.endOfDeciplines',
          field: 'endOfDeciplines',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 150,
        },
        {
          title: 'development.proviso.table.titleLevel',
          field: 'titleLevel',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 150,
        },
        {
          title: 'development.proviso.table.ratedCapacity',
          field: 'ratedCapacity',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 150,
        },
        {
          title: 'development.proviso.table.interviewResult',
          field: 'interviewResult',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 150,
        },
        {
          title: ' ',
          field: 'action',
          tdClassList: ['text-nowrap','text-center'], thClassList: ['text-nowrap','text-center'],
          width: 100,
          tdTemplate: this.action,
          fixed: window.innerWidth > 1024,
          fixedDir: 'right',
        }


      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1,
    };
  }

  onClickCreate() {
    this.router.navigate(['/development/proviso/add'], {
      skipLocationChange: true,
    });
  }


}
