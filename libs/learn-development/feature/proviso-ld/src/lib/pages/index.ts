import {ProvisoViewComponent} from "./proviso-view/proviso-view.component";
import {ProvisoActionComponent} from "./proviso-action/proviso-action.component";
export const pages = [ProvisoViewComponent, ProvisoActionComponent];
export * from "./proviso-view/proviso-view.component";
export * from "./proviso-action/proviso-action.component";

