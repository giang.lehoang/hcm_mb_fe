import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ProvisoViewComponent} from "./pages/proviso-view/proviso-view.component";
import {ProvisoActionComponent} from "./pages/proviso-action/proviso-action.component";
import {ScreenType} from "@hcm-mfe/learn-development/data-access/common";

const routes: Routes = [
  { path: '', component: ProvisoViewComponent },
  {
    path: 'add',
    component: ProvisoActionComponent,
    data: {
      pageName: 'development.pageName.proviso-ld-add',
      breadcrumb: 'development.breadcrumb.proviso-ld-add',
      screenType: ScreenType.Create,
    },
  },
  {
    path: 'update',
    component: ProvisoActionComponent,
    data: {
      pageName: 'development.pageName.proviso-ld-update',
      breadcrumb: 'development.breadcrumb.proviso-ld-update',
      screenType: ScreenType.Update,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProvisoLdRoutingModule {}
