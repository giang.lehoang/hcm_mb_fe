import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {IssueViewComponent} from "./pages";

const routes: Routes = [
  { path: '', component: IssueViewComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IssueLevelRoutingModule {}
