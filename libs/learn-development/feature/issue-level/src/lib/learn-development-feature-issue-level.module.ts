import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import {pages} from "./pages";
import {components} from "./components";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzCardModule} from "ng-zorro-antd/card";
import {NzGridModule} from "ng-zorro-antd/grid";
import {NzIconModule} from "ng-zorro-antd/icon";
import {SharedUiMbEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/mb-employee-data-picker";
import {SharedUiMbEmployeeInfoModule} from "@hcm-mfe/shared/ui/mb-employee-info";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {NzTagModule} from "ng-zorro-antd/tag";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import {NzTableModule} from "ng-zorro-antd/table";
import {SharedCommonBaseComponentModule} from "@hcm-mfe/shared/common/base-component";
import {NzMessageModule} from "ng-zorro-antd/message";
import {NzModalModule} from "ng-zorro-antd/modal";
import {PopupService} from "@hcm-mfe/shared/ui/popup";
import {NzPageHeaderModule} from "ng-zorro-antd/page-header";
import {NzButtonModule} from "ng-zorro-antd/button";
import {IssueLevelRoutingModule} from "./learn-development-feature-issue-level-routing.module";
import {dialog} from "./dialogs";
import {SharedUiMbSelectCheckAbleModule} from "@hcm-mfe/shared/ui/mb-select-check-able";

@NgModule({
  declarations: [...pages, ...components, ...dialog],
  exports: [...pages, ...components, ...dialog],
  imports: [
    CommonModule,
    TranslateModule,
    IssueLevelRoutingModule,
    FormsModule,
    SharedUiMbButtonModule,
    NzCardModule,
    NzGridModule,
    ReactiveFormsModule,
    SharedUiMbSelectModule,
    SharedUiMbInputTextModule,
    NzIconModule,
    SharedUiMbEmployeeDataPickerModule,
    SharedUiMbEmployeeInfoModule,
    SharedUiMbTableWrapModule,
    SharedUiMbTableModule,
    NzTagModule,
    SharedUiLoadingModule,
    NzPaginationModule,
    NzTableModule,
    NzMessageModule,
    NzModalModule,
    NzPageHeaderModule,
    NzButtonModule,
    NzIconModule,
    SharedCommonBaseComponentModule,
    SharedUiMbSelectCheckAbleModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
  providers: [PopupService]
})
export class LearnDevelopmentFeatureIssueLevelModule {}
