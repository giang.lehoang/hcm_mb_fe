import { Component, Injector, Input, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import {ScreenType} from "@hcm-mfe/learn-development/data-access/common";
import {CustomValidators} from "@hcm-mfe/shared/common/validators";
import { SelectCheckAbleModal } from "@hcm-mfe/shared/ui/mb-select-check-able";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import { cleanDataForm } from '@hcm-mfe/shared/common/utils';
import {IssueViewComponent} from "../../pages";

@Component({
  selector: 'app-issue-dialog',
  templateUrl: './issue-dialog.component.html',
  styleUrls: ['./issue-dialog.component.scss'],
})
export class IssueDialogComponent extends BaseComponent implements OnInit {
  @Input() dataCommon: any;
  public form = this.fb.group({
    code: null,
    sortOrder: [null, CustomValidators.onlyNumber],
    name: [null, Validators.required],
    approvalTitle: null,
    signerTitle: null,
  });
  isSubmitted = false;
  @Input() type?: ScreenType;
  @Input() data: any;

  constructor(injector: Injector, public issue: IssueViewComponent) {
    super(injector);
  }

  ngOnInit(): void {
    if (this.data) {
      this.form.patchValue(this.data);
    }
  }

  save() {
    this.isSubmitted = true;
    const event = {
      data: cleanDataForm(this.form),
      type: this.type,
    };
    if (this.form.valid) {
      this.issue.handleOK(event);
    }
  }

  emitValue(item: SelectCheckAbleModal, field: string) {
    this.form.controls[field].setValue(item.listOfSelected);
  }
}
