import { Component, Injector, OnInit } from '@angular/core';
import { Constants, ScreenType, userConfig, Utils } from "@hcm-mfe/learn-development/data-access/common";
import { IssueLevel, Title } from "@hcm-mfe/learn-development/data-access/models";
import { IssueLevelService } from "@hcm-mfe/learn-development/data-access/services";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import { ValidateService } from '@hcm-mfe/shared/core';
import { Pageable } from '@hcm-mfe/shared/data-access/models';
import * as FileSaver from 'file-saver';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-issue-view',
  templateUrl: './issue-view.component.html',
  styleUrls: ['./issue-view.component.scss'],
})
export class IssueViewComponent extends BaseComponent implements OnInit {
  dataTable?: Array<IssueLevel>;
  listTitle?: Array<Title>;
  isVisible = false;
  isOKLoading = false;
  scrollY = '';
  titlePage = '';
  titleModal: string = '';
  utcId?: string;
  limit = userConfig.pageSize;
  item?: IssueLevel | null;
  params: IssueLevel = {
    page: 0,
    size: userConfig.pageSize,
    flag: true,
  };
  pageable: Pageable = {
    totalElements: 0,
    currentPage: 0,
    size: userConfig.pageSize,
  };
  type?: ScreenType;
  modalRef?: NzModalRef;
  private readonly subs: Subscription[] = [];

  constructor(injector: Injector, public validateService: ValidateService, private service: IssueLevelService) {
    super(injector);

  }

  ngOnInit() {
    this.isLoading = true;
    this.titlePage = this.route.snapshot?.data['pageName'];
    this.service.getState().subscribe((data) => {
      this.listTitle = data?.sort((a: { jobName: number; }, b: { jobName: number; }) => (a.jobName > b.jobName ? 1 : -1));
      this.search();
    });
  }

  search() {
    this.isLoading = true;
    this.service.search(this.params).subscribe(
      (response) => {
        this.dataTable = response?.data?.issueLevel?.content || [];
        this.pageable.totalElements = response?.data?.issueLevel?.totalElements || 0;
        this.pageable.currentPage = this.params.page;
        if (this.dataTable?.length === 0 && this.params.page! > 0) {
          this.params.page = this.params.page! - 1;
          return this.search();
        }
        this.isLoading = false;
        this.scrollY = Utils.setScrollYTable('table');
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
        this.dataTable = [];
      }
    );
  }

  pageChange(page: number) {
    if (this.isLoading) {
      return;
    }
    this.params.page = page - 1;
    this.search();
  }

  addNewIssueLevel(req: any) {
    this.service.createIssueLevel(req).subscribe(() => {
      this.isVisible = false;
      this.isOKLoading = false;
    });
  }

  showModal(type: ScreenType, item?: any) {
    this.isLoading = true;
    this.type = type;
    if (type === ScreenType.Update) {
      this.service.getIssueLevelByCode(item?.code).subscribe((data) => {
        this.item = data;
        this.titleModal = this.translate.instant('development.issueLevel.modalName.update');
        this.isVisible = true;
        this.isLoading = false;
      });
    } else if (type === ScreenType.Create) {
      this.item = {};
      this.isVisible = true;
      this.titleModal = this.translate.instant('development.issueLevel.modalName.add');
      this.isLoading = false;
    }
  }

  hideModal() {
    this.item = null;
    this.isVisible = false;
  }

  handleOK(event: any) {
    this.isOKLoading = true;
    let api: any;
    if (event.type === ScreenType.Create) {
      api = this.service.createIssueLevel(event.data);
    } else if (event.type === ScreenType.Update) {
      api = this.service.updateIssueLevel(event.data);
    }
    api.subscribe(
      () => {
        this.isVisible = false;
        this.isOKLoading = false;
        if (event.type === ScreenType.Create) {
          this.toastrCustom.success(this.translate.instant('development.issueLevel.messages.addSuccess'));
        } else {
          this.toastrCustom.success(this.translate.instant('development.issueLevel.messages.updateSuccess'));
        }
        this.search();
      },
      (e: any) => {
        this.isOKLoading = false;
        this.getMessageError(e);
      }
    );
  }

  showDeleModal(code: string) {
    if (this.isLoading) {
      return;
    }
    this.modalRef = this.modal.warning({
      nzTitle: this.translate.instant('development.issueLevel.messages.deleteTitle'),
      nzCancelText: 'Không',
      nzOkText: 'Có',
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        this.service.deleteItem(code).subscribe(
          () => {
            this.toastrCustom.success(this.translate.instant('development.issueLevel.messages.deleteSuccess'));
            this.search();
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
      },
    });
  }

  override ngOnDestroy() {
    this.modalRef?.destroy();
    this.subs.forEach((s) => s.unsubscribe());
  }
}
