import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import {LearnDevelopmentRoutingModule} from "./learn-development-routing.module";
import {TranslateModule} from "@ngx-translate/core";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzCardModule} from "ng-zorro-antd/card";
import {NzGridModule} from "ng-zorro-antd/grid";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {NzIconModule} from "ng-zorro-antd/icon";
import {SharedUiMbEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/mb-employee-data-picker";
import {SharedUiMbEmployeeInfoModule} from "@hcm-mfe/shared/ui/mb-employee-info";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {NzTagModule} from "ng-zorro-antd/tag";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzMessageModule} from "ng-zorro-antd/message";
import {NzModalModule} from "ng-zorro-antd/modal";
import {NzPageHeaderModule} from "ng-zorro-antd/page-header";
import {NzButtonModule} from "ng-zorro-antd/button";
import {SharedCommonBaseComponentModule} from "@hcm-mfe/shared/common/base-component";
import {NzUploadModule} from "ng-zorro-antd/upload";
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";
import {NzInputNumberModule} from "ng-zorro-antd/input-number";
import {NzDatePickerModule} from "ng-zorro-antd/date-picker";
import {NzRadioModule} from "ng-zorro-antd/radio";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {LearnDevelopmentUiMbSelectUnitModule} from "@hcm-mfe/learn-development/ui/mb-select-unit";
import {NzSwitchModule} from "ng-zorro-antd/switch";
import {QuillModule} from "ngx-quill";
import {NzMenuModule} from "ng-zorro-antd/menu";
import {NzInputModule} from "ng-zorro-antd/input";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
import {NzFormModule} from "ng-zorro-antd/form";
import {NzCollapseModule} from "ng-zorro-antd/collapse";
import {SharedUiMbTextLabelModule} from "@hcm-mfe/shared/ui/mb-text-label";
import { SharedUiMbSelectCheckAbleModule } from '@hcm-mfe/shared/ui/mb-select-check-able';
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {SharedUiOrgDataPickerModule} from "@hcm-mfe/shared/ui/org-data-picker";



const Modules = [CommonModule,
  TranslateModule,
  FormsModule,
  ReactiveFormsModule,
  SharedUiMbButtonModule,
  SharedUiMbSelectModule,
  SharedUiMbInputTextModule,
  SharedUiMbEmployeeDataPickerModule,
  SharedUiMbEmployeeInfoModule,
  SharedUiMbTableWrapModule,
  SharedCommonBaseComponentModule,
  SharedUiMbDatePickerModule,
  SharedUiMbTableModule,
  SharedUiLoadingModule,
  NzCardModule,
  NzGridModule,
  NzIconModule,
  NzTagModule,
  NzPaginationModule,
  NzTableModule,
  NzMessageModule,
  NzModalModule,
  NzPageHeaderModule,
  NzButtonModule,
  NzIconModule,
  NzUploadModule,
  NzCheckboxModule,
  NzInputNumberModule,
  NzDatePickerModule,
  NzRadioModule,
  NzSwitchModule,
  NzMenuModule,
  NzInputModule,
  NzToolTipModule,
  NzFormModule,
  QuillModule,
  LearnDevelopmentUiMbSelectUnitModule,
  NzCollapseModule,
  SharedUiMbTableWrapModule,
  SharedUiMbTableModule,
  SharedUiMbTextLabelModule,
  SharedUiMbSelectCheckAbleModule,
  NzDropDownModule,
  SharedUiOrgDataPickerModule
];

@NgModule({
  imports: [
    ...Modules,
    LearnDevelopmentRoutingModule,
  ],
  exports: [...Modules],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class LearnDevelopmentFeatureShellModule {}
