import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {FunctionCode} from "@hcm-mfe/learn-development/data-access/common";
import {
  LearnDevelopmentFeatureAppointmentRotationRoutingModule
} from "../../../appointment-rotation/src/lib/learn-development-feature-appointment-rotation-routing.module";

const routes: Routes = [
  {
    path: 'issue-level',
    loadChildren: () => import('@hcm-mfe/learn-development/feature/issue-level').then((m) => m.LearnDevelopmentFeatureIssueLevelModule),
    data: {
      code: FunctionCode.DEVELOPMENT_CAP_BAN_HANH,
      pageName: 'development.pageName.issue-level',
      breadcrumb: 'development.breadcrumb.issue-level',
    },
  },
  {
    path: 'approvement-ld-config',
    loadChildren: () => import('@hcm-mfe/learn-development/feature/approvement-config-ld').then((m) => m.LearnDevelopmentFeatureApprovementConfigLdModule),
    data: {
      // code: FunctionCode.DEVELOPMENT_APPROVE_CONFIG,
      pageName: 'development.pageName.approvement-config',
      breadcrumb: 'development.breadcrumb.approvement-config',
    },
  },
  {
    path: 'form-ld',
    loadChildren: () => import('@hcm-mfe/learn-development/feature/form-ld').then((m) => m.LearnDevelopmentFeatureFormLdModule),
    data: {
      code: FunctionCode.ADMIN_SESSIONS_HISTORY,
      pageName: 'development.pageName.form-ld',
      breadcrumb: 'development.breadcrumb.form-ld',
    },
  },
  {
    path: 'template',
    loadChildren: () => import('@hcm-mfe/learn-development/feature/template').then((m) => m.LearnDevelopmentFeatureTemplateModule),
    data: {
      code: FunctionCode.ADMIN_SESSIONS_HISTORY,
      pageName: 'development.pageName.template',
      breadcrumb: 'development.breadcrumb.template',
    },
  },
  {
    path: 'propose',
    loadChildren: () => import('@hcm-mfe/learn-development/feature/propose-unit').then((m) => m.LearnDevelopmentFeatureProposeUnitModule),
    data: {
      code: FunctionCode.ADMIN_SESSIONS_HISTORY,
      pageName: 'development.pageName.propose',
      breadcrumb: 'development.breadcrumb.propose',
    },
  },
  {
    path: 'proviso',
    loadChildren: () => import('@hcm-mfe/learn-development/feature/proviso-ld').then((m) => m.LearnDevelopmentFeatureProvisoLdModule),
    data: {
      code: FunctionCode.ADMIN_SESSIONS_HISTORY,
      pageName: 'development.pageName.proviso-ld',
      breadcrumb: 'development.breadcrumb.proviso-ld',
    },
  },
  {
    path: 'consultation-config',
    loadChildren: () =>
      import('@hcm-mfe/learn-development/feature/consultation-config').then((m) => m.LearnDevelopmentFeatureConsultationConfigModule),
    data: {
      code: FunctionCode.ADMIN_SESSIONS_HISTORY,
      pageName: 'development.pageName.consultation-config',
      breadcrumb: 'development.breadcrumb.consultation-config',
    },
  },
  {
    path: 'interview',
    loadChildren: () => import('@hcm-mfe/learn-development/feature/interview-ld').then((m) => m.LearnDevelopmentFeatureInterviewLdModule),
    data: {
      code: FunctionCode.ADMIN_SESSIONS_HISTORY,
      pageName: 'development.pageName.interview-ld',
      breadcrumb: 'development.breadcrumb.interview-ld',
    },
  },
  {
    path: 'salary-list',
    loadChildren: () => import('@hcm-mfe/learn-development/feature/salary').then((m) => m.LearnDevelopmentFeatureSalaryModule),
    data: {
      code: FunctionCode.ADMIN_SESSIONS_HISTORY,
      pageName: 'development.pageName.salary-list',
      breadcrumb: 'development.breadcrumb.salary-list',
    },
  },
  {
    path: 'promulgate',
    loadChildren: () => import('@hcm-mfe/learn-development/feature/promulgate-ld').then((m) => m.LearnDevelopmentFeaturePromulgateLdModule),
    data: {
      code: FunctionCode.ADMIN_SESSIONS_HISTORY,
      pageName: 'development.pageName.promulgate',
      breadcrumb: 'development.breadcrumb.promulgate',
    }
  },
  {
    path: 'approvement-issue',
    loadChildren: () => import('@hcm-mfe/learn-development/feature/approvement-issue').then((m) => m.LearnDevelopmentFeatureApprovementIssueModule),
    data: {
      code: FunctionCode.ADMIN_SESSIONS_HISTORY,
      pageName: 'development.pageName.approvement-issue',
      breadcrumb: 'development.breadcrumb.approvement-issue',
    },
  },
  {
    path: 'approvement-propose',
    loadChildren: () =>
      import('@hcm-mfe/learn-development/feature/approvement-propose-ksptnl').then(
        (m) => m.LearnDevelopmentFeatureApprovementProposeKsptnlModule
      ),
    data: {
      code: FunctionCode.DEVELOPMENT_APPROVEMENT_PROPOSE,
      pageName: 'development.pageName.approvement-propose',
      breadcrumb: 'development.breadcrumb.approvement-propose',
    },
  },
  {
    path: 'list-consultations',
    loadChildren: () => import('@hcm-mfe/learn-development/feature/consultations').then((m) => m.LearnDevelopmentFeatureConsultationsModule),
    data: {
      code: FunctionCode.DEVELOPMENT_APPROVE_PROPOSE,
      pageName: 'development.pageName.list-of-consultations',
      breadcrumb: 'development.breadcrumb.list-of-consultations',
    },
  },
  {
    path: 'list-plan',
    loadChildren: () => import('@hcm-mfe/learn-development/feature/list-plan').then((m) => m.LearnDevelopmentFeatureListPlanModule),
    data: {
      code: FunctionCode.DEVELOPMENT_APPROVE_PROPOSE,
      pageName: 'development.pageName.list-plan',
      breadcrumb: 'development.breadcrumb.list-plan',
    },
  },
  {
    path: 'confirm-promulgate',
    loadChildren: () => import('@hcm-mfe/learn-development/feature/confirm-promulgate').then((m)=>  m.LearnDevelopmentFeatureConfirmPromulgateModule),
    data:{
      // code: FunctionCode
      pageName: 'development.pageName.confirm-promulgate',
      breadcrumb: 'development.breadcrumb.confirm-promulgate',
    }
  },

  {
    path: 'interview-confirm',
    loadChildren: () => import('@hcm-mfe/learn-development/feature/interview-confirm').then((m) => m.LearnDevelopmentFeatureInterviewConfirmModule),
    data: {
      code: FunctionCode.ADMIN_SESSIONS_HISTORY,
      pageName: 'development.pageName.interview-confirm',
      breadcrumb: 'development.breadcrumb.interview-confirm',
    },
  },
  {
    path: 'issued-decisions',
    loadChildren: () => import('@hcm-mfe/learn-development/feature/issued-decisions').then((m) => m.LearnDevelopmentFeatureIssuedDecisionsModule),
    data: {
      code: FunctionCode.ADMIN_SESSIONS_HISTORY,
      pageName: 'development.pageName.issued-decisions',
      breadcrumb: 'development.breadcrumb.issued-decisions',
    },
  },
  {
    path: 'recommend-individual',
    loadChildren: () => import('@hcm-mfe/learn-development/feature/recommend-individual').then((m) => m.LearnDevelopmentFeatureRecommendIndividualModule),
    data: {
      code: FunctionCode.ADMIN_SESSIONS_HISTORY,
      pageName: 'development.pageName.personal-recommendation',
      breadcrumb: 'development.breadcrumb.personal-recommendation',
    },
  },
  {
    path: 'list-issue-transfer',
    loadChildren: () => import('@hcm-mfe/learn-development/feature/list-change-issue').then((m) => m.LearnDevelopmentFeatureListChangeIssueModule),
    data: {
      code: FunctionCode.DEVELOPMENT_LIST_ISSUE_TRANSFER,
      pageName: 'development.pageName.list-change-issue',
      breadcrumb: 'development.breadcrumb.list-change-issue',
    },
  },
  {
    path: 'appointment-rotation',
    loadChildren: () => import('@hcm-mfe/learn-development/feature/appointment-rotation').then((m) => m.LearnDevelopmentFeatureAppointmentRotationModule),
    data: {
      code: FunctionCode.ADMIN_SESSIONS_HISTORY,
      pageName: 'development.pageName.appointment-rotation-view',
      breadcrumb: 'development.breadcrumb.appointment-rotation-view',
    },
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LearnDevelopmentRoutingModule {}
