import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ScreenType} from "@hcm-mfe/learn-development/data-access/common";
import {TemplateActionComponent, TemplateViewComponent} from "./pages";

const routes: Routes = [
  { path: '', component: TemplateViewComponent },
  {
    path: 'create',
    component: TemplateActionComponent,
    data: {
      pageName: 'development.pageName.template-create',
      breadcrumb: 'development.breadcrumb.template-create',
      type: ScreenType.Create,
    },
  },
  {
    path: 'update',
    component: TemplateActionComponent,
    data: {
      pageName: 'development.pageName.template-update',
      breadcrumb: 'development.breadcrumb.template-update',
      type: ScreenType.Update,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TemplateRoutingModule {}
