import { Component, Injector, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Constants, FunctionCode, userConfig, Utils } from "@hcm-mfe/learn-development/data-access/common";
import {
  bodyExcel,
  CategoryModel,
  FormLDContent,
  IssueLevel,
  Template
} from "@hcm-mfe/learn-development/data-access/models";
import { TemplateService } from "@hcm-mfe/learn-development/data-access/services";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import { cleanData } from "@hcm-mfe/shared/common/utils";
import { MBTableConfig, Pageable, Pagination } from "@hcm-mfe/shared/data-access/models";
import * as FileSaver from 'file-saver';
import * as moment from 'moment';
import { NzSafeAny } from "ng-zorro-antd/core/types";
import { NzModalRef } from 'ng-zorro-antd/modal';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-template-view',
  templateUrl: './template-view.component.html',
  styleUrls: ['./template-view.component.scss'],
})
export class TemplateViewComponent extends BaseComponent implements OnInit {
  @ViewChild('action', {static: true}) action!: TemplateRef<NzSafeAny>;
  @ViewChild('multipleLevels', {static: true}) multipleLevels!: TemplateRef<NzSafeAny>;

  dataTable: Template[] = [];
  scrollY = '';
  titlePage = '';
  tableConfig!: MBTableConfig;
  pagination = new Pagination();
  heightTable = { x: '100vw', y: '25em'};
  dataCommon = {
    issueLevelObj: <IssueLevel[]>[],
    templateTypeObj: <CategoryModel[]>[],
    subTemplateTypeObj: <CategoryModel[]>[],
    formsObj: <FormLDContent[]>[],
    proposeType: <CategoryModel[]>[],
  };
  params = {
    effectiveStartDate: null,
    effectiveEndDate: null,
    flag: true,
    templateType: null,
    issueLevel: null,
    page: 0,
    size: userConfig.pageSize,
  };
  pageable: Pageable = {
    totalElements: 0,
    currentPage: 0,
    size: userConfig.pageSize,
  };

  modalRef?: NzModalRef;
  private readonly subs: Subscription[] = [];

  disabledStartDate = (startValue: Date): boolean => {
    if (!startValue || !this.params.effectiveEndDate) {
      return false;
    }
    return startValue.getTime() > (this.params.effectiveEndDate as Date).getTime();
  };

  disabledEndDate = (endValue: Date): boolean => {
    if (!endValue || !this.params.effectiveStartDate) {
      return false;
    }
    return endValue.getTime() <= (this.params.effectiveStartDate as Date).getTime();
  };

  constructor(injector: Injector, private readonly service: TemplateService) {
    super(injector);

  }

  ngOnInit(): void {
    this.initTable();
    this.isLoading = true;
    this.titlePage = this.route.snapshot?.data['pageName'];
    this.service.getState().subscribe((data) => {
      this.dataCommon = {
        issueLevelObj: data?.issueLevelObj || [],
        templateTypeObj: data?.templateTypeObj || [],
        subTemplateTypeObj: data?.subTemplateTypeObj || [],
        formsObj: data?.formsObj || [],
        proposeType: data?.proposeType || [],
      };
      this.search(true);
    });
  }

  pageChange(page: number) {
    if (this.isLoading) {
      return;
    }
    this.params.page = page - 1;
    this.search();
  }

  search(firstPage?: boolean) {
    this.isLoading = true;
    if (firstPage) {
      this.params.page = 0;
    }
    const newParams = { ...cleanData(this.params) };
    newParams.effectiveStartDate = newParams.effectiveStartDate
      ? moment(newParams.effectiveStartDate).format(Constants.FORMAT_DATE.YYYY_MM_DD)
      : newParams.effectiveStartDate;
    newParams.effectiveEndDate = newParams.effectiveEndDate
      ? moment(newParams.effectiveEndDate).format(Constants.FORMAT_DATE.YYYY_MM_DD)
      : newParams.effectiveEndDate;
    this.service.search(newParams).subscribe(
      (response) => {
        this.sessionService.setSessionData(FunctionCode.DEVELOPMENT_APPROVE_CONFIG, this.dataCommon);
        this.dataTable = response?.data?.template?.content || [];
        this.tableConfig.total = response?.data?.template?.totalElements || 0;
        this.pageable.currentPage = this.params.page;
        this.isLoading = false;
        this.scrollY = Utils.setScrollYTable('table', this.tableConfig.total! > 15 ? Constants.HeightPaginator : 0);
      },
      (e) => {
        this.isLoading = false;
        this.getMessageError(e);
        this.dataTable = [];
      }
    );
  }

  detail(code: string) {
    this.router.navigate([this.router.url, 'update'], {
      queryParams: {
        code: code,
      },
      skipLocationChange: true,
    });
  }

  deleteItem(code:any) {
    if (this.isLoading) {
      return;
    }
    this.modalRef = this.modal.warning({
      nzTitle: this.translate.instant('development.formLD.messages.deleteTitle'),
      nzCancelText: this.translate.instant('development.consultation-config.messages.no'),
      nzOkText: this.translate.instant('development.consultation-config.messages.yes'),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        this.service.deleteItem(code).subscribe(
          () => {
            this.toastrCustom.success(this.translate.instant('development.template.messages.deleteSuccess'));
            this.search();
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
      },
    });
  }

  downLoadFile(item: Template) {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;
    this.service.downloadFile(item.attachedFiles!, item.fileName!).subscribe(
      () => {
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

  exportExcel() {
    if (this.dataTable?.length === 0) {
      this.toastrCustom.warning(
        this.translate.instant(
          'development.proposed-unit.messages.dataTableEmpty'
        )
      );
      return;
    }
    const body: bodyExcel = {
      effectiveStartDate: this.params.effectiveStartDate ? moment(this.params.effectiveStartDate).format('yyyy-MM-DD') : null,
      effectiveEndDate: this.params.effectiveEndDate ? moment(this.params.effectiveEndDate).format('yyyy-MM-DD') : null,
      templateType: this.params.templateType!,
      issueLevel: this.params.issueLevel!,
    };
    this.isLoading = true;
    this.service.exportExcel(body).subscribe(
      (data) => {
        let urlExcel = 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,';
        urlExcel = urlExcel + data.data;
        FileSaver.saveAs(urlExcel, this.translate.instant('development.template.messages.excelFileName'));
        this.toastrCustom.success(this.translate.instant('development.template.messages.exportExcelSuccess'));
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'development.template.search.nameTemplate',
          field: 'templateName',
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
          fixedDir: 'left',
        },
        {
          title: 'development.template.search.templateType',
          width: 150,
          field: 'templateType',
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          fixedDir: 'left',
        },
        {
          title: 'development.template.search.subTemplateType',
          field: 'subTemplateType',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
          fixedDir: 'left',
        },
        {
          title: 'development.template.search.proposeType2',
          field: 'proposeType',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
          fixedDir: 'left',

        },
        {
          title: 'development.template.search.formName',
          field: 'formName',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
        },
        {
          title: 'development.template.search.issueLevel',
          field: 'issuedLevelsName',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
        },
        {
          title: 'development.template.search.signerCode',
          field: 'signerCode',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 200,
        },
        {
          title: 'development.template.search.multipleLevels',
          field: '',
          tdTemplate: this.multipleLevels,
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 200,
        },
        {
          title: 'development.template.search.effectiveStartDate',
          field: 'effectiveStartDate',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
        },
        {
          title: 'development.template.search.effectiveEndDate',
          field: 'effectiveEndDate',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
        },
        {
          title: '',
          field: '',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_NOWRAP,Constants.TABLE_CONFIG.TEXT_CENTER], thClassList: [Constants.TABLE_CONFIG.TEXT_NOWRAP,Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 100,
          tdTemplate: this.action,
          fixed: true,
          fixedDir: 'right',
        }

      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: userConfig.pageSize,
      pageIndex: 1,
    };
  }



  override ngOnDestroy() {
    this.modalRef?.destroy();
    this.subs.forEach((s) => s.unsubscribe());
  }
}
