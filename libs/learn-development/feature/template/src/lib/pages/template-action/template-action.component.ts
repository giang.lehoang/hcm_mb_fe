import { AfterViewInit, Component, Injector, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { Constants, ScreenType, _1Mb } from "@hcm-mfe/learn-development/data-access/common";
import { CategoryModel, FormLDContent, IssueLevel, Template } from "@hcm-mfe/learn-development/data-access/models";
import { TemplateService } from "@hcm-mfe/learn-development/data-access/services";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import { cleanDataForm, Utils } from "@hcm-mfe/shared/common/utils";
import { SelectCheckAbleModal } from "@hcm-mfe/system/data-access/models";
import * as moment from 'moment';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { Observable, Observer } from 'rxjs';

@Component({
  selector: 'app-template-action',
  templateUrl: './template-action.component.html',
  styleUrls: ['./template-action.component.scss'],
})
export class TemplateActionComponent extends BaseComponent implements OnInit, AfterViewInit {
  dataCommon = {
    issueLevelObj: <IssueLevel[]>[],
    templateTypeObj: <CategoryModel[]>[],
    subTemplateTypeObj: <CategoryModel[]>[],
    formsObj: <FormLDContent[]>[],
    proposeType: <CategoryModel[]>[],
  };
  signers: CategoryModel[] = [
    {
      code: 'Người ký',
      name: 'Người ký',
    },
    {
      code: 'Người duyệt',
      name: 'Người duyệt',
    },
  ];
  termSubTemplateType: CategoryModel[] = [];
  termProposeType: CategoryModel[] = [];
  isSubmitted = false;
  linkBITemplate?: '';
  form = this.fb.group({
    typeCode: [null, Validators.required],
    subTypeCode: null,
    proposeType: null,
    formCode: null,
    issuedLevelsCode: [null, Validators.required],
    signerCode: null,
    attachedFiles: null,
    multipleLevels: false,
    effectiveStartDate: [null, Validators.required],
    effectiveEndDate: null,
    attachedFileName: null,
    attachedFileId: null,
    linkBITemplate: [null, Validators.required],
    templateName: [null, Validators.required],
  });
  type?: ScreenType;
  title?: string;
  fileList: File[] = [];
  fileTemplate?: File;
  editFile?: boolean;
  itemTemplate?: Template;

  constructor(injector: Injector, private readonly service: TemplateService) {
    super(injector);

  }

  ngOnInit() {
    this.type = this.route.snapshot?.data['type'] || ScreenType.Create;
    this.title = this.route.snapshot?.data['pageName'];
    const code = this.route.snapshot.queryParams['code'];
    this.editFile = !code;
    this.isLoading = true;
    this.service.getState().subscribe(
      (state) => {
        this.dataCommon = {
          issueLevelObj: state?.issueLevelObj || [],
          templateTypeObj: state?.templateTypeObj || [],
          subTemplateTypeObj: [],
          formsObj: state?.formsObj || [],
          proposeType: [],
        };
        state?.subTemplateTypeObj?.forEach((item: any) => {
          this.termSubTemplateType.push({ code: item.code, name: item.name, parentCode: item.templateTypeCode });
        });
        this.termProposeType = state?.proposeType || [];
        if (this.type !== ScreenType.Create) {
          this.service.findByCode(code).subscribe(
            (data) => {
              this.itemTemplate = data;
              data.effectiveEndDate = data.effectiveEndDate ? new Date(data.effectiveEndDate) : null;
              data.effectiveStartDate = data.effectiveStartDate ? new Date(data.effectiveStartDate) : null;
              this.form.patchValue(data || {});
              this.isLoading = false;
            },
            (e) => {
              this.isLoading = false;
              this.getMessageError(e);
            }
          );
        } else {
          this.isLoading = false;
        }
      },
      (e) => {
        this.isLoading = false;
        this.getMessageError(e);
      }
    );
  }

  ngAfterViewInit(): void {
    this.form.get('typeCode')?.valueChanges.subscribe((value) => {
      this.dataCommon.subTemplateTypeObj = this.termSubTemplateType.filter((item) => item.parentCode === value);
      this.form.get('subTypeCode')?.setValue(null);
      if (value === Constants.TemplateType.QD) {
        this.form.get('formCode')?.setValidators(Validators.required);
      } else {
        this.form.get('formCode')?.setValidators(null);
        this.form.get('formCode')?.setValue(null);
        this.form.get('formCode')?.disable();
      }
      this.form.get('formCode')?.updateValueAndValidity();
    });
    this.form.get('subTypeCode')?.valueChanges.subscribe((value) => {
      if (value === Constants.SubTemplateType.DX) {
        this.dataCommon.proposeType = this.termProposeType;
        this.form.get('proposeType')?.setValidators(Validators.required);
      } else {
        this.dataCommon.proposeType = [];
        this.form.get('proposeType')?.setValidators(null);
      }
      this.form.get('proposeType')?.updateValueAndValidity();
    });
    this.form.get('proposeType')?.valueChanges.subscribe((value) => {
      if (value) {
        this.form.get('issuedLevelsCode')?.setValidators(null);
        this.form.get('signerCode')?.setValidators(null);
      } else {
        this.form.get('issuedLevelsCode')?.setValidators(Validators.required);
        this.form.get('signerCode')?.setValidators(Validators.required);
      }
      this.form.get('issuedLevelsCode')?.updateValueAndValidity();
      this.form.get('signerCode')?.updateValueAndValidity();
    });
  }

  save() {
    this.isSubmitted = true;
    const dataForm = { ...cleanDataForm(this.form) };
    if (this.form.invalid || this.isLoading) {
      return;
    }
    this.isLoading = true;
    let api: any;
    const data = new FormData();
    dataForm.effectiveStartDate = dataForm.effectiveStartDate
      ? moment(dataForm.effectiveStartDate).format(Constants.FORMAT_DATE.YYYY_MM_DD)
      : dataForm.effectiveStartDate;
    dataForm.effectiveEndDate = dataForm.effectiveEndDate
      ? moment(dataForm.effectiveEndDate).format(Constants.FORMAT_DATE.YYYY_MM_DD)
      : dataForm.effectiveEndDate;
    if (Utils.isEmpty(dataForm.formCode)) {
      dataForm.formCode = [];
    }
    if (Utils.isEmpty(dataForm.issuedLevelsCode)) {
      dataForm.issuedLevelsCode = [];
    }
    data.append('dto', new Blob([JSON.stringify(dataForm)], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    if (this.type === ScreenType.Create) {
      api = this.service.create(data);
    } else if (this.type === ScreenType.Update) {
      api = this.service.update(data, this.itemTemplate?.code!);
    }
    api.subscribe(
      () => {
        if (this.type === ScreenType.Create) {
          this.toastrCustom.success(this.translate.instant('development.template.messages.addSuccess'));
        } else {
          this.toastrCustom.success(this.translate.instant('development.template.messages.updateSuccess'));
        }
        this.isLoading = false;
        this.router.navigateByUrl('/development/template');
      },
      (e: any) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }



  emitValue(item: SelectCheckAbleModal, field: string) {
    this.form.controls[field].setValue(item.listOfSelected);
  }

  beforeUpload = (file: File, _fileList: NzUploadFile[]): Observable<boolean> =>
    new Observable((observer: Observer<boolean>) => {
      const isJpgOrPng =
        file.type === Constants.TypeFile.doc ||
        file.type === Constants.TypeFile.docx ||
        file.type === Constants.TypeFile.xls ||
        file.type === Constants.TypeFile.xlsx;
      if (!isJpgOrPng) {
        this.toastrCustom.warning(this.translate.instant('shared.uploadFile.error.format'));
        observer.complete();
        return;
      }
      if (file.size > _1Mb) {
        this.toastrCustom.warning(this.translate.instant('shared.uploadFile.error.maxSizeByParam', { maxSize: '1Mb' }));
        observer.complete();
        return;
      }
      this.fileTemplate = file;
      observer.next(true);
      observer.complete();
    });

  disabledStartDate = (effectiveStartDate: Date): boolean => {
    const effectiveEndDate = this.form.get('effectiveEndDate')?.value;
    if (!effectiveStartDate || !effectiveEndDate) {
      return false;
    }
    return effectiveStartDate.getTime() > effectiveEndDate.getTime();
  };

  disabledEndDate = (effectiveEndDate: Date): boolean => {
    const effectiveStartDate = this.form.get('effectiveStartDate')?.value;
    if (!effectiveEndDate || !effectiveStartDate) {
      return false;
    }
    return effectiveEndDate.getTime() <= effectiveStartDate.getTime();
  };

  getLabelText(text: string, key: string) {
    return `${text} ${this.form.get(key)?.validator ? '<span class="label__required">*</span>' : ''}`;
  }

  openEditFile() {
    this.editFile = true;
  }
}
