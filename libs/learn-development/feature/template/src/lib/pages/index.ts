import { TemplateActionComponent } from './template-action/template-action.component';
import { TemplateViewComponent } from './template-view/template-view.component';

export const pages = [TemplateViewComponent, TemplateActionComponent];

export * from './template-action/template-action.component';
export * from './template-view/template-view.component';
