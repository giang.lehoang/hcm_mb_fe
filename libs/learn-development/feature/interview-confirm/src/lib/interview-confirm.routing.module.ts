import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InterviewConfirmSearchComponent } from './interview-confirm-search/interview-confirm-search.component';

const routes: Routes = [
  {
    path: '',
    component: InterviewConfirmSearchComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InterviewConfirmRoutingModule {}
