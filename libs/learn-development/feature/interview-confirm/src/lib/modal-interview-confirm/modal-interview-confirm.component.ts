import {
  Component,
  EventEmitter,
  Injector, OnInit,
  Output,
  Input
} from '@angular/core';
import { ProposedUnitService } from '@hcm-mfe/learn-development/data-access/services';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';

@Component({
  selector: 'app-modal-interview-confirm',
  templateUrl: './modal-interview-confirm.component.html',
  styleUrls: ['./modal-interview-confirm.component.scss'],
})
export class ModalInterviewConfirmComponent extends BaseComponent implements OnInit {
  @Output() reasonEvent = new EventEmitter<string>();
  @Output() closeEvent = new EventEmitter<boolean>();
  @Input() isConfirm: boolean = false;
  reason: string = '';
  constructor(injector: Injector, private service: ProposedUnitService) {
    super(injector);
  }

  ngOnInit() {
  }

  closeModal() {
    this.closeEvent.emit(false);
  }

  save() {
    if (!this.isConfirm && !this.reason.trim()) {
      this.toastrCustom.error(this.translate.instant('development.interview-config.field.confirmReason'));
    } else {
      this.reasonEvent.emit(this.reason);
    }
  }

}
