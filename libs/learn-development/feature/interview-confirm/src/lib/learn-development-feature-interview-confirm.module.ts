import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import {LearnDevelopmentFeatureShellModule} from "@hcm-mfe/learn-development/feature/shell";
import {PopupService} from "@hcm-mfe/shared/ui/popup";
import {InterviewConfirmRoutingModule} from "./interview-confirm.routing.module";
import { InterviewConfirmSearchComponent } from './interview-confirm-search/interview-confirm-search.component';
import {ModalInterviewConfirmComponent} from "./modal-interview-confirm/modal-interview-confirm.component";

@NgModule({
  declarations: [InterviewConfirmSearchComponent, ModalInterviewConfirmComponent],
  exports: [InterviewConfirmSearchComponent, ModalInterviewConfirmComponent],
  imports: [CommonModule, LearnDevelopmentFeatureShellModule, InterviewConfirmRoutingModule],
  providers: [PopupService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class LearnDevelopmentFeatureInterviewConfirmModule {}
