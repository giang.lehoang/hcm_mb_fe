import { ProposedUnitComponent } from './proposed-unit-view/proposed-unit.component';
import { ProposedUnitAddComponent } from './proposed-unit-add/proposed-unit-add.component';
import { ProposedUnitUpdateComponent } from './proposed-unit-update/proposed-unit-update.component';
import { ProposedUnitHandleComponent } from './proposed-unit-handle/proposed-unit-handle.component';
export const pages = [
  ProposedUnitComponent,
  ProposedUnitAddComponent,
  ProposedUnitUpdateComponent,
  ProposedUnitHandleComponent,
];
export * from './proposed-unit-view/proposed-unit.component';
export * from './proposed-unit-add/proposed-unit-add.component';
export * from './proposed-unit-update/proposed-unit-update.component';
export * from './proposed-unit-handle/proposed-unit-handle.component';