import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { Constants, ScreenType, SessionKey } from "@hcm-mfe/learn-development/data-access/common";
import { FormGroupList, FormGroupMultipleList, StatusList } from "@hcm-mfe/learn-development/data-access/models";
import { ProposedUnitService, ProposeMultipleEmployeeService } from "@hcm-mfe/learn-development/data-access/services";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import { forkJoin, Observable, of, Subscription } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ProposeMultipleComponent, ProposeSingleComponent } from '../../components';
import { ProposedAppointmentRenewalCreateComponent } from '../../components/proposed-appointment-renewal-create/proposed-appointment-renewal-create.component';

@Component({
  selector: 'app-proposed-unit-add-view',
  templateUrl: './proposed-unit-add.component.html',
  styleUrls: ['./proposed-unit-add.component.scss'],
})
export class ProposedUnitAddComponent extends BaseComponent implements OnInit {
  typeAction = ScreenType.Create;
  dataCommon = {
    jobDTOS: [], // vị trí
    typeDTOS: [], // loại đề xuất
    formDisplaySearchDTOS: [], // hình thức
    proposeRotationReasonConfigDTOS: [], // lý do luân chuyển
    flowStatusApproveConfigList: <StatusList[]>[],
    flowStatusPublishConfigList: <StatusList[]>[],
    formGroupList: <FormGroupList[]>[],
    formGroupMultipleList: <FormGroupMultipleList[]>[],
    conditionViolationList: [],
  };

  // form đơn vị đề xuất
  proposeUnitForm = this.fb.group({
    id: null,
    proposeUnitRequest: null,
    proposeUnitRequestName: [{ value: null, disabled: true }],
    relationType: true,
    formGroup: null,
  });

  screenCode = '';
  screenCodeConst = Constants.ScreenCode;
  formGroupConstants = Constants.FormGroupList;
  @ViewChild(ProposeSingleComponent) proposeSingle?: ProposeSingleComponent;
  @ViewChild(ProposeMultipleComponent) proposeMultiple?: ProposeMultipleComponent;
  @ViewChild(ProposedAppointmentRenewalCreateComponent) proposedAppointmentRenewal?: ProposedAppointmentRenewalCreateComponent;
  proposeId: number | null = null;
  private readonly subs: Subscription[] = [];
  viewScreenCreate = false;

  constructor(
    injector: Injector,
    private readonly service: ProposedUnitService,
    private readonly proposeMultipleService: ProposeMultipleEmployeeService
  ) {
    super(injector);
    const action = this.router.getCurrentNavigation()?.extras?.state ? this.router.getCurrentNavigation()?.extras?.state!['action'] : null;
    const proposeUnit = this.sessionService.getSessionData(
      `${SessionKey.URL_PROPOSE_ELEMENT}_${Constants.ProposeCategory.GROUP}`
    );
    if (proposeUnit && action === 'add') {
      this.proposeUnitForm.patchValue(proposeUnit);
      this.proposeId = proposeUnit.id;
    }
  }

  checkPageName() {
    const pageName = this.route.snapshot?.data['pageName'];
    if (pageName === 'development.pageName.proposedLD-add') {
      this.screenCode = this.screenCodeConst.PTNL;
    } else if (pageName === 'development.pageName.proposed-unit-add') {
      this.screenCode = this.screenCodeConst.HRDV;
    }
  }


  ngOnInit() {
    this.isLoading = true;
    this.checkPageName();
    let apis: Observable<any>[] = [];
    const state = this.sessionService.getSessionData(`${SessionKey.URL_PROPOSE_ELEMENT}_${this.screenCode}`);
    if (state) {
      apis.push(of(state));
    } else {
      apis.push(this.service.getState({screenCode: this.screenCode}).pipe(catchError(() => of(undefined))));
    }
    if (this.proposeId) {
      apis.push(of({ data: this.proposeId }));
    } else {
      apis.push(this.proposeMultipleService.getProposeId().pipe(catchError(() => of(undefined))));
    }

    const sub = forkJoin(apis).subscribe(
      ([state, result]) => {
        this.proposeId = result?.data;
        this.proposeUnitForm.get('id')?.setValue(this.proposeId);
        if (state) {
          this.dataCommon = state;
          this.sessionService.setSessionData(`${SessionKey.URL_PROPOSE_ELEMENT}_${this.screenCode}`, this.dataCommon);
          if (this.screenCode === this.screenCodeConst.HRDV) {
            this.proposeUnitForm.get('proposeUnitRequestName')?.setValue(state.currentUnit.orgName || '');
            this.proposeUnitForm.get('proposeUnitRequest')?.setValue(state.currentUnit.orgId || 0);
          }
        }
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }

  onChangeProposeType(e: number) {
    this.viewScreenCreate =  Constants.AddFormGroup.includes(e);
    this.proposeMultipleService.formGroupProposeMultiple(e).subscribe((res) => {
      this.dataCommon.formGroupMultipleList = res?.data || [];
      if (this.viewScreenCreate) {
        this.proposeMultiple?.getFormGroup(e);
      }
    })

  }

  override ngOnDestroy() {
    this.subs.forEach((s) => s.unsubscribe());
  }


}
