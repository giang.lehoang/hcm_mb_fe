import { Component, Injector, OnInit, TemplateRef, ViewChild } from '@angular/core';
import {
  Constants, functionUri,
  maxInt32,
  ScreenType,
  SessionKey,
  userConfig,
  Utils
} from "@hcm-mfe/learn-development/data-access/common";
import {
  FormGroupList,
  FormGroupMultipleList,
  FormTTQD,
  IssueLevelList,
  LevelTitle,
  ListOrgItem, OrgEmployee, ProposedError, ProposeDetail, ProposeLogs,
  StatusList
} from "@hcm-mfe/learn-development/data-access/models";
import { ProposedUnitService, ProposeMultipleEmployeeService } from "@hcm-mfe/learn-development/data-access/services";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import { cleanDataForm } from "@hcm-mfe/shared/common/utils";
import { Category, MBTableConfig, Pagination } from '@hcm-mfe/shared/data-access/models';
import { User } from "@hcm-mfe/system/data-access/models";
import { NzSafeAny } from "ng-zorro-antd/core/types";
import { NzModalRef } from 'ng-zorro-antd/modal';
import { forkJoin, Observable, of, Subscription } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ProposeMultipleComponent, ProposeSingleComponent, ResourceManagementComponent } from '../../components';

@Component({
  selector: 'app-proposed-unit-update-view',
  templateUrl: './proposed-unit-update.component.html',
  styleUrls: ['./proposed-unit-update.component.scss'],
})
export class ProposedUnitUpdateComponent extends BaseComponent implements OnInit {
  typeAction: ScreenType;
  dataCommon = {
    jobDTOS: [], // vị trí
    typeDTOS: [], // loại đề xuất
    formDisplaySearchDTOS: [], // hình thức
    proposeRotationReasonConfigDTOS: [], // lý do luân chuyển
    statusList: <Category[]>[],
    flowStatusApproveConfigList: <StatusList[]>[],
    flowStatusPublishConfigList: <StatusList[]>[],
    issueLevelList: <IssueLevelList[]>[],
    units: [],
    formGroupList: <FormGroupList[]>[],
    formGroupMultipleList: <FormGroupMultipleList[]>[],
    levelTitle:<LevelTitle[]>[],
    conditionViolationList: [],
  };

  // form đơn vị đề xuất
  proposeUnitForm = this.fb.group({
    id: null,
    isHo: null,
    proposeDetails: null, // formArray
    proposeStatement: null, // formGroup
    proposeDecision: null, //formGroup
    proposeLogs: null, //formArray
    proposeGroupingHistories: null, //formArray
    proposeUnitRequest: null,
    proposeUnitRequestName: [{ value: null, disabled: true }],
    originator: null,
    proposeType: null,
    relationType: null,
    externalApprove: null,
    status: null,
    flowStatusCode: null,
    statusPublishName: null,
    groupCode: null,
    sentDate: null,
    issueLevelCode: null,
    signerCode: null,
    approvalCode: null,
    signDate: null,
    approveDate: null,
    flowStatusName: null,
    formGroup: null,
    withdrawNote: null,
    handleCode: null,
    optionNote: null,
    isGroup: null,
    groupByKSPTNL: null,
  });
  proposeDTO: any;
  proposeId: number;
  status?: string;
  modalRef?: NzModalRef;
  proposeDetailId: number;
  proposeType = 0;
  screenCode = '';
  showMinTime = false;
  showMaxTime = false;

  @ViewChild('proposeSingle') proposeSingle?: ProposeSingleComponent;
  @ViewChild('proposeMultiple') proposeMultiple?: ProposeMultipleComponent;

  issueLevelName = '';
  displayReport = false;
  //config nzModal
  note = '';
  comment = '';
  checkUnitRequest = false;
  flowStatusCode = '';
  withdrawNoteCode = '';
  statusPublishCode = '';
  proposeCategory = '';
  proposeLogs: ProposeLogs[] = [];
  unitIssueLevel = this.translate.instant('development.proposed-unit.table.unit');
  unitIssueLevelLive = this.translate.instant('development.proposed-unit.table.branch');
  screenCodeConst = Constants.ScreenCode;
  violationCode = 0;
  formTTQD?: FormTTQD;
  proposeDecisionStatement?: FormTTQD;
  // relationType: string;
  stateUrl: any;
  functionCode: string;
  formRotationType = '';
  listInUnit?: ListOrgItem[];
  pageName = Constants.PageName;
  proposeGroupId: number;
  currentUser?: User;
  formRotationTypeConst = Constants.FormRotationType;
  flowCodeConst = Constants.FlowCodeConstants;
  flowStatusCodeDisplay = '';
  checkWithdrawType = false;
  proposeDetais: ProposeDetail[] = [];
  listProposeCategory = Constants.ProposeCategory;
  isSingle = false;
  isMultiple = false;
  displayReportElement = false;
  externalApproveParent = false;
  formGroupConstants = Constants.FormGroupList;
  getRolesUsername: string[] = [];
  iconStatus = Constants.IconStatus.UP;
  isExpand = false;
  heightTable = { x: '50vw', y: '16em'};
  tableConfig!: MBTableConfig;
  pagination = new Pagination();
  @ViewChild('dateLog', {static: true}) dateLog!: TemplateRef<NzSafeAny>;
  isAcceptPropose = false;
  pageNumber = 0;
  pageSize =  userConfig.pageSize;
  approveNote = '';

  @ViewChild(ResourceManagementComponent) resourceManagement?: ResourceManagementComponent;
  private readonly subs: Subscription[] = [];

  constructor(injector: Injector, private readonly service: ProposedUnitService, private readonly proposeMultipleService: ProposeMultipleEmployeeService) {
    super(injector);
    this.typeAction = this.route.snapshot.data['screenType'];
    this.functionCode = this.route.snapshot.data['code'];
    this.proposeId = +this.route.snapshot.queryParams['id'];
    this.proposeDetailId = this.route.snapshot.queryParams['proposeDetailId'];
    this.proposeCategory = this.route.snapshot.queryParams['proposeCategory'];
    this.flowStatusCode = this.route.snapshot.queryParams['flowStatusCode'];
    this.withdrawNoteCode = this.route.snapshot.queryParams['withdrawNoteCode'];
    this.stateUrl = this.router.getCurrentNavigation()?.extras || null;
    // Sửa ELEMENT trong GROUP
    this.proposeGroupId = this.route.snapshot.queryParams['proposeGroupId'] || '';
    const relationType = this.route.snapshot.queryParams['relationType'];
    this.proposeUnitForm.get('relationType')?.setValue(relationType);
  }

  checkUrl() {
    const pageName = this.route.snapshot?.data['pageName'];
    if (
      pageName === 'development.pageName.proposed-unit-update' ||
      pageName === 'development.pageName.proposed-unit-detail'
    ) {
      this.screenCode = this.screenCodeConst.HRDV;
    }
    if (
      pageName === 'development.pageName.proposedLD-update' ||
      pageName === 'development.pageName.proposedLD-detail' ||
      pageName === 'development.pageName.proposedLD-handle'
    ) {
      this.screenCode = this.screenCodeConst.PTNL;
    }


    if (
      pageName === 'development.pageName.proposed-unit-approve-detail' ||
      pageName === 'development.pageName.proposed-unit-approve-approve'
    ) {
      this.screenCode = this.screenCodeConst.LDDV;
    }
  }

  displayButonHeader(display: boolean) {
    this.displayReport = display;
    this.proposeUnitForm.get('externalApprove')?.setValue(display);
  }

  ngOnInit() {
    this.initTable();
    if (localStorage.getItem(SessionKey.CURRENCY_USER)) {
      this.currentUser = JSON.parse(localStorage.getItem(SessionKey.CURRENCY_USER)!);
    }
    this.proposeType = Utils.getGroupType(this.proposeCategory)!;
    this.checkUrl();
    this.proposeUnitForm.disable();
    this.isLoading = true;
    const paramSrcCode = {
      screenCode: this.screenCode,
    };
    const paramDetail = {
      ids: this.proposeId,
      screenCode: this.screenCode,
      flowStatusCode: this.flowStatusCode,
    };
    let api: Observable<any>;
    if (this.proposeCategory === Constants.ProposeCategory.ELEMENT) {
      api = this.service.getDetailElement(this.proposeDetailId, this.screenCode, this.proposeGroupId);
    } else {
      api = this.service.findById(paramDetail);
    }
    const subs = forkJoin([
      this.service.getState(paramSrcCode).pipe(catchError(() => of(undefined))),
      api,
      this.service
        .getLogsPage(
          this.proposeCategory === Constants.ProposeCategory.ELEMENT ? +this.proposeDetailId : +this.proposeId,
          this.proposeCategory,
          this.screenCode,
          this.pageNumber,
          this.pageSize
        )
        .pipe(catchError(() => of(undefined))),
      this.service.getRolesByUsername().pipe(catchError(() => of(undefined))),
    ]).subscribe(
      ([state, data, proposeLogs, roles]) => {
        if (state) {
          this.dataCommon = state;
          this.proposeDTO = data?.proposeDTO[0];
          this.proposeDetais = data?.proposeDTO[0]?.proposeDetails;
          this.checkWithdrawType = data?.proposeDTO[0].withdrawType === undefined;
          this.proposeUnitForm.get('proposeUnitRequest')?.setValue(this.proposeDTO?.proposeUnitRequest || 0);
          const requestObject: any = this.dataCommon.units.find((i: any) => i.orgId === this.proposeDTO?.proposeUnitRequest);
          this.proposeUnitForm.get('proposeUnitRequestName')?.setValue(requestObject?.orgName);
          if (this.proposeUnitForm.get('proposeUnitRequestName')?.value) {
            this.checkUnitRequest = true;
          }
          const proposeDecision = this.proposeDTO?.proposeDecision;
          const proposeStatementList = this.proposeDTO?.proposeStatementList || [];
          const reportTCNS = proposeStatementList?.find(
            (item: FormTTQD) => !item?.typeStatement);
          this.proposeDecisionStatement = {
            statementNumber: reportTCNS?.statementNumber || null,
            submissionDate: reportTCNS?.submissionDate || null,
            decisionNumber: proposeDecision?.decisionNumber || null,
            signedDate: proposeDecision?.signedDate || null,
            typeStatement: reportTCNS?.typeStatement || null,
          };
          if (this.proposeDTO) {
            if (this.proposeCategory === Constants.ProposeCategory.ELEMENT) {
              this.flowStatusCode = this.proposeDTO.proposeDetails[0].flowStatusCode;
              this.statusPublishCode = this.proposeDTO.proposeDetails[0].flowStatusPublishCode;
            } else {
              this.flowStatusCode = this.proposeDTO.flowStatusCode;
              this.statusPublishCode = this.proposeDTO.flowStatusPublishCode;
            }
            if (this.proposeDTO.commentConfirm) {
              this.note = this.proposeDTO?.commentConfirm;
            }

            const issueObject = this.dataCommon.issueLevelList.find((i) => i.code === this.proposeDTO.issueLevelCode);
            this.issueLevelName = issueObject?.name!;

            this.proposeDTO.relationType =
              this.proposeCategory === Constants.ProposeCategory.ELEMENT ? false : this.proposeDTO.relationType;
            if (!this.proposeDTO.relationType) {
              this.proposeDetailId = this.proposeDTO.proposeDetails[0].id;
              // Lấy ds đơn vị
              const levelOrgPathId = this.proposeDTO?.proposeDetails[0]?.employee?.orgPathId?.split('/');
              this.onChangeUnitDiff(levelOrgPathId[3]);
            }
            if (this.proposeDTO.proposeDetails[0]) {
              this.formRotationType = this.proposeDTO.proposeDetails[0].formRotationType;
              this.violationCode = this.proposeDTO.proposeDetails[0].conditionViolationCode;
            }
            this.flowStatusCodeDisplay = this.proposeDTO?.flowStatusCodeDisplay
              ? this.proposeDTO.flowStatusCodeDisplay : this.proposeDTO.flowStatusCode;
            this.proposeUnitForm.patchValue(this.proposeDTO);
            this.isSingle = this.proposeCategory === Constants.ProposeCategory.ELEMENT;
            this.isMultiple = this.proposeCategory !== Constants.ProposeCategory.ELEMENT;

            const withdrawNoteApi = this.proposeCategory === Constants.ProposeCategory.ELEMENT ? this.proposeDTO?.proposeDetails[0].withdrawNote : this.proposeDTO?.withdrawNote;
            const withdrawNote = data?.withdrawNoteConfigList.find(
              (i: StatusList) => i.key  === withdrawNoteApi?.toString())?.role;

            this.proposeUnitForm.controls['withdrawNote'].setValue(withdrawNote);
            const statusObject = this.dataCommon.flowStatusApproveConfigList.find(
              (item) => item.key === this.flowStatusCode
            );
            const flowCodeManyObject = this.dataCommon.flowStatusApproveConfigList?.find(
              (item) => item.key === this.flowStatusCodeDisplay
            );
            if (statusObject != null) {
              this.proposeUnitForm.patchValue({
                id: this.proposeCategory === Constants.ProposeCategory.ELEMENT
                  ? this.proposeDTO?.proposeDetails[0]?.proposeCode
                  : this.checkProposeCode(),
                flowStatusName:
                  this.proposeDTO.relationType ? flowCodeManyObject?.role : statusObject?.role,
              });
            }

            const statusPubishObject = this.dataCommon.flowStatusPublishConfigList.find(
              (item) => item.key === this.statusPublishCode
            );
            if (statusPubishObject != null) {
              this.proposeUnitForm.patchValue({
                statusPublishName: statusPubishObject?.role,
              });
            }
            this.proposeUnitForm.controls['formGroup'].setValue(+this.proposeDTO.formGroup);
            if (this.proposeDTO.formGroup) {
              this.onChangeProposeType(+this.proposeDTO.formGroup)
            }

            // Log
            this.proposeLogs = proposeLogs?.data?.content || [];
            this.externalApproveParent = this.proposeDTO?.externalApprove;
            this.getRolesUsername = roles;
            this.approveNote = this.proposeDTO.approveNote;
          }
        }
        this.isLoading = false;
      },

      (e) => {
        this.isLoading = false;
        if(e?.error?.code === Constants.CodeError.NOTEXISTRECORD) {
          switch (this.screenCode) {
            case this.screenCodeConst.PTNL:
              return (
                this.router.navigateByUrl(functionUri.propose_list)
              );
            case this.screenCodeConst.KSPTNL:
              return (
                this.router.navigateByUrl(functionUri.approvement_propose)
              );
            case this.screenCodeConst.HRDV:
              return (
                this.router.navigateByUrl(functionUri.propose_unit)
              );
            default:
              return this.back();
          }
        } else {
          this.getMessageError(e);
        }
      }
    );
    this.subs.push(subs);
  }

  checkProposeCode() {
    if (this.proposeCategory === Constants.ProposeCategory.ORIGINAL) {
      return this.proposeId;
    } else if (this.proposeCategory === Constants.ProposeCategory.ELEMENT) {
      return this.proposeDetailId;
    } else if (this.proposeCategory === Constants.ProposeCategory.GROUP) {
      return this.proposeId;
    }
    return null
  }

  checkLeaderApprove(status: string) {
    const listStatusLeaderApproved = ['1', '3'];
    return listStatusLeaderApproved.indexOf(status) > -1;
  }

  checkHandleButton() {
    const listFlowStatusCode = ['5B', '5C'];
    return listFlowStatusCode.indexOf(this.flowStatusCode) > -1;
  }

  checkWithdrawBtn(){
    const listWithdrawBtn = Constants.StatusButtonWithdraw;
    const listNoteCodeWithdraw = Constants.noteWithdraw;
    return listWithdrawBtn.indexOf(this.flowStatusCode) > -1 && (this.proposeCategory === Constants.ProposeCategory.ORIGINAL)
      && listNoteCodeWithdraw.indexOf(this.withdrawNoteCode) > -1
      && (this.screenCode === this.screenCodeConst.LDDV);
  }

  checkSaveButton() {
    const listFlowStatusCode = ['6', '8', '10B1', '10C'];
    return listFlowStatusCode.indexOf(this.flowStatusCode) > -1;
  }

  handle() {
    this.isLoading = true;
    const param = {
      id: this.proposeCategory === Constants.ProposeCategory.ELEMENT ? this.proposeDetailId : this.proposeId,
      proposeType: this.proposeType,
    };
    this.service.proposalProcessing(param).subscribe(
      (res) => {
        this.router.navigate([Utils.getUrlByFunctionCode(this.functionCode), 'handle'], {
          queryParams: {
            id: this.proposeId,
            proposeDetailId: this.proposeDetailId,
            proposeCategory: this.proposeCategory,
            flowStatusCode: res?.data || '',
            relationType: this.proposeUnitForm.get('relationType')?.value,
          },
          skipLocationChange: true,
        });
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

  checkBtnDelete() {
    return this.typeAction === ScreenType.Update &&
      (this.proposeCategory === Constants.ProposeCategory.ELEMENT
        ? this.proposeDTO?.proposeDetails[0].createdBy === this.currentUser?.username
        : this.proposeDTO?.createdBy === this.currentUser?.username) &&
      (this.screenCode === this.screenCodeConst.HRDV || this.screenCode === this.screenCodeConst.PTNL) &&
      Constants.DeleteButtonDetailStatus.includes(this.flowStatusCode);
  }

  delete(event: MouseEvent) {
    event.stopPropagation();
    if (this.isLoading) {
      return;
    }
    const groupProposeType = Utils.getGroupType(this.proposeCategory);
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.proposed-unit.messages.delete'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        this.service.deleteProposeListLD(this.checkProposeCode(), groupProposeType).subscribe(
          () => {
            this.toastrCustom.success(this.translate.instant('development.proposed-unit.messages.deleteSuccess'));
            this.back();
            this.modalRef?.destroy();
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
      },
    });
  }

  // duyệt rút đề xuất
  agreeWithdrawProposal(ideaType: number) {
    const checkId = this.proposeCategory === Constants.ProposeCategory.ORIGINAL ? this.proposeId : this.proposeDetailId;
    const checkProposeType = Utils.getGroupType(this.proposeCategory);
    const comment = this.proposeMultiple?.commentWithdraw || '';
    if (!comment && (ideaType === Constants.WithdrawConfirmIdeaType.REJECT
      || ideaType === Constants.WithdrawConfirmIdeaType.OTHERIDEA)) {
        this.scrollToValidationSection('commentWithdraw', 'development.commonMessage.approveIdeaRequired');
        return;
      }
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.proposed-unit.messages.agreeLeader2'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        if (!this.checkWithdrawType) {
          this.service.approveWithdrawPropose(checkId, checkProposeType, comment, ideaType, this.screenCode).subscribe(
          () => {
            this.toastrCustom.success(this.translate.instant(Constants.KeyCommons.MESSAGES_SUCCESS2));
            this.router.navigateByUrl(Constants.KeyCommons.DEVELOPMENT_PROPOSE_UNIT_APPROVE);
            this.isLoading = false;
            this.modalRef?.destroy();
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
        } else {
          const ids: number[] = [];
          this.proposeDetais.forEach((item) => {
            if (item.withdrawNote !== undefined ) {
              ids.push(item.id);
            }
          });
          this.service.approveWithdrawProposeDetail(ids, comment, ideaType, this.screenCode).subscribe(
            () => {
              this.toastrCustom.success(this.translate.instant(Constants.KeyCommons.MESSAGES_SUCCESS_APPROVED));
              this.router.navigateByUrl(Constants.KeyCommons.DEVELOPMENT_PROPOSE_UNIT_APPROVE);
              this.isLoading = false;
              this.modalRef?.destroy();
            },
            (e) => {
              this.getMessageError(e);
              this.isLoading = false;
            }
          );
        }
      },
    });
  }

  // duyệt đồng ý LDDV
  agreeLeader() {
    const noteLDDV = this.proposeCategory === Constants.ProposeCategory.ELEMENT
      ? this.proposeSingle?.noteLDDV || ''
      : this.proposeMultiple?.noteLDDV || '';
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.proposed-unit.messages.agreeLeader'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL2_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.CONTINUE_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        const id = this.proposeCategory === Constants.ProposeCategory.ELEMENT ? this.proposeDetailId : this.proposeId;
        const proposeType = this.proposeCategory === Constants.ProposeCategory.ELEMENT ? 2 : 1
        this.service.agreeLeader(id, noteLDDV, proposeType).subscribe(
          () => {
            this.toastrCustom.success(this.translate.instant(Constants.KeyCommons.MESSAGES_SUCCESS2));
            this.router.navigateByUrl(Constants.KeyCommons.DEVELOPMENT_PROPOSE_UNIT_APPROVE);
            this.isLoading = false;
            this.modalRef?.destroy();
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
      },
    });
  }

  // duyệt từ chối LDDV
  rejectLeader() {
    const noteLDDV = this.proposeCategory === Constants.ProposeCategory.ELEMENT
      ? this.proposeSingle?.noteLDDV || ''
      : this.proposeMultiple?.noteLDDV || '';
    if (!noteLDDV) {
      this.toastrCustom.error(this.translate.instant('development.commonMessage.approveIdeaRequired'));
      let el = document.getElementById('idea');
      el?.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
      return;
    }
    const id = this.proposeCategory === Constants.ProposeCategory.ELEMENT ? this.proposeDetailId : this.proposeId;
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.proposed-unit.messages.rejectLeader'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        this.service.rejectLeader(id, noteLDDV).subscribe(
          () => {
            this.toastrCustom.success(this.translate.instant(Constants.KeyCommons.MESSAGES_SUCCESS2));
            this.router.navigateByUrl(Constants.KeyCommons.DEVELOPMENT_PROPOSE_UNIT_APPROVE);
            this.isLoading = false;
            this.modalRef?.destroy();
          },
          (e) => {
            this.toastrCustom.error(this.translate.instant('development.proposed-unit.messages.errorMes'));
            this.isLoading = false;
          }
        );
      },
    });
  }

  // duyệt ý kiến khác LDDV
  otherIdea() {
    const noteLDDV = this.proposeCategory === Constants.ProposeCategory.ELEMENT
      ? this.proposeSingle?.noteLDDV || ''
      : this.proposeMultiple?.noteLDDV || '';
    if (!noteLDDV) {
      this.toastrCustom.error(this.translate.instant('development.commonMessage.approveIdeaRequired'));
      let el = document.getElementById('idea');
      el?.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
      return;
    }
    const id = this.proposeCategory === Constants.ProposeCategory.ELEMENT ? this.proposeDetailId : this.proposeId;
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.proposed-unit.messages.otherIdeaLDDV'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        this.service.otherIdea(id, noteLDDV, this.proposeType).subscribe(
          (res) => {
            this.toastrCustom.success(
              this.translate.instant(Constants.KeyCommons.MESSAGES_SUCCESS2)
            );
            this.router.navigateByUrl(Constants.KeyCommons.DEVELOPMENT_PROPOSE_UNIT_APPROVE);
            this.isLoading = false;
            this.modalRef?.destroy();
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
      },
    });
  }

  submit() {
    this.isAcceptPropose = false;
    const proposeForm = { ...cleanDataForm(this.proposeUnitForm) };
    if (!proposeForm.relationType) {
      this.save();
    } else {
      this.proposeMultiple?.submit();
    }
  }

  save() {
    this.isLoading = true;
    const paramSrcCode = {
      screenCode: this.screenCode,
    };
    if (this.flowStatusCode !== Constants.FlowCodeConstants.CHUAGUI1) {
      this.proposeSingle?.saveReportElement();
    }
    const update = this.service.update(this.proposeSingle?.submit(), paramSrcCode).subscribe(
      () => {
        this.toastrCustom.success(this.translate.instant('development.proposed-unit.messages.updateSuccess'));
        this.ngOnInit();
        this.isLoading = false;
      },
      (e) => {
        if (e?.error?.data) {
          this.toastrCustom.error(e?.error?.data?.map((item: any) => item.errorMessage).join(', '));
        } else if (e?.error?.message) {
          this.toastrCustom.error(e?.error?.message);
        } else {
          this.toastrCustom.error(this.translate.instant('shared.error.errorCode500'));
        }
        this.isLoading = false;
      }
    );
    this.subs.push(update)
  }

  creteStatementDecision(data: FormTTQD) {
    this.formTTQD = data;
    this.formTTQD.id = this.proposeId;
    this.formTTQD.typeStatement = this.screenCode === 'HCM-LND-HRDV' ? 0 : 1;
  }

  //ban hành
  issueDecision() {
    const data = { ...cleanDataForm(this.proposeUnitForm) };
    const dataDecisionStatement =
      data.relationType
        ? this.proposeMultiple?.emitDataIssueDecision()
        : this.proposeSingle?.saveStatementDecision(true);

    if (dataDecisionStatement) {
      this.service.saveStatementDecision(dataDecisionStatement).subscribe((res) => {
        this.modalRef = this.modal.confirm({
          nzTitle: this.translate.instant('development.proposed-unit.messages.issueDecision'),
          nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
          nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
          nzClassName: Constants.ClassName.LD_CONFIRM,
          nzOnOk: () => {
            this.isLoading = true;
            this.service.issueDecision(this.proposeId).subscribe(
              () => {
                this.toastrCustom.success(
                  this.translate.instant('development.proposed-unit.messages.issueDecisionSuccess')
                );
                this.router.navigateByUrl(Utils.getUrlByFunctionCode(this.functionCode));
                this.isLoading = false;
                this.modalRef?.destroy();
              },
              (e) => {
                this.getMessageError(e);
                this.isLoading = false;
              }
            );
          },
        });

      });
    } else {
      return;
    }
  }

  //gửi duyệt
  sendPropose() {
    this.isAcceptPropose = false;
    if (!this.proposeDTO?.relationType) {
      if (!this.proposeSingle?.validateSubmitForm()) {
        return;
      }
    }
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.proposed-unit.messages.sendPropose'),
      nzCancelText: this.translate.instant('development.proposed-unit.button.cancel'),
      nzOkText: this.translate.instant('development.proposed-unit.button.continue'),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        if (this.violationCode === Constants.ViolationCode.REQUIRED) {
          this.isLoading = false;
          this.modalRef = this.modal.confirm({
            nzTitle: this.translate.instant('development.proposed-unit.messages.sendProposeWarning2'),
            nzCancelText: this.translate.instant('development.proposed-unit.button.cancel'),
            nzOkText: this.translate.instant('development.proposed-unit.button.continue'),
            nzClassName: Constants.ClassName.LD_CONFIRM,
            nzOnOk: () => {
              this.isLoading = true;
              this.callApiSendPropose();
              this.modalRef?.destroy();
            },
          });
        } else {
          this.callApiSendPropose();
        }
      },
    });
  }

  callApiSendPropose() {
    if (this.proposeUnitForm.get('relationType')?.value) {
      this.proposeMultiple?.submit();
    } else {
      this.proposeSingle?.saveReportElement();
    }
    const send = this.service.sendToBrower(this.checkProposeCode(), this.proposeType,
      !this.proposeUnitForm.get('relationType')?.value ? this.proposeSingle?.submit() : null).subscribe(
      (res) => {
        if (res?.data && res?.data?.length > 0) {
          this.toastrCustom.error(res?.data.map((item: ProposedError) => item.errorMessage).join(', '));
        } else {
          this.toastrCustom.success(this.translate.instant('development.proposed-unit.messages.sendProposeSuccess'));
          this.router.navigateByUrl(Utils.getUrlByFunctionCode(this.functionCode));
        }
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    this.subs.push(send)
  }

  acceptPropose() {
    this.isAcceptPropose = true;
    if (this.proposeUnitForm.get('relationType')?.value) {
      if (!this.proposeMultiple?.validApproveButton()) {
        this.toastrCustom.error(this.translate.instant('development.commonMessage.validateRecordApprove'));
        return;
      }
    } else {
      if (!this.proposeSingle?.validateSubmitForm()) {
        return;
      }
      if (!this.proposeSingle?.validApproveButton()) {
        this.toastrCustom.error(this.translate.instant('development.commonMessage.validateRecordApprove'));
        return;
      }
    }
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.proposed-unit.messages.acceptPropose'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        if (this.violationCode === Constants.ViolationCode.REQUIRED) {
          this.isLoading = false;
          this.modalRef = this.modal.confirm({
            nzTitle: this.translate.instant('development.proposed-unit.messages.sendProposeWarning2'),
            nzCancelText: this.translate.instant('development.proposed-unit.button.cancelText'),
            nzOkText: this.translate.instant('development.proposed-unit.button.okText'),
            nzClassName: 'ld-confirm',
            nzOnOk: () => {
              this.isLoading = true;
              this.saveTTQD();
            },
          })
        } else {
          this.saveTTQD();
        }
      },
    });
  }

  saveTTQD() {
    if (this.proposeUnitForm.get('relationType')?.value) {
      this.proposeMultiple?.submit();
    } else {
      this.proposeSingle?.saveReportElement();
    }
  }

  //gọi API ghi nhận đồng ý
  callApiAcceptPropose() {
    const reason =
      this.proposeUnitForm.get('relationType')?.value
        ? this.proposeMultiple?.getReasonsReject().trim()
        : this.proposeSingle?.getReasonsReject().trim();
    const sub = this.service.agreeUnitDCDD(this.checkProposeCode(), reason,
      this.proposeType, !this.proposeUnitForm.get('relationType')?.value ? this.proposeSingle?.submit() : null).subscribe(
      (res) => {
        if (res?.data && res?.data?.length > 0 ) {
          this.toastrCustom.error(res?.data.map((item: ProposedError) => item.errorMessage).join(', '));
        } else {
          this.toastrCustom.success(
            this.translate.instant('development.proposed-unit.messages.acceptProposeSuccess')
          );
          this.router.navigateByUrl(Utils.getUrlByFunctionCode(this.functionCode));
        }
        this.isLoading = false;
        this.modalRef?.destroy();
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }

  //LDDV k đồng ý
  reject() {
    let reason =
      this.proposeUnitForm.get('relationType')?.value
        ? this.proposeMultiple?.getReasonsReject().trim()
        : this.proposeSingle?.getReasonsReject().trim();
    if (!reason) {
      this.toastrCustom.error(this.translate.instant('development.proposed-unit.messages.reasonRequired'));
      return;
    }
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.proposed-unit.messages.rejectPropose'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        this.service.reject(this.proposeId, reason!).subscribe(
          () => {
            this.toastrCustom.success(
              this.translate.instant('development.proposed-unit.messages.rejectProposeSuccess')
            );
            this.router.navigateByUrl(Utils.getUrlByFunctionCode(this.functionCode));
            this.isLoading = false;
            this.modalRef?.destroy();
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
      },
    });
  }

  processingPTNL() {
    this.isLoading = true;
    const param = { id: this.proposeDetailId, proposeType: this.proposeType };
    const process = this.service.processingPTNL(param).subscribe(
      (res) => {
        this.toastrCustom.success(this.translate.instant('development.interview-config.notificationMessage.success'));
        this.router.navigateByUrl(Utils.getUrlByFunctionCode(this.functionCode));
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    this.subs.push(process)
  }

  override ngOnDestroy() {
    this.modalRef?.destroy();
    this.subs.forEach((sub) => sub.unsubscribe());
  }

  override back() {
    if (this.stateUrl?.state?.backUrl) {
      this.router.navigateByUrl(Utils.getUrlByFunctionCode(this.functionCode)).then(() => {
        this.router.navigateByUrl(this.stateUrl.state.backUrl, {
          skipLocationChange: true,
          state: {
            action: 'add',
          },
        });
      });
    } else {
      this.router.navigateByUrl(Utils.getUrlByFunctionCode(this.functionCode));
    }
  }

  onChangeUnitDiff(orgIdLevel: number) {
    if (orgIdLevel) {
      const param = {
        id: orgIdLevel,
        pag: 0,
        size: maxInt32,
      };
      const getOrganization = this.service.getOrganizationTreeById(param).subscribe((res) => {
        if (res && res?.content?.length > 0) {
          res?.content.forEach((i: OrgEmployee) => {
            if (i.treeLevel > 2) {
              i.orgNameFull = i.pathName.split("-").splice(2).join("-") + " - " + i.orgName;
            }
          });
          this.listInUnit = res?.content.map((item: OrgEmployee) => {
            return {
              name: item.orgNameFull,
              value: item.orgId,
              valueItem: {
                orgId: item.orgId,
                orgName: item.orgName,
                pathId: item.pathId,
                pathResult: item.pathName,
                orgLevel: item.treeLevel,
              },
            };
          });
        }
      });
      this.subs.push(getOrganization)
    }
  }

  onChangeProposeType(e: number) {
    const sub = this.proposeMultipleService.formGroupProposeMultiple(e).subscribe((res) => {
      this.dataCommon.formGroupMultipleList = res?.data || [];
      const formGroupObj = this.dataCommon.formGroupMultipleList?.find(
        (item) => item.formCode === this.proposeDTO.proposeDetails[0].formRotationType);
      this.showMinTime = formGroupObj?.showMinTime!;
      this.showMaxTime = formGroupObj?.showMaxTime!;
      this.proposeMultiple?.getFormGroup(+this.proposeDTO.formGroup);
    })
    this.subs.push(sub)
  }

  //check hiển thị 3 nút duyệt của LDDV và ý kiến phê duyệt
  checkApproveLDDV(){
    if (this.proposeCategory !== Constants.ProposeCategory.ELEMENT) {
      return this.checkIdeaApproveLDDV();
    } else {
      return this.screenCode === this.screenCodeConst.LDDV
        && this.flowStatusCode === this.flowCodeConst.SENDAPPROVE2A
        && this.flowStatusCodeDisplay !== this.flowCodeConst.SENDAPPROVE2A;
    }
  }

  onLoading(event: boolean) {
    this.isLoading = event;
  }

  //check hiển thị nút ghi nhận đồng ý
  checkRecordApprove(){
    if (this.proposeUnitForm.get('relationType')?.value) {
      return !this.displayReport
      && Constants.StatusApproveSendProposeMany.indexOf(this.flowStatusCode) > -1
      && this.proposeMultiple?.checkRadioIsHCM();
    } else {
      return !this.displayReportElement && (this.flowStatusCode === Constants.FlowCodeConstants.STATUS3A || this.flowStatusCode === this.flowCodeConst.STATUS3A1);
    }
  }

  //check hiển thị nút gửi duyệt
  checkSendApprove2(){
    if (!this.proposeUnitForm.get('relationType')?.value) {
      return this.displayReportElement
        && (this.flowStatusCode === this.flowCodeConst.STATUS3A || this.flowStatusCode === this.flowCodeConst.STATUS3A1)
        && this.flowStatusCodeDisplay !== this.flowCodeConst.STATUS3A;
    } else {
      return this.displayReport
        && Constants.StatusApproveSendProposeMany.indexOf(this.flowStatusCode) > -1
        && this.proposeMultiple?.checkRadioIsHCM();
    }
  }

  //check hiển thị nút ban hành
  checkIssueDecision(){
    return this.proposeUnitForm.get('relationType')?.value
      && Constants.IssueDecisionButtonStatus.indexOf(this.flowStatusCode) > -1;
  }

  //check hiển thị ý kiến phê duyệt của LDDV
  checkIdeaApproveLDDV() {
    return this.screenCode === this.screenCodeConst.LDDV
      && (this.flowStatusCode === this.flowCodeConst.CHODUYET2 ||
        this.flowStatusCode === this.flowCodeConst.SENDAPPROVE2A)
  }

  //check hiển thị 2 nút ghi nhận đồng ý và gửi duyệt trong màn thành phần
  displayButtonElement(radio: boolean) {
    this.displayReportElement = radio;
  }

  //load lại trang sau khi rút đề xuất
  loadPage(withdraw: boolean) {
    if (withdraw) {
      const paramDetail = {
        ids: this.proposeId,
        screenCode: this.screenCode,
        flowStatusCode: this.flowStatusCode,
      };
      let api: Observable<any>;
      if (this.proposeCategory === Constants.ProposeCategory.ELEMENT) {
        api = this.service.getDetailElement(this.proposeDetailId, this.screenCode, this.proposeGroupId);
      } else {
        api = this.service.findById(paramDetail);
      }
      const subs = forkJoin([
        api.pipe(catchError(() => of(undefined))),
        this.service
          .getLogsPage(
            this.proposeCategory === Constants.ProposeCategory.ELEMENT ? +this.proposeDetailId : +this.proposeId,
            this.proposeCategory,
            this.screenCode,
            this.pageNumber,
            this.pageSize
          )
          .pipe(catchError(() => of(undefined))),
      ]).subscribe(
        ([data, proposeLogs]) => {
          const proposeDTO = data?.proposeDTO[0];
          this.proposeDTO.withdrawNote = proposeDTO?.withdrawNote;
          this.flowStatusCode = proposeDTO?.flowStatusCode;
          const statusObject = this.dataCommon.flowStatusApproveConfigList.find(
            (item) => item.key === this.flowStatusCode
          );
          if (statusObject) {
            this.proposeUnitForm.patchValue({
              flowStatusName: statusObject?.role,
            });
          }
          const withdrawNoteApi = proposeDTO?.withdrawNote;
          const withdrawNote = data?.withdrawNoteConfigList.find(
              (i: StatusList) => i.key  === withdrawNoteApi?.toString())?.role;
          this.proposeUnitForm.controls['withdrawNote'].setValue(withdrawNote);
          this.proposeLogs = proposeLogs?.data?.content || [];
        });
    }
  }

  //check hiển thị nút lưu
  checkSaveButtonUpdateScreen() {
    return this.typeAction === this.screenType.update &&
      (this.screenCode === Constants.ScreenCode.HRDV
        || this.screenCode === Constants.ScreenCode.PTNL);
  }

  expandLog() {
    this.isExpand = !this.isExpand;
    if (this.isExpand) {
      this.iconStatus = Constants.IconStatus.DOWN;
    } else {
      this.iconStatus = Constants.IconStatus.UP;
    }
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'development.proposed-unit.proposeLogsTable.time',
          field: 'actionDate',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
          tdTemplate: this.dateLog
        },
        {
          title: 'development.proposed-unit.proposeLogsTable.personAction',
          field: 'userName',
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
        },
        {
          title: 'development.proposed-unit.proposeLogsTable.action',
          field: 'action',
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
        },
        {
          title: 'development.proposed-unit.proposeLogsTable.description',
          field: 'comment',
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 200,
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1,
    };
  }

  scrollToValidationSection(section: string, messsage: string) {
    this.toastrCustom.warning(this.translate.instant(messsage));
    const el = document.getElementById(section);
    el?.scrollIntoView({behavior: 'smooth', block: 'start', inline: 'nearest'});
  }

  //check ghi nhận đồng ý
  saveMultiple(isSave: boolean) {
    if (isSave && this.isAcceptPropose) {
      this.callApiAcceptPropose();
    }
  }
}
