import { Component, HostBinding, Injector, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import {
  Constants,
  functionUri,
  maxInt32,
  ScreenType,
  SessionKey,
  userConfig,
  Utils
} from "@hcm-mfe/learn-development/data-access/common";
import {
  DataCommon,
  DataMessageModel, DataPropose,
  FormGroupList,
  FormTTQD,
  ListOrgItem, ProposedError, ProposeDetail,
  ProposeLogs,
  ReturnPropose,
  StatusList,
  OrgEmployee,
  ExistingEmployee,
  ConcurrentPositionDTOList
} from "@hcm-mfe/learn-development/data-access/models";
import { ProposedUnitService, ProposeMultipleEmployeeService } from "@hcm-mfe/learn-development/data-access/services";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import { cleanData, cleanDataForm } from "@hcm-mfe/shared/common/utils";
import { User } from "@hcm-mfe/system/data-access/models";
import { InputBoolean } from 'ng-zorro-antd/core/util';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { NzUploadFile } from "ng-zorro-antd/upload";
import {catchError, forkJoin, Observable, of, Subscription} from 'rxjs';
import {
  ConsultationScreenComponent,
  ExistingEmployeeComponent,
  ProposeMultipleComponent,
  ProposeSingleComponent,
  ResourceManagementComponent,
  ReturnProposeComponent
} from '../../components';
import {MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {NzSafeAny} from "ng-zorro-antd/core/types";



@Component({
  selector: 'app-proposed-unit-handle-view',
  templateUrl: './proposed-unit-handle.component.html',
  styleUrls: ['./proposed-unit-handle.component.scss'],
})
export class ProposedUnitHandleComponent extends BaseComponent implements OnInit {
  messageListError?: DataMessageModel[];
  typeAction: ScreenType;
  dataCommon: DataCommon = {
    jobDTOS: [], // vị trí
    typeDTOS: [], // loại đề xuất
    formDisplaySearchDTOS: [], // hình thức
    proposeRotationReasonConfigDTOS: [], // lý do luân chuyển
    statusList: [],
    flowStatusApproveConfigList: [],
    flowStatusPublishConfigList: [],
    formGroupList: <FormGroupList[]>[],
    conditionViolationList: [],
  };

  // form đơn vị đề xuất
  proposeUnitForm = this.fb.group({
    id: null,
    proposeDetails: null, // formArray
    proposeStatement: null, // formGroup
    proposeDecision: null, //formGroup
    proposeLogs: null, //formArray
    proposeGroupingHistories: null, //formArray
    proposeUnitRequest: null,
    proposeUnitRequestName: [{ value: null, disabled: true }],
    originator: null,
    proposeType: null,
    relationType: null,
    flowStatusCode: null,
    statusPublishName: null,
    externalApprove: null,
    status: null,
    groupCode: null,
    sentDate: null,
    issueLevelCode: null,
    signerCode: null,
    approvalCode: null,
    signDate: null,
    approveDate: null,
    employeeUnitPathId: null,
    employeeUnitPathName: null,
    contentUnitPathId: null,
    contentUnitPathName: null,
    flowStatusName: null,
    formGroup: null,
    withdrawNote: null,
    approveNote: null,
    optionNote: null,
    handleCode: null,
  });
  proposeDTO: any;
  proposeId: number;
  idGroup: number;
  modalRef?: NzModalRef;
  proposeDetailId: number;
  screenCode = '';
  // relationTypeHandle: string;

  @ViewChild('proposeSingle') proposeSingle?: ProposeSingleComponent;
  @ViewChild('proposeMultiple') proposeMultiple?: ProposeMultipleComponent;
  @ViewChild('consultationScreenComponent') consultationScreenComponent?: ConsultationScreenComponent;
  @ViewChild('popUpError') modalContent?: TemplateRef<any>;

  //config nzModal
  proposeUnitApprove = false;
  note = '';
  comment = '';
  // cancelReason = '';
  // explanations = '';
  proposeUnit = false;
  proposeListPage = false;
  updateProposePage = false;
  salarySrc = false;
  proposeCategory = '';
  private readonly subs: Subscription[] = [];
  checkUnitRequest = false;
  flowStatusCode = '';
  statusPublishCode = '';
  idApprove = 0;
  formRotationType = '';
  flowStatusCodeDisplay = '';
  groupByKSPTNL?: boolean;
  checkgroupByKSPTNL = false;
  pageName = Constants.PageName;
  proposeCategoryParent = '';
  flowCodeConstants = Constants.FlowCodeConstants;
  relationTypeParent = false;
  proposeDetais: ProposeDetail[] = [];
  proposestatementNumber?: number;
  proposesubmissionDate?: Date;
  proposedocId?: number | undefined;
  proposedocName = '';
  withdrawNoteCode: number;
  withdrawNote?: number | boolean;
  checkWithdrawType: number | undefined;
  externalApprove = '';
  listProposeCategory = Constants.ProposeCategory;
  proposeType = 0;
  isSingle = false;
  isMultiple = false;
  checkFlowStatusElement = false;
  externalApproveParent = false;
  bodySendApprove?: ExistingEmployee;
  concurrentPositionDTOList?: ConcurrentPositionDTOList[];
  hasProcess = false;

  scrollTabs = [
    {
      title: 'development.pageName.tab1',
      scrollTo: 'tab1',
    },
    {
      title: 'development.pageName.tab2',
      scrollTo: 'tab2',
    },
    {
      title: 'development.pageName.tab3',
      scrollTo: 'tab3',
    },
    {
      title: 'development.pageName.tab4',
      scrollTo: 'tab4',
    },
    {
      title: 'development.pageName.tab5',
      scrollTo: 'tab5',
    },
    {
      title: 'development.pageName.tab6',
      scrollTo: 'tab6',
    },
    {
      title: 'development.pageName.tab7',
      scrollTo: 'tab7',
    },
    {
      title: 'development.pageName.tab8',
      scrollTo: 'tab8',
    },
  ];
  screenCodeConst = Constants.ScreenCode;
  stateUrl: any;
  viewParentID = 0;
  currentUser: User | undefined;
  rolesOfCurrentUser: string[] = [];

  @ViewChild(ResourceManagementComponent) resourceManagement?: ResourceManagementComponent;
  @HostBinding('class.scroll__tab--sticky') @Input() @InputBoolean() sticky = true;
  proposeDecisionStatement?: FormTTQD;
  positionDTOListReq = [];
  showMinTime = false;
  showMaxTime = false;
  showWarningSendPropose = false;

  checkUrl() {
    const pageName = this.route.snapshot?.data['pageName'];
    if (pageName === 'development.pageName.proposed-unit-update') {
      this.proposeUnit = true;
      this.screenCode = this.screenCodeConst.HRDV;
    } else if (Constants.PageNamePTNL.includes(pageName)) {
      this.proposeListPage = true;
      this.screenCode = this.screenCodeConst.PTNL;
    } else {
      this.proposeListPage = false;
    }

    if (pageName === 'development.pageName.proposedLD-update') {
      this.updateProposePage = true;
    }

    if (pageName === 'development.pageName.proposed-unit-approve-detail') {
      this.proposeUnitApprove = true;
      this.screenCode = this.screenCodeConst.LDDV;
    } else {
      this.proposeUnitApprove = false;
    }
    if (
      pageName === 'development.pageName.approvement-propose-detail' ||
      pageName === 'development.pageName.approvement-propose-handle' ||
      pageName === 'development.pageName.approvement-propose-approve'
    ) {
      this.screenCode = this.screenCodeConst.KSPTNL;
    }
  }

  proposeLogs: ProposeLogs[] = [];
  functionCode: string;
  isScreenHandle = false;
  isDeletePTNL = false;
  listInUnit?: ListOrgItem[];
  elementPrintReport = false;
  fileListSelectApprove: NzUploadFile[] = [];
  constantsFormGroupCode = Constants.FormGroupList;
  personalCode = ''

  //log
  isExpand = false;
  iconStatus = Constants.IconStatus.UP;
  heightTable = { x: '50vw', y: '16em'};
  tableConfig!: MBTableConfig;
  pagination = new Pagination();
  @ViewChild('dateLog', {static: true}) dateLog!: TemplateRef<NzSafeAny>;
  pageNumber = 0;
  pageSize =  userConfig.pageSize;
  reportTCNS?: FormTTQD;


  constructor(injector: Injector, private readonly service: ProposedUnitService, private readonly proposeMultipleService: ProposeMultipleEmployeeService) {
    super(injector);
    this.typeAction = this.route.snapshot.data['screenType'];
    this.functionCode = this.route.snapshot.data['code'];
    this.proposeUnitForm.disable();
    this.proposeId = +this.route.snapshot.queryParams['id'];
    this.proposeCategory = this.route.snapshot.queryParams['proposeCategory'] || '';
    this.idGroup = this.route.snapshot.queryParams['idGroup'];
    this.proposeDetailId = +this.route.snapshot.queryParams['proposeDetailId'];
    // Khác ELEMENT
    this.flowStatusCode = this.route.snapshot.queryParams['flowStatusCode'];
    this.proposeCategoryParent = this.route.snapshot.queryParams['categoryParent'];
    // lấy id của cha để lấy được tờ trình đính kèm của cha nêu có(màn KSPTNL)
    this.viewParentID = this.route.snapshot.queryParams['parentID'];
    this.stateUrl = this.router.getCurrentNavigation()?.extras || null;
    // Check màn hình xử lý
    this.isScreenHandle = this.route.snapshot?.data['handlePropose'];
    this.elementPrintReport = this.route.snapshot?.queryParams['elementPrintReport']
    this.withdrawNoteCode = this.route.snapshot.queryParams['withdrawNoteCode'];
    const relationType = this.route.snapshot.queryParams['relationType'];
    this.proposeUnitForm.get('relationType')?.setValue(relationType);
  }

  ngOnInit() {
    this.initTable();
    if (this.isScreenHandle) {
      this.proposeUnitForm.controls['optionNote'].enable();
    }
    this.proposeType = Utils.getGroupType(this.proposeCategory) ?? 0;
    if (localStorage.getItem(SessionKey.CURRENCY_USER)) {
      this.currentUser = JSON.parse(localStorage.getItem(SessionKey.CURRENCY_USER) ?? '');
    }
    this.checkUrl();
    this.isLoading = true;
    const paramSrcCode = {
      screenCode: this.screenCode,
    };
    // Satus để call API
    const paramDetail = {
      ids: this.proposeId,
      screenCode: this.screenCode,
      flowStatusCode: this.flowStatusCode,
    };
    if (this.screenCode === this.screenCodeConst.KSPTNL || this.screenCode === this.screenCodeConst.PTNL) {
      const idDelete = this.proposeCategory === Constants.ProposeCategory.ELEMENT ? this.proposeDetailId : this.proposeId;
      const groupProposeType = this.checkTypeKSPTNL();
      this.service.checkDeletePTNL(idDelete, groupProposeType).subscribe((res) => {
        this.isDeletePTNL = res?.data;
      });
    }
    let api: Observable<any>;
    if (this.proposeCategory === Constants.ProposeCategory.ELEMENT) {
      api = this.service.getDetailElement(this.proposeDetailId, this.screenCode, this.viewParentID);
    } else {
      api = this.service.findById(paramDetail);
    }
    const idLog = (this.proposeCategory === Constants.ProposeCategory.ORIGINAL || this.proposeCategory === Constants.ProposeCategory.GROUP)
      ? this.proposeId : +this.proposeDetailId;
    forkJoin([
      this.service.getRolesByUsername(),
      this.service.getState(paramSrcCode),
      this.service.getLogsPage(idLog, this.proposeCategory, this.screenCode, this.pageNumber, this.pageSize)
    ]).subscribe(([roles, state, logs])=>{
      if (state) {
        this.dataCommon = state;
        api.subscribe(
          (data) => {
            this.proposeDTO = data?.proposeDTO[0];
            this.personalCode = this.proposeDTO.personalCode
            this.proposestatementNumber = data?.statementNumber;
            this.proposesubmissionDate = data?.submissionDate;
            this.proposedocId = data?.docId;
            this.proposedocName = data?.fileName;
            this.hasProcess = data?.proposeDTO[0]?.proposeDetails[0]?.hasProcess;
            this.proposeDetais = data?.proposeDTO[0]?.proposeDetails;
            this.checkgroupByKSPTNL = data?.proposeDTO[0]?.groupByKSPTNL;
            this.externalApprove = data?.proposeDTO[0]?.externalApprove;
            this.checkWithdrawType = data?.proposeDTO[0].proposeDetails.find((item: ProposeDetail) => item?.withdrawType !== undefined)?.withdrawType;
            this.proposeUnitForm.get('proposeUnitRequestName')?.setValue(this.proposeDTO?.proposeUnitRequestName || '');
            this.proposeUnitForm.get('proposeUnitRequest')?.setValue(this.proposeDTO?.proposeUnitRequest || 0);
            if (this.proposeUnitForm.get('proposeUnitRequestName')?.value) {
              this.checkUnitRequest = true;
            }
            this.withdrawNote = data?.proposeDTO[0].proposeDetails.find((item: ProposeDetail) => item.withdrawNote)?.withdrawNote;
            if (this.proposeDTO) {
              this.relationTypeParent = this.proposeDTO?.relationType;
              const proposeStatementList = this.proposeDTO?.proposeStatementList || [];
              if (this.proposeCategory === Constants.ProposeCategory.GROUP) {
                this.reportTCNS = proposeStatementList?.find(
                  (item: FormTTQD) => item?.typeStatement === Constants.TypeStatement.TCNS);
              } else {
                this.reportTCNS = proposeStatementList?.find(
                  (item: FormTTQD) => !item?.typeStatement);
              }
              if (this.reportTCNS) {
                this.proposeDecisionStatement = {
                  statementNumber: this.reportTCNS?.statementNumber,
                  submissionDate: this.reportTCNS?.submissionDate,
                  decisionNumber: null,
                  signedDate: null,
                  typeStatement: this.reportTCNS?.typeStatement,
                };
              }
              if (this.proposeDTO.proposeDetails[0]) {
                if (this.proposeCategory === Constants.ProposeCategory.ELEMENT) {
                  this.flowStatusCode = this.proposeDTO.proposeDetails[0].flowStatusCode;
                  this.statusPublishCode = this.proposeDTO.proposeDetails[0].flowStatusPublishCode;
                } else {
                  // Get value flowStatusCode
                  this.flowStatusCode = this.proposeDTO.flowStatusCode;
                  //nếu là màn nhiều cá nhân thì lấy flowCodeDisplay để hiển thị
                  if (this.proposeDTO.relationType) {
                    this.flowStatusCodeDisplay = this.proposeDTO.flowStatusCodeDisplay;
                  }
                  this.statusPublishCode = this.proposeDTO.flowStatusPublishCode;

                }
                // view text flowStatusCode
                const statusObject = this.dataCommon.flowStatusApproveConfigList?.find(
                  (item) => item.key === this.flowStatusCode
                );
                // logs
                this.proposeLogs = logs?.data?.content || [];
                this.tableConfig.total = logs?.data?.totalElements || 0;
                this.tableConfig.pageIndex = 1;
                // Check trạng thái của thàng ELEMENT của GROUP
                this.proposeDTO.relationType =
                  this.proposeCategory === Constants.ProposeCategory.ELEMENT
                    ? false
                    : this.proposeDTO.relationType;
                this.proposeUnitForm.patchValue(this.proposeDTO);
                this.isSingle = (this.proposeCategory === Constants.ProposeCategory.ELEMENT || this.proposeCategory === Constants.ProposeCategory.ORIGINAL) && !this.proposeUnitForm.get('relationType')?.value;
                this.isMultiple = (this.proposeCategory === Constants.ProposeCategory.GROUP || this.proposeCategory === Constants.ProposeCategory.ORIGINAL) && this.proposeUnitForm.get('relationType')?.value;
                if (!this.proposeDTO.relationType) {
                  // Lấy ds đơn vị
                  const levelOrgPathId = this.proposeDTO?.proposeDetails[0]?.employee?.orgPathId?.split('/');
                  this.onChangeUnitDiff(levelOrgPathId[3]);
                }

                const statusPubishObject = this.dataCommon.flowStatusPublishConfigList?.find(
                  (item) => item.key === this.statusPublishCode
                );
                //flowCode để hiển thị trạng thái phê duyệt của nhiều cá nhân
                const flowCodeManyObject = this.dataCommon.flowStatusApproveConfigList?.find(
                  (item) => item.key === this.flowStatusCodeDisplay
                );
                this.proposeUnitForm.patchValue({
                  id: this.proposeCategory === Constants.ProposeCategory.ELEMENT
                    ? this.proposeDTO?.proposeDetails[0]?.proposeCode
                    : this.checkProposeCode(),
                });

                this.proposeUnitForm.patchValue({
                  flowStatusName:
                    this.proposeDTO.relationType ? flowCodeManyObject?.role : statusObject?.role,
                });

                if (statusPubishObject) {
                  this.proposeUnitForm.patchValue({
                    statusPublishName: statusPubishObject?.role,
                  });
                }
                const withdrawNote = data?.withdrawNoteConfigList.find((i: StatusList) => i.key  === this.proposeDTO?.withdrawNote?.toString())?.role;
                this.proposeUnitForm.controls['withdrawNote'].setValue(withdrawNote);
                if (this.proposeDTO?.formGroup) {
                  this.onChangeProposeType(+this.proposeDTO?.formGroup);
                }
              }
              this.externalApproveParent = this.proposeDTO?.externalApprove;
            }
            this.isLoading = false;
          },
          (e) => {
            this.isLoading = false;
            if(e?.error?.code === Constants.CodeError.NOTEXISTRECORD) {
              switch (this.screenCode) {
                case this.screenCodeConst.PTNL:
                  return (
                    this.router.navigateByUrl(functionUri.propose_list)
                  );
                case this.screenCodeConst.KSPTNL:
                  return (
                    this.router.navigateByUrl(functionUri.approvement_propose)
                  );
                case this.screenCodeConst.HRDV:
                  return (
                    this.router.navigateByUrl(functionUri.propose_unit)
                  );
                default:
                  return this.back();
              }
            } else {
              this.getMessageError(e);
            }
          }
        );
      }
      this.rolesOfCurrentUser = roles;
    },
    (e) => {
      this.getMessageError(e);
      this.isLoading = false;
    });
  }

  checkIdeaApproveKSPTNL() {
    const list = [Constants.ProposeCategory.ELEMENT, Constants.ProposeCategory.GROUP];
    return (
      this.screenCode === this.screenCodeConst.KSPTNL &&
      this.flowStatusCode === Constants.FlowCodeConstants.CHODUYET7 &&
      list.indexOf(this.proposeCategory) > -1
    );
  }

  checkBTNprocessingPTNL() {
    return this.proposeCategory === Constants.ProposeCategory.GROUP
  }

  checkSendPropose() {
    const listSendPropose = ['6', '8', '10B1', '10C'];
    if (this.proposeCategory === Constants.ProposeCategory.ELEMENT) {
      return listSendPropose.indexOf(this.flowStatusCode) > -1;
    } else if (this.proposeCategory === Constants.ProposeCategory.GROUP) {
      return listSendPropose.indexOf(this.proposeDTO?.flowStatusCode) > -1;
    } else if (this.proposeCategory === Constants.ProposeCategory.ORIGINAL) {
      return false;
    }
    return null;
  }

  checkProposeCode() {
    if (this.proposeCategory === Constants.ProposeCategory.ORIGINAL) {
      return this.proposeId;
    } else if (this.proposeCategory === Constants.ProposeCategory.ELEMENT) {
      return this.proposeDetailId;
    } else if (this.proposeCategory === Constants.ProposeCategory.GROUP) {
      return this.proposeId;
    }
    return null;
  }

  checkTypeCancelPlan() {
    const list = ['12.3A', '12.4'];
    return list.indexOf(this.flowStatusCode) > -1;
  }

  checkHandleButton() {
    switch (this.proposeCategory) {
      case Constants.ProposeCategory.ELEMENT:
        return Constants.ViewButtonHandleRemove.includes(this.flowStatusCode);
      case Constants.ProposeCategory.GROUP:
        return Constants.ViewButtonHandleRemove.includes(this.flowStatusCode)
          && this.proposeDTO?.createdBy === this.currentUser?.username;
      case Constants.ProposeCategory.ORIGINAL:
        return Constants.CheckDeleteBtn.includes(this.flowStatusCode) && !this.isScreenHandle;
      default: return false;
    }
  }

  checkDeleButtonKSPTNL() {
   return this.flowStatusCode === Constants.FlowCodeConstants.CHODUYET7 &&
     this.proposeCategory === Constants.ProposeCategory.GROUP && this.checkgroupByKSPTNL
  }

  checkSaveSendPropose() {
    return Constants.StatusSendApprovePTNL.indexOf(this.flowStatusCode) > -1;
  }

  //xóa đề xuất PTNL
  deleteHandle() {
    const proposeID = this.checkIdParams() ;
    const groupProposeType = this.checkTypeKSPTNL();
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.proposed-unit.messages.delete'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        this.service.deleteProposeListLD(proposeID, groupProposeType).subscribe(
          () => {
            this.toastrCustom.success(this.translate.instant('development.proposed-unit.messages.deleteSuccess'));
            if (this.stateUrl?.state?.['backUrl'] && this.proposeCategory !== Constants.ProposeCategory.ORIGINAL) {
              this.back();
            } else if (this.screenCode === Constants.ScreenCode.PTNL) {
              this.router.navigateByUrl(functionUri.propose_list);
            } else if (this.screenCode === Constants.ScreenCode.KSPTNL) {
              this.router.navigateByUrl(functionUri.approvement_propose);
            }
            this.isLoading = false;
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
      },
    });
  }

  checkIdParams() {
    const prototypeID = this.proposeCategory;
    if (prototypeID === Constants.ProposeCategory.GROUP) {
      return this.proposeId;
    } else if (prototypeID === Constants.ProposeCategory.ORIGINAL) {
      return this.proposeId;
    }
    return this.proposeDetailId;

  }

  checkTypeKSPTNL() {
    const prototypeID = this.proposeCategory;
    if (prototypeID === Constants.ProposeCategory.GROUP) {
      return 0;
    } else if (prototypeID === Constants.ProposeCategory.ORIGINAL) {
      return 1;
    }
    return 2;

  }

  approveParams = {
    groupProposeType: null,
    action: null,
  };

  messageParams = {
    message: null,
  };

  //duyệt đề xuất KSPTNL
  approveKSPTNL() {
    const newApprove = { ...cleanData(this.approveParams) };
    newApprove.groupProposeType = this.checkTypeKSPTNL();
    if (newApprove.groupProposeType === Constants.ProposeTypeID.ELEMENT) {
      this.idApprove = this.proposeDetailId;
    } else {
      this.idApprove = this.proposeId;
    }
    newApprove.action = Constants.ApproveKSPTNLAction.AGREE;
    const newMessage = { ...cleanData(this.messageParams) };
    newMessage.message = this.proposeMultiple?.noteKSPTNL;
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.approvement-propose.messages.approvedConfirm'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        this.service
          .approveKSPTNL(this.idApprove, newApprove.groupProposeType, newApprove.action, newMessage)
          .subscribe(
            () => {
              this.toastrCustom.success(this.translate.instant('development.approvement-propose.messages.approvedSuccess'));
              this.back();
              this.isLoading = false;
            },
            (e) => {
              this.getMessageError(e);
              this.isLoading = false;
            }
          );
      },
    });
  }

  otherIdeaKSPTNL() {
    const newApprove = { ...cleanData(this.approveParams) };
    newApprove.groupProposeType = this.checkTypeKSPTNL();
    if (newApprove.groupProposeType === Constants.ProposeTypeID.ELEMENT) {
      this.idApprove = this.proposeDetailId;
    } else {
      this.idApprove = this.proposeId;
    }
    newApprove.action = Constants.ApproveKSPTNLAction.OTHERIDEA;
    const newMessage = { ...cleanData(this.messageParams) };
    newMessage.message = this.proposeMultiple?.noteKSPTNL;
    if (!this.proposeMultiple?.noteKSPTNL) {
      this.toastrCustom.error(this.translate.instant('development.commonMessage.approveIdeaRequired'));
      const el = document.getElementById('idea');
      el?.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
      return;
    }
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.approvement-propose.messages.otherIdeaConfirm'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        this.service
          .approveKSPTNL(this.idApprove, newApprove.groupProposeType, newApprove.action, newMessage)
          .subscribe(
            () => {
              this.toastrCustom.success(this.translate.instant('development.approvement-propose.messages.otherIdeaSuccess'));
              this.back();
              this.isLoading = false;
            },
            (e) => {
              this.getMessageError(e);
              this.isLoading = false;
            }
          );
      },
    });
  }

  rejectLeader() {
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.proposed-unit.messages.rejectLeader'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        this.service.rejectLeader(this.proposeId, this.note).subscribe(
          () => {
            this.toastrCustom.success(this.translate.instant(Constants.KeyCommons.MESSAGES_SUCCESS2));
            this.location.back();
            this.isLoading = false;
          },
          (e) => {
            this.toastrCustom.error(this.translate.instant('development.proposed-unit.messages.errorMes'));
            this.isLoading = false;
          }
        );
      },
    });
  }

  otherIdea() {
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.proposed-unit.messages.otherIdea'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        this.service.otherIdea(this.proposeId, this.note, this.proposeType).subscribe(
          () => {
            this.toastrCustom.success(this.translate.instant(Constants.KeyCommons.MESSAGES_SUCCESS2));
            this.location.back();
            this.isLoading = false;
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
      },
    });
  }

  saveHandle() {
    if (this.proposeUnitForm.get('relationType')?.value) {
      this.savePropose();
    } else {
      const dataHandle = this.consultationScreenComponent?.saveHandle();
      if (this.consultationScreenComponent?.formReason?.valid) {
        this.service.saveHandle(this.proposeDetailId, dataHandle).subscribe(
          () => {
            this.isLoading = false;
            this.ngOnInit();
            this.toastrCustom.success(this.translate.instant('development.commonMessage.success'));
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
      }
    }
  }

  save(dataPropose: DataPropose | DataPropose[]) {
    const data = { ...cleanDataForm(this.proposeUnitForm) };
    if (data.relationType) {
      data.proposeDetails = dataPropose;
    } else {
      data.proposeDetails = [dataPropose];
    }
    this.isLoading = true;
    const paramSrcCode = {
      screenCode: this.screenCode,
    };
    this.service.update([data], paramSrcCode).subscribe(
      () => {
        this.toastrCustom.success(this.translate.instant('development.proposed-unit.messages.updateSuccess'));
        this.location.back();
        this.isLoading = false;
      },
      (e) => {
        if (e?.error?.data) {
          this.toastrCustom.error(e?.error?.data?.map((item: any) => item.errorMessage).join(', '));
        } else if (e?.error?.message) {
          this.toastrCustom.error(e?.error?.message);
        } else {
          this.toastrCustom.error(this.translate.instant('shared.error.errorCode500'));
        }
        this.isLoading = false;
      }
    );
  }

  reject() {
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.proposed-unit.messages.rejectPropose'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        this.service.reject(this.proposeId, this.note).subscribe(
          () => {
            this.toastrCustom.success(this.translate.instant('development.proposed-unit.messages.rejectProposeSuccess'));
            this.back();
            this.isLoading = false;
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
      },
    });
  }

  processingPTNL(content?: DataMessageModel[]) {
    const param: ExistingEmployee = {
      id: this.proposeCategory === Constants.ProposeCategory.ELEMENT ? this.proposeDetailId : this.proposeId,
      proposeType: this.proposeCategory === Constants.ProposeCategory.ELEMENT ? 2 : 0,
      isShow: !!content,
      isShowForConcurrent: false,
    };
    if (!content) {
      delete param.isShow;
    }
    this.modalRef = this.modal.confirm({
      nzTitle: content ? '' : this.translate.instant('development.proposed-unit.messages.sendProposeDV'),
      nzContent: !!content ? this.modalContent : '',
      nzOkText: this.translate.instant(Constants.KeyCommons.CONTINUE_TEXT),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL2_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        this.service.processingPTNL(param).subscribe(
          (res) => {
            if (res?.data) {
              if (!res?.data.titleEmployee) {
                this.toastrCustom.success(this.translate.instant('development.proposed-unit.messages.sendProposeSuccess2'));
                this.back();
              } else {
                this.messageListError = res?.data.titleEmployee;
                this.processingPTNL(this.messageListError);
              }
            } else {
              this.toastrCustom.success(this.translate.instant('development.proposed-unit.messages.sendProposeSuccess'));
              this.back();
            }
            this.isLoading = false;
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
      },
    });
  }

  //Nút xử lý bên trong chi tiết
  handle() {
    this.isLoading = true;
    const param = {
      id: this.proposeCategory === Constants.ProposeCategory.ELEMENT ? this.proposeDetailId : this.proposeId,
      proposeType: this.checkGroupType(),
    };
    this.service.proposalProcessing(param).subscribe(
      (res) => {
        this.isLoading = false;
        this.router.navigate([Utils.getUrlByFunctionCode(this.functionCode), 'handle'], {
          queryParams: {
            id: this.proposeId,
            proposeDetailId: this.proposeDetailId,
            proposeCategory: this.proposeCategory,
            flowStatusCode: res?.data || '',
            relationType: this.proposeUnitForm.get('relationType')?.value,
          },
          skipLocationChange: true,
        });

      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

  override ngOnDestroy() {
    this.modalRef?.destroy();
    this.subs.forEach((s) => s.unsubscribe());
  }

  scrollTo(tab: any) {
    const el = document.getElementById(tab.scrollTo);
    el?.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
  }

  override back() {
    if (this.stateUrl?.state?.backUrl) {
      this.router.navigateByUrl(Utils.getUrlByFunctionCode(this.functionCode)).then(() => {
        this.router.navigateByUrl(this.stateUrl.state.backUrl, {
          skipLocationChange: true,
          state: {
            action: 'add',
          },
        });
      });
    } else {
      this.router.navigateByUrl(Utils.getUrlByFunctionCode(this.functionCode));
    }
  }

  //lưu tờ trình TCNS
  savePropose() {
    const paramStatement = this.proposeMultiple?.emitDataStatement();
    if (paramStatement) {
      this.isLoading = true;
      paramStatement.typeStatement = Constants.TypeStatement.TCNS;
      paramStatement.optionNote = this.proposeUnitForm.controls['optionNote'].value;
      this.service.saveStatementDecision(paramStatement).subscribe(
        (res) => {
          this.toastrCustom.success(this.translate.instant('development.interview-config.notificationMessage.success'));
          this.isLoading = false;
          this.ngOnInit();
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        }
      );
    } else {
      return;
    }
  }

  checkGroupType() {
    if (this.proposeCategory === Constants.ProposeCategory.GROUP) {
      return 0;
    } else if (this.proposeCategory === Constants.ProposeCategory.ORIGINAL) {
      return 1;
    } else if (this.proposeCategory === Constants.ProposeCategory.ELEMENT) {
      return 2;
    }
    return null;
  }

  onLoading(event: boolean) {
    this.isLoading = event;
  }

  onChangeUnitDiff(orgIdLevel: number) {
    if (orgIdLevel) {
      const param = {
        id: orgIdLevel,
        pag: 0,
        size: maxInt32,
      };
      this.service.getOrganizationTreeById(param).subscribe((res) => {
        if (res && res?.content?.length > 0) {
          res?.content.forEach((i: OrgEmployee) => {
            if (i.treeLevel > 2) {
              i.orgNameFull = i.pathName.split("-").splice(2).join("-") + " - " + i.orgName;
            }
          });
          this.listInUnit = res?.content.map((item: OrgEmployee) => {
            return {
              name: item.orgNameFull,
              value: item.orgId,
              valueItem: {
                orgId: item.orgId,
                orgName: item.orgName,
                pathId: item.pathId,
                pathResult: item.pathName,
                orgLevel: item.treeLevel,
              },
            };
          });
        }
      });
    }
  }

  onChangeProposeType(e: number) {
    this.proposeMultipleService.formGroupProposeMultiple(e).subscribe((res) => {
      this.dataCommon.formGroupMultipleList = res?.data || [];
      this.proposeUnitForm.controls['formGroup'].setValue(e);
      const formGroupObj = this.dataCommon.formGroupMultipleList?.find(
        (item) => item.formCode === this.proposeDTO.proposeDetails[0].formRotationType);
      this.showMinTime = formGroupObj?.showMinTime!;
      this.showMaxTime = formGroupObj?.showMaxTime!;
    })
  }

  //duyệt rút đề xuất thành phần
  agreeWithdrawProposalDetail(ideaType: number) {
    const ids: number[] = [];
    this.proposeDetais.forEach((item) => {
      if (item.withdrawNote === Constants.withdrawNoteCode.Note3 || item.withdrawNote === Constants.withdrawNoteCode.Note4) {
        ids.push(item.id);
      }
    });
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.proposed-unit.messages.agreeLeader2'),
      nzCancelText: this.translate.instant('development.proposed-unit.button.cancelText'),
      nzOkText: this.translate.instant('development.proposed-unit.button.okText'),
      nzClassName: 'ld-confirm',
      nzOnOk: () => {
        this.isLoading = true;
        this.service.approveWithdrawProposeDetail(ids, this.comment, ideaType, this.screenCode).subscribe(
          () => {
            this.toastrCustom.success(this.translate.instant('development.proposed-unit.messages.Success'));
            this.router.navigateByUrl('development/propose/propose-list');
            this.isLoading = false;
            this.modalRef?.destroy();
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
      },
    });
  }

  checkBtnWithdrawPTNL() {
    return (this.proposeCategory === Constants.ProposeCategory.ELEMENT)
      && Constants.ChangeStatusWithdraw.indexOf(this.flowStatusCode) === -1
      && (this.withdrawNote === Constants.withdrawNoteCode.Note3 || this.withdrawNote === Constants.withdrawNoteCode.Note4)
      && (this.screenCode === this.screenCodeConst.PTNL);
  }

  checkWithdrawTypebtn() {
    return (this.checkWithdrawType === 0 || this.checkWithdrawType === 1)
  }

  downloadFile = () => {
    if (this.proposedocId) {
      this.service.downloadFile(this.proposedocId, this.proposedocName).subscribe(
        () => {
          this.isLoading = false;
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        }
      );
    }
  };

  checkReturnProposeButton(){
    return Constants.ReturnProposeStatus.indexOf(this.flowStatusCode) > -1
      && (this.proposeUnitForm.get('proposeUnitRequestName')?.value || this.personalCode);
  }

  showModalReturnPropose() {
    this.modalRef = this.modal.create({
      nzTitle: this.translate.instant('development.proposed-unit.section.returnPropose'),
      nzWidth: '75vw',
      nzContent: ReturnProposeComponent,
      nzComponentParams: {
        proposeDetailId: this.proposeDetailId,
      },
      nzFooter: null,
    });
    this.modalRef.componentInstance.closeEvent.subscribe((data:boolean) => {
      if (data) {
        this.isLoading = true;
        this.modalRef?.componentInstance.returnPropose.subscribe((body: ReturnPropose) => {
          const sub = this.service.returnPropose(body).subscribe((res) => {
            this.toastrCustom.success(this.translate.instant(Constants.KeyCommons.MESSAGES_SUCCESS2));
            this.router.navigateByUrl(Utils.getUrlByFunctionCode(this.functionCode));
            this.isLoading = false;
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          });
          this.subs.push(sub);
          this.modalRef?.destroy();
        })
      } else {
        this.modalRef?.destroy();
      }
    });
  }

  checkButtonIdeaApproveKSPTNL() {
    const list = [Constants.ProposeCategory.ELEMENT, Constants.ProposeCategory.GROUP];
    return (
      this.screenCode === this.screenCodeConst.KSPTNL &&
      this.flowStatusCode === Constants.FlowCodeConstants.CHODUYET7 &&
      list.indexOf(this.proposeCategory) > -1 && this.proposeDTO?.createdBy === this.currentUser?.username
    );
  }

  otherIdeaElementKSPTNL() {
    const note = this.consultationScreenComponent?.noteKSPTNL || '';
    if (!note) {
      this.toastrCustom.error(this.translate.instant('development.commonMessage.approveIdeaRequired'));
      const el = document.getElementById('idea');
      el?.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
    } else {
      this.modalRef = this.modal.confirm({
        nzTitle: this.translate.instant('development.approvement-propose.messages.otherIdeaElementConfirm'),
        nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
        nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
        nzClassName: Constants.ClassName.LD_CONFIRM,
        nzOnOk: () => {
          this.isLoading = true;
          this.service.otherIdeaElementKSPTNL(this.idGroup, this.proposeDetailId, note).subscribe(
              (res) => {
                this.toastrCustom.success(this.translate.instant('development.approvement-propose.messages.otherIdeaSuccess'));
                this.back();
                this.isLoading = false;
              },
              (e) => {
                this.getMessageError(e);
                this.isLoading = false;
              }
            );
        },
      });
    }

  }

  getFlowStatusCodeElement(status: boolean) {
    this.checkFlowStatusElement = status;
  }

  //popup nhân viên hiện hữu khi gửi duyệt
  showModalExistingEmployee() {
    this.modalRef = this.modal.create({
      nzTitle: this.translate.instant('development.existingEmployee.addEmployeeConfirm'),
      nzWidth: '70vw',
      nzContent: ExistingEmployeeComponent,
      nzComponentParams: {
        bodySendApprove: this.bodySendApprove,
        concurrentPositionDTOList: this.concurrentPositionDTOList,
      },
      nzFooter: null,
    });
    this.modalRef?.componentInstance.closeEvent.subscribe((data:boolean) => {
      if (data) {
        this.isLoading = true;
        this.modalRef?.componentInstance?.bodyExistingEmp.subscribe((body: ExistingEmployee) => {
          const sub = this.service.processingPTNL(body).subscribe((res) => {
            this.toastrCustom.success(this.translate.instant('development.existingEmployee.addEmployeeSuccess'));
            if (res?.data) {
              const formGroup = res.data?.formGroup;
              if (+formGroup === Constants.FormGroupList.TGCV || +formGroup === Constants.FormGroupList.BO_NHIEM) {
                const routerUpdate = +formGroup === Constants.FormGroupList.TGCV ? Constants.SubRouterLink.UPDATETGCV : Constants.TypeScreenHandle.UPDATE_APPOINTMENT;
                this.router.navigate([Utils.getUrlByFunctionCode(this.functionCode), routerUpdate], {
                  queryParams: {
                    id: res.data?.proposeId,
                    proposeCategory: Constants.ProposeCategory.ORIGINAL,
                    flowStatusCode:
                    this.screenCode === Constants.ScreenCode.HRDV
                      ? Constants.FlowCodeConstants.CHUAGUI1
                      : Constants.FlowCodeConstants.NHAP,
                  },
                  skipLocationChange: true,
                });
              } else {
                this.router.navigate([Utils.getUrlByFunctionCode(this.functionCode), 'update'], {
                  queryParams: {
                    id: res.data?.proposeId,
                    proposeType: 0,
                    proposeCategory: Constants.ProposeCategory.ORIGINAL,
                    flowStatusCode: this.screenCode === Constants.ScreenCode.HRDV ? Constants.FlowCodeConstants.CHUAGUI1 : Constants.FlowCodeConstants.NHAP,
                    relationType: true,
                  },
                  skipLocationChange: true,
                });
              }
            } else {
              this.back();
            }
            this.isLoading = false;
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          });
          this.subs.push(sub);
          this.modalRef?.destroy();
        })
      } else {
        this.modalRef?.destroy();
      }
    });
  }

  checkHidenHandleScreen() {
    return ((!Constants.ViewButtonHandleRemove.includes(this.flowStatusCode) && this.flowStatusCode !== Constants.FlowCodeConstants.WITHDRAW) || (this.flowStatusCode === Constants.FlowCodeConstants.WITHDRAW && this.hasProcess))
  }

  //hàm gửi duyệt gộp PTNL
  sendApproveGroupPTNL(content?: DataMessageModel[]) {
    this.modalRef = this.modal.confirm({
      nzTitle: content ? '' : this.translate.instant('development.proposed-unit.messages.sendProposeDV'),
      nzContent: content ? this.modalContent : '',
      nzOkText: this.translate.instant(Constants.KeyCommons.CONTINUE_TEXT),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL2_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        if (!this.showWarningSendPropose) {
          this.callApiSendApproveGroupPTNL(false, false);
        } else {
          this.callApiSendApproveGroupPTNL(true, false);
        }
      },
    });
  }

  callApiSendApproveGroupPTNL(show: boolean, success: boolean) {
    const param: ExistingEmployee = {
      id: this.proposeId,
      proposeType: Constants.ProposeTypeID.GROUP,
      isShow: show,
      isShowForConcurrent: false,
      isSuccess: success,
    };
    this.service.processingPTNL(param).subscribe(
      (res) => {
        if (res?.data) {
          if (res?.data.titleEmployee) {
            this.messageListError = res?.data.titleEmployee;
            this.showWarningSendPropose = true;
            this.sendApproveGroupPTNL(this.messageListError);
          } else if (!res?.data.titleEmployee && !res?.data?.concurrentPositionDTOList) {
            this.toastrCustom.success(this.translate.instant('development.proposed-unit.messages.sendProposeSuccess2'));
            this.callApiSendApproveGroupPTNL(true, true);
          } else if (res?.data?.concurrentPositionDTOList) {
            this.isLoading = true;
            this.bodySendApprove = param;
            this.concurrentPositionDTOList = res?.data?.concurrentPositionDTOList || [];
            this.toastrCustom.success(this.translate.instant(Constants.KeyCommons.MESSAGES_SUCCESS2));
            this.modalRef?.destroy();
            if (this.concurrentPositionDTOList && this.concurrentPositionDTOList?.length > 0) {
              this.loadGroupPage();
              this.proposeMultiple?.getInfoEmp();
              this.showModalExistingEmployee();
            } else {
              this.back();
            }
          }
        } else {
          this.toastrCustom.success(this.translate.instant('development.proposed-unit.messages.sendProposeSuccess'));
          this.back();
        }
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

  //load lại page đề xuất gộp
  loadGroupPage() {
    const paramDetail = {
      ids: this.proposeId,
      screenCode: this.screenCode,
      flowStatusCode: Constants.FlowCodeConstants.CHODUYET7,
    };
    const idDelete = this.proposeCategory === Constants.ProposeCategory.ELEMENT ? this.proposeDetailId : this.proposeId;
    const groupProposeType = this.checkTypeKSPTNL();
    forkJoin([
      this.service.checkDeletePTNL(idDelete, groupProposeType),
      this.service.findById(paramDetail)
    ]).subscribe(([dele, data])=>{
      this.isDeletePTNL = dele?.data;
      this.proposeDTO = data?.proposeDTO[0];
      this.flowStatusCode = this.proposeDTO?.flowStatusCodeDisplay || '';
      const flowCodeManyObject = this.dataCommon.flowStatusApproveConfigList?.find(
        (item) => item.key === this.flowStatusCode
      );
      this.proposeUnitForm.patchValue({
        flowStatusName: flowCodeManyObject?.role,
      });
      this.isLoading = false;
    },
    (e) => {
      this.getMessageError(e);
      this.isLoading = false;
    });
  }

  expandLog() {
    this.isExpand = !this.isExpand;
    if (this.isExpand) {
      this.iconStatus = Constants.IconStatus.DOWN;
    } else {
      this.iconStatus = Constants.IconStatus.UP;
    }
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'development.proposed-unit.proposeLogsTable.time',
          field: 'actionDate',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
          tdTemplate: this.dateLog
        },
        {
          title: 'development.proposed-unit.proposeLogsTable.personAction',
          field: 'userName',
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
        },
        {
          title: 'development.proposed-unit.proposeLogsTable.action',
          field: 'action',
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
        },
        {
          title: 'development.proposed-unit.proposeLogsTable.description',
          field: 'comment',
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 200,
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1,
    };
  }

  getLog(pageIndex: number){
    this.pageNumber = pageIndex - 1;
    this.isLoading = true;
    const idLog = (this.proposeCategory === Constants.ProposeCategory.ORIGINAL || this.proposeCategory === Constants.ProposeCategory.GROUP)
      ? this.proposeId : +this.proposeDetailId;
    const log = this.service.getLogsPage(idLog, this.proposeCategory, this.screenCode, this.pageNumber, this.pageSize)
      .subscribe((logs) => {
      this.proposeLogs = logs?.data?.content || [];
      this.tableConfig.total = logs?.data?.totalElements || 0;
      this.tableConfig.pageIndex = pageIndex;
      this.isLoading = false;
    }, (e) => {
      this.getMessageError(e);
      this.isLoading = false;
    })
    this.subs.push(log);
  }

}
