import { Component, Injector, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Constants, FunctionCode, ScreenType, SessionKey, userConfig, Utils } from "@hcm-mfe/learn-development/data-access/common";
import {
  ConditionViolationList,
  IssueLevelList,
  Proposed,
  ProposeList,
  StatusList
} from "@hcm-mfe/learn-development/data-access/models";
import { ProposedUnitService } from "@hcm-mfe/learn-development/data-access/services";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import { cleanData } from "@hcm-mfe/shared/common/utils";
import { Category, MBTableConfig, Pageable, Pagination } from "@hcm-mfe/shared/data-access/models";
import { User } from "@hcm-mfe/system/data-access/models";
import * as FileSaver from 'file-saver';
import * as moment from 'moment';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { forkJoin, Subscription } from 'rxjs';
import { ProposePopupMergeComponent } from "../../components/propose-popup-merge/propose-popup-merge.component";

@Component({
  selector: 'app-proposed-unit-view',
  templateUrl: './proposed-unit.component.html',
  styleUrls: ['./proposed-unit.component.scss'],
})
export class ProposedUnitComponent extends BaseComponent implements OnInit {
  params = {
    formGroup: null,
    proposeType: null,
    proposeCode: null,
    rangeDate: null,
    proposeStartDate: null,
    proposeEndDate: null,
    currentUnit: null,
    currentTitle: null,
    proposeUnit: null,
    proposeTitle: null,
    employeeCode: null,
    employeeName: null,
    issueLevelCode: null,
    approvalFullName: null,
    approve: <string | null>null,
    screenCode: null,
    statusPublish: null,
    proposeItem: true,
    proposeOrigin: true,
    proposeGroup: true,
    fromDate: null,
    toDate: null,
    optionNote: null,
    conditionViolation: null,
    consultationStatus: null,
    interviewStatus: null,
    flag: false,
    size: userConfig.pageSize,
    page: 0,
  };

  prevParams = { ...this.params };

  dataCommon = {
    statusList: <Category[]>[],
    jobDTOS: <Category[]>[],
    typeDTOS: <Category[]>[],
    flowStatusApproveConfigList: <StatusList[]>[],
    flowStatusPublishConfigList: <StatusList[]>[],
    issueLevelList: <IssueLevelList[]>[],
    formGroupList: [],// nhóm hình thức,
    consultationStatus: [],
    interviewStatus: [],
    conditionViolationList: <ConditionViolationList[]>[]
  };

  pageable: Pageable = {
    totalElements: 0,
    currentPage: 0,
    size: userConfig.pageSize,
  };
  proposeUnitScreen = false;
  proposeUnitApprove = false;
  proposeListPage = false;
  titlePage = '';
  isExpand = true;
  iconStatus = Constants.IconStatus.DOWN;
  type?: ScreenType;
  dataTable: Proposed[] = [];
  checked = false;
  indeterminate = false;
  setOfCheckID = new Set<number>();
  // deleteModal: DeletePopupService;
  statusApprove = '';
  screenCode = '';
  modalRef?: NzModalRef;
  subs: Subscription[] = [];
  checkProposeTypeTrue = true;
  checkProposeTypeFalse = false;
  scrollWidth?: string;
  colSpans?: number;
  colWidthName?: string;
  functionCode: string;
  screenCodeConst = Constants.ScreenCode;
  currentUser?: User;
  rolesOfCurrentUser: string[] = [];
  quantityMap: Map<number, any> = new Map<number, any>();
  flowCodeConstants = Constants.FlowCodeConstants;
  proposeCategoryConst = Constants.ProposeCategory;
  violationListConst = Constants.ViolationList;
  tableConfig!: MBTableConfig;
  pagination = new Pagination();
  @ViewChild('groupCBTmpl', {static: true}) groupCheckbox!: TemplateRef<NzSafeAny>;
  @ViewChild('groupCBHeaderTmpl', {static: true}) groupCheckboxHeader!: TemplateRef<NzSafeAny>;
  @ViewChild('salaryStatusTmpl', {static: true}) salaryStatus!: TemplateRef<NzSafeAny>;
  @ViewChild('interviewStatusTmpl', {static: true}) interviewStatus!: TemplateRef<NzSafeAny>;
  @ViewChild('consultationStatusTmpl', {static: true}) consultationStatus!: TemplateRef<NzSafeAny>;
  @ViewChild('statusDisplayTmpl', {static: true}) statusDisplay!: TemplateRef<NzSafeAny>;
  @ViewChild('sentDateTmpl', {static: true}) sentDate!: TemplateRef<NzSafeAny>;
  @ViewChild('statusPublishDisplayTmpl', {static: true}) statusPublishDisplay!: TemplateRef<NzSafeAny>;
  @ViewChild('actionTmpl', {static: true}) action!: TemplateRef<NzSafeAny>;
  @ViewChild('conditionViolationTmpl', {static: true}) conditionViolation!: TemplateRef<NzSafeAny>;
  heightTable = { x: '100%', y: '25em'};

  constructor(injector: Injector, private readonly service: ProposedUnitService) {
    super(injector);
    this.functionCode = this.route.snapshot?.data['code'];
    if(localStorage.getItem(SessionKey.CURRENCY_USER)) {
      this.currentUser = JSON.parse(localStorage.getItem(SessionKey.CURRENCY_USER)!);
    }
  }

  checkStatusApprove(item: ProposeList) {
    return item.statusDisplay === 'Chờ duyệt';
  }

  checkStatusHandle(item: ProposeList) {
    return item.statusDisplay === 'Chờ xử lý' || item.statusDisplay === 'Đang xử lý';
  }

  //check hiển thị nút sửa HRDV
  checkUpdateButton(indexFix: number) {
    const item = this.dataTable.find((itemTable: Proposed) => itemTable.indexFix === indexFix);
    return (Constants.EditButtonHRDV.indexOf(item?.flowStatusCode!) > -1
      && item?.proposeCategory === Constants.ProposeCategory.ORIGINAL) ||
      ((item?.flowStatusCode === Constants.FlowCodeConstants.STATUS3A || item?.flowStatusCode === Constants.FlowCodeConstants.STATUS3A1)
        && item.proposeCategory === Constants.ProposeCategory.ELEMENT) ||
        !item?.isEndDateWithdraw;
  }

  displayStatus(statusDisplay: string) {
    if (statusDisplay.includes('Chờ') || statusDisplay.includes('Đang')) {
      return 'yellow';
    } else if (statusDisplay.includes('Đã') || statusDisplay.includes('đã')) {
      if (statusDisplay.includes('hủy')) {
        return 'red';
      } else {
        return 'green';
      }
    } else if (statusDisplay.includes('Hủy') || statusDisplay.includes('Từ chối')) {
      return 'red';
    } else if (statusDisplay.includes('Đồng ý') || statusDisplay.includes('đồng ý')) {
      if (statusDisplay.includes('Không') || statusDisplay.includes('không')) {
        return 'red';
      } else {
        return 'green';
      }
    } else if (statusDisplay.includes('Thiếu')) {
      return 'yellow';
    } else {
      return 'gray';
    }
  }

  updateCheckedSet(item: Proposed, checked: boolean): void {
    if (checked) {
      this.quantityMap.set(item.indexFix!, item);
    } else {
      this.quantityMap.delete(item.indexFix!);
    }
  }

  onItemChecked(indexFix: number, checked: boolean): void {
    const item = this.dataTable.find((itemTable: Proposed) => itemTable.indexFix === indexFix);
    this.updateCheckedSet(item!, checked);
    this.refreshCheckedStatus();
  }

  showCheckbox2(item: Proposed) {
    return Constants.ShowCheckBox.includes(item?.flowStatusCode ? item?.flowStatusCode : '')
        && ((item?.proposeCategory === Constants.ProposeCategory.ELEMENT)
        || (item?.proposeCategory === Constants.ProposeCategory.GROUP
          && item?.createdBy === this.currentUser?.username));
  }

  //list các bản ghi có hiển thị checkbox
  listCheckbox() {
    return this.dataTable.filter((item) => this.showCheckbox2(item));
  }

  onAllChecked(value: boolean): void {
    const list = this.listCheckbox();
    list.forEach((item) => this.updateCheckedSet(item, value));
    this.refreshCheckedStatus();
  }

  refreshCheckedStatus(): void {
    const list = this.listCheckbox();
    this.checked = list.every(({ indexFix }) => this.quantityMap.has(indexFix ? indexFix : 0));
    this.indeterminate = list.some(({ indexFix }) => this.quantityMap.has(indexFix ? indexFix : 0)) && !this.checked;
  }

  checkUrl() {
    const pageName = this.route.snapshot?.data['pageName'];
    if (pageName === 'development.pageName.proposed-unit') {
      this.screenCode = this.screenCodeConst.HRDV;
      this.proposeUnitScreen = true;
      this.prevParams.proposeGroup = false;
    } else {
      this.proposeUnitScreen = false;
    }

    if (pageName === 'development.pageName.proposed-unit-approve') {
      this.screenCode = this.screenCodeConst.LDDV;
      this.proposeUnitApprove = true;
    } else {
      this.proposeUnitApprove = false;
    }
    if (pageName === 'development.pageName.proposedLD') {
      this.screenCode = this.screenCodeConst.PTNL;
      this.proposeListPage = true;
    } else {
      this.proposeListPage = false;
    }
  }



  search(firstPage: number) {
    this.isLoading = true;
    this.params.page = firstPage - 1;
    const newParams = { ...cleanData(this.params) };
    newParams.proposeStartDate =
      this.params.rangeDate && this.params.rangeDate[0] ? moment(newParams.rangeDate[0]).format(Constants.FORMAT_DATE.YYYY_MM_DD) : null;
    newParams.proposeEndDate =
      this.params.rangeDate && this.params.rangeDate[1] ? moment(this.params.rangeDate[1]).format(Constants.FORMAT_DATE.YYYY_MM_DD) : null;
    newParams.fromDate =
      this.params.fromDate ? moment(this.params.fromDate).format(Constants.FORMAT_DATE.YYYY_MM_DD) : null;
    newParams.toDate =
      this.params.toDate ? moment(this.params.toDate).format(Constants.FORMAT_DATE.YYYY_MM_DD) : null;
    const currentUnitID = newParams?.currentUnit?.orgId || '';
    newParams.currentUnit = currentUnitID;
    const proposeUnitID = newParams?.proposeUnit?.orgId || '';

    if (this.params.statusPublish !== undefined) {
      const publishSingle = this.dataCommon.flowStatusPublishConfigList.find(
        (i) => i.key === this.params.statusPublish
      );
      const role = publishSingle?.role;
      const publishMulti = this.dataCommon.flowStatusPublishConfigList.filter((i) => i.role === role);
      newParams.statusPublish = publishMulti.map((i) => i.key).toString();
    } else {
      newParams.statusPublish = null;
    }
    newParams.proposeGroup = this.params.proposeGroup;
    newParams.proposeUnit = proposeUnitID;
    newParams.screenCode = this.screenCode;
    const sub = this.service.search(newParams).subscribe(
      (dataProposeUnit) => {
        let indexFix = 1 + this.params.page * this.params.size;
        this.dataTable = dataProposeUnit?.data?.content?.map((item: any) => {
          return {
            ...item,
            indexFix: indexFix++,
          };
        }) || [];
        this.checked = true;
        this.dataTable?.forEach((item) => {
          if (!this.quantityMap.has(item.indexFix!)) {
            this.checked = false;
          } else {
            this.quantityMap.set(item.indexFix!, item);
          }
        });
        if (!this.dataTable?.length) {
          this.checked = false;
        }
        this.tableConfig.total = dataProposeUnit?.data?.totalElements || 0;
        this.tableConfig.pageIndex = firstPage;
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
        this.dataTable = [];
      }
    );
    this.subs.push(sub);
  }

  expandSearch() {
    this.isExpand = !this.isExpand;
    if (this.isExpand) {
      this.iconStatus = Constants.IconStatus.DOWN;
    } else {
      this.iconStatus = Constants.IconStatus.UP;
    }
  }

  checkScreen() {
    if (this.screenCode === this.screenCodeConst.PTNL) {
      this.scrollWidth = '165vw';
      this.colSpans = 2;
      // class name
      this.colWidthName = 'subColumnPTNL';
    } else {
      this.scrollWidth = '135vw';
      this.colSpans = 3;
      this.colWidthName = 'subColumnOthers';
    }
  }

  ngOnInit() {
    this.isLoading = true;
    this.checkUrl();
    this.initTable();
    this.titlePage = this.route.snapshot?.data['pageName'];
    if (this.screenCode === this.screenCodeConst.PTNL) {
      this.params.proposeGroup = true;
    } else {
      this.params.proposeGroup = false;
    }
    const srcCodeParam = {
      screenCode: this.screenCode,
    };
    forkJoin([
      this.service.getRolesByUsername(),
      this.service.getState(srcCodeParam)
    ]).subscribe(([roles, data])=>{
      if (data) {
        this.dataCommon = data;
        this.dataCommon.flowStatusApproveConfigList = this.convertStatus(data?.flowStatusApproveConfigList);
        if (this.functionCode === FunctionCode.DEVELOPMENT_APPROVE_PROPOSE) {
          const listPendingApprove = this.dataCommon.flowStatusApproveConfigList.find(
            (item) => item.role === Constants.FlowCodeName.CHODUYET);
          this.params.approve = listPendingApprove?.key!;
        }
        this.dataCommon.flowStatusPublishConfigList = this.convertStatus(data?.flowStatusPublishConfigList);
        this.dataCommon.conditionViolationList = this.convertStatus(this.violationListConst);
        this.isLoading = false;
      }
      this.rolesOfCurrentUser = roles;
      this.search(1);
    },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      })
  }

  convertStatus(valueStatus: any) {
    let arrayStatus: any = [];
    if (valueStatus) {
      const textStatusConsultation = [
        ...new Set(
          valueStatus.map((item: any) => {
            return item.role;
          })
        ),
      ];
      textStatusConsultation.forEach((item) => {
        let filterValueStatus = valueStatus
          .filter((i: any) => i.role === item)
          ?.map((a: any) => {
            return a.key;
          });
        arrayStatus.push({ key: filterValueStatus.toString(), role: item });
      });
    }
    return arrayStatus;
  }

  checkProposeCode(indexFix: number, isCheckTypeScript: boolean) {
    const item = this.dataTable.find((itemTable: Proposed) => itemTable.indexFix === indexFix);
    switch (item?.proposeCategory) {
      case Constants.ProposeCategory.ORIGINAL:
        return item.proposeId;
      case Constants.ProposeCategory.ELEMENT:
        return isCheckTypeScript ? item.proposeDetailId : item.proposeCode;
      case Constants.ProposeCategory.GROUP:
        return item.proposeId;
      default:
        return null;
    }
  }

  create() {
    this.router.navigate([this.router.url, 'add']);
  }

  update(indexFix: number) {
    const item = this.dataTable.find((itemTable: Proposed) => itemTable.indexFix === indexFix);
    const queryParams = {
      id: item?.proposeId,
      proposeCategory: item?.proposeCategory,
      proposeDetailId: item?.proposeDetailId,
      flowStatusCode: item?.flowStatusCode,
      relationType: item?.relationType && +item?.relationType === Constants.RelationType.ONE,
    }
    if (item?.formGroup) {
      if (+item?.formGroup === Constants.FormGroupList.TGCV && item.proposeCategory !== Constants.ProposeCategory.GROUP) {
        this.router.navigate([this.router.url, 'update-tgcv'], {
          queryParams: queryParams,
          skipLocationChange: true,
        });
      } else if (+item?.formGroup === Constants.FormGroupList.BO_NHIEM) {
        this.router.navigate([this.router.url, 'update-appointment-renewal'], {
          queryParams: queryParams,
          skipLocationChange: true,
        });
      } else {
        this.router.navigate([this.router.url, 'update'], {
          queryParams: queryParams,
          skipLocationChange: true,
        });
      }
    } else {
      this.router.navigate([this.router.url, 'update'], {
        queryParams: queryParams,
        skipLocationChange: true,
      });
    }
  }

  detail(item: Proposed) {
    const queryParams = {
      id: item.proposeId,
      proposeDetailId: item.proposeDetailId,
      proposeCategory: item.proposeCategory,
      flowStatusCode: item.flowStatusCode,
      withdrawNoteCode: item.withdrawNoteCode,
      relationType: item?.relationType && +item?.relationType === Constants.RelationType.ONE,
      screenCode: this.screenCode,
    };
    if (item.formGroup) {

      if (
        +item.formGroup === Constants.FormGroupList.TGCV &&
        !item?.personalCode &&
        item.proposeCategory !== Constants.ProposeCategory.GROUP
      ) {
        this.router.navigate([this.router.url, 'detail-tgcv'], {
          queryParams: queryParams,
          skipLocationChange: true,
        });
      } else if (
        (+item.formGroup === Constants.FormGroupList.TGCV ||
          +item.formGroup === Constants.FormGroupList.DCDD) &&
        item?.personalCode &&
        (((item?.flowStatusCode === Constants.FlowCodeConstants.HANDLING5B ||
          item?.flowStatusCode === Constants.FlowCodeConstants.NHAP) &&
          this.screenCode === this.screenCodeConst.PTNL) ||
          this.screenCode === this.screenCodeConst.HRDV ||
          this.screenCode === this.screenCodeConst.LDDV)
      ) {
        this.router.navigate(
          [Constants.KeyCommons.DEVELOPMENT_RECOMMEND_INDIVIDUAL_DETAIL],
          {
            queryParams: {
              idCreate: item.proposeId,
              screenCode: this.screenCode,
              proposeDetailId: item?.proposeDetailId,
              proposeCategory: item?.proposeCategory,
            },
            state: { backUrl: this.router.url },
            skipLocationChange: true,
          }
        );
      } else if (+item?.formGroup === Constants.FormGroupList.BO_NHIEM) {
        // const routerDetail = item?.proposeCategory === Constants.ProposeCategory.GROUP ? Constants.TypeScreenHandle.DETAIL : Constants.TypeScreenHandle.DETAIL_APPOINTMENT
        this.router.navigate(
          [this.router.url, Constants.TypeScreenHandle.DETAIL_APPOINTMENT],
          {
            queryParams: queryParams,
            skipLocationChange: true,
          }
        );
      } else {
        this.router.navigate([this.router.url, 'detail'], {
          queryParams: queryParams,
          skipLocationChange: true,
        });
      }
    } else {
      this.router.navigate([this.router.url, 'detail'], {
        queryParams: queryParams,
        skipLocationChange: true,
      });
    }
  }

  // duyệt đề xuất LDDV
  approve(indexFix: number) {
    const item = this.dataTable.find((itemTable: Proposed) => itemTable.indexFix === indexFix);
    this.router.navigate([this.router.url, 'approve'], {
      queryParams: {
        id: item?.proposeId,
        proposeDetailId: item?.proposeDetailId,
        proposeCategory: item?.proposeCategory,
        flowStatusCode: item?.flowStatusCode,
      },
      skipLocationChange: true,
    });
  }

  handle(indexFix: number) {
    const item = this.dataTable.find((itemTable: Proposed) => itemTable.indexFix === indexFix);
      const param = {
        id: this.checkProposeCode(indexFix, true),
        proposeType: Utils.getGroupType(item?.proposeCategory!),
      };
      this.isLoading = true;
      if (item) {
        const sub = this.service.proposalProcessing(param).subscribe(
          (res) => {
            this.isLoading = false;
            let routerHandle = Constants.TypeScreenHandle.HANDEL;
            if (+item.formGroup  === Constants.FormGroupList.BO_NHIEM) {
              routerHandle = item?.proposeCategory === Constants.ProposeCategory.GROUP ? Constants.TypeScreenHandle.HANDEL : Constants.TypeScreenHandle.HANDEL_APPOINTMENT;
            }
            if (+item?.formGroup === Constants.FormGroupList.TGCV && !item?.personalCode) {
              routerHandle = item?.proposeCategory === Constants.ProposeCategory.GROUP ? Constants.TypeScreenHandle.HANDEL : Constants.TypeScreenHandle.HANDLE_TGCV;
            }
            this.router.navigate([this.router.url, routerHandle], {
              queryParams: {
                screenCode: this.screenCode,
                id: item?.proposeId,
                proposeDetailId: item?.proposeDetailId,
                proposeCategory: item?.proposeCategory,
                flowStatusCode: res?.data ? res?.data : item?.flowStatusCode,
                withdrawNoteCode: item?.withdrawNoteCode,
                relationType: item?.relationType && +item?.relationType === Constants.RelationType.ONE,
              },
              skipLocationChange: true,
            });
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
        this.subs.push(sub);
      }



  }

  delete(id: number, event: MouseEvent) {
    event.stopPropagation();
    if (this.isLoading) {
      return;
    }
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.proposed-unit.messages.delete'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        this.service.deletePropose(id).subscribe(
          () => {
            this.toastrCustom.success(this.translate.instant('development.proposed-unit.messages.deleteSuccess'));
            this.search(1);
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
      },
    });
  }

  deleteProposeLD(indexFix: number, groupProposeType: number, event: MouseEvent) {
    event.stopPropagation();
    if (this.isLoading) {
      return;
    }
    const idDelete: number = this.checkProposeCode(indexFix, true)!;
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.proposed-unit.messages.delete'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        this.service.deleteProposeListLD(idDelete, groupProposeType).subscribe(
          () => {
            this.toastrCustom.success(this.translate.instant('development.proposed-unit.messages.deleteSuccess'));
            this.search(1);
            this.isLoading = false;
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
      },
    });
  }

  exportExcel() {
    if (this.dataTable?.length === 0) {
      this.toastrCustom.warning(
        this.translate.instant(
          'development.proposed-unit.messages.dataTableEmpty'
        )
      );
      return;
    }
    this.isLoading = true;
    const params = { ...cleanData(this.params) };
    delete params.page;
    delete params.size;
    params.proposeStartDate =
      this.params.rangeDate && this.params.rangeDate[0] ? moment(params.rangeDate[0]).format(Constants.FORMAT_DATE.YYYY_MM_DD) : null;
    params.proposeEndDate =
      this.params.rangeDate && this.params.rangeDate[1] ? moment(this.params.rangeDate[1]).format(Constants.FORMAT_DATE.YYYY_MM_DD) : null;
    const currentUnitID = params?.currentUnit?.orgId || '';
    params.currentUnit = currentUnitID;
    const proposeUnitID = params?.proposeUnit?.orgId || '';

    if (this.params.statusPublish) {
      const publishSingle = this.dataCommon.flowStatusPublishConfigList.find(
        (i) => i.key === this.params.statusPublish
      );
      const role = publishSingle?.role;
      const publishMulti = this.dataCommon.flowStatusPublishConfigList.filter((i) => i.role === role);
      params.statusPublish = publishMulti.map((i) => i.key).toString();
    } else {
      params.statusPublish = null;
    }
    params.proposeGroup = this.params.proposeGroup;
    params.proposeUnit = proposeUnitID;
    params.screenCode = this.screenCode;
    this.service.exportExcel(params).subscribe(
      (data) => {
        let urlExcel = 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,';
        urlExcel = urlExcel + data.data;
        if (this.screenCode === this.screenCodeConst.HRDV) {
          FileSaver.saveAs(urlExcel, this.translate.instant('development.proposed-unit.messages.excelHRDV'));
        } else if (this.screenCode === this.screenCodeConst.LDDV) {
          FileSaver.saveAs(urlExcel, this.translate.instant('development.proposed-unit.messages.excelLDDV'));
        } else {
          FileSaver.saveAs(urlExcel, this.translate.instant('development.proposed-unit.messages.excelPTNL'));
        }
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

  override ngOnDestroy() {
    this.modalRef?.destroy();
    this.subs.forEach((s) => s.unsubscribe());
  }

  checkMergePropose(){
    return this.screenCode === this.screenCodeConst.PTNL
      && this.rolesOfCurrentUser?.includes(Constants.ScreenCode.HCMLDEXLKTPTNL);
  }

  mergePropose() {
    if (this.quantityMap.size === 0) {
      this.toastrCustom.error(this.translate.instant('development.commonMessage.chooseMin'));
      return;
    }
    let listPropose: Proposed[] = [];
    this.quantityMap.forEach((item) => {
      if (item.groupProposeTypeId === 1 || item.groupProposeTypeId === 0) {
        delete item.proposeDetailId;
      } else {
        delete item.proposeId;
      }
      listPropose.push(item);
    });
    listPropose = listPropose?.map((item) => {
      return {
        proposeTypeCode: item?.formGroup,
        groupProposeTypeId: item?.groupProposeTypeId,
        flow: item?.flowStatusCode,
        issueLevelCode: item?.issueLevelCode,
        approvalCode: item?.approvalCode,
        proposeId: item?.proposeId,
        proposeDetailId: item?.proposeDetailId,
        createdBy: item?.createdBy,
        isGroup: item?.isGroup,
        ksptnlIsGroup: item?.groupByKSPTNL,
      };
    });
    this.modalRef = this.modal.create({
      nzTitle: this.translate.instant('development.proposed-unit.messages.mergeConfirm'),
      nzContent: ProposePopupMergeComponent,
      nzFooter: null,
    });
    this.modalRef.componentInstance.eventoptionNotes.subscribe((optionNotes: string) => {
      const screeenCode = Constants.ScreenCode.PTNL
      this.service.mergePropose(listPropose, screeenCode, optionNotes).subscribe(
        (res) => {
          this.toastrCustom.success(this.translate.instant('development.proposed-unit.messages.mergeSuccess'));
          this.quantityMap.clear();
          this.search(1);
          this.modalRef?.destroy();
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
          this.modalRef?.destroy();
        }
      );
    });
  }

  checkHandle(indexFix: number) {
    const item = this.dataTable.find((itemTable: Proposed) => itemTable.indexFix === indexFix);
    const listStatus = Constants.ShowCheckBox;
    // Nếu Gốc 1 thành phần Check thêm isGroup
    if (item?.relationType === Constants.RelationTypeString.ONE
      && item.proposeCategory === Constants.ProposeCategory.ORIGINAL) {
      return listStatus.includes(item?.flowStatusCode!) && !item.isGroup;
    }
    return listStatus.includes(item?.flowStatusCode!);
  }

  showCheckBox(indexFix: number) {
    const item = this.dataTable.find((itemTable: Proposed) => itemTable.indexFix === indexFix);
    return Constants.ShowCheckBox.includes(item?.flowStatusCode!)
      && ((item?.proposeCategory === Constants.ProposeCategory.ELEMENT)
      || (item?.proposeCategory === Constants.ProposeCategory.GROUP
        && item?.createdBy === this.currentUser?.username));
  }

  //check nút sửa PTNL
  checkUpdate(status: string, indexFix: number) {
    const item = this.dataTable.find((itemTable: Proposed) => itemTable.indexFix === indexFix);
    return status === Constants.FlowCodeConstants.NHAP && item?.createdBy === this.currentUser?.username;
  }

  //check nút xóa PTNL
  checkDelete(status: string, groupProposeType: number, indexFix: number) {
    const item = this.dataTable.find((itemTable: Proposed) => itemTable.indexFix === indexFix);
    if ((groupProposeType === Constants.ProposeTypeID.ORIGINAL
      || groupProposeType === Constants.ProposeTypeID.ELEMENT)
      && Constants.CheckDeleteBtn.indexOf(status) > -1
      && item?.createdBy === this.currentUser?.username) {
      return true;
    } else if (groupProposeType === Constants.ProposeTypeID.GROUP && Constants.CheckDeleteBtnGroup.indexOf(status) > -1 && item?.createdBy === this.currentUser?.username) {
      return true;
    } else {
      return item?.createdBy === this.currentUser?.username && Constants.checkdeletebtnGroupKTPTNL === status &&
        groupProposeType === Constants.ProposeTypeID.GROUP && this.rolesOfCurrentUser?.findIndex((role) => role.includes(Constants.ScreenCode.HCMLDEXLPDPTNL)) !== -1
    }
  }

  disabledStartDate = (startValue: Date): boolean => {
    if (!startValue || !this.params.toDate) {
      return false;
    }
    return startValue.getTime() > (this.params.toDate as Date).getTime();
  };

  disabledEndDate = (endValue: Date): boolean => {
    if (!endValue || !this.params.fromDate) {
      return false;
    }
    return endValue.getTime() <= (this.params.fromDate as Date).getTime();
  };

  checkApproveButtonLDDV(indexFix: Proposed) {
    const item = this.dataTable.find((itemTable: Proposed) => itemTable.indexFix === indexFix);
    return this.screenCode === this.screenCodeConst.LDDV &&
      (item?.proposeCategory === Constants.ProposeCategory.ORIGINAL
        && item.flowStatusCode === Constants.FlowCodeConstants.CHODUYET2) ||
      (item?.proposeCategory === Constants.ProposeCategory.ELEMENT
        && item.flowStatusCode === Constants.FlowCodeConstants.SENDAPPROVE2A)
  }

  initTable(){
    this.tableConfig = {
      headers: [
        {
          title: '',
          field: 'select',
          width: 40,
          thClassList: [Constants.TABLE_CONFIG.TEXT_NOWRAP, Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [Constants.TABLE_CONFIG.TEXT_NOWRAP, Constants.TABLE_CONFIG.TEXT_CENTER],
          fixed: true,
          thTemplate: this.groupCheckboxHeader,
          tdTemplate: this.groupCheckbox,
          fixedDir: 'left',
          show: this.screenCode === this.screenCodeConst.PTNL
        },
        {
          title: 'development.proposed-unit.table.handleCode',
          field: 'handleCode',
          width: 90,
          thClassList: [Constants.TABLE_CONFIG.TEXT_NOWRAP, Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [Constants.TABLE_CONFIG.TEXT_NOWRAP, Constants.TABLE_CONFIG.TEXT_CENTER],
          fixed: true,
          fixedDir: 'left',
          show: this.screenCode === this.screenCodeConst.PTNL
        },
        {
          title: 'development.promulgate.search.proposeCode',
          field: 'proposeCode',
          thClassList: [Constants.TABLE_CONFIG.TEXT_NOWRAP, Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [Constants.TABLE_CONFIG.TEXT_NOWRAP, Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 100,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.proposed-unit.table.contentPlan',
          field: 'optionNote',
          width: 170,
          fixed: true,
          fixedDir: 'left',
          show: this.screenCode === this.screenCodeConst.PTNL
        },
        {
          title: 'development.proposed-unit.table.groupForm',
          field: 'formGroupName',
          width: 110,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.proposed-unit.table.form',
          field: 'formRotationName',
          width: 120,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.proposed-unit.table.empCode',
          field: 'employeeCode',
          width: 110,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.proposed-unit.table.empName',
          field: 'employeeName',
          width: 150,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.proposed-unit.table.currentUnit',
          field: 'employeeUnitPathName',
          width: 200,
        },
        {
          title: 'development.proposed-unit.table.currentTitle',
          field: 'employeeTitleName',
          width: 150,
        },
        {
          title: 'development.proposed-unit.table.currentLevel',
          field: 'currentLevel',
          width: 80,
          show: this.screenCode !== this.screenCodeConst.PTNL
        },
        {
          title: 'development.proposed-unit.table.proposeUnit',
          field: 'contentUnitPathName',
          width: 200,
        },
        {
          title: 'development.proposed-unit.table.proposeTitle',
          field: 'contentTitleName',
          width: 150,
        },

        {
          title: 'development.proposed-unit.table.resultCheckCondition',
          field: 'conditionViolation',
          width: 130,
          show: this.screenCode === this.screenCodeConst.PTNL,
          tdTemplate: this.conditionViolation
        },
        {
          title: 'development.proposed-unit.table.interviewCondition',
          field: 'interviewStatus',
          tdTemplate: this.interviewStatus,
          width: 120,
          show: this.screenCode === this.screenCodeConst.PTNL
        },
        {
          title: 'development.proposed-unit.table.consultationCondition',
          field: 'consultationStatus',
          tdTemplate: this.consultationStatus,
          width: 115,
          show: this.screenCode === this.screenCodeConst.PTNL
        },
        {
          title: 'development.proposed-unit.table.proposeLevel',
          field: 'proposeLevel',
          width: 80,
          show: this.screenCode !== this.screenCodeConst.PTNL
        },
        {
          title: 'development.proposed-unit.table.statusApprove',
          field: 'statusDisplay',
          tdTemplate: this.statusDisplay,
          width: 180,
        },
        {
          title: 'development.proposed-unit.table.noteWithdraw',
          field: 'withdrawNote',
          width: 200,
        },
        {
          title: 'development.proposed-unit.table.proposeTypePerson',
          field: 'createdBy',
          width: 100,
          show: this.screenCode === this.screenCodeConst.PTNL
        },
        {
          title: 'development.proposed-unit.table.sentDate',
          field: 'sentDate',
          tdTemplate: this.sentDate,
          width: 110,
        },
        {
          title: 'development.promulgate.search.issueLevelCode',
          field: 'issueLevelName',
          width: 100,
          show: this.screenCode === this.screenCodeConst.PTNL
        },
        {
          title: 'development.approvement-propose.search.approvePerson',
          field: 'approvalFullName',
          width: 170,
          show: this.screenCode === this.screenCodeConst.PTNL
        },
        {
          title: 'development.approvement-propose.search.handlePerson',
          field: 'lastUpdatedBy',
          width: 100,
        },
        {
          title: 'development.proposed-unit.table.statusPublish',
          field: 'statusPublishDisplay',
          tdTemplate: this.statusPublishDisplay,
          width: 110,
        },
        {
          title: ' ',
          field: 'action',
          width: 70,
          tdTemplate: this.action,
          fixed: window.innerWidth > 1024,
          fixedDir: 'right',
        },
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: userConfig.pageSize,
      pageIndex: 1,
    };
  }

  //check button xóa HRDV
  checkDeleteButtonHRDV(indexFix: number) {
    const item = this.dataTable.find((itemTable: Proposed) => itemTable.indexFix === indexFix);
    return item?.proposeCategory === Constants.ProposeCategory.ORIGINAL
      && item.flowStatusCode === Constants.FlowCodeConstants.CHUAGUI1;
  }

  // check hiển thị nút 3 chấm màn danh sách của đề xuất 1 cá nhân
  checkButtonMoreHRDV(indexFix: number) {
    const item = this.dataTable.find((itemTable: Proposed) => itemTable.indexFix === indexFix);
    return !item?.personalCode;
  }

  //Kiểm tra điều kiện LCBN
  checkConditionViolationCode (conditionViolationCode: string) {
    switch(+conditionViolationCode) {
      case Constants.ViolationCode.REQUIRED:
        return Constants.ViolationName.FAILED;
      case Constants.ViolationCode.VIOLATE:
        return Constants.ViolationName.FAILED;
      case Constants.ViolationCode.PASSED:
        return Constants.ViolationName.PASSED;
      default:
        return '';
    }
  }
}
