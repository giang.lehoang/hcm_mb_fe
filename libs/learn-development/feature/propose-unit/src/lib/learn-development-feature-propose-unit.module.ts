import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {PopupService} from "@hcm-mfe/shared/ui/popup";
import {pages} from "./pages";
import {components} from "./components";
import {ProposedUnitRoutingModule} from "./proposed-unit-routing.module";
import {LearnDevelopmentFeatureShellModule} from "@hcm-mfe/learn-development/feature/shell";
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';

@NgModule({
  declarations: [...pages, ...components],
  exports: [...pages, ...components],
    imports: [
        LearnDevelopmentFeatureShellModule,
        ProposedUnitRoutingModule,
        NzDropDownModule,
    ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
  providers: [PopupService]
})
export class LearnDevelopmentFeatureProposeUnitModule {}
