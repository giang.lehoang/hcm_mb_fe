import {
  Component,
  EventEmitter,
  Injector,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Constants } from '@hcm-mfe/learn-development/data-access/common';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { ProposedUnitService } from '@hcm-mfe/learn-development/data-access/services';
import { CancelPlan } from '@hcm-mfe/learn-development/data-access/models';

@Component({
  selector: 'app-cancel-plan',
  templateUrl: './cancel-plan.component.html',
  styleUrls: ['./cancel-plan.component.scss'],
})
export class CancelPlanComponent extends BaseComponent implements OnInit {
  form: FormGroup;
  isSubmitted = false;
  @Input() proposeCode: string = '';
  @Input() fullName: string = '';
  @Input() id: number = 0;
  @Input() proposeCategory: string = '';
  @Output() closeEvent = new EventEmitter<boolean>();
  @Output() cancelPlanBody = new EventEmitter<CancelPlan>();
  labelMessage = '';

  constructor(injector: Injector, private service: ProposedUnitService) {
    super(injector);
    this.form = new FormGroup({
      reason: new FormControl(null, Validators.required),
    });
  }

  ngOnInit() {
    this.labelMessage = this.translate.instant(
      'development.commonMessage.labelPlan',
      {
        proposeCode: this.proposeCode,
        fullName: this.fullName,
      }
    );
  }

  submit() {
    this.isSubmitted = true;
    if (this.form.valid) {
      const body = {
        id: this.id,
        cancellationReason: this.form.get('reason')?.value,
      };
      this.closeEvent.emit(true);
      this.cancelPlanBody.emit(body);
    }
  }

  closeModal() {
    this.closeEvent.emit(false);
  }
}
