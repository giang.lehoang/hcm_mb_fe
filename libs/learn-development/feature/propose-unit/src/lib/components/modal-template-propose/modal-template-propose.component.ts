import {Component, EventEmitter, Output} from '@angular/core';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {NzModalRef} from "ng-zorro-antd/modal";

@Component({
  selector: 'app-modal-template-propose',
  templateUrl: './modal-template-propose.component.html',
  styleUrls: ['./modal-template-propose.component.scss'],
})
export class ModalTemplateProposeComponent extends BaseComponent {
  @Output() eventBoolean = new EventEmitter<boolean>();
  @Output() closeEvent = new EventEmitter<boolean>();
  modalRef?: NzModalRef;
  isSort: boolean | undefined

  closeModal() {
    this.closeEvent.emit(false);
  }

  save() {
    this.eventBoolean.emit(this.isSort)
  }


}
