import { Component, EventEmitter, Injector, Input, OnInit, Output } from '@angular/core';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {ProposedUnitService} from "@hcm-mfe/learn-development/data-access/services";
import {cleanDataForm} from "@hcm-mfe/shared/common/utils";
import {FileCustom} from "@hcm-mfe/learn-development/data-access/models";
import {NzUploadFile} from "ng-zorro-antd/upload";
import {Constants} from "@hcm-mfe/learn-development/data-access/common";

@Component({
  selector: 'app-resource-management',
  templateUrl: './resource-management.component.html',
  styleUrls: ['./resource-management.component.scss'],
})
export class ResourceManagementComponent extends BaseComponent implements OnInit {
  type = this.screenType;
  @Output() submitResource: EventEmitter<any> = new EventEmitter<any>();
  @Input() proposeDetailId?: string;
  @Input() typeAction?: string;

  resourceForm = this.fb.group({
    proposeDetailId: '',
    currentUnitComment: '',
    hrComment: '',
    statementNumber: '',
    statementDate: '',

  });

  fileListSelect: FileCustom[] = [];

  constructor(injector: Injector, private serviceProposed: ProposedUnitService) {
    super(injector);
  }

  ngOnInit(): void {
  }

  emitData(){
    const newForm = { ...cleanDataForm(this.resourceForm) };
    return newForm;
  }

  beforeUpload = (file: File | NzUploadFile): boolean => {
    if ((file as File).size >= 3000000) {
      return false;
    }
    const formData = new FormData();
    formData.append('files', file as File);
    formData.append('proposeDetailId', new Blob([JSON.stringify(this.proposeDetailId)], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('type', new Blob([JSON.stringify('statement')], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    this.serviceProposed.uploadFile(formData).subscribe((res) => {
      this.fileListSelect.push({code: res.data?.id, file:file as File})
    }, (e) => {
      this.getMessageError(e);
      this.isLoading = false;
    })
    return true;
  };

  doRemoveFile = (file: NzUploadFile): boolean => {
    const idDelete: number = <number>this.fileListSelect.find((item: any) => item.file.uid === file.uid)?.code!;
    this.serviceProposed.deleteUploadFile(idDelete!).subscribe((res) => {
    }, (e) => {
      this.getMessageError(e);
      this.isLoading = false;
    })
    return true;
  };

  beforeUpload1 = (file: File | NzUploadFile): boolean => {
    if ((file as File).size >= 3000000) {
      return false;
    }
    const formData = new FormData();
    formData.append('files', file as File);
    formData.append('proposeDetailId', new Blob([JSON.stringify(this.proposeDetailId)], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('type', new Blob([JSON.stringify('decision')], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    this.serviceProposed.uploadFile(formData).subscribe((res) => {
      this.fileListSelect.push({code: res.data?.id, file:file as File})
    }, (e) => {
      this.getMessageError(e);
      this.isLoading = false;
    })
    return true;
  };


  summarize(){
    this.isLoading = true;
    this.serviceProposed.summarizeContent(+this.proposeDetailId!).subscribe((res)=>{
      this.resourceForm.get('hrComment')?.setValue(res.data);
      this.isLoading = false;
    },
    (e)=>{
      this.getMessageError(e);
      this.isLoading = false;
    }
    )
  }
}
