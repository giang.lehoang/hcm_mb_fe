import {
  Component,
  EventEmitter,
  Injector, OnInit,
  Output
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';

@Component({
  selector: 'app-stop-interview',
  templateUrl: './stop-interview.component.html',
  styleUrls: ['./stop-interview.component.scss'],
})
export class StopInterviewComponent extends BaseComponent implements OnInit {
  form: FormGroup;
  @Output() closeEvent = new EventEmitter<boolean>();
  @Output() sendReason = new EventEmitter<string>();
  
  constructor(injector: Injector) {
    super(injector);
    this.form = new FormGroup({
      reason: new FormControl(null, [Validators.required]),
    });
  }
  ngOnInit() {}

  submit(){
    this.closeEvent.emit(true);
    this.sendReason.emit(this.form.get('reason')?.value);
  }

  closeModal(){
    this.closeEvent.emit(false);
  }
}
