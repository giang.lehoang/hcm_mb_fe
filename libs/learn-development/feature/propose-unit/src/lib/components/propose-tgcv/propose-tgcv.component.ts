import {
  Component,
  HostBinding,
  Injector,
  Input,
  OnInit,
  ViewChild
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NavigationExtras } from '@angular/router';
import {
  Constants,
  maxInt32,
  Scopes,
  ScreenType,
  SessionKey,
  userConfig,
  Utils
} from '@hcm-mfe/learn-development/data-access/common';
import {
  AttachFile,
  DataCommon,
  DataFormTGCV,
  FileDTOList,
  JobDTOS,
  ListFileTemplate,
  ListOrgItem,
  ListTitle, OrgEmployee, OrgItem,
  ParamSearchPosition,
  Proposed, ProposedError,
  ProposeDTOTGCV,
  ProposeLogs,
  ProposePerformanceResults,
  ProposeStatementDTO,
  ProposeTypeModel,
  ReturnPropose,
  RotationReasons,
  ScreenCode,
  StatusList, User,
  WithdrawPropose
} from '@hcm-mfe/learn-development/data-access/models';
import {
  ProposedUnitService,
  ProposeMultipleEmployeeService,
  ProposeTGCVService
} from '@hcm-mfe/learn-development/data-access/services';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { cleanDataForm } from '@hcm-mfe/shared/common/utils';
import { ScrollTabOption } from '@hcm-mfe/shared/data-access/models';
import * as FileSaver from 'file-saver';
import { MBTableConfig } from 'libs/shared/data-access/models/src/lib/mb-table.interfaces';
import * as moment from 'moment';
import { InputBoolean } from 'ng-zorro-antd/core/util';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { catchError, forkJoin, Observable, of, Subscription } from 'rxjs';
import { ConsultationScreenComponent } from '../consultation-screen/consultation-screen.component';
import { ProposeMultipleEmployeeComponent } from '../propose-multiple-employee/propose-multiple-employee.component';
import { ReturnProposeComponent } from '../return-propose/return-propose.component';
import { WithdrawProposeComponent } from '../withdraw-propose/withdraw-propose.component';

@Component({
  selector: 'app-propose-tgcv',
  templateUrl: './propose-tgcv.component.html',
  styleUrls: ['./propose-tgcv.component.scss'],
})
export class ProposeTGCVComponent extends BaseComponent implements OnInit {
  dataCommon: DataCommon = {
    jobDTOS: <JobDTOS[]>[], // vị trí
    typeDTOS: [], // loại đề xuất
    formDisplaySearchDTOS: [], // hình thức
    proposeRotationReasonConfigDTOS: [], // lý do luân chuyển
    statusList: [],
    flowStatusApproveConfigList: [],
    flowStatusPublishConfigList: [],
    formGroupList: [],
    formGroupMultipleList: [],
    conditionViolationList: [],
    timeTypeEnums: [],
  };
  typeAction = '';
  dataForm?: DataFormTGCV;
  proposeDTO?: ProposeDTOTGCV;
  screenCode: string = '';
  flowStatusCode: string = '';
  flowStatusCodeDisplay?: string;
  issueLevelName?: string = '';
  proposeLogs?: ProposeLogs[] = [];
  @Input() formGroupCreate: number | null = null;
  proposeDetailId: number = 0;
  proposeCategory = '';
  @Input() proposeId: number = 0;
  proposeIdOriginal = 0;
  @Input() proposeUnit?: Proposed;
  stateUrl?: NavigationExtras;
  getRolesUsername: string[] = [];

  rotationReasons: RotationReasons[] = [];
  proposePerformanceResults: ProposePerformanceResults[] = [];
  currentSumProposePerformanceResults = 0;
  sumProposePerformanceResults = 0;
  currentSumPoint = 0;
  sumPoint = 0;
  modalRef?: NzModalRef;
  iconStatus = Constants.IconStatus.DOWN;
  isExpand = true;
  formRotationType = '';
  formTypeConst = Constants.FormGroupList;
  isChangeLevel = false;
  sortOrder = 0;
  subs: Subscription[] = [];
  tableLogs!: MBTableConfig;
  listAttachFile: NzUploadFile[] = [];
  screenTypeConst = ScreenType;

  heightTable = { x: '100%', y: '25em' };

  //trường chuyển từ update sang
  currentUser?: User;
  proposeType: number | null = 0;
  proposeGroupId: number = 0;
  statusPublishCode: string = '';
  violationCode = 0;
  externalApprove = false;
  flowCodeConst = Constants.FlowCodeConstants;
  listProposeCategory = Constants.ProposeCategory;
  withdrawNote = 0;
  withdrawReason = '';
  checkWithdrawType: number | undefined;
  comment = '';
  violationCodeConst = Constants.ViolationCode;
  conditionViolation: string[] = [];
  fileElementObj?: FileDTOList = {};

  rotations: RotationReasons[] = [
    {
      formRotationLabel: Constants.UnitTypeTGCV.SAMEUNIT,
      formRotation: null, // IN/OUT
      formRotationType: null, // DC/DD
      formRotationTitle: null,
      contentUnit: null,
      contentUnitCode: null,
      contentTitleCode: null,
      contentTitleName: null,
      contentUnitPathId: null,
      contentUnitPathName: null,
      contentUnitTreeLevel: null,
      disabled: true,
      level: null,
    },
    {
      formRotationLabel: Constants.UnitTypeTGCV.OTHERUNIT,
      formRotation: null, // IN/OUT
      formRotationType: null, // DC/DD
      formRotationTitle: null,
      contentUnit: null,
      contentUnitCode: null,
      contentTitleCode: null,
      contentTitleName: null,
      contentUnitPathId: null,
      contentUnitPathName: null,
      contentUnitTreeLevel: null,
      disabled: true,
      level: null,
    },
  ];

  // thông tin nhân viên
  employeeForm = this.fb.group({
    empIdp: null,
    orgId: null,
    empId: null,
    empCode: null,
    fullName: null,
    email: null,
    posId: null,
    posName: null,
    jobId: null,
    jobName: null,
    orgPathId: null,
    orgPathName: null,
    orgName: null,
    majorLevelId: null,
    majorLevelName: null,
    faculityId: null,
    faculityName: null,
    schoolId: null,
    schoolName: null,
    positionLevel: null,
    gender: null,
    joinCompanyDate: null,
    dateOfBirth: null,
    partyDate: null,
    partyOfficialDate: null,
    posSeniority: null,
    seniority: null,
    qualificationEnglish: null,
    facultyName: null,
    semesterGradeT1: null,
    semesterRankT1: null,
    semesterGradeT2: null,
    semesterRankT2: null,
    ratedCapacity: null,
    nearestDiscipline: null,
    endOfDisciplinaryDuration: null,
    userToken: null,
    treeLevel: null,
    posSeniorityName: null,
    seniorityName: null,
    levelName: null,
    concurrentTitleId: null,
    concurrentTitleName: null,
    concurrentUnitPathName: null,
    concurrentTitleId2: null,
    concurrentTitleName2: null,
    concurrentUnitPathName2: null,
    periodNameT1: null,
    periodNameT2: null,
  });

  // form proposeDetail
  proposeDetailForm = this.fb.group({
    proposeID: null,
    empCode: null,
    formRotation: null,
    formRotationType: null,
    formRotationCode: null,
    formRotationTitle: null,
    contentUnitCode: null,
    contentTitleCode: null,
    contentTitleName: null,
    contentUnitPathId: null,
    contentUnitPathName: null,
    contentUnitTreeLevel: null,
    contentPositionCode: null,
    rotationFromDate: null,
    rotationToDate: null,
    rotationTime: [{ value: null, disabled: true }],
    employee: null, //formGroup
    proposeAlternative: null, //formGroup
    proposeComment: null, //formGroup

    employeeUnitPathId: null,
    employeeUnitPathName: null,
    conditionViolationCode: null,
    conditionViolations: null,
    //
    implementDate: [null, Validators.required],
    groupByKSPTNL: null,
    isGroup: null,
    sameDecision: null,
    isIncome: null,
    isInsurance: null,
    isDecision: null,
    issuedLater: null,
    minimumTime: [null, Validators.required],
    maximumTime: [null, Validators.required],
    timeType: [null, Validators.required],
  });

  listInUnit?: ListOrgItem[] = [];
  scope: string = Scopes.VIEW;
  functionCode: string = '';
  isSubmitFormTTQD = false;
  isSubmitFormReportElement = false;

  contentTitleEmp?: string;
  differentUnits = '';
  inUnit = '';
  isSeeMore = false;
  listLevel = Constants.Level;
  typeEmpAlternative = Constants.TypeEmpAlternative;
  displayReport = false;
  isSubmitIdeaLDDV = false;
  isAcceptPropose = false;
  issuedLater = false;
  attachFileObj: AttachFile = {};
  pageName = '';
  isScreenHandle = false;
  scrollTabs = Constants.ScrollTabs;
  @HostBinding('class.scroll__tab--sticky')
  @Input() @InputBoolean() sticky = true;
  disableFormRotationRadio = false;
  isSubmitProposeDetail = false;
  idGroup: number;
  getReportFirst?: boolean = false;
  fileElement: AttachFile = {};
  fileListSelectTTElement: NzUploadFile[] = [];
  showMinTime = false;
  showMaxTime = false;
  constantsFormGroupCode = Constants.FormGroupList;

  @ViewChild('consultationScreenComponent') consultationScreenComponent?: ConsultationScreenComponent;

  constructor(
    injector: Injector,
    private readonly service: ProposedUnitService,
    private readonly proposeMultipleService: ProposeMultipleEmployeeService,
    private readonly TGCVService: ProposeTGCVService
  ) {
    super(injector);
    this.functionCode = this.route.snapshot?.data['code'];
    this.typeAction = this.route.snapshot.data['screenType'];
    this.functionCode = this.route.snapshot.data['code'];
    this.proposeIdOriginal = this.route.snapshot.queryParams['id'];
    this.proposeDetailId = this.route.snapshot.queryParams['proposeDetailId'];
    this.proposeCategory = this.route.snapshot.queryParams['proposeCategory'];
    this.flowStatusCode = this.route.snapshot.queryParams['flowStatusCode'];
    this.proposeGroupId = this.route.snapshot.queryParams['proposeGroupId'];
    const relationType = this.route.snapshot.queryParams['relationType'];
    this.proposeUnitForm.get('relationType')?.setValue(relationType);
    this.stateUrl = this.router?.getCurrentNavigation()?.extras;
    this.isScreenHandle = this.route.snapshot?.data['handlePropose'];
    this.idGroup = this.route.snapshot.queryParams['idGroup'];

  }

  proposeCommentForm = new FormGroup({
    proposeReason: new FormControl(null, Validators.required),
    generalComment: new FormControl(null, Validators.required),
  });

  attachDocumentForm = new FormGroup({
    personalReview: new FormControl(false),
    reportBranchMeeting: new FormControl(false),
    consultationComment: new FormControl(false),
    different: new FormControl(false),
    statement: new FormControl(false),
    docId: new FormControl(null),
    fileName: new FormControl(null),
  });

  formTTQD = new FormGroup({
    statementNumber: new FormControl(null, Validators.required),
    submissionDate: new FormControl(null, Validators.required),
    approveNote: new FormControl(null, Validators.required),
  });

  formIdeaLDDV = new FormGroup({
    idea: new FormControl(null, Validators.required),
  });

  formReportElement = new FormGroup({
    statementNumber: new FormControl(null, [Validators.required]),
    submissionDate: new FormControl(null, [Validators.required]),
    approveNote: new FormControl(null, [Validators.required]),
    typeStatement: new FormControl(null),
  });

  screenCodeConst = Constants.ScreenCode;
  jobDTOSApi: JobDTOS[] = [];

  proposeUnitForm = this.fb.group({
    id: null,
    proposeDetails: null, // formArray
    proposeStatement: null, // formGroup
    proposeDecision: null, //formGroup
    proposeLogs: null, //formArray
    proposeGroupingHistories: null, //formArray
    proposeUnitRequest: null,
    proposeUnitRequestName: [{ value: null, disabled: true }],
    originator: null,
    proposeType: null,
    relationType: null,
    externalApprove: null,
    status: null,
    flowStatusCode: null,
    statusPublishName: null,
    groupCode: null,
    sentDate: null,
    issueLevelCode: null,
    signerCode: null,
    approvalCode: null,
    signDate: null,
    approveDate: null,
    flowStatusName: null,
    formGroup: null,
    withdrawNote: null,
    handleCode: null,
    optionNote: null,
    isGroup: null,
    groupByKSPTNL: null,
    withdrawReason: null,
  });

  // log
  pageNumber = 0;
  pageSize =  userConfig.pageSize;

  // check đã từng qua trạng thái 6 hay chưa
  hasProcess = false;

  getReasonsReject() {
    return this.formTTQD.get('approveNote')?.value;
  }

  checkLeaderApprove(status: string) {
    return Constants.LeaderAppproveStatus.indexOf(status) > -1;
  }

  ngOnInit() {
    if (this.typeAction !== ScreenType.Update || this.isScreenHandle) {
      this.proposeDetailForm.disable();
      this.formTTQD.disable();
      this.proposeCommentForm.disable();
      this.attachDocumentForm.disable();
    }

    this.jobDTOSApi = this.dataCommon.jobDTOS ? this.dataCommon.jobDTOS : [];
    this.initLogs();
    if (localStorage.getItem(SessionKey.CURRENCY_USER)) {
      this.currentUser = JSON.parse(
        localStorage.getItem(SessionKey.CURRENCY_USER)!
      );
    }
    this.proposeType = Utils.getGroupType(this.proposeCategory);
    this.checkUrl();
    this.proposeUnitForm.disable();
    this.loadAll();
  }

  loadAll() {
    const paramDetail = {
      ids: this.proposeIdOriginal,
      screenCode: this.screenCode,
      flowStatusCode: this.flowStatusCode,
    };
    const paramSrcCode = {
      screenCode: this.screenCode,
    };
    let api: Observable<any>;
    if (this.proposeCategory === Constants.ProposeCategory.ELEMENT) {
      api = this.service.getDetailElement(
        this.proposeDetailId,
        this.screenCode,
        this.proposeGroupId
      );
    } else {
      api = this.service.findById(paramDetail);
    }
    const idLog = this.proposeCategory === Constants.ProposeCategory.ELEMENT
      ? +this.proposeDetailId
      : +this.proposeIdOriginal;
    if (this.typeAction !== this.screenTypeConst.Create) {
      this.isLoading = true;
      const subs = forkJoin([
        this.service
          .getState(paramSrcCode)
          .pipe(catchError(() => of(undefined))),
        api.pipe(catchError(() => of(undefined))),
        this.service.getRolesByUsername().pipe(catchError(() => of(undefined))),
        this.service.getLogsPage(idLog, this.proposeCategory, this.screenCode, this.pageNumber, this.pageSize)
          .pipe(catchError(() => of(undefined))),
      ]).subscribe(
        ([state, data, roles, logs]) => {
          if (state) {
            //Logs
            this.proposeLogs = logs?.data?.content || [];
            this.proposeLogs = this.proposeLogs?.map((item) => {
              return {
                ...item,
                actionDate: moment(item.actionDate).format(Constants.FORMAT_DATE.DD_MM_YYYY_HH_MM),
              }
            });
            this.tableLogs.total = logs?.data?.totalElements || 0;
            this.tableLogs.pageIndex = 1;
            this.dataCommon = state;
            this.proposeDTO = data?.proposeDTO[0];
            this.dataForm = data?.proposeDTO[0]?.proposeDetails[0];
            this.hasProcess = this.dataForm?.hasProcess ?? false;
            this.checkWithdrawType = data?.proposeDTO[0]?.withdrawType;
            this.withdrawNote = data?.proposeDTO[0]?.withdrawNote;
            this.withdrawReason = data?.proposeDTO[0]?.withdrawReason;
            this.proposeUnitForm
              .get('proposeUnitRequest')
              ?.setValue(this.proposeDTO?.proposeUnitRequest || 0);
            const requestObject = this.dataCommon?.units?.find(
              (i) => i.orgId === this.proposeDTO?.proposeUnitRequest
            );
            this.proposeUnitForm
              .get('proposeUnitRequestName')
              ?.setValue(requestObject?.orgName);
            const proposeStatement = this.proposeDTO?.proposeStatementList || [];
            const reportHRDV = proposeStatement?.find((item) => !item?.typeStatement);
            if (reportHRDV) {
              this.formTTQD.patchValue({
                statementNumber: reportHRDV?.statementNumber,
                submissionDate: reportHRDV?.submissionDate,
                approveNote: this.proposeDTO?.approveNote,
              });
            }
            if (this.proposeDTO) {
              if (this.dataForm) {
                if (this.dataForm?.employee?.empCode) {
                  this.employeeForm.patchValue({
                    empCode: this.dataForm?.employee?.empCode,
                  });
                }
                this.proposeDetailForm.patchValue(this.dataForm);
                if (this.dataForm.employee.posSeniority) {
                  const years = Math.floor(
                    this.dataForm.employee.posSeniority / 12
                  );
                  const months = Math.floor(
                    this.dataForm.employee.posSeniority % 12
                  );
                  if (months === 0) {
                    this.dataForm.employee.posSeniorityName =
                      years < 1 ? `` : `${years} năm`;
                  } else {
                    this.dataForm.employee.posSeniorityName =
                      years < 1
                        ? `${months} tháng`
                        : `${years} năm ${months} tháng`;
                  }
                }

                if (this.dataForm.employee.seniority) {
                  const years = Math.floor(
                    this.dataForm.employee.seniority / 12
                  );
                  const months = Math.floor(
                    this.dataForm.employee.seniority % 12
                  );
                  if (months === 0) {
                    this.dataForm.employee.seniorityName =
                      years < 1 ? `` : `${years} năm`;
                  } else {
                    this.dataForm.employee.seniorityName =
                      years < 1
                        ? `${months} tháng`
                        : `${years} năm ${months} tháng`;
                  }
                }

                this.employeeForm.patchValue(this.dataForm?.employee);
                this.employeeForm.patchValue({
                  levelName: this.dataForm?.employee.positionLevel,
                });
                let disciplinaryDuration =
                  this.employeeForm?.get('endOfDisciplinaryDuration')?.value ||
                  '';
                if (disciplinaryDuration) {
                  const check2 = moment(
                    disciplinaryDuration,
                    Constants.FORMAT_DATE.DD_MM_YYYY
                  ).isValid();
                  if (!check2) {
                    disciplinaryDuration = moment(disciplinaryDuration).format(
                      Constants.FORMAT_DATE.DD_MM_YYYY
                    );
                  }
                  this.employeeForm.patchValue({
                    endOfDisciplinaryDuration: disciplinaryDuration,
                  });
                }
                this.getRolesUsername = roles;
                if (
                  this.getRolesUsername?.includes(Constants.ScreenCode.HRDV) ||
                  this.getRolesUsername?.includes(Constants.ScreenCode.LDDV) ||
                  this.proposeDTO?.proposeUnitRequestName
                ) {
                  this.rotations = this.rotations.filter(
                    (item) =>
                      item.formRotationLabel === Constants.UnitTypeTGCV.SAMEUNIT
                  );
                }
                const levelOrgPathId =
                  this.proposeDTO?.proposeDetails[0]?.employee?.orgPathId?.split(
                    '/'
                  );
                this.onChangeUnitDiff(levelOrgPathId[3]);
                this.rotations.forEach((item, i) => {
                  if (i === this.dataForm?.formRotation) {
                    item.disabled =
                      this.typeAction === ScreenType.Detail ||
                      this.isScreenHandle;
                    item.formRotation = this.dataForm?.formRotation?.toString();
                    item.formRotationTitle =
                      this.dataForm?.formRotationTitle?.toString();
                    item.formRotationType = this.dataForm?.formRotationType;
                    item.contentTitleCode = this.dataForm?.contentTitleCode;
                    item.level = this.dataForm?.contentLevel;
                    if (i === 1) {
                      item.contentUnit = {
                        orgId: +this.dataForm?.contentUnitCode,
                        orgName: this.dataForm?.contentUnitName,
                        pathId: this.dataForm?.contentUnitPathId,
                        pathResult: this.dataForm?.contentUnitPathName,
                        orgLevel: this.dataForm?.contentUnitTreeLevel,
                      };
                    }
                    item.contentUnitCode = +this.dataForm?.contentUnitCode;
                    if (
                      this.dataForm?.contentUnitCode ||
                      item.contentTitleCode
                    ) {
                      this.onChangeUnit(
                        this.dataForm?.contentUnitCode?.toString(),
                        +this.dataForm?.formRotation,
                        this.dataForm?.contentTitleCode
                      );
                    }
                  }
                });
                if (this.screenCode === Constants.ScreenCode.HRDV) {
                  this.rotations[0].formRotation =
                    Constants.FormRotationName.INUNIT.toString();
                }
                if (
                  this.typeAction === this.screenTypeConst.Update &&
                  this.screenCode === Constants.ScreenCode.HRDV
                ) {
                  this.onChangeFormRotation(0);
                }
                this.employeeForm.patchValue({
                  partyDate: !!this.dataForm.employee.partyDate,
                });

                this.proposePerformanceResults =
                  this.dataForm?.proposePerformanceResults || [];
                this.proposePerformanceResults?.forEach((item) => {
                  item.disabled =
                    this.typeAction === ScreenType.Detail ||
                    this.isScreenHandle;
                });
                const totalDensity = this.dataForm?.totalDensity;
                const totalPoint = this.dataForm?.totalPoint;
                this.sumProposePerformanceResults = totalDensity;
                this.sumPoint = totalPoint;
                this.sortOrder = this.dataForm?.sortOrder;
                if (
                  this.typeAction === ScreenType.Update &&
                  !this.isScreenHandle
                ) {
                  if (this.proposePerformanceResults?.length === 0) {
                    this.addProposePerformanceResult();
                  }
                }
                this.proposeCommentForm.patchValue(
                  this.dataForm?.proposeComment
                );
                this.attachDocumentForm.patchValue(
                  this.dataForm?.proposeAttachmentFiles
                );
              }
              if (this.proposeCategory === Constants.ProposeCategory.ELEMENT) {
                this.flowStatusCode =
                  this.proposeDTO.proposeDetails[0].flowStatusCode;
                if (this.proposeDTO.proposeDetails[0].flowStatusPublishCode) {
                  this.statusPublishCode =
                    this.proposeDTO.proposeDetails[0].flowStatusPublishCode;
                }
                this.issuedLater = this.dataForm?.issuedLater || false;
              } else {
                this.flowStatusCode = this.proposeDTO?.flowStatusCode;
                this.statusPublishCode = this.proposeDTO?.flowStatusPublishCode;
              }

              const issueObject = this.dataCommon?.issueLevelList?.find(
                (i) => i.code === this.proposeDTO?.issueLevelCode
              );
              this.issueLevelName = issueObject?.name ? issueObject?.name : '';

              this.proposeDTO.relationType =
                this.proposeCategory === Constants.ProposeCategory.ELEMENT
                  ? false
                  : this.proposeDTO.relationType;
              this.proposeDetailId = this.proposeDTO.proposeDetails[0].id;
              // Lấy ds đơn vị

              if (this.proposeDTO.proposeDetails[0]) {
                this.formRotationType =
                  this.proposeDTO?.proposeDetails[0]?.formRotationType;
                this.violationCode =
                  this.proposeDTO?.proposeDetails[0]?.conditionViolationCode;
              }
              this.flowStatusCodeDisplay = this.proposeDTO
                ?.flowStatusCodeDisplay
                ? this.proposeDTO.flowStatusCodeDisplay
                : this.proposeDTO.flowStatusCode;
              this.proposeUnitForm.patchValue(this.proposeDTO);

              const statusObject =
                this.dataCommon?.flowStatusApproveConfigList?.find(
                  (item) => item.key === this.flowStatusCode
                );
              if (statusObject) {
                this.proposeUnitForm.patchValue({
                  formGroup: this.route.snapshot.queryParams['formGroupCreate'],
                  id:
                    this.proposeCategory === Constants.ProposeCategory.ELEMENT
                      ? this.proposeDTO?.proposeDetails[0]?.proposeCode
                      : this.checkProposeCode(),
                  flowStatusName: statusObject?.role,
                });
              }

              const statusPubishObject =
                this.dataCommon?.flowStatusPublishConfigList?.find(
                  (item) => item.key === this.statusPublishCode
                );
              if (statusPubishObject) {
                this.proposeUnitForm.patchValue({
                  statusPublishName: statusPubishObject?.role,
                });
              }
              this.proposeUnitForm.controls['formGroup'].setValue(
                +this.proposeDTO.formGroup
              );
              this.externalApprove = this.proposeDTO?.externalApprove;
              this.getReportFirst = this.dataForm?.externalApprove;

              this.listAttachFile = [];
              if (this.dataForm?.proposeAttachmentFiles?.docId) {
                this.listAttachFile = [
                  ...this.listAttachFile,
                  {
                    uid: this.dataForm?.proposeAttachmentFiles?.docId,
                    name: this.dataForm?.proposeAttachmentFiles?.fileName,
                    status: 'done',
                    id: this.dataForm?.proposeAttachmentFiles?.id,
                  },
                ];
                this.attachFileObj.uid =
                  this.dataForm?.proposeAttachmentFiles?.docId;
                this.attachFileObj.fileName =
                  this.dataForm?.proposeAttachmentFiles?.fileName;
              }
              this.displayReport = this.dataForm?.externalApprove || false;
              const withdrawNote = data?.withdrawNoteConfigList.find(
                (i: StatusList) =>
                  i.key === this.proposeDTO?.withdrawNote?.toString()
              )?.role;
              this.proposeUnitForm.controls['withdrawNote'].setValue(
                withdrawNote
              );
              this.conditionViolation =
                this.dataForm?.conditionViolations || [];
              this.disableFormRotationRadio =
                this.typeAction === ScreenType.Detail || this.isScreenHandle
                  ? true
                  : false;
              const proposeStatementDetails =
                this.dataForm?.proposeStatement || [];
              if (proposeStatementDetails?.length > 0) {
                proposeStatementDetails?.forEach((item: ProposeStatementDTO) => {
                  if (
                    item?.typeStatement === Constants.TypeStatement.EDITHRDV
                  ) {
                    this.formReportElement.patchValue({
                      statementNumber: item?.statementNumber,
                      submissionDate: item?.submissionDate,
                      approveNote: this.proposeDTO?.approveNote,
                    });
                  }
                });
              }
              this.fileListSelectTTElement = [];
              this.fileElementObj =
                this.screenCode === Constants.ScreenCode.HRDV
                  ? this.dataForm?.fileDTOS?.find(
                      (item: FileDTOList) =>
                        item.type === Constants.TypeStatementFile.STATEMENTEDIT
                    )
                  : this.dataForm?.fileDTOList?.find(
                      (item: FileDTOList) =>
                        item.type === Constants.TypeStatementFile.STATEMENTEDIT
                    );
              if (this.fileElementObj?.docId && this.fileElementObj?.fileName) {
                this.fileListSelectTTElement = [
                  ...this.fileListSelectTTElement,
                  {
                    uid: this.fileElementObj?.docId,
                    name: this.fileElementObj?.fileName,
                    status: 'done',
                    id: this.fileElementObj?.id,
                  },
                ];
                this.fileElement.uid = this.fileElementObj?.docId;
                this.fileElement.fileName = this.fileElementObj?.fileName;
              }
              this.onChangeProposeType(+this.proposeDTO?.formGroup);
            }
          }
          this.isLoading = false;
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        }
      );
      this.subs.push(subs);
    }
  }

  checkUrl() {
    this.pageName = this.route.snapshot?.data['pageName'];
    if (Constants.PageNameHRDV.includes(this.pageName)) {
      this.screenCode = Constants.ScreenCode.HRDV;
    } else if (Constants.PageNamePTNL.includes(this.pageName)) {
      this.screenCode = Constants.ScreenCode.PTNL;
    } else if (Constants.PageNameLDDV.includes(this.pageName)) {
      this.screenCode = Constants.ScreenCode.LDDV;
    } else {
      this.screenCode = Constants.ScreenCode.KSPTNL;
    }
  }

  checkProposePerformanceResultsHasAction() {
    return !this.proposePerformanceResults?.find((item) => !item.disabled);
  }

  checkDeleteReason() {
    return this.rotationReasons?.length === 1;
  }

  disableSubmissionSignedDate = (dateVal: Date) => {
    return dateVal.getTime() >= Date.now();
  };

  addProposePerformanceResult() {
    this.proposePerformanceResults = [
      ...(this.proposePerformanceResults ? this.proposePerformanceResults : []),
      ...[
        {
          id: null,
          proposeDetailId: null,
          targetAssigned: null,
          density: null,
          planIndicator: null,
          planDeadline: null,
          realityIndicator: null,
          realityDeadline: null,
          disabled: false,
        },
      ],
    ];
  }

  checkDeletePerformance() {
    return this.proposePerformanceResults.length === 1;
  }

  saveCalculateItem(item: ProposePerformanceResults) {
    this.calculate(item);
    this.calculateDensity();
    if (this.sumProposePerformanceResults > 100) {
      this.sumProposePerformanceResults =
        this.currentSumProposePerformanceResults;
      this.sumPoint = this.currentSumPoint;
      return this.toastrCustom.warning(
        this.translate.instant(
          'development.proposed-unit.messages.sumProposePerformanceBig'
        )
      );
    }
  }

  editProposePerformanceResult(item: ProposePerformanceResults) {
    item.disabled = false;
  }

  saveProposePerformanceResult(item: ProposePerformanceResults) {
    this.saveCalculateItem(item);
    this.checkDeletePerformance();
  }

  deleteProposePerformanceResult(index: number) {
    this.proposePerformanceResults.splice(index, 1);
    this.proposePerformanceResults = [...this.proposePerformanceResults];
    this.checkDeletePerformance();
    this.calculateDensity();
    this.calculatePoint();
  }

  submit() {
    if (this.screenCode === Constants.ScreenCode.HRDV) {
      this.isSubmitProposeDetail = false;
    } else {
      if (!this.validateSubmitForm()) {
        return;
      }
    }
    const paramSrcCode: ScreenCode = {
      screenCode: this.screenCode ? this.screenCode : '',
    };
    this.isLoading = true;
    if (!this.displayReport) {
      this.saveReportElement();
    }
    const update = this.TGCVService.updateTGCV(this.payloadSubmitForm(true), paramSrcCode).subscribe(
      () => {
        switch(this.screenCode){
          case Constants.ScreenCode.HRDV:
            this.toastrCustom.success(
              this.translate.instant(
                'development.proposed-unit.messages.updateSuccess'
              )
            );
            this.loadAll();
            break;
          case Constants.ScreenCode.PTNL:
            this.toastrCustom.success(
              this.translate.instant(
                'development.proposed-unit.messages.updateSuccess2'
              )
            );
            this.isLoading = false;
            break;
        }
      },
      (e) => {
        this.toastrCustom.error(e?.error?.message);
        this.isLoading = false;
      }
    );
    this.subs.push(update);
  }

  calculate(item: ProposePerformanceResults, isDensity?: boolean) {
    //Tính điểm
    const totalDensity = this.sumProposePerformanceResults + item?.density!;
    if (totalDensity > 100 && isDensity) {
      item.density = 0;
      this.calculateDensity();
      this.toastrCustom.warning(
        this.translate.instant(
          'development.proposed-unit.messages.sumProposePerformanceBig'
        )
      );
      return;
    }
    this.calculateDensity();
  }

  calculatePoint() {
    this.sumPoint = 0;
    this.proposePerformanceResults?.forEach((item) => {
      this.sumPoint = this.sumPoint + parseFloat(<string>item.point);
    });
  }

  calculateDensity() {
    this.sumProposePerformanceResults = 0;
    this.proposePerformanceResults?.forEach((item) => {
      this.sumProposePerformanceResults =
        this.sumProposePerformanceResults + item.density!;
    });
  }

  onChangeFormRotation(index: number) {
    this.rotations.forEach((item, i) => {
      if (i === index) {
        item.disabled = false;
      } else {
        item.disabled = true;
        item.formRotation = null;
        item.contentUnit = null;
        item.formRotationType = null;
        item.formRotationTitle = null;
        item.contentUnitCode = null;
        item.contentTitleCode = null;
        item.contentTitleName = null;
        item.contentUnitPathId = null;
        item.contentUnitPathName = null;
        item.contentUnitTreeLevel = null;
        item.level = null;
      }
    });
  }

  setEmpNullValue() {
    this.employeeForm.patchValue({
      empIdp: null,
      orgId: null,
      empId: null,
      empCode: null,
      fullName: null,
      email: null,
      posId: null,
      posName: null,
      jobId: null,
      jobName: null,
      orgPathId: null,
      orgPathName: null,
      orgName: null,
      majorLevelId: null,
      majorLevelName: null,
      faculityId: null,
      faculityName: null,
      schoolId: null,
      schoolName: null,
      positionLevel: null,
      gender: null,
      joinCompanyDate: null,
      dateOfBirth: null,
      partyDate: null,
      partyOfficialDate: null,
      posSeniority: null,
      seniority: null,
      qualificationEnglish: null,
      facultyName: null,
      semesterGradeT1: null,
      semesterRankT1: null,
      semesterGradeT2: null,
      semesterRankT2: null,
      ratedCapacity: null,
      nearestDiscipline: null,
      endOfDisciplinaryDuration: null,
      userToken: null,
      treeLevel: null,
    });
  }

  override handleDestroy(): void {
    this.rotations.forEach((item) => {
      item.formRotation = null;
      item.contentTitleCode = null;
      item.contentUnitCode = null;
      item.formRotationTitle = null;
      item.formRotationType = null;
    });
    this.rotationReasons?.forEach((item) => {
      item.checked = false;
      item.description = null;
      item.proposeDetailId = null;
      item.rotationReasonID = item.id;
      item.disabled = true;
    });
  }

  downloadStatement(action: number) {
    const params = {
      groupProposeType: Constants.ProposeTypeID.ELEMENT,
      action: action,
      screenCode: this.screenCode,
    };
    this.isLoading = true;
    let listFile: ListFileTemplate[] = [];
    if (this.proposeDetailId) {
      const sub = this.service
        .exportReportDecision(this.proposeDetailId, params)
        .subscribe(
          (res) => {
            listFile = res?.data;
            if (!listFile || listFile?.length === 0) {
              this.toastrCustom.error('development.list-plan.messages.noFile');
              this.isLoading = false;
            } else {
              listFile?.forEach((item) => {
                this.service.callBaseUrl(item.linkTemplate).subscribe(
                  (res) => {
                    const contentDisposition = res?.headers.get(
                      'content-disposition'
                    );
                    const filename = contentDisposition
                      ?.split(';')[1]
                      ?.split('filename')[1]
                      ?.split('=')[1]
                      ?.trim();
                    const blob = new Blob([res?.body], {
                      type: 'application/octet-stream',
                    });
                    FileSaver.saveAs(blob, filename);
                    this.isLoading = false;
                  },
                  (e) => {
                    this.getMessageError(e);
                    this.isLoading = false;
                  }
                );
              });
            }
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
      this.subs.push(sub);
    }
  }

  checkformRotationLabel(formRotationLabel: string) {
    if (this.rotations[0].formRotationLabel === formRotationLabel) {
      return 'development.proposed-unit.table.formRotationLabel1';
    } else {
      return 'development.proposed-unit.table.formRotationLabel2';
    }
  }

  showModalPropose() {
    if (this.typeAction !== ScreenType.Create) {
      return;
    }
    this.modalRef = this.modal.create({
      nzTitle: this.translate.instant(
        'development.propose-tgcv.searchEmployee'
      ),
      nzWidth: this.getInnerWidth(),
      nzContent: ProposeMultipleEmployeeComponent,
      nzComponentParams: {
        isEmp: true,
      },
      nzFooter: null,
    });
    this.modalRef.afterClose.subscribe((result) => {
      if (result?.length > 0) {
        let listCode = [];
        listCode.push(result[0]?.empCode);
        this.proposeDetailForm.enable();
        const paramAddEmp = {
          proposeId: this.proposeId,
          employeeCodes: listCode,
          proposeUnitRequest:
            this.screenCode === Constants.ScreenCode.HRDV
              ? this.proposeUnit?.proposeUnitRequest
              : null,
          formGroup: this.formGroupCreate,
          proposeUnitRequestName:
            this.screenCode === Constants.ScreenCode.HRDV
              ? this.proposeUnit?.proposeUnitRequestName
              : null,
        };
        this.isLoading = true;
        if (this.screenCode) {
          this.proposeMultipleService
            .addEmployee(paramAddEmp, this.screenCode)
            .subscribe(
              () => {
                this.router.navigate(
                  [
                    Utils.getUrlByFunctionCode(this.functionCode),
                    Constants.SubRouterLink.UPDATETGCV,
                  ],
                  {
                    queryParams: {
                      id: this.proposeId,
                      proposeType: Utils.getGroupType(
                        Constants.ProposeCategory.ORIGINAL
                      ),
                      proposeCategory: Constants.ProposeCategory.ORIGINAL,
                      flowStatusCode:
                        this.screenCode === Constants.ScreenCode.HRDV
                          ? Constants.FlowCodeConstants.CHUAGUI1
                          : Constants.FlowCodeConstants.NHAP,
                      relationType: true,
                    },
                    skipLocationChange: true,
                  }
                );
                this.isLoading = false;
              },
              (e) => {
                this.getMessageError(e);
                this.isLoading = false;
              }
            );
        }
      }
    });
  }

  getInnerWidth(): number {
    if (window.innerWidth > 767) {
      return window.innerWidth / 1.5 > 1368 ? 1368 : window.innerWidth / 1.3;
    }
    return window.innerWidth;
  }

  onChangeUnit(orgId: string, index: number, contentTitleDetail?: number) {
    this.jobDTOSApi = [];
    if (orgId) {
      this.isLoading = true;
      const paramPosition: ParamSearchPosition = {
        orgId: orgId,
        jobId: '',
        status: 1,
        type: Constants.TYPE_SEARCH_POSITION,
        page: 0,
        size: maxInt32,
      };
      this.service.getPositionByUnit(paramPosition).subscribe(
        (res) => {
          const listPositionParent = res?.data?.content;
          this.jobDTOSApi = listPositionParent.map((item: ListTitle) => {
            return { jobId: item.jobId, jobName: item.jobName };
          });
          this.rotations[index].contentTitleCode = contentTitleDetail;
          this.isLoading = false;
        },
        (e) => {
          this.toastrCustom.error(e?.error?.message);
          this.isLoading = false;
        }
      );
    } else {
      this.rotations.forEach((item, i) => {
        if (i === this.dataForm?.formRotation) {
          item.contentTitleCode = null;
        }
      });
    }
  }

  expandLog() {
    this.isExpand = !this.isExpand;
    if (this.isExpand) {
      this.iconStatus = Constants.IconStatus.DOWN;
    } else {
      this.iconStatus = Constants.IconStatus.UP;
    }
  }

  changeTitle(valueTitle: string, index: number) {
    if (+valueTitle === 0) {
      this.rotations[index].contentTitleCode =
        this.jobDTOSApi.find((item) => item.jobId === +this.contentTitleEmp!)
          ?.jobId || null;
      if (
        !this.rotations[index].contentTitleCode &&
        (this.rotations[index].contentUnitCode ||
          this.rotations[index].contentUnit)
      ) {
        this.toastrCustom.error(
          this.translate.instant('development.commonMessage.E001')
        );
      }
    } else {
      this.rotations[index].contentTitleCode = null;
    }
  }

  scrollToValidationSection(section: string, messsage: string) {
    this.toastrCustom.warning(this.translate.instant(messsage));
    let el = document.getElementById(section);
    el?.scrollIntoView({
      behavior: 'smooth',
      block: 'start',
      inline: 'nearest',
    });
  }

  downloadFile = (file: NzUploadFile) => {
    this.service.downloadFile(+file?.uid, file?.name).subscribe(
      () => {
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  };

  expandInfoEmp() {
    this.isSeeMore = !this.isSeeMore;
  }

  expandInfoEmpText() {
    const infoEmpText = !this.isSeeMore
      ? this.translate.instant('development.proposed-unit.button.down')
      : this.translate.instant('development.proposed-unit.button.up');
    return infoEmpText;
  }

  //check hiển thi radio lãnh đạo duyệt trên HCM
  checkHCMRadio() {
    return (
      this.issueLevelName !== Constants.IssueLevelName.UNIT ||
      (this.issueLevelName === Constants.IssueLevelName.UNIT &&
        this.proposeDTO?.isHo)
    );
  }

  //validate điền thông tin cho nút ghi nhận đồng ý
  validApproveButton() {
    return (
      this.formTTQD.get('statementNumber')?.value &&
      this.formTTQD.get('submissionDate')?.value &&
      this.formTTQD.get('approveNote')?.value &&
      this.listAttachFile.length === 1
    );
  }

  //lưu tờ trình của thành phần
  saveReportElement() {
    const submitForm = this.checkDisplayReportEdit()
      ? this.formReportElement.getRawValue()
      : this.formTTQD.getRawValue();
    if (this.checkDisplayReportEdit()) {
      submitForm.typeStatement = Constants.TypeStatement.EDITHRDV;
    }
    submitForm.id = +this.proposeIdOriginal;
    submitForm.externalApprove = this.displayReport;
    const sub = this.service.saveStatementDecision(submitForm).subscribe(
      () => {
        if (this.isAcceptPropose) {
          this.callApiAcceptPropose();
        }
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }

  override ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
    this.modalRef?.destroy();
  }

  //validate ngày đề xuất triển khai
  disabledImplementDate = (dateVal: Date) => {
    return dateVal.valueOf() <= Date.now();
  };

  initLogs() {
    this.tableLogs = {
      headers: [
        {
          title: 'development.proposed-unit.proposeLogsTable.time',
          field: 'actionDate',
          width: 160,
        },
        {
          title: 'development.proposed-unit.proposeLogsTable.personAction',
          field: 'userName',
          width: 150,
        },
        {
          title: 'development.proposed-unit.proposeLogsTable.action',
          field: 'action',
          width: 130,
        },
        {
          title: 'development.proposed-unit.proposeLogsTable.description',
          field: 'comment',
          width: 500,
        },
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: userConfig.pageSize,
      pageIndex: 1,
    };
  }

  beforeUploadAttachFile = (file: File | NzUploadFile): boolean => {
    this.isLoading = true;
    const body = {
      proposeDetailId: this.proposeDetailId,
      personalReview: this.attachDocumentForm.get('personalReview')?.value,
      different: this.attachDocumentForm.get('different')?.value,
      consultationComment: this.attachDocumentForm.get('consultationComment')?.value,
      reportBranchMeeting: this.attachDocumentForm.get('reportBranchMeeting')
        ?.value,
      statement: this.displayReport,
    };
    const formData = new FormData();
    formData.append('files', file as File);
    formData.append(
      'proposeAttachmentFiles',
      new Blob([JSON.stringify(body)], {
        type: Constants.CONTENT_TYPE.APPLICATION_JSON,
      })
    );
    this.TGCVService.uploadFileTGCV(formData).subscribe(
      (res) => {
        this.attachDocumentForm.patchValue({
          docId: res.data?.docId,
          fileName: res.data?.fileName,
        });
        this.listAttachFile = [
          ...this.listAttachFile,
          {
            uid: res.data?.docId,
            name: res.data?.fileName,
            status: 'done',
            id: res.data?.id,
          },
        ];
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    return false;
  };

  doRemoveAttachFile = (): boolean => {
    this.isLoading = true;
    if (this.listAttachFile?.length === 1) {
      const id = this.dataForm?.id || 0;
      const sub = this.TGCVService.deleteFile(id).subscribe(
        () => {
          this.isLoading = false;
          this.listAttachFile = [];
          this.attachDocumentForm.get('docId')?.setValue(null);
          this.attachDocumentForm.get('fileName')?.setValue(null);
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        }
      );
      this.subs.push(sub);
    }
    return false;
  };

  downloadAttachFile = (file: NzUploadFile) => {
    this.isLoading = true;
    this.service.downloadFile(+file?.uid, file?.name).subscribe(
      () => {
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  };

  //button header
  handle() {
    this.isLoading = true;
    const param = {
      id:
        this.proposeCategory === Constants.ProposeCategory.ELEMENT
          ? this.proposeDetailId
          : +this.proposeIdOriginal,
      proposeType: this.proposeType,
    };
    this.service.proposalProcessing(param).subscribe(
      (res) => {
        this.router.navigate(
          [Utils.getUrlByFunctionCode(this.functionCode), Constants.TypeScreenHandle.HANDLE_TGCV],
          {
            queryParams: {
              id: this.proposeIdOriginal,
              proposeDetailId: this.proposeDetailId,
              proposeCategory: this.proposeCategory,
              flowStatusCode: res?.data || '',
              relationType: this.proposeUnitForm.get('relationType')?.value,
            },
            skipLocationChange: true,
          }
        );
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

  checkBtnDelete() {
    return (this.screenCode === this.screenCodeConst.HRDV
        && this.flowStatusCode === Constants.FlowCodeConstants.CHUAGUI1
        && this.typeAction === this.screenTypeConst.Update) ||
      (this.proposeDTO?.createdBy === this.currentUser?.username
        && this.screenCode === this.screenCodeConst.PTNL && Constants.CheckDeleteBtn.includes(this.flowStatusCode));
  }

  delete(event: MouseEvent) {
    event.stopPropagation();
    if (this.isLoading) {
      return;
    }
    const groupProposeType = Utils.getGroupType(this.proposeCategory);
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant(
        'development.proposed-unit.messages.delete'
      ),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        this.service
          .deleteProposeListLD(this.checkProposeCode(), groupProposeType)
          .subscribe(
            () => {
              this.toastrCustom.success(
                this.translate.instant(
                  'development.proposed-unit.messages.deleteSuccess'
                )
              );
              if (this.stateUrl?.state?.['backUrl'] && this.proposeCategory !== Constants.ProposeCategory.ORIGINAL) {
                this.back();
              } else {
                this.router.navigateByUrl(Utils.getUrlByFunctionCode(this.functionCode));
              }
              this.modalRef?.destroy();
            },
            (e) => {
              this.getMessageError(e);
              this.isLoading = false;
            }
          );
      },
    });
  }

  checkProposeCode() {
    switch (this.proposeCategory) {
      case Constants.ProposeCategory.ORIGINAL:
        return this.proposeIdOriginal;
      case Constants.ProposeCategory.ELEMENT:
        return this.proposeDetailId;
      case Constants.ProposeCategory.GROUP:
        return this.proposeIdOriginal;
      default:
        return null;
    }
  }

  override back() {
    if (this.stateUrl?.state?.['backUrl']) {
      this.router
        .navigateByUrl(Utils.getUrlByFunctionCode(this.functionCode))
        .then(() => {
          this.router.navigateByUrl(this.stateUrl?.state?.['backUrl'], {
            skipLocationChange: true,
            state: {
              action: 'add',
            },
          });
        });
    } else {
      this.router.navigateByUrl(Utils.getUrlByFunctionCode(this.functionCode));
    }
  }

  //ghi nhận đồng ý
  acceptPropose() {
    if (!this.validateSubmitForm()) {
      return;
    }
    //nếu có tờ trình chỉnh sửa thì validate tờ trình này
    //nếu không thì validate tờ trình ban đầu
    if (this.checkDisplayReportEdit()) {
      if (!this.validApproveEditReport()) {
        this.scrollToValidationSection('formTTQD', 'development.commonMessage.validateRecordApprove2');
        return;
      }
    } else {
      if (!this.validApproveButton()) {
        this.scrollToValidationSection('formTTQD', 'development.commonMessage.validateRecordApprove2');
        return;
      }
    }
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant(
        'development.proposed-unit.messages.acceptPropose'
      ),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        if (this.violationCode === Constants.ViolationCode.REQUIRED) {
          this.isLoading = false;
          this.modalRef = this.modal.confirm({
            nzTitle: this.translate.instant(
              'development.proposed-unit.messages.sendProposeWarning2'
            ),
            nzCancelText: this.translate.instant(
              'development.proposed-unit.button.cancelText'
            ),
            nzOkText: this.translate.instant(
              'development.proposed-unit.button.okText'
            ),
            nzClassName: 'ld-confirm',
            nzOnOk: () => {
              this.isLoading = true;
              this.isAcceptPropose = true;
              this.saveReportElement();
            },
          });
        } else {
          this.isAcceptPropose = true;
          this.saveReportElement();
        }
      },
    });
  }

  callApiAcceptPropose() {
    const params = {
      proposeType: this.proposeType,
      note: this.checkDisplayReportEdit()
        ? this.formReportElement.get('approveNote')?.value
        : this.formTTQD.get('approveNote')?.value,
    }
    const sub = this.service
      .agreeUnitNew(this.checkProposeCode(), this.payloadSubmitForm(false), params)
      .subscribe(
        (res) => {
          if (res?.data && res?.data?.length > 0 ) {
            this.toastrCustom.error(res?.data.map((item: ProposedError) => item.errorMessage).join(', '));
          } else {
            this.toastrCustom.success(
              this.translate.instant(
                'development.proposed-unit.messages.acceptProposeSuccess'
              )
            );
            this.router.navigateByUrl(
              Utils.getUrlByFunctionCode(this.functionCode)
            );
          }
          this.isLoading = false;
          this.modalRef?.destroy();
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        }
      );
    this.subs.push(sub);
  }

  onChangeUnitDiff(orgIdLevel: string) {
    if (orgIdLevel) {
      const param = {
        id: +orgIdLevel,
        pag: 0,
        size: maxInt32,
      };
      const getOrganization = this.service
        .getOrganizationTreeById(param)
        .subscribe((res) => {
          if (res && res?.content?.length > 0) {
            res?.content.forEach((i: OrgEmployee) => {
              if (i.treeLevel > 2) {
                i.orgNameFull = i.pathName.split("-").splice(2).join("-") + " - " + i.orgName;
              }
            });
            this.listInUnit = res?.content.map((item: OrgEmployee) => {
              return {
                name: item.orgNameFull,
                value: item.orgId,
                valueItem: {
                  orgId: item.orgId,
                  orgName: item.orgName,
                  pathId: item.pathId,
                  pathResult: item.pathName,
                  orgLevel: item.treeLevel,
                },
              };
            });
          }
        });
      this.subs.push(getOrganization);
    }
  }

  onChangeProposeType(e: number) {
    const sub = this.proposeMultipleService
      .formGroupProposeMultiple(e)
      .subscribe((res) => {
        this.dataCommon.formGroupMultipleList = res?.data || [];
        if (this.dataCommon.formGroupMultipleList) {
          this.showMinTime = this.dataCommon.formGroupMultipleList[0]?.showMinTime || false;
          this.showMaxTime = this.dataCommon.formGroupMultipleList[0]?.showMaxTime || false;
        }
        if (this.showMaxTime && !this.proposeDetailForm.get('timeType')?.value) {
          this.proposeDetailForm.get('timeType')?.setValue(Constants.TypeMaximumValue.MONTH);
        }
        if (!this.showMinTime) {
          this.proposeDetailForm.get('minimumTime')?.clearValidators();
          this.proposeDetailForm.get('minimumTime')?.updateValueAndValidity();
        }
        if (!this.showMaxTime) {
          this.proposeDetailForm.get('maximumTime')?.clearValidators();
          this.proposeDetailForm.get('maximumTime')?.updateValueAndValidity();
          this.proposeDetailForm.get('timeType')?.clearValidators();
          this.proposeDetailForm.get('timeType')?.updateValueAndValidity();
        }
      });
    this.subs.push(sub);
  }

  //check hiển thị nút ghi nhận đồng ý
  checkRecordApprove() {
    return (
      !this.displayReport &&
      Constants.StatusApproveSendProposeMany.indexOf(this.flowStatusCode) >
        -1 &&
      this.checkHCMRadio()
    );
  }

  //check hiển thị nút gửi duyệt
  checkSendApprove() {
    return (
      this.displayReport &&
      Constants.StatusApproveSendProposeMany.indexOf(this.flowStatusCode) >
        -1 &&
      this.checkHCMRadio()
    );
  }

  //check hiển thị ý kiến phê duyệt của LDDV
  checkIdeaApproveLDDV() {
    return (
      this.screenCode === this.screenCodeConst.LDDV &&
      (this.flowStatusCode === this.flowCodeConst.CHODUYET2 ||
        this.flowStatusCode === this.flowCodeConst.SENDAPPROVE2A)
    );
  }

  //check hiển thị nút xử lý của PTNL
  checkHandleButton() {
    switch (this.proposeCategory) {
      case Constants.ProposeCategory.ELEMENT:
        return Constants.ViewButtonHandleRemove.includes(this.flowStatusCode) && !this.isScreenHandle;
      case Constants.ProposeCategory.ORIGINAL:
        return Constants.CheckDeleteBtn.includes(this.flowStatusCode) && !this.isScreenHandle;
      default: return false;
    }
  }

  //gửi duyệt
  sendPropose() {
    if (!this.validateSubmitForm()) {
      return;
    }
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant(
        'development.proposed-unit.messages.sendPropose'
      ),
      nzCancelText: this.translate.instant(
        'development.proposed-unit.button.cancel'
      ),
      nzOkText: this.translate.instant(
        'development.proposed-unit.button.continue'
      ),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        if (this.violationCode === Constants.ViolationCode.REQUIRED) {
          this.isLoading = false;
          this.modalRef = this.modal.confirm({
            nzTitle: this.translate.instant(
              'development.propose-tgcv.confirmSendApprove'
            ),
            nzCancelText: this.translate.instant(
              'development.proposed-unit.button.cancel'
            ),
            nzOkText: this.translate.instant(
              'development.proposed-unit.button.continue'
            ),
            nzClassName: Constants.ClassName.LD_CONFIRM,
            nzOnOk: () => {
              this.isLoading = true;
              this.callApiSendPropose();
              this.modalRef?.destroy();
            },
          });
        } else {
          this.callApiSendPropose();
        }
      },
    });
  }

  callApiSendPropose() {
    const params = {
      proposeType: this.proposeType,
    }
    const send = this.service
      .sendProposeNew(this.checkProposeCode(), this.payloadSubmitForm(false), params)
      .subscribe(
        (res) => {
          if (res?.data && res?.data?.length > 0 ) {
            this.toastrCustom.error(res?.data.map((item: ProposedError) => item.errorMessage).join(', '));
          } else {
            this.toastrCustom.success(
              this.translate.instant(
                'development.proposed-unit.messages.sendProposeSuccess'
              )
            );
            this.router.navigateByUrl(
              Utils.getUrlByFunctionCode(this.functionCode)
            );
          }

          this.isLoading = false;
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        }
      );
    this.subs.push(send);
  }

  //check hiển thị 3 nút duyệt của LDDV và ý kiến phê duyệt
  checkApproveLDDV() {
    if (this.proposeCategory !== Constants.ProposeCategory.ELEMENT) {
      return this.checkIdeaApproveLDDV();
    } else {
      return (
        this.screenCode === this.screenCodeConst.LDDV &&
        this.flowStatusCode === this.flowCodeConst.SENDAPPROVE2A &&
        this.flowStatusCodeDisplay !== this.flowCodeConst.SENDAPPROVE2A
      );
    }
  }

  // duyệt đồng ý LDDV
  agreeLeader() {
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant(
        'development.proposed-unit.messages.agreeLeader'
      ),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL2_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.CONTINUE_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        const proposeType = Constants.ProposeTypeID.ORIGINAL;
        this.service
          .agreeLeader(
            this.proposeIdOriginal,
            this.formIdeaLDDV.get('idea')?.value,
            proposeType
          )
          .subscribe(
            () => {
              this.toastrCustom.success(
                this.translate.instant(
                  Constants.KeyCommons.MESSAGES_SUCCESS2
                )
              );
              this.router.navigateByUrl(
                Constants.KeyCommons.DEVELOPMENT_PROPOSE_UNIT_APPROVE
              );
              this.isLoading = false;
              this.modalRef?.destroy();
            },
            (e) => {
              this.getMessageError(e);
              this.isLoading = false;
            }
          );
      },
    });
  }

  // duyệt từ chối LDDV
  rejectLeader() {
    this.isSubmitIdeaLDDV = true;
    if (this.formIdeaLDDV.invalid) {
      this.scrollToValidationSection(
        'idea',
        'development.commonMessage.approveIdeaRequired'
      );
      return;
    }
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant(
        'development.proposed-unit.messages.rejectLeader'
      ),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        this.service
          .rejectLeader(
            this.proposeIdOriginal,
            this.formIdeaLDDV.get('idea')?.value
          )
          .subscribe(
            () => {
              this.toastrCustom.success(
                this.translate.instant(
                  Constants.KeyCommons.MESSAGES_SUCCESS2
                )
              );
              this.router.navigateByUrl(
                Constants.KeyCommons.DEVELOPMENT_PROPOSE_UNIT_APPROVE
              );
              this.isLoading = false;
              this.modalRef?.destroy();
            },
            (e) => {
              this.toastrCustom.error(
                this.translate.instant(
                  'development.proposed-unit.messages.errorMes'
                )
              );
              this.isLoading = false;
            }
          );
      },
    });
  }

  // duyệt ý kiến khác LDDV
  otherIdea() {
    this.isSubmitIdeaLDDV = true;
    if (this.formIdeaLDDV.invalid) {
      this.scrollToValidationSection(
        'idea',
        'development.commonMessage.approveIdeaRequired'
      );
      return;
    }
    const proposeType = Constants.ProposeTypeID.ORIGINAL;
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.proposed-unit.messages.otherIdeaLDDV'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        this.service
          .otherIdea(
            this.proposeIdOriginal,
            this.formIdeaLDDV.get('idea')?.value,
            proposeType
          )
          .subscribe(
            (res) => {
              this.toastrCustom.success(this.translate.instant(Constants.KeyCommons.MESSAGES_SUCCESS2));
              this.router.navigateByUrl(
                Constants.KeyCommons.DEVELOPMENT_PROPOSE_UNIT_APPROVE
              );
              this.isLoading = false;
              this.modalRef?.destroy();
            },
            (e) => {
              this.getMessageError(e);
              this.isLoading = false;
            }
          );
      },
    });
  }

  //check hiển thị rút toàn bộ
  checkWithdrawPropose(){
    const flowCodeParent = this.proposeDTO?.flowStatusCode || '';
    const flowCodeElement = this.dataForm?.flowStatusCode || '';
    return (flowCodeParent === Constants.FlowCodeConstants.HANDLING5B ||
      flowCodeParent === Constants.FlowCodeConstants.HANDLING6) &&
      Constants.ChangeStatusWithdraw.indexOf(flowCodeElement) <= -1 &&
      !this.dataForm?.withdrawNote
      && this.screenCode === Constants.ScreenCode.HRDV;
  }

  showModalWithdraw(typeButton: number) {
    const ids: number[] = [];
    ids.push(this.proposeIdOriginal);
    this.modalRef = this.modal.create({
      nzTitle: this.translate.instant('development.proposed-unit.proposeMultiple.withdrawPropose'),
      nzWidth: '40vw',
      nzContent: WithdrawProposeComponent,
      nzComponentParams: {
        typeWithdraw: typeButton,
        ids: ids,
        isApproveHCMBefore: this.dataForm?.externalApprove,
      },
      nzFooter: null,
    });
    this.modalRef.componentInstance.closeEvent.subscribe((data:boolean) => {
      if (data) {
        this.isLoading = true;
        this.modalRef?.componentInstance.sendBody.subscribe((body: WithdrawPropose) => {
          const sub = this.service.withdrawPropose(body).subscribe(() => {
            this.toastrCustom.success(
              this.translate.instant('development.withdrawPropose.withdrawProposeSuccess'));
            this.ngOnInit();
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          });
          this.subs.push(sub);
          this.modalRef?.destroy();
        });
      } else {
        this.modalRef?.destroy();
      }
    });
  }

  //check nút duyệt rút
  checkWithdrawBtn(){
    const listWithdrawBtn = Constants.StatusButtonWithdraw;
    const listNoteCodeWithdraw = Constants.noteWithdraw;
    if(this.dataForm?.withdrawNote) {
      return listWithdrawBtn.indexOf(this.flowStatusCode) > -1
        && listNoteCodeWithdraw.indexOf(this.dataForm?.withdrawNote?.toString()) > -1
        && (this.screenCode === this.screenCodeConst.LDDV);
    }
    return false;
  }

  checkBtnWithdrawLDDV() {
    const listNoteCodeWithdraw = Constants.noteWithdraw;
    return Constants.ChangeStatusWithdraw.indexOf(this.flowStatusCode) === -1
      && listNoteCodeWithdraw.indexOf(this.withdrawNote?.toString()) > -1
      && (this.screenCode === this.screenCodeConst.LDDV);
  }

  checkWithdrawTypebtn() {
    return (this.checkWithdrawType === 0 || this.checkWithdrawType === 1)
      && (this.screenCode === this.screenCodeConst.LDDV);
  }

  // duyệt rút đề xuất
  agreeWithdrawProposal(ideaType: number) {
    if (!this.comment && (ideaType === Constants.WithdrawConfirmIdeaType.REJECT
      || ideaType === Constants.WithdrawConfirmIdeaType.OTHERIDEA)) {
        this.scrollToValidationSection('confirmWithdraw', 'development.commonMessage.approveIdeaRequired');
        return;
      }
    const checkProposeType = Constants.ProposeTypeID.ORIGINAL;
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.proposed-unit.messages.agreeLeader2'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        if(this.proposeIdOriginal && checkProposeType){
          this.service.approveWithdrawPropose(this.proposeIdOriginal, checkProposeType, this.comment, ideaType, this.screenCode).subscribe(
            () => {
              this.toastrCustom.success(this.translate.instant(Constants.KeyCommons.MESSAGES_SUCCESS2));
              this.router.navigateByUrl(Constants.KeyCommons.DEVELOPMENT_PROPOSE_UNIT_APPROVE);
              this.isLoading = false;
              this.modalRef?.destroy();
            },
            (e) => {
              this.getMessageError(e);
              this.isLoading = false;
            }
          );
        }
      },
    });
  }

  //check điều kiện vi phạm
  displayConditionViolation() {
    const violationObj = this.dataCommon.conditionViolationList?.find(
      (item) => +item.code === this.violationCode);
    switch (this.violationCode) {
      case Constants.ViolationCode.REQUIRED:
        return { color: Constants.ConditionViolationColor.REQUIRED, label: violationObj?.name };
      case Constants.ViolationCode.VIOLATE:
        return { color: Constants.ConditionViolationColor.VIOLATE, label: violationObj?.name };
      case Constants.ViolationCode.PASSED:
        return { color: Constants.ConditionViolationColor.PASSED, label: violationObj?.name };
      default:
        return null;
    }
  }

  downloadAttachFile2(uid: number | string, name: string) {
    this.isLoading = true;
    this.service.downloadFile(+uid, name).subscribe(
      () => {
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  };

  scrollTo(tab: ScrollTabOption) {
    const el = document.getElementById(tab.scrollTo ?? '');
    el?.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
  }

  onLoading(event: boolean) {
    this.isLoading = event;
  }

  //nút lưu
  checkSaveButton() {
    return (this.screenCode === Constants.ScreenCode.HRDV || Constants.ScreenCode.PTNL)
      && this.typeAction === this.screenTypeConst.Update;
  }

  checkConsultationSrc() {
    return (this.pageName === Constants.PageNamePTNL[2] ||
      this.pageName === Constants.PageNamePTNL[3] ||
      this.pageName === Constants.PageNamePTNL[4] ||
      this.pageName === Constants.PageName.detailKSPTNL) &&
      ((!Constants.ViewButtonHandleRemove.includes(this.flowStatusCode) && this.flowStatusCode !== Constants.FlowCodeConstants.WITHDRAW) || (this.flowStatusCode === Constants.FlowCodeConstants.WITHDRAW && this.hasProcess));
  }

  saveHandle() {
    const dataHandle = this.consultationScreenComponent?.saveHandle();
    if (this.consultationScreenComponent?.formReason?.valid) {
      this.service.saveHandle(this.proposeDetailId, dataHandle).subscribe(
        () => {
          this.isLoading = false;
          this.loadAll();
          this.toastrCustom.success(
            this.translate.instant('development.commonMessage.success')
          );
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        }
      );
    }
  }

  saveTGCV() {
    this.isScreenHandle ? this.saveHandle() : this.submit();
  }

  checkIdeaApproveKSPTNL() {
    const list = [Constants.ProposeCategory.ELEMENT, Constants.ProposeCategory.GROUP];
    return (
      this.screenCode === this.screenCodeConst.KSPTNL &&
      this.flowStatusCode === Constants.FlowCodeConstants.CHODUYET7 &&
      list.indexOf(this.proposeCategory) > -1
    );
  }

  otherIdeaElementKSPTNL() {
    const note = this.consultationScreenComponent?.noteKSPTNL || '';
    if (!note) {
      this.scrollToValidationSection('idea', 'development.commonMessage.approveIdeaRequired');
      return;
    } else {
      this.modalRef = this.modal.confirm({
        nzTitle: this.translate.instant('development.approvement-propose.messages.otherIdeaElementConfirm'),
        nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
        nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
        nzClassName: Constants.ClassName.LD_CONFIRM,
        nzOnOk: () => {
          this.isLoading = true;
          this.service.otherIdeaElementKSPTNL(this.idGroup, this.proposeDetailId, note).subscribe(
              () => {
                this.toastrCustom.success(this.translate.instant('development.approvement-propose.messages.otherIdeaSuccess'));
                this.back();
                this.isLoading = false;
              },
              (e) => {
                this.getMessageError(e);
                this.isLoading = false;
              }
            );
        },
      });
    }
  }

  //trả lại đề xuất
  checkReturnProposeButton() {
    return Constants.ReturnProposeStatus.indexOf(this.flowStatusCode) > -1
      && this.proposeUnitForm.get('proposeUnitRequestName')?.value
      && this.screenCode === Constants.ScreenCode.PTNL
      && this.typeAction === this.screenTypeConst.Detail;
  }

  showModalReturnPropose() {
    this.modalRef = this.modal.create({
      nzTitle: this.translate.instant('development.proposed-unit.section.returnPropose'),
      nzWidth: '75vw',
      nzContent: ReturnProposeComponent,
      nzComponentParams: {
        proposeDetailId: this.proposeDetailId,
      },
      nzFooter: null,
    });
    this.modalRef.componentInstance.closeEvent.subscribe((data:boolean) => {
      if (data) {
        this.isLoading = true;
        this.modalRef?.componentInstance.returnPropose.subscribe((body: ReturnPropose) => {
          const sub = this.service.returnPropose(body).subscribe(() => {
            this.toastrCustom.success(this.translate.instant(Constants.KeyCommons.MESSAGES_SUCCESS2));
            this.router.navigateByUrl(Utils.getUrlByFunctionCode(this.functionCode));
            this.isLoading = false;
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          });
          this.subs.push(sub);
          this.modalRef?.destroy();
        })
      } else {
        this.modalRef?.destroy();
      }
    });
  }

  checkBtnWithdrawPTNL() {
    return (this.proposeCategory === Constants.ProposeCategory.ELEMENT)
      && Constants.ChangeStatusWithdraw.indexOf(this.flowStatusCode) === -1
      && (this.withdrawNote === Constants.withdrawNoteCode.Note3|| this.withdrawNote === Constants.withdrawNoteCode.Note4)
      && (this.screenCode === this.screenCodeConst.PTNL);
  }


  //duyệt rút đề xuất thành phần
  agreeWithdrawProposalDetail(ideaType: number) {
    if (!this.comment && ideaType === Constants.WithdrawConfirmIdeaType.REJECT) {
      this.scrollToValidationSection('confirmWithdraw', 'development.commonMessage.approveIdeaRequired');
      return;
    }
    const ids: number[] = [];
    this.proposeDTO?.proposeDetails?.forEach((item) => {
      if (item.withdrawNote === Constants.withdrawNoteCode.Note3 || item.withdrawNote === Constants.withdrawNoteCode.Note4) {
        ids.push(item.id);
      }
    });
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.proposed-unit.messages.agreeLeader2'),
      nzCancelText: this.translate.instant('development.proposed-unit.button.cancelText'),
      nzOkText: this.translate.instant('development.proposed-unit.button.okText'),
      nzClassName: 'ld-confirm',
      nzOnOk: () => {
        this.isLoading = true;
        this.service.approveWithdrawProposeDetail(ids, this.comment, ideaType, this.screenCode).subscribe(
          () => {
            this.toastrCustom.success(this.translate.instant(Constants.KeyCommons.MESSAGES_SUCCESS2));
            this.router.navigateByUrl('development/propose/propose-list');
            this.isLoading = false;
            this.modalRef?.destroy();
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
        this.modalRef?.destroy();
      }
    });
  }

  //validate các trường khi bấm gửi duyệt và ghi nhận đồng ý của HRDV, lưu của PTNL
  validateSubmitForm() {
    const proposeDetail = { ...cleanDataForm(this.proposeDetailForm) };
    proposeDetail.employee = { ...cleanDataForm(this.employeeForm) };
    const rotation = this.rotations.find((item) => item.formRotation);
    if (!rotation) {
      this.scrollToValidationSection(
        'formRotation',
        'development.commonMessage.validateFormRotationTGCV'
      );
      return false;
    }
    this.isSubmitProposeDetail = true;
    if (this.proposeDetailForm.invalid) {
      let el = document.getElementById('formRotation');
      el?.scrollIntoView({
        behavior: 'smooth',
        block: 'start',
        inline: 'nearest',
      });
      return false;
    }
    const minTime = +this.proposeDetailForm.get('minimumTime')?.value;
    const maxTime = +this.proposeDetailForm.get('maximumTime')?.value;
    const timeType = this.proposeDetailForm.get('timeType')?.value;
    //nếu thời gian tối thiểu và thời gian tối đa không disable thì mới validate 2 trường này
    if (this.showMinTime && this.showMaxTime) {
      if (timeType === Constants.TypeMaximumValue.MONTH) {
        if (minTime >= maxTime) {
          this.scrollToValidationSection(
            'formRotation',
            'development.promulgate.message.validateMinMaxTime'
          );
          return false;
        }
      } else if (timeType === Constants.TypeMaximumValue.YEAR) {
        if (minTime >= maxTime * 12) {
          this.scrollToValidationSection(
            'formRotation',
            'development.promulgate.message.validateMinMaxTime'
          );
          return false;
        }
      }
    }
    if (this.proposeCommentForm.invalid) {
      this.scrollToValidationSection(
        'commentForm',
        'development.propose-tgcv.validateCommentForm'
      );
      return false;
    }
    if (this.listAttachFile?.length === 0) {
      this.scrollToValidationSection(
        'attachFileTable',
        'development.propose-tgcv.validateAttachFile'
      );
      return false;
    }
    if (!this.attachDocumentForm.get('personalReview')?.value &&
      !this.attachDocumentForm.get('reportBranchMeeting')?.value &&
      !this.attachDocumentForm.get('consultationComment')?.value &&
      !this.attachDocumentForm.get('different')?.value) {
        this.scrollToValidationSection(
          'attachFileTable',
          'development.commonMessage.validateFileCheckbox'
        );
        return false;
      }
    return true;
  }

  //body lưu đề xuất
  payloadSubmitForm(isSave: boolean) {
    const proposeDetail = { ...cleanDataForm(this.proposeDetailForm) };
    proposeDetail.employee = { ...cleanDataForm(this.employeeForm) };
    delete proposeDetail.employee.partyDate;
    const rotation = this.rotations.find((item) => item.formRotation);
    proposeDetail.id = this.dataForm?.id;
    proposeDetail.totalPoint = this.sumPoint;
    proposeDetail.totalDensity = this.sumProposePerformanceResults;
    proposeDetail.formGroup = +this.proposeUnitForm.get('formGroup')?.value;
    if (rotation?.formRotation) {
      proposeDetail.formRotation = +rotation?.formRotation;
    }
    proposeDetail.formRotationType = this.dataForm?.formRotationType;
    proposeDetail.formRotationCode = this.dataForm?.formRotationCode;
    proposeDetail.contentTitleCode = rotation?.contentTitleCode;
    proposeDetail.contentTitleName = this.dataCommon.jobDTOS?.find(
      (item) => item.jobId === proposeDetail.contentTitleCode
    )?.jobName;
    let contentUnit: OrgItem | null;
    if (rotation?.formRotation === Constants.FormRotationTitle.SAME_TITLE) {
        contentUnit = this.listInUnit?.find(
          (item) => item?.value === rotation.contentUnitCode
        )?.valueItem ?? null;
    } else {
      contentUnit = rotation?.contentUnit ?? null;
    }
    if (contentUnit) {
      proposeDetail.contentUnitCode = contentUnit?.orgId?.toString();
      proposeDetail.contentUnitName = contentUnit?.orgName;
      proposeDetail.contentUnitPathId = contentUnit?.pathId;
      proposeDetail.contentUnitPathName = contentUnit?.pathResult;
      proposeDetail.contentUnitTreeLevel = contentUnit?.orgLevel;
    }

    proposeDetail.contentLevel = rotation?.level;
    proposeDetail.implementDate =
      this.proposeDetailForm.get('implementDate')?.value;
    proposeDetail.sortOrder = this.sortOrder;
    proposeDetail.proposeComment = {
      ...cleanDataForm(this.proposeCommentForm),
    };
    proposeDetail.proposePerformanceResults = [
      ...this.proposePerformanceResults,
    ];
    if (
      this.proposePerformanceResults.length === 1 &&
      !this.proposePerformanceResults[0].targetAssigned
    ) {
      delete proposeDetail.proposePerformanceResults;
    }
    proposeDetail.isGroup = this.proposeDetailForm.get('isGroup')?.value;
    proposeDetail.groupByKSPTNL =
      this.proposeDetailForm.get('groupByKSPTNL')?.value;
    proposeDetail.flowStatusCode = this.flowStatusCode;
    proposeDetail.externalApprove = this.displayReport;
    this.attachDocumentForm.patchValue({
      statement: this.displayReport,
    });
    proposeDetail.proposeAttachmentFiles = {
      ...cleanDataForm(this.attachDocumentForm),
    };
    proposeDetail.sameDecision = this.proposeDetailForm.get('sameDecision')?.value;
    proposeDetail.isIncome = this.proposeDetailForm.get('isIncome')?.value;
    proposeDetail.isInsurance = this.proposeDetailForm.get('isInsurance')?.value;
    proposeDetail.isDecision = this.proposeDetailForm.get('isDecision')?.value;
    proposeDetail.issuedLater = this.proposeDetailForm.get('issuedLater')?.value;

    //xử lý ProposeDTO
    const data = { ...cleanDataForm(this.proposeUnitForm) };
    data.proposeUnitRequest =
      this.proposeUnitForm.get('proposeUnitRequest')?.value;
    data.proposeUnitRequestName = this.proposeUnitForm.get(
      'proposeUnitRequestName'
    )?.value;
    data.relationType = false;
    data.sentDate = this.proposeDTO?.sentDate;
    data.formGroup = this.proposeUnitForm.get('formGroup')?.value;
    data.isGroup = this.proposeUnitForm.get('isGroup')?.value;
    data.groupByKSPTNL = this.proposeUnitForm.get('groupByKSPTNL')?.value;
    data.flowStatusCode = this.proposeUnitForm.get('flowStatusCode')?.value;
    data.flowStatusPublishCode = this.statusPublishCode;
    data.externalApprove = this.proposeDTO?.externalApprove;
    data.proposeDetails = [proposeDetail];
    data.approveNote = this.checkDisplayReportEdit()
      ? this.formReportElement.get('approveNote')?.value
      : this.formTTQD.get('approveNote')?.value;
    delete data.proposeDecision;
    delete data.proposeStatement;
    data.id = this.proposeDTO?.id;
    delete data.proposeLogs;
    return isSave ? data : [data];
  }

  //check ý kiến phê duyệt LDDV ban đầu
  checkApproveNoteOrigin() {
    return !this.displayReport
      && !this.checkDisplayReportEdit()
      && !this.dataForm?.hasAgreeUnit
      && Constants.StatusApproveSendProposeMany.includes(this.flowStatusCode);
  }

  //check hiển thị ý kiến phê duyệt của LDDV
  checkApproveNote() {
    return !this.displayReport
      && ((this.screenCode === Constants.ScreenCode.HRDV && this.typeAction === this.screenType.update)
        || (this.screenCode === Constants.ScreenCode.HRDV && this.typeAction === this.screenType.detail
        && this.formReportElement.get('approveNote')?.value))
      && this.flowStatusCode !== this.flowCodeConst.HANDLING5B
      && this.flowStatusCode !== this.flowCodeConst.CANCELPROPOSE;
  }

  disabledEndDateSubmission = (endDate: Date): boolean => {
    return endDate >=  new Date();
  };

  getLog(pageIndex: number){
    this.pageNumber = pageIndex - 1;
    this.isLoading = true;
    const idLog = this.proposeCategory === Constants.ProposeCategory.ELEMENT
        ? +this.proposeDetailId
        : +this.proposeIdOriginal;
      const log = this.service.getLogsPage(idLog, this.proposeCategory, this.screenCode, this.pageNumber, this.pageSize)
      .subscribe((logs) => {
        this.proposeLogs = logs?.data?.content || [];
        this.proposeLogs = this.proposeLogs?.map((item) => {
          return {
            ...item,
            actionDate: moment(item.actionDate).format(Constants.FORMAT_DATE.DD_MM_YYYY_HH_MM),
          }
        });
        this.tableLogs.total = logs?.data?.totalElements || 0;
        this.tableLogs.pageIndex = pageIndex;
        this.isLoading = false;
      }, (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      })
    this.subs.push(log);
  }

  //tờ trình chỉnh sửa
  checkDisplayReportEdit() {
    return !this.displayReport &&
        (
          ((Constants.StatusApproveSendProposeMany.includes(this.flowStatusCode) && this.dataForm?.hasAgreeUnit)
          && this.screenCode === Constants.ScreenCode.HRDV
          && this.typeAction === this.screenType.update) ||
          this.checkDataReportEdit()
        );
  }

  //check xem tờ trình chỉnh sửa có dữ liệu không
  checkDataReportEdit() {
    return this.formReportElement.get('statementNumber')?.value
      || this.formReportElement.get('submissionDate')?.value
      || this.formReportElement.get('approveNote')?.value
      || this.fileListSelectTTElement.length === 1;
  }

  //validate điền thông tin của tờ trình chỉnh sửa
  validApproveEditReport() {
    return (
      this.formReportElement.get('statementNumber')?.value &&
      this.formReportElement.get('submissionDate')?.value &&
      this.formReportElement.get('approveNote')?.value &&
      this.fileListSelectTTElement.length === 1
    );
  }

  //check view đính kèm tờ trình chỉnh sửa của đơn vị
  checkViewFileElement() {
    return (this.screenCode === Constants.ScreenCode.HRDV && (this.fileElement?.uid && this.fileElement?.fileName)
      && this.typeAction === this.screenType.detail)
      || ((this.screenCode === Constants.ScreenCode.PTNL || this.screenCode === Constants.ScreenCode.KSPTNL)
        && this.checkDataReportEdit());
  }

  //upload file tờ trình chỉnh sửa
  beforeUploadTT = (file: File | NzUploadFile): boolean => {
    this.isLoading = true;
    const formData = new FormData();
    formData.append('files', file as File);
    formData.append('proposeDetailId', new Blob([JSON.stringify(this.proposeIdOriginal)], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('type', new Blob([Constants.TypeStatementFile.STATEMENTEDIT], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('groupProposeTypeId', new Blob(['1'], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('isIn', new Blob([JSON.stringify(this.displayReport)], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    this.service.uploadFile(formData).subscribe(
      (res) => {
        this.fileListSelectTTElement = [
          ...this.fileListSelectTTElement,
          {
            uid: res.data?.docId,
            name: res.data?.fileName,
            status: 'done',
            id: res.data?.id,
          },
        ];
        this.validApproveEditReport();
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    return false;
  };

  //xóa file tờ trình chỉnh sửa
  doRemoveFileTT = (file: NzUploadFile): boolean => {
    this.isLoading = true;
    if (this.fileListSelectTTElement.find((item) => item.uid === file.uid)){
      const idDelete = this.fileListSelectTTElement.find((item) => item.uid === file.uid)?.['id'];
      this.service.deleteUploadFile(+idDelete).subscribe(
        (res) => {
          this.fileListSelectTTElement = this.fileListSelectTTElement.filter((item) => item['id'] !== idDelete);
          this.validApproveEditReport();
          this.isLoading = false;
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        }
      );
    }
    return false;
  };

  downloadFile2(uid: string | number, name: string) {
    this.isLoading = true;
    this.service.downloadFile(+uid, name).subscribe(
      () => {
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    )
  }

  //view tờ trình từ đơn vị gửi lên
  viewReportHRDV() {
    if (Constants.ListScreenCodeUnit.includes(this.screenCode)) {
      return !this.displayReport;
    } else if (Constants.ListScreenCodePTNL.includes(this.screenCode)) {
      return !this.displayReport && this.proposeDTO?.proposeUnitRequestName;
    }
    return false;
  }
}
