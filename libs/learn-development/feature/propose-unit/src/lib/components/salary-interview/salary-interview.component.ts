import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {cleanData, cleanDataForm} from "@hcm-mfe/shared/common/utils";


@Component({
  selector: 'app-salary-interview',
  templateUrl: './salary-interview.component.html',
  styleUrls: ['./salary-interview.component.scss'],
})
export class SalaryInterviewComponent extends BaseComponent implements OnInit, OnChanges {
  @Output() submitSalary: EventEmitter<any> = new EventEmitter<any>();
  @Input() data: any;

  interviewForm = this.fb.group({
    interviewStt: null,
    interviewResult: null,
    interviewDate: null,
    interviewSentResultDate: null,
    note: null,
  });

  interviewResults = [
    { label: 'Đạt', value: '1' },
    { label: 'Cân nhắc', value: '2' },
    { label: 'Không đạt', value: '3' },
  ];

  submit() {
    const newForm = { ...cleanDataForm(this.interviewForm) };
    this.submitSalary.emit(cleanData(newForm));
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.data) {
      this.interviewForm.patchValue(this.data);
    }
  }

  emitData() {
    const newForm = { ...cleanDataForm(this.interviewForm) };
    return newForm;
  }
}
