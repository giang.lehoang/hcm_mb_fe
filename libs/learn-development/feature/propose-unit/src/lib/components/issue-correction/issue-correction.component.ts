import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Constants } from '@hcm-mfe/learn-development/data-access/common';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import {
  DateModelIssueCorrection,
  IssueCorrection,
} from '@hcm-mfe/learn-development/data-access/models';

@Component({
  selector: 'app-issue-correction',
  templateUrl: './issue-correction.component.html',
  styleUrls: ['./issue-correction.component.scss'],
})
export class IssueCorrectionComponent extends BaseComponent implements OnInit {
  @Input() proposeCode: string = '';
  @Input() fullName: string = '';
  @Input() proposeId: number = 0;
  @Input() proposeDetailId: number = 0;
  @Input() formRotationType = '';
  @Input() commonDecision: boolean = false;
  @Input() oldEffectiveDate: Date | string | null = '';
  @Input() oldEndDate: Date | string | null = '';
  @Input() oldMinTime: number | null = null;
  @Input() oldMaxTime: number | null = null;
  @Output() closeEvent = new EventEmitter<boolean>();
  @Output() issueCorrectionBody = new EventEmitter<IssueCorrection>();

  labelMessage = '';
  listTypeMaximumTime = Constants.TypeMaximumTime;
  listPromulgate = Constants.ListPromulgate;
  rotationName = Constants.RotationName;

  issueCorrectionModel: DateModelIssueCorrection = {
    effectiveDate: null,
    sameDecision: false,
    minimumTime: null,
    maximumTime: null,
    typeMaximumTime: null,
    endDate: null,
    reason: null,
    detailReason: null,
  };

  ngOnInit() {
    this.labelMessage = this.translate.instant(
      'development.commonMessage.labelIssue',
      {
        proposeCode: this.proposeCode,
        fullName: this.fullName,
      }
    );
    this.issueCorrectionModel.effectiveDate = this.oldEffectiveDate || '';
    this.issueCorrectionModel.minimumTime = this.oldMinTime;
    this.issueCorrectionModel.maximumTime = this.oldMaxTime;
    this.issueCorrectionModel.endDate = this.oldEndDate || '';
    this.issueCorrectionModel.typeMaximumTime =
      Constants.TypeMaximumTime[0].type;
  }

  submit() {
    //check ngày hiệu lực chung riêng
    if (!this.commonDecision) {
      if (
        !this.issueCorrectionModel.effectiveDate &&
        !this.issueCorrectionModel.sameDecision
      ) {
        this.toastrCustom.error(
          this.translate.instant(
            'development.promulgate.message.effectiveDateRequired'
          )
        );
        return;
      }
    } else {
      if (!this.issueCorrectionModel.effectiveDate) {
        this.toastrCustom.error(
          this.translate.instant(
            'development.promulgate.message.effectiveDateRequired2'
          )
        );
        return;
      }
    }
    //check thời gian tối thiểu và tối đa
    if (this.formRotationType === Constants.RotationName.DD) {
      if (
        !this.issueCorrectionModel.minimumTime ||
        !this.issueCorrectionModel.maximumTime
      ) {
        this.toastrCustom.error(
          this.translate.instant(
            'development.promulgate.message.validateMinMaxTime2'
          )
        );
        return;
      }
    }
    if (
      this.issueCorrectionModel.minimumTime &&
      this.issueCorrectionModel.maximumTime
    ) {
      if (
        this.issueCorrectionModel.typeMaximumTime ===
        Constants.TypeMaximumValue.MONTH
      ) {
        if (
          +this.issueCorrectionModel.minimumTime >=
          +this.issueCorrectionModel.maximumTime
        ) {
          this.toastrCustom.error(
            this.translate.instant(
              'development.promulgate.message.validateMinMaxTime'
            )
          );
          return;
        }
      } else if (
        this.issueCorrectionModel.typeMaximumTime ===
        Constants.TypeMaximumValue.YEAR
      ) {
        if (
          +this.issueCorrectionModel.minimumTime >=
          +this.issueCorrectionModel.maximumTime * 12
        ) {
          this.toastrCustom.error(
            this.translate.instant(
              'development.promulgate.message.validateMinMaxTime'
            )
          );
          return;
        }
      }
    }
    //validate lý do và chi tiết lý do hiệu chỉnh
    if (
      this.issueCorrectionModel.reason === null ||
      !this.issueCorrectionModel.detailReason
    ) {
      this.toastrCustom.error(
        this.translate.instant(
          'development.promulgate.message.validateReasonAndDetailCorrection'
        )
      );
      return;
    }
    const body = {
      effectiveDate: this.issueCorrectionModel.effectiveDate,
      effectiveDateOld: this.oldEffectiveDate,
      sameDecision: this.issueCorrectionModel.sameDecision,
      minimumTime: this.issueCorrectionModel.minimumTime,
      maximumTime: this.issueCorrectionModel.maximumTime,
      issueEffectiveEndDate1: this.issueCorrectionModel.endDate,
      reasonOrCorrection: this.issueCorrectionModel.reason,
      viewDetailReasonOrCorrection: this.issueCorrectionModel.detailReason,
    };
    this.closeEvent.emit(true);
    this.issueCorrectionBody.emit(body);
  }

  closeModal() {
    this.closeEvent.emit(false);
  }

  checkCommonEffectiveDate() {
    if (this.issueCorrectionModel.sameDecision) {
      this.issueCorrectionModel.effectiveDate = null;
    }
  }

  //tính thời gian tối đa
  calculateMaxTime() {
    if (
      this.issueCorrectionModel.effectiveDate &&
      this.issueCorrectionModel.endDate
    ) {
      const rangeDate =
        (new Date(this.issueCorrectionModel.endDate).getTime() -
          new Date(this.issueCorrectionModel.effectiveDate).getTime()) /
        (1000 * 3600 * 24);
      if (rangeDate < 0) {
        this.toastrCustom.warning(
          this.translate.instant(
            'development.promulgate.message.validateEffectiveEndDate'
          )
        );
        this.issueCorrectionModel.maximumTime = null;
        return;
      }
      if (
        this.issueCorrectionModel.typeMaximumTime ===
        Constants.TypeMaximumValue.MONTH
      ) {
        this.issueCorrectionModel.maximumTime = Math.round(rangeDate / 30);
      } else if (
        this.issueCorrectionModel.typeMaximumTime ===
        Constants.TypeMaximumValue.YEAR
      ) {
        this.issueCorrectionModel.maximumTime = Math.round(rangeDate / 30 / 12);
      }
    }
  }

  //tính ngày kết thúc
  calculateEndDate() {
    let rangeTime = 0;
    if (this.issueCorrectionModel.maximumTime) {
      if (
        this.issueCorrectionModel.typeMaximumTime ===
        Constants.TypeMaximumValue.MONTH
      ) {
        rangeTime = +this.issueCorrectionModel.maximumTime;
      } else if (
        this.issueCorrectionModel.typeMaximumTime ===
        Constants.TypeMaximumValue.YEAR
      ) {
        rangeTime = +this.issueCorrectionModel.maximumTime * 12;
      }
    }
    //tính ngày kết thúc
    if (this.issueCorrectionModel.effectiveDate && rangeTime > 0) {
      const oldDate = new Date(this.issueCorrectionModel.effectiveDate);
      const date1 = oldDate.getDate();
      const month1 = oldDate.getMonth() + 1;
      let year1 = oldDate.getFullYear();
      let newMonth = 0;
      newMonth = month1 + rangeTime;
      if (rangeTime <= 12) {
        if (newMonth > 12) {
          year1 = year1 + 1;
          newMonth = newMonth - 12;
        }
      } else {
        const yearBonus = Math.floor(rangeTime / 12);
        const monthBalance = rangeTime - yearBonus * 12;
        newMonth = month1 + monthBalance;
        if (rangeTime > 12) {
          year1 = year1 + yearBonus;
          if (newMonth > 12) {
            newMonth = newMonth - 12;
            year1 = year1 + 1;
          }
        } else {
          year1 = year1 + 1;
        }
      }
      const newDate = new Date();
      newDate.setDate(date1);
      newDate.setMonth(newMonth - 1);
      newDate.setFullYear(year1);
      this.issueCorrectionModel.endDate = newDate;
    }
  }
}
