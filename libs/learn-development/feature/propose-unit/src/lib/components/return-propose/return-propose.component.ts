import {
  Component,
  EventEmitter,
  Injector,
  Input,
  OnInit,
  Output, TemplateRef, ViewChild
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Constants } from '@hcm-mfe/learn-development/data-access/common';
import { EmployeeInfoMultiple, ReturnPropose } from '@hcm-mfe/learn-development/data-access/models';
import { ProposedUnitService } from "@hcm-mfe/learn-development/data-access/services";
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { MBTableConfig } from '@hcm-mfe/shared/data-access/models';
import { NzSafeAny } from 'ng-zorro-antd/core/types';

@Component({
  selector: 'app-return-propose',
  templateUrl: './return-propose.component.html',
  styleUrls: ['./return-propose.component.scss'],
})
export class ReturnProposeComponent extends BaseComponent implements OnInit {
  form: FormGroup;
  tableConfig!: MBTableConfig;
  stopInterviewTable: EmployeeInfoMultiple[] = [];
  @Input() proposeDetailId: number = 0;
  @Output() closeEvent = new EventEmitter<boolean>();
  @Output() returnPropose = new EventEmitter<ReturnPropose>();
  heightTable = { x: '100%', y: '25em'};
  @ViewChild('checkboxTmpl', {static: true}) checkbox!: TemplateRef<NzSafeAny>;
  @ViewChild('currentTitleTmpl', {static: true}) currentTitle!: TemplateRef<NzSafeAny>;
  @ViewChild('fullNameTmpl', {static: true}) fullName!: TemplateRef<NzSafeAny>;
  
  checked = false;
  quantityMap: Map<number, EmployeeInfoMultiple> = new Map<number, EmployeeInfoMultiple>();
  proposeDetailIds: number[] = [];
  isSubmitted = false;

  constructor(injector: Injector, private service: ProposedUnitService) {
    super(injector);
    this.form = new FormGroup({
      reason: new FormControl(null, [Validators.required]),
    });
  }

  ngOnInit() {
    this.initTable();
    this.isLoading = true;
    this.service.getListReturnPropose(this.proposeDetailId).subscribe((res) => {
      this.stopInterviewTable = res?.data?.proposeDetails;
      this.isLoading = false;
    }, 
    (e) => {
      this.getMessageError(e);
      this.isLoading = false;
    })
  }

  submit() {
    this.isSubmitted = true;
    if (this.form.valid) {
      this.proposeDetailIds.push(this.proposeDetailId);
      const body = {
        proposeDetailIds: this.proposeDetailIds,
        comment: this.form.get('reason')?.value,
      }
      this.closeEvent.emit(true);
      this.returnPropose.emit(body);
    }
  }

  closeModal() {
    this.closeEvent.emit(false);
  }

  updateCheckedSet(item: EmployeeInfoMultiple, checked: boolean): void {
    if (checked) {
      this.quantityMap.set(item.id!, item);
    } else {
      this.quantityMap.delete(item.id!);
    }
    this.getListOfIDSelected();
  }

  onItemChecked(id: number, checked: boolean): void {
    const item = this.stopInterviewTable.find((item: EmployeeInfoMultiple) => item.id === id);
    this.updateCheckedSet(item!, checked);
    this.refreshCheckedStatus();
  }

  onAllChecked(value: boolean): void {
    this.stopInterviewTable.forEach((item) => this.updateCheckedSet(item, value));
    this.refreshCheckedStatus();
  }

  refreshCheckedStatus(): void {
    this.checked = this.stopInterviewTable.every(({ id }) => this.quantityMap.has(id!));
  }

  getListOfIDSelected() {
    this.proposeDetailIds = [];
    this.quantityMap.forEach((values) => {
      this.proposeDetailIds.push(values.id!);
    });
    return this.proposeDetailIds;
  }

  initTable() {
    this.tableConfig = {
      headers: [
        {
          title: '',
          field: '',
          width: 50,
          thClassList: [Constants.TABLE_CONFIG.TEXT_NOWRAP, Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [Constants.TABLE_CONFIG.TEXT_NOWRAP, Constants.TABLE_CONFIG.TEXT_CENTER],
          tdTemplate: this.checkbox,
        },
        {
          title: 'development.promulgate.search.proposeCode',
          field: 'proposeCode',
          width: 100,
          thClassList: [Constants.TABLE_CONFIG.TEXT_NOWRAP, Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [Constants.TABLE_CONFIG.TEXT_NOWRAP, Constants.TABLE_CONFIG.TEXT_CENTER],
        },
        {
          title: 'development.proposed-unit.table.empCode',
          field: 'employeeCode',
          width: 100,
          thClassList: [Constants.TABLE_CONFIG.TEXT_NOWRAP, Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [Constants.TABLE_CONFIG.TEXT_NOWRAP, Constants.TABLE_CONFIG.TEXT_CENTER],
        },
        {
          title: 'development.proposed-unit.table.empName',
          field: 'fullName',
          width: 140,
          tdTemplate: this.fullName
        },
        {
          title: 'development.proposed-unit.table.currentUnit',
          field: 'employeeUnitPathName',
          width: 170,
        },
        {
          title: 'development.proposed-unit.table.currentTitle',
          field: '',
          width: 170,
          tdTemplate: this.currentTitle
        },
        {
          title: 'development.proposed-unit.table.proposeUnit',
          field: 'contentUnitPathName',
          width: 170,
        },
        {
          title: 'development.proposed-unit.table.proposeTitle',
          field: 'contentTitleName',
          width: 170,
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.stopInterviewTable.length,
      pageIndex: 1,
    };
  }

  getFullName(proposeCode: string) {
    const item = this.stopInterviewTable.find((item) => item.proposeCode === proposeCode);
    return item?.employee?.fullName;
  }

  getCurrentTitle(proposeCode: string) {
    const item = this.stopInterviewTable.find((item) => item.proposeCode === proposeCode);
    return item?.employee?.jobName;
  }
}
