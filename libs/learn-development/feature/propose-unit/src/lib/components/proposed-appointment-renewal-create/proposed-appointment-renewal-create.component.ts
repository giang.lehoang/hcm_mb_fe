import { Component, Injector, Input, OnInit, TemplateRef, ViewChild, HostBinding } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NavigationExtras } from '@angular/router';
import {
  Constants,
  maxInt32,
  Scopes,
  ScreenType,
  SessionKey,
  userConfig,
  Utils
} from "@hcm-mfe/learn-development/data-access/common";
import {
  DataCommon,
  EmployeeAppointment,
  EmployeeModal,
  EmployeeTraining, IssueLevelList,
  JobDTOS,
  ListFileTemplate,
  ListOrgItem, OrgEmployee, OrgItem,
  OrientationJob,
  ParamSearchPosition,
  ProposeAlternative,
  ProposeAlternativeRenewal,
  Proposed,
  ProposedError,
  ProposeDTOTGCV,
  ProposeLogs,
  ProposePerformanceResults,
  ProposeRotationReasons,
  RotationReasons, StatusList, TitleUnit, UnitFeedbackRenewal,
  WithdrawPropose,
  DataFormTGCV,
  DataMessageModel,
  ReturnPropose,
  ProposedAppointmentRenewal,
  ProposeStatementDTO,
  AttachFile,
  FileDTOList
} from "@hcm-mfe/learn-development/data-access/models";
import { ProposedAppointmentRenewalService, ProposedUnitService, ProposeMultipleEmployeeService } from "@hcm-mfe/learn-development/data-access/services";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import { cleanDataForm } from '@hcm-mfe/shared/common/utils';
import {MBTableConfig, Pagination, ScrollTabOption} from '@hcm-mfe/shared/data-access/models';
import { User } from "@hcm-mfe/system/data-access/models";
import * as FileSaver from 'file-saver';
import * as moment from 'moment';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { NzUploadFile } from "ng-zorro-antd/upload";
import { catchError, forkJoin, Observable, of, Subscription } from 'rxjs';
import { ConsultationScreenComponent } from '../consultation-screen/consultation-screen.component';
import { ProposeMultipleEmployeeComponent } from '../propose-multiple-employee/propose-multiple-employee.component';
import { ProposedAppointmentRenewalEmpComponent } from '../proposed-appointment-renewal-emp/proposed-appointment-renewal-emp.component';
import { WithdrawProposeComponent } from '../withdraw-propose/withdraw-propose.component';
import {InputBoolean} from 'ng-zorro-antd/core/util';
import { ReturnProposeComponent } from '../return-propose/return-propose.component';
import {NzSafeAny} from "ng-zorro-antd/core/types";



@Component({
  selector: 'app-proposed-appointment-renewal-create',
  templateUrl: './proposed-appointment-renewal-create.component.html',
  styleUrls: ['./proposed-appointment-renewal-create.component.scss'],
})
export class ProposedAppointmentRenewalCreateComponent extends BaseComponent implements OnInit {
  @Input() dataCommon: DataCommon = {
    jobDTOS: [], // vị trí
    typeDTOS: [], // loại đề xuất
    formDisplaySearchDTOS: [], // hình thức
    proposeRotationReasonConfigDTOS: [], // lý do luân chuyển
    statusList: [],
    flowStatusApproveConfigList: [],
    flowStatusPublishConfigList: [],
    formGroupList: [],
    formGroupMultipleList: [],
    conditionViolationList: [],
    employeeTrainingList: [],
    unitFeedbackList: [],
    unitFeedbackConfigList: [],
    issueLevelList: <IssueLevelList[]>[],
    alternativePlanConcurrentList: [],
    alternativePlanSpecializeList: [],
    timeTypeEnums: [],
  };
  @Input() typeAction = '';
  @Input() screenCode = '';

  @Input() formGroupCreate: number | null = null;
  @Input() proposeUnit?: Proposed;

  rotationReasons: RotationReasons[] = [];
  sumProposePerformanceResults = 0;
  currentSumPoint = 0;
  sumPoint = 0;

  modalRef?: NzModalRef;
  formRotationType = Constants.FormRotationType;


  formTypeConst = Constants.FormGroupList;
  // isChangeLevel = false;
  // sortOrder = 0;
  // getReportMany = false;
  subs: Subscription[] = [];
  @Input() proposeId = 0;
  proposeDetailId = 0;
  listCommentsUnitCharge : UnitFeedbackRenewal[] = [];
  proposeDTO: ProposeDTOTGCV | undefined;
  idGroup: number;



  proposeAlternative: ProposeAlternative[] = [
    {
      id: null,
      proposeDetailId: null,
      alternativeType: null,
      estimatedDate: null,
      replacementEmployeeName: null,
      replacementEmployeeCode: null,
      replacementEmployeeTitleCode: null,
      replacementEmployeeTitleName: null,
      handoverEmployeeCode: null,
      handoverEmployeeName: null,
      handoverEmployeeTitleCode: null,
      handoverEmployeeTitleName: null,
      replacementEmployee: null,
      handoverEmployee: null,
      disabled: true,
    },
  ];

  // thông tin nhân viên
  employeeForm = this.fb.group({
    empIdp: null,
    orgId: null,
    empId: null,
    empCode: null,
    fullName: null,
    email: null,
    posId: null,
    posName: null,
    jobId: null,
    jobName: null,
    orgPathId: null,
    orgPathName: null,
    orgName: null,
    majorLevelId: null,
    majorLevelName: null,
    faculityId: null,
    faculityName: null,
    schoolId: null,
    schoolName: null,
    positionLevel: null,
    gender: null,
    joinCompanyDate: null,
    dateOfBirth: null,
    partyDate: null,
    partyOfficialDate: null,
    posSeniority: null,
    seniority: null,
    qualificationEnglish: null,
    facultyName: null,
    semesterGradeT1: null,
    semesterRankT1: null,
    semesterGradeT2: null,
    semesterRankT2: null,
    ratedCapacity: null,
    nearestDiscipline: null,
    endOfDisciplinaryDuration: null,
    userToken: null,
    treeLevel: null,
    posSeniorityName: null,
    seniorityName: null,
    levelName: null,
    concurrentTitleName: null,
    concurrentUnitPathName: null,
    concurrentTitleName2: null,
    concurrentUnitPathName2: null,
    periodNameT1: null,
    periodNameT2: null,
  });

   // Thông tin đề xuất
   rotations: RotationReasons[] = [
    {
      formRotationLabel: 'Trong đơn vị',
      formRotation: null, // IN/OUT
      formRotationType: null, // DC/DD
      formRotationTitle: null,
      contentUnit: null,
      contentUnitCode: null,
      contentTitleCode: null,
      contentTitleName: null,
      contentUnitPathId: null,
      contentUnitPathName: null,
      contentUnitTreeLevel: null,
      disabled: false,
      level: null,
    },
    {
      formRotationLabel: 'Khác đơn vị',
      formRotation: null, // IN/OUT0
      formRotationType: null, // DC/DD
      formRotationTitle: null,
      contentUnit: null,
      contentUnitCode: null,
      contentTitleCode: null,
      contentTitleName: null,
      contentUnitPathId: null,
      contentUnitPathName: null,
      contentUnitTreeLevel: null,
      disabled: false,
      level: null,
    },
  ];

  // form proposeDetail
  //Ngày triển khai đề xuất
  proposeDetailForm = this.fb.group({
    implementDate: new FormControl(null, [Validators.required]),
    minimumTime: new FormControl(null, [Validators.required]),
    maximumTime: new FormControl(null, [Validators.required]),
    timeType: new FormControl(1, [Validators.required]),
    concurrentReason: new FormControl(null, [Validators.required]),

  });
  isSubmitted = false;
  // concurrentReason = '';
  // Định hướng mảng công việc
  orientationJob: OrientationJob[] = [];
  currentUnit: ProposeAlternativeRenewal[] = [];
  proposedUnit: ProposeAlternativeRenewal[] = [];
  employeeTraining: EmployeeTraining[] = [];
  proposePerformanceResults: ProposePerformanceResults[] = [];
  @Input() listInUnit?: ListOrgItem[];
  scope: string = Scopes.VIEW;
  contentTitleEmp?: string;
  differentUnits = '';
  inUnit = '';
  isSeeMore = false;
  listLevel = Constants.Level;
  planAlternative = Constants.PlanAlternative;
  typeEmpAlternative = Constants.TypeEmpAlternative
  functionCode = '';
  proposeCategory = '';

  proposeUnitForm = this.fb.group({
    id: null,
    flowStatusName: null,
    formGroup: null,
    withdrawNote: null,
    statusPublishName: null,
    proposeUnitRequestName: null,
    optionNote: null,

  });
  isExpand = false;
  iconStatus = Constants.IconStatus.DOWN;
  proposeLogs: ProposeLogs[] = [];
  issueLevelName = '';
  screenTypeConst = ScreenType;
  stateUrl?: NavigationExtras;
  isAcceptPropose = false;
  rotationTimeViewMin = false;
  rotationTimeViewMax = false;
  flowCodeConst = Constants.FlowCodeConstants;
  note = '';
  proposeType = 0;
  withdrawNoteCode = '';
  checkWithdrawType = false;
  comment = '';
  positionTypeEmp = Constants.PositionType;
  conditionViolation: string[] = [];
  constantsVoditionsCode = Constants.ViolationCode;
  isScreenHandle = false;
  @ViewChild('consultationScreenComponent') consultationScreenComponent?: ConsultationScreenComponent;
  @ViewChild('popUpError') modalContent?: TemplateRef<any>;
  listProposeCategory = Constants.ProposeCategory;
  // proposeUnitApprove = false;
  rotationList:  ProposeRotationReasons[] = [];
  rotationRemoveList: ProposeRotationReasons[] = [];
  screenCodeConst = Constants.ScreenCode;
  jobDTOSApi: JobDTOS[] = [];
  getReportFirst?: boolean = false;
  fileElement: AttachFile = {};
  fileListSelectTTElement: NzUploadFile[] = [];
  fileElementObj?: FileDTOList = {};

  formTTQD = new FormGroup({
    statementNumber: new FormControl(null, [Validators.required]),
    submissionDate: new FormControl(null, [Validators.required]),
    approveNote: new FormControl(null, Validators.required),

  });

  formReportElement = new FormGroup({
    statementNumber: new FormControl(null, [Validators.required]),
    submissionDate: new FormControl(null, [Validators.required]),
    approveNote: new FormControl(null, [Validators.required]),
    typeStatement: new FormControl(null),
  });


  isSubmitFormReportElement = false;
  displayReport = false;

  fileListCreate: NzUploadFile[] = [];
  fileListCreateNull: NzUploadFile[] = [];
  nameFileListCreate = '';
  // listTypeMaximumTime = Constants.TypeMaximumTime;

  strategicCapacity: UnitFeedbackRenewal[] = [];
  learningCapacity: UnitFeedbackRenewal[] = [];
  performanceCapacity: UnitFeedbackRenewal[] = [];

  personalReview = false;
  different = false;
  targetAppoint = false;
  reportBranchMeeting = false;
  consultationComment = false;
  statement = false;
  violationCode = 0;
  flowStatusCode = '';
  statusPublishCode = '';
  currentUser: User | undefined;
  scrollTabs = Constants.ScrollTabs;
  formRotationTitle = Constants.FormRotationTitle;
  withdrawNote = 0;
  messageListError?: DataMessageModel[];
  isDeletePTNL = false;
  @HostBinding('class.scroll__tab--sticky') @Input() @InputBoolean() sticky = true;
  //action click nút lưu
  isSubmitButton = false;
  //check hoàn thành validate
  validateComplete = false;
  hasProcess = false;

  //log
  heightTable = { x: '50vw', y: '16em'};
  tableConfig!: MBTableConfig;
  pagination = new Pagination();
  @ViewChild('dateLog', {static: true}) dateLog!: TemplateRef<NzSafeAny>;
  pageNumber = 0;
  pageSize =  userConfig.pageSize;


  constructor(injector: Injector, private readonly service: ProposedUnitService,
    private readonly proposeMultipleService: ProposeMultipleEmployeeService,
    private readonly proposedAppointmentRenewalService: ProposedAppointmentRenewalService,
    ) {
    super(injector);
    this.typeAction = this.route.snapshot.data['screenType'];
    this.functionCode = this.route.snapshot?.data['code'];
    this.proposeCategory = this.route.snapshot.queryParams['proposeCategory'];
    this.proposeDetailId = this.route.snapshot.queryParams['proposeDetailId'];
    this.stateUrl = this.router?.getCurrentNavigation()?.extras;
    this.proposeType = Utils.getGroupType(this.proposeCategory);
    this.withdrawNoteCode = this.route.snapshot.queryParams['withdrawNoteCode'];
    this.isScreenHandle = this.route.snapshot?.data['handlePropose'];
    this.flowStatusCode = this.route.snapshot.queryParams['flowStatusCode'];
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
    this.idGroup = this.route.snapshot.queryParams['idGroup'];
  }





  checkLeaderApprove(status: string) {
    return Constants.LeaderAppproveStatus.indexOf(status) > -1;
  }


  checkUrl() {
    const pageName = this.route.snapshot?.data['pageName'];
    if (Constants.PageNameHRDV.includes(pageName)) {
      this.screenCode = Constants.ScreenCode.HRDV;
    } else if (Constants.PageNamePTNL.includes(pageName)) {
      this.screenCode = Constants.ScreenCode.PTNL;
    } else if (Constants.PageNameLDDV.includes(pageName)) {
      this.screenCode = Constants.ScreenCode.LDDV;
    } else {
      this.screenCode = Constants.ScreenCode.KSPTNL;
    }
  }

  ngOnInit() {
    this.initTable();
    if (localStorage.getItem(SessionKey.CURRENCY_USER)) {
      this.currentUser = JSON.parse(localStorage.getItem(SessionKey.CURRENCY_USER)!);
    }
    this.checkUrl();
    // this.employeeForm.disable();
    // this.employeeForm.get('empCode')?.enable();

    if (this.typeAction !== ScreenType.Create) {
      if (this.typeAction === ScreenType.Detail || this.isScreenHandle) {
        this.proposeDetailForm.disable();
        this.formTTQD.disable();
      }

      this.isLoading = true;
      this.proposeId = +this.route.snapshot.queryParams['id'];
      this.getDataProposedSppointment();
    } else {
      this.getCommentsUnit();
      // this.rotationList = this.dataCommon.proposeRotationReasonConfigDTOS || [];
      this.addOrientationWork();
      this.addCurrentUnit();
      if (this.screenCode === Constants.ScreenCode.HRDV) {
        this.rotations.pop();
      }
    }
    this.addRotationReasons();
    this.addProposePerformanceResult();
    this.rotations[0].formRotation = this.screenCode === Constants.ScreenCode.HRDV ? Constants.FormRotationName.INUNIT.toString() : null;
  }




  getCommentsUnit() {
    if (this.dataCommon?.unitFeedbackList && this.dataCommon?.unitFeedbackConfigList) {
      const unitFeedbackList = this.dataCommon?.unitFeedbackList || [];
      const unitFeedbackConfigList = this.dataCommon?.unitFeedbackConfigList || [];
      this.listCommentsUnitCharge = unitFeedbackList.concat(unitFeedbackConfigList);
      this.strategicCapacity = this.listCommentsUnitCharge?.filter((item) => item.code === 'STRATEGIC_CAPACITY' || item.commonCode === 'STRATEGIC_CAPACITY');
      this.learningCapacity = this.listCommentsUnitCharge?.filter((item) => item.code === 'LEARNING_CAPACITY' || item.commonCode === 'LEARNING_CAPACITY');
      this.performanceCapacity = this.listCommentsUnitCharge?.filter((item) => item.code === 'PERFORMANCE_CAPACITY' || item.commonCode === 'PERFORMANCE_CAPACITY');
    }

    this.employeeTraining = this.dataCommon?.employeeTrainingList?.map((item) => { return {
      proposeDetailId: this.proposeDetailId,
      commonCode: item.code,
      contentTraining: '',
      name: item.name,
      isCheck: false,
    }}) || [];


  }

  savePropose() {
    if (this.screenCode === Constants.ScreenCode.HRDV) {
      this.isSubmitted = false;
    } else {
      if (!this.validateSubmitForm()) {
        return;
      }
      this.isSubmitted = false;
    }
    this.isLoading = true;
    if (!this.displayReport && this.screenCode === Constants.ScreenCode.HRDV) {
      this.saveReportElement();
    }
    this.proposedAppointmentRenewalService.updateProposedAppointmentRenewal(this.payloadSubmitForm(true), this.screenCode).subscribe((res) => {
      if (res) {
        this.toastrCustom.success(this.translate.instant('development.commonMessage.success'));
        this.getDataProposedSppointment();
      }
    }, (e) => {
      if (e?.error?.data) {
        this.toastrCustom.error(e?.error?.data?.map((item: ProposedError) => item.errorMessage).join(', '));
      } else if (e?.error?.message) {
        this.toastrCustom.error(e?.error?.message);
      } else {
        this.toastrCustom.error(this.translate.instant('shared.error.errorCode500'));
      }
      this.isLoading = false;
    })
  }


  saveReportElement() {
    const submitForm = this.checkDisplayReportEdit()
      ? this.formReportElement.getRawValue()
      : this.formTTQD.getRawValue();
    if (this.checkDisplayReportEdit()) {
      submitForm.typeStatement = Constants.TypeStatement.EDITHRDV;
    }
    submitForm.id = +this.proposeId;
    submitForm.externalApprove = this.displayReport;
    const sub = this.service.saveStatementDecision(submitForm).subscribe(
      () => {
        if (this.isAcceptPropose) {
          this.callApiAcceptPropose();
        }
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }

  checkProposePerformanceResultsHasAction() {
    return !this.proposePerformanceResults?.find((item) => !item.disabled);
  }

  addRotationReasons() {
    this.rotationReasons = [
      ...(this.rotationReasons ? this.rotationReasons : []),
      ...[
        {
          id: null,
          rotationReasonID: null,
          proposeDetailId: this.proposeDetailId,
          name: null,
          description: null,
          disabled: false,
        },
      ],
    ];
  }

  checkValue(e: string, index: number, item: RotationReasons) {
    const itemCreate = this.rotationReasons.find((itemRotationReasons) => itemRotationReasons.name === e);
    if (item.id === null) {
      if (
        e &&
        itemCreate?.name === e &&
        ((itemCreate?.id && this.rotationReasons.length < 8) ||
          (itemCreate?.id === null && this.rotationReasons.length === 8))
      ) {
        this.deleteRotationReasons(index, item);
      }
    } else if (item.id && itemCreate?.id) {
      this.editRotationReasons(item);
    }
  }


  editRotationReasons(item: RotationReasons) {
    item.disabled = false;
  }

  checkDeleteReason() {
    return this.rotationReasons?.length === 1;
  }

  disableSubmissionSignedDate = (dateVal: Date) => {
    return dateVal.getTime() >= Date.now();
  };


  deleteRotationReasons(index: number, item: RotationReasons) {
    if (this.typeAction !== this.screenType.detail) {
      this.rotationReasons.splice(index, 1);
    }
    this.rotationReasons = [...this.rotationReasons];
  }

  addProposePerformanceResult() {
    this.proposePerformanceResults = [
      ...(this.proposePerformanceResults ? this.proposePerformanceResults : []),
      ...[
        {
          id: null,
          proposeDetailId: this.proposeDetailId,
          targetAssigned: null,
          density: null,
          planIndicator: null,
          planDeadline: null,
          realityIndicator: null,
          realityDeadline: null,
          disabled: this.typeAction === ScreenType.Detail || (this.typeAction === ScreenType.Update && this.isScreenHandle),
        },
      ],
    ];
  }

  checkDeletePerformance() {
    return this.proposePerformanceResults.length === 1;
  }

  deleteProposePerformanceResult(index: number) {
    this.proposePerformanceResults.splice(index, 1);
    this.proposePerformanceResults = [...this.proposePerformanceResults];
    this.checkDeletePerformance();
    this.calculateDensity();
    this.calculatePoint();
  }

  addOrientationWork() {
    this.orientationJob = [
      ...(this.orientationJob?.length > 0 ? this.orientationJob : []),
      ...[
        {
          proposeDetailId: this.proposeDetailId,
          orientationJob: '',
        },
      ],
    ];
  }

  deleteOrientationWork(index: number, item: OrientationJob) {
    if (this.typeAction !== this.screenType.detail) {
      this.orientationJob.splice(index, 1);
    }
    this.orientationJob = [...this.orientationJob];
  }

  checkOrientationWork() {
    return this.orientationJob?.length === 1;
  }
  //3

  addCurrentUnit() {
    this.currentUnit = [
      ...(this.currentUnit ? this.currentUnit : []),
      ...[
        {
          unitType: 0,
          alternativeType: null,
          replacementEmployee: '',
        },
      ],
    ];
  }

  deleteCurrentUnit(index: number, item: ProposeAlternativeRenewal) {
    if (this.typeAction !== this.screenType.detail) {
      this.currentUnit.splice(index, 1);
    }
    this.currentUnit = [...this.currentUnit];
  }

  checkCurrentUnit() {
    return this.currentUnit?.length === 1;
  }



  saveCalculateItem(item: ProposePerformanceResults) {
    this.calculate(item);
    this.calculateDensity();
    if (this.sumProposePerformanceResults > 100) {
      this.sumProposePerformanceResults = 0;
      this.sumPoint = this.currentSumPoint;
      return this.toastrCustom.warning(
        this.translate.instant('development.proposed-unit.messages.sumProposePerformanceBig')
      );
    }
  }

  editProposePerformanceResult(item: ProposePerformanceResults) {
    item.disabled = false;
  }

  saveProposePerformanceResult(item: ProposePerformanceResults) {
    this.saveCalculateItem(item);
    this.checkDeletePerformance();
  }

  disabledStartDate = (startValue: Date): boolean => {
    let endValue;
    if (this.proposeDetailForm.get('rotationToDate')?.value) {
      endValue = new Date(this.proposeDetailForm.get('rotationToDate')?.value);
    } else {
      endValue = null;
    }
    if (!startValue || !endValue) {
      return false;
    }
    return startValue.getTime() > endValue.getTime();
  };

  disabledEndDate = (endValue: Date): boolean => {
    let startValue;
    if (this.proposeDetailForm.get('rotationFromDate')?.value) {
      startValue = new Date(this.proposeDetailForm.get('rotationFromDate')?.value);
    } else {
      startValue = null;
    }
    if (!startValue || !endValue) {
      return false;
    }
    return startValue.getTime() > endValue.getTime();
  };

  rotationTime() {
    const newVal =
      moment(this.proposeDetailForm.get('rotationToDate')?.value).diff(
        moment(this.proposeDetailForm.get('rotationFromDate')?.value),
        'days'
      ) / 30;
    const fixed = newVal.toFixed(1);
    if (!this.proposeDetailForm.get('rotationFromDate')?.value || !this.proposeDetailForm.get('rotationToDate')?.value) {
      this.proposeDetailForm.get('rotationTime')?.setValue(null);
    } else {
      this.proposeDetailForm.get('rotationTime')?.setValue(+fixed);
    }
  }

  calculate(item: ProposePerformanceResults, isDensity?: boolean) {
    //Tính điểm
    const totalDensity = this.sumProposePerformanceResults + item?.density!;
    if (totalDensity > 100 && isDensity) {
      item.density = 0;
      this.calculateDensity();
       this.toastrCustom.warning(
        this.translate.instant('development.proposed-unit.messages.sumProposePerformanceBig')
      );
       return;
    }
    this.calculateDensity();
  }

  calculatePoint() {
    this.sumPoint = 0;
    this.proposePerformanceResults?.forEach((item) => {
      this.sumPoint = this.sumPoint + parseFloat(<string>item.point);
    });
  }

  calculateDensity() {
    this.sumProposePerformanceResults = 0;
    this.proposePerformanceResults?.forEach((item) => {
      this.sumProposePerformanceResults = this.sumProposePerformanceResults + item.density!;
    });
  }

  // Disable onChangeFormRotation
  onChangeFormRotation(index: number) {
    this.rotations.forEach((item, i) => {
      if (i === index) {
        item.disabled = false;
      } else {
        item.disabled = true;
        item.formRotation = null;
        item.contentUnit = null;
        item.formRotationType = null;
        item.formRotationTitle = null;
        item.contentUnitCode = null;
        item.contentTitleCode = null;
        item.contentTitleName = null;
        item.contentUnitPathId = null;
        item.contentUnitPathName = null;
        item.contentUnitTreeLevel = null;
        item.level = null;
      }
    });
  }

  setEmpNullValue() {
    this.employeeForm.patchValue({
      empIdp: null,
      orgId: null,
      empId: null,
      empCode: null,
      fullName: null,
      email: null,
      posId: null,
      posName: null,
      jobId: null,
      jobName: null,
      orgPathId: null,
      orgPathName: null,
      orgName: null,
      majorLevelId: null,
      majorLevelName: null,
      faculityId: null,
      faculityName: null,
      schoolId: null,
      schoolName: null,
      positionLevel: null,
      gender: null,
      joinCompanyDate: null,
      dateOfBirth: null,
      partyDate: null,
      partyOfficialDate: null,
      posSeniority: null,
      seniority: null,
      qualificationEnglish: null,
      facultyName: null,
      semesterGradeT1: null,
      semesterRankT1: null,
      semesterGradeT2: null,
      semesterRankT2: null,
      ratedCapacity: null,
      nearestDiscipline: null,
      endOfDisciplinaryDuration: null,
      userToken: null,
      treeLevel: null,
    });
  }

  displayConditionViolation() {
    const violationObj = this.dataCommon.conditionViolationList?.find(
      (item) => +item.code === this.violationCode);
    switch (this.violationCode) {
      case 0:
        return { color: Constants.ConditionViolationColor.REQUIRED, label: violationObj?.name };
      case 1:
        return { color: Constants.ConditionViolationColor.VIOLATE, label: violationObj?.name };
      case 2:
        return { color: Constants.ConditionViolationColor.PASSED, label: violationObj?.name };
      default:
        return null;
    }
  }


  override handleDestroy(): void {
    this.proposeAlternative.forEach((item) => {
      item.alternativeType = null;
      item.id = null;
      item.proposeDetailId = null;
      item.handoverEmployeeName = null;
      item.handoverEmployeeTitleCode = null;
      item.estimatedDate = null;
      item.replacementEmployeeName = null;
      item.replacementEmployeeTitleCode = null;
    });
    this.rotations.forEach((item) => {
      item.formRotation = null;
      item.contentTitleCode = null;
      item.contentUnitCode = null;
      item.formRotationTitle = null;
      item.formRotationType = null;
    });
    this.rotationReasons?.forEach((item) => {
      item.checked = false;
      item.description = null;
      item.proposeDetailId = null;
      item.rotationReasonID = item.id;
      item.disabled = true;
    });
  }

  checkReportStatus() {
  }


  checkformRotationLabel(formRotationLabel: string) {
    if (this.rotations[0]?.formRotationLabel === formRotationLabel) {
      return 'development.proposed-unit.table.formRotationLabel1';
    } else {
      return 'development.proposed-unit.table.formRotationLabel2';
    }
  }



  showModalPropose(footerTmplSingle: TemplateRef<any>, mode: string, data?: any) {
    if (this.typeAction !== this.screenType.create) {
      return;
    }
    this.modalRef = this.modal.create({
      nzTitle: 'Tìm kiếm nhân viên',
      nzWidth: this.getInnerWidth(),
      nzContent: ProposeMultipleEmployeeComponent,
      nzComponentParams: {
        isEmp: true,
      },
      nzFooter: footerTmplSingle,
    });
    this.modalRef.afterClose.subscribe((result) => {
      if (result?.length > 0) {
        const paramAddEmp = {
          sortOrder: null,
          proposeId: this.proposeId,
          employeeCodes: [result[0].empCode],
          proposeUnitRequest: this.screenCode === Constants.ScreenCode.HRDV ? this.proposeUnit?.proposeUnitRequest : null,
          formGroup: this.formGroupCreate,
          proposeUnitRequestName: this.screenCode === Constants.ScreenCode.HRDV ? this.proposeUnit?.proposeUnitRequestName : null,
        };
        this.isLoading = true;
        this.proposeMultipleService.addEmployee(paramAddEmp, this.screenCode).subscribe(
          (res) => {
            // Khi thêm mới xong vào màn cập nhật
              this.router.navigate([Utils.getUrlByFunctionCode(this.functionCode), 'update-appointment-renewal'], {
                queryParams: {
                  id: this.proposeId,
                  proposeCategory: Constants.ProposeCategory.ORIGINAL,
                  flowStatusCode:
                        this.screenCode === Constants.ScreenCode.HRDV
                          ? Constants.FlowCodeConstants.CHUAGUI1
                          : Constants.FlowCodeConstants.NHAP,
                },
                skipLocationChange: true,
              });
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
      }
    });
  }

  clearEmployeeCode(event: any) {
    event.preventDefault();
    this.employeeForm.get('empCode')?.setValue(null);
    this.proposeDetailForm.disable();
    // this.proposeCommentForm.disable();
  }

  getInnerWidth(): number {
    if (window.innerWidth > 767) {
      return window.innerWidth / 1.5 > 1368 ? 1368 : window.innerWidth / 1.3;
    }
    return window.innerWidth;
  }

  onChangeUnit(orgId: string, index: number, contentTitleDetail?: number) {
    this.jobDTOSApi = [];
    if (index > 0 && this.rotations[index].contentUnit && orgId) {
      const unitLevel = this.rotations[index].contentUnit?.pathId?.split(':')[3];
      const unitLevelOfEmployee = this.employeeForm.controls['orgPathId'].value?.split('/')[3];
      if (unitLevel === unitLevelOfEmployee && !this.isScreenHandle) {
        this.toastrCustom.error(this.translate.instant('development.commonMessage.validateUnit'));
        return;
      }
    }
    if (orgId) {
      this.isLoading = true;
      const paramPosition: ParamSearchPosition = {
        orgId: orgId,
        jobId: '',
        status: 1,
        type: Constants.TYPE_SEARCH_POSITION,
        page: 0,
        size: maxInt32,
      };
      this.service.getPositionByUnit(paramPosition).subscribe(
        (res) => {
          const listPositionParent = res?.data?.content;
          this.jobDTOSApi = listPositionParent.map((item: TitleUnit) => {
            return { jobId: item.jobId, jobName: item.jobName, posId: item.posId };
          });
          this.rotations[index].contentTitleCode = contentTitleDetail ? this.jobDTOSApi.find((item) => item.jobId === contentTitleDetail)?.jobId ?? null :  null;
          this.isLoading = false;
        },
        (e) => {
          this.isLoading = false;
        }
      );
    }
  }

  expandLog() {
    this.isExpand = !this.isExpand;
    if (this.isExpand) {
      this.iconStatus = Constants.IconStatus.UP;
    } else {
      this.iconStatus = Constants.IconStatus.DOWN;
    }
  }

  // changeTitle(valueTitle: string, index: number) {
  //   if (+valueTitle === 0) {
  //     this.rotations[index].contentTitleCode =
  //       this.jobDTOSApi.find((item) => item.jobId === +this.contentTitleEmp!)?.jobId || null;
  //     if (
  //       !this.rotations[index].contentTitleCode &&
  //       (this.rotations[index].contentUnitCode || this.rotations[index].contentUnit)
  //     ) {
  //       this.toastrCustom.error(this.translate.instant('development.commonMessage.E001'));
  //     }
  //   } else {
  //     this.rotations[index].contentTitleCode = null;
  //   }
  // }

  onChangeUnitDiff(orgIdLevel: number) {
    if (orgIdLevel) {
      const param = {
        id: orgIdLevel,
        pag: 0,
        size: maxInt32,
      };
      this.service.getOrganizationTreeById(param).subscribe((res) => {
        if (res && res?.content?.length > 0) {
          res?.content.forEach((i: OrgEmployee) => {
            if (i.treeLevel > 2) {
              i.orgNameFull = i.pathName.split("-").splice(2).join("-") + " - " + i.orgName;
            }
          });
          this.listInUnit = res?.content.map((item: OrgEmployee) => {
            return <ListOrgItem>{
              name: item.orgNameFull,
              value: item.orgId,
              valueItem: <OrgItem>{
                orgId: item.orgId,
                orgName: item.orgName,
                pathId: item.pathId,
                pathResult: item.pathName,
                orgLevel: item.treeLevel,
              },
            };
          });
        }
      });
    }
  }

  scrollToValidationSection(section: string, messsage: string){
    this.toastrCustom.warning(this.translate.instant(messsage));
    let el = document.getElementById(section);
    el?.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
  }

  downloadFile = (file?: NzUploadFile) => {
    this.service.downloadFile(+this.fileListCreate[0]?.uid, this.fileListCreate[0]?.name).subscribe(
      () => {
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    )
  }

  expandInfoEmp() {
    this.isSeeMore = !this.isSeeMore;
  }

  expandInfoEmpText() {
    const infoEmpText = !this.isSeeMore
      ? this.translate.instant('development.proposed-unit.button.down')
      : this.translate.instant('development.proposed-unit.button.up')
    return infoEmpText;
  }


  showModalEmpAlternative(footerTmplSingle: TemplateRef<any>, type: string,index: number) {
    if (this.typeAction === this.screenType.detail) {
      return;
    }
    this.modalRef = this.modal.create({
      nzTitle: 'Tìm kiếm nhân viên',
      nzWidth: this.getInnerWidth(),
      nzContent: ProposeMultipleEmployeeComponent,
      nzComponentParams: {
        isEmp: true,
      },
      nzFooter: footerTmplSingle,
    });
    this.modalRef.afterClose.subscribe((result) => {
      if (result?.length > 0) {
        this.currentUnit.forEach((item, i) => {
          if (i === index) {
            item.replacementEmployeeCode = result[0]?.empCode;
            item.replacementEmployeeName = result[0]?.fullName;
            item.replacementEmployee = result[0]?.empCode + '-' + result[0]?.fullName + '-' + result[0]?.jobName + '-' + result[0]?.orgName;
            item.replacementEmployeeTitleCode = result[0]?.jobId;
            item.replacementEmployeeTitleName = result[0]?.jobName;
          }
        })
      }
    });
  }

  addProposeAlternative() {
    this.proposeAlternative = [
      ...(this.proposeAlternative ? this.proposeAlternative : []),
      ...[
        {
          id: null,
          estimatedDate: null,
          replacementEmployeeCode: null,
          replacementEmployeeName: null,
          replacementEmployeeTitleCode: null,
          replacementEmployeeTitleName: null,
          handoverEmployeeCode: null,
          handoverEmployeeName: null,
          handoverEmployeeTitleCode: null,
          handoverEmployeeTitleName: null,
          disabled: true,
          replacementEmployee: null,
          handoverEmployee: null,
          }
      ],
    ];
  }

  checkDeleteReason1() {
    return this.proposeAlternative?.length === 1;
  }

  deleteRotationReasons1(index: number, item: ProposeAlternative) {
    if (this.typeAction !== this.screenType.detail) {
      this.proposeAlternative.splice(index, 1);
    }
    this.proposeAlternative = [...this.proposeAlternative];
  }

  override ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
    this.modalRef?.destroy(true);
  }

  //validate ngày đề xuất triển khai
  disabledImplementDate = (dateVal: Date) => {
    return dateVal.valueOf() <= Date.now();
  };



  doRemoveFile = (file?: NzUploadFile): boolean => {
    this.isLoading = true;
    this.proposedAppointmentRenewalService.deleteFile(this.proposeDetailId).subscribe(
      (res) => {
        this.isLoading = false;
        this.fileListCreate = [];
        this.nameFileListCreate = '';
      },
      (e) => {
        this.isLoading = false;
        this.getMessageError(e);
      }
    );
    return false;
  };

  dowloadStatement(action: number) {
    const params = {
      groupProposeType: Constants.ProposeTypeID.ELEMENT,
      action: action,
      screenCode: this.screenCode,
    };
    this.isLoading = true;
    let listFile: ListFileTemplate[] = [];
    if (this.proposeDetailId) {
      const sub = this.service.exportReportDecision(this.proposeDetailId, params).subscribe(
        (res) => {
          listFile = res?.data;
          if (!listFile || listFile?.length === 0) {
            this.toastrCustom.error('development.list-plan.messages.noFile');
            this.isLoading = false;
          } else {
            listFile?.forEach((item) => {
              this.service.callBaseUrl(item.linkTemplate).subscribe((res) => {
                const contentDisposition = res?.headers.get('content-disposition');
                const filename = contentDisposition?.split(';')[1]?.split('filename')[1]?.split('=')[1]?.trim();
                const blob = new Blob([res?.body], {
                  type: 'application/octet-stream',
                });
                FileSaver.saveAs(blob, filename);
                this.isLoading = false;
              },
              (e) => {
                this.getMessageError(e);
                this.isLoading = false;
              })
            })
          }
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        }
      );
      this.subs.push(sub);
    }
  }

  onChangeProposeType(e: number, contentTitleCode: string) {
    this.proposeMultipleService.formGroupProposeMultiple(e).subscribe((res) => {
      this.dataCommon.formGroupMultipleList = res?.data || [];
      this.onChangeFormCode(contentTitleCode);
    })

  }


  beforeUpload = (file: File | NzUploadFile): boolean => {
    this.isLoading = true;
    const body = {
      proposeDetailId: this.proposeDetailId,
      personalReview: this.personalReview,
      different: this.different,
      targetAppoint: this.targetAppoint,
      reportBranchMeeting: this.reportBranchMeeting,
      consultationComment: this.consultationComment,
      statement: this.displayReport ?? false,
    };
    const formData = new FormData();
    formData.append('files', file as File);
    formData.append(
      'proposeAttachmentFiles',
      new Blob([JSON.stringify(body)], {
        type: Constants.CONTENT_TYPE.APPLICATION_JSON,
      })
    );
    this.proposedAppointmentRenewalService.uploadFileAppointmentRenewal(formData).subscribe(
      (res) => {
        // this.attachFileObj = res?.data || {};
        this.nameFileListCreate = res.data?.fileName;

        this.fileListCreate = [
          ...this.fileListCreate,
          {
            uid: res.data?.docId,
            name: res.data?.fileName,
            status: 'done',
            id: res.data?.id,
          },
        ];
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    return false;
  };


  selectProposeTitle(e: number) {
    const posId = this.jobDTOSApi.find((item) => item.jobId === e)?.posId;
    if (posId) {
      this.proposedAppointmentRenewalService.getPersonnelPlace(posId).subscribe((res) => {
        if (res) {
          this.proposedUnit = res?.data.map((item: EmployeeModal) => { return {
            contentTitle: item?.employeeCode + '-' + item?.fullName + '-' + (item?.positionType === Constants.PositionType.CT ?
              this.translate.instant('development.propose-tgcv.responsible') :
              this.translate.instant('development.propose-tgcv.concurrently')),
            concurrentEmployeeCode: item?.employeeCode,
            concurrentEmployeeName:  item?.fullName,
            positionType: item?.positionType,
            unitType: 1,
            alternativePlan: null,
            replacementUnitName: item?.orgName,
            replacementEmployeeTitleName: item?.jobName,

          } }) || [];
        }
      }, (e) => {
        this.getMessageError(e);
      })
    }

  }

  //gửi duyệt
  sendPropose() {
    if (!this.validateSubmitForm()) {
      return;
    }
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant(
        'development.proposed-unit.messages.sendPropose'
      ),
      nzCancelText: this.translate.instant(
        'development.proposed-unit.button.cancel'
      ),
      nzOkText: this.translate.instant(
        'development.proposed-unit.button.continue'
      ),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        if (this.violationCode === Constants.ViolationCode.REQUIRED) {
          this.isLoading = false;
          this.modalRef = this.modal.confirm({
            nzTitle: this.translate.instant(
              'development.propose-tgcv.confirmSendApprove'
            ),
            nzCancelText: this.translate.instant(
              'development.proposed-unit.button.cancel'
            ),
            nzOkText: this.translate.instant(
              'development.proposed-unit.button.continue'
            ),
            nzClassName: Constants.ClassName.LD_CONFIRM,
            nzOnOk: () => {
              this.isLoading = true;
              this.callApiSendPropose();
            },
          });
        } else {
          this.callApiSendPropose();
        }
      },
    });
  }

  callApiSendPropose() {
    const params = {
      proposeType: Constants.ProposeTypeID.ORIGINAL,
    }
    const send = this.proposedAppointmentRenewalService
      .sendProposeAppointment(this.proposeId, this.payloadSubmitForm(false), params)
      .subscribe(
        (res) => {
          if ( res?.data && res?.data?.length > 0 ) {
            this.toastrCustom.error(res?.data.map((item: ProposedError) => item.errorMessage).join(', '));
          } else {
            this.toastrCustom.success(
              this.translate.instant(
                'development.proposed-unit.messages.sendProposeSuccess'
              )
            );
            this.showModalCreateEmp();
          }
          this.isLoading = false;
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        }
      );
    this.subs.push(send);
  }

  //ghi nhận đồng ý
  acceptPropose() {
    if (!this.validateSubmitForm()) {
      return;
    }
    //nếu có tờ trình chỉnh sửa thì validate tờ trình này
    //nếu không thì validate tờ trình ban đầu
    if (this.checkDisplayReportEdit()) {
      if (!this.validApproveEditReport()) {
        this.scrollToValidationSection('formTTQD', 'development.commonMessage.validateRecordApprove2');
        this.isSubmitFormReportElement = true;
        return;
      }
    } else {
      if (!this.formTTQD.valid) {
        this.toastrCustom.error(this.translate.instant('development.commonMessage.validateReportUnit'));
        this.isSubmitFormReportElement = true;
        return;
      }
    }

    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant(
        'development.proposed-unit.messages.acceptPropose'
      ),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        if (this.violationCode === Constants.ViolationCode.REQUIRED) {
          this.isLoading = false;
          this.modalRef = this.modal.confirm({
            nzTitle: this.translate.instant(
              'development.proposed-unit.messages.sendProposeWarning2'
            ),
            nzCancelText: this.translate.instant(
              'development.proposed-unit.button.cancelText'
            ),
            nzOkText: this.translate.instant(
              'development.proposed-unit.button.okText'
            ),
            nzClassName: 'ld-confirm',
            nzOnOk: () => {
              this.isLoading = true;
              this.isAcceptPropose = true;
              this.saveReportElement();
            },
          });
        } else {
          this.isAcceptPropose = true;
          this.saveReportElement();
        }
      },
    });
  }

  callApiAcceptPropose() {
    const params = {
      proposeType: Constants.ProposeTypeID.ORIGINAL,
      note: this.checkDisplayReportEdit()
        ? this.formReportElement.get('approveNote')?.value
        : this.formTTQD.get('approveNote')?.value,
    }
    this.isLoading = true;
    const sub = this.proposedAppointmentRenewalService
        .agreeUnitAppointment(this.proposeId, this.payloadSubmitForm(false), params)
      .subscribe(
        (res) => {
          if (res?.data && res?.data?.length > 0 ) {
            this.toastrCustom.error(res?.data.map((item: ProposedError) => item.errorMessage).join(', '));
          } else {
            this.toastrCustom.success(
              this.translate.instant(
                'development.proposed-unit.messages.acceptProposeSuccess'
              )
            );
            this.showModalCreateEmp()
          }
          this.isLoading = false;
          },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        }
      );
    this.subs.push(sub);
  }

  //check hiển thị nút ghi nhận đồng ý
  checkRecordApprove() {
    return (
      !this.displayReport &&
      Constants.StatusApproveSendProposeMany.indexOf(this.flowStatusCode) >
        -1 &&
      this.checkHCMRadio()
    );
  }

  //check hiển thị nút gửi duyệt
  checkSendApprove() {
    return (
      this.displayReport &&
      Constants.StatusApproveSendProposeMany.indexOf(this.flowStatusCode) >
        -1 &&
      this.checkHCMRadio()
    );
  }


  //check hiển thi radio lãnh đạo duyệt trên HCM
  checkHCMRadio() {
    return (
      this.issueLevelName !== Constants.IssueLevelName.UNIT ||
      (this.issueLevelName === Constants.IssueLevelName.UNIT &&
        this.proposeDTO?.isHo)
    );
  }

  checkBtnDelete() {
    if (!this.isScreenHandle) {
      return (this.screenCode === this.screenCodeConst.HRDV && this.flowStatusCode === Constants.FlowCodeConstants.CHUAGUI1) ||
      (this.proposeDTO?.createdBy === this.currentUser?.username && this.screenCode === this.screenCodeConst.PTNL &&
      Constants.CheckDeleteBtn.includes(this.flowStatusCode));
    } else {
      return (this.screenCode === this.screenCodeConst.KSPTNL || this.screenCode === this.screenCodeConst.PTNL) && this.isDeletePTNL;
    }

  }

  delete(event: MouseEvent) {
    event.stopPropagation();
    if (this.isLoading) {
      return;
    }
    const groupProposeType = Utils.getGroupType(this.proposeCategory);
    const idPropose = this.proposeCategory === Constants.ProposeCategory.ELEMENT ? this.proposeDetailId : this.proposeId;
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant(
        'development.proposed-unit.messages.delete'
      ),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        this.service
          .deleteProposeListLD(idPropose, groupProposeType)
          .subscribe(
            () => {
              this.toastrCustom.success(
                this.translate.instant(
                  'development.proposed-unit.messages.deleteSuccess'
                )
              );
              if (this.stateUrl?.state?.['backUrl'] && this.proposeCategory !== Constants.ProposeCategory.ORIGINAL) {
                this.back();
              } else {
                this.router.navigateByUrl(Utils.getUrlByFunctionCode(this.functionCode));
              }

              this.modalRef?.destroy();
            },
            (e) => {
              this.getMessageError(e);
              this.isLoading = false;
            }
          );
      },
    });
  }

  //check hiển thị rút toàn bộ
  checkWithdrawPropose(){
    const flowCodeParent = this.proposeDTO?.flowStatusCode || '';
    const flowCodeElement = this.proposeDTO?.proposeDetails[0]?.flowStatusCode || '';
    return (flowCodeParent === Constants.FlowCodeConstants.HANDLING5B ||
      flowCodeParent === Constants.FlowCodeConstants.HANDLING6) &&
      Constants.ChangeStatusWithdraw.indexOf(flowCodeElement) <= -1 &&
      !this.proposeDTO?.withdrawNote
      && this.screenCode === Constants.ScreenCode.HRDV;
  }

  showModalWithdraw(typeButton: number) {
    const ids: number[] = [];
    ids.push(this.proposeId);
    this.modalRef = this.modal.create({
      nzTitle: this.translate.instant('development.proposed-unit.proposeMultiple.withdrawPropose'),
      nzWidth: '40vw',
      nzContent: WithdrawProposeComponent,
      nzComponentParams: {
        typeWithdraw: typeButton,
        ids: ids,
        isApproveHCMBefore: this.proposeDTO?.proposeDetails[0]?.externalApprove,
      },
      nzFooter: null,
    });
    this.modalRef.componentInstance.closeEvent.subscribe((data:boolean) => {
      if (data) {
        this.isLoading = true;
        this.modalRef?.componentInstance.sendBody.subscribe((body: WithdrawPropose) => {
          const sub = this.service.withdrawPropose(body).subscribe(() => {
            this.toastrCustom.success(
              this.translate.instant('development.withdrawPropose.withdrawProposeSuccess'));
            this.ngOnInit();
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          });
          this.subs.push(sub);
          this.modalRef?.destroy();
        });
      } else {
        this.modalRef?.destroy();
      }
    });
  }

  override back() {
    if (this.stateUrl?.state?.['backUrl']) {
      this.router
        .navigateByUrl(Utils.getUrlByFunctionCode(this.functionCode))
        .then(() => {
          this.router.navigateByUrl(this.stateUrl?.state?.['backUrl'], {
            skipLocationChange: true,
            state: {
              action: 'add',
            },
          });
        });
    } else {
      this.router.navigateByUrl(Utils.getUrlByFunctionCode(this.functionCode));
    }
  }

  onChangeFormCode(e: string) {
    const item = this.dataCommon.formGroupMultipleList?.find((itemformGroupMultiple) => itemformGroupMultiple.formCode === e);
    this.rotationTimeViewMax = item?.showMaxTime ?? false;
    this.rotationTimeViewMin = item?.showMinTime ?? false;

    this.proposeDetailForm.get('minimumTime')?.clearValidators();
    this.proposeDetailForm.get('maximumTime')?.clearValidators();
    if (this.rotationTimeViewMax) {
      this.proposeDetailForm.get('maximumTime')?.addValidators(Validators.required);
    }
    if (this.rotationTimeViewMin) {
      this.proposeDetailForm.get('minimumTime')?.addValidators(Validators.required);
    }

    this.proposeDetailForm.get('minimumTime')?.updateValueAndValidity();
    this.proposeDetailForm.get('maximumTime')?.updateValueAndValidity();
  }



  //check hiển thị 3 nút duyệt của LDDV và ý kiến phê duyệt
  checkApproveLDDV(){
    if (this.proposeCategory !== Constants.ProposeCategory.ELEMENT) {
      return this.checkIdeaApproveLDDV();
    } else {
      return this.screenCode === this.screenCodeConst.LDDV
        && this.flowStatusCode === this.flowCodeConst.SENDAPPROVE2A
    }
  }

    //check hiển thị ý kiến phê duyệt của LDDV
    checkIdeaApproveLDDV() {
      return this.screenCode === this.screenCodeConst.LDDV
        && (this.flowStatusCode === this.flowCodeConst.CHODUYET2 ||
          this.flowStatusCode === this.flowCodeConst.SENDAPPROVE2A)
    }

    // duyệt đồng ý LDDV
  agreeLeader() {
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.proposed-unit.messages.agreeLeader'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL2_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.CONTINUE_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        const id = this.proposeCategory === Constants.ProposeCategory.ELEMENT ? this.proposeDetailId : this.proposeId
        const proposeType = this.proposeCategory === Constants.ProposeCategory.ELEMENT ? 2 : 1
        this.service.agreeLeader(id, this.note, proposeType).subscribe(
          () => {
            this.toastrCustom.success(this.translate.instant(Constants.KeyCommons.MESSAGES_SUCCESS2));
            this.router.navigateByUrl(Constants.KeyCommons.DEVELOPMENT_PROPOSE_UNIT_APPROVE);
            this.isLoading = false;
            this.modalRef?.destroy();
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
      },
    });
  }


   // duyệt từ chối LDDV
   rejectLeader() {
    if (!this.note.trim()) {
      this.toastrCustom.error(this.translate.instant('development.commonMessage.approveIdeaRequired'));
      let el = document.getElementById('idea');
      el?.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
      return;
    }
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.proposed-unit.messages.rejectLeader'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        this.service.rejectLeader(this.proposeId, this.note).subscribe(
          () => {
            this.toastrCustom.success(this.translate.instant(Constants.KeyCommons.MESSAGES_SUCCESS2));
            this.router.navigateByUrl(Constants.KeyCommons.DEVELOPMENT_PROPOSE_UNIT_APPROVE);
            this.isLoading = false;
            this.modalRef?.destroy();
          },
          (e) => {
            this.toastrCustom.error(this.translate.instant('development.proposed-unit.messages.errorMes'));
            this.isLoading = false;
          }
        );
      },
    });
  }

  // duyệt ý kiến khác LDDV
  otherIdea() {
    if (!this.note) {
      this.toastrCustom.error(this.translate.instant('development.commonMessage.approveIdeaRequired'));
      let el = document.getElementById('idea');
      el?.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
      return;
    }
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.proposed-unit.messages.otherIdeaLDDV'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        this.service.otherIdea(this.proposeId, this.note, this.proposeType).subscribe(
          (res) => {
            this.toastrCustom.success(this.translate.instant(Constants.KeyCommons.MESSAGES_SUCCESS2));
            this.router.navigateByUrl(Constants.KeyCommons.DEVELOPMENT_PROPOSE_UNIT_APPROVE);
            this.isLoading = false;
            this.modalRef?.destroy();
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
      },
    });
  }

  checkWithdrawBtn(){
    const listWithdrawBtn = Constants.StatusButtonWithdraw;
    const listNoteCodeWithdraw = Constants.noteWithdraw;
    return listWithdrawBtn.indexOf(this.flowStatusCode) > -1 && (this.proposeCategory === Constants.ProposeCategory.ORIGINAL)
      && listNoteCodeWithdraw.indexOf(this.withdrawNoteCode) > -1
      && (this.screenCode === this.screenCodeConst.LDDV);
  }

   // duyệt rút đề xuất
  agreeWithdrawProposal(ideaType: number) {
    if (!this.comment && (ideaType === Constants.WithdrawConfirmIdeaType.REJECT
      || ideaType === Constants.WithdrawConfirmIdeaType.OTHERIDEA)) {
        this.scrollToValidationSection('confirmWithdraw', 'development.commonMessage.approveIdeaRequired');
        return;
      }
    const checkId = this.proposeCategory === Constants.ProposeCategory.ORIGINAL ? this.proposeId : this.proposeDetailId;
    const checkProposeType = Utils.getGroupType(this.proposeCategory);
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.proposed-unit.messages.agreeLeader2'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        if(!this.checkWithdrawType){
          this.service.approveWithdrawPropose(checkId, checkProposeType, this.comment, ideaType, this.screenCode).subscribe(
          () => {
            this.toastrCustom.success(this.translate.instant(Constants.KeyCommons.MESSAGES_SUCCESS2));
            this.router.navigateByUrl(Constants.KeyCommons.DEVELOPMENT_PROPOSE_UNIT_APPROVE);
            this.isLoading = false;
            this.modalRef?.destroy();
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
        } else {
          const ids: number[] = [];
          this.proposeDTO?.proposeDetails?.forEach((item) => {
            if (item.withdrawNote !== undefined ) {
              ids.push(item.id);
            }
          });
          this.service.approveWithdrawProposeDetail(ids, this.comment, ideaType, this.screenCode).subscribe(
            () => {
              this.toastrCustom.success(this.translate.instant(Constants.KeyCommons.MESSAGES_SUCCESS_APPROVED));
              this.router.navigateByUrl(Constants.KeyCommons.DEVELOPMENT_PROPOSE_UNIT_APPROVE);
              this.isLoading = false;
              this.modalRef?.destroy();
            },
            (e) => {
              this.getMessageError(e);
              this.isLoading = false;
            }
          );
        }
      },
    });
  }

  showModalCreateEmp() {
    const listCurrentUnit = this.currentUnit?.filter((i) => i.alternativeType === this.planAlternative[0].code);
    const listProposedUnit = this.proposedUnit?.filter((i) => i.positionType === Constants.PositionType.CT && i.alternativePlan === Constants.ROTATION_SPECIALIZE)
    if (listCurrentUnit.length > 0 || listProposedUnit.length > 0) {
      const currentUnitCreate = listCurrentUnit.map((item) => { return {
        employeeCode: item.replacementEmployeeCode,
        employeeInfo: item.replacementEmployee,
        formGroupEmp: null,
        relatedProposeId: this.proposeId,
        unitType: Constants.TypeProposed.CURRENTUNIT,

      } }) || [];
      const proposedUnitCreate = listProposedUnit?.map((item) => { return {
        employeeCode: item.concurrentEmployeeCode,
        employeeInfo: item?.concurrentEmployeeCode + '-' + item?.concurrentEmployeeName + '-' + item?.replacementUnitName + '-' + item?.replacementEmployeeTitleName,
        formGroupEmp: null,
        relatedProposeId: this.proposeId,
        unitType: Constants.TypeProposed.PROPOSEDUNIT,
      } }) || [];

        this.modalRef = this.modal.create({
          nzTitle: this.translate.instant('development.appointmentRenewal.createScreen.confirmProposalInformation'),
          nzWidth: this.getInnerWidth(),
          nzContent: ProposedAppointmentRenewalEmpComponent,
          nzComponentParams: {
            lisEmployeeCreate: [...currentUnitCreate, ...proposedUnitCreate],
            formGroupList :this.dataCommon.formGroupList,
          },
          nzFooter: null,
        });
        this.modalRef.componentInstance.saveEmployee.subscribe((data: EmployeeAppointment[]) => {
          const body = data.map((item) => {
            return {
              formGroup: item.formGroupEmp,
              proposeUnitRequestName: this.proposeDTO?.proposeUnitRequestName ?? null,
              proposeUnitRequest: this.proposeDTO?.proposeUnitRequest ?? null,
              employeeCodes: [item?.employeeCode ? item?.employeeCode.toString() : ''],
              proposeId: null,
              sortOrder: null,
              relatedProposeId: this.proposeId,
              unitType: item?.unitType,
            }
          })
          this.isLoading = true;
          this.proposedAppointmentRenewalService.saveEmployeeFormGroup(this.screenCode, body).subscribe((res) => {
            if (res.data) {
              const formGroup = res.data?.formGroup;
              if (+formGroup === Constants.FormGroupList.TGCV || +formGroup === Constants.FormGroupList.BO_NHIEM) {
                const routerUpdate = +formGroup === Constants.FormGroupList.TGCV ? Constants.SubRouterLink.UPDATETGCV : Constants.TypeScreenHandle.UPDATE_APPOINTMENT;
                this.router.navigate([Utils.getUrlByFunctionCode(this.functionCode), routerUpdate], {
                  queryParams: {
                    id: res.data?.proposeId,
                    proposeCategory: Constants.ProposeCategory.ORIGINAL,
                    flowStatusCode:
                    this.screenCode === Constants.ScreenCode.HRDV
                      ? Constants.FlowCodeConstants.CHUAGUI1
                      : Constants.FlowCodeConstants.NHAP,
                  },
                  skipLocationChange: true,
                });
              } else {
                this.router.navigate([Utils.getUrlByFunctionCode(this.functionCode), 'update'], {
                  queryParams: {
                    id: res.data?.proposeId,
                    proposeType: 0,
                    proposeCategory: Constants.ProposeCategory.ORIGINAL,
                    flowStatusCode: this.screenCode === Constants.ScreenCode.HRDV ? Constants.FlowCodeConstants.CHUAGUI1 : Constants.FlowCodeConstants.NHAP,
                    relationType: true,
                  },
                  skipLocationChange: true,
                });
              }
            }
            this.isLoading = false;
            this.translate.instant('development.interview-config.notificationMessage.success')
          }, (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          });
        })
       this.modalRef?.afterClose.subscribe((result: boolean) => {
          if (!result) {
            this.router.navigateByUrl(
              Utils.getUrlByFunctionCode(this.functionCode)
            );
          }

        });

    } else {
      this.router.navigateByUrl(
        Utils.getUrlByFunctionCode(this.functionCode)
      );
    }
  }

   //check hiển thị nút xử lý của PTNL
   checkHandleButton() {
    return Constants.HandleButtonPTNL.indexOf(this.flowStatusCode) > -1 && this.screenCode === this.screenCodeConst.PTNL;
  }

  handle() {
    this.isLoading = true;
    const param = {
      id: this.proposeCategory === Constants.ProposeCategory.ELEMENT ? this.proposeDetailId : this.proposeId,
      proposeType: this.proposeType,
    };
    this.service.proposalProcessing(param).subscribe(
      (res) => {
        this.isLoading = false;
        this.toastrCustom.success(this.translate.instant('development.commonMessage.success'));
        const queryParams = {
          screenCode: this.screenCode,
          id: this.proposeId,
          proposeDetailId: this.proposeDetailId,
          proposeCategory: this.proposeCategory,
          relationType: 0,
        };
        this.router.navigate([Utils.getUrlByFunctionCode(this.functionCode), 'handle-appointment-renewal'], {
          queryParams: queryParams,
          state: { backUrl: this.router.url },
          skipLocationChange: true,
        });
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

  scrollTo(tab: ScrollTabOption) {
    const el = document.getElementById(tab?.scrollTo ?? '');
    el?.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
  }

  onLoading(event: boolean) {
    this.isLoading = event;
  }

  checkBtnWithdrawPTNL() {
    return (this.proposeCategory === Constants.ProposeCategory.ELEMENT)
      && Constants.ChangeStatusWithdraw.indexOf(this.flowStatusCode) === -1
      && (this.withdrawNote === Constants.withdrawNoteCode.Note3 || this.withdrawNote === Constants.withdrawNoteCode.Note4)
      && (this.screenCode === this.screenCodeConst.PTNL);
  }


  //duyệt rút đề xuất thành phần
  agreeWithdrawProposalDetail(ideaType: number) {
    if (!this.comment && ideaType === Constants.WithdrawConfirmIdeaType.REJECT) {
      this.scrollToValidationSection('idea2', 'development.commonMessage.approveIdeaRequired');
      return;
    }
    const ids: number[] = [];
    this.proposeDTO?.proposeDetails?.forEach((item) => {
      if (item.withdrawNote === Constants.withdrawNoteCode.Note3 || item.withdrawNote === Constants.withdrawNoteCode.Note4) {
        ids.push(item.id);
      }
    });
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.proposed-unit.messages.agreeLeader2'),
      nzCancelText: this.translate.instant('development.proposed-unit.button.cancelText'),
      nzOkText: this.translate.instant('development.proposed-unit.button.okText'),
      nzClassName: 'ld-confirm',
      nzOnOk: () => {
        this.isLoading = true;
        this.service.approveWithdrawProposeDetail(ids, this.comment, ideaType, this.screenCode).subscribe(
          () => {
            this.toastrCustom.success(this.translate.instant(Constants.KeyCommons.MESSAGES_SUCCESS2));
            this.router.navigateByUrl('development/propose/propose-list');
            this.isLoading = false;
            this.modalRef?.destroy();
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
      },
    });
  }

  checkSaveSendPropose() {
    return Constants.StatusCB.indexOf(this.flowStatusCode) > -1;
  }

  saveHandle() {
      const dataHandle = this.consultationScreenComponent?.saveHandle();
      if (this.consultationScreenComponent?.formReason?.valid) {
        this.service.saveHandle(this.proposeDetailId, dataHandle).subscribe(
          () => {
            this.isLoading = false;
            this.toastrCustom.success(this.translate.instant('development.commonMessage.success'));
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
      }
  }

  checkBTNprocessingPTNL() {
    return this.proposeCategory === Constants.ProposeCategory.GROUP
  }

  processingPTNL(content?: DataMessageModel[]) {
    const param: any = {
      id: this.proposeCategory === Constants.ProposeCategory.ELEMENT ? this.proposeDetailId : this.proposeId,
      proposeType: this.proposeCategory === Constants.ProposeCategory.ELEMENT ? 2 : 0,
      isShow: !!content,
    };
    if (!content) {
      delete param.isShow;
    }
    this.modalRef = this.modal.confirm({
      nzTitle: content ? '' : this.translate.instant('development.proposed-unit.messages.sendProposeDV'),
      nzContent: !!content ? this.modalContent : '',
      nzOkText: this.translate.instant(Constants.KeyCommons.CONTINUE_TEXT),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL2_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        this.service.processingPTNL(param).subscribe(
          (res) => {
            if (res?.data) {
              this.messageListError = res?.data.titleEmployee;
              this.processingPTNL(this.messageListError);
            } else {
              this.toastrCustom.success(this.translate.instant('development.proposed-unit.messages.sendProposeSuccess'));
              this.back();
            }
            this.isLoading = false;
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
      },
    });
  }

  viewProposeLogsStatus () {
    return (
      (this.flowStatusCode === Constants.FlowCodeConstants.CHUAGUI1 ||
        this.flowStatusCode === Constants.FlowCodeConstants.NHAP ||
        this.flowStatusCode === Constants.FlowCodeConstants.HANDLING5B ||
        (!this.isScreenHandle && this.typeAction === this.screenType.update)) &&
      this.screenCode === this.screenCodeConst.PTNL
    ) || (this.screenCode === this.screenCodeConst.HRDV || this.screenCode === this.screenCodeConst.LDDV);
  }

  //view tờ trình từ đơn vị gửi lên
  viewReportHRDV() {
    if (Constants.ListScreenCodeUnit.includes(this.screenCode)) {
      return !this.displayReport;
    } else if (Constants.ListScreenCodePTNL.includes(this.screenCode)) {
      return !this.displayReport && this.proposeDTO?.proposeUnitRequestName;
    }
    return false;
  }

  getDataProposedSppointment() {
    const paramDetail = {
      ids: this.proposeId,
      screenCode: this.screenCode,
      flowStatusCode: this.flowStatusCode,
    };
    if (this.screenCode === this.screenCodeConst.KSPTNL || this.screenCode === this.screenCodeConst.PTNL) {
      const idDelete = this.proposeCategory === Constants.ProposeCategory.ELEMENT ? this.proposeDetailId : this.proposeId;
      const groupProposeType = Utils.getGroupType(this.proposeCategory);
      this.service.checkDeletePTNL(idDelete, groupProposeType).subscribe((res) => {
        this.isDeletePTNL = res?.data;
      });
    }

    let api: Observable<any>;
    if (this.proposeCategory === Constants.ProposeCategory.ELEMENT) {
      api = this.service.getDetailElement(this.proposeDetailId, this.screenCode);
    } else {
      api = this.service.findById(paramDetail);
    }
    const ids = this.proposeCategory === Constants.ProposeCategory.ELEMENT ? this.proposeDetailId : this.proposeId;
    const subs = forkJoin([
      this.service.getState({
        screenCode: this.screenCode,
      }).pipe(catchError(() => of(undefined))),
      api.pipe(catchError(() => of(undefined))),
      this.service.getLogsPage(ids, this.proposeCategory, this.screenCode, this.pageNumber, this.pageSize).pipe(catchError(() => of(undefined))),
    ]).subscribe(
      ([state, data, logs]) => {
        this.proposeLogs = logs?.data?.content || [];
        this.tableConfig.total = logs?.data?.totalElements || 0;
        this.tableConfig.pageIndex = 1;
        this.dataCommon = state;
        this.proposeDTO = data?.proposeDTO[0];
        this.checkWithdrawType = this.proposeDTO?.withdrawType === undefined;
        if (this.proposeDTO) {
          const proposeDetailsElement = this.proposeDTO.proposeDetails[0];
          this.hasProcess = proposeDetailsElement?.hasProcess ?? false;
          this.withdrawNote = this.proposeDTO?.proposeDetails?.find((item: DataFormTGCV) => item.withdrawNote)?.withdrawNote ?? 0;


          //Điều kiện vi phạm cảnh báo
          proposeDetailsElement?.conditionViolations?.forEach((item: string) => {
            this.conditionViolation.push(item);
          });
          this.violationCode = proposeDetailsElement?.conditionViolationCode;
          if (this.proposeCategory === Constants.ProposeCategory.ELEMENT) {
            this.flowStatusCode = proposeDetailsElement.flowStatusCode;
            this.statusPublishCode = proposeDetailsElement.flowStatusPublishCode ?? '';
          } else {
            this.flowStatusCode = this.proposeDTO?.flowStatusCode;
            this.statusPublishCode = this.proposeDTO?.flowStatusPublishCode;
          }
          // this.employeeForm.patchValue(this.proposeDTO.proposeDetails[0].employee);
          // Map thông tin nhân viên
          const employeeData = this.proposeDTO.proposeDetails[0].employee;
          if (employeeData.posSeniority) {
            const years = Math.floor(employeeData.posSeniority / 12);
            const months = Math.floor(employeeData.posSeniority % 12);
            if (months === 0) {
              employeeData.posSeniorityName = years < 1 ? `` : `${years} năm`;
            } else {
              employeeData.posSeniorityName = years < 1 ? `${months} tháng` : `${years} năm ${months} tháng`;
            }
          }

          if (employeeData.seniority) {
            const years = Math.floor(employeeData.seniority / 12);
            const months = Math.floor(employeeData.seniority % 12);
            if (months === 0) {
              employeeData.seniorityName = years < 1 ? `` : `${years} năm`;
            } else {
              employeeData.seniorityName = years < 1 ? `${months} tháng` : `${years} năm ${months} tháng`;
            }
          }

          this.employeeForm.patchValue(employeeData);
          this.employeeForm.patchValue({
            levelName: employeeData.positionLevel
          });
          let disciplinaryDuration = this.employeeForm?.get('endOfDisciplinaryDuration')?.value || '';
          if (disciplinaryDuration) {
            const check2 = moment(disciplinaryDuration, Constants.FORMAT_DATE.DD_MM_YYYY).isValid();
            if (!check2) {
              disciplinaryDuration = moment(disciplinaryDuration).format(Constants.FORMAT_DATE.DD_MM_YYYY);
            }
            this.employeeForm.patchValue({ endOfDisciplinaryDuration: disciplinaryDuration });
          }
          this.proposeDetailId = this.proposeDTO.proposeDetails[0].id;
          const levelOrgPathId = employeeData?.orgPathId?.split('/');
          this.onChangeUnitDiff(+levelOrgPathId[3]);
          if (this.screenCode === Constants.ScreenCode.HRDV
            || this.screenCode === Constants.ScreenCode.LDDV
            || this.proposeDTO?.proposeUnitRequestName) {
            this.rotations = this.rotations.filter(
              (item) =>
                item.formRotationLabel === Constants.UnitTypeTGCV.SAMEUNIT
            );
          }


          this.rotations.forEach((item, i) => {
            if (i === proposeDetailsElement?.formRotation) {
              item.disabled = (this.typeAction === this.screenType.detail) || (this.isScreenHandle);
              item.formRotation = proposeDetailsElement?.formRotation?.toString();
              item.formRotationType = proposeDetailsElement?.formRotationType;
              item.contentTitleCode = proposeDetailsElement?.contentTitleCode;
              item.level = proposeDetailsElement?.contentLevel;
              if (i === 1) {
                item.contentUnit = {
                  orgId: +proposeDetailsElement?.contentUnitCode,
                  orgName: proposeDetailsElement?.contentUnitName,
                  pathId: proposeDetailsElement?.contentUnitPathId,
                  pathResult: proposeDetailsElement?.contentUnitPathName,
                  orgLevel: proposeDetailsElement?.contentUnitTreeLevel,
                };
              }
              item.contentUnitCode = proposeDetailsElement?.contentUnitCode;
              setTimeout(() => {
                this.onChangeUnit(proposeDetailsElement?.contentUnitCode?.toString(), +proposeDetailsElement?.formRotation, proposeDetailsElement?.contentTitleCode);

              }, 500);
              this.onChangeFormCode(item.formRotationType);

            } else {
              item.disabled = proposeDetailsElement?.formRotation ? true : false;
            }
          });
          // this.concurrentReason = proposeDetailsElement?.concurrentReason ?? '';
          this.proposeDetailForm.patchValue(
            {
              implementDate: proposeDetailsElement?.implementDate,
              // challengeTime: proposeDetailsElement?.challengeTime,
              minimumTime: proposeDetailsElement?.minimumTime,
              maximumTime: proposeDetailsElement?.maximumTime,
              timeType: proposeDetailsElement?.timeType ?? 1,
              concurrentReason: proposeDetailsElement?.concurrentReason,
            }
          )


          const statusObject = this.dataCommon?.flowStatusApproveConfigList?.find(
            (item) => item.key === this.flowStatusCode
          );
          const statusPubishObject = this.dataCommon.flowStatusPublishConfigList?.find(
            (item) => item.key === this.statusPublishCode
          );

          this.proposeUnitForm.patchValue({
            flowStatusName: statusObject?.role,
            formGroup: +this.proposeDTO.formGroup,
            id: this.proposeCategory === Constants.ProposeCategory.ELEMENT
            ? proposeDetailsElement?.proposeCode : this.proposeDTO.id,
            statusPublishName: statusPubishObject?.role,
            proposeUnitRequestName: this.proposeDTO.proposeUnitRequestName,
            optionNote: this.proposeDTO.optionNote,
          });
          this.onChangeProposeType(+this.proposeDTO.formGroup, proposeDetailsElement?.formRotationType);
          this.getCommentsUnit();

          this.orientationJob = proposeDetailsElement?.orientationJob ?? [];
          if (this.orientationJob?.length === 0 || !this.orientationJob) {
            this.addOrientationWork();
          }
          if (proposeDetailsElement?.employeeTraining && proposeDetailsElement?.employeeTraining?.length > 0) {
            this.employeeTraining = this.dataCommon?.employeeTrainingList?.map((item) => { return {
              proposeDetailId: this.proposeDetailId,
              commonCode: item.code,
              contentTraining: proposeDetailsElement?.employeeTraining?.find((e: EmployeeTraining) => e.commonCode === item.code)?.contentTraining ?? '',
              name: item.name,
              isCheck: proposeDetailsElement?.employeeTraining?.find((e: EmployeeTraining) => e.commonCode === item.code) ? true : false,
            }}) || [];
          }
          this.currentUnit = proposeDetailsElement.proposeAlternative?.filter((item: ProposeAlternativeRenewal) => item.unitType === 0) || [];
          this.proposedUnit = proposeDetailsElement.proposeAlternative?.filter((i: ProposeAlternativeRenewal) => i.unitType === 1)?.map((item) => { return {
            contentTitle: item?.replacementEmployee,
            concurrentEmployeeCode: item?.concurrentEmployeeCode,
            concurrentEmployeeName:  item?.concurrentEmployeeName,
            positionType: item?.positionType,
            unitType: 1,
            alternativePlan: item?.alternativePlan,
            replacementUnitName: item?.replacementUnitName,
            replacementEmployeeTitleName: item?.replacementEmployeeTitleName,
          } }) || [];
          this.currentUnit?.map((item) => {
            return {
                alternativeType: item?.alternativeType,
                replacementEmployeeCode: item?.replacementEmployeeCode || null,
                replacementEmployeeName:
                  (item?.replacementEmployeeName || null),
                replacementEmployeeTitleCode: item.replacementEmployeeCode ? +item?.replacementEmployeeTitleCode : null,
                replacementEmployeeTitleName: item?.replacementEmployeeTitleName,
                replacementEmployee: item?.replacementEmployee,
            }
          })

          if (this.currentUnit?.length === 0 || !this.currentUnit) {
            this.addCurrentUnit()
          }
          if (proposeDetailsElement?.proposePerformanceResults?.length > 0) {
            this.proposePerformanceResults = proposeDetailsElement.proposePerformanceResults;
            this.proposePerformanceResults?.forEach((item) => {
              item.disabled = this.typeAction === ScreenType.Detail;
            });
            this.sumProposePerformanceResults = proposeDetailsElement.totalDensity;
            this.sumPoint = proposeDetailsElement.totalPoint;
          }
          //
          const unitFeedback = proposeDetailsElement.unitFeedback;
          if (this.dataCommon?.unitFeedbackList && this.dataCommon?.unitFeedbackConfigList) {
            const unitFeedbackList = this.dataCommon?.unitFeedbackList || [];
            const unitFeedbackConfigList = this.dataCommon?.unitFeedbackConfigList || [];
            this.listCommentsUnitCharge = unitFeedbackList.concat(unitFeedbackConfigList);

            this.listCommentsUnitCharge.forEach((item) => {
              if (item.code) {
                item.strength = unitFeedback?.find((i: UnitFeedbackRenewal) => i.configCode === item.code)?.strength;
                item.weakness = unitFeedback?.find((i: UnitFeedbackRenewal) => i.configCode === item.code)?.weakness;
              }
            })
            this.strategicCapacity = this.listCommentsUnitCharge?.filter((item) => item.code === 'STRATEGIC_CAPACITY' || item.commonCode === 'STRATEGIC_CAPACITY');
            this.learningCapacity = this.listCommentsUnitCharge?.filter((item) => item.code === 'LEARNING_CAPACITY' || item.commonCode === 'LEARNING_CAPACITY');
            this.performanceCapacity = this.listCommentsUnitCharge?.filter((item) => item.code === 'PERFORMANCE_CAPACITY' || item.commonCode === 'PERFORMANCE_CAPACITY');
          }
          const proposeStatement = this.proposeDTO?.proposeStatementList || [];
          const reportHRDV = proposeStatement?.find(
            (item) => !item?.typeStatement
          );
          if (reportHRDV) {
            this.formTTQD.patchValue({
              statementNumber: reportHRDV?.statementNumber,
              submissionDate: reportHRDV?.submissionDate,
              approveNote: this.proposeDTO?.approveNote,
            });
          }


          const proposeAttachmentFiles = proposeDetailsElement.proposeAttachmentFiles;
          this.consultationComment = proposeAttachmentFiles?.consultationComment;
          this.different = proposeAttachmentFiles?.different;
          this.personalReview = proposeAttachmentFiles?.personalReview;
          this.reportBranchMeeting = proposeAttachmentFiles?.reportBranchMeeting;
          this.statement = proposeAttachmentFiles?.statement;
          this.targetAppoint = proposeAttachmentFiles?.targetAppoint;
          if (proposeAttachmentFiles?.docId) {
            this.nameFileListCreate = proposeAttachmentFiles?.fileName;

            this.fileListCreate = [
              ...this.fileListCreate,
              {
                uid: proposeAttachmentFiles.docId,
                name: proposeAttachmentFiles?.fileName,
                status: 'done',
                id:proposeAttachmentFiles?.id,
              },
            ];
          }

          this.displayReport = this.proposeDTO?.externalApprove;
          this.getReportFirst = proposeDetailsElement?.externalApprove;
          const issueObject = this.dataCommon?.issueLevelList?.find(
            (i) => i.code === this.proposeDTO?.issueLevelCode
          );
          this.issueLevelName = issueObject?.name ?? '';

          const withdrawNote = data?.withdrawNoteConfigList.find(
            (i: StatusList) => i.key  === this.proposeDTO?.proposeDetails[0]?.withdrawNote?.toString())?.role;
          this.proposeUnitForm.controls['withdrawNote'].setValue(withdrawNote);
          const proposeStatementDetails = proposeDetailsElement?.proposeStatement || [];
          if (proposeStatementDetails?.length > 0) {
            proposeStatementDetails?.forEach((item: ProposeStatementDTO) => {
              if (item?.typeStatement === Constants.TypeStatement.EDITHRDV) {
                this.formReportElement.patchValue({
                  statementNumber: item?.statementNumber,
                  submissionDate: item?.submissionDate,
                  approveNote: this.proposeDTO?.approveNote,
                });
              }
            });
          }
          this.fileListSelectTTElement = [];
              this.fileElementObj =
                this.screenCode === Constants.ScreenCode.HRDV
                  ? proposeDetailsElement?.fileDTOS?.find(
                      (item: FileDTOList) =>
                        item.type === Constants.TypeStatementFile.STATEMENTEDIT
                    )
                  : proposeDetailsElement?.fileDTOList?.find(
                      (item: FileDTOList) =>
                        item.type === Constants.TypeStatementFile.STATEMENTEDIT
                    );
              if (this.fileElementObj?.docId && this.fileElementObj?.fileName) {
                this.fileListSelectTTElement = [
                  ...this.fileListSelectTTElement,
                  {
                    uid: this.fileElementObj?.docId,
                    name: this.fileElementObj?.fileName,
                    status: 'done',
                    id: this.fileElementObj?.id,
                  },
                ];
                this.fileElement.uid = this.fileElementObj?.docId;
                this.fileElement.fileName = this.fileElementObj?.fileName;
              }
        }
        this.isLoading = false;
      },
    (e) => {
      this.getMessageError(e);
      this.isLoading = false;
    });
    this.subs.push(subs);
  }

  //hiển thị xuất tờ trình và ý kiến phê duyệt LDDV (tờ trình của đơn vị)
  displayExportApproveLDDV() {
    return Constants.ListScreenCodeUnit.includes(this.screenCode);
  }

  //hiển thị nút ý kiến khác KSPTNL
  checkIdeaApproveKSPTNL() {
    const list = [Constants.ProposeCategory.ELEMENT, Constants.ProposeCategory.GROUP];
    return (
      this.screenCode === this.screenCodeConst.KSPTNL &&
      this.flowStatusCode === Constants.FlowCodeConstants.CHODUYET7 &&
      list.indexOf(this.proposeCategory) > -1
    );
  }

  otherIdeaElementKSPTNL() {
    const note = this.consultationScreenComponent?.noteKSPTNL || '';
    if (!note) {
      this.scrollToValidationSection('idea', 'development.commonMessage.approveIdeaRequired');
      return;
    } else {
      this.modalRef = this.modal.confirm({
        nzTitle: this.translate.instant('development.approvement-propose.messages.otherIdeaElementConfirm'),
        nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
        nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
        nzClassName: Constants.ClassName.LD_CONFIRM,
        nzOnOk: () => {
          this.isLoading = true;
          this.service.otherIdeaElementKSPTNL(this.idGroup, this.proposeDetailId, note).subscribe(
              () => {
                this.toastrCustom.success(this.translate.instant('development.approvement-propose.messages.otherIdeaSuccess'));
                this.back();
                this.isLoading = false;
              },
              (e) => {
                this.getMessageError(e);
                this.isLoading = false;
              }
            );
        },
      });
    }
  }

  //trả lại đề xuất
  checkReturnProposeButton() {
    return Constants.ReturnProposeStatus.indexOf(this.flowStatusCode) > -1
      && this.proposeUnitForm.get('proposeUnitRequestName')?.value
      && this.screenCode === Constants.ScreenCode.PTNL
      && this.typeAction === this.screenTypeConst.Detail;
  }

  showModalReturnPropose() {
    this.modalRef = this.modal.create({
      nzTitle: this.translate.instant('development.proposed-unit.section.returnPropose'),
      nzWidth: '75vw',
      nzContent: ReturnProposeComponent,
      nzComponentParams: {
        proposeDetailId: this.proposeDetailId,
      },
      nzFooter: null,
    });
    this.modalRef.componentInstance.closeEvent.subscribe((data:boolean) => {
      if (data) {
        this.isLoading = true;
        this.modalRef?.componentInstance.returnPropose.subscribe((body: ReturnPropose) => {
          const sub = this.service.returnPropose(body).subscribe(() => {
            this.toastrCustom.success(this.translate.instant(Constants.KeyCommons.MESSAGES_SUCCESS2));
            this.router.navigateByUrl(Utils.getUrlByFunctionCode(this.functionCode));
            this.isLoading = false;
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          });
          this.subs.push(sub);
          this.modalRef?.destroy();
        })
      } else {
        this.modalRef?.destroy();
      }
    });
  }

  //validate các trường khi bấm gửi duyệt và ghi nhận đồng ý của HRDV, lưu của PTNL
  validateSubmitForm() {
    this.isSubmitted = true;
    const rotation = this.rotations.find((item) => item.formRotation);
    if (
      !rotation?.formRotationType || (!rotation?.contentUnitCode && !rotation?.contentUnit) || !rotation?.contentTitleCode
    ) {
      this.scrollToValidationSection('formRotation', 'development.commonMessage.validateProposeInfo');
      return false;
    }
    if (!this.proposeDetailForm.valid) {
      this.scrollToValidationSection('proposeDetailForm', 'development.commonMessage.validateReportUnit');
      return false;
    }
    if (this.orientationJob?.filter((item) => item.orientationJob?.trim() === '').length > 0) {
      this.scrollToValidationSection('infoPropose', 'development.commonMessage.E011');
      return false;
    }
    if (this.proposedUnit?.length > 0 && this.proposedUnit?.filter((item) => !item.alternativePlan)?.length > 0) {
      this.scrollToValidationSection('proposedUnitTable', 'development.commonMessage.E015');
      return false;
    }
    if (this.listCommentsUnitCharge.filter((item) => item.commonCode && (!item.weakness?.trim() || !item.strength?.trim())).length > 0) {
      this.scrollToValidationSection('commentsUnitCharge', 'development.commonMessage.E012');
      return false;
    }
    if (this.fileListCreate.length === 0) {
      this.scrollToValidationSection('attachments', 'development.commonMessage.E014');
      return false;
    }
    if (!this.personalReview && !this.different && !this.targetAppoint
      && !this.reportBranchMeeting && !this.consultationComment) {
        this.scrollToValidationSection('attachments', 'development.commonMessage.validateFileCheckbox');
        return false;
      }
    return true;
  }

  //body lưu đề xuất
  payloadSubmitForm(isSave: boolean) {
    const rotation = this.rotations.find((item) => item.formRotation);
    const proposeDetail = { ...cleanDataForm(this.proposeDetailForm) }
    proposeDetail.employee = { ...cleanDataForm(this.employeeForm) };
    proposeDetail.formGroup = (this.proposeUnitForm.controls['formGroup'].value).toString();
    delete proposeDetail.employee.partyDate;
    proposeDetail.formRotation = rotation?.formRotation ? rotation.formRotation : null;
    proposeDetail.formRotationType = rotation?.formRotationType;
    let formCode = this.dataCommon.formGroupMultipleList?.find(
      (item) => item.formCode === rotation?.formRotationType);
    proposeDetail.formRotationCode = formCode?.code;

    let contentUnit: OrgItem | null;
    if (rotation?.formRotation === Constants.FormRotationTitle.SAME_TITLE) {
      contentUnit = this.listInUnit?.find((item) => item?.value === rotation.contentUnitCode)?.valueItem ?? null;
    } else {
      contentUnit = rotation?.contentUnit ?? null;
    }
    if (contentUnit) {
      proposeDetail.contentUnitCode = contentUnit?.orgId?.toString();
      proposeDetail.contentUnitName = contentUnit?.orgName;
      proposeDetail.contentUnitPathId = contentUnit?.pathId;
      proposeDetail.contentUnitPathName = contentUnit?.pathResult;
      proposeDetail.contentUnitTreeLevel = contentUnit?.orgLevel;
      proposeDetail.contentLevel = rotation?.level;
    }
    proposeDetail.contentTitleCode = rotation?.contentTitleCode;
    proposeDetail.contentTitleName = this.jobDTOSApi.find(
      (item) => item.jobId === proposeDetail.contentTitleCode
    )?.jobName;
    proposeDetail.concurrentReason = this.proposeDetailForm.get('concurrentReason')?.value;

    proposeDetail.proposePerformanceResults = [...this.proposePerformanceResults];
    if (this.proposePerformanceResults.length === 1 && !this.proposePerformanceResults[0].targetAssigned) {
      proposeDetail.proposePerformanceResults = undefined;
    }
    proposeDetail.totalPoint = this.sumPoint;
    proposeDetail.totalDensity = this.sumProposePerformanceResults;
    proposeDetail.groupByKSPTNL = false;
    proposeDetail.isGroup = false;
    proposeDetail.id = this.proposeDetailId;
    proposeDetail.orientationJob = [...this.orientationJob];
    const proposedUnitSave = this.proposedUnit.map((item) => {
      return {
        replacementEmployee: item?.contentTitle,
        concurrentEmployeeCode: item?.concurrentEmployeeCode,
        concurrentEmployeeName:  item?.concurrentEmployeeName,
        positionType: item?.positionType,
        unitType: item?.unitType,
        alternativePlan: item?.alternativePlan,
        replacementUnitName: item?.replacementUnitName,
        replacementEmployeeTitleName: item?.replacementEmployeeTitleName,
      }
    }) || [];
    proposeDetail.proposeAlternative = [...this.currentUnit, ...proposedUnitSave];
    proposeDetail.employeeTraining = this.employeeTraining.filter((item) => item.isCheck)?.map((i) => {return {
      proposeDetailId: i.proposeDetailId,
      commonCode: i.commonCode,
      contentTraining: i.contentTraining,
    }});
    proposeDetail.unitFeedback = this.listCommentsUnitCharge.filter((item) => item.commonCode)?.map((i) => {return {
      configCode: i.code,
			proposeDetailId: this.proposeDetailId,
			weakness: i?.weakness,
		  strength: i?.strength,
    }} );

    proposeDetail.proposeAttachmentFiles = {
      docId: this.fileListCreate.length > 0 ? this.fileListCreate[0].uid : '',
      fileName: this.fileListCreate.length > 0 ? this.fileListCreate[0].name : '',
      personalReview: this.personalReview,
      different: this.different,
      targetAppoint: this.targetAppoint,
      reportBranchMeeting: this.reportBranchMeeting,
      consultationComment: this.consultationComment,
      statement: this.statement
    };
    proposeDetail.flowStatusCode = this.proposeDTO ? this.proposeDTO.flowStatusCode : null;
    proposeDetail.externalApprove = this.displayReport;
    proposeDetail.conditionViolationCode = this.violationCode;
    proposeDetail.conditionViolations = this.conditionViolation;
    if (this.proposeDTO) {
      const proposeDetailsElement = this.proposeDTO.proposeDetails[0];
      proposeDetail.proposeComment = proposeDetailsElement.proposeComment;
      proposeDetail.sameDecision = proposeDetailsElement.sameDecision ?? null;
      proposeDetail.isIncome = proposeDetailsElement.isIncome ?? null;
      proposeDetail.isInsurance = proposeDetailsElement.isInsurance ?? null;
      proposeDetail.isDecision = proposeDetailsElement.isDecision ?? null;
      proposeDetail.issuedLater = proposeDetailsElement.issuedLater ?? null;
    }
    const paramProposed = {
      id: this.proposeDTO?.id ?? 0,
      proposeDetails: [proposeDetail],
      proposeUnitRequest: this.proposeDTO?.proposeUnitRequest,
      proposeUnitRequestName: this.proposeDTO?.proposeUnitRequestName,
      relationType: this.proposeDTO?.relationType,
      flowStatusCode: this.proposeDTO?.flowStatusCode,
      formGroup: this.proposeDTO?.formGroup,
      isGroup: this.proposeDTO?.isGroup,
      groupByKSPTNL: this.proposeDTO?.groupByKSPTNL,
      externalApprove: this.displayReport,
      sentDate: this.proposeDTO?.sentDate,
      approveNote: this.checkDisplayReportEdit()
        ? this.formReportElement.get('approveNote')?.value
        : this.formTTQD.get('approveNote')?.value,
    }
    return isSave ? paramProposed : [paramProposed];
  }

  //check ý kiến phê duyệt LDDV ban đầu
  checkApproveNoteOrigin() {
    return !this.displayReport
      && !this.checkDisplayReportEdit()
      && !this.proposeDTO?.proposeDetails[0]?.hasAgreeUnit
      && Constants.StatusApproveSendProposeMany.includes(this.flowStatusCode);
  }

  //check hiển thị ý kiến phê duyệt của LDDV (tờ trình chỉnh sửa)
  //hiển thị trong màn cập nhật HRDV hoặc màn chi tiết nhưng phải có ý kiến mới và chưa được lưu vào log
  checkApproveNote() {
    return !this.displayReport
      && ((this.screenCode === Constants.ScreenCode.HRDV && this.typeAction === this.screenType.update)
        || (this.screenCode === Constants.ScreenCode.HRDV && this.typeAction === this.screenType.detail
          && this.formReportElement.get('approveNote')?.value))
      && this.flowStatusCode !== this.flowCodeConst.HANDLING5B
      && this.flowStatusCode !== this.flowCodeConst.CANCELPROPOSE;
  }

  disabledEndDateSubmission = (endDate: Date): boolean => {
    return endDate >=  new Date();
  };

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'development.proposed-unit.proposeLogsTable.time',
          field: 'actionDate',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
          tdTemplate: this.dateLog
        },
        {
          title: 'development.proposed-unit.proposeLogsTable.personAction',
          field: 'userName',
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
        },
        {
          title: 'development.proposed-unit.proposeLogsTable.action',
          field: 'action',
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
        },
        {
          title: 'development.proposed-unit.proposeLogsTable.description',
          field: 'comment',
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 200,
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1,
    };
  }

  getLog(pageIndex: number){
    this.pageNumber = pageIndex - 1;
    this.isLoading = true;
    const ids = this.proposeCategory === Constants.ProposeCategory.ELEMENT ? this.proposeDetailId : this.proposeId;
    if (ids) {
      const log = this.service.getLogsPage(ids, this.proposeCategory, this.screenCode, this.pageNumber, this.pageSize)
        .subscribe((logs) => {
          this.proposeLogs = logs?.data?.content || [];
          this.tableConfig.total = logs?.data?.totalElements || 0;
          this.tableConfig.pageIndex = pageIndex;
          this.isLoading = false;
        }, (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        })
      this.subs.push(log);
    }
  }

  //tờ trình chỉnh sửa
  checkDisplayReportEdit() {
    return !this.displayReport &&
        (
          ((Constants.StatusApproveSendProposeMany.includes(this.flowStatusCode)
            && this.proposeDTO?.proposeDetails[0]?.hasAgreeUnit)
          && this.screenCode === Constants.ScreenCode.HRDV
          && this.typeAction === this.screenType.update) ||
          this.checkDataReportEdit()
        );
  }

  //check xem tờ trình chỉnh sửa có dữ liệu không
  checkDataReportEdit() {
    return this.formReportElement.get('statementNumber')?.value
      || this.formReportElement.get('submissionDate')?.value
      || this.formReportElement.get('approveNote')?.value
      || this.fileListSelectTTElement.length === 1;
  }

  //validate điền thông tin của tờ trình chỉnh sửa
  validApproveEditReport() {
    return (
      this.formReportElement.get('statementNumber')?.value &&
      this.formReportElement.get('submissionDate')?.value &&
      this.formReportElement.get('approveNote')?.value &&
      this.fileListSelectTTElement.length === 1
    );
  }

  //check view đính kèm tờ trình chỉnh sửa của đơn vị
  checkViewFileElement() {
    return (this.screenCode === Constants.ScreenCode.HRDV && (this.fileElement?.uid && this.fileElement?.fileName)
      && this.typeAction === this.screenType.detail)
      || ((this.screenCode === Constants.ScreenCode.PTNL || this.screenCode === Constants.ScreenCode.KSPTNL)
        && this.checkDataReportEdit());
  }

  //upload file tờ trình chỉnh sửa
  beforeUploadTT = (file: File | NzUploadFile): boolean => {
    this.isLoading = true;
    const formData = new FormData();
    formData.append('files', file as File);
    formData.append('proposeDetailId', new Blob([JSON.stringify(this.proposeId)], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('type', new Blob([Constants.TypeStatementFile.STATEMENTEDIT], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('groupProposeTypeId', new Blob(['1'], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('isIn', new Blob([JSON.stringify(this.displayReport)], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    this.service.uploadFile(formData).subscribe(
      (res) => {
        this.fileListSelectTTElement = [
          ...this.fileListSelectTTElement,
          {
            uid: res.data?.docId,
            name: res.data?.fileName,
            status: 'done',
            id: res.data?.id,
          },
        ];
        this.validApproveEditReport();
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    return false;
  };

  //xóa file tờ trình chỉnh sửa
  doRemoveFileTT = (file: NzUploadFile): boolean => {
    this.isLoading = true;
    if (this.fileListSelectTTElement.find((item) => item.uid === file.uid)){
      const idDelete = this.fileListSelectTTElement.find((item) => item.uid === file.uid)?.['id'];
      this.service.deleteUploadFile(+idDelete).subscribe(
        (res) => {
          this.fileListSelectTTElement = this.fileListSelectTTElement.filter((item) => item['id'] !== idDelete);
          this.validApproveEditReport();
          this.isLoading = false;
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        }
      );
    }
    return false;
  };

  downloadFile2(uid: string | number, name: string) {
    this.isLoading = true;
    this.service.downloadFile(+uid, name).subscribe(
      () => {
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    )
  }

  onChangeEmployeeTraining(item: EmployeeTraining) {
    if (!item?.isCheck) {
      item.contentTraining = '';
    }
  }
}
