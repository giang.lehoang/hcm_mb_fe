import {
  Component,
  EventEmitter,
  Injector,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Constants } from '@hcm-mfe/learn-development/data-access/common';
import { WithdrawPropose } from '@hcm-mfe/learn-development/data-access/models';
import { ProposedUnitService } from '@hcm-mfe/learn-development/data-access/services';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import {Subscription} from "rxjs";

@Component({
  selector: 'app-withdraw-propose',
  templateUrl: './withdraw-propose.component.html',
  styleUrls: ['./withdraw-propose.component.scss'],
})
export class WithdrawProposeComponent extends BaseComponent implements OnInit {
  form: FormGroup;
  isSubmitted = false;
  modalRef?: NzModalRef;
  subs: Subscription[] = [];
  @Input() typeWithdraw: number = 0;
  @Input() ids: number[] = [];
  @Input() idFlagComponent: number[] = [];
  @Input() isApproveHCMBefore: boolean = false;
  @Output() closeEvent = new EventEmitter<boolean>();
  @Output() sendBody = new EventEmitter<WithdrawPropose>();
  typeWithDrawConst = Constants.TypeWithdrawPropose;
  fileAttachReport: NzUploadFile[] = [];

  constructor(injector: Injector, private service: ProposedUnitService) {
    super(injector);
    this.form = new FormGroup({
      proposeWithdrawType: new FormControl(null, [Validators.required]),
      reason: new FormControl(null, [
        Validators.required,
        Validators.maxLength(200),
      ]),
      isApproveHcm: new FormControl(true, [Validators.required]),
      statementNumber: new FormControl(null),
      statementDate: new FormControl(null),
      attachFile: new FormControl(null),
    });
  }

  closeModal() {
    this.closeEvent.emit(false);
  }

  ngOnInit() {
    this.form.get('isApproveHcm')?.setValue(this.isApproveHCMBefore);
    if (this.typeWithdraw === this.typeWithDrawConst.ELEMENT) {
      this.form.get('proposeWithdrawType')!.disable();
      this.form.get('proposeWithdrawType')?.setValue(0);
      this.toastrCustom.info(this.translate.instant('development.withdrawPropose.withdrawElementMessage'));
    }
    if (this.checkDisableApproveHCM()) {
      this.form.get('isApproveHcm')?.setValue(false);
    }
  }

  checkDisableApproveHCM() {
    return (
      this.typeWithdraw === this.typeWithDrawConst.ELEMENT &&
      !this.isApproveHCMBefore
    );
  }

  submit() {
    this.isSubmitted = true;
    const body = {
      ids:
        this.typeWithdraw === this.typeWithDrawConst.ORIGINAL
          ? this.ids
          : this.idFlagComponent,
      proposeType: this.typeWithdraw,
      proposeWithdrawType: this.form.get('proposeWithdrawType')?.value,
      reason: this.form.get('reason')?.value,
      isApproveHcm: this.form.get('isApproveHcm')?.value,
      statementNumber: this.form.get('statementNumber')?.value,
      statementDate: this.form.get('statementDate')?.value,
    };
    if (body.isApproveHcm === false) {
      if (this.fileAttachReport.length === 0) {
        this.toastrCustom.error(
          this.translate.instant(
            'development.withdrawPropose.validateAttachReport'
          )
        );
        return;
      }
    }
    if (this.form.valid) {
      this.closeEvent.emit(true);
      this.sendBody.emit(body);
    } else {
      this.toastrCustom.error(
        this.translate.instant('development.withdrawPropose.validateCommon')
      );
    }
  }

  beforeUploadApproveLDDV = (file: File | NzUploadFile): boolean => {
    const formData = new FormData();
    formData.append(
      'ids',
      new Blob(
        [
          JSON.stringify(
            this.typeWithdraw === this.typeWithDrawConst.ORIGINAL
              ? this.ids
              : this.idFlagComponent
          ),
        ],
        { type: Constants.CONTENT_TYPE.APPLICATION_JSON }
      )
    );
    formData.append(
      'type',
      new Blob([Constants.TypeStatementFile.STATEMENTWITHDRAW], { type: Constants.CONTENT_TYPE.APPLICATION_JSON })
    );
    formData.append('files', file as File);
    formData.append('isIn', new Blob(['true'], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append(
      'groupProposeTypeId',
      new Blob(
        [
          JSON.stringify(
            this.typeWithdraw === this.typeWithDrawConst.ORIGINAL
              ? Constants.ProposeTypeID.ORIGINAL
              : Constants.ProposeTypeID.ELEMENT
          ),
        ],
        { type: Constants.CONTENT_TYPE.APPLICATION_JSON }
      )
    );
    this.isLoading = true;
    const subs = this.service.uploadFileWithdraw(formData).subscribe(
      (res) => {
        this.fileAttachReport = [
          ...this.fileAttachReport,
          {
            uid: res.data[0]?.docId,
            name: res.data[0]?.fileName,
            status: 'done',
            id: res.data[0]?.id,
          },
        ];
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    this.subs.push(subs)
    // this.fileAttachReport.forEach((file: NzUploadFile) => {
    //   formData.append('file', file as NzSafeAny);
    // });
    return false;
  };

  override ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
