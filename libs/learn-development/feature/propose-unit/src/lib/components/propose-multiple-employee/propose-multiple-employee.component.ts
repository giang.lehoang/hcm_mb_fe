import { Component, Injector, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import {
  Constants,
  FunctionCode,
  maxInt32,
  Mode,
  Scopes,
  userConfig
} from "@hcm-mfe/learn-development/data-access/common";
import {EmployeeModal, JobDTOS, ListTitle, ParamSearchPosition} from "@hcm-mfe/learn-development/data-access/models";
import {ProposedUnitService, ProposeMultipleEmployeeService} from "@hcm-mfe/learn-development/data-access/services";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import { MBTableConfig, Pagination } from "@hcm-mfe/shared/data-access/models";
import { NzSafeAny } from "ng-zorro-antd/core/types";
import { NzModalRef } from 'ng-zorro-antd/modal';

@Component({
  selector: 'app-propose-multiple-employee',
  templateUrl: './propose-multiple-employee.component.html',
  styleUrls: ['./propose-multiple-employee.component.scss'],
})
export class ProposeMultipleEmployeeComponent extends BaseComponent implements OnInit {
  mode = Mode.ADD;
  paramsSearchEmp: FormGroup;
  listStatus = [];
  listEmployeeModal = [];
  proposeDetailId?: number;
  typeConsultation = 1;
  jobDTOS: JobDTOS[] = [];
  isEmp?: boolean;
  constructor(private readonly service: ProposeMultipleEmployeeService, private readonly servicePropose: ProposedUnitService, injector: Injector, readonly modalRef: NzModalRef) {
    super(injector);
    this.paramsSearchEmp = new FormGroup({
      employeeCode: new FormControl(''),
      employeeName: new FormControl(''),
      currentTitle: new FormControl(''),
      currentUnit: new FormControl(''),

    });
  }

  params = {
    startRecord: 0,
    pageSize: 15,
  };
  //
  dataTable: EmployeeModal[] = [];
  checked = false;
  quantityMap: Map<number, EmployeeModal> = new Map<number, EmployeeModal>();
  countView = 0;
  tableConfig!: MBTableConfig;
  pagination = new Pagination();
  heightTable = { x: '30%', y: '18em'};
  @ViewChild('checkboxHeader', {static: true}) checkboxHeader!: TemplateRef<NzSafeAny>;
  @ViewChild('checkbox', {static: true}) checkbox!: TemplateRef<NzSafeAny>;
  functionCode: string = FunctionCode.HR_PERSONAL_INFO;
  scope = Scopes.VIEW

  ngOnInit(): void {
    this.initTable();
    this.search(1);
  }

  // Lay cd theo don vi
  onChangeUnit(orgId: string) {
    this.jobDTOS = [];
    if (orgId) {
      this.isLoading = true;
      const paramPosition: ParamSearchPosition = {
        orgId: orgId,
        jobId: '',
        status: 1,
        type: Constants.TYPE_SEARCH_POSITION,
        page: 0,
        size: maxInt32,
      };
      const sub = this.servicePropose.getPositionByUnit(paramPosition).subscribe(
        (res) => {
          const listPositionParent = res?.data?.content;
          this.jobDTOS = listPositionParent?.map((item: ListTitle) => {
            return { jobId: item.jobId, jobName: item.jobName };
          }) || [];
          this.isLoading = false;
        },
        (e) => {
          this.isLoading = false;
          this.getMessageError(e);
        }
      );
    }
  }

  search(firstPage: number) {
    this.isLoading = true;
    this.params.startRecord = firstPage - 1;
    const searchParams = {
      keySearch: this.paramsSearchEmp.get('employeeCode')?.value || this.paramsSearchEmp.get('employeeName')?.value,
      orgId: this.paramsSearchEmp.get('currentUnit')?.value?.orgId ,
      jobIds: this.paramsSearchEmp.get('currentTitle')?.value,
      startRecord: this.params.startRecord*15,
      pageSize: this.params.pageSize,
      status: Constants.EmployeeStatus.WORKING,
    };

    this.service.searchEmployee(searchParams).subscribe(
      (res) => {
        this.dataTable = res.data?.listData;
        this.checked = true;
        if (this.countView === 0) {
          ++this.countView;
          this.fillDataShowModal();
        }
        this.dataTable?.forEach((item) => {
          if (!this.quantityMap.has(item?.empId)) {
            this.checked = false;
          } else {
            this.quantityMap.set(item.empId, item);
          }
        });
        if (!this.dataTable?.length) {
          this.checked = false;
        }
        this.tableConfig.total = res?.data?.count || 0;
        this.tableConfig.pageIndex = firstPage;
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
        this.toastrCustom.error(this.translate.instant('development.interview-config.notificationMessage.errorApi'));
        this.dataTable = [];
      }
    );
  }

  addData() {
    const itemSelect: EmployeeModal[] = [];
    this.quantityMap.forEach((value) => {
      itemSelect.push(value);
    });
    this.modalRef.close(itemSelect);
  }

  selectEmp(item: any) {
    if (this.isEmp) {
      this.modalRef.close([item]);
    }
  }

  fillDataShowModal() {
    if (this.listEmployeeModal?.length > 0) {
      this.listEmployeeModal?.forEach((item) => this.updateCheckedSet(item, true));
      this.refreshCheckedStatus();
    }
  }

  onAllChecked(checked: boolean): void {
    this.dataTable.forEach((item) => this.updateCheckedSet(item, checked));
    this.refreshCheckedStatus();
  }

  refreshCheckedStatus(): void {
    this.checked = this.dataTable.every(({ empId }) => this.quantityMap.has(empId));
  }

  updateCheckedSet(item: EmployeeModal, checked: boolean): void {
    if (checked) {
      this.quantityMap.set(item.empId, item);
    } else {
      this.quantityMap.delete(item.empId);
    }
  }

  onItemChecked(empId: number, checked: boolean): void {
    const item = this.dataTable.find((itemTable: EmployeeModal) => itemTable.empId === empId);
    this.updateCheckedSet(item!, checked);
    this.refreshCheckedStatus();
  }

  selectEmployee() {
    if (!this.quantityMap.size) {
      this.toastrCustom.error(this.translate.instant('modelOrganization.planYear.chooseMin'));
      return;
    }
    const itemEdit:  EmployeeModal[] = [];
    this.quantityMap.forEach((value) => {
      itemEdit.push(value);
    });

    this.router.navigate([this.router.url, 'update'], {
      skipLocationChange: true,
      queryParams: {
        dataInterview: JSON.stringify(itemEdit),
      },
    });
  }

  initTable() {
    this.tableConfig = {
      headers: [
        {
          title: 'development.approvement-propose.search.merge',
          field: 'select',
          fixed: true,
          fixedDir: 'left',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 40,
          thTemplate: this.checkboxHeader,
          tdTemplate: this.checkbox,
          show: !this.isEmp,

        },
        {
          title: 'development.interview-config.colTable.codeEmployee',
          field: 'empCode',
          width: 80,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.interview-config.colTable.nameEmployee',
          field: 'fullName',
          width: 80,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.interview-config.colTable.unitName',
          field: 'orgName',
          width: 80,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.interview-config.colTable.titleName',
          field: 'jobName',
          width: 80,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.interview-config.colTable.dateIn',
          field: 'joinCompanyDate',
          width: 50,
          fixed: true,
          fixedDir: 'left',
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: userConfig.pageSize,
      pageIndex: 1,
    };
  }
}
