import { ProposeMultipleComponent } from './propose-multiple/propose-multiple.component';
import { ProposeSingleComponent } from './propose-single/propose-single.component';
import { ConsultationScreenComponent } from './consultation-screen/consultation-screen.component';
import { SalaryInterviewComponent } from './salary-interview/salary-interview.component';
import { ResourceManagementComponent } from './resource-management/resource-management.component';
import { ProposeMultipleEmployeeComponent } from './propose-multiple-employee/propose-multiple-employee.component';
import { WithdrawProposeComponent } from './withdraw-propose/withdraw-propose.component';
import { ProposeTemplateComponent } from './propose-template/propose-template.component';
import { ProposePopupMergeComponent } from './propose-popup-merge/propose-popup-merge.component';
import { StopInterviewComponent } from './stop-interview/stop-interview.component';
import { ReturnProposeComponent } from './return-propose/return-propose.component';
import { ProposeUploadFileComponent } from './propose-upload-file/propose-upload-file.component';
import { CancelIssueComponent } from './cancel-issue/cancel-issue.component';
import { CancelPlanComponent } from './cancel-plan/cancel-plan.component';
import { IssueCorrectionComponent } from './issue-correction/issue-correction.component';
import { ProposedAppointmentRenewalCreateComponent } from './proposed-appointment-renewal-create/proposed-appointment-renewal-create.component';
import { ProposeTGCVComponent } from './propose-tgcv/propose-tgcv.component';
import { ProposeMultipleInterviewComponent } from './propose-multiple-interview/propose-multiple-interview.component';
import { ProposedAppointmentRenewalEmpComponent } from './proposed-appointment-renewal-emp/proposed-appointment-renewal-emp.component';
import { ExistingEmployeeComponent } from './existing-employee/existing-employee.component';
import {ModalTemplateProposeComponent} from "./modal-template-propose/modal-template-propose.component";
import {
  RecommendIndividualProposeComponent
} from "./recommend-individual-propose/recommend-individual-propose.component";

export const components = [
  ProposeMultipleComponent,
  ProposeSingleComponent,
  ConsultationScreenComponent,
  SalaryInterviewComponent,
  ResourceManagementComponent,
  ProposeMultipleEmployeeComponent,
  WithdrawProposeComponent,
  ProposeTemplateComponent,
  ProposePopupMergeComponent,
  StopInterviewComponent,
  ReturnProposeComponent,
  ProposeUploadFileComponent,
  CancelIssueComponent,
  CancelPlanComponent,
  IssueCorrectionComponent,
  ProposedAppointmentRenewalCreateComponent,
  ProposeTGCVComponent,
  ProposeMultipleInterviewComponent,
  ProposedAppointmentRenewalEmpComponent,
  ExistingEmployeeComponent,
  ModalTemplateProposeComponent,
  RecommendIndividualProposeComponent
];

export * from './propose-multiple/propose-multiple.component';
export * from './propose-single/propose-single.component';
export * from './consultation-screen/consultation-screen.component';
export * from './salary-interview/salary-interview.component';
export * from './resource-management/resource-management.component';
export * from './propose-multiple-employee/propose-multiple-employee.component';
export * from './withdraw-propose/withdraw-propose.component';
export * from './propose-template/propose-template.component';
export * from './stop-interview/stop-interview.component';
export * from './return-propose/return-propose.component';
export * from './propose-upload-file/propose-upload-file.component';
export * from './cancel-issue/cancel-issue.component';
export * from './cancel-plan/cancel-plan.component';
export * from './issue-correction/issue-correction.component';
export * from './proposed-appointment-renewal-create/proposed-appointment-renewal-create.component';
export * from './propose-tgcv/propose-tgcv.component';
export * from'./propose-multiple-interview/propose-multiple-interview.component';
export * from './proposed-appointment-renewal-emp/proposed-appointment-renewal-emp.component';
export * from './existing-employee/existing-employee.component';
export * from './modal-template-propose/modal-template-propose.component';
export * from './recommend-individual-propose/recommend-individual-propose.component';
