import {
  Component,
  EventEmitter,
  Injector,
  Input,
  OnChanges,
  OnInit,
  Output,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NavigationExtras } from '@angular/router';
import {
  Constants,
  FunctionCode,
  maxInt32,
  Scopes,
  ScreenType,
  userConfig
} from "@hcm-mfe/learn-development/data-access/common";
import {
  AttachFile, DataCommon,
  FileDTOList,
  FormTTQD, JobDTOS, ListFileTemplate, ListOrgItem, OrgEmployee, OrgItem, ParamSearchPosition, ProposeAlternative,
  ProposeLogs, ProposePerformanceResults, ProposeRotationReasons,
  RotationReasons
} from "@hcm-mfe/learn-development/data-access/models";
import { ProposedUnitService } from "@hcm-mfe/learn-development/data-access/services";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import { cleanDataForm } from "@hcm-mfe/shared/common/utils";
import { MBTableConfig, Pagination } from "@hcm-mfe/shared/data-access/models";
import * as FileSaver from 'file-saver';
import * as moment from 'moment';
import { NzSafeAny } from "ng-zorro-antd/core/types";
import { NzModalRef } from 'ng-zorro-antd/modal';
import { NzUploadFile } from "ng-zorro-antd/upload";
import { Subscription } from 'rxjs';
import { ProposeMultipleEmployeeComponent } from '../propose-multiple-employee/propose-multiple-employee.component';

@Component({
  selector: 'app-propose-single',
  templateUrl: './propose-single.component.html',
  styleUrls: ['./propose-single.component.scss'],
  host: { id: 'tab1' },
})
export class ProposeSingleComponent extends BaseComponent implements OnInit, OnChanges {
  @Input() dataCommon: DataCommon = {
    jobDTOS: [], // vị trí
    typeDTOS: [], // loại đề xuất
    formDisplaySearchDTOS: [], // hình thức
    proposeRotationReasonConfigDTOS: [], // lý do luân chuyển
    statusList: [],
    flowStatusApproveConfigList: [],
    flowStatusPublishConfigList: [],
    formGroupList: [],
    formGroupMultipleList: [],
    conditionViolationList: []
  };
  @Input() typeAction?: ScreenType;
  @Input() dataForm: any;
  @Input() proposeDTO: any;
  @Input() checkUnitRequest?: boolean;
  @Input() screenCode?: string;
  @Input() flowStatusCode: string = '';
  @Input() flowStatusCodeDisplay?: string;
  @Input() issueLevelName?: string;
  @Input() formGroupCreate?: number;
  @Input() showMinTime?: boolean;
  @Input() showMaxTime?: boolean;
  @Input() flowStatusPublishCode?: string;
  @Output() submitData: EventEmitter<any> = new EventEmitter<any>();
  @Output() submitDetailForm: EventEmitter<any> = new EventEmitter<any>();
  @Output() submitReport: EventEmitter<any> = new EventEmitter<any>();

  @Output() submitStatementDecision: EventEmitter<any> = new EventEmitter<any>();
  @Input() proposeDetailId: number | null = null;
  @Input() proposeCategory = '';
  @Input() proposeId?: number;
  @Input() stateUrl?: NavigationExtras;
  @Input() isScreenHandle = false;
  @Input() externalApproveParent = false;

  disableInput = false;
  displayReport = false;
  unitIssueLevel = this.translate.instant('development.proposed-unit.table.unit');

  rotationReasons: RotationReasons[] = [];
  proposePerformanceResults: ProposePerformanceResults[] = [];
  currentSumProposePerformanceResults = 0;
  sumProposePerformanceResults = 0;
  currentSumPoint = 0;
  sumPoint = 0;
  conditionViolation = [];
  violationCode = 0;
  modalRef?: NzModalRef;
  iconStatus = Constants.IconStatus.DOWN;
  isExpand = false;
  formRotationType = Constants.FormRotationType;
  disableShowMinTime = false;
  disableShowMaxTime = false;
  formTypeConst = Constants.FormGroupList;
  isChangeLevel = false;
  sortOrder = 0;
  getReportMany = false;
  subs: Subscription[] = [];
  @Input() proposeUnitRequestName = '';
  fileElement: AttachFile = {};
  fileParent: AttachFile = {};
  @Output() saveReport = new EventEmitter<boolean>();

  rotations: RotationReasons[] = [
    {
      formRotationLabel: 'Trong đơn vị',
      formRotation: null, // IN/OUT
      formRotationType: null, // DC/DD
      formRotationTitle: null,
      contentUnit: null,
      contentUnitCode: null,
      contentTitleCode: null,
      contentTitleName: null,
      contentUnitPathId: null,
      contentUnitPathName: null,
      contentUnitTreeLevel: null,
      disabled: true,
      level: null,
    },
    {
      formRotationLabel: 'Khác đơn vị',
      formRotation: null, // IN/OUT
      formRotationType: null, // DC/DD
      formRotationTitle: null,
      contentUnit: null,
      contentUnitCode: null,
      contentTitleCode: null,
      contentTitleName: null,
      contentUnitPathId: null,
      contentUnitPathName: null,
      contentUnitTreeLevel: null,
      disabled: true,
      level: null,
    },
  ];

  proposeAlternative: ProposeAlternative[] = [
    {
      id: null,
      proposeDetailId: null,
      alternativeType: null,
      estimatedDate: null,
      replacementEmployeeName: null,
      replacementEmployeeCode: null,
      replacementEmployeeTitleCode: null,
      replacementEmployeeTitleName: null,
      handoverEmployeeCode: null,
      handoverEmployeeName: null,
      handoverEmployeeTitleCode: null,
      handoverEmployeeTitleName: null,
      replacementEmployee: null,
      handoverEmployee: null,
      disabled: true,
    },
  ];

  // thông tin nhân viên
  employeeForm = this.fb.group({
    empIdp: null,
    orgId: null,
    empId: null,
    empCode: null,
    fullName: null,
    email: null,
    posId: null,
    posName: null,
    jobId: null,
    jobName: null,
    orgPathId: null,
    orgPathName: null,
    orgName: null,
    majorLevelId: null,
    majorLevelName: null,
    faculityId: null,
    faculityName: null,
    schoolId: null,
    schoolName: null,
    positionLevel: null,
    gender: null,
    joinCompanyDate: null,
    dateOfBirth: null,
    partyDate: null,
    partyOfficialDate: null,
    posSeniority: null,
    seniority: null,
    qualificationEnglish: null,
    facultyName: null,
    semesterGradeT1: null,
    semesterRankT1: null,
    semesterGradeT2: null,
    semesterRankT2: null,
    ratedCapacity: null,
    nearestDiscipline: null,
    endOfDisciplinaryDuration: null,
    userToken: null,
    treeLevel: null,
    posSeniorityName: null,
    seniorityName: null,
    levelName: null,
    concurrentTitleName: null,
    concurrentUnitPathName: null,
    concurrentTitleName2: null,
    concurrentUnitPathName2: null,
    periodNameT1: null,
    periodNameT2: null,
  });

  // form proposeDetail
  proposeDetailForm = this.fb.group({
    proposeID: null,
    empCode: null,
    formRotation: null,
    formRotationType: null,
    formRotationCode: null,
    formRotationTitle: null,
    contentUnitCode: null,
    contentTitleCode: null,
    contentTitleName: null,
    contentUnitPathId: null,
    contentUnitPathName: null,
    contentUnitTreeLevel: null,
    contentPositionCode: null,
    rotationFromDate: null,
    rotationToDate: null,
    rotationTime: [{ value: null, disabled: true }],
    employee: null, //formGroup
    proposeAlternative: null, //formGroup
    proposeComment: null, //formGroup

    employeeUnitPathId: null,
    employeeUnitPathName: null,
    conditionViolationCode: null,
    conditionViolations: null,
    //
    implementDate: [null, Validators.required],
    minimumTime: null,
    maximumTime: null,
    groupByKSPTNL: null,
    isGroup: null,
    isHo: null,
    formGroup: null,
    sameDecision: null,
    isIncome: null,
    isInsurance: null,
    isDecision: null,
    issuedLater: null,
  });
  // form nhận xét
  proposeCommentForm = this.fb.group({
    id: null,
    proposeDetailId: null,
    strengths: [null, Validators.required],
    weaknesses: [null, Validators.required],
    reviewCode: null,
    reviewDate: null,
    // conditionViolations: null,
  });
  //Phần sửa
  fileListSelectQD : NzUploadFile[] = [];
  fileListSelectTT : NzUploadFile[] = [];
  fileListSelectTTElement: NzUploadFile[] = [];

  paramTT = {
    statementNumber: null,
    decisionDate: null,
  };

  paramQD = {
    decisionNumber: null,
    decisionDate: null,
  };
  @Input() listInUnit?: ListOrgItem[];
  @Input() checkLogs: boolean | undefined;
  @Input() proposeDecisionStatement?: FormTTQD;

  formTTQD = new FormGroup({
    statementNumber: new FormControl(null, [Validators.required]),
    submissionDate: new FormControl(null, [Validators.required]),
    decisionNumber: new FormControl(null, [Validators.required]),
    signedDate: new FormControl(null, [Validators.required]),
    approveNote: new FormControl(null, [Validators.required]),
  });

  formReportElement = new FormGroup({
    statementNumber: new FormControl(null, [Validators.required]),
    submissionDate: new FormControl(null, [Validators.required]),
    approveNote: new FormControl(null, [Validators.required]),
    typeStatement: new FormControl(null),
  });

  proposeUnitForm = this.fb.group({
    id: null,
    isHo: null,
    proposeDetails: null, // formArray
    proposeStatement: null, // formGroup
    proposeDecision: null, //formGroup
    proposeLogs: null, //formArray
    proposeGroupingHistories: null, //formArray
    proposeUnitRequest: null,
    proposeUnitRequestName: [{ value: null, disabled: true }],
    originator: null,
    proposeType: null,
    relationType: null,
    externalApprove: null,
    status: null,
    flowStatusCode: null,
    statusPublishName: null,
    groupCode: null,
    sentDate: null,
    issueLevelCode: null,
    signerCode: null,
    approvalCode: null,
    signDate: null,
    approveDate: null,
    flowStatusName: null,
    formGroup: null,
    withdrawNote: null,
    handleCode: null,
    optionNote: null,
    isGroup: null,
    groupByKSPTNL: null,
  });

  issueForm = this.fb.group({
    effectiveDate: null,
    signedDate: null,
    signerFullName: null,
    comments: null,
  });
  scope: string = Scopes.VIEW;
  functionCode: string;
  isSubmitFormTTQD = false;
  rotationTimeView = false;

  contentTitleEmp?: string;
  differentUnits = '';
  inUnit = '';
  isSeeMore = false;
  listLevel = Constants.Level;
  planAlternative = Constants.PlanAlternative;
  typeEmpAlternative = Constants.TypeEmpAlternative;
  noteLDDV = '';
  isSubmitPropose = false;
  formRotationTitle = Constants.FormRotationTitle;
  flowCodeConst = Constants.FlowCodeConstants;
  hasAgreeUnit = false;
  fileStatement?: FileDTOList = {};

  //log
  proposeLogs: ProposeLogs[] = [];
  heightTable = { x: '50vw', y: '16em'};
  tableConfig!: MBTableConfig;
  pagination = new Pagination();
  @ViewChild('dateLog', {static: true}) dateLog!: TemplateRef<NzSafeAny>;
  pageNumber = 0;
  pageSize =  userConfig.pageSize;

  constructor(injector: Injector, private readonly service: ProposedUnitService) {
    super(injector);
    this.functionCode = this.route.snapshot?.data['code'];
  }

  proposeUnitApprove = false;
  rotationList:  ProposeRotationReasons[] = [];
  rotationRemoveList: ProposeRotationReasons[] = [];
  rotationListDelete = [];
  screenCodeConst = Constants.ScreenCode;

  //fix tạm
  stateNumber = new FormControl(null);
  submissionDate = new FormControl(null);
  decisionNumber = new FormControl(null);
  signedDate = new FormControl(null);
  //
  jobDTOSApi: JobDTOS[] = [];
  //
  functionCodeUnit = FunctionCode.HR_PERSONAL_INFO;


  checkUrl() {
    const pageName = this.route.snapshot?.data['pageName'];
    this.proposeUnitApprove = pageName === 'development.pageName.proposed-unit-approve-detail';
    if (this.route.snapshot?.data['handlePropose']) {
      this.typeAction = this.screenType.detail;
    }
    if (pageName === 'development.pageName.proposedLD-handle') {
      this.proposeCommentForm.disable();
    }
  }

  getReasonsReject() {
    return this.checkDisplayReportEdit()
      ? this.formReportElement.get('approveNote')?.value
      : this.formTTQD.get('approveNote')?.value;
  }

  ngOnChanges(): void {
    if (!this.dataCommon) {
      this.dataCommon = {
        jobDTOS: [], // vị trí
        typeDTOS: [], // loại đề xuất
        formDisplaySearchDTOS: [], // hình thức
        proposeRotationReasonConfigDTOS: [], // lý do luân chuyển
        statusList: [],
        flowStatusApproveConfigList: [],
        flowStatusPublishConfigList: [],
      };
    }
    this.jobDTOSApi = this.dataCommon.jobDTOS!;
    this.issueForm.disable();
    if (this.dataForm?.employee?.empCode) {
      this.employeeForm.patchValue({
        empCode: this.dataForm?.employee?.empCode,
      });
    }
    if (this.typeAction === this.screenType.detail || !this.employeeForm.get('empCode')?.value) {
      this.proposeDetailForm.disable();
      this.employeeForm.disable();
      this.proposeCommentForm.disable();
      this.formReportElement.disable();
    }


    this.setValueChangeLevel();
    if (this.dataForm) {
      const typeStatement = this.dataForm?.proposeStatement?.find(
        (item: FormTTQD) => !item.typeStatement
      );
      //nếu edit tờ trình ngoài gốc thì map tờ trình theo proposeStatementList
      if (!typeStatement) {
        this.formTTQD.patchValue({
          statementNumber: this.proposeDecisionStatement?.statementNumber,
          submissionDate: this.proposeDecisionStatement?.submissionDate,
          approveNote: this.dataForm?.approveNote,
        });
      } else {
        this.formTTQD.patchValue({
          statementNumber: typeStatement ? typeStatement?.statementNumber : null,
          submissionDate: typeStatement ? typeStatement?.submissionDate : null,
          decisionNumber: typeStatement ? typeStatement?.decisionNumber : null,
          signedDate: typeStatement ? typeStatement?.signedDate : null,
          approveNote: this.dataForm?.approveNote,
        });
      }

      this.proposeDetailForm.patchValue(this.dataForm);

      const fileDecision = this.dataForm?.fileDTOS?.find((item: any) => item.type === 'decision');
      if (fileDecision) {
        this.fileListSelectQD.push({
          uid: fileDecision.docId,
          name: fileDecision.fileName,
          status: 'done',
          id: fileDecision.id,
        });
      }

      const effectiveDate = this.proposeDTO?.proposeCorrection?.effectiveDate;
      const signerFullName = this.dataForm?.employee?.signerFullName;
      this.issueForm.patchValue({
        effectiveDate: effectiveDate,
        signerFullName: signerFullName,
      });

      const employeeData = this.dataForm?.employee;
      employeeData.currentUnitName = employeeData?.currentUnit?.orgName;
      if (this.dataForm.employee.posSeniority) {
        const years = Math.floor(this.dataForm.employee.posSeniority / 12);
        const months = Math.floor(this.dataForm.employee.posSeniority % 12);
        if (months === 0) {
          this.dataForm.employee.posSeniorityName = years < 1 ? `` : `${years} năm`;
        } else {
          this.dataForm.employee.posSeniorityName = years < 1 ? `${months} tháng` : `${years} năm ${months} tháng`;
        }
      }

      if (this.dataForm.employee.seniority) {
        const years = Math.floor(this.dataForm.employee.seniority / 12);
        const months = Math.floor(this.dataForm.employee.seniority % 12);
        if (months === 0) {
          this.dataForm.employee.seniorityName = years < 1 ? `` : `${years} năm`;
        } else {
          this.dataForm.employee.seniorityName = years < 1 ? `${months} tháng` : `${years} năm ${months} tháng`;
        }
      }

      this.employeeForm.patchValue(this.dataForm?.employee);
      this.employeeForm.patchValue({
        levelName: this.dataForm?.employee.positionLevel
      });
      let disciplinaryDuration = this.employeeForm?.get('endOfDisciplinaryDuration')?.value || '';
      if (disciplinaryDuration) {
        const check2 = moment(disciplinaryDuration, Constants.FORMAT_DATE.DD_MM_YYYY).isValid();
        if (!check2) {
          disciplinaryDuration = moment(disciplinaryDuration).format(Constants.FORMAT_DATE.DD_MM_YYYY);
        }
        this.employeeForm.patchValue({ endOfDisciplinaryDuration: disciplinaryDuration });
      }
      this.rotations.forEach((item, i) => {
        if (i === this.dataForm?.formRotation) {
          item.disabled = this.typeAction === this.screenType.detail;
          item.formRotation = this.dataForm?.formRotation?.toString();
          item.formRotationTitle = this.dataForm?.formRotationTitle?.toString();
          item.formRotationType = this.dataForm?.formRotationType;
          item.contentTitleCode = this.dataForm?.contentTitleCode;
          item.level = this.dataForm?.contentLevel;
          if (i === 1) {
            item.contentUnit = {
              orgId: this.dataForm?.contentUnitCode,
              orgName: this.dataForm?.contentUnitName,
              pathId: this.dataForm?.contentUnitPathId,
              pathResult: this.dataForm?.contentUnitPathName,
              orgLevel: this.dataForm?.contentUnitTreeLevel,
            };
          }
          item.contentUnitCode = this.dataForm?.contentUnitCode;
          this.onChangeUnit(this.dataForm?.contentUnitCode, +this.dataForm?.formRotation, item.contentTitleCode!);
          if (item.formRotationType === Constants.FormRotationType.DD) {
            this.rotationTimeView = true;
          }
        }
      });
      this.proposeAlternative = this.dataForm.proposeAlternative || [];
      this.proposeAlternative?.forEach((item, i) => {
        item.disabled = this.typeAction === this.screenType.detail;
        item.alternativeType = this.dataForm?.proposeAlternative[i].alternativeType;
        item.estimatedDate = this.dataForm?.proposeAlternative[i]?.estimatedDate || null;
        item.handoverEmployeeCode = this.dataForm?.proposeAlternative[i]?.handoverEmployeeCode || null;
        item.handoverEmployeeName = this.dataForm?.proposeAlternative[i]?.handoverEmployeeName;
        item.handoverEmployeeTitleCode = +this.dataForm?.proposeAlternative[i]?.handoverEmployeeTitleCode;
        item.handoverEmployeeTitleName = this.dataForm?.proposeAlternative[i]?.handoverEmployeeTitleName || null;
        item.handoverEmployee = this.dataForm?.proposeAlternative[i]?.handoverEmployee || null;

        item.replacementEmployeeCode = this.dataForm?.proposeAlternative[i]?.replacementEmployeeCode || null;
        item.replacementEmployeeName =
          (this.dataForm?.proposeAlternative[i]?.replacementEmployeeName || null);
        item.replacementEmployeeTitleCode = +this.dataForm?.proposeAlternative[i]?.replacementEmployeeTitleCode;
        item.replacementEmployee = this.dataForm?.proposeAlternative[i]?.replacementEmployee;

      });
      this.rotationReasons = this.dataForm?.proposeRotationReasons;
      this.employeeForm.patchValue({
        partyDate: !!this.dataForm.employee.partyDate,
      });

      this.rotationReasons?.forEach((item, i = -1) => {
        item.disabled = true;
        const id = this.rotationReasons[i].rotationReasonID;
        if (id) {
          const itemFind = this.dataCommon.proposeRotationReasonConfigDTOS?.find((data) => data.id === +id);
          this.rotationReasons[i].rotationReasonID = itemFind?.id;
          this.rotationReasons[i].name = itemFind?.name;
        }
      });
      if (this.typeAction === this.screenType.update && !this.rotationReasons) {
        this.addRotationReasons();
        this.checkDeleteReason();
      }
      this.proposeCommentForm.patchValue(this.dataForm?.proposeComment);
      this.proposePerformanceResults = this.dataForm?.proposePerformanceResults;
      this.proposePerformanceResults?.forEach((item) => {
        item.disabled = true;
      });
      const totalDensity = this.dataForm.totalDensity;
      const totalPoint = this.dataForm.totalPoint;
      this.sumProposePerformanceResults = totalDensity;
      this.sumPoint = totalPoint;
      this.sortOrder = this.dataForm?.sortOrder;
      this.disableShowMinTime = this.showMinTime ?? false;
      this.disableShowMaxTime = this.showMaxTime ?? false;
      if (this.typeAction === this.screenType.update) {
        if (!this.proposePerformanceResults) {
          this.addProposePerformanceResult();
        }

        if (this.proposeAlternative.length === 0) {
          this.addProposeAlternative();
        }
      }
      this.conditionViolation = this.dataForm?.conditionViolations || [];
      this.violationCode = this.dataForm?.conditionViolationCode;
      const proposeStatementDetails = this.dataForm?.proposeStatement || [];
      const proposeStatementList = this.proposeDTO?.proposeStatementList || [];
      if (proposeStatementDetails?.length > 0) {
        proposeStatementDetails?.forEach((item: FormTTQD) => {
          if (item?.typeStatement === Constants.TypeStatement.EDITHRDV) {
            this.formReportElement.patchValue({
              statementNumber: item?.statementNumber,
              submissionDate: item?.submissionDate,
              approveNote: this.dataForm?.approveNote,
            })
          }
        })
      } else {
        proposeStatementList?.forEach((item: FormTTQD) => {
          if (item?.typeStatement === Constants.TypeStatement.EDITHRDV) {
            this.formReportElement.patchValue({
              statementNumber: item?.statementNumber,
              submissionDate: item?.submissionDate,
              approveNote: this.dataForm?.approveNote,
            })
          }
        })
      }
      this.hasAgreeUnit = this.dataForm?.hasAgreeUnit;
    } else {
      if (this.typeAction === this.screenType.create) {
        if (this.rotationReasons.length === 0) {
          this.addRotationReasons();
        }
        this.checkDeleteReason();
        if (this.proposePerformanceResults.length === 0) {
          this.addProposePerformanceResult();
        }
        this.checkDeletePerformance();
      }
    }
    if (this.typeAction === this.screenType.detail || this.checkDisplayReportEdit() || this.isScreenHandle
      || this.flowStatusCode === Constants.FlowCodeConstants.CHUAGUI1) {
      this.formTTQD.disable();
    }
    this.proposeUnitForm?.patchValue(this.proposeDTO);
  }

  checkLeaderApprove(status: string) {
    return Constants.LeaderAppproveStatus.indexOf(status) > -1;
  }

  ngOnInit() {
    this.initTable();
    this.checkUrl();
    this.employeeForm.disable();
    this.employeeForm.get('empCode')?.enable();
    this.rotationList = this.dataCommon.proposeRotationReasonConfigDTOS || [];
    this.getReportMany = this.externalApproveParent;
    this.displayReport = this.dataForm?.externalApprove || false;
    this.checkReportStatus();
    this.getLog(1)
    const fileElement = this.dataForm?.fileDTOList?.find(
      (item: FileDTOList) => item.type === Constants.TypeStatementFile.STATEMENTEDIT);
    if (fileElement) {
      this.fileListSelectTTElement.push({
        uid: fileElement.docId,
        name: fileElement.fileName,
        status: 'done',
        id: fileElement.id,
      });
      this.fileElement.uid = fileElement.docId;
      this.fileElement.fileName = fileElement.fileName;
    }
    this.fileListSelectTT = [];
    if (this.proposeCategory === Constants.ProposeCategory.ELEMENT) {
      this.fileStatement = this.dataForm?.fileDTOList?.find(
        (item: FileDTOList) => item.type === Constants.TypeStatementFile.STATEMENT);
    } else {
      this.fileStatement = this.dataForm?.fileDTOS?.find(
        (item: FileDTOList) => item.type === Constants.TypeStatementFile.STATEMENT);
    }
    if (this.fileStatement?.docId && this.fileStatement?.fileName) {
      this.fileListSelectTT = [
        ...this.fileListSelectTT,
        {
          uid: this.fileStatement?.docId,
          name: this.fileStatement?.fileName,
          status: 'done',
          id: this.fileStatement?.id,
        },
      ];
      this.fileParent.uid = this.fileStatement?.docId;
      this.fileParent.fileName = this.fileStatement?.fileName;
    }
    this.contentTitleEmp = this.dataForm?.employee?.jobId;
  }

  checkProposePerformanceResultsHasAction() {
    return !this.proposePerformanceResults?.find((item) => !item.disabled);
  }

  addRotationReasons() {
    this.rotationReasons = [
      ...(this.rotationReasons ? this.rotationReasons : []),
      ...[
        {
          id: null,
          rotationReasonID: null,
          proposeDetailId: null,
          name: null,
          description: null,
          disabled: false,
        },
      ],
    ];
  }

  checkValue(e: string, index: number, item: RotationReasons) {
    const itemCreate = this.rotationReasons.find((itemRotationReasons) => itemRotationReasons.name === e);
    if (item.id === null) {
      if (
        e &&
        itemCreate?.name === e &&
        ((itemCreate?.id && this.rotationReasons.length < 8) ||
          (itemCreate?.id === null && this.rotationReasons.length === 8))
      ) {
        this.deleteRotationReasons(index, item);
      }
    } else if (item.id && itemCreate?.id) {
      this.editRotationReasons(item);
    }
  }

  displayRotationReasons(item: RotationReasons) {
    this.rotationList.forEach((itemRotation) => {
      if (itemRotation.name === item?.name) {
        item.id = itemRotation.id;
        item.rotationReasonID = item.id;
        itemRotation.disabled = true;
        this.rotationRemoveList.push(itemRotation);
      } else {
        itemRotation.disabled =
          this.rotationReasons?.findIndex((itemSelected) => itemRotation.name === itemSelected.name) !== -1;
      }
    });
  }

  editRotationReasons(item: RotationReasons) {
    item.disabled = false;
  }

  checkDeleteReason() {
    return this.rotationReasons?.length === 1;
  }

  disableSubmissionSignedDate = (dateVal: Date) => {
    return dateVal.getTime() >= Date.now();
  };

  saveRotationReasons(item: RotationReasons, index: number) {
    this.displayRotationReasons(item);
    item.disabled = true;
    this.rotationReasons?.forEach((itemSelected, i) => {
      if (i !== index && item.name === itemSelected.name) {
        itemSelected.name = null;
      }
    });
  }

  deleteRotationReasons(index: number, item: RotationReasons) {
    if (this.typeAction !== this.screenType.detail) {
      this.rotationReasons.splice(index, 1);
    }
    this.rotationReasons = [...this.rotationReasons];
  }

  addProposePerformanceResult() {
    this.proposePerformanceResults = [
      ...(this.proposePerformanceResults ? this.proposePerformanceResults : []),
      ...[
        {
          id: null,
          proposeDetailId: null,
          targetAssigned: null,
          density: null,
          planIndicator: null,
          planDeadline: null,
          realityIndicator: null,
          realityDeadline: null,
          disabled: false,
        },
      ],
    ];
  }

  checkDeletePerformance() {
    return this.proposePerformanceResults.length === 1;
  }

  saveCalculateItem(item: ProposePerformanceResults) {
    this.calculate(item);
    this.calculateDensity();
    if (this.sumProposePerformanceResults > 100) {
      this.sumProposePerformanceResults = this.currentSumProposePerformanceResults;
      this.sumPoint = this.currentSumPoint;
      return this.toastrCustom.warning(
        this.translate.instant('development.proposed-unit.messages.sumProposePerformanceBig')
      );
    }
  }

  editProposePerformanceResult(item: ProposePerformanceResults) {
    item.disabled = false;
  }

  saveProposePerformanceResult(item: ProposePerformanceResults) {
    this.saveCalculateItem(item);
    this.checkDeletePerformance();
  }

  deleteProposePerformanceResult(index: number) {
    this.proposePerformanceResults.splice(index, 1);
    this.proposePerformanceResults = [...this.proposePerformanceResults];
    this.checkDeletePerformance();
    this.calculateDensity();
    this.calculatePoint();
  }

  submit() {
    const proposeDetail = { ...cleanDataForm(this.proposeDetailForm) };
    proposeDetail.employee = { ...cleanDataForm(this.employeeForm) };
    delete proposeDetail.employee.partyDate;
    const rotation = this.rotations.find((item) => item.formRotation);
    proposeDetail.id = this.dataForm?.id;
    proposeDetail.proposeID = this.dataForm?.proposeID;
    proposeDetail.totalPoint = this.sumPoint;
    proposeDetail.totalDensity = this.sumProposePerformanceResults;
    proposeDetail.formGroup = +this.proposeDetailForm.get('formGroup')?.value;
    proposeDetail.formRotation = +rotation?.formRotation!;
    proposeDetail.formRotationType = rotation?.formRotationType;
    let formCode = this.dataCommon.formGroupMultipleList?.find(
      (item) => item.formCode === rotation?.formRotationType);
    proposeDetail.formRotationCode = formCode?.code;
    proposeDetail.formRotationTitle = rotation?.formRotationTitle;
    proposeDetail.contentTitleCode = rotation?.contentTitleCode;
    proposeDetail.contentTitleName = this.dataCommon.jobDTOS?.find(
      (item: JobDTOS) => item.jobId === proposeDetail.contentTitleCode
    )?.jobName;
    let contentUnit: OrgItem | null;
    if (rotation?.formRotation === Constants.FormRotationTitle.SAME_TITLE) {
      contentUnit = this.listInUnit?.find((item) => item?.value === rotation?.contentUnitCode)?.valueItem ?? null;
    } else {
      contentUnit = rotation?.contentUnit ?? null;
    }
    proposeDetail.contentUnitCode = contentUnit?.orgId?.toString();
    proposeDetail.contentUnitName = contentUnit?.orgName;
    proposeDetail.contentUnitPathId = contentUnit?.pathId;
    proposeDetail.contentUnitPathName = contentUnit?.pathResult;
    proposeDetail.contentUnitTreeLevel = contentUnit?.orgLevel;
    proposeDetail.contentLevel = rotation?.level;
    proposeDetail.implementDate = this.proposeDetailForm.get('implementDate')?.value;
    proposeDetail.minimumTime = this.proposeDetailForm.get('minimumTime')?.value;
    proposeDetail.maximumTime = this.proposeDetailForm.get('maximumTime')?.value;

    proposeDetail.sortOrder = this.sortOrder;
    proposeDetail.proposeRotationReasons = [...this.rotationReasons];

    proposeDetail.proposeRotationReasons?.forEach((item: ProposeRotationReasons) => {
      if (item?.rotationReasonID && !item?.id) {
        const itemRotation = this.rotationList.find((itemRotationList) => itemRotationList?.id === item?.rotationReasonID);
        item.id = itemRotation?.id;
        delete item.name;
        this.rotationReasons.find((rotationReasons: RotationReasons) => rotationReasons?.name === item?.name)!.disabled = true;
      }
    });

    proposeDetail.proposeAlternative = this.proposeAlternative;
    proposeDetail.proposeComment = { ...cleanDataForm(this.proposeCommentForm) };
    proposeDetail.proposePerformanceResults = [...this.proposePerformanceResults];
    if (this.proposePerformanceResults.length === 1 && !this.proposePerformanceResults[0].targetAssigned) {
      proposeDetail.proposePerformanceResults = undefined;
    }
    proposeDetail.isGroup = this.proposeDetailForm.get('isGroup')?.value;
    proposeDetail.groupByKSPTNL = this.proposeDetailForm.get('groupByKSPTNL')?.value;
    proposeDetail.flowStatusCode = this.flowStatusCode;
    proposeDetail.flowStatusPublishCode = this.flowStatusPublishCode;
    proposeDetail.externalApprove = this.displayReport;
    proposeDetail.isHo = this.proposeDetailForm.get('isHo')?.value;

    proposeDetail.sameDecision = this.proposeDetailForm.get('sameDecision')?.value;
    proposeDetail.isIncome = this.proposeDetailForm.get('isIncome')?.value;
    proposeDetail.isInsurance = this.proposeDetailForm.get('isInsurance')?.value;
    proposeDetail.isDecision = this.proposeDetailForm.get('isDecision')?.value;
    proposeDetail.issuedLater = this.proposeDetailForm.get('issuedLater')?.value;
    proposeDetail.approveNote = this.checkDisplayReportEdit()
      ? this.formReportElement.get('approveNote')?.value
      : this.formTTQD.get('approveNote')?.value;

    const data = { ...cleanDataForm(this.proposeUnitForm) };
    data.formGroup = this.proposeDetailForm.get('formGroup')?.value;
    data.isGroup = this.proposeDTO?.isGroup;
    data.groupByKSPTNL = this.proposeDTO?.groupByKSPTNL;
    data.flowStatusCode = this.proposeDTO?.flowStatusCode;
    data.flowStatusPublishCode = this.flowStatusPublishCode;
    data.externalApprove = this.proposeDTO?.externalApprove;
    data.isHo = this.proposeDTO?.isHo;
    data.issueLevelCode = this.proposeDTO?.issueLevelCode;
    delete data.withdrawNote;
    data.proposeDetails = [proposeDetail];
    // Kiểm tra trường hợp sử thành phần từ Màn nhiều thành phần vào
    data.id = this.proposeDTO.id;
    if (this.stateUrl?.state?.backUrl) {
      data.id = this.proposeId;
    };
    delete data.relationType;
    return [data];
  }

  saveStatementDecision(issueDecision?: boolean) {
    let checkValid = false;
    if (!this.formTTQD.valid) {
      checkValid = true;
    }
    if (checkValid && this.typeAction !== ScreenType.Update) {
      this.isSubmitFormTTQD = true;
    } else {
      const dataForm = this.formTTQD.getRawValue();
      this.isSubmitFormTTQD = false;
      dataForm.hasDecision = false;
      if (issueDecision) {
        dataForm.id = this.proposeDetailId;
        return dataForm;
      } else {
        this.submitStatementDecision.emit(dataForm);
      }
    }
  }

  disabledStartDate = (startValue: Date): boolean => {
    let endValue;
    if (this.proposeDetailForm.get('rotationToDate')?.value) {
      endValue = new Date(this.proposeDetailForm.get('rotationToDate')?.value);
    } else {
      endValue = null;
    }
    if (!startValue || !endValue) {
      return false;
    }
    return startValue.getTime() > endValue.getTime();
  };

  disabledEndDate = (endValue: Date): boolean => {
    let startValue;
    if (this.proposeDetailForm.get('rotationFromDate')?.value) {
      startValue = new Date(this.proposeDetailForm.get('rotationFromDate')?.value);
    } else {
      startValue = null;
    }
    if (!startValue || !endValue) {
      return false;
    }
    return startValue.getTime() > endValue.getTime();
  };

  rotationTime() {
    const newVal =
      moment(this.proposeDetailForm.get('rotationToDate')?.value).diff(
        moment(this.proposeDetailForm.get('rotationFromDate')?.value),
        'days'
      ) / 30;
    const fixed = newVal.toFixed(1);
    if (!this.proposeDetailForm.get('rotationFromDate')?.value || !this.proposeDetailForm.get('rotationToDate')?.value) {
      this.proposeDetailForm.get('rotationTime')?.setValue(null);
    } else {
      this.proposeDetailForm.get('rotationTime')?.setValue(+fixed);
    }
  }

  calculate(item: ProposePerformanceResults, isDensity?: boolean) {
    //Tính điểm
    const totalDensity = this.sumProposePerformanceResults + item?.density!;
    if (totalDensity > 100 && isDensity) {
      item.density = 0;
      this.calculateDensity();
       this.toastrCustom.warning(
        this.translate.instant('development.proposed-unit.messages.sumProposePerformanceBig')
      );
       return;
    }
      this.calculateDensity();

  }

  calculatePoint() {
    this.sumPoint = 0;
    this.proposePerformanceResults?.forEach((item) => {
      this.sumPoint = this.sumPoint + parseFloat(<string>item.point);
    });
  }

  calculateDensity() {
    this.sumProposePerformanceResults = 0;
    this.proposePerformanceResults?.forEach((item) => {
      this.sumProposePerformanceResults = this.sumProposePerformanceResults + item.density!;
    });
  }

  onChangeFormRotation(index: number) {
    this.rotations.forEach((item, i) => {
      if (i === index) {
        item.disabled = false;
      } else {
        item.disabled = true;
        item.formRotation = null;
        item.contentUnit = null;
        item.formRotationType = null;
        item.formRotationTitle = null;
        item.contentUnitCode = null;
        item.contentTitleCode = null;
        item.contentTitleName = null;
        item.contentUnitPathId = null;
        item.contentUnitPathName = null;
        item.contentUnitTreeLevel = null;
        item.level = null;
      }
    });
  }

  setEmpNullValue() {
    this.employeeForm.patchValue({
      empIdp: null,
      orgId: null,
      empId: null,
      empCode: null,
      fullName: null,
      email: null,
      posId: null,
      posName: null,
      jobId: null,
      jobName: null,
      orgPathId: null,
      orgPathName: null,
      orgName: null,
      majorLevelId: null,
      majorLevelName: null,
      faculityId: null,
      faculityName: null,
      schoolId: null,
      schoolName: null,
      positionLevel: null,
      gender: null,
      joinCompanyDate: null,
      dateOfBirth: null,
      partyDate: null,
      partyOfficialDate: null,
      posSeniority: null,
      seniority: null,
      qualificationEnglish: null,
      facultyName: null,
      semesterGradeT1: null,
      semesterRankT1: null,
      semesterGradeT2: null,
      semesterRankT2: null,
      ratedCapacity: null,
      nearestDiscipline: null,
      endOfDisciplinaryDuration: null,
      userToken: null,
      treeLevel: null,
    });
  }

  displayConditionViolation() {
    const violationObj = this.dataCommon.conditionViolationList?.find(
      (item) => +item.code === this.violationCode);
    switch (this.violationCode) {
      case 0:
        return { color: Constants.ConditionViolationColor.REQUIRED, label: violationObj?.name };
      case 1:
        return { color: Constants.ConditionViolationColor.VIOLATE, label: violationObj?.name };
      case 2:
        return { color: Constants.ConditionViolationColor.PASSED, label: violationObj?.name };
      default:
        return null;
    }
  }


  override handleDestroy(): void {
    this.proposeAlternative.forEach((item) => {
      item.alternativeType = null;
      item.id = null;
      item.proposeDetailId = null;
      item.handoverEmployeeName = null;
      item.handoverEmployeeTitleCode = null;
      item.estimatedDate = null;
      item.replacementEmployeeName = null;
      item.replacementEmployeeTitleCode = null;
    });
    this.rotations.forEach((item) => {
      item.formRotation = null;
      item.contentTitleCode = null;
      item.contentUnitCode = null;
      item.formRotationTitle = null;
      item.formRotationType = null;
    });
    this.rotationReasons?.forEach((item) => {
      item.checked = false;
      item.description = null;
      item.proposeDetailId = null;
      item.rotationReasonID = item.id;
      item.disabled = true;
    });
  }

  checkReportStatus() {
    this.submitReport.emit(this.displayReport);
  }

  print() {
    const id = this.route.snapshot.queryParams['id'];
    this.service.printReportCard(id).subscribe((res) => {
      const url = window.URL.createObjectURL(res);
      const a = document.createElement('a');
      a.setAttribute('style', 'display:none;');
      document.body.appendChild(a);
      a.href = url;
      a.download = 'Tờ trình';
      a.click();
    });
  }

  checkDisableJob(myDisable: any, formRotationTitle: string) {
    return !formRotationTitle || +formRotationTitle === 0;
  }

  // Upload file
  beforeUploadQD = (file: File | NzUploadFile): boolean => {
    const formData = new FormData();
    formData.append('files', file as File);
    formData.append('proposeDetailId', new Blob([JSON.stringify(this.proposeDetailId)], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('type', new Blob(['decision'], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('groupProposeTypeId', new Blob([JSON.stringify(1)], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('isIn', new Blob([JSON.stringify(this.displayReport)], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));

    this.service.uploadFile(formData).subscribe(
      (res) => {
         this.fileListSelectQD = [
          ...this.fileListSelectQD,
          {
            uid: res.data?.docId,
            name: res.data?.fileName,
            status: 'done',
            id: res.data?.id,
          },
        ];
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    return false;
  };

  doRemoveFileQD = (file: NzUploadFile): boolean => {
    const idDelete = <number>this.fileListSelectQD.find((item) => item.uid === file.uid)!['id'];
    this.service.deleteUploadFile(+idDelete).subscribe(
      (res) => {
        this.fileListSelectQD = this.fileListSelectQD.filter((item) => item['id'] !== idDelete);
      },
      (e) => {
        this.getMessageError(e);
      }
    );
    return false;
  };

  beforeUploadTT = (file: File | NzUploadFile): boolean => {
    this.isLoading = true;
    const formData = new FormData();
    formData.append('files', file as File);
    formData.append('proposeDetailId', new Blob([JSON.stringify(this.proposeDetailId)], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('type', new Blob([Constants.TypeStatementFile.STATEMENTEDIT], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('groupProposeTypeId', new Blob(['1'], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('isIn', new Blob([JSON.stringify(this.displayReport)], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    this.service.uploadFile(formData).subscribe(
      (res) => {
        this.fileListSelectTTElement = [
          ...this.fileListSelectTTElement,
          {
            uid: res.data?.docId,
            name: res.data?.fileName,
            status: 'done',
            id: res.data?.id,
          },
        ];
        this.validApproveButton();
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    return false;
  };

  doRemoveFileTT = (file: NzUploadFile): boolean => {
    this.isLoading = true;
    if (this.fileListSelectTTElement.find((item) => item.uid === file.uid)){
      const idDelete = this.fileListSelectTTElement.find((item) => item.uid === file.uid)?.['id'];
      this.service.deleteUploadFile(+idDelete).subscribe(
        (res) => {
          this.fileListSelectTTElement = this.fileListSelectTTElement.filter((item) => item['id'] !== idDelete);
          this.validApproveButton();
          this.isLoading = false;
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        }
      );
    }
    return false;
  };

  dowloadStatement(action: number) {
    const params = {
      groupProposeType: Constants.ProposeTypeID.ELEMENT,
      action: action,
      screenCode: this.screenCode,
    };
    this.isLoading = true;
    let listFile: ListFileTemplate[] = [];
    if (this.proposeDetailId) {
      const sub = this.service.exportReportDecision(this.proposeDetailId, params).subscribe(
        (res) => {
          listFile = res?.data;
          if (!listFile || listFile?.length === 0) {
            this.toastrCustom.error('development.list-plan.messages.noFile');
            this.isLoading = false;
          } else {
            listFile?.forEach((item) => {
              this.service.callBaseUrl(item.linkTemplate).subscribe((res) => {
                const contentDisposition = res?.headers.get('content-disposition');
                const filename = contentDisposition?.split(';')[1]?.split('filename')[1]?.split('=')[1]?.trim();
                const blob = new Blob([res?.body], {
                  type: 'application/octet-stream',
                });
                FileSaver.saveAs(blob, filename);
                this.isLoading = false;
              },
              (e) => {
                this.getMessageError(e);
                this.isLoading = false;
              })
            })
          }
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        }
      );
      this.subs.push(sub);
    }
  }

  changeRadio(e: string) {
    if (e === Constants.DCDD) {
      this.rotationTimeView = true;
    } else {
      this.proposeDetailForm.get('rotationFromDate')?.setValue(null);
      this.proposeDetailForm.get('rotationToDate')?.setValue(null);
      this.proposeDetailForm.get('rotationTime')?.setValue(null);
      this.proposeDetailForm.get('rotationTime')?.disable();

      this.rotationTimeView = false;
    }
  }

  checkformRotationLabel(formRotationLabel: string) {
    if (this.rotations[0].formRotationLabel === formRotationLabel) {
      return 'development.proposed-unit.table.formRotationLabel1';
    } else {
      return 'development.proposed-unit.table.formRotationLabel2';
    }
  }

  checkFormStatement() {
    const dataForm = this.issueForm.getRawValue();
    if (
      this.typeAction !== ScreenType.Create &&
      (this.screenCode === this.screenCodeConst.HRDV || this.screenCode === this.screenCodeConst.LDDV) &&
      Object.values(dataForm).every((item) => item)
    ) {
      return true;
    }
    return false;
  }

  showModalPropose(footerTmplSingle: TemplateRef<any>, mode: string, data?: any) {
    if (this.typeAction !== this.screenType.create) {
      return;
    }
    this.modalRef = this.modal.create({
      nzTitle: 'Tìm kiếm nhân viên',
      nzWidth: this.getInnerWidth(),
      nzContent: ProposeMultipleEmployeeComponent,
      nzComponentParams: {
        isEmp: true,
      },
      nzFooter: footerTmplSingle,
    });
    this.modalRef.afterClose.subscribe((result) => {
      if (result?.length > 0) {
        this.proposeDetailForm.enable();
        this.proposeCommentForm.enable();
        this.formTTQD.enable();
        this.employeeForm.patchValue(result[0]);
        let dob = this.employeeForm.controls['dateOfBirth'].value || '';
        const check = moment(dob, Constants.FORMAT_DATE.DD_MM_YYYY).isValid();
        if (check === false) {
          dob = moment(dob).format(Constants.FORMAT_DATE.DD_MM_YYYY);
        }
        this.employeeForm.patchValue({ dateOfBirth: dob });
        const orgPathId = this.employeeForm.controls['orgPathId'].value;
        const elementOrgPathId = orgPathId?.split('/');
        const treeLevel = elementOrgPathId.length - 2;
        const levelOrgPathId = elementOrgPathId[3];
        this.onChangeUnitDiff(levelOrgPathId);

        this.setValueChangeLevel();
        const yearsPosSeniority = Math.floor(this.employeeForm.controls['posSeniority'].value / 12);
        const monthsPosSeniority = Math.floor(this.employeeForm.controls['posSeniority'].value % 12);
        let posSeniorityName: string;
        if (monthsPosSeniority === 0) {
          posSeniorityName = yearsPosSeniority < 1 ? `` : `${yearsPosSeniority} năm`;
        } else {
          posSeniorityName =
            yearsPosSeniority < 1
              ? `${monthsPosSeniority} tháng`
              : `${yearsPosSeniority} năm ${monthsPosSeniority} tháng`;
        }

        const yearsSeniority = Math.floor(this.employeeForm.controls['seniority'].value / 12);
        const monthsSeniority = Math.floor(this.employeeForm.controls['seniority'].value % 12);
        let seniorityName: string;
        if (monthsSeniority === 0) {
          seniorityName = yearsSeniority < 1 ? `` : `${yearsSeniority} năm`;
        } else {
          seniorityName =
            yearsSeniority < 1 ? `${monthsSeniority} tháng` : `${yearsSeniority} năm ${monthsSeniority} tháng`;
        }
        this.employeeForm.patchValue({
          treeLevel: treeLevel,
          partyDate: !!this.employeeForm.controls['partyDate'].value,
          posSeniorityName: posSeniorityName,
          seniorityName: seniorityName,
        });
        this.contentTitleEmp = result[0]?.jobId;
      }
    });
  }

  clearEmployeeCode(event: any) {
    event.preventDefault();
    this.employeeForm.get('empCode')?.setValue(null);
    this.proposeDetailForm.disable();
    this.proposeCommentForm.disable();
    this.formTTQD.disable();
  }

  getInnerWidth(): number {
    if (window.innerWidth > 767) {
      return window.innerWidth / 1.5 > 1368 ? 1368 : window.innerWidth / 1.3;
    }
    return window.innerWidth;
  }

  onChangeUnit(orgId: string, index: number, contentTitleDetail?: number) {
    setTimeout(() => {
      this.differentUnits =
        this.listInUnit?.find((item) => item?.value === this.rotations[index]?.contentUnitCode)?.valueItem
          ?.pathResult || '';
      this.inUnit = this.rotations[index]?.contentUnit?.pathResult || '';
    }, 1000);
    this.jobDTOSApi = [];
    if (index > 0 && this.rotations[index].contentUnit && orgId) {
      const unitLevel = this.rotations[index].contentUnit?.pathId?.split(':')[3];
      const unitLevelOfEmployee = this.employeeForm.controls['orgPathId'].value?.split('/')[3];
      if (unitLevel === unitLevelOfEmployee && !this.isScreenHandle) {
        this.toastrCustom.error(this.translate.instant('development.commonMessage.validateUnit'));
        return;
      }
    }
    if (orgId && !this.isLoading) {
      this.isLoading = true;
      const paramPosition: ParamSearchPosition = {
        orgId: orgId,
        jobId: '',
        status: 1,
        type: Constants.TYPE_SEARCH_POSITION,
        page: 0,
        size: maxInt32,
      };
      this.service.getPositionByUnit(paramPosition).subscribe(
        (res) => {
          const listPositionParent = res?.data?.content;
          this.jobDTOSApi = listPositionParent.map((item: any) => {
            return { jobId: item.jobId, jobName: item.jobName };
          });
          const codeContentTitle = contentTitleDetail ? contentTitleDetail : this.contentTitleEmp;
          this.rotations[index].contentTitleCode =
            !contentTitleDetail && this.rotations[index].formRotationTitle === null
              ? null
              : this.jobDTOSApi.find((item) => item.jobId === codeContentTitle)?.jobId || null;
          if (
            !this.rotations[index].contentTitleCode &&
            this.rotations[index].formRotationTitle === Constants.FormRotationTitle.SAME_TITLE
          ) {
            if (this.typeAction !== ScreenType.Detail && !this.isScreenHandle) {
              this.toastrCustom.error(this.translate.instant('development.commonMessage.E001'));
            }
          }
          this.isLoading = false;
        },
        (e) => {
          this.isLoading = false;
        }
      );
    }
  }

  expandLog() {
    this.isExpand = !this.isExpand;
    if (this.isExpand) {
      this.iconStatus = Constants.IconStatus.DOWN;
    } else {
      this.iconStatus = Constants.IconStatus.UP;
    }
  }

  changeTitle(valueTitle: string, index: number) {
    if (+valueTitle === 0) {
      this.rotations[index].contentTitleCode =
        this.jobDTOSApi.find((item) => item.jobId === +this.contentTitleEmp!)?.jobId || null;
      if (
        !this.rotations[index].contentTitleCode &&
        (this.rotations[index].contentUnitCode || this.rotations[index].contentUnit)
      ) {
        this.toastrCustom.error(this.translate.instant('development.commonMessage.E001'));
      }
    } else {
      this.rotations[index].contentTitleCode = null;
    }
  }

  onChangeUnitDiff(orgIdLevel: number) {
    if (orgIdLevel) {
      const param = {
        id: orgIdLevel,
        pag: 0,
        size: maxInt32,
      };
      this.service.getOrganizationTreeById(param).subscribe((res) => {
        if (res && res?.content?.length > 0) {
          res?.content.forEach((i: OrgEmployee) => {
            if (i.treeLevel > 2) {
              i.orgNameFull = i.pathName.split("-").splice(2).join("-") + " - " + i.orgName;
            }
          });
          this.listInUnit = res?.content.map((item: OrgEmployee) => {
            return <ListOrgItem>{
              name: item.orgNameFull,
              value: item.orgId,
              valueItem: <OrgItem>{
                orgId: item.orgId,
                orgName: item.orgName,
                pathId: item.pathId,
                pathResult: item.pathName,
                orgLevel: item.treeLevel,
              },
            };
          });
        }
      });
    }
  }

  scrollToValidationSection(section: string, messsage: string){
    this.toastrCustom.warning(this.translate.instant(messsage));
    let el = document.getElementById(section);
    el?.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
  }

  downloadFile = (file: NzUploadFile) => {
    this.service.downloadFile(+file?.uid, file?.name).subscribe(
      () => {
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    )
  }

  expandInfoEmp() {
    this.isSeeMore = !this.isSeeMore;
  }

  expandInfoEmpText() {
    const infoEmpText = !this.isSeeMore
      ? this.translate.instant('development.proposed-unit.button.down')
      : this.translate.instant('development.proposed-unit.button.up')
    return infoEmpText;
  }


  showModalEmpAlternative(footerTmplSingle: TemplateRef<any>, type: string,index: number) {
    this.modalRef = this.modal.create({
      nzTitle: 'Tìm kiếm nhân viên',
      nzWidth: this.getInnerWidth(),
      nzContent: ProposeMultipleEmployeeComponent,
      nzComponentParams: {
        isEmp: true,
      },
      nzFooter: footerTmplSingle,
    });
    this.modalRef.afterClose.subscribe((result) => {
      if (result?.length > 0) {
        this.proposeAlternative.forEach((item, i) => {
          if (i === index) {
            if (type === this.typeEmpAlternative.REPLACE) {
              item.replacementEmployeeCode = result[0]?.empCode;
              item.replacementEmployeeName = result[0]?.fullName;
              item.replacementEmployee = result[0]?.fullName + '-' + result[0]?.jobName + '-' + result[0]?.orgName;
              item.replacementEmployeeTitleCode = result[0]?.jobId;
              item.replacementEmployeeTitleName = result[0]?.jobName;
            } else {
              item.handoverEmployeeCode = result[0]?.empCode;
              item.handoverEmployeeName = result[0]?.fullName;
              item.handoverEmployeeTitleCode = result[0]?.jobId;
              item.handoverEmployeeTitleName = result[0]?.jobName;
              item.handoverEmployee = result[0]?.fullName + '-' + result[0]?.jobName + '-' + result[0]?.orgName;
            }

          }
        })
      }
    });
  }

  addProposeAlternative() {
    this.proposeAlternative = [
      ...(this.proposeAlternative ? this.proposeAlternative : []),
      ...[
        {
          id: null,
          estimatedDate: null,
          replacementEmployeeCode: null,
          replacementEmployeeName: null,
          replacementEmployeeTitleCode: null,
          replacementEmployeeTitleName: null,
          handoverEmployeeCode: null,
          handoverEmployeeName: null,
          handoverEmployeeTitleCode: null,
          handoverEmployeeTitleName: null,
          disabled: true,
          replacementEmployee: null,
          handoverEmployee: null,
          }
      ],
    ];
  }

  checkDeleteReason1() {
    return this.proposeAlternative?.length === 1;
  }

  deleteRotationReasons1(index: number, item: ProposeAlternative) {
    if (this.typeAction !== this.screenType.detail) {
      this.proposeAlternative.splice(index, 1);
    }
    this.proposeAlternative = [...this.proposeAlternative];
  }


  setFormRotationTitle(formRotationType: string) {
    const formGroupObj = this.dataCommon.formGroupMultipleList?.find((item) => item.formCode === formRotationType);
    this.disableShowMinTime = formGroupObj?.showMinTime!;
    this.disableShowMaxTime = formGroupObj?.showMaxTime!;
  }

  // set value cho hình thức luân chuyển khi nhóm hình thức là điều chỉnh level
  setValueChangeLevel() {
    if (this.formGroupCreate === Constants.FormGroupList.DCLEVEL) {
      if (this.rotations[1].formRotation){
        this.onChangeFormRotation(0);
      }
      this.isChangeLevel = true;
      this.rotations[0].disabled = false;
      this.rotations[0].formRotation = '0';
      this.rotations[0].formRotationTitle = Constants.FormRotationTitle.SAME_TITLE;
      this.rotations[0].contentUnitCode = this.employeeForm.controls['orgId'].value;
      this.rotations[0].contentTitleCode = this.employeeForm.controls['jobId'].value;
    } else {
      this.isChangeLevel = false;
    }
  }

  //check hiển thi radio lãnh đạo duyệt trên HCM
  checkHCMRadio(){
    return Constants.LeaderAppproveStatus.indexOf(this.flowStatusCode!) > -1
      && Constants.LeaderAppproveStatus.indexOf(this.flowStatusCodeDisplay!) <= -1;
  }

  //validate điền thông tin cho nút ghi nhận đồng ý
  validApproveButton() {
    return this.checkDataReportEdit()
      ? (this.formReportElement.get('statementNumber')?.value
        && this.formReportElement.get('submissionDate')?.value
        && this.formReportElement.get('approveNote')?.value
        && this.fileListSelectTTElement.length === 1)
      : (this.formTTQD.get('statementNumber')?.value
        && this.formTTQD.get('submissionDate')?.value
        && this.formTTQD.get('approveNote')?.value
        && this.fileListSelectTT.length === 1)
  }

  //check hiển thị cục tờ trình chỉnh sửa của đơn vị(tờ trình riêng của màn thành phần)
  // hiển thị ở trạng thái khác 1 ở HRDV(màn update) duyệt ngoài của cha,
  // có dữ liệu(màn view) hoặc ở trên PTNL nhưng phải có số tờ trình
  checkDisplayReportEdit() {
    return !this.displayReport &&
        (
          ((Constants.StatusApproveSendProposeMany.includes(this.flowStatusCode) && this.dataForm?.hasAgreeUnit)
          && this.screenCode === Constants.ScreenCode.HRDV
          && this.typeAction === this.screenType.update) ||
          this.checkDataReportEdit()
        );
  }

  //check xem tờ trình chỉnh sửa có dữ liệu không
  checkDataReportEdit() {
    return this.formReportElement.get('statementNumber')?.value
      || this.formReportElement.get('submissionDate')?.value
      || this.formReportElement.get('approveNote')?.value
      || this.fileListSelectTTElement.length === 1
  }

  //lưu tờ trình của thành phần
  saveReportElement() {
    this.isSubmitPropose = false;
    const submitForm = this.checkDisplayReportEdit()
      ? this.formReportElement.getRawValue()
      : this.formTTQD.getRawValue();
    submitForm.hasDecision = false;
    submitForm.externalApprove = this.displayReport;
    submitForm.proposeDetailId = this.proposeDetailId;
    if (this.checkDisplayReportEdit()) {
      submitForm.typeStatement = Constants.TypeStatement.EDITHRDV;
    }
    const sub = this.service.saveStatementDecision(submitForm).subscribe(() => {
      this.saveReport.emit(true);
    },
    (e) => {
      this.getMessageError(e);
      this.isLoading = false;
    });
    this.subs.push(sub);
  }

  override ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
    this.modalRef?.destroy();
  }

  //check hiển thị cục tờ trình (tờ trình của gốc và tờ trình thành phần riêng)
  checkReportSectionElement() {
    if (this.screenCode === Constants.ScreenCode.HRDV) {
      return true;
    } else if (this.screenCode === Constants.ScreenCode.PTNL || this.screenCode === Constants.ScreenCode.KSPTNL) {
      return (this.formReportElement.get('statementNumber')?.value
        || this.formTTQD.get('statementNumber')?.value)
        && (!this.getReportMany || !this.displayReport)
        && this.proposeUnitRequestName;
    }
  }

  //check hiển thị tờ trình của đơn vị(tờ trình của gốc)
  checkReportParent() {
    if (this.screenCode === Constants.ScreenCode.HRDV) {
      return !this.displayReport 
        || (this.formTTQD.get('statementNumber')?.value && this.flowStatusCode === Constants.FlowCodeConstants.CHUAGUI1);
    } else if (this.screenCode === Constants.ScreenCode.PTNL || this.screenCode === Constants.ScreenCode.KSPTNL) {
      return (!this.getReportMany || !this.displayReport) && this.formTTQD.get('statementNumber')?.value;
    }
  }

  //validate ngày đề xuất triển khai
  disabledImplementDate = (dateVal: Date) => {
    return dateVal.valueOf() <= Date.now();
  };

  //check hiển thị 3 nút duyệt của LDDV và ý kiến phê duyệt
  checkApproveLDDV(){
    if (this.proposeCategory !== Constants.ProposeCategory.ELEMENT) {
      return this.checkIdeaApproveLDDV();
    } else {
      return this.screenCode === this.screenCodeConst.LDDV
        && this.flowStatusCode === Constants.FlowCodeConstants.SENDAPPROVE2A
        && this.flowStatusCodeDisplay !== Constants.FlowCodeConstants.SENDAPPROVE2A;
    }
  }

  //check hiển thị ý kiến phê duyệt của LDDV
  checkIdeaApproveLDDV() {
    return this.screenCode === this.screenCodeConst.LDDV
      && (this.flowStatusCode === Constants.FlowCodeConstants.CHODUYET2 ||
        this.flowStatusCode === Constants.FlowCodeConstants.SENDAPPROVE2A)
  }

  checkPrintReportElement() {
    return !this.displayReport && this.typeAction === this.screenType.update;
  }

  downloadFile2(uid: string | number, name: string) {
    this.isLoading = true;
    this.service.downloadFile(+uid, name).subscribe(
      () => {
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    )
  }

  viewProposeLogsStatus() {
    return (this.screenCode === this.screenCodeConst.HRDV
      || this.screenCode === this.screenCodeConst.LDDV
      || (this.screenCode === this.screenCodeConst.PTNL
        && Constants.ViewButtonHandleRemove.includes(this.flowStatusCode)))
        && this.typeAction !== this.screenType.create && !this.isScreenHandle;
  }

  disabledEndDateSubmission = (endDate: Date): boolean => {
    return endDate >=  new Date();
  };


  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'development.proposed-unit.proposeLogsTable.time',
          field: 'actionDate',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
          tdTemplate: this.dateLog
        },
        {
          title: 'development.proposed-unit.proposeLogsTable.personAction',
          field: 'userName',
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
        },
        {
          title: 'development.proposed-unit.proposeLogsTable.action',
          field: 'action',
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
        },
        {
          title: 'development.proposed-unit.proposeLogsTable.description',
          field: 'comment',
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 200,
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1,
    };
  }

  getLog(pageIndex: number){
    this.pageNumber = pageIndex - 1;
    this.isLoading = true;
    const id = this.proposeCategory === Constants.ProposeCategory.ELEMENT ? this.proposeDetailId : this.proposeId
    if(this.screenCode) {
      const log = this.service.getLogsPage(id!, this.proposeCategory, this.screenCode, this.pageNumber, this.pageSize)
        .subscribe((logs) => {
          this.proposeLogs = this.viewProposeLogsStatus()  ?  logs?.data?.content : [];
          this.tableConfig.total = logs?.data?.totalElements || 0;
          this.tableConfig.pageIndex = pageIndex;
          this.isLoading = false;
        }, (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        })
      this.subs.push(log);
    }
  }

  //check view đính kèm tờ trình của màn nhiều
  checkViewFileParent() {
    return (this.screenCode === Constants.ScreenCode.HRDV
      && (this.fileParent?.uid && this.fileParent?.fileName && 
        (this.typeAction === this.screenType.detail || this.flowStatusCode === Constants.FlowCodeConstants.CHUAGUI1)))
      || ((this.screenCode === Constants.ScreenCode.PTNL || this.screenCode === Constants.ScreenCode.KSPTNL)
        && this.proposeUnitRequestName);
  }

  //check view đính kèm tờ trình chỉnh sửa của đơn vị
  checkViewFileElement() {
    return (this.screenCode === Constants.ScreenCode.HRDV && (this.fileElement?.uid && this.fileElement?.fileName)
      && this.typeAction === this.screenType.detail)
      || ((this.screenCode === Constants.ScreenCode.PTNL || this.screenCode === Constants.ScreenCode.KSPTNL)
        && this.proposeUnitRequestName);
  }

  //validate các trường khi gửi duyệt hoặc ghi nhận đồng ý
  validateSubmitForm() {
    const proposeDetail = { ...cleanDataForm(this.proposeDetailForm) };
    proposeDetail.employee = { ...cleanDataForm(this.employeeForm) };
    this.isSubmitPropose = true;
    //nếu ngày tờ trình nhập sau ngày quyết định thì hiển thị cảnh báo
    if (this.typeAction === this.screenType.update) {
      let submissionDate = Date.parse(this.formTTQD?.get('submissionDate')?.value);
      let signedDate = Date.parse(this.formTTQD?.get('signedDate')?.value);
      if (submissionDate > signedDate) {
        this.scrollToValidationSection('submissionDate', 'development.commonMessage.validateSubmissionSignedDate');
        return false;
      }
    }
    const rotation = this.rotations.find((item) => item.formRotation);
    if (Object.keys(proposeDetail.employee).length === 0) {
      this.scrollToValidationSection('empCode', 'development.commonMessage.validateEmpCode');
      return false;
    }
    //màn phỏng vấn không có cột hình thức nên không validate trường này
    const validFormRotation = rotation && rotation.formRotationTitle
      && (rotation.contentUnitCode || rotation.contentUnit) && rotation.contentTitleCode;
    if (this.formGroupCreate === Constants.FormGroupList.PV) {
      if (!validFormRotation) {
        this.scrollToValidationSection('formRotation', 'development.commonMessage.validateFormRotation');
        return false;
      }
    } else {
      if (!validFormRotation || !rotation.formRotationType) {
        this.scrollToValidationSection('formRotation', 'development.commonMessage.validateFormRotation');
        return false;
      }
    }
    proposeDetail.implementDate = this.proposeDetailForm.get('implementDate')?.value;
    proposeDetail.minimumTime = this.proposeDetailForm.get('minimumTime')?.value;
    proposeDetail.maximumTime = this.proposeDetailForm.get('maximumTime')?.value;
    //validate ngày đề xuất triển khai
    if (!proposeDetail.implementDate) {
      this.scrollToValidationSection('formRotation', 'development.commonMessage.validateImplementDate');
      return false;
    }
    //nếu thời gian tối thiểu và thời gian tối đa không disable thì mới validate 2 trường này
    if (this.disableShowMinTime && this.disableShowMaxTime) {
      if (!proposeDetail.minimumTime || !proposeDetail.maximumTime) {
        this.scrollToValidationSection('formRotation', 'development.commonMessage.validateMaximumTime2');
        return false;
      } else {
        if (proposeDetail.minimumTime >= proposeDetail.maximumTime) {
          this.scrollToValidationSection('formRotation', 'development.commonMessage.validateMaximumTime');
          return false;
        }
      }
    } else {
      if (proposeDetail.minimumTime >= proposeDetail.maximumTime
        && (proposeDetail.minimumTime && proposeDetail.maximumTime)) {
        this.scrollToValidationSection('formRotation', 'development.commonMessage.validateMaximumTime');
        return false;
      }
    }
    proposeDetail.proposeRotationReasons = [...this.rotationReasons];
    const uniqueRotationReasons = new Set(proposeDetail?.proposeRotationReasons.map(
      (item: ProposeRotationReasons) => item.rotationReasonID));
    if (uniqueRotationReasons.size < proposeDetail.proposeRotationReasons?.length) {
      this.toastrCustom.warning(this.translate.instant('development.commonMessage.E005'));
      return false;
    }
    if (this.rotationReasons.find((item) => !item?.rotationReasonID)) {
      this.toastrCustom.warning(this.translate.instant('development.proposed-unit.table.requiredReasons'));
      let el = document.getElementById('rotationReason');
      el?.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
      return false;
    }
    if (this.proposeCommentForm.invalid) {
      this.scrollToValidationSection('comments', 'development.commonMessage.validateCommentForm');
      return false;
    }
    return true;
  }

  //check hiển thị ý kiến phê duyệt của LDDV (tờ trình chỉnh sửa)
  checkApproveNoteElement() {
    return !this.displayReport
      && ((this.screenCode === Constants.ScreenCode.HRDV && this.typeAction === this.screenType.update)
        || (this.screenCode === Constants.ScreenCode.HRDV && this.typeAction === this.screenType.detail
        && this.formReportElement.get('approveNote')?.value))
      && this.flowStatusCode !== this.flowCodeConst.HANDLING5B
      && this.flowStatusCode !== this.flowCodeConst.CANCELPROPOSE;
  }

  //ý kiến phê duyệt của LDDV ban đầu
  checkApproveNoteOrigin() {
    return !this.displayReport
      && !this.checkDisplayReportEdit()
      && !this.hasAgreeUnit
      && Constants.StatusApproveSendProposeMany.includes(this.flowStatusCode);
  }

  beforeUploadTTOrigin = (file: File | NzUploadFile): boolean => {
    this.isLoading = true;
    const formData = new FormData();
    formData.append('files', file as File);
    formData.append('proposeDetailId', new Blob([JSON.stringify(this.proposeDetailId)], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('type', new Blob([Constants.TypeStatementFile.STATEMENT], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('groupProposeTypeId', new Blob(['1'], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('isIn', new Blob([JSON.stringify(this.displayReport)], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    this.service.uploadFile(formData).subscribe(
      (res) => {
        this.fileListSelectTT = [
          ...this.fileListSelectTT,
          {
            uid: res.data?.docId,
            name: res.data?.fileName,
            status: 'done',
            id: res.data?.id,
          },
        ];
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    return false;
  };

  doRemoveFileTTOrigin = (file: NzUploadFile): boolean => {
    this.isLoading = true;
    if (this.fileListSelectTT.find((item) => item.uid === file.uid)){
      const idDelete = this.fileListSelectTT.find((item) => item.uid === file.uid)?.['id'];
      this.service.deleteUploadFile(+idDelete).subscribe(
        () => {
          this.fileListSelectTT = this.fileListSelectTT.filter((item) => item['id'] !== idDelete);
          this.isLoading = false;
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        }
      );
    }
    return false;
  };
}
