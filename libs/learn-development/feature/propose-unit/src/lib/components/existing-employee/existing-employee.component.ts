import {
  Component,
  EventEmitter,
  Injector,
  Input,
  OnInit,
  Output,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import {
  ConcurrentPositionDTOList,
  ExistingEmployee,
  ProposeCategoryConsultation,
} from '@hcm-mfe/learn-development/data-access/models';
import { ProposedHandleService } from '@hcm-mfe/learn-development/data-access/services';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { MBTableConfig } from '@hcm-mfe/shared/data-access/models';
import { NzSafeAny } from 'ng-zorro-antd/core/types';

@Component({
  selector: 'app-existing-employee',
  templateUrl: './existing-employee.component.html',
  styleUrls: ['./existing-employee.component.scss'],
})
export class ExistingEmployeeComponent extends BaseComponent implements OnInit {
  tableConfig!: MBTableConfig;
  listFormGroup: ProposeCategoryConsultation[] = [];
  listSubmitEmployee: ConcurrentPositionDTOList[] = [];
  isSubmitted = false;
  @ViewChild('formGroupColTmpl', { static: true })
  formGroupCol!: TemplateRef<NzSafeAny>;
  heightTable = { x: '100%', y: '25em' };
  @Input() bodySendApprove?: ExistingEmployee;
  @Input() concurrentPositionDTOList: ConcurrentPositionDTOList[] = [];
  @Output() closeEvent = new EventEmitter<boolean>();
  @Output() bodyExistingEmp = new EventEmitter<ExistingEmployee>();

  constructor(
    injector: Injector,
    private readonly service: ProposedHandleService
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.concurrentPositionDTOList = this.concurrentPositionDTOList.map(
      (item) => {
        return {
          ...item,
          concurrentEmployee:
            item?.concurrentEmployeeCode +
            ' - ' +
            item?.concurrentEmployeeName +
            ' - ' +
            item?.concurrentTitle,
          employee:
            item?.employeeCode +
            ' - ' +
            item?.employeeName +
            ' - ' +
            item?.employeeTitle,
        };
      }
    );
    this.isLoading = true;
    this.service.listFormGroupConcurrentPosition().subscribe(
      (res) => {
        this.listFormGroup = res?.data?.formGroupList || [];
        this.isLoading = false;
      },
      (err) => {
        this.isLoading = true;
        this.toastrCustom.error(err?.error?.message);
      }
    );
  }

  submit() {
    const uniqueConcurrentPos = new Set(
      this.concurrentPositionDTOList.map((item) => item.formGroup)
    );
    if (uniqueConcurrentPos.size < this.concurrentPositionDTOList?.length) {
      this.toastrCustom.warning(
        this.translate.instant('development.formLD.validate.formGroup')
      );
      return;
    }
    this.isSubmitted = true;
    this.listSubmitEmployee = this.concurrentPositionDTOList.map((item) => {
      return {
        employeeCode: item.employeeCode,
        employeeName: item.employeeName,
        employeeTitle: item.employeeTitle,
        concurrentEmployeeCode: item.concurrentEmployeeCode,
        concurrentEmployeeName: item.concurrentEmployeeName,
        concurrentTitle: item.concurrentTitle,
        formGroup: item.formGroup,
      }
    })
    const body = {
      id: this.bodySendApprove?.id || 0,
      proposeType: this.bodySendApprove?.proposeType || 0,
      isShow: this.bodySendApprove?.isShow || false,
      isShowForConcurrent: true,
      isSuccess: true,
      positionDTOListReq: this.listSubmitEmployee,
    };
    this.closeEvent.emit(true);
    this.bodyExistingEmp.emit(body);
  }

  closeModal() {
    this.closeEvent.emit(false);
  }
}
