import { Component, EventEmitter, Injector, Input, OnInit, Output } from '@angular/core';
import { EmployeeAppointment, FormGroupList } from '@hcm-mfe/learn-development/data-access/models';
import { ProposeMultipleEmployeeService } from "@hcm-mfe/learn-development/data-access/services";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import { NzModalRef } from 'ng-zorro-antd/modal';



@Component({
  selector: 'app-proposed-appointment-renewal-emp',
  templateUrl: './proposed-appointment-renewal-emp.component.html',
  styleUrls: ['./proposed-appointment-renewal-emp.component.scss'],
})
export class ProposedAppointmentRenewalEmpComponent extends BaseComponent implements OnInit {

  @Output() saveEmployee = new EventEmitter<EmployeeAppointment[]>();
  @Output() closeEvent = new EventEmitter<boolean>();
  @Input()  lisEmployeeCreate: EmployeeAppointment[] = [];
  @Input()  formGroupList: FormGroupList[] = [];


  constructor(private readonly service: ProposeMultipleEmployeeService,private readonly modalRef: NzModalRef, injector: Injector) {
    super(injector);
  }



  ngOnInit(): void {
  }

  closeModal() {
    this.modalRef?.destroy();
  }

  save() {
    if (this.lisEmployeeCreate.filter((item) => !item.formGroupEmp)?.length > 0) {
      this.toastrCustom.warning(this.translate.instant('development.commonMessage.E016'));
      return;
    } else {
      this.saveEmployee.emit(this.lisEmployeeCreate);
    }

  }


}
