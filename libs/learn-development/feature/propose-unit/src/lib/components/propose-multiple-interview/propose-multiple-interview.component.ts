import { Component, EventEmitter, Injector, Input, Output, TemplateRef, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Constants, maxInt32, ScreenType, SessionKey, userConfig, Utils } from "@hcm-mfe/learn-development/data-access/common";
import {
  DataCommon,
  EmployeeInfoMultiple, EmployeeInfoMultipleEdit, FormGroupMultipleList, FormTTQD, IssueTransfer, JobDTOS, LevelTitle, ListFileTemplate, ParamSearchPosition, Proposed,
  ProposedCreate,
  ProposeDetail,
  ProposedModel,
  ProposeEmployeeCreateDTO, ProposeRotationReasons, WithdrawPropose
} from "@hcm-mfe/learn-development/data-access/models";
import { ProposedUnitService, ProposeMultipleEmployeeService } from "@hcm-mfe/learn-development/data-access/services";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import { cleanData, cleanDataForm } from "@hcm-mfe/shared/common/utils";
import {MBTableConfig, Pageable, Pagination} from "@hcm-mfe/shared/data-access/models";
import { User } from "@hcm-mfe/system/data-access/models";
import { saveAs } from 'file-saver';
import * as moment from 'moment';
import { NzSafeAny } from "ng-zorro-antd/core/types";
import { NzModalRef } from 'ng-zorro-antd/modal';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { forkJoin, of, Subscription } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ProposeMultipleEmployeeComponent } from "../propose-multiple-employee/propose-multiple-employee.component";
import { ProposeTemplateComponent } from "../propose-template/propose-template.component";
import { WithdrawProposeComponent } from '../withdraw-propose/withdraw-propose.component';

@Component({
  selector: 'app-propose-multiple-interview',
  templateUrl: './propose-multiple-interview.component.html',
  styleUrls: ['./propose-multiple-interview.component.scss'],
})
export class ProposeMultipleInterviewComponent extends BaseComponent {
  @Input() dataCommon: DataCommon = {
    jobDTOS: [], // vị trí
    typeDTOS: [], // loại đề xuất
    formDisplaySearchDTOS: [], // hình thức
    proposeRotationReasonConfigDTOS: [], // lý do luân chuyển
    statusList: [],
    flowStatusApproveConfigList: [],
    flowStatusPublishConfigList: [],
    formGroupMultipleList: <FormGroupMultipleList[]>[],
    levelTitle:<LevelTitle[]>[],
  };
  @Input() typeAction?: ScreenType;
  @Input() dataForm: any;
  @Input() screenCode = '';
  @Output() submitData: EventEmitter<any> = new EventEmitter();
  @Output() submitReport: EventEmitter<any> = new EventEmitter<any>();
  @Output() loadProposedUpdate: EventEmitter<any> = new EventEmitter<any>();
  @Output() onLoading = new EventEmitter<boolean>();
  @Output() checkFlowStatusElement = new EventEmitter<boolean>();

  @Input() proposeCategory = '';
  @Input() isScreenHandle = false;
  type?: ScreenType;

  proposeList: ProposedModel[] = [];
  itemPropose?: ProposeEmployeeCreateDTO;
  displayReport = false;

  // stateNumber = new FormControl(null);
  // submissionDate = new FormControl(null);
  // decisionNumber = new FormControl(null);
  // signedDate = new FormControl(null);
  @Input() formGroupCreate: number | null = null;
  isSubmitted = false;
  isSubmittedTable = false;

  formAppletEmployee = new FormGroup({
    formCode: new FormControl(null, [Validators.required]),
    currentUnit: new FormControl(null, [Validators.required]),
    currentTitle: new FormControl(null, [Validators.required]),
    sameCurrentTitle: new FormControl(false),
    implementDate: new FormControl(null, [Validators.required]),
    minimumTime: new FormControl(null),
    maximumTime: new FormControl(null),
  });

  modalRef?: NzModalRef;
  listEmployeeInfo: EmployeeInfoMultiple[] = [];
  @Input() proposeId: number | null = null;
  @Input() proposeDetailId: number | null = null;
  idApprove = 0;
  currentUser?: User;
  proposeDTO?: ProposedCreate;
  proposeLogs = [];
  listNotifyDecision = [];
  listPromulgate = Constants.ListPromulgate;
  // fileListSelect1: NzUploadFile[] = [];
  fileListSelect2: NzUploadFile[] = [];
  fileListSelectTT: NzUploadFile[] = [];
  listFileNotifyDecision1: NzUploadFile[] = [];
  listFileNotifyDecision2: NzUploadFile[] = [];
  fileListSelectQD: NzUploadFile[] = [];
  flowCodeConstants = Constants.FlowCodeConstants;
  formGroupListConst = Constants.FormGroupList;
  formGroupDivison = Constants.FormGroupDivison;

  formTTQD = new FormGroup({
    statementNumber: new FormControl(null, [Validators.required]),
    submissionDate: new FormControl(null, [Validators.required]),
    decisionNumber: new FormControl(null, [Validators.required]),
    signedDate: new FormControl(null, [Validators.required]),
    typeStatement: new FormControl(null),
    id: new FormControl(null),
  });
  isSubmitFormTTQD = false;
  reasonsReject = '';
  @Input() issueLevelName = '';
  @Input() flowStatusCode = '';
  unitIssueLevel = this.translate.instant('development.proposed-unit.table.unit');
  @Input() proposeDecisionStatement?: FormTTQD;
  @Input() statusPublishCode = '';
  @Input() proposeUnit?: Proposed;

  typeIssueCorrection = false;
  checkStatusElement = false;

  paramDetail = {
    ghiNhanyKienPd: null,
  };
  approveParams = {
    groupProposeType: null,
    action: null,
  };
  listProposeCategory = Constants.ListProposeCategory;
  functionCode = '';
  pageable: Pageable = {
    totalElements: 0,
    currentPage: 0,
    size: userConfig.pageSize,
  };
  hasDecision = true;
  viewAppletEmployee = true;
  iconStatus = Constants.IconStatus.UP;
  isExpand = false;
  screenCodeConst = Constants.ScreenCode;
  listEmployeeInfoTotal = [];
  checkFlowStatusCodeElement = false;
  rotationTimeViewMin = false;
  rotationTimeViewMax = false;
  jobDTOS: JobDTOS[] = [];
  subs: Subscription[] = [];
  proposeDetailIdSame: number[] = [];
  elementPrintReport = false;
  formProposeMultiple: FormGroup;
  listLevel = Constants.Level;
  planAlternative = Constants.PlanAlternative;
  rotationList:  ProposeRotationReasons[] = [];
  jobDTOSOffer: JobDTOS[] = [];
  // approvalCreate = false;
  // viewScreenCreate = false;
  isSeeMore = false;
  quantityMapWithdraw = new Map<number, EmployeeInfoMultiple>();
  checkedWithdrawAll = false;
  categoryProposed = Constants.ProposeCategory;
  // editTCNS = false;
  viewWithdrawNoteName = false;
  handlePropose = false;

  //logs phân trang
  pageNumber = 0;
  pageSize =  userConfig.pageSize;
  heightTable = { x: '50vw', y: '16em'};
  tableConfig!: MBTableConfig;
  pagination = new Pagination();
  @ViewChild('dateLog', {static: true}) dateLog!: TemplateRef<NzSafeAny>;

  constructor(
    injector: Injector,
    private readonly service: ProposedUnitService,
    private readonly proposeMultipleService: ProposeMultipleEmployeeService,
    private readonly serviceProposed: ProposedUnitService
  ) {
    super(injector);
    this.functionCode = this.route.snapshot?.data['code'];
    this.handlePropose = this.route.snapshot?.data['handlePropose'];
    this.formProposeMultiple = this.fb.group({
      arrayProposeMultiple: this.fb.array([]),
    });
  }

  createFormGroups(data: EmployeeInfoMultiple[], isEdit = false, index?: number) {
    data?.forEach((item: EmployeeInfoMultiple) => {
      const formRotationCode = item.formRotationCode ? +item.formRotationCode : null;

      const contentUnit = item?.contentUnitCode ?

      {
        orgId: item?.contentUnitCode,
        orgName: item?.contentUnitName,
        pathId: item?.contentUnitPathId,
        pathResult: item?.contentUnitPathName,
        orgLevel: item?.contentUnitTreeLevel,
      } : null;

      const form = this.fb.group({
        cancelProposeRetractFlag: [item?.cancelProposeRetractFlag || null],
        contentUnit: [contentUnit || null, Validators.required],
        contentTitleCode: [item?.contentTitleCode || null, Validators.required],
        contentLevel: [item?.contentLevel || null, Validators.required],
        strengths: [item?.proposeComment?.strengths || null, Validators.required],
        weaknesses: [item?.proposeComment?.weaknesses || null, Validators.required],
        alternativeType: item?.proposeAlternative ? item?.proposeAlternative[0].alternativeType : null,//Phương án thay thế
        replacementEmployeeCode: item?.proposeAlternative ? item?.proposeAlternative[0]?.replacementEmployeeCode : null,// Mã nhân viên thay thế
        replacementEmployeeName: item?.proposeAlternative ? item?.proposeAlternative[0]?.replacementEmployeeName : null,// Mã nhân viên thay thế
        replacementEmployeeTitleCode: item?.proposeAlternative ? item?.proposeAlternative[0]?.replacementEmployeeTitleCode : null,// Mã nhân viên thay thế
        replacementEmployeeTitleName: item?.proposeAlternative ? item?.proposeAlternative[0]?.replacementEmployeeTitleName : null,// Mã nhân viên thay thế
        replacementEmployee: item?.proposeAlternative ? item?.proposeAlternative[0]?.replacementEmployee : null,
        //
        handoverEmployeeCode: item?.proposeAlternative ? item?.proposeAlternative[0]?.handoverEmployeeCode : null,// Mã nhân viên thay thế
        handoverEmployeeName: item?.proposeAlternative ? item?.proposeAlternative[0]?.handoverEmployeeName : null,// Mã nhân viên thay thế
        handoverEmployeeTitleCode: item?.proposeAlternative ? item?.proposeAlternative[0]?.handoverEmployeeTitleCode : null,// Mã nhân viên thay thế
        handoverEmployeeTitleName: item?.proposeAlternative ? item?.proposeAlternative[0]?.handoverEmployeeTitleName : null,// Mã nhân viên thay thế
        handoverEmployee: item?.proposeAlternative ? item?.proposeAlternative[0]?.handoverEmployee : null,// Mã nhân viên thay thế


        formName: [this.dataCommon.formGroupMultipleList?.find((i) => i.code === formRotationCode)?.formCode || null, Validators.required],//Hình thức
        implementDate: [item?.implementDate|| null, Validators.required],
        minimumTime: [item?.minimumTime || null, Validators.required],
        maximumTime: [item?.maximumTime || null, Validators.required],
        rotationReasonID: [item?.proposeRotationReasons?.map((i) => { return i.rotationReasonID ? +i.rotationReasonID : null }) || [], Validators.required],
        isDecision: item?.isDecision || false,
        isIncome: item?.isIncome || false,
        isInsurance: item?.isInsurance || false,


      });
      if (isEdit) {
        this.arrayProposeMultiple.at(index).patchValue(form.getRawValue());
      } else {
        (this.formProposeMultiple.controls['arrayProposeMultiple'] as FormArray).push(form);
      }
    });
  }

  get arrayProposeMultiple(): NzSafeAny {
      if (this.formProposeMultiple.get('arrayProposeMultiple')) {

      return this.formProposeMultiple.get('arrayProposeMultiple') as FormArray;
    }
    return null;
  }


  ngOnChanges(): void {
    if (this.dataCommon.formGroupMultipleList?.length === 1) {
      this.formAppletEmployee.get('formCode')?.setValue(this.dataCommon.formGroupMultipleList[0]?.formCode);
    }
  }

  changeCurrentTitl(e: any) {
    if (e) {
      this.formAppletEmployee.get('currentTitle')?.disable();
    } else {
      this.formAppletEmployee.get('currentTitle')?.enable();
    }
    this.formAppletEmployee.get('currentTitle')?.setValue(null);
  }


  ngOnInit(): void {

    // Check view button app dụng cho màn sửa theo trạng thái
    this.viewAppletEmployee = this.checkStatusEditDelete({ flowStatusCode: this.flowStatusCode });

    // Khác màn sửa thì view tt
    if (this.typeAction !== ScreenType.Create) {
      this.hasDecision = this.dataForm?.hasDecision;
      if (localStorage.getItem(SessionKey.CURRENCY_USER)) {
        this.currentUser = JSON.parse(localStorage.getItem(SessionKey.CURRENCY_USER)!);
      }

      if (this.proposeDecisionStatement) {
        this.formTTQD.patchValue({
          statementNumber: this.proposeDecisionStatement?.statementNumber,
          submissionDate: this.proposeDecisionStatement?.submissionDate,
          decisionNumber: this.proposeDecisionStatement?.decisionNumber,
          signedDate: this.proposeDecisionStatement?.signedDate,
          typeStatement: this.proposeDecisionStatement?.typeStatement,
          id: this.proposeId,
        });
      }

      const statementHR1 = this.dataForm?.fileDTOList?.find((item: any) => item.type === 'statementHR1');
      if (statementHR1) {
        this.fileListSelect2.push({
          uid: statementHR1.docId,
          name: statementHR1.fileName,
          status: 'done',
          id: statementHR1.id,
        });
      }

      const fileDecision = this.dataForm?.fileDTOList?.find((item: any) => item.type === 'decision');
      if (fileDecision) {
        this.fileListSelectQD.push({
          uid: fileDecision.docId,
          name: fileDecision.fileName,
          status: 'done',
          id: fileDecision.id,
        });
      }

      const fileStatement = this.dataForm?.fileDTOList?.find((item: any) => item.type === 'statement');
      if (fileStatement) {
        this.fileListSelectTT.push({
          uid: fileStatement.docId,
          name: fileStatement.fileName,
          status: 'done',
          id: fileStatement.id,
        });
      }

      forkJoin([
        this.service
          .getLogs(this.proposeId!, this.proposeCategory, this.screenCode)
          .pipe(catchError(() => of(undefined))),
        this.service.viewDetailFile(this.proposeId!, this.proposeCategory).pipe(catchError(() => of(undefined))),
        this.proposeMultipleService.listEmpPropose({ id: this.proposeId,screenCode: this.screenCode  }).pipe(catchError(() => of({ data: [] }))),
      ]).subscribe(
        ([proposeLogs, listNotifyDecision, listEmployeeInfo]) => {
          // GET EMP
          this.listEmployeeInfoTotal = listEmployeeInfo?.data?.content || [];
          this.checkFlowStatusCodeElement = this.listEmployeeInfoTotal.filter
          ((item: ProposeDetail) => item?.flowStatusCode !== Constants.FlowCodeConstants.KSDongY).length === 0;//check flowStatusCode của element
          this.checkFlowStatusElement.emit(this.checkFlowStatusCodeElement);// truyền dữ liệu sang màn handle
          this.pageable.totalElements = this.listEmployeeInfoTotal.length;
          this.viewWithdrawNoteName = this.listEmployeeInfoTotal.filter((item: EmployeeInfoMultiple) => item.withdrawNote).length > 0;
          this.listEmployeeInfoTotal?.forEach((item: EmployeeInfoMultiple) => {
            item.flowStatusPublishName = this.dataCommon.flowStatusPublishConfigList?.find(
              (itemStatus) => item.flowStatusPublishCode === itemStatus.key
            )?.role;
            this.onItemCheckedWithdraw(item, item.cancelProposeRetractFlag!);
          });
          this.pageChange(1);
          // nếu phân trang các bản ghi thành phần thì gán giá trị, đã call api listEmployeePropose
          this.proposeLogs = proposeLogs?.data?.content || [];

          const dataNotifyDecision = listNotifyDecision?.data;
          if (dataNotifyDecision?.length > 0) {
            this.listFileNotifyDecision2 = dataNotifyDecision[0]?.decisionSignId
              ? [
                  {
                    id: dataNotifyDecision[0].id,
                    uid: dataNotifyDecision[0].decisionSignId,
                    name: dataNotifyDecision[0].decisionSignName,
                    status: 'done',
                  },
                ]
              : [];

            this.listFileNotifyDecision1 =
              dataNotifyDecision?.decisionId?.length > 0
                ? [
                    {
                      id: dataNotifyDecision.id,
                      uid: dataNotifyDecision.decisionId[0],
                      name: dataNotifyDecision.decisionName[0],
                      status: 'done',
                    },
                  ]
                : [];
          }
          this.displayReport = !this.dataForm?.externalApprove;
          this.checkReportStatus();
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        }
      );
    } else if (this.typeAction === ScreenType.Create && this.proposeId) {
      this.getInfoEmp();
    }
  }

  downloadFile = (file: NzUploadFile) => {
    this.serviceProposed.downloadFile(+file?.uid, file?.name).subscribe(
      () => {
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  };

  checkAlternativePropose(alternativeType?: number) {
    if (alternativeType === 0) {
      return this.translate.instant('development.proposed-unit.proposeAlternative.in');
    } else if (alternativeType === 1) {
      return this.translate.instant('development.proposed-unit.proposeAlternative.out');
    } else if (alternativeType === 2) {
      return this.translate.instant('development.proposed-unit.proposeAlternative.new');
    }
  }

  emitDataStatement() {
    if (this.screenCode === this.screenCodeConst.HRDV && !this.displayReport) {
      this.formTTQD.controls['statementNumber'].clearValidators();
      this.formTTQD.controls['statementNumber'].updateValueAndValidity();
      this.formTTQD.controls['submissionDate'].clearValidators();
      this.formTTQD.controls['submissionDate'].updateValueAndValidity();
    }
    if (this.listEmployeeInfo.length > 0) {
      if (!(this.issueLevelName === this.unitIssueLevel && !this.dataForm.isHo)
        || !this.checkLeaderApprove(this.flowStatusCode)) {
        this.formTTQD.removeControl('decisionNumber');
        this.formTTQD.removeControl('signedDate');
      }
      if (!this.formTTQD.valid && this.flowStatusCode !== this.flowCodeConstants.HANDLING6) {
        this.isSubmitFormTTQD = true;
        this.scrollToValidationSection('reportUnit', 'development.commonMessage.validateReportUnit');
        return;
      }
      if (
        (this.typeAction !== ScreenType.Detail &&
          !(this.screenCode === this.screenCodeConst.PTNL || this.screenCode === this.screenCodeConst.KSPTNL) )
        || (this.typeAction === ScreenType.Detail && this.checkFlowStatusCodeElement)
        || (this.flowStatusCode === Constants.FlowCodeConstants.HANDLING6 && this.typeAction !== ScreenType.Detail)
      ) {
        this.formTTQD.get('id')?.setValue(this.proposeId);
        const newForm = { ...cleanDataForm(this.formTTQD) };
        newForm.hasDecision = this.hasDecision;
        newForm.externalApprove = !this.displayReport;
        return newForm;
      }
    } else {
      this.toastrCustom.error(this.translate.instant('development.proposed-unit.messages.dontApplyStatement'));
      return null;
    }
  }

  checkLeaderApprove(status: string) {
    const list = ['1', '3'];
    return list.indexOf(status) > -1;
  }

  getReasonsReject() {
    return this.reasonsReject;
  }

  submit() {
    const paramStatement = this.emitDataStatement();
    if (paramStatement) {
      this.isLoading = true;
      paramStatement.typeStatement = this.screenCode === Constants.ScreenCode.HRDV
        ? Constants.TypeStatement.HRDV
        : Constants.TypeStatement.TCNS;
      this.service.saveStatementDecision(paramStatement).subscribe(
        (res) => {
          this.toastrCustom.success(this.translate.instant('development.interview-config.notificationMessage.success'));
          this.isLoading = false;
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        }
      );
    } else {
      return;
    }
  }

  editProposeItemInline(item: EmployeeInfoMultiple, index: number) {
    const formName = this.arrayProposeMultiple.at(index).get('formName').value;
    this.onChangeFormName(formName, index);
    this.listEmployeeInfo.forEach((i) => {
      if (item.id !== i.id) {
        i.isEdit = false;
      }
    })
    item.isEdit = true;
    this.onChangeUnitOffer(item.contentUnitCode!, index, item.contentTitleCode)
  }

  // xem chi tiết từng thành phần
  detailProposeItem(item: EmployeeInfoMultiple) {
    this.routerFromElement(item, 'detail');
  }

  routerFromElement(item: EmployeeInfoMultiple, type: string) {
    this.sessionService.setSessionData(`${SessionKey.URL_PROPOSE_ELEMENT}_${Constants.ProposeCategory.GROUP}`, this.proposeUnit);
    const url = this.router.url;
    this.service.viewParent(item.id!).subscribe((res) => {
      this.router.navigateByUrl(Utils.getUrlByFunctionCode(this.functionCode)).then(() => {
        this.router.navigate([Utils.getUrlByFunctionCode(this.functionCode), type], {
          queryParams: {
            idGroup: this.dataForm?.id,
            id: item.proposeId,
            proposeDetailId: item.id,
            proposeCategory: Constants.ProposeCategory.ELEMENT,
            categoryParent: this.proposeCategory,
            parentID: item.isGroup || item.groupByKSPTNL ? res.data?.groupId : res.data?.parentId,
            proposeType: Constants.ProposeTypeID.ELEMENT,
            relationType: Constants.RelationType.ONE,
            proposeGroupId: this.proposeCategory === Constants.ProposeCategory.GROUP ? this.proposeId : '',
            elementPrintReport: this.elementPrintReport,
          },
          state: {backUrl: url},
          skipLocationChange: true,
        });
      });
    })
  }

  deleteProposeDetail(item: any) {
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.proposed-unit.messages.delete'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        this.service.deleteProposeListLD(item.id, this.listProposeCategory[2].id).subscribe(
          () => {
            this.toastrCustom.success(this.translate.instant('development.proposed-unit.messages.deleteSuccess'));
            this.arrayProposeMultiple.clear();
            this.getInfoEmp();
            this.isLoading = false;
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
      },
    });
  }

  checkReportStatus() {
    this.submitReport.emit(this.displayReport);
    if (this.displayReport) {
      return true;
    } else{
      return false;
    }
  }


  showModalPropose(footerTmpl: TemplateRef<any>, sortOrder?: number) {
    this.modalRef = this.modal.create({
      nzTitle: 'Tìm kiếm nhân viên',
      nzWidth: this.getInnerWidth(),
      nzContent: ProposeMultipleEmployeeComponent,
      nzComponentParams: {
        listEmployeeModal: [],
        isEmp: false,
      },
      nzFooter: footerTmpl,
    });
    this.modalRef.afterClose.subscribe((result) => {
      const listEmployeeModal = result;
      if (listEmployeeModal?.length > 0) {
        const listCode: string[] = [];
        listEmployeeModal?.forEach((item: any) => {
          if (item?.empCode) {
            listCode.push(item.empCode);
          }
        });
        const proposeRotationReasons = Constants.ListProposeRotationReasonsNCN;
        const paramAddEmp = {
          sortOrder: sortOrder ? sortOrder : null,
          proposeId: this.proposeId,
          employeeCodes: listCode,
          proposeUnitRequest: this.screenCode === Constants.ScreenCode.HRDV ? this.proposeUnit?.proposeUnitRequest : null,
          formGroup: this.formGroupCreate,
          proposeRotationReasons: proposeRotationReasons,
          proposeUnitRequestName: this.screenCode === Constants.ScreenCode.HRDV ? this.proposeUnit?.proposeUnitRequestName : null,
        };
        this.isLoading = true;
        this.proposeMultipleService.addEmployee(paramAddEmp, this.screenCode).subscribe(
          (res) => {
            // Khi thêm mới xong vào màn cập nhật
            if (this.typeAction === ScreenType.Create) {
              this.router.navigate([Utils.getUrlByFunctionCode(this.functionCode), 'update-propose-interview'], {
                queryParams: {
                  id: this.proposeId,
                  proposeType: 0,
                  proposeCategory: Constants.ProposeCategory.ORIGINAL,
                  flowStatusCode: this.screenCode === Constants.ScreenCode.HRDV ? Constants.FlowCodeConstants.CHUAGUI1 : Constants.FlowCodeConstants.NHAP,
                  relationType: true,
                },
                skipLocationChange: true,
              });
            } else {
              this.arrayProposeMultiple.clear();
              this.getInfoEmp();
            }
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
      }
    });
  }

  showModalTemplate(listFileTemplate: ListFileTemplate[]) {
    this.isLoading = false;
    const listDecision = listFileTemplate?.filter((item) => item?.typeCode === Constants.TemplateType.QD && item?.subTypeCode !== Constants.TemplateType.PLDCDD);
    const listNotDecision = listFileTemplate?.filter((item) => item?.typeCode !== Constants.TemplateType.QD || (item?.typeCode === Constants.TemplateType.QD && item?.subTypeCode === Constants.TemplateType.PLDCDD));
    if (listDecision?.length > 1) {
      this.modalRef = this.modal.create({
        nzTitle: 'Chọn tờ trình muốn xuất',
        nzWidth: 800,
        nzContent: ProposeTemplateComponent,
        nzComponentParams: {
          listDecision: listDecision
        },
        nzFooter: null,
      });
      this.modalRef.componentInstance.eventIdFile.subscribe((idFile: number) => {

        this.modalRef?.destroy();
        this.isLoading = true;
        const itemFileDecision = listDecision.find((item) => item?.id === +idFile)
        const listNewFileDecision =
          [...listNotDecision,
            itemFileDecision];
        this.dowloadAllFile(listNewFileDecision as ListFileTemplate[]);
        this.modalRef?.destroy();
      })
      this.modalRef?.componentInstance.closeEvent.subscribe((data:boolean) => {
        if (!data) {
          this.modalRef?.destroy();
        }
      })
    }else {
      this.isLoading = true;
      this.dowloadAllFile(listFileTemplate);
    }
  }

  dowloadAllFile (listNewFileDecision: ListFileTemplate[]) {
    const listNewTemplateCode = listNewFileDecision.map((item) => {
      return item.templateCode
    });
    this.service.updateTemplateUsed(listNewTemplateCode).subscribe();
    listNewFileDecision?.forEach((item ) => {
      this.service.callBaseUrl(item?.linkTemplate!).subscribe(
        (data) => {
          const contentDisposition = data?.headers.get('content-disposition');
          const filename = contentDisposition?.split(';')[1]?.split('filename')[1]?.split('=')[1]?.trim();
          const blob = new Blob([data.body], {
            type: 'application/octet-stream',
          });
          saveAs(blob, filename);
          this.isLoading = false;
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        }
      );
    });
  }

  pageChange(page: number) {
    if (this.isLoading) {
      return;
    }
    this.pageable.currentPage = page - 1;
    const start: number = this.pageable.currentPage * this.pageable.size!;
    this.listEmployeeInfo = this.listEmployeeInfoTotal.slice(start, this.pageable.size! + start);
    this.listEmployeeInfo = this.listEmployeeInfo.map((item, index) => { return{...item,
      alternativeTypeCheck: item?.proposeAlternative ? item?.proposeAlternative[0]?.alternativeType === 0 : false}
     })
    this.createFormGroups(this.listEmployeeInfo);
  }

  getInfoEmp() {
    this.isLoading = true;
    // nếu chốt phân trang thì bỏ pageable.size * 10, chỉ để pageable.size
    this.proposeMultipleService
      .listEmpPropose({ id: this.proposeId,screenCode: this.screenCode  })
      .pipe(catchError(() => of({ data: [] })))
      .subscribe((res) => {
        this.listEmployeeInfoTotal = res?.data?.content || [];
        this.viewWithdrawNoteName = this.listEmployeeInfoTotal.filter((item: EmployeeInfoMultiple) => !item.withdrawNote).length > 0;
        this.pageable.totalElements = this.listEmployeeInfoTotal.length;
        this.isLoading = false;
        this.pageChange(1);
      });
  }

  getInnerWidth(): number {
    if (window.innerWidth > 767) {
      return window.innerWidth / 1.5 > 1368 ? 1368 : window.innerWidth / 1.3;
    }
    return window.innerWidth;
  }

  // getFormGroup(value: number) {
  //   this.formGroup = value;
  //   this.viewScreenCreate =  Constants.AddFormGroup.includes(this.formGroup);
  // }

  applyEmployee() {
    // Check length
    const dataForm = this.formAppletEmployee.getRawValue();
    if (this.listEmployeeInfo.length === 0) {
      this.toastrCustom.error(this.translate.instant('development.proposed-unit.messages.dontApplyEmp'));
      return;
    }
    if (!this.formAppletEmployee.valid) {
      this.isSubmitted = true;
      return;
    }
    const minimumTime = this.formAppletEmployee.get('minimumTime')?.value;
    const maximumTime = this.formAppletEmployee.get('maximumTime')?.value
    if (minimumTime && maximumTime && minimumTime > maximumTime) {
      this.toastrCustom.error(this.translate.instant('development.commonMessage.E006'));
      return;
    }

    if (dataForm.sameCurrentTitle && this.jobDTOS.length === 0) {
      this.toastrCustom.error(this.translate.instant('development.commonMessage.E002'));
      return;
    }
    // kiểm tra các bản ghi không cùng chức danh tại đơn vị đích
    const proposeCode = this.checkSameContentTitle();
    if (dataForm.sameCurrentTitle && proposeCode.length > 0) {
      this.toastrCustom.error(
        this.translate.instant('development.commonMessage.E003', { codes: proposeCode.join(', ') })
      );
    }
    this.isLoading = true;
    const itemFormCode: any = this.dataCommon.formGroupMultipleList?.find((item: any) => item.formCode === dataForm.formCode);
    const itemJobDTO: any = this.jobDTOS.find((item: any) => item.jobId === dataForm?.currentTitle);
    const paramApplet = {
      proposeDetailId: dataForm.sameCurrentTitle ? this.proposeDetailIdSame : [],
      proposeId: this.proposeId,
      formCode: itemFormCode?.code,
      formType: itemFormCode?.formCode,
      unitId: dataForm?.currentUnit?.orgId || null,
      unitName: dataForm?.currentUnit?.orgName || null,
      orgLevel: dataForm?.currentUnit?.orgLevel || null,
      pathId: dataForm?.currentUnit?.pathId || null,
      pathResult: dataForm?.currentUnit?.pathResult || null,
      jobId: itemJobDTO?.jobId || null,
      jobName: itemJobDTO?.jobName || null,
      formRotationTitle: dataForm.sameCurrentTitle,
      implementDate: this.formAppletEmployee.get('implementDate')?.value,
      minimumTime: this.formAppletEmployee.get('minimumTime')?.value,
      maximumTime: this.formAppletEmployee.get('maximumTime')?.value,
    };
    this.proposeMultipleService.appletEmployee(paramApplet, this.screenCode).subscribe(
      (res) => {
        this.isLoading = false;
        this.loadProposedUpdate.emit();
        this.arrayProposeMultiple.clear();
        this.getInfoEmp();
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

  checkSameContentTitle() {
    const proposeCode: number[] = [];
    this.listEmployeeInfoTotal.forEach((item: any) => {
      if (!this.jobDTOS?.find((itemTitle) => itemTitle.jobId === item?.employee?.jobId)) {
        proposeCode.push(item.id);
      } else {
        this.proposeDetailIdSame.push(item.id);
      }
    });
    return proposeCode;
  }

  checkIdParams() {
    if (this.proposeCategory === Constants.ProposeCategory.GROUP) {
      return this.proposeId;
    } else if (this.proposeCategory === Constants.ProposeCategory.ORIGINAL) {
      return this.proposeId;
    } else if (this.proposeCategory === Constants.ProposeCategory.ELEMENT) {
      return this.proposeDetailId;
    }
    return null;
  }

  checkTypeKSPTNL() {
    switch (this.proposeCategory) {
      case Constants.ProposeCategory.GROUP:
        return 0;
      case Constants.ProposeCategory.ORIGINAL:
        return 1;
      case Constants.ProposeCategory.ELEMENT:
        return 2;
      default:
        return null;
    }
  }


  //duyệt đề xuất PTNL
  approvePTNL(action: number) {
    this.idApprove = this.checkIdParams()!;
    const newApprove = { ...cleanData(this.approveParams) };
    newApprove.groupProposeType = this.checkTypeKSPTNL();
    newApprove.action = action;
    let title = '';
    let success = '';
    if (action !== 0 && !this.paramDetail.ghiNhanyKienPd) {
      this.toastrCustom.error(this.translate.instant('development.commonMessage.ideaRecordValidate'));
      return;
    }
    if (action === 0) {
      title = this.translate.instant('development.approvement-propose.messages.approvedConfirm');
      success = this.translate.instant('development.approvement-propose.messages.recordSuccess');
    } else if (action === 1) {
      title = this.translate.instant('development.approvement-propose.messages.rejectConfirm');
      success = this.translate.instant('development.approvement-propose.messages.recordRejectSuccess');
    } else {
      title = this.translate.instant('development.approvement-propose.messages.otherIdeaConfirm');
      success = this.translate.instant('development.approvement-propose.messages.recordOtherIdeaSuccess');
    }
    this.modalRef = this.modal.confirm({
      nzTitle: title,
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        this.service.approvePTNL(this.idApprove, this.paramDetail.ghiNhanyKienPd!, newApprove).subscribe(
          () => {
            this.toastrCustom.success(success);
            this.router.navigateByUrl(Utils.getUrlByFunctionCode(this.functionCode));
            this.isLoading = false;
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
      },
    });
  }

  //check hiển thị 3 nút ghi nhận đồng ý, k đồng ý vs ý kiến khác
  checkApproveThreeButton() {
    const elementApprove = this.listEmployeeInfoTotal.filter(
      (item: EmployeeInfoMultiple) => item?.flowStatusCode !== this.flowCodeConstants.KSDongY
    );
    const checkUserCreate = this.currentUser?.username === this.dataForm?.createdBy;
    return ((this.screenCode === this.screenCodeConst.KSPTNL && this.dataForm?.groupByKSPTNL && checkUserCreate) ||
        (this.screenCode === this.screenCodeConst.PTNL && checkUserCreate) &&
        elementApprove.length === 0 && this.typeAction !== this.screenType.create);
  }


  doRemoveFile2 = (file: NzUploadFile): boolean => {
    this.isLoading = true;
    let idDelete = 0;
    const fileListSelectTT = this.fileListSelect2.find((item) => item.uid === file.uid)
    if (fileListSelectTT) {
      idDelete = fileListSelectTT['id'];
    }
    const sub = this.service.deleteUploadFile(+idDelete).subscribe(
      (res) => {
        this.isLoading = false;
        this.fileListSelect2 = this.fileListSelect2.filter((item) => item['id'] !== idDelete);
      },
      (e) => {
        this.isLoading = false;
        this.getMessageError(e);
      }
    );
    this.subs.push(sub);
    return false;
  };

  beforeUpload2 = (file: File | NzUploadFile): boolean => {
    const formData = new FormData();
    formData.append('files', file as File);
    formData.append('proposeDetailId', new Blob([JSON.stringify(this.proposeId)], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('type', new Blob(['statementHR1'], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('groupProposeTypeId', new Blob(['0'], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('isIn', new Blob(['true'], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    const sub = this.service.uploadFile(formData).subscribe(
      (res) => {
        this.fileListSelect2 = [
          ...this.fileListSelect2,
          {
            uid: res.data?.docId,
            name: res.data?.fileName,
            status: 'done',
            id: res.data?.id,
          },
        ];
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
    return false;
  };

  beforeUploadTT = (file: File | NzUploadFile): boolean => {
    const formData = new FormData();
    formData.append('files', file as File);
    formData.append('proposeDetailId', new Blob([JSON.stringify(this.proposeId)], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('type', new Blob(['statement'], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('groupProposeTypeId', new Blob(['0'], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('isIn', new Blob(['true'], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    const sub = this.service.uploadFile(formData).subscribe(
      (res) => {
        this.fileListSelectTT = [
          ...this.fileListSelectTT,
          {
            uid: res.data?.docId,
            name: res.data?.fileName,
            status: 'done',
            id: res.data?.id,
          },
        ];
      },
      (e) => {
        this.getMessageError(e);
      }
    );
    this.subs.push(sub);
    return false;
  };

  doRemoveFileTT = (file: NzUploadFile): boolean => {
    this.isLoading = true;
    // const idDelete = <number>this.fileListSelectTT.find((item) => item.uid === file.uid)['id'];
    let idDelete = 0;
    const fileListSelectTT = this.fileListSelectTT.find((item) => item.uid === file.uid)
    if (fileListSelectTT) {
      idDelete = fileListSelectTT['id'];
    }
    const sub = this.service.deleteUploadFile(+idDelete).subscribe(
      (res) => {
        this.isLoading = false;
        this.fileListSelectTT = this.fileListSelectTT.filter((item) => item['id'] !== idDelete);
      },
      (e) => {
        this.isLoading = false;
        this.getMessageError(e);
      }
    );
    this.subs.push(sub);
    return false;
  };


  checkStatusHandle(item: EmployeeInfoMultiple) {
    if (this.typeAction !== ScreenType.Create && item.flowStatusCode && this.screenCode !== Constants.ScreenCode.HRDV) {
      return (this.typeAction === ScreenType.Update && Constants.ListHandleStatusMany.indexOf(item.flowStatusCode) > -1) || (this.typeAction === ScreenType.Detail && Constants.ViewButtonHandleRemove.includes(item.flowStatusCode));
    }
    return false;
  }

  checkStatusEditDelete(item: EmployeeInfoMultiple) {
    if (this.typeAction !== ScreenType.Create) {
      if (this.screenCode === Constants.ScreenCode.PTNL) {
        return (
          item?.flowStatusCode === Constants.FlowCodeConstants.HANDLING6
            && this.currentUser?.username === item?.createdBy
        ) || item?.flowStatusCode === Constants.FlowCodeConstants.NHAP;
      } else if (this.screenCode === Constants.ScreenCode.HRDV) {
        return item?.flowStatusCode === Constants.FlowCodeConstants.CHUAGUI1 ||
          item?.flowStatusCode === Constants.FlowCodeConstants.OTHERIDEA3;
      }
    } else {
      return true;
    }
    return false;
  }

  viewTextStatusApprove(codeStatus: string) {
    return this.dataCommon.flowStatusApproveConfigList?.find((item) => item.key === codeStatus)?.role || '';
  }
  // Hàm xử lý 1 thành phần trong GROUP
  handle(item: EmployeeInfoMultiple) {
    const param = {
      id: item.id,
      proposeType: 2,
    };
    if (param.proposeType !== 0) {
      this.isLoading = true;
      const sub = this.service.proposalProcessing(param).subscribe(
        (res) => {
          this.isLoading = false;
          this.router.navigate([Utils.getUrlByFunctionCode(this.functionCode), 'handle', 'element-of-group'], {
            queryParams: {
              screenCode: this.screenCode,
              id: item.proposeId,
              proposeDetailId: item.id,
              proposeCategory: Constants.ProposeCategory.ELEMENT,
              relationType: 0,
            },
            state: { backUrl: this.router.url },
            skipLocationChange: true,
          });
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        }
      );
      this.subs.push(sub);
    }
  }

  removeTheLump(item: EmployeeInfoMultiple) {
    this.isLoading = true;
    this.service.removeTheLump(this.proposeId!, item.id!, this.screenCode).subscribe(
      () => {
        this.toastrCustom.success(this.translate.instant('development.commonMessage.success'));
        this.arrayProposeMultiple.clear();
        this.getInfoEmp();
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

  checkStatusTheLump(item: EmployeeInfoMultiple) {
    if (this.screenCode === Constants.ScreenCode.KSPTNL) {
      return +item.flowStatusCode === Constants.EmployeeInfoMultipleFlowStatus.flowStatus7
        || (+item.flowStatusCode === Constants.EmployeeInfoMultipleFlowStatus.flowStatus8
          && item.groupByKSPTNL)
      && this.dataForm?.groupProposeTypeId !== 1;
    } else if (this.screenCode === Constants.ScreenCode.PTNL) {
      return ((this.typeAction === ScreenType.Detail && Constants.ViewButtonHandleRemove.includes(item.flowStatusCode))
        || (this.typeAction === ScreenType.Update
        && (Constants.ListStatusTheLump.indexOf(item.flowStatusCode) > -1
          || item.flowStatusCode === Constants.FlowCodeConstants.STATUS3A)))
          &&  this.dataForm?.groupProposeTypeId !== 1;
    } else {
      return false;
    }
  }

  checkProposeCode() {
    if (this.proposeCategory === Constants.ProposeCategory.ELEMENT) {
      return 2;
    } else if (this.proposeCategory === Constants.ProposeCategory.ORIGINAL) {
      return 1;
    } else if (this.proposeCategory === Constants.ProposeCategory.GROUP) {
      return 0;
    }
    return null;
  }

  dowloadStatement(action: number) {
    const params = {
      groupProposeType: this.checkProposeCode(),
      action: action,
      screenCode: this.screenCode,
    };
    this.isLoading = true;
    let listFile = [];
    const sub = this.service.exportReportDecision(this.proposeId!, params).subscribe(
      (res) => {
        listFile = res?.data;
        if (!listFile || listFile?.length === 0) {
          this.toastrCustom.error('Không có file nào!');
          this.isLoading = false;
        } else {
          this.showModalTemplate(listFile as ListFileTemplate[])
        }
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }

  // statusDisabledAttachPresentation(item: any) {
  //   const listDisabledAttach = ['6', '8', '10B1', '10C'];
  //   return listDisabledAttach.indexOf(item.flowStatusPublishCode) > -1;
  // }

  expandLog() {
    this.isExpand = !this.isExpand;
    if (this.isExpand) {
      this.iconStatus = Constants.IconStatus.DOWN;
    } else {
      this.iconStatus = Constants.IconStatus.UP;
    }
  }

  checkPTNLOrKSPNTViewTCNSNew(){
    return this.dataForm?.groupProposeTypeId === Constants.ProposeTypeID.GROUP
      && this.typeAction === ScreenType.Detail
      && (this.screenCode === this.screenCodeConst.PTNL || this.screenCode === this.screenCodeConst.KSPTNL)
      && (this.statusPublishCode || this.flowStatusCode === Constants.FlowCodeConstants.KSDongY)
      && this.currentUser?.username === this.dataForm?.createdBy;
  }




  disableSignDate = (dateVal: Date) => {
    if (this.dataForm.proposeDetails[0].rotationFromDate) {
      const rotationFromDate = new Date(this.dataForm.proposeDetails[0].rotationFromDate).getTime();
      return dateVal.valueOf() > rotationFromDate;
    }
    return false;
  };

  // Upload file
  beforeUploadQD = (file: File | NzUploadFile): boolean => {
    const formData = new FormData();
    formData.append('files', file as File);
    formData.append('proposeDetailId', new Blob([JSON.stringify(this.proposeId)], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('type', new Blob(['decision'], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('groupProposeTypeId', new Blob([JSON.stringify(0)], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('isIn', new Blob([JSON.stringify(this.displayReport)], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));

    const sub = this.service.uploadFile(formData).subscribe(
      (res) => {
        this.fileListSelectQD = [
          ...this.fileListSelectQD,
          {
            uid: res.data?.docId,
            name: res.data?.fileName,
            status: 'done',
            id: res.data?.id,
          },
        ];
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
    return false;
  };

  doRemoveFileQD = (file: NzUploadFile): boolean => {
    this.isLoading = true;
    let idDelete = 0;
    const fileListSelectTT = this.fileListSelectQD.find((item) => item.uid === file.uid)
    if (fileListSelectTT) {
      idDelete = fileListSelectTT['id'];
    }
    const sub = this.service.deleteUploadFile(+idDelete).subscribe(
      (res) => {
        this.isLoading = false;
        this.fileListSelectQD = this.fileListSelectQD.filter((item) => item['id'] !== idDelete);
      },
      (e) => {
        this.isLoading = false;
        this.getMessageError(e);
      }
    );
    this.subs.push(sub);
    return false;
  };

  checkRadioIsHCM() {
    return (
      this.screenCode === this.screenCodeConst.HRDV &&
      this.typeAction === ScreenType.Update &&
      ((this.issueLevelName !== this.unitIssueLevel)
      || (this.issueLevelName === this.unitIssueLevel && this.dataForm?.isHo))
    );
  }

  onChangeFormCode(e: FormGroupMultipleList) {
    const item = this.dataCommon.formGroupMultipleList?.find((itemformGroupMultiple) => itemformGroupMultiple.formCode === e);
      this.rotationTimeViewMax = item?.showMaxTime ? item?.showMaxTime : false;
      this.rotationTimeViewMin = item?.showMinTime ? item?.showMinTime : false;
    if (item?.formGroup === this.formGroupListConst.DCLEVEL.toString()) {
      this.formAppletEmployee.get('currentUnit')?.clearValidators();
      this.formAppletEmployee.get('currentTitle')?.clearValidators();
      this.formAppletEmployee.get('sameCurrentTitle')?.clearValidators();

    } else {
      this.formAppletEmployee.get('currentUnit')?.addValidators(Validators.required);
      this.formAppletEmployee.get('currentTitle')?.addValidators(Validators.required);
      this.formAppletEmployee.get('sameCurrentTitle')?.addValidators(Validators.required);
    }
    this.formAppletEmployee.get('currentUnit')?.updateValueAndValidity();
    this.formAppletEmployee.get('currentTitle')?.updateValueAndValidity();
    this.formAppletEmployee.get('sameCurrentTitle')?.updateValueAndValidity();
  }


  onChangeUnit(orgId: string) {
    this.jobDTOS = [];
    this.formAppletEmployee.get('currentTitle')?.setValue(null);
    if (orgId) {
      this.isLoading = true;
      const paramPosition: ParamSearchPosition = {
        orgId: orgId,
        jobId: '',
        status: 1,
        type: Constants.TYPE_SEARCH_POSITION,
        page: 0,
        size: maxInt32,
      };
      const sub = this.service.getPositionByUnit(paramPosition).subscribe(
        (res) => {
          const listPositionParent = res?.data?.content;
          this.jobDTOS = listPositionParent.map((item: any) => {
            return { jobId: item.jobId, jobName: item.jobName };
          });
          this.isLoading = false;
        },
        (e) => {
          this.isLoading = false;
        }
      );
      this.subs.push(sub);
    }
  }

  override ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
    this.modalRef?.destroy();
  }


  checkBtnUpload() {
    return (
      (this.dataForm?.groupByKSPTNL && this.screenCode === this.screenCodeConst.KSPTNL) ||
      (this.dataForm?.isGroup && this.screenCode === this.screenCodeConst.PTNL)
    );
  }


  showModalWithdraw(typeButton: number) {
    //0 là rút đề xuất, 1 là rút thành phần
    //ids là id của cha
    //idFlagComponent là id của thành phần có tick hủy đề xuất
    const ids: number[] = [];
    ids.push(this.proposeId!);
    this.modalRef = this.modal.create({
      nzTitle: this.translate.instant('development.proposed-unit.proposeMultiple.withdrawPropose'),
      nzWidth: '40vw',
      nzContent: WithdrawProposeComponent,
      nzComponentParams: {
        typeWithdraw: typeButton,
        ids: ids,
        idFlagComponent: this.getListOfIDComponent(),
        isApproveHCMBefore: this.dataForm?.externalApprove,
      },
      nzFooter: null,
    });
    this.modalRef.componentInstance.closeEvent.subscribe((data:boolean) => {
      if (data) {
        this.isLoading = true;
        this.modalRef?.componentInstance.sendBody.subscribe((body: WithdrawPropose) => {
          const sub = this.service.withdrawPropose(body).subscribe(() => {
            this.toastrCustom.success(
              this.translate.instant('development.withdrawPropose.withdrawProposeSuccess'));
            this.arrayProposeMultiple.clear();
            this.getInfoEmp();
            this.isLoading = false;
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          });
          this.subs.push(sub);
          this.modalRef?.destroy();
        });
      } else {
        this.modalRef?.destroy();
      }
    });
  }

  getListOfIDComponent(){
    const idList: number[] = [];
    this.quantityMapWithdraw.forEach((values) => {
      idList.push(values.id!);
    })
    return idList;
  }

  //check hiển thị nút rút thành phần
  checkWithdrawElement(){
    return (this.flowStatusCode === Constants.FlowCodeConstants.HANDLING5B ||
      this.flowStatusCode === Constants.FlowCodeConstants.HANDLING6) &&
      !this.checkDisplayButtonWithdrawAll();
  }

  //check hiển thị nút rút đề xuất (cả lô)
  checkWithdrawPropose(){
    return (this.flowStatusCode === Constants.FlowCodeConstants.HANDLING5B ||
      this.flowStatusCode === Constants.FlowCodeConstants.HANDLING6) &&
      this.checkDisplayButtonWithdrawAll() &&
        this.listEmployeeInfoTotal.filter(
          (item: ProposeDetail) =>
            Constants.ChangeStatusWithdraw.indexOf(item.flowStatusCode!) <= -1).length > 0;
  }

  checkColumCancelPropose() {
    const validStatusWithdraw = this.listEmployeeInfoTotal.filter(
      (item: ProposeDetail) =>
        Constants.ChangeStatusWithdraw.indexOf(item.flowStatusCode!) <= -1)?.length > 0;
    return this.screenCode === this.screenCodeConst.HRDV
      ? (validStatusWithdraw && this.checkFlagElement())
      : validStatusWithdraw && (this.listEmployeeInfoTotal.filter(
      (item: ProposeDetail) =>
        item.cancelProposeRetractFlag

    )?.length > 0) && !this.dataForm?.optionNote && this.checkFlagElement();
  }

  changeStatusWithdraw(proposeDetailId: number, flag: boolean){
    this.isLoading = true;
    const sub = this.service.changeStatusWithdraw(proposeDetailId).subscribe((status) => {
      if (!flag) {
        this.toastrCustom.success(this.translate.instant('development.withdrawPropose.withdrawCancelSuccess'));
      } else {
        this.toastrCustom.success(this.translate.instant('development.withdrawPropose.withdrawCancelSuccess2'));
      }
      this.arrayProposeMultiple.clear();
      this.getInfoEmp();
    },
    (e) => {
      this.getMessageError(e);
      this.isLoading = false;
    });
    this.subs.push(sub);
  }


  showModalEmp(footerTmpl: TemplateRef<any>, isEmpReplace?: boolean, index?: number) {
    this.modalRef = this.modal.create({
      nzTitle: 'Tìm kiếm nhân viên',
      nzWidth: this.getInnerWidth(),
      nzContent: ProposeMultipleEmployeeComponent,
      nzComponentParams: {
        listEmployeeModal: [],
        isEmp: true,
      },
      nzFooter: footerTmpl,
    });
    this.modalRef.afterClose.subscribe((result) => {
      const listEmployeeModal = result;
      if (listEmployeeModal?.length > 0) {
        if (isEmpReplace) {
          this.arrayProposeMultiple.at(index).get('replacementEmployeeCode').setValue(listEmployeeModal[0].empCode);
          this.arrayProposeMultiple.at(index).get('replacementEmployee').setValue(listEmployeeModal[0].fullName + '-' + listEmployeeModal[0].jobName + '-' + listEmployeeModal[0].orgName);
          this.arrayProposeMultiple.at(index).get('replacementEmployeeName').setValue(listEmployeeModal[0].fullName);
          this.arrayProposeMultiple.at(index).get('replacementEmployeeTitleCode').setValue(listEmployeeModal[0].jobId);
          this.arrayProposeMultiple.at(index).get('replacementEmployeeTitleName').setValue(listEmployeeModal[0].jobName);
        } else {
          this.arrayProposeMultiple.at(index).get('handoverEmployeeCode').setValue(listEmployeeModal[0].empCode);
          this.arrayProposeMultiple.at(index).get('handoverEmployee').setValue(listEmployeeModal[0].fullName + '-' + listEmployeeModal[0].jobName + '-' + listEmployeeModal[0].orgName);
          this.arrayProposeMultiple.at(index).get('handoverEmployeeName').setValue(listEmployeeModal[0].fullName);
          this.arrayProposeMultiple.at(index).get('handoverEmployeeTitleCode').setValue(listEmployeeModal[0].jobId);
          this.arrayProposeMultiple.at(index).get('handoverEmployeeTitleName').setValue(listEmployeeModal[0].jobName);
        }
      }
    });
  }

  onChangeUnitOffer(orgId: string, index: number,codeTitle?: number) {
    this.jobDTOSOffer = [];
    this.arrayProposeMultiple.at(index).get('contentTitleCode').setValue(null);
    if (orgId) {
      this.isLoading = true;
      const paramPosition: ParamSearchPosition = {
        orgId: orgId,
        jobId: '',
        status: 1,
        type: Constants.TYPE_SEARCH_POSITION,
        page: 0,
        size: maxInt32,
      };
      const sub = this.service.getPositionByUnit(paramPosition).subscribe(
        (res) => {
          const listPositionParent = res?.data?.content;
          this.jobDTOSOffer = listPositionParent.map((item: any) => {
            return { jobId: item.jobId, jobName: item.jobName };
          });
          this.arrayProposeMultiple.at(index).get('contentTitleCode').setValue(codeTitle || null);
          this.isLoading = false;
        },
        (e) => {
          this.isLoading = false;
        }
      );
      this.subs.push(sub);
    }
  }

  getFormGroupMultipleList(data: FormGroupMultipleList[]) {
    this.dataCommon.formGroupMultipleList = data;
  }

  saveProposeMultipleItem (item: EmployeeInfoMultiple,index?: number) {

    if (!this.arrayProposeMultiple.at(index).valid) {
      this.isSubmittedTable = true;
      return;
    }
    const dataFormProposeMultiple = this.formProposeMultiple.getRawValue()?.arrayProposeMultiple;
    const data = dataFormProposeMultiple[index!];
    if (data.implementDate && new Date(data.implementDate).getTime() < new Date().getTime()) {
      this.toastrCustom.error(this.translate.instant('development.commonMessage.E010'));
      return;
    }
    this.onLoading.emit(true);
    const param: EmployeeInfoMultipleEdit = {
      id: item.id,
      contentTitleCode: data?.contentTitleCode,
      contentTitleName:  this.jobDTOSOffer.find((itemJob) => itemJob.jobId === data?.contentTitleCode)?.jobName ,
      contentUnitCode: data.contentUnit?.orgId?.toString(),
      contentUnitName: data.contentUnit?.orgName,
      contentUnitPathId: data.contentUnit?.pathId,
      contentUnitPathName: data.contentUnit?.pathResult,
      contentUnitTreeLevel: data.contentUnit?.orgLevel,
      contentLevel: data.contentLevel,
      proposeComment: {
        id: item?.proposeComment?.id || null,
        strengths: data.strengths,
        weaknesses: data.weaknesses
      },
      proposeAlternative: {
        id: item?.proposeAlternative ? item?.proposeAlternative[0]?.id : null,
        proposeDetailId:item.id,
        alternativeType: data.alternativeType,
        estimatedDate: null,
        replacementEmployeeCode: data?.replacementEmployeeCode,
        replacementEmployeeName: data?.replacementEmployeeName,
        replacementEmployeeTitleCode: data?.replacementEmployeeTitleCode,
        handoverEmployeeCode: data?.handoverEmployeeCode,
        handoverEmployeeName: data?.handoverEmployeeName,
        handoverEmployeeTitleCode: data?.handoverEmployeeTitleCode,
        handoverEmployeeTitleName: data?.handoverEmployeeTitleName,
        replacementEmployeeTitleName: data?.replacementEmployeeTitleName,
        replacementEmployee: data?.replacementEmployee,
        handoverEmployee: data?.handoverEmployee,
      },
      implementDate: data?.implementDate,
      minimumTime: data?.minimumTime,
      maximumTime: data?.maximumTime,
      rotationReasonID: data?.rotationReasonID,
      formRotationCode: this.dataCommon.formGroupMultipleList?.find((itemFormGroupMultiple) => itemFormGroupMultiple.formCode === data?.formName)?.code,
      formRotationType: data?.formName,
    };
    this.proposeMultipleService.editItemProposeMultiple(param, this.screenCode).subscribe((res) => {
      this.onLoading.emit(false);
      this.toastrCustom.success(this.translate.instant('development.commonMessage.success'));
      this.arrayProposeMultiple.clear();
      this.getInfoEmp();


    }, (e) => {
      this.getMessageError(e);
      this.onLoading.emit(false);
    })

  }

  closeProposeMultipleItem(item: EmployeeInfoMultiple, index: number) {
    item.isEdit = false;
    this.createFormGroups([item], true, index);
  }


  displayConditionViolation(conCode?: number) {
    switch (conCode) {
      case 0:
        return { color: 'red', label: this.translate.instant('development.proposed-unit.conditionLD.0') };
      case 1:
        return { color: 'yellow', label: this.translate.instant('development.proposed-unit.conditionLD.1') };
      case 2:
        return { color: 'green', label: this.translate.instant('development.proposed-unit.conditionLD.2') };
      default:
        return null;
    }
  }

  viewSemesterGrade(semesterGradeT?: any, semesterRankT?: any) {
    return semesterGradeT ? `${semesterGradeT} (${semesterRankT})` : ''
  }

  viewValidators(index: number) {
    return this.isSubmittedTable && this.listEmployeeInfo[index]?.isEdit;
  }

  selectAlternativeType(e: number, item: EmployeeInfoMultiple) {
    item.alternativeTypeCheck = e === 0;
  }

  editProposeItem(item: EmployeeInfoMultiple, index: number) {
    this.routerFromElement(item, 'update');

  }

  validApproveButton(){
    return this.formTTQD.get('statementNumber')?.value
      && this.formTTQD.get('submissionDate')?.value
      && this.reasonsReject
      && this.fileListSelectTT.length === 1
  }

  viewRotationReasonID(rotationReasons?: ProposeRotationReasons[]) {
    const reasons: string[] = [];
    if (rotationReasons && rotationReasons.length > 0) {
      rotationReasons?.forEach((item) => {
        reasons.push(<string>this.dataCommon.proposeRotationReasonConfigDTOS?.find((i) => i.id?.toString() === item.rotationReasonID)?.name)
      })
      return reasons.join(' , ');
    }
    return;
  }

  emitDataIssueDecision() {
    if (this.listEmployeeInfo.length > 0) {
      if (!this.formTTQD.valid) {
        this.isSubmitFormTTQD = true;
        return;
      } else {
        this.formTTQD.get('id')?.setValue(this.proposeId);
        const newForm = { ...cleanDataForm(this.formTTQD) };
        newForm.hasDecision = this.hasDecision;
        newForm.externalApprove = !this.displayReport;
        return newForm;
      }
    } else {
      this.toastrCustom.error(this.translate.instant('development.proposed-unit.messages.dontApplyStatement'));
      return;
    }
  }

  expandInfoEmp() {
    this.isSeeMore = !this.isSeeMore;
  }

  expandInfoEmpText() {
    const infoEmpText = !this.isSeeMore
      ? this.translate.instant('development.proposed-unit.button.down')
      : this.translate.instant('development.proposed-unit.button.up')
    return infoEmpText;
  }

  updateCheckedSetWithdraw(item: EmployeeInfoMultiple, checked: boolean): void {
    if (checked) {
      this.quantityMapWithdraw.set(item.id!, item);
    } else {
      this.quantityMapWithdraw.delete(item.id!);
    }
  }

  onItemCheckedWithdraw(item: EmployeeInfoMultiple, checked: boolean): void {
    this.updateCheckedSetWithdraw(item, checked);
    this.refreshCheckedStatusWithdraw();
  }

  onAllCheckedWithdraw(checked: boolean) {
    const list = this.listEmployeeInfoTotal.filter(
      (item: EmployeeInfoMultiple) => Constants.DisableCancelProposeCheckbox.indexOf(item.flowStatusCode) <= -1);
    list.forEach((item: EmployeeInfoMultiple) => this.updateCheckedSetWithdraw(item, checked));
    this.refreshCheckedStatusWithdraw();
  }

  refreshCheckedStatusWithdraw() {
    const list = this.listEmployeeInfoTotal.filter(
      (item: EmployeeInfoMultiple) => Constants.DisableCancelProposeCheckbox.indexOf(item.flowStatusCode) <= -1);
    this.checkedWithdrawAll = list.every((item: EmployeeInfoMultiple) => this.quantityMapWithdraw.has(item.id!));
  }

  checkDisplayButtonWithdrawAll() {
    return this.quantityMapWithdraw.size === 0;
  }

  scrollToValidationSection(section: string, messsage: string){
    this.toastrCustom.warning(this.translate.instant(messsage));
    const el = document.getElementById(section);
    el?.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
  }


  checkButtonDecisionReport() {
    return ((this.flowStatusCode === this.flowCodeConstants.HANDLING6 ||
      this.flowStatusCode === this.flowCodeConstants.CHODUYET7 || this.flowStatusCode === this.flowCodeConstants.KSDongY)
      && this.dataForm?.groupProposeTypeId === Constants.ProposeTypeID.GROUP)
  }

  clearArrayProposeMultiple() {
    this.arrayProposeMultiple.clear();
  }

  viewTitleNumberUpdatedInterview(timesInterview: number) {
    return timesInterview ? this.translate.instant('development.proposed-unit.proposeMultiple.times') + ' ' +  timesInterview.toString() : '';
  }

  //check disable cục tờ trình của PTNL
  checkDisableReportPTNL(){
    return !(this.checkFlowStatusCodeElement && this.dataForm?.createdBy === this.currentUser?.username)
  }

  onChangeFormName(formCode: string, index: number) {
    const itemFormCode = this.dataCommon.formGroupMultipleList?.find((item: FormGroupMultipleList) => item.formCode === formCode);
    if (itemFormCode?.showMaxTime && itemFormCode?.showMinTime) {
      this.arrayProposeMultiple.at(index).get('minimumTime')?.addValidators(Validators.required);
      this.arrayProposeMultiple.at(index).get('maximumTime')?.addValidators(Validators.required);
    } else {
      this.arrayProposeMultiple.at(index).get('minimumTime')?.clearValidators();
      this.arrayProposeMultiple.at(index).get('maximumTime')?.clearValidators();
    }
    this.arrayProposeMultiple.at(index).get('minimumTime')?.updateValueAndValidity();
    this.arrayProposeMultiple.at(index).get('maximumTime')?.updateValueAndValidity();

  }

  //check disable checkbox Hủy đề xuất
  disableCancelProposeCheckbox(flowStatusCode: string) {
    return (this.typeAction === this.screenType.detail
      && this.screenCode === this.screenCodeConst.LDDV)
      || (this.screenCode === this.screenCodeConst.HRDV
        && Constants.DisableCancelProposeCheckbox.indexOf(flowStatusCode) > -1);
  }

  //check cờ của Thành phần đã được rút đề xuất
  checkFlagElement() {
    return this.listEmployeeInfoTotal.filter(
      (item: EmployeeInfoMultiple) => !item.cancelProposeRetractFlag).length > 0;
  }

  disabledImplementDate = (dateVal: Date) => {
    return dateVal.valueOf() <= Date.now();
  };

  // lấy log phân trang
  getLog(pageIndex: number){
    this.pageNumber = pageIndex - 1;
    this.isLoading = true;
    if(this.proposeId) {
      const log = this.service.getLogsPage(this.proposeId, this.proposeCategory, this.screenCode, this.pageNumber, this.pageSize)
        .subscribe((log) => {
          this.proposeLogs = log?.data?.content || [];
          this.tableConfig.total = log?.data?.totalElements || 0;
          this.tableConfig.pageIndex = pageIndex;
          this.isLoading = false;
        }, (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        })
      this.subs.push(log);
    }
  }
}
