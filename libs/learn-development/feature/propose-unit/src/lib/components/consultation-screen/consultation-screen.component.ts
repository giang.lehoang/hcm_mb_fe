import { DecimalPipe } from '@angular/common';
import {
  Component,
  EventEmitter,
  Injector,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges, TemplateRef,
  ViewChild
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NavigationExtras } from '@angular/router';
import {
  Constants,
  functionUri,
  maxInt32,
  ScreenType,
  SessionKey,
  userConfig,
  Utils
} from "@hcm-mfe/learn-development/data-access/common";
import {
  ApproverSignerOption,
  CancelPlan,
  DataCommon, DropDownConcurrentModel,
  FileDTOList, InfoConcurrentlyNowModel, InforConcurrentPositionModel,
  InfoTTQD,
  IssueCorrection,
  IssueLevelOption,
  IssueLevelOptionType,
  IssueTransfer,
  ListConsultationBefore,
  ListFileTemplate,
  ListHandleOffer,
  ListInterviewNow,
  ListPlanCheckOption,
  ParamDetail,
  ParamSearchPosition, RequestCancelPropose,
  SendInterviewsModel,
  ViewDetailParent,
  FormTTQD
} from "@hcm-mfe/learn-development/data-access/models";
import { ProposedUnitService } from "@hcm-mfe/learn-development/data-access/services";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import { clearParamSearch } from "@hcm-mfe/shared/common/utils";
import { User } from "@hcm-mfe/system/data-access/models";
import * as FileSaver from 'file-saver';
import * as moment from 'moment';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { forkJoin, of, Subscription } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { CancelIssueComponent } from '../cancel-issue/cancel-issue.component';
import { CancelPlanComponent } from '../cancel-plan/cancel-plan.component';
import { IssueCorrectionComponent } from '../issue-correction/issue-correction.component';
import { ProposeTemplateComponent } from "../propose-template/propose-template.component";
import { StopInterviewComponent } from '../stop-interview/stop-interview.component';
import {MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {NzSafeAny} from "ng-zorro-antd/core/types";
@Component({
  selector: 'app-consultation-screen',
  templateUrl: './consultation-screen.component.html',
  styleUrls: ['./consultation-screen.component.scss'],
  providers: [DecimalPipe],
})
export class ConsultationScreenComponent extends BaseComponent implements OnInit, OnChanges {
  isVisible = false;
  isOKLoading = false;
  checkedConsultation = false;
  consulationForm = this.fb.group({
    consultDate: '',
    consultName: '',
    status: null,
    note: '',
    show: false,
    consultUnit: null,
  });
  modalRef?: NzModalRef;
  @Input() dataTable: any[] = [];
  @Input() proposeDetailId = 0;
  @Input() typeAction = '';
  @Input() proposeCategory = '';
  //baopt
  @Input() dataCommon?: DataCommon;
  @Input() screenCode = '';
  @Input() dataForm: any;

  @Input() proposeId = 0;
  @Input() flowStatusCode = '';
  @Input() statusPublishCode = '';
  formRotationType = '';
  @Output() onLoading = new EventEmitter<boolean>();
  @Output() backScreenGroup: EventEmitter<any> = new EventEmitter<any>();
  @Input() stateUrl?: NavigationExtras;

  @Input() elementPrintReport?: boolean;

  flowCodeConstants = Constants.FlowCodeConstants;
  publishCodeConstants = Constants.PublishCodeConstants;
  keyConstants = Constants.KeyConstants;

  level = Constants.Level;
  screenCodeConst = Constants.ScreenCode;
  decisionNumber = '';
  positionHandleOffer = [];
  isAddingListHandleOffer = false;
  isAddingListConsultationBefore = false;
  listIdeaConsultationBefore: any[] = [];
  infoConsultation = [];
  currentListConsultationBefore = [];
  currentListHandleOffer = '';
  enableListConsultationBefore = false;
  proposeLogs = [];
  queryCategory = '';
  //end: baopt
  countApi = false;
  listStatus: any[] = [];
  listInterviewNow = [];
  listSalary = [];
  listHandleOffer: any[] = [];
  listConsultationAfter = [];
  listConsultationBefore: any[] = [];
  listNotifyDecision: any[] = [];
  viewDetailParent?: ViewDetailParent;
  infoTTQD?: InfoTTQD;
  fileListSelect: NzUploadFile[] = [];
  fileListSelectApprove: NzUploadFile[] = [];
  fileListReportUnit: NzUploadFile[] = [];
  fileDTOList: FileDTOList[] = [];
  fileInterviewResult: NzUploadFile[] = [];
  listPlanCheckOption: ListPlanCheckOption[] = [];
  hasDecision = false;
  isNotifyIncome = false;
  isModeInsurance = false;

  statementNumber = '';
  submissionDate: Date | null = new Date();
  formReason?: FormGroup;
  isCheckboxNotify1 = false;
  isCheckboxNotify2 = false;
  hidenButon = false;
  isCheckboxNotify3 = false;
  onOpenEdit = false;
  onOpenHandleFormEdit = false;
  statementNumberDV = '';
  submissionDateDV: Date | null = new Date();
  onOpenEditItem = false;
  issueLevelCodeBefore = '';
  approvalCodeBefore = '';
  signerCodeBefore = '';

  // List propose category
  listProposeCategory = Constants.ListProposeCategory;
  listFileNotifyDecision1: NzUploadFile[] = [];
  listFileNotifyDecision2: NzUploadFile[] = [];
  listFileNotifyDecision3: NzUploadFile[] = [];
  listPromulgate = Constants.ListPromulgate;
  proposeCategoryConst = Constants.ProposeCategory;
  subs: Subscription[] = [];
  decisionList = [];
  planCheck: number = 0;
  dataOptionPlan: boolean = false;
  listIssueLevelOption: IssueLevelOption | undefined;
  listIssueLevelOptionItem: IssueLevelOptionType[] = [];
  planCheckFirst = false;
  noteKSPTNL = '';
  submitHandle = false;

  listSigner: ApproverSignerOption[] = [];
  listApprover: ApproverSignerOption[] = [];
  listTypeMaximumTime = Constants.TypeMaximumTime;
  listSendInterviews: SendInterviewsModel[] = [];

  paramDetail: ParamDetail = {
    ghiChuGiaiTrinhThem: null,
    ghiChuChuyenBanHanh: null,
    ngayKyHieuLuc: null,
    lyDohuy: null,
    chiTietLydoHuy: null,
    ngayHieuLucDaChuyen: null,
    ngayHieuLucMoi: null,
    lyDoHuyPa: null,
    commonEffectiveDateCB: false,
    issueLater: false,
    effectiveDate: null,
    effectiveDateTime: null,
    endDate1: null,
    endDate2: null,
    minimumTime: null,
    maximumTime: null,
    typeMaximumTime: null,
  };

  iconStatus = Constants.IconStatus.DOWN;
  isExpand = false;
  functionCode: string;
  rolesOfCurrentUser: string[] = [];
  currentUser: User | undefined;
  reportTCNSFile: FileDTOList = {};

  formReportTCNS = new FormGroup({
    statementNumber: new FormControl(null),
    submissionDate: new FormControl(null),
  })
  infoConcurrently: InforConcurrentPositionModel[] = [];
  infoConcurrentlyNow: InfoConcurrentlyNowModel[] = [];
  dropDownListConcurrently: DropDownConcurrentModel[] = [];
  dropDownListSpecializeList: DropDownConcurrentModel[] = [];
  showConcurrently = false;
  showConcurrentlyNow = false;
  constantsCodeFormGroup = Constants.FormGroupList;
  constantsCodeConcurrent = Constants.PositionType;
  constantsCodeConcurrentName = Constants.PositionTypeName;
  formGroupPropose = 0;
  //log
  heightTable = { x: '50vw', y: '16em'};
  tableConfig!: MBTableConfig;
  pagination = new Pagination();
  @ViewChild('dateLog', {static: true}) dateLog!: TemplateRef<NzSafeAny>;
  pageNumber = 0;
  pageSize =  userConfig.pageSize;
  constantsTimeType = Constants.TimeTypeCode

  constructor(injector: Injector, private readonly serviceProposed: ProposedUnitService, private readonly decimalPipe: DecimalPipe) {
    super(injector);
    this.addFormReason();
    this.functionCode = this.route.snapshot?.data['code'];
  }

  ngOnChanges(changes: SimpleChanges): void {
    const proposeDetails = this.dataForm?.proposeDetails[0];
    this.paramDetail.effectiveDate = proposeDetails?.issueEffectiveStartDate || '';
    this.paramDetail.ngayHieuLucDaChuyen = proposeDetails?.issueEffectiveStartDate || '';
    this.decisionNumber = proposeDetails?.decisionNumber || '';
    this.paramDetail.ngayKyHieuLuc = proposeDetails?.signedDate || '';
    this.paramDetail.endDate1 = proposeDetails?.issueEffectiveEndDate1 || '';
    //sau khi chuyển ban hành xong mới check theo 2 trường này
    if (this.checkHandleCard() && !this.checkTypeIssueTransfer2()) {
      this.paramDetail.minimumTime = proposeDetails?.minimumTimePublish;
      this.paramDetail.maximumTime = proposeDetails?.maximumTimePublish;
    }
    this.paramDetail.issueLater = proposeDetails?.issuedLater;
    this.formGroupPropose = +proposeDetails?.formGroup;
    this.hidenButon = proposeDetails?.isDecision;
    this.isCheckboxNotify1 = proposeDetails?.isDecision;
    this.isCheckboxNotify2 = proposeDetails?.isIncome;
    this.isCheckboxNotify3 = proposeDetails?.isInsurance;
    const fileTCNS = proposeDetails?.fileDTOList?.find(
      (item: FileDTOList) => item?.type === Constants.TypeStatementFile.HR1);
    if (fileTCNS) {
      this.reportTCNSFile.docId = fileTCNS?.docId;
      this.reportTCNSFile.fileName = fileTCNS?.fileName;
    }
    const statementDTO = proposeDetails?.proposeStatement || [];
    if (statementDTO?.length > 0) {
      const tcnsObj = statementDTO?.find((item: FormTTQD) => item?.typeStatement === Constants.TypeStatement.TCNS);
      if (tcnsObj) {
        this.formReportTCNS.patchValue({
          statementNumber: tcnsObj?.statementNumber,
          submissionDate: tcnsObj?.submissionDate,
        });
      }
    }
  }

  checkCommonEffectiveDate() {
    if (this.paramDetail.commonEffectiveDateCB) {
      this.paramDetail.effectiveDate = null;
    }
  }

  //check ngày kết thúc chuyển ban hành
  checkEndDateValue() {
    let rangeTime = 0;
    if (this.paramDetail.effectiveDate) {
      if (this.paramDetail.maximumTime) {
        if (this.paramDetail.typeMaximumTime === Constants.TypeMaximumValue.MONTH) {
          rangeTime = +this.paramDetail.maximumTime;
        } else if (this.paramDetail.typeMaximumTime === Constants.TypeMaximumValue.YEAR) {
          rangeTime = +(this.paramDetail.maximumTime * 12);
        }
      } else if (this.paramDetail.minimumTime && !this.paramDetail.maximumTime) {
        rangeTime = +this.paramDetail.minimumTime;
      }
    } else {
      rangeTime = 0;
      this.paramDetail.endDate1 = null;
    }
      //tính ngày kết thúc
      if (this.paramDetail.effectiveDate && rangeTime > 0) {
        const oldDate = new Date(this.paramDetail.effectiveDate);
        const date1 = oldDate.getDate();
        const month1 = oldDate.getMonth() + 1;
        let year1 = oldDate.getFullYear();
        let newMonth = 0;
        newMonth = month1 + rangeTime;
        if (rangeTime <= 12) {
          if (newMonth > 12) {
            year1 = year1 + 1;
            newMonth = newMonth - 12;
          }
        } else {
          const yearBonus = Math.floor(rangeTime / 12);
          const monthBalance = rangeTime - yearBonus * 12;
          newMonth = month1 + monthBalance;
          if (rangeTime > 12) {
            year1 = year1 + yearBonus;
            if (newMonth > 12) {
              newMonth = newMonth - 12;
              year1 = year1 + 1;
            }
          } else {
            year1 = year1 + 1;
          }
        }
        const newDate = new Date();
        newDate.setDate(date1);
        newDate.setMonth(newMonth - 1);
        newDate.setFullYear(year1);
        this.paramDetail.endDate1 = newDate;
      }
  }

  disabledDate = (dateVal: Date) => {
    return dateVal.valueOf() >= Date.now();
  };

  addFormReason() {
    this.formReason = this.fb.group({
      unitOpinion: [''],
      workProcess: ['', Validators.required],
      businessNumber: [''],
      strengths: [''],
      weaknesses: [''],
      summarizeLdComment: ['', Validators.required],
      isDecision: [''],
      isIncome: [''],
      isInsurance: [''],
      ideaRegion: ['']
    });
  }

  checkIdParams() {
    if (this.proposeCategory === 'GROUP') {
      return this.proposeId;
    } else if (this.proposeCategory === 'ORIGINAL') {
      return this.proposeId;
    } else if (this.proposeCategory === 'ELEMENT') {
      return this.proposeDetailId;
    }
    return -1;
  }

  checkTypePTNL() {
    if (this.proposeCategory === 'GROUP') {
      return 0;
    } else if (this.proposeCategory === 'ORIGINAL') {
      return 1;
    } else if (this.proposeCategory === 'ELEMENT') {
      return 2;
    }
    return -1;
  }

  //giải trình thêm
  moreExplanation() {
    const id = <number>this.checkIdParams();
    if(!this.paramDetail.ghiChuGiaiTrinhThem) {
      this.toastrCustom.error(this.translate.instant('development.commonMessage.validateMoreExplanation'));
      return;
    }
    const body = {
      message: this.paramDetail.ghiChuGiaiTrinhThem,
    };
    const params = {
      groupProposeType: this.checkTypePTNL(),
    };
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.proposed-unit.messages.explanationConfirm'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.onLoading.emit(true);
        this.serviceProposed.moreExplanation(id, body, params).subscribe(
          () => {
            this.toastrCustom.success(this.translate.instant('development.proposed-unit.messages.explanationSuccess'));
            if (this.stateUrl?.state && this.stateUrl?.state['backUrl']) {
              this.backScreenGroup.emit();
            } else {
              this.router.navigateByUrl(functionUri.propose_list);
            }
            this.onLoading.emit(false);
          },
          (e) => {
            this.getMessageError(e);
            this.onLoading.emit(false);
          }
        );
      },
    });
  }

  //ban hành sau
  issueLater() {
    const id = <number>this.checkIdParams();
    this.isLoading = true;
    this.serviceProposed.issueLater(id).subscribe(
      () => {
        this.toastrCustom.success(this.translate.instant('development.proposed-unit.messages.issueLaterSuccess'));
        this.paramDetail.issueLater = true;
        this.paramDetail.commonEffectiveDateCB = false;
        this.paramDetail.effectiveDate = null;
        this.paramDetail.endDate1 = null;
        this.paramDetail.endDate2 = null;
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

  //check thời gian tối thiểu và tối đa trong cục chuyển ban hành
  checkRotationIssueTransfer() {
    if (this.statusPublishCode === Constants.StatusPublishCode.PENDINGISSUE5A1) {
      return this.dataForm?.proposeDetails[0]?.minimumTime || this.dataForm?.proposeDetails[0]?.maximumTime;
    } else {
      const optionSelected = this.listHandleOffer?.find((item: ListHandleOffer) => item?.selected);
      if (optionSelected) {
        return optionSelected?.minimumTime || optionSelected?.maximumTime;
      }
    }
  }

  //check hiển thị 3 nút ghi nhận
  checkRecordButton() {
    return (
      this.flowStatusCode === '9' &&
      (this.screenCode === this.screenCodeConst.PTNL ||
        (this.screenCode === this.screenCodeConst.KSPTNL && this.dataForm?.proposeDetails[0]?.groupByKSPTNL))
    );
  }


  //chuyển ban hành
  issueTransfer() {
    if (this.paramDetail.minimumTime && this.paramDetail.maximumTime) {
      //validate thời gian tối thiểu và thời gian tối đa
      if ((this.paramDetail.maximumTime <= +this.paramDetail.minimumTime
        && this.paramDetail.typeMaximumTime === Constants.TypeMaximumValue.MONTH) || 
        (this.paramDetail.maximumTime * 12 <= +this.paramDetail.minimumTime
        && this.paramDetail.typeMaximumTime === Constants.TypeMaximumValue.YEAR)) {
          this.toastrCustom.warning(this.translate.instant('development.promulgate.message.validateMinMaxTime'));
          return;
      }
    }
    //validate ngày hiệu lực chung riêng
    if (!this.isCheckboxNotify1) {
      if (!this.paramDetail.commonEffectiveDateCB && !this.paramDetail.effectiveDate) {
        this.toastrCustom.error(this.translate.instant('development.promulgate.message.effectiveDateRequired'));
        return;
      }
    } else {
      if (!this.paramDetail.effectiveDate) {
        this.toastrCustom.error(this.translate.instant('development.promulgate.message.effectiveDateRequired2'));
        return;
      }
    }
    //validate ghi chú chuyển ban hành
    if (Constants.CancelPlanPublishStatus.indexOf(this.statusPublishCode) > -1
      && !this.paramDetail.ghiChuChuyenBanHanh) {
      this.toastrCustom.error(this.translate.instant('development.promulgate.message.validateNoteIssue'));
      return;
    }
    this.issueTransferModal();
  }

  issueTransferModal() {
    const params: IssueTransfer = {
      id: <number>this.checkIdParams(),
      proposeCategory: this.proposeCategory,
      screenCode: this.screenCode,
    };
    const body = {
      effectiveDate: this.paramDetail.effectiveDate || '',
      issueEffectiveEndDate1: this.paramDetail.endDate1,
      issueEffectivePeriod: this.paramDetail.effectiveDateTime,
      sameDecision: this.paramDetail.commonEffectiveDateCB,
      commentsIssueTransfer: this.paramDetail.ghiChuChuyenBanHanh,
      minimumTime: this.paramDetail.minimumTime,
      maximumTime: this.paramDetail.maximumTime,
      timeType: this.paramDetail.typeMaximumTime,
    };
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.proposed-unit.messages.changeIssue'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.onLoading.emit(true);
        this.paramDetail.issueLater = false;
        const checkDate = moment(new Date()).isAfter(new Date(body.effectiveDate));
        if (checkDate && !body?.effectiveDate) {
          this.toastrCustom.warning(this.translate.instant('development.commonMessage.effectiveDateValidateNow'));
        }

        this.serviceProposed.issueTransfer(body, params).subscribe(
          () => {
            this.toastrCustom.success(this.translate.instant('development.proposed-unit.messages.changeIssueSuccess'));
            if (this.stateUrl?.state && this.stateUrl?.state['backUrl']) {
              this.backScreenGroup.emit();
            } else {
              this.router.navigateByUrl(functionUri.propose_list);
            }
            this.onLoading.emit(false);
          },
          (e) => {
            this.getMessageError(e);
            this.onLoading.emit(false);
          }
        );
      },
    });
  }

  //hiệu chỉnh ban hành
  issueCorrection() {
    const params: IssueTransfer = {
      id: <number>this.checkIdParams(),
      proposeCategory: this.proposeCategory,
      screenCode: this.screenCode,
    };
    this.modalRef = this.modal.create({
      nzTitle: this.translate.instant('development.promulgate.section.issueCorrection'),
      nzWidth: '40vw',
      nzContent: IssueCorrectionComponent,
      nzComponentParams: {
        proposeCode: this.dataForm?.proposeDetails[0]?.proposeCode,
        fullName: this.dataForm?.proposeDetails[0]?.employee?.fullName,
        proposeId: this.proposeId,
        proposeDetailId: this.proposeDetailId,
        formRotationType: this.formRotationType,
        commonDecision: this.isCheckboxNotify1,
        oldEffectiveDate: this.paramDetail.effectiveDate,
        oldEndDate: this.paramDetail.endDate1,
        oldMinTime: this.paramDetail.minimumTime,
        oldMaxTime: this.paramDetail.maximumTime,
      },
      nzFooter: null,
    });
    this.modalRef.componentInstance.closeEvent.subscribe((data:boolean) => {
      if (data) {
        this.onLoading.emit(true);
        this.modalRef?.componentInstance.issueCorrectionBody.subscribe((body: IssueCorrection) => {
          const sub = this.serviceProposed.issueCorrection(body, params).subscribe(
            () => {
              this.toastrCustom.success(
                this.translate.instant('development.proposed-unit.messages.issueCorrectionSuccess')
              );
              if (this.stateUrl?.state && this.stateUrl?.state['backUrl']) {
                this.backScreenGroup.emit();
              } else {
                this.router.navigateByUrl(functionUri.propose_list);
              }
              this.onLoading.emit(false);
            },
            (e) => {
              this.getMessageError(e);
              this.onLoading.emit(false);
            }
          );
          this.subs.push(sub);
          this.modalRef?.destroy();
        })
      } else {
        this.modalRef?.destroy();
      }
    });
  }

  //yêu cầu hủy ban hành
  requestCancelation() {
    const params: IssueTransfer = {
      id: <number>this.checkIdParams(),
      proposeCategory: this.proposeCategory,
      screenCode: this.screenCode,
    };
    this.modalRef = this.modal.create({
      nzTitle: this.translate.instant('development.promulgate.section.requestCancelIssue'),
      nzWidth: '40vw',
      nzContent: CancelIssueComponent,
      nzComponentParams: {
        proposeCode: this.dataForm?.proposeDetails[0]?.proposeCode,
        fullName: this.dataForm?.proposeDetails[0]?.employee?.fullName,
      },
      nzFooter: null,
    });
    this.modalRef.componentInstance.closeEvent.subscribe((data:boolean) => {
      if (data) {
        this.onLoading.emit(true);
        this.modalRef?.componentInstance.cancelIssueBody.subscribe((body: RequestCancelPropose) => {
          const sub = this.serviceProposed.requestCancelation(params, body).subscribe(
            () => {
              this.onLoading.emit(false);
              this.toastrCustom.success(this.translate.instant('development.cancelIssue.cancelIssueSuccess'));
              if (this.stateUrl?.state && this.stateUrl?.state['backUrl']) {
                this.backScreenGroup.emit();
              } else {
                this.router.navigateByUrl(functionUri.propose_list);
              }
            },
            (e) => {
              this.getMessageError(e);
              this.onLoading.emit(false);
            }
          );
          this.subs.push(sub);
        })
      }
      this.modalRef?.destroy();
    });
    
  }

  checkHandleButton() {
    const list = ['5B', '5C'];
    return list.indexOf(this.flowStatusCode) > -1;
  }

  checkDeleButton() {
    const list = ['5C', '6'];
    return list.indexOf(this.flowStatusCode) > -1;
  }

  checkSaveSendPropose() {
    const list = ['6', '8', '10B1', '10C'];
    return list.indexOf(this.flowStatusCode) > -1;
  }

  checkTypeIssueTransfer() {
    const list = ['12.3A', '12.4'];
    return list.indexOf(this.statusPublishCode) > -1;
  }

  // check chuyển ban hành
  checkTypeIssueTransfer2() {
    return Constants.ChangeIssueStatus.indexOf(this.statusPublishCode) > -1
      && !this.dataForm?.proposeDetails[0]?.publishCode;
  }

  //check hiển thị nút hủy ban hành
  checkTypeCancelIssueCorrection() {
    const validStatus = Constants.CancelIssueStatus.indexOf(this.statusPublishCode) > -1;
    if (!this.isCheckboxNotify1) {
      return validStatus && !this.dataForm?.proposeDetails[0]?.publishCode;
    } else {
      return validStatus;
    }
  }

  //hiệu chỉnh ban hành
  checkIssueCorrectionButton() {
    const validStatus = Constants.IssueCorrectionButton.indexOf(this.statusPublishCode) > -1;
    if (!this.isCheckboxNotify1) {
      return validStatus && !this.dataForm?.proposeDetails[0]?.publishCode;
    } else {
      return validStatus;
    }
  }

  //check hiển thị nút hủy phương án
  checkTypeCancelPlan() {
    const validStatus = Constants.CancelPlanPublishStatus2.indexOf(this.statusPublishCode) > -1;
    if (!this.isCheckboxNotify1) {
      return validStatus && !this.dataForm?.proposeDetails[0]?.publishCode;
    } else {
      return validStatus;
    }
  }

  //check hiển thị card kết quả phê duyệt
  checkHandleCard() {
    switch (this.formGroupPropose) {
      case Constants.FormGroupList.PV:
        return this.flowStatusCode === this.flowCodeConstants.KHONGDONGY10B;
      default:
        return this.typeAction === this.screenType.detail
        && this.screenCode === this.screenCodeConst.PTNL
        && (this.flowStatusCode === this.flowCodeConstants.KHONGDONGY10B
          || (this.proposeCategory === Constants.ProposeCategory.ELEMENT &&
            (this.checkTypeIssueTransfer()
            || (this.splitStatusPublish() && this.checkChangeIssueSection())
            || this.checkTypeCancelIssueCorrection()
            || this.checkTypeCancelPlan()
            ) ));
    }
  }

  //hủy phương án
  cancelPlanAction() {
    this.modalRef = this.modal.create({
      nzTitle: this.translate.instant('development.list-plan.button.cancel'),
      nzWidth: '40vw',
      nzContent: CancelPlanComponent,
      nzComponentParams: {
        proposeCode: this.dataForm?.proposeDetails[0]?.proposeCode,
        fullName: this.dataForm?.proposeDetails[0]?.employee?.fullName,
        id: this.proposeDetailId,
        proposeCategory: this.proposeCategory,
      },
      nzFooter: null,
    });
    this.modalRef.componentInstance.closeEvent.subscribe((data:boolean) => {
      if (data) {
        this.onLoading.emit(true);
        this.modalRef?.componentInstance.cancelPlanBody.subscribe((body: CancelPlan) => {
          const sub = this.serviceProposed.cancelPlan(body, this.screenCode).subscribe(
            () => {
              this.toastrCustom.success(this.translate.instant('development.list-plan.messages.cancelPlanSuccess'));
              this.router.navigateByUrl('development/list-plan');
              this.onLoading.emit(false);
            },
            (e) => {
              this.getMessageError(e);
              this.onLoading.emit(false);
            }
          );
          this.subs.push(sub);
          this.modalRef?.destroy();
        })
      } else {
        this.modalRef?.destroy();
      }
    });
  }

  splitStatusPublish() {
    const numberSatus = this.statusPublishCode?.replace(/\D/g, '');
    return +numberSatus >= 10;
  }

  checkChangeIssueSection() {
    const list = ['10B', '10B1', '10C'];
    return list.indexOf(this.statusPublishCode) <= -1;
  }

  exportReport(action: number) {
    const params = {
      groupProposeType: this.proposeCategory === Constants.ProposeCategory.GROUP ? 0 : 2,
      action: action,
      screenCode: this.screenCode,
    };
    this.onLoading.emit(true);
    let listFile = [];
    this.serviceProposed.exportReportDecision(+this.proposeDetailId, params).subscribe(
      (res) => {
        listFile = res?.data;
        if (!listFile) {
          this.toastrCustom.error('development.list-plan.messages.noFile');
          this.onLoading.emit(false);
        } else {
          listFile.forEach((item: string) => {
            this.serviceProposed.callBaseUrl(item).subscribe(
              (data) => {
                const contentDisposition = data?.headers.get('content-disposition');
                const filename = contentDisposition?.split(';')[1]?.split('filename')[1]?.split('=')[1]?.trim();
                const blob = new Blob([data.body], {
                  type: 'application/octet-stream',
                });
                FileSaver.saveAs(blob, filename);
                this.onLoading.emit(false);
              },
              (e) => {
                this.getMessageError(e);
                this.onLoading.emit(false);
              }
            );
          });
        }
      },
      (e) => {
        this.getMessageError(e);
        this.onLoading.emit(false);
      }
    );
  }

  checkProposeCode() {
    if (this.proposeCategory === Constants.ProposeCategory.ELEMENT) {
      return 2;
    } else if (this.proposeCategory === Constants.ProposeCategory.ORIGINAL) {
      return 1;
    } else if (this.proposeCategory === Constants.ProposeCategory.GROUP) {
      return 0;
    }
    return null;
  }

  dowloadStatement(action: number) {
    const params = {
      groupProposeType: this.checkProposeCode(),
      action: action,
      screenCode: this.screenCode,
    };
    this.onLoading.emit(true);
    let listFile = [];
    if (this.proposeDetailId) {
      const sub = this.serviceProposed.exportReportDecision(this.proposeDetailId, params).subscribe(
        (res) => {
          listFile = res?.data;
          if (!listFile || listFile?.length === 0) {
            this.toastrCustom.error('development.list-plan.messages.noFile');
            this.onLoading.emit(false);
          } else {
            this.showModalTemplate(listFile as ListFileTemplate[])
          }
        },
        (e) => {
          this.getMessageError(e);
          this.onLoading.emit(false);
        }
      );
      this.subs.push(sub);
    }
  }

  showModalTemplate(listFileTemplate: ListFileTemplate[]) {
    this.isLoading = false;
    const listDecision = listFileTemplate?.filter((item) => item?.typeCode === Constants.TemplateType.QD && item?.subTypeCode !== Constants.TemplateType.PLDCDD);
    const listNotDecision = listFileTemplate?.filter((item) => item?.typeCode !== Constants.TemplateType.QD || (item?.typeCode === Constants.TemplateType.QD && item?.subTypeCode === Constants.TemplateType.PLDCDD));
    if (listDecision?.length > 1) {
      this.modalRef = this.modal.create({
        nzTitle: this.translate.instant('development.list-plan.messages.pickFile'),
        nzWidth: 800,
        nzContent: ProposeTemplateComponent,
        nzComponentParams: {
          listDecision: listDecision
        },
        nzFooter: null,
      });
      this.modalRef.componentInstance.eventIdFile.subscribe((idFile: number) => {

        this.modalRef?.destroy();
        this.isLoading = true;
        const itemFileDecision = listDecision.find((item) => item?.id === +idFile)
        const listNewFileDecision =
          [...listNotDecision,
            itemFileDecision];
        this.dowloadAllFile(listNewFileDecision as ListFileTemplate[]);
        this.modalRef?.componentInstance.closeEvent.subscribe((data:boolean) => {
          if (!data) {
            this.modalRef?.destroy();
          }
        })
      })
    } else {
      this.dowloadAllFile(listFileTemplate);
    }
  }

  dowloadAllFile (listNewFileDecision: ListFileTemplate[]) {
    const listNewTemplateCode = listNewFileDecision.map((item) => {
      return item.templateCode
    });
    this.serviceProposed.updateTemplateUsed(listNewTemplateCode).subscribe();
    listNewFileDecision?.forEach((item ) => {
      this.serviceProposed.callBaseUrl(item?.linkTemplate!).subscribe(
        (data) => {
          const contentDisposition = data?.headers.get('content-disposition');
          const filename = contentDisposition?.split(';')[1]?.split('filename')[1]?.split('=')[1]?.trim();
          const blob = new Blob([data.body], {
            type: 'application/octet-stream',
          });
          FileSaver.saveAs(blob, filename);
          this.onLoading.emit(false);
        },
        (e) => {
          this.getMessageError(e);
          this.onLoading.emit(false);
        }
      );
    });
  }

  addData() {
    const data = this.consulationForm.getRawValue();
    data.consultUnitName = data.consultUnit?.orgName;
    data.consultUnit = data.consultUnit?.orgId;
    this.serviceProposed.saveConsultation(+this.proposeDetailId, data).subscribe(
      (res) => {
        data.status = this.listStatus.find((item: any) => item.code === data.status)?.name;
        this.dataTable.push(data);
        this.isOKLoading = true;
        this.isVisible = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

  handleOK(_event: any) {
    this.isOKLoading = true;
  }

  showModal() {
    this.isVisible = true;
    this.consulationForm.reset();
  }

  hideModal() {
    this.isVisible = false;
  }

  checkConsultationStatus(event: boolean) {
    this.checkedConsultation = event;
  }

  issueForm = this.fb.group({
    effectiveDate: null,
    signedDate: null,
    signerFullName: null,
    comments: null,
  });

  ngOnInit() {
    this.initTable();
    if (localStorage.getItem(SessionKey.CURRENCY_USER)) {
      this.currentUser = JSON.parse(localStorage.getItem(SessionKey.CURRENCY_USER)!);
    }
    this.onLoading.emit(true);
    this.queryCategory = this.route.snapshot.queryParams['proposeCategory'];

    const idLog = this.proposeCategory === Constants.ProposeCategory.ORIGINAL ? this.proposeId : +this.proposeDetailId;
    const sendInterviewBody = {
      detailId: this.proposeDetailId
    }
    forkJoin([
      this.serviceProposed.listInterview(+this.proposeDetailId).pipe(catchError(() => of(undefined))),
      this.serviceProposed.listSalary(+this.proposeDetailId).pipe(catchError(() => of(undefined))),
      this.serviceProposed.listHandleOffer(+this.proposeDetailId).pipe(catchError(() => of(undefined))),
      this.serviceProposed.displayConsultation(+this.proposeDetailId).pipe(catchError(() => of(undefined))),
      this.serviceProposed.listConsultation(+this.proposeDetailId).pipe(catchError(() => of(undefined))),
      this.serviceProposed
        .viewDetailFile(+this.proposeDetailId, this.proposeCategory)
        .pipe(catchError(() => of(undefined))),
      this.serviceProposed.viewParent(this.proposeDetailId).pipe(catchError(() => of(undefined))),
      this.serviceProposed.getInfoTTQD(this.proposeDetailId).pipe(catchError(() => of(undefined))),
      this.serviceProposed.getInfoConsult(this.proposeDetailId).pipe(catchError(() => of(undefined))),
      this.serviceProposed.getPositionGroup().pipe(catchError(() => of(undefined))),
      this.serviceProposed.getRolesByUsername(),
      this.serviceProposed.getIssueLevelOption(this.proposeDetailId),
      this.serviceProposed.sendInterview(sendInterviewBody),
      this.serviceProposed.getInfoConcurrently(this.proposeDetailId),
      this.serviceProposed.getInfoConcurrentlyNow(this.proposeDetailId),
      this.serviceProposed.dropdownConcurrently(),
      this.serviceProposed.getLogsPage(idLog, this.proposeCategory, this.screenCode, this.pageNumber, this.pageSize)
        .pipe(catchError(() => of(undefined))),
    ]).subscribe(
      ([
        interviewIsPresentNow,
        listSalary,
        listHandleOffer,
        listConsultationAfter,
        listConsultationBefore,
        listNotifyDecision,
        viewDetailParent,
        infoTTQD,
        infoConsultation,
        listPositionGroup,
        roles,
        issueLevelOption,
        sendInterview,
        infoConcurrently,
        infoConcurrentlyNow,
        dropDownListConcurrently,
        logs
      ]) => {
        this.infoConcurrently = infoConcurrently?.data?.concurrentOptionDTOList;
        this.infoConcurrentlyNow = infoConcurrentlyNow?.data?.alternativeOptionDTOList;
        this.showConcurrently = infoConcurrently?.data?.hasShow;
        this.showConcurrentlyNow = infoConcurrentlyNow?.data?.hasShow;
        this.dropDownListConcurrently = dropDownListConcurrently?.data?.concurrentList
        this.dropDownListSpecializeList = dropDownListConcurrently?.data?.specializeList
        this.positionHandleOffer = listPositionGroup?.data?.content || [];
        this.listInterviewNow = interviewIsPresentNow?.data?.content;
        this.listInterviewNow?.forEach((item: ListInterviewNow) => {
          if (item.numberUpdated && item.numberUpdated !== 0) {
            item.numberUpdated = this.translate.instant('development.commonMessage.time') + item?.numberUpdated;
          }
        });
        this.fileInterviewResult = this.listInterviewNow?.map((item: ListInterviewNow) => {
          return {
            uid: item.docId ? item.docId : '',
            name: item.fileName ? item.fileName : '',
            status: 'done',
          }
        })
        this.proposeLogs = logs?.data?.content || [];
        this.tableConfig.total = logs?.data?.totalElements || 0;
        this.tableConfig.pageIndex = 1;
        this.listSalary = listSalary?.data;
        this.listConsultationAfter = listConsultationAfter?.data?.consultationListDTOS?.content;
        this.listConsultationBefore = listConsultationBefore?.data?.consultationListDTOS?.content || [];
        this.listHandleOffer = listHandleOffer?.data?.content;
        this.listHandleOffer?.forEach((item: ListHandleOffer) => {
          if (this.dataCommon?.formDisplaySearchDTOS && this.dataCommon.formDisplaySearchDTOS.length > 0) {
            const formDisplayObject = this.dataCommon.formDisplaySearchDTOS.find(
              (i) => i.code === item.formRotationCode);
            item.formRotationName = formDisplayObject?.formName ? formDisplayObject.formName : '';
          }
        })
        this.getDataListPlan(this.listHandleOffer);
        if (this.listHandleOffer.length === 1) {
          this.planCheck = this.listHandleOffer[0]?.id;
          if (this.typeAction === this.screenType.update) {
            this.callApiOptionPlan();
          }
        } else if (this.listHandleOffer?.length > 1) {
          this.listHandleOffer?.forEach((item: ListHandleOffer) => {
            if (item?.selected) {
              this.planCheck = item.id;
            }
          })
        }
        this.checkRotationIssueTransfer();
        const effectiveDate = this.dataForm?.proposeCorrection?.effectiveDate || null;
        const signerFullName = this.dataForm?.proposeDetails[0]?.employee?.signerFullName || null;
        this.issueForm.patchValue({
          effectiveDate: effectiveDate,
          signerFullName: signerFullName,
        });
        this.issueForm.disable();
        this.rolesOfCurrentUser = roles;

        let listNotifyDecisionAPI: any[] =
          listNotifyDecision?.data?.length > 0
            ? listNotifyDecision?.data?.map((item: any) => {
                return {
                  ...item,
                  // textNotifyDecision: this.listTextNotifyDecision?.find((i) => i.id === item.documentType).name,
                  valueFile:
                    [
                      {
                        id: item.id,
                        uid: item.decisionSignId,
                        name: item.decisionSignName,
                        status: 'done',
                      },
                    ] || [],
                };
              })
            : [];
        // Quyết định & thông báo
        for (let i = 0; i < 3; i++) {
          if (listNotifyDecisionAPI[i]?.decisionSignId) {
            this.listNotifyDecision.push(listNotifyDecisionAPI[i]);
          } else {
            let item = {
              valueFile: [],
            };
            this.listNotifyDecision.push(item);
          }
          switch (i) {
            case 0:
              this.listFileNotifyDecision1 = this.listNotifyDecision[i].valueFile || [];
              break;
            case 1:
              this.listFileNotifyDecision2 = this.listNotifyDecision[i].valueFile || [];
              break;
            case 2:
              this.listFileNotifyDecision3 = this.listNotifyDecision[i].valueFile || [];
              break;
            default:
          }
        }

        this.viewDetailParent = viewDetailParent?.data;
        this.infoTTQD = infoTTQD?.data;
        this.statementNumber = this.infoTTQD?.proposeStatementDTO?.statementNumber!;
        this.submissionDate = this.infoTTQD?.proposeStatementDTO?.submissionDate
          ? new Date(this.infoTTQD?.proposeStatementDTO?.submissionDate)
          : null;
        this.fileDTOList = this.dataForm?.fileDTOList;
        this.fileDTOList?.forEach((item) => {
          this.fileListReportUnit.push({
            uid: item.docId ? item.docId : '',
            name: item.fileName ? item.fileName : '',
          })
        });
        const TTHRDV: boolean = this.dataForm?.proposeStatementDTO?.typeStatement === Constants.TypeStatement.HRDV;
        this.statementNumberDV = TTHRDV ? this.dataForm?.proposeStatementDTO?.statementNumber : '';
        this.submissionDateDV = TTHRDV ? this.dataForm?.proposeStatementDTO?.submissionDate : '';
        this.infoTTQD?.proposeProcessFileDTOlist?.forEach((item) => {
          this.fileListSelect.push({
            uid: item.docId,
            name: item.fileName,
            status: 'removed',
            id: item.id,
          });
        });
        this.fileListSelect = [...this.fileListSelect];

        this.formReason?.patchValue(infoTTQD?.data);

        // // get Data proposeLogs
        //enableListConsultationBefore = true when listConsultation has 1 filed hasConsultation = true
        this.checkEnableListConsultation();
        this.infoConsultation = infoConsultation?.data?.listOptionCode;
        //map status from object to array
        const statusMap = infoConsultation?.data?.statusMap;
        Object.keys(statusMap).forEach((item) => {
          this.listIdeaConsultationBefore.push({ code: item, name: statusMap[item] });
        });
        this.listIssueLevelOption = issueLevelOption.data;
        if (this.typeAction === this.screenType.detail) {
          this.formReason?.disable();
        }
        
        this.listSendInterviews = sendInterview?.data || [];
        this.checkSendInterviewButton();
        const optionSelected = this.listHandleOffer?.find((item: ListHandleOffer) => item?.selected);
        const proposeDetails = this.dataForm?.proposeDetails[0];
        if (!proposeDetails?.minimumTimePublish) {
          if (this.statusPublishCode === Constants.StatusPublishCode.PENDINGISSUE5A1) {
            this.paramDetail.minimumTime = this.dataForm?.proposeDetails[0]?.minimumTime;
          } else {
            if (optionSelected) {
              this.paramDetail.minimumTime =  optionSelected?.minimumTime;
            }
          }
        }
        if (!proposeDetails?.maximumTimePublish) {
          if (this.statusPublishCode === Constants.StatusPublishCode.PENDINGISSUE5A1) {
            this.paramDetail.maximumTime = this.dataForm?.proposeDetails[0]?.maximumTime;
          } else {
            if (optionSelected) {
              this.paramDetail.maximumTime =  optionSelected?.maximumTime;
            }
          }
        }
        this.paramDetail.typeMaximumTime = this.statusPublishCode === Constants.StatusPublishCode.PENDINGISSUE5A1
          ? Constants.TypeMaximumTime[0].type
          : optionSelected?.timeType;
        this.onLoading.emit(false);
      },
      (e) => {
        this.onLoading.emit(false);
      }
    );
  }

  checkEnableListConsultation() {
    const itemConsultation: any = this.listHandleOffer?.find((item: any) => item.hasConsultation);
    if (itemConsultation && Object.keys(itemConsultation).length > 0 && itemConsultation.hasConsultation) {
      this.getInfoConsult();
      this.enableListConsultationBefore = true;
      return true;
    } else {
      this.enableListConsultationBefore = false;
    }
    return false;
  }

  delete(id: number) {
    this.isLoading = true;
    this.serviceProposed.deleteConsultation(id).subscribe(
      (res) => {
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }
  // baopt
  addFormItemHandleOffer() {
    const item = {
      optionCode: null,
      formRotationCode: null,
      proposeUnitName: null,
      proposeTitleName: null,
      titleLevelCode: null,
      proposePositionName: null,
      proposeBusinessLine: null,
      note: null,
      desiredMonthlyIncome: null,
      salaryComment: null,
      hasConsultation: false,
      hasInterview: false,
      sentInterviewComment: null,
      checkInterviewResults: null,
      selected: false,
      selectedDate: null,
      unselectedDate: null,
      isEdit: true,
      notSave: true,
    };
    this.listHandleOffer.push(item);
  }

  addItemListHandleOffer() {
    this.onOpenHandleFormEdit = true;
    if (this.isAddingListHandleOffer) {
      return;
    }
    this.isAddingListHandleOffer = true;
    this.addFormItemHandleOffer();
  }

  changeValueUnit(event: any) {
    if (!event) {
      this.positionHandleOffer = [];
      return;
    }
    this.isLoading = true;
    this.serviceProposed.getPosByOrgId(event.orgId).subscribe(
      (res) => {
        this.isLoading = false;
        this.positionHandleOffer = res.data || [];
      },
      (e) => {
        this.isLoading = false;
        this.getMessageError(e);
      }
    );
  }

  checkFormHandle(item: ListHandleOffer) {
    if (!item) {
      this.toastrCustom.error(this.translate.instant('development.commonMessage.formHandleValidate'));
    }
  }

  checkUnitHandle(item: ListHandleOffer) {
    if (!item) {
      this.toastrCustom.error(this.translate.instant('development.commonMessage.unitHandleValidate'));
    }
  }

  checkTitleHandle(item: ListHandleOffer) {
    if (!item) {
      this.toastrCustom.error(this.translate.instant('development.commonMessage.titleHandleValidate'));
    }
  }

  checkPositionHandle(item: ListHandleOffer) {
    if (!item) {
      this.toastrCustom.error(this.translate.instant('development.commonMessage.positionHandleValidate'));
    }
  }

  changeValuePosition(event: any) {
    this.isLoading = true;
    if (!event) {
      return;
    }
    this.serviceProposed.getLine(event).subscribe(
      (res) => {
        this.isLoading = false;
      },
      (e) => {
        this.isLoading = false;
      }
    );
  }

  saveItemListHandleOffer(item: ListHandleOffer, index: number) {
    if (this.formGroupPropose === Constants.FormGroupList.TGCV
      || this.formGroupPropose === Constants.FormGroupList.PV) {
        if (!item.proposeUnitName || !item.proposeTitleName || !item.proposePositionName) {
          this.toastrCustom.error(this.translate.instant('development.commonMessage.handleCommonValidate3'));
          return;
        }
      } else {
        if (!item.formRotationCode || !item.proposeUnitName || !item.proposeTitleName || !item.proposePositionName) {
          this.toastrCustom.error(this.translate.instant('development.commonMessage.handleCommonValidate'));
          return;
        }
      }
    if ((!item.issueLevelCode || !item.approvalCode || !item.signerCode) && !this.isAddingListHandleOffer) {
      this.toastrCustom.error(this.translate.instant('development.commonMessage.validateIssueLevelApprovalSigner'));
      return;
    }
    //validate thời gian tối thiểu và tối đa (nếu có)
    if (item?.hasMinimumTime && !item?.minimumTime) {
      this.toastrCustom.error(this.translate.instant('development.promulgate.message.validateMinTime'));
      return;
    }
    if (item?.hasMaximumTime && (!item?.maximumTime || 
        (!item?.timeType && item?.timeType !== Constants.TimeTypeCode.MONTH))) {
      this.toastrCustom.error(this.translate.instant('development.promulgate.message.validateMaxTime'));
      return;
    }
    if (item?.minimumTime && item?.maximumTime) {
      if (item?.timeType === Constants.TimeTypeCode.MONTH) {
        if (+item?.minimumTime >= +item.maximumTime) {
          this.toastrCustom.error(this.translate.instant('development.promulgate.message.validateMinMaxTime'));
          return;
        }
      } else if (item?.timeType === Constants.TimeTypeCode.YEAR) {
        if (+item?.minimumTime >= +item.maximumTime * 12) {
          this.toastrCustom.error(this.translate.instant('development.promulgate.message.validateMinMaxTime'));
          return;
        }
      }
    }
    const body = clearParamSearch(item);
    body.selectedDate = body.selectedDate ? moment(body.selectedDate).format(Constants.FORMAT_DATE.YYYY_MM_DD) : body.selectedDate;
    body.unselectedDate = body.unselectedDate ? moment(body.unselectedDate).format(Constants.FORMAT_DATE.YYYY_MM_DD) : body.unselectedDate;
    body.proposeUnitPathId = body.proposeUnitName?.pathId || ''; // Rỗng
    // API trả về là pathName nhưng dùng mbSelectUnit lại bị chuyền thành trường pathResult
    body.proposeUnitPathName = body.proposeUnitName?.pathResult; // Có
    body.proposeUnit = parseInt(body.proposeUnitName?.orgId);
    body.proposeUnitName = body.proposeUnitName?.orgName;

    body.proposeTitle = body.proposeTitleName ? +body.proposeTitleName : body.proposeTitleName;
    body.proposeTitleName = this.dataCommon?.jobDTOS?.find((itemJob) => itemJob.jobId === body.proposeTitle)?.jobName;
    body.proposeBusinessLine = body.proposeBusinessLine ? parseInt(body.proposeBusinessLine) : body.proposeBusinessLine;
    body.titleLevelName = body.titleLevelCode;
    body.proposePosition = body.proposePositionName;
    body.proposePositionName = body.proposePositionName
      ? this.positionHandleOffer.find((itemHandleOffer: any) => itemHandleOffer.id === body.proposePosition)
      : body.proposePositionName;
    body.proposePositionName = body.proposePositionName?.name || null;
    body.proposeDetailId = this.proposeDetailId;
    body.issueLevelCode = item.issueLevelCode;
    body.issueLevel = this.listIssueLevelOptionItem?.find((item) => item.code === body.issueLevelCode)?.name;
    body.signerCode = item.signerCode;
    body.approvalCode = item.approvalCode;
    body.approvalName = this.listApprover.find((item) => item.code === body.approvalCode)?.fullname;
    body.signerName = this.listSigner.find((item) => item.code === body.signerCode)?.fullname;
    body.approvalTitleName = this.listApprover.find((item) => item.code === body.approvalCode)?.title;
    body.signerTitleName = this.listSigner.find((item) => item.code === body.signerCode)?.title;
    body.approvementApproveCode = this.listApprover.find((item) => item.code === body.approvalCode)?.approvementConfigCode;
    body.approvementSignerCode = this.listSigner.find((item) => item.code === body.signerCode)?.approvementConfigCode;
    body.formRotationCode = item.formRotationCode;
    body.formRotationName = this.dataCommon?.formDisplaySearchDTOS?.find(
      (item) => item.code === body.formRotationCode)?.formName;
    body.hasInterview = item.hasInterview;
    body.hasConsultation = item.hasConsultation;
    body.minimumTime = item.minimumTime;
    body.maximumTime = item.maximumTime;
    body.timeType = item.timeType;
    body.proposeUnitPathId = item?.proposeUnitPathId;
    delete body.optionCode; // remove optionCode when submit
    this.onLoading.emit(true);
    //nếu là edit thì call thêm api này để check phương án chọn trình thay đổi cấp phê duyệt, người duyệt
    if (this.onOpenEditItem && !this.isAddingListHandleOffer) {
      this.serviceProposed.saveItemHandleOffer(body, '').subscribe((res) => {
        if (res?.data) {
          this.onLoading.emit(false);
          this.modalRef = this.modal.confirm({
            nzTitle: this.translate.instant('development.commonMessage.warningSaveHandleOffer'),
            nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL2_TEXT),
            nzOkText: this.translate.instant(Constants.KeyCommons.CONTINUE_TEXT),
            nzClassName: Constants.ClassName.LD_CONFIRM,
            nzOnOk: () => {
              this.onLoading.emit(true);
              this.callApiSaveItemHandleOffer(body, item, index);
            },
          });
        } else {
          this.loadListHandleOffer(item);
        }
      },
      (err) => {
        this.toastrCustom.error(err?.error?.message);
        this.onLoading.emit(false);
      })
    } else {
      this.callApiSaveItemHandleOffer(body, item, index);
    }
  }

  loadListHandleOffer(item: ListHandleOffer) {
    item.isEdit = false;
    this.isAddingListHandleOffer = false;
    this.onOpenEditItem = false;
    item.notSave
      ? this.toastrCustom.success(
          this.translate.instant(
            'development.proposed-unit.consultationScreen.addSuccess'
          )
        )
      : this.toastrCustom.success(
          this.translate.instant(
            'development.proposed-unit.consultationScreen.editSuccess'
          )
        );
    const sendInterviewBody = {
      detailId: this.proposeDetailId,
    };
    forkJoin([
      this.serviceProposed
        .getIssueLevelOption(this.proposeDetailId)
        .pipe(catchError(() => of(undefined))),
      this.serviceProposed
        .listHandleOffer(this.proposeDetailId)
        .pipe(catchError(() => of(undefined))),
      this.serviceProposed
        .sendInterview(sendInterviewBody)
        .pipe(catchError(() => of(undefined))),
    ]).subscribe(
      ([resOption, resHandleOffer, sendInterview]) => {
        this.listIssueLevelOption = resOption?.data;
        this.listHandleOffer = resHandleOffer?.data?.content;
        this.getDataListPlan(this.listHandleOffer);
        this.checkEnableListConsultation();
        if (this.listHandleOffer?.length > 1) {
          this.planCheck = 0;
        }
        this.listHandleOffer?.forEach((item: ListHandleOffer) => {
          if (
            this.dataCommon?.formDisplaySearchDTOS &&
            this.dataCommon.formDisplaySearchDTOS.length > 0
          ) {
            const formDisplayObject =
              this.dataCommon.formDisplaySearchDTOS.find(
                (i) => i.code === item.formRotationCode
              );
            item.formRotationName = formDisplayObject?.formName
              ? formDisplayObject.formName
              : '';
          }
        });
        this.listSendInterviews = sendInterview?.data;
        this.checkSendInterviewButton();
        this.onLoading.emit(false);
      },
      (e) => {
        this.getMessageError(e);
        this.onLoading.emit(false);
      }
    );
  }

  //call api lưu phương án xử lý
  callApiSaveItemHandleOffer(body: ListHandleOffer, item: ListHandleOffer, index: number) {
    this.serviceProposed.saveItemHandleOffer(body, true).subscribe(
      () => {
        this.loadListHandleOffer(item);
      },
      (err) => {
        this.onLoading.emit(false);
        this.toastrCustom.error(err?.error?.message);
        item.isEdit = true;
        const listHandleOfferOld = JSON.parse(this.currentListHandleOffer);
        this.listHandleOffer[index] = listHandleOfferOld[index];
      }
    );
  }

  expandLog() {
    this.isExpand = !this.isExpand;
    if (this.isExpand) {
      this.iconStatus = Constants.IconStatus.DOWN;
    } else {
      this.iconStatus = Constants.IconStatus.UP;
    }
  }

  editItemHandleOffer(item: ListHandleOffer, index: number) {
    if (this.listIssueLevelOption) {
      this.listIssueLevelOptionItem = this.listIssueLevelOption[`${item.id}`];
    }
    this.onOpenEditItem = true;
    this.issueLevelCodeBefore = item.issueLevelCode;
    this.approvalCodeBefore = item.approvalCode;
    this.signerCodeBefore = item.signerCode;
    this.selectIssueLevelOptionItem(item.issueLevelCode, item);
    this.planCheckFirst = false;
    this.onOpenHandleFormEdit = true;
    this.currentListHandleOffer = JSON.stringify(this.listHandleOffer);
    item.isEdit = true;
    this.listHandleOffer[index].proposeUnitName = {
      orgId: this.listHandleOffer[index].proposeUnit,
      orgName: this.listHandleOffer[index].proposeUnitName,
      pathResult: this.listHandleOffer[index].proposeUnitPathName,
      pathId: '',
    };
    this.listHandleOffer[index].proposeTitleName = this.listHandleOffer[index].proposeTitle;
    this.listHandleOffer[index].titleLevelCode = this.listHandleOffer[index].titleLevelCode;
    this.listHandleOffer[index].proposePositionName = +this.listHandleOffer[index].proposePosition;
    this.listHandleOffer[index].note = this.listHandleOffer[index].note || '';
    this.listHandleOffer[index].salaryComment = this.listHandleOffer[index].salaryComment || '';
    this.listHandleOffer[index].sentInterviewComment = this.listHandleOffer[index].sentInterviewComment || '';
    this.listHandleOffer[index].checkInterviewResults = this.listHandleOffer[index].checkInterviewResults || '';
    this.listHandleOffer[index].proposeBusinessLine = this.listHandleOffer[index].proposeBusinessLine || '';
  }

  //lấy người ký người duyệt của phương án được chọn trình
  getApproverSignerOptionPlan(item: ListHandleOffer) {
    this.serviceProposed.getApproverSignerOptionPlan(item.id).subscribe((res) => {
      this.listApprover = [];
      this.listSigner = [];
      this.listApprover.push({
        code: res?.data?.approvalCode,
        fullname: res?.data?.approvalName,
        title: res?.data?.approvalTitleName,
        approvementConfigCode: res?.data?.approvementApproveCode,
      });
      this.listSigner.push({
        code: res?.data?.signerCode,
        fullname: res?.data?.signerName,
        title: res?.data?.signerTitleName,
        approvementConfigCode: res?.data?.approvementSignerCode,
      });
      item.approvalCode = this.listApprover[0]?.code;
      item.signerCode = this.listSigner[0]?.code;
      this.onLoading.emit(false);
    },
    (e) => {
      this.getMessageError(e);
      this.onLoading.emit(false);
    })
  }

  cancelITemListHandleOffer(item: ListHandleOffer, index: number) {
    if (item.notSave) {
      this.listHandleOffer.splice(index, 1);
    } else {
      item.isEdit = false;
      const listHandleOfferOld = JSON.parse(this.currentListHandleOffer);
      this.listHandleOffer[index] = listHandleOfferOld[index];
    }
    this.onOpenEditItem = false;
    this.isAddingListHandleOffer = false;
  }

  deleteItemHandleOffer(id: number) {
    this.onLoading.emit(true);
    this.serviceProposed.deleteItemHandleOffer(id).subscribe(
      (res) => {
        this.onLoading.emit(false);
        this.toastrCustom.success(this.translate.instant('development.list-plan.messages.deletePlanSuccess'));
        this.serviceProposed.listHandleOffer(this.proposeDetailId).subscribe((resHandleOffer) => {
          this.listHandleOffer = resHandleOffer.data.content;
          this.getDataListPlan(this.listHandleOffer);
          this.checkEnableListConsultation();
        });
      },
      (err) => {
        this.onLoading.emit(false);
        this.toastrCustom.error(err?.error?.message);
      }
    );
  }

  get checkItemHandleOffer(): boolean {
    const item = this.listHandleOffer.find((itemHandleOffer) => itemHandleOffer.selected);
    return !!item;
  }

  //hàm chọn để trình cũ
  toggleCheckItemHandleOffer(isCheck: boolean, id: number) {
    if (isCheck) {
      if (this.checkItemHandleOffer) {
        return this.toastrCustom.error(
          this.translate.instant('development.proposed-unit.consultationScreen.onlyOnePresent')
        );
      }
    }
    this.isLoading = true;
    this.serviceProposed.toggleCheckItemHandleOffer(isCheck, id).subscribe(
      (res) => {
        this.isLoading = false;
        if (isCheck) {
          this.toastrCustom.success(
            this.translate.instant('development.proposed-unit.consultationScreen.presentSuccess')
          );
        } else {
          this.toastrCustom.success(
            this.translate.instant('development.proposed-unit.consultationScreen.unPresentSuccess')
          );
        }
        this.serviceProposed.listHandleOffer(this.proposeDetailId).subscribe((resHandleOffer) => {
          this.listHandleOffer = resHandleOffer.data.content;
        });
      },
      (err) => {
        this.isLoading = false;
        this.toastrCustom.error(err.error.message);
      }
    );
  }

  checkConsultation(code: string, id: number) {
    this.isLoading = true;
    this.serviceProposed.checkConditionConsultation(code, id).subscribe(
      (res) => {
        this.isLoading = false;
        this.toastrCustom.success(res.message);
        this.serviceProposed.listHandleOffer(this.proposeDetailId).subscribe((resHandleOffer) => {
          this.listHandleOffer = resHandleOffer.data.content;
          this.checkEnableListConsultation();
        });
      },
      (err) => {
        this.isLoading = false;
        this.getMessageError(err);
      }
    );
  }

  checkInterview(code: string, id: number) {
    this.isLoading = true;
    this.serviceProposed.checkConditionInterview(code, id).subscribe(
      (res) => {
        this.isLoading = false;
        this.toastrCustom.success(res.message);
        this.serviceProposed.listHandleOffer(this.proposeDetailId).subscribe((resHandleOffer) => {
          this.listHandleOffer = resHandleOffer.data.content;
        });
      },
      (err) => {
        this.isLoading = false;
        this.getMessageError(err);
      }
    );
  }

  sendInterview(id: number) {
    this.onLoading.emit(true);
    const body = {
      ids: [id],
    };
    this.serviceProposed.sendInterview(body).subscribe(
      (res) => {
        this.toastrCustom.success(this.translate.instant('development.list-plan.messages.sendInterviewSuccess'));
        this.viewLog();
        const sendInterviewBody = {
          detailId: this.proposeDetailId,
        }
        forkJoin([
          this.serviceProposed.sendInterview(sendInterviewBody).pipe(catchError(() => of(undefined))),
          this.serviceProposed.listHandleOffer(this.proposeDetailId).pipe(catchError(() => of(undefined))),
        ]).subscribe(
          ([
            sendInterview,
            resHandleOffer
          ]) => {
            this.listSendInterviews = sendInterview?.data || [];
            this.checkSendInterviewButton();
            this.listHandleOffer = resHandleOffer?.data?.content;
            this.onLoading.emit(false);
          },
          (e) => {
            this.getMessageError(e);
            this.onLoading.emit(false);
          }
        );
        this.onLoading.emit(false);
      },
      (err) => {
        this.onLoading.emit(false);
        this.getMessageError(err);
      }
    );
  }

  sendSalary(id: number) {
    this.onLoading.emit(true);
    const body = [];
    body.push(id);
    this.serviceProposed.sendSalary(body).subscribe(
      (res) => {
        this.toastrCustom.success(this.translate.instant('development.list-plan.messages.sendSalarySuccess'));
        this.viewLog();
        this.serviceProposed.listHandleOffer(this.proposeDetailId).subscribe((resHandleOffer) => {
          this.listHandleOffer = resHandleOffer.data.content;
        });
        this.onLoading.emit(false);
      },
      (err) => {
        this.onLoading.emit(false);
        this.getMessageError(err);
      }
    );
  }

  checkResultInterview() {
    this.isLoading = true;
    const body: number[] = [];
    this.listHandleOffer.forEach((item) => {
      if (item.id) {
        body.push(item.id);
      }
    });
    this.serviceProposed.checkResultInterview(body).subscribe(
      (res) => {
        this.isLoading = false;
        this.toastrCustom.success('Success');
        this.serviceProposed.listHandleOffer(this.proposeDetailId).subscribe((resHandleOffer) => {
          this.listHandleOffer = resHandleOffer.data.content;
          this.checkEnableListConsultation();
        });
      },
      (err) => {
        this.isLoading = false;
        this.getMessageError(err.message);
      }
    );
  }

  addFormItemConsultationBefore() {
    const item = {
      proposeOptionId: null,
      formName: null,
      contentUnitName: null,
      contentTitleName: null,
      contentLevelName: null,
      consultDate: null,
      consultName: null,
      consultUnitName: null,
      status: null,
      note: null,
      show: false,
      showOpinion: false,
      isEdit: true,
      notSave: true,
    };
    this.listConsultationBefore.push(item);
  }

  addItemListConsultationBefore() {
    if (this.isAddingListConsultationBefore) {
      return;
    }
    this.onOpenEdit = true;
    this.isAddingListConsultationBefore = true;
    this.addFormItemConsultationBefore();
  }

  openCalendar() {
    this.onOpenEdit = false;
  }

  checkValidConsultDate(item: any) {
    if (!item && !this.onOpenEdit) {
        this.toastrCustom.error(this.translate.instant('development.commonMessage.validateConsultDate'));
    }
  }

  checkValidConsultName(name: string) {
    if (name?.length === 0) {
      this.toastrCustom.error(this.translate.instant('development.commonMessage.validateConsultName'));
    }
  }

  checkValidConsultIdea(item: any) {
    if (!item) {
      this.toastrCustom.error(this.translate.instant('development.commonMessage.validateConsultIdea'));
    }
  }

  saveItemListConsultationBefore(item: ListConsultationBefore, index: number) {
    if (item.consultDate && item.consultName && item.status) {
      this.onLoading.emit(true);
      const typeConsultation = 2;
      const body = clearParamSearch(item);
      body.consultDate = body.consultDate ? moment(body.consultDate).format(Constants.FORMAT_DATE.YYYY_MM_DD) : body.consultDate;
      body.consultUnit = parseInt(body.consultUnitName?.orgId);
      body.consultUnitName = body.consultUnitName?.orgName;
      body.contentLevel = body.contentLevelName;
      body.contentLevelName = this.level.find((itemLevel) => itemLevel.code === body.contentLevelName)?.name;
      body.status = parseInt(body.status);
      body.proposeDetailId = this.proposeDetailId;
      this.serviceProposed.addConsultation(typeConsultation, body).subscribe(
        (res) => {
          this.onLoading.emit(false);
          item.notSave
            ? this.toastrCustom.success(
                this.translate.instant('development.proposed-unit.consultationScreen.addSuccess')
              )
            : this.toastrCustom.success(
                this.translate.instant('development.proposed-unit.consultationScreen.editSuccess')
              );
          this.cancelItemListConsultationBefore(item, index);
          const sendInterviewBody = {
            detailId: this.proposeDetailId,
          }
          forkJoin([
            this.serviceProposed.listConsultation(+this.proposeDetailId).pipe(catchError(() => of(undefined))),
            this.serviceProposed.listHandleOffer(this.proposeDetailId).pipe(catchError(() => of(undefined))),
            this.serviceProposed.sendInterview(sendInterviewBody).pipe(catchError(() => of(undefined))),
          ]).subscribe(
            ([
              resConsultation,
              resHandleOffer,
              sendInterview
            ]) => {
              this.listHandleOffer = resHandleOffer?.data?.content;
              this.listHandleOffer?.forEach((item: ListHandleOffer) => {
                if (this.dataCommon?.formDisplaySearchDTOS && this.dataCommon.formDisplaySearchDTOS.length > 0) {
                  const formDisplayObject = this.dataCommon.formDisplaySearchDTOS.find(
                    (i) => i.code === item.formRotationCode);
                  item.formRotationName = formDisplayObject?.formName ? formDisplayObject.formName : '';
                }
              })
              this.listConsultationBefore = resConsultation?.data?.consultationListDTOS?.content || [];
              this.listSendInterviews = sendInterview?.data || [];
              this.checkSendInterviewButton();
              this.onLoading.emit(false);
            },
            (e) => {
              this.getMessageError(e);
              this.onLoading.emit(false);
            }
          );
        },
        (err) => {
          this.onLoading.emit(false);
          this.getMessageError(err);
        }
      );
    } else {
      this.toastrCustom.error(this.translate.instant('development.commonMessage.validateHandleConsult'));
    }
  }

  optionIdChange(event: any, item: any) {
    if (!event) {
      return;
    }
    const data: any = this.infoConsultation.find((itemInfo: any) => itemInfo.proposeOptionId === event);
    item.formName = data.formName;
    item.contentUnitName = { orgName: data.contentUnitName };
    item.contentTitleName = data.contentTitleName;
    item.contentLevelName = data?.contentLevelName;
  }

  cancelItemListConsultationBefore(item: any, index: number) {
    if (item.notSave) {
      this.listConsultationBefore.splice(index, 1);
     this.isAddingListConsultationBefore = false;
    }else{
      item.isEdit = false;
      this.listConsultationBefore[index] = this.currentListConsultationBefore[index];
    }
  }

  deleteItemConsultationBefore(index: number) {
    this.onLoading.emit(true);
    this.serviceProposed.deleteConsultationBefore(index).subscribe(
      () => {
        this.toastrCustom.success(
          this.translate.instant('development.proposed-unit.consultationScreen.deleteConsultationBeforeSuccess')
        );
        const sub = forkJoin([
          this.serviceProposed.listHandleOffer(this.proposeDetailId).pipe(catchError(() => of(undefined))),
          this.serviceProposed.listConsultation(+this.proposeDetailId).pipe(catchError(() => of(undefined))),
        ]).subscribe(
          ([
            resHandleOffer,
            resConsultation
          ]) => {
            this.listHandleOffer = resHandleOffer?.data?.content;
            this.listConsultationBefore = resConsultation?.data?.consultationListDTOS?.content || [];
            this.onLoading.emit(false);
          },
          (e) => {
            this.getMessageError(e);
            this.onLoading.emit(false);
          }
        );
        this.subs.push(sub);
      },
      (err) => {
        this.onLoading.emit(false);
        this.getMessageError(err.message);
      }
    );
  }

  editItemListConsultationBefore(item: any, index: number) {
    this.currentListConsultationBefore = JSON.parse(JSON.stringify(this.listConsultationBefore));
    item.isEdit = true;
    this.listConsultationBefore[index].consultUnitName = {
      orgId: this.listConsultationBefore[index].consultUnit,
      orgName: this.listConsultationBefore[index].consultUnitName,
    };
    this.listConsultationBefore[index].status = this.listIdeaConsultationBefore.find(
      (child) => child.name === item.status
    )?.code;
  }

  sumItUp() {
    this.onLoading.emit(true);
    this.serviceProposed.summarizeContent(+this.proposeDetailId).subscribe(
      (res) => {
        this.formReason?.get('summarizeLdComment')?.setValue(res.data);
        this.onLoading.emit(false);
      },
      (e) => {
        this.getMessageError(e);
        this.onLoading.emit(false);
      }
    );
  }

  downloadFile = (file: NzUploadFile) => {
    this.onLoading.emit(true);
    this.serviceProposed.downloadFile(+file?.uid, file?.name).subscribe(
      () => {
        this.onLoading.emit(false);
      },
      (e) => {
        this.getMessageError(e);
        this.onLoading.emit(false);
      }
    );
  };

  checkViewDetail(id: number) {
    return !!id;
  }

  // Handle
  doRemoveFile = (file: NzUploadFile): boolean => {
    if (this.typeAction !== ScreenType.Detail) {
      const idDelete = <number>this.fileListSelect.find((item) => item.uid === file.uid)?.['id'];
      this.serviceProposed.deleteUploadFile(+idDelete).subscribe(
        (res) => {
          this.fileListSelect = this.fileListSelect.filter((item) => item['id'] !== idDelete);
        },
        (e) => {
          this.getMessageError(e);
        }
      );
    }
    return false;
  };

  beforeUploadStatement = (file: File | NzUploadFile): boolean => {
    let groupProposeTypeId = this.listProposeCategory.find((item) => item.name === this.proposeCategory)?.id;
    const formData = new FormData();
    formData.append('files', file as File);
    formData.append('proposeDetailId', new Blob([JSON.stringify(this.proposeDetailId)], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('type', new Blob(['statement'], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('groupProposeTypeId', new Blob([JSON.stringify(groupProposeTypeId)], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('isIn', new Blob(['true'], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    this.serviceProposed.uploadFile(formData).subscribe(
      (res) => {
        this.fileListSelect = [
          ...this.fileListSelect,
          {
            uid: res.data?.docId,
            name: res.data?.fileName,
            status: 'removed',
            id: res.data?.id,
          },
        ];
      },
      (e) => {
        this.getMessageError(e);
      }
    );
    return false;
  };

  beforeUploadApprove = (file: File | NzUploadFile): boolean => {
    let groupProposeTypeId = this.listProposeCategory.find((item) => item.name === this.proposeCategory)?.id;
    const formData = new FormData();
    formData.append('files', file as File);
    formData.append('proposeDetailId', new Blob([JSON.stringify(this.proposeDetailId)], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('type', new Blob(['statementApprove'], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('groupProposeTypeId', new Blob([JSON.stringify(groupProposeTypeId)], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('isIn', new Blob(['true'], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    this.serviceProposed.uploadFile(formData).subscribe(
      (res) => {
        this.fileListSelectApprove = [
          ...this.fileListSelectApprove,
          {
            uid: res.data?.docId,
            name: res.data?.fileName,
            status: 'removed',
            id: res.data?.id,
          },
        ];
      },
      (e) => {
        this.getMessageError(e);
      }
    );
    return false;
  };

  //1
  beforeUpload1 = (file: File | NzUploadFile): boolean => {
    const formData = new FormData();
    formData.append('proposeCategory', this.proposeCategory);
    formData.append('files', file as File);
    formData.append('id', this.proposeCategory === 'ELEMENT' ? this.proposeDetailId.toString() : this.proposeId.toString());
    formData.append('type', 'DECISION_SIGN');
    formData.append('documentType', '1');

    this.serviceProposed.uploadFileHandle(formData).subscribe(
      (res) => {
        this.listFileNotifyDecision1 = [
          ...this.listFileNotifyDecision1,
          {
            uid: res.data[0]?.decisionSignId,
            name: res.data[0]?.decisionSignName,
            status: 'done',
            id: res.data[0]?.id,
          },
        ];
      },
      (e) => {
        this.getMessageError(e);
      }
    );
    return false;
  };

  doRemoveFile1 = (file: NzUploadFile): boolean => {
    if (this.typeAction === ScreenType.Detail) {
      const paramDelete = {
        id:
          this.proposeCategory === Constants.ProposeCategory.ELEMENT
            ? this.proposeDetailId
            : this.proposeId,
        proposeCategory: this.proposeCategory,
        documentType: 1,
        type: 'DECISION_SIGN',
        docId: file.uid,
        fileName: file.name,
      };
      this.serviceProposed.removeFileNotify(paramDelete).subscribe(
        (res) => {
          this.listFileNotifyDecision1 = [];
        },
        (e) => {
          this.getMessageError(e);
        }
      );
    }
    return false;
  };

  //2
  beforeUpload2 = (file: File | NzUploadFile): boolean => {
    const formData = new FormData();
    formData.append('proposeCategory', this.proposeCategory);
    formData.append('files', file as File);
    formData.append('id', this.proposeCategory === 'ELEMENT' ? this.proposeDetailId.toString() : this.proposeId.toString());
    formData.append('type', 'DECISION_SIGN');
    formData.append('documentType', '2');

    this.serviceProposed.uploadFileHandle(formData).subscribe(
      (res) => {
        this.listFileNotifyDecision2 = [
          ...this.listFileNotifyDecision2,
          {
            uid: res.data[0]?.decisionSignId,
            name: file.name,
            status: 'done',
            id: res.data[0]?.id,
          },
        ];
      },
      (e) => {
        this.getMessageError(e);
      }
    );
    return false;
  };

  doRemoveFile2 = (file: NzUploadFile): boolean => {
    if (this.typeAction === ScreenType.Detail) {
      const paramDelete = {
        id:
          this.proposeCategory === Constants.ProposeCategory.ELEMENT
            ? this.proposeDetailId
            : this.proposeId,
        proposeCategory: this.proposeCategory,
        documentType: 2,
        type: 'DECISION_SIGN',
        docId: file.uid,
        fileName: file.name,
      };
      this.serviceProposed.removeFileNotify(paramDelete).subscribe(
        (res) => {
          this.listFileNotifyDecision2 = [];
        },
        (e) => {
          this.getMessageError(e);
        }
      );
    }
    return false;
  };
  //3
  beforeUpload3 = (file: File | NzUploadFile): boolean => {
    const formData = new FormData();
    formData.append('proposeCategory', this.proposeCategory);
    formData.append('files', file as File);
    formData.append('id', this.proposeCategory === 'ELEMENT' ? this.proposeDetailId.toString() : this.proposeId.toString());
    formData.append('type', 'DECISION_SIGN');
    formData.append('documentType', '3');

    this.serviceProposed.uploadFileHandle(formData).subscribe(
      (res) => {
        this.listFileNotifyDecision3 = [
          ...this.listFileNotifyDecision3,
          {
            uid: res.data[0]?.decisionSignId,
            name: file.name,
            status: 'done',
            id: res.data[0]?.id,

          },
        ];
      },
      (e) => {
        this.getMessageError(e);
      }
    );
    return false;
  };

  doRemoveFile3 = (file: NzUploadFile): boolean => {
    if (this.typeAction === ScreenType.Detail) {
      const paramDelete = {
        id:
          this.proposeCategory === Constants.ProposeCategory.ELEMENT
            ? this.proposeDetailId
            : this.proposeId,
        proposeCategory: this.proposeCategory,
        documentType: 3,
        type: 'DECISION_SIGN',
        docId: file.uid,
        fileName: file.name,
      };
      this.serviceProposed.removeFileNotify(paramDelete).subscribe(
        (res) => {
          this.listFileNotifyDecision3 = [];
        },
        (e) => {
          this.getMessageError(e);
        }
      );
    }
    return false;
  };

  saveHandle() {
    this.submitHandle = true;
    if (this.formGroupPropose === Constants.FormGroupList.PV) {
      this.formReason?.get('summarizeLdComment')?.clearValidators();
      this.formReason?.get('summarizeLdComment')?.updateValueAndValidity();
    }
    if (this.formReason?.invalid) {
      if (!this.formReason?.get('summarizeLdComment')?.value) {
        this.scrollToValidationSection('tab6', 'development.commonMessage.validateTCNS');
        return;
      }
    } else {
      this.isLoading = true;
      const data = this.formReason?.getRawValue();
      data.statementNumber = this.statementNumber;
      data.submissionDate = this.submissionDate ? new Date(this.submissionDate) : null;
      data.isDecision = this.isCheckboxNotify1;
      data.isIncome = this.isCheckboxNotify2;
      data.isInsurance = this.isCheckboxNotify3;
      return data;
    }
  }

  changeNotifyCheck(e: boolean) {
    if (!e) {
      this.hidenButon = false;
      this.isCheckboxNotify2 = e;
      this.isCheckboxNotify3 = e;
    } else {
      this.hidenButon = true;
    }
  }

  changeItemSalary(itemSalary:any, e: boolean) {
    this.isLoading = true;
    this.serviceProposed.checkedSalary(itemSalary.salaryId, e).subscribe(
      (res) => {
        this.listSalary.forEach((item: any) => {
          if (item.id === itemSalary.salaryId) {
            item.selected = e;
          }
        });
        this.isLoading = false;
      },
      (err) => {
        this.getMessageError(err);
        this.listSalary.forEach((item: any) => {
          if (item.id === itemSalary.salaryId) {
            item.selected = !err;
          }
        });
        this.isLoading = false;
      }
    );
  }

  checkRequiredSendPropose() {
    if (
      this.listFileNotifyDecision1?.length === 0 ||
      (this.listFileNotifyDecision2?.length === 0 && this.isCheckboxNotify2) ||
      (this.listFileNotifyDecision3?.length === 0 && this.isCheckboxNotify3)
    ) {
      this.toastrCustom.error('Vui lòng đính kèm quyết định/thông báo trước khi gửi duyệt!');
      return true;
    }
    return false;
  }

  redirectScreenGroupOriginal(id: number, type: string) {
    if (Number.isInteger(id)) {
      this.router.navigateByUrl(Utils.getUrlByFunctionCode(this.functionCode)).then(() => {
        this.router.navigate([Utils.getUrlByFunctionCode(this.functionCode), 'detail'], {
          queryParams: {
            id: id,
            proposeDetailId: Constants.ProposeCategory.ORIGINAL ? this.proposeDetailId : null,
            proposeCategory: type,
            proposeType: type === Constants.ProposeCategory.GROUP ? 0 : 1,
            flowStatusCode: Constants.ProposeCategory.ORIGINAL ? this.flowStatusCode : null,
          },
          skipLocationChange: true,
        });
      });
    }
  }

  formatterCurrency = (value: number): string => {
    if (!Number.isInteger(value)) {
      return '';
    }
    return this.decimalPipe.transform(value, '1.0-2');
  };

  viewLog() {
    const idLog = this.proposeCategory === 'ORIGINAL' ? this.proposeId : +this.proposeDetailId;
    this.serviceProposed.getLogs(+idLog, this.queryCategory, this.screenCode).subscribe((res) => {
      this.proposeLogs = res?.data?.content || [];
    });
  }

  getInfoConsult() {
    this.serviceProposed.getInfoConsult(this.proposeDetailId).subscribe((res) => {
      this.infoConsultation = res?.data?.listOptionCode || [];
    });
  }

  //check hiển thị đính kèm tở trình
  checkDisplayAttachReport() {
    switch (this.screenCode) {
      case this.screenCodeConst.PTNL:
        return (
          this.typeAction === this.screenType.detail &&
          (this.flowStatusCode === this.flowCodeConstants.KSDongY ||
            this.flowStatusCode === this.flowCodeConstants.DongY10A)
        );
      case this.screenCodeConst.KSPTNL:
        return (
          this.flowStatusCode === this.flowCodeConstants.KSDongY ||
          this.flowStatusCode === this.flowCodeConstants.DongY10A
        );
      default:
        return false;
    }
  }
  //check hiển thị nút xuất tờ trình
  checkExportReport() {
    return (!this.dataForm.proposeDetails[0]?.isGroup && this.rolesOfCurrentUser?.includes(this.screenCodeConst.HCMLDEXLPDPTNL))
      || (!this.dataForm.proposeDetails[0]?.groupByKSPTNL && this.rolesOfCurrentUser?.includes(this.screenCodeConst.HCMLDEXLKTPTNL));
  }

  onChangeUnit(orgId: string) {
    if (orgId) {
      this.isLoading = true;
      const paramPosition: ParamSearchPosition = {
        orgId: orgId,
        jobId: '',
        status: 1,
        type: Constants.TYPE_SEARCH_POSITION,
        page: 0,
        size: maxInt32,
      };
      this.serviceProposed.getPositionByUnit(paramPosition).subscribe(
        (res) => {
          this.dataCommon!.jobDTOS = res?.data?.content?.map((item: any) => {
            return { jobId: item.jobId, jobName: item.jobName };
          });
          this.isLoading = false;
        },
        (e) => {
          this.isLoading = false;
        }
      );
    }
  }

  viewlistHandleOffer() {
    this.serviceProposed.listHandleOffer(this.proposeDetailId).subscribe((res) => {
      this.listHandleOffer = res.data.content
    });
  }

  //hiển thị popup Dừng phỏng vấn
  showModalStopInterview(optionId: number){
    this.modalRef = this.modal.create({
      nzTitle: this.translate.instant('development.consultation-screen.stopInterviewReason'),
      nzWidth: '30vw',
      nzContent: StopInterviewComponent,
      nzComponentParams: {},
      nzFooter: null,
    });
    this.modalRef.componentInstance.closeEvent.subscribe((data:boolean) => {
      if (data) {
        this.modalRef!.componentInstance.sendReason.subscribe((reason: string) => {
          this.onLoading.emit(true);
          const sub = this.serviceProposed.cancelInterview(optionId, reason).subscribe(() => {
            this.toastrCustom.success(this.translate.instant('development.commonMessage.cancelInterviewSuccess'));
            this.viewLog();
            const sendInterviewBody = {
              detailId: this.proposeDetailId
            };
            forkJoin([
              this.serviceProposed.sendInterview(sendInterviewBody).pipe(catchError(() => of(undefined))),
              this.serviceProposed.listHandleOffer(this.proposeDetailId).pipe(catchError(() => of(undefined))),
            ]).subscribe(
              ([
                sendInterview,
                resHandleOffer
              ]) => {
                this.listHandleOffer = resHandleOffer?.data?.content;
                this.listSendInterviews = sendInterview?.data || [];
                this.checkSendInterviewButton();
                this.onLoading.emit(false);
              },
              (e) => {
                this.getMessageError(e);
                this.onLoading.emit(false);
              }
            );
          },
          (e) => {
            this.getMessageError(e);
            this.onLoading.emit(false);
          });
          this.subs.push(sub);
          this.modalRef?.destroy();
        });
      } else {
        this.modalRef?.destroy();
      }
    });
  }

  checkViewDigitalUpload(){
    return this.flowStatusCode === this.flowCodeConstants.KSDongY && this.hidenButon
  }

  checkViewDigital(){
    return this.flowStatusCode === Constants.FlowCodeConstants.CHODUYET7 ||
      this.flowStatusCode === Constants.FlowCodeConstants.HANDLING6 ||
      this.flowStatusCode === Constants.FlowCodeConstants.KSDongY
      && this.proposeCategory === Constants.ProposeCategory.ELEMENT
  }

  //check hiển thị dropdownList phương án chọn trình
  checkDropListPlan() {
    return this.typeAction === ScreenType.Detail
      || (this.typeAction === ScreenType.Update && Constants.StatusCB.indexOf(this.flowStatusCode) > -1
      && this.listHandleOffer?.length > 0);
  }

  getDataListPlan(data: ListHandleOffer[]){
    this.listPlanCheckOption = data?.map((item) => {
      return {
        value:
          item?.optionCode
            + (item.proposeUnitPathName ? (' - ' + item.proposeUnitPathName) : '')
            + (item.proposeTitleName ? (' - ' + item.proposeTitleName) : '')
            + (item.titleLevelName ? (' - ' + item.titleLevelName) : ''),
        key: item?.id
      }
    })
  }

  //chọn phương án được chọn trình
  chooseListPlanOption() {
    this.onLoading.emit(true);
    const sub = this.serviceProposed.checkOptionPlan(this.planCheck, '').subscribe((res) => {
      this.dataOptionPlan = res?.data;
      if (this.dataOptionPlan) {
        this.onLoading.emit(false);
        this.modalRef = this.modal.confirm({
          nzTitle: this.translate.instant('development.commonMessage.warningCheckOption'),
          nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
          nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
          nzClassName: Constants.ClassName.LD_CONFIRM,
          nzOnOk: () => {
            this.onLoading.emit(true);
            this.callApiOptionPlan();
          },
        });
      } else {
        this.callApiOptionPlan();
      }
    },
    (e) => {
      this.getMessageError(e);
      this.onLoading.emit(false);
    });
    this.subs.push(sub);
  }

  //gọi API chọn trình
  callApiOptionPlan() {
    const sub = this.serviceProposed.checkOptionPlan(this.planCheck, true).subscribe((res) => {
      this.onLoading.emit(false);
    },
    (e) => {
      this.getMessageError(e);
      this.onLoading.emit(false);
    });
    this.subs.push(sub);
  }

  //check hiển thị cục Tờ trình
  checkDisplayReport() {
    return this.flowStatusCode === Constants.FlowCodeConstants.KSDongY && this.statusPublishCode
  }

  override ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
    this.modalRef?.destroy();
  }

  selectIssueLevelOptionItem(issueLevelCode: string, item: ListHandleOffer) {
    this.onLoading.emit(true);
    if (!this.planCheckFirst) {
      if (issueLevelCode === this.issueLevelCodeBefore) {
        this.getApproverSignerOptionPlan(item);
      } else {
        this.serviceProposed.getApproverSignerOption(issueLevelCode).subscribe((res) => {
          this.listApprover = res?.data?.approverList || [];
          this.listSigner = res?.data?.signerList || [];
          if (this.listApprover?.length === 1) {
            item.approvalCode = this.listApprover[0]?.code;
          } else {
            item.approvalCode = '';
          }
          if (this.listSigner?.length === 1) {
            item.signerCode = this.listSigner[0]?.code;
          } else {
            item.signerCode = '';
          }
          this.onLoading.emit(false);
        },
        (e) => {
          this.getMessageError(e);
          this.onLoading.emit(false);
        })
      }
    }
  }

  downloadFileInterview(uid: string, fileName: string) {
    this.onLoading.emit(true);
    this.serviceProposed.downloadFile(+uid, fileName).subscribe(
      () => {
        this.onLoading.emit(false);
      },
      (e) => {
        this.getMessageError(e);
        this.onLoading.emit(false);
      }
    );
  }

  //view số quyết định và ngày ký
  checkDecisionNumberSignDate() {
    return this.statusPublishCode.startsWith(Constants.DecisionNumberSignDate.START11DOT1)
      || this.statusPublishCode.startsWith(Constants.DecisionNumberSignDate.START11DOT2)
      || this.statusPublishCode.startsWith(Constants.DecisionNumberSignDate.START11DOT3)
      || this.statusPublishCode.startsWith(Constants.DecisionNumberSignDate.START12DOT);
  }


  //check hiển thị ý kiến phê duyệt KSPTNL
  checkIdeaApproveKSPTNL() {
    return this.screenCode === this.screenCodeConst.KSPTNL
      && this.flowStatusCode === Constants.FlowCodeConstants.CHODUYET7
      && this.dataForm?.proposeDetails[0]?.groupByKSPTNL;
  }

  scrollToValidationSection(section: string, messsage: string){
    this.toastrCustom.warning(this.translate.instant(messsage));
    const el = document.getElementById(section);
    el?.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
  }

  //save thông tin kiêm nhiệm hiện tại
  saveConcurrently(concurrentUnitId: number) {
    const body = this.infoConcurrently.find((item) => item?.concurrentUnitId === concurrentUnitId)
    this.isLoading = true;
    if(body) {
      this.serviceProposed.saveConcurrent(body, this.proposeDetailId).subscribe((res) => {
        this.isLoading = false;
      })
    }
  }

  saveConcurrentPosition(alternativeId : number, alternativePlan: string) {
    const alternativeIdNew = this.infoConcurrentlyNow.find((item) => item?.alternativeId === alternativeId)?.alternativeId
    this.isLoading = true;
    if(alternativeIdNew && alternativePlan){
      this.serviceProposed.saveConcurrentPosition(alternativeIdNew, alternativePlan).subscribe((res) => {
        this.isLoading = false;
      })
    }
  }

  //check hiển thị nút gửi phỏng vấn trong bảng xử lý
  checkSendInterviewButton() {
    this.listHandleOffer?.forEach((item: ListHandleOffer) => {
      const sendInterviewObj = this.listSendInterviews.find(
        (interview: SendInterviewsModel) => interview?.optionId === item?.id);
      item.isShowSendInterview = sendInterviewObj?.isShowSendInterview;
      item.isPtnl = sendInterviewObj?.isPtnl;
    });
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'development.proposed-unit.proposeLogsTable.time',
          field: 'actionDate',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
          tdTemplate: this.dateLog
        },
        {
          title: 'development.proposed-unit.proposeLogsTable.personAction',
          field: 'userName',
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
        },
        {
          title: 'development.proposed-unit.proposeLogsTable.action',
          field: 'action',
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
        },
        {
          title: 'development.proposed-unit.proposeLogsTable.description',
          field: 'comment',
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 200,
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1,
    };
  }

  getLog(pageIndex: number){
    this.pageNumber = pageIndex - 1;
    this.isLoading = true;
    const idLog = (this.proposeCategory === Constants.ProposeCategory.ORIGINAL || this.proposeCategory === Constants.ProposeCategory.GROUP)
      ? this.proposeId : +this.proposeDetailId;
    const log = this.serviceProposed.getLogsPage(idLog, this.proposeCategory, this.screenCode, this.pageNumber, this.pageSize)
      .subscribe((logs) => {
        this.proposeLogs = logs?.data?.content || [];
        this.tableConfig.total = logs?.data?.totalElements || 0;
        this.tableConfig.pageIndex = pageIndex;
        this.isLoading = false;
      }, (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      })
    this.subs.push(log);
  }
}
