import {Component, EventEmitter, Injector, Input, OnInit, Output} from '@angular/core';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {NzModalRef} from "ng-zorro-antd/modal";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-propose-popup-merge',
  templateUrl: './propose-popup-merge.component.html',
  styleUrls: ['./propose-popup-merge.component.scss'],
})
export class ProposePopupMergeComponent extends BaseComponent {
  @Output() eventoptionNotes = new EventEmitter<string>();
  @Output() closeEvent = new EventEmitter<boolean>();
  modalRef?: NzModalRef;
  form: FormGroup;

  constructor(injector: Injector) {
    super(injector);
    this.form = new FormGroup({
      optionNotes: new FormControl('')
    });
  }

  closeModal() {
    this.closeEvent.emit(false);
  }

  save() {
    this.isLoading = true;
    this.eventoptionNotes?.emit(this.form.controls['optionNotes'].value);
  }

}
