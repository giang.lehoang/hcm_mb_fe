import { Component, EventEmitter, Injector, Input, Output, TemplateRef, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import {
  Constants, functionUri,
  maxInt32,
  ScreenType,
  SessionKey,
  url,
  userConfig,
  Utils
} from "@hcm-mfe/learn-development/data-access/common";
import {
  AttachFile,
  DataCommon,
  EmployeeInfoMultiple, EmployeeInfoMultipleEdit, FileDTOList, FormGroupMultipleList, FormTTQD, IssueTransfer, JobDTOS, LevelTitle, ListFileTemplate, ParamSearchPosition, ProposeAlternative, ProposeCommentAppoint, Proposed,
  ProposedCreate,
  ProposeDetail,
  ProposedModel,
  ProposeEmployeeCreateDTO, ProposeRotationReasons, WithdrawPropose
} from "@hcm-mfe/learn-development/data-access/models";
import { ProposedUnitService, ProposeMultipleEmployeeService } from "@hcm-mfe/learn-development/data-access/services";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import { cleanData, cleanDataForm } from "@hcm-mfe/shared/common/utils";
import {MBTableConfig, Pageable, Pagination} from "@hcm-mfe/shared/data-access/models";
import { User } from "@hcm-mfe/system/data-access/models";
import { saveAs } from 'file-saver';
import * as moment from 'moment';
import { NzSafeAny } from "ng-zorro-antd/core/types";
import { NzModalRef } from 'ng-zorro-antd/modal';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { forkJoin, of, Subscription } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ProposeMultipleEmployeeComponent } from "../propose-multiple-employee/propose-multiple-employee.component";
import { ProposeSingleComponent } from "../propose-single/propose-single.component";
import { ProposeTemplateComponent } from "../propose-template/propose-template.component";
import { WithdrawProposeComponent } from '../withdraw-propose/withdraw-propose.component';
import {ModalTemplateProposeComponent} from "../modal-template-propose/modal-template-propose.component";

@Component({
  selector: 'app-propose-multiple',
  templateUrl: './propose-multiple.component.html',
  styleUrls: ['./propose-multiple.component.scss'],
})
export class ProposeMultipleComponent extends BaseComponent {
  @Input() dataCommon: DataCommon = {
    jobDTOS: [], // vị trí
    typeDTOS: [], // loại đề xuất
    formDisplaySearchDTOS: [], // hình thức
    proposeRotationReasonConfigDTOS: [], // lý do luân chuyển
    statusList: [],
    flowStatusApproveConfigList: [],
    flowStatusPublishConfigList: [],
    formGroupMultipleList: <FormGroupMultipleList[]>[],
    levelTitle: <LevelTitle[]>[],
  };
  @Input() typeAction?: ScreenType;
  @Input() dataForm: any;
  @Input() screenCode = '';
  @Output() submitData: EventEmitter<any> = new EventEmitter();
  @ViewChild('proposeSingle') proposeSingle?: ProposeSingleComponent;
  @Output() submitReport: EventEmitter<any> = new EventEmitter<any>();
  @Output() loadProposedUpdate: EventEmitter<any> = new EventEmitter<any>();
  @Output() onLoading = new EventEmitter<boolean>();
  @Output() checkFlowStatusElement = new EventEmitter<boolean>();

  @Input() proposeCategory = '';
  @Input() isScreenHandle = false;
  type?: ScreenType;
  isVisible = false;
  isOKLoading = false;
  titleModal = '';
  proposeList: ProposedModel[] = [];
  itemPropose?: ProposeEmployeeCreateDTO;
  displayReport = false;
  indexEdit: number | null = null;

  stateNumber = new FormControl(null);
  submissionDate = new FormControl(null);
  decisionNumber = new FormControl(null);
  signedDate = new FormControl(null);
  formGroup = 0;
  isSubmitted = false;
  isSubmittedTable = false;

  formAppletEmployee = new FormGroup({
    formCode: new FormControl(null, [Validators.required]),
    currentUnit: new FormControl(null, [Validators.required]),
    currentTitle: new FormControl(null, [Validators.required]),
    sameCurrentTitle: new FormControl(false),
    implementDate: new FormControl(null, [Validators.required]),
    minimumTime: new FormControl(null),
    maximumTime: new FormControl(null),
  });

  modalRef?: NzModalRef;
  listEmployeeInfo: EmployeeInfoMultiple[] = [];
  @Input() proposeId: number | null = null;
  @Input() proposeDetailId: number | null = null;
  idApprove: number | null = 0;
  currentUser?: User;
  proposeDTO?: ProposedCreate;
  proposeLogs = [];
  listNotifyDecision = [];
  listPromulgate = Constants.ListPromulgate;
  // fileListSelect1: NzUploadFile[] = [];
  fileListSelect2: NzUploadFile[] = [];
  fileListSelectTT: NzUploadFile[] = [];
  listFileNotifyDecision1: NzUploadFile[] = [];
  listFileNotifyDecision2: NzUploadFile[] = [];
  fileListSelectQD: NzUploadFile[] = [];
  flowCodeConstants = Constants.FlowCodeConstants;
  formGroupListConst = Constants.FormGroupList;
  formGroupDivison = Constants.FormGroupDivison;
  fileReportDV: AttachFile = {};
  fileReportQD: AttachFile = {};
  fileTCNS: AttachFile = {};

  formTTQD = new FormGroup({
    statementNumber: new FormControl(null, [Validators.required]),
    submissionDate: new FormControl(null, [Validators.required]),
    decisionNumber: new FormControl(null, [Validators.required]),
    signedDate: new FormControl(null, [Validators.required]),
    typeStatement: new FormControl(null),
    id: new FormControl(null),
  });

  // tờ trình chỉnh sửa Đơn vị
  formReportElement = new FormGroup({
    statementNumber: new FormControl(null, [Validators.required]),
    submissionDate: new FormControl(null, [Validators.required]),
    approveNote: new FormControl(null, [Validators.required]),
    typeStatement: new FormControl(null),
  });

  isSubmitFormTTQD = false;
  @Input() reasonsReject = '';
  @Input() issueLevelName = '';
  @Input() flowStatusCode = '';
  unitIssueLevel = this.translate.instant('development.proposed-unit.table.unit');
  @Input() proposeDecisionStatement?: FormTTQD;
  @Input() statusPublishCode = '';
  @Input() proposeUnit?: Proposed;
  @Input() flowStatusCodeDisplay?: string = '';
  @Input() rolesOfCurrentUser: string[] = [];
  @Output() withdraw: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() saveMultiple = new EventEmitter<boolean>();

  typeIssueCorrection = false;
  checkStatusElement = false;

  paramDetail = {
    ghiNhanyKienPd: null,
    ghiChuGiaiTrinhThem: null,
    ghiChuChuyenBanHanh: null,
    ngayKyHieuLuc: null,
    lyDohuy: null,
    chiTietLydoHuy: null,
    ngayHieuLucDaChuyen: null,
    ngayHieuLucMoi: null,
    lyDoHuyPa: null,
  };
  approveParams = {
    groupProposeType: null,
    action: null,
  };
  note = '';
  listProposeCategory = Constants.ListProposeCategory;
  functionCode = '';
  pageable: Pageable = {
    totalElements: 0,
    currentPage: 0,
    size: userConfig.pageSize,
  };
  hasDecision = true;
  disabledPresentation = true;
  viewAppletEmployee = true;
  iconStatus = Constants.IconStatus.UP;
  isExpand = false;
  screenCodeConst = Constants.ScreenCode;
  listEmployeeInfoTotal = [];
  checkFlowStatusCodeElement = false;
  rotationTimeViewMin = false;
  rotationTimeViewMax = false;
  jobDTOS: JobDTOS[] = [];
  subs: Subscription[] = [];
  proposeDetailIdSame: number[] = [];
  elementPrintReport = false;
  formProposeMultiple: FormGroup;
  listLevel = Constants.Level;
  planAlternative = Constants.PlanAlternative;
  rotationList: ProposeRotationReasons[] = [];
  jobDTOSOffer: JobDTOS[] = [];
  approvalCreate = false;
  isSeeMore = false;
  quantityMapWithdraw = new Map<number, EmployeeInfoMultiple>();
  checkedWithdrawAll = false;
  categoryProposed = Constants.ProposeCategory;
  editTCNS = false;
  viewWithdrawNoteName = false;
  handlePropose = false;
  noteKSPTNL = '';
  noteLDDV = '';
  @Input() withdrawNoteCode = '';
  checkWithdrawType = false;
  commentWithdraw = '';
  //log
  heightTable = { x: '50vw', y: '16em'};
  tableConfig!: MBTableConfig;
  pagination = new Pagination();
  @ViewChild('dateLog', {static: true}) dateLog!: TemplateRef<NzSafeAny>;
  pageNumber = 0;
  pageSize =  userConfig.pageSize;
  getReportMany = false;
  fileListSelectTTEdit: NzUploadFile[] = [];
  fileAttachEdit: AttachFile = {};
  @Input() isAcceptPropose: boolean = false;

  constructor(
    injector: Injector,
    private readonly service: ProposedUnitService,
    private readonly proposeMultipleService: ProposeMultipleEmployeeService,
    private readonly serviceProposed: ProposedUnitService
  ) {
    super(injector);
    this.functionCode = this.route.snapshot?.data['code'];
    this.handlePropose = this.route.snapshot?.data['handlePropose'];
    this.formProposeMultiple = this.fb.group({
      arrayProposeMultiple: this.fb.array([]),
    });
  }

  createFormGroups(data: EmployeeInfoMultiple[], isEdit = false, index?: number) {
    data?.forEach((item: EmployeeInfoMultiple) => {
      const formRotationCode = item.formRotationCode ? +item.formRotationCode : null;

      const contentUnit = item?.contentUnitCode ?

        {
          orgId: item?.contentUnitCode,
          orgName: item?.contentUnitName,
          pathId: item?.contentUnitPathId,
          pathResult: item?.contentUnitPathName,
          orgLevel: item?.contentUnitTreeLevel,
        } : null;

      const form = this.fb.group({
        cancelProposeRetractFlag: [item?.cancelProposeRetractFlag || null],
        contentUnit: [contentUnit || null, Validators.required],
        contentTitleCode: [item?.contentTitleCode || null, Validators.required],
        contentLevel: item?.contentLevel || null,
        strengths: [item?.proposeComment?.strengths || null, Validators.required],
        weaknesses: [item?.proposeComment?.weaknesses || null, Validators.required],
        alternativeType: item?.proposeAlternative ? item?.proposeAlternative[0].alternativeType : null,//Phương án thay thế
        replacementEmployeeCode: item?.proposeAlternative ? item?.proposeAlternative[0]?.replacementEmployeeCode : null,// Mã nhân viên thay thế
        replacementEmployeeName: item?.proposeAlternative ? item?.proposeAlternative[0]?.replacementEmployeeName : null,// Mã nhân viên thay thế
        replacementEmployeeTitleCode: item?.proposeAlternative ? item?.proposeAlternative[0]?.replacementEmployeeTitleCode : null,// Mã nhân viên thay thế
        replacementEmployeeTitleName: item?.proposeAlternative ? item?.proposeAlternative[0]?.replacementEmployeeTitleName : null,// Mã nhân viên thay thế
        replacementEmployee: (item?.proposeAlternative && ((item?.proposeAlternative[0]?.alternativeType !== undefined) && (item?.proposeAlternative[0]?.alternativeType !== null))) ? item?.proposeAlternative[0]?.replacementEmployee : null,
        //
        handoverEmployeeCode: item?.proposeAlternative ? item?.proposeAlternative[0]?.handoverEmployeeCode : null,// Mã nhân viên thay thế
        handoverEmployeeName: item?.proposeAlternative ? item?.proposeAlternative[0]?.handoverEmployeeName : null,// Mã nhân viên thay thế
        handoverEmployeeTitleCode: item?.proposeAlternative ? item?.proposeAlternative[0]?.handoverEmployeeTitleCode : null,// Mã nhân viên thay thế
        handoverEmployeeTitleName: item?.proposeAlternative ? item?.proposeAlternative[0]?.handoverEmployeeTitleName : null,// Mã nhân viên thay thế
        handoverEmployee: item?.proposeAlternative ? item?.proposeAlternative[0]?.handoverEmployee : null,// Mã nhân viên thay thế


        formName: [this.dataCommon.formGroupMultipleList?.find((i) => i.code === formRotationCode)?.formCode || null, this.formGroup === this.formGroupListConst.PV ? null : Validators.required],//Hình thức
        implementDate: [item?.implementDate || null, Validators.required],
        minimumTime: [item?.minimumTime || null, Validators.required],
        maximumTime: [item?.maximumTime || null, Validators.required],
        rotationReasonID: [item?.proposeRotationReasons?.map((i) => {
          return +i.rotationReasonID
        }) || [], Validators.required],
        isDecision: item?.isDecision || false,
        isIncome: item?.isIncome || false,
        isInsurance: item?.isInsurance || false,


      });
      if (isEdit) {
        this.arrayProposeMultiple.at(index).patchValue(form.getRawValue());
      } else {
        (this.formProposeMultiple.controls['arrayProposeMultiple'] as FormArray).push(form);
      }
    });
  }

  get arrayProposeMultiple(): NzSafeAny {
    if (this.formProposeMultiple.get('arrayProposeMultiple')) {

      return this.formProposeMultiple.get('arrayProposeMultiple') as FormArray;
    }
    return null;
  }


  ngOnChanges(): void {
    if (this.dataCommon.formGroupMultipleList?.length === 1) {
      this.formAppletEmployee.get('formCode')?.setValue(this.dataCommon.formGroupMultipleList[0]?.formCode);
    }
  }

  changeCurrentTitl(e: any) {
    if (e) {
      this.formAppletEmployee.get('currentTitle')?.disable();
    } else {
      this.formAppletEmployee.get('currentTitle')?.enable();
    }
    this.formAppletEmployee.get('currentTitle')?.setValue(null);
  }


  ngOnInit(): void {
    this.initTable();

    // Check view button app dụng cho màn sửa theo trạng thái
    this.onLoading.emit(true);

    this.viewAppletEmployee = this.checkStatusEditDelete({flowStatusCode: this.flowStatusCode});

    // Khác màn sửa thì view tt
    if (this.typeAction !== ScreenType.Create) {

      this.hasDecision = this.dataForm?.hasDecision;
      this.paramDetail.ngayHieuLucDaChuyen = this.dataForm?.proposeCorrection?.effectiveDate || '';

      if (localStorage.getItem(SessionKey.CURRENCY_USER)) {
        this.currentUser = JSON.parse(localStorage.getItem(SessionKey.CURRENCY_USER)!);
      }

      if (this.proposeDecisionStatement) {
        this.formTTQD.patchValue({
          statementNumber: this.proposeDecisionStatement?.statementNumber,
          submissionDate: this.proposeDecisionStatement?.submissionDate,
          decisionNumber: this.proposeDecisionStatement?.decisionNumber,
          signedDate: this.proposeDecisionStatement?.signedDate,
          typeStatement: this.proposeDecisionStatement?.typeStatement,
          id: this.proposeId,
        });
      }

      const statementHR1 = this.dataForm?.fileDTOList?.find((item: any) => item.type === 'statementHR1');
      if (statementHR1) {
        this.fileListSelect2.push({
          uid: statementHR1.docId,
          name: statementHR1.fileName,
          status: 'done',
          id: statementHR1.id,
        });
        this.fileTCNS.uid = statementHR1.docId;
        this.fileTCNS.fileName = statementHR1.fileName;
      }

      const fileDecision = this.dataForm?.fileDTOList?.find((item: any) => item.type === 'decision');
      if (fileDecision) {
        this.fileListSelectQD.push({
          uid: fileDecision.docId,
          name: fileDecision.fileName,
          status: 'done',
          id: fileDecision.id,
        });
        this.fileReportQD.uid = fileDecision.docId;
        this.fileReportQD.fileName = fileDecision.fileName;
      }

      const fileStatement = this.dataForm?.fileDTOList?.find((item: any) => item.type === 'statement');
      if (fileStatement) {
        this.fileListSelectTT.push({
          uid: fileStatement.docId,
          name: fileStatement.fileName,
          status: 'done',
          id: fileStatement.id,
        });
        this.fileReportDV.uid = fileStatement.docId;
        this.fileReportDV.fileName = fileStatement.fileName;
      }

      forkJoin([
        this.service.viewDetailFile(this.proposeId!, this.proposeCategory).pipe(catchError(() => of(undefined))),
        this.proposeMultipleService.listEmpPropose({
          id: this.proposeId,
          screenCode: this.screenCode
        }).pipe(catchError(() => of({data: []}))),
        this.serviceProposed.getLogsPage(this.proposeId!, this.proposeCategory, this.screenCode, this.pageNumber, this.pageSize).pipe(catchError(() => of(undefined))),
      ]).subscribe(
        ([ listNotifyDecision, listEmployeeInfo, logs]) => {
          // GET EMP
          this.listEmployeeInfoTotal = listEmployeeInfo?.data?.content || [];
          if(this.listEmployeeInfoTotal.length === 0) {
            this.router.navigateByUrl(functionUri.propose_list);
          }
          this.checkFlowStatusCodeElement = this.listEmployeeInfoTotal.filter
          ((item: ProposeDetail) => item?.flowStatusCode !== Constants.FlowCodeConstants.KSDongY).length === 0;//check flowStatusCode của element
          this.checkFlowStatusElement.emit(this.checkFlowStatusCodeElement);// truyền dữ liệu sang màn handle
          this.pageable.totalElements = this.listEmployeeInfoTotal.length;
          this.viewWithdrawNoteName = this.listEmployeeInfoTotal.filter((item: EmployeeInfoMultiple) => item.withdrawNote).length > 0;
          this.listEmployeeInfoTotal?.forEach((item: EmployeeInfoMultiple) => {
            item.flowStatusPublishName = this.dataCommon.flowStatusPublishConfigList?.find(
              (itemStatus) => item.flowStatusPublishCode === itemStatus.key
            )?.role;
            this.onItemCheckedWithdraw(item, item.cancelProposeRetractFlag!);
          });
          this.disabledPresentation =
            this.listEmployeeInfoTotal?.filter((item) => this.statusDisabledAttachPresentation(item))?.length ===
            this.listEmployeeInfo.length;
          //log
          this.proposeLogs = logs?.data?.content || [];
          this.tableConfig.total = logs?.data?.totalElements || 0;
          this.tableConfig.pageIndex = 1;
          this.pageChange(1);
          // nếu phân trang các bản ghi thành phần thì gán giá trị, đã call api listEmployeePropose
          const dataNotifyDecision = listNotifyDecision?.data;
          if (dataNotifyDecision?.length > 0) {
            this.listFileNotifyDecision2 = dataNotifyDecision[0]?.decisionSignId
              ? [
                {
                  id: dataNotifyDecision[0].id,
                  uid: dataNotifyDecision[0].decisionSignId,
                  name: dataNotifyDecision[0].decisionSignName,
                  status: 'done',
                },
              ]
              : [];

            this.listFileNotifyDecision1 =
              dataNotifyDecision?.decisionId?.length > 0
                ? [
                  {
                    id: dataNotifyDecision.id,
                    uid: dataNotifyDecision.decisionId[0],
                    name: dataNotifyDecision.decisionName[0],
                    status: 'done',
                  },
                ]
                : [];
          }
          this.displayReport = this.dataForm?.externalApprove;
          this.getReportMany = this.dataForm?.externalApprove;
          this.checkReportStatus();
          this.checkWithdrawType = this.dataForm?.withdrawType;
          const proposeStatementDetails = this.dataForm?.proposeStatementList || [];
          if (proposeStatementDetails?.length > 0) {
            proposeStatementDetails?.forEach((item: FormTTQD) => {
              if (item?.typeStatement === Constants.TypeStatement.EDITHRDV) {
                this.formReportElement.patchValue({
                  statementNumber: item?.statementNumber,
                  submissionDate: item?.submissionDate,
                  approveNote: this.dataForm?.approveNote,
                })
              }
            })
          };
          this.fileListSelectTTEdit = [];
          const fileReportEdit = this.dataForm?.fileDTOList?.find(
            (item: FileDTOList) => item.type === Constants.TypeStatementFile.STATEMENTEDIT);
          if (fileReportEdit) {
            this.fileListSelectTTEdit = [
              ...this.fileListSelectTTEdit,
              {
                uid: fileReportEdit.docId,
                name: fileReportEdit.fileName,
                status: 'done',
                id: fileReportEdit.id,
              }
            ];
            this.fileAttachEdit.uid = fileReportEdit.docId;
            this.fileAttachEdit.fileName = fileReportEdit.fileName;
          }
          this.onLoading.emit(false);
        },
        (e) => {
          this.getMessageError(e);
          this.onLoading.emit(false);
        }
      );
    } else if (this.typeAction === ScreenType.Create && this.proposeId) {
      this.getInfoEmp();
    }
  }

  downloadFile = (file: NzUploadFile) => {
    this.serviceProposed.downloadFile(+file?.uid, file?.name).subscribe(
      () => {
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  };

  checkAlternativePropose(alternativeType?: number) {
    if (alternativeType === 0) {
      return this.translate.instant('development.proposed-unit.proposeAlternative.in');
    } else if (alternativeType === 1) {
      return this.translate.instant('development.proposed-unit.proposeAlternative.out');
    } else if (alternativeType === 2) {
      return this.translate.instant('development.proposed-unit.proposeAlternative.new');
    }
  }

  emitDataStatement() {
    //nếu màn HRDV duyệt trên HCM hoặc màn PTNL k có tờ trình của TCNS thì k validate các trường này
    if ((this.screenCode === this.screenCodeConst.HRDV && this.displayReport) ||
        !(this.screenCode === this.screenCodeConst.PTNL && this.checkPTNLOrKSPNTViewTCNSNew())) {
      this.formTTQD.controls['statementNumber'].clearValidators();
      this.formTTQD.controls['statementNumber'].updateValueAndValidity();
      this.formTTQD.controls['submissionDate'].clearValidators();
      this.formTTQD.controls['submissionDate'].updateValueAndValidity();
    }
    if (this.listEmployeeInfo.length > 0) {
      if (!(this.issueLevelName === this.unitIssueLevel && !this.dataForm.isHo)
        || !this.checkLeaderApprove(this.flowStatusCode)) {
        this.formTTQD.removeControl('decisionNumber');
        this.formTTQD.removeControl('signedDate');
      }
      //khi bấm ghi nhận đồng ý thì validate các trường
      if (this.isAcceptPropose) {
        if (this.checkDisplayReportEdit()) {
          if (!this.validApproveEditReport()) {
            this.isSubmitFormTTQD = true;
            this.scrollToValidationSection('reportUnit', 'development.commonMessage.validateReportUnit');
            return;
          }
        } else {
          if (!this.formTTQD.valid && this.flowStatusCode !== this.flowCodeConstants.HANDLING6) {
            this.isSubmitFormTTQD = true;
            this.scrollToValidationSection('reportUnit', 'development.commonMessage.validateReportUnit');
            return;
          }
        }
      }
        const newForm = this.checkDisplayReportEdit()
          ? {...cleanDataForm(this.formReportElement)}
          : {...cleanDataForm(this.formTTQD)};
        newForm.hasDecision = this.hasDecision;
        if (this.screenCode === Constants.ScreenCode.HRDV) {
          newForm.externalApprove = this.displayReport;
        }
        newForm.approveNote = this.checkDisplayReportEdit()
          ? this.formReportElement.get('approveNote')?.value
          : this.reasonsReject;
        newForm.id = this.proposeId;
        return newForm;
    } else {
      this.toastrCustom.error(this.translate.instant('development.proposed-unit.messages.dontApplyStatement'));
      return null;
    }
  }

  beforeUploadDecision = (file: File | NzUploadFile): boolean => {
    const formData = new FormData();
    formData.append('proposeCategory', this.proposeCategory);
    formData.append('files', file as File);
    formData.append('id', this.proposeId!.toString());
    formData.append('type', 'DECISION');
    formData.append('documentType', '1');

    const sub = this.service.uploadFileHandle(formData).subscribe(
      (res) => {
        this.listFileNotifyDecision1 = [
          ...this.listFileNotifyDecision1,
          {
            uid: (file as NzUploadFile).uid,
            name: file.name,
            status: 'removed',
          },
        ];
      },
      (e) => {
        this.getMessageError(e);
      }
    );
    this.subs.push(sub);
    return false;
  };

  doRemoveFileDecision = (file: NzUploadFile): boolean => {
    this.isLoading = true;
    const paramDelete = {
      id: this.proposeId,
      proposeCategory: this.proposeCategory,
      documentType: 1,
      type: 'DECISION',
    };
    this.service.removeFileNotify(paramDelete).subscribe(
      (res) => {
        this.isLoading = false;
        this.listFileNotifyDecision1 = [];
      },
      (e) => {
        this.isLoading = false;
        this.getMessageError(e);
      }
    );
    return false;
  };

  checkTypeIssueTransfer() {
    const list = ['12.3A', '12.4', '10A4'];
    return list.indexOf(this.statusPublishCode) > -1;
  }

  checkTypeIssueTransfer2() {
    const list = ['10A', '12.3A', '12.4', '10A4'];
    return list.indexOf(this.statusPublishCode) > -1;
  }

  checkTypeIssueCorrection() {
    const list = ['10A1', '10A3', '11.1', '11.2', '11.1BB', '11.2BB'];
    return list.indexOf(this.statusPublishCode) > -1;
  }

  checkTypeCancelIssue() {
    const list = ['10A1', '10A3', '11.1', '11.2'];
    return list.indexOf(this.statusPublishCode) > -1;
  }

  checkTypeCancelPlan() {
    const list = ['12.3A', '12.4'];
    return list.indexOf(this.statusPublishCode) > -1;
  }

  checkLeaderApprove(status: string) {
    const list = ['1', '3'];
    return list.indexOf(status) > -1;
  }

  getReasonsReject() {
    return this.checkDisplayReportEdit()
      ? this.formReportElement.get('approveNote')?.value
      : this.reasonsReject;
  }

  showModal() {
    this.isVisible = true;
    this.titleModal = this.translate.instant('development.proposed-unit.proposeMultiple.addModal');
  }

  handleOK(_event: any) {
    this.isOKLoading = true;
  }

  hideModal() {
    this.isVisible = false;
  }

  submit() {
    const paramStatement = this.emitDataStatement();
    if (paramStatement) {
      this.isLoading = true;
      if (this.screenCode === Constants.ScreenCode.PTNL || this.screenCode === Constants.ScreenCode.KSPTNL) {
        paramStatement.typeStatement = Constants.TypeStatement.TCNS;
      } else if (this.screenCode === Constants.ScreenCode.HRDV && this.checkDisplayReportEdit()) {
        paramStatement.typeStatement = Constants.TypeStatement.EDITHRDV;
      }
      this.service.saveStatementDecision(paramStatement).subscribe(
        () => {
          this.toastrCustom.success(this.translate.instant('development.interview-config.notificationMessage.success'));
          this.saveMultiple.emit(true);
          this.isLoading = false;
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        }
      );
    } else {
      return;
    }
  }

  getReasonDesc(item: any) {
    const textReasons: string[] = [];
    const listProposeRotationReasons = item.proposeRotationReasons;
    listProposeRotationReasons?.forEach((element: any) => {
      textReasons.push(
        <string>this.dataCommon.proposeRotationReasonConfigDTOS?.find((i) => i.id === +element.rotationReasonID)?.name
      );
    });
    return textReasons.join(' , ');
  }

  editProposeItemInline(item: EmployeeInfoMultiple, index: number) {
    const formName = this.arrayProposeMultiple.at(index).get('formName').value;
    this.onChangeFormName(formName, index);
    this.listEmployeeInfo.forEach((i) => {
      if (item.id !== i.id) {
        i.isEdit = false;
      }
    })
    item.isEdit = true;
    this.onChangeUnitOffer(item.contentUnitCode!, index, item.contentTitleCode)
  }

  // xem chi tiết từng thành phần
  detailProposeItem(item: EmployeeInfoMultiple) {
    const queryParams = {
      screenCode: this.screenCode,
      id: item.proposeId,
      proposeDetailId: item.id,
      proposeCategory: Constants.ProposeCategory.ELEMENT,
      relationType: 0,
    };
    if (item?.formGroup) {
      if (+item?.formGroup === Constants.FormGroupList.TGCV && !item?.personalCode) {
        this.routerFromElement(item, Constants.SubRouterLink.DETAILTGCV);
      } else if (+item?.formGroup === Constants.FormGroupList.BO_NHIEM) {
        this.routerFromElement(item, Constants.TypeScreenHandle.DETAIL_APPOINTMENT);
      } else if ((+item?.formGroup === Constants.FormGroupList.TGCV || +item?.formGroup === Constants.FormGroupList.DCDD)
        && item?.personalCode
        && (item?.flowStatusCode === Constants.FlowCodeConstants.HANDLING5B ||
          item?.flowStatusCode === Constants.FlowCodeConstants.CHUAGUI1 ||
          item?.flowStatusCode === Constants.FlowCodeConstants.NHAP)) {
        this.router.navigate([Constants.KeyCommons.DEVELOPMENT_RECOMMEND_INDIVIDUAL_DETAIL], {
          queryParams: {
            idGroup: this.dataForm?.id,
            id: item.proposeId,
            proposeDetailId: item.id,
            proposeCategory: Constants.ProposeCategory.ELEMENT,
            screenCode: this.screenCode,
          },
          state: {backUrl: this.router.url},
          skipLocationChange: true,
        });
      } else {
        this.routerFromElement(item, Constants.SubRouterLink.DETAIL);
      }
    }
  }

  routerFromElement(item: EmployeeInfoMultiple, type: string) {
    this.sessionService.setSessionData(`${SessionKey.URL_PROPOSE_ELEMENT}_${Constants.ProposeCategory.GROUP}`, this.proposeUnit);
    const url = this.router.url;
    this.service.viewParent(item.id!).subscribe((res) => {
      this.router.navigateByUrl(Utils.getUrlByFunctionCode(this.functionCode)).then(() => {
        this.router.navigate([Utils.getUrlByFunctionCode(this.functionCode), type], {
          queryParams: {
            idGroup: this.dataForm?.id,
            id: item.proposeId,
            proposeDetailId: item.id,
            proposeCategory: Constants.ProposeCategory.ELEMENT,
            categoryParent: this.proposeCategory,
            parentID: item.isGroup || item.groupByKSPTNL ? res.data?.groupId : res.data?.parentId,
            proposeType: Constants.ProposeTypeID.ELEMENT,
            relationType: Constants.RelationType.ONE,
            proposeGroupId: this.proposeCategory === Constants.ProposeCategory.GROUP ? this.proposeId : '',
            elementPrintReport: this.elementPrintReport,
            screenCode: this.screenCode,
          },
          state: {backUrl: url},
          skipLocationChange: true,
        });
      });
    })
  }

  deleteProposeDetail(item: any) {
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.proposed-unit.messages.delete'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        this.service.deleteProposeListLD(item.id, this.listProposeCategory[2].id).subscribe(
          () => {
            this.toastrCustom.success(this.translate.instant('development.proposed-unit.messages.deleteSuccess'));
            this.arrayProposeMultiple.clear();
            this.getInfoEmp();
            this.isLoading = false;
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
      },
    });
  }

  checkReportStatus() {
    this.submitReport.emit(this.displayReport);
    if (this.displayReport) {
      return true;
    } else {
      return false;
    }
  }

  print() {
    const id = this.route.snapshot.queryParams['id'];
    this.service.printReportCard(id).subscribe((res) => {
      const url = window.URL.createObjectURL(res);
      const a = document.createElement('a');
      a.setAttribute('style', 'display:none;');
      document.body.appendChild(a);
      a.href = url;
      a.download = 'Tờ trình';
      a.click();
    });
  }

  clearIndex() {
    this.indexEdit = null;
    this.itemPropose = undefined;
  }

  showModalPropose(footerTmpl: TemplateRef<any>, sortOrder?: number) {
    this.modalRef = this.modal.create({
      nzTitle: 'Tìm kiếm nhân viên',
      nzWidth: this.getInnerWidth(),
      nzContent: ProposeMultipleEmployeeComponent,
      nzComponentParams: {
        listEmployeeModal: [],
        isEmp: false,
      },
      nzFooter: footerTmpl,
    });
    this.modalRef.afterClose.subscribe((result) => {
      const listEmployeeModal = result;
      if (listEmployeeModal?.length > 0) {
        const listCode: string[] = [];
        listEmployeeModal?.forEach((item: any) => {
          if (item?.empCode) {
            listCode.push(item.empCode);
          }
        });
        const proposeRotationReasons = Constants.ListProposeRotationReasonsNCN;
        const paramAddEmp = {
          sortOrder: sortOrder ? sortOrder : null,
          proposeId: this.proposeId,
          employeeCodes: listCode,
          proposeUnitRequest: this.screenCode === Constants.ScreenCode.HRDV ? this.proposeUnit?.proposeUnitRequest : null,
          formGroup: this.formGroup,
          proposeRotationReasons: proposeRotationReasons,
          proposeUnitRequestName: this.screenCode === Constants.ScreenCode.HRDV ? this.proposeUnit?.proposeUnitRequestName : null,
        };
        this.isLoading = true;
        this.proposeMultipleService.addEmployee(paramAddEmp, this.screenCode).subscribe(
          (res) => {
            // Khi thêm mới xong vào màn cập nhật
            if (this.typeAction === ScreenType.Create) {
              this.router.navigate([Utils.getUrlByFunctionCode(this.functionCode), 'update'], {
                queryParams: {
                  id: this.proposeId,
                  proposeType: 0,
                  proposeCategory: Constants.ProposeCategory.ORIGINAL,
                  flowStatusCode: this.screenCode === Constants.ScreenCode.HRDV ? Constants.FlowCodeConstants.CHUAGUI1 : Constants.FlowCodeConstants.NHAP,
                  relationType: true,
                },
                skipLocationChange: true,
              });
            } else {
              this.arrayProposeMultiple.clear();
              this.getInfoEmp();
            }
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
      }
    });
  }

  showModalTemplate(listFileTemplate: ListFileTemplate[]) {
    this.isLoading = false;
    const listDecision = listFileTemplate?.filter((item) => item?.typeCode === Constants.TemplateType.QD && item?.subTypeCode !== Constants.TemplateType.PLDCDD);
    const listNotDecision = listFileTemplate?.filter((item) => item?.typeCode !== Constants.TemplateType.QD || (item?.typeCode === Constants.TemplateType.QD && item?.subTypeCode === Constants.TemplateType.PLDCDD));
    if (listDecision?.length > 1) {
      this.modalRef = this.modal.create({
        nzTitle: 'Chọn tờ trình muốn xuất',
        nzWidth: 800,
        nzContent: ProposeTemplateComponent,
        nzComponentParams: {
          listDecision: listDecision
        },
        nzFooter: null,
      });
      this.modalRef.componentInstance.eventIdFile.subscribe((idFile: number) => {

        this.modalRef?.destroy();
        this.isLoading = true;
        const itemFileDecision = listDecision.find((item) => item?.id === +idFile)
        const listNewFileDecision =
          [...listNotDecision,
            itemFileDecision];
        this.dowloadAllFile(listNewFileDecision as ListFileTemplate[]);
        this.modalRef?.componentInstance.closeEvent.subscribe((data: boolean) => {
          if (!data) {
            this.modalRef?.destroy();
          }
        })
      })
    } else {
      this.isLoading = true;
      this.dowloadAllFile(listFileTemplate);
    }
  }

  dowloadAllFile(listNewFileDecision: ListFileTemplate[]) {
    const listNewTemplateCode = listNewFileDecision.map((item) => {
      return item.templateCode
    });
    this.service.updateTemplateUsed(listNewTemplateCode).subscribe();
    listNewFileDecision?.forEach((item) => {
      this.service.callBaseUrl(item?.linkTemplate!).subscribe(
        (data) => {
          const contentDisposition = data?.headers.get('content-disposition');
          const filename = contentDisposition?.split(';')[1]?.split('filename')[1]?.split('=')[1]?.trim();
          const blob = new Blob([data.body], {
            type: 'application/octet-stream',
          });
          saveAs(blob, filename);
          this.isLoading = false;
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        }
      );
    });
  }

  pageChange(page: number) {
    if (this.isLoading) {
      return;
    }
    this.pageable.currentPage = page - 1;
    const start: number = this.pageable.currentPage * this.pageable.size!;
    this.listEmployeeInfo = this.listEmployeeInfoTotal.slice(start, this.pageable.size! + start);
    this.listEmployeeInfo = this.listEmployeeInfo.map((item, index) => {
      return {
        ...item,
        alternativeTypeCheck: item?.proposeAlternative ? item?.proposeAlternative[0]?.alternativeType === 0 : false
      }
    })
    this.arrayProposeMultiple.clear();
    this.createFormGroups(this.listEmployeeInfo);
  }

  getInfoEmp() {
    this.isLoading = true;
    // nếu chốt phân trang thì bỏ pageable.size * 10, chỉ để pageable.size
    this.proposeMultipleService
      .listEmpPropose({id: this.proposeId, screenCode: this.screenCode})
      .pipe(catchError(() => of({data: []})))
      .subscribe((res) => {
        this.listEmployeeInfoTotal = res?.data?.content || [];
        if(this.listEmployeeInfoTotal.length === 0 && this.typeAction !== ScreenType.Create) {
          switch (this.screenCode) {
            case this.screenCodeConst.PTNL:
              return (
                this.router.navigateByUrl(functionUri.propose_list)
              );
            case this.screenCodeConst.KSPTNL:
              return (
                this.router.navigateByUrl(functionUri.approvement_propose)
              );
            case this.screenCodeConst.HRDV:
              return (
                this.router.navigateByUrl(functionUri.propose_unit)
              );
            default:
              return this.back();
          }

        }
        this.viewWithdrawNoteName = this.listEmployeeInfoTotal.filter((item: EmployeeInfoMultiple) => !item.withdrawNote).length > 0;
        this.pageable.totalElements = this.listEmployeeInfoTotal.length;
        this.isLoading = false;
        this.pageChange(1);
      });
  }

  getInnerWidth(): number {
    if (window.innerWidth > 767) {
      return window.innerWidth / 1.5 > 1368 ? 1368 : window.innerWidth / 1.3;
    }
    return window.innerWidth;
  }

  getFormGroup(value: number) {
    this.formGroup = value;
    if (this.formGroup === this.formGroupListConst.PV) {
      this.formAppletEmployee.get('formCode')?.clearValidators();
    } else {
      this.formAppletEmployee.get('formCode')?.addValidators(Validators.required);
    }
    this.formAppletEmployee.get('formCode')?.updateValueAndValidity();
  }

  applyEmployee() {
    // Check length
    const dataForm = this.formAppletEmployee.getRawValue();
    if (this.formGroup === Constants.FormGroupList.DCLEVEL) {
      this.formAppletEmployee.get('currentUnit')?.clearValidators();
      this.formAppletEmployee.get('currentUnit')?.updateValueAndValidity();
      this.formAppletEmployee.get('currentTitle')?.clearValidators();
      this.formAppletEmployee.get('currentTitle')?.updateValueAndValidity();
    }
    if (this.listEmployeeInfo.length === 0) {
      this.toastrCustom.error(this.translate.instant('development.proposed-unit.messages.dontApplyEmp'));
      return;
    }
    if (!this.formAppletEmployee.valid) {
      this.isSubmitted = true;
      return;
    }
    const minimumTime = this.formAppletEmployee.get('minimumTime')?.value;
    const maximumTime = this.formAppletEmployee.get('maximumTime')?.value
    if (minimumTime && maximumTime && minimumTime > maximumTime) {
      this.toastrCustom.error(this.translate.instant('development.commonMessage.E006'));
      return;
    }

    if (dataForm.sameCurrentTitle && this.jobDTOS.length === 0) {
      this.toastrCustom.error(this.translate.instant('development.commonMessage.E002'));
      return;
    }
    // kiểm tra các bản ghi không cùng chức danh tại đơn vị đích
    const proposeCode = this.checkSameContentTitle();
    if (dataForm.sameCurrentTitle && proposeCode.length > 0) {
      this.toastrCustom.error(
        this.translate.instant('development.commonMessage.E003', {codes: proposeCode.join(', ')})
      );
    }
    this.isLoading = true;
    const itemFormCode: any = this.dataCommon.formGroupMultipleList?.find((item: any) => item.formCode === dataForm.formCode);
    const itemJobDTO: any = this.jobDTOS.find((item: any) => item.jobId === dataForm?.currentTitle);
    const paramApplet = {
      proposeDetailId: dataForm.sameCurrentTitle ? this.proposeDetailIdSame : [],
      proposeId: this.proposeId,
      formCode: itemFormCode?.code,
      formType: itemFormCode?.formCode,
      unitId: dataForm?.currentUnit?.orgId || null,
      unitName: dataForm?.currentUnit?.orgName || null,
      orgLevel: dataForm?.currentUnit?.orgLevel || null,
      pathId: dataForm?.currentUnit?.pathId || null,
      pathResult: dataForm?.currentUnit?.pathResult || null,
      jobId: itemJobDTO?.jobId || null,
      jobName: itemJobDTO?.jobName || null,
      formRotationTitle: dataForm.sameCurrentTitle,
      implementDate: this.formAppletEmployee.get('implementDate')?.value,
      minimumTime: this.formAppletEmployee.get('minimumTime')?.value,
      maximumTime: this.formAppletEmployee.get('maximumTime')?.value,
    };
    this.proposeMultipleService.appletEmployee(paramApplet, this.screenCode).subscribe(
      (res) => {
        this.isLoading = false;
        this.loadProposedUpdate.emit();
        this.arrayProposeMultiple.clear();
        this.getInfoEmp();
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

  checkSameContentTitle() {
    const proposeCode: number[] = [];
    this.listEmployeeInfoTotal.forEach((item: any) => {
      if (!this.jobDTOS?.find((itemTitle) => itemTitle.jobId === item?.employee?.jobId)) {
        proposeCode.push(item.id);
      } else {
        this.proposeDetailIdSame.push(item.id);
      }
    });
    return proposeCode;
  }

  checkIdParams() {
    if (this.proposeCategory === Constants.ProposeCategory.GROUP) {
      return this.proposeId;
    } else if (this.proposeCategory === Constants.ProposeCategory.ORIGINAL) {
      return this.proposeId;
    } else if (this.proposeCategory === Constants.ProposeCategory.ELEMENT) {
      return this.proposeDetailId;
    }
    return null;
  }

  checkTypeKSPTNL() {
    switch (this.proposeCategory) {
      case Constants.ProposeCategory.GROUP:
        return 0;
      case Constants.ProposeCategory.ORIGINAL:
        return 1;
      case Constants.ProposeCategory.ELEMENT:
        return 2;
      default:
        return null;
    }
  }


  //duyệt đề xuất PTNL
  approvePTNL(action: number) {
    if (this.checkPTNLOrKSPNTViewTCNSNew()) {
      this.isSubmitFormTTQD = true;
      this.formTTQD.get('decisionNumber')?.clearValidators();
      this.formTTQD.get('decisionNumber')?.updateValueAndValidity();
      this.formTTQD.get('signedDate')?.clearValidators();
      this.formTTQD.get('signedDate')?.updateValueAndValidity();
      if (this.formTTQD.invalid || this.fileListSelect2?.length === 0) {
        this.scrollToValidationSection('TCNSReport', 'development.commonMessage.validateRecordApprove3');
        return;
      }
    }
    this.idApprove = this.checkIdParams();
    const newApprove = {...cleanData(this.approveParams)};
    newApprove.groupProposeType = this.checkTypeKSPTNL();
    newApprove.action = action;
    let title = '';
    let success = '';
    if (action !== 0 && !this.paramDetail.ghiNhanyKienPd) {
      this.toastrCustom.error(this.translate.instant('development.commonMessage.ideaRecordValidate'));
      return;
    }
    if (action === 0) {
      title = this.translate.instant('development.approvement-propose.messages.approvedConfirm');
      success = this.translate.instant('development.approvement-propose.messages.recordSuccess');
    } else if (action === 1) {
      title = this.translate.instant('development.approvement-propose.messages.rejectConfirm');
      success = this.translate.instant('development.approvement-propose.messages.recordRejectSuccess');
    } else {
      title = this.translate.instant('development.approvement-propose.messages.otherIdeaConfirm');
      success = this.translate.instant('development.approvement-propose.messages.recordOtherIdeaSuccess');
    }
    this.modalRef = this.modal.confirm({
      nzTitle: title,
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        if (this.checkPTNLOrKSPNTViewTCNSNew()) {
          this.submit();
        }
        this.service.approvePTNL(this.idApprove, this.paramDetail.ghiNhanyKienPd, newApprove).subscribe(
          () => {
            this.router.navigateByUrl(Utils.getUrlByFunctionCode(this.functionCode));
            this.isLoading = false;
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
      },
    });
  }

  //check hiển thị 3 nút ghi nhận đồng ý, k đồng ý vs ý kiến khác
  checkApproveThreeButton() {
    const elementApprove = this.listEmployeeInfoTotal.filter(
      (item: EmployeeInfoMultiple) => item?.flowStatusCode === this.flowCodeConstants.KSDongY
    );
    const checkUserCreate = this.currentUser?.username === this.dataForm?.createdBy;
    return ((this.screenCode === this.screenCodeConst.KSPTNL
          && this.dataForm?.groupByKSPTNL
          && checkUserCreate)
        || (this.screenCode === this.screenCodeConst.PTNL && checkUserCreate)) &&
      elementApprove?.length > 0 && this.typeAction !== this.screenType.create;
  }

  checkAttachReportApprove() {
    const checkUserCreate = this.currentUser?.username === this.dataForm?.createdBy;
    return this.flowStatusCode === Constants.FlowCodeConstants.KSDongY && checkUserCreate
  }

  //chuyển ban hành
  issueTransfer() {
    if (this.statusPublishCode === Constants.FlowCodeConstants.DongY10A) {
      if (!this.paramDetail.ngayKyHieuLuc) {
        this.toastrCustom.error(this.translate.instant('development.commonMessage.effectiveDateValidate'));
        return;
      }
    } else {
      if (!this.paramDetail.ghiChuChuyenBanHanh || !this.paramDetail.ngayKyHieuLuc) {
        this.toastrCustom.error(this.translate.instant('development.commonMessage.changeIssueManyRequired'));
        return;
      }
    }
    const params: IssueTransfer = {
      id: <number>this.proposeId,
      proposeCategory: this.proposeCategory,
      screenCode: this.screenCode,
    };
    const checkDate = moment(new Date()).isAfter(new Date(this.paramDetail?.ngayKyHieuLuc));
    if (checkDate && !this.paramDetail?.ngayKyHieuLuc) {
      this.toastrCustom.warning(this.translate.instant('development.commonMessage.effectiveDateValidateNow'));
    }
    const body = {
      proposeId: this.proposeId,
      effectiveDate: this.paramDetail.ngayKyHieuLuc,
      commentsIssueTransfer: this.paramDetail.ghiChuChuyenBanHanh,
    };
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.proposed-unit.messages.changeIssue'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        const sub = this.service.issueTransfer(body, params).subscribe(
          () => {
            this.toastrCustom.success(this.translate.instant('development.proposed-unit.messages.changeIssueSuccess'));
            this.router.navigateByUrl(Utils.getUrlByFunctionCode(this.functionCode));
            this.isLoading = false;
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
        this.subs.push(sub);
      },
    });
  }

  //hiệu chỉnh ban hành
  issueCorrection() {
    const params: IssueTransfer = {
      id: <number>this.proposeId,
      proposeCategory: this.proposeCategory,
      screenCode: this.screenCode,
    };
    const body = {
      proposeId: this.proposeId,
      effectiveDate: this.paramDetail.ngayHieuLucMoi,
      reasonOrCorrection: this.paramDetail.lyDohuy,
      viewDetailReasonOrCorrection: this.paramDetail.chiTietLydoHuy,
    };
    this.isLoading = true;
    const sub = this.service.issueCorrection(body, params).subscribe(
      () => {
        this.toastrCustom.success(this.translate.instant('development.proposed-unit.messages.issueCorrectionSuccess'));
        this.router.navigateByUrl(Utils.getUrlByFunctionCode(this.functionCode));
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }

  //yêu cầu hủy ban hành
  requestCancelation() {
    const params: IssueTransfer = {
      id: <number>this.proposeId,
      proposeCategory: this.proposeCategory,
      screenCode: this.screenCode,
    };
    const body = {
      reasonOrCorrection: this.paramDetail.lyDohuy,
      viewDetailReasonOrCorrection: this.paramDetail.chiTietLydoHuy,
    };
    if (
      (body.reasonOrCorrection !== true && body.reasonOrCorrection !== false) ||
      !body.viewDetailReasonOrCorrection
    ) {
      this.toastrCustom.error(this.translate.instant('development.commonMessage.validateRequestCancelation'));
      return;
    }
    this.modalRef = this.modal.confirm({
      nzTitle: this.translate.instant('development.proposed-unit.messages.cancelIssue'),
      nzCancelText: this.translate.instant(Constants.KeyCommons.CANCEL_TEXT),
      nzOkText: this.translate.instant(Constants.KeyCommons.OK_TEXT),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.onLoading.emit(true);
        const sub = this.service.requestCancelation(params, body).subscribe(
          () => {
            this.toastrCustom.success(this.translate.instant('development.proposed-unit.messages.cancelIssueSuccess'));
            this.router.navigateByUrl(Utils.getUrlByFunctionCode(this.functionCode));
            this.onLoading.emit(false);
          },
          (e) => {
            this.getMessageError(e);
            this.onLoading.emit(false);
          }
        );
        this.subs.push(sub);
      },
    });
  }


  doRemoveFile2 = (file: NzUploadFile): boolean => {
    this.isLoading = true;
    const idDelete = <number>this.fileListSelect2.find((item) => item.uid === file.uid)?.['id'];
    const sub = this.service.deleteUploadFile(+idDelete).subscribe(
      (res) => {
        this.isLoading = false;
        this.fileListSelect2 = this.fileListSelect2.filter((item) => item['id'] !== idDelete);
      },
      (e) => {
        this.isLoading = false;
        this.getMessageError(e);
      }
    );
    this.subs.push(sub);
    return false;
  };

  beforeUpload2 = (file: File | NzUploadFile): boolean => {
    this.isLoading = true;
    const formData = new FormData();
    formData.append('files', file as File);
    formData.append('proposeDetailId', new Blob([JSON.stringify(this.proposeId)], {type: Constants.CONTENT_TYPE.APPLICATION_JSON}));
    formData.append('type', new Blob(['statementHR1'], {type: Constants.CONTENT_TYPE.APPLICATION_JSON}));
    formData.append('groupProposeTypeId', new Blob(['0'], {type: Constants.CONTENT_TYPE.APPLICATION_JSON}));
    formData.append('isIn', new Blob(['true'], {type: Constants.CONTENT_TYPE.APPLICATION_JSON}));
    const sub = this.service.uploadFile(formData).subscribe(
      (res) => {
        this.fileListSelect2 = [
          ...this.fileListSelect2,
          {
            uid: res.data?.docId,
            name: res.data?.fileName,
            status: 'done',
            id: res.data?.id,
          },
        ];
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
    return false;
  };

  beforeUploadTT = (file: File | NzUploadFile): boolean => {
    this.isLoading = true;
    const formData = new FormData();
    formData.append('files', file as File);
    formData.append('proposeDetailId', new Blob([JSON.stringify(this.proposeId)], {type: Constants.CONTENT_TYPE.APPLICATION_JSON}));
    formData.append('type', new Blob(['statement'], {type: Constants.CONTENT_TYPE.APPLICATION_JSON}));
    formData.append('groupProposeTypeId', new Blob(['0'], {type: Constants.CONTENT_TYPE.APPLICATION_JSON}));
    formData.append('isIn', new Blob(['true'], {type: Constants.CONTENT_TYPE.APPLICATION_JSON}));
    const sub = this.service.uploadFile(formData).subscribe(
      (res) => {
        this.fileListSelectTT = [
          ...this.fileListSelectTT,
          {
            uid: res.data?.docId,
            name: res.data?.fileName,
            status: 'done',
            id: res.data?.id,
          },
        ];
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
    return false;
  };

  doRemoveFileTT = (file: NzUploadFile): boolean => {
    this.isLoading = true;
    const idDelete = <number>this.fileListSelectTT.find((item) => item.uid === file.uid)?.['id'];
    const sub = this.service.deleteUploadFile(+idDelete).subscribe(
      (res) => {
        this.isLoading = false;
        this.fileListSelectTT = this.fileListSelectTT.filter((item) => item['id'] !== idDelete);
      },
      (e) => {
        this.isLoading = false;
        this.getMessageError(e);
      }
    );
    this.subs.push(sub);
    return false;
  };


  checkStatusHandle(item: EmployeeInfoMultiple) {
    if (this.typeAction !== ScreenType.Create && item.flowStatusCode && this.screenCode !== Constants.ScreenCode.HRDV) {
      return (this.typeAction === ScreenType.Update && Constants.ListHandleStatusMany.indexOf(item.flowStatusCode) > -1) || (this.typeAction === ScreenType.Detail && Constants.ViewButtonHandleRemove.includes(item.flowStatusCode));
    }
    return false;
  }

  checkStatusEditDelete(item: EmployeeInfoMultiple) {
    if (this.typeAction !== ScreenType.Create) {
      if (this.screenCode === Constants.ScreenCode.PTNL) {
        return (
          item?.flowStatusCode === Constants.FlowCodeConstants.HANDLING6
          && this.currentUser?.username === item?.createdBy
        ) || item?.flowStatusCode === Constants.FlowCodeConstants.NHAP;
      } else if (this.screenCode === Constants.ScreenCode.HRDV) {
        return item?.flowStatusCode === Constants.FlowCodeConstants.CHUAGUI1 && this.typeAction !== ScreenType.Detail;
      }
    } else {
      return true;
    }
    return false;
  }

  viewTextStatusApprove(codeStatus: string) {
    return this.dataCommon.flowStatusApproveConfigList?.find((item) => item.key === codeStatus)?.role || '';
  }

  // Hàm xử lý 1 thành phần trong GROUP
  handle(item: EmployeeInfoMultiple) {
    const param = {
      id: item.id,
      proposeType: 2,
    };
    if (param.proposeType !== 0) {
      this.isLoading = true;
      const sub = this.service.proposalProcessing(param).subscribe(
        (res) => {
          this.isLoading = false;
          const queryParams = {
            screenCode: this.screenCode,
            id: item.proposeId,
            proposeDetailId: item.id,
            proposeCategory: Constants.ProposeCategory.ELEMENT,
            relationType: 0,
          };
          if (item?.formGroup) {
            if (+item?.formGroup === Constants.FormGroupList.TGCV && !item?.personalCode) {
              this.router.navigate([Utils.getUrlByFunctionCode(this.functionCode),
                Constants.TypeScreenHandle.HANDLE_TGCV], {
                queryParams: queryParams,
                state: {backUrl: this.router.url},
                skipLocationChange: true,
              });
            } else if (+item?.formGroup === Constants.FormGroupList.BO_NHIEM) {
              this.router.navigate([Utils.getUrlByFunctionCode(this.functionCode), 'handle-appointment-renewal'], {
                queryParams: queryParams,
                state: {backUrl: this.router.url},
                skipLocationChange: true,
              });

            } else {
              this.router.navigate([Utils.getUrlByFunctionCode(this.functionCode), 'handle', 'element-of-group'], {
                queryParams: queryParams,
                state: {backUrl: this.router.url},
                skipLocationChange: true,
              });
            }
          }
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        }
      );
      this.subs.push(sub);
    }
  }

  removeTheLump(item: EmployeeInfoMultiple) {
    this.isLoading = true;
    this.service.removeTheLump(this.proposeId!, item.id!, this.screenCode).subscribe(
      () => {
        this.toastrCustom.success(this.translate.instant('development.commonMessage.success'));
        this.arrayProposeMultiple.clear();
        this.getInfoEmp();
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

  checkStatusTheLump(item: EmployeeInfoMultiple) {
    if (this.screenCode === Constants.ScreenCode.KSPTNL) {
      return (+item.flowStatusCode === Constants.EmployeeInfoMultipleFlowStatus.flowStatus7 && item.groupByKSPTNL)
        && this.dataForm?.groupProposeTypeId !== 1;
    } else if (this.screenCode === Constants.ScreenCode.PTNL) {
      return ((this.typeAction === ScreenType.Detail && Constants.ViewButtonHandleRemove.includes(item.flowStatusCode))
          || (this.typeAction === ScreenType.Update
            && (Constants.ListStatusTheLump.indexOf(item.flowStatusCode) > -1
              || item.flowStatusCode === Constants.FlowCodeConstants.STATUS3A)))
        && this.dataForm?.groupProposeTypeId !== 1;
    } else {
      return false;
    }
  }

  checkProposeCode() {
    if (this.proposeCategory === Constants.ProposeCategory.ELEMENT) {
      return 2;
    } else if (this.proposeCategory === Constants.ProposeCategory.ORIGINAL) {
      return 1;
    } else if (this.proposeCategory === Constants.ProposeCategory.GROUP) {
      return 0;
    }
    return null;
  }

  dowloadStatement(action: number) {
    const params = {
      groupProposeType: this.checkProposeCode(),
      action: action,
      screenCode: this.screenCode,
    };
    this.isLoading = true;
    let listFile = [];
    const sub = this.service.exportReportDecision(this.proposeId!, params).subscribe(
      (res) => {
        listFile = res?.data;
        if (!listFile || listFile?.length === 0) {
          this.toastrCustom.error('Không có file nào!');
          this.isLoading = false;
        } else {
          this.showModalTemplate(listFile as ListFileTemplate[])
        }
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }

  statusDisabledAttachPresentation(item: any) {
    const listDisabledAttach = ['6', '8', '10B1', '10C'];
    return listDisabledAttach.indexOf(item.flowStatusPublishCode) > -1;
  }

  expandLog() {
    this.isExpand = !this.isExpand;
    if (this.isExpand) {
      this.iconStatus = Constants.IconStatus.DOWN;
    } else {
      this.iconStatus = Constants.IconStatus.UP;
    }
  }

  checkViewButtonTTTCNS() {
    return !(
      this.screenCode === this.screenCodeConst.KSPTNL ||
      this.typeAction === ScreenType.Detail ||
      this.isScreenHandle
    );
  }

  checkViewDigitalUpload() {
    return this.currentUser?.username === this.dataForm?.createdBy && this.flowStatusCode === this.flowCodeConstants.KSDongY &&
      ((this.screenCode === this.screenCodeConst.PTNL && this.dataForm.proposeDetails[0]?.isGroup) ||
        (this.screenCode === this.screenCodeConst.KSPTNL && this.dataForm.proposeDetails[0]?.groupByKSPTNL));
  }

  checkPTNLOrKSPNTViewTCNSTable() {
    this.elementPrintReport = this.typeAction !== ScreenType.Create &&
      (this.screenCode === this.screenCodeConst.PTNL || this.screenCode === this.screenCodeConst.KSPTNL) &&
      !this.flowStatusCode?.startsWith('5')
    return this.elementPrintReport;
  }

  checkPTNLOrKSPNTViewTCNS() {
    const editViewTCNS = this.dataForm?.groupProposeTypeId === Constants.ProposeTypeID.GROUP
      && this.typeAction === ScreenType.Detail && (this.screenCode === this.screenCodeConst.PTNL || this.screenCode === this.screenCodeConst.KSPTNL)
      && this.currentUser?.username === this.dataForm?.createdBy;
    this.editTCNS = editViewTCNS && this.flowStatusCode === this.flowCodeConstants.KSDongY;
    if (this.editTCNS) {
      this.formTTQD.disable();
    }
    return editViewTCNS && this.statusPublishCode;
  }

  checkPTNLOrKSPNTViewTCNSNew() {
    return this.dataForm?.groupProposeTypeId === Constants.ProposeTypeID.GROUP
      && this.typeAction === ScreenType.Detail
      && (this.screenCode === this.screenCodeConst.PTNL || this.screenCode === this.screenCodeConst.KSPTNL)
      && (this.statusPublishCode || this.flowStatusCode === Constants.FlowCodeConstants.KSDongY)
      && ((this.dataForm?.groupByKSPTNL && this.screenCode === Constants.ScreenCode.KSPTNL)
        || (!this.dataForm?.groupByKSPTNL && this.dataForm?.isGroup && this.screenCode === Constants.ScreenCode.PTNL)
        || this.checkDataTCNSReport());
  }

  //check dữ liệu tờ trình TCNS
  checkDataTCNSReport() {
    return this.formTTQD.get('statementNumber')?.value ||
      this.formTTQD.get('submissionDate')?.value ||
      this.fileListSelect2?.length > 0
  }

  disableSignDate = (dateVal: Date) => {
    if (this.dataForm.proposeDetails[0].rotationFromDate) {
      const rotationFromDate = new Date(this.dataForm.proposeDetails[0].rotationFromDate).getTime();
      return dateVal.valueOf() > rotationFromDate;
    }
    return false;
  };

  // Upload file
  beforeUploadQD = (file: File | NzUploadFile): boolean => {
    const formData = new FormData();
    formData.append('files', file as File);
    formData.append('proposeDetailId', new Blob([JSON.stringify(this.proposeId)], {type: Constants.CONTENT_TYPE.APPLICATION_JSON}));
    formData.append('type', new Blob(['decision'], {type: Constants.CONTENT_TYPE.APPLICATION_JSON}));
    formData.append('groupProposeTypeId', new Blob([JSON.stringify(0)], {type: Constants.CONTENT_TYPE.APPLICATION_JSON}));
    formData.append('isIn', new Blob([JSON.stringify(this.displayReport)], {type: Constants.CONTENT_TYPE.APPLICATION_JSON}));

    const sub = this.service.uploadFile(formData).subscribe(
      (res) => {
        this.fileListSelectQD = [
          ...this.fileListSelectQD,
          {
            uid: res.data?.docId,
            name: res.data?.fileName,
            status: 'done',
            id: res.data?.id,
          },
        ];
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
    return false;
  };

  doRemoveFileQD = (file: NzUploadFile): boolean => {
    this.isLoading = true;
    const idDelete = <number>this.fileListSelectQD.find((item) => item.uid === file.uid)?.['id'];
    const sub = this.service.deleteUploadFile(+idDelete).subscribe(
      (res) => {
        this.isLoading = false;
        this.fileListSelectQD = this.fileListSelectQD.filter((item) => item['id'] !== idDelete);
      },
      (e) => {
        this.isLoading = false;
        this.getMessageError(e);
      }
    );
    this.subs.push(sub);
    return false;
  };

  checkRadioIsHCM() {
    return (
      this.screenCode === this.screenCodeConst.HRDV &&
      ((this.issueLevelName !== this.unitIssueLevel)
        || (this.issueLevelName === this.unitIssueLevel && this.dataForm?.isHo))
    );
  }

  onChangeFormCode(e: FormGroupMultipleList) {
    const item = this.dataCommon.formGroupMultipleList?.find((itemformGroupMultiple) => itemformGroupMultiple.formCode === e);
    this.rotationTimeViewMax = item?.showMaxTime ? item?.showMaxTime : false;
    this.rotationTimeViewMin = item?.showMinTime ? item?.showMinTime : false;
    if (item?.formGroup === this.formGroupListConst.DCLEVEL.toString()) {
      this.formAppletEmployee.get('currentUnit')?.clearValidators();
      this.formAppletEmployee.get('currentTitle')?.clearValidators();
      this.formAppletEmployee.get('sameCurrentTitle')?.clearValidators();

    } else {
      this.formAppletEmployee.get('currentUnit')?.addValidators(Validators.required);
      this.formAppletEmployee.get('currentTitle')?.addValidators(Validators.required);
      this.formAppletEmployee.get('sameCurrentTitle')?.addValidators(Validators.required);
    }
    this.formAppletEmployee.get('currentUnit')?.updateValueAndValidity();
    this.formAppletEmployee.get('currentTitle')?.updateValueAndValidity();
    this.formAppletEmployee.get('sameCurrentTitle')?.updateValueAndValidity();
  }


  onChangeUnit(orgId: string) {
    this.jobDTOS = [];
    this.formAppletEmployee.get('currentTitle')?.setValue(null);
    if (orgId) {
      this.isLoading = true;
      const paramPosition: ParamSearchPosition = {
        orgId: orgId,
        jobId: '',
        status: 1,
        type: Constants.TYPE_SEARCH_POSITION,
        page: 0,
        size: maxInt32,
      };
      const sub = this.service.getPositionByUnit(paramPosition).subscribe(
        (res) => {
          const listPositionParent = res?.data?.content;
          this.jobDTOS = listPositionParent.map((item: any) => {
            return {jobId: item.jobId, jobName: item.jobName};
          });
          this.isLoading = false;
        },
        (e) => {
          this.isLoading = false;
        }
      );
      this.subs.push(sub);
    }
  }

  override ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
    this.modalRef?.destroy();
  }

  beforeUploadSign = (file: File | NzUploadFile): boolean => {
    const formData = new FormData();
    formData.append('proposeCategory', this.proposeCategory);
    formData.append('files', file as File);
    formData.append(
      'id',
      this.proposeCategory === 'ELEMENT' ? this.proposeDetailId!.toString() : this.proposeId!.toString()
    );
    formData.append('type', 'DECISION_SIGN');
    formData.append('documentType', '1');

    this.serviceProposed.uploadFileHandle(formData).subscribe(
      (res) => {
        this.listFileNotifyDecision2 = [
          ...this.listFileNotifyDecision2,
          {
            uid: res.data[0]?.decisionSignId,
            name: res.data[0]?.decisionSignName,
            status: 'done',
            id: res.data[0]?.id,
          },
        ];
      },
      (e) => {
        this.getMessageError(e);
      }
    );
    return false;
  };

  doRemoveFileSign = (file: NzUploadFile): boolean => {
    if (this.typeAction === ScreenType.Detail) {
      this.isLoading = true;
      const paramDelete = {
        id: this.proposeId,
        proposeCategory: this.proposeCategory,
        documentType: 1,
        type: 'DECISION_SIGN',
        docId: file.uid,
        fileName: file.name,
      };
      const sub = this.serviceProposed.removeFileNotify(paramDelete).subscribe(
        (res) => {
          this.listFileNotifyDecision2 = [];
          this.isLoading = false;
        },
        (e) => {
          this.isLoading = false;
          this.getMessageError(e);
        }
      );
      this.subs.push(sub);
    }
    return false;
  };

  checkBtnUpload() {
    return (
      (this.dataForm?.groupByKSPTNL && this.screenCode === this.screenCodeConst.KSPTNL) ||
      (this.dataForm?.isGroup && this.screenCode === this.screenCodeConst.PTNL)
    );
  }

  checkAttachApprove() {
    const elementApprove = this.listEmployeeInfoTotal.filter(
      (item: any) => item?.flowStatusCode !== this.flowCodeConstants.KSDongY
    );
    return elementApprove.length === 0
  }

  showModalWithdraw(typeButton: number) {
    //0 là rút đề xuất, 1 là rút thành phần
    //ids là id của cha
    //idFlagComponent là id của thành phần có tick hủy đề xuất
    const ids: number[] = [];
    ids.push(this.proposeId!);
    this.modalRef = this.modal.create({
      nzTitle: this.translate.instant('development.proposed-unit.proposeMultiple.withdrawPropose'),
      nzWidth: '40vw',
      nzContent: WithdrawProposeComponent,
      nzComponentParams: {
        typeWithdraw: typeButton,
        ids: ids,
        idFlagComponent: this.getListOfIDComponent(),
        isApproveHCMBefore: this.dataForm?.externalApprove,
      },
      nzFooter: null,
    });
    this.modalRef.componentInstance.closeEvent.subscribe((data: boolean) => {
      if (data) {
        this.isLoading = true;
        this.modalRef?.componentInstance.sendBody.subscribe((body: WithdrawPropose) => {
          const sub = this.service.withdrawPropose(body).subscribe(() => {
              this.toastrCustom.success(
                this.translate.instant('development.withdrawPropose.withdrawProposeSuccess'));
              this.arrayProposeMultiple.clear();
              this.getInfoEmp();
              this.quantityMapWithdraw.clear();
              this.withdraw.emit(true);
              this.isLoading = false;
            },
            (e) => {
              this.getMessageError(e);
              this.isLoading = false;
            });
          this.subs.push(sub);
          this.modalRef?.destroy();
        });
      } else {
        this.modalRef?.destroy();
      }
    });
  }

  getListOfIDComponent() {
    const idList: number[] = [];
    this.quantityMapWithdraw.forEach((values) => {
      idList.push(values.id!);
    })
    return idList;
  }

  //check hiển thị nút rút thành phần
  checkWithdrawElement() {
    return (this.flowStatusCode === Constants.FlowCodeConstants.HANDLING5B ||
        this.flowStatusCode === Constants.FlowCodeConstants.HANDLING6) &&
      !this.checkDisplayButtonWithdrawAll() &&
      this.typeAction === this.screenType.detail;
  }

  //check hiển thị nút rút đề xuất (cả lô)
  checkWithdrawPropose() {
    return (this.flowStatusCode === Constants.FlowCodeConstants.HANDLING5B ||
        this.flowStatusCode === Constants.FlowCodeConstants.HANDLING6) &&
      this.checkDisplayButtonWithdrawAll() &&
      this.listEmployeeInfoTotal.filter(
        (item: ProposeDetail) =>
          Constants.ChangeStatusWithdraw.indexOf(item.flowStatusCode!) <= -1).length > 0;
  }

  //check 2 nút rút hủy đề xuất trên bảng chi tiết
  checkChangeStatusWithdraw(item: EmployeeInfoMultiple) {
    return Constants.ChangeStatusWithdraw.indexOf(item.flowStatusCode!) <= -1
      && this.typeAction === this.screenType.detail;
  }

  checkColumCancelPropose() {
    const validStatusWithdraw = this.listEmployeeInfoTotal.filter(
      (item: ProposeDetail) =>
        Constants.ChangeStatusWithdraw.indexOf(item.flowStatusCode!) <= -1)?.length > 0;
    // số bản ghi có cờ(đã được rút)
    const numOfRetractFlag = this.listEmployeeInfoTotal.filter(
      (item: ProposeDetail) =>
        item.cancelProposeRetractFlag
    )?.length;
    //chỉ hiển thị cột hủy đề xuất khi được rút thành phần
    //rút cả lô không hiển thị
    return this.screenCode === this.screenCodeConst.HRDV
      ? (validStatusWithdraw && numOfRetractFlag < this.listEmployeeInfoTotal?.length)
      : (validStatusWithdraw && numOfRetractFlag > 0
        && numOfRetractFlag < this.listEmployeeInfoTotal?.length
        && !this.dataForm?.optionNote);
  }

  changeStatusWithdraw(proposeDetailId: number, flag: boolean) {
    this.isLoading = true;
    const sub = this.service.changeStatusWithdraw(proposeDetailId).subscribe((status) => {
        if (!flag) {
          this.toastrCustom.success(this.translate.instant('development.withdrawPropose.withdrawCancelSuccess'));
        } else {
          this.toastrCustom.success(this.translate.instant('development.withdrawPropose.withdrawCancelSuccess2'));
        }
        this.arrayProposeMultiple.clear();
        this.getInfoEmp();
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      });
    this.subs.push(sub);
  }


  showModalEmp(footerTmpl: TemplateRef<any>, isEmpReplace?: boolean, index?: number) {
    this.modalRef = this.modal.create({
      nzTitle: 'Tìm kiếm nhân viên',
      nzWidth: this.getInnerWidth(),
      nzContent: ProposeMultipleEmployeeComponent,
      nzComponentParams: {
        listEmployeeModal: [],
        isEmp: true,
      },
      nzFooter: footerTmpl,
    });
    this.modalRef.afterClose.subscribe((result) => {
      const listEmployeeModal = result;
      if (listEmployeeModal?.length > 0) {
        if (isEmpReplace) {
          this.arrayProposeMultiple.at(index).get('replacementEmployeeCode').setValue(listEmployeeModal[0].empCode);
          this.arrayProposeMultiple.at(index).get('replacementEmployee').setValue(listEmployeeModal[0].fullName + '-' + listEmployeeModal[0].jobName + '-' + listEmployeeModal[0].orgName);
          this.arrayProposeMultiple.at(index).get('replacementEmployeeName').setValue(listEmployeeModal[0].fullName);
          this.arrayProposeMultiple.at(index).get('replacementEmployeeTitleCode').setValue(listEmployeeModal[0].jobId);
          this.arrayProposeMultiple.at(index).get('replacementEmployeeTitleName').setValue(listEmployeeModal[0].jobName);
        } else {
          this.arrayProposeMultiple.at(index).get('handoverEmployeeCode').setValue(listEmployeeModal[0].empCode);
          this.arrayProposeMultiple.at(index).get('handoverEmployee').setValue(listEmployeeModal[0].fullName + '-' + listEmployeeModal[0].jobName + '-' + listEmployeeModal[0].orgName);
          this.arrayProposeMultiple.at(index).get('handoverEmployeeName').setValue(listEmployeeModal[0].fullName);
          this.arrayProposeMultiple.at(index).get('handoverEmployeeTitleCode').setValue(listEmployeeModal[0].jobId);
          this.arrayProposeMultiple.at(index).get('handoverEmployeeTitleName').setValue(listEmployeeModal[0].jobName);
        }
      }
    });
  }

  onChangeUnitOffer(orgId: string, index: number, codeTitle?: number) {
    this.jobDTOSOffer = [];
    this.arrayProposeMultiple.at(index).get('contentTitleCode').setValue(null);
    if (orgId) {
      this.isLoading = true;
      const paramPosition: ParamSearchPosition = {
        orgId: orgId,
        jobId: '',
        status: 1,
        type: Constants.TYPE_SEARCH_POSITION,
        page: 0,
        size: maxInt32,
      };
      const sub = this.service.getPositionByUnit(paramPosition).subscribe(
        (res) => {
          const listPositionParent = res?.data?.content;
          this.jobDTOSOffer = listPositionParent.map((item: any) => {
            return {jobId: item.jobId, jobName: item.jobName};
          });
          this.arrayProposeMultiple.at(index).get('contentTitleCode').setValue(codeTitle || null);
          this.isLoading = false;
        },
        (e) => {
          this.isLoading = false;
        }
      );
      this.subs.push(sub);
    }
  }

  getFormGroupMultipleList(data: FormGroupMultipleList[]) {
    this.dataCommon.formGroupMultipleList = data;
  }

  saveProposeMultipleItem(item: EmployeeInfoMultiple, index?: number) {

    if (!this.arrayProposeMultiple.at(index).valid) {
      this.isSubmittedTable = true;
      return;
    }
    const dataFormProposeMultiple = this.formProposeMultiple.getRawValue()?.arrayProposeMultiple;
    const data = dataFormProposeMultiple[index!];
    if (data.implementDate && new Date(data.implementDate).getTime() < new Date().getTime()) {
      this.toastrCustom.error(this.translate.instant('development.commonMessage.E010'));
      return;
    }
    this.onLoading.emit(true);
    const param: EmployeeInfoMultipleEdit = {
      id: item.id,
      contentTitleCode: data?.contentTitleCode,
      contentTitleName: this.jobDTOSOffer.find((itemJob) => itemJob.jobId === data?.contentTitleCode)?.jobName,
      contentUnitCode: data.contentUnit?.orgId?.toString(),
      contentUnitName: data.contentUnit?.orgName,
      contentUnitPathId: data.contentUnit?.pathId,
      contentUnitPathName: data.contentUnit?.pathResult,
      contentUnitTreeLevel: data.contentUnit?.orgLevel,
      contentLevel: data.contentLevel,
      proposeComment: {
        id: item?.proposeComment?.id || null,
        strengths: data.strengths,
        weaknesses: data.weaknesses
      },
      proposeAlternative: {
        id: item?.proposeAlternative ? item?.proposeAlternative[0]?.id : null,
        proposeDetailId: item.id,
        alternativeType: data.alternativeType,
        estimatedDate: null,
        replacementEmployeeCode: data?.replacementEmployeeCode,
        replacementEmployeeName: data?.replacementEmployeeName,
        replacementEmployeeTitleCode: data?.replacementEmployeeTitleCode,
        handoverEmployeeCode: data?.handoverEmployeeCode,
        handoverEmployeeName: data?.handoverEmployeeName,
        handoverEmployeeTitleCode: data?.handoverEmployeeTitleCode,
        handoverEmployeeTitleName: data?.handoverEmployeeTitleName,
        replacementEmployeeTitleName: data?.replacementEmployeeTitleName,
        replacementEmployee: data?.replacementEmployee,
        handoverEmployee: data?.handoverEmployee,
      },
      implementDate: data?.implementDate,
      minimumTime: data?.minimumTime,
      maximumTime: data?.maximumTime,
      rotationReasonID: data?.rotationReasonID,
      formRotationCode: this.dataCommon.formGroupMultipleList?.find((itemFormGroupMultiple) => itemFormGroupMultiple.formCode === data?.formName)?.code,
      formRotationType: data?.formName,
    };
    this.proposeMultipleService.editItemProposeMultiple(param, this.screenCode).subscribe((res) => {
      this.onLoading.emit(false);
      this.toastrCustom.success(this.translate.instant('development.commonMessage.success'));
      this.arrayProposeMultiple.clear();
      this.getInfoEmp();


    }, (e) => {
      this.getMessageError(e);
      this.onLoading.emit(false);
    })

  }

  closeProposeMultipleItem(item: EmployeeInfoMultiple, index: number) {
    item.isEdit = false;
    this.createFormGroups([item], true, index);
  }


  displayConditionViolation(conCode?: number) {
    switch (conCode) {
      case 0:
        return {color: 'red', label: this.translate.instant('development.proposed-unit.conditionLD.0')};
      case 1:
        return {color: 'yellow', label: this.translate.instant('development.proposed-unit.conditionLD.1')};
      case 2:
        return {color: 'green', label: this.translate.instant('development.proposed-unit.conditionLD.2')};
      default:
        return null;
    }
  }

  viewSemesterGrade(semesterGradeT?: string | number, semesterRankT?: string | number) {
    if (semesterRankT) {
      return semesterGradeT ? `${semesterGradeT} (${semesterRankT})` : ''
    } else {
      return semesterGradeT ? `${semesterGradeT}` : ''
    }
  }

  viewValidators(index: number) {
    return this.isSubmittedTable && this.listEmployeeInfo[index]?.isEdit;
  }

  selectAlternativeType(e: number, item: EmployeeInfoMultiple) {
    item.alternativeTypeCheck = e === 0;
  }

  editProposeItem(item: EmployeeInfoMultiple, index: number) {
    if (item?.formGroup) {
      if (+item?.formGroup === Constants.FormGroupList.TGCV && !item?.personalCode) {
        this.routerFromElement(item, Constants.SubRouterLink.UPDATETGCV);
      } else if (+item?.formGroup === Constants.FormGroupList.BO_NHIEM) {
        this.routerFromElement(item, Constants.TypeScreenHandle.UPDATE_APPOINTMENT);
      } else if ((+item?.formGroup === Constants.FormGroupList.TGCV || +item?.formGroup === Constants.FormGroupList.DCDD) && item?.personalCode) {
        this.router.navigate([Constants.KeyCommons.DEVELOPMENT_RECOMMEND_INDIVIDUAL_ACTION], {
          queryParams: {
            id: item.proposeId,
            proposeDetailId: item.id,
            proposeCategory: Constants.ProposeCategory.ELEMENT,
            personalCode: item?.personalCode,
            screenCode: this.screenCode,
          },
          state: {backUrl: this.router.url},
          skipLocationChange: true,
        });
      } else {
        this.routerFromElement(item, Constants.SubRouterLink.UPDATE);
      }
    }
  }

  validApproveButton() {
    this.isSubmitFormTTQD = true;
    return this.checkDisplayReportEdit()
      ? (this.formReportElement.get('statementNumber')?.value &&
        this.formReportElement.get('submissionDate')?.value &&
        this.formReportElement.get('approveNote')?.value &&
        this.fileListSelectTTEdit.length === 1)
      : (this.formTTQD.get('statementNumber')?.value &&
        this.formTTQD.get('submissionDate')?.value &&
        this.reasonsReject &&
        this.fileListSelectTT.length === 1);
  }

  viewRotationReasonID(rotationReasons?: ProposeRotationReasons[]) {
    const reasons: string[] = [];
    if (rotationReasons && rotationReasons.length > 0) {
      rotationReasons?.forEach((item) => {
        reasons.push(<string>this.dataCommon.proposeRotationReasonConfigDTOS?.find((i) => i.id?.toString() === item.rotationReasonID)?.name)
      })
      return reasons.join(' , ');
    }
    return;
  }

  emitDataIssueDecision() {
    if (this.listEmployeeInfo.length > 0) {
      if (!this.formTTQD.valid) {
        this.isSubmitFormTTQD = true;
        return;
      } else {
        this.formTTQD.get('id')?.setValue(this.proposeId);
        const newForm = {...cleanDataForm(this.formTTQD)};
        newForm.hasDecision = this.hasDecision;
        newForm.externalApprove = this.displayReport;
        return newForm;
      }
    } else {
      this.toastrCustom.error(this.translate.instant('development.proposed-unit.messages.dontApplyStatement'));
      return;
    }
  }

  expandInfoEmp() {
    this.isSeeMore = !this.isSeeMore;
  }

  expandInfoEmpText() {
    const infoEmpText = !this.isSeeMore
      ? this.translate.instant('development.proposed-unit.button.down')
      : this.translate.instant('development.proposed-unit.button.up')
    return infoEmpText;
  }

  updateCheckedSetWithdraw(item: EmployeeInfoMultiple, checked: boolean): void {
    if (checked && Constants.DisableCancelProposeCheckbox.indexOf(item.flowStatusCode) <= -1
      && !item?.cancelProposeRetractFlag) {
      this.quantityMapWithdraw.set(item.id!, item);
    } else {
      this.quantityMapWithdraw.delete(item.id!);
    }
  }

  onItemCheckedWithdraw(item: EmployeeInfoMultiple, checked: boolean): void {
    this.updateCheckedSetWithdraw(item, checked);
    this.refreshCheckedStatusWithdraw();
  }

  onAllCheckedWithdraw(checked: boolean) {
    const list = this.listEmployeeInfoTotal.filter(
      (item: EmployeeInfoMultiple) =>
      Constants.DisableCancelProposeCheckbox.indexOf(item.flowStatusCode) <= -1 && !item?.cancelProposeRetractFlag);
    list.forEach((item: EmployeeInfoMultiple) => this.updateCheckedSetWithdraw(item, checked));
    this.refreshCheckedStatusWithdraw();
  }

  refreshCheckedStatusWithdraw() {
    const list = this.listEmployeeInfoTotal.filter(
      (item: EmployeeInfoMultiple) =>
      Constants.DisableCancelProposeCheckbox.indexOf(item.flowStatusCode) <= -1 && !item?.cancelProposeRetractFlag);
    this.checkedWithdrawAll = list.every((item: EmployeeInfoMultiple) => this.quantityMapWithdraw.has(item.id!));
  }

  checkDisplayButtonWithdrawAll() {
    return this.quantityMapWithdraw.size === 0;
  }

  scrollToValidationSection(section: string, messsage: string) {
    this.toastrCustom.warning(this.translate.instant(messsage));
    const el = document.getElementById(section);
    el?.scrollIntoView({behavior: 'smooth', block: 'start', inline: 'nearest'});
  }


  checkButtonDecisionReport() {
    return ((this.flowStatusCode === this.flowCodeConstants.HANDLING6 ||
        this.flowStatusCode === this.flowCodeConstants.CHODUYET7 || this.flowStatusCode === this.flowCodeConstants.KSDongY)
      && this.dataForm?.groupProposeTypeId === Constants.ProposeTypeID.GROUP)
  }

  clearArrayProposeMultiple() {
    this.arrayProposeMultiple.clear();
  }

  viewTitleNumberUpdatedInterview(timesInterview: number) {
    return timesInterview ? this.translate.instant('development.proposed-unit.proposeMultiple.times') + ' ' + timesInterview.toString() : '';
  }

  //check disable cục tờ trình của PTNL
  checkDisableReportPTNL() {
    return !(this.checkFlowStatusCodeElement && this.dataForm?.createdBy === this.currentUser?.username)
  }

  onChangeFormName(formCode: string, index: number) {
    const itemFormCode = this.dataCommon.formGroupMultipleList?.find((item: FormGroupMultipleList) => item.formCode === formCode);
    if (itemFormCode?.showMaxTime && itemFormCode?.showMinTime) {
      this.arrayProposeMultiple.at(index).get('minimumTime')?.addValidators(Validators.required);
      this.arrayProposeMultiple.at(index).get('maximumTime')?.addValidators(Validators.required);
    } else {
      this.arrayProposeMultiple.at(index).get('minimumTime')?.clearValidators();
      this.arrayProposeMultiple.at(index).get('maximumTime')?.clearValidators();
    }
    this.arrayProposeMultiple.at(index).get('minimumTime')?.updateValueAndValidity();
    this.arrayProposeMultiple.at(index).get('maximumTime')?.updateValueAndValidity();

  }

  //check disable checkbox Hủy đề xuất
  disableCancelProposeCheckbox(flowStatusCode: string) {
    return (this.typeAction === this.screenType.detail
        && this.screenCode === this.screenCodeConst.LDDV)
      || (this.screenCode === this.screenCodeConst.HRDV
        && Constants.DisableCancelProposeCheckbox.indexOf(flowStatusCode) > -1);
  }

  //check cờ của Thành phần đã được rút đề xuất
  checkFlagElement() {
    return this.listEmployeeInfoTotal.filter(
      (item: EmployeeInfoMultiple) => !item.cancelProposeRetractFlag).length > 0;
  }

  disabledImplementDate = (dateVal: Date) => {
    return dateVal.valueOf() <= Date.now();
  };

  mapConditionViolation(code: number) {
    return this.dataCommon.conditionViolationList?.find(
      (item) => item.code === code.toString())?.name;
  }

  //check hiển thị ý kiến phê duyệt KSPTNL
  checkIdeaApproveKSPTNL() {
    switch (this.proposeCategory) {
      case Constants.ProposeCategory.ELEMENT:
        return this.screenCode === this.screenCodeConst.KSPTNL
          && this.flowStatusCode === Constants.FlowCodeConstants.CHODUYET7
          && this.dataForm?.groupByKSPTNL;
      case Constants.ProposeCategory.GROUP:
        return this.screenCode === this.screenCodeConst.KSPTNL
          && this.flowStatusCode === Constants.FlowCodeConstants.CHODUYET7
      default:
        return false;
    }
  }

  //check hiển thị 3 nút duyệt của LDDV và ý kiến phê duyệt
  checkApproveLDDV() {
    if (this.proposeCategory !== Constants.ProposeCategory.ELEMENT) {
      return this.checkIdeaApproveLDDV();
    } else {
      return this.screenCode === this.screenCodeConst.LDDV
        && this.flowStatusCode === Constants.FlowCodeConstants.SENDAPPROVE2A
        && this.flowStatusCodeDisplay !== Constants.FlowCodeConstants.SENDAPPROVE2A;
    }
  }

  //check hiển thị ý kiến phê duyệt của LDDV
  checkIdeaApproveLDDV() {
    return this.screenCode === this.screenCodeConst.LDDV
      && (this.flowStatusCode === Constants.FlowCodeConstants.CHODUYET2 ||
        this.flowStatusCode === Constants.FlowCodeConstants.SENDAPPROVE2A);
  }

  //check hiển thị nút xuất tờ trình màn HRDV
  checkPrintReportHRDV() {
    return !this.displayReport && this.typeAction === this.screenType.update;
  }

  //check hiển thị nút xuất tờ trình(CPD khác đơn vị)
  checkPrintReportHRDV2() {
    return this.issueLevelName === this.unitIssueLevel && !this.dataForm?.isHo;
  }

  downloadFileDV(uid: string | number, name: string) {
    this.isLoading = true;
    this.serviceProposed.downloadFile(+uid, name).subscribe(
      () => {
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  };

  // check và in tờ trình màn nhiều
  checkShowModalAndPrintTemplate(action: number) {
    this.isLoading = true;
    if (this.proposeId) {
      const sub = this.service.checkShowModalPrintTemplate(this.proposeId)
        .subscribe((res) => {
          const checkShowModal = res.data;
          if (checkShowModal) {
            this.showModalTemplatePropose(checkShowModal, action)
          } else {
            const params = {
              groupProposeType: this.checkProposeCode(),
              action: action,
              screenCode: this.screenCode,
            };
            let listFile = []
            if(this.proposeId){
              const file = this.service.exportReportDecision(this.proposeId, params).subscribe((dataFile) => {
                listFile = dataFile?.data;
                if (!listFile || listFile?.length === 0) {
                  this.toastrCustom.error(this.translate.instant('development.proposed-unit.messages.noFile'));
                  this.isLoading = false;
                } else {
                  this.dowloadAllFile(listFile as ListFileTemplate[])
                }
                this.subs.push(file)
              })
            }
          }
          this.subs.push(sub)
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        });
    }
  };

  //show modal nếu api checkShowModalPrintTemplate trả ra true
  showModalTemplatePropose(checkShowModal: boolean, action: number) {
    if (checkShowModal) {
      this.modalRef = this.modal.create({
        nzTitle: this.translate.instant('development.proposed-unit.messages.typePrintTemplate'),
        nzWidth: 800,
        nzContent: ModalTemplateProposeComponent,
        nzFooter: null,
      });
      this.modalRef.componentInstance.eventBoolean.subscribe((isSort: boolean) => {
        this.modalRef?.destroy();
        this.isLoading = true;
        const params = {
          groupProposeType: this.checkProposeCode(),
          action: action,
          screenCode: this.screenCode,
          isSort: isSort,
        };
        let listFile = []
        if(this.proposeId) {
          const file = this.service.exportReportDecision(this.proposeId, params).subscribe((dataFile) => {
            listFile = dataFile?.data;
            if (!listFile || listFile?.length === 0) {
              this.toastrCustom.error(this.translate.instant('development.proposed-unit.messages.noFile'));
              this.isLoading = false;
            } else {
              this.dowloadAllFile(listFile as ListFileTemplate[])
            }
            this.subs.push(file)
          })
        }
        this.modalRef?.componentInstance.closeEvent.subscribe((data:boolean) => {
          if (!data) {
            this.modalRef?.destroy();
          }
        })
      })
    }
  }

  getStrengthListPropose(item: ProposeCommentAppoint) {
    if (this.screenCode === Constants.ScreenCode.PTNL) {
      return item?.strengthsPtnl ? item?.strengthsPtnl : item?.strengths;
    } else {
      return item?.strengths;
    }
  }

  getWeaknessListPropose(item: ProposeCommentAppoint) {
    if (this.screenCode === Constants.ScreenCode.PTNL) {
      return item?.weaknessesPtnl ? item?.weaknessesPtnl : item?.weaknesses;
    } else {
      return item?.weaknesses;
    }
  }

  //load lại bảng thành phần
  loadTableElement() {
    this.proposeMultipleService.listEmpPropose({
      id: this.proposeId,
      screenCode: this.screenCode
    }).subscribe((res) => {
      this.listEmployeeInfoTotal = res?.data?.content || [];
      this.listEmployeeInfoTotal?.forEach((item: EmployeeInfoMultiple) => {
        this.viewTextStatusApprove(item?.flowStatusCode);
      });
      this.isLoading = false;
    },
    (e) => {
      this.getMessageError(e);
      this.isLoading = false;
    })
  }

  //check hiển thị duyệt rút đề xuất
  checkWithdrawBtn() {
    const listWithdrawBtn = Constants.StatusButtonWithdraw;
    const listNoteCodeWithdraw = Constants.noteWithdraw;
    return listWithdrawBtn.indexOf(this.flowStatusCode) > -1
      && (this.proposeCategory === Constants.ProposeCategory.ORIGINAL)
      && listNoteCodeWithdraw.indexOf(this.withdrawNoteCode) > -1
      && (this.screenCode === this.screenCodeConst.LDDV);
  }

  disabledEndDateSubmission = (endDate: Date): boolean => {
    return endDate >=  new Date();
  };

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'development.proposed-unit.proposeLogsTable.time',
          field: 'actionDate',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
          tdTemplate: this.dateLog
        },
        {
          title: 'development.proposed-unit.proposeLogsTable.personAction',
          field: 'userName',
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
        },
        {
          title: 'development.proposed-unit.proposeLogsTable.action',
          field: 'action',
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 150,
        },
        {
          title: 'development.proposed-unit.proposeLogsTable.description',
          field: 'comment',
          thClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          tdClassList: [ Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 200,
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1,
    };
  }

  getLog(pageIndex: number){
    this.pageNumber = pageIndex - 1;
    this.isLoading = true;
    if (this.proposeId) {
      const log = this.serviceProposed.getLogsPage(this.proposeId, this.proposeCategory, this.screenCode, this.pageNumber, this.pageSize)
        .subscribe((logs) => {
          this.proposeLogs = logs?.data?.content || [];
          this.tableConfig.total = logs?.data?.totalElements || 0;
          this.tableConfig.pageIndex = pageIndex;
          this.isLoading = false;
        }, (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        })
      this.subs.push(log);
    }
  }

  //xem file đính kèm tờ trình của TCNS
  viewFileReportTCNS() {
    return this.currentUser?.username === this.dataForm?.createdBy
      ? (this.flowStatusCodeDisplay !== Constants.FlowCodeConstants.KSDongY
        && this.flowStatusCodeDisplay !== Constants.FlowCodeConstants.KSDongY9A)
      : (this.fileTCNS?.uid && this.fileTCNS?.fileName);
  }

  //check hiển thị cục tờ trình chỉnh sửa của đơn vị(tờ trình riêng của màn thành phần)
  // hiển thị ở trạng thái khác 1 ở HRDV(màn update) duyệt ngoài của cha,
  // có dữ liệu(màn view) hoặc ở trên PTNL nhưng phải có số tờ trình
  checkDisplayReportEdit() {
    return !this.displayReport &&
        (
          ((Constants.StatusApproveSendProposeMany.includes(this.flowStatusCode)
            && this.dataForm?.proposeDetails[0]?.hasAgreeUnit)
          && this.screenCode === Constants.ScreenCode.HRDV
          && this.typeAction === this.screenType.update) ||
          this.checkDataReportEdit()
        )
        && this.screenCode === Constants.ScreenCode.HRDV;
  }

  //check xem tờ trình chỉnh sửa có dữ liệu không
  checkDataReportEdit() {
    return this.formReportElement.get('statementNumber')?.value
      || this.formReportElement.get('submissionDate')?.value
      || this.formReportElement.get('approveNote')?.value
      || this.fileListSelectTTEdit.length === 1;
  }

  checkViewFileEdit() {
    return (this.screenCode === Constants.ScreenCode.HRDV && (this.fileAttachEdit?.uid && this.fileAttachEdit?.fileName)
      && this.typeAction === this.screenType.detail)
      || ((this.screenCode === Constants.ScreenCode.PTNL || this.screenCode === Constants.ScreenCode.KSPTNL)
        && this.checkDataReportEdit());
  }

  beforeUploadEdit = (file: File | NzUploadFile): boolean => {
    this.isLoading = true;
    const formData = new FormData();
    formData.append('files', file as File);
    formData.append('proposeDetailId', new Blob([JSON.stringify(this.proposeId)], {type: Constants.CONTENT_TYPE.APPLICATION_JSON}));
    formData.append('type', new Blob([Constants.TypeStatementFile.STATEMENTEDIT], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('groupProposeTypeId', new Blob(['1'], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('isIn', new Blob([JSON.stringify(this.displayReport)], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    this.service.uploadFile(formData).subscribe(
      (res) => {
        this.fileListSelectTTEdit = [
          ...this.fileListSelectTTEdit,
          {
            uid: res.data?.docId,
            name: res.data?.fileName,
            status: 'done',
            id: res.data?.id,
          },
        ];
        this.validApproveEditReport();
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    return false;
  };

  doRemoveFileEdit = (file: NzUploadFile): boolean => {
    this.isLoading = true;
    if (this.fileListSelectTTEdit.find((item) => item.uid === file.uid)){
      const idDelete = this.fileListSelectTTEdit.find((item) => item.uid === file.uid)?.['id'];
      this.service.deleteUploadFile(+idDelete).subscribe(
        () => {
          this.fileListSelectTTEdit = this.fileListSelectTTEdit.filter((item) => item['id'] !== idDelete);
          this.validApproveEditReport();
          this.isLoading = false;
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        }
      );
    }
    return false;
  };

  //validate tờ trình chỉnh sửa
  validApproveEditReport() {
    return this.formReportElement.get('statementNumber')?.value
      && this.formReportElement.get('submissionDate')?.value
      && this.formReportElement.get('approveNote')?.value
      && this.fileListSelectTTEdit.length === 1
  }

  downloadFile2(uid: string | number, name: string) {
    this.isLoading = true;
    this.service.downloadFile(+uid, name).subscribe(
      () => {
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    )
  }

  //check hiển thị ý kiến phê duyệt của LDDV (tờ trình chỉnh sửa)
  checkApproveNoteElement() {
    return !this.displayReport
      && ((this.screenCode === Constants.ScreenCode.HRDV && this.typeAction === this.screenType.update)
        || (this.screenCode === Constants.ScreenCode.HRDV && this.typeAction === this.screenType.detail
        && this.formReportElement.get('approveNote')?.value))
      && this.flowStatusCode !== Constants.FlowCodeConstants.HANDLING5B
      && this.flowStatusCode !== Constants.FlowCodeConstants.CANCELPROPOSE;
  }

  //check ý kiến phê duyệt LDDV ban đầu
  checkApproveNoteOrigin() {
    return !this.displayReport
      && !this.checkDisplayReportEdit()
      && !this.dataForm?.proposeDetails[0]?.hasAgreeUnit
      && Constants.StatusApproveSendProposeMany.includes(this.flowStatusCode);
  }

  checkEditHRDV(item: EmployeeInfoMultiple) {
    return this.screenCode === Constants.ScreenCode.HRDV && (Constants.EditButtonHRDV.includes(item?.flowStatusCode));
  }

  getNameReplacementEmployee(item: EmployeeInfoMultiple) {
    if (item?.proposeAlternative && item?.proposeAlternative[0]?.alternativeType !== undefined && item?.proposeAlternative[0]?.alternativeType !== null) {
      return item.proposeAlternative[0].replacementEmployee;
    }
    return '';
  }
}
