import {
  Component,
  EventEmitter,
  Injector,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Constants } from '@hcm-mfe/learn-development/data-access/common';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { ProposedUnitService } from '@hcm-mfe/learn-development/data-access/services';
import { RequestCancelPropose } from '@hcm-mfe/learn-development/data-access/models';

@Component({
  selector: 'app-cancel-issue',
  templateUrl: './cancel-issue.component.html',
  styleUrls: ['./cancel-issue.component.scss'],
})
export class CancelIssueComponent extends BaseComponent implements OnInit {
  form: FormGroup;
  isSubmitted = false;
  listPromulgate = Constants.ListPromulgate;
  @Input() proposeCode: string = '';
  @Input() pubLishCode: string = '';
  @Input() fullName: string = '';
  @Output() closeEvent = new EventEmitter<boolean>();
  @Output() cancelIssueBody = new EventEmitter<RequestCancelPropose>();
  labelMessage = '';
  constructor(injector: Injector, private service: ProposedUnitService) {
    super(injector);
    this.form = new FormGroup({
      reason: new FormControl(null, Validators.required),
      detail: new FormControl(null, Validators.required),
    });
  }

  ngOnInit() {
    this.labelMessage = this.translate.instant(
      'development.cancelIssue.labelHeader',
      {
        proposeCode: this.proposeCode,
        fullName: this.fullName,
      }
    );
  }

  submit() {
    this.isSubmitted = true;
    if (this.form.valid) {
      const body = {
        reasonOrCorrection: this.form.get('reason')?.value,
        viewDetailReasonOrCorrection: this.form.get('detail')?.value,
      };
      this.closeEvent.emit(true);
      this.cancelIssueBody.emit(body);
    }
  }

  closeModal() {
    this.closeEvent.emit(false);
  }
}
