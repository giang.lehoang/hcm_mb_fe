import {Component, Injector, Input, OnInit, TemplateRef, ViewChild} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {Constants, maxInt32, ScreenType, SessionKey, Utils} from "@hcm-mfe/learn-development/data-access/common";
import {
  AttachFile,
  ConditionViolationList,
  EmployeeRecommendIndividual, FileDTOList,
  FormGroupList,
  FormGroupMultipleList,
  JobDTOS,
  ListFileTemplate, ListTitle,
  OrgItem,
  ParamSearchPosition, ProposeStatementDTO,
  RecommendIndividualComment,
  StatusList,
  WithdrawNoteConfigList
} from '@hcm-mfe/learn-development/data-access/models';
import { ProposedUnitService, ProposeMultipleEmployeeService } from "@hcm-mfe/learn-development/data-access/services";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import * as FileSaver from 'file-saver';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { catchError, forkJoin, of, Subscription } from 'rxjs';
import {MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {NzUploadFile} from "ng-zorro-antd/upload";
import {User} from "@hcm-mfe/system/data-access/models";


@Component({
  selector: 'app-recommend-individual-propose',
  templateUrl: './recommend-individual-propose.component.html',
  styleUrls: ['./recommend-individual-propose.component.scss'],
})
export class RecommendIndividualProposeComponent extends BaseComponent implements OnInit {

  // thông tin nhân viên
  employeeForm = this.fb.group({
    concurrentTitleId: null,
    concurrentTitleName: null,
    concurrentUnitId: null,
    concurrentUnitName: null,
    concurrentUnitPathId: null,
    concurrentUnitPathName: null,
    concurrentTitleId2: null,
    concurrentTitleName2: null,
    concurrentUnitId2: null,
    concurrentUnitName2: null,
    concurrentUnitPathId2: null,
    concurrentUnitPathName2: null,
    empIdp: null,
    orgId: null,
    empId: null,
    empCode: null,
    fullName: null,
    email: null,
    posId: null,
    posName: null,
    jobId: null,
    jobName: null,
    orgPathId: null,
    orgPathName: null,
    orgName: null,
    majorLevelId: null,
    majorLevelName: null,
    faculityId: null,
    faculityName: null,
    schoolId: null,
    schoolName: null,
    positionLevel: null,
    gender: null,
    joinCompanyDate: null,
    dateOfBirth: null,
    partyDate: null,
    partyOfficialDate: null,
    posSeniority: null,
    seniority: null,
    qualificationEnglish: null,
    facultyName: null,
    semesterGradeT1: null,
    semesterRankT1: null,
    semesterGradeT2: null,
    semesterRankT2: null,
    ratedCapacity: null,
    nearestDiscipline: null,
    endOfDisciplinaryDuration: null,
    userToken: null,
    treeLevel: null,
    posSeniorityName: null,
    seniorityName: null,
    levelName: null,
    formGroup: null,
    periodNameT1: null,
    periodNameT2: null,
  });

  isExpand = false;
  isSeeMore = false;
  modalRef?: NzModalRef;
  typeAction?: ScreenType;
  screenCodeConst = Constants.ScreenCode;

  dataCommon = {
    jobDTOS: [], // vị trí
    formGroupList: <FormGroupList[]>[],
    formGroupMultipleList: <FormGroupMultipleList[]>[],
    conditionViolationList: <ConditionViolationList[]>[],
  };
  screenCode = Constants.ScreenCode.HRDV;
  proposeId = 0;
  proposeUnitForm = this.fb.group({
    id: null,
    formGroup: null,
    flowStatusCode: null,
    externalApprove: false,
    sentDate: null,
  });


  private readonly subs: Subscription[] = [];
  formReportElement = new FormGroup({
    statementNumber: new FormControl(null, [Validators.required]),
    submissionDate: new FormControl(null, [Validators.required]),
    idea: new FormControl(null, [Validators.required]),
  });

  formReportElementEdit = new FormGroup({
    statementNumber: new FormControl(null, [Validators.required]),
    submissionDate: new FormControl(null, [Validators.required]),
  });
  isSubmitFormSuggestionsPersonal = false;
  externalApprove = false;

  formSuggestionsPersonal = new FormGroup({
    proposeUnit: new FormControl(null, [Validators.required]),
    proposeTitle: new FormControl(null, [Validators.required]),
    proposeReason: new FormControl(null, [Validators.required]),
    commitment: new FormControl(null, [Validators.required]),
  });

  proposeDetailId = 0;
  jobDTOS: JobDTOS[] = [];
  checkDisplay: string | number | undefined;
  id: number | undefined;
  checkDisplayScreen = false;
  checkScreen = false;
  dataTableEmpDetail: EmployeeRecommendIndividual | undefined;
  dataproposeComment: RecommendIndividualComment | undefined;
  dataproposeStatement: ProposeStatementDTO[] = [];
  dataproposeStatementEdit: ProposeStatementDTO | undefined;
  conditionViolations = [];
  violationCode = 0;
  checkDisplayCode: number | undefined;
  formGroupDetail ='';
  formGroupNameDetail: string | undefined;
  approveNoteDetail: string | undefined;
  iconStatus = Constants.IconStatus.UP;
  pagination = new Pagination();
  idDetail: number | undefined;
  contentUnitNameData = '';
  contentTitleNameData = '';
  formGroup: number | undefined;
  fileListSelectTTElement: NzUploadFile[] = [];
  fileListSelectTTElementEdit: NzUploadFile[] = [];
  fileElement: AttachFile = {};
  fileElementEdit: AttachFile = {};
  contentUnit?: OrgItem | null;
  flowStatusCodeDisplay = '';
  flowStatusCode = '';
  createdBy = '';
  statusPublishCodeDisplay = '';
  flowStatusName = '';
  statusPublishName = '';
  withdrawNoteName = '';
  ConstantsFormGroup = Constants.FormGroupList
  functionCode: string;
  rolesOfCurrentUser: string[] = [];
  sentDate: Date | undefined;
  idCreate = 0;
  note = '';
  withdrawNote = '';
  checkWithdrawType: number | undefined;
  withdrawReason = '';
  comment = '';
  screenCodeRouting = '';
  currentUser: User | undefined;
  constantsVoditionsCode = Constants.ViolationCode
  @Input() personalCode = '';
  proposeCategoryView = '';
  proposeIdDetailView = 0;
  flowCodeConst = Constants.FlowCodeConstants;

  constructor(injector: Injector,
              private readonly service: ProposedUnitService,
              private readonly proposeMultipleService: ProposeMultipleEmployeeService

  ) {
    super(injector);
    this.typeAction = this.route.snapshot.data['screenType'];
    // this.formGroup = +this.route.snapshot?.queryParams['formGroup'];
    this.checkScreen = this.route.snapshot?.queryParams['checkFormGroup'];
    if(!this.route.snapshot?.queryParams['idCreate']) {
      this.idCreate = this.route.snapshot?.queryParams['id']
    } else {
      this.idCreate = this.route.snapshot?.queryParams['idCreate']
    }
    this.checkDisplayCode = this.route.snapshot?.queryParams['checkRouterScreen'];
    this.screenCodeRouting = this.route.snapshot?.queryParams['screenCode'];
    //lấy proposeCategory và proposeDetailId từ màn danh sách
    this.proposeCategoryView = this.route.snapshot?.queryParams['proposeCategory'];
    this.proposeIdDetailView = this.route.snapshot?.queryParams['proposeDetailId'];
    this.functionCode = this.route.snapshot.data['code'];
  }




  ngOnInit() {
    if (localStorage.getItem(SessionKey.CURRENCY_USER)) {
      this.currentUser = JSON.parse(localStorage.getItem(SessionKey.CURRENCY_USER) ?? '');
    }
    this.proposeUnitForm.controls['formGroup']?.setValue(this.formGroup);
    this.isLoading = true;
    this.checkDisplayScreen = true;
    const proposeCategory = Constants.ProposeCategory.ORIGINAL
    const screenCode = this.screenCodeRouting ? this.screenCodeRouting : this.screenCode
    const sub = forkJoin(
      [
        this.service.getState({screenCode: this.screenCode}).pipe(catchError(() => of(undefined))),
        this.proposeMultipleService.getProposeId().pipe(catchError(() => of(undefined))),
        //check nếu là thành phần thì call viewDetail, còn lại thì call proposeids
        this.proposeCategoryView === Constants.ProposeCategory.ELEMENT
          ? this.service.getDetailElementRecommendIndividual(this.proposeIdDetailView, screenCode).pipe(catchError(() => of(undefined)))
          : this.service.getDetailRecommendIndividual(this.idCreate, screenCode).pipe(catchError(() => of(undefined))),
        this.service.getRolesByUsername().pipe(catchError(() => of(undefined))),
      ]
    ).subscribe(
      ([state, result, res, roles]) => {
        this.isLoading = false;
        this.proposeId = result?.data;
        this.rolesOfCurrentUser = roles;
        this.proposeUnitForm.get('id')?.setValue(this.proposeId);
        if (state) {
          this.dataCommon = state;
        }
        // this.checkDisplayCode = display?.data?.checkDisplay;
        this.id = res?.data?.id;
        this.dataTableEmpDetail = res?.data?.proposeDTO[0]?.proposeDetails[0]?.employee;
        this.dataproposeComment = res?.data?.proposeDTO[0]?.proposeDetails[0]?.proposeComment;
        this.conditionViolations = res?.data?.proposeDTO[0]?.proposeDetails[0]?.conditionViolations;
        this.violationCode = res?.data?.proposeDTO[0]?.proposeDetails[0]?.conditionViolationCode;
        this.idDetail = res?.data?.proposeDTO[0]?.proposeDetails[0]?.id;
        this.flowStatusCode = res?.data?.proposeDTO[0]?.flowStatusCode;
        this.createdBy = res?.data?.proposeDTO[0]?.createdBy
        this.flowStatusCodeDisplay = res?.data?.proposeDTO[0]?.flowStatusCodeDisplay;
        this.checkWithdrawType = res?.data?.proposeDTO[0]?.withdrawType;
        this.withdrawNote = res?.data?.proposeDTO[0]?.withdrawNote;
        this.statusPublishCodeDisplay = res?.data?.proposeDTO[0]?.flowStatusPublishCode;
        this.withdrawReason = res?.data?.proposeDTO[0]?.withdrawReason;
        this.dataproposeStatement = res?.data?.proposeDTO[0]?.proposeStatementList.find((item: ProposeStatementDTO) => !item?.typeStatement);
        this.externalApprove = res?.data?.proposeDTO[0]?.externalApprove;
        this.id = res?.data?.proposeDTO[0]?.id;
        this.sentDate = res?.data?.proposeDTO[0]?.sentDate;
        this.approveNoteDetail = res?.data?.proposeDTO[0]?.approveNote;
        this.formReportElement.controls['idea']?.setValue(this.approveNoteDetail)
        this.formGroupDetail = res?.data?.proposeDTO[0]?.formGroup;
        const flowCodeManyObject = res?.data?.flowStatusApproveConfigList?.find(
          (item: WithdrawNoteConfigList) => item.key === this.flowStatusCode
        );
        this.flowStatusName = flowCodeManyObject?.role
        if(this.formGroupDetail){
          this.formGroupNameDetail = this.dataCommon.formGroupList.find((item) => item?.id === +this.formGroupDetail)?.name
        }
        if (this.dataTableEmpDetail) {
          if (this.dataTableEmpDetail.posSeniority) {
            const years = Math.floor(
              this.dataTableEmpDetail.posSeniority / 12
            );
            const months = Math.floor(
              this.dataTableEmpDetail.posSeniority % 12
            );
            if (months === 0) {
              this.dataTableEmpDetail.posSeniorityName =
                years < 1 ? `` : `${years} năm`;
            } else {
              this.dataTableEmpDetail.posSeniorityName =
                years < 1
                  ? `${months} tháng`
                  : `${years} năm ${months} tháng`;
            }
          }

          if (this.dataTableEmpDetail.seniority) {
            const years = Math.floor(
              this.dataTableEmpDetail.seniority / 12
            );
            const months = Math.floor(
              this.dataTableEmpDetail.seniority % 12
            );
            if (months === 0) {
              this.dataTableEmpDetail.seniorityName =
                years < 1 ? `` : `${years} năm`;
            } else {
              this.dataTableEmpDetail.seniorityName =
                years < 1
                  ? `${months} tháng`
                  : `${years} năm ${months} tháng`;
            }
          }
          this.employeeForm.patchValue(this.dataTableEmpDetail);
        }
        if (this.dataproposeComment) {
          this.formSuggestionsPersonal.patchValue(this.dataproposeComment);
        }
        if (this.dataproposeStatement) {
          this.formReportElement.patchValue(this.dataproposeStatement);
        }
        this.proposeUnitForm.controls['formGroup']?.setValue(+this.formGroupDetail)
        this.contentUnitNameData = res?.data?.proposeDTO[0]?.proposeDetails[0]?.contentUnitCode;
        this.contentTitleNameData = res?.data?.proposeDTO[0]?.proposeDetails[0]?.contentTitleCode;
        // this.formSuggestionsPersonal.controls['proposeUnit']?.setValue(+this.contentUnitNameData);
        if(this.contentUnitNameData && this.contentTitleNameData) {
          this.contentUnit = {
            orgId: res?.data?.proposeDTO[0]?.proposeDetails[0]?.contentUnitCode,
            orgName: res?.data?.proposeDTO[0]?.proposeDetails[0]?.contentUnitName,
            pathId: res?.data?.proposeDTO[0]?.proposeDetails[0]?.contentUnitPathId,
            pathResult: res?.data?.proposeDTO[0]?.proposeDetails[0]?.contentUnitPathName,
            orgLevel: res?.data?.proposeDTO[0]?.proposeDetails[0]?.contentUnitTreeLevel,
          };
        }
        this.formSuggestionsPersonal.controls['proposeUnit']?.setValue(this.contentUnit);
        if(this.contentTitleNameData && this.contentUnitNameData){
          this.onChangeUnit(res?.data?.proposeDTO[0]?.proposeDetails[0]?.contentUnitCode?.toString(),
            res?.data?.proposeDTO[0]?.proposeDetails[0]?.contentTitleCode)
        }

        const statusPublishCodeManyObject = res?.data?.flowStatusPublishConfigList?.find(
          (item: WithdrawNoteConfigList) => item.key === this.statusPublishCodeDisplay
        );
        this.statusPublishName = statusPublishCodeManyObject?.role

        const withdrawNote = res?.data?.withdrawNoteConfigList?.find(
          (i: StatusList) => i.key  === this.withdrawNote?.toString());
        this.withdrawNoteName = withdrawNote?.role
        this.dataproposeStatementEdit = res?.data?.proposeDTO[0]?.proposeDetails[0]?.proposeStatement?.find((item: ProposeStatementDTO) => item.typeStatement === Constants.EmployeeInfoMultipleFlowStatus.flowStatus2)
        if (this.dataproposeStatementEdit) {
          this.formReportElementEdit.patchValue(this.dataproposeStatementEdit);
        }
        const dataNotifyDecision = res?.data?.proposeDTO[0]?.proposeDetails[0].fileDTOList?.find((item: FileDTOList) => item.type === Constants.TypeStatementFile.STATEMENT);
        const dataNotifyDecisionEdit = res?.data?.proposeDTO[0]?.proposeDetails[0].fileDTOList?.find((item: FileDTOList) => item.type === Constants.TypeStatementFile.STATEMENTEDIT);
        this.fileElement.uid = dataNotifyDecision?.docId;
        this.fileElement.fileName = dataNotifyDecision?.fileName;
        this.fileElementEdit.uid = dataNotifyDecisionEdit?.docId;
        this.fileElementEdit.fileName = dataNotifyDecisionEdit?.fileName;

        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }

  expandInfoEmp() {
    this.isSeeMore = !this.isSeeMore;
  }

  expandInfoEmpText() {
    const infoEmpText = !this.isSeeMore
      ? this.translate.instant('development.proposed-unit.button.down')
      : this.translate.instant('development.proposed-unit.button.up')
    return infoEmpText;
  }


  override ngOnDestroy() {
    this.subs.forEach((s) => s.unsubscribe());
  }



  getInnerWidth(): number {
    if (window.innerWidth > 767) {
      return window.innerWidth / 1.5 > 1368 ? 1368 : window.innerWidth / 1.3;
    }
    return window.innerWidth;
  }


  dowloadStatement(action: number) {
    const params = {
      groupProposeType: Constants.ProposeTypeID.ORIGINAL,
      action: action,
      screenCode: Constants.ScreenCode.HRDV,
    };
    this.isLoading = true;
    let listFile: ListFileTemplate[] = [];
    if (this.id) {
      const sub = this.service.exportReportDecision(this.id, params).subscribe(
        (res) => {
          listFile = res?.data;
          if (!listFile || listFile?.length === 0) {
            this.toastrCustom.error('development.list-plan.messages.noFile');
            this.isLoading = false;
          } else {
            listFile?.forEach((item) => {
              this.service.callBaseUrl(item.linkTemplate).subscribe((res) => {
                  const contentDisposition = res?.headers.get('content-disposition');
                  const filename = contentDisposition?.split(';')[1]?.split('filename')[1]?.split('=')[1]?.trim();
                  const blob = new Blob([res?.body], {
                    type: 'application/octet-stream',
                  });
                  FileSaver.saveAs(blob, filename);
                  this.isLoading = false;
                },
                (e) => {
                  this.getMessageError(e);
                  this.isLoading = false;
                })
            })
          }
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        }
      );
      this.subs.push(sub);
    }
  }

  // Lay cd theo don vi
  onChangeUnit(orgId: string, titleUnit?: number) {
    this.jobDTOS = [];
    this.formSuggestionsPersonal.controls['proposeTitle']?.setValue(null);
    if (orgId) {
      // this.isLoading = true;
      const paramPosition: ParamSearchPosition = {
        orgId: orgId,
        jobId: '',
        status: 1,
        type: Constants.TYPE_SEARCH_POSITION,
        page: 0,
        size: maxInt32,
      };
      const sub = this.service.getPositionByUnit(paramPosition).subscribe(
        (res) => {
          const listPositionParent = res?.data?.content;
          this.jobDTOS = listPositionParent.map((item: ListTitle) => {
            return { jobId: item.jobId, jobName: item.jobName };
          });
          if (titleUnit) {
            this.formSuggestionsPersonal.controls['proposeTitle']?.setValue(titleUnit);
          }
          // this.isLoading = false;
        },
        (e) => {
          this.isLoading = false;
          this.getMessageError(e);
        }
      );
      this.subs.push(sub);
    }
  }


  expandLog() {
    this.isExpand = !this.isExpand;
    if (this.isExpand) {
      this.iconStatus = 'down';
    } else {
      this.iconStatus = 'up';
    }
  }

  downloadFile = (file: NzUploadFile) => {
    this.service.downloadFile(+file?.uid, file?.name).subscribe(
      () => {
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    )
  }

  beforeUploadTT = (file: File | NzUploadFile): boolean => {
    this.isLoading = true;
    const formData = new FormData();
    formData.append('files', file as File);
    formData.append('proposeDetailId', new Blob([JSON.stringify(this.idDetail)], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('type', new Blob(['statement'], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('groupProposeTypeId', new Blob(['1'], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('isIn', new Blob(['true'], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    this.service.uploadFile(formData).subscribe(
      (res) => {
        this.fileListSelectTTElement = [
          ...this.fileListSelectTTElement,
          {
            uid: res.data?.docId,
            name: res.data?.fileName,
            status: 'done',
            id: res.data?.id,
          },
        ];
        this.validApproveButton();
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    return false;
  };

  doRemoveFileTT = (file: NzUploadFile): boolean => {
    this.isLoading = true;
    if (this.fileListSelectTTElement.find((item) => item.uid === file.uid)){
      const idDelete = this.fileListSelectTTElement.find((item) => item.uid === file.uid)?.['id'];
      this.service.deleteUploadFile(+idDelete).subscribe(
        (res) => {
          this.fileListSelectTTElement = this.fileListSelectTTElement.filter((item) => item['id'] !== idDelete);
          this.validApproveButton();
          this.isLoading = false;
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        }
      );
    }
    return false;
  };

  //validate điền thông tin cho nút ghi nhận đồng ý
  validApproveButton(){
    return this.formReportElement.get('statementNumber')?.value
      && this.formReportElement.get('submissionDate')?.value
      && this.formReportElement.get('idea')?.value
      && this.fileListSelectTTElement.length === 1
  }

  displayConditionViolation() {
    const violationObj = this.dataCommon.conditionViolationList?.find(
      (item) => +item.code === this.violationCode);
    switch (this.violationCode) {
      case 0:
        return { color: Constants.ConditionViolationColor.REQUIRED, label: violationObj?.name };
      case 1:
        return { color: Constants.ConditionViolationColor.VIOLATE, label: violationObj?.name };
      case 2:
        return { color: Constants.ConditionViolationColor.PASSED, label: violationObj?.name };
      default:
        return null;
    }
  }

  checkDisabledHandleSingle() {
   return this.personalCode ? true : false;
  }

  //check hiển thị cục tờ trình chỉnh sửa của đơn vị(tờ trình riêng của màn thành phần)
  // hiển thị ở trạng thái khác 1 ở HRDV(màn update) duyệt ngoài của cha,
  // có dữ liệu(màn view) hoặc ở trên PTNL nhưng phải có số tờ trình
  checkDisplayReportEdit() {
    // ở xử lý, nếu duyệt ngoài và tờ trình chỉnh sửa có giá trị, và screenCode = PTNL || KSPTNL thì hiển thị cụm tờ trình chỉnh sửa
    return !this.externalApprove &&
        (this.checkDataReportEdit() &&
          (this.screenCodeRouting === Constants.ScreenCode.PTNL || this.screenCodeRouting === Constants.ScreenCode.KSPTNL));
  }

  //check xem tờ trình chỉnh sửa có dữ liệu không
  checkDataReportEdit() {
    return this.dataproposeStatementEdit?.statementNumber
      || this.dataproposeStatementEdit?.submissionDate
      || this.fileListSelectTTElementEdit.length === 1
  }

  //check view đính kèm tờ trình chỉnh sửa của đơn vị
  checkViewFileElement() {
    return (this.screenCode === Constants.ScreenCode.HRDV && (this.fileElementEdit?.uid && this.fileElementEdit?.fileName))
      || ((this.screenCodeRouting === Constants.ScreenCode.PTNL || this.screenCode === Constants.ScreenCode.KSPTNL));
  }

  //check hiển thị ý kiến phê duyệt của LDDV (tờ trình chỉnh sửa)
  checkApproveNoteElement() {
    return !this.externalApprove && this.screenCode === Constants.ScreenCode.HRDV
      && this.flowStatusCode !== this.flowCodeConst.HANDLING5B
      && this.flowStatusCode !== this.flowCodeConst.CANCELPROPOSE;
  }

  //dowload file tờ trình chỉnh sửa
  downloadFile2(uid: string | number, name: string) {
    this.isLoading = true;
    this.service.downloadFile(+uid, name).subscribe(
      () => {
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    )
  }

}
