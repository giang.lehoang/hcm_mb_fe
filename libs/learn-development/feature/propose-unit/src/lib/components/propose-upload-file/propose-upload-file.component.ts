import {
  Component,
  EventEmitter,
  Injector,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { Constants } from '@hcm-mfe/learn-development/data-access/common';
import { ProposeTableFile } from '@hcm-mfe/learn-development/data-access/models';
import {
  PromulgateLdService,
  ProposedUnitService,
} from '@hcm-mfe/learn-development/data-access/services';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-propose-upload-file',
  templateUrl: './propose-upload-file.component.html',
  styleUrls: ['./propose-upload-file.component.scss'],
})
export class ProposeUploadFileComponent
  extends BaseComponent
  implements OnInit
{
  subs: Subscription[] = [];
  @Output() sendBody = new EventEmitter<ProposeTableFile[]>();
  @Input() fileList: NzUploadFile[] = [];
  @Input() id: number = 0;
  @Input() documentType: number = 0;
  @Output() closeEvent = new EventEmitter<boolean>();
  @Input() typeIssue = '';
  fileImportName = '';
  fileImportSize = '500';
  constructor(
    injector: Injector,
    private service: PromulgateLdService,
    private serviceProposed: ProposedUnitService
  ) {
    super(injector);
  }

  submit() {
    this.sendBody.emit([]);
  }

  beforeUpload = (file: File | NzUploadFile): boolean => {
    const formData = new FormData();
    formData.append('proposeCategory', Constants.ProposeCategory.ELEMENT);
    formData.append('id', this.id.toString());
    formData.append('type', this.typeIssue);
    formData.append('documentType', this.documentType.toString());
    formData.append('files', file as File);
    this.isLoading = true;
    this.service.upLoadFile(formData).subscribe(
      (res) => {
        this.fileList = [
          ...this.fileList,
          {
            uid: res.data[0]?.decisionPublishId,
            name: res.data[0]?.decisionPublishName,
            status: 'done',
            id: res.data[0]?.id,
          },
        ];
        this.closeEvent.emit(true);
        this.isLoading = false;
      },
      (e) => {
        this.isLoading = false;
        this.getMessageError(e);
      }
    );
    return false;
  };

  doRemoveFile = (file: NzUploadFile): boolean => {
    this.isLoading = true;
    const paramDelete = {
      id: this.id,
      proposeCategory: Constants.ProposeCategory.ELEMENT,
      documentType: this.documentType,
      type: this.typeIssue,
    };
    this.serviceProposed.removeFileNotify(paramDelete).subscribe(
      (res) => {
        this.isLoading = false;
        this.fileList = [];
      },
      (e) => {
        this.isLoading = false;
        this.getMessageError(e);
      }
    );
    return false;
  };

  ngOnInit() {}
}
