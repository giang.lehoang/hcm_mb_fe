import {Component, EventEmitter, Injector, Input, OnInit, Output} from '@angular/core';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {ListFileTemplate} from "@hcm-mfe/learn-development/data-access/models";
import {NzModalRef} from "ng-zorro-antd/modal";

@Component({
  selector: 'app-propose-template',
  templateUrl: './propose-template.component.html',
  styleUrls: ['./propose-template.component.scss'],
})
export class ProposeTemplateComponent extends BaseComponent {
  @Output() eventIdFile = new EventEmitter<number>();
  @Output() closeEvent = new EventEmitter<boolean>();
  @Input()  listDecision: ListFileTemplate[] = [];
  modalRef?: NzModalRef;
  id?: number;
  idtempalte?: string;

  closeModal() {
    this.closeEvent.emit(false);
  }

  save() {
    this.eventIdFile.emit(this.id)
  }

  onChangeListFileTemplate(idFile: number) {
    this.id = idFile;
  }

}
