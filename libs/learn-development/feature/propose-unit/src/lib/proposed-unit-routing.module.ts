import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
  ProposedUnitAddComponent,
  ProposedUnitComponent,
  ProposedUnitHandleComponent,
  ProposedUnitUpdateComponent,
} from "./pages";
import {ScreenType, FunctionCode} from "@hcm-mfe/learn-development/data-access/common";
import { ProposedAppointmentRenewalCreateComponent, ProposeMultipleInterviewComponent, ProposeTGCVComponent } from './components';

const routes: Routes = [
  {
    path: 'unit',
    data: {
      code: FunctionCode.DEVELOPMENT_PROPOSE,
      pageName: 'development.pageName.proposed-unit',
      breadcrumb: 'development.breadcrumb.proposed-unit',
    },
    children: [
      { path: '', component: ProposedUnitComponent },
      {
        path: 'add',
        component: ProposedUnitAddComponent,
        data: {
          pageName: 'development.pageName.proposed-unit-add',
          breadcrumb: 'development.breadcrumb.proposed-unit-add',
          screenType: ScreenType.Create,
        },
      },
      {
        path: 'update',
        component: ProposedUnitUpdateComponent,
        data: {
          pageName: 'development.pageName.proposed-unit-update',
          breadcrumb: 'development.breadcrumb.proposed-unit-update',
          screenType: ScreenType.Update,
        },
      },
      {
        path: 'update-tgcv',
        component: ProposeTGCVComponent,
        data: {
          pageName: 'development.pageName.proposed-unit-update',
          breadcrumb: 'development.breadcrumb.proposed-unit-update',
          screenType: ScreenType.Update,
        },
      },
      {
        path: 'detail',
        component: ProposedUnitUpdateComponent,
        data: {
          pageName: 'development.pageName.proposed-unit-detail',
          breadcrumb: 'development.breadcrumb.proposed-unit-detail',
          screenType: ScreenType.Detail,
        },
      },
      {
        path: 'detail-tgcv',
        component: ProposeTGCVComponent,
        data: {
          pageName: 'development.pageName.proposed-unit-detail',
          breadcrumb: 'development.breadcrumb.proposed-unit-detail',
          screenType: ScreenType.Detail,
        },
      },
      {
        path: 'detail/element-of-group',
        component: ProposedUnitUpdateComponent,
        data: {
          pageName: 'development.pageName.proposed-unit-detail',
          breadcrumb: 'development.breadcrumb.proposed-unit-detail',
          screenType: ScreenType.Detail,
        },
      },

      {
        path: 'update-appointment-renewal',
        component: ProposedAppointmentRenewalCreateComponent,
        data: {
          pageName: 'development.pageName.proposed-unit-update',
          breadcrumb: 'development.breadcrumb.proposed-unit-update',
          screenType: ScreenType.Update,
        },
      },
      {
        path: 'detail-appointment-renewal',
        component: ProposedAppointmentRenewalCreateComponent,
        data: {
          pageName: 'development.pageName.proposed-unit-detail',
          breadcrumb: 'development.breadcrumb.proposed-unit-detail',
          screenType: ScreenType.Detail,
        },
      },

      {
        path: 'update-propose-interview',
        component: ProposeMultipleInterviewComponent,
        data: {
          pageName: 'development.pageName.proposed-unit-update',
          breadcrumb: 'development.breadcrumb.proposed-unit-update',
          screenType: ScreenType.Update,
        },
      },

    ],
  },
  {
    path: 'unit-approve',
    data: {
      code: FunctionCode.DEVELOPMENT_APPROVE_PROPOSE,
      pageName: 'development.pageName.proposed-unit-approve',
      breadcrumb: 'development.breadcrumb.proposed-unit-approve',
    },
    children: [
      { path: '', component: ProposedUnitComponent },
      {
        path: 'detail',
        component: ProposedUnitUpdateComponent,
        data: {
          pageName: 'development.pageName.proposed-unit-approve-detail',
          breadcrumb: 'development.breadcrumb.proposed-unit-approve-detail',
          screenType: ScreenType.Detail,
        },
      },
      {
        path: 'detail-tgcv',
        component: ProposeTGCVComponent,
        data: {
          pageName: 'development.pageName.proposed-unit-approve-detail',
          breadcrumb: 'development.breadcrumb.proposed-unit-approve-detail',
          screenType: ScreenType.Detail,
        },
      },
      {
        path: 'detail-appointment-renewal',
        component: ProposedAppointmentRenewalCreateComponent,
        data: {
          pageName: 'development.pageName.proposed-unit-approve-detail',
          breadcrumb: 'development.breadcrumb.proposed-unit-approve-detail',
          screenType: ScreenType.Detail,
        },
      },
      {
        path: 'approve',
        component: ProposedUnitUpdateComponent,
        data: {
          pageName: 'development.pageName.proposed-unit-approve-approve',
          breadcrumb: 'development.breadcrumb.proposed-unit-approve-approve',
          screenType: ScreenType.Detail,
        },
      },
    ],
  },
  {
    path: 'propose-list',
    data: {
      code: FunctionCode.DEVELOPMENT_PROPOSE_LIST,
      pageName: 'development.pageName.proposedLD',
      breadcrumb: 'development.breadcrumb.proposedLD',
    },
    children: [
      { path: '', component: ProposedUnitComponent },
      {
        path: 'add',
        component: ProposedUnitAddComponent,
        data: {
          pageName: 'development.pageName.proposedLD-add',
          breadcrumb: 'development.breadcrumb.proposedLD-add',
          screenType: ScreenType.Create,
        },
      },
      {
        path: 'update',
        component: ProposedUnitUpdateComponent,
        data: {
          pageName: 'development.pageName.proposedLD-update',
          breadcrumb: 'development.breadcrumb.proposedLD-update',
          screenType: ScreenType.Update,
        },
      },
      {
        path: 'update-tgcv',
        component: ProposeTGCVComponent,
        data: {
          pageName: 'development.pageName.proposedLD-update',
          breadcrumb: 'development.breadcrumb.proposedLD-update',
          screenType: ScreenType.Update,
        },
      },
      {
        path: 'detail',
        component: ProposedUnitHandleComponent,
        data: {
          pageName: 'development.pageName.proposedLD-detail',
          breadcrumb: 'development.breadcrumb.proposedLD-detail',
          screenType: ScreenType.Detail,
        },
      },
      {
        path: 'detail-tgcv',
        component: ProposeTGCVComponent,
        data: {
          pageName: 'development.pageName.proposedLD-detail',
          breadcrumb: 'development.breadcrumb.proposedLD-detail',
          screenType: ScreenType.Detail,
        },
      },
      {
        path: 'handle',
        component: ProposedUnitHandleComponent,
        data: {
          pageName: 'development.pageName.proposedLD-handle',
          breadcrumb: 'development.breadcrumb.proposedLD-handle',
          screenType: ScreenType.Update,
          handlePropose: true,
        },
      },
      {
        path: 'handle-tgcv',
        component: ProposeTGCVComponent,
        data: {
          pageName: 'development.pageName.proposedLD-handle',
          breadcrumb: 'development.breadcrumb.proposedLD-handle',
          screenType: ScreenType.Update,
          handlePropose: true,
        },
      },


      {
        path: 'detail/element-of-group',
        component: ProposedUnitHandleComponent,
        data: {
          pageName: 'development.pageName.proposedLD-detail',
          breadcrumb: 'development.breadcrumb.proposedLD-detail',
          screenType: ScreenType.Detail,
        },
      },

      {
        path: 'handle/element-of-group',
        component: ProposedUnitHandleComponent,
        data: {
          pageName: 'development.pageName.proposedLD-handle',
          breadcrumb: 'development.breadcrumb.proposedLD-handle',
          screenType: ScreenType.Update,
          handlePropose: true,
        },
      },

      {
        path: 'update-appointment-renewal',
        component: ProposedAppointmentRenewalCreateComponent,
        data: {
          pageName: 'development.pageName.proposedLD-update',
          breadcrumb: 'development.pageName.proposedLD-update',
          screenType: ScreenType.Update,
        },
      },

      {
        path: 'detail-appointment-renewal',
        component: ProposedAppointmentRenewalCreateComponent,
        data: {
          pageName: 'development.pageName.proposedLD-detail',
          breadcrumb: 'development.pageName.proposedLD-detail',
          screenType: ScreenType.Detail,
        },
      },

      {
        path: 'handle-appointment-renewal',
        component: ProposedAppointmentRenewalCreateComponent,
        data: {
          pageName: 'development.pageName.proposedLD-handle',
          breadcrumb: 'development.breadcrumb.proposedLD-handle',
          screenType: ScreenType.Update,
          handlePropose: true,
        },
      },
    ],
  },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProposedUnitRoutingModule {}
