import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ApprovementConfigActionComponent, ApprovementConfigComponent, ApprovementConfigImportComponent} from "./pages";
import {ScreenType} from "@hcm-mfe/learn-development/data-access/common";
import {InterviewLdImportComponent} from "@hcm-mfe/learn-development/feature/interview-ld";


const routes: Routes = [
  { path: '', component: ApprovementConfigComponent },
  {
    path: 'create',
    component: ApprovementConfigActionComponent,
    data: {
      pageName: 'development.pageName.approvement-config-create',
      breadcrumb: 'development.breadcrumb.approvement-config-create',
      type: ScreenType.Create,
    },
  },
  {
    path: 'update',
    component: ApprovementConfigActionComponent,
    data: {
      pageName: 'development.pageName.approvement-config-update',
      breadcrumb: 'development.breadcrumb.approvement-config-update',
      type: ScreenType.Update,
    },
  },
  {
    path: 'clone',
    component: ApprovementConfigActionComponent,
    data: {
      pageName: 'development.pageName.approvement-config-clone',
      breadcrumb: 'development.breadcrumb.approvement-config-clone',
      type: ScreenType.Clone,
    },
  },
  {
    path: 'import',
    component: ApprovementConfigImportComponent,
    data: {
      pageName: 'development.pageName.interview-ld-import',
      breadcrumb: 'development.breadcrumb.interview-ld-import',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ApprovementConfigLdRoutingModule {}
