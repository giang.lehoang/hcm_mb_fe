import {Component, Injector, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {Category, MBTableConfig, Pageable} from "@hcm-mfe/shared/data-access/models";
import * as moment from 'moment';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import * as FileSaver from 'file-saver';
import { Subscription } from 'rxjs';
import {ApproveConfig, JobDTO} from "@hcm-mfe/learn-development/data-access/models";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {ApprovementConfigService} from "@hcm-mfe/learn-development/data-access/services";
import {Constants, userConfig, Utils} from "@hcm-mfe/learn-development/data-access/common";
import {cleanData} from "@hcm-mfe/shared/common/utils";
import {NzSafeAny} from "ng-zorro-antd/core/types";

@Component({
  selector: 'app-approvement-config-view',
  templateUrl: './approvement-config.component.html',
  styleUrls: ['./approvement-config.component.scss'],
})
export class ApprovementConfigComponent extends BaseComponent implements OnInit {
  dataTable: ApproveConfig[] = [];
  scrollY = '';
  dataCommon = {
    issueLevel: [],
    units: [],
    form: [],
    position: [],
    title: <JobDTO[]>[],
    businessLine: [],
  };
  params:any = {
    effectiveStartDate: null,
    effectiveEndDate: null,
    rangeDate: null,
    approvementStartDate: null,
    approvementEndDate: null,
    targetUnit: null,
    issueLevel: null,
    name: null,
    pageNumber: 0,
    pageSize: userConfig.pageSize,
    flag: true,
  };
  targetUnitName?: string | null;
  titlePage = '';
  pageable: Pageable = {
    totalElements: 0,
    currentPage: 0,
    size: userConfig.pageSize,
  };
  modalRef?: NzModalRef;
  private readonly subs: Subscription[] = [];
  preParam = this.params;

  @ViewChild('action', {static: true}) action!: TemplateRef<NzSafeAny>;
  @ViewChild('labelCanApprove', {static: true}) labelCanApprove!: TemplateRef<NzSafeAny>;
  @ViewChild('labelCanSign', {static: true}) labelCanSign!: TemplateRef<NzSafeAny>;
  @ViewChild('startDate', {static: true}) startDate!: TemplateRef<NzSafeAny>;
  @ViewChild('endDate', {static: true}) endDate!: TemplateRef<NzSafeAny>;
  @ViewChild('decisionDate', {static: true}) decisionDate!: TemplateRef<NzSafeAny>;
  @ViewChild('isSameTitle', {static: true}) isSameTitle!: TemplateRef<NzSafeAny>;
  @ViewChild('isSameLevel', {static: true}) isSameLevel!: TemplateRef<NzSafeAny>;
  @ViewChild('isSameUnit', {static: true}) isSameUnit!: TemplateRef<NzSafeAny>;
  @ViewChild('hasConsultation', {static: true}) hasConsultation!: TemplateRef<NzSafeAny>;
  @ViewChild('hasInterview', {static: true}) hasInterview!: TemplateRef<NzSafeAny>;
  tableConfig!: MBTableConfig;
  heightTable = { x: '100vw', y: '25em'};

  disabledStartDate = (startValue: Date): boolean => {
    if (!startValue || !this.params.effectiveEndDate) {
      return false;
    }
    return startValue.getTime() > (this.params.effectiveEndDate as Date).getTime();
  };

  disabledEndDate = (endValue: Date): boolean => {
    if (!endValue || !this.params.effectiveStartDate) {
      return false;
    }
    return endValue.getTime() <= (this.params.effectiveStartDate as Date).getTime();
  };

  constructor(injector: Injector, private service: ApprovementConfigService, private nzModalService: NzModalService) {
    super(injector);
  }

  ngOnInit(): void {
    this.initTable();
    this.isLoading = true;
    this.titlePage = this.route.snapshot?.data['pageName'];
    this.service.getState().subscribe((data) => {
      this.dataCommon = {
        businessLine: data?.businessLine || [],
        form: data?.form || [],
        issueLevel: data?.issueLevel || [],
        position: data?.position || [],
        title: data?.title || [],
        units: data?.units || [],
      };
      this.search(true);
    });
  }

  pageChange(page: number) {
    if (this.isLoading) {
      return;
    }
    this.params.pageNumber = page - 1;
    this.search();
  }

  search(firstPage?: boolean) {
    this.isLoading = true;
    if (firstPage) {
      this.params.pageNumber = 0;
    }
    const newParams = { ...cleanData(this.params) };
    newParams.effectiveStartDate =
      this.params.rangeDate && this.params.rangeDate[0] ? moment(newParams.rangeDate[0]).format(Constants.FORMAT_DATE.YYYY_MM_DD) : null;
    newParams.effectiveEndDate =
      this.params.rangeDate && this.params.rangeDate[1] ? moment(this.params.rangeDate[1]).format(Constants.FORMAT_DATE.YYYY_MM_DD) : null;
    newParams.targetUnit = newParams ? newParams.targetUnit?.orgId : null;
    this.service.search(newParams).subscribe(
      (response) => {
        this.dataTable = response?.data?.approvementConfig?.content || [];
        this.tableConfig.total = response?.data?.approvementConfig?.totalElements || 0;
        this.pageable.currentPage = this.params.pageNumber;
        this.preParam = newParams;
        (this.dataTable.length === 0 && this.params.pageNumber) > 0
          ? this.pageChange(this.params.pageNumber)
          : (this.isLoading = false);
        this.scrollY = Utils.setScrollYTable('table', this.pageable.totalElements! > 15 ? Constants.HeightPaginator : 0);
      },
      (e) => {
        this.isLoading = false;
        this.getMessageError(e);
        this.dataTable = [];
      }
    );
  }

  deleteItem(code: string) {
    if (this.isLoading) {
      return;
    }
    this.modalRef = this.modal.warning({
      nzTitle: this.translate.instant('development.appprovementConfig.messages.deleteTitle'),
      nzCancelText: this.translate.instant('development.consultation-config.messages.no'),
      nzOkText: this.translate.instant('development.consultation-config.messages.yes'),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        this.service.deleteItem(code).subscribe(
          () => {
            this.toastrCustom.success(this.translate.instant('development.appprovementConfig.messages.deleteSuccess'));
            this.search();
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
      },
    });
  }

  exportExcel() {
    if (this.dataTable?.length === 0) {
      this.toastrCustom.warning(
        this.translate.instant(
          'development.proposed-unit.messages.dataTableEmpty'
        )
      );
      return;
    }
    this.isLoading = true;
    delete this.preParam.pageNumber;
    delete this.preParam.pageSize;
    this.service.exportExcel(this.preParam).subscribe(
      (data) => {
        let urlExcel = 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,';
        urlExcel = urlExcel + data.data;
        FileSaver.saveAs(urlExcel, this.translate.instant('development.appprovementConfig.messages.excelFileName'));
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

  override ngOnDestroy() {
    this.modalRef?.destroy();
    this.subs.forEach((s) => s.unsubscribe());
  }

  cloneDetail(code: string) {
    this.router.navigate([this.router.url, 'clone'], {
      queryParams: {
        code: code,
        titleAppronemt: JSON.stringify(this.dataCommon?.title),
      },
      skipLocationChange: true,
    });
  }

  viewDetail(code: string) {
    this.router.navigate([this.router.url, 'update'], {
      queryParams: {
        code: code,
        titleAppronemt: JSON.stringify(this.dataCommon?.title),
      },
      skipLocationChange: true,
    });
  }

  onClickCreate() {
    this.router.navigate(['/development/approvement-ld-config/create'], {
      queryParams: {
        titleAppronemt: JSON.stringify(this.dataCommon?.title),
      },
      skipLocationChange: true,
    });
  }

  initTable() {
    this.tableConfig = {
      headers: [
        {
          title: 'development.approvement-config.table.issueLevelCode',
          field: 'issueLevel',
          tdClassList: ['text-nowrap', 'text-center'],
          width: 115,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.approvement-config.sign',
          field: '',
          tdTemplate: this.labelCanSign,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 80,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.approvement-config.approve',
          field: '',
          tdTemplate: this.labelCanApprove,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 80,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.approvement-config.table.employeeTitle',
          field: 'employeeTitle',
          fixed: true,
          fixedDir: 'left',
          width: 200,
        },
        {
          title: 'development.approvement-config.table.employeeFullName2',
          field: 'employeeFullName',
          width: 200,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.approvement-config.table.sourceUnit2',
          field: 'sourceUnitName',
          width: 200,
        },
        {
          title: 'development.approvement-config.table.targetUnit',
          field: 'targetUnitName',
          width: 200,
        },
        {
          title: 'development.approvement-config.table.inUnit',
          field: '',
          tdTemplate: this.isSameUnit,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 100,
        },
        {
          title: 'development.approvement-config.table.formCode',
          field: 'form',
          width: 150,
        },
        // {
        //   title: 'development.approvement-config.table.targetPositionCode',
        //   field: 'targetPosition',
        //   width: 200,
        // },
        {
          title: 'development.approvement-config.table.targetLevelCode',
          field: 'targetLevel',
          width: 200,
        },
        {
          title: 'development.approvement-config.table.targetTitleCode',
          field: 'targetTitle',
          width: 200,
        },
        {
          title: 'development.approvement-config.table.sameTitle',
          field: '',
          tdClassList: ['text-center'],
          tdTemplate: this.isSameTitle,
          width: 100,
        },
        {
          title: 'development.approvement-config.form.level',
          field: 'level',
          width: 100,
        },
        {
          title: 'development.approvement-config.table.sameLevel',
          field: '',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          tdTemplate: this.isSameLevel,
          width: 120,
        },
        {
          title: 'development.approvement-config.table.businessLineCode',
          field: 'businessLineName',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 150,
        },
        {
          title: 'development.approvement-config.form.areaList',
          field: 'area',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 100,
        },
        {
          title: 'development.approvement-config.form.listRegions',
          field: 'subArea',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 100,
        },
        {
          title: 'development.approvement-config.table.needConsultation',
          field: '',
          tdTemplate: this.hasConsultation,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 90,
        },
        {
          title: 'development.approvement-config.table.needInterview',
          field: '',
          tdTemplate: this.hasInterview,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 90,
        },
        {
          title: 'development.approvement-config.table.effectiveStartDate',
          field: '',
          tdTemplate: this.startDate,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 130,
        },
        {
          title: 'development.approvement-config.table.effectiveEndDate',
          field: '',
          tdTemplate: this.endDate,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 130,
        },
        {
          title: 'development.approvement-config.form.decisionNumber',
          field: 'decisionNumber',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 115,
        },
        {
          title: 'development.approvement-config.form.dateIssued',
          field: '',
          tdTemplate: this.decisionDate,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 130,
        },
        {
          title: ' ',
          field: 'action',
          tdClassList: ['text-nowrap','text-center'], thClassList: ['text-nowrap','text-center'],
          width: 50,
          tdTemplate: this.action,
          fixed: window.innerWidth > 1024,
          fixedDir: 'right',
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pageable.size,
      pageIndex: 1,
    };
  }

  importFile(){
    this.router.navigate([this.router.url, 'import'], {
      // skipLocationChange: true,
    });
  }
}
