import { ApprovementConfigActionComponent } from './approvement-config-action/approvement-config-action.component';
import { ApprovementConfigComponent } from './approvement-config-view/approvement-config.component';
import {ApprovementConfigImportComponent} from "./approvement-config-import/approvement-config-import.component";

export const pages = [ApprovementConfigComponent, ApprovementConfigActionComponent, ApprovementConfigImportComponent];

export * from './approvement-config-view/approvement-config.component';
export * from './approvement-config-action/approvement-config-action.component';
export * from './approvement-config-import/approvement-config-import.component';
