import { Component, Injector, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Constants, userConfig } from "@hcm-mfe/learn-development/data-access/common";
import { DataInterview, ParamSearchInterview } from "@hcm-mfe/learn-development/data-access/models";
import {InterviewService, ProposedUnitService} from "@hcm-mfe/learn-development/data-access/services";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import { MBTableConfig } from "@hcm-mfe/shared/data-access/models";
import { saveAs } from 'file-saver';
import * as moment from 'moment';
import { NzModalRef } from "ng-zorro-antd/modal";
import {NzUploadFile} from "ng-zorro-antd/upload";

@Component({
  selector: 'app-approvement-config-import',
  templateUrl: './approvement-config-import.component.html',
  styleUrls: ['./approvement-config-import.component.scss'],
})
export class ApprovementConfigImportComponent extends BaseComponent implements OnInit {
  dataTable: DataInterview[] = [];
  title?: string;
  modalRef?: NzModalRef;
  params: ParamSearchInterview = {
    code: null,
    proposeCode: null,
    fromDate: null,
    toDate: null,
    rangeDate: null,
    employeeCode: null,
    employeeName: null,
    currentUnit: null,
    currentTitle: null,
    proposeUnit: null,
    proposeTitle: null,
    interviewStatus: null,
    page: 0,
    size: 15,
  };

  //Checkbox
  isExpand = true;
  iconStatus = Constants.IconStatus.DOWN;
  tableConfig!: MBTableConfig;
  tableConfigError!: MBTableConfig;
  heightTable = { x: '100%', y: '30em'};
  fileImport: NzUploadFile[] = [];

  constructor(injector: Injector, private readonly interviewService: InterviewService, private readonly servicePropose: ProposedUnitService) {
    super(injector);
  }

  ngOnInit() {
    this.initTable();
    this.initTableError();
    this.title = this.route.snapshot?.data['pageName'];
    this.search(1);
  }

  search(firstPage: number) {
    this.isLoading = true;
    this.params.page = firstPage - 1;
    const searchParams = { ...this.params };
    const currentUnitId = searchParams?.currentUnit?.orgId || '';
    searchParams.currentUnit = currentUnitId;
    const proposeUnitId = searchParams?.proposeUnit?.orgId || '';
    searchParams.proposeUnit = proposeUnitId;
    if (searchParams?.rangeDate) {
      searchParams.fromDate =
      this.params.rangeDate && this.params.rangeDate[0] ? moment(searchParams.rangeDate[0]).format(Constants.FORMAT_DATE.YYYY_MM_DD) : null;
      searchParams.toDate =
      this.params.rangeDate && this.params.rangeDate[1] ? moment(searchParams.rangeDate[1]).format(Constants.FORMAT_DATE.YYYY_MM_DD) : null;
    }
    delete searchParams?.rangeDate;
    this.interviewService.searchInterview(searchParams).subscribe(
      (res) => {
        this.dataTable = res.data?.content;
        this.tableConfig.total = res?.data?.totalElements || 0;
        this.tableConfig.pageIndex = firstPage;
        this.tableConfigError.total = res?.data?.totalElements || 0;
        this.tableConfigError.pageIndex = firstPage;
        this.isLoading = false;
      },
      (e) => {
        this.isLoading = false;
        this.getMessageError(e);
        this.dataTable = [];
      }
    );
  }

  expandSearch() {
    this.isExpand = !this.isExpand;
    if (this.isExpand) {
      this.iconStatus = Constants.IconStatus.DOWN;
    } else {
      this.iconStatus = Constants.IconStatus.UP;
    }
  }

  exportExcel(isExcelInterview: boolean) {
    if (this.dataTable?.length === 0) {
      this.toastrCustom.warning(
        this.translate.instant(
          'development.proposed-unit.messages.dataTableEmpty'
        )
      );
      return;
    }
    this.isLoading = true;
    const searchParams = { ...this.params };
    const currentUnitId = searchParams?.currentUnit?.orgId || '';
    searchParams.currentUnit = currentUnitId;
    const proposeUnitId = searchParams?.proposeUnit?.orgId || '';
    searchParams.proposeUnit = proposeUnitId;
    if (searchParams?.rangeDate) {
      searchParams.fromDate =
      this.params.rangeDate && this.params.rangeDate[0] ? moment(searchParams.rangeDate[0]).format(Constants.FORMAT_DATE.YYYY_MM_DD) : null;
      searchParams.toDate =
      this.params.rangeDate && this.params.rangeDate[1] ? moment(searchParams.rangeDate[1]).format(Constants.FORMAT_DATE.YYYY_MM_DD) : null;
    }
    searchParams.isExportInterview = isExcelInterview;
    delete searchParams?.rangeDate;
    this.interviewService.exportExcelInterview(searchParams).subscribe(
      (data) => {
        let urlExcel = 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,';
        urlExcel = urlExcel + data.data;
        saveAs(urlExcel, 'Danh sách có lỗi.xlsx');
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

  downloadFile = (file: NzUploadFile) => {
    this.servicePropose.downloadFile(+file?.uid, file?.name).subscribe(
      () => {
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    )
  }

  beforeUpload = (file: File | NzUploadFile): boolean => {
    this.isLoading = true;
    const formData = new FormData();
    formData.append('files', file as File);
    formData.append('proposeDetailId', new Blob([JSON.stringify(1758)], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('type', new Blob(['statement'], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('groupProposeTypeId', new Blob(['1'], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    formData.append('isIn', new Blob(['true'], { type: Constants.CONTENT_TYPE.APPLICATION_JSON }));
    this.servicePropose.uploadFile(formData).subscribe(
      (res) => {
        this.fileImport = [
          ...this.fileImport,
          {
            uid: res.data?.docId,
            name: res.data?.fileName,
            status: 'done',
            id: res.data?.id,
          },
        ];
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    return false;
  };

  doRemoveFile = (file: NzUploadFile): boolean => {
    this.isLoading = true;
    if (this.fileImport.find((item) => item.uid === file.uid)){
      const idDelete = this.fileImport.find((item) => item.uid === file.uid)?.['id'];
      this.servicePropose.deleteUploadFile(+idDelete).subscribe(
        (res) => {
          this.fileImport = this.fileImport.filter((item) => item['id'] !== idDelete);
          this.isLoading = false;
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
        }
      );
    }
    return false;
  };

  initTable() {
    this.tableConfig = {
      headers: [
        {
          title: 'development.interview-config.colTable.planCode',
          field: 'planCode',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 100,
        },
        {
          title: 'development.interview-config.colTable.codeEmployee',
          field: 'employeeCode',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 120,
        },
        {
          title: 'development.interview-config.colTable.nameEmployee',
          field: 'employeeName',
          width: 200,
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
        },
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: userConfig.pageSize,
      pageIndex: 1,
    };
  }

  initTableError() {
    this.tableConfigError = {
      headers: [
        {
          title: 'development.interview-config.colTable.tdError',
          field: 'tdError',
          width: 60,
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
        },
        {
          title: 'development.interview-config.colTable.thError',
          field: 'employeeCode',
          width: 60,
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
        },
        {
          title: 'development.interview-config.colTable.errorDescription',
          field: 'employeeName',
          width: 250,
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
        },
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: userConfig.pageSize,
      pageIndex: 1,
    };
  }

}
