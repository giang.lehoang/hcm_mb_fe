import { Component, Injector, OnInit, TemplateRef } from '@angular/core';
import { Validators } from '@angular/forms';
import { cleanDataForm, Utils } from "@hcm-mfe/shared/common/utils";
import { SelectCheckAbleModal } from "@hcm-mfe/shared/ui/mb-select-check-able";
import {
  ApproveConfig,
  CategoryModel, checkArea,
  FormLDContent, JobDTO,
  JobDTOS, listFullName, IssueLevel,
} from "@hcm-mfe/learn-development/data-access/models";
import { NzModalRef } from 'ng-zorro-antd/modal';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {ApprovementConfigService} from "@hcm-mfe/learn-development/data-access/services";
import {ProposeMultipleEmployeeComponent} from "@hcm-mfe/learn-development/feature/propose-unit";
import { ScreenType } from 'libs/learn-development/data-access/common/src/lib/constants/common-constants';

@Component({
  selector: 'app-approvement-config-action',
  templateUrl: './approvement-config-action.component.html',
  styleUrls: ['./approvement-config-action.component.scss'],
})
export class ApprovementConfigActionComponent extends BaseComponent implements OnInit {
  dataCommon = {
    issueLevel: <IssueLevel[]>[],
    units: <CategoryModel[]>[],
    form: <FormLDContent[]>[],
    position: <CategoryModel[]>[],
    title: <JobDTOS[]>[],
    businessLine: <CategoryModel[]>[],
    targetLevel: <CategoryModel[]>[],
    levels: <CategoryModel[]>[],
    areas: <CategoryModel[]>[],
  };
  isSubmitted = false;
  form = this.fb.group({
    code: null,
    issueLevelCode: [null, Validators.required],
    sourceUnit: null,
    targetUnit: null,
    formCode: null,
    targetPositionCode: null,
    targetTitleCode: [],
    businessLineCode: null,
    businessLineName: null,
    targetLevelCode: [],
    canSign: false,
    canApprove: false,
    employeeCode: null,
    employeeFullName: null,
    employeeTitle: [null, Validators.required],
    employeeTitleName: null,
    effectiveStartDate: [null, Validators.required],
    effectiveEndDate: null,
    decisionDate: [null, Validators.required],
    decisionNumber: [null, Validators.required],
    subArea: null,
    area: null,
    level: [],
    hasConsultation: false,
    hasInterview: false,
    isSameLevel: null,
    isSameTitle: null,
    isSameUnit: null,
  });
  type?: ScreenType;
  title?: string;
  isLastIssueLevel = false;
  jobIds = '';
  modalRef?: NzModalRef;
  subArea?: checkArea[] = [];
  titleAppronemt: JobDTO[] = [];
  listTitleApproment: JobDTO [] = [];
  listNewTitle: JobDTO [] = [];
  listFullName: listFullName[] = [];
  checkOninit = false;
  isStarted = false;

  constructor(injector: Injector, private readonly service: ApprovementConfigService) {
    super(injector);
    this.isLoading = true;
  }

  ngOnInit() {
    this.type = this.route.snapshot?.data['type'] || ScreenType.Create;
    this.title = this.route.snapshot?.data['pageName'];
    const code: string = this.route.snapshot.queryParams['code'];
    this.titleAppronemt = JSON.parse(this.route.snapshot.queryParams['titleAppronemt']);
    this.service.getState().subscribe(
      (state) => {
        this.dataCommon = {
          businessLine: state?.businessLine || [],
          form: state?.form || [{ formCode: 'test', formName: 'test' }],
          issueLevel: state?.issueLevel || [],
          position: state?.position || [],
          title: state?.title || [],
          units: state?.units || [],
          targetLevel: state?.targetLevel || [],
          levels: state?.levels || [],
          areas: state?.areas || [],
        };
        this.dataCommon.issueLevel = Utils.orderBy(this.dataCommon.issueLevel, 'sortOrder');
        if (this.type !== ScreenType.Create) {
          this.service.findByCode(code).subscribe(
            (data) => {
              data.targetUnit = { orgId: data.targetUnitId!, orgName: data.targetUnitName! };
              data.sourceUnit = { orgId: data.sourceUnitId!, orgName: data.sourceUnitName! };
              data.effectiveEndDate = data.effectiveEndDate ? new Date(data.effectiveEndDate) : null;
              data.effectiveStartDate = data.effectiveStartDate ? new Date(data.effectiveStartDate) : null;
              //data targetTitleCode đang trả ra string[] cần parse sang number[] thì chức danh đích mới có thể hiển thị
              data.targetTitleCode =
                data?.targetTitleCode?.length! > 0
                  ? data?.targetTitleCode?.map((item: string | number) => parseInt(item.toString()))
                  : data?.targetTitleCode;
              data.targetLevelCode =
                data?.targetLevelCode?.length! > 0
                  ? data?.targetLevelCode?.map((item: string | number) => parseInt(item.toString()))
                  : data?.targetLevelCode;
              data.targetPositionCode = data?.targetPositionCode ? +data?.targetPositionCode : null;
               this.form.patchValue(data || {});
              this.getIssueLevel(data);
              this.isStarted = true;
              this.getEmployeeByJobIds(data);
              this.checkOninit = true;
              this.checksubArea(this.form.controls['area']?.value);
            },
            (e) => {
              this.isLoading = false;
              this.getMessageError(e);
            }
          );
        } else {
          this.isLoading = false;
        }

      },
      (e) => {
        this.isLoading = false;
        this.getMessageError(e);
      }
    );
  }

  ngAfterViewInit(): void {
    this.form.get('issueLevelCode')?.valueChanges.subscribe((value) => {
      const lastIssueLevel = this.dataCommon.issueLevel[this.dataCommon.issueLevel.length - 1];
      if ( value && lastIssueLevel && lastIssueLevel.code === value) {
        this.form.get('targetUnit')?.setValue(this.form.get('sourceUnit')?.value);
        this.form.get('targetUnit')?.disable();
        this.isLastIssueLevel = true;
      } else {
        this.form.get('targetUnit')?.enable();
        this.isLastIssueLevel = false;
      }
    });
    this.form.get('sourceUnit')?.valueChanges.subscribe((value) => {
      if (this.isLastIssueLevel) {
        this.form.get('targetUnit')?.patchValue(value);
      }
    });
  }

  save() {
    this.isSubmitted = true;
    const data: ApproveConfig = { ...cleanDataForm(this.form) };
    if (this.form.invalid || this.isLoading || !this.checkValidate()) {
      return;
    }
    data.employeeFullName = this.listFullName.find((item) => item?.empCode! === data.employeeCode)?.fullName;
    data.businessLineName = this.dataCommon.businessLine.find((item) => item.code === data.businessLineCode)?.name;
    data.employeeTitleName = this.titleAppronemt.find((item) => item?.jobId === data.employeeTitle)?.jobName;
    data.targetUnitId = data.targetUnit?.orgId;
    data.sourceUnitId = data.sourceUnit?.orgId;
    data.targetUnitName = data.targetUnit?.orgName;
    this.isLoading = true;
    let api: any;
    if (this.type === ScreenType.Create) {
      api = this.service.create(data);
    } else if (this.type === ScreenType.Update) {
      api = this.service.update(data);
    } else if (this.type === ScreenType.Clone) {
      delete data.code
      api = this.service.create(data);
    }
    api.subscribe(
      () => {
        if (this.type === ScreenType.Create) {
          this.toastrCustom.success(this.translate.instant('development.appprovementConfig.messages.addSuccess'));
        } else if (this.type === ScreenType.Update) {
          this.toastrCustom.success(this.translate.instant('development.appprovementConfig.messages.updateSuccess'));
        } else if (this.type === ScreenType.Clone){
          this.toastrCustom.success(this.translate.instant('development.appprovementConfig.messages.cloneSuccess'));
        }
        this.router.navigateByUrl('/development/approvement-ld-config');
        this.isLoading = false;
      },
      (e: any) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

  setEmpNullValue() {
    this.form.patchValue({ employeeFullName: '', employeeTitle: null, employeeTitleName: '' });
  }

  checkValidate() {
    const data = this.form.getRawValue();
    if (
      Utils.isEmpty(data.issueLevelCode) &&
      Utils.isEmpty(data.effectiveStartDate) &&
      Utils.isEmpty(data.decisionNumber) &&
      Utils.isEmpty(data.decisionDate) &&
      Utils.isEmpty(data.employeeTitle)
    ) {
      this.toastrCustom.warning(this.translate.instant('development.approvement-config.messages.validate1'));
      return false;
    }
    if (!data.canSign && !data.canApprove) {
      this.toastrCustom.warning(this.translate.instant('development.approvement-config.messages.validate2'));
      return false;
    }
    return true;
  }

  emitValue(item: SelectCheckAbleModal, field: string) {
    this.form.controls[field].setValue(item.listOfSelected);
  }

  disabledDecisionDate = (decisionDate: Date): boolean => {
    return decisionDate.getTime() >= Date.now();
  };

  disabledStartDate = (effectiveStartDate: Date): boolean => {
    const effectiveEndDate = this.form.get('effectiveEndDate')?.value;
    if (!effectiveStartDate || !effectiveEndDate) {
      return false;
    }
    return effectiveStartDate.getTime() > effectiveEndDate.getTime();
  };

  disabledEndDate = (effectiveEndDate: Date): boolean => {
    const effectiveStartDate = this.form.get('effectiveStartDate')?.value;
    if (!effectiveEndDate || !effectiveStartDate) {
      return false;
    }
    return effectiveEndDate.getTime() <= effectiveStartDate.getTime();
  };


  getIssueLevel(data? : any){
    const code = this.form.controls['issueLevelCode'].value;
    const sign = this.form.controls['canSign'].value;
    const approve = this.form.controls['canApprove'].value;
    this.service.getIssueLevelByCode(code, sign, approve).subscribe((res) =>
    {
      const listTitle = res?.data?.split(',');
      this.listNewTitle = this.titleAppronemt.filter((item) => listTitle?.includes(item?.jobId!.toString()))
      const jobNewIdTitle = this.listNewTitle.map((item) => {
        return item.jobId
      });
    });
  }

  getEmployeeByJobIds(dataEmp?: ApproveConfig) {
    this.isLoading = true;
    const jobId = this.form.controls['employeeTitle'].value;
    const orgId = 2;
      this.service.getEmployeeByJobIds(jobId, orgId).subscribe(
      (emp) => {
        if (!this.isStarted) {
          this.form.get('employeeCode')?.setValue(null);
        }
        this.listFullName = emp?.data?.listData;
        if (emp?.data?.listData?.length === 1) {
          this.form.patchValue({
            employeeFullName: emp?.data?.listData[0].fullName,
            employeeTitle: emp?.data?.listData[0].jobId,
            employeeTitleName: emp?.data?.listData[0].jobName,
            employeeCode: emp?.data?.listData[0].empCode,
          });
        }
        this.isStarted = false;
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

  showModalPropose(footerTmplSingle: TemplateRef<any>, mode: string, data?: any) {
    this.modalRef = this.modal.create({
      nzTitle: 'Tìm kiếm nhân viên',
      nzWidth: this.getInnerWidth(),
      nzContent: ProposeMultipleEmployeeComponent,
      nzComponentParams: {
        isEmp: true,
      },
      nzFooter: footerTmplSingle,
    });
    this.modalRef.afterClose.subscribe((result) => {
      if (result?.length > 0) {
        this.form.patchValue({
          employeeFullName: result[0]?.fullName,
          employeeTitle: result[0]?.jobId,
          employeeTitleName: result[0]?.jobName,
          employeeCode: result[0]?.empCode,
        });
      }
    });
  }

  getInnerWidth(): number {
    if (window.innerWidth > 767) {
      return window.innerWidth / 1.5 > 1368 ? 1368 : window.innerWidth / 1.3;
    }
    return window.innerWidth;
  }

  checksubArea(code: number){
    this.isLoading = true;
    this.service.getSubAreaByCode(code).subscribe((res) => {
      this.subArea = res?.data;
      if(!this.checkOninit){
        this.form.controls['subArea'].setValue(null)
      }
      this.isLoading = false;
      this.checkOninit = false;
    },
    (e) => {
      this.getMessageError(e);
      this.isLoading = false;
    });
  }

}
