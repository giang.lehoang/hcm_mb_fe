import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ApprovementProposeKsptnlComponent } from './pages';
import {
  ProposedAppointmentRenewalCreateComponent,
  ProposedUnitHandleComponent,
  ProposeTGCVComponent
} from "@hcm-mfe/learn-development/feature/propose-unit";
import {ScreenType} from "@hcm-mfe/learn-development/data-access/common";

const routes: Routes = [
  { path: '', component: ApprovementProposeKsptnlComponent },
  {
    path: 'detail',
    component: ProposedUnitHandleComponent,
    data: {
      pageName: 'development.pageName.approvement-propose-detail',
      breadcrumb: 'development.breadcrumb.approvement-propose-detail',
      screenType: ScreenType.Detail,
    },
  },
  {
    path: 'approve',
    component: ProposedUnitHandleComponent,
    data: {
      pageName: 'development.pageName.approvement-propose-approve',
      breadcrumb: 'development.breadcrumb.approvement-propose-approve',
      screenType: ScreenType.Detail,
    },
  },

  {
    path: 'handle',
    component: ProposedUnitHandleComponent,
    data: {
      pageName: 'development.pageName.approvement-propose-handle',
      breadcrumb: 'development.breadcrumb.approvement-propose-handle',
      screenType: ScreenType.Update,
    },
  },
  {
    path: 'handle/element-of-group',
    component: ProposedUnitHandleComponent,
    data: {
      pageName: 'development.pageName.approvement-propose-handle',
      breadcrumb: 'development.breadcrumb.approvement-propose-handle',
      screenType: ScreenType.Update,
      handlePropose: true,
    },
  },
  {
    path: 'detail-tgcv',
    component: ProposeTGCVComponent,
    data: {
      pageName: 'development.pageName.approvement-propose-detail',
      breadcrumb: 'development.breadcrumb.approvement-propose-detail',
      screenType: ScreenType.Detail,
      handlePropose: true,
    },
  },
  {
    path: 'detail-appointment-renewal',
    component: ProposedAppointmentRenewalCreateComponent,
    data: {
      pageName: 'development.pageName.approvement-propose-detail',
      breadcrumb: 'development.breadcrumb.approvement-propose-detail',
      screenType: ScreenType.Detail,
    },
  },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ApprovementProposeKSPTNLRoutingModule {}
