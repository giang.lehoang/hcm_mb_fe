import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {LearnDevelopmentFeatureShellModule} from "@hcm-mfe/learn-development/feature/shell";
import {LearnDevelopmentFeatureProposeUnitModule} from "@hcm-mfe/learn-development/feature/propose-unit";
import {ApprovementProposeKSPTNLRoutingModule} from "./approvement-propose-ksptnl-routing.module";
import {pages} from "./pages";

@NgModule({
  declarations: [...pages],
  imports: [
    LearnDevelopmentFeatureShellModule,
    LearnDevelopmentFeatureProposeUnitModule,
    ApprovementProposeKSPTNLRoutingModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class LearnDevelopmentFeatureApprovementProposeKsptnlModule {}
