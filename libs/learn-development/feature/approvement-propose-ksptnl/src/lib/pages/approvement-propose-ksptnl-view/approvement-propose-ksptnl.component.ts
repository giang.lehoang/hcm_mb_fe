import { Component, Injector, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Constants, maxInt32, ScreenType, userConfig, Utils } from "@hcm-mfe/learn-development/data-access/common";
import { ListApprove, Proposed, StatusList } from "@hcm-mfe/learn-development/data-access/models";
import { ApprovementProposeService, ProposedUnitService } from "@hcm-mfe/learn-development/data-access/services";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import { cleanData } from "@hcm-mfe/shared/common/utils";
import { Category, MBTableConfig, Pageable, Pagination } from "@hcm-mfe/shared/data-access/models";
import * as FileSaver from 'file-saver';
import * as moment from 'moment';
import { NzSafeAny } from "ng-zorro-antd/core/types";
import { NzModalRef } from 'ng-zorro-antd/modal';
import { forkJoin, Subscription } from 'rxjs';
import {
  ProposePopupMergeComponent
} from "../../../../../propose-unit/src/lib/components/propose-popup-merge/propose-popup-merge.component";




@Component({
  selector: 'app-approvement-propose-ksptnl',
  templateUrl: './approvement-propose-ksptnl.component.html',
  styleUrls: ['./approvement-propose-ksptnl.component.scss'],
})
export class ApprovementProposeKsptnlComponent extends BaseComponent implements OnInit {
  params = {
    proposeType: null,
    formGroup: null,
    proposeCode: null,
    rangeDate: null,
    rangePTNLDate: null,
    proposeStartDate: null,
    proposeEndDate: null,
    approveStartDate: null,
    approveEndDate: null,
    issueLevelCode: null,
    currentUnit: null,
    currentTitle: null,
    proposeUnit: null,
    proposeTitle: null,
    conditionViolation: null,
    interviewStatus: null,
    consultationStatus: null,
    employeeCode: null,
    employeeName: null,
    optionNote: null,
    approvalFullName: null,
    approve: <string | null>null,
    status: null,
    screenCode: null,
    statusPublish: null,
    proposeItem: true,
    proposeOrigin: true,
    proposeGroup: true,
    flag: false,
    size: userConfig.pageSize,
    page: 0,
  };

  prevParams = { ...this.params };

  dataCommon = {
    statusList: <Category[]>[],
    jobDTOS: <Category[]>[],
    typeDTOS: <Category[]>[],
    formGroupList: <Category[]>[],
    issueLevelList: <Category[]>[],
    flowStatusApproveConfigList: <StatusList[]>[],
    flowStatusPublishConfigList: <StatusList[]>[],
    conditionViolationList: <StatusList[]>[],
    interviewStatus: <StatusList[]>[],
    consultationStatus: <StatusList[]>[],
  };

  pageable: Pageable = {
    totalElements: 0,
    currentPage: 0,
    size: userConfig.pageSize,
  };
  scrollY = '';
  proposeUnitScreen = false;
  proposeUnitApprove = false;
  titlePage = '';
  isExpand = true;
  iconStatus = Constants.IconStatus.DOWN;
  type?: ScreenType;
  dataTable: Proposed[] = [];
  checked = false;
  indeterminate = false;
  setOfCheckID = new Set<number>();
  showTickIcon = false;
  statusApprove = '';
  statusApproval: Category[] = [];
  statusPublish: Category[] = [];
  screenCode = this.translate.instant('development.screenCode.KSPTNL');
  modalRef?: NzModalRef;
  private readonly subs: Subscription[] = [];
  listChecked: Proposed[] = [];
  rolesOfCurrentUser: string[] = [];

  quantityMap: Map<number, Proposed> = new Map();
  tableConfig!: MBTableConfig;
  pagination = new Pagination();
  heightTable = { x: '100%', y: '25em'};
  @ViewChild('checkboxHeader', {static: true}) checkboxHeader!: TemplateRef<NzSafeAny>;
  @ViewChild('checkbox', {static: true}) checkbox!: TemplateRef<NzSafeAny>;
  @ViewChild('checkPropose', {static: true}) checkPropose!: TemplateRef<NzSafeAny>;
  @ViewChild('interView', {static: true}) interView!: TemplateRef<NzSafeAny>;
  @ViewChild('salary', {static: true}) salary!: TemplateRef<NzSafeAny>;
  @ViewChild('consultation', {static: true}) consultation!: TemplateRef<NzSafeAny>;
  @ViewChild('statusDisplayView', {static: true}) statusDisplayView!: TemplateRef<NzSafeAny>;
  @ViewChild('sentDateView', {static: true}) sentDateView!: TemplateRef<NzSafeAny>;
  @ViewChild('ptnlSendApproveView', {static: true}) ptnlSendApproveView!: TemplateRef<NzSafeAny>;
  @ViewChild('statusPublishDisplayView', {static: true}) statusPublishDisplayView!: TemplateRef<NzSafeAny>;


  constructor(
    injector: Injector,
    private readonly service: ApprovementProposeService,
    public pUnitService: ProposedUnitService
  ) {
    super(injector);
  }

  gray = false;
  green = false;
  red = false;
  yellow = false;

  displayStatus(statusDisplay: string) {
    if (statusDisplay.includes('Chờ') || statusDisplay.includes('Đang')) {
      return 'yellow';
    } else if (statusDisplay.includes('Đã') || statusDisplay.includes('đã')) {
      if (statusDisplay.includes('hủy')) {
        return 'red';
      } else {
        return 'green';
      }
    } else if (statusDisplay.includes('Hủy') || statusDisplay.includes('Từ chối')) {
      return 'red';
    } else if (statusDisplay.includes('Đồng ý') || statusDisplay.includes('đồng ý')) {
      if (statusDisplay.includes('Không') || statusDisplay.includes('không')) {
        return 'red';
      } else {
        return 'green';
      }
    } else if (statusDisplay.includes('Thiếu')) {
      return 'yellow';
    } else {
      return 'gray';
    }
  }

  updateCheckedSet(item: Proposed, checked: boolean): void {
    if (checked) {
      this.quantityMap.set(item.indexFix!, item);
    } else {
      this.quantityMap.delete(item.indexFix!);
    }
  }

  onItemChecked(item: Proposed, checked: boolean): void {
    this.updateCheckedSet(item, checked);
    this.refreshCheckedStatus();
  }

  onItemChecked2(indexFix: number, checked: boolean): void {
    const item = this.dataTable.find((item: Proposed) => item.indexFix === indexFix);
    this.updateCheckedSet(item!, checked);
    this.refreshCheckedStatus();
  }

  onAllChecked(value: boolean): void {
    const list = this.dataTable.filter((item) => this.displayMergeCB(item));
    list.forEach((item) => this.updateCheckedSet(item, value));
    this.refreshCheckedStatus();
    this.checkNumMerge();
  }

  refreshCheckedStatus(): void {
    const list = this.dataTable.filter((item) => this.displayMergeCB(item));
    this.checked = list.every(({ indexFix }) => this.quantityMap.has(indexFix!));
    this.indeterminate = list.some(({ indexFix }) => this.quantityMap.has(indexFix!)) && !this.checked;
  }

  checkNumMerge() {
    if (this.quantityMap.size > 0) {
      return false;
    } else {
      return true;
    }
  }

  checkProposeCode(indexFix: number) {
    const item = this.dataTable.find((item: Proposed) => item.indexFix === indexFix);
    switch (item?.proposeCategory) {
      case Constants.ProposeCategory.ORIGINAL:
        return item.proposeId;
      case Constants.ProposeCategory.ELEMENT:
        return item.proposeDetailId;
      case Constants.ProposeCategory.GROUP:
        return item.proposeId;
      default:
        return null;
    }
  }

  search(pageIndex: number) {
    this.isLoading = true;
    this.params.page = pageIndex - 1;
    const newParams = { ...cleanData(this.params) };
    newParams.proposeStartDate =
      this.params.rangeDate && this.params.rangeDate[0] ? moment(newParams.rangeDate[0]).format(Constants.FORMAT_DATE.YYYY_MM_DD) : null;
    newParams.proposeEndDate =
      this.params.rangeDate && this.params.rangeDate[1] ? moment(this.params.rangeDate[1]).format(Constants.FORMAT_DATE.YYYY_MM_DD) : null;
    newParams.sendApproveStartDate =
      this.params.rangePTNLDate && this.params.rangePTNLDate[0] ? moment(newParams.rangePTNLDate[0]).format(Constants.FORMAT_DATE.YYYY_MM_DD) : null;
    newParams.sendApproveEndDate =
      this.params.rangePTNLDate && this.params.rangePTNLDate[1] ? moment(this.params.rangePTNLDate[1]).format(Constants.FORMAT_DATE.YYYY_MM_DD) : null;
    const currentUnitID = newParams?.currentUnit?.orgId || '';
    newParams.currentUnit = currentUnitID;
    const proposeUnitID = newParams?.proposeUnit?.orgId || '';
    newParams.proposeUnit = proposeUnitID;

    if (this.params.statusPublish !== undefined) {
      const publishSingle = this.dataCommon.flowStatusPublishConfigList.find(
        (i) => i.key === this.params.statusPublish
      );
      const publishMulti = this.dataCommon.flowStatusPublishConfigList.filter((i) => i.role === publishSingle?.role);
      const keyPublishMap = publishMulti.map((i) => i.key).toString();
      newParams.statusPublish = keyPublishMap;
    } else {
      newParams.statusPublish = null;
    }
    newParams.screenCode = this.screenCode;
    this.service.search(newParams).subscribe(
      (res) => {
        this.isLoading = false;
        this.dataTable = res?.data?.content || [];
        let indexFix = 1 + this.params.page * this.params.size;

        this.dataTable = this.dataTable.map((item) => {
          return {
            ...item,
            indexFix: this.dataTable.length === 1 && this.quantityMap.has(1) ? maxInt32 : indexFix++,
          };
        });
        this.checked = true;
        this.dataTable?.forEach((item) => {
          if (!this.quantityMap.has(item?.indexFix!)) {
            this.checked = false;
          } else {
            this.quantityMap.set(item.indexFix!, item);
          }
        });
        if (!this.dataTable?.length) {
          this.checked = false;
        }
        this.tableConfig.total = res?.data?.totalElements || 0;
        this.pageable.currentPage = this.params.page;
        this.tableConfig.pageIndex = pageIndex;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
        this.dataTable = [];
      }
    );
  }

  checkGroupProposeType(groupProposeType: string) {
    if (groupProposeType === this.translate.instant('development.proposed-unit.table.proposeMerge')) {
      return 0;
    } else if (groupProposeType === this.translate.instant('development.proposed-unit.table.proposeOriginal')) {
      return 1;
    } else if (groupProposeType === this.translate.instant('development.proposed-unit.table.proposeComponents')) {
      return 2;
    }
    return null;
  }

  mergePropose() {
    if (this.quantityMap.size === 0) {
      this.toastrCustom.error(this.translate.instant('modelOrganization.planYear.chooseMin'));
      return;
    }
    let listPropose: Proposed[] = [];
    this.quantityMap.forEach((item) => {
      if (item.groupProposeTypeId === 1 || item.groupProposeTypeId === 0) {
        delete item.proposeDetailId;
      } else {
        delete item.proposeId;
      }
      listPropose.push(item);
    });

    const listStatusMerge = ['7', '9', '10A'];
    const statusItem = listStatusMerge.find((item) => item === listPropose[0].flowStatusCode);
    if (statusItem) {
      const listProposeStatus = listPropose.filter((item) => item.flowStatusCode === statusItem);
      if (listProposeStatus?.length !== listPropose.length) {
        this.toastrCustom.error(this.translate.instant('development.commonMessage.notTheSameStatus'));
        return;
      }
    } else {
      this.toastrCustom.error(this.translate.instant('development.commonMessage.notTheSameStatus'));
      return;
    }

    listPropose = listPropose?.map((item) => {
      return {
        proposeTypeCode: item?.formGroup,
        groupProposeTypeId: item?.groupProposeTypeId,
        flow: item?.flowStatusCode,
        issueLevelCode: item?.issueLevelCode,
        approvalCode: item?.approvalCode,
        proposeId: item?.proposeId,
        proposeDetailId: item?.proposeDetailId,
        createdBy: item?.createdBy,
        isGroup: item?.isGroup,
        ksptnlIsGroup: item?.groupByKSPTNL,
      };
    });

    this.modalRef = this.modal.create({
      nzTitle: this.translate.instant('development.proposed-unit.messages.mergeConfirm'),
      nzContent: ProposePopupMergeComponent,
      nzFooter: null,
    });
    this.modalRef!.componentInstance.eventoptionNotes.subscribe((optionNotes: string) => {
      this.isLoading = true;
      const screeenCode = Constants.ScreenCode.KSPTNL
      this.pUnitService.mergePropose(listPropose, screeenCode, optionNotes).subscribe(
        (res) => {
          this.isLoading = false;
          this.modalRef?.destroy();
          this.toastrCustom.success(this.translate.instant('development.proposed-unit.messages.mergeSuccess'));
          this.quantityMap.clear();
          this.search(1);
        },
        (e) => {
          this.getMessageError(e);
          this.isLoading = false;
          this.modalRef?.destroy();
        }
      );
    });
  }

  detail(item: Proposed) {
    if (item.proposeCategory === Constants.ProposeCategory.ELEMENT) {
      this.pUnitService.viewParent(item.proposeDetailId || 0).subscribe((res) => {
        const queryParams = {
          id: item.proposeId,
          proposeDetailId: item.proposeDetailId,
          proposeCategory: item.proposeCategory,
          flowStatusCode: item.flowStatusCode,
          relationType: item.relationType,
          parentID:
            item.isGroup || item.groupByKSPTNL
              ? res.data?.groupId
              : res.data?.parentId,
        };
        if (item?.formGroup) {
          if (+item?.formGroup === Constants.FormGroupList.TGCV) {
            this.router.navigate([this.router.url, Constants.SubRouterLink.DETAILTGCV], {
              queryParams: queryParams,
              skipLocationChange: true,
            });
          } else if (+item?.formGroup === Constants.FormGroupList.BO_NHIEM) {
            this.router.navigate([this.router.url, Constants.TypeScreenHandle.DETAIL_APPOINTMENT], {
              queryParams: queryParams,
              skipLocationChange: true,
            });
          } else {
            this.router.navigate([this.router.url, Constants.SubRouterLink.DETAIL], {
              queryParams: queryParams,
              skipLocationChange: true,
            });
          }
        }
      });
    } else {
      this.router.navigate([this.router.url, Constants.SubRouterLink.DETAIL], {
        queryParams: {
          id: item.proposeId,
          proposeDetailId: item.proposeDetailId,
          proposeCategory: item.proposeCategory,
          flowStatusCode: item.flowStatusCode,
          relationType: item.relationType,
        },
        skipLocationChange: true,
      });
    }
  }

  expandSearch() {
    this.isExpand = !this.isExpand;
    if (this.isExpand) {
      this.iconStatus = Constants.IconStatus.DOWN;
    } else {
      this.iconStatus = Constants.IconStatus.UP;
    }
    setTimeout(() => {
      this.scrollY = Utils.setScrollYTable('table', this.pageable.totalElements! > 15 ? Constants.HeightPaginator : 0);
    },100);
  }

  //check hiển thị checkbox gộp
  displayMergeCB(item: Proposed) {
    return item?.flowStatusCode === Constants.FlowCodeConstants.CHODUYET7 && (!item?.groupByKSPTNL || !item?.isGroup);
  }

  showCheckBox(indexFix: number) {
    const item = this.dataTable.find((item: Proposed) => item.indexFix === indexFix);
    return item?.flowStatusCode === Constants.FlowCodeConstants.CHODUYET7 && (!item?.groupByKSPTNL || !item?.isGroup);
  }


  ngOnInit() {
    this.initTable();
    this.titlePage = this.route.snapshot?.data['pageName'];
    this.isLoading = true;
    const srcCodeParams = {
      screenCode: this.screenCode,
    };
    const sub = forkJoin([
      this.service.getState(srcCodeParams),
      this.service.getRolesByUsername(),
    ])
    .subscribe(
      ([data , roles]) => {
        if (data) {
          this.dataCommon = data;
          this.dataCommon.flowStatusApproveConfigList = this.convertStatus(data?.flowStatusApproveConfigList);
          this.params.approve = '7';
          this.dataCommon.flowStatusPublishConfigList = this.convertStatus(data?.flowStatusPublishConfigList);
        }
        this.rolesOfCurrentUser = roles;
        this.search(1);
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }

  exportExcel() {
    if (this.dataTable?.length === 0) {
      this.toastrCustom.warning(
        this.translate.instant(
          'development.proposed-unit.messages.dataTableEmpty'
        )
      );
      return;
    }
    this.isLoading = true;
    const newParams = { ...cleanData(this.params) };
    delete newParams.page;
    delete newParams.size;
    newParams.proposeStartDate =
      this.params.rangeDate && this.params.rangeDate[0] ? moment(newParams.rangeDate[0]).format(Constants.FORMAT_DATE.YYYY_MM_DD) : null;
    newParams.proposeEndDate =
      this.params.rangeDate && this.params.rangeDate[1] ? moment(this.params.rangeDate[1]).format(Constants.FORMAT_DATE.YYYY_MM_DD) : null;
    newParams.currentUnit = newParams?.currentUnit?.orgId || '';
    newParams.proposeUnit = newParams?.proposeUnit?.orgId || '';

    if (this.params.statusPublish) {
      const publishSingle = this.dataCommon.flowStatusPublishConfigList.find(
        (i) => i.key === this.params.statusPublish
      );
      const publishMulti = this.dataCommon.flowStatusPublishConfigList.filter((i) => i.role === publishSingle?.role);
      newParams.statusPublish = publishMulti.map((i) => i.key).toString();
    } else {
      newParams.statusPublish = null;
    }
    newParams.screenCode = this.screenCode;
    this.pUnitService.exportExcel(newParams).subscribe(
      (data) => {
        let urlExcel = 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,';
        urlExcel = urlExcel + data.data;
        FileSaver.saveAs(urlExcel, this.translate.instant('development.proposed-unit.messages.excelKSPTNL'));
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }

  convertStatus(valueStatus: any) {
    let arrayStatus: any[] = [];
    if (valueStatus) {
      const textStatusConsultation = [
        ...new Set(
          valueStatus.map((item: any) => {
            return item.role;
          })
        ),
      ];
      textStatusConsultation.forEach((item) => {
        let filterValueStatus = valueStatus
          .filter((i: any) => i.role === item)
          ?.map((a: any) => {
            return a.key;
          });
        arrayStatus.push({ key: filterValueStatus.toString(), role: item });
      });
    }
    return arrayStatus;
  }

  override ngOnDestroy() {
    this.subs.forEach((s) => s.unsubscribe());
  }

  initTable() {
    this.tableConfig = {
      headers: [
        {
          title: 'development.approvement-propose.search.merge',
          field: 'formCode',
          fixed: true,
          fixedDir: 'left',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          thClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
          width: 60,
          thTemplate: this.checkboxHeader,
          tdTemplate: this.checkbox,
        },
        {
          title: 'development.proposed-unit.table.handleCode',
          field: 'handleCode',
          width: 90,
          fixed: true,
          fixedDir: 'left',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
        },
        {
          title: 'development.promulgate.search.proposeCode',
          field: 'proposeCode',
          width: 100,
          fixed: true,
          fixedDir: 'left',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
        },
        {
          title: 'development.proposed-unit.table.contentPlan',
          field: 'optionNote',
          width: 150,
          fixed: true,
          fixedDir: 'left',
          tdClassList: [Constants.TABLE_CONFIG.TEXT_CENTER],
        },
        {
          title: 'development.proposed-unit.table.groupForm',
          field: 'formGroupName',
          width: 150,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.approvement-issue.search.form',
          field: 'formRotationName',
          width: 110,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.proposed-unit.table.empCode',
          field: 'employeeCode',
          width: 110,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.proposed-unit.table.empName',
          field: 'employeeName',
          width: 170,
          fixed: true,
          fixedDir: 'left',
        },
        {
          title: 'development.proposed-unit.table.currentUnit',
          field: 'employeeUnitPathName',
          width: 170,
        },
        {
          title: 'development.proposed-unit.table.currentTitle',
          field: 'employeeTitleName',
          width: 170,
        },
        {
          title: 'development.proposed-unit.table.proposeUnit',
          field: 'contentUnitPathName',
          width: 170,
        },
        {
          title: 'development.proposed-unit.table.proposeTitle',
          field: 'contentTitleName',
          width: 170,
        },
        {
          title: 'development.proposed-unit.table.resultCheckCondition',
          field: 'conditionViolation',
          width: 170,
        },
        {
          title: 'development.proposed-unit.table.consultationCondition',
          field: 'consultationStatus',
          width: 170,
          tdTemplate: this.consultation,
        },
        {
          title: 'development.proposed-unit.table.interviewCondition',
          field: 'interviewStatus',
          width: 170,
          tdTemplate: this.interView,
        },
        // {
        //   title: 'development.proposed-unit.table.salaryStatus',
        //   field: 'salaryStatus',
        //   width: 170,
        //   tdTemplate: this.salary,
        // },
        {
          title: 'development.proposed-unit.table.statusApprove',
          field: 'statusDisplay',
          width: 170,
          tdTemplate: this.statusDisplayView,
        },
        {
          title: 'development.proposed-unit.table.noteWithdraw',
          field: 'withdrawNote',
          width: 170,
        },
        // {
        //   title: 'development.proposed-unit.table.proposeTypeName',
        //   field: 'proposeTypeName',
        //   width: 170,
        // },
        // {
        //   title: 'development.proposed-unit.table.proposeTypeGroup',
        //   field: 'groupProposeType',
        //   width: 170,
        // },
        {
          title: 'development.proposed-unit.table.proposeTypePerson',
          field: 'createdBy',
          width: 170,
        },
        {
          title: 'development.proposed-unit.table.sentDate',
          field: 'sentDate',
          width: 170,
          tdTemplate: this.sentDateView,
        },
        {
          title: 'development.proposed-unit.table.dateOfPTNL',
          field: 'ptnlSendApprove',
          width: 170,
          tdTemplate: this.ptnlSendApproveView,
        },
        {
          title: 'development.promulgate.search.issueLevelCode',
          field: 'issueLevelName',
          width: 150,
        },
        {
          title: 'development.approvement-propose.search.approvePerson',
          field: 'approvalFullName',
          width: 150,
        },
        {
          title: 'development.approvement-propose.search.handlePerson',
          field: 'lastUpdatedBy',
          width: 150,
        },
        {
          title: 'development.proposed-unit.table.statusPublish',
          field: 'statusPublishDisplay',
          width: 170,
          tdTemplate: this.statusPublishDisplayView,
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: userConfig.pageSize,
      pageIndex: 1,
    };
  }
}
