import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ScreenType} from "@hcm-mfe/learn-development/data-access/common";
import {ConsultationConfigActionComponent, ConsultationConfigViewComponent} from "./pages";


const routes: Routes = [
  {
    path: '',
    component: ConsultationConfigViewComponent,
  },
  {
    path: 'create',
    component: ConsultationConfigActionComponent,
    data: {
      pageName: 'development.pageName.consultation-config-create',
      breadcrumb: 'development.breadcrumb.consultation-config-create',
      screenType: ScreenType.Create,
    },
  },
  {
    path: 'update',
    component: ConsultationConfigActionComponent,
    data: {
      pageName: 'development.pageName.consultation-config-update',
      breadcrumb: 'development.breadcrumb.consultation-config-update',
      screenType: ScreenType.Update,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConsultationConfigRoutingModule {}
