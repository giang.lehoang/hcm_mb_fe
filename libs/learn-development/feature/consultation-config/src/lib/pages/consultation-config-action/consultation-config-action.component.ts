import { Component, Injector, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { Constants, ScreenType } from "@hcm-mfe/learn-development/data-access/common";
import { DataCommonConsulation } from "@hcm-mfe/learn-development/data-access/models";
import { ConsultationConfigService } from "@hcm-mfe/learn-development/data-access/services";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import { cleanDataForm, Utils } from "@hcm-mfe/shared/common/utils";
import { SelectCheckAbleModal } from "@hcm-mfe/shared/ui/mb-select-check-able";
import * as moment from 'moment';
import { Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-consultation-config-action',
  templateUrl: './consultation-config-action.component.html',
  styleUrls: ['./consultation-config-action.component.scss'],
})
export class ConsultationConfigActionComponent extends BaseComponent implements OnInit, OnDestroy {
  type?: string;
  dataCommon: DataCommonConsulation = {};
  form?: FormGroup;
  title?: string;
  isSubmitted = false;
  id?: string | null;
  subs: Subscription[] = [];

  constructor(injector: Injector, private readonly service: ConsultationConfigService) {
    super(injector);
    this.initForm();
  }
  ngOnInit() {
    this.isLoading = true;

    this.type = this.route.snapshot?.data['screenType'] || ScreenType.Create;
    this.title = this.route.snapshot?.data['pageName'];
    this.id = this.route.snapshot?.queryParams['id'];
    if (this.type === 'CREATE') {
      this.service.getStateConsultation().subscribe((res) => {
        this.dataCommon = res;
        this.isLoading = false;
      });
    } else {
      this.service
        .getStateConsultation()
        .pipe(
          switchMap((res) => {
            this.dataCommon = res;
            return this.service.findById(this.id!);
          })
        )
        .subscribe(
          (res) => {
            res.data.currentUnit = res.data.currentUnit
              ? (res.data.currentUnit = this.dataCommon.units!.find((item) => item.orgId === res.data.currentUnit))
              : null;
            res.data.proposeUnit = res.data.proposeUnit
              ? (res.data.proposeUnit = this.dataCommon.units!.find((item) => item.orgId === res.data.proposeUnit))
              : null;
            this.form?.patchValue(res.data);
            this.isLoading = false;
          },
          (err) => {
            this.getMessageError(err);
            this.isLoading = false;
          }
        );
    }
  }

  initForm() {
    this.form = this.fb.group({
      formCode: [[]],
      fromDate: [null, Validators.required],
      toDate: [null],
      currentUnit: [null],
      currentPosition: [[]],
      proposeUnit: [null],
      proposePosition: [[]],
    });
  }
  save() {
    this.confirmService.error().then((res) => {
      if (res) {
        this.isSubmitted = true;
        const data = { ...cleanDataForm(this.form!) };
        if (this.form?.invalid || this.isLoading || this.checkValidate()) {
          return;
        }
        //convert date về YYYYMMDD
        data.fromDate = data.fromDate ? moment(data.fromDate).format(Constants.FORMAT_DATE.YYYY_MM_DD) : data.fromDate;
        data.toDate = data.toDate ? moment(data.toDate).format(Constants.FORMAT_DATE.YYYY_MM_DD) : data.toDate;

        data['currentUnit'] = data.currentUnit ? data.currentUnit.orgId : '';
        data['proposeUnit'] = data.proposeUnit ? data.proposeUnit.orgId : '';
        let api: any;
        if (this.type === ScreenType.Create) {
          api = this.service.createConsultation(data);
        } else if (this.type === ScreenType.Update) {
          data.id = parseInt(this.id!);
          api = this.service.updateConsultation(data);
        }
        this.subs.push(
          api.subscribe(
            () => {
              if (this.type === ScreenType.Create) {
                this.toastrCustom.success(this.translate.instant('development.consultation-config.messages.createSuccess'));
              } else {
                this.toastrCustom.success(this.translate.instant('development.consultation-config.messages.updateSuccess'));
              }
              this.isLoading = false;
              this.router.navigateByUrl('/development/consultation-config');
            },
            (e: any) => {
              this.getMessageError(e);
              this.isLoading = false;
            }
          )
        );
      }
    });
  }

  emitValue(item: SelectCheckAbleModal, field: string) {
    this.form?.controls[field].setValue(item.listOfSelected);
  }

  disabledStartDate = (startValue: Date): boolean => {
    let endValue = null;
    if (this.form?.get('toDate')?.value) {
      endValue = new Date(this.form.get('toDate')?.value);
    }
    if (!startValue || !endValue) {
      return false;
    }
    return startValue.getTime() > endValue.getTime();
  };

  disabledEndDate = (endValue: Date): boolean => {
    let startValue = null;
    if (this.form?.get('fromDate')?.value) {
      startValue = new Date(this.form.get('fromDate')?.value);
    }
    if (!endValue || !startValue) {
      return false;
    }
    return endValue.getTime() <= startValue.getTime();
  };



  override ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }

  checkValidate() {
    const data = this.form?.getRawValue();
    if (
      Utils.isEmpty(data.formCode) &&
      Utils.isEmpty(data.currentUnit) &&
      Utils.isEmpty(data.currentPosition) &&
      Utils.isEmpty(data.proposePosition) &&
      Utils.isEmpty(data.proposeUnit)
    ) {
      this.toastrCustom.warning(this.translate.instant('development.consultation-config.messages.validate1'));
    }
    return false;
  }

}
