import { Component, Injector, OnDestroy, OnInit } from '@angular/core';
import { Constants, userConfig, Utils } from "@hcm-mfe/learn-development/data-access/common";
import { DataCommonConsulation } from "@hcm-mfe/learn-development/data-access/models";
import { ConsultationConfigService } from "@hcm-mfe/learn-development/data-access/services";
import { BaseComponent } from "@hcm-mfe/shared/common/base-component";
import { cleanData } from "@hcm-mfe/shared/common/utils";
import { Pageable } from "@hcm-mfe/shared/data-access/models";
import * as FileSaver from 'file-saver';
import * as moment from 'moment';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-consultation-config-view',
  templateUrl: './consultation-config-view.component.html',
  styleUrls: ['./consultation-config-view.component.scss'],
})
export class ConsultationConfigViewComponent extends BaseComponent implements OnInit, OnDestroy {
  scrollY = '';
  dataTable = [];
  dataCommon: DataCommonConsulation = {};
  params = {
    formCode: null,
    fromDate: null,
    toDate: null,
    currentUnit: null,
    currentPosition: null,
    proposeUnit: null,
    proposePosition: null,
    page: 0,
    size: userConfig.pageSize,
  };
  pageable: Pageable = {
    totalElements: 0,
    currentPage: 0,
    size: userConfig.pageSize,
  };
  title?: string;
  private readonly subs: Subscription[] = [];
  modalRef?: NzModalRef;

  constructor(injector: Injector, private service: ConsultationConfigService) {
    super(injector);
  }

  ngOnInit() {
    this.isLoading = true;

    this.title = this.route.snapshot?.data['pageName'];
    this.service.getStateConsultation().subscribe((res) => {
      this.dataCommon = res;
      this.search(true);
    });
  }


  search(firstPage?: boolean) {
    this.isLoading = true;
    if (firstPage) {
      this.params.page = 0;
    }
    const newParams = { ...cleanData(this.params) };
    newParams.fromDate = newParams.fromDate ? moment(newParams.fromDate).format(Constants.FORMAT_DATE.YYYY_MM_DD) : newParams.fromDate;
    newParams.toDate = newParams.toDate ? moment(newParams.toDate).format(Constants.FORMAT_DATE.YYYY_MM_DD) : newParams.toDate;
    newParams['currentUnit'] = newParams.currentUnit ? newParams.currentUnit.orgId : '';
    newParams['proposeUnit'] = newParams.proposeUnit ? newParams.proposeUnit.orgId : '';
    this.subs.push(
      this.service.getAndSearch(newParams).subscribe(
        (res) => {
          this.dataTable = res.data?.content;
          this.pageable.totalElements = res?.data?.totalElements || 0;
          //check nếu dataTable = [] thì giảm page đi 1 tránh hiện thì trống ở  page >= 1
          if (this.dataTable.length === 0 && this.params.page > 0) {
            this.params.page = this.params.page - 1;
            return this.search();
          }
          this.isLoading = false;
          this.scrollY = Utils.setScrollYTable('table', this.pageable.totalElements! > 15 ? Constants.HeightPaginator : 0)
        },
        (error) => {
          this.isLoading = false;
          this.getMessageError(error);
          this.dataTable = [];
        }
      )
    );
  }

  detail(id: string) {
    this.router.navigate([this.router.url, 'update'], {
      queryParams: {
        id: id,
      },
      skipLocationChange: true,
    });
  }

  deleteItem(id: string) {
    if (this.isLoading) {
      return;
    }
    this.modalRef = this.modal.warning({
      nzTitle: this.translate.instant('development.consultation-config.messages.isDelete'),
      nzCancelText: this.translate.instant('development.consultation-config.messages.no'),
      nzOkText: this.translate.instant('development.consultation-config.messages.yes'),
      nzClassName: Constants.ClassName.LD_CONFIRM,
      nzOnOk: () => {
        this.isLoading = true;
        this.service.deleteById(id).subscribe(
          () => {
            this.toastrCustom.success(this.translate.instant('development.consultation-config.messages.deleteSuccess'));
            this.search();
          },
          (e) => {
            this.getMessageError(e);
            this.isLoading = false;
          }
        );
      },
    });
  }

  pageChange(page: number) {
    if (this.isLoading) {
      return;
    }
    this.params.page = page - 1;
    this.search();
  }
  exportExcel() {
    if (this.dataTable?.length === 0) {
      this.toastrCustom.warning(
        this.translate.instant(
          'development.proposed-unit.messages.dataTableEmpty'
        )
      );
      return;
    }
    const newParams = { ...cleanData(this.params) };
    delete newParams.page;
    delete newParams.size;
    newParams.fromDate = newParams.fromDate ? moment(newParams.fromDate).format(Constants.FORMAT_DATE.YYYY_MM_DD) : newParams.fromDate;
    newParams.toDate = newParams.toDate ? moment(newParams.toDate).format(Constants.FORMAT_DATE.YYYY_MM_DD) : newParams.toDate;
    newParams['currentUnit'] = newParams.currentUnit ? newParams.currentUnit.orgId : '';
    newParams['proposeUnit'] = newParams.proposeUnit ? newParams.proposeUnit.orgId : '';
    this.isLoading = true;
    this.service.exportExcel(newParams).subscribe(
      (data) => {
        let urlExcel = 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,';
        urlExcel = urlExcel + data.data;
        FileSaver.saveAs(urlExcel, this.translate.instant('development.consultation-config.messages.excelFileName'));
        this.toastrCustom.success(this.translate.instant('development.consultation-config.messages.exportExcelSuccess'));
        this.isLoading = false;
      },
      (e) => {
        this.getMessageError(e);
        this.isLoading = false;
      }
    );
  }



  override ngOnDestroy() {
    this.modalRef?.destroy();
    this.subs.forEach((sub) => sub.unsubscribe());
  }

  disabledStartDate = (startValue: Date): boolean => {
    if (!startValue || !this.params.toDate) {
      return false;
    }
    return startValue.getTime() > (this.params.toDate as Date).getTime();
  };

  disabledEndDate = (endValue: Date): boolean => {
    if (!endValue || !this.params.fromDate) {
      return false;
    }
    return endValue.getTime() <= (this.params.fromDate as Date).getTime();
  };

}
