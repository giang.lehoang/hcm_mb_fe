import { ConsultationConfigActionComponent } from './consultation-config-action/consultation-config-action.component';
import { ConsultationConfigViewComponent } from './consultation-config-view/consultation-config-view.component';

export const pages = [ConsultationConfigViewComponent, ConsultationConfigActionComponent];

export * from './consultation-config-view/consultation-config-view.component';
export * from './consultation-config-action/consultation-config-action.component';
