import {Constants} from "../constants/constant.class";

export class Utils {

// lấy màu theo description, trường hợp này k thể lấy đc theo key vì key có nhiều description
// và mỗi description có màu khác nhau theo role

  public  static  getColorFlowStatusApporveOrPublish(statusDisplay: string) {
    if (statusDisplay.includes('Chờ') || statusDisplay.includes('Đang')) {
      return 'yellow';
    } else if (statusDisplay.includes('Đã')) {
      if (statusDisplay.includes('hủy')) {
        return 'red';
      } else {
        return 'green';
      }
    } else if (statusDisplay.includes('Hủy') || statusDisplay.includes('Từ chối')) {
      return 'red';
    } else if (statusDisplay.includes('Đồng ý') || statusDisplay.includes('đồng ý')) {
      if (statusDisplay.includes('Không') || statusDisplay.includes('không')) {
        return 'red';
      } else return 'green';
    } else {
      return 'gray';
    }
  }


  static setScrollYTable(id: string, heightPagination: number = 0) {
    const offsetTop = document.getElementById(id)?.offsetTop;

    if (offsetTop) {
      const fullHeight = window.innerHeight;
      return `${fullHeight - offsetTop - 129 - heightPagination}px`;
    }
    return '';
  }

  static getGroupType(proposeCategory: string) {
    switch (proposeCategory) {
      case Constants.ProposeCategory.GROUP:
        return Constants.ProposeTypeID.GROUP;

      case Constants.ProposeCategory.ORIGINAL:
        return Constants.ProposeTypeID.ORIGINAL;

      case Constants.ProposeCategory.ELEMENT:
        return Constants.ProposeTypeID.ELEMENT;
      default:
        return Constants.ProposeTypeID.ISSUE;
    }
  }

  static getUrlByFunctionCode(code: string) {
    return Constants.FunctionUrlOfPropose[code];
  }
}

