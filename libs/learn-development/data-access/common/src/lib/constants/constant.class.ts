// import {FunctionCode} from "@hcm-mfe/learn-development/data-access/enums";
import {
  EmployeeInfoMultiple,
  StateApprovement,
  StateConfirmPromulgate
} from "@hcm-mfe/learn-development/data-access/models";
import { FunctionCode } from "../enums/enums.class";


export class Constants {

  public static readonly KeyConstants = {
    key1: 'Có',
    key2: 'Không',
    key3: 'Không tham vấn',
    key4: 'Đã tham vấn',
    key5: 'Thiếu tham vấn',
    key6: 'Chưa gửi yêu cầu',
    key7: 'Gửi chưa xử lí',
    key8: 'Không phỏng vấn',
    key9: 'Gửi đã xử lí',
    key10: 'Đã gửi',
    key11: 'Chưa gửi',
    key12: 'Đã có kết quả'
  }

  public static readonly Colors = {
    red: 'red',
    green: 'green',
    yellow: 'yellow',
    gray: 'gray'
  }

  public static readonly ConsultationStatus = [
    {
      description: Constants.KeyConstants.key3,
      color: Constants.Colors.red
    },
    {
      description: Constants.KeyConstants.key4,
      color: Constants.Colors.green
    },
    {
      description: Constants.KeyConstants.key5,
      color: Constants.Colors.yellow
    }
  ]

  public static readonly SalaryStatus = [
    {
      description: Constants.KeyConstants.key1,
      color: Constants.Colors.green
    },
    {
      description: Constants.KeyConstants.key2,
      color: Constants.Colors.red
    },
  ];

  public static readonly InterviewStatus = [
    {
      description: Constants.KeyConstants.key6,
      color: Constants.Colors.gray
    },
    {
      description: Constants.KeyConstants.key7,
      color: Constants.Colors.yellow
    },
    {
      description: Constants.KeyConstants.key8,
      color: Constants.Colors.red
    },
    {
      description: Constants.KeyConstants.key9,
      color: Constants.Colors.green
    },
    {
      description: Constants.KeyConstants.key10,
      color: Constants.Colors.green
    },
    {
      description: Constants.KeyConstants.key11,
      color: Constants.Colors.gray
    },
    {
      description: Constants.KeyConstants.key12,
      color: Constants.Colors.green
    },
  ]

  public static readonly ClassName = {
    LD_CONFIRM: 'ld-confirm',
    NOTIFICATION_MODAL: 'notification-modal',
    NOTIFICATION_CONTAINER: 'notification-container',
  };

  public static readonly KeyCommons = {
    CANCEL_TEXT: 'development.proposed-unit.button.cancelText',
    OK_TEXT: 'development.proposed-unit.button.okText',
    CONTINUE_TEXT: 'development.proposed-unit.button.continue',
    CANCEL2_TEXT: 'development.proposed-unit.button.cancel2',
    NUMBER_DECISION_SIGN_DATE_VALID: 'development.commonMessage.numberDecisionSignDateValid',
    DECISION: 'development.promulgate.table.decision',
    COMMON_DECISION: 'development.promulgate.table.commonDecision',
    DEVELOPMENT_PROMULGATE: '/development/promulgate',
    DEVELOPMENT_PROPOSE_UNIT_APPROVE: 'development/propose/unit-approve',
    DEVELOPMENT_PROPOSE_UNIT: 'development/propose/unit',
    DEVELOPMENT_PROPOSE_LIST: 'development/propose/propose-list',
    DEVELOPMENT_RECOMMEND_INDIVIDUAL_DETAIL: '/development/recommend-individual/detail-recommend-individual',
    DEVELOPMENT_RECOMMEND_INDIVIDUAL_ACTION: '/development/recommend-individual/action-recommend-individual',
    DEVELOPMENT_RECOMMEND_INDIVIDUAL: '/development/recommend-individual/add',
    DEVELOPMENT_ISSUED_DECISIONS: 'development/issued-decisions',
    DEVELOPMENT_INTERVIEW: '/development/interview',
    MESSAGES_SUCCESS: 'development.approvement-propose.messages.Success',
    MESSAGES_SUCCESS2: 'development.commonMessage.success',
    MESSAGES_SUCCESS_APPROVED: 'development.approvement-propose.messages.approvedSuccess',
    MESSAGES_SUCCESS_REJECTED: 'development.approvement-propose.messages.rejectSuccess',
    MESSAGES_SUCCESS_OTHERIDEA: 'development.approvement-propose.messages.otherIdeaSuccess',
    MESSAGES_SUCCESS_OTHERIDEALDDV: 'development.approvement-propose.messages.otherIdeaLDDVSuccess'
  };

  public static readonly FORMAT_DATE = {
    YYYY_MM_DD: 'YYYY-MM-DD',
    DD_MM_YYYY: 'DD/MM/YYYY',
    DD_MM_YYYY_HH_MM: 'DD/MM/YYYY HH:mm'
  };

  public static readonly CONTENT_TYPE = {
    APPLICATION_JSON: 'application/json',
  };

  public static readonly TABLE_CONFIG = {
    TEXT_NOWRAP: 'text-nowrap',
    TEXT_CENTER: 'text-center',
  };

  public static readonly Salary = {
    DGL: 'Chưa gửi lương',
    CGL: 'Đã gửi lương',
  };

  public static readonly FlowStatusColor = [
    {
      keys: ['6D', '9D', '11', '6C', '9B', '5A', '10A', '12', '6B', '9C', '9', '9A', '10A3', '10A1BA', '11.2'],
      color: Constants.Colors.green
    },
    {
      keys: ['6D', '9D', '11', '6C', '9B', '5A', '10A', '12', '6B', '9C', '9', '9A'],
      color: Constants.Colors.green
    },
  ]

  public static readonly StatusCB = ['6', '8', '10B1', '10C'];


  public static readonly ProcessStatusPublishDescription = {
      CHO_XAC_NHAN: 'Chờ xác nhận',
      CHO_XAC_NHAN_HUY: 'Chờ xác nhận hủy',
      DA_XAC_NHAN: 'Đã xác nhận',
      DA_XAC_NHAN_HUY: 'Đã xác nhận hủy',
      TU_CHOI_XAC_NHAN: 'Từ chối xác nhận',
      TU_CHOI_XAC_NHAN_HUY: 'Từ chối xác nhận hủy',
  };

  public static readonly HeightPaginator = 50;

  public static readonly ListTextNotifyDecision = [
    {
      id: 1,
      name: 'Quyết định',
    },
    {
      id: 2,
      name: 'Thông báo thu nhập',
    },
    {
      id: 3,
      name: 'Thông báo chế độ bảo hiểm',
    },
  ];

  public static readonly ListProposeCategory = [
    {
      id: 0,
      name: 'GROUP',
    },
    {
      id: 1,
      name: 'ORIGINAL',
    },
    {
      id: 2,
      name: 'ELEMENT',
    },
  ];

  public static readonly ListPromulgate = [
    {
      id: true,
      name: 'Thay đổi phương án',
    },
    {
      id: false,
      name: 'Thay đổi ngày hiệu lực',
    },
  ];

  public static readonly ListProposeRotationReasonsNCN = [
    {
      rotationReasonID: '1',
      description: '',
      disabled: true,
      name: '',
    },
  ];



  public static readonly DCDD = 'DD';

  public static readonly TYPE_SEARCH_POSITION = 'POSISTION';

  public static readonly FormRotationTitle = {
    SAME_TITLE: '0',
    DIFFERENT_TITLES: '1',
  };

  public static readonly ProposeCategory = {
    ELEMENT: 'ELEMENT',
    ORIGINAL: 'ORIGINAL',
    GROUP: 'GROUP',
  };

  public static readonly FunctionUrlOfPropose : any = {
    [FunctionCode.DEVELOPMENT_PROPOSE]: '/development/propose/unit',
    [FunctionCode.DEVELOPMENT_APPROVE_PROPOSE]: '/development/propose/unit-approve',
    [FunctionCode.DEVELOPMENT_PROPOSE_LIST]: '/development/propose/propose-list',
    [FunctionCode.DEVELOPMENT_APPROVEMENT_PROPOSE]: '/development/approvement-propose',
    [FunctionCode.DEVELOPMENT_LIST_ISSUE_TRANSFER]: 'development/list-issue-transfer'
  };

  public static readonly PageName = {
    approveKSPTNL: 'development.pageName.approvement-propose-approve',
    detailKSPTNL: 'development.pageName.approvement-propose-detail',
    approveLDDV: 'development.pageName.proposed-unit-approve-approve',
    detailLDDV: 'development.pageName.proposed-unit-approve-detail',
  }

  public static readonly SERVICE_NAME_POSITION = {
    MODEL_POSITION: 'hcm-model-plan',
  };

  public static readonly ScreenCode = {
    HRDV: "HCM-LND-HRDV",
    LDDV: "HCM-LND-LDDV",
    PTNL: "HCM-LND-PTNL",
    KSPTNL: "HCM-LND-KSPTNL",
    GDNS: "HCM-LND-GDNS",
    // HCMLDEXLPDPTNL : "HCM-LND-KSPTNL",
    // HCMLDEXLKTPTNL: "HCM-LND-PTNL",

    // LIVE
    //KSPTNL
    //PTNL
    HCMLDEXLPDPTNL: "HCM-LDE-XLPD-PTNL",
    HCMLDEXLKTPTNL: "HCM-LDE-XLKT-PTNL",

    DVNS: "HCM-LND-DVNS",
    KSDVNS: "HCM-LND-KSDVNS"
  }

  public static readonly KSPTNL = 'KSPTNL';

  public static readonly FlowCodeConstants = {
    CHUAGUI1: "1",
    WITHDRAW: "1A",
    CHODUYET2: "2",
    SENDAPPROVE2A: "2A",
    OTHERIDEA3: '3',
    STATUS3A: "3A",
    STATUS3A1: "3A1",
    CANCELPROPOSE: "4A",
    HANDLING5B: "5B",
    NHAP: "5C",
    HANDLING6: "6",
    HANDLING6A: "6A",
    CHODUYET7: "7",
    KSDongY: "9",
    KSDongY9A: "9A",
    DongY10A: "10A",
    KHONGDONGY10B: "10B"
  }

  public static readonly PublicStatusCodeConstants = {
    issueTransfer12dot3A: "12.3A",
    issueTransfer12dot4: "12.4",
  };

  public static readonly PublicStatusCodeNoteIssueConstants = ['10A4', '10A', '12.3A', '12.4']

  public static readonly ListPublicStatusCodeConstants = ['10A', '12.3A', '12.4'];

  public static readonly ListButtonDeleteIssuedCodeConstants = ['10A', '12.3A', '12.4', '12.1AB', '12.1BB', '12.1CB', '12.1AA', '12.2AA',
  '12.1BA', '12.2BA', '12.1CA', '12.2CA', '12.3B', '12.5'];

  public static readonly ListIssuedDecision =  ['10A', '10A1', '10A2', '10A3', '11.1', '11.1BB', '12.2AB'];


  public static readonly PublishCodeConstants = {
    issueTransfer11dot1: "11.1"
  }

  public static readonly FormRotationType = {
    DC: "DC",
    DD: "DD",
    LEV: "LEV"
  }

  public static readonly Level = [
    { code: 1, name: 'Level 1' },
    { code: 2, name: 'Level 2' },
    { code: 3, name: 'Level 3' },
  ];

  public static readonly Idea = [
    { code: 'Đồng ý', name: 'Đồng ý' },
    { code: 'Không đồng ý', name: 'Không đồng ý' },
    { code: 'Ý kiến khác', name: 'Ý kiến khác' },
  ];

  public static readonly statePromulgate = [
    {
      name: 'Ban hành',
      list: ['10A1', '10A3', '10A1A', '10A1BA', '10A1BB', '12.2AB'],
      type: 1,
      action: 'BH',
    },
    {
      name: 'Ban hành lại',
      list: ['11.1A', '11.1BA', '11.2A', '11.2BA'],
      type: 2,
      action: 'BHL',
    },
    {
      name: 'Hủy ban hành',
      list: ['12.1AA', '12.2AA', '12.1BA', '12.2BA', '12.1CA', '12.2CA'],
      type: 3,
      action: 'HBH',
    },
  ];

  public static readonly statusPublishColor = [
    {
      name: ['Chưa ban hành', 'Hiệu chỉnh ban hành'],
      color: '#d3d3d3',
    },
    {
      name: ['Chờ hủy ban hành', 'Chờ duyệt ban hành', 'Chờ duyệt ban hành lại', 'Chờ duyệt hủy ban hành', 'Chờ ban hành'],
      color: 'yellow',
    },
    {
      name: ['Đã ban hành', 'Đã ban hành lại'],
      color: 'green',
    },
    {
      name: ['Đã hủy ban hành'],
      color: 'red',
    },
  ];

  public static readonly ButtonStatus = ['11.1', '11.2', '11.1C', '11.2C', '11.3', '12.2BB', '12.2CB', '12.3B', '12.4', '12.5', '12.3A'];
  public static readonly editSignDate = ['10A1', '10A1A', '10A1BA', '10A3', '11.1A', '11.1BA', '11.2A', '11.2BA', '12.2AB'];
  public static readonly EditSignDateIssue = ['10A1', '10A1A', '10A1BA', '10A1BB', '10A3', '11.1A', '11.1BA', '11.2A', '11.2BA', '12.2AB']

  public static readonly stateApprovement: StateApprovement[] = [
    {
      buttonListScreen: 'Duyệt ban hành',
      list: ['11.1', '11.1C', '12.2BB'],
      buttonAction: [
        {
          name: 'Duyệt ban hành',
          type: 'primary',
          action: true,
          typeState: 'confirm'
        },
      ],
      screenAction: 'action',
    },
    {
      buttonListScreen: 'Duyêt hủy ban hành',
      list: ['12.3B'],
      buttonAction: [
        {
          name: 'Duyêt hủy ban hành',
          type: 'primary',
          action: true,
          typeState: 'confirm'
        },
      ],
      screenAction: 'action',
    },

    {
      buttonListScreen: 'Duyệt ban hành lại',
      list: ['11.2C'],
      buttonAction: [
        {
          name: 'Duyệt ban hành lại',
          type: 'primary',
        }
      ],
      screenAction: 'action',
    },
    {
      buttonListScreen: 'Xem',
      list: [
        '11.2',
        '11.3',
        '12.2CB',
        '12.4',
        '12.5'
      ],
      buttonAction: [],
      screenAction: 'view',
    },
  ];

  public static readonly DataDropdown = [
    { state: false, value: 'Thay đổi phương án' },
    { state: true, value: 'Thay đổi ngày hiệu lực' },
  ];


  public static readonly TemplateType = {
    TT: 'TT', // Tờ trình
    PLTT: 'PLTT', // Phụ lục tờ trình
    QD: 'QĐ', // Quyết định
    TBTN: 'TBTN', // TB thu nhập
    TBCDBH: 'TBCDBH', // TB chế độ bảo hiểm
    PLDCDD: 'PLĐCĐĐ',
    TDLV: 'TDLV',
  };

  public static readonly SubTemplateType = {
    DX: 'ĐX', // Đề xuất
    TH: 'TH', // Tổng hợp
    BNCT: 'BNCT', // Bổ nhiệm chính thức
    TDCD: 'TDCD', // Thay đổi chức danh
    NSLĐ: 'NSLĐ', // NSLĐ
    HQ: 'HQ', // Hiệu quả
  };

  public static readonly TypeFile = {
    xls: 'application/vnd.ms-excel',
    xlsx: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    doc: 'application/msword',
    docx: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  };

  public static readonly stateConfirmPromulgate: StateConfirmPromulgate[] = [
    {
      buttonListScreen: 'Xác nhận',
      list: ['10A2', '10A1B', '11.1B', '11.2B'],
      buttonAction: [
        {
          name: 'Xác nhận',
          type: 'primary',
          action: true,
          typeState: 'confirm',
          confirmQues: 'Xác nhận ban hành này?'
        },
        {
          name: 'Từ chối xác nhận',
          type: 'default',
          action: false,
          typeState: 'confirm',
          confirmQues: 'Từ chối xác nhận ban hành này?'
        },
      ],
      screenAction: 'action',
    },
    {
      buttonListScreen: 'Xác nhận hủy',
      list: ['12.1AB', '12.1BB', '12.1CB'],
      buttonAction: [
        {
          name: 'Xác nhận hủy',
          type: 'primary',
          action: true,
          typeState: 'cancel',
          confirmQues: 'Xác nhận hủy ban hành này?'
        },
        {
          name: 'Từ chối xác nhận hủy',
          type: 'default',
          action: false,
          typeState: 'cancel',
          confirmQues: 'Từ chối xác nhận hủy ban hành này?'
        },
      ],
      screenAction: 'action',
    },
  ];

  public static readonly StatusButton = [
    '10A2', '10A1B', '11.1B', '11.2B', '12.1AB', '12.1BB', '12.1CB'
  ];

  public static readonly StatusButtonWithdraw = [
    '5B', '6'
  ];

  public static readonly ProposeTypeID = {
    GROUP: 0,
    ORIGINAL: 1,
    ELEMENT: 2,
    ISSUE: 3,
  }

  public static readonly RelationType = {
    ONE: 0,
    MANY: 1
  }

  public static readonly IconStatus = {
    UP: 'up',
    DOWN: 'down'
  }

  public static readonly PlanAlternative = [
    { code: 0, name: 'Trong đơn vị' },
    { code: 1, name: 'Khác đơn vị' },
    { code: 2, name: 'Tuyển mới' },
  ];

  public static readonly TypeEmpAlternative = {
    HANDOVER: 'HANDOVER',
    REPLACE: 'REPLACE'
  };


  public static readonly EditButtonHRDV = ['1', '3', '3A', '1A', '3A1'];

  public static readonly TypeWithdrawPropose = {
    ORIGINAL: 0,
    ELEMENT: 1
  }
  public static readonly ListHandleStatusMany = [
    '6', '8', '10B1', '10C', '5B', '5C'
  ];

  public static readonly FlowCodeName = {
    CHODUYET: "Chờ duyệt",
    CHOXACNHAN: "Chờ xác nhận",
    CHOBANHANH: "Chờ ban hành",
    CDBANHANH: "Chờ duyệt ban hành",
    CHUABANHANH: "Chưa ban hành",
  }

  public static readonly ListStatusTheLump = ['5B', '5C', '6', '8', '10B1', '10C'];

  public static readonly ShowCheckBox = ['5B', '5C', '6', '8', '10B1', '10C'];

  public static readonly RelationTypeString = {
    ONE: '0',
    MANY: '1'
  }

  public static readonly ChangeStatusWithdraw = [
    '1', '1A', '2','3','4', '4A', '5A', '5A1','11.2','12.5','11.2BB','11.3'
  ]

  public static readonly FormGroupList = {
    BO_NHIEM: 1,
    DCDD: 2,
    DCLEVEL: 3,
    TGCV: 4,
    PV: 5,
    MHTC: 6,
  }

  public static readonly FormRotationName = {
    INUNIT: 0,
    DIFFERUNIT: 1
  }

  public static readonly ViolationCode = {
    REQUIRED: 0,
    VIOLATE: 1,
    PASSED: 2
  }

  public static readonly ConditionViolationColor = {
    REQUIRED: 'red',
    VIOLATE: 'yellow',
    PASSED: 'green'
  }

  public static readonly ViolationName = {
    PASSED: 'Đạt',
    FAILED: 'Không đạt'
  }

  public static readonly LeaderAppproveStatus = ['3A', '3A1'];

  public static readonly noteWithdraw = [
    '1', '2'
  ];

  public static readonly noteWithdrawPTNL = [
    '3', '4'
  ];

  public static readonly StatusApproveSendProposeMany = ['1', '3', '3A', '1A', '3A1'];

  public static readonly AddFormGroup = [2, 3, 6, 5];

  public static readonly IssueDecisionButtonStatus = ['1', '3', '3A'];

  public static readonly IssueLevelName = {
    UNIT: "Đơn vị",
    BLOCK: "Khối",
  }

  public static readonly TypeStatement = {
    HRDV: 0,
    TCNS: 1,
    EDITHRDV: 2,
  }

  public static readonly InterviewHandleStatus = {
    CGKQ: '1',
    DGKQ: '3',
  }

  public static readonly DisplayInterviewResult = [
    {
      description: 'Không đạt',
      color: Constants.Colors.red
    },
    {
      description: 'Đạt',
      color: Constants.Colors.green
    },
  ]

  public static readonly withdrawNoteCode = {
    Note3: 3,
    Note4: 4,
  }

  public static readonly ReturnProposeStatus = ['5B', '6', '8'];
  public static readonly ViewCheckBoxInterview = [1,3,6,4];
  public static readonly ViewCheckBoxInterviewConfirm = 4;

  public static readonly POSITION_CODE_INTERVIEW = 'POSITION_CODE';

  public static readonly CheckDeleteBtn = [
    '5C', '6'
  ];

  public static readonly CheckDeleteBtnGroup = [
    '5B', '5C', '6'
  ];

  public static readonly checkdeletebtnGroupKTPTNL = '7'

  public static readonly DeleteButtonDetailStatus = ['1', '5C', '6'];

  public static readonly TypeStatementFile = {
    HR1: 'statementHR1',
    DECISION: 'decision',
    STATEMENT: 'statement',
    STATEMENTEDIT: 'statementEdit',
    STATEMENTWITHDRAW: 'statementWithdraw'
  }

  public static readonly ViewButtonHandleRemove = ['5B','5C'];

  public static readonly EmployeeInfoMultipleFlowStatus = {
    flowStatus1: 1,
    flowStatus2: 2,
    flowStatus3: 3,
    flowStatus4: 4,
    flowStatus5: 5,
    flowStatus6: 6,
    flowStatus7: 7,
    flowStatus8: 8,
    flowStatus9: 9,
    flowStatus10: 10,
  }

  public static readonly FormGroupDivison = {
    KHCN: 'KHCN',
  }

  public static readonly ActionIssueScreen = {
    BH: "BH",
    DBH: "DBH",
    XN: "XN",
    XNH: "XNH",
    DBHL: "DBHL",
    DHBH: "DHBH",
  }

  public static readonly ActionIssueDecisionScreen = {
    CBH: "CBH"
  }

  public static readonly DisableCancelProposeCheckbox = ['1A', '4A'];

  public static readonly TypeStatusPublish = {
    ISSUE: '1',
    ISSUEAGAIN: '2',
    CANCELISSUE: '3',
  }

  //trạng thái hiển thị cục chuyển ban hành
  public static readonly ChangeIssueStatus = ['5A1', '10A', '10A4', '12.3A', '12.4'];

  public static readonly TypeMaximumTime = [
    {
      type: 0,
      name: 'Tháng'
    },
    {
      type: 1,
      name: 'Năm'
    }
  ]

  public static readonly TypeMaximumValue = {
    MONTH: 0,
    YEAR: 1,
  }

  //hình thức điều chuyển điều động
  public static readonly RotationName = {
    DC: "Điều chuyển",
    DD: "Điều động",
  }

  public static readonly CancelIssueStatus = ['10A1', '10A3', '11.1', '11.2', '11.1BB', '11.2BB'];

  public static readonly IssueCorrectionButton = ['10A1', '10A1BB', '10A3', '11.1', '11.2', '11.1BB', '11.2BB'];

  public static readonly ListViewIssueStatus = ['5A1', '10A', '10A4', '12.3A', '12.4',
    '10A1', '10A3', '11.1', '11.2', '11.1BB', '11.2BB', '10A2',];

  public static readonly CancelPlanPublishStatus = ['12.3A', '12.4'];
  public static readonly CancelPlanPublishStatus2 = ['5A1', '10A', '12.3A', '12.4'];
  public static readonly CancelPlanFlowStatus = ['5A1', '10A'];

  //check hiển thị số quyết định ngày ký màn thành phần
  public static readonly DecisionNumberSignDate = {
    START11DOT1: '11.1',
    START11DOT2: '11.2',
    START11DOT3: '11.3',
    START12DOT: '12.',
  };

  //list trạng thái Ban hành
  public static readonly StatusPublishCode = {
    PENDINGISSUE5A1: "5A1",
    CANCELPUBLISH12DOT3A: "12.3A",
    PENDINGAPPROVEISSUEAGAIN: "11.2C",
    PENDINGAPPROVECANCELISSUE: "12.3B",
  }

  //trạng thái hiển thị nút Hủy ban hành
  public static readonly ApprovementIssueButton = ['11.1', '11.1C', '12.2BB'];

  public static readonly DocumentTypeIssue = {
    DECISION: 1,
    TBINCOME: 2,
    TBINSURANCE: 3,
  }

  public static readonly UploadFileEmployeeIssue = {
    INCOME: "Thông báo thu nhập",
    SALARY: "Thông báo bảo hiểm"
  }

  public static readonly StatusCheckboxDetailConfirmIssue = ['10A1B', '10A2', '11.1B', '11.2B', '12.1AB','12.1BB', '12.1CB'];

  public static readonly TypeStateConfirmIssue = {
    CONFIRM: 'confirm',
    CANCEL: 'cancel',
  }

  public static readonly StatusButtonConfirmIssue = ['10A2', '10A1B', '11.1B', '11.2B', '12.1AB', '12.1BB', '12.1CB'];

  //trạng thái hiển thị nút xác nhận / xác nhận hủy màn xác nhận ban hành
  public static readonly ConfirmButtonConfirmIssue = ['10A2', '10A1B', '11.1B', '11.2B'];

  //trạng thái hiển thị nút từ chối xác nhận / từ chối xác nhận hủy màn xác nhận ban hành
  public static readonly RejectConfirmButtonConfirmIssue = ['12.1AB', '12.1BB', '12.1CB'];

  public static readonly ConfirmQuesConfirmIssue = {
    CONFIRM: "Xác nhận ban hành này?",
    CONFIRMCANCEL: "Từ chối xác nhận ban hành này?",
    REJECTCONFIRM: "Xác nhận hủy ban hành này?",
    CANCELREJECTCONFIRM: "Từ chối xác nhận hủy ban hành này?"
  }

  public static readonly ButtonConfirmIssueName1 = {
    CONFIRM: 'Xác nhận',
    CONFIRMCANCEL: "Từ chối xác nhận",
    REJECTCONFIRM: 'Xác nhận hủy',
    CANCELREJECTCONFIRM: 'Từ chối xác nhận hủy'
  }

  public static readonly ButtonConfirmIssueName2 = {
    CONFIRM: 'xác nhận',
    CONFIRMCANCEL: "từ chối xác nhận",
    REJECTCONFIRM: 'xác nhận hủy',
    CANCELREJECTCONFIRM: 'từ chối xác nhận hủy'
  }

  public static readonly ProposeTypeIDIssue = {
    ELEMENT: 2,
    GROUP: 3,
  }

  //loại file màn ban hành
  public static readonly FileTypeIssue = {
    DECISION_PUBLISH: 'DECISION_PUBLISH',
    DECISION_SIGN: 'DECISION_SIGN',
  }

  public static readonly UnitTypeTGCV = {
    SAMEUNIT: "Trong đơn vị",
    OTHERUNIT: "Khác đơn vị"
  }

  public static readonly TypeAction = {
    ADD: 'add',
    CREATE: 'create',
    UPDATE: 'update',
    VIEW: 'view',
  }

  public static readonly SubRouterLink = {
    DETAIL: 'detail',
    DETAILTGCV: 'detail-tgcv',
    UPDATE: 'update',
    UPDATETGCV: 'update-tgcv'
  }

  public static readonly HandleButtonPTNL = ['5B', '5C'];

  public static readonly ApproveKSPTNLAction = {
    AGREE: 0,
    OTHERIDEA: 2,
  }

  public static readonly PageNameHRDV = [
    'development.pageName.proposed-unit-add',
    'development.pageName.proposed-unit-update',
    'development.pageName.proposed-unit-detail'
  ];

  public static readonly PageNamePTNL = [
    'development.pageName.proposedLD-add',
    'development.pageName.proposedLD-update',
    'development.pageName.proposedLD-detail',
    'development.pageName.proposedLD-handle',
    'development.pageName.issue-transfer-detail'
  ];

  public static readonly PageNameLDDV = [
    'development.pageName.proposed-unit-approve-detail',
    'development.pageName.proposed-unit-approve-approve'
  ]

  public static readonly HRDV = 'HRDV';

  // trạng thái màn đề xuất 1 cá nhân
  public static readonly RecommendIndividualCode = {
    CREATE: 1,
    DETAIL: 2,
    UPDATE: 3,
  }

  public static readonly PositionType = {
    KN: 'KN',
    CT: 'CT',
  }

  public static readonly SUITABLEPOSITION = 2;

  public static readonly ROTATION_SPECIALIZE = 'ROTATION_SPECIALIZE';

  public static readonly ApprovementIssueButtonName = {
    ISSUEAPPROVE: 0,
    CANCELAPPROVE: 1,
    ISSUEAPPROVEAGAIN: 2,
  }

  public static readonly ViolationList = [
    { key: '0', role: 'Không đạt' },
    { key: '1', role: 'Không đạt' },
    { key: '2', role: 'Đạt' }
  ]
  public static readonly TimeTypeCode = {
    MONTH: 0,
    YEAR: 1,
  }

  public static readonly ScrollTabs = [
    {
      title: 'development.pageName.tab1',
      scrollTo: 'tab1',
    },
    {
      title: 'development.pageName.tab2',
      scrollTo: 'tab2',
    },
    {
      title: 'development.pageName.tab3',
      scrollTo: 'tab3',
    },
    {
      title: 'development.pageName.tab4',
      scrollTo: 'tab4',
    },
    {
      title: 'development.pageName.tab5',
      scrollTo: 'tab5',
    },
    {
      title: 'development.pageName.tab6',
      scrollTo: 'tab6',
    },
    {
      title: 'development.pageName.tab7',
      scrollTo: 'tab7',
    },
    {
      title: 'development.pageName.tab8',
      scrollTo: 'tab8',
    },
  ];

  public static readonly TypeScreenHandle = {
    HANDEL: 'handle',
    HANDEL_APPOINTMENT: 'handle-appointment-renewal',
    DETAIL: 'detail',
    DETAIL_APPOINTMENT: 'detail-appointment-renewal',
    HANDLE_TGCV: 'handle-tgcv',
    UPDATE_APPOINTMENT: 'update-appointment-renewal'
  }

  public static readonly TypeProposed = {
    CURRENTUNIT: 0,
    PROPOSEDUNIT: 1,
  }

  public static readonly ListScreenCodeUnit = ['HCM-LND-HRDV', 'HCM-LND-LDDV'];
  public static readonly ListScreenCodePTNL = ['HCM-LND-PTNL', 'HCM-LND-KSPTNL'];

  public static readonly PositionTypeName = {
    KN: 'Kiêm nhiệm',
    CT: 'Chuyên trách',
  }

  public static readonly WithdrawConfirmIdeaType = {
    APPROVE: 1,
    REJECT: 2,
    OTHERIDEA: 3,
  }

  public static readonly EmployeeStatus = {
    WORKING: 1
  }

  public static readonly CodeError = {
    NOTEXISTRECORD: 1000,
  }

  // trạng thái hiện ý kiến phê duyệt của LDDV (tờ trình chỉnh sửa)
  public static readonly ApproveNoteEditReport = ['1A', '3A', '3A1'];

  //trạng thái edit tờ trình gốc của đơn vị
  public static readonly EditOriginReport = ['1', '3', '3A1'];

  //trạng thái hiển thị ô ý kiến của tờ trình đơn vị
  public static readonly EditIdeaOriginReport = ['1', '3', '3A', '3A1'];

  public static readonly HCM_LD_REASON_EXTEND = 'HCM_LD_REASON_EXTEND';

  // trạng thái hiển thị nút lưu và gửi duyệt trong PTNL
  public static readonly StatusSendApprovePTNL = ['6', '8'];

  public static readonly StatusImportInterview = {
    pending: 0,
    success: 1,
    error: 2,
  }

  public static readonly RoleAppointmentRotation = {
    HRDV: "HCM_LDE_XLKT_HR",
    PTNL: "HCM_LDE_XLKT_PTNL",
    KSPTNL: "HCM_LDE_XLPD_PTNL",

  }
}

