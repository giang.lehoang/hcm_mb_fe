import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { clearParamSearch } from "@hcm-mfe/shared/common/utils";
import { environment } from "@hcm-mfe/shared/environment";
import { ProvisoModel} from "../../../models/src/lib/proviso-ld.model";
import {DataCommonProvisoLd, ParamsSearch} from "@hcm-mfe/learn-development/data-access/models";
import { url } from '@hcm-mfe/learn-development/data-access/common';


@Injectable({
  providedIn: 'root',
})
export class ProvisoLdService {
  baseUrl = `${environment.baseUrl}${url.url_endpoint_get_proviso}`;
  baseUrlCategoryTd = `${environment.baseUrl}hcm-person-info/v1.0/major-level/get-all`;
  baseUrlCategoryCn = `${environment.baseUrl}hcm-person-info/v1.0/faculties`;
  state: DataCommonProvisoLd | undefined;

  constructor(private http: HttpClient) {}

  getState(): Observable<any> {
    if (this.state) {
      return of(this.state);
    }
    // return this.http.get(`${this.baseUrl}/conditionInfor`);
    return this.http.get(`${this.baseUrl}/conditionInfor`).pipe(
      map((res: any) => {
        this.state = res.data;
        return this.state;
      }),
      catchError(() => of(undefined))
    );
  }

  getAndSearch(params: ParamsSearch): Observable<any> {
    return this.http.get(`${this.baseUrl}/search`, { params: clearParamSearch(params) });
  }

  findById(id: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  createProviso(body: ProvisoModel): Observable<any> {
    return this.http.post(`${this.baseUrl}`, body);
  }

  updateProviso(body: ProvisoModel): Observable<any> {
    return this.http.put(`${this.baseUrl}`, body);
  }

  deleteById(id: string): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`);
  }

  getLevel(): Observable<any> {
    return this.http.get(`${this.baseUrlCategoryTd}`);
  }

  getSpecialized(): Observable<any> {
    return this.http.get(`${this.baseUrlCategoryCn}`);
  }

  exportExcel(params: ProvisoModel): Observable<any> {
    return this.http.get(`${this.baseUrl}/export`, { params: clearParamSearch(params) });
  }
}
