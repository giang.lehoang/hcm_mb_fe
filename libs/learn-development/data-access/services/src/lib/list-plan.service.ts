import {HttpClient, HttpResponse} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { Observable } from 'rxjs/internal/Observable';
import { map, catchError } from 'rxjs/operators';
import {environment} from "@hcm-mfe/shared/environment";
import { url } from '@hcm-mfe/learn-development/data-access/common'
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";
import {clearParamSearch} from "@hcm-mfe/shared/common/utils";
import {Consultion, DataTempalte, SendInterviewModel} from "@hcm-mfe/learn-development/data-access/models";

@Injectable({
  providedIn: 'root',
})
export class ListPlanService {
  baseUrl = `${environment.baseUrl}${url.url_endpoint_list_plan}`;
  proposeUrl = `${environment.baseUrl}${url.url_endpoint_get_proposedUnit}`;
  salaryUrl = `${environment.baseUrl}${url.url_endpoint_salary_list}`;
  baseUrlTemplate = `${environment.baseUrl}${url.url_endpoint_get_ld_template}`;
  consultationUrl = `${environment.baseUrl}${url.url_endpoint_get_consultation}`
  state?: BaseResponse;
  constructor(private http: HttpClient) {}

  getState(params: any): Observable<any> {
    return this.http.get(`${this.proposeUrl}/proposeInfor`, { params: clearParamSearch(params) }).pipe(
      map((res: BaseResponse) => {
        this.state = res.data;
        return this.state;
      }),
      catchError(() => of(undefined))
    );
  }

  getList(params: any): Observable<any> {
    return this.http.get(this.baseUrl, { params: clearParamSearch(params) });
  }

  exportExcelInterview(body: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/export`, body);
  }

  exportExcelSalary(body: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/export-salary`, body);
  }

  exportExcelOption(params: any): Observable<any> {
    return this.http.get(`${this.baseUrl}/export-option`, { params: clearParamSearch(params) });
  }

  exportExcelConsultationCV(body: any): Observable<any> {
    return this.http.post(`${this.consultationUrl}/propose-export`, body);
  }

  delete(): Observable<any> {
    return this.http.delete(`${this.baseUrl}`);
  }

  sendInterview(params: SendInterviewModel): Observable<any> {
    return this.http.put(`${this.baseUrl}/sendInterviews`, params);
  }

  sendSalary(params: number[]): Observable<any> {
    return this.http.post(`${this.salaryUrl}/sendSalary`, params);
  }

  handlePlan(proposeDetailId: number): Observable<BaseResponse> {
    return this.http.get(`${this.baseUrl}/optionByProseDetailId?proposeDetailId=${proposeDetailId}`);
  }

  getExportTemplateListPlan(body: Consultion): Observable<BaseResponse> {
    return this.http.post(`${this.baseUrlTemplate}/getLinkReportConsultation/option`, body);
  }

  exportTemplate(body: DataTempalte): Observable<HttpResponse<Blob>> {
    return this.http.post<Blob>(`${this.baseUrlTemplate}/getReportConsultation`, body, {responseType: 'blob' as 'json', observe: 'response' });
  }
}

