import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { url } from '@hcm-mfe/learn-development/data-access/common'
import {clearParamSearch} from "@hcm-mfe/shared/common/utils";
import {environment} from "@hcm-mfe/shared/environment";
import {
  BodyExcel,
  ConsultationModel,
  DataCommonConsulation,
  ParamsSearchConsulation
} from "@hcm-mfe/learn-development/data-access/models";

@Injectable({
  providedIn: 'root',
})
export class ConsultationConfigService {
  constructor(private http: HttpClient) {}
  baseUrl = `${environment.baseUrl}${url.url_endpoint_get_consultationConfig}`;
  state: DataCommonConsulation | undefined;

  getStateConsultation(): Observable<any> {
    if (this.state) {
      return of(this.state);
    }
    return this.http.get(`${this.baseUrl}/consultationInfor`).pipe(
      map((res: any) => {
        this.state = res.data;
        return this.state;
      }),
      catchError(() => of(undefined))
    );
  }

  getAndSearch(params: ParamsSearchConsulation): Observable<any> {
    return this.http.get(`${this.baseUrl}/search`, { params: clearParamSearch(params) });
  }

  findById(id: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  createConsultation(body: ConsultationModel): Observable<any> {
    return this.http.post(`${this.baseUrl}`, body);
  }

  updateConsultation(body: ConsultationModel): Observable<any> {
    return this.http.put(`${this.baseUrl}`, body);
  }

  deleteById(id: string): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`);
  }

  exportExcel(params: BodyExcel): Observable<any> {
    return this.http.get(`${this.baseUrl}/export`, { params: clearParamSearch(params) });
  }
}
