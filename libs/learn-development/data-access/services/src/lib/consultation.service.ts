import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import {environment} from "@hcm-mfe/shared/environment";
import { maxInt32, url } from '@hcm-mfe/learn-development/data-access/common'
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";
import {Consultion, DataTempalte} from "@hcm-mfe/learn-development/data-access/models";
@Injectable({
  providedIn: 'root',
})
export class ConsultationService {
  baseUrl = `${environment.baseUrl}${url.url_endpoint_get_proposedUnit}`;
  baseUrlTemplate = `${environment.baseUrl}${url.url_endpoint_get_ld_template}`;
  consultationUrl = `${environment.baseUrl}${url.url_endpoint_get_consultation}`;
  state?: BaseResponse;

  constructor(private http: HttpClient) {}

  searchConsultation(params: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/search-consultation`, params );
  }

  saveConsultation(typeConsultation: number, param: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/${typeConsultation}/consultations`, param);
  }

  displayConsultation(idConsultation: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/display-consultation?id=${idConsultation}&page=0&size=${maxInt32}`);
  }

  updateConsultation(idConsultation: number, param: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/${idConsultation}/consultations`, param);
  }

  sendConsultation(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/send-consultation?id=${id}`);
  }

  deleteConsultation(proposeDetailId: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${proposeDetailId}/delete-consultations`);
  }

  exportExcel(body: any): Observable<any>{
    return this.http.post(`${this.consultationUrl}/export`, body);
  }

  getState(params: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/search-consultation`, params).pipe(
      map((res: BaseResponse) => {
        this.state = res.data;
        return this.state;
      }),
      catchError(() => of(undefined))
    );
  }

  getExportTemplate(body: Consultion): Observable<any> {
    return this.http.post(`${this.baseUrlTemplate}/getLinkReportConsultation`, body);
  }

  exportTemplate(body: DataTempalte): Observable<any> {
    return this.http.post(`${this.baseUrlTemplate}/getReportConsultation`, body, {responseType: 'blob' as 'json', observe: 'response' });
  }
}
