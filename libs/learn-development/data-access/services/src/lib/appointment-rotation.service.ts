import { maxInt32 } from "@hcm-mfe/learn-development/data-access/common";
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { url } from '@hcm-mfe/learn-development/data-access/common';
import { clearParamSearch } from "@hcm-mfe/shared/common/utils";
import { BaseResponse } from "@hcm-mfe/shared/data-access/models";
import { environment } from "@hcm-mfe/shared/environment";
import { catchError, map, Observable, of } from 'rxjs';
import { AppointmentRotationSearch, DataCommon, ScreenCode } from './../../../models/src/lib/proposed-unit.model';

@Injectable({
  providedIn: 'root',
})
export class AppointmentRotationService {
  baseUrl = `${environment.baseUrl}${url.url_endpoint_get_proposedUnit}`;

  state: DataCommon | undefined;
  constructor(private http: HttpClient) {}

  getState(params: ScreenCode): Observable<DataCommon | undefined> {
    return this.http.get(`${this.baseUrl}/proposeInfor`, { params: clearParamSearch(params) }).pipe(
      map((res: BaseResponse) => {
        this.state = res.data;
        return this.state;
      }),
      catchError(() => of(undefined))
    );
  }

  formGroupProposeMultiple(formGroupId?: number): Observable<any> {
    return this.http.get(`${environment.baseUrl}hcm-ld/v1.0/form-ld/listForm?formGroupId=${formGroupId}`);
  }

  searchAppointmentRotation(body: AppointmentRotationSearch): Observable<BaseResponse> {
    return this.http.post(`${environment.baseUrl}hcm-ld/v1.0/due-work-process/search`, body);
  }

  getExtendReason(code: string): Observable<BaseResponse> {
    return this.http.get(`${environment.baseUrl}${url.url_endpoint_get_formID}/system-config?code=${code}`);
  }

  saveProposalExtension(workProcessId: number, extendReason: string, reasonDetail: string): Observable<BaseResponse> {
    return this.http.post(`${environment.baseUrl}hcm-ld/v1.0/due-work-process/extend-propose?workProcessId=${workProcessId}&extendReason=${extendReason}&reasonDetail=${reasonDetail}`, null);
  }

  exportExcelAppointmentRotation(body: AppointmentRotationSearch): Observable<BaseResponse> {
    return this.http.post(`${environment.baseUrl}hcm-ld/v1.0/due-work-process/export`, body);
  }

  getAllFormGroup(): Observable<BaseResponse> {
    return this.http.post(`${environment.baseUrl}${url.url_endpoint_get_formID}/search`, {page: 0, size: maxInt32});
  }

}
