import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@hcm-mfe/shared/environment';
import { Observable } from 'rxjs';
import { url } from '@hcm-mfe/learn-development/data-access/common';
import { clearParamSearch } from '@hcm-mfe/shared/common/utils';
import { BaseResponse } from '@hcm-mfe/shared/data-access/models';
import { ApplyProposePublish, IssueTransferPopup, ListIssueTransferTable } from '@hcm-mfe/learn-development/data-access/models';

@Injectable({
  providedIn: 'root',
})
export class ListIssueTransferService {
  baseUrl = `${environment.baseUrl}${url.url_endpoint_get_proposedUnit}`;
  constructor(private http: HttpClient) {}

  search(params: ListIssueTransferTable): Observable<BaseResponse> {
    return this.http.get(`${this.baseUrl}/list-issue-transfer`, {
      params: clearParamSearch(params),
    });
  }

  issueTransfer(body: IssueTransferPopup): Observable<BaseResponse> {
    return this.http.post(`${this.baseUrl}/issueTransferList`, body);
  }

  applyProposePublish(body: ApplyProposePublish): Observable<BaseResponse> {
    return this.http.post(`${this.baseUrl}/applyProposePublish`, body);
  }

  exportExcel(params: ListIssueTransferTable): Observable<BaseResponse> {
    return this.http.get(`${this.baseUrl}/export-issue-transfer`, {
      params: clearParamSearch(params),
    });
  }
}
