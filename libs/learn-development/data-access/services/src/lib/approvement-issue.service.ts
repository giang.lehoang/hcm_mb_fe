import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { url } from '@hcm-mfe/learn-development/data-access/common';
import { DataCommon, ParamPromulgate, SearchApprovementIssue } from "@hcm-mfe/learn-development/data-access/models";
import { clearParamSearch } from "@hcm-mfe/shared/common/utils";
import { BaseResponse } from "@hcm-mfe/shared/data-access/models";
import { environment } from "@hcm-mfe/shared/environment";
import { saveAs } from 'file-saver';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ApprovementIssueService {
  baseUrl = `${environment.baseUrl}${url.url_endpoint_get_proposedUnit}`;
  state: DataCommon | undefined;
  constructor(private http: HttpClient) {}

  search(params: SearchApprovementIssue): Observable<any> {
    return this.http.get(`${this.baseUrl}/list-publish`, { params: clearParamSearch(params) });
  }

  promulgate(proposeId: number, groupProposeType: number): Observable<any> {
    return this.http.put(`${this.baseUrl}/${proposeId}/promulgate`, groupProposeType);
  }

  export(params: SearchApprovementIssue): Observable<any> {
    return this.http.get(`${this.baseUrl}/export-publish`, { params: clearParamSearch(params) });
  }

  getState(params: any): Observable<any> {
    return this.http.get(`${this.baseUrl}/proposeInfor`, {params}).pipe(
      map((res: BaseResponse) => {
        this.state = res.data;
        return this.state;
      }),
      catchError(() => of(undefined))
    );
  }

  getListApprovement(params: ParamPromulgate): Observable<any>{
    return this.http.get(`${this.baseUrl}/listEmpPublish`, { params: clearParamSearch(params)})
  }

  getListDetail(params: ParamPromulgate): Observable<any>{
    return this.http.get(`${this.baseUrl}/viewDetailPublish`, { params: clearParamSearch(params)})
  }


  getFile(params: ParamPromulgate): Observable<any>{
    return this.http.get(`${this.baseUrl}/viewDetailFile`, { params: clearParamSearch(params)});
  }

  downloadFile(docId: number, fileName: string): Observable<boolean> {
    return this.http
      .get(`${environment.baseUrl}hcm_utilities/v1.0/files/download/${docId}`, {
        responseType: 'arraybuffer',
      })
      .pipe(
        map((val) => {
          saveAs(
            new Blob([val], {
              type: 'application/octet-stream',
            }),
            fileName
          );
          return true;
        })
      );
  }

  handleApprovement(id:number, type: number): Observable<any>{
    return this.http.put(`${this.baseUrl}/${id}/promulgate?groupProposeType=${type}`,  null)
  }

  getLogs(id: number, type: number, screenCode: string, pageNumber: number, pageSize: number): Observable<BaseResponse> {
    const queryParams = {
      proposeId: id,
      proposeType: type,
      screenCode: screenCode,
      page: pageNumber,
      size: pageSize
    }
    return this.http.get(`${environment.baseUrl}hcm-ld/v1.0/propose-log`, { params: clearParamSearch(queryParams)})
  }
}
