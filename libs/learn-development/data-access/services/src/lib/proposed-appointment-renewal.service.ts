import { EmployeeInfoMultipleEdit, ProposeTypeModel, ScreenCode } from './../../../models/src/lib/proposed-unit.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {environment} from "@hcm-mfe/shared/environment";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";
import { url } from '@hcm-mfe/learn-development/data-access/common'
import {clearParamSearch} from "@hcm-mfe/shared/common/utils";
import {
  ProposedAppointmentRenewal,
  SaveEmployeeFormGroupModel,
  SearchParamsEmp
} from "@hcm-mfe/learn-development/data-access/models";

@Injectable({
  providedIn: 'root',
})
export class ProposedAppointmentRenewalService {
  baseUrl = `${environment.baseUrl}${url.url_endpoint_get_proposedUnit}`;
  employeeUrl = `${environment.baseUrl}hcm-person-info/v1.0/employees`;
  newUrl = `${'http://10.7.18.235:10435/'}${url.url_endpoint_get_proposedUnit}`;
  state?: BaseResponse;
  baseUrlSalry = `${environment.baseUrl}${url.url_endpoint_salary_list}`;
  baseUrlHandleOffer = `${environment.baseUrl}${url.url_endpoint_list_plan}`;
  baseUrlDowloadFile = `${environment.baseUrl}${url.url_endpoint_download_file_v10}`;

  constructor(private http: HttpClient) {}

  searchInterview(params: any): Observable<any> {
    return this.http.get(`${environment.baseUrl}${url.url_endpoint_get_interview}`, { params: clearParamSearch(params) });
  }

  searchEmployee(searchParams: SearchParamsEmp): Observable<any> {
    return this.http.get(`${this.employeeUrl}/search/auth`, { params: clearParamSearch(searchParams)} );
  }

  getProposeId(): Observable<any> {
    return this.http.get(`${this.baseUrl}/proposeId`);
  }

  appletEmployee(param: any, screenCode: string): Observable<any> {
    return this.http.post(`${this.baseUrl}/applyPropose?screenCode=${screenCode}`, param);
  }

  addEmployee(param: any, screenCode: string): Observable<any> {
    return this.http.post(`${this.baseUrl}/createProposes?screenCode=${screenCode}`, param);
  }

  listEmpPropose(params: any): Observable<any> {
    return this.http.get(`${this.baseUrl}/listProposeDetail`, {params});
  }

  viewDetailPropose(proposeDetailId: number, screenCode: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/${proposeDetailId}/viewDetail?screenCode=${screenCode}`);
  }

  formGroupProposeMultiple(formGroupId: number): Observable<any> {
    return this.http.get(`${environment.baseUrl}hcm-ld/v1.0/form-ld/listForm?formGroupId=${formGroupId}`);
  }

  editItemProposeMultiple(body: EmployeeInfoMultipleEdit, screenCode: string): Observable<any> {
    return this.http.put(`${environment.baseUrl}hcm-ld/v1.0/propose/updateProposeLine?screenCode=${screenCode}`, body);
  }

  uploadFileAppointmentRenewal(formData: FormData): Observable<any> {
    return this.http.post(`${this.baseUrl}/proposeUpload`, formData);
  }

  updateProposedAppointmentRenewal(proposed: ProposedAppointmentRenewal | ProposedAppointmentRenewal[], screenCode: string): Observable<any> {
    return this.http.put(`${this.baseUrl}/appoint/update?screenCode=${screenCode}`, proposed);
  }


  getPersonnelPlace(posIds: number): Observable<any> {
    return this.http.get(`${this.employeeUrl}/positions?posIds=${posIds}`);
  }

  checkCreateByAlternative(employeeCode: number, contentUnit: number) {
    return this.http.get(`${this.baseUrl}/check-create-by-alternative?employeeCode=${employeeCode}&contentUnit=${contentUnit}`);
  }

  deleteFile(id: number): Observable<any> {
    return this.http.post(`${this.baseUrl}/deleteFile/${id}`, null);
  }

  saveEmployeeFormGroup(screenCode: string, body: SaveEmployeeFormGroupModel[]): Observable<BaseResponse> {
    return this.http.post(`${this.baseUrl}/appoint/createListProposes?screenCode=${screenCode}`, body)
  }

  //API gửi duyệt mới
  sendProposeAppointment(id: number | null, body: ProposedAppointmentRenewal | ProposedAppointmentRenewal[], 
    params: ProposeTypeModel): Observable<BaseResponse> {
    return this.http.post(`${this.baseUrl}/${id}/send-approve`, body, { params: clearParamSearch(params) });
  }

  //ghi nhận đồng ý mới
  agreeUnitAppointment(id: number | null, body: ProposedAppointmentRenewal | ProposedAppointmentRenewal[], 
    params: ProposeTypeModel): Observable<BaseResponse> {
    return this.http.post(`${this.baseUrl}/${id}/agree-unit?`, body, { params: clearParamSearch(params) });
  }
}
