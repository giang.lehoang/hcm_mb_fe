import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { url } from '@hcm-mfe/learn-development/data-access/common';
import { DataCommon, ParamErrorSuccess, ParamSearchInterview } from '@hcm-mfe/learn-development/data-access/models';
import { clearParamSearch } from "@hcm-mfe/shared/common/utils";
import { BaseResponse } from "@hcm-mfe/shared/data-access/models";
import { environment } from "@hcm-mfe/shared/environment";
import { saveAs } from "file-saver";
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root',
})
export class InterviewService {
  baseUrlProposeInfor = `${environment.baseUrl}${url.url_endpoint_get_proposedUnit}`;
  baseUrl = `${environment.baseUrl}${url.url_endpoint_get_interview}`;
  state: DataCommon | undefined;
  baseUrlTitle = `${environment.baseUrl}${url.url_endpoint_get_formID}`;


  constructor(private http: HttpClient) {}

  getState(): Observable<any> {
    if (this.state) {
      return of(this.state);
    }
    return this.http.get(`${this.baseUrlProposeInfor}/proposeInfor`, {}).pipe(
      map((res: BaseResponse) => {
        this.state = res.data;
        return this.state;
      }),
      catchError(() => of(undefined))
    );
  }

  searchInterview(params: ParamSearchInterview): Observable<any> {
    return this.http.get(`${this.baseUrl}`, { params: clearParamSearch(params) });
  }

  updateInterview(data: ParamSearchInterview, isSend: boolean): Observable<any> {
    return this.http.post(`${this.baseUrl}?isSend=${isSend}`, data);
  }

  sendInterview(data: ParamSearchInterview, isSend: boolean): Observable<any> {
    return this.http.post(`${this.baseUrl}/sendApprove?isSend=${isSend}`, data);
  }

  sendInterviewSearch(params: number[]): Observable<any> {
    return this.http.put(`${this.baseUrl}/sentInterview?interviewIds=${params}`, {});
  }

  detailSendInterview(params: number[]): Observable<any> {
    return this.http.get(`${this.baseUrl}/interviewUpdate?interviewIds=${params}`);
  }

  exportExcelInterview(params: ParamSearchInterview): Observable<any> {
    return this.http.get(`${this.baseUrl}/export`, { params: clearParamSearch(params) });
  }

  confirmInterview(body: number[], isConfirm: boolean, comment: string): Observable<any> {
    return this.http.put(`${this.baseUrl}/confirm?body=${body}&isConfirm=${isConfirm}&comment=${comment}`, null)
  }

  uploadFileInterview(formUpload: FormData): Observable<any> {
    return this.http.post(`${this.baseUrl}/upload`, formUpload)
  }

  findTitleInterview(code: string): Observable<any> {
    return this.http.get(`${this.baseUrlTitle}/common/findByCode?code=${code}`)
  }

  downloadFile(docId: number, fileName: string): Observable<boolean> {
    return this.http
      .get(`${environment.baseUrl}hcm_utilities/v1.0/files/download/${docId}`, {
        responseType: 'arraybuffer',
      })
      .pipe(
        map((val: any) => {
          saveAs(
            new Blob([val], {
              type: 'application/octet-stream',
            }),
            fileName
          );
          return true;
        })
      );
  }

  importFileInterview(body: FormData): Observable<BaseResponse> {
    return this.http.post(`${this.baseUrl}/import`, body);
  }

  getStatusFileInterview(idMessage: string): Observable<BaseResponse> {
    return this.http.get(`${this.baseUrl}/import/checking?transKey=${idMessage}`)
  }

  getErrorSuccessFile(params: ParamErrorSuccess): Observable<BaseResponse> {
    return this.http.get(`${this.baseUrl}/import/data`, { params: clearParamSearch(params) });
  }

  saveDataTableSuccess(idMessage: string): Observable<BaseResponse> {
    return this.http.get(`${this.baseUrl}/import/import-success-data?importKey=${idMessage}`);
  }

  dowloadTemplate(): Observable<BaseResponse> {
    return this.http.get(`${this.baseUrl}/template-import`)
  }

  exportExcelFile(idMessage: string): Observable<BaseResponse> {
    return this.http.get(`${this.baseUrl}/import/export-data-failed?importKey=${idMessage}`)
  }

}
