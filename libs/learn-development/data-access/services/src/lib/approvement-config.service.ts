import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import {ApproveConfig, BodyExcel} from "@hcm-mfe/learn-development/data-access/models";
import { catchError, map } from 'rxjs/operators';
import {clearParamSearch} from "@hcm-mfe/shared/common/utils";
import {environment} from "@hcm-mfe/shared/environment";
import { url } from '@hcm-mfe/learn-development/data-access/common';
import { BaseResponse } from '@hcm-mfe/shared/data-access/models';

@Injectable({
  providedIn: 'root',
})
export class ApprovementConfigService {
  baseUrl = `${environment.baseUrl}${url.url_endpoint_get_approvementConfig}`;
  issueLevelUrl = `${environment.baseUrl}${url.url_endpoint_get_issueLevel}`;
  employeeUrl = `${environment.baseUrl}hcm-person-info/v1.0/employees`;
  state: any;
  constructor(private http: HttpClient) {}

  search(params: ApproveConfig): Observable<any> {
    return this.http.post(`${this.baseUrl}/search`, clearParamSearch(params));
  }

  findByCode(code: string): Observable<ApproveConfig> {
    return this.http.get<ApproveConfig>(`${this.baseUrl}/${code}`).pipe(map((val: any) => val.data));
  }

  create(request: ApproveConfig): Observable<any> {
    return this.http.post(this.baseUrl, request);
  }

  update(request: ApproveConfig): Observable<any> {
    return this.http.put(this.baseUrl, request);
  }

  deleteItem(code: string): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${code}`, {});
  }

  getEmployeeCode(code: string): Observable<any> {
    return this.http
      .get<ApproveConfig>(
        `${this.employeeUrl}/search?keySearch=${code}&startRecord=0&pageSize=1`
      )
      .pipe(map((val: any) => val.data));
  }

  getState(): Observable<any> {
    return this.http.get(`${this.baseUrl}/infoLD`).pipe(
      map((res: BaseResponse) => {
        this.state = res.data;
        return this.state;
      }),
      catchError(() => of(undefined))
    );
  }

  exportExcel(params: any): Observable<any> {
    return this.http.get(`${this.baseUrl}/export`, { params: clearParamSearch(params) });
  }

  getEmployeeByJobIds(jobIds: string, orgId: number): Observable<any> {
    return this.http.get(`${this.employeeUrl}/get-detail?jobIds=${jobIds}&orgId=${orgId}`);
  }

  getIssueLevelByCode(code: number, signer: boolean, approval: boolean): Observable<any> {
    return this.http.get(`${this.issueLevelUrl}/jobApprovalAndSigner?code=${code}&signer=${signer}&approval=${approval}`);
  }

  getSubAreaByCode(code: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/getSubArea?lvaValue=${code}`);
  }

}
