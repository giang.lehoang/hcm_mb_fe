import { url } from '@hcm-mfe/learn-development/data-access/common'
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import {environment} from "@hcm-mfe/shared/environment";
import {clearParamSearch} from "@hcm-mfe/shared/common/utils";
import {IssueLevel} from "@hcm-mfe/learn-development/data-access/models";

@Injectable({
  providedIn: 'root',
})
export class IssueLevelService {
  baseUrl = `${environment.baseUrl}${url.url_endpoint_get_issueLevel}`;
  state: any;
  constructor(private http: HttpClient) {}

  search(params: IssueLevel): Observable<any> {
    return this.http.get(`${this.baseUrl}`, { params: clearParamSearch(params) });
  }

  createIssueLevel(request: IssueLevel): Observable<any> {
    return this.http.post(this.baseUrl, request);
  }

  updateIssueLevel(request: IssueLevel): Observable<any> {
    return this.http.put(this.baseUrl, request);
  }

  getIssueLevelByCode(code: string): Observable<IssueLevel> {
    return this.http.get<IssueLevel>(`${this.baseUrl}/${code}`).pipe(map((val: any) => val?.data));
  }

  deleteItem(code: string): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${code}`);
  }

  getState(): Observable<any> {
    if (this.state) {
      return of(this.state);
    }
    return this.http.get(`${this.baseUrl}/title`).pipe(
      map((res: any) => {
        this.state = res.data;
        return this.state;
      }),
      catchError(() => of(undefined))
    );
  }

  exportExcel(): Observable<any> {
    return this.http.get(`${this.baseUrl}/export`);
  }
}
