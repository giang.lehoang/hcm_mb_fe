import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import {environment} from "@hcm-mfe/shared/environment";
import { url } from '@hcm-mfe/learn-development/data-access/common'
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";
import {ProposedExcel} from "@hcm-mfe/learn-development/data-access/models";
import {clearParamSearch} from "@hcm-mfe/shared/common/utils";

@Injectable({
  providedIn: 'root',
})
export class ProposedHandleService {
  baseUrl = `${environment.baseUrl}${url.url_endpoint_get_proposedUnit}`;
  employeeUrl = `${environment.baseUrl}hcm-person-info/v1.0/employees/search`;
  newUrl = `${'http://10.7.18.50:10435/'}${url.url_endpoint_get_proposedUnit}`
  state?: BaseResponse;
  baseUrlPlan = `${environment.baseUrl}${url.url_endpoint_list_plan}`;

  constructor(private http: HttpClient) {}

  synthesizeInformation(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/ld-summary-comment?id=${id}`);
  }

  getState(): Observable<any> {
    if (this.state) {
      return of(this.state);
    }
    return this.http.get(`${this.baseUrl}/proposeInfor`).pipe(
      map((res: BaseResponse) => {
        this.state = res.data;
        return this.state;
      }),
      catchError(() => of(undefined))
    );
  }

  sendToBrower(id: string | number): Observable<any> {
    // gửi duyệt
    return this.http.post(`${this.baseUrl}/${id}/send-approve`, {});
  }

  accept(id: number) {
    return this.http.post(`${this.baseUrl}/${id}/agree-unit`, {});
  }

  reject(id: number, note: string) {
    return this.http.post(`${this.baseUrl}/${id}/reject-approve`, note);
  }

  agreeLeader(id: number) {
    return this.http.post(`${this.baseUrl}/${id}/agree-leader`, {});
  }

  rejectLeader(id: number, note: string) {
    return this.http.post(`${this.baseUrl}/${id}/reject-leader`, note);
  }

  otherIdea(id: number, note: string) {
    return this.http.post(`${this.baseUrl}/${id}/other-idea`, note);
  }

  exportExcel(params: ProposedExcel): Observable<any> {
    return this.http.get(`${this.baseUrl}/export`, { params: clearParamSearch(params) });
  }

  listFormGroupConcurrentPosition(): Observable<BaseResponse> {
    return this.http.get(`${this.baseUrlPlan}/alternative-dropdown`);
  }




}
