import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import {environment} from "@hcm-mfe/shared/environment";
import { url } from '@hcm-mfe/learn-development/data-access/common'
import {BodyExcel, FormLDContent} from "@hcm-mfe/learn-development/data-access/models";
import {clearParamSearch} from "@hcm-mfe/shared/common/utils";
import { BaseResponse } from '@hcm-mfe/shared/data-access/models';

@Injectable({
  providedIn: 'root',
})
export class FormLDService {
  baseUrl = `${environment.baseUrl}${url.url_endpoint_get_formID}`;
  state?: BaseResponse;
  constructor(private http: HttpClient) {}

  getFormLDByCode(code: number): Observable<FormLDContent> {
    return this.http.get<FormLDContent>(`${this.baseUrl}/${code}`).pipe(map((val: any) => val?.data));
  }

  getAllFormLD(params: FormLDContent): Observable<any> {
    return this.http.post(`${this.baseUrl}/search`, clearParamSearch(params));
  }

  deleteFormLD(code: string): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${code}`);
  }

  updateFormLD(request: any): Observable<any> {
    return this.http.put(this.baseUrl, request);
  }

  addFormLD(request: any): Observable<any> {
    return this.http.post(this.baseUrl, request);
  }

  exportExcel(params: BodyExcel): Observable<any> {
    return this.http.get(`${this.baseUrl}/export`, { params: clearParamSearch(params) });
  }

  getState(): Observable<any> {
    return this.http.get(`${this.baseUrl}/form-category`).pipe(
      map((res: BaseResponse) => {
        this.state = res.data;
        return this.state;
      }),
      catchError(() => of(undefined))
    );
  }
}
