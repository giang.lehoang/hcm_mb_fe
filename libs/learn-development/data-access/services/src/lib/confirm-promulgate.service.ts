import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { url } from '@hcm-mfe/learn-development/data-access/common'
import { saveAs } from 'file-saver';
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";
import {environment} from "@hcm-mfe/shared/environment";
import {clearParamSearch} from "@hcm-mfe/shared/common/utils";
import { DataCommon, ParamPromulgate } from "@hcm-mfe/learn-development/data-access/models";

@Injectable({
  providedIn: 'root',
})
export class ConfirmPromulgateService {
  constructor(private http: HttpClient) {}
  baseUrl = `${environment.baseUrl}${url.url_endpoint_get_proposedUnit}`;
  state: DataCommon | undefined;
  getState(params: any): Observable<any> {
    return this.http.get(`${this.baseUrl}/proposeInfor`, {params}).pipe(
      map((res:  BaseResponse) => {
        this.state = res.data;
        return this.state;
      }),
      catchError(() => of(undefined))
      );
    }

  getAndSearch(params: any) : Observable<any> {
    return this.http.get(`${this.baseUrl}/list-publish`, { params: clearParamSearch(params)});
  }

  getPromulgate(params: ParamPromulgate) : Observable<any> {
    return this.http.get(`${this.baseUrl}/listEmpPublish`, { params: clearParamSearch(params)});
  }

  getDetail(params: ParamPromulgate): Observable<any> {
    return this.http.get(`${this.baseUrl}/viewDetailPublish`, { params: clearParamSearch(params)});
  }

  getFile(params: ParamPromulgate): Observable<any>{
    return this.http.get(`${this.baseUrl}/viewDetailFile`, { params: clearParamSearch(params)});
  }

  downloadFile(docId: number, fileName: string): Observable<boolean> {
    return this.http
      .get(`${environment.baseUrl}hcm_utilities/v1.0/files/download/${docId}`, {
        responseType: 'arraybuffer',
      })
      .pipe(
        map((val) => {
          saveAs(
            new Blob([val], {
              type: 'application/octet-stream',
            }),
            fileName
          );
          return true;
        })
      );
  }

  confirmAndReject(body: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/confirm?screenCode=HCM-LND-GDNS`, body);
  }

  cancelConfirmAndReject(body: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/confirmCancel?screenCode=HCM-LND-GDNS`, body);
  }

  export(params: any): Observable<any> {
    return this.http.get(`${this.baseUrl}/export-publish`, { params: clearParamSearch(params) });
  }
}
