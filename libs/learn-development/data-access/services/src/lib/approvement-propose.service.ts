import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import {environment} from "@hcm-mfe/shared/environment";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";
import {ListApprove, ListGroupPropose, RoleInterface, ScreenCode} from "@hcm-mfe/learn-development/data-access/models";
import { url } from '@hcm-mfe/learn-development/data-access/common'
import {clearParamSearch} from "@hcm-mfe/shared/common/utils";

@Injectable({
  providedIn: 'root',
})
export class ApprovementProposeService {
  baseUrl = `${environment.baseUrl}${url.url_endpoint_get_proposedUnit}`;
  urlLocal = `${environment.baseUrl}`;
  state?: BaseResponse;
  constructor(private http: HttpClient) {}

  search(params: ListApprove): Observable<any> {
    return this.http.get(`${this.baseUrl}/list`, { params: clearParamSearch(params) });
  }

  approveKSPTNL(proposeID: number, grProposeType: number, action: number) {
    return this.http.put(
      `${this.baseUrl}/${proposeID}/processPromulgatePropose?groupProposeType=${grProposeType}&action=${action}`,
      {}
    );
  }

  getState(params: ScreenCode): Observable<any> {
    return this.http.get(`${this.baseUrl}/proposeInfor`, { params: clearParamSearch(params) }).pipe(
      map((res: BaseResponse) => {
        this.state = res.data;
        return this.state;
      }),
      catchError(() => of(undefined))
    );
  }

  groupPropose(listGroup: ListGroupPropose[], params: ScreenCode): Observable<any>{
    return this.http.post(`${this.baseUrl}/group`, listGroup, { params: clearParamSearch(params) });
  }

  getRolesByUsername(): Observable<any>{
    return this.http.get(`${this.urlLocal}hcm-ld/v1.0/users/role-function-permission`).pipe(map((val: BaseResponse)=> val?.data?.roleFunctionPermissions?.map((item: RoleInterface) => item.roleCode)))
  }
}
