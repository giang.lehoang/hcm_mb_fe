import { saveAs } from 'file-saver';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import {environment} from "@hcm-mfe/shared/environment";
import { bodyExcel, Template } from "@hcm-mfe/learn-development/data-access/models";
import {clearParamSearch} from "@hcm-mfe/shared/common/utils";
import { url } from '@hcm-mfe/learn-development/data-access/common';
import { BaseResponse } from '@hcm-mfe/shared/data-access/models';

@Injectable({
  providedIn: 'root',
})
export class TemplateService {
  baseUrl = `${environment.baseUrl}${url.url_endpoint_get_ld_template}`;
  state: any;
  constructor(private http: HttpClient) {}

  search(params: Template): Observable<any> {
    return this.http.post(`${this.baseUrl}/search`, clearParamSearch(params));
  }

  findByCode(code: string): Observable<Template> {
    return this.http.get<Template>(`${this.baseUrl}/${code}`).pipe(map((val: any) => val.data));
  }

  create(request: FormData): Observable<any> {
    return this.http.post(this.baseUrl, request);
  }

  update(request: FormData, code: string): Observable<any> {
    return this.http.put(`${this.baseUrl}/${code}`, request);
  }

  deleteItem(code: string): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${code}`, {});
  }

  getState(): Observable<any> {
    return this.http.get(`${this.baseUrl}/searchDM`).pipe(
      map((res: BaseResponse) => {
        this.state = res.data;
        return this.state;
      }),
      catchError(() => of(undefined))
    );
  }

  downloadFile(docId: number, fileName: string): Observable<boolean> {
    return this.http
      .get(`${environment.utilitiesUrl}/v1.0/files/download/${docId}`, {
        responseType: 'arraybuffer',
      })
      .pipe(
        map((val: any) => {
          saveAs(
            new Blob([val], {
              type: 'application/octet-stream',
            }),
            fileName
          );
          return true;
        })
      );
  }

  exportExcel(params: bodyExcel): Observable<any> {
    return this.http.get(`${this.baseUrl}/export`, { params: clearParamSearch(params) });
  }
}
