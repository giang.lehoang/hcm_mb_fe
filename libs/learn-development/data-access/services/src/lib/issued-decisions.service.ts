import { url } from '@hcm-mfe/learn-development/data-access/common'
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import {clearParamSearch} from "@hcm-mfe/shared/common/utils";
import {environment} from "@hcm-mfe/shared/environment";
import {
  bodyExcel, BodyTransferToIssue, DataCommon,
  DataModelMergeIssuedDecision, IssueTransfer,
  ScreenCode,
  SearchIssuedDecision
} from "@hcm-mfe/learn-development/data-access/models";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";

@Injectable({
  providedIn: 'root',
})
export class IssuedDecisionsService {
  baseUrl = `${environment.baseUrl}${url.url_endpoint_get_proposedUnit}`;
  state: DataCommon | undefined;
  constructor(private http: HttpClient) {}

  getState(params: ScreenCode): Observable<DataCommon | undefined> {
    return this.http.get(`${this.baseUrl}/proposeInfor`, { params: clearParamSearch(params) }).pipe(
      map((res: BaseResponse) => {
        this.state = res.data;
        return this.state;
      }),
      catchError(() => of(undefined))
    );
  }

  search(params: SearchIssuedDecision): Observable<BaseResponse> {
    return this.http.get(`${this.baseUrl}/list-transfer`, { params: clearParamSearch(params) });
  }

  exportExcel(params: bodyExcel): Observable<BaseResponse> {
    return this.http.get(`${this.baseUrl}/export-list-transfer`, { params: clearParamSearch(params) });
  }

  mergePropose(listPropose: DataModelMergeIssuedDecision[], screenCode: string, optionNotes: string): Observable<any> {
    return this.http.post(`${this.baseUrl}/groupPublish?screenCode=${screenCode}&optionNotes=${optionNotes}`, listPropose);
  }

  deletePropose(groupPublishId: number, proposeDetailId: number) {
    return this.http.post(`${this.baseUrl}/removeProposeDetailPublish/${groupPublishId}/${proposeDetailId}`, null);
  }

  actionIssueDecision(id: number, screenCode: string, proposeCategory: string, action: string): Observable<BaseResponse> {
    return this.http.get(`${this.baseUrl}/listEmpPublish?id=${id}&screenCode=${screenCode}&proposeCategory=${proposeCategory}&action=${action}`,);
  }

  viewDetail(id: number, screenCode: string, proposeCategory: string, action: string): Observable<BaseResponse> {
    return this.http.get(`${this.baseUrl}/viewDetailPublish?id=${id}&screenCode=${screenCode}&proposeCategory=${proposeCategory}&action=${action}`,);
  }

  deleteIssuedGroup(id: number,) {
    return this.http.delete(`${this.baseUrl}/deleteGroupPublish?id=${id}`,);
  }

  transferToIssue(body: BodyTransferToIssue, params: IssueTransfer): Observable<BaseResponse> {
    return this.http.post(`${this.baseUrl}/issueTransfer`, body, { params: clearParamSearch(params) });
  }

  getLogs(id: number, proposeType: string, screenCode: string): Observable<BaseResponse> {
    const queryParams = {
      proposeId: id,
      proposeType: proposeType,
      screenCode: screenCode,
    };
    return this.http.get(`${environment.baseUrl}hcm-ld/v1.0/propose-log`, { params: clearParamSearch(queryParams) });
  }

}


