import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@hcm-mfe/shared/environment';
import { Observable } from 'rxjs';

import { url } from '@hcm-mfe/learn-development/data-access/common';
import {
  ProposedModel,
  ScreenCode,
} from '@hcm-mfe/learn-development/data-access/models';
import { clearParamSearch } from '@hcm-mfe/shared/common/utils';

@Injectable({
  providedIn: 'root',
})
export class ProposeTGCVService {
  baseUrl = `${environment.baseUrl}${url.url_endpoint_get_proposedUnit}`;
  constructor(private http: HttpClient) {}

  uploadFileTGCV(formData: FormData): Observable<any> {
    return this.http.post(`${this.baseUrl}/proposeUpload`, formData);
  }

  updateTGCV(data: ProposedModel, params: ScreenCode): Observable<any> {
    return this.http.put(`${this.baseUrl}/update`, data, {
      params: clearParamSearch(params),
    });
  }

  deleteFile(id: number): Observable<any> {
    return this.http.post(`${this.baseUrl}/deleteFile/${id}`, null);
  }
}
