import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {environment} from "@hcm-mfe/shared/environment";
import { url } from '@hcm-mfe/learn-development/data-access/common'
import {clearParamSearch} from "@hcm-mfe/shared/common/utils";
import {Salary} from "@hcm-mfe/learn-development/data-access/models";

@Injectable({
  providedIn: 'root',
})
export class SalaryService {
  baseUrlSalary = `${environment.baseUrl}${url.url_endpoint_salary_list}`;

  constructor(private http: HttpClient) {}

  getAndSearch(params: Salary): Observable<any> {
    return this.http.post(`${this.baseUrlSalary}/search`, clearParamSearch(params));
  }

  exportExcel(body: Salary): Observable<any> {
    return this.http.post(`${this.baseUrlSalary}/export`, body);
  }

  wrapperActionSalary(body: Salary | Salary[], action: string): Observable<any> {
    return this.http.post(`${this.baseUrlSalary}/handle?action=${action}`, body );
  }
}
