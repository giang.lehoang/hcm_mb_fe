import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from "@hcm-mfe/shared/environment";
import { saveAs } from 'file-saver';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { Constants, maxInt32, url, Utils } from "@hcm-mfe/learn-development/data-access/common";
import {
  ApproveKSPTNL,
  Detail, InforConcurrentPositionModel,
  IssueCorrection,
  IssueTransfer, ListHandleOffer, NoteKSPTNL,
  Proposed, ProposedExcel,
  ProposedModel, ProposeTypeModel, RecommendIndividualModel,
  ReturnPropose,
  ScreenCode, WithdrawPropose
} from "@hcm-mfe/learn-development/data-access/models";
import { clearParamSearch } from "@hcm-mfe/shared/common/utils";
import { BaseResponse } from "@hcm-mfe/shared/data-access/models";

@Injectable({
  providedIn: 'root',
})
export class ProposedUnitService {
  baseUrl = `${environment.baseUrl}${url.url_endpoint_get_proposedUnit}`;
  baseUrlTempalte = `${environment.baseUrl}${url.url_endpoint_get_ld_template}`;
  baseUrlApprove = `${environment.baseUrl}${url.url_endpoint_get_proposedUnit_approve}`;
  employeeUrl = `${environment.baseUrl}hcm-person-info/v1.0/employees/search`;
  newUrl = `${'http://10.7.18.235:10435/'}${url.url_endpoint_get_proposedUnit}`;
  state?: BaseResponse;
  baseUrlSalry = `${environment.baseUrl}${url.url_endpoint_salary_list}`;
  baseUrlHandleOffer = `${environment.baseUrl}${url.url_endpoint_list_plan}`;
  baseUrlDowloadFile = `${environment.baseUrl}${url.url_endpoint_download_file_v10}`;
  lineUrl = `${environment.baseUrl}hcm-model-plan/v1.0/line?`;
  lineDetailUrl = `${environment.baseUrl}${url.url_enpoint_line_details}`;
  urlLocal = `${environment.baseUrl}`;

  constructor(private http: HttpClient) {}

  search(params: Proposed): Observable<any> {
    return this.http.get(`${this.baseUrl}/list`, { params: clearParamSearch(params) });
  }

  findById(params: Detail): Observable<any> {
    return this.http.get(`${this.baseUrl}`, { params: clearParamSearch(params) }).pipe(map((val: any) => val.data));
  }

  create(data: ProposedModel[], params: ScreenCode): Observable<any> {
    return this.http.post(this.baseUrl, data, { params: clearParamSearch(params) });
  }

  update(data: ProposedModel[] | undefined, params: ScreenCode): Observable<BaseResponse> {
    return this.http.put(this.baseUrl, data, { params: clearParamSearch(params) });
  }

  deletePropose(id: number) {
    return this.http.delete(`${this.baseUrl}/${id}`);
  }

  deleteProposeListLD(proposeID: number | null, groupProposeType: number | null) {
    return this.http.delete(`${this.baseUrl}/ld/${proposeID}?groupProposeType=${groupProposeType}`);
  }

  getEmployee(code: string): Observable<any> {
    return this.http
      .get(`${this.employeeUrl}?keySearch=${code}&startRecord=0&pageSize=1`)
      .pipe(map((val: any) => val.data));
  }

  issueDecision(proposeId: number): Observable<any> {
    return this.http.post(`${this.baseUrl}/${proposeId}/issue-decision`, {});
  }

  getState(params: ScreenCode): Observable<any> {
    return this.http.get(`${this.baseUrl}/proposeInfor`, { params: clearParamSearch(params) }).pipe(
      map((res: BaseResponse) => {
        this.state = res.data;
        return this.state;
      }),
      catchError(() => of(undefined))
    );
  }

  approveKSPTNL(proposeId: number, groupProposeType: number, action: number, note: NoteKSPTNL): Observable<any> {
    return this.http.put(
      `${this.baseUrl}/${proposeId}/processPromulgatePropose?groupProposeType=${groupProposeType}&action=${action}`,
      note
    );
  }

  approvePTNL(proposeID: number | null, message: string | null, params: ApproveKSPTNL): Observable<any> {
    return this.http.put(
      `${this.baseUrl}/${proposeID}/processPromulgate`,
      { message: message || '' },
      { params: clearParamSearch(params) }
    );
  }

  moreExplanation(proposeID: number, body: any, params: any): Observable<any> {
    return this.http.put(`${this.baseUrl}/${proposeID}/processPromulgate/explanation`, body, {
      params: clearParamSearch(params),
    });
  }

  issueTransfer(body: any, params: IssueTransfer): Observable<any> {
    return this.http.post(`${this.baseUrl}/issueTransfer`, body, { params: clearParamSearch(params) });
  }

  issueLater(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/issuedLater?id=${id}`);
  }

  issueCorrection(body: IssueCorrection, params: IssueTransfer): Observable<any> {
    return this.http.post(`${this.baseUrl}/issueCorrection`, body, { params: clearParamSearch(params) });
  }

  requestCancelation(params: IssueTransfer, body: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/requestCancellation`, body, { params: clearParamSearch(params) });
  }

  cancelPlan(cancelOption: any, screenCode: string): Observable<any> {
    return this.http.post(`${this.baseUrl}/cancelOption?screenCode=${screenCode}`, cancelOption);
  }

  sendToBrower(id: number | null, proposeType: number | null, body?: ProposedModel[] | null | undefined): Observable<BaseResponse> {
    // gửi duyệt
    return this.http.post(`${this.baseUrl}/${id}/send-approve?proposeType=${proposeType}`, body);
  }

  accept(id: number | null, note: string, proposeType: number | null): Observable<BaseResponse> {
    return this.http.post(`${this.baseUrl}/${id}/agree-unit?proposeType=${proposeType}`, note);
  }

  reject(id: number, note: string) {
    return this.http.post(`${this.baseUrl}/${id}/reject-approve`, note);
  }

  agreeLeader(id: number, comment: string, proposeType: number) {
    return this.http.post(`${this.baseUrl}/${id}/agree-leader?comment=${comment}&proposeType=${proposeType}`, null);
  }

  rejectLeader(id: number, note: string) {
    return this.http.post(`${this.baseUrl}/${id}/reject-leader`, note);
  }

  otherIdea(id: number, note: string, proposeType: number) {
    return this.http.post(`${this.baseUrl}/${id}/other-idea?note=${note}&proposeType=${proposeType}`, null);
  }

  exportExcel(params: ProposedExcel): Observable<any> {
    return this.http.get(`${this.baseUrl}/export`, { params: clearParamSearch(params) });
  }

  printReportCard(id: string) {
    const params = {
      reportPath: 'HCM/LD_TTr_001',
      format: 'PDF',
      proposeId: id,
    };
    return this.http.get(`${this.baseUrl}/print`, {
      params,
      responseType: 'blob',
    });
  }

  //xuất tờ trình xử lý đề xuất
  exportReportDecision(id: number, params: any): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}/template-link`, { params: clearParamSearch(params) });
  }

  updateTemplateUsed(body: string[]): Observable<any> {
    return this.http.put(`${this.baseUrlTempalte}/update-template-used`, body);
  }

  callBaseUrl(text: string): Observable<any> {
    return this.http.get(`${this.urlLocal}${text}`, { responseType: 'blob' as 'json', observe: 'response' });
  }

  mergePropose(listPropose: Proposed[], screenCode: string, optionNotes: string): Observable<any> {
    return this.http.post(`${this.baseUrl}/group?screenCode=${screenCode}&optionNotes=${optionNotes}`, listPropose);
  }

  uploadFile(formUpload: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/upload-file`, formUpload);
  }

  deleteUploadFile(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}/delete-file`);
  }

  savePtnl(data: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/process/save`, data);
  }

  getConsultation(proposeDetailId: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${proposeDetailId}/consultations`);
  }

  saveConsultation(proposeDetailId: number, data: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/${proposeDetailId}/consultations`, data);
  }

  deleteConsultation(proposeDetailId: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${proposeDetailId}/delete-consultations`);
  }

  summarizeContent(proposeDetailId: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/ld-summary-comment?id=${proposeDetailId}`);
  }

  // API xử lý lần đầu cho đề xuất lẻ và GROUP
  proposalProcessing(data: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/processing-process`, data);
  }

  processingPTNL(data: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/send-approve-ptnl`, data);
  }

  // Xử lý
  listInterview(proposeDetailId: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/interview?proposeDetailId=${proposeDetailId}`);
  }

  listSalary(proposeDetailId: number): Observable<any> {
    return this.http.get(`${this.baseUrlSalry}/view-details?proposeId=${proposeDetailId}`);
  }

  listHandleOffer(proposeDetailId: number): Observable<any> {
    return this.http.get(`${this.baseUrlHandleOffer}/optionByProseDetailId?proposeDetailId=${proposeDetailId}`);
  }

  //lấy khối trục dọc
  getBusinessLine(orgID: number, jobID: number): Observable<any> {
    return this.http.get(`${this.lineUrl}orgId=${orgID}&jobId=${jobID}`);
  }

  getBusinessLineDetail(id: number): Observable<any> {
    return this.http.get(`${this.lineDetailUrl}${id}`);
  }

  //SơnLQ

  displayConsultation(idConsultation: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/display-consultation?id=${idConsultation}&page=0&size=${maxInt32}`);
  }

  listConsultation(idConsultation: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${idConsultation}/consultations`);
  }
  // Quyết định và thông báo

  viewDetailFile(proposeDetailId: number, proposeCategory: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/viewDetailFile?id=${proposeDetailId}&proposeCategory=${proposeCategory}`);
  }

  downloadFile(docId: number, fileName: string): Observable<boolean> {
    return this.http
      .get(`${environment.baseUrl}hcm_utilities/v1.0/files/download/${docId}`, {
        responseType: 'arraybuffer',
      })
      .pipe(
        map((val: any) => {
          saveAs(
            new Blob([val], {
              type: 'application/octet-stream',
            }),
            fileName
          );
          return true;
        })
      );
  }

  getPosByOrgId(orgId: number): Observable<any> {
    return this.http.get(environment.baseUrl + url.url_endpoint_getPositionsByOrgId + orgId);
  }

  getLine(posId: number): Observable<any> {
    return this.http.get(`${environment.baseUrl}hcm-model-plan/v1.0/line-by-position?posId=${posId}`);
  }

  saveItemHandleOffer(body: ListHandleOffer, checkOption: boolean | string): Observable<any> {
    return this.http.post(`${environment.baseUrl + url.url_endpoint_list_plan}?checkOption=${checkOption}`, body);
  }

  deleteItemHandleOffer(id: number): Observable<any> {
    return this.http.delete(`${environment.baseUrl + url.url_endpoint_list_plan}/${id}`);
  }

  toggleCheckItemHandleOffer(isCheck: boolean, id: number): Observable<any> {
    const state = isCheck ? 'checkOption' : 'unCheckOption';
    return this.http.put(`${environment.baseUrl + url.url_endpoint_list_plan}/${id}/${state}`, null);
  }

  checkConditionConsultation(code: string, id: number): Observable<any> {
    const queryParams = {
      formLd: code,
      id: id,
    };
    return this.http.get(`${environment.baseUrl + url.url_endpoint_list_plan}/check-condition-consultation`, {
      params: queryParams,
    });
  }

  checkConditionInterview(code: string, id: number): Observable<any> {
    const queryParams = {
      formLd: code,
      id: id,
    };
    return this.http.get(`${environment.baseUrl + url.url_endpoint_list_plan}/check-condition-interview`, {
      params: queryParams,
    });
  }

  sendInterview(body: {}): Observable<any> {
    return this.http.put(`${environment.baseUrl + url.url_endpoint_list_plan}/sendInterviews`, body);
  }

  sendSalary(body: number[]): Observable<any> {
    return this.http.post(`${environment.baseUrl + url.url_endpoint_salary_list}/sendSalary`, body);
  }

  checkResultInterview(body: number[]): Observable<any> {
    return this.http.post(`${this.baseUrl}/checkInterview`, body);
  }

  getInfoConsult(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/viewOption?proposeDetailId=${id}`);
  }

  //Thêm tham vấn đơn vị đến và các tham vấn khác type = 2
  addConsultation(typeConsultation: number, body: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/${typeConsultation}/consultations`, body);
  }

  deleteConsultationBefore(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}/delete-consultations`);
  }

  viewParent(proposeDetailId: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/viewParent?proposeDetailId=${proposeDetailId}`);
  }

  getInfoTTQD(idProposeDetail: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/display-text?idProposeDetail=${idProposeDetail}`);
  }

  getLogs(id: number, type: string, screenCode: string): Observable<any> {

    const queryParams = {
      proposeId: id,
      proposeType: Utils.getGroupType(type),
      screenCode: screenCode,
    };
    return this.http.get(`${environment.baseUrl}hcm-ld/v1.0/propose-log`, { params: clearParamSearch(queryParams) });
  }

  saveHandle(idProposeDetail: number, data: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/update-area-text-statement?idProposeDetail=${idProposeDetail}`, data);
  }

  uploadFileHandle(formUpload: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/uploadFiles`, formUpload);
  }

  removeFileNotify(data: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/deleteFile`, data);
  }

  saveStatementDecision(data: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/proposeStatementDecision`, data);
  }

  dowloadStatement(reportPath: string, proposeId: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/print?reportPath=${reportPath}&format=XLS&proposeId=${proposeId}`);
  }

  checkedSalary(salaryId: string, flag: boolean): Observable<any> {
    return this.http.get(`${this.baseUrlSalry}/checked?salaryId=${salaryId}&flag=${flag}`);
  }

  getDetailElement(proposeDetailId: number, screenCode: string, proposeGroupId?: number): Observable<any> {
    const params = {proposeDetailId, screenCode, proposeGroupId}
    return this.http
      .get(`${this.baseUrl}/${proposeDetailId}/viewDetail`, {params: clearParamSearch(params)})
      .pipe(map((val: any) => val.data));
  }

  getPositionGroup(): Observable<any> {
    return this.http.post(`${environment.baseUrl}hcm-model-plan/v1.0/position/group/title`, {
      name: '',
      page: 0,
      size: 1000,
      type: 'POSITION',
    });
  }

  removeTheLump(proposeId: number, proposeDetailId: number, screenCode: string): Observable<any> {
    return this.http.post(`${this.baseUrl}/${proposeId}/${proposeDetailId}?screenCode=${screenCode}`, {});
  }

  getPositionByUnit(paramPosition: any): Observable<any> {
    return this.http.post(
      `${environment.baseUrl}${Constants.SERVICE_NAME_POSITION.MODEL_POSITION}/v1.0/position/list/all`,
      paramPosition
    );
  }

  //Check delete PTNL
  checkDeletePTNL(id: number, groupProposeType: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}/check-delete?groupProposeType=${groupProposeType}`);
  }

  getOrganizationTreeById(params: any): Observable<any> {
    return this.http.get(environment.baseUrl + url.url_enpoint_organization_tree_get_by_id, { params: params });
  }

  public getByParent(parentId: number, params: any): Observable<any> {
    const url = `${environment.baseUrl}hcm-person-info/v1.0/org-tree/get-by-parent/${parentId}`;
    return this.http.get(url, { params: params });
  }

  getRolesByUsername(): Observable<any>{
    return this.http.get(`${this.urlLocal}hcm-ld/v1.0/users/role-function-permission`).pipe(map((val: any)=> val?.data?.roleFunctionPermissions?.map((item: any) => item.roleCode)))
  }

  changeStatusWithdraw(proposeDetail: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/change-status-withdraw?proposeDetailId=${proposeDetail}`);
  }

  withdrawPropose(body: WithdrawPropose): Observable<any>{
    return this.http.post(`${this.baseUrl}/withdraw`, body);
  }

  uploadFileWithdraw(body: FormData): Observable<any>{
    return this.http.post(`${this.baseUrl}/upload-file-withdraw`, body);
  }

  approveWithdrawPropose(id: number, proposeType: number, comment: string, ideaType: number, screenCode: string): Observable<any>{
    return this.http.put(`${this.baseUrlApprove}/approve?id=${id}&proposeType=${proposeType}&comment=${comment}&ideaType=${ideaType}&screenCode=${screenCode}`, null);
  }

  approveWithdrawProposeDetail(ids: number[], comment: string, ideaType: number, screenCode: string): Observable<any>{
    return this.http.post(`${this.baseUrl}/approve-withdraw/batch?ids=${ids}&ideaType=${ideaType}&comment=${comment}&screenCode=${screenCode}`, null);
  }

  otherIdeaElementKSPTNL(proposeId: number, proposeDetailId: number, otherIdeas: string): Observable<any>{
    return this.http.put(`${this.baseUrl}/ksptnlOtherIdeas?proposeId=${proposeId}&proposeDetailId=${proposeDetailId}&otherIdeas=${otherIdeas}`, null)
  }

  cancelInterview(optionId: number, reason: string): Observable<any>{
    return this.http.put(`${this.baseUrlHandleOffer}/cancel-interview?optionId=${optionId}&reason=${reason}`, null);
  }

  checkOptionPlan(optionId: number, confirm: boolean | string): Observable<any> {
    return this.http.put(`${this.baseUrlHandleOffer}/${optionId}/checkOption?confirm=${confirm}`, null);
  }

  getListReturnPropose(proposeDetailId: number): Observable<any>{
    return this.http.get(`${this.baseUrl}/list-details-permission?proposeDetailId=${proposeDetailId}`);
  }

  returnPropose(body: ReturnPropose): Observable<any>{
    return this.http.post(`${this.baseUrl}/return-propose`, body);
  }

  getIssueLevelOption(proposeDetailId: number): Observable<any>{
    return this.http.get(`${this.baseUrlHandleOffer}/issueLevel-option?proposeDetailId=${proposeDetailId}`);
  }

  getApproverSignerOption(issueLevelCode : string): Observable<any>{
    return this.http.get(`${this.baseUrlHandleOffer}/approver-signer-option?issueLevelCode=${issueLevelCode}`);
  }

  getApproverSignerOptionPlan(optionId: number): Observable<any>{
    return this.http.get(`${this.baseUrlHandleOffer}/default-config?optionId=${optionId}`);
  }

  getValueRecommendIndividual(): Observable<BaseResponse>{
    return this.http.get(`${this.baseUrl}/value-display-personal`);
  }

  saveRecommendIndividual(data: RecommendIndividualModel[], screenCode: string): Observable<BaseResponse> {
    return this.http.post(`${this.baseUrl}/create-personal?screenCode=${screenCode}`, data);
  }
  sendToBrowerRecommend(id: number | null , proposeType: number | null): Observable<BaseResponse> {
    // gửi duyệt màn cá nhân đề xuất
    return this.http.post(`${this.baseUrl}/${id}/send-approve`, {proposeType: proposeType});
  }

  uploadFileTGCV(formData: FormData): Observable<any> {
    return this.http.post(`${this.baseUrl}/proposeUpload`, formData);
  }

  updateTGCV(data: ProposedModel, params: ScreenCode): Observable<any> {
    return this.http.put(`${this.baseUrl}/update`, data, { params: clearParamSearch(params) });
  }

  // Chi tiết màn đề xuất 1 cá nhân
  getDetailRecommendIndividual(ids : number, screenCode: string): Observable<BaseResponse>{
    return this.http.get(`${this.baseUrl}?ids=${ids}&screenCode=${screenCode}`);
  }

  // chi tiết 1 cá nhân detail
  getDetailElementRecommendIndividual(proposeDetailId: number, screenCode: string, proposeGroupId?: number): Observable<BaseResponse> {
    const params = {proposeDetailId, screenCode, proposeGroupId}
    return this.http
      .get(`${this.baseUrl}/${proposeDetailId}/viewDetail`, {params: clearParamSearch(params)});
  }

  // thông tim kiêm nhiệm màn xử lý
  //kiêm nhiệm hiện tại
  getInfoConcurrently(proposeDetailId: number): Observable<BaseResponse>{
    return this.http.get(`${this.baseUrlHandleOffer}/concurrent-detail?proposeDetailId=${proposeDetailId}`);
  }

  // vị trí hiện hữu
  getInfoConcurrentlyNow(proposeDetailId: number): Observable<BaseResponse>{
    return this.http.get(`${this.baseUrlHandleOffer}/concurrent-option-selected?proposeDetailId=${proposeDetailId}`);
  }

  //dropdown phương án bố trí
  dropdownConcurrently(): Observable<BaseResponse>{
    return this.http.get(`${this.baseUrlHandleOffer}/alternative-dropdown`);
  }

  //Lưu thông tin kiêm nhiệm hiện tại
  saveConcurrent(body: InforConcurrentPositionModel , proposeDetailId: number): Observable<BaseResponse> {
    return this.http.post(`${this.baseUrlHandleOffer}/save-concurrent-employee?proposeDetailId=${proposeDetailId}`, body);
  }

  // Lưu thông tin hiện hữu
  saveConcurrentPosition( alternativeId: number, alternativePlan: string): Observable<BaseResponse> {
    return this.http.post(`${this.baseUrlHandleOffer}/save-concurrent-position?alternativeId=${alternativeId}&alternativePlan=${alternativePlan}`, null);
  }

  //check show modal lúc xuất tờ trình màn nhiều
  checkShowModalPrintTemplate(proposeId: number): Observable<BaseResponse>{
    return this.http.get(`${this.baseUrl}/is-show-popup?proposeId=${proposeId}`);
  }

  //API gửi duyệt mới
  sendProposeNew(id: number | null, body: ProposedModel[], params: ProposeTypeModel): Observable<BaseResponse> {
    return this.http.post(`${this.baseUrl}/${id}/send-approve`, body, { params: clearParamSearch(params) });
  }

  //ghi nhận đồng ý mới
  agreeUnitNew(id: number | null, body: ProposedModel[], params: ProposeTypeModel): Observable<BaseResponse> {
    return this.http.post(`${this.baseUrl}/${id}/agree-unit?`, body, { params: clearParamSearch(params) });
  }

  //ghi nhận đồng ý cho màn nhiều
  agreeUnitDCDD(id: number | null, note: string, proposeType: number | null, body: ProposedModel[] | null | undefined): Observable<BaseResponse> {
    return this.http.post(`${this.baseUrl}/${id}/agree-unit?proposeType=${proposeType}&note=${note}`, body);
  }

  //get log phân trang
  getLogsPage(id: number, type: string, screenCode: string, pageNumber: number, pageSize: number): Observable<BaseResponse> {
    const queryParams = {
      proposeId: id,
      proposeType: Utils.getGroupType(type),
      screenCode: screenCode,
      page: pageNumber,
      size: pageSize
    };
    return this.http.get(`${environment.baseUrl}hcm-ld/v1.0/propose-log`, { params: clearParamSearch(queryParams) });
  }
}
