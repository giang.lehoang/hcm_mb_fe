export interface ListIssueTransferTable {
  proposeDetailId?: number;
  proposeCode?: string;
  proposeCategory?: string;
  employeeCode?: string;
  employeeName?: string;
  formRotationCode?: string;
  formRotationName?: string;
  employeeUnitId?: number;
  employeeUnitName: string;
  employeeUnitPathId?: string;
  employeeUnitPathName?: string;
  employeeTitleId?: number;
  employeeTitleName?: string;
  contentUnitId?: number;
  contentUnitName?: string;
  contentTitleName?: string;
  contentTitleId?: number;
  contentUnitPathId?: string;
  contentUnitPathName: string;
  issueLevelCode?: string;
  issueLevelName?: string;
  signerFullName?: string;
  approvalFullName?: string;
  effectiveDate?: Date | string;
  sameDecision?: boolean;
  minimumTime?: number;
  effectiveEndDate1?: Date | string;
  maximumTime?: number;
  effectiveEndDate2?: Date | string;
  impactWorkProcess?: string;
  withdrawNoteCode?: number;
  withdrawNote?: string;
  flowStatusCode?: string;
  formGroup?: string | number;
  personalCode?: string;
}

export interface IssueTransferPopup {
  proposeDetailIds?: number[];
  commentPublish?: string;
}

export interface ApplyProposePublish {
  proposeDetailIds?: number[];
  effectiveDate?: Date | string,
  minimumTimePublish?: number,
  issueEffectiveEndDate1: Date | string,
  maximumTimePublish?: number,
  timeType?: number,
  issueEffectiveEndDate2?: Date | string,
  checkWorkProcess?: boolean
}