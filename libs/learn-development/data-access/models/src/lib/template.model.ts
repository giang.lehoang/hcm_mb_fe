export interface Template {
  id?: number;
  code?: string;
  templateType?: string;
  subTemplateType?: string;
  proposeType?: string;
  forms?: string;
  issueLevel?: string;
  signer?: string;
  multipleLevels?: boolean;
  effectiveStartDate?: Date | string | null;
  effectiveEndDate?: Date | string | null;
  fileDTO?: FileDTO;
  attachedFiles?: number;
  fileName?: string;
  formName?: string;
  issuedLevelsName?: string;
  signerCode?: string | number;
}

export interface FileDTO {
  docId?: number;
  fileName?: string;
  mineType?: string;
  transCode?: string;
}


