export interface SearchInterview {
  startDate?: string;
  endDate?: string;
  currentUnit?: string;
  currentTitle?: string;
  proposeUnit?: number;
  proposeTitle?: string;
  employeeName?: string;
  employeeCode?: string;
}

export interface DataInterview {
  proposeId?: string;
  proposeDetailId?: number;
  sentDate?: string;
  status?: string;
  sentResultDate?: string;
  result?: string | number;
  proposeTypeName?: string;
  employeeCode?: string;
  employeeName?: string;
  joinDate?: string;
  currentUnitName?: string; // Đơn vị hiện tại
  currentTitleName?: string; // Tên chức danh hiện tại
  currentPositionName?: string; // Tên vị trí hiện tại
  proposeUnitName?: string; // Đơn vị đề xuất
  proposeTitleName?: string; // Tên chức danh đề xuất
  proposePositionName?: string; // Tên vị trí đề xuất
  interviewDate?: string;
  interviewBoard?: string;
  interviewResults?: string;
  interviewNote?: string;
  unitRequestName?: string;
  pointInterview?: number;
  interviewId: number;
  optionId?: number;
  birthDay?: Date | string,
  contentLevel?: string,
  businessFigures?: string,
  contentPositionName?: string,
  contentTitleName?: string,
  contentUnit?: string,
  employeeLevel?: string,
  employeeTitleName?: string,
  employeeUnit?: string,
  employeeUnitCode?: string,
  formGroup?: string,
  interviewResultDescription?: string,
  interviewSentDate?: Date | string,
  interviewStatus?: number,
  interviewStatusDescription?: string | null,
  ndx?: string | null,
  positionSeniorityMB?: string | number | null,
  proposeOptionCode?: string | null,
  strengths?: string,
  unitOpinion?: string,
  weaknesses?: string,
  boardCode?: string | null,
  recruitment?: string,
  setup?: string,
  boardMembers?: string,
  interviewPoint?: number | string,
  suitablePosition?: string,
  note?: string,
  processFile?: ProcessFile,
  idMO?: string,
  contentPosition?: number,
  refuseReason?: string,


}

export interface ParamUpdateInterview {
  proposeDetailId?: number;
  interviewBoard?: String;
  interviewResults?: number;
  interviewNote?: String;
  interviewDate?: string;
}

export interface ParamSearchInterview {
  code: string | null;
  fromDate?: string | Date | null;
  toDate?: string | Date | null;
  rangeDate?: Date[] | string | null;
  employeeCode?: string | null;
  employeeName?: string | null;
  currentUnit?: any;
  currentTitle?: string | null;
  proposeUnit?: any;
  proposeTitle?: string | null;
  interviewStatus?: string | null;
  page: number;
  size: number;
  proposeCode?: string | null,
  isExportInterview?: boolean,
  boardCode?: string | null,
  isKSTD?: boolean,
}

export interface ProcessFile {
  createdBy?:string,
  createdDate?: Date | string,
  docId?: string | number,
  fileName?: string,
  id?: number,
  parentId?: number,
  type?: string,
  updatedBy?: string,
  updatedDate?: Date | string,
}


export interface TitleEmpInterview
{
  categoryCode?: string,
  categoryId?: number,
  code: string,
  id?: number,
  name?: string,
}

export interface ParamErrorSuccess
{
  importKey: string,
  dataType: number,
  page: number,
  size: number,
}

export interface TableErrorImport {
  colName: string;
  description: string;
  errorCode: string;
  errorData: string;
  errorType: string;
  rowNumber: number;
  importFailedData?: TableErrorImport;
  importSuccessData?: TableSuccessImport;
}


export interface TableSuccessImport {
  optionCode: string,
  employeeCode: string,
  employeeName: string,
}




