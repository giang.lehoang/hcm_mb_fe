import { EmployeeTraining, OrientationJob, ProposeAlternativeRenewal, UnitFeedbackRenewal } from "./proposed-appointment-renewal.model";
import { FileDTOList, FormTTQD, ProposeLogs, ProposeStatementDTO } from "./proposed-unit.model";

export interface UploadFileTGCV {
  proposeDetailId: number;
  personalReview: boolean;
  different: boolean;
  targetAppoint: boolean;
  reportBranchMeeting: boolean;
  consultationComment: boolean;
  statement: boolean;
}

export interface ProposeDTOTGCV {
  approvalCode: string;
  approveNote?: string;
  createdBy: string;
  externalApprove: boolean;
  flowStatusCode: string;
  flowStatusCodeDisplay: string;
  formGroup: string | number;
  flowStatusPublishCode: string;
  groupByKSPTNL: boolean;
  groupProposeTypeId: number;
  hasDecision: boolean;
  id: number;
  isGroup: boolean;
  isHo: boolean;
  issueLevelCode: string;
  proposeDetails: DataFormTGCV[];
  proposeLogs: ProposeLogs[];
  proposeUnitRequest: number;
  proposeUnitRequestName: string;
  proposeStatement?: ProposeStatementDTO;
  relationType: boolean;
  sentDate: Date | string | null;
  signerCode: string;
  withdrawNote?: string | number;
  withdrawType?: number | string;
  withdrawReason?: string;
  optionNote?: string;
  handleCode?: string;
  proposeStatementList?: FormTTQD[];
}
export interface DataFormTGCV {
  conditionViolationCode: number;
  createdBy: string;
  formRotation: number | string;
  formRotationType: string;
  formRotationCode: number;
  formRotationTitle: string | number;
  contentUnitCode: string | number;
  contentTitleCode: number;
  contentTitleName: string;
  contentUnitPathId: string;
  contentUnitPathName: string;
  contentUnitTreeLevel: number;
  employee: EmployeeFormTGCV;
  employeeUnitPathId: string;
  employeeUnitPathName: string;
  implementDate: Date | string | null;
  groupByKSPTNL: boolean;
  isGroup: boolean;
  id: number;
  contentUnitName: string;
  contentLevel: string | number;
  sortOrder: number;
  proposePerformanceResults: ProposePerformanceResultsTGCV[];
  proposeComment: ProposeCommentTGCV | ProposeCommentAppoint;
  proposeCode: string;
  proposeAttachmentFiles: ProposeAttachmentFilesTGCV;
  flowStatusCode: string;
  externalApprove: boolean;
  totalDensity: number;
  totalPoint: number;
  flowStatusPublishCode?: string;
  orientationJob?: OrientationJob[];
  employeeTraining?: EmployeeTraining[];
  proposeAlternative?: ProposeAlternativeRenewal[];
  unitFeedback?: UnitFeedbackRenewal[];
  concurrentReason?: string;
  // implementDate: Date | string,
  challengeTime: number;
  minimumTime: number;
  maximumTime: number;
  timeType: number;
  withdrawNote?: number;
  withdrawReason?: string;
  conditionViolations?: string[];
  isConsultation?: boolean;
  issuedLater?: boolean;
  sameDecision?: boolean;
  isIncome?: boolean;
  isInsurance?: boolean;
  isDecision?: boolean;
  proposeStatement?: ProposeStatementDTO[];
  fileDTOS?: FileDTOList[];
  fileDTOList?: FileDTOList[];
  hasAgreeUnit?: boolean;
  hasProcess?: boolean;
}

export interface EmployeeFormTGCV {
  orgId: number;
  empId: number;
  empCode: string;
  fullName: string;
  jobId: number;
  jobName: string;
  orgPathId: string;
  orgPathName: string;
  orgName: string;
  positionLevel: string;
  gender: string;
  joinCompanyDate: Date | string | null;
  dateOfBirth: Date | string | null;
  posSeniority: number;
  seniority: number;
  posSeniorityName: string;
  seniorityName: string;
  levelName: string;
  concurrentTitleId: number;
  concurrentTitleName: string;
  concurrentUnitPathName: string;
  concurrentTitleId2: number;
  concurrentTitleName2: string;
  concurrentUnitPathName2: string;
  partyDate?: boolean;
  periodNameT2?: string;
  periodNameT1?: string;
}

export interface ProposePerformanceResultsTGCV {
  targetAssigned: string;
  density: number;
  planIndicator: number;
  planDeadline: Date | string | null;
  realityIndicator: number;
  realityDeadline: Date | string | null;
  point: number;
}

export interface ProposeCommentTGCV {
  proposeReason: string;
  generalComment: string;
}

export interface ProposeAttachmentFilesTGCV {
  personalReview: boolean;
  different: boolean;
  targetAppoint: boolean;
  reportBranchMeeting: boolean;
  consultationComment: boolean;
  statement: boolean;
  docId: string;
  fileName: string;
  id: number;
  proposeDetailId: number;
}

export interface AttachFile {
  uid?: number | string,
  fileName?: string;
}

export interface ProposeCommentAppoint {
  id?: number;
  proposeDetailId?: number;
  strengths?: string;
  strengthsPtnl?: string;
  weaknesses?: string;
  weaknessesPtnl?: string;
}

