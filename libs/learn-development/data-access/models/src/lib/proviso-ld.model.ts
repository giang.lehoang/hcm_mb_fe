export interface ProvisoModel {
  id?: number | number;
  formLD?: string | null;
  conditionType?: string;
  effectiveFromDate?: string | Date | null;
  effectiveToDate?: string | Date | null;
  semesterGradeT2?: number;
  semesterRankT2?: string;
  semesterGradeT1?: number;
  semesterRankT1?: string;
  seniority?: number;
  positionSeniority?: number;
  educationDegree?: string;
  major?: string;
  endOfDisciplinaryDurationDismiss?: number;
  endOfDeciplines?: number;
  titleLevel?: string;
  ratedCapacity?: number;
  interviewResult?: string;
  page?: number;
  size?: number;
}
export interface DataCommonProvisoLd {
  conditionType?: ConditionType[];
  educationDegree?: EducationDegree[];
  formDTOs?: FormDTO[];
  interviewResult?: InterviewResult[];
  levelPosition?: LevelPosition[];
  major?: major[];
  rank?: rank[];
}

export interface ConditionType {
  code?: string;
  name?: string;
}

export interface EducationDegree {
  code?: string;
  name?: string;
}
export interface FormDTO {
  formCode?: string;
  formName?: string;
  sortOrder?: number;
  newRecruitment?: boolean;
  resign?: boolean;
  newExternalRecruitment?: boolean;
  resignExternal?: boolean;
  status?: boolean;
  code?: string;
}
export interface InterviewResult {
  code?: string;
  name?: string;
}
export interface LevelPosition {
  code?: string;
  name?: string;
}
export interface major {
  code?: string;
  name?: string;
}
export interface rank {
  code?: string;
  name?: string;
}
export interface ParamsSearch {
  effectiveStartDate?: string | null;
  effectiveEndDate?: string | null;
  formLD: string;
  page?: number;
  size?: number;
}
