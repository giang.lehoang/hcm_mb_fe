export interface StateApprovement {
  buttonListScreen?: string;
  list?: string[];
  buttonAction?: TypeButtonActionIssue[];
  screenAction?: string;
}

export interface TypeButtonActionIssue {
  name?: string;
  type?: 'primary'|'dashed'|'link'|'text'|'default';
  action?: boolean;
  typeState?: string;
}

export interface DropdownReasonCancel {
  state?: boolean;
  value?: string;
}

export interface PanelsCollapse {
  active?: boolean;
  disabled?: boolean;
}

export interface SearchApprovementIssue {
  proposeType?: string;
  releaseFromDate?: Date | string | null;
  releaseToDate?: Date | string | null;
  effectiveStartDate?: Date | string | null;
  effectiveEndDate?: Date | string | null;
  currentUnit?: string;
  currentTitle?: string;
  proposeUnit?: string;
  proposeTitle?: string;
  employeeName?: string;
  employeeCode?: string;
  statusPublish?: string | null;
  proposeOrigin?: boolean;
  proposeItem?: boolean;
  proposeGroup?: boolean;
  issueLevel?: string;
  signerCode?: string;
  groupProposeType?: number;
  flag?: boolean;
  screenCode?: string;
  pageable?: number;
  proposeCode?: number | string;
  formCode?: number | string;
  timeApprove?: string;
  effectiveDate?: string;
  signerFullName?: string;
  page?: number;
  size?: number;
  rangeApproveDate?: string;
  rangeEffectiveDate?: string;
  decisionNumber?: string | null;
  publishContent?: string;
  issueContent?: string | null;
  issueCode?: string | null;
  publishCode?: string | null;
}

export interface DropdownReasonCancel {
  state?: boolean;
  value?: string;
}

export interface PanelsCollapse {
  active?: boolean;
  disabled?: boolean;
}

export interface DataReason {
  viewDetailReasonOrCorrection?: string;
  reasonOrCorrection?: boolean;
  cancellationReason?: string;
  commentsIssueTransfer: string;
}



