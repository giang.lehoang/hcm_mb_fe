import {
  FormDisplaySearchDTOS,
  IssueLevelList,
  StatusList,
  TypeDTOS,
} from './promulgate.model';

import { Category } from '@hcm-mfe/shared/data-access/models';
import {CategoryModel} from "./approvement-config-ld.model";
import { EmployeeTrainingRenewal, UnitFeedback, UnitFeedbackRenewal } from './proposed-appointment-renewal.model';
export interface Proposed {
  proposeId?: number;
  proposeDetailId?: number;
  proposeCategory?: string;
  proposeUnitRequest?: string;
  proposeTypeCode?: number | string;
  proposeTypeName?: string;
  relationType?: string | number;
  groupProposeTypeId?: number;
  createdBy?: string;
  sentDate?: Date | string | null;
  flowStatusCode?: string;
  publishStatusCode?: string;
  publishStatusName?: string;
  employeeCode?: string;
  employeeName?: string;
  joinDate?: Date | string | null;
  currentUnitCode?: string;
  currentUnitName?: string;
  currentTitleId?: string;
  currentTitleName?: string;
  currentPosition?: string;
  proposeUnit?: string;
  proposeTitle?: string;
  proposePosition?: string;
  statusDisplay?: string;
  statusPublishDisplay?: string;
  isGroup?: boolean;
  issueLevelCode?: string;
  issueLevelName?: string;
  signerCode?: string;
  signerFullName?: string;
  signerTitleId?: string;
  signDate?: Date | string | null;
  approvalCode?: string;
  approvalFullName?: string;
  approvalTitleId?: string;
  approvalDate?: Date | string | null;
  indexFix?: number;
  id?: number;
  proposeUnitRequestName?: string;
  groupByKSPTNL?: boolean;
  formRotationName?: string;
  employeeUnitPathName?: string;
  employeeTitleName?: string;
  employeePositionName?: string;
  contentUnitPathName?: string;
  contentTitleName?: string;
  conditionViolation?: string;
  conditionViolationCode?: number;
  salaryStatus?: string;
  interviewStatus?: string;
  consultationStatus?: string;
  contentPositionName?: string;
  groupProposeType?: string;
  lastUpdatedBy?: string;
  followStatusCode?: string;
  proposeCode?: number;
  isEdit?: boolean;
  formGroupName?: string;
  withdrawNote?: string;
  withdrawNoteCode?: string;
  lastUpdateDate?: Date | string | null;
  isEndDateWithdraw?: boolean;
  formGroup?: number | string;
  personalCode?: number | string;
}
export interface ProposedExcel {
  proposeType?: string;
  proposeStartDate?: string;
  proposeEndDate?: string;
  currentUnit?: string;
  currentTitle?: string;
  proposeUnit?: string;
  proposeTitle?: string;
  employeeCode?: string;
  employeeName?: string;
  approve?: string;
  statusPublish?: string;
  proposeOrigin?: boolean;
  proposeItem?: boolean;
}

export interface ProposeSearchTable {
  proposeID?: number;
  proposeType?: string;
  sentDate?: string;
  joinDate?: string;
  currentUnit?: string;
  currentTitle?: string;
  currentPosition?: string;
  proposeUnit?: string;
  proposeTitle?: string;
  proposePosition?: string;
  employeeCode?: string;
  employeeName?: string;
  status?: string;
  statusPublish?: string;
  proposeOriginal?: boolean;
  proposeItem?: boolean;
}

export interface ProposedModel {
  proposeUnitRequest: number;
  proposeUnitRequestName: string;
  proposeType: string;
  relationType: string;
  proposeDetails: [
    {
      rotationFromDate: Date | string | null;
      rotationToDate: Date | string | null;
      rotationTime: number;
      employee: {
        orgId: number;
        empId: number;
        empCode: string;
        fullName: string;
        email: string;
        posId: number;
        posName: string;
        jobId: number;
        jobName: string;
        orgPathId: string;
        orgPathName: string;
        orgName: string;
        joinCompanyDate: Date | string | null;
        dateOfBirth: Date | string | null;
        posSeniority: number;
        seniority: number;
        totalPoint: number;
        totalDensity: number;
        treeLevel: number;
      };
      formRotation: string;
      formRotationType: string;
      formRotationCode: string;
      formRotationTitle: string;
      contentTitleCode: number;
      contentTitleName: string;
      contentUnitCode: string | number;
      contentUnitName: string;
      contentUnitPathId: string;
      contentUnitPathName: string;
      contentUnitTreeLevel: number;
      proposeRotationReasons: [
        {
          id: number;
          rotationReasonID: number;
          name: string;
          description: string;
          disabled: true;
        }
      ];
      proposeAlternative: {
        label: string;
        alternativeType: string;
        estimatedDate: Date | string | null;
        replacementEmployeeName: string;
        replacementEmployeeTitleCode: number;
        handoverEmployeeName: string;
        handoverEmployeeTitleCode: number;
        disabled: false;
      };
      proposeComment: {
        strengths: string;
        weaknesses: string;
      };
      proposePerformanceResults: [
        {
          targetAssigned: string;
          density: number;
          planIndicator: number;
          planDeadline: Date | string | null;
          realityIndicator: number;
          realityDeadline: Date | string | null;
          disabled: true;
          point: number;
        }
      ];
    }
  ];
}

export interface ProposeEmployeeCreateDTO {
  empIdp?: string;
  orgId?: number;
  empId?: number;
  empCode?: string;
  fullName: string;
  email: string;
  posId: number;
  posName: string;
  jobId: number;
  jobName: string;
  orgPathId: number;
  orgPathName: string;
  orgName: string;
  majorLevelId: number;
  majorLevelName: string;
  faculityId: number;
  faculityName: string;
  schoolId: number;
  schoolName: string;
  positionLevel: string;
  gender: string;
  joinCompanyDate: Date | string | null;
  dateOfBirth: Date | string | null;
  partyDate: Date | string | null;
  partyOfficialDate: Date | string | null;
  posSeniority: string;
  seniority: string;
  userToken?: null; // Thông tin userToken - Sử dụng cho currentUser
}
export interface ProposePerformanceResults {
  id?: number | null;
  proposeDetailId?: number | null;
  targetAssigned?: string | null;
  density?: number | null;
  planIndicator?: number | null;
  planDeadline?: Date | string | null;
  realityIndicator?: number | null;
  realityDeadline?: Date | string | null;
  disabled?: boolean;
  point?: string | number | null;
}

export interface Consultations {
  dateIdea?: string;
  personIdea?: string;
  representativeUnit?: string;
  idea?: string;
  ideaDetail?: string;
  displayOnReport?: boolean;
}

export interface ResourceManagement {
  consultancyUnit?: string;
  consultancyTCNS?: string;
  submissionNumber?: string;
  submissionDate?: string;
  proposeOriginalComponent?: boolean;
  proposeMergeComponent?: boolean;
}

export interface ProposeList {
  approvalCode: string;
  approvalFullName: string;
  approvalTitleId: number;
  createdBy: string;
  currentPosition: string;
  currentTitleId: number;
  currentTitleName: string;
  currentUnitCode: number;
  currentUnitName: string;
  employeeCode: string;
  employeeName: string;
  employeeUnitPathId: string;
  employeeUnitPathName: string;
  flowStatusCode: string;
  groupProposeType: string;
  isGroup: boolean;
  issueLevelCode: string;
  issueLevelName: string;
  joinDate: Date | string | null;
  proposeCategory: string;
  proposeDetailId: number;
  proposeId: number;
  proposeTitle: string;
  proposeTypeCode: string;
  proposeTypeName: string;
  proposeUnit: string;
  proposeUnitRequest: number;
  relationType: string;
  sentDate: Date | string | null;
  signerCode: string;
  signerFullName: string;
  signerTitleId: number;
  statusDisplay: string;
  statusPublishDisplay: string;
  groupByKSPTNL: boolean;
  proposeCode?: number | string;
}

export interface ProposedCreate {
  approvalCode: string;
  externalApprove: boolean;
  flowStatusCode: string;
  id: number;
  issueLevelCode: string;
  proposeDetails: [
    {
      rotationFromDate: Date | string | null;
      rotationToDate: Date | string | null;
      rotationTime: number;
      employee: {
        orgId: number;
        empId: number;
        empCode: string;
        fullName: string;
        email: string;
        posId: number;
        posName: string;
        jobId: number;
        jobName: string;
        orgPathId: string;
        orgPathName: string;
        orgName: string;
        joinCompanyDate: Date | string | null;
        dateOfBirth: Date | string | null;
        posSeniority: number;
        seniority: number;
        totalPoint: number;
        totalDensity: number;
        treeLevel: number;
      };
      formRotation: string;
      formRotationType: string;
      formRotationCode: string;
      formRotationTitle: string;
      contentTitleCode: number;
      contentTitleName: string;
      contentUnitCode: string | number;
      contentUnitName: string;
      contentUnitPathId: string;
      contentUnitPathName: string;
      contentUnitTreeLevel: number;
      proposeRotationReasons: [
        {
          id: number;
          rotationReasonID: number;
          name: string;
          description: string;
          disabled: true;
        }
      ];
      proposeAlternative: {
        label: string;
        alternativeType: string;
        estimatedDate: Date | string | null;
        replacementEmployeeName: string;
        replacementEmployeeTitleCode: number;
        handoverEmployeeName: string;
        handoverEmployeeTitleCode: number;
        disabled: false;
      };
      proposeComment: {
        strengths: string;
        weaknesses: string;
      };
      proposePerformanceResults: [
        {
          targetAssigned: string;
          density: number;
          planIndicator: number;
          planDeadline: Date | string | null;
          realityIndicator: number;
          realityDeadline: Date | string | null;
          disabled: true;
          point: number;
        }
      ];
    }
  ];
  proposeLogs: [];
  proposeType: string;
  relationType: boolean;
  sentDate: string | Date;
  signerCode: string;
  status: string;
}

export interface DataMessageModel {
  titleEmployee: string;
  listException: string[];
}

export interface DataPropose {
  contentUnitCode?: string;
  employee?: Employee;
  employeeCode?: string;
  formRotation?: string | number;
  formRotationTitle?: number;
  formRotationType?: string;
  proposeAlternative?: ProposeAlternative;
  proposeComment?: ProposeComment[];
  proposeID?: number;
  proposePerformanceResults?: ProposePerformanceResults[];
  proposeRotationReasons?: ProposeRotationReasons[];
  rotationFromDate?: string;
  rotationTime?: number;
  rotationToDate?: string;
  contentTitleCode?: string;

  employeeUnitPathId?: string;
  employeeUnitPathName?: string;
  contentUnitPathId?: string;
  contentUnitPathName?: string;
}

export interface Employee {
  birthday?: string;
  college?: string;
  currentUnit?: {
    orgId?: number;
    orgName?: string;
    pathId?: string;
    pathName?: string;
  };
  currentUnitName?: string;
  education?: string;
  employeeCode?: string;
  endOfDisciplinaryDuration?: string;
  fullName?: string;
  joinDate?: string;
  major?: string;
  nearestDiscipline?: string;
  positionSeniority?: string;
  qualificationEnglish?: string;
  ratedCapacity?: string;
  semesterGradeT1?: string;
  semesterGradeT2?: string;
  semesterRankT1?: string;
  semesterRankT2?: string;
  title?: string;
}

export interface ProposeComment {
  proposeDetailId?: number;
  strengths?: string;
  weaknesses?: string;
}

export interface ProposeRotationReasons {
  proposeDetailId?: number | null;
  rotationReasonID?: string | number | null;
  description?: string | null;
  id?: number | null;
  name?: string | null;
  disabled?: boolean;
  code?: string | null;
}

export interface ProposeAlternative {
  id?: number | null;
  label?: string | null;
  proposeDetailId?: number | null;
  alternativeType?: number | null;
  estimatedDate?: string | null;
  replacementEmployeeCode?: string | number | null;
  replacementEmployeeName?: string | null;
  replacementEmployeeTitleCode?: string | number | null;
  replacementEmployeeTitleName?: string | null;
  handoverEmployeeCode?: string | number | null;
  handoverEmployeeName?: string | null;
  handoverEmployeeTitleCode?: string | number | null;
  handoverEmployeeTitleName?: string | null;
  replacementEmployee?: string | null;
  disabled?: boolean;
  empCodeReplace?: string | null;
  empCodeHand?: string | null;
  handoverEmployee?: string | null;
}

export interface ProposeLogs {
  id: number;
  parentId: number;
  type: string;
  executedTime: string;
  action: string;
  comments: string;
  executorCode: string;
  proposeId: number;
  actionDate?: string;
  userName?: string;
  comment?: string;
  flow?: string;
}

export interface DataCommon {
  formGroupMultipleList?: FormGroupMultipleList[];
  actions?: String[];
  consultationStatus?: ConsultationStatus[];
  formDisplaySearchDTOS?: FormDisplaySearchDTOS[];
  interviewStatus?: InterviewStatus[];
  issueLevelList?: IssueLevelList[];
  jobDTOS?: JobDTOS[];
  proposeRotationReasonConfigDTOS?: ProposeRotationReasons[];
  flowStatusPublishConfigList?: StatusList[];
  flowStatusApproveConfigList?: StatusList[];
  typeDTOS?: TypeDTOS[];
  units?: Unit[];
  statusList?: Category[];
  levelTitle?: LevelTitle[];
  formGroupList?: FormGroupList[];
  conditionViolationList?: ConditionViolationList[];
  publishStatus?: CategoryModel[];
  employeeTrainingList?: EmployeeTrainingRenewal[];
  unitFeedbackList?: UnitFeedbackRenewal[];
  unitFeedbackConfigList?: UnitFeedbackRenewal[];
  alternativePlanConcurrentList?: EmployeeTrainingRenewal[];
  alternativePlanSpecializeList?: EmployeeTrainingRenewal[];
  timeTypeEnums?: TimeTypeEnums[];
}

export interface ConsultationStatus {
  code: string;
  id: number;
  name: string;
  statusType: number;
}

export interface InterviewStatus {
  code: string;
  id: number;
  name: string;
  statusType: number;
}

export interface JobDTOS {
  jobCode?: string;
  jobDescription?: string;
  jobId?: number;
  jobName?: string;
  posId?: number;
}

export interface Unit {
  pathName?: string;
  orgId?: number;
  orgName?: string;
  pathId?: string;
  treeLevel?: string;
}
export interface ViewDetailParent {
  parentId: number; //Gốc
  groupId: number; // Gộp
}

export interface ProposeStatementDTO {
  groupProposeType: string;
  id: number;
  processFileId: number;
  proposeId: number;
  statementNumber?: string;
  submissionDate: string | Date;
  approveNote?: string;
  typeStatement?: number;
}

export interface ProposeProcessFileDTOItem {
  docId: string;
  fileName: string;
  id: number;
  mimeType: string;
  parentId: number;
  transCode: string;
  type: string;
}

export interface InfoTTQD {
  proposeStatementDTO: ProposeStatementDTO;
  proposeProcessFileDTOlist: ProposeProcessFileDTOItem[];
}

export interface ScreenCode {
  screenCode: string;
}

export interface Detail {
  ids: number;
  screenCode: string;
}

export interface EmployeeModal {
  dateOfBirth?: string | Date;
  email?: string;
  empCode?: string;
  empId: number;
  fullName?: string;
  gender?: string;
  jobId?: number;
  jobName?: string;
  joinCompanyDate?: string | Date;
  mobileNumber?: string;
  orgId?: number;
  orgName?: string;
  orgPathId?: string;
  orgPathName?: string;
  posId?: number;
  posName?: string;
  posSeniority?: number;
  seniority?: number;
  employeeCode?: string;
  positionType?: string;
}

export interface ApproveKSPTNL {
  groupProposeType: number;
  action: number;
}

export interface NoteKSPTNL {
  message: string;
}
export interface SearchParamsEmp {
  keySearch?: string;
  orgId?: string | number;
  positionIds?: string | number;
  startRecord: number;
  pageSize: number;
}

export interface ProposeCategoryConsultation {
  id: number;
  name: string;
}

export interface IssueCorrctionSelect {
  id: boolean;
  name: string;
}

export interface CancelPlan {
  id: number;
  cancellationReason: string;
}

export interface IssueTransfer {
  id: string | number;
  proposeCategory: string;
  screenCode: string;
}

export interface ProposeRotationReasonsNCN {
  rotationReasonID?: string;
  description?: string;
  disabled?: boolean;
  name?: string;
  code?: string;
}

export interface FormTTQD {
  id?: number;
  statementNumber?: string;
  submissionDate?: Date | string;
  decisionNumber?: string | null;
  signedDate?: Date | null;
  typeStatement?: number;
}

export interface ParamSearchPosition {
  orgId: string;
  jobId: string;
  status: number;
  type: string;
  page: number;
  size: number;
}

export interface ListOrgItem {
  name?: string;
  value: number;
  valueItem: OrgItem;
}

export interface OrgItem {
  orgId?: number;
  orgName?: string;
  pathId?: string;
  pathResult?: string;
  orgLevel?: number;
}

export interface RotationReasons {
  id?: number | null;
  rotationReasonID?: number | null;
  proposeDetailId?: number | null;
  name?: string | null;
  code?: string | null;
  description?: string | null;
  checked?: boolean;
  disabled?: boolean;
  formRotationLabel?: string | null;
  formRotation?: string | number | null;
  formRotationType?: string | null;
  formRotationTitle?: string | null;
  contentTitleName?: string | null;
  contentUnitPathId?: string | null;
  contentUnitPathName?: string | null;
  contentUnitCode?: number | string | null;
  contentTitleCode?: number | null;
  contentUnitTreeLevel?: number | null;
  contentUnit?: OrgItem | null;
  level?: string | number | null;
}

export interface FileCustom {
  id?: number;
  uid?: string;
  name?: string;
  code?: string | number;
  status?: string;
  file?: File;
}

export interface ParamDetail {
  ghiNhanyKienPd?: string | null;
  ghiChuGiaiTrinhThem?: string | null;
  ghiChuChuyenBanHanh?: string | null;
  ngayKyHieuLuc?: Date | null;
  lyDohuy?: boolean | null;
  chiTietLydoHuy?: string | null;
  ngayHieuLucDaChuyen?: Date | null;
  ngayHieuLucMoi?: Date | null;
  lyDoHuyPa?: string | null;
  commonEffectiveDateCB?: boolean;
  issueLater?: boolean;
  effectiveDate?: Date | string | null;
  effectiveDateTime?: Date | null;
  endDate1: Date | string | null;
  endDate2: Date | null;
  minimumTime: number | null;
  maximumTime: number | null;
  typeMaximumTime: number | null;
}

export interface ViewParent {
  groupId: number;
  parentId: number;
}

export interface FormGroupList {
  id?: number;
  categoryId?: number;
  name?: string;
}

export interface EmployeeInfoMultiple {
  createdBy?: string;
  employee?: EmployeeDetailMultiple;
  employeeCode?: string;
  employeeUnitPathId?: string;
  employeeUnitPathName?: string;
  flowStatusCode: string;
  flowStatusPublishName?: string;
  groupByKSPTNL?: boolean;
  groupProposeTypeId?: number;
  id?: number;
  isDecision?: boolean;
  isEdit?: boolean;
  isGroup?: boolean;
  isIncome?: boolean;
  isInsurance?: boolean;
  issuedLater?: boolean;
  proposeId?: number;
  proposePerformanceResults?: ProposePerformanceResults[];
  proposeRotationReasons?: ProposeRotationReasons[];
  relationType?: boolean;
  sameDecision?: boolean;
  proposeComment?: {
    id?: number;
    strengths: string;
    weaknesses: string;
  };
  contentUnitPathName?: string;
  contentTitleName?: string;
  proposeAlternative?: ProposeAlternative[];
  formRotationName?: string;
  conditionViolation?: string;
  consultationStatus?: string;
  salaryStatus?: string;
  interviewStatus?: string;
  interviewResult?: string;
  cancelProposeRetractFlag?: boolean;
  contentLevel?: string;
  contentUnitName?: string;
  positionLevel?: string;
  implementDate?: Date;
  conditionViolationCode?: number;
  contentUnitCode?: string;
  contentTitleCode?: number;
  totalPoint?: number;
  contentUnitPathId?: string;
  contentUnitTreeLevel?: number;
  formName?: string;
  minimumTime?: number;
  maximumTime?: number;
  formRotationCode?: string;
  alternativeTypeCheck?: boolean;
  sortOrder?: number;
  proposeCode?: string | number;
  flowStatusPublishCode?: string | number;
  withdrawNote?: string;
  numberUpdatedInterview?: number;
  withdrawNoteName?: string;
  updatedDate?: Date;
  formGroup?: string | number;
  personalCode?: string;
}

export interface EmployeeDetailMultiple {
  dateOfBirth?: string | Date;
  empCode?: string;
  empId?: number | string;
  fullName?: string;
  gender?: string;
  jobId?: number | string;
  jobName?: string;
  joinCompanyDate?: string | Date;
  orgId?: number | string;
  orgName?: string;
  orgPathId?: string;
  orgPathName?: string;
  posSeniority?: number;
  positionLevel?: string;
  seniority?: number;
  semesterGradeT1?: string | number;
  semesterGradeT2?: string | number;
  semesterRankT1?: string | number;
  semesterRankT2?: string | number;
  employeeBusinessLineCode?: string;
  employeeBusinessLineName?: string;
}

export interface FormGroupMultipleList {
  code?: number;
  formCode?: string;
  formGroup?: string;
  formName?: string;
  formType?: string;
  showMaxTime?: boolean | null;
  showMinTime?: boolean | null;
  showSeniority?: boolean;
  sortOrder?: number;
  status?: string;
}

export interface ListFileTemplate {
  id: number;
  typeCode: string;
  subTypeCode: string;
  templateCode: string;
  templateName: string;
  linkTemplate: string;
}

export interface WithdrawPropose {
  ids?: number[];
  proposeType: number;
  proposeWithdrawType: number;
  reason: string;
  isApproveHcm: boolean;
  statementNumber: string | number;
  statementDate: Date | string | null;
}

export interface ApproveWithdrawPropose {
  id?: number;
  proposeType?: number;
  ideaType?: number;
  comment?: string;
  screenCode?: string;
}

export interface ProposeDetail {
  cancelProposeRetractFlag?: boolean;
  conditionViolation?: string;
  conditionViolationCode?: number;
  contentTitleCode?: number;
  contentTitleName?: string;
  contentUnitCode?: number;
  contentUnitName?: string;
  contentUnitPathId?: string;
  contentUnitPathName?: string;
  contentUnitTreeLevel?: number;
  createdBy?: string;
  employeeCode?: string;
  employeeUnitPathId?: string;
  employeeUnitPathName?: string;
  flowStatusCode?: string;
  formRotation?: number;
  formRotationCode?: string;
  formRotationName?: string;
  formRotationTitle?: number;
  formRotationType?: string;
  groupByKSPTNL?: boolean;
  groupProposeTypeId?: number;
  id: number;
  isDecision?: boolean;
  isGroup?: boolean;
  isIncome?: boolean;
  isInsurance?: boolean;
  issueLevelCode?: string;
  issuedLater?: boolean;
  proposeId?: number;
  proposePerformanceResults?: [];
  proposeRotationReasons?: [];
  relationType?: boolean;
  sameDecision?: boolean;
  withdrawNote?: number | string;
  statementNumber?: number | string;
  submissionDate?: Date;
  docId?: number | string;
  withdrawType?: number | string;
  formGroup?: number | string;
}

export interface LevelTitle {
  code?: string;
  id?: number;
  name?: string;
  positionLevel?: string;
  sortOrder?: number;
}

export interface FileDTOList {
  docId?: string;
  fileName?: string;
  id?: number;
  isIn?: boolean;
  parentId?: number;
  type?: string;
}

export interface listPropose {
  proposeTypeCode?: string;
  groupProposeTypeId?: number;
  flowStatusCode?: string;
  issueLevelCode?: string;
  approvalCode?: string;
  proposeId?: number;
  proposeDetailId?: number;
  createdBy?: string;
  isGroup?: boolean;
  groupByKSPTNL?: boolean;
  formGroup?: string | number;
}

export interface ListHandleOffer {
  formRotationCode: string | number;
  formRotationName: string;
  titleLevelName: string;
  proposePositionName: string | number;
  conditionLcbn: string;
  contentViolation: string;
  desiredMonthlyIncome: string | number;
  opinionSentSalary: string;
  statusSalary: string;
  hasInterview: boolean;
  sentInterviewComment: string;
  statusInterview: string;
  hasConsultation: boolean;
  statusConsultation: string;
  issueLevelName: string;
  approvalName: string;
  signerName: string;
  id: number;
  isInterviewed: boolean;
  isPtnl: boolean | undefined;
  isSentSalary: boolean;
  optionCode: string | number;
  proposeTitle: number;
  proposeTitleName: string | null;
  proposeUnit: number;
  proposeUnitName: string;
  proposeUnitPathId: string;
  proposeUnitPathName: string;
  selected: boolean;
  isEdit: boolean;
  notSave: boolean;
  issueLevelCode: string;
  signerCode: string;
  approvalCode: string;
  issueLevel: string;
  isShowSendInterview?: boolean;
  approvementApproveCode?: string;
  approvementSignerCode?: string;
  salaryComment?: string;
  titleLevelCode?: string | number;
  minimumTime?: number;
  maximumTime?: number;
  timeType?: number;
  hasMinimumTime?: boolean;
  hasMaximumTime?: boolean;
}

export interface ListPlanCheckOption {
  key: number;
  value: string | null;
}

export interface ReturnPropose {
  proposeDetailIds: number[];
  comment: string;
}
export interface ConditionViolationList {
  categoryCode: string;
  categoryId: number;
  code: string | number;
  id: number;
  name: string;
}

export interface CheckOptionPlan {
  approvalCode: string | null;
  issueLevelCode: string | null;
  signerCode: string | null;
}

export interface IssueLevelOption {
  [key: number]: IssueLevelOptionType[];
}

export interface IssueLevelOptionType {
  code: string;
  name: string;
}

export interface ApproverSignerOption {
  code: string;
  fullname: string;
  title: string;
  approvementConfigCode: string;
}

export interface EmployeeInfoMultipleEdit {
  id?: number;
  contentTitleCode?: number;
  contentTitleName?: string;
  contentUnitCode?: string;
  contentUnitName?: string;
  contentUnitPathId?: string;
  contentUnitPathName?: string;
  contentUnitTreeLevel?: number;
  contentLevel?: string;
  proposeComment?: {
    id?: number | null;
    strengths: string;
    weaknesses: string;
  };
  proposeAlternative: {
    id?: number | null;
    proposeDetailId?: number | null;
    alternativeType?: number | null;
    estimatedDate?: string | null;
    replacementEmployeeCode?: string | number | null;
    replacementEmployeeName?: string | null;
    replacementEmployeeTitleCode?: string | null;
    handoverEmployeeCode?: string | number | null;
    handoverEmployeeName?: string | null;
    handoverEmployeeTitleCode?: string | number | null;
    handoverEmployeeTitleName?: string | null;
    replacementEmployeeTitleName?: string | null;
    replacementEmployee?: string | null;
    handoverEmployee?: string | null;
  };
  implementDate?: Date;
  minimumTime?: number;
  maximumTime?: number;
  rotationReasonID?: string | null;
  formRotationCode?: number;
  formRotationType?: string | null;
  updatedDate?: Date;
}

export interface ListConsultationBefore {
  consultDate: Date | string | null;
  consultName: string;
  consultUnit: string | number;
  consultUnitName: string;
  contentLevelName: string;
  contentTitleName: string;
  contentUnitName: string;
  id: number;
  note: string;
  proposeDetailId: number;
  proposeOptionId: number;
  show: boolean;
  showOpinion: boolean;
  status: string;
  proposeOptionCode: string;
  formName: string;
  notSave?: boolean;
}

export interface ListInterviewNow {
  boardCode: string;
  boardMember: string;
  interviewDate: Date | string | null;
  interviewPoint: number;
  interviewResult: number;
  interviewResultDescription: string;
  interviewSentDate: Date | string | null;
  note: string;
  numberUpdated: string | number;
  proposeDetailId: number;
  proposeOptionCode: string;
  proposeOptionId: number;
  proposeTitle: string;
  proposeUnit: string;
  receivedResultDate: Date | string | null;
  recruitment: string;
  sendInterviewResultBy: string;
  suitablePosition: string;
  docId: string;
  fileName: string;
}

export interface WithdrawNoteConfigList {
  key: string | number;
  role: string;
}


export interface ProposeTableFile {
  uid: string | number,
  name: string,
  status: string,
  id: string | number,
}

export interface RequestCancelPropose {
  reasonOrCorrection?: boolean;
  viewDetailReasonOrCorrection?: string;
}

export interface SendInterviewsModel {
  isShowSendInterview?: boolean;
  isPtnl?: boolean;
  optionId?: number;
}

export interface IssueCorrection {
  effectiveDate?: Date | string | null,
  sameDecision?: boolean,
  minimumTime?: number | null,
  maximumTime?: number | null,
  issueEffectiveEndDate1?: Date | string | null,
  reasonOrCorrection?: boolean | null,
  viewDetailReasonOrCorrection?: string | null,
}

export interface DateModelIssueCorrection {
  effectiveDate: Date | string | null;
  sameDecision: boolean;
  minimumTime: number | null;
  maximumTime: number | null;
  typeMaximumTime: number | null;
  endDate: Date | string | null;
  reason: boolean | null;
  detailReason: string | null;
}

export interface RecommendIndividualModel {
  id: number;
  formGroup: number;
  externalApprove?: boolean;
  flowStatusCode?: string;
  sentDate?: Date;
  personalCode?: string
  proposeDetails?: ProposeDetails[];
}

export interface DataRecommendIndividual {
  employee?: EmployeeRecommendIndividual;
  proposeComment?: RecommendIndividualComment[];
  contentTitleCode?: number;
  contentTitleName?: string;
  contentUnitCode?: string | number;
  contentUnitName?: string;
  contentUnitPathId?: string;
  contentUnitPathName?: string;
  contentUnitTreeLevel?: number;
}

export interface RecommendIndividualComment {
  proposeReason?: string;
  commitment?: string;
  strengthsPtnl?: string | null;
  weaknessesPtnl?: string | null;
}

export interface EmployeeRecommendIndividual {
  orgId: number;
  empId: number;
  empCode: string;
  fullName: string;
  email: string;
  posId: number;
  posName: string;
  jobId: number;
  jobName: string;
  orgPathId: string;
  orgPathName: string;
  orgName: string;
  joinCompanyDate: Date | string | null;
  dateOfBirth: Date | string | null;
  posSeniority: number;
  seniority: number;
  treeLevel: number;
  empIdp: number | string;
  majorLevelId: number | string;
  majorLevelName: number | string;
  faculityId: number;
  faculityName: string;
  schoolId: number;
  schoolName: string;
  positionLevel: number | string;
  gender: number | string;
  partyDate: Date | number | string;
  partyOfficialDate: Date | number | string;
  qualificationEnglish: number | string;
  facultyName: number | string;
  semesterGradeT1: number | string;
  semesterRankT1: number | string;
  semesterGradeT2: number | string;
  semesterRankT2: number | string;
  ratedCapacity: number | string;
  nearestDiscipline: number | string;
  endOfDisciplinaryDuration: number | string;
  userToken:  number | string;
  posSeniorityName:  number | string;
  seniorityName:  number | string;
  levelName:  number | string;
  formGroup:  number | string;
  concurrentTitleId:  number | string;
  concurrentTitleName:  number | string;
  concurrentUnitId:  number | string;
  concurrentUnitName:  number | string;
  concurrentUnitPathId:  number | string;
  concurrentUnitPathName:  number | string;
  concurrentTitleId2:  number | string;
  concurrentTitleName2:  number | string;
  concurrentUnitId2:  number | string;
  concurrentUnitName2:  number | string;
  concurrentUnitPathId2: number | string;
  concurrentUnitPathName2:  number | string;
  periodNameT2?: string;
  periodNameT1?: string;
}

export interface ProposeDetails {
  employee?: EmployeeRecommendIndividual;
  contentTitleCode?: number;
  contentTitleName?: string;
  contentUnitCode?: string | number;
  contentUnitName?: string;
  contentUnitPathId?: string;
  contentUnitPathName?: string;
  contentUnitTreeLevel?: number;
  proposeComment?: RecommendIndividualComment;
  id?: number;
  flowStatusCode?: string;
  conditionViolationCode?: number;
  conditionViolations?: string[];
  formGroup: number;
  sameDecision?: boolean | null;
  isIncome?: boolean | null;
  isInsurance?: boolean | null;
  isDecision?: boolean | null;
  issuedLater?: boolean | null;

}

export interface ListTitle {
  jobId?: number;
  jobName?: string;
}

export interface TimeTypeEnums {
  code: number;
  description: string;
}

export interface OrgEmployee {
  orgId: number;
  orgName: string;
  pathId: string;
  pathName: string;
  treeLevel: number;
  orgNameFull: string;
}

export interface OrgItemUnit {
  orgId: number;
  orgName: string;
  pathId: string;
  pathResult: string;
  orgLevel: number;
  orgNameFull: string;
}

export interface ExistingEmployee {
  id?: number;
  proposeType?: number;
  isShow?: boolean;
  isShowForConcurrent?: boolean;
  isSuccess?: boolean;
  positionDTOListReq?: ConcurrentPositionDTOList[];
}

export interface ConcurrentPositionDTOList {
  employeeCode?: string;
  employeeName?: string;
  employeeTitle?: string;
  concurrentEmployeeCode?: string;
  concurrentEmployeeName?: string;
  concurrentTitle?: string;
  formGroup?: number;
  employee?: string;
  concurrentEmployee?: string;
}

export interface ProposeTypeModel {
  proposeType: number | null;
  note?: string;
}

export interface ProposeReason {
  formGroupList: number,
  reasonExtension: string,
  detailReason: string,
}



export interface AppointmentRotationSearch {
  employeeCode: string | null,
  employeeName: string | number | null,
  position: string | null,
  title: string | null,
  unit: string | number | null,
  formGroup: string | number | null,
  form: string | number | null,
  issueLevel: string | number | null,
  dueDateFrom: string | Date | null,
  dueDateTo: string | Date | null,
  proposeStatus: string | null,
  autoPrompted: string | null,
  extendReason: string | null,
  page: number,
  size: number,
}

export interface TableAppointmentRotation {
  autoPromted: string;
  capacityPoint: number;
  dateOfBirth: string | Date;
  effectiveDate: string | Date;
  employeeCode: string;
  employeeId: number | string;
  employeeName: string;
  form: string;
  formGroup: string;
  formGroupName: string;
  formName: string;
  joinDate: string | Date;
  mbSeniority: number;
  originalTitle: number;
  originalUnit: number;
  planToDate2: string | Date;
  position: number;
  positionName: string;
  positionSeniority: number;
  proposeStatus: string;
  title: number;
  titleName: string;
  unit: number;
  workProcessId: number;
}
