

export interface FlowStatusPublishConfigList {
  role?: string;
  key?: string;
}

export interface FormDisplaySearchDTOS {
  code?: string;
  formCode?: string;
  formName?: string;
  newExternalRecruitment?: boolean;
  newRecruitment?: boolean;
  resign?: boolean;
  resignExternal?: boolean;
  sortOrder?: number;
  status?: boolean;
}

export interface IssueLevelList {
  code?: string;
  name?: string;
  sortOrder?: number;
}

export interface JobDTO {
  jobId?: number;
  jobCode?: string;
  jobName?: string;
}

export interface ProposeRotationReasonsConfigDTOS {
  id?: number;
  code?: string;
  name?: string;
}

export interface StatusList {
  id?: number;
  code?: string;
  name?: string;
  statusType?: number;
  role?: string;
  key?: string;
}

export interface TypeDTOS {
  code?: string;
  groupingType?: number;
  multipleTemplate?: number;
  name?: string;
  selfPropose?: boolean;
  singleTemplate?: boolean;
  sortOrder?: number;
  status?: number;
}

export interface ParamSearch {
  proposeCode?: string | null; // mã đề xuất
  formCode?: string | null; // loại đề xuất
  timeApprove?: string | null; // thời gian phê duyệt
  issueLevel?: string | null; // cấp ban hành
  statusPublish?: string | null; // trạng thái ban hành
  employeeCode?: string | null; // mã nhân viên
  employeeName?: string | null; // họ tên
  effectiveDate?: string | null; //ngày hiệu lực
  effectiveStartDate?: string | null; //ngày hiệu lực
  effectiveEndDate?: string | null; //ngày hiệu lực
  signerCode?: string | null; // người ký
  signerFullName?: string | null; // tìm kiếm người kí theo tên
  currentUnit?: string | null; // đơn vị  hiện tại
  currentTitle?: string | null; // chức danh hiện tại
  proposeUnit?: string | null; // đơn vị đề xuất
  proposeTitle?: string | null; // chức danh đề xuất
  proposeOrigin?: boolean; // Đề xuất gốc - Default
  proposeItem?: boolean; //	Đề xuất thành phần - Default
  proposeGroup?: boolean; //	Đề xuất gop - Default
  screenCode?: string | null; // Default
  approveStartDate?: string | null;
  approveEndDate?: string | null;
  page?: number;
  size?: number;
  decisionNumber?: string | null;
  issueContent?: string | null;
  issueCode?: string | null;
}

export interface ParamPromulgate {
  id?: number;
  proposeCategory?: string;
  state?: string;
  type?: string;
  screenCode?: string;
  action?: string;
}

export interface listFullName {
  empCode?: string;
  empId?: number;
  fullName?: string;
}
