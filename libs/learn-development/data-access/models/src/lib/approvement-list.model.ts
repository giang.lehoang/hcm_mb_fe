export interface ListApprove {
  proposeId: number;
  proposeDetailId: number;
  proposeCategory: string;
  proposeUnitRequest: string;
  proposeTypeCode: string;
  proposeTypeName: string;
  relationType: string;
  groupProposeType: string;
  groupProposeTypeId: number;
  createdBy: string;
  sentDate: Date | string | null;
  flowStatusCode: string;
  publishStatusCode: string;
  publishStatusName: string;
  employeeCode: string;
  employeeName: string;
  joinDate: Date | string | null;
  currentUnitCode: string;
  currentUnitName: string;
  currentTitleId: string;
  currentTitleName: string;
  currentPosition: string;
  proposeUnit: string;
  proposeTitle: string;
  proposePosition: string;
  statusDisplay: string;
  statusPublishDisplay: string;
  isGroup: boolean;
  issueLevelCode: string;
  issueLevelName: string;
  signerCode: string;
  signerFullName: string;
  signerTitleId: string;
  signDate: Date | string | null;
  approvalCode: string;
  approvalFullName: string;
  approvalTitleId: string;
  approvalDate: Date | string | null;
  groupByKSPTNL: boolean;
}

export interface ListGroupPropose {
  proposeTypeCode: string;
  groupProposeTypeId: string | number;
  flow: string;
  issueLevelCode: string;
  approvalCode: string;
  proposeId: number;
  proposeDetailId: number;
  isGroup: boolean;
  createdBy: string;
}

export interface  RoleInterface {
  key: string;
  roleCode: string;
}
