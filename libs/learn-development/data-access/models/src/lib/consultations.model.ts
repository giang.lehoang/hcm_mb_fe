export interface Consultion {
  sentDateFrom: string;
  sentDateTo: Date;
  employeeTitle: string;
  statusApproval: string;
  proposeType: string; // Loại đề xuất
  sentDate: string; // Ngày gửi đề xuất
  statusApprove: string; // Trạng thái phê duyệt
  statusPublish: string; // Trạng thái ban hành
  statusConsultation: string; // Trạng thái tham vấn
  employeeCode: string; // Mã nhân viên
  employeeName: string; // Họ và tên
  joinDate: string; // Ngày vào
  currentUnit: string; // Đơn vị đi
  currentTitle: string; // Chức danh đi
  currentPosition: string; // Vị trí đi
  currentLevelTitle: string; // Level đi
  currentBusinessLine: string; // Khối trục dọc đi
  formName: string; // Hình thức
  proposeUnit: string; // Đơn vị đến
  proposeTitle: string; // Chức danh đến
  proposePosition: string; // Vị trí đến
  proposeLevelTitle: string; // Level đến
  proposeBusinessLine: string; // Khối trục dọc đến
  issueLevelName: string; // Cấp ban hành
  proposeDetailId?: number;
  statusApproveText: string;
  tatusPublishText: string;
  statusConsultationText: string;
  page: number;
  size: number;
}

export interface ConsultionCreate {
  consultDate: Date;
  consultName: string;
  consultUnit: string;
  status: number;
  note: string;
  show: boolean;
  id: number;
  consultUnitName: string;
  showOpinion?: boolean
}

export interface DataTempalte {
  approveName?: string;
  format: string;
  idAll?: string | string[];
  idCeo: string | string[];
  idHdqt: string | string[];
  linkReport: string;
  reportPath: string;
}
