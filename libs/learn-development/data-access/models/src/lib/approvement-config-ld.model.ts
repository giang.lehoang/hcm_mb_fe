export interface ApproveConfig {
  id?: string;
  code?: string;
  issueLevel?: string;
  issueLevelCode?: string;
  employeeTitle?: number;
  targetLevel?: string;
  sourceUnit?: {
    orgId: number;
    orgName: string;
  };
  sourceUnitId?: number;
  sourceUnitName?: string;
  targetUnit?: {
    orgId: number;
    orgName: string;
  };
  targetUnitId?: number;
  targetUnitName?: string;
  targetPositionCode?: string | number | null;
  targetPosition?: string;
  targetLevelCode?: string[] | number[];
  targetTitleCode?: string[] | number[];
  targetTitle?: string;
  businessLineName?: string;
  businessLineCode?: string;
  form?: string;
  formCode?: string;
  effectiveStartDate?: Date | string | null;
  effectiveEndDate?: Date | string | null;
  decisionDate?: Date | string | null;
  employeeCode?: string;
  employeeFullName?: string;
  employeeTitleCode?: string;
  canApprove?: boolean;
  canSign?: boolean;
  isSameTitle?: boolean;
  isSameLevel?: boolean;
  level?: string;
  area?: string;
  subArea?: string;
  hasConsultation?: boolean;
  hasInterview?: boolean;
  decisionNumber?: string;
  employeeTitleName?: string;

}

export interface bodyExcel {
  targetUnit?: string;
  issueLevel?: string;
  effectiveStartDate?: Date | string | null;
  effectiveEndDate?: Date | string | null;
  pageNumber?: number;
  pageSize?: number;
  templateType?: string;
  issuedLevels?: string;
}

export interface CategoryModel {
  code?: string | number;
  name?: string;
  description?: string;
  value?: string;
  orderNum?: string;
  parentCode?: string;
  statusType?: number;
  id?: number;
  sortOrder?: number;
  positionLevel?: string;
  subArea?: checkArea[];
}

export  interface checkArea {
  code?: string | number;
  name?: string;
}

export  interface PublishStatusModel {
  key?: string | number;
  role?: string;
}

