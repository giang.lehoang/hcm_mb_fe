export interface IssueLevel {
  id?: number;
  code?: string;
  name?: string;
  approvalTitle?: string[] | string;
  signerTitle?: string[] | string;
  flag?: boolean;
  page?: number;
  size?: number;
  sortOrder?: number;
}
