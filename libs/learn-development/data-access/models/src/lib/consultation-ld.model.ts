import {Unit} from "./proposed-unit.model";
import {JobDTO} from "./promulgate.model";

export interface DataCommonConsulation {
  formLD?: FormLD[];
  jobDTOS?: JobDTO[];
  units?: Unit[];
}

export interface FormLD {
  code?: string;
  formCode?: number;
  jobName?: string;
  newExternalRecruitment?: boolean;
  newRecruitment?: boolean;
  resign?: boolean;
  resignExternal?: boolean;
  sortOrder?: number;
  status?: boolean;
}
export interface ParamsSearchConsulation {
  formCode?: string[];
  formDate?: string;
  fromDate?: string;
  toDate?: string;
  currentUnit?: unit;
  currentPosition?: string;
  proposeUnit?: unit;
  rangeTime?: string[];
  proposePosition?: string;
  page?: number;
  size?: number;
}

export interface unit {
  orgId: string;
  orgLevel: number;
  orgName: string;
  pathResult: string;
}
export interface ConsultationModel {
  formCode: string[];
  formDate: string;
  toDate: string;
  currentUnit: string;
  currentPosition: string[];
  proposeUnit: string;
  proposePosition: string[];
}

