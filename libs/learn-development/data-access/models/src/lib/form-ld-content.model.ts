export interface FormLDContent {
  formCode?: null;
  formName?: null;
  sortOrder?: null;
  formType?: null;
  formGroup?: string[] | number[];
  publishStatus?: string[] | number[];
  showMinTime: boolean | null,
  showMaxTime: boolean | null,
  showSeniority: boolean | null,
  status?: null;
  page?: number;
  size?: number;
  code?: string;
}

export interface BodyExcel {
  formCode?: string | null;
  formName?: string | null;
  sortOrder?: number | null;
  formType?: number | null;
  formGroup?: number | null;
  status?: boolean | null;
  page?: number;
  size?: number;
}

export interface FormLDCommon {
  mapFormGroup?: ArraySection[],
  mapFormStatus?: ArraySection[],
  mapFormType?: ArraySection[],
}

export interface ArraySection {
  name?: string;
  code?: number | string;
}
