
export interface Plan {
  proposeType?: string;
  sentDate?: Date | string | null;
  sentStartDate?: Date | string | null;
  sentEndDate?: Date | string | null;
  employeeCode?: string;
  employeeName?: string;
  issueLevelCode?: string | number;
  approvalCode?: string | number;
  employeeUnits?: string | number;
  employeeLines?: string | number;
  employeeUnit?: string | number;
  employeeTitleId?: string | number;
  proposeUnit?: string | number;
  proposeTitle?: string;
  interviewStatus?: string | number;
  page?: number;
  size?: number;
  toDate?: Date | string | null;
  toStartDate?: Date | string | null;
  toEndDate?: Date | string | null;
  salaryStatus?: string | number;
  consultationStatus?: string | number;
  screenCode?: string;
  proposeTitleName?: string;
  proposeUnitPathName?: string;
  id?: number;
  proposePosition?: number;
  employeeTitle?: number;
  employeePosition?: number;
  followStatusCode?: string;
  proposeDetailId?: number;
  sender?: string;
  selected?: boolean;
  approvalName: string;
  consultationStatusDescription: string;
  employeePositionName: string;
  employeeTitleName: string;
  employeeUnitName: string;
  flowStatusCode: string;
  flowStatusDescription: string;
  interviewStatusDescription: string;
  issueLevel: string;
  issueLevelName: string;
  joinDate?: Date | string | null;
  proposeId: number;
  proposeCategory: string | number;
  proposeUnitName: string;
  salaryStatusDescription: string;
  formRotationName?: string;
  optionCode?: string;
  employeeUnitPathName?: string;
  isInterviewed?: boolean;
  withdrawNoteCode?: string | number;
  relationType?: string | number;
}

export interface SendInterviewModel {
  ids: number[];
}
