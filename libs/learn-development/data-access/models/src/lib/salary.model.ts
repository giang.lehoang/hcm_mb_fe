export interface Salary {
  id?: number | null;
  employeeCode?: string | null;
  fullName?: string;
  currentUnit?: string;
  currentTitle?: string;
  currentPosition?: string;
  isCreate?: boolean;
  proposeUnit?: string;
  time?: string | null;
  formName?: string;
  currentBasicSalary?: number;
  currentRank?: number;
  currentLevel?: number;
  currentBenefitRate?: number;
  currentCoefficientLcb?: number;
  currentLevelEfficiency?: number;
  currentBenefitRateEfficiency?: number;
  currentMonthSalary?: number;
  sendDateSalary?: string;
  proposeStrip?: string;
  proposeRank?: number;
  proposeLevel?: number;
  proposeBenefitRate?: number;
  proposeCoefficientLcb?: number;
  proposeLevelEfficiency?: number;
  proposeBenefitRateEfficiency?: number;
  incomeDifferenceLevel?: number;
  travelFamily?: number;
  carContract?: number;
  rentHouse?: number;
  lifeInsurance?: number;
  minimumSalaryNsld?: number;
  baseProposition?: string;
  optionId?:  string ;
  proposeCode?: string;
  requestType?: string;
  salaryStatus?: string;
  requestDate?: string;
  sentDate?: string;
  proposeType?: string;
  employeeName?: string | null;
  joinDate?: string;
  employeeUnit?: any;
  employeeUnitName?: string;
  employeeTitleId?: string;
  employeeTitleName?: string;
  employeePosition?: string;
  employeeLevelTitleCode?: string;
  contentUnitCode?: string;
  contentUnitName?: string;
  contentTitleCode?: string;
  contentTitleName?: string;
  contentPositionCode?: string;
  contentPositionName?: string;
  contentTargetLevelTitleCode?: string;
  formRotationType?: string;
  semesterRankT2?: string;
  proposeMonthSalary?: string;
  desiredMonthlyIncome?: string;
  note?: string;
  currentBasicRange?: string,
  currentBasicRank?: number,
  currentBasicLevel?: number,
  currentBasicBenefitRate?: number,
  currentBasicBenefitIncentiveCoefficient?: number,
  currentBasicBenefitIncentiveLevel?: number,
  currentBasicBenefitIncentiveRate?: number,
  proposeBasicRange?: string,
  proposeBasicRank?: number,
  proposeBasicLevel?: number,
  proposeBasicBenefitRate?: number,
  proposeBasicIncentiveCoefficient?: number,
  proposeBasicIncentiveLevel?: number,
  proposeBasicIncentiveRate?: number,
  differentRate?: number,
  differentLevel?: number,
  minPerformanceSalary?: number,
  salaryBaseComment?: number,
  bonusTravel?: number,
  bonusTransport?: number,
  bonusHouseRent?: number,
  bonusLifeAssurance?: number,
  bonus1?: number,
  bonus2?: number,
  bonus3?: number,
  bonus4?: number,
  bonus5?: number,
  sentSalaryTo?: string | null;
  sentSalaryFrom?: string | null;
  statusSalaryArrangement?: string | null;
  employeeTitle?: string | null;
  contentTitle?: string | null;
  contentUnit?: any;
  isFirstRequest?: boolean;
  isRecommendAgain?: boolean;
  contentUnitPathName?: string;
  employeeUnitPathName?: string;
  proposeDetailId: null,
  page?: number;
  size?: number;
}

export interface  DataCommonSalary {
  listStatus?: any[];
  listJob?: any[];
}
