export interface FileDetail {
  decisionId: number[];
  decisionName: string[];
  decisionSignId: number;
  decisionSignName: string;
  documentType: number;
  id: number;
  proposeDetailId: number;
  proposeId: number;
  decisionPublishName: string;
  decisionPublishId: number;
}

export interface StateConfirmPromulgate {
  buttonListScreen?: string;
  list?: string[];
  buttonAction?: TypeButtonAction[];
  screenAction?: string;
}

export interface TypeButtonAction {
  name?: string;
  type?: 'primary' | 'dashed' | 'link' | 'text' | 'default';
  action?: boolean;
  typeState?: string;
  confirmQues: string;
}