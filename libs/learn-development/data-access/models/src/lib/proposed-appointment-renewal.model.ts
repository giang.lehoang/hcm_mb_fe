export interface CommentsUnitCharge {
  index?: number | string | null;
  nameFunction?: string;
  strength?: string;
  weaknesses?: string;
}

export interface WorkAssigned {
  id?: number | string | null;
  index?: number | null;
  contentTitle?: string | null;
  source?: number | null;
  personnelPlace?: string | null;
  trainingPropose?: boolean | null;
  trainingProposeName?: string | null;
  disabledItem?: boolean | null;
  layoutPlan?: string;
  positionType?: string;
  unitType?: number;
  concurrentEmployeeCode?: string;
  concurrentEmployeeName?: string;
  alternativePlan?: string | null;
}

export interface ProposedAppointmentRenewal {
  id: number;
  proposeDetails?: ProposeDetailsAppointmentRenewal[];
  proposeUnitRequest?: number;
  proposeUnitRequestName?: string;
  relationType?: boolean;
  flowStatusCode?: string | number;
  sentDate?: Date | string | null;
  flowStatusName?: string;
  formGroup?: number | string;
  isGroup?: boolean;
  groupByKSPTNL?: boolean;
  externalApprove?: boolean | null;
  // sentDate: this.proposeDTO?.sentDate

}
export interface ProposeDetailsAppointmentRenewal {
  // Thông tin đề xuất
  formRotation: number;
  formRotationType: string;
  formRotationCode: number;
  contentUnitCode: number;
  contentTitleCode: number;
  contentTitleName: string;
  contentUnitPathId: string;
  contentUnitPathName: string;
  contentUnitTreeLevel: number;
  //Thông tin nhân viên
  employee: {
    orgId?: number;
    empId?: number;
    empCode?: string;
    fullName?: string;
    jobId?: number;
    jobName?: string;
    orgPathId?: string;
    orgPathName?: string;
    positionLevel?: string;
    gender?: string;
    joinCompanyDate?: Date | string;
    dateOfBirth?: Date | string;
    posSeniority?: number;
    seniority?: number;
    posSeniorityName?: string;
    seniorityName?: string;
    levelName?: string;
  };
  employeeUnitPathId: string;
  employeeUnitPathName: string;
  implementDate: Date | string;
  challengeTime: number;
  minimumTime: number;
  maximumTime: number;
  // Lý do bổ nhiệm
  concurrentReason: string;
  groupByKSPTNL: boolean;
  isGroup: boolean;
  id: number;
  contentUnitName: string;
  contentLevel: string;
  sortOrder: number;
  // Kết quả đánh giá THCV (Đánh giá chỉ tiêu được giao hiện tại)
  proposePerformanceResults?: ProposePerformanceResultsRenewal[];
  // Định hướng mảng công việc được giao sau khi bổ nhệm/gia hạn
  orientationJob: OrientationJob[];
  // Bố trí nhân sự tại các đơn vị liên quan
  proposeAlternative?: ProposeAlternativeRenewal[];
  // Nhận xét của phụ trách đơn v
  unitFeedback: UnitFeedback[];
  // Đề xuất đào tạo
  employeeTraining?: EmployeeTraining[];
  flowStatusCode?: number;
  externalApprove?: boolean;
}





export interface ProposePerformanceResultsRenewal {
  targetAssigned: string;
  density: number;
  planIndicator: number;
  planDeadline: Date | string;
  realityIndicator: number;
  realityDeadline: Date | string;
  point: number;
}

export interface OrientationJob {
  proposeDetailId?: number | null;
  orientationJob?: string;
  id?: number;
}

export interface ProposeAlternativeRenewal {
  unitType?: number | null;
  alternativeType?: number | null;
  replacementEmployeeCode?: string;
  replacementEmployeeName?: string;
  replacementEmployeeTitleCode?: string | number;
  replacementEmployeeTitleName?: string | null;
  replacementEmployee?: string;

  // unitType:1,
  concurrentEmployeeCode?: string | number;
  concurrentEmployeeName?: string | number;
  positionType?: string | number;
  alternativePlan?: string | number;
  contentTitle?: string;
  replacementUnitName?: string,

}

export interface UnitFeedback {
  configCode: string;
  proposeDetailId: number;
  weakness: string;
  strength: string;
}

export interface EmployeeTraining {
  proposeDetailId?: string | number | null;
  commonCode?: string;
  contentTraining?: string;
  name?: string;
  isCheck?: boolean;
  id?: number;
}

export interface EmployeeTrainingRenewal {
  categoryCode: string;
  categoryId: number;
  code: string;
  id: number;
  name: string;
}

export interface UnitFeedbackRenewal {
  categoryCode?: string;
  categoryId?: number;
  code?: string;
  id?: number;
  name?: string;
  commonCode?: string;
  strength?: string;
  weakness?: string;
  configCode?: string;
}

export interface ProposedError {
  code: string;
  errorMessage: string;
  warning: string;
}


export interface EmployeeAppointment {
  employeeCode?: string | number;
  employeeInfo?: string;
  formGroupEmp?: number | null;
  relatedProposeId: number;
  unitType: number;
}

export interface SaveEmployeeFormGroupModel {
  sortOrder?: number | null;
  proposeId?: string | null;
  employeeCodes?: string[];
  proposeUnitRequest: number | null;
  formGroup?: number | null;
  proposeUnitRequestName: string | null;
  relatedProposeId?: number;
  unitType?: number;
}


export interface TitleUnit {
  flagStatus: number;
  jobId: number;
  jobName: string;
  posId: number;
  pathId: string;
}

export interface InfoConcurrentlyNowModel {
  alternativeId: number;
  concurrentEmployeeCode: string;
  concurrentEmployeeName: string;
  concurrentEmployeeNameNew: string;
  positionType: string;
  optionId: number;
  alternativePlan: string;
}

export interface InforConcurrentPositionModel {
  concurrentTitleId: number;
  concurrentUnitId: number;
  alternativeId: number;
  id: number;
  concurrentUnitName: string;
  concurrentPosName: string;
  alternativePlan: string;
}

export interface DropDownConcurrentModel {
  code: number;
  description: string;
}
