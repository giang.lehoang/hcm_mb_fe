import { Pipe, PipeTransform } from '@angular/core';
import {Constants} from "@hcm-mfe/learn-development/data-access/common";


@Pipe({
  name: 'statusApprovementPromulgate',
})
export class StatusApprovementPromulgatePipe implements PipeTransform {
  transform(value: string, args?: any): string {
    let name;
    for (let i = 0; i < Constants.stateApprovement.length; i++) {
      const found = Constants.stateApprovement[i].list!.find((name) => name == value);
      if (found) {
        name = Constants.stateApprovement[i].buttonListScreen!;
        return name;
      }
    }
    return '';
  }
}
