import { Pipe, PipeTransform } from '@angular/core';
import {Constants} from "@hcm-mfe/learn-development/data-access/common";

@Pipe({
  name: 'statusConfirmPromulgate',
})
export class StatusConfirmPromulgatePipe implements PipeTransform {
  transform(value: string): string {
    let name;
    for (let i = 0; i < Constants.stateConfirmPromulgate.length; i++) {
      const found = Constants.stateConfirmPromulgate[i].list!.find((name) => name == value);
      if (found) {
        name = Constants.stateConfirmPromulgate[i].buttonListScreen!;
        return name;
      }
    }
    return '';
  }
}
