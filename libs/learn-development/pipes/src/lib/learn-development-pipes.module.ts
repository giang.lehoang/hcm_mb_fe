import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {StatusPromulgatePipe} from "./status-promulgate.pipe";
import {StatusConfirmPromulgatePipe} from "./status-confirm-promulgate.pipe";
import {StatusApprovementPromulgatePipe} from "./status-approvement-promulgate.pipe";


@NgModule({
  exports: [StatusPromulgatePipe,StatusConfirmPromulgatePipe, StatusApprovementPromulgatePipe],
  imports: [CommonModule],
  declarations: [StatusPromulgatePipe,StatusConfirmPromulgatePipe, StatusApprovementPromulgatePipe]
})
export class LearnDevelopmentPipesModule {}
