import { Pipe, PipeTransform } from '@angular/core';
import {Constants} from "@hcm-mfe/learn-development/data-access/common";

@Pipe({
  name: 'statusPromulgate'
})
export class StatusPromulgatePipe implements PipeTransform {
  transform(value: any, args?: any): string {
    let name;
    for(let i = 0; i < Constants.statePromulgate.length; i++){
      const found = Constants.statePromulgate[i].list.find((name)=>
       name == value
      )
      if(found){
        name = Constants.statePromulgate[i].name;
        return name
      }
    }
    return '';
  }

}
