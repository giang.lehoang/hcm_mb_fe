import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  forwardRef,
  Input,
  OnInit,
  Output,
  TemplateRef,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { InputBoolean } from 'ng-zorro-antd/core/util';
import { NzModalService } from 'ng-zorro-antd/modal';
import { noop } from 'rxjs';
import {FormModalShowTreeUnitComponent} from "@hcm-mfe/shared/ui/form-modal-show-tree-unit";
import {OrgItem, OrgItemUnit} from "@hcm-mfe/learn-development/data-access/models";

@Component({
  selector: 'mb-select-unit',
  templateUrl: './mb-select-unit.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => MbSelectUnitComponent),
    },
  ],
})
export class MbSelectUnitComponent implements OnInit, ControlValueAccessor {
  @Input() mbLabelText?: string;
  @Input() mbSuffixTemplate?: string | TemplateRef<any>;
  @Input() mbPlaceholder = '';
  @Input() mbAutofocus = false;
  @Input() mbType?: 'default' | 'warning' | 'error' | 'success';
  @Input() mbShowError = false;
  @Input() mbErrors: any;
  @Input() mbErrorDefs?: { errorName: string; errorDescription: string }[];
  @Input() mbUnitSelected: any;
  @Input() @InputBoolean() mbDisable: boolean = false;

  @Output() mbBlur: EventEmitter<any> = new EventEmitter<any>();
  @Output() mbKeyup: EventEmitter<any> = new EventEmitter<any>();
  @Output() mbChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() mbClick: EventEmitter<any> = new EventEmitter<any>();

  onTouched: () => void = noop;
  onChange: (_: any) => void = noop;
  iconType?: string;
  orgName: string  = '';
  orgNameFull: string  = '';
  value: OrgItemUnit | undefined | null;

  constructor(private modalService: NzModalService, private cd: ChangeDetectorRef) { }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.mbDisable = isDisabled;
  }

  writeValue(obj: OrgItemUnit): void {
    this.value = obj;
    this.orgName = this.value?.orgName;
    this.orgNameFull = obj?.orgLevel > 2 ? obj?.pathResult.split("-").splice(2).join("-") + " - " + obj?.orgName : obj?.orgName;
    if (obj?.orgLevel > 2 && obj?.pathResult.split("-").splice(2).length === 0) {
      this.orgNameFull = obj?.pathResult
    }
    this.onChange(this.value);
    this.cd.detectChanges();
  }

  ngOnInit() { }

  clearValue() {
    if(this.mbDisable){
      return;
    }
    this.orgName = '';
    this.orgNameFull = '';
    this.value = null;
    this.onChange(null);
  }

  openModal(): void {
    const modal = this.modalService.create({
      nzWidth:
        window.innerWidth > 767 ? (window.innerWidth / 1.5 > 1100 ? 1368 : window.innerWidth / 1.5) : window.innerWidth,
      nzTitle: ' ',
      nzContent: FormModalShowTreeUnitComponent,
      nzFooter: null,
    });
    modal.afterClose.subscribe((result) => {
      if (result?.orgName) {
        this.orgNameFull = result?.orgLevel > 2 ? result?.pathResult.split("-").splice(2).join("-") + " - " + result?.orgName : result?.orgName;
        this.value = result;
        this.onChange(this.value);
      }
    });
  }
}
