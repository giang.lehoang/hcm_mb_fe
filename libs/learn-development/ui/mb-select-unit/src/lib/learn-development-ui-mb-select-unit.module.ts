import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiFormModalShowTreeUnitModule} from "@hcm-mfe/shared/ui/form-modal-show-tree-unit";
import {MbSelectUnitComponent} from "./mb-select-unit/mb-select-unit.component";
import {NzIconModule} from "ng-zorro-antd/icon";
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [MbSelectUnitComponent],
  exports: [MbSelectUnitComponent],
  imports: [CommonModule, FormsModule, SharedUiMbInputTextModule, SharedUiFormModalShowTreeUnitModule, NzIconModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class LearnDevelopmentUiMbSelectUnitModule {}
