import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { HttpParams } from '@angular/common/http';
import {UserLogin} from "@hcm-mfe/shared/data-access/models";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {SearchFormService} from "@hcm-mfe/staff-abs/data-access/services";
import {ValidateService} from "@hcm-mfe/shared/core";
import {HTTP_STATUS_CODE, STORAGE_NAME} from "@hcm-mfe/shared/common/constants";
import {StringUtils} from "@hcm-mfe/shared/common/utils";
import {CatalogModel, SearchForm} from "@hcm-mfe/staff-abs/data-access/models";
import { StorageService } from '@hcm-mfe/shared/common/store';

@Component({
  selector: 'search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {
  @Input() moduleName = '';
  isSubmitted = false;
  form: FormGroup;
  empTypeList: CatalogModel[] = [];
  userLogin: UserLogin = new UserLogin();

  constructor(
    private baseService: BaseService,
    private router: Router,
    private toastrService: ToastrService,
    private translate: TranslateService,
    private searchFormService: SearchFormService,
    public validateService: ValidateService,
    private fb: FormBuilder
  ) {
    this.form = fb.group({
      employeeCode: [null],
      fullName: [null],
      employeeType: [null],
      organizationId: [null],
    });
  }

  ngOnInit(): void {
    this.userLogin = StorageService.get(STORAGE_NAME.USER_LOGIN);
    this.getEmpTypeList();
  }

  // empTypeList: this.searchFormService.getEmpTypeList();
  getEmpTypeList(){
    this.searchFormService.getEmpTypeList().subscribe(data => {
      if (data.code == HTTP_STATUS_CODE.OK) {
        this.empTypeList = data.data;
      }
    }, () => {
    });
  }

  get formControl() {
    return this.form.controls;
  }

  // getOptions(): Option[] {
  //   let options: Option[] = [];
  //
  //   let option = new Option();
  //
  //   option.code = 'employeeCode';
  //   option.values = this.formControl['employeeCode'].value ? [this.formControl['employeeCode'].value] : [];
  //   options.push(_.clone(option));
  //
  //   option.code = 'fullName';
  //   option.values = this.formControl['fullName'].value ? [this.formControl['fullName'].value] : [];
  //   options.push(_.clone(option));
  //
  //   option.code = 'employeeType';
  //   option.values = this.formControl['employeeType'].value ? [this.formControl['employeeType'].value] : [];
  //   options.push(_.clone(option));
  //
  //   option.code = 'organizationId';
  //   option.values = this.formControl['organizationId'].value ? [this.formControl['organizationId'].value['orgId'] + '|' + (this.formControl['organizationId'].value['orgName'] + '|' + this.formControl['organizationId'].value['parentName'])] : [];
  //   options.push(_.clone(option));
  //   return options;
  // }

  parseOptions() {
    let params = new HttpParams();
    if (!StringUtils.isNullOrEmpty(this.formControl['employeeCode'].value))
      params = params.set('employeeCode', this.formControl['employeeCode'].value);
    if (!StringUtils.isNullOrEmpty(this.formControl['fullName'].value))
      params = params.set('fullName', this.formControl['fullName'].value);
    if (this.formControl['employeeType'].value !== null)
      params = params.set('empTypeCode', this.formControl['employeeType'].value);
    if (this.formControl['organizationId'].value !== null)
      params = params.set('organizationId', this.formControl['organizationId'].value['orgId']);
    return params;
  }

    getSearchForm(): SearchForm {
        return {
            employeeCode: this.formControl['employeeCode'].value,
            fullName: this.formControl['fullName'].value,
            organizationId: this.formControl['organizationId'].value ? this.formControl['organizationId'].value.orgId : null,
            empTypeCode: this.formControl['employeeType'].value
        };
    }

}
