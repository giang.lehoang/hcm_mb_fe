import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkCalendarDetailImportComponent } from './work-calendar-detail-import.component';

describe('WorkCalendarDetailImportComponent', () => {
  let component: WorkCalendarDetailImportComponent;
  let fixture: ComponentFixture<WorkCalendarDetailImportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkCalendarDetailImportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkCalendarDetailImportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
