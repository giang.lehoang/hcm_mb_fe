import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {WorkCalendarDetailImportComponent} from "./work-calendar-detail-import/work-calendar-detail-import.component";
import {NzModalModule} from "ng-zorro-antd/modal";
import {TranslateModule} from "@ngx-translate/core";
import {NzFormModule} from "ng-zorro-antd/form";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzUploadModule} from "ng-zorro-antd/upload";
import {NzTableModule} from "ng-zorro-antd/table";

@NgModule({
  imports: [CommonModule, NzModalModule, TranslateModule, NzFormModule, SharedUiMbButtonModule, NzUploadModule, NzTableModule],
  declarations: [WorkCalendarDetailImportComponent],
  exports: [WorkCalendarDetailImportComponent],
})
export class StaffAbsUiWorkCalendarDetailImportModule {}
