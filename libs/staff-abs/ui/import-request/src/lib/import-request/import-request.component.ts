import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { saveAs } from 'file-saver';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import {ErrorImport} from "@hcm-mfe/personal-tax/data-access/models";
import {RequestService} from "@hcm-mfe/staff-abs/data-access/services";
import {beforeUploadFile, getTypeExport} from "@hcm-mfe/shared/common/utils";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {NzUploadFile} from "ng-zorro-antd/upload";
import { ImportFormService } from '@hcm-mfe/shared/data-access/services';

@Component({
  selector: 'import-request',
  templateUrl: './import-request.component.html',
  styleUrls: ['./import-request.component.scss']
})
export class ImportRequestComponent implements OnInit {

  @Input() showContent = false;
  @Input() isRequiredFileAttach = false;
  @Input() closeModalWhenClick = true;
  @Input() leaveType = '';
  @Output() onCloseModal: EventEmitter<boolean> = new EventEmitter<boolean>();

  isModalError = false;
  errorList: ErrorImport[] = [];
  fileList: NzUploadFile[] = [];
  fileAttachList: NzUploadFile[] = [];
  fileName?: string;
  fileImportName = '';
  fileImportSize?: string;
  isSubmitted=false;

  isExistFileImport = false;
  isRequired = false;
  nzWidth: number;
  nzWidthError: number;

  constructor(
    private requestService: RequestService,
    private importFormService: ImportFormService,
    private toastrService: ToastrService,
    private translate: TranslateService
  ) {
    this.nzWidth = window.innerWidth > 767 ? window.innerWidth / 3 : window.innerWidth / 2.5;
    this.nzWidthError = window.innerWidth > 767 ? window.innerWidth / 2 : window.innerWidth / 1.5;
  }

  ngOnInit(): void {
    this.isRequired = this.isRequiredFileAttach;
  }

  doClose(isSearch?: boolean) {
    this.fileList = [];
    this.showContent = false;
    this.onCloseModal.emit(isSearch);
    this.fileImportName = '';
    this.isExistFileImport = false;
  }

  doDownloadTemplate() {
    this.requestService.downloadFileTemplate(this.leaveType).subscribe(res => {
      const arr = res.headers.get("Content-Disposition")?.split(';');
      const fileName: string = arr[arr.length - 1].replace('filename=', '').trim();
      const reportFile = new Blob([res.body], { type: getTypeExport('xlsx') });
      saveAs(reportFile, fileName);
    });
  }

  doCloseModal() {
    this.isModalError = false;
  }

  doDownloadFile() {
    this.importFormService.doDownloadFileByNameFromABS(this.fileName ?? '').subscribe(res => {
      const reportFile = new Blob([res.body], { type: getTypeExport('xlsx') });
      saveAs(reportFile, 'file_error_information.xlsx');
    });
  }

  beforeUpload = (file: NzUploadFile) => {
    this.fileList = [];
    this.fileList =  beforeUploadFile(file, this.fileList, 3000000, this.toastrService, this.translate, 'staffAbs.notification.upload.limitSize')
    this.isExistFileImport = true;
    this.fileImportName = file.name;
    this.fileImportSize = (file.size ?? 1 / 1000000).toFixed(2);
    return false;
  };

  beforeUploadAttach = (file: NzUploadFile) => {
    this.fileAttachList = [];
    this.fileAttachList =  beforeUploadFile(file, this.fileAttachList, 3000000, this.toastrService, this.translate, 'staffAbs.notification.upload.limitSize')
    if (this.isRequiredFileAttach) {
      this.isRequired = false;
    }
    return false;
  };

  doImportFile() {
    // Tạo form data
    const formData = new FormData();
    this.fileList.forEach((file: NzUploadFile) => {
      formData.append('file', file as any);
    });
    this.fileAttachList.forEach((file: NzUploadFile) => {
      formData.append('fileAttachList', file as any);
    });
    formData.append('leaveType', this.leaveType);

    // Tiến hành import
    this.isSubmitted=true;
    this.requestService.import(formData).subscribe(res => {
      this.isSubmitted=false;
      if (res != undefined && res.code != HTTP_STATUS_CODE.OK) {
        this.errorList = res.data.errorList;
        if (this.errorList != undefined && this.errorList.length > 0) {
          this.isModalError = true;
          this.fileName = res.data.errorFile;
          this.toastrService.error(this.translate.instant('staffAbs.notification.upload.error'));
        } else {
          this.isModalError = false;
          this.errorList = [];
          this.fileName = undefined;
          this.toastrService.error(res.message);
        }
      } else {
        this.isModalError = false;
        this.errorList = [];
        this.fileName = undefined;
        this.doClose(true);
        this.toastrService.success(this.translate.instant('staffAbs.notification.upload.success'));
      }
    }, error => {
      this.toastrService.error(error.message);
    });
  }

  doRemoveFile = (): boolean => {
    this.fileList = [];
    this.isExistFileImport = false;
    return true;
  };

  doRemoveFileAttach = (): boolean => {
    this.fileAttachList = [];
    if (this.isRequiredFileAttach) {
      this.isRequired = true;
    }
    return true;
  };
}
