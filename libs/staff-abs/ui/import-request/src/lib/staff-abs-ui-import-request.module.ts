import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ImportRequestComponent} from "./import-request/import-request.component";
import {NzModalModule} from "ng-zorro-antd/modal";
import {NzFormModule} from "ng-zorro-antd/form";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {TranslateModule} from "@ngx-translate/core";
import {NzUploadModule} from "ng-zorro-antd/upload";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzIconModule} from "ng-zorro-antd/icon";
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';

@NgModule({
  imports: [CommonModule, NzModalModule, NzFormModule, SharedUiMbButtonModule, SharedUiLoadingModule, TranslateModule, NzUploadModule, NzTableModule, NzIconModule],
  declarations: [ImportRequestComponent],
  exports: [ImportRequestComponent],
})
export class StaffAbsUiImportRequestModule {}
