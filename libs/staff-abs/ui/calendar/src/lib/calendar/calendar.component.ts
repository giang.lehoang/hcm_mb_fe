
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {
  @Input()
  public month?: string;
  public firstDayOfMonth: any;
  public firstDayOfWeek: any;
  public listWeek: any = [];
  public mapDescriptions: any = {};
  @Input()
  public mapWorkCalendarDetails: any;
  checkData = false;
  constructor(public actr: ActivatedRoute, public translateService: TranslateService) {
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.firstDayOfMonth = moment('01/' + this.month, 'DD/MM/YYYY');
    this.firstDayOfWeek = this.startOfWeek(moment('01/' + this.month, 'DD/MM/YYYY'));
    const numOfDays = 35;
    for (let i = 0; i < numOfDays; i++) {
      const t = this.startOfWeek(moment('01/' + this.month, 'DD/MM/YYYY'));
      let weekNum = i / 7;
      weekNum = parseInt('' + weekNum);
      let current: any = [];
      if (this.listWeek.length > weekNum) {
        current = this.listWeek[weekNum];
      } else {
        this.listWeek[weekNum] = current;
      }
      const item = this.firstDayOfWeek.clone().add(i, 'day');
      this.makeStringWorkDay(item);
      current.push(item);
      this.listWeek[weekNum] = current;
    }

  }
  startOfWeek(momentDate: any) {
    let iDate = momentDate.clone();
    while (iDate.day() != 1) {
      iDate = iDate.add(-1, 'day');
    }
    return iDate;
  }

  makeStringWorkDay(time: any): any {
    const dataString = time.format('DD/MM/YYYY');
    const bean = this.mapWorkCalendarDetails[dataString];
    this.mapDescriptions[dataString] = '';
    if (!bean) {
      return;
    }
    const list = [];
    if (bean.workdayType) {
      list.push(this.translateService.instant('staffAbs.workCalendarDetail.workdayType.' + bean.workdayType));
    }
    if (bean.workdayTime) {
      list.push(this.translateService.instant('staffAbs.workCalendar.workTime.' + bean.workdayTime));
    }
    if (bean.description) {
      list.push(bean.description);
    }
    this.mapDescriptions[dataString] = list.join(', ');
  }
}
