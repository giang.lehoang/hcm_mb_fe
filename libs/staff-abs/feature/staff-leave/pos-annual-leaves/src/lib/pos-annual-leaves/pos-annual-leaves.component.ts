import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, ValidatorFn } from '@angular/forms';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { HttpParams } from '@angular/common/http';
import * as moment from 'moment';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { TranslateService } from '@ngx-translate/core';
import * as FileSaver from 'file-saver';
import {CatalogModel, PosAnnualLeaves} from "@hcm-mfe/staff-abs/data-access/models";
import {MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {MbPositionGroupService, PosAnnualLeavesService} from "@hcm-mfe/staff-abs/data-access/services";
import {ToastrService} from "ngx-toastr";
import {HTTP_STATUS_CODE, Mode, STATUS} from "@hcm-mfe/shared/common/constants";
import {DateValidator} from "@hcm-mfe/shared/common/validators";
import {UrlConstant} from "@hcm-mfe/staff-abs/data-access/common";
import {getTypeExport} from "@hcm-mfe/shared/common/utils";
import {PosAnnualLeavesFormComponent} from "@hcm-mfe/staff-abs/feature/staff-leave/pos-annual-leaves-form";

@Component({
    selector: 'app-pos-annual-leaves',
    templateUrl: './pos-annual-leaves.component.html',
    styleUrls: ['./pos-annual-leaves.component.scss']
})
export class PosAnnualLeavesComponent implements OnInit {
    @ViewChild('actionTmpl', { static: true }) action!: TemplateRef<NzSafeAny>;

    modal?: NzModalRef;

    form: FormGroup = this.fb.group([]);
    positionGroupList: CatalogModel[] = [];
    tableConfig!: MBTableConfig;
    searchResult: PosAnnualLeaves[] = [];

    pagination = new Pagination();
    validators: ValidatorFn[] = [];

    constructor(
        private fb: FormBuilder,
        private mbPositionGroupService: MbPositionGroupService,
        private posAnnualLeavesService: PosAnnualLeavesService,
        private modalService: NzModalService,
        private translate: TranslateService,
        private toastrService: ToastrService
    ) {
    }

    ngOnInit(): void {
        this.initTable();
        this.initForm();
        this.doSearch(1);
    }

    getPositionGroupList() {
        this.mbPositionGroupService.getPositionGroupList().subscribe(data => {
            if (data.code == HTTP_STATUS_CODE.OK) {
                this.positionGroupList = data.data
                    .filter((item: any) => item.flagStatus === STATUS.ACTIVE)
                    .map((item: any) => {return {value: item.pgrId, label: item.pgrName}});
            }
        }, () => {
        });
    }

    initForm() {
        this.getPositionGroupList();
        this.form = this.fb.group({
            positionGroupId: [null],
            fromDate: [null],
            toDate: [null],
        });
        this.validators.push(DateValidator.validateTwoDate('fromDate', 'toDate'));
        this.form.setValidators(this.validators);
    }

    parseOptions() {
        let params = new HttpParams();
        if (this.form.value.positionGroupId)
            params = params.set('pgrId', this.form.value.positionGroupId);
        if (this.form.value.fromDate)
            params = params.set('fromDate', moment(this.form.value.fromDate).format("DD/MM/YYYY"));
        if (this.form.value.toDate)
            params = params.set('toDate', moment(this.form.value.toDate).format("DD/MM/YYYY"));
        return params;
    }

    doSearch(page: number) {
        if (!this.form.invalid) {
            this.pagination.pageNumber = page;
            this.tableConfig.loading = true;
            this.posAnnualLeavesService.searchPosAnnualLeaves(this.parseOptions(), this.pagination.getCurrentPage()).subscribe(res => {
                if (res.code == HTTP_STATUS_CODE.OK) {
                    this.searchResult = res.data.listData;
                    this.tableConfig.total = res.data.count;
                }
                this.tableConfig.loading = false;
            }, () => this.tableConfig.loading = true);
        }
    }

    private initTable(): void {
        this.tableConfig = {
            headers: [
                {
                    title: 'staffAbs.staffLeave.posAnnualLeaves.table.positionGroupName',
                    field: 'pgrName',
                    tdClassList: ['text-left'], thClassList: ['text-center'],
                },
                {
                    title: 'staffAbs.staffLeave.posAnnualLeaves.table.addedDays',
                    field: 'addedDays',
                    tdClassList: ['text-right'], thClassList: ['text-center'],
                },
                {
                    title: 'staffAbs.staffLeave.posAnnualLeaves.table.fromDate',
                    field: 'fromDate',
                    tdClassList: ['text-center'], thClassList: ['text-center'],
                },
                {
                    title: 'staffAbs.staffLeave.posAnnualLeaves.table.toDate',
                    field: 'toDate',
                    tdClassList: ['text-center'], thClassList: ['text-center'],
                },
                {
                    title: 'staffAbs.staffLeave.posAnnualLeaves.table.action',
                    field: 'action',
                    tdTemplate: this.action,
                    tdClassList: ['text-center'], thClassList: ['text-center'],
                }
            ],
            total: 0,
            loading: false,
            size: 'small',
            pageSize: this.pagination.pageSize,
            pageIndex: 1
        };
    }

    doExport() {
        this.posAnnualLeavesService.doExportFile(UrlConstant.EXPORT_REPORT.POS_ANNUAL_LEAVES, this.parseOptions()).subscribe(res => {
            const reportFile = new Blob([res.body], {type: getTypeExport('xlsx')});
            FileSaver.saveAs(reportFile, "BM_Xuat_DS_Ngay_Phep_Bo_Sung_Theo_Nhom_Chuc_danh.xlsx");
        });
    }

    showModalUpdate(posAnnualLeaveId: number, footerTmpl: TemplateRef<{}>) {
        this.modal = this.modalService.create({
            nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
            nzTitle: this.translate.instant('staffAbs.staffLeave.posAnnualLeaves.modal.update'),
            nzContent: PosAnnualLeavesFormComponent,
            nzFooter: footerTmpl,
            nzComponentParams: {
                mode: Mode.EDIT,
                data: posAnnualLeaveId
            },
        });
        this.modal.afterClose.subscribe(result => result?.refresh ? this.doSearch(1): '');
    }

    deleteItem(workdayTypeId: number) {
        this.posAnnualLeavesService.deletePosAnnualService(workdayTypeId).subscribe((res) => {
            if (res.code === HTTP_STATUS_CODE.OK) {
                this.toastrService.success(this.translate.instant('common.notification.deleteSuccess'));
                this.doSearch(1);
            }
        }, error => {
            this.toastrService.error(this.translate.instant('common.notification.deleteSuccess') + ":" + error.message);
        });
    }

    showModalAdd(footerTmpl: TemplateRef<{}>) {
        this.modal = this.modalService.create({
            nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
            nzTitle: this.translate.instant('staffAbs.staffLeave.posAnnualLeaves.modal.add'),
            nzContent: PosAnnualLeavesFormComponent,
            nzFooter: footerTmpl
        });
        this.modal.afterClose.subscribe(result => result?.refresh ? this.doSearch(1) : '');
    }

}
