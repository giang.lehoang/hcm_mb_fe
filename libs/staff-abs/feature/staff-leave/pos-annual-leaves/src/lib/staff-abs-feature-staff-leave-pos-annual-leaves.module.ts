import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PosAnnualLeavesComponent} from "./pos-annual-leaves/pos-annual-leaves.component";
import {NzFormModule} from "ng-zorro-antd/form";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {NzPopconfirmModule} from "ng-zorro-antd/popconfirm";
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [CommonModule, NzFormModule, ReactiveFormsModule, FormsModule, SharedUiMbSelectModule, TranslateModule,
    SharedUiMbDatePickerModule, SharedUiMbButtonModule, SharedUiMbTableWrapModule, SharedUiMbTableModule, NzPopconfirmModule,
    RouterModule.forChild([
      {
        path: '',
        component: PosAnnualLeavesComponent
      }
    ])
  ],
  declarations: [PosAnnualLeavesComponent],
  exports: [PosAnnualLeavesComponent],
})
export class StaffAbsFeatureStaffLeavePosAnnualLeavesModule {}
