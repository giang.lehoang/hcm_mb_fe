import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LayoutLeaveComponent} from "./layout-leave/layout-leave.component";
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [CommonModule, RouterModule],
  declarations: [LayoutLeaveComponent],
  exports: [LayoutLeaveComponent],
})
export class StaffAbsFeatureStaffLeaveLayoutLeaveModule {}
