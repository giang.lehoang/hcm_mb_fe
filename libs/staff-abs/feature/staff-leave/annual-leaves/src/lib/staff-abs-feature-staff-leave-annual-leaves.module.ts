import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AnnualLeavesComponent} from "./annual-leaves/annual-leaves.component";
import {NzFormModule} from "ng-zorro-antd/form";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiOrgDataPickerModule} from "@hcm-mfe/shared/ui/org-data-picker";
import {NzDatePickerModule} from "ng-zorro-antd/date-picker";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzTableModule} from "ng-zorro-antd/table";
import {SharedPipesNumberFilterModule} from "@hcm-mfe/shared/pipes/number-filter";
import {RouterModule} from "@angular/router";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import { SharedUiMbTableModule } from '@hcm-mfe/shared/ui/mb-table';
import { SharedUiMbTableWrapModule } from '@hcm-mfe/shared/ui/mb-table-wrap';
import { SharedUiMbSelectCheckAbleModule } from '@hcm-mfe/shared/ui/mb-select-check-able';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';

@NgModule({
    imports: [CommonModule, SharedUiMbSelectCheckAbleModule, NzFormModule, ReactiveFormsModule, FormsModule, SharedUiMbInputTextModule, TranslateModule,
        SharedUiMbTableWrapModule, SharedUiMbTableModule, SharedUiMbSelectModule, SharedUiOrgDataPickerModule, NzDatePickerModule, SharedUiMbButtonModule, NzTableModule, SharedPipesNumberFilterModule, NzPopconfirmModule,
        RouterModule.forChild([
            {
                path: '',
                component: AnnualLeavesComponent
            }
        ]), SharedUiLoadingModule
    ],
  declarations: [AnnualLeavesComponent],
  exports: [AnnualLeavesComponent],
})
export class StaffAbsFeatureStaffLeaveAnnualLeavesModule {}
