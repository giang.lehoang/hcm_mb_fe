import { Component, Injector, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { HttpParams } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import * as FileSaver from 'file-saver';
import {FunctionCode, Scopes} from "@hcm-mfe/shared/common/enums";
import {AppFunction, Pagination} from "@hcm-mfe/shared/data-access/models";
import {AnnualLeaves, CatalogModel} from "@hcm-mfe/staff-abs/data-access/models";
import {AnnualLeaveService, SearchFormService} from "@hcm-mfe/staff-abs/data-access/services";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {Constant, SearchBaseComponent, UrlConstant} from "@hcm-mfe/staff-abs/data-access/common";
import {getTypeExport} from "@hcm-mfe/shared/common/utils";
import { SessionService } from '@hcm-mfe/shared/common/store';

@Component({
    selector: 'app-annual-leaves',
    templateUrl: './annual-leaves.component.html',
    styleUrls: ['./annual-leaves.component.scss']
})
export class AnnualLeavesComponent extends SearchBaseComponent  implements OnInit {
    scope = Scopes.VIEW;
    functionCode = FunctionCode.ABS_ANNUAL_LEAVES;
    objFunction: AppFunction;
    form?: FormGroup = this.fb?.group([]);
    empTypeList: CatalogModel[] = [];
    listData: AnnualLeaves[] = [];
    totalRecord?: number;
    pageIndex?: number;
    pageSize?: number;
    tableLoading?: boolean;

    constructor(
        injector: Injector,
        private searchFormService: SearchFormService,
        private annualLeaveService: AnnualLeaveService,
        public sessionService: SessionService,
    ) {
        super(injector);
        this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ABS_ANNUAL_LEAVES}`);
    }

    ngOnInit(): void {
        this.initForm();
        this.initTable();
        this.doSearch(1);
    }

    initTable() {
        this.tableConfig = {
            headers: [
            {
                title: 'staffAbs.staffLeave.annualLeaves.table.fromDate',
                field: 'fromDate',
                width: 30,
                thClassList: ['text-center'],
                fixed:true,
                fixedDir: 'left'
                },
                {
                    title: 'staffAbs.staffLeave.annualLeaves.table.toDate',
                    field: 'toDate',
                    width: 30,
                    thClassList: ['text-center'],
                    fixed:true,
                    fixedDir: 'left'
                    },
              {
                title: 'staffAbs.label.staffCode',
                field: 'employeeCode',
                width: 50,
                thClassList: ['text-center'],
                fixed:true,
                fixedDir: 'left'
              },
              {
                title: 'staffAbs.label.employeeName',
                field: 'fullName',
                width: 60,
                fixed:true,
                fixedDir: 'left'
              },
              {
                title: 'staffAbs.staffLeave.annualLeaves.table.seniority',
                field: 'seniority',
                width: 30,
                fixedDir: 'left'
              },
              {
                title: 'staffAbs.staffLeave.annualLeaves.table.workingMonths',
                field: 'workingMonths',
                show:false,
                width: 30,
                fixedDir: 'left'
              },
              {
                title: 'staffAbs.staffLeave.annualLeaves.table.unpaidLeaveMonths',
                field: 'unpaidLeaveMonths',
                show:false,
                width: 30,
                fixedDir: 'left'
              },
              {
                title: 'staffAbs.staffLeave.annualLeaves.table.workAccidentMonths',
                field: 'workAccidentMonths',
                show:false,
                width: 30,
                fixedDir: 'left'
              },
              {
                title: 'staffAbs.staffLeave.annualLeaves.table.sickLeaveMonths',
                field: 'sickLeaveMonths',
                show:false,
                width: 30,
                fixedDir: 'left'
              },
              {
                title: 'staffAbs.staffLeave.annualLeaves.table.remainDaysLastYear',
                field: 'totalDaysLastYear',
                width: 30,
                fixedDir: 'left'
              },
              {
                title: 'staffAbs.staffLeave.annualLeaves.table.dayUsedLastYear',
                field: 'useSwitchedDays',
                width: 30,
                fixedDir: 'left'
              },
              {
                title: 'staffAbs.staffLeave.annualLeaves.table.annualDays',
                field: 'annualDays',
                show:false,
                width: 30,
                fixedDir: 'left'
              },
              {
                title: 'staffAbs.staffLeave.annualLeaves.table.posBonusDays',
                field: 'posBonusDays',
                show:false,
                width: 30,
                fixedDir: 'left'
              },
              {
                title: 'staffAbs.staffLeave.annualLeaves.table.seniorBonusDays',
                field: 'seniorBonusDays',
                show:false,
                width: 30,
                fixedDir: 'left'
              },
              {
                title: 'staffAbs.staffLeave.annualLeaves.table.totalDays',
                field: 'totalDaysInYear',
                width: 30,
                fixedDir: 'left'
              },
              {
                title: 'staffAbs.staffLeave.annualLeaves.table.remainDays',
                field: 'remainDaysInYear',
                width: 30,
                fixed:true,
                fixedDir: 'right'
              },
            ],
            total: 0,
            needScroll: true,
            loading: false,
            size: 'small',
            pageSize: this.pagination.pageSize,
            pageIndex: 1,
            widthIndex: '15px'
          };
    }

    initForm() {
        this.getEmpTypeList();
        this.form = this.fb?.group({
            employeeCode: [null],
            fullName: [null],
            employeeType: [null],
            organizationId: [null],
            empStatus: [null],
            year: [new Date(), Validators.required],
        });
    }

    getEmpTypeList(){
        this.searchFormService.getEmpTypeList().subscribe(data => {
            if (data.code == HTTP_STATUS_CODE.OK) {
                this.empTypeList = data.data;
            }
        });
    }

    parseOptions() {
        let params = new HttpParams();
        if (this.form?.value.employeeCode)
            params = params.set('employeeCode', this.form.value.employeeCode);
        if (this.form?.value.fullName)
            params = params.set('fullName', this.form.value.fullName);
        if (this.form?.value.employeeType)
            params = params.set('empTypeCode', this.form.value.employeeType);
        if (this.form?.value.empStatus)
            params = params.set('empStatus', this.form.value.empStatus.join(','));
        if (this.form?.value.organizationId)
            params = params.set('organizationId', this.form.value.organizationId.orgId);
        if (this.form?.value.year)
            params = params.set('year', ~~moment(new Date(this.form.value.year)).format('yyyy'));
        return params;
    }

    override doSearch(page: number) {
        this.isLoadingPage = true;
        this.pagination.pageNumber = page ?? 1;
        this.tableConfig.pageIndex = page ?? 1;
        this.annualLeaveService.searchAnnualLeaves(this.parseOptions(), this.pagination.getCurrentPage()).subscribe({
            next: (res) => {
                if (res.code === HTTP_STATUS_CODE.OK) {
                    this.searchResult = res.data.listData as AnnualLeaves[];
                    this.listData = res.data.listData;
                    this.totalRecord = res.data.count;
                    this.tableConfig.total = res.data.count;
                }
                this.isLoadingPage = false;
            },
            error: (err) => {
                this.isLoadingPage = true;
            }
        });
    }

    onPageIndexChange($event: number) {
        this.doSearch($event);
    }

    doExportData() {
        this.isLoadingPage = true;
        this.annualLeaveService.doExportFile(UrlConstant.EXPORT_REPORT.ANNUAL_LEAVES, this.parseOptions()).subscribe(res => {
            const reportFile = new Blob([res.body], {type: getTypeExport('xlsx')});
            FileSaver.saveAs(reportFile, "BM_Xuat_DS_Ngay_phep.xlsx");
            this.isLoadingPage = false;
        });
    }

    calculateAnnualLeave() {
      if (this.form?.invalid) {
        return;
      }
      const year = ~~moment(new Date(this.form?.value?.year)).format('yyyy');
      this.isLoadingPage = true;
      this.annualLeaveService.calculateAnnualLeave(year).subscribe(res => {
        if(res.code === HTTP_STATUS_CODE.OK) {
          this.doSearch(1);
          this.isLoadingPage = false;
          this.toastrService?.success(res.message);
        } else {
          this.toastrService?.error(res.message);
        }
      }, () => {
        this.isLoadingPage = false;
      });
    }

}
