import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import {HTTP_STATUS_CODE, Mode, STATUS} from "@hcm-mfe/shared/common/constants";
import {CatalogModel, PosAnnualLeaves} from "@hcm-mfe/staff-abs/data-access/models";
import {MbPositionGroupService, PosAnnualLeavesService} from "@hcm-mfe/staff-abs/data-access/services";
import {UrlConstant} from "@hcm-mfe/staff-abs/data-access/common";
import {DateValidator} from "@hcm-mfe/shared/common/validators";

@Component({
    selector: 'app-pos-annual-leaves-form',
    templateUrl: './pos-annual-leaves-form.component.html',
    styleUrls: ['./pos-annual-leaves-form.component.scss']
})
export class PosAnnualLeavesFormComponent implements OnInit {
    mode = Mode.ADD;
    form: FormGroup = this.fb.group([]);
    positionGroupList: CatalogModel[] = [];
    isSubmitted = false;
    data?: number;

    validators: ValidatorFn[] = [];

    constructor(
        private fb: FormBuilder,
        private mbPositionGroupService: MbPositionGroupService,
        private posAnnualLeavesService: PosAnnualLeavesService,
        private toastrService: ToastrService,
        private modalRef: NzModalRef,
        private translate: TranslateService,
    ) {
    }

    ngOnInit(): void {
        this.initForm();
        if (this.mode == Mode.EDIT) {
            this.posAnnualLeavesService.getPosAnnualLeaves(UrlConstant.GET.POS_ANNUAL_LEAVES, this.data).subscribe({
                next: (res) => {
                    if (res.code == HTTP_STATUS_CODE.OK) {
                        this.form.patchValue(res.data);
                    }
                },
                error: () => {

                }
            });
        }
    }

    initForm() {
        this.getPositionGroupList();
        this.form = this.fb.group({
            pgrId: [null, Validators.required],
            addedDays: [null, Validators.required],
            fromDate: [null, Validators.required],
            toDate: [null]
        }, {
            validators: [
                DateValidator.validateRangeDate('fromDate', 'toDate', 'rangeDateError')
            ]
        });
    }

    getPositionGroupList() {
        this.mbPositionGroupService.getPositionGroupList().subscribe(data => {
            if (data.code == HTTP_STATUS_CODE.OK) {
                this.positionGroupList = data.data
                    .filter((item: any) => item.flagStatus === STATUS.ACTIVE)
                    .map((item: any) => {return {value: item.pgrId, label: item.pgrName}});
            }
        }, () => {
        });
    }

    save() {
        this.isSubmitted = true;
        if (this.form.valid) {
            let data: PosAnnualLeaves;
            data = this.formToEntity();
            if (this.mode === Mode.EDIT) {
                data = { ...data, posAnnualLeaveId: this.data };
            }
            this.posAnnualLeavesService.savePosAnnualLeaves(UrlConstant.SAVE.POS_ANNUAL_LEAVES, data).subscribe(res => {
                if (res.code === HTTP_STATUS_CODE.OK) {
                    this.toastrService.success(this.translate.instant(this.mode === Mode.EDIT ? 'common.notification.updateSuccess' : 'common.notification.addSuccess'));
                    this.modalRef.close({refresh:true});
                } else
                    this.toastrService.error(this.translate.instant(this.mode === Mode.EDIT ? 'common.notification.updateError' : 'common.notification.addError') + ": " + res?.message);
            }, error => {
                this.toastrService.error(this.translate.instant(this.mode === Mode.EDIT ? 'common.notification.updateError' : 'common.notification.addError') + ": " + error.message);
            })
        }
    }

    private formToEntity(): PosAnnualLeaves {
        return {
            pgrId: this.form.value.pgrId ? ~~this.form.value.pgrId : undefined,
            addedDays: this.form.value.addedDays ? ~~this.form.value.addedDays : undefined,
            fromDate: this.form.value.fromDate ? moment(this.form.value.fromDate).format("DD/MM/YYYY") : undefined,
            toDate: this.form.value.toDate ? moment(this.form.value.toDate).format("DD/MM/YYYY") : undefined
        }
    }

}
