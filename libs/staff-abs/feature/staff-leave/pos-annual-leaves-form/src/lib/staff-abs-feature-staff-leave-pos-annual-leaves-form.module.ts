import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PosAnnualLeavesFormComponent} from "./pos-annual-leaves-form/pos-annual-leaves-form.component";
import {NzFormModule} from "ng-zorro-antd/form";
import {ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedDirectivesNumberInputModule} from "@hcm-mfe/shared/directives/number-input";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";

@NgModule({
  imports: [CommonModule, NzFormModule, ReactiveFormsModule, SharedUiMbSelectModule, TranslateModule, SharedUiMbInputTextModule, SharedDirectivesNumberInputModule, SharedUiMbDatePickerModule],
  declarations: [PosAnnualLeavesFormComponent],
  exports: [PosAnnualLeavesFormComponent],
})
export class StaffAbsFeatureStaffLeavePosAnnualLeavesFormModule {}
