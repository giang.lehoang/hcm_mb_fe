import {RouterModule, Routes} from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: 'category',
    data: {
      pageName: 'staffAbs.breadcrumb.mng',
      breadcrumb: 'staffAbs.breadcrumb.mng'
    },
    children: [
      {
        path: 'leave-reason',
        data: {
          pageName: 'staffAbs.leaveReason.pageName',
          breadcrumb: 'staffAbs.leaveReason.pageName'
        },
        loadChildren: () => import('@hcm-mfe/staff-abs/feature/category/leave-reason').then((m) => m.StaffAbsFeatureCategoryLeaveReasonModule)
      },
      {
        path: 'work-calendar-bk',
        data: {
          pageName: 'staffAbs.workCalendar.pageName',
          breadcrumb: 'staffAbs.workCalendar.pageName'
        },
        loadChildren: () => import('@hcm-mfe/staff-abs/feature/category/work-calendar').then((m) => m.StaffAbsFeatureCategoryWorkCalendarModule)
      },
      {
        path: 'work-calendar',
        data: {
          pageName: 'staffAbs.workCalendarPage.pageName',
          breadcrumb: 'staffAbs.workCalendarPage.breadcrumb'
        },
        loadChildren: () => import('@hcm-mfe/staff-abs/feature/category/work-calendar-page').then((m) => m.StaffAbsFeatureCategoryWorkCalendarPageModule)
      },
      {
        path: 'work-calendar-detail',
        data: {
          pageName: 'staffAbs.workCalendarDetail.pageName',
          breadcrumb: 'staffAbs.workCalendarDetail.pageName'
        },
        loadChildren: () => import('@hcm-mfe/staff-abs/feature/category/work-calendar-detail').then((m) => m.StaffAbsFeatureCategoryWorkCalendarDetailModule)
      },
      {
        path: 'approval-flow',
        data: {
          pageName: 'staffAbs.approvalFlow.pageName',
          breadcrumb: 'staffAbs.approvalFlow.pageName'
        },
        loadChildren: () => import('@hcm-mfe/staff-abs/feature/category/approval-flow-index').then((m) => m.StaffAbsFeatureCategoryApprovalFlowIndexModule)
      }
    ]
  },
  {
    path: 'timekeeping-management',
    data: {
      pageName: 'staffAbs.breadcrumb.timekeepingManagement',
      breadcrumb: 'staffAbs.breadcrumb.timekeepingManagement'
    },
    children: [
      {
        path: 'requests-leave',
        data: {
          pageName: 'staffAbs.breadcrumb.requestsLeave',
          breadcrumb: 'staffAbs.breadcrumb.requestsLeave'
        },
        loadChildren: () => import('@hcm-mfe/staff-abs/feature/timekeeping/requests-leave').then((m) => m.StaffAbsFeatureTimekeepingRequestsLeaveModule),
      },
      {
        path: 'work-early',
        data: {
          pageName: 'staffAbs.breadcrumb.workEarly',
          breadcrumb: 'staffAbs.breadcrumb.workEarly'
        },
        loadChildren: () => import('@hcm-mfe/staff-abs/feature/timekeeping/work-early').then((m) => m.StaffAbsFeatureTimekeepingWorkEarlyModule),
      },
      {
        path: 'work-travel',
        data: {
          pageName: 'staffAbs.breadcrumb.workTravel',
          breadcrumb: 'staffAbs.breadcrumb.workTravel'
        },
        loadChildren: () => import('@hcm-mfe/staff-abs/feature/timekeeping/work-travel').then((m) => m.StaffAbsFeatureTimekeepingWorkTravelModule),
      },
      {
        path: 'timekeeping-ot',
        data: {
          pageName: 'staffAbs.breadcrumb.timekeepingOT',
          breadcrumb: 'staffAbs.breadcrumb.timekeepingOT'
        },
        loadChildren: () => import('@hcm-mfe/staff-abs/feature/timekeeping/timekeeping-ot').then((m) => m.StaffAbsFeatureTimekeepingTimekeepingOtModule),
      },
      {
        path: 'timekeeping-saturday',
        data: {
          pageName: 'staffAbs.breadcrumb.timekeepingSaturday',
          breadcrumb: 'staffAbs.breadcrumb.timekeepingSaturday'
        },
        loadChildren: () => import('@hcm-mfe/staff-abs/feature/timekeeping/timekeeping-saturday').then((m) => m.StaffAbsFeatureTimekeepingTimekeepingSaturdayModule),
      },
      {
        path: 'attending-training',
        data: {
          pageName: 'staffAbs.breadcrumb.attendingTraining',
          breadcrumb: 'staffAbs.breadcrumb.attendingTraining'
        },
        loadChildren: () => import('@hcm-mfe/staff-abs/feature/timekeeping/attending-training').then((m) => m.StaffAbsFeatureTimekeepingAttendingTrainingModule),
      },
    ]
  },
  {
    path: 'staff-leave',
    data: {
      pageName: 'staffAbs.pageName.staffLeave',
      breadcrumb: 'staffAbs.breadcrumb.staffLeave'
    },
    children: [
      {
        path: 'annual-leaves',
        data: {
          pageName: 'staffAbs.pageName.annualLeaves',
          breadcrumb: 'staffAbs.breadcrumb.annualLeaves'
        },
        loadChildren: () => import('@hcm-mfe/staff-abs/feature/staff-leave/annual-leaves').then((m) => m.StaffAbsFeatureStaffLeaveAnnualLeavesModule),
      },
      {
        path: 'pos-annual-leaves',
        data: {
          pageName: 'staffAbs.pageName.posAnnualLeaves',
          breadcrumb: 'staffAbs.breadcrumb.posAnnualLeaves'
        },
        loadChildren: () => import('@hcm-mfe/staff-abs/feature/staff-leave/pos-annual-leaves').then((m) => m.StaffAbsFeatureStaffLeavePosAnnualLeavesModule),
      }
    ]
  },
  {
    path: 'workday-type',
    data: {
      pageName: 'staffAbs.pageName.workdayType',
      breadcrumb: 'staffAbs.breadcrumb.workdayType'
    },
    loadChildren: () => import('@hcm-mfe/staff-abs/feature/timekeeping/workday-types').then((m) => m.StaffAbsFeatureTimekeepingWorkdayTypesModule),
  },
  {
    path: 'timekeeping',
    data: {
      pageName: 'staffAbs.pageName.timekeeping',
      breadcrumb: 'staffAbs.breadcrumb.timekeeping'
    },
    loadChildren: () => import('@hcm-mfe/staff-abs/feature/timekeeping/timekeeping-search').then((m) => m.StaffAbsFeatureTimekeepingTimekeepingSearchModule),
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class StaffAbsFeatureShellRoutingModule {}
