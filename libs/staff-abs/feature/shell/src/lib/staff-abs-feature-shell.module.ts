import { NgModule } from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {StaffAbsFeatureShellRoutingModule} from "./staff-abs-feature-shell.routing.module";
import {FormatCurrencyPipe} from "@hcm-mfe/shared/pipes/format-currency";
import {NzModalModule} from "ng-zorro-antd/modal";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
  imports: [CommonModule,
    NzModalModule,
    FormsModule, ReactiveFormsModule,
    StaffAbsFeatureShellRoutingModule
  ],
  providers: [DatePipe, FormatCurrencyPipe]
})
export class StaffAbsFeatureShellModule {}
