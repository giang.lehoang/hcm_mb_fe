import { ChangeDetectorRef, Component, Injector, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ValidatorFn } from '@angular/forms';
import * as moment from 'moment';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import {Constant, RequestUtils, SearchBaseComponent} from "@hcm-mfe/staff-abs/data-access/common";
import {FunctionCode, Scopes} from "@hcm-mfe/shared/common/enums";
import {AppFunction, Category, MBTableConfig} from "@hcm-mfe/shared/data-access/models";
import {CommonUtils, StaffInfoService, ValidateService} from "@hcm-mfe/shared/core";
import {RequestService, DownloadFileAttachService} from "@hcm-mfe/staff-abs/data-access/services";
import {TimekeepingOtFormComponent} from "@hcm-mfe/staff-abs/feature/timekeeping/timekeeping-ot-form";
import {DateValidator} from "@hcm-mfe/shared/common/validators";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import { SessionService } from '@hcm-mfe/shared/common/store';
import {getTypeExport} from "@hcm-mfe/shared/common/utils";
import {saveAs} from "file-saver";
import { NzModalService } from 'ng-zorro-antd/modal';
import { ConfirmBackdateComponent, ConfirmBackdateRejectComponent } from '@hcm-mfe/staff-abs/feature/timekeeping/confirm-backdate';
import { forkJoin } from 'rxjs';


@Component({
  selector: 'app-timekeeping-ot',
  templateUrl: './timekeeping-ot.component.html',
  styleUrls: ['./timekeeping-ot.component.scss']
})
export class TimekeepingOTComponent extends SearchBaseComponent implements OnInit {
  @ViewChild('actionTmpl', { static: true }) action!: TemplateRef<NzSafeAny>;
  @ViewChild('filesTmpl', { static: true }) filesTmpl!: TemplateRef<NzSafeAny>;
  @ViewChild('statusTmpl', { static: true }) status!: TemplateRef<NzSafeAny>;

  scope = Scopes.VIEW;
  functionCode = FunctionCode.ABS_TIMEKEEPINGS;
  objFunction: AppFunction;
  listEmpType: Category[] = [];
  formSearchConfig = {
    leaveType: [Constant.LEAVE_TYPE.OT],
    status: [null],
    empTypeCode: [null],
    empStatus: [null],
    employeeCode: [''],
    fullName: [''],
    fromTime: [''],
    toTime: [''],
    keySearch: [''],
    organizationId: [''],
    pageName: [Constant.PAGE_NAME.ADMIN],
    listStatus: [[]]
  }

  OT = Constant.LEAVE_TYPE.OT;
  REQUEST_STATUS_APPROVAL_DATA_SOURCE = Constant.REQUEST_STATUS_APPROVAL_DATA_SOURCE;
  REQUEST_STATUS_DATASOURCE = Constant.REQUEST_STATUS_DATASOURCE;
  REQUEST_STATUS = Constant.REQUEST_STATUS;
  NOT_ALLOW_EDIT = Constant.NOT_ALLOW_EDIT;
  NOT_ALLOW_DELETE = Constant.NOT_ALLOW_DELETE;
  TAG = Constant.TAG;
  isImportData = false;

  validators: ValidatorFn[] = []

  fileNameExport = Constant.FILE_NAME_EXPORT.OT

  constructor(
    injector: Injector,
    public validateService: ValidateService,
    private workEarlyService: RequestService,
    private cdr: ChangeDetectorRef,
    private staffInfoService: StaffInfoService,
    public sessionService: SessionService,
    public downloadFileAttachService: DownloadFileAttachService,
    private modalServices: NzModalService,
  ) {
    super(injector);
    this.modalComponent = TimekeepingOtFormComponent;
    this.setMainService(workEarlyService);
    this.setValidators();
    this.buildFormSearch(this.formSearchConfig, this.validators);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ABS_TIMEKEEPINGS}`);
  }

  public setValidators() {
    this.validators.push(DateValidator.validateTwoDate('fromTime', 'toTime', 'greaterAndEqual'));
  }

  ngOnInit(): void {
    this.initTable();
    forkJoin([
      this.workEarlyService.getTotalRequestBackdate(Constant.LEAVE_TYPE.OT)
    ]).subscribe(
      ([countBackdateResponse]) =>{
        this.beforeSearch(countBackdateResponse?.data);
      },() => {
        this.toastrService?.error(this.translate?.instant('common.notification.error'));
      }
    )
    // this.doSearch();
    this.getListEmployeeType();
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'staffAbs.label.staffCode',
          field: 'employeeCode',
          width: 120,
          thClassList: ['text-center'],
          fixedDir: 'left'
        },
        {
          title: 'staffAbs.label.employeeName',
          field: 'fullName',
          width: 180,
          fixedDir: 'left'
        },
        {
          title: 'staffAbs.timekeepingManagement.OT.timekeepingDate',
          field: 'fromTime',
          pipe: "date: dd/MM/yyyy",
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 120
        },
        {
          title: 'staffAbs.timekeepingManagement.fromTime',
          field: 'fromTime',
          pipe: "date: HH:mm",
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 100
        },
        {
          title: 'staffAbs.timekeepingManagement.toTime',
          field: 'toTime',
          pipe: "date: HH:mm",
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 100
        },
        {
          title: 'staffAbs.timekeepingManagement.OT.content',
          field: 'content',
          tdClassList: ['text-left'],
          thClassList: ['text-center'],
        },
        {
          title: 'staffAbs.timekeepingManagement.organizationName',
          field: 'organizationName',
          tdClassList: ['text-left'],
          thClassList: ['text-center'],
        },
        {
          title: 'staffAbs.label.file',
          field: 'files',
          tdTemplate: this.filesTmpl,
          tdClassList: ['text-left'],
          thClassList: ['text-center']
        },
        {
          title: 'staffAbs.label.createdBy',
          field: 'createdBy',
          show: false,
          tdClassList: ['text-left'],
          thClassList: ['text-center']
        },
        {
          title: 'staffAbs.label.createDate',
          field: 'createDate',
          show: false,
          tdClassList: ['text-left'],
          thClassList: ['text-center']
        },
        {
          title: 'staffAbs.label.lastUpdatedBy',
          field: 'lastUpdatedBy',
          show: false,
          tdClassList: ['text-left'],
          thClassList: ['text-center']
        },
        {
          title: 'staffAbs.label.lastUpdateDate',
          field: 'lastUpdateDate',
          show: false,
          tdClassList: ['text-left'],
          thClassList: ['text-center']
        },
        {
          title: 'staffAbs.timekeepingManagement.approvalStatus',
          field: 'status',
          tdTemplate: this.status,
          tdClassList: ['text-nowrap', 'text-center'],
          width: 170,
          fixed: window.innerWidth > 1024,
          fixedDir: 'right'
        },
        {
          title: '',
          field: 'action',
          tdClassList: ['text-nowrap', 'text-center'],
          thClassList: ['text-nowrap', 'text-center'],
          width: 100,
          tdTemplate: this.action,
          fixed: window.innerWidth > 1024,
          show: this.objFunction.edit,
          fixedDir: 'right'
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  public override doSearch(pageNumber?: number): void {
    this.pagination.pageNumber = pageNumber ?? 1;
    this.tableConfig.pageIndex = pageNumber ? this.tableConfig.pageIndex: 1;
    const params = this.formSearch ? {...this.formSearch.value} : {};

    if (params.fromTime) {
      params.fromTime = moment(new Date(params.fromTime)).format('DD/MM/yyyy HH:mm:ss');
    }
    if (params.toTime) {
      params.toTime = moment(new Date(params.toTime)).format('DD/MM/yyyy HH:mm:ss');
    }
    if (params.organizationId) {
      params.organizationId = params.organizationId.orgId;
    }
    if(params.empStatus){
      params.empStatus = params.empStatus.join(',');
    }
    this.isLoadingPage = true;
    this.workEarlyService.search({ ...params, ...this.pagination.getCurrentPage(), leaveType: Constant.LEAVE_TYPE.OT })
      .subscribe(res => {
        if (CommonUtils.isSuccessRequest(res)) {
          this.searchResult = res.data.listData as Request[];
          this.searchResult = this.searchResult.map((request: Request) => (RequestUtils.filterGetOneRequestLeave(request)))
          this.tableConfig.total = res.data.count;
        }
        this.resultList = res;
        this.isLoadingPage = false;
      },
        () => {
          this.isLoadingPage = false;
        }
      );
  }

  doCloseImport(isSearch: boolean) {
    this.isImportData = false;
    isSearch && this.doSearch(this.pagination.pageNumber);
  }

  doOpenImport() {
    this.isImportData = true;
  }
  getListEmployeeType() {
    this.staffInfoService.getCatalog(Constant.CATALOGS.DOI_TUONG_CV).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) this.listEmpType = res.data;
      });
  }

  downloadFile(file: NzSafeAny) {
    this.downloadFileAttachService.doDownloadAttachFile(Number(file.docId)).subscribe(res => {
      const reportFile = new Blob([res], {type: getTypeExport(file.fileName?.split(".").pop() ?? '')});
      saveAs(reportFile, file.fileName);
    });
  }

  getStatus(value: number) {
    return this.REQUEST_STATUS_DATASOURCE.find(status => status.value === value)?.label;
  }

  doApproveBackdate(requestId: number) {
    this.isLoadingPage = true;
    this.workEarlyService.approveBackdateRequests(requestId).subscribe(res => {
      if (CommonUtils.isSuccessRequest(res)) {
        this.doSearch(1);
        this.isLoadingPage = false;
        this.toastrService?.success(this.translate?.instant('staffAbs.timekeepingManagement.confirmBackdate.approvalSuccess'));
      } else {
        this.toastrService?.success(this.translate?.instant('staffAbs.timekeepingManagement.confirmBackdate.approvalError'));
      }
    }, () => {
      this.isLoadingPage = false;
    });
  }

  beforeSearch(countBackdate: number) {
    if (countBackdate && countBackdate > 0) {
      // show popup
      this.formSearch?.controls['listStatus'].setValue([Constant.REQUEST_STATUS.WAIT_APPROVE_BACKDATE
        ,Constant.REQUEST_STATUS.CANCEL_BACKDATE]);
      this.doSearch(1);

      this.modal = this.modalServices.create({
        nzTitle: '',
        nzStyle: {
          width: '414px',
          height: '223px',
        },
        nzContent: ConfirmBackdateComponent,
        nzComponentParams: {
          countBackdate: countBackdate
        },
        nzFooter: null,
        nzClosable: false
      });
      // this.modal.afterClose.subscribe(() => {});
    } else {
      this.doSearch(1);
    }
  }

  doPrepareReject(requestId: number, footerTmpl: TemplateRef<NzSafeAny>) {
    this.modal = this.modalServices.create(
      {
        nzWidth: 500,
        nzTitle: this.translate?.instant('staffAbs.timekeepingManagement.confirmBackdate.rejectTitle'),
        nzContent: ConfirmBackdateRejectComponent,
        nzComponentParams: {
          data: {
            requestId: requestId,
          }
        },
        nzFooter: footerTmpl
      }
    );
    this.subs.push(this.modal.afterClose.subscribe(result => {
      if (result?.refresh) {
        // this.toastrService?.success(this.translate?.instant('staffAbs.timekeepingManagement.confirmBackdate.rejectedSuccess'));
        this.doSearch(1);
      }
    }));
  }
}
