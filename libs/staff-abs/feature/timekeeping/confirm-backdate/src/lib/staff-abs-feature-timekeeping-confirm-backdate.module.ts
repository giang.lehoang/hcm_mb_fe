import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NzFormModule} from "ng-zorro-antd/form";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiOrgDataPickerModule} from "@hcm-mfe/shared/ui/org-data-picker";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {NzPopconfirmModule} from "ng-zorro-antd/popconfirm";
import {StaffAbsUiImportRequestModule} from "@hcm-mfe/staff-abs/ui/import-request";
import {NzTagModule} from "ng-zorro-antd/tag";
import {NzButtonModule} from "ng-zorro-antd/button";
import { SharedUiMbSelectCheckAbleModule } from '@hcm-mfe/shared/ui/mb-select-check-able';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { ConfirmBackdateComponent } from './confirm-backdate/confirm-backdate.component';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { ConfirmBackdateRejectComponent } from './confirm-backdate-reject/confirm-backdate-reject.component';

@NgModule({
    imports: [CommonModule, NzFormModule, SharedUiLoadingModule, ReactiveFormsModule, FormsModule, SharedUiMbSelectModule, SharedUiMbSelectCheckAbleModule, TranslateModule,
        SharedUiMbInputTextModule, SharedUiOrgDataPickerModule, SharedUiMbDatePickerModule, SharedUiMbButtonModule,
        SharedUiMbTableWrapModule, SharedUiMbTableModule, NzPopconfirmModule, StaffAbsUiImportRequestModule, NzTagModule,
        NzToolTipModule, NzButtonModule
    ],
  declarations: [ConfirmBackdateComponent, ConfirmBackdateRejectComponent],
  exports: [],
})
export class StaffAbsFeatureTimekeepingConfirmBackdateModule {}
