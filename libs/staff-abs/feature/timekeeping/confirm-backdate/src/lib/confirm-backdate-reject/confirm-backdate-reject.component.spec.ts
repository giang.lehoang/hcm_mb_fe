import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmBackdateRejectComponent } from './confirm-backdate-reject.component';

describe('ConfirmBackdateRejectComponent', () => {
  let component: ConfirmBackdateRejectComponent;
  let fixture: ComponentFixture<ConfirmBackdateRejectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfirmBackdateRejectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmBackdateRejectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
