import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AbsRequests } from '@hcm-mfe/self-service/data-access/models';
import { RequestsService } from '@hcm-mfe/self-service/data-access/services';
import { TranslateService } from '@ngx-translate/core';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';

@Component({
  selector: 'hcm-mfe-confirm-backdate-reject',
  templateUrl: './confirm-backdate-reject.component.html',
  styleUrls: ['./confirm-backdate-reject.component.scss']
})
export class ConfirmBackdateRejectComponent implements OnInit, OnDestroy {
  isLoadingPage = false;
  form: FormGroup = new FormGroup({});
  isSubmitted = false;
  data?: AbsRequests;
  subs: Subscription[] = [];
  constructor(
    private fb: FormBuilder,
    private requestsService: RequestsService,
    private modalRef: NzModalRef,
    private toastrService: ToastrService,
    private translate: TranslateService,
  ) {
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      rejectReason: ['', [Validators.required, Validators.maxLength(500)]],
    });
  }

  get f() { return this.form.controls; }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      const formData = {
        ...this.data,
        ...this.form.value
      }
      this.isLoadingPage = true;
      this.subs.push(this.requestsService.rejectBackdate(formData).subscribe(res => {
        if (res && res.code === 200) {
          this.toastrService.success(this.translate.instant('staffAbs.timekeepingManagement.confirmBackdate.rejectedSuccess'));
          this.modalRef.close({ refresh: true });
        } else {
          this.toastrService.error(this.translate.instant('staffAbs.timekeepingManagement.confirmBackdate.rejectError'));
          this.modalRef.close({ refresh: false });
        }
        this.isLoadingPage = false;
      }, ()=> {
        this.isLoadingPage = false;
      }));
    }
  }

  ngOnDestroy(): void {
    this.subs.forEach(sub => sub.unsubscribe());
  }
}
