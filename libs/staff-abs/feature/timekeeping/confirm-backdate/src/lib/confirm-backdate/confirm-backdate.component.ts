import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NzModalRef } from 'ng-zorro-antd/modal';

@Component({
  selector: 'hcm-mfe-confirm-backdate',
  templateUrl: './confirm-backdate.component.html',
  styleUrls: ['./confirm-backdate.component.scss'],
})
export class ConfirmBackdateComponent implements OnInit {
  countBackdate = 0;
  content = '';
  constructor(
    private translate: TranslateService,
    private modalRef: NzModalRef
  ) {}

  ngOnInit(): void {
    this.content = this.translate?.instant(
      'staffAbs.timekeepingManagement.confirmBackdate.content',
      {
        count: this.countBackdate,
      }
    );
  }

  confirm() {
    this.modalRef.close();
  }
}
