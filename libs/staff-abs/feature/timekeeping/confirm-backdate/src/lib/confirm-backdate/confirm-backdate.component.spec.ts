import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmBackdateComponent } from './confirm-backdate.component';

describe('ConfirmBackdateComponent', () => {
  let component: ConfirmBackdateComponent;
  let fixture: ComponentFixture<ConfirmBackdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfirmBackdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmBackdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
