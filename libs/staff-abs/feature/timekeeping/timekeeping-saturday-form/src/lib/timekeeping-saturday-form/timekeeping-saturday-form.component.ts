import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { ToastrService } from 'ngx-toastr';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import * as _ from 'lodash';
import { saveAs } from 'file-saver';
import {ObjectCategory} from "@hcm-mfe/staff-abs/data-access/models";
import {Constant, RequestUtils} from "@hcm-mfe/staff-abs/data-access/common";
import {RequestService, DownloadFileAttachService} from "@hcm-mfe/staff-abs/data-access/services";
import {CommonUtils} from "@hcm-mfe/shared/core";
import {beforeUploadFile, getTypeExport} from "@hcm-mfe/shared/common/utils";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";

@Component({
  selector: 'app-timekeeping-saturday-form',
  templateUrl: './timekeeping-saturday-form.component.html',
  styleUrls: ['./timekeeping-saturday-form.component.scss']
})
export class TimekeepingSaturdayFormComponent implements OnInit {
  public static readonly addTitleKey = 'staffAbs.timekeepingManagement.satuday.modal.add'
  public static readonly updateTitleKey = 'staffAbs.timekeepingManagement.satuday.modal.update'

  id?: number;
  form: FormGroup = this.fb.group([]);
  isSubmitted = false;
  listPartOfTime: ObjectCategory[];

  fileList: NzUploadFile[] = [];
  docIdsDelete: number[] = [];
  fileType = Constant.FILETYPE_UPLOAD;
  isFileEmpty = false;

  constructor(
    private fb: FormBuilder,
    private modalRef: NzModalRef,
    private toastrService: ToastrService,
    private translateService: TranslateService,
    private requestService: RequestService,
    private downloadFileAttachService: DownloadFileAttachService,
    private modalService: NgbModal
  ) {
    this.buildForm();
    this.listPartOfTime = Constant.LIST_PART_OF_TIME;
  }

  ngOnInit(): void {
    this.innitData();
  }

  buildForm() {
    this.form = this.fb.group({
      status: [Constant.REQUEST_STATUS.APPROVED],
      employeeId: ['', [Validators.required]],
      timekeepingDate: ['', [Validators.required]],
      leaveType: [Constant.LEAVE_TYPE.SATURDAY],
      note: ['', Validators.maxLength(2000)],
      pageName: [Constant.PAGE_NAME.ADMIN]
    });
  }

  innitData() {
    if(!this.id) {
      return;
    }
    this.requestService.findById(this.id).subscribe((res: BaseResponse) => {
      if (CommonUtils.isSuccessRequest(res)) {
        // transform employeeId data
        res.data.employeeId = {
          employeeId: res.data.employeeId,
          employeeCode: res.data.employeeCode,
          fullName: res.data.fullName
        }
        this.form.patchValue(RequestUtils.filterGetOneRequestLeave(res.data))

        if(res.data?.files && res.data?.files.length > 0){
          res.data.files.forEach((file: any) => {
            this.fileList.push({
              uid: file.docId,
              name: file.fileName,
              status: 'done',
            })
          });
        }

        this.fileList = _.cloneDeep(this.fileList);
      }
    })
  }

  downloadFile = (file: NzUploadFile) => {
    this.downloadFileAttachService.doDownloadAttachFile(Number(file.uid)).subscribe(res => {
      const reportFile = new Blob([res], {type: getTypeExport(file.filename?.split(".").pop() ?? '')});
      saveAs(reportFile, file.filename);
    });
  }

  removeFile = (file: NzUploadFile): boolean => {
    this.docIdsDelete.push(Number(file.uid));
    const index = this.fileList.indexOf(file);
    this.fileList.splice(index, 1);
    if (this.fileList.length === 0) {
      this.isFileEmpty = true;
    }
    return true;
  }

  isValidForm() {
  }

  close(isSearch?: boolean) {
    this.modalRef.close(isSearch)
  }

  saveOrUpdate() {
    this.isSubmitted = true;
    if (this.fileList.length === 0) this.isFileEmpty = true;
    if (this.form.valid && this.fileList.length > 0) {
      let formSave = {...this.form.value};
      formSave.files = this.fileList;
      formSave.docIdsDelete = this.docIdsDelete;
      formSave.employeeId = this.form.value.employeeId.employeeId;
      formSave.timekeepingDate =  moment(new Date(formSave.timekeepingDate)).format('DD/MM/YYYY hh:mm:ss');
      formSave = RequestUtils.convertFromRequestLeaveToRequest(formSave);
      formSave.requestId = this.id
      this.requestService.saveOrUpdateFormData(formSave).subscribe((res: BaseResponse) => {
        if (CommonUtils.isSuccessRequest(res)) {
          this.toastrService.success(this.translateService.instant(this.id ? 'common.notification.updateSuccess' : 'common.notification.addSuccess'));
          const isSearch = true;
          this.close(isSearch);
        }else{
          this.toastrService.error(this.translateService.instant(res.message ?? ' '));
        }

      }, () => {
        this.toastrService.error(this.translateService.instant(this.id ? 'common.notification.updateError' : 'common.notification.addError'));
      })
    }
  }

  beforeUpload = (file: NzUploadFile): boolean => {
    this.fileList =  beforeUploadFile(file, this.fileList, 3000000, this.toastrService, this.translateService, 'staffAbs.notification.errorFile',
      ".zip", ".rar", ".7z", ".pdf", ".png", ".jpg", ".jpeg", ".bmp"
    )
    this.isFileEmpty = false;
    return false;
  };

}
