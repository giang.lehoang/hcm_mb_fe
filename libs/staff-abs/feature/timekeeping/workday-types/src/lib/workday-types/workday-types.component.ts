import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {NzModalRef, NzModalService} from 'ng-zorro-antd/modal';
import {TranslateService} from '@ngx-translate/core';
import {HttpParams} from '@angular/common/http';
import {NzSafeAny} from 'ng-zorro-antd/core/types';
import {ToastrService} from 'ngx-toastr';
import {MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {WorkdayType} from "@hcm-mfe/staff-abs/data-access/models";
import {ConstantColor, UrlConstant} from "@hcm-mfe/staff-abs/data-access/common";
import {WorkdayTypesService} from "@hcm-mfe/staff-abs/data-access/services";
import {HTTP_STATUS_CODE, Mode} from "@hcm-mfe/shared/common/constants";
import {AddWorkdayTypesComponent} from "@hcm-mfe/staff-abs/feature/timekeeping/add-workday-types";
import {StringUtils} from "@hcm-mfe/shared/common/utils";

@Component({
  selector: 'app-workday-types',
  templateUrl: './workday-types.component.html',
  styleUrls: ['./workday-types.component.scss']
})
export class WorkdayTypesComponent implements OnInit {
  form:FormGroup = this.fb.group([]);
  tableConfig!: MBTableConfig;
  searchResult: WorkdayType[] = [];
  pagination = new Pagination();
  modal!: NzModalRef;
  tagGroupNameColor: string = ConstantColor.TAG.groupName;
  tagFactorColor: string = ConstantColor.TAG.factor;

  @ViewChild('actionTmpl', {static: true}) action!: TemplateRef<NzSafeAny>;
  @ViewChild('actionFactorTmpl', {static: true}) actionFactor!: TemplateRef<NzSafeAny>;
  constructor(private fb:FormBuilder,
              private modalService: NzModalService,
              private workdayTypesService: WorkdayTypesService,
              private translate: TranslateService,
              private toastrService: ToastrService) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      code: [null],
      name: [null]
    });
    this.initTable();
    this.doSearch(1);
  }

  doSearch(pageIndex: number) {
    this.pagination.pageNumber = pageIndex;
    const params = this.parseOptions();
    this.tableConfig.loading = true;
    this.workdayTypesService.searchWorkdayType(UrlConstant.SEARCH_FORM.WORKDAY_TYPE, params, this.pagination.getCurrentPage()).subscribe(res => {
      if (res.code == HTTP_STATUS_CODE.OK) {
        this.searchResult = res.data.listData;
        this.tableConfig.pageIndex = pageIndex;
        this.tableConfig.total = res.data.count;
      }
      this.tableConfig.loading = false;
    }, () => this.tableConfig.loading = true);
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'staffAbs.timekeepingAdmin.workdayType.table.code',
          field: 'code',
          fixed: window.innerWidth > 1024,
          fixedDir: 'left'
        },
        {
          title: 'staffAbs.timekeepingAdmin.workdayType.table.name',
          field: 'name',
        },
        {
          title: 'staffAbs.timekeepingAdmin.workdayType.table.groupNameAndFactor',
          field: 'groupName',
          tdTemplate: this.actionFactor
        },
        {
          title: '',
          tdTemplate: this.action,
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          width: 120
        }
      ],
      total: 0,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  showModalAdd(footerTmpl: TemplateRef<{}>) {
    this.modal = this.modalService.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
      nzTitle: this.translate.instant('staffAbs.timekeepingAdmin.workdayType.modal.add'),
      nzContent: AddWorkdayTypesComponent,
      nzFooter: footerTmpl
    });
    this.modal.afterClose.subscribe(result => result?.refresh ? this.doSearch(1): '');
  }

  showModalUpdate(id:number,footerTmpl: TemplateRef<{}>) {
    const data: WorkdayType = this.searchResult.filter(item => item.workdayTypeId == id)[0];
    this.modal = this.modalService.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
      nzTitle: this.translate.instant('staffAbs.timekeepingAdmin.workdayType.modal.update'),
      nzContent: AddWorkdayTypesComponent,
      nzFooter: footerTmpl,
      nzComponentParams: {
        mode: Mode.EDIT,
        data: data
      },
    });
    this.modal.afterClose.subscribe(result => result?.refresh ? this.doSearch(1): '');
  }

  deleteItem(id: number) {
    this.workdayTypesService.deleteWorkdayType(id).subscribe((res) => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.toastrService.success(this.translate.instant('common.notification.deleteSuccess'));
        this.doSearch(1);
      }
    }, error => {
      this.toastrService.error(this.translate.instant('common.notification.deleteSuccess') + ":" + error.message);
    });
  }

  parseOptions() {
    let params = new HttpParams();
    if (!StringUtils.isNullOrEmpty(this.form.controls['code'].value))
      params = params.set('code', this.form.controls['code'].value);
    if (!StringUtils.isNullOrEmpty(this.form.controls['name'].value))
      params = params.set('name', this.form.controls['name'].value);
    return params;
  }
}
