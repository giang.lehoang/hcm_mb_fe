import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { ToastrService } from 'ngx-toastr';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import * as _ from 'lodash';
import { saveAs } from 'file-saver';
import {ObjectCategory} from "@hcm-mfe/staff-abs/data-access/models";
import {Constant, RequestUtils} from "@hcm-mfe/staff-abs/data-access/common";
import {RequestService, DownloadFileAttachService} from "@hcm-mfe/staff-abs/data-access/services";
import {DateValidator} from "@hcm-mfe/shared/common/validators";
import {CommonUtils} from "@hcm-mfe/shared/core";
import {beforeUploadFile, getTypeExport} from "@hcm-mfe/shared/common/utils";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";

@Component({
  selector: 'app-timekeeping-ot-form',
  templateUrl: './timekeeping-ot-form.component.html',
  styleUrls: ['./timekeeping-ot-form.component.scss']
})
export class TimekeepingOtFormComponent implements OnInit {

  public static readonly addTitleKey = 'staffAbs.timekeepingManagement.OT.modal.add'
  public static readonly updateTitleKey = 'staffAbs.timekeepingManagement.OT.modal.update'

  id?: number;
  form: FormGroup = this.fb.group([]);
  isSubmitted = false;
  listPartOfTime: ObjectCategory[];
  fileType = Constant.FILETYPE_UPLOAD;
  fileList: NzUploadFile[] = [];
  docIdsDelete: number[] = [];
  isFileEmpty = false;

  constructor(
    private fb: FormBuilder,
    private modalRef: NzModalRef,
    private toastrService: ToastrService,
    private translateService: TranslateService,
    private requestService: RequestService,
    private downloadFileAttachService: DownloadFileAttachService,
    private modalService: NgbModal
  ) {
    this.buildForm();
    this.listPartOfTime = Constant.LIST_PART_OF_TIME;
  }

  ngOnInit(): void {
    this.innitData();
  }

  buildForm() {
    this.form = this.fb.group({
      employeeId: ['', [Validators.required]],
      timekeepingDate: ['', [Validators.required]],
      fromTime: [new Date(), [Validators.required]],
      toTime: [new Date(), [Validators.required]],
      content: ['', Validators.maxLength(2000)],
      leaveType: [Constant.LEAVE_TYPE.OT],
      status: [Constant.REQUEST_STATUS.APPROVED],
      pageName: [Constant.PAGE_NAME.ADMIN]
    },
    {
      validators: DateValidator.validateTwoDateTime('fromTime', 'toTime' )
    }
    );
  }

  innitData() {
    if(!this.id) {
      return;
    }
    this.requestService.findById(this.id).subscribe((res: BaseResponse) => {
      if (CommonUtils.isSuccessRequest(res)) {
        // transform employeeId data
        res.data.employeeId = {
          employeeId: res.data.employeeId,
          employeeCode: res.data.employeeCode,
          fullName: res.data.fullName
        }
        this.form.patchValue(RequestUtils.filterGetOneRequestLeave(res.data))

        if(res.data?.files && res.data?.files.length > 0){
          res.data.files.forEach((file: any) => {
            this.fileList.push({
              uid: file.docId,
              name: file.fileName,
              status: 'done',
            })
          });
        }

        this.fileList = _.cloneDeep(this.fileList);
      }
    })
  }

  downloadFile = (file: NzUploadFile) => {
    this.downloadFileAttachService.doDownloadAttachFile(Number(file.uid)).subscribe(res => {
      const reportFile = new Blob([res], {type: getTypeExport(file.filename?.split(".").pop() ?? '')});
      saveAs(reportFile, file.filename);
    });
  }

  removeFile = (file: NzUploadFile): boolean => {
    this.docIdsDelete.push(Number(file.uid));
    const index = this.fileList.indexOf(file);
    this.fileList.splice(index, 1);
    if (this.fileList.length === 0) {
      this.isFileEmpty = true;
    }
    return true;
  }

  isValidForm() {
  }

  close(isSearch?: boolean) {
    this.modalRef.close(isSearch)
  }

  saveOrUpdate() {
    this.isSubmitted = true;
    if (this.fileList.length === 0) this.isFileEmpty = true;
    if (this.form.valid && this.fileList.length > 0) {
      const timekeepingDate : Date = this.form.get("timekeepingDate")?.value ;
      const fromTime : Date = this.form.get("fromTime")?.value;
      const toTime : Date  = this.form.get("toTime")?.value;

      let formSave = {...this.form.value};
      formSave.files = this.fileList;
      formSave.docIdsDelete = this.docIdsDelete;
      formSave.employeeId = this.form.value.employeeId.employeeId;
      formSave.timekeepingDate = moment(new Date(timekeepingDate)).format('DD/MM/yyyy HH:mm:ss');
      formSave.fromTime = moment(new Date(timekeepingDate.getFullYear(), timekeepingDate.getMonth(), timekeepingDate.getDate(), fromTime.getHours(), fromTime.getMinutes())).format('DD/MM/yyyy HH:mm:ss')
      formSave.toTime = moment(new Date(timekeepingDate.getFullYear(), timekeepingDate.getMonth(), timekeepingDate.getDate(), toTime.getHours(), toTime.getMinutes())).format('DD/MM/yyyy HH:mm:ss')
      formSave = RequestUtils.convertFromRequestLeaveToRequest(formSave);
      formSave.requestId = this.id
      console.log("formSave", formSave);


      this.requestService.saveOrUpdateFormData(formSave).subscribe((res: BaseResponse) => {
        if (CommonUtils.isSuccessRequest(res)) {
          this.toastrService.success(this.translateService.instant(this.id ? 'common.notification.updateSuccess' : 'common.notification.addSuccess'));
          const isSearch = true;
          this.close(isSearch);
        }else{
          this.toastrService.error(this.translateService.instant(res.message ?? ' '));
        }

      }, () => {
        this.toastrService.error(this.translateService.instant(this.id ? 'common.notification.updateError' : 'common.notification.addError'));
      })
    }
  }

  beforeUpload = (file: NzUploadFile): boolean => {
    this.fileList =  beforeUploadFile(file, this.fileList, 3000000, this.toastrService, this.translateService, 'staffAbs.notification.errorFile',
      ".zip", ".rar", ".7z", ".pdf", ".png", ".jpg", ".jpeg", ".bmp"
    )
    this.isFileEmpty = false;
    return false;
  };

}
