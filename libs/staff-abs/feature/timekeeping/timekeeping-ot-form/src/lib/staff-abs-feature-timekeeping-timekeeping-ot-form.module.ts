import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TimekeepingOtFormComponent} from "./timekeeping-ot-form/timekeeping-ot-form.component";
import {NzFormModule} from "ng-zorro-antd/form";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedUiEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/employee-data-picker";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {SharedUiMbTimePickerModule} from "@hcm-mfe/shared/ui/mb-time-picker";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {NzUploadModule} from "ng-zorro-antd/upload";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzModalModule} from "ng-zorro-antd/modal";

@NgModule({
  imports: [CommonModule, NzFormModule, ReactiveFormsModule, FormsModule, SharedUiEmployeeDataPickerModule, TranslateModule,
    SharedUiMbDatePickerModule, SharedUiMbTimePickerModule, SharedUiMbInputTextModule, NzUploadModule, NzIconModule, NzButtonModule, NzModalModule],
  declarations: [TimekeepingOtFormComponent],
  exports: [TimekeepingOtFormComponent],
})
export class StaffAbsFeatureTimekeepingTimekeepingOtFormModule {}
