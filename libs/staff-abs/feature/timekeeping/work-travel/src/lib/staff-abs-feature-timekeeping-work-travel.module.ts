import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {WorkTravelComponent} from "./work-travel/work-travel.component";
import {NzFormModule} from "ng-zorro-antd/form";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiOrgDataPickerModule} from "@hcm-mfe/shared/ui/org-data-picker";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {NzPopconfirmModule} from "ng-zorro-antd/popconfirm";
import {StaffAbsUiImportRequestModule} from "@hcm-mfe/staff-abs/ui/import-request";
import {RouterModule} from "@angular/router";
import {NzButtonModule} from "ng-zorro-antd/button";
import { SharedUiMbSelectCheckAbleModule } from '@hcm-mfe/shared/ui/mb-select-check-able';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { NzTagModule } from 'ng-zorro-antd/tag';

@NgModule({
    imports: [CommonModule, SharedUiLoadingModule, SharedUiMbSelectCheckAbleModule, NzFormModule, ReactiveFormsModule, FormsModule, SharedUiMbSelectModule, TranslateModule,
        SharedUiMbInputTextModule, SharedUiOrgDataPickerModule, SharedUiMbDatePickerModule, SharedUiMbButtonModule,
        SharedUiMbTableWrapModule, SharedUiMbTableModule, NzPopconfirmModule, StaffAbsUiImportRequestModule, NzTagModule,
        RouterModule.forChild([
            {
                path: '',
                component: WorkTravelComponent
            }
        ]), NzButtonModule
    ],
  declarations: [WorkTravelComponent],
  exports: [WorkTravelComponent],
})
export class StaffAbsFeatureTimekeepingWorkTravelModule {}
