import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {NzModalRef} from 'ng-zorro-antd/modal';
import {HTTP_STATUS_CODE, Mode} from "@hcm-mfe/shared/common/constants";
import {WorkdayType, WorkdayTypeGroup} from "@hcm-mfe/staff-abs/data-access/models";
import {Category} from "@hcm-mfe/shared/data-access/models";
import {WorkdayTypesService} from "@hcm-mfe/staff-abs/data-access/services";
import {Constant, UrlConstant} from "@hcm-mfe/staff-abs/data-access/common";

@Component({
  selector: 'app-add-workday-types',
  templateUrl: './add-workday-types.component.html',
  styleUrls: ['./add-workday-types.component.scss']
})
export class AddWorkdayTypesComponent implements OnInit {
  mode = Mode.ADD;
  data = new WorkdayType();
  form: FormGroup;
  isSubmitted = false;
  listVLookupWorkdayType:Category[] = [];

  constructor(private fb: FormBuilder,
              private workdayTypesService: WorkdayTypesService,
              private toastrService: ToastrService,
              private translate: TranslateService,
              private modalRef: NzModalRef) {
    this.form = this.fb.group({
      code: [null, Validators.required],
      name: [null, Validators.required],
      groupCodes: this.fb.array([[null, Validators.required]]),
      factors: this.fb.array([[null, Validators.required]])
    });
  }

  get f() { return this.form.controls; }

  ngOnInit(): void {
    this.getVLookupWorkdayType();
    if(this.mode === Mode.EDIT) {
      this.patchValueInfo();
    }
  }

  getVLookupWorkdayType() {
    this.workdayTypesService.getVLookupWorkdayType(Constant.CATALOGS.PHAN_LOAI_NGAY_CONG).subscribe((res) => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.listVLookupWorkdayType = res.data;
        this.listVLookupWorkdayType.forEach((value, i) => {
          if(this.groupCodes.value.indexOf(value.value) > -1) {
            this.listVLookupWorkdayType[i].disabled = true;
          }
        })
      }
    }, () => {
      this.listVLookupWorkdayType = [];
    })
  }

  patchValueInfo() {
    this.f['code'].setValue(this.data.code);
    this.f['name'].setValue(this.data.name);
    this.data.listGroup?.forEach((item, index) => {
      if(index === 0) {
        this.f['groupCodes'].setValue([item.groupCode]);
        this.f['factors'].setValue([item.factor]);
      } else {
        this.groupCodes.push(new FormControl(item.groupCode, Validators.required));
        this.factors.push(new FormControl(item.factor, Validators.required));
      }
    });
  }

  get groupCodes(): FormArray {
    return this.form.get('groupCodes') as FormArray;
  }

  get factors(): FormArray {
    return this.form.get('factors') as FormArray;
  }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      let data: WorkdayType = new WorkdayType();
      if(this.mode === Mode.EDIT) {
        data = this.data;
      }
      data = this.parseData(data);
      this.workdayTypesService.saveWorkdayType(UrlConstant.SEARCH_FORM.WORKDAY_TYPE, data).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.toastrService.success(this.translate.instant(this.mode === Mode.EDIT ? 'common.notification.updateSuccess' : 'common.notification.addSuccess'));
          this.modalRef.close({refresh:true})
        } else
          this.toastrService.error(this.translate.instant(this.mode === Mode.EDIT ? 'common.notification.updateError' : 'common.notification.addError') + ": " + res?.message);
      }, error => {
        this.toastrService.error(this.translate.instant(this.mode === Mode.EDIT ? 'common.notification.updateError' : 'common.notification.addError') + ": " + error.message);
      })
    }
  }

  addFactor() {
    this.groupCodes.push(new FormControl(null, Validators.required));
    this.factors.push(new FormControl(null, Validators.required));
  }

  removeFactor(index: number, groupCodes: any) {
    this.groupCodes.removeAt(index);
    this.factors.removeAt(index);
    const data = this.listVLookupWorkdayType.filter(item => item.value === groupCodes.value)
    if(data.length > 0) {
      const i = this.listVLookupWorkdayType.indexOf(data[0]);
      this.listVLookupWorkdayType[i].disabled = false;
    }
  }

  parseData(data: any) {
    data.code = this.f['code'].value;
    data.name = this.f['name'].value;
    data.listGroup = [];
    this.form.controls['factors'].value.forEach((value: any,index: any) => {
      const listGroup: WorkdayTypeGroup = {
        groupCode: this.f['groupCodes'].value[index],
        factor: value
      };
      data.listGroup.push(listGroup)
    });
    return data;
  }
  selectGroupCode(value:string) {
    const data = this.listVLookupWorkdayType.filter(item => item.value === value);
    let index: any;
    if(data.length > 0) {
      index =  this.listVLookupWorkdayType.indexOf(data[0]);
    }
    this.listVLookupWorkdayType.forEach((item, i) => {
      if(index != null && index == i) {
        this.listVLookupWorkdayType[i].disabled = true;
      } else if(this.groupCodes.value.indexOf(item.value) < 0) {
        this.listVLookupWorkdayType[i].disabled = false;
      }
    })
  }
}
