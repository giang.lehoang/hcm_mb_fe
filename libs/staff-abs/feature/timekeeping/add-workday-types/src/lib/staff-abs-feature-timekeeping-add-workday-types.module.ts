import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AddWorkdayTypesComponent} from "./add-workday-types/add-workday-types.component";
import {NzFormModule} from "ng-zorro-antd/form";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedDirectivesNumbericModule} from "@hcm-mfe/shared/directives/numberic";

@NgModule({
  imports: [CommonModule, NzFormModule, ReactiveFormsModule, FormsModule, SharedUiMbInputTextModule, TranslateModule,
    SharedUiMbSelectModule, SharedUiMbButtonModule, SharedDirectivesNumbericModule],
  declarations: [AddWorkdayTypesComponent],
  exports: [AddWorkdayTypesComponent],
})
export class StaffAbsFeatureTimekeepingAddWorkdayTypesModule {}
