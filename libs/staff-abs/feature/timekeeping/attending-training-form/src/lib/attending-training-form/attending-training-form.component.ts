import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DownloadFileAttachService } from "@hcm-mfe/personal-tax/data-access/services";
import { beforeUploadFile, getTypeExport } from "@hcm-mfe/shared/common/utils";
import { DateValidator } from "@hcm-mfe/shared/common/validators";
import { CommonUtils } from "@hcm-mfe/shared/core";
import { BaseResponse } from "@hcm-mfe/shared/data-access/models";
import { Constant, RequestUtils } from "@hcm-mfe/staff-abs/data-access/common";
import { ObjectCategory } from "@hcm-mfe/staff-abs/data-access/models";
import { RequestService } from "@hcm-mfe/staff-abs/data-access/services";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { saveAs } from 'file-saver';
import * as _ from 'lodash';
import * as moment from 'moment';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-attending-training-form',
  templateUrl: './attending-training-form.component.html',
  styleUrls: ['./attending-training-form.component.scss']
})
export class AttendingTrainingFormComponent implements OnInit {
  public static readonly addTitleKey = 'staffAbs.timekeepingManagement.attendingTraining.modal.add'
  public static readonly updateTitleKey = 'staffAbs.timekeepingManagement.attendingTraining.modal.update'

  id?: number;
  form: FormGroup = this.fb.group([]);
  isSubmitted = false;
  listPartOfTime: ObjectCategory[] = [];

  fileList: NzUploadFile[] = [];
  docIdsDelete: number[] = [];
  fileType = Constant.FILETYPE_UPLOAD;
  isFileEmpty = false;

  constructor(
    private fb: FormBuilder,
    private modalRef: NzModalRef,
    private toastrService: ToastrService,
    private translateService: TranslateService,
    private requestService: RequestService,
    private downloadFileAttachService: DownloadFileAttachService,
    private modalService: NgbModal
  ) {
    this.buildForm();
    this.listPartOfTime = Constant.LIST_PART_OF_TIME;
  }

  ngOnInit(): void {
    this.innitData();
  }

  buildForm() {
    this.form = this.fb.group({
      employeeId: ['', [Validators.required]],
      fromTime: ['', [Validators.required]],
      toTime: ['', [Validators.required]],
      content: ['', Validators.maxLength(2000)], // noi dung dao tao
      note: ['', Validators.maxLength(2000)], //ghi chu
      leaveType: [Constant.LEAVE_TYPE.TRAINING],
      status: [Constant.REQUEST_STATUS.APPROVED],
    },
    {
      validators: DateValidator.validateTwoDateTime('fromTime', 'toTime' )
    }
    );
  }

  innitData() {
    if(!this.id) {
      return;
    }
    this.requestService.findById(this.id).subscribe((res: BaseResponse) => {
      if (CommonUtils.isSuccessRequest(res)) {
        // transform employeeId data
        res.data.employeeId = {
          employeeId: res.data.employeeId,
          employeeCode: res.data.employeeCode,
          fullName: res.data.fullName
        }
        this.form.patchValue(RequestUtils.filterGetOneRequestLeave(res.data))

        if(res.data?.files && res.data?.files.length > 0){
          res.data.files.forEach((file: any) => {
            this.fileList.push({
              uid: file.docId,
              name: file.fileName,
              status: 'done',
            })
          });
        }

        this.fileList = _.cloneDeep(this.fileList);
      }
    })
  }

  downloadFile = (file: NzUploadFile) => {
    this.downloadFileAttachService.doDownloadAttachFile(Number(file.uid)).subscribe(res => {
      const reportFile = new Blob([res], {type: getTypeExport(file.filename?.split(".").pop() ?? '')});
      saveAs(reportFile, file.filename);
    });
  }

  removeFile = (file: NzUploadFile): boolean => {
    this.docIdsDelete.push(Number(file.uid));
    const index = this.fileList.indexOf(file);
    this.fileList.splice(index, 1);
    if (this.fileList.length === 0) {
      this.isFileEmpty = true;
    }
    return true;
  }

  isValidForm() {
  }

  close(isSearch?: boolean) {
    this.modalRef.close(isSearch)
  }

  saveOrUpdate() {
    this.isSubmitted = true;
    if (this.fileList.length === 0) this.isFileEmpty = true;
    if (this.form.valid && this.fileList.length > 0) {
      let formSave = {...this.form.value};
      formSave.files = this.fileList;
      formSave.docIdsDelete = this.docIdsDelete;
      formSave.employeeId = this.form.value.employeeId.employeeId;

      formSave.fromTime = typeof formSave.fromTime === 'string' ? formSave.fromTime :  moment(new Date(formSave.fromTime)).format('DD/MM/yyyy HH:mm:ss');
      formSave.toTime = typeof formSave.toTime === 'string' ? formSave.toTime :  moment(new Date(formSave.toTime)).format('DD/MM/yyyy HH:mm:ss');

      formSave = RequestUtils.convertFromRequestLeaveToRequest(formSave);
      formSave.requestId = this.id
      this.requestService.saveOrUpdateFormData(formSave).subscribe((res: BaseResponse) => {
        if (CommonUtils.isSuccessRequest(res)) {
          this.toastrService.success(this.translateService.instant(this.id ? 'common.notification.updateSuccess' : 'common.notification.addSuccess'));
          const isSearch = true;
          this.close(isSearch);
        }else{
          this.toastrService.error(this.translateService.instant(res.message ?? ' '));
        }

      }, () => {
        this.toastrService.error(this.translateService.instant(this.id ? 'common.notification.updateError' : 'common.notification.addError'));
      })
    }
  }

  beforeUpload = (file: NzUploadFile): boolean => {
    this.fileList =  beforeUploadFile(file, this.fileList, 3000000, this.toastrService, this.translateService, 'staffAbs.notification.errorFile',
      ".zip", ".rar", ".7z", ".pdf", ".png", ".jpg", ".jpeg", ".bmp"
    )
    this.isFileEmpty = false;
    return false;
  };

}
