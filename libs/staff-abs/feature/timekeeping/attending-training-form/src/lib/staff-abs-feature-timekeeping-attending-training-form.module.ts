import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AttendingTrainingFormComponent} from "./attending-training-form/attending-training-form.component";
import {NzGridModule} from "ng-zorro-antd/grid";
import {ReactiveFormsModule} from "@angular/forms";
import {SharedUiEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/employee-data-picker";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbDateTimeWorkModule} from "@hcm-mfe/shared/ui/mb-date-time-work";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {NzUploadModule} from "ng-zorro-antd/upload";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzModalModule} from "ng-zorro-antd/modal";

@NgModule({
  imports: [CommonModule, NzGridModule, ReactiveFormsModule, SharedUiEmployeeDataPickerModule, TranslateModule,
    SharedUiMbDateTimeWorkModule, SharedUiMbInputTextModule, NzUploadModule, NzIconModule, NzButtonModule, NzModalModule],
  declarations: [AttendingTrainingFormComponent],
  exports: [AttendingTrainingFormComponent],
})
export class StaffAbsFeatureTimekeepingAttendingTrainingFormModule {}
