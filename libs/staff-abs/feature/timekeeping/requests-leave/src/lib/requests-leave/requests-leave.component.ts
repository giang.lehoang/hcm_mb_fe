import { ChangeDetectorRef, Component, Injector, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ValidatorFn } from '@angular/forms';
import * as moment from 'moment';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import * as _ from 'lodash';
import {ModalOptions, NzModalService} from 'ng-zorro-antd/modal';
import { forkJoin } from 'rxjs';
import {Constant, SearchBaseComponent} from "@hcm-mfe/staff-abs/data-access/common";
import {FunctionCode, Scopes} from "@hcm-mfe/shared/common/enums";
import {AppFunction, Category} from "@hcm-mfe/shared/data-access/models";
import {CommonUtils, StaffInfoService, ValidateService} from "@hcm-mfe/shared/core";
import {ReasonLeaveService, RequestService, DownloadFileAttachService} from "@hcm-mfe/staff-abs/data-access/services";
import {RequestsLeaveFormComponent} from "@hcm-mfe/staff-abs/feature/timekeeping/requests-leave-form";
import {DateValidator} from "@hcm-mfe/shared/common/validators";
import {HTTP_STATUS_CODE, Mode, SYSTEM_FORMAT_DATA} from "@hcm-mfe/shared/common/constants";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";
import { SessionService } from '@hcm-mfe/shared/common/store';
import {getTypeExport} from "@hcm-mfe/shared/common/utils";
import {saveAs} from "file-saver";
import { ConfirmBackdateComponent } from '../confirm-backdate/confirm-backdate.component';
import { ConfirmBackdateRejectComponent } from '@hcm-mfe/staff-abs/feature/timekeeping/confirm-backdate';
import { AbsRequestApprovers } from '@hcm-mfe/staff-abs/data-access/models';

@Component({
  selector: 'app-requests-leave',
  templateUrl: './requests-leave.component.html',
  styleUrls: ['./requests-leave.component.scss']
})
export class RequestsLeaveComponent extends SearchBaseComponent implements OnInit {
  @ViewChild('actionTmpl', { static: true }) action!: TemplateRef<NzSafeAny>;
  @ViewChild('filesTmpl', { static: true }) filesTmpl!: TemplateRef<NzSafeAny>;
  scope = Scopes.VIEW;
  functionCode = FunctionCode.ABS_TIMEKEEPINGS;
  objFunction: AppFunction;
  listEmpType: Category[] = [];
  formSearchConfig = {
    leaveType: [Constant.LEAVE_TYPE.LEAVE],
    fromTime: [null],
    empTypeCode: [null],
    toTime: [null],
    listStatus: [[]],
    empStatus : [[]],
    reasonLeaveId : [[]],
    startRecord: [0],
    pageSize: [10],
    employeeCode: [''],
    fullName: [''],
    keySearch: [''],
    organizationId: [''],
    pageName: [Constant.PAGE_NAME.ADMIN]
    // pageName: [Constant.PAGE_NAME.EMPLOYEE]
  }
  REQUEST_STATUS = Constant.REQUEST_STATUS;
  LEAVE = Constant.LEAVE_TYPE.LEAVE
  isImportData = false;
  validators: ValidatorFn[] = [];
  fileNameExport = Constant.FILE_NAME_EXPORT.LEAVE;
  REQUEST_STATUS_DATASOURCE = Constant.REQUEST_STATUS_DATASOURCE;
  listReasonLeave: any = []; // Loại đơn
  TAG = Constant.TAG;
  @ViewChild('timeLeaveTmpl', { static: true }) timeLeave!: TemplateRef<NzSafeAny>;
  @ViewChild('statusTmpl', { static: true }) status!: TemplateRef<NzSafeAny>;
  @ViewChild('reviewersTmpl', { static: true }) reviewers!: TemplateRef<NzSafeAny>;
  @ViewChild('approversTmpl', { static: true }) approvers!: TemplateRef<NzSafeAny>;
  constructor(
    injector: Injector,
    public validateService: ValidateService,
    private cdr: ChangeDetectorRef,
    private reasonLeavesService: ReasonLeaveService,
    private requestsService: RequestService,
    private staffInfoService: StaffInfoService,
    public sessionService: SessionService,
    public downloadFileAttachService: DownloadFileAttachService,
    private modalServices: NzModalService,
  ) {
    super(injector);
    this.modalComponent = RequestsLeaveFormComponent;
    this.setMainService(requestsService);
    this.setValidators();
    this.buildFormSearch(this.formSearchConfig, this.validators);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ABS_TIMEKEEPINGS}`);
    // this.getListReasonLeave();
  }

  public setValidators() {
    this.validators.push(DateValidator.validateTwoDate('fromTime', 'toTime', 'greaterAndEqual'));
  }

  ngOnInit(): void {
    this.initTable();
    this.getListEmployeeType();
    forkJoin([
      this.reasonLeavesService.getAllReasonLeaves(Constant.LEAVE_TYPE.LEAVE),
      this.requestsService.getTotalRequestBackdate(Constant.LEAVE_TYPE.LEAVE)
    ]).subscribe(
      ([listReasonLeave, countBackdateResponse]) =>{
        this.getListReasonLeave(listReasonLeave);
        this.beforeSearch(countBackdateResponse.data);
      },() => {
        this.toastrService?.error(this.translate?.instant('common.notification.error'));
      }
    )
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'staffAbs.label.staffCode',
          field: 'employeeCode',
          width: 120,
          thClassList: ['text-center'],
          fixedDir: 'left'
        },
        {
          title: 'staffAbs.label.employeeName',
          field: 'fullName',
          thClassList: ['text-center'],
          width: 180,
        },
        {
          title: 'staffAbs.label.organization',
          field: 'organizationName',
          thClassList: ['text-center'],
          width: 200,
          show: false
        },
        {
          title: 'staffAbs.timekeepingManagement.requestsLeave.timeLeave',
          field: 'requestLeave',
          tdTemplate: this.timeLeave,
          thClassList: ['text-center'],
        },
        {
          title: 'staffAbs.timekeepingManagement.requestsLeave.numberDayLeave',
          field: 'allDays',
          thClassList: ['text-center'],
          width: 75,
          show: false
        },
        {
          title: 'staffAbs.timekeepingManagement.requestsLeave.numberDayWorkLeave',
          field: 'totalDays',
          thClassList: ['text-center'],
          width: 75
        },
        {
          title: 'staffAbs.timekeepingManagement.requestsLeave.reasonType',
          field: 'reasonDetail',
          thClassList: ['text-center'],
        },
        {
          title: 'staffAbs.label.file',
          field: 'files',
          tdTemplate: this.filesTmpl,
          tdClassList: ['text-left'],
          thClassList: ['text-center']
        },
        {
          title: 'staffAbs.table.reviewer',
          field: 'reviewer',
          show: false,
          tdTemplate: this.reviewers,
          width: 150
        },
        {
          title: 'staffAbs.table.approver',
          field: 'approver',
          show: false,
          tdTemplate: this.approvers,
          width: 150
        },
        {
          title: 'staffAbs.label.createdBy',
          field: 'createdBy',
          show: false,
          tdClassList: ['text-left'],
          thClassList: ['text-center']
        },
        {
          title: 'staffAbs.label.createDate',
          field: 'createDate',
          show: false,
          tdClassList: ['text-left'],
          thClassList: ['text-center']
        },
        {
          title: 'staffAbs.label.lastUpdatedBy',
          field: 'lastUpdatedBy',
          show: false,
          tdClassList: ['text-left'],
          thClassList: ['text-center']
        },
        {
          title: 'staffAbs.label.lastUpdateDate',
          field: 'lastUpdateDate',
          show: false,
          tdClassList: ['text-left'],
          thClassList: ['text-center']
        },
        {
          title: 'staffAbs.timekeepingManagement.requestsLeave.status',
          field: 'status',
          thClassList: ['text-center'],
          tdTemplate: this.status,
          tdClassList: ['text-nowrap', 'text-center'],
          width: 170,
          fixed: window.innerWidth > 1024,
          fixedDir: 'right'
        },
        {
          title: '',
          field: 'action',
          tdClassList: ['text-nowrap', 'text-center'],
          thClassList: ['text-nowrap', 'text-center'],
          width: 90,
          show: this.objFunction.edit,
          tdTemplate: this.action,
          fixed: window.innerWidth > 1024,
          fixedDir: 'right'
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  /**
   * Tìm kiếm
   * @param pageIndex
   */
  override doSearch(pageNumber?: number) {
    this.pagination.pageNumber = pageNumber ?? 1;
    this.tableConfig.pageIndex = pageNumber ? this.tableConfig.pageIndex : 1;
    this.tableConfig.loading = true;
    const params = this.formSearch ? { ...this.formSearch.value } : {};

    params.fromTime = params.fromTime ? moment(params.fromTime).format(SYSTEM_FORMAT_DATA.DATE_TIME_FORMAT) : null;
    params.toTime = params.toTime ? moment(params.toTime).format(SYSTEM_FORMAT_DATA.DATE_TIME_FORMAT) : null;

    if (params.organizationId) {
      params.organizationId = params.organizationId.orgId
    }
    if(params.listStatus){
      params.listStatus = params.listStatus.join(',');
    }
    if(params.empStatus){
      params.empStatus = params.empStatus.join(',');
    }
    if(params.reasonLeaveId){
      params.reasonLeaveId = params.reasonLeaveId.join(',');
    }
    this.isLoadingPage = true;    
    this.requestsService.search({ ...params, ...this.pagination.getCurrentPage() })
      .subscribe(res => {
        this.isLoadingPage = false;
        if (CommonUtils.isSuccessRequest(res)) {
          this.searchResult = this.beforeRender(res.data.listData);
          this.tableConfig.total = res.data.count;
        }
        this.resultList = res;
        this.tableConfig.loading = false;
      },
        () => {
          this.tableConfig.loading = false;
        }
      );
  }

  beforeRender(listData: any) {
    if (!listData) {
      return listData;
    }
    return listData.map((item: any) => {
      if (item.listAbsRequestLeaves) {
        item.listAbsRequestLeavesText = item.listAbsRequestLeaves.map((leave: any) => {
          const { fromTime, toTime, reasonLeaveId } = leave;
          const leaveTypeName = this.listReasonLeave.find((e: any) => e.reasonLeaveId === reasonLeaveId)?.name;
          return this.translate?.instant('staffAbs.timekeepingManagement.requestsLeave.requestLeavesText', {
            leaveTypeName: leaveTypeName,
            form: this._formatDateTime(fromTime),
            to: this._formatDateTime(toTime),
          });
        });
        item.allDays = this.translate?.instant('staffAbs.timekeepingManagement.requestsLeave.numberDate', { param: item.allDays ?? 0 });
        item.totalDays = this.translate?.instant('staffAbs.timekeepingManagement.requestsLeave.numberDate', { param: item.totalDays ?? 0 });
        if (item.listAbsRequestApprovers) {
          item.listAbsRequestApprovers = _.orderBy(item.listAbsRequestApprovers, ['approvalOrder'], ['asc']);
          const numApprover = item.listAbsRequestApprovers.length;
          item.listReviewer = item.listAbsRequestApprovers.slice(0, numApprover - 1).map((e: AbsRequestApprovers) => ({ ...e, fullName: `${e.fullName}-${e.employeeCode}`}));
          item.listApprover = item.listAbsRequestApprovers.slice(numApprover - 1, numApprover).map((e: AbsRequestApprovers) => ({ ...e, fullName: `${e.fullName}-${e.employeeCode}`}));
        }
      }
      return item;
    })
  }


  _formatDateTime(dateInput: any) {
    const time = moment(dateInput, SYSTEM_FORMAT_DATA.DATE_TIME_FORMAT).format('HH:mm');
    const date = moment(dateInput, SYSTEM_FORMAT_DATA.DATE_TIME_FORMAT).format(SYSTEM_FORMAT_DATA.DATE_FORMAT);
    return this.translate?.instant('staffAbs.timekeepingManagement.requestsLeave.dateTimeLabel', { time, date });
  }

  doCloseImport(isSearch: boolean) {
    this.isImportData = false;
    isSearch && this.doSearch(this.pagination.pageNumber);
  }

  doOpenImport() {
    this.isImportData = true;
  }

  /**
  * Lấy danh mục Loại đơn
  */
  getListReasonLeave(res: any) {
    this.subs.push(
      this.listReasonLeave = res.data
    );
  }

  getStatus(value: number) {
    return this.REQUEST_STATUS_DATASOURCE.find(status => status.value === value)?.label;
  }

  override objectCreateModal(id?: number): ModalOptions {
    return {
      nzWidth: window.innerWidth > 767 ? (window.innerWidth / 1.5 > 1500 ? 1500 : window.innerWidth / 1.5) : window.innerWidth,
      nzTitle: this.translate?.instant(id ? this.modalComponent.updateTitleKey : this.modalComponent.addTitleKey),
      nzContent: this.modalComponent,
      nzComponentParams: {
        mode: id ? Mode.EDIT : Mode.ADD,
        id: id
      }
    }
  };
  getListEmployeeType() {
    this.staffInfoService.getCatalog(Constant.CATALOGS.DOI_TUONG_CV).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) this.listEmpType = res.data;
      });
  }

  downloadFile(file: NzSafeAny) {
    this.downloadFileAttachService.doDownloadAttachFile(Number(file.docId)).subscribe(res => {
      const reportFile = new Blob([res], {type: getTypeExport(file.fileName?.split(".").pop() ?? '')});
      saveAs(reportFile, file.fileName);
    });
  }

  beforeSearch(countBackdate: number) {
    if (countBackdate && countBackdate > 0) {
      // show popup
      this.formSearch?.controls['listStatus'].setValue([Constant.REQUEST_STATUS.WAIT_APPROVE_BACKDATE
        ,Constant.REQUEST_STATUS.CANCEL_BACKDATE]);
      this.doSearch(1);

      this.modal = this.modalServices.create({
        nzTitle: '',
        nzStyle: {
          width: '414px',
          height: '223px',
        },
        nzContent: ConfirmBackdateComponent,
        nzComponentParams: {
          countBackdate: countBackdate
        },
        nzFooter: null,
        nzClosable: false
      });
      // this.modal.afterClose.subscribe(() => {});
    } else {
      this.doSearch(1);
    }
  }

  doApproveBackdate(requestId: number) {
    this.requestsService.approveBackdateRequests(requestId).subscribe((res: BaseResponse) => {
      if (CommonUtils.isSuccessRequest(res)) {
        this.toastrService?.success(this.translate?.instant('staffAbs.timekeepingManagement.confirmBackdate.approvalSuccess'));
        this.doSearch(1);
      } else {
        this.toastrService?.success(this.translate?.instant('staffAbs.timekeepingManagement.confirmBackdate.approvalError'));
      }
    });
  }

  doPrepareReject(requestId: number, footerTmpl: TemplateRef<NzSafeAny>) {
    this.modal = this.modalServices.create(
      {
        nzWidth: 500,
        nzTitle: this.translate?.instant('staffAbs.timekeepingManagement.confirmBackdate.rejectTitle'),
        nzContent: ConfirmBackdateRejectComponent,
        nzComponentParams: {
          data: {
            requestId: requestId,
          }
        },
        nzFooter: footerTmpl
      }
    );
    this.subs.push(this.modal.afterClose.subscribe(result => {
      if (result?.refresh) {
        // this.toastrService?.success(this.translate?.instant('staffAbs.timekeepingManagement.confirmBackdate.rejectedSuccess'));
        this.doSearch(1);
      }
    }));
  }
}
