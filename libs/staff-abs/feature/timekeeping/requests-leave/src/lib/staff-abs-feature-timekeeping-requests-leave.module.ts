import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RequestsLeaveComponent} from "./requests-leave/requests-leave.component";
import {NzFormModule} from "ng-zorro-antd/form";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiOrgDataPickerModule} from "@hcm-mfe/shared/ui/org-data-picker";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {NzPopconfirmModule} from "ng-zorro-antd/popconfirm";
import {StaffAbsUiImportRequestModule} from "@hcm-mfe/staff-abs/ui/import-request";
import {NzTagModule} from "ng-zorro-antd/tag";
import {RouterModule} from "@angular/router";
import {NzButtonModule} from "ng-zorro-antd/button";
import { SharedUiMbSelectCheckAbleModule } from '@hcm-mfe/shared/ui/mb-select-check-able';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { ConfirmBackdateComponent } from './confirm-backdate/confirm-backdate.component';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';

@NgModule({
    imports: [CommonModule, NzFormModule, SharedUiLoadingModule, ReactiveFormsModule, FormsModule, SharedUiMbSelectModule, SharedUiMbSelectCheckAbleModule, TranslateModule,
        SharedUiMbInputTextModule, SharedUiOrgDataPickerModule, SharedUiMbDatePickerModule, SharedUiMbButtonModule,
        SharedUiMbTableWrapModule, SharedUiMbTableModule, NzPopconfirmModule, StaffAbsUiImportRequestModule, NzTagModule,
        NzToolTipModule,
        RouterModule.forChild([
            {
                path: '',
                component: RequestsLeaveComponent
            }
        ]), NzButtonModule
    ],
  declarations: [RequestsLeaveComponent, ConfirmBackdateComponent],
  exports: [RequestsLeaveComponent],
})
export class StaffAbsFeatureTimekeepingRequestsLeaveModule {}
