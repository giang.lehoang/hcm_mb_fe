import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TimekeepingSearchComponent} from "./timekeeping-search/timekeeping-search.component";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {NzFormModule} from "ng-zorro-antd/form";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiOrgDataPickerModule} from "@hcm-mfe/shared/ui/org-data-picker";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzTableModule} from "ng-zorro-antd/table";
import {RouterModule} from "@angular/router";
import { SharedUiMbSelectCheckAbleModule } from '@hcm-mfe/shared/ui/mb-select-check-able';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { ExportConditionComponent } from './export-condition/export-condition.component';

@NgModule({
  imports: [CommonModule, SharedUiMbSelectCheckAbleModule, SharedUiLoadingModule, NzFormModule, ReactiveFormsModule, FormsModule, SharedUiMbSelectModule,
    TranslateModule, SharedUiMbInputTextModule, SharedUiOrgDataPickerModule, SharedUiMbDatePickerModule, SharedUiMbButtonModule, NzTableModule, NzPopconfirmModule,
    NzDropDownModule,
    RouterModule.forChild([
      {
        path: '',
        component: TimekeepingSearchComponent
      }
    ])
  ],
  declarations: [TimekeepingSearchComponent, ExportConditionComponent],
  exports: [TimekeepingSearchComponent],
})
export class StaffAbsFeatureTimekeepingTimekeepingSearchModule {}
