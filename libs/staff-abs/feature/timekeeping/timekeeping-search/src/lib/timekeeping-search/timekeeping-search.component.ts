import {ChangeDetectorRef, Component, OnInit, TemplateRef } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpParams} from '@angular/common/http';
import { saveAs } from 'file-saver';
import {ToastrService} from 'ngx-toastr';
import {FunctionCode, Scopes} from "@hcm-mfe/shared/common/enums";
import {AppFunction, BaseResponse, Category, Pagination} from "@hcm-mfe/shared/data-access/models";
import {Timekeepings} from "@hcm-mfe/staff-abs/data-access/models";
import {StaffInfoService, ValidateService} from "@hcm-mfe/shared/core";
import {TimekeepingsService} from "@hcm-mfe/staff-abs/data-access/services";
import {DateValidator} from "@hcm-mfe/shared/common/validators";
import {Constant, UrlConstant} from "@hcm-mfe/staff-abs/data-access/common";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {getTypeExport, StringUtils} from "@hcm-mfe/shared/common/utils";
import { SessionService } from '@hcm-mfe/shared/common/store';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { TranslateService } from '@ngx-translate/core';
import { ExportConditionComponent, ExportType } from '../export-condition/export-condition.component';
import { Subscription } from 'rxjs';
import * as moment from 'moment';

@Component({
  selector: 'app-timekeeping-search',
  templateUrl: './timekeeping-search.component.html',
  styleUrls: ['./timekeeping-search.component.scss']
})
export class TimekeepingSearchComponent implements OnInit {
  form:FormGroup = this.fb.group([]);
  scope = Scopes.VIEW;
  objFunction: AppFunction;
  objFunctionReport: AppFunction;
  functionCode = "ABS_TIMEKEEPING_TABLE";
  pagination = new Pagination();
  isSearch = false;
  isLoadingPage = false;
  searchResult: Timekeepings[] = [];
  listEmpType: Category[] = [];
  EMP_STATUS_DATASOURCE = Constant.EMP_STATUS_DATASOURCE;
  count = 0;
  loading = false;
  colSpan = 1;
  nzScrollWidth?: number;
  innerWidth:number = window.innerWidth;
  modal?: NzModalRef;
  subs: Subscription[] = [];
  ExportType = ExportType;
  DATE_TIME_FORMAT = 'DD/MM/yyyy HH:mm:ss';
  constructor(private fb:FormBuilder,
              public validateService: ValidateService,
              private timekeepingsService:TimekeepingsService,
              private toastrService: ToastrService,
              private staffInfoService: StaffInfoService,
              private sessionService: SessionService,
              private modalServices: NzModalService,
              public translate: TranslateService,
              private ref: ChangeDetectorRef) { 
                this.objFunction = this.sessionService.getSessionData(`FUNCTION_ABS_TIMEKEEPING_TABLE`);
                this.objFunctionReport = this.sessionService.getSessionData(`FUNCTION_ABS_TIMEKEEPING_REPORT`);
              }

  ngOnInit(): void {
    const date = new Date();
    const fromDate = new Date(date.getFullYear(), date.getMonth() - 1, 26);
    const toDate = new Date(date.getFullYear(), date.getMonth() , 25);
    this.form = this.fb.group({
      employeeCode: [null],
      fullName: [null],
      organizationId: [null],
      empTypeCode: [null],
      empStatus:[null],
      keySearch: [''],
      fromDate: [fromDate, [Validators.required]],
      toDate: [toDate, [Validators.required]],
    },{
      validators: [
        DateValidator.validateRangeDate('fromDate', 'toDate', 'rangeDateError')
      ]
    });
    this.getListEmployeeType();
    this.doSearch(1);
  }

  doSearch(pageNumber?: number) {
    this.pagination.pageNumber = pageNumber ?? 1;
    this.isSearch = true;
    if(this.form.valid) {
      const params = this.parseOptions();
      this.isLoadingPage = true;
      console.log(this.pagination.getCurrentPage());
      this.timekeepingsService.searchTimekeepings(UrlConstant.SEARCH_FORM.TIMEKEEPING, params, this.pagination.getCurrentPage()).subscribe(res => {
        this.isLoadingPage = false;
        if (res.code == HTTP_STATUS_CODE.OK) {
          this.searchResult = res.data.listData;
          this.count = res.data.count;
          this.colSpan = res.data.listData[0]?.listTimeKeeping.length;
          this.nzScrollWidth = 500 + this.colSpan*70;
          this.ref.markForCheck();
        } else {
          this.toastrService.error(res.message);
        }
      }, () => this.loading = true);
    }
  }

  doExportData() {
    this.isSearch = true;
    if(this.form.valid) {
      const searchParam = this.parseOptions();
      this.isLoadingPage = true;
      this.timekeepingsService.doExportFile(UrlConstant.EXPORT_REPORT.TIMEKEEPING, searchParam).subscribe(res => {
        this.isLoadingPage = false;
        const arr = res.headers.get("Content-Disposition")?.split(';');
        const fileName:string = arr[arr.length - 1].replace('filename=','').trim();
        const reportFile = new Blob([res.body], { type: getTypeExport('xlsx') });
        saveAs(reportFile, fileName);
      }, error => {
        this.toastrService.error(error.message);
      });
    }
  }
  getListEmployeeType() {
    this.staffInfoService.getCatalog(Constant.CATALOGS.DOI_TUONG_CV).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) this.listEmpType = res.data;
    });
}

  parseOptions() {
    let params = new HttpParams();
    if (!StringUtils.isNullOrEmpty(this.form.controls['organizationId'].value))
      params = params.set('organizationId', this.form.controls['organizationId'].value?.orgId);
    if (!StringUtils.isNullOrEmpty(this.form.controls['employeeCode'].value))
      params = params.set('employeeCode', this.form.controls['employeeCode'].value);
    if (!StringUtils.isNullOrEmpty(this.form.controls['keySearch'].value))
      params = params.set('keySearch', this.form.controls['keySearch'].value);
    if (!StringUtils.isNullOrEmpty(this.form.controls['fullName'].value))
      params = params.set('fullName', this.form.controls['fullName'].value);
    if (!StringUtils.isNullOrEmpty(this.form.controls['fromDate'].value))
      params = params.set('fromDate', this.convertDate(this.form.controls['fromDate'].value));
    if (!StringUtils.isNullOrEmpty(this.form.controls['toDate'].value))
      params = params.set('toDate', this.convertDate(this.form.controls['toDate'].value));
    if (!StringUtils.isNullOrEmpty(this.form.controls['empTypeCode'].value))
      params = params.set('empTypeCode', this.form.controls['empTypeCode'].value);
    if (this.form?.value.empStatus)
      params = params.set('empStatus', this.form.value.empStatus.join(','));
    return params;
  }
  convertDate(date:Date):string {
    return date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();
  }

  timekeepingAuto() {
    if(this.form.valid) {
      this.isLoadingPage = true;
      const params = this.parseOptions();
      this.timekeepingsService.timekeepingAuto(params).subscribe(res => {
        if(res.code === HTTP_STATUS_CODE.OK) {
          this.doSearch(1);
          this.isLoadingPage = false;
          this.toastrService?.success(res.message);
        } else {
          this.toastrService?.error(res.message);
        }
      }, () => {
        this.isLoadingPage = false;
      });
    }
  }

  prepareExport(type: ExportType, footerTmpl: TemplateRef<NzSafeAny>) {
    this.modal = this.modalServices.create(
      {
        nzWidth: 500,
        nzTitle: this.translate?.instant('staffAbs.label.exportCondition'),
        nzContent: ExportConditionComponent,
        nzComponentParams: {
          type: type
        },
        nzFooter: footerTmpl
      }
    );
    this.subs.push(this.modal.afterClose.subscribe(result => {
      if (result?.export) {
        if (result.type === ExportType.AT_TIME) {
          this.doExportAtTime(result.reportTime);
        } else {
          this.doExportChanged(result.fromTime, result.toTime);
        }
      }
    }));
  }

  doExportAtTime(reportTime: Date) {
    let searchParam = this.parseOptions();
    searchParam = searchParam.set('reportTime', moment(new Date(reportTime)).format(this.DATE_TIME_FORMAT));
    this.isLoadingPage = true;
    this.timekeepingsService.doExportFileAtTime(searchParam).subscribe(res => {
      this.isLoadingPage = false;
      const arr = res.headers.get("Content-Disposition")?.split(';');
      const fileName:string = arr[arr.length - 1].replace('filename=','').trim();
      const reportFile = new Blob([res.body], { type: getTypeExport('xlsx') });
      saveAs(reportFile, fileName);
    }, error => {
      this.toastrService.error(error.message);
      this.isLoadingPage = false;
    });
  }

  doExportChanged(fromTime: Date, toTime: Date) {
    let searchParam = this.parseOptions();
    searchParam = searchParam.set('fromTime', moment(new Date(fromTime)).format(this.DATE_TIME_FORMAT));
    searchParam = searchParam.set('toTime', moment(new Date(toTime)).format(this.DATE_TIME_FORMAT));
    this.isLoadingPage = true;
    this.timekeepingsService.doExportFileChanged(searchParam).subscribe(res => {
      this.isLoadingPage = false;
      const arr = res.headers.get("Content-Disposition")?.split(';');
      const fileName:string = arr[arr.length - 1].replace('filename=','').trim();
      const reportFile = new Blob([res.body], { type: getTypeExport('xlsx') });
      saveAs(reportFile, fileName);
    }, error => {
      this.toastrService.error(error.message);
      this.isLoadingPage = false;
    });
  }
}
