import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DateValidator } from '@hcm-mfe/shared/common/validators';
import { NzModalRef } from 'ng-zorro-antd/modal';

export enum ExportType {
  AT_TIME = 'AT_TIME',
  CHANGED = 'CHANGED',
}

@Component({
  selector: 'hcm-mfe-export-condition',
  templateUrl: './export-condition.component.html',
  styleUrls: ['./export-condition.component.scss']
})
export class ExportConditionComponent implements OnInit {
  form: FormGroup = new FormGroup({});
  type = '';
  ExportType = ExportType;
  isSubmitted = false;
  constructor(
    private fb: FormBuilder,
    private modalRef: NzModalRef
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      reportTime: ['', []],
      fromTime: ['', []],
      toTime: ['', []],
    });
    if (this.type === ExportType.AT_TIME) {
      this.form.controls['reportTime'].addValidators(Validators.required);
    } else if (this.type === ExportType.CHANGED) {
      this.form.controls['fromTime'].addValidators(Validators.required);
      this.form.controls['toTime'].addValidators(Validators.required);
      this.form.addValidators(DateValidator.validateTwoDateTime('fromTime', 'toTime'));
    }
  }

  save() {
    this.isSubmitted = true;
    if (this.form.invalid) {
      return;
    }
    this.modalRef.close({ 
      export: true,
      type: this.type,
      ...this.form.value
    });
  }
}
