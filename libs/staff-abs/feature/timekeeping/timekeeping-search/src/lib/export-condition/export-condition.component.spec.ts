import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExportConditionComponent } from './export-condition.component';

describe('ExportConditionComponent', () => {
  let component: ExportConditionComponent;
  let fixture: ComponentFixture<ExportConditionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExportConditionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExportConditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
