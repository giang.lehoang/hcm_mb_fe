import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject, forkJoin, Subscription } from 'rxjs';
import * as moment from 'moment';
import * as _ from 'lodash';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { saveAs } from 'file-saver';
import {Constant} from "@hcm-mfe/staff-abs/data-access/common";
import {CaculateLeavesResponse, Request} from "@hcm-mfe/staff-abs/data-access/models";
import {CommonUtils, StaffInfoService} from "@hcm-mfe/shared/core";
import {ReasonLeaveService, RequestLeavesService, RequestService, DownloadFileAttachService} from "@hcm-mfe/staff-abs/data-access/services";
import {DateValidator} from "@hcm-mfe/shared/common/validators";
import {HTTP_STATUS_CODE, SYSTEM_FORMAT_DATA} from "@hcm-mfe/shared/common/constants";
import {beforeUploadFile, getTypeExport} from "@hcm-mfe/shared/common/utils";
import {BaseResponse, EmployeeDetail} from "@hcm-mfe/shared/data-access/models";

@Component({
  selector: 'app-requests-leave-form',
  templateUrl: './requests-leave-form.component.html',
  styleUrls: ['./requests-leave-form.component.scss']
})
export class RequestsLeaveFormComponent implements OnInit, AfterViewInit {

  public static readonly addTitleKey = 'staffAbs.timekeepingManagement.requestsLeave.modal.add'
  public static readonly updateTitleKey = 'staffAbs.timekeepingManagement.requestsLeave.modal.update'
  fileType = Constant.FILETYPE_UPLOAD;

  id?: number;
  form: FormGroup;
  isSubmitted = false;
  isLoading = false;
  REQUEST_STATUS = Constant.REQUEST_STATUS;
  SCREEN_MODE = Constant.SCREEN_MODE;
  formId = 'request-form';
  fileList: NzUploadFile[] = [];
  docIdsDelete: number[] = [];
  subs: Subscription[] = [];
  isHR = false;
  listReasonType = []; // Lý do
  listReasonLeave = []; // Loại đơn
  employeeId?: number; // id user login
  listCaculateLeaves = [];
  requestId: number;
  isChanged = false;
  valueBeforeChange = null;
  screenMode = Constant.SCREEN_MODE.CREATE;
  subjectCalulateLeaves = new BehaviorSubject<CaculateLeavesResponse[]>([]);
  isFileEmpty = false;
  constructor(
    private fb: FormBuilder,
    private staffInfoService: StaffInfoService,
    private requestsService: RequestService,
    private reasonLeavesService: ReasonLeaveService,
    private toastrService: ToastrService,
    private translate: TranslateService,
    private router: Router,
    private route: ActivatedRoute,
    private requestLeavesService: RequestLeavesService,
    private downloadFileAttachService: DownloadFileAttachService,
    private modalRef: NzModalRef,

  ) {
    this.handleGetCatalog();
    const { requestId } = this.route.snapshot.queryParams;
    this.requestId = requestId;
    this.form = this.fb.group({
      requestId: [this.requestId],
      leaveType: [Constant.LEAVE_TYPE.LEAVE],
      employeeId: [null, [Validators.required]],
      employeeCode: [''],
      fullName: [''],
      reasonDetail: ['', Validators.maxLength(2000)],
      isCommitted: [null],
      status: [Constant.REQUEST_STATUS.APPROVED],
      allDays: [0], // số ngày nghỉ
      totalDays: [0], // số ngày làm việc nghỉ
      listAbsRequestLeaves: this.fb.array([
        this._createReason(null)
      ], {
        validators: [this.validatorFormReason()]
      }),
      listAbsRequestSupporters: this.fb.array([
        this._createSupporter(null)
      ], {
        validators: [this.validatorFormSupporter(), this.validateSupporterDuplicateEmployee()]
      }),
      pageName: [Constant.PAGE_NAME.ADMIN]
    });
  }

  ngOnInit(): void {
    this.innitData();
  }

  ngAfterViewInit() {
    const formInput = document.getElementById(this.formId);
    if (formInput) {
      formInput.addEventListener('input', this._detectClickItemOrChangeInput);
      formInput.addEventListener('mousedown', this._detectClickItemOrChangeInput);
    }
    this.subjectCalulateLeaves.subscribe((calLeaves) => {
      this._caculateLeaves(calLeaves);
    });
  }

  ngOnDestroy() {
    const formInput = document.getElementById(this.formId);
    if (formInput) {
      formInput.removeEventListener('input', this._detectClickItemOrChangeInput);
      formInput.removeEventListener('mousedown', this._detectClickItemOrChangeInput);
    }
  }

  _detectClickItemOrChangeInput = (event: any) => {
    this.isChanged = true;
  }

  get f() { return this.form.controls; }

  get reasons() {
    return this.f['listAbsRequestLeaves'] as FormArray;
  }

  get supporters() {
    return this.f['listAbsRequestSupporters'] as FormArray;
  }

  /**
   * Tính Số ngày nghỉ + Tổng số ngày nghỉ
   * Người xét duyệt + Người phê duyệt
   * @param calLeaves
   */
  _caculateLeaves(calLeaves: CaculateLeavesResponse[]) {
    if (!this.isChanged) {
      return;
    }
    if (calLeaves && calLeaves.length > 0) {
      let _allDays = 0;
      let _totalDays = 0;
      calLeaves.forEach((item: CaculateLeavesResponse) => {
        _allDays += item?.allDays || 0;
        _totalDays += item?.totalDays || 0;
      });
      this.f['allDays'].setValue(_allDays);
      this.f['totalDays'].setValue(_totalDays);
    } else {
      this.f['allDays'].setValue(0);
      this.f['totalDays'].setValue(0);
    }
  }

  /**
   * Xử lý lấy các datasource
   */
  handleGetCatalog() {
    this.getListReasonLeave();
    this.getUserLogin();
  }

  /**
   * Lấy danh mục Loại đơn
   */
  getListReasonLeave() {
    this.subs.push(
      this.reasonLeavesService.getAllReasonLeaves(Constant.LEAVE_TYPE.LEAVE).subscribe(res => {
        const response: BaseResponse = res;
        this.listReasonLeave = response.data;
      })
    );
  }

  /**
   * Lấy userlogin
   */
  getUserLogin() {
    this.subs.push(
      this.requestsService.getUserLogin().subscribe(res => {
        const response: BaseResponse = res;
        this.employeeId = response.data;
      })
    );
  }

  /**
   * Tạo Loại đơn
   * @param data
   * @returns
   */
  _createReason(data: any) {
    const reason = this.fb.group({
      reasonLeaveId: [null, [Validators.required]],
      fromTime: [null, [Validators.required]],
      toTime: [null, [Validators.required]],
    }, {
      validators: [DateValidator.validateTwoDateTime('fromTime', 'toTime')]
    });
    if (data) {
      reason.patchValue(data);
    }
    return reason;
  }

  /**
   * validateReasonDuplication
   */
  validatorFormReason(): ValidatorFn | any {
    return (formArray: FormArray): { [key: string]: string } | null => {
      const rowNum = formArray.controls.length;
      const check = [];
      const isFail = [];
      for (let index = 0; index < rowNum; index++) {
        check[index] = false;
        isFail[index] = false;
      }
      for (let i = 0; i < rowNum - 1; i++) {
        if (!check[i]) {
          check[i] = true;
          for (let j = i + 1; j < rowNum; j++) {
            if (this._isDuplicate(formArray.controls[i].value, formArray.controls[j].value)) {
              check[j] = true;
              isFail[i] = true;
              isFail[j] = true;
              formArray.controls[i].get('fromTime')?.setErrors({ 'dublicateTime': true });
              formArray.controls[i].get('toTime')?.setErrors({ 'dublicateTime': true });
              formArray.controls[j].get('fromTime')?.setErrors({ 'dublicateTime': true });
              formArray.controls[j].get('toTime')?.setErrors({ 'dublicateTime': true });
            } else {
              if (!isFail[i]) {
                formArray.controls[i].get('fromTime')?.setErrors(null);
                formArray.controls[i].get('toTime')?.setErrors(null);
                if (!formArray.controls[i].get('fromTime')?.value) {
                  formArray.controls[i].get('fromTime')?.setErrors({ 'required': true });
                }
                if (!formArray.controls[i].get('toTime')?.value) {
                  formArray.controls[i].get('toTime')?.setErrors({ 'required': true });
                }
              }
              if (!isFail[j]) {
                formArray.controls[j].get('fromTime')?.setErrors(null);
                formArray.controls[j].get('toTime')?.setErrors(null);
                if (!formArray.controls[j].get('fromTime')?.value) {
                  formArray.controls[j].get('fromTime')?.setErrors({ 'required': true });
                }
                if (!formArray.controls[j].get('toTime')?.value) {
                  formArray.controls[j].get('toTime')?.setErrors({ 'required': true });
                }
              }
            }
          }
        }
      }
      return null;
    }
  }

  /**
   * Check giao khoảng thời gian
   * @param value1
   * @param value2
   * @returns
   */
  _isDuplicate(value1: any, value2: any) {
    if (value1.fromTime && value1.toTime && value2.fromTime && value2.toTime) {
      const from1 = moment(value1.fromTime, SYSTEM_FORMAT_DATA.DATE_TIME_FORMAT).toDate().getTime();
      const to1 = moment(value1.toTime, SYSTEM_FORMAT_DATA.DATE_TIME_FORMAT).toDate().getTime();
      const from2 = moment(value2.fromTime, SYSTEM_FORMAT_DATA.DATE_TIME_FORMAT).toDate().getTime();
      const to2 = moment(value2.toTime, SYSTEM_FORMAT_DATA.DATE_TIME_FORMAT).toDate().getTime();
      return !(from2 >= to1 || to2 <= from1);
    }
    return false;
  }

  /**
 * Tạo người tiếp nhận
 * @param data
 * @returns
 */
  _createSupporter(data: any) {
    const supporter = this.fb.group({
      employeeId: [null, [Validators.required]],
      taskContent: [null, [Validators.required, Validators.maxLength(2000)]],
    })
    if (data) {
      supporter.patchValue(data);
    }
    return supporter;
  }

  validatorFormSupporter(): ValidatorFn | any {
    return (formArray: FormArray): { [key: string]: string } | null => {
      const arrayDataValid: any[] = [];
      formArray.controls.forEach((item: FormGroup | any, i) => {
        if (item.value.employeeId && item.value.employeeId['employeeId']) {
          const value = item.value.employeeId['employeeId'];
          const indexOf = arrayDataValid.indexOf(value);
          if (indexOf === -1) {
            arrayDataValid.push(value);
            const error = formArray.controls[i].get('employeeId')?.errors;
            formArray.controls[i].get('employeeId')?.setErrors(null);
            if(error?.['duplicate']){
              formArray.controls[i].get('employeeId')?.setErrors({ 'duplicateEmployee': true });
            }
            if(error?.['required']){
              formArray.controls[i].get('employeeId')?.setErrors({ 'required': true });
            }
          } else {
            formArray.controls[i].get('employeeId')?.setErrors({ 'duplicate': true });
            formArray.controls[indexOf].get('employeeId')?.setErrors({ 'duplicate': true });
          }
        }
      })
      return null;
    }
  }

  validateSupporterDuplicateEmployee(): ValidatorFn | any {
    return (formArray: FormArray): { [key: string]: string } | null => {
      if (!this.form) {
        return null;
      }
      const _employeeId = this.f['employeeId'] ? this.f['employeeId'].value : null
      formArray.controls.forEach((item: FormGroup | any, i) => {
        if (item.value.employeeId && item.value.employeeId['employeeId'] && _employeeId) {
          const value = item.value.employeeId['employeeId'];
          const valueEmployee = this.requestId ? _employeeId : _employeeId['employeeId'] ?? 0;
          if (value === valueEmployee) {
            formArray.controls[i].get('employeeId')?.setErrors({ 'duplicateEmployee': true });
          } else {
            const error = formArray.controls[i].get('employeeId')?.errors;
            formArray.controls[i].get('employeeId')?.setErrors(null);
            if(error?.['duplicate']){
              formArray.controls[i].get('employeeId')?.setErrors({ 'duplicate': true });
            }
            if(error?.['required']){
              formArray.controls[i].get('employeeId')?.setErrors({ 'required': true });
            }
            // formArray.controls[i].get('employeeId')?.setErrors({ 'duplicateEmployee': false });

          }
        }
      })
      return null;
    }
  }

  onChangeReason(idx: number) {
    const _reason = this.reasons.value[idx];
    let _employeeId = null;
    const request: Request | any = _.cloneDeep(this.form.value);
    if (request.employeeId && request.employeeId['employeeId']) {
      _employeeId = request.employeeId['employeeId'];
    }
    if (_reason && _reason.reasonLeaveId && _reason.fromTime && _reason.toTime && _employeeId) {
      _reason.employeeId = _employeeId;
      this.requestLeavesService.caculateLeaves(_reason).subscribe(res => {
        const currentCaculateLeaves = this.subjectCalulateLeaves.value as Array<CaculateLeavesResponse>;
        currentCaculateLeaves[idx] = res.data;
        this.subjectCalulateLeaves.next(currentCaculateLeaves);
      });
    } else {
      const currentCaculateLeaves: any = this.subjectCalulateLeaves.value as Array<CaculateLeavesResponse>;
      currentCaculateLeaves[idx] = null;
      this.subjectCalulateLeaves.next(currentCaculateLeaves);
    }
  }

  /**
   * Thêm Loại đơn
   */
  addReason() {
    this.reasons.push(this._createReason(null))
  }

  /**
   * Xóa loại đơn
   * @param index
   */
  removeReason(index: number) {
    if (this.reasons.length > 1)
      this.reasons.removeAt(index);
    else {
      this.reasons.removeAt(index);
      this.addReason();
    }
    const currentCaculateLeaves = this.subjectCalulateLeaves.value;
    if (currentCaculateLeaves[index]) {
      currentCaculateLeaves.splice(index, 1);
      this.subjectCalulateLeaves.next(currentCaculateLeaves);
    }
  }

  /**
   * Thêm người nhận bàn giao
   */
  addSupporter() {
    this.supporters.push(this._createSupporter(null));
  }

  /**
   * Xóa người nhận bàn giao
   * @param index
   */
  removeSupporter(index: number) {
    if (this.supporters.length > 1)
      this.supporters.removeAt(index);
    else {
      this.supporters.removeAt(index);
      this.addSupporter();
    }
  }

  beforeUpload = (file: NzUploadFile): boolean => {
    this.fileList =  beforeUploadFile(file, this.fileList, 3000000, this.toastrService, this.translate, 'staffAbs.notification.errorFile',
      ".zip", ".rar", ".7z", ".pdf", ".png", ".jpg", ".jpeg", ".bmp"
    )
    this.isFileEmpty = false;
    return false;
  }

  close(isSearch?: boolean) {
    this.modalRef.close(isSearch)
  }

  /**
   * Lưu đơn
   */
  onSave() {
    this.isSubmitted = true;
    // this.validateReasonDuplication();
    if (this.fileList.length === 0) this.isFileEmpty = true;
    if (this.form.valid && this.fileList.length > 0) {
      const request = _.cloneDeep(this.form.value);
      if (request.employeeId && request.employeeId['employeeId']) {
        request.employeeId = request.employeeId['employeeId'];
      }
      request.listAbsRequestSupporters = request.listAbsRequestSupporters.map((e: any) => ({
        employeeId: e.employeeId['employeeId'],
        taskContent: e.taskContent
      }))
      request.status = Constant.REQUEST_STATUS.APPROVED;
      request.files = this.fileList;
      request.docIdsDelete = this.docIdsDelete;
      this.isLoading = true;
      this.requestsService.saveOrUpdateRequestLeave(request).subscribe((res: BaseResponse) => {
        this.isLoading = false;
        if (res.code === HTTP_STATUS_CODE.OK) {
          const msg = !this.requestId ? 'common.notification.addSuccess' : 'common.notification.updateSuccess';
          this.toastrService.success(this.translate.instant(msg));
          this.close(true);
        } else {
          this.toastrService.error(this.translate.instant('common.notification.updateError') + ":" + res.message);
        }
      })
    }
  }

  innitData() {
    if (!this.id) {
      return;
    }
    this.requestsService.findById(this.id).subscribe((res: BaseResponse) => {
      if (CommonUtils.isSuccessRequest(res)) {
        // transform employeeId data
        const { listAbsRequestLeaves, listAbsRequestSupporters } = res.data;
        res.data.employeeId = {
          employeeId: res.data.employeeId,
          employeeCode: res.data.employeeCode,
          fullName: res.data.fullName
        }
        this.form.patchValue(res.data);
        if (listAbsRequestLeaves) {
          this.reasons.clear();
          listAbsRequestLeaves.forEach((item: any) => {
            this.reasons.push(this._createReason(item));
          });
        }

        if(res.data?.files && res.data?.files.length > 0){
          res.data.files.forEach((file: any) => {
            this.fileList.push({
              uid: file.docId,
              name: file.fileName,
              status: 'done',
            })
          });
        }

        this.fileList = _.cloneDeep(this.fileList);
        if (listAbsRequestSupporters) {
          this.supporters.clear();
          listAbsRequestSupporters.forEach((item: any) => {
            const empl = new EmployeeDetail();
            empl.employeeId = item.employeeId;
            empl.fullName = item.fullName;
            empl.employeeCode = item.employeeCode;
            this.supporters.push(this._createSupporter({
              ...item,
              employeeId: empl
            }));
          });
        }
      } else {
        this.toastrService.error(res.message);
      }
    })
  }

  changeEmployee(evt: any) {
    let _employeeId = null;
    if (evt && this.reasons?.length > 0) {
      _employeeId = evt.employeeId;
      const listPromise = [];
      for (let idx = 0; idx < this.reasons.value.length; idx++) {
        const _reason = this.reasons.value[idx];
        if (_reason && _reason.reasonLeaveId && _reason.fromTime && _reason.toTime && _employeeId) {
          _reason.employeeId = _employeeId;
          listPromise.push(this.requestLeavesService.caculateLeaves(_reason));
        }
      }
      forkJoin(listPromise).subscribe((res: BaseResponse[]) => {
        if (res?.length > 0) {
          const _reasons = this.reasons.value;
          const currentCaculateLeaves = this.subjectCalulateLeaves.value as Array<CaculateLeavesResponse>;
          for (let i = 0; i < res.length; i++) {
            const cal = res[i].data;
            const idx = _reasons.findIndex((e: any) => e.reasonLeaveId == cal.reasonLeaveId
              && e.fromTime == cal.fromTime
              && e.toTime == cal.toTime);
            if (idx >= 0) {
              currentCaculateLeaves[idx] = res[i].data;
            }
          }
          this.subjectCalulateLeaves.next(currentCaculateLeaves);
        }
      })
    }
  }

  downloadFile = (file: NzUploadFile) => {
    this.downloadFileAttachService.doDownloadAttachFile(Number(file.uid)).subscribe(res => {
      const reportFile = new Blob([res], {type: getTypeExport(file.filename?.split(".").pop() ?? '')});
      saveAs(reportFile, file.filename);
    });
  }

  removeFile = (file: NzUploadFile): boolean => {
    this.docIdsDelete.push(Number(file.uid));
    const index = this.fileList.indexOf(file);
    this.fileList.splice(index, 1);
    if (this.fileList.length === 0) {
      this.isFileEmpty = true;
    }
    return true;
  }
}
