import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RequestsLeaveFormComponent} from "./requests-leave-form/requests-leave-form.component";
import {NzFormModule} from "ng-zorro-antd/form";
import {ReactiveFormsModule} from "@angular/forms";
import {SharedDirectivesScrollSpyModule} from "@hcm-mfe/shared/directives/scroll-spy";
import {SharedUiEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/employee-data-picker";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiMbDateTimeWorkModule} from "@hcm-mfe/shared/ui/mb-date-time-work";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiMbTextLabelModule} from "@hcm-mfe/shared/ui/mb-text-label";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {NzUploadModule} from "ng-zorro-antd/upload";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzModalModule} from "ng-zorro-antd/modal";
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';

@NgModule({
  imports: [CommonModule, NzFormModule, SharedUiLoadingModule, ReactiveFormsModule, SharedDirectivesScrollSpyModule, SharedUiEmployeeDataPickerModule,
    TranslateModule, SharedUiMbSelectModule, SharedUiMbDateTimeWorkModule, SharedUiMbButtonModule, SharedUiMbTextLabelModule,
    SharedUiMbInputTextModule, NzUploadModule, NzIconModule, NzButtonModule, NzModalModule],
  declarations: [RequestsLeaveFormComponent],
  exports: [RequestsLeaveFormComponent],
})
export class StaffAbsFeatureTimekeepingRequestsLeaveFormModule {}
