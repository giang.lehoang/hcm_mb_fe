import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {WorkEarlyFormComponent} from "./work-early-form/work-early-form.component";
import {NzFormModule} from "ng-zorro-antd/form";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedUiEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/employee-data-picker";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {NzUploadModule} from "ng-zorro-antd/upload";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzModalModule} from "ng-zorro-antd/modal";
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';

@NgModule({
  imports: [CommonModule, NzFormModule, SharedUiLoadingModule, ReactiveFormsModule, FormsModule, SharedUiEmployeeDataPickerModule,
    TranslateModule, SharedUiMbSelectModule, SharedUiMbDatePickerModule, SharedUiMbInputTextModule, NzUploadModule,
    NzIconModule, NzButtonModule, NzModalModule],
  declarations: [WorkEarlyFormComponent],
  exports: [WorkEarlyFormComponent]
})
export class StaffAbsFeatureTimekeepingWorkEarlyFormModule {}
