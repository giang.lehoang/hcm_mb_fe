import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WorkCalendarPageComponent } from './work-calendar-page/work-calendar-page.component';
import { NzFormModule } from 'ng-zorro-antd/form';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { SharedUiMbTableWrapModule } from '@hcm-mfe/shared/ui/mb-table-wrap';
import { SharedUiMbTableModule } from '@hcm-mfe/shared/ui/mb-table';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';
import { RouterModule } from '@angular/router';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { StaffAbsUiCalendarModule } from '@hcm-mfe/staff-abs/ui/calendar';
import { StaffAbsUiWorkCalendarDetailImportModule } from '@hcm-mfe/staff-abs/ui/work-calendar-detail-import';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';

@NgModule({
  imports: [
    CommonModule,
    NzFormModule,
    ReactiveFormsModule,
    FormsModule,
    SharedUiMbInputTextModule,
    TranslateModule,
    SharedUiMbButtonModule,
    SharedUiMbTableWrapModule,
    SharedUiMbTableModule,
    NzPopconfirmModule,
    SharedUiMbSelectModule,
    StaffAbsUiCalendarModule,
    StaffAbsUiWorkCalendarDetailImportModule,
    SharedUiLoadingModule,
    RouterModule.forChild([
      {
        path: '',
        component: WorkCalendarPageComponent,
      },
    ]),
  ],
  declarations: [WorkCalendarPageComponent],
  exports: [WorkCalendarPageComponent],
})
export class StaffAbsFeatureCategoryWorkCalendarPageModule {}
