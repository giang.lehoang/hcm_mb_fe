import { Component, Injector, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BaseResponse } from '@hcm-mfe/self-service/data-access/models';
import {
  HTTP_STATUS_CODE,
  SYSTEM_FORMAT_DATA,
} from '@hcm-mfe/shared/common/constants';
import { CatalogModel, SelectModal } from '@hcm-mfe/shared/data-access/models';
import { SearchBaseComponent } from '@hcm-mfe/staff-abs/data-access/common';
import { WorkCalendars } from '@hcm-mfe/staff-abs/data-access/models';
import {
  WorkCalendarDetailService,
  WorkCalendarService,
} from '@hcm-mfe/staff-abs/data-access/services';
import { WorkCalendarFormComponent } from '@hcm-mfe/staff-abs/feature/category/work-calendar-form';
import { WorkCalendarOrgComponent } from '@hcm-mfe/staff-abs/feature/category/work-calendar-org';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { NzModalService } from 'ng-zorro-antd/modal';

@Component({
  selector: 'hcm-mfe-work-calendar-page',
  templateUrl: './work-calendar-page.component.html',
  styleUrls: ['./work-calendar-page.component.scss'],
})
export class WorkCalendarPageComponent extends SearchBaseComponent implements OnInit {
  form: FormGroup;
  isSubmitted = false;
  workCalendarList: WorkCalendars[] = [];
  mapWorkCalendarDetails: any;
  listYear: CatalogModel[] = [];
  listMonth: any = [];
  workCalendars: WorkCalendars = {};
  isImportData = false;
  isLoading = false;

  constructor(
    injector: Injector,
    private _fb: FormBuilder,
    public workCalendarService: WorkCalendarService,
    public workCalendarDetailService: WorkCalendarDetailService,
    private modalServices: NzModalService,
    public translateService: TranslateService,
  ) {
    super(injector);
    this.form = this._fb.group({
      workCalendarId: [null, [Validators.required]],
      year: [
        parseInt(moment(new Date()).format('YYYY')),
        [Validators.required],
      ],
    });
    this.listYear = this.getYearList();
  }

  ngOnInit(): void {
    this.loadWorkCalendarDataSource(null);
  }

  loadWorkCalendarDataSource(workCalendarId: number | null) {
    this.workCalendarService.getActiveWorkCalendars().subscribe(
      (res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.workCalendarList = res.data;
          if (!!res.data && res.data.length > 0) {
            if (!workCalendarId) {
              this.form.controls['workCalendarId'].setValue(
                res.data[0].workCalendarId
              );
            }
            this.getWorkCalendarDetail();
          }
        }
      },
      () => {
        this.workCalendarList = [];
      }
    );
  }

  getWorkCalendarDetail() {
    const workCalendarId = this.form.controls['workCalendarId'].value;
    this.mapWorkCalendarDetails = null;
    const year = this.form.controls['year'].value;
    const fromDate = moment('01/01/' + year, 'DD/MM/YYYY')
      .startOf('week')
      .add(1, 'day');
    const toDate = moment('01/12/' + year, 'DD/MM/YYYY')
      .startOf('week')
      .add(42, 'day');
    const formData = {
      workCalendarId: workCalendarId,
      year: year,
      fromDate: fromDate.format(SYSTEM_FORMAT_DATA.DATE_FORMAT),
      toDate: toDate.format(SYSTEM_FORMAT_DATA.DATE_FORMAT),
    };
    this.isLoading = true;
    this.workCalendarDetailService.search(formData).subscribe(
      (res) => {
        const map: any = {};
        res.data.forEach((e: any) => (map[e.dateTimekeeping] = e));
        this.mapWorkCalendarDetails = map;
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );

    this.workCalendarService
      .getRecord(workCalendarId)
      .subscribe((res: BaseResponse) => {
        this.workCalendars = res.data;
      });
    this.initListMonthSearch();
  }

  getYearList() {
    const listYear = [];
    const currentYear = new Date().getFullYear();
    for (let i = currentYear - 50; i <= currentYear + 50; i++) {
      listYear.push(new CatalogModel(i.toString(), i));
    }
    return listYear;
  }

  initListMonthSearch() {
    const year = this.form.controls['year'].value;
    this.listMonth = [
      '01/' + year,
      '02/' + year,
      '03/' + year,
      '04/' + year,
      '05/' + year,
      '06/' + year,
      '07/' + year,
      '08/' + year,
      '09/' + year,
      '10/' + year,
      '11/' + year,
      '12/' + year,
    ];
  }

  onChangeWorkCalendar() {
    this.getWorkCalendarDetail();
  }

  onChangeYear() {
    this.getWorkCalendarDetail();
  }

  showModalUpdate(workCalendarId: number | any, footerTmpl: TemplateRef<{}>) {
    this.modal = this.modalServices.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
      nzTitle: workCalendarId ? this.translateService.instant('staffAbs.workCalendar.update') :  this.translateService.instant('staffAbs.workCalendar.add'),
      nzContent: WorkCalendarFormComponent,
      nzComponentParams: {
        workCalendarId: workCalendarId
      },
      nzFooter: footerTmpl
    });
    this.modal.afterClose.subscribe(result => result?.refresh ? this.reloadAfterSave(workCalendarId) : '');
  }

  reloadAfterSave(workCalendarId: number) {
    this.loadWorkCalendarDataSource(workCalendarId);
  }

  showModalOrg(footerTmpl: TemplateRef<{}>) {
    this.modal = this.modalServices.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.1 > 1600 ? 1600 : window.innerWidth / 1.1 : window.innerWidth,
      nzTitle: this.translateService.instant('staffAbs.workCalendarPage.org'),
      nzContent: WorkCalendarOrgComponent,
      nzComponentParams: {
        workCalendarId: this.form.controls['workCalendarId'].value
      },
      nzFooter: footerTmpl
    });
    // this.modal.afterClose.subscribe(result => result?.refresh ? this.reloadAfterSave(workCalendarId) : '');
  }

  showModalImport() {
    this.isImportData = true;
  }

  doCloseImport(isSearch: boolean) {
    this.isImportData = false;
    isSearch && this.getWorkCalendarDetail();
  }
}
