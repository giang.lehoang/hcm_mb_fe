import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkCalendarPageComponent } from './work-calendar-page.component';

describe('WorkCalendarPageComponent', () => {
  let component: WorkCalendarPageComponent;
  let fixture: ComponentFixture<WorkCalendarPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkCalendarPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkCalendarPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
