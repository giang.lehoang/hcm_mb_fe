import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {WorkCalendarComponent} from "./work-calendar/work-calendar.component";
import {NzFormModule} from "ng-zorro-antd/form";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {NzPopconfirmModule} from "ng-zorro-antd/popconfirm";
import {RouterModule} from "@angular/router";
import {NzModalModule} from "ng-zorro-antd/modal";

@NgModule({
  imports: [CommonModule, NzFormModule, ReactiveFormsModule, FormsModule, SharedUiMbInputTextModule, TranslateModule,
    SharedUiMbButtonModule, SharedUiMbTableWrapModule, SharedUiMbTableModule, NzPopconfirmModule, NzModalModule,
    RouterModule.forChild([
      {
        path: '',
        component: WorkCalendarComponent
      }
    ])
  ],
  declarations: [WorkCalendarComponent],
  exports: [WorkCalendarComponent],
})
export class StaffAbsFeatureCategoryWorkCalendarModule {}
