import { Component, Injector, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { NzModalService } from 'ng-zorro-antd/modal';
import { ToastrService } from 'ngx-toastr';
import {Constant, SearchBaseComponent} from "@hcm-mfe/staff-abs/data-access/common";
import {WorkCalendars} from "@hcm-mfe/staff-abs/data-access/models";
import {BaseResponse, RegionCategory, ResponseEntity} from "@hcm-mfe/shared/data-access/models";
import {CommonUtils, StaffInfoService, ValidateService} from "@hcm-mfe/shared/core";
import {WorkCalendarService} from "@hcm-mfe/staff-abs/data-access/services";
import {WorkCalendarFormComponent} from "@hcm-mfe/staff-abs/feature/category/work-calendar-form";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";

@Component({
  selector: 'app-work-calendar',
  templateUrl: './work-calendar.component.html',
  styleUrls: ['./work-calendar.component.scss']
})
export class WorkCalendarComponent extends SearchBaseComponent implements OnInit {
  items: WorkCalendars[] = [];
  groupCodeList: RegionCategory[] = [];
  loading = false;
  @ViewChild('actionTmpl', { static: true }) action!: TemplateRef<NzSafeAny>;
  @ViewChild('monWorkTimeTemplate', { static: true }) monWorkTimeTemplate!: TemplateRef<NzSafeAny>;
  @ViewChild('tueWorkTimeTemplate', { static: true }) tueWorkTimeTemplate!: TemplateRef<NzSafeAny>;
  @ViewChild('wedWorkTimeTemplate', { static: true }) wedWorkTimeTemplate!: TemplateRef<NzSafeAny>;
  @ViewChild('thuWorkTimeTemplate', { static: true }) thuWorkTimeTemplate!: TemplateRef<NzSafeAny>;
  @ViewChild('friWorkTimeTemplate', { static: true }) friWorkTimeTemplate!: TemplateRef<NzSafeAny>;
  @ViewChild('satWorkTimeTemplate', { static: true }) satWorkTimeTemplate!: TemplateRef<NzSafeAny>;
  @ViewChild('sunWorkTimeTemplate', { static: true }) sunWorkTimeTemplate!: TemplateRef<NzSafeAny>;

  formSearchConfig = {
      name: [null],
  }
  count = 0;
  constructor(
    injector: Injector,
    public validateService: ValidateService,
    public workCalendarService: WorkCalendarService,
    public toastr: ToastrService,
    private modalServices: NzModalService,
    public translateService: TranslateService,
    private staffInfoService: StaffInfoService,
    private router: Router,
    ) {
      super(injector);
      this.modalComponent = WorkCalendarFormComponent;
      this.setMainService(workCalendarService);
      this.buildFormSearch(this.formSearchConfig);
    }

  ngOnInit(): void {
    this.staffInfoService.getCatalog(Constant.CatalogType.LOAI_HINH_CHAM_CONG).subscribe((data: ResponseEntity<RegionCategory>) => {
      this.groupCodeList = data.data ?? [];
    });

    this.initTable();
    this.search(undefined);
  }
  search(pageNumber?: number) {
    this.pagination.pageNumber = pageNumber ?? 1;
    this.tableConfig.pageIndex = pageNumber ? this.tableConfig.pageIndex: 1;
    this.tableConfig.loading = true;
    const params = this.formSearch ? {...this.formSearch.value} : {};
    const data = { ...params, ...this.pagination.getCurrentPage()}
    this.workCalendarService.search(data).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.items = res.data.listData;
          this.tableConfig.total = res.data.count;
        }
        this.tableConfig.loading  = false
      }, () => {
        this.tableConfig.loading  = false
        this.items = []
    })
  }
  public override doDelete(id: number): void {
    this.workCalendarService.deleteRecord(id).subscribe(res => {
      if (CommonUtils.isSuccessRequest(res)) {
        this.pagination.pageNumber = 1;
        this.search(undefined);
        this.toastr.success(this.translateService.instant('common.notification.deleteSuccess'));
      }
    })
  }
  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'staffAbs.workCalendar.name',
          field: 'name',
          width: 150,
          needBreakword: true,
          fixed: window.innerWidth > 1024,
          thClassList: ['text-center'],
          fixedDir: 'left'
        },
        {
          title: 'staffAbs.workCalendar.monWorkTime',
          field: 'monWorkTime',
          width: 60,
          needBreakword: true,
          fixed: window.innerWidth > 1024,
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          tdTemplate: this.monWorkTimeTemplate,
          fixedDir: 'left'
        },
        {
          title: 'staffAbs.workCalendar.tueWorkTime',
          field: 'tueWorkTime',
          width: 60,
          needBreakword: true,
          fixed: window.innerWidth > 1024,
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          tdTemplate: this.tueWorkTimeTemplate,
          fixedDir: 'left'
        },
        {
          title: 'staffAbs.workCalendar.wedWorkTime',
          field: 'wedWorkTime',
          width: 60,
          needBreakword: true,
          fixed: window.innerWidth > 1024,
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          tdTemplate: this.wedWorkTimeTemplate,
          fixedDir: 'left'
        },
        {
          title: 'staffAbs.workCalendar.thuWorkTime',
          field: 'thuWorkTime',
          width: 60,
          needBreakword: true,
          fixed: window.innerWidth > 1024,
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          tdTemplate: this.thuWorkTimeTemplate,
          fixedDir: 'left'
        },
        {
          title: 'staffAbs.workCalendar.friWorkTime',
          field: 'friWorkTime',
          width: 60,
          needBreakword: true,
          fixed: window.innerWidth > 1024,
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          tdTemplate: this.friWorkTimeTemplate,
          fixedDir: 'left'
        },
        {
          title: 'staffAbs.workCalendar.satWorkTime',
          field: 'satWorkTime',
          width: 60,
          needBreakword: true,
          fixed: window.innerWidth > 1024,
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          tdTemplate: this.satWorkTimeTemplate,
          fixedDir: 'left'
        },
        {
          title: 'staffAbs.workCalendar.sunWorkTime',
          field: 'sunWorkTime',
          width: 70,
          needBreakword: true,
          fixed: window.innerWidth > 1024,
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          tdTemplate: this.sunWorkTimeTemplate,
          fixedDir: 'left'
        },
        {
          title: 'staffAbs.workCalendar.defaultHodidayDate',
          field: 'defaultHodidayDate',
          thClassList: ['text-center'],
          width: 150
        },
        {
          title: 'staffAbs.staffResearch.projectHis.table.action',
          field: 'action',
          tdClassList: ['text-nowrap', 'text-center'],
          thClassList: ['text-nowrap', 'text-center'],
          width: 150,
          tdTemplate: this.action,
          fixed: window.innerWidth > 1024,
          fixedDir: 'right'
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }
  showModalUpdate(workCalendarId: number, footerTmpl: TemplateRef<{}>) {
    this.modal = this.modalServices.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
      nzTitle: workCalendarId ? this.translateService.instant('staffAbs.workCalendar.update') :  this.translateService.instant('staffAbs.workCalendar.add'),
      nzContent: WorkCalendarFormComponent,
      nzComponentParams: {
        workCalendarId: workCalendarId
      },
      nzFooter: footerTmpl
    });
    this.modal.afterClose.subscribe(result => result?.refresh ? this.search(undefined): '')
  }

  showDetaiCalendar(workCalendarId: any) {
    this.router.navigate(['/staff-abs/category/work-calendar-detail'], {
      queryParams: { workCalendarId: workCalendarId }
    });
  }
}
