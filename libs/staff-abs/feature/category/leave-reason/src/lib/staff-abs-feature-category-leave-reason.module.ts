import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LeaveReasonComponent} from "./leave-reason/leave-reason.component";
import {NzFormModule} from "ng-zorro-antd/form";
import {ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {NzPopconfirmModule} from "ng-zorro-antd/popconfirm";
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [CommonModule, NzFormModule, ReactiveFormsModule, SharedUiMbSelectModule, TranslateModule, SharedUiMbInputTextModule,
    SharedUiMbButtonModule, SharedUiMbTableWrapModule, SharedUiMbTableModule, NzPopconfirmModule,
    RouterModule.forChild([
      {
        path: '',
        component: LeaveReasonComponent
      }
    ])
  ],
  declarations: [LeaveReasonComponent],
  exports: [LeaveReasonComponent],
})
export class StaffAbsFeatureCategoryLeaveReasonModule {}
