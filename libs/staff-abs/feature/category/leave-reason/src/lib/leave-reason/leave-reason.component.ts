import { Component, Injector, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { NzModalService } from 'ng-zorro-antd/modal';
import { ToastrService } from 'ngx-toastr';
import {Constant, SearchBaseComponent} from "@hcm-mfe/staff-abs/data-access/common";
import {ReasonLeave, WorkdayType} from "@hcm-mfe/staff-abs/data-access/models";
import {BaseResponse, RegionCategory, ResponseEntity} from "@hcm-mfe/shared/data-access/models";
import {CommonUtils, StaffInfoService, ValidateService} from "@hcm-mfe/shared/core";
import {ReasonLeaveService} from "@hcm-mfe/staff-abs/data-access/services";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {LeaveReasonFormComponent} from "@hcm-mfe/staff-abs/feature/category/leave-reason-form";

@Component({
  selector: 'app-leave-reason',
  templateUrl: './leave-reason.component.html',
  styleUrls: ['./leave-reason.component.scss']
})
export class LeaveReasonComponent extends SearchBaseComponent implements OnInit {
  items: ReasonLeave[] = [];
  groupCodeList: RegionCategory[] = [];
  workDayTypeList: WorkdayType[] = [];
  loading = false;
  @ViewChild('actionTmpl', { static: true }) action!: TemplateRef<NzSafeAny>;
  @ViewChild('timeOffTypeTemplate', { static: true }) timeOffTypeTemplate!: TemplateRef<NzSafeAny>;

  formSearchConfig = {
      groupCode: [null],
      workdayTypeCode: [null],
      code: [null],
      name: [null],
  }
  constructor(
    injector: Injector,
    public validateService: ValidateService,
    public reasonLeaveService: ReasonLeaveService,
    public toastr: ToastrService,
    private modalServices: NzModalService,
    public translateService: TranslateService,
    private staffInfoService: StaffInfoService
    ) {
      super(injector);
      this.modalComponent = LeaveReasonFormComponent;
      this.setMainService(reasonLeaveService);
      this.buildFormSearch(this.formSearchConfig);
    }

  ngOnInit(): void {
    this.initDropDowmData();
    this.initTable();
    this.search(undefined);
  }
  initDropDowmData() {
    // loại hình chấm công
    this.staffInfoService.getCatalog(Constant.CatalogType.LOAI_HINH_CHAM_CONG).subscribe((data: ResponseEntity<RegionCategory>) => {
      this.groupCodeList = data.data ?? [];
    });

    // Ký hiệu công
    this.reasonLeaveService.getAllWorkDayType().subscribe(res => {
      this.workDayTypeList = res.data;
    })
  }

  search(pageNumber?: number) {
    this.pagination.pageNumber = pageNumber ?? 1;
    this.tableConfig.pageIndex = pageNumber ? this.tableConfig.pageIndex: 1;
    this.tableConfig.loading  = true;
    const params = this.formSearch ? {...this.formSearch.value} : {};
    const data = { ...params, ...this.pagination.getCurrentPage()}
    // Nạp dữ liệu tìm kiếm
    this.reasonLeaveService.search(data).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.items = res.data.listData;
          if(this.items) {
            this.items = this.items.map(item => ({...item, workdayTypeCodeAndName: `${item.workdayTypeCode} (${item.workdayTypeName})`}))
          }
          this.tableConfig.total = res.data.count;
        }
        this.tableConfig.loading  = false
      }, error => {
        this.tableConfig.loading  = false
        this.items = []
    })
  }
  public override doDelete(id: number): void {
    this.reasonLeaveService.deleteRecord(id).subscribe(res => {
      if (CommonUtils.isSuccessRequest(res)) {
        this.pagination.pageNumber = 1;
        this.search(undefined);
        this.toastr.success(this.translateService.instant('common.notification.deleteSuccess'));
      }
    })
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'staffAbs.leaveReason.code',
          field: 'code',
          width: 150,
          needBreakword: true,
          fixed: window.innerWidth > 1024,
          thClassList: ['text-center'],
          fixedDir: 'left'
        },
        {
          title: 'staffAbs.leaveReason.name',
          field: 'name',
          width: 150,
          needBreakword: true,
          fixed: window.innerWidth > 1024,
          thClassList: ['text-center'],
          fixedDir: 'left'
        },
        {
          title: 'staffAbs.leaveReason.workdayTypeCode',
          field: 'workdayTypeCodeAndName',
          tdClassList: ['text-left'],
          thClassList: ['text-center'],
          width: 150
        },
        {
          title: 'staffAbs.leaveReason.groupCode',
          field: 'groupName',
          tdClassList: ['text-left'],
          thClassList: ['text-center'],
          width: 150
        },
        {
          title: 'staffAbs.leaveReason.timeOffType',
          field: 'timeOffType',
          tdTemplate: this.timeOffTypeTemplate,
          tdClassList: ['text-left'],
          thClassList: ['text-center'],
          width: 150,
        },
        {
          title: 'staffAbs.leaveReason.maxTimeOff',
          field: 'maxTimeOff',
          tdClassList: ['text-right'],
          thClassList: ['text-center'],
          width: 150,
        },
        {
          title: 'staffAbs.leaveReason.yearMaxTimeOff',
          field: 'yearMaxTimeOff',
          tdClassList: ['text-right'],
          thClassList: ['text-center'],
          width: 150,
        },
        {
          title: 'staffAbs.staffResearch.projectHis.table.action',
          field: 'action',
          tdClassList: ['text-nowrap', 'text-center'],
          thClassList: ['text-nowrap', 'text-center'],
          width: 150,
          tdTemplate: this.action,
          fixed: window.innerWidth > 1024,
          fixedDir: 'right'
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  showModalUpdate(reasonLeaveId: number, footerTmpl: TemplateRef<{}>) {
    this.modal = this.modalServices.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
      nzTitle: reasonLeaveId ? this.translateService.instant('staffAbs.leaveReason.update') :  this.translateService.instant('staffAbs.leaveReason.add'),
      nzContent: LeaveReasonFormComponent,
      nzComponentParams: {
        reasonLeaveId: reasonLeaveId
      },
      nzFooter: footerTmpl
    });
    this.modal.afterClose.subscribe(result => result?.refresh ? this.search(undefined): '')
  }
}
