import { HttpParams } from '@angular/common/http';
import { Component, Injector, OnInit, TemplateRef } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormGroup,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { BaseResponse } from '@hcm-mfe/self-service/data-access/models';
import {
  HTTP_STATUS_CODE,
  SYSTEM_FORMAT_DATA,
} from '@hcm-mfe/shared/common/constants';
import { DateValidator } from '@hcm-mfe/shared/common/validators';
import { StaffInfoService, ValidationService } from '@hcm-mfe/shared/core';
import { Category } from '@hcm-mfe/shared/data-access/models';
import { Constant } from '@hcm-mfe/staff-abs/data-access/common';
import {
  WorkCalendarOrgs,
  WorkCalendars,
} from '@hcm-mfe/staff-abs/data-access/models';
import {
  MbPositionGroupService,
  WorkCalendarService,
} from '@hcm-mfe/staff-abs/data-access/services';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'hcm-mfe-work-calendar-org',
  templateUrl: './work-calendar-org.component.html',
  styleUrls: ['./work-calendar-org.component.scss'],
})
export class WorkCalendarOrgComponent implements OnInit {
  form: FormGroup;
  isSubmitted: boolean = false;
  workCalendarId: number = 0;
  listEmpType: Category[] = [];
  listPositionGroup = [];
  isLoading = false;

  constructor(
    private fb: FormBuilder,
    private modalRef: NzModalRef,
    public toastr: ToastrService,
    public translate: TranslateService,
    public workCalendarService: WorkCalendarService,
    private staffInfoService: StaffInfoService,
    public toastService: ToastrService,
    public positionGroupService: MbPositionGroupService
  ) {
    this.form = this.fb.group({
      workCalendarId: [null],
      listWorkCalendarOrgs: this.fb.array(
        [this.createWorkCalendarOrgsForm({})],
        { validators: [this.validatorDuplicateOrg()] }
      ),
    });
  }

  ngOnInit(): void {
    this.getListEmpType();
    this.getListPositionGroup();
    this.getRecord();
  }

  getRecord() {
    if (!this.workCalendarId) {
      return;
    }
    this.isLoading = true;
    this.workCalendarService
      .getRecord(this.workCalendarId)
      .subscribe((res: BaseResponse) => {
        if (res.code !== HTTP_STATUS_CODE.OK) {
          this.toastr.error(
            this.translate.instant('common.notification.updateError') +
              ': ' +
              res.message
          );
          this.modalRef.close({ refresh: false });
          return;
        }
        const formData: WorkCalendars = res.data;
        this.form.patchValue(formData);
        this.setFormValueWorkCalendarOrgs(formData.listWorkCalendarOrgs);
        this.isLoading = false;
      }, () => {
        this.isLoading = false;
      });
  }

  createWorkCalendarOrgsForm(data: any): FormGroup {
    const form = this.fb.group(
      {
        workCalendarOrgId: [null],
        org: [null, [Validators.required]],
        empType: [null],
        positionGroupId: [null],
        fromDate: [null, [Validators.required]],
        toDate: [null],
      },
      {
        validators: [DateValidator.validateTwoDateTime('fromDate', 'toDate')],
      }
    );

    // build value for org
    data.org = data.orgId
      ? {
          orgId: data.orgId,
          orgName: data.orgName,
          path: data.orgPath,
        }
      : null;

    form.patchValue(data);
    return form;
  }

  get listWorkCalendarOrgs(): FormArray {
    return this.form.get('listWorkCalendarOrgs') as FormArray;
  }

  setFormValueWorkCalendarOrgs(listWorkCalendarOrgsData?: WorkCalendarOrgs[]) {
    this.listWorkCalendarOrgs.clear();
    if (listWorkCalendarOrgsData && listWorkCalendarOrgsData.length > 0) {
      listWorkCalendarOrgsData.forEach((item) => {
        const { fromDate, toDate } = item;
        item.fromDate = fromDate ? moment(fromDate, SYSTEM_FORMAT_DATA.DATE_FORMAT).toDate() : fromDate;
        item.toDate = toDate ? moment(toDate, SYSTEM_FORMAT_DATA.DATE_FORMAT).toDate() : toDate;
        this.listWorkCalendarOrgs.push(this.createWorkCalendarOrgsForm(item))
      });
    } else {
      this.listWorkCalendarOrgs.push(this.createWorkCalendarOrgsForm({}));
    }
  }

  addOrgs() {
    this.listWorkCalendarOrgs.push(this.createWorkCalendarOrgsForm({}));
  }

  removeOrgs(index: number) {
    this.listWorkCalendarOrgs.removeAt(index);
  }

  // validate chọn trùng Tổ chức
  validatorDuplicateOrg(): ValidatorFn | any {
    return (formArray: FormArray): { [key: string]: string } | null => {
      formArray.controls.forEach((item: FormGroup | any, i) => {
        if (item.value.org) {
          const lstValueOrg = formArray.controls.filter((_formGroup) => {
            return item.value.org?.orgId == _formGroup.value.org?.orgId 
            && item.value.empType == _formGroup.value.empType
            && item.value.positionGroupId == _formGroup.value.positionGroupId;
          });
          if (lstValueOrg.length <= 1) {
            formArray.controls[i].get('org')?.setErrors(null);
            formArray.controls[i].get('empType')?.setErrors(null);
            formArray.controls[i].get('positionGroupId')?.setErrors(null);
          } else {
            formArray.controls[i].get('org')?.setErrors({ duplicate: true });
            formArray.controls[i].get('empType')?.setErrors({ duplicate: true });
            formArray.controls[i].get('positionGroupId')?.setErrors({ duplicate: true });
          }
        }
      });
      return null;
    };
  }

  getListEmpType() {
    this.staffInfoService.getCatalog(Constant.CATALOGS.DOI_TUONG_CV).subscribe(
      (res) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listEmpType = res.data;
        }
      },
      (error) => {
        this.toastService.error(error.message);
      }
    );
  }

  getListPositionGroup() {
    this.positionGroupService.getAllPositionGroups().subscribe(
      (res) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listPositionGroup = res.data;
        }
      },
      (error) => {
        this.toastService.error(error.message);
      }
    );
  }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      const formData: WorkCalendars = { ...this.form.value };

      // get orgId from org control
      const listWorkCalendarOrgs = formData.listWorkCalendarOrgs?.map((x) => ({
        ...x,
        orgId: x.org?.orgId,
        fromDate: x.fromDate
          ? moment(x.fromDate).format(SYSTEM_FORMAT_DATA.DATE_FORMAT)
          : null,
        toDate: x.toDate
          ? moment(x.toDate).format(SYSTEM_FORMAT_DATA.DATE_TIME_FORMAT)
          : null,
      }));
      formData.listWorkCalendarOrgs = listWorkCalendarOrgs;

      this.workCalendarService
        .assignWorkCalendar(this.workCalendarId, formData)
        .subscribe(
          (res: BaseResponse) => {
            if (res.code === HTTP_STATUS_CODE.OK) {
              this.toastr.success(
                this.translate.instant(
                  this.workCalendarId
                    ? 'common.notification.updateSuccess'
                    : 'common.notification.addSuccess'
                )
              );
              this.modalRef.close({ refresh: false });
            } else
              this.toastr.error(
                this.translate.instant(
                  this.workCalendarId
                    ? 'common.notification.updateError'
                    : 'common.notification.addError'
                ) +
                  ': ' +
                  res?.message
              );
          },
          (error) => {
            this.toastr.error(
              this.translate.instant(
                this.workCalendarId
                  ? 'common.notification.updateError'
                  : 'common.notification.addError'
              ) +
                ': ' +
                error.message
            );
          }
        );
    }
  }
}
