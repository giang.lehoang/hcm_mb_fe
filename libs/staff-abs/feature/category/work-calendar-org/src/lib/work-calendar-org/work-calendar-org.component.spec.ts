import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkCalendarOrgComponent } from './work-calendar-org.component';

describe('WorkCalendarOrgComponent', () => {
  let component: WorkCalendarOrgComponent;
  let fixture: ComponentFixture<WorkCalendarOrgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkCalendarOrgComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkCalendarOrgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
