import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WorkCalendarOrgComponent } from './work-calendar-org/work-calendar-org.component';
import { NzFormModule } from 'ng-zorro-antd/form';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { SharedUiMbTableWrapModule } from '@hcm-mfe/shared/ui/mb-table-wrap';
import { SharedUiMbTableModule } from '@hcm-mfe/shared/ui/mb-table';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { StaffAbsUiCalendarModule } from '@hcm-mfe/staff-abs/ui/calendar';
import { SharedUiOrgDataPickerModule } from '@hcm-mfe/shared/ui/org-data-picker';
import { SharedUiMbDatePickerModule } from '@hcm-mfe/shared/ui/mb-date-picker';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';

@NgModule({
  imports: [
    CommonModule,
    NzFormModule,
    ReactiveFormsModule,
    FormsModule,
    SharedUiMbInputTextModule,
    TranslateModule,
    SharedUiMbButtonModule,
    SharedUiMbTableWrapModule,
    SharedUiMbTableModule,
    NzPopconfirmModule,
    SharedUiMbSelectModule,
    StaffAbsUiCalendarModule,
    SharedUiOrgDataPickerModule,
    SharedUiMbDatePickerModule,
    SharedUiLoadingModule,
  ],
  declarations: [WorkCalendarOrgComponent],
  exports: [WorkCalendarOrgComponent],
})
export class StaffAbsFeatureCategoryWorkCalendarOrgModule {}
