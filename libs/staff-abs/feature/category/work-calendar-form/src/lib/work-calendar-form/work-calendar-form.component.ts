import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { ToastrService } from 'ngx-toastr';
import {CatalogModel, WorkCalendarOrgs, WorkCalendars} from "@hcm-mfe/staff-abs/data-access/models";
import {StaffInfoService, ValidationService} from "@hcm-mfe/shared/core";
import {WorkCalendarService} from "@hcm-mfe/staff-abs/data-access/services";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";

@Component({
  selector: 'app-work-calendar-form',
  templateUrl: './work-calendar-form.component.html',
  styleUrls: ['./work-calendar-form.component.scss']
})
export class WorkCalendarFormComponent implements OnInit {
  workTimeList: CatalogModel[] = [
    { label: this.translate.instant('staffAbs.workCalendar.workTime.ALL'), value: 'ALL' },
    { label: this.translate.instant('staffAbs.workCalendar.workTime.PM'), value: 'PM' },
    { label: this.translate.instant('staffAbs.workCalendar.workTime.AM'), value: 'AM' },
    { label: this.translate.instant('staffAbs.workCalendar.workTime.WEEKEND'), value: 'WEEKEND' }
  ]
  data = new WorkCalendars();
  form: FormGroup;
  workCalendarId?: number;
  isSubmitted = false;
  constructor(
    private fb: FormBuilder,
    private modalRef: NzModalRef,
    public toastr: ToastrService,
    public translate: TranslateService,
    private staffInfoService: StaffInfoService,
    public workCalendarService: WorkCalendarService,
  ) {
    this.form = this.fb.group({
      workCalendarId: [null],
      name: [null, [Validators.required, Validators.maxLength(200)]],
      monWorkTime: [null, [Validators.required]],
      tueWorkTime: [null, [Validators.required]],
      wedWorkTime: [null, [Validators.required]],
      thuWorkTime: [null, [Validators.required]],
      friWorkTime: [null, [Validators.required]],
      satWorkTime: [null, [Validators.required]],
      sunWorkTime: [null, [Validators.required]],
      defaultHodidayDate: [null, [Validators.maxLength(2000)]],
    }, {
      validators: [
        this.validateCustom('defaultHodidayDate')
      ]
    });
  }
  
  createWorkCalendarOrgsform(data: any): FormGroup {
    const form = this.fb.group({
      workCalendarOrgId: [null],
      org: [null, [Validators.required]],
      fromYear: [null, [Validators.required, ValidationService.positiveInteger]],
      toYear: [null, [ValidationService.positiveInteger]]
    },
      {validator: [ValidationService.notLessThanNumber('toYear', 'fromYear', 'staffAbs.workCalendar.workCalendarOrg.fromYear')]}
    );

    // build value for org
    data.org = data.orgId ? {
      orgId: data.orgId,
      orgName: data.orgName,
      path: data.orgPath
    } : null;

    form.patchValue(data);
    return form;
  }
  
  validateCustom(defaultHodidayDate: string): ValidatorFn {
    return (formGroup: AbstractControl): { [key: string]: boolean } | null => {
      const defaultHodidayDateValue = formGroup.get(defaultHodidayDate)?.value;
      if (!defaultHodidayDateValue) {
        return null;
      }
      const a = defaultHodidayDateValue.toString().trim().split(',');
      if (a.length == 0) {
        formGroup.get(defaultHodidayDate)?.setErrors({'invalidDefaultDate': true});
        return {['invalidDefaultDate']: true};
      }

      for (let i = 0; i < a.length; i++) {
        const data = a[i].toString().trim();
        if(!/^\d{1,2}\/\d{1,2}$/.test(data)) {
          formGroup.get(defaultHodidayDate)?.setErrors({'invalidDefaultDate': true});
          return {['invalidDefaultDate']: true};
        }
        const d = data.split('/');
        const day = parseInt(d[0]);
        const month = parseInt(d[1]);
        if (day < 0 || d > 31 || month < 0 || month > 12) {
          formGroup.get(defaultHodidayDate)?.setErrors({'invalidDefaultDate': true});
          return {['invalidDefaultDate']: true};
        }
      }
      return null;
    };
  }

  ngOnInit(): void {
    if (!this.workCalendarId) {
      return;
    }
    this.workCalendarService.getRecord(this.workCalendarId).subscribe((res: BaseResponse) => {
      if (res.code !== HTTP_STATUS_CODE.OK) {
        this.toastr.error(this.translate.instant('common.notification.updateError') + ": " + res.message);
        this.modalRef.close({ refresh: false });
        return;
      }
      const formData: WorkCalendars = res.data;
      this.form.patchValue(formData);
    });
  }
  
  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      const formData: WorkCalendars = {...this.form.value};

      this.workCalendarService.saveRecord(formData).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.toastr.success(this.translate.instant(this.workCalendarId ? 'common.notification.updateSuccess' : 'common.notification.addSuccess'));
          this.modalRef.close({refresh:true})
        } else
          this.toastr.error(this.translate.instant(this.workCalendarId ? 'common.notification.updateError' : 'common.notification.addError') + ": " + res?.message);
      }, error => {
        this.toastr.error(this.translate.instant(this.workCalendarId ? 'common.notification.updateError' : 'common.notification.addError') + ": " + error.message);
      })
    }
  }
}
