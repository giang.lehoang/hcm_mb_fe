import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {WorkCalendarFormComponent} from "./work-calendar-form/work-calendar-form.component";
import {NzFormModule} from "ng-zorro-antd/form";
import {ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiOrgDataPickerModule} from "@hcm-mfe/shared/ui/org-data-picker";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";

@NgModule({
  imports: [CommonModule, NzFormModule, ReactiveFormsModule, SharedUiMbInputTextModule, TranslateModule, SharedUiMbSelectModule,
    SharedUiOrgDataPickerModule, SharedUiMbButtonModule],
  declarations: [WorkCalendarFormComponent],
  exports: [WorkCalendarFormComponent],
})
export class StaffAbsFeatureCategoryWorkCalendarFormModule {}
