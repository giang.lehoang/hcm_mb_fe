import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {WorkCalendarDetailComponent} from "./work-calendar-detail/work-calendar-detail.component";
import {NzFormModule} from "ng-zorro-antd/form";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {TranslateModule} from "@ngx-translate/core";
import {StaffAbsUiCalendarModule} from "@hcm-mfe/staff-abs/ui/calendar";
import {StaffAbsUiWorkCalendarDetailImportModule} from "@hcm-mfe/staff-abs/ui/work-calendar-detail-import";
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [CommonModule, NzFormModule, ReactiveFormsModule, FormsModule, SharedUiMbButtonModule, SharedUiMbSelectModule,
    TranslateModule, StaffAbsUiCalendarModule, StaffAbsUiWorkCalendarDetailImportModule,
    RouterModule.forChild([
      {
        path: '',
        component: WorkCalendarDetailComponent
      }
    ])
  ],
  declarations: [WorkCalendarDetailComponent],
  exports: [WorkCalendarDetailComponent],
})
export class StaffAbsFeatureCategoryWorkCalendarDetailModule {}
