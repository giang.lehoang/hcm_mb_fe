import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkCalendarDetailComponent } from './work-calendar-detail.component';

describe('WorkCalendarDetailComponent', () => {
  let component: WorkCalendarDetailComponent;
  let fixture: ComponentFixture<WorkCalendarDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkCalendarDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkCalendarDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
