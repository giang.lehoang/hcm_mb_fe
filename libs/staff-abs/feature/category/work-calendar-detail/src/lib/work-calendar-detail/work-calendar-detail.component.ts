import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import {CatalogModel, WorkCalendars} from "@hcm-mfe/staff-abs/data-access/models";
import {WorkCalendarDetailService, WorkCalendarService} from "@hcm-mfe/staff-abs/data-access/services";
import {SYSTEM_FORMAT_DATA} from "@hcm-mfe/shared/common/constants";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";

@Component({
  selector: 'app-work-calendar-detail',
  templateUrl: './work-calendar-detail.component.html',
  styleUrls: ['./work-calendar-detail.component.scss']
})
export class WorkCalendarDetailComponent implements OnInit {
  formConfig = {
    year: [parseInt(moment(new Date()).format('YYYY')), [Validators.required]],
  };
  workCalendarId?: number;
  listYear: CatalogModel[] = [];
  public listMonth: any = [];
  public mapWorkCalendarDetails: any;
  public workCalendars: WorkCalendars = {};
  modal?: NzModalRef;
  form: FormGroup;
  isImportData = false;
  constructor(private route: ActivatedRoute,
    public workCalendarDetailService: WorkCalendarDetailService,
    public workCalendarService: WorkCalendarService,
    private modalServices: NzModalService,
    private translateService: TranslateService,
    private fb: FormBuilder) {
    this.listYear = this.getYearList();
    this.form = this.fb.group({
      year: [parseInt(moment(new Date()).format('YYYY')), [Validators.required]]
    })
  }

  ngOnInit(): void {
    const { workCalendarId } = this.route.snapshot.queryParams;
    this.workCalendarId = workCalendarId
    this.getWorkCalendarDetail();
  }
  getWorkCalendarDetail() {
    this.mapWorkCalendarDetails = null;
    const year = this.form.controls['year'].value;
    const fromDate = moment('01/01/' + year, 'DD/MM/YYYY').startOf('week').add(1, 'day');
    const toDate = moment('01/12/' + year, 'DD/MM/YYYY').startOf('week').add(42, 'day');
    const formData = {workCalendarId: this.workCalendarId, year: year, fromDate: fromDate.format(SYSTEM_FORMAT_DATA.DATE_FORMAT), toDate: toDate.format(SYSTEM_FORMAT_DATA.DATE_FORMAT)};
    this.workCalendarDetailService.search(formData).subscribe((res: BaseResponse) => {
      const map: any = {};
      res.data.forEach((e: any) => map[e.dateTimekeeping] = e);
      this.mapWorkCalendarDetails = map;
    })
    this.workCalendarService.getRecord(this.workCalendarId).subscribe((res: BaseResponse) => {
      this.workCalendars = res.data;
    })
    this.initListMonthSearch();
  }
  private getYearList() {
    const listYear = [];
    const currentYear = new Date().getFullYear();
    for (let i = (currentYear - 50) ; i <= (currentYear + 50) ; i++ ) {
      listYear.push(new CatalogModel(i.toString(), i));
    }
    return listYear;
  }
  onChangeYear() {
    this.getWorkCalendarDetail();
  }
  /**
   * initListMonthSearch
   * @ param year
   */
   initListMonthSearch() {
    const year = this.form.controls['year'].value;
    this.listMonth = ['01/' + year, '02/' + year, '03/' + year, '04/' + year
                    , '05/' + year, '06/' + year, '07/' + year, '08/' + year
                    , '09/' + year, '10/' + year, '11/' + year, '12/' + year ];
  }

  showModalImport() {
    this.isImportData = true;
  }

  doCloseImport(isSearch: boolean) {
    this.isImportData = false;
    isSearch && this.getWorkCalendarDetail();
  }
}
