import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { ToastrService } from 'ngx-toastr';
import {HTTP_STATUS_CODE, Mode} from "@hcm-mfe/shared/common/constants";
import {
  ApprovalFlow,
  ObjectCategory,
  RegisterLevel,
  ResonLeave
} from "@hcm-mfe/staff-abs/data-access/models";
import {ApprovalFlowService} from "@hcm-mfe/staff-abs/data-access/services";
import {Constant} from "@hcm-mfe/staff-abs/data-access/common";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";

@Component({
  selector: 'app-approval-flow-form',
  templateUrl: './approval-flow-form.component.html',
  styleUrls: ['./approval-flow-form.component.scss']
})
export class ApprovalFlowFormComponent implements OnInit {
  public static readonly addTitleKey = 'staffAbs.approvalFlow.modal.add'
  public static readonly updateTitleKey = 'staffAbs.approvalFlow.modal.update'

  mode?: Mode;
  id: number | undefined;

  form: FormGroup = this.fb.group([]);
  formReason: FormGroup = this.fb.group([]);
  formRegisterLevel: FormGroup = this.fb.group([]);
  isSubmitted = false;
  listHRLevels: ObjectCategory[] = [];

  listResonCode: ObjectCategory[] = [];

  listRegisterLevel: ObjectCategory[] =[];

  constructor(
    private fb: FormBuilder,
    private modalRef: NzModalRef,
    private toastrService: ToastrService,
    private translate: TranslateService,
    private approvalFlowService: ApprovalFlowService
  ) {
    this.buildForm();
  }

  ngOnInit(): void {
    this.listHRLevels = Constant.HR_LEVELS.map(e =>{
      e.label = this.translate.instant(e.label)
      return e;
    });

    this.getListLevelNV();
    this.getListResonCode();

    if (this.mode === Mode.EDIT && this.id && this.id > 0) {
      this.setFormValue();
    }
  }

  getListResonCode() {
    this.approvalFlowService.getListReason().subscribe(res => {
      if (res.data && res.data.length > 0) {
        this.listResonCode = res.data.map((item: any) => {
          return {
            label: item.name,
            value: item.code
          }
        })
      }
    })
  }

  getListLevelNV() {
    this.approvalFlowService.getPositionGroupsLevel().subscribe(res => {
      if(res?.data && res.data.length > 0){
        this.listRegisterLevel = res.data.map((e: any) =>{
          return {
            'label': e.pgrName,
            'value': e.pgrId
          }
        })
      }
    })
  }

  buildForm() {
    this.formReason = this.fb.group({
      approvalFlowTypeLeavesId: [''],
      approvalFlowId: [''],
      reasonCode: [null, [Validators.required]],
      maxDay: [null, [Validators.required, Validators.max(100000), Validators.min(1)]]
    });

    this.formRegisterLevel = this.fb.group({
      approvalFlowRegisterId: [''],
      approvalFlowId: [''],
      registerLevelId: [null, [Validators.required]],
      approvalLevelId: [null, Validators.required],
      hrLevelCode: [null, Validators.required],
    });

    this.form = this.fb.group({
      approvalFlowId: [''],
      name: ['', [Validators.required, Validators.maxLength(255)]],
      createDate: [''],
      createdBy: [''],
      lastUpdateDate: [''],
      lastUpdatedBy: [''],

      listReasons: this.fb.array([
        this.formReason
      ], {
        validators: this.validatorFormReason()
      }),

      registerLevels: this.fb.array([
        this.formRegisterLevel
      ], {
        validators: this.validatorFormregisterLevels()
      })
    });
  }
  // validate chọn trùng loại nghỉ , ngày nghỉ tối đa
  validatorFormReason(): ValidatorFn | any {
    return (formArray: FormArray): { [key: string]: string } | null => {
      const arrayDataValid: any = [];
      formArray.controls.forEach((item: FormGroup | any, i: number) => {
        if (item.value.reasonCode && item.value.maxDay) {
          const value = item.value.reasonCode + '_' + item.value.maxDay;
          const indexOf = arrayDataValid.indexOf(value);
          if (indexOf === -1) {
            arrayDataValid.push(value);
            formArray.controls[i].get('reasonCode')?.setErrors(null);
            formArray.controls[i].get('maxDay')?.setErrors(null);
          } else {
            formArray.controls[i].get('reasonCode')?.setErrors({ 'dublicate': true });
            formArray.controls[i].get('maxDay')?.setErrors({ 'dublicate': true });

            formArray.controls[indexOf].get('reasonCode')?.setErrors({ 'dublicate': true });
            formArray.controls[indexOf].get('maxDay')?.setErrors({ 'dublicate': true });
          }

        } else {
          formArray.controls[i].get('reasonCode')?.setErrors(null);
          formArray.controls[i].get('maxDay')?.setErrors(null);
          if (!item.value.reasonCode) {
            formArray.controls[i].get('reasonCode')?.setErrors({ 'required': true });
          }

          if (!item.value.maxDay) {
            formArray.controls[i].get('maxDay')?.setErrors({ 'required': true });
          }
        }

      })
      return null;
    };
  }

  // validate chọn trùng chức danh đăng ký
  validatorFormregisterLevels(): ValidatorFn | any {

    return (formArray: FormArray): { [key: string]: string } | null => {
      const arrayDataValid: any = [];
      formArray.controls.forEach((item: FormGroup | any, i) => {
        if (item.value.registerLevelId) {
          let value = item.value.registerLevelId;
          const indexOf = arrayDataValid.indexOf(value);
          if (indexOf === -1) {
            arrayDataValid.push(value);
            formArray.controls[i].get('registerLevelId')?.setErrors(null);
          } else {
            formArray.controls[i].get('registerLevelId')?.setErrors({ 'dublicate': true });

            formArray.controls[indexOf].get('registerLevelId')?.setErrors({ 'dublicate': true });
          }
        }

      })
      return null;
    }
  }

  get reasons(): FormArray {
    return this.form.get('listReasons') as FormArray;
  }

  addReason() {
    const formReason = this.fb.group({
      approvalFlowTypeLeavesId: [''],
      approvalFlowId: [''],
      reasonCode: [null, [Validators.required]],
      maxDay: [null, [Validators.required, Validators.max(100000), Validators.min(1)]]
    });
    this.reasons.push(formReason)
  }

  removeReason(index: number) {
    this.reasons.removeAt(index);
  }

  get registerLevels(): FormArray {
    return this.form.get('registerLevels') as FormArray;
  }

  addRegisterLevel() {
    const registerLevelId = this.fb.group({
      approvalFlowRegisterId: [''],
      approvalFlowId: [''],
      registerLevelId: [null, [Validators.required]],
      approvalLevelId: [null, Validators.required],
      hrLevelCode: [null, Validators.required],
    });
    this.registerLevels.push(registerLevelId)
  }

  removeRegisterLevel(index: number) {
    this.registerLevels.removeAt(index);
  }

  cancel() {
    this.modalRef.close()
  }

  saveOrUpdate() {
    this.isSubmitted = true;

    if (this.form.valid) {
      const request: ApprovalFlow = this.form.value;
      this.approvalFlowService.saveOrUpdate(request).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.toastrService.success(this.translate.instant(this.mode === Mode.EDIT ? 'common.notification.updateSuccess' : 'common.notification.addSuccess'));
          this.modalRef.close(true)
        } else {
          this.toastrService.error(this.translate.instant(this.mode === Mode.EDIT ? 'common.notification.updateError' : 'common.notification.addError') + ": " + res.message);
        }
      }, error => {
        this.toastrService.error(this.translate.instant(this.mode === Mode.EDIT ? 'common.notification.updateError' : 'common.notification.addError') + ": " + error.message);
      })
    }
  }

  setFormValue() {
    this.approvalFlowService.getRecord(this.id).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        const approvalFlow: ApprovalFlow = res.data;
        this.form.patchValue({
          approvalFlowId: approvalFlow.approvalFlowId,
          name: approvalFlow.name,
          createDate: approvalFlow.createDate,
          createdBy: approvalFlow.createdBy,
          lastUpdateDate: approvalFlow.lastUpdateDate,
          lastUpdatedBy: approvalFlow.lastUpdatedBy,

          listReasons: this.setValueFormReason(approvalFlow.listReasons ?? []),

          registerLevels: this.setValueFormregisterLevel(approvalFlow.registerLevels ?? []),
        })
      } else {
        this.toastrService.error(this.translate.instant(this.mode === Mode.EDIT ? 'common.notification.updateError' : 'common.notification.addError') + ": " + res.message);
        this.modalRef.close({ refresh: false })
      }
    }, error => {
      this.toastrService.error(this.translate.instant(this.mode === Mode.EDIT ? 'common.notification.updateError' : 'common.notification.addError') + ": " + error.message);
      this.modalRef.close({ refresh: false })
    })
  }

  setValueFormReason(resonLeaves: ResonLeave[]) {
    this.reasons.clear();
    if (resonLeaves && resonLeaves.length > 0) {
      resonLeaves.forEach(item => {
        let reson = this.fb.group({
          approvalFlowTypeLeavesId: [''],
          approvalFlowId: [''],
          reasonCode: [null, [Validators.required]],
          maxDay: [null, [Validators.required, Validators.max(100000), Validators.min(1)]]
        })

        reson.patchValue({
          approvalFlowTypeLeavesId: item.approvalFlowTypeLeavesId,
          approvalFlowId: item.approvalFlowId,
          reasonCode: item.reasonCode,
          maxDay: item.maxDay
        })
        this.reasons.push(reson)
      })
    }
  }

  setValueFormregisterLevel(registerLevels: RegisterLevel[]) {
    this.registerLevels.clear();
    if (registerLevels && registerLevels.length > 0) {
      registerLevels.forEach(item => {
        const registerLevelId = this.fb.group({
          approvalFlowRegisterId: [''],
          approvalFlowId: [''],
          registerLevelId: [null, [Validators.required]],
          approvalLevelId: [null, Validators.required],
          hrLevelCode: [null, Validators.required],
        })

        registerLevelId.patchValue({
          approvalFlowRegisterId: item.approvalFlowRegisterId,
          approvalFlowId: item.approvalFlowId,
          registerLevelId: item.registerLevelId,
          approvalLevelId: item.approvalLevelId,
          hrLevelCode: item.hrLevelCode
        })
        this.registerLevels.push(registerLevelId);
      })
    }
  }
}
