import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ApprovalFlowFormComponent} from "./approval-flow-form/approval-flow-form.component";
import {NzFormModule} from "ng-zorro-antd/form";
import {ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedDirectivesNumbericModule} from "@hcm-mfe/shared/directives/numberic";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzModalModule} from "ng-zorro-antd/modal";

@NgModule({
  imports: [CommonModule, NzFormModule, ReactiveFormsModule, SharedUiMbInputTextModule, TranslateModule, SharedUiMbSelectModule,
    SharedDirectivesNumbericModule, SharedUiMbButtonModule, NzButtonModule, NzModalModule],
  declarations: [ApprovalFlowFormComponent],
  exports: [ApprovalFlowFormComponent],
})
export class StaffAbsFeatureCategoryApprovalFlowFormModule {}
