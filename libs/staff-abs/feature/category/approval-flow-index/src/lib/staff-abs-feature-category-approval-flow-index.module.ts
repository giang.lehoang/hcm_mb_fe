import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ApprovalFlowIndexComponent} from "./approval-flow-index/approval-flow-index.component";
import {NzFormModule} from "ng-zorro-antd/form";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {NzPopconfirmModule} from "ng-zorro-antd/popconfirm";
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [CommonModule, NzFormModule, ReactiveFormsModule, FormsModule, SharedUiMbInputTextModule, TranslateModule,
    SharedUiMbButtonModule, SharedUiMbTableWrapModule, SharedUiMbTableModule, NzPopconfirmModule,
    RouterModule.forChild([
      {
        path: '',
        component: ApprovalFlowIndexComponent
      }
    ])
  ],
  declarations: [ApprovalFlowIndexComponent],
  exports: [ApprovalFlowIndexComponent],
})
export class StaffAbsFeatureCategoryApprovalFlowIndexModule {}
