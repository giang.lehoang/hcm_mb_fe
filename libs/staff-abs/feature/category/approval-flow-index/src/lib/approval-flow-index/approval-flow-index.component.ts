import { Component, Injector, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { forkJoin } from 'rxjs';
import {Constant, SearchBaseComponent} from "@hcm-mfe/staff-abs/data-access/common";
import {ApprovalFlow, ObjectCategory, RegisterLevel, ResonLeave} from "@hcm-mfe/staff-abs/data-access/models";
import {CommonUtils, ValidateService} from "@hcm-mfe/shared/core";
import {ApprovalFlowService} from "@hcm-mfe/staff-abs/data-access/services";
import {ApprovalFlowFormComponent} from "@hcm-mfe/staff-abs/feature/category/approval-flow-form";
@Component({
  selector: 'app-approval-flow-index',
  templateUrl: './approval-flow-index.component.html',
  styleUrls: ['./approval-flow-index.component.scss']
})
export class ApprovalFlowIndexComponent extends SearchBaseComponent implements OnInit {
  listHRLevels: ObjectCategory[] = [];

  listResonCode: ObjectCategory[] = [];

  listRegisterLevel: ObjectCategory[] = [];

  @ViewChild('actionTmpl', { static: true }) action!: TemplateRef<NzSafeAny>;
  @ViewChild('resonCodeTmpl', { static: true }) resonCodeTmpl!: TemplateRef<NzSafeAny>;
  @ViewChild('maxDayTmpl', { static: true }) maxDayTmpl!: TemplateRef<NzSafeAny>;
  @ViewChild('registerLevelTmpl', { static: true }) registerLevelTmpl!: TemplateRef<NzSafeAny>;
  @ViewChild('approvalLevelTmpl', { static: true }) approvalLevelTmpl!: TemplateRef<NzSafeAny>;
  @ViewChild('hrLevelTmpl', { static: true }) hrLevelTmpl!: TemplateRef<NzSafeAny>;
  formSearchConfig = {
    name: ['']
  }

  constructor(
    injector: Injector,
    public validateService: ValidateService,
    public approvalFlowService: ApprovalFlowService

  ) {
    super(injector);
    this.modalComponent = ApprovalFlowFormComponent;
    this.setMainService(approvalFlowService);
    this.buildFormSearch(this.formSearchConfig);
  }

  ngOnInit(): void {
    this.initTable();

    this.listHRLevels = Constant.HR_LEVELS.map(e =>{
      e.label = this.translate?.instant(e.label)
      return e;
    });

    this.tableConfig.loading = true;

    forkJoin([
      this.approvalFlowService.getPositionGroupsLevel(),
      this.approvalFlowService.getListReason()
    ]).subscribe(
      ([dataRegisterCodes, dataResonCodes]) => {
        this.getListLevelNV(dataRegisterCodes);
        this.getListResonCode(dataResonCodes);
        this.doSearch()
      }, error => {
        this.toastrService?.error(this.translate?.instant('common.notification.error'));
      }

    )
  }

  getListResonCode(res: any) {
    if (res.data && res.data.length > 0) {
      this.listResonCode = res.data.map((item: any) => {
        return {
          'label': item.name,
          'value': item.code
        }
      })
    }
  }

  getListLevelNV(res: any) {
    if(res?.data && res.data.length > 0){
      this.listRegisterLevel = res.data.map((e: any) =>{
        return {
          'label': e.pgrName,
          'value': e.pgrId
        }
      })
    }
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'staffAbs.approvalFlow.search.titleName',
          field: 'name',
          width: 150,
          needBreakword: true,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left',
          tdClassList: ['text-left'],
          thClassList: ['text-center'],
        },
        {
          title: 'staffAbs.approvalFlow.search.titelResons',
          field: 'reasonCodeArray',
          tdTemplate: this.resonCodeTmpl,
          tdClassList: ['text-left'],
          thClassList: ['text-center'],
          width: 150
        },
        {
          title: 'staffAbs.approvalFlow.search.titleMaxDay',
          field: 'maxDayArray',
          tdTemplate: this.maxDayTmpl,
          tdClassList: ['text-left'],
          thClassList: ['text-center'],
          width: 110
        },
        {
          title: 'staffAbs.approvalFlow.search.titleRegister',
          field: 'registerLevelIdArray',
          tdTemplate: this.registerLevelTmpl,
          tdClassList: ['text-left'],
          thClassList: ['text-center'],
          width: 150
        },
        {
          title: 'staffAbs.approvalFlow.search.titleApproval',
          field: 'approvalLevelIdString',
          tdTemplate: this.approvalLevelTmpl,
          tdClassList: ['text-left'],
          thClassList: ['text-center'],
          width: 150,
        },
        {
          title: 'staffAbs.approvalFlow.search.titleHR',
          field: 'hrLevelCodeString',
          tdTemplate: this.hrLevelTmpl,
          tdClassList: ['text-left'],
          thClassList: ['text-center'],
          width: 150
        },
        {
          title: '',
          field: 'action',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 150,
          tdTemplate: this.action,
          fixed: window.innerWidth > 1024,
          fixedDir: 'right'
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  public override doSearch(pageNumber?: number): void {
    this.pagination.pageNumber = pageNumber ?? 1;
    this.tableConfig.pageIndex = pageNumber ? this.tableConfig.pageIndex: 1;
    this.tableConfig.loading = true;
    const params = this.formSearch ? {...this.formSearch.value} : {};
    this.approvalFlowService.search({ ...params, ...this.pagination.getCurrentPage() })
      .subscribe(res => {
        if (CommonUtils.isSuccessRequest(res)) {
          this.searchResult = res.data.listData as ApprovalFlow[];
          this.searchResult = this.convertData(this.searchResult);
          this.tableConfig.total = res.data.count;
        }
        this.tableConfig.loading = false;
      }, () => {
        this.tableConfig.loading = false;
      }
      );
  }

  public convertData(listData: ApprovalFlow[]): ApprovalFlow[] {
    listData.forEach((item) => {
      item.reasonCodeArray = this.getReasonLabel(item.listReasons ?? []);
      item.maxDayArray = item.listReasons?.map((item) => item.maxDay) ?? [];
      item.registerLevelIdArray = this.getLabelByValue(item.registerLevels ?? [], 'registerLevelId');
      item.approvalLevelIdArray = this.getLabelByValue(item.registerLevels ?? [], 'approvalLevelId');
      item.hrLevelCodeArray = this.getLabelByValue(item.registerLevels ?? [], 'hrLevelCode');
    })
    return listData;
  }

  public getReasonLabel(listReasons: ResonLeave[]) {
    let reasonCodeArray: string[] = []
    reasonCodeArray = listReasons.map((item) => {
      const resonLeave = this.listResonCode.find(e => e.value === item.reasonCode);
      if (resonLeave) {
        return resonLeave['label']
      }
      return ''
    })
    return reasonCodeArray;
  }

  public getLabelByValue(registerLevels: RegisterLevel[], field: string) {

    let labelArray: string[] = []
    if (field === 'hrLevelCode') {
      labelArray = registerLevels.map((item) => {
        const result = this.listHRLevels.find(e => e.value === item['hrLevelCode']);
        if (result) {
          return result['label']
        }
        return ''
      })
    } else {
      labelArray = registerLevels.map((item: any) => {
        const result = this.listRegisterLevel.find(e => e.value === item[field]);
        if (result) {
          return result['label']
        }
        return ''
      })
    }

    return labelArray;
  }


}
