import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LeaveReasonFormComponent} from "./leave-reason-form/leave-reason-form.component";
import {NzFormModule} from "ng-zorro-antd/form";
import {ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";

@NgModule({
  imports: [CommonModule, NzFormModule, ReactiveFormsModule, SharedUiMbSelectModule, TranslateModule, SharedUiMbInputTextModule],
  declarations: [LeaveReasonFormComponent],
  exports: [LeaveReasonFormComponent],
})
export class StaffAbsFeatureCategoryLeaveReasonFormModule {}
