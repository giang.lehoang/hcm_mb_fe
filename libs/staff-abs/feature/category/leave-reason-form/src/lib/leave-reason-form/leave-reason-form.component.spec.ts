import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LeaveReasonFormComponent } from './leave-reason-form.component';

describe('LeaveReasonFormComponent', () => {
  let component: LeaveReasonFormComponent;
  let fixture: ComponentFixture<LeaveReasonFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LeaveReasonFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LeaveReasonFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
