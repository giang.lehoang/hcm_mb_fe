import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { ToastrService } from 'ngx-toastr';
import { forkJoin } from 'rxjs';
import {BaseResponse, RegionCategory, ResponseEntity} from "@hcm-mfe/shared/data-access/models";
import {CatalogModel, ReasonLeave, WorkdayType} from "@hcm-mfe/staff-abs/data-access/models";
import {StaffInfoService} from "@hcm-mfe/shared/core";
import {ReasonLeaveService} from "@hcm-mfe/staff-abs/data-access/services";
import {CustomValidators} from "@hcm-mfe/shared/common/validators";
import {Constant} from "@hcm-mfe/staff-abs/data-access/common";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";

@Component({
  selector: 'app-leave-reason-form',
  templateUrl: './leave-reason-form.component.html',
  styleUrls: ['./leave-reason-form.component.scss']
})
export class LeaveReasonFormComponent implements OnInit {
  groupCodeList: RegionCategory[] = [];
  timeOffTypeList: CatalogModel[] = [
    { label: this.translate.instant('staffAbs.leaveReason.timeOffType.1'), value: 1 },
    { label: this.translate.instant('staffAbs.leaveReason.timeOffType.2'), value: 2 }
  ]
  workdayTypeCodeList: WorkdayType[] = [];
  data = new ReasonLeave();
  form: FormGroup;
  reasonLeaveId?: number;
  isSubmitted = false;
  constructor(
    private fb: FormBuilder,
    private modalRef: NzModalRef,
    public toastr: ToastrService,
    public translate: TranslateService,
    private staffInfoService: StaffInfoService,
    public reasonLeaveService: ReasonLeaveService,
  ) {
    this.form = this.fb.group({
      reasonLeaveId: [null],
      groupCode: [null, [Validators.required, Validators.maxLength(200)]],
      workdayTypeCode: [null, [Validators.required, Validators.maxLength(200)]],
      code: [null, [Validators.required, Validators.maxLength(50)]],
      name: [null, [Validators.required, Validators.maxLength(200)]],
      maxTimeOff: [null, [CustomValidators.onlyNumber]],
      yearMaxTimeOff: [null, [CustomValidators.onlyNumber]],
      timeOffType: [null, [Validators.required]],
      defaultTimeOff: [null, [CustomValidators.onlyNumber]],
    }, {
      validators: [
        LeaveReasonFormComponent.validateTripRequired('timeOffType', 'maxTimeOff', 'yearMaxTimeOff'),
      ]
    });
  }
  static validateTripRequired(timeOffType: string, maxTimeOff: string, yearMaxTimeOff: string): ValidatorFn {
    return (formGroup: AbstractControl): { [key: string]: boolean } | null => {
      const timeOffTypeValue = formGroup.get(timeOffType)?.value;
      const maxTimeOffValue = formGroup.get(maxTimeOff)?.value;
      const yearMaxTimeOffValue = formGroup.get(yearMaxTimeOff)?.value;
      const hasValue = maxTimeOffValue || yearMaxTimeOffValue;
      if (timeOffTypeValue == null && hasValue) {
        formGroup.get(timeOffType)?.setErrors({'required': true});
        return {['tripRequired']: true};
      }
      return null;
    };
  }
  ngOnInit(): void {
    this.innitData();
  }
  innitData() {
    forkJoin([
      this.reasonLeaveService.getAllWorkDayType(),
      this.staffInfoService.getCatalog(Constant.CatalogType.LOAI_HINH_CHAM_CONG)
    ]).subscribe(
      ([resWorkType, resGroupCodeList]) => {
        // Nạp dữ liệu các bản ghi dropdown
        this.getDataWorkTypeList(resWorkType);
        this.getGroupCodeList(resGroupCodeList);

        // Nạp dữ liệu bản ghi master
        if (!this.reasonLeaveId) {
          return;
        }
        this.innitRecord();
      }, error => {
        this.innitRecord();
        this.toastr.error(this.translate.instant('common.notification.error'));
      }
    )
  }
  getDataWorkTypeList(res: any) {
    this.workdayTypeCodeList = res.data;
  }
  getGroupCodeList(res: any) {
    this.groupCodeList = res.data;
  }
  innitRecord() {
    this.reasonLeaveService.getRecord(this.reasonLeaveId).subscribe((data: ResponseEntity<ReasonLeave>) => {
      this.form.patchValue(data.data ?? {});
    });
  }
  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      const formData: ReasonLeave = this.form.value;
      this.reasonLeaveService.saveRecord(formData).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.toastr.success(this.translate.instant(this.reasonLeaveId ? 'common.notification.updateSuccess' : 'common.notification.addSuccess'));
          this.modalRef.close({refresh:true})
        } else
          this.toastr.error(this.translate.instant(this.reasonLeaveId ? 'common.notification.updateError' : 'common.notification.addError') + ": " + res?.message);
      }, error => {
        this.toastr.error(this.translate.instant(this.reasonLeaveId ? 'common.notification.updateError' : 'common.notification.addError') + ": " + error.message);
      })
    }
  }


}
