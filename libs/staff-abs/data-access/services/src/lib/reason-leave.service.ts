import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {ReasonLeave} from "@hcm-mfe/staff-abs/data-access/models";
import {UrlConstant} from "@hcm-mfe/staff-abs/data-access/common";
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";
import {CommonUtils} from "@hcm-mfe/shared/core";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";

@Injectable({
    providedIn: 'root'
})
export class ReasonLeaveService extends BaseService {
    readonly baseUrl = UrlConstant.API_VERSION + UrlConstant.REASON_LEAVE.PREFIX;

    // Lấy danh sách
    public search(searchData: ReasonLeave): Observable<BaseResponse> {
        const params = CommonUtils.buildParams(searchData);
        const url = this.baseUrl;
        return this.get(url, { params: params }, MICRO_SERVICE.ABS_MANAGEMENT);
    }

    // Lưu bản ghi
    public saveRecord(request: ReasonLeave) {
        const url = this.baseUrl;
        return this.post(url, request, {}, MICRO_SERVICE.ABS_MANAGEMENT);
    }

    // Xóa bản ghi
    public deleteRecord(reasonLeaveId: number) {
        const url = this.baseUrl + UrlConstant.REASON_LEAVE.DELETE.replace('{reasonLeaveId}', reasonLeaveId.toString())
        return this.delete(url, {}, MICRO_SERVICE.ABS_MANAGEMENT);
    }

    // Lấy bản ghi
    public getRecord(reasonLeaveId: number | any) {
        const url = this.baseUrl + UrlConstant.REASON_LEAVE.DETAIL.replace('{reasonLeaveId}', reasonLeaveId.toString())
        return this.get(url, {}, MICRO_SERVICE.ABS_MANAGEMENT);
    }

    // Lấy thông tin workday-type ký hiệu chấm công
    public getAllWorkDayType() {
      const url = UrlConstant.API_VERSION + "/workday-type/get-all";
      return this.get(url, {}, MICRO_SERVICE.ABS_MANAGEMENT);
  }

  public getAllReasonLeaves(groupCode: string,) {
    const url = UrlConstant.API_VERSION + UrlConstant.REASON_LEAVE.PREFIX;
    return this.get(`${url}/${groupCode}/all`, { }, MICRO_SERVICE.ABS_MANAGEMENT);
  }
}
