import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {WorkCalendarDetails} from "@hcm-mfe/staff-abs/data-access/models";
import {UrlConstant} from "@hcm-mfe/staff-abs/data-access/common";
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";
import {CommonUtils} from "@hcm-mfe/shared/core";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";

@Injectable({
    providedIn: 'root'
})
export class WorkCalendarDetailService extends BaseService {
    readonly baseUrl = UrlConstant.API_VERSION + UrlConstant.WORK_CALENDAR_DETAIL.PREFIX;

    // Lấy danh sách
    public search(searchData: WorkCalendarDetails): Observable<BaseResponse> {
        const params = CommonUtils.buildParams(searchData);
        const url = this.baseUrl;
        return this.get(url, { params: params }, MICRO_SERVICE.ABS_MANAGEMENT);
    }

    // Lưu bản ghi
    public saveRecord(request: WorkCalendarDetails) {
        const url = this.baseUrl;
        return this.post(url, request, {}, MICRO_SERVICE.ABS_MANAGEMENT);
    }


    // Xóa bản ghi
    public deleteRecord(workCalendarDetailId: number) {
        const url = this.baseUrl + UrlConstant.WORK_CALENDAR.DELETE.replace('{workCalendarDetailId}', workCalendarDetailId.toString())
        return this.delete(url, {}, MICRO_SERVICE.ABS_MANAGEMENT);
    }

    // Lấy bản ghi
    public getRecord(workCalendarDetailId: number) {
        const url = this.baseUrl + UrlConstant.WORK_CALENDAR.DETAIL.replace('{workCalendarDetailId}', workCalendarDetailId.toString())
        return this.get(url, {}, MICRO_SERVICE.ABS_MANAGEMENT);
    }
    /**
     * downloadFileTemplate
     * @returns
     */
    public downloadFileTemplate() {
        const url = this.baseUrl + "/get-template-import";
        return this.getRequestFileD2T(url, {}, MICRO_SERVICE.ABS_MANAGEMENT);
    }

    /**
     * Thực hiện import
     * @param form
     */
    import(form: FormData) {
      const url = this.baseUrl + "/import";
      return this.post(url, form, {}, MICRO_SERVICE.ABS_MANAGEMENT)
    }
}
