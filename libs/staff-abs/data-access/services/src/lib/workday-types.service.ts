import {Injectable} from '@angular/core';
import {HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {WorkdayType} from "@hcm-mfe/staff-abs/data-access/models";
import {UrlConstant} from "@hcm-mfe/staff-abs/data-access/common";
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";

@Injectable({
  providedIn: 'root'
})
export class WorkdayTypesService extends BaseService {
  public searchWorkdayType(urlEndpoint: string, searchParam: HttpParams, pagination: { startRecord: number, pageSize: number }): Observable<BaseResponse> {
    if (pagination) searchParam = searchParam.appendAll(pagination);
    const url = UrlConstant.API_VERSION + urlEndpoint;
    return this.get(url, {params: searchParam}, MICRO_SERVICE.ABS_MANAGEMENT);
  }

  public saveWorkdayType(urlEndpoint: string, data: WorkdayType) {
    const url = UrlConstant.API_VERSION + urlEndpoint;
    return this.post(url, data, {}, MICRO_SERVICE.ABS_MANAGEMENT);
  }

  public deleteWorkdayType(id:number) {
    const url = UrlConstant.API_VERSION + UrlConstant.DELETE.WORKDAY_TYPE.replace("{id}", id.toString());
    return this.delete(url, {}, MICRO_SERVICE.ABS_MANAGEMENT)
  }

  public getVLookupWorkdayType(typeCode: string): Observable<BaseResponse> {
    const params = new HttpParams().set('typeCode', typeCode);
    const url = UrlConstant.API_VERSION + UrlConstant.CATALOGS.PREFIX;
    return this.get(url, {params: params}, MICRO_SERVICE.ABS_MANAGEMENT);
  }
}
