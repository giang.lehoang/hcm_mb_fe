import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {PosAnnualLeaves} from "@hcm-mfe/staff-abs/data-access/models";
import {UrlConstant} from "@hcm-mfe/staff-abs/data-access/common";
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";
import {environment} from "@hcm-mfe/shared/environment";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";

@Injectable({
    providedIn: 'root'
})
export class PosAnnualLeavesService extends BaseService {

    public searchPosAnnualLeaves(searchParam: HttpParams, pagination: { startRecord: number, pageSize: number }): Observable<BaseResponse> {
        if (pagination) searchParam = searchParam.appendAll(pagination);
        return this.get(
            UrlConstant.API_VERSION + UrlConstant.SEARCH_FORM.POS_ANNUAL_LEAVES,
            {params: searchParam},
            MICRO_SERVICE.ABS_MANAGEMENT);
    }

    public savePosAnnualLeaves(urlEndpoint: string, data: PosAnnualLeaves) {
        const url = UrlConstant.API_VERSION + urlEndpoint;
        return this.post(url, data, {}, MICRO_SERVICE.ABS_MANAGEMENT);
    }

    public getPosAnnualLeaves(urlEndpoint: string, id: number | any): Observable<BaseResponse> {
        const url = UrlConstant.API_VERSION + urlEndpoint + id;
        return this.get(url, {}, MICRO_SERVICE.ABS_MANAGEMENT);
    }

    public deletePosAnnualService(id: number) {
        const url = UrlConstant.API_VERSION + UrlConstant.DELETE.POS_ANNUAL_LEAVES + id;
        return this.delete(url, {}, MICRO_SERVICE.ABS_MANAGEMENT)
    }

    doExportFile(urlEndpoint: string, searchParam: HttpParams) {
        const url = UrlConstant.API_VERSION + urlEndpoint;
        const baseUrl = environment.backend.absServiceBackend + url;
        return this.getRequestFile(url, {params: searchParam}, baseUrl);
    }

}
