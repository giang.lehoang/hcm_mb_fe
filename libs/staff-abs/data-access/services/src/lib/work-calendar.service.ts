import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {WorkCalendars} from "@hcm-mfe/staff-abs/data-access/models";
import {UrlConstant} from "@hcm-mfe/staff-abs/data-access/common";
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";
import {CommonUtils} from "@hcm-mfe/shared/core";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";


@Injectable({
    providedIn: 'root'
})
export class WorkCalendarService extends BaseService {
    readonly baseUrl = UrlConstant.API_VERSION + UrlConstant.WORK_CALENDAR.PREFIX;

    // Lấy danh sách
    public search(searchData: WorkCalendars): Observable<BaseResponse> {
        const params = CommonUtils.buildParams(searchData);
        const url = this.baseUrl;
        return this.get(url, { params: params }, MICRO_SERVICE.ABS_MANAGEMENT);
    }

    // Lưu bản ghi
    public saveRecord(request: WorkCalendars) {
        const url = this.baseUrl;
        return this.post(url, request, {}, MICRO_SERVICE.ABS_MANAGEMENT);
    }


    // Xóa bản ghi
    public deleteRecord(workCalendarId: number) {
        const url = this.baseUrl + UrlConstant.WORK_CALENDAR.DELETE.replace('{workCalendarId}', workCalendarId.toString())
        return this.delete(url, {}, MICRO_SERVICE.ABS_MANAGEMENT);
    }

    // Lấy bản ghi
    public getRecord(workCalendarId: number | any) {
        const url = this.baseUrl + UrlConstant.WORK_CALENDAR.DETAIL.replace('{workCalendarId}', workCalendarId.toString())
        return this.get(url, {}, MICRO_SERVICE.ABS_MANAGEMENT);
    }

    // Lấy danh sách lịch làm việc còn hiệu lực (flagStatus=1)
    public getActiveWorkCalendars() {
        const url = this.baseUrl + UrlConstant.WORK_CALENDAR.ALL_ACTIVE
        return this.get(url, {}, MICRO_SERVICE.ABS_MANAGEMENT);
    }

    // Gán lịch làm việc
    public assignWorkCalendar(workCalendarId: number, request: WorkCalendars) {
        const url = this.baseUrl + UrlConstant.WORK_CALENDAR.ASSIGN.replace('{workCalendarId}', workCalendarId.toString())
        return this.post(url, request, {}, MICRO_SERVICE.ABS_MANAGEMENT);
    }
}
