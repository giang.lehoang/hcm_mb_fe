import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";
import {UrlConstant} from "@hcm-mfe/staff-abs/data-access/common";
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";
import {environment} from "@hcm-mfe/shared/environment";

@Injectable({
    providedIn: 'root'
})
export class AnnualLeaveService extends BaseService {

    public searchAnnualLeaves(searchParam: HttpParams, pagination: { startRecord: number, pageSize: number }): Observable<BaseResponse> {
        if (pagination) searchParam = searchParam.appendAll(pagination);
        return this.get(
            UrlConstant.API_VERSION + UrlConstant.SEARCH_FORM.ANNUAL_LEAVES,
            {params: searchParam},
            MICRO_SERVICE.ABS_MANAGEMENT);
    }

    doExportFile(urlEndpoint: string, searchParam: HttpParams) {
        const url = UrlConstant.API_VERSION + urlEndpoint;
        const baseUrl = environment.backend.absServiceBackend + url;
        return this.getRequestFile(url, {params: searchParam}, baseUrl);
    }

    calculateAnnualLeave(year: number) {
        const url = `${UrlConstant.API_VERSION}${UrlConstant.SEARCH_FORM.ANNUAL_LEAVES_CALCULATE.replace("{year}", year.toString())}`
        return this.get(url, {}, MICRO_SERVICE.ABS_MANAGEMENT);
    }
}
