import { Injectable } from '@angular/core';
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {UrlConstant} from "@hcm-mfe/staff-abs/data-access/common";
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";
import {ApprovalFlow} from "@hcm-mfe/staff-abs/data-access/models";

@Injectable({
  providedIn: 'root'
})
export class ApprovalFlowService extends BaseService {

  readonly baseUrl = UrlConstant.API_VERSION + UrlConstant.APPROVAL_FLOW.PREFIX;
  // lấy danh sách cấp chwucs danh
  public getPositionGroupsLevel() {
    const url = UrlConstant.API_VERSION +  UrlConstant.APPROVAL_FLOW.LEVEL_NV;
    return this.get(url, {}, MICRO_SERVICE.ABS_MANAGEMENT)
  }
  // lấy loai nghỉ phép
  public getListReason() {
    const url = UrlConstant.API_VERSION +  UrlConstant.APPROVAL_FLOW.REASONS;
    return this.get(url, {}, MICRO_SERVICE.ABS_MANAGEMENT)
  }

  // luu
  public saveOrUpdate(dataForm: ApprovalFlow){
    const url = this.baseUrl + UrlConstant.APPROVAL_FLOW.SAVE;
    return this.post(url, dataForm, {}, MICRO_SERVICE.ABS_MANAGEMENT);
  }

  // lay thong tin ban ghi
  public getRecord(approvalFlowId: number | any) {
    const url = this.baseUrl + UrlConstant.APPROVAL_FLOW.DETAIL.replace('{approvalFlowId}', approvalFlowId.toString())
    return this.get(url, {}, MICRO_SERVICE.ABS_MANAGEMENT);
  }

  public search(dataForm: ApprovalFlow) {
    const url = this.baseUrl + UrlConstant.APPROVAL_FLOW.SEARCH ;
    return this.get(url, { params: dataForm }, MICRO_SERVICE.ABS_MANAGEMENT );
  }

  deleteById(id: number) {
    return this.delete(`${this.baseUrl}/${id}`, {}, MICRO_SERVICE.ABS_MANAGEMENT);
  }
}
