import { Injectable } from '@angular/core';
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {UrlConstant} from "@hcm-mfe/staff-abs/data-access/common";
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";

@Injectable({
  providedIn: 'root'
})
export class RequestLeavesService extends BaseService {
  readonly baseUrl = UrlConstant.API_VERSION + UrlConstant.REQUEST_LEAVE_PATH;

  public caculateLeaves(params: any) {
    return this.get(`${this.baseUrl}/caculate-leaves`, { params: params }, MICRO_SERVICE.ABS_MANAGEMENT);
  }
}
