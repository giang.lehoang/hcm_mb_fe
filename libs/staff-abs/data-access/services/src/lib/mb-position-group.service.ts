import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";
import {UrlConstant} from "@hcm-mfe/staff-abs/data-access/common";
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";

@Injectable({
    providedIn: 'root'
})
export class MbPositionGroupService extends BaseService {

    getPositionGroupList(): Observable<BaseResponse> {
        return this.get(
            UrlConstant.API_VERSION + UrlConstant.SEARCH_FORM.LOOKUP_POSITION_GROUP,
            {},
            MICRO_SERVICE.ABS_MANAGEMENT);
    }


    // l?y danh s�ch c?p ch?c danh
    public getAllPositionGroups(): Observable<BaseResponse> {
        const url = UrlConstant.API_VERSION +  UrlConstant.SEARCH_FORM.POSITION_GROUP_ALL;
        return this.get(url, {}, MICRO_SERVICE.ABS_MANAGEMENT)
    }
}
