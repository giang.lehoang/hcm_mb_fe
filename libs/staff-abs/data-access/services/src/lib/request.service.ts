import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {Request, RequestLeave} from "@hcm-mfe/staff-abs/data-access/models";
import {UrlConstant} from "@hcm-mfe/staff-abs/data-access/common";
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";
import {CommonUtils} from "@hcm-mfe/shared/core";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";
import { HttpParams } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class RequestService extends BaseService {
  readonly baseUrl = UrlConstant.API_VERSION + UrlConstant.REQUEST_PATH;

  search(formSearch: RequestLeave) {
    const url = this.baseUrl + "/admin/search" + `/${formSearch.leaveType}`;
    return this.get(url, { params: this._parseOptions(formSearch)  }, MICRO_SERVICE.ABS_MANAGEMENT);
  }

  deleteById(id: number) {
    return this.delete(`${this.baseUrl}/admin/${id}`, {}, MICRO_SERVICE.ABS_MANAGEMENT);
  }

  findById(id: number) {
    return this.get(`${this.baseUrl}/admin/${id}`, {}, MICRO_SERVICE.ABS_MANAGEMENT);
  }

  saveOrUpdate(form: Request) {
    return this.post(this.baseUrl, form, {}, MICRO_SERVICE.ABS_MANAGEMENT)
  }

  saveOrUpdateFormData(form: Request) {
    let formData = new FormData();
    formData.append("data", new Blob([JSON.stringify(form)], {
      type: 'application/json',
    }));

    form.files?.forEach((nzFile: File) => {
      formData.append('files', nzFile)
    });

    return this.post(`${this.baseUrl}/admin`, formData, {}, MICRO_SERVICE.ABS_MANAGEMENT)
  }

  saveOrUpdateRequestLeave(form: Request) {
    let formData = new FormData();
    formData.append("data", new Blob([JSON.stringify(form)], {
      type: 'application/json',
    }));

    form.files?.forEach((nzFile: File) => {
      formData.append('files', nzFile)
    });
    return this.post(`${this.baseUrl}/admin/leave-save`, formData, {}, MICRO_SERVICE.ABS_MANAGEMENT)
  }

  export(formSearch: RequestLeave) {
    const url = this.baseUrl + "/export";
    return this.getRequestFileD2T(url, { params: CommonUtils.buildParams(formSearch) }, MICRO_SERVICE.ABS_MANAGEMENT);
  }

  downloadFileTemplate(leaveType: string) {
    const url = this.baseUrl + "/get-template-import" + `/${leaveType}`;
    return this.getRequestFileD2T(url, {}, MICRO_SERVICE.ABS_MANAGEMENT);
  }

  import(form: FormData) {
    const url = this.baseUrl + "/import";
    return this.post(url, form, {}, MICRO_SERVICE.ABS_MANAGEMENT)
  }

  getUserLogin(): Observable<BaseResponse> {
    const url = this.baseUrl + "/emp-info";
    return this.get(`${url}`, {}, MICRO_SERVICE.ABS_MANAGEMENT);
  }

  getTotalRequestBackdate(leaveType: string) {
    const url = `${this.baseUrl}/backdate/count/${leaveType}`;
    return this.get(url, { }, MICRO_SERVICE.ABS_MANAGEMENT);
  }

  approveBackdateRequests(requestId: number) {
    const payload: Request = {
      requestId
    };
    const url = `${this.baseUrl}/action/approveBackdate`;
    return this.post(url, payload, { }, MICRO_SERVICE.ABS_MANAGEMENT);
  }

  _parseOptions(formData: RequestLeave) {
    let params = new HttpParams();
    if (!formData) {
        return params;
    }
    for (const [key, value] of Object.entries(formData)) {
        if(value) {
            params = params.set(key, value + '');
        }
    }
    return params;
  }
}
