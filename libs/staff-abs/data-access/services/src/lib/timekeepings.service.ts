import { Injectable } from '@angular/core';
import {HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";
import {UrlConstant} from "@hcm-mfe/staff-abs/data-access/common";
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";
import {environment} from "@hcm-mfe/shared/environment";

@Injectable({
  providedIn: 'root'
})
export class TimekeepingsService extends BaseService{
  public searchTimekeepings(urlEndpoint: string, searchParam: HttpParams, pagination: { startRecord: number, pageSize: number }): Observable<BaseResponse> {
    if (pagination) searchParam = searchParam.appendAll(pagination);
    const url = UrlConstant.API_VERSION + urlEndpoint;
    return this.get(url, {params: searchParam}, MICRO_SERVICE.ABS_MANAGEMENT);
  }

  doExportFile(urlEndpoint: string, searchParam: HttpParams) {
    const url = UrlConstant.API_VERSION + urlEndpoint;
    return this.getRequestFile(url, {params: searchParam},  environment.backend.absServiceBackend + url);
  }

  timekeepingAuto(params: HttpParams) {
    const url = `${UrlConstant.API_VERSION}${UrlConstant.SEARCH_FORM.TIMEKEEPING_AUTO}`
    return this.get(url, {params: params}, MICRO_SERVICE.ABS_MANAGEMENT);
  }

  doExportFileAtTime(searchParam: HttpParams) {
    const url = `${UrlConstant.API_VERSION}${UrlConstant.SEARCH_FORM.EXPORT_AT_TIME}`
    return this.getRequestFile(url, {params: searchParam},  environment.backend.absServiceBackend + url);
  }


  doExportFileChanged(searchParam: HttpParams) {
    const url = `${UrlConstant.API_VERSION}${UrlConstant.SEARCH_FORM.EXPORT_CHANGED}`
    return this.getRequestFile(url, {params: searchParam},  environment.backend.absServiceBackend + url);
  }

}
