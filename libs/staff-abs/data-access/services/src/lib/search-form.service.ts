import { Injectable } from '@angular/core';
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {UrlConstant} from "@hcm-mfe/staff-abs/data-access/common";


@Injectable({
  providedIn: 'root'
})
export class SearchFormService extends BaseService {

    public getEmpTypeList() {
        return this.get(UrlConstant.API_VERSION + UrlConstant.SEARCH_FORM.LOOKUP_EMP_TYPE);
    }

}
