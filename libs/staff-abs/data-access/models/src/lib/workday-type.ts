export class WorkdayType {
  workdayTypeId?: number;
  workdayTypeGroupId?: number;
  code: string;
  name: string;
  groupCode?: string;
  groupName?: string;
  factor?: number;
  listGroup?: WorkdayTypeGroup[];
}

export class WorkdayTypeGroup {
  workdayTypeGroupId?: number;
  groupName?: string;
  groupCode?: string;
  factor?: number;
}
