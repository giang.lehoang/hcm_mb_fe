import { WorkCalendarOrgs } from "./work-calendar-orgs";

export class WorkCalendarDetails {
    workCalendarDetailId?: number;
    workCalendarId?: number;
    dateTimekeeping?: string;
    workdayType?: string;
    workdayTime?: string;
    description?: string;
}
