export class AnnualLeavesSearch {
    employeeCode: string;
    fullName: string;
    empTypeCode: string;
    organizationId: number;
    year?: number;
}
