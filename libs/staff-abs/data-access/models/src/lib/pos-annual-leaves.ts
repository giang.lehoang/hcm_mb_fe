export class PosAnnualLeaves {
    posAnnualLeaveId?: number;
    pgrId?: number;
    pgrName?: string;
    addedDays?: number;
    fromDate?: string;
    toDate?: string;
}
