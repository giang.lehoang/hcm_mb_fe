export class AnnualLeaves {
  employeeCode?: string;
  fullName?: string;
  empTypeCode?: string;
  organizationId?: string;
  positionId?: number;
  year?: number;
  positionName?: string;
  orgName?: string;
  orgNameLevel1?: string;
  orgNameLevel2?: string;
  orgNameLevel3?: string;
  orgNameLevel4?: string;
  fromDate?: string;
  toDate?: string;
  annualDays?: string;
  posBonusDays?: string;
  seniorBonusDays?: string;
  seniority?: number;
  workingMonths?: number;
  unpaidLeaveMonths?: number;
  workAccidentMonths?: number;
  sickLeaveMonths?: number;
  // Phep nam cu
  totalDaysLastYear?: number;
  remainDaysLastYear?: number;
  // Phep trong nam
  totalDaysInYear?: number;
  remainDaysInYear?: number;
  useSwitchedDays?: number;
  usedDays?: number;
}
