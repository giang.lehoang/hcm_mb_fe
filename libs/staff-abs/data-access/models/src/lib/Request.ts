import { NzUploadFile } from "ng-zorro-antd/upload";
import { AbsRequestSupporters } from "./request-supporters";
import { RequestLeave } from "./RequestLeave";
import {EmployeeDetail} from "@hcm-mfe/shared/data-access/models";

export interface Request {
  requestId?: number,
  employeeId?: number | EmployeeDetail,
  employeeCode?: string,
  fullName?: string,
  leaveType?: string,
  files?: File[],
  docIdsDelete?: number[],
  listAbsRequestLeaves?: RequestLeave[],
  requestIds?: number[],
  reasonCode?: string,
  reasonDetail?: string,
  status?: number,
  reason?: string,
  allowApprove?: boolean,
  oldRequestId?: number,
  createDate?: string,
  createdBy?: string,
  lastUpdateDate?: string,
  lastUpdatedBy?: string,
  listAbsRequestSupporters?: AbsRequestSupporters[]
}


