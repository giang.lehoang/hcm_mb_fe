import { WorkCalendarOrgs } from "./work-calendar-orgs";

export class WorkCalendars {
    workCalendarId?: number;
    name?: string;
    monWorkTime?: string;
    tueWorkTime?: string;
    wedWorkTime?: string;
    thuWorkTime?: string;
    friWorkTime?: string;
    satWorkTime?: string;
    sunWorkTime?: string;
    defaultHodidayDate?: string;
    flagStatus?: number;
    createdBy?: string;
    createDate?: Date;
    lastUpdatedBy?: Date;
    lastUpdateDate?: Date;
    startRecord?: number;
    pageSize?: number;
    
    listWorkCalendarOrgs?: WorkCalendarOrgs[];
}
