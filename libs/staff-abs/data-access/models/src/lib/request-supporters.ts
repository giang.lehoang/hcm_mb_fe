import {EmployeeDetail} from "@hcm-mfe/shared/data-access/models";

export interface AbsRequestSupporters {
  requestSupporterId?: number,
  requestId?: number,
  employeeId?: number | EmployeeDetail,
  taskContent?: string,
}
