export class ReasonLeave {
    reasonLeaveId?: number;
    code?: string;
    name?: string;
    workdayTypeCode?: string;
    workdayTypeName?: string;
    groupCode?: string;
    groupName?: string;
    maxTimeOff?: number;
    yearMaxTimeOff?: number;
    timeOffType?: number;
    flagStatus?: number;
    createdBy?: string;
    createDate?: Date;
    lastUpdatedBy?: Date;
    lastUpdateDate?: Date;
    startRecord?: number;
    pageSize?: number;
}
