export class CatalogModel {
    value: number | string;
    label: string;

    constructor(label: any, value: any) {
        this.value = value;
        this.label = label;
    }

}
