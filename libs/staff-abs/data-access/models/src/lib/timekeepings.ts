export class Timekeepings {
  dateOfBirth?: string;
  employeeCode?: string;
  orgName?: string;
  positionName?: string;
  empTypeName?: string;
  employeeId?: number;
  fullName?: string;
  listTimeKeeping: TimekeepingInfor[];
}

export class TimekeepingInfor {
  dateTimekeeping?: string;
  dayOfMonth?: number;
  dayOfWeek?: string;
  totalHours?: number;
  workdayTypeCode?: string;
  timekeeping?: string;
}
