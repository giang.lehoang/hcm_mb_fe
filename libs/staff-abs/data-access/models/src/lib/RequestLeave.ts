import {EmployeeDetail} from "@hcm-mfe/shared/data-access/models";


export interface RequestLeave {
  requestLeaveId?: number,
  requestId?: number,
  fromTime?: string | Date,
  toTime?: string | Date,
  timekeepingDate?: Date,
  reasonLeaveId?: number,
  partOfTime?: string,
  workPlace?: string,
  content?: string,
  note?: string,

  employeeCode?: string,
  employeeId?: number | EmployeeDetail,
  fullName?: string,
  leaveType?: string,
  pageNumber?: number,
}


