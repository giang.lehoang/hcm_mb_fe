export interface AbsRequestLeaves {
  requestLeaveId?: number,
  requestId?: number,
  fromTime?: string,
  toTime?: string,
  timekeepingDate?: string,
  reasonLeaveId?: number,
  partOfTime?: string,
  workPlace?: string,
  content?: string,
  note?: string,
}