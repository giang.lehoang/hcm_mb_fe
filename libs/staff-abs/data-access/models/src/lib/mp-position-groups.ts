export class MpPositionGroups {
    id?: number;
    pgrName?: string;
    pgrCode?: string;
    pgrType?: string;
    parentId?: number;
    flagStatus?: number;
    fromDate?: Date;
    toDate?: Date;
}
