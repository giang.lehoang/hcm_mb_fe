
import * as moment from 'moment';
import {Request, RequestLeave} from "@hcm-mfe/staff-abs/data-access/models";

export class RequestUtils {
  public static filterGetOneRequestLeave(request: Request | any): RequestLeave {
    const fistLeave = request.listAbsRequestLeaves ? request.listAbsRequestLeaves[0] : {};
    let requestLeave = { ...request, ...fistLeave };
    if (requestLeave.fromTime)
      requestLeave.fromTime = moment(requestLeave.fromTime, 'DD/MM/YYYY hh:mm:ss').toDate();
    if (requestLeave.toTime)
      requestLeave.toTime = moment(requestLeave.toTime, 'DD/MM/YYYY hh:mm:ss').toDate();
    if (requestLeave.timekeepingDate)
      requestLeave.timekeepingDate = moment(requestLeave.timekeepingDate, 'DD/MM/YYYY hh:mm:ss').toDate();
    return requestLeave;
  }

  public static convertFromRequestLeaveToRequest(requestLeave: RequestLeave): Request {
    const request = { ...requestLeave, listAbsRequestLeaves: [requestLeave] };
    return request;
  }
}
