import { TemplateRef } from '@angular/core';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import {MBTableConfig} from "@hcm-mfe/shared/data-access/models";

export class TableConfig {
    // Truyền động tham số thứ 2
    public static GET_ANNUAL_LEAVES_TBC(pageSize: number,
                                        workInfoThTmpl: TemplateRef<NzSafeAny>,
                                        workInfoTdTmpl: TemplateRef<NzSafeAny>,
                                        lastYearLeaveNumberTransferredTmpl: TemplateRef<NzSafeAny>,
                                        leaveInYear: TemplateRef<NzSafeAny>): MBTableConfig {
        return {
            headers: [
                {
                    title: 'staffAbs.staffLeave.annualLeaves.table.employeeCode',
                    field: 'employeeCode',
                    tdClassList: ['text-left'], thClassList: ['text-center'],
                    fixed: true,
                    fixedDir: 'left',
                    width: 100
                },
                {
                    title: 'staffAbs.staffLeave.annualLeaves.table.fullName',
                    field: 'fullName',
                    tdClassList: ['text-nowrap'], thClassList: ['text-center'],
                    width: 150
                },
                {
                    title: 'staffAbs.staffLeave.annualLeaves.table.positionName',
                    field: 'positionName',
                    tdClassList: ['text-left'], thClassList: ['text-center'],
                    width: 150
                },
                {
                    title: 'staffAbs.staffLeave.annualLeaves.table.orgName',
                    field: 'orgName',
                    tdClassList: ['text-left'], thClassList: ['text-center'],
                    width: 150
                },
                {
                    title: 'staffAbs.staffLeave.annualLeaves.table.orgNameLevel1',
                    field: 'orgNameLevel1',
                    tdClassList: ['text-left'], thClassList: ['text-center'],
                    width: 150
                },
                {
                    title: 'staffAbs.staffLeave.annualLeaves.table.orgNameLevel2',
                    field: 'orgNameLevel2',
                    tdClassList: ['text-left'], thClassList: ['text-center'],
                    width: 150
                },
                {
                    title: 'staffAbs.staffLeave.annualLeaves.table.orgNameLevel3',
                    field: 'orgNameLevel3',
                    tdClassList: ['text-left'], thClassList: ['text-center'],
                    width: 150
                },
                {
                    title: 'staffAbs.staffLeave.annualLeaves.table.orgNameLevel4',
                    field: 'orgNameLevel4',
                    tdClassList: ['text-left'], thClassList: ['text-center'],
                    width: 150
                },
                {
                    title: 'staffAbs.staffLeave.annualLeaves.table.seniority',
                    field: 'seniority',
                    tdClassList: ['text-left'], thClassList: ['text-center'],
                    width: 150
                },
                {
                    title: 'staffAbs.staffLeave.annualLeaves.table.workInfo',
                    field: '',
                    tdClassList: ['text-left'], thClassList: ['text-center'],
                    width: 600
                },
                {
                    title: 'staffAbs.staffLeave.annualLeaves.table.lastYearLeaveNumberTransferred',
                    field: '',
                    thTemplate: lastYearLeaveNumberTransferredTmpl,
                    tdClassList: ['text-left'], thClassList: ['text-center'],
                    width: 300
                },
                {
                    title: 'staffAbs.staffLeave.annualLeaves.table.leaveInYear',
                    field: '',
                    thTemplate: leaveInYear,
                    tdClassList: ['text-left'], thClassList: ['text-center'],
                    width: 300
                },
            ],
            total: 0,
            needScroll: true,
            loading: false,
            size: 'small',
            pageSize: pageSize,
            pageIndex: 1,
        }
    }
}
