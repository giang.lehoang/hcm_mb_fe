export class UrlConstant {
  public static readonly API_VERSION = '/v1.0';

  public static readonly REASON_LEAVE = {
    PREFIX: '/reason-leaves',
    DELETE: '/{reasonLeaveId}',
    DETAIL: '/{reasonLeaveId}'
  };
  public static readonly WORK_CALENDAR = {
    PREFIX: '/work-calendars',
    DELETE: '/{workCalendarId}',
    DETAIL: '/{workCalendarId}',
    ALL_ACTIVE: '/get-all-active',
    ASSIGN: '/{workCalendarId}/assign'
  };
  public static readonly WORK_CALENDAR_DETAIL = {
    PREFIX: '/work-calendar-details',
    DELETE: '/{workCalendarDetailId}',
    DETAIL: '/{workCalendarDetailId}'
  };
  public static readonly APPROVAL_FLOW = {
    PREFIX: '/approval-flow',
    SAVE: '',
    LEVEL_NV: '/mp-position-groups/RANKING',
    REASONS: '/reason-leaves/LEAVE/all',
    DELETE: '/{approvalFlowId}',
    DETAIL: '/{approvalFlowId}',
    SEARCH: '/search',
    position_groups: '/position_groups/'
  };

  public static readonly SEARCH_FORM = {
    ANNUAL_LEAVES: '/annual-leaves/search',
    ANNUAL_LEAVES_CALCULATE: '/annual-leaves/calculate/{year}',
    POS_ANNUAL_LEAVES: '/pos-annual-leaves/search',
    LOOKUP_EMP_TYPE: '/lookup-values?typeCode=DOI_TUONG_CV',
    LOOKUP_POSITION_GROUP: '/mp-position-groups/TITLE_GROUP',
    POSITION_GROUP_ALL: '/mp-position-groups/get-all',
    WORKDAY_TYPE: '/workday-type',
    TIMEKEEPING: '/time-keepings',
    TIMEKEEPING_AUTO: '/time-keepings/auto',
    EXPORT_AT_TIME: '/time-keepings/export-at-time',
    EXPORT_CHANGED: '/time-keepings/export-changed'
  };

  public static readonly SAVE = {
    POS_ANNUAL_LEAVES: '/pos-annual-leaves',
  };

  public static readonly GET = {
    POS_ANNUAL_LEAVES: '/pos-annual-leaves/',
  };

  public static readonly DELETE = {
    POS_ANNUAL_LEAVES: '/pos-annual-leaves/',
    WORKDAY_TYPE: '/workday-type/{id}'
  };

  public static readonly EXPORT_REPORT = {
    ANNUAL_LEAVES: '/annual-leaves/export',
    POS_ANNUAL_LEAVES: '/pos-annual-leaves/export',
    TIMEKEEPING: '/time-keepings/export'
  };

  public static readonly REQUEST_PATH = '/requests';
  public static readonly REQUEST_LEAVE_PATH = '/request-leaves';

  public static readonly CATALOGS = {
    PREFIX: '/lookup-values'
  }

}
