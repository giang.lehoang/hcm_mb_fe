
import { FormGroup, FormControl } from '@angular/forms';
import { Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild, OnDestroy, ElementRef } from '@angular/core';
import { NzDropDownDirective } from 'ng-zorro-antd/dropdown';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import {CustomToastrService} from "@hcm-mfe/shared/core";
import {DataService} from "@hcm-mfe/model-organization/data-access/services";
import {userConfig} from "@hcm-mfe/shared/common/constants";
import {InitJobComponent} from "../../../../../feature/list-job/src/lib/init-job/init-job.component";
import {Pagination} from "@hcm-mfe/model-organization/data-access/models";

export interface Item extends Object {
  id: number;
  value: string;
  checked?:boolean
}

@Component({
  selector: 'app-job-select',
  templateUrl: './job-select.component.html',
  styleUrls: ['./job-select.component.scss'],
})
export class JobSelectComponent implements OnInit, OnDestroy {
  @Input('list-select') fruits: any[] = [];
  @Input('list-input') allFruits: any[] = [];
  @Input() type:string |undefined
  @Input() key = 'id';
  @Input() value = 'value';
  @Input() height = '40px';
  @Input() fontSize = '12px';
  @Input() showCreate = true;
  @Input() error = false;
  @Input() disable = false;
  @Input() showCheckAll = false;
  @Input() errorMessage:string | undefined;
  @ViewChild('dropdown', { static: true }) dropDown: NzDropDownDirective | undefined;
  @ViewChild('scoll') scoll:ElementRef | undefined;
  @Output('eventAdd') addJobEvent:EventEmitter<boolean> = new EventEmitter<boolean>()
  pagination:Pagination = new Pagination(userConfig.pageSize);
  mapSelect:Map<number, Item> = new Map<number,Item>()
  listFilter: Item[] = [];
  isVisible:boolean | undefined;
  isClose:boolean | undefined;
  inputValue:string | undefined;
  form:FormGroup | undefined;
  modal: NzModalRef | undefined;
  jobMax:number;
  isCheckAll:boolean;

  constructor( readonly modalService: NzModalService, readonly toastr:CustomToastrService, readonly dataService:DataService) {
    this.isClose = false
    this.jobMax = 20;
    this.isCheckAll = false;
  }

  ngOnInit(): void {
    if(this.dropDown) {
      this.dropDown.nzClickHide = false;
    }
    this.form = new FormGroup({
      inputValue: new FormControl('')
    })
  }

  ngOnDestroy(): void {
    this.addJobEvent.unsubscribe()
    if(this.modal){
      this.modal.componentInstance?.closeEvent.unsubscribe();
    }
  }

  checkAllJob(){
    if(!this.mapSelect.size){
      this.isCheckAll = false;
      return;
    }

    for (const item of this.allFruits){
      if(!this.mapSelect.has(item[this.key])){
        this.isCheckAll = false;
        return;
      }
    }
    this.isCheckAll = true;
  }

  onSelect(op:any){
    if(this.mapSelect.has(op[this.key])){
      this.mapSelect.delete(op[this.key]);
      this.isCheckAll = false;
    } else {
      this.mapSelect.set(op[this.key], op);
    }
  }


  onTyping(value:KeyboardEvent) {
    const filterValue = (value.target as HTMLTextAreaElement).value.toLowerCase();
    this.listFilter = this.allFruits.filter((fruit:any) => fruit[this.value].toLowerCase().indexOf(filterValue) >= 0);
    this.listFilter.forEach( (item:any) => {
      item.checked = this.mapSelect.has(item[this.key])
    })
    this.jobMax = 20;
  }


  onInitJob(titleTmpl: TemplateRef<any>, footerTmpl: TemplateRef<any>){
    this.modal = this.modalService.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 2 : window.innerWidth,
      nzTitle: titleTmpl,
      nzContent: InitJobComponent,
      nzComponentParams: {
        jobShare: {action: null, job:null, type: this.type, },
      },
      nzBodyStyle: {
        padding: '0px',
        border: 'none',
        'border-radius':'20px'
      },
      nzFooter: footerTmpl
    });

    this.modal.componentInstance.closeEvent.subscribe((data:boolean) => {
      this.addJobEvent.emit(data);
      this.modal?.destroy();
      this.isVisible = true;
    })
  }

  onSelectJob(){
    while(this.fruits.length){
      this.fruits.pop();
    }
    this.mapSelect.forEach(value => {
      this.fruits.push(value);
    })
    this.isVisible = false;
  }


  onCloseTag(index:number, item:any, event:MouseEvent){
    event.stopPropagation();
    this.fruits.splice(index, 1);
    item.checked = false;
    this.mapSelect.delete(item[this.key]);
  }

  clearAll(){
    this.mapSelect.clear();
    this.listFilter.forEach((item) => {
      (<Item>item).checked = false;
    })
    while(this.fruits.length){
      this.fruits.pop();
    }
  }


  change(value:boolean){
    if(value){
      for(const item of this.fruits){
        if(!this.mapSelect.has(item[this.key])){
          this.mapSelect.set(item[this.key], item);
        }
      }
      this.listFilter = this.allFruits.filter((fruit:any) => fruit[this.value].toLowerCase().indexOf(this.form?.value.inputValue.trim()) >= 0);
      this.listFilter.forEach( (item:any) => {
        item.checked = this.mapSelect.has(item[this.key]);
      });
      this.checkAllJob();
    } else {
      this.mapSelect.clear();
      this.listFilter.forEach( (item:any) => {
        item.checked = this.mapSelect.has(item[this.key]);
      })
    }
    this.jobMax = 20;
  }

  onScroll(event:Event){
    const el = this.scoll?.nativeElement;
    if(el.scrollHeight === el.clientHeight + el.scrollTop){
      this.jobMax = this.jobMax + 20;
    }
  }

  trackByFn(item:any){
    return item[this.key];
  }

  checkAll($event: any) {
    this.isCheckAll = $event;
    if($event){
      this.listFilter.forEach(item => {
        item.checked = true;
        this.onSelect(item);
      })
    } else {
      this.mapSelect.clear();
    }
  }
}
