import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {JobSelectComponent} from "./job-select/job-select.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
import {NzTagModule} from "ng-zorro-antd/tag";
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzGridModule} from "ng-zorro-antd/grid";
import {NzSwitchModule} from "ng-zorro-antd/switch";
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  imports: [CommonModule, FormsModule, NzDropDownModule, SharedUiMbButtonModule, NzCheckboxModule, NzIconModule, NzGridModule,
    ReactiveFormsModule, SharedUiMbInputTextModule, NzToolTipModule, NzTagModule, NzButtonModule, NzSwitchModule, TranslateModule],
  declarations: [JobSelectComponent],
  exports: [JobSelectComponent]
})
export class ModelOrganizationUiJobSelectModule {}
