import { Component, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { NzDropDownDirective } from 'ng-zorro-antd/dropdown';
import {Pagination} from "@hcm-mfe/model-organization/data-access/models";

@Component({
  selector: 'app-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.scss'],
})
export class SearchInputComponent {
  @ViewChild('dropdown', { static: true }) dropDown: NzDropDownDirective | undefined;
  @Input('listInput') list:any[] = [];
  @Input('listOutput') listObjectSelect:any[] = [];
  @Input() value = 'id';
  @Input() label = 'value';
  @Output() emitType = new EventEmitter<string>();
  @Output() emitChange = new EventEmitter<any>();
  @Output() loadMore = new EventEmitter<boolean>();
  @Input() itemSelected = new Set<number>();
  @Input() type = true;
  @Input() clickHide = false;
  @Input() trigger:'click' | 'hover' = 'hover';
  @Input() showIcon = true;
  @Input() showValue = ''
  @Input() showError = false;
  @Input() placeholder = '';
  @Input() pagination: Pagination | undefined;
  nzVisible: boolean;

  constructor() {
    this.nzVisible = false;
  }

  onTyping(value:Event) {
    this.emitType.emit((value.target as HTMLTextAreaElement ).value);
  }

  checked(item:any) {
    if(this.type){
      this.checkMutil(item);
    } else {
      this.nzVisible = false;
      this.showValue = item[this.value];
      this.emitChange.emit(item);
      this.list = [];
    }
  }

  checkMutil(item:any){
    if (this.itemSelected.has(item[this.value])) {
      this.itemSelected.delete(item[this.value]);
      for (let i = 0; i < this.listObjectSelect.length; i++) {
        if (this.listObjectSelect[i][this.value] === item[this.value]) {
          this.listObjectSelect.splice(i, 1);
          break;
        }
      }
      this.emitChange.emit({ type: 0, value: item });
      return;
    }
    this.listObjectSelect.push(item);
    this.itemSelected.add(item[this.value]);
    this.emitChange.emit({ type: 1, value: item });
  }

  trackByFn(index:number, item:any) {
    return item[this.value];
  }

  loadMoreClick(){
    this.loadMore.emit(true);
  }

}
