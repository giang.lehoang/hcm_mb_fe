import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {SearchInputComponent} from "./search-input/search-input.component";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {NzInputModule} from "ng-zorro-antd/input";
import {TranslateModule} from "@ngx-translate/core";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzTableModule} from "ng-zorro-antd/table";
import {FormsModule} from "@angular/forms";

@NgModule({
  imports: [CommonModule, NzDropDownModule, NzInputModule, FormsModule, TranslateModule, NzToolTipModule, SharedUiMbButtonModule, NzTableModule],
  declarations:[SearchInputComponent],
  exports: [SearchInputComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ModelOrganizationUiSearchInputModule {}
