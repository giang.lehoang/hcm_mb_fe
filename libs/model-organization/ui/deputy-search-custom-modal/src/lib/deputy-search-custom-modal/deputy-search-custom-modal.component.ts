import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { OrganizationalModelTemplateService } from '@hcm-mfe/model-organization/data-access/services';
import { debounceTime, distinctUntilChanged, fromEvent } from 'rxjs';
@Component({
  selector: 'app-deputy-search-custom-modal',
  templateUrl: './deputy-search-custom-modal.component.html',
  styleUrls: ['./deputy-search-custom-modal.component.scss'],
})
export class DeputySearchCustomModalComponent implements OnInit {
  param = { keyword: '', page: 0, size: 8};
  isLoadingPage = true;
  @Output() deputySearchEvent = new EventEmitter<any>();
  @ViewChild('searchInput', { static: true }) searchInput: ElementRef | undefined;
  listSettingPaginationCustom = {
    size: 0,
    total: 0,
    pageIndex: 0,
    numberOfElements: 0
  };
  listSurrogate:any[] = [];
  constructor(readonly modal: NzModalService, readonly orgService: OrganizationalModelTemplateService) {
    this.fetchData();
  }

  fetchData(){
    this.isLoadingPage = true;
    this.orgService.getListSurrogate(this.param).subscribe((data: any) => {
      this.listSurrogate = data.data.content;
      console.log(this.listSurrogate)
      this.listSettingPaginationCustom.size = data.data.size;
      this.listSettingPaginationCustom.total = data.data.totalElements;
      this.listSettingPaginationCustom.pageIndex = data.data.pageable.pageNumber;
      this.listSettingPaginationCustom.numberOfElements = data.data.numberOfElements;
      this.isLoadingPage = false;
    });
  }

  ngOnInit(): void {
    fromEvent(this.searchInput?.nativeElement, 'keyup')
      .pipe(debounceTime(800), distinctUntilChanged())
      .subscribe((val) => {
        this.param.keyword = this.searchInput?.nativeElement.value;
        this.fetchData();
      });
  }

  getListTemplateBasePagination(value:any): void {
    this.param.page = value - 1;
    this.isLoadingPage = true;
    this.fetchData();
  }

  chooseDeputy(i:any): void {
    this.deputySearchEvent.emit(this.listSurrogate[i]);
    const ref: NzModalRef = this.modal.info();
    ref.close();
  }
}
