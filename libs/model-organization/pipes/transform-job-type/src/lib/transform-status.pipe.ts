import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'jobType',
})
export class TransformStatusPipe implements PipeTransform {
  transform(value: any, ...args: any[]) {
    value = value.toUpperCase();
    if (value === 'ORG') {
      return 'MH cứng';
    }

    if (value === 'PRJ') {
      return 'MH dự án';
    }

    if (value === 'DV') {
      return 'MH dịch vụ';
    }

    return value;
  }
}
