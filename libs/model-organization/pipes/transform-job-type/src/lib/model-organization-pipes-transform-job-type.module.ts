import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TransformStatusPipe} from "./transform-status.pipe";

@NgModule({
  imports: [CommonModule],
  declarations: [TransformStatusPipe],
  exports: [TransformStatusPipe]
})
export class ModelOrganizationPipesTransformJobTypeModule {}
