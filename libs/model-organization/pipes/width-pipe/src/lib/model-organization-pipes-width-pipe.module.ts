import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {WidthDirective} from "./width-directive";

@NgModule({
  imports: [CommonModule],
  declarations: [WidthDirective],
  exports: [WidthDirective]
})
export class ModelOrganizationPipesWidthPipeModule {}
