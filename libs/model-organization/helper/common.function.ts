export const  renderFlagStatus = (status: number | string) => {
  switch (status) {
    case 0:
      return {
        text: 'Không hoạt động',
        class: 'tag-custom ant-tag-magenta-custom',
      };
    case 1:
      return {
        text: 'Đang hoạt động',
        class: 'tag-custom ant-tag-green-custom',
      };
    case 2:
      return {
        text: 'Chờ duyệt',
        class: 'tag-custom ant-tag-orange-custom',
      };
    case 3:
      return {
        text: 'Từ chối',
        class: 'tag-custom ant-tag-red-custom',
      };
    case 4:
      return {
        text: 'Phê duyệt',
        class: 'tag-custom ant-tag-green-custom',
      };
      case 5:
      return {
        text: 'Tạo mới',
        class: 'tag-custom ant-tag-orange-custom',
      };
    case 6:
      return {
        text: 'Xác nhận',
        class: 'tag-custom ant-tag-orange-custom',
      };
    case 7:
    return {
        text: 'Chưa tạo',
        class: 'cyan tag-custom ant-tag-cyan-custom',
      };
    default:
      return {
        text: 'Tạo mới',
        class: 'tag-custom ant-tag-orange-custom',
      };
  }
}

export const  renderUseStatus = (status: number | string) => {
  switch (status) {
    case 0:
      return {
        text: 'Không sử dụng',
        class: 'tag-custom ant-tag-magenta-custom',
      };
    case 1:
      return {
        text: 'Sử dụng',
        class: 'tag-custom ant-tag-green-custom',
      };
    default:
      return {
        text: 'Sử dụng',
        class: 'tag-custom ant-tag-orange-custom',
      };
  }
}

