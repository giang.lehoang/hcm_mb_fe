import { Component, Injector, OnInit, TemplateRef } from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd/modal';
import {
  listStatusJob, LookupCode, ManageCategory,
  Pagination, ParamsModel,
} from '@hcm-mfe/model-organization/data-access/models';
import { MICRO_SERVICE, Mode, userConfig } from '@hcm-mfe/shared/common/constants';
import { CategoryService, JobService } from '@hcm-mfe/model-organization/data-access/services';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { CategoryInitComponent } from '@hcm-mfe/model-organization/feature/category';
import { renderUseStatus } from '../../../../../helper/common.function';
import * as FileSaver from 'file-saver';
import { FunctionCode } from '@hcm-mfe/shared/common/enums';
import { ActionDocumentAttachedFileComponent } from '@hcm-mfe/goal-management/ui/form-input';
import * as moment from 'moment';
@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent extends BaseComponent implements OnInit {
  loading: boolean;
  modalRef: NzModalRef | undefined;
  categoryParam:ParamsModel = new ParamsModel();
  dataTable: ManageCategory[] = [];
  pagination: Pagination = new Pagination(userConfig.pageSize);
  listStatus = listStatusJob;
  listLookupCode: Array<LookupCode> = [];
  listManageCategory: Array<ManageCategory> = [];
  isInit: boolean;

  renderFlagStatus = renderUseStatus;

  constructor(injector: Injector, readonly jobService: JobService, private readonly categoryService: CategoryService) {
    super(injector)
    this.isInit = true;
    this.loading = false;
  }

  ngOnInit(): void {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ORGANIZATION_CATEGORY}`);
    console.log(this.objFunction);
    this.getLookupCode();
  }

  getLookupCode() {
    this.loading = true;
    this.categoryService.getLookupCode().subscribe(data => {
      this.listLookupCode = data?.data;
      this.search();
      this.loading = false;
    })
  }
  getCategoryManage(lcoId: number) {
    this.categoryParam.childId = '';
    this.categoryService.getCategoryManage(lcoId).subscribe(data => {
      this.listManageCategory = data.data;
    })
  }

  callGetCategory() {
    this.loading = true;
    this.categoryService.listCategory(this.checkParams()).subscribe(data => {
      this.loading = false
      this.dataTable = data?.data?.content;
      this.pagination.currentPage = data.data.pageable.pageNumber + 1
      this.pagination.numberOfElements = data.data.numberOfElements
      this.pagination.totalElements = data.data.totalElements
      this.pagination.totalPages = data.data.totalPages
      this.pagination.size = data.data.size
      this.caculatePaging();
      this.isInit = false;
    }, error => {
      this.loading = false
      this.toastrCustom.error(error.message)
    });
  }

  showModal(data?:ManageCategory) {
    this.modalRef = this.modal.create({
      nzWidth: this.getInnerWidth(2),
      nzTitle: data?.lvaId ? "Chỉnh sửa" : "Thêm mới",
      nzComponentParams: {
        listLookupCode: this.listLookupCode,
        id: data?.lvaId,
        categoryObj: data,
        listManageCategory: this.listManageCategory
      },
      nzBodyStyle: {
        padding: '0px 24px 24px'
      },
      nzFooter: null,
      nzContent: CategoryInitComponent,
    });
    this.modalRef.componentInstance.closeEvent.subscribe((isClose:boolean) => {
        this.search();
      this.modalRef?.destroy();
    })
  }

  search() {
    this.categoryParam.page = 0;
    if(!this.isValid()){
      this.dataTable = [];
      this.pagination.totalElements = 0
    } else {
      this.callGetCategory();
    }
  }

  caculatePaging() {
    this.pagination.result = this.pagination.totalElements;
    this.pagination.min = this.getIndex(0);
    this.pagination.max = this.pagination.size * (this.pagination.currentPage - 1) + this.pagination.numberOfElements;
  }

  getIndex(i: number) {
    return this.pagination.size * (this.pagination.currentPage - 1) + (i + 1);
  }

  changePage() {
    this.categoryParam.page = this.pagination.currentPage - 1;
    this.callGetCategory();
  }

  override triggerSearchEvent(): void {
    this.search();
  }

  override ngOnDestroy(): void {
    this.modalRef?.destroy()
  }

  trackByFn(index:number, item: ManageCategory) {
    return item.lvaId;
  }

  deletePositionGroup(id:number) {
    this.deletePopup.showModal(() => {
      this.loading = true;
      this.categoryService.deleteCategory(id).subscribe(data => {
        this.loading = false;
        this.toastrCustom.success();
        this.search();
      }, error => {
        this.loading = false;
        this.toastrCustom.error(error.message)
      })
    })
  }
  onChangeCategory(event:any){
    if(event && this.categoryParam?.lcoIds?.length === 1){
      this.getCategoryManage(this.categoryParam?.lcoIds[0]);
    } else {
      this.listManageCategory = [];
      this.categoryParam.childId = '';
    }
  }
  checkParams(isExport?:boolean){
    const params:any = {};
    params.lcoIds = this.categoryParam.lcoIds ? this.categoryParam.lcoIds.join(",") : '';
    params.lvaParentId = this.categoryParam.childId ? this.categoryParam.childId : '';
    params.page = this.categoryParam.page;
    params.size = this.categoryParam.size;
    params.lvaMean = this.categoryParam.lvaMean ? this.categoryParam.lvaMean:'';
    params.flagStatus = this.categoryParam.flagStatus ? this.categoryParam.flagStatus:'';
    // params.lcoIds = isExport?this.listLookupCode.map(value => value.id):this.listLookupCode.map(value => value.id).join(",");
    params.fromDate = this.categoryParam.fromDate ? moment(this.categoryParam.fromDate).format('DD/MM/YYYY') : '';
    params.toDate = this.categoryParam.toDate ? moment(this.categoryParam.toDate).format('DD/MM/YYYY') : '';
    return params;
  }
  exportCategory() {
    this.categoryService.exportCategory(this.checkParams(true)).subscribe((data:any) => {
      const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      FileSaver.saveAs(blob, 'Danh mục.xlsx');
    });
  }
  isValid(): boolean {
    if(this.categoryParam?.lcoIds?.length === 0 && !this.categoryParam.lvaMean
      && !this.categoryParam.flagStatus && !this.categoryParam.toDate && !this.categoryParam.fromDate && !this.categoryParam.childId){
      return false;
    } else{
      return true;
    }
  }
  showUpload(footerTmpl: TemplateRef<any>) {
    this.modalRef = this.modal.create({
      nzWidth: this.getInnerWidth(3.5),
      nzTitle: this.translate.instant('common.label.uploadFileExel'),
      nzContent: ActionDocumentAttachedFileComponent,
      nzComponentParams: {
        mode: Mode.ADD,
        url: this.categoryService.importExcel(),
        microService: MICRO_SERVICE.POL,
        fileName: 'TEMPLATE_DANH_MUC',
        module: 'MODEL_PLAN',
        urlDownload:this.categoryService.templateExcel()
      },
      nzFooter: footerTmpl,
    });
    this.modalRef.afterClose.subscribe((result) => (result?.refresh ? this.search() : ''));
  }
  getInnerWidth(point: number): number {
    if (window.innerWidth > 767) {
      return window.innerWidth / point > 1100 ? 1100 : window.innerWidth / point;
    }
    return window.innerWidth;
  }
}
