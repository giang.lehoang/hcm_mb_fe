import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PositionGroupInitComponent } from './category-init.component';

describe('PositionGroupInitComponent', () => {
  let component: PositionGroupInitComponent;
  let fixture: ComponentFixture<PositionGroupInitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PositionGroupInitComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PositionGroupInitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
