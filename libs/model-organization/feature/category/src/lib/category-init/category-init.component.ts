import { Component, EventEmitter, Injector, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {
  listStatusJob, LookupCode, ManageCategory, ParamsModel, CategoryRequest
} from '@hcm-mfe/model-organization/data-access/models';
import { CategoryService, JobService } from '@hcm-mfe/model-organization/data-access/services';
import {regexSpecialForLine} from "@hcm-mfe/shared/common/constants";
import {maxInt32} from "@hcm-mfe/shared/common/enums";
import * as moment from "moment/moment";


@Component({
  selector: 'app-category-init',
  templateUrl: './category-init.component.html',
  styleUrls: ['./category-init.component.scss']
})
export class CategoryInitComponent extends BaseComponent implements OnInit, OnDestroy {

  isSubmitted: boolean;
  categoryForm: FormGroup;
  loading: boolean;
  isDisable: boolean;
  isShowLevel1=false
  isShowLevel2 = false;
  require = "<span class='label__required'>*</span>"
  @Input() id: number | undefined;
  @Input() categoryObj: ManageCategory | undefined;
  @Input() listLookupCode: Array<LookupCode> = [];
  @Input() listManageCategory: Array<ManageCategory> = [];
  @Output() closeEvent = new EventEmitter<boolean>();
  listLookupCodeParent: Array<LookupCode> = [];
  listLookupCodeChlid1: Array<LookupCode> = [];
  listLookupCodeChlid2: Array<LookupCode> = [];
  listCategoryChild: Array<ManageCategory> = [];
  listStatus = listStatusJob;
  constructor(readonly jobService: JobService, injector: Injector,private readonly categoryService: CategoryService) {
    super(injector)
    this.loading = false;
    this.isSubmitted = false;
    this.isDisable = false;
    this.categoryForm = new FormGroup({
      lvaId: new FormControl('', Validators.required),
      flagStatus: new FormControl('1'),
      lvaValue: new FormControl(''),
      categoryManager: new FormControl('', Validators.required),
      valueManager: new FormControl('', Validators.required),
      categoryChild1: new FormControl('', Validators.required),
      valueChild1: new FormControl('', Validators.required),
      categoryChild2: new FormControl('', Validators.required),
      valueChild2: new FormControl('', Validators.required),
      fromDate: new FormControl(''),
      toDate: new FormControl('')
    })
  }

  ngOnInit(): void {
    if (this.id) {
      this.listLookupCodeParent = this.listLookupCode;
      this.loading = true;
      this.isDisable = true;
      this.categoryService.detailCategory(this.id).subscribe(data => {
        this.loading = false;
        this.categoryForm.controls['categoryManager'].setValue(this.categoryObj?.lcoId);
        this.categoryForm.controls['valueManager'].setValue(data.data.lvaMean);
        this.categoryForm.controls['lvaId'].setValue(data.data.lvaId);
        this.categoryForm.controls['flagStatus'].setValue(''+data.data.flagStatus);
        this.categoryForm.controls['fromDate'].setValue(data.data.fromDate ? moment(data.data.fromDate).toDate() : '');
        this.categoryForm.controls['toDate'].setValue(data.data.toDate ? moment(data.data.toDate).toDate() : '');
      }, error => {
        this.loading = false
      })
    } else {
      this.listLookupCodeParent = this.listLookupCode.filter(item => !item.parentId);
      this.listManageCategory = [];
      this.categoryForm.controls['flagStatus'].setValue('1');
      this.categoryForm.controls['flagStatus'].disable();
    }
  }


  submit() {
    this.isSubmitted = true;

    const request: CategoryRequest = {
      lcoId: this.categoryForm.controls['categoryManager'].value,
      flagStatus: this.categoryForm.controls['flagStatus'].value,
      fromDate: this.categoryForm.controls['fromDate'].value ? moment(this.categoryForm.controls['fromDate'].value).format('DD/MM/YYYY') : '',
      toDate: this.categoryForm.controls['toDate'].value ? moment(this.categoryForm.controls['toDate'].value).format('DD/MM/YYYY') : '',
      lvaMean: this.isShowLevel1 ? '' : this.categoryForm.controls['valueManager'].value,
      lvaId: this.isShowLevel1 ? this.categoryForm.controls['valueManager'].value : '',
      parentValue: "",
      sub1: null,
      sub2: null
    }


    if(!this.categoryForm.controls['categoryManager'].value || !this.categoryForm.controls['valueManager'].value){
      return;
    }

    if(this.isShowLevel1){
      if(!this.categoryForm.controls['categoryChild1'].value || !this.categoryForm.controls['valueChild1'].value){
        return;
      }

      request.sub1 = {
        subLcoId: this.categoryForm.controls['categoryChild1'].value,
        subLvaId: this.isShowLevel2 ? this.categoryForm.controls['valueChild1'].value : '',
        subLvaMean: this.isShowLevel2 ? '' : this.categoryForm.controls['valueChild1'].value,
        subParentValue: ""
      }
    }

    if(this.isShowLevel2){
      if(!this.categoryForm.controls['categoryChild2'].value || !this.categoryForm.controls['valueChild2'].value){
        return;
      }
      request.sub2 = {
        subLcoId: this.categoryForm.controls['categoryChild2'].value,
        subLvaId: '',
        subLvaMean: this.categoryForm.controls['valueChild2'].value,
        subParentValue: ""
      }
    }

    if(this.id){
      request.lvaId = this.categoryForm.controls['lvaId'].value
      this.categoryService.updateCategory(request).subscribe(data => {
        this.loading = false;
        this.toastrCustom.success(this.translate.instant('modelOrganization.message.create'))
        this.closeEvent.emit(true)
      }, error => {
        this.toastrCustom.error(error.message)
        this.loading = false;
      });
      return;
    }
    console.log(request)
    this.loading = true;
      this.categoryService.createCategory(request).subscribe(data => {
        this.loading = false;
        this.toastrCustom.success(this.translate.instant('modelOrganization.message.create'))
        this.closeEvent.emit(true)
      }, error => {
        // const message = error.message
        // if (message === this.translate.instant('modelOrganization.validate.duplicatePos')) {
        //   this.categoryForm.controls['lvaMean'].setErrors({ duplicatePos: true })
        // } else {
        //   this.toastrCustom.error(message)
        // }
        this.toastrCustom.error(error.message)
        this.loading = false;
      })

  }

  onKeypress(event: KeyboardEvent) {
    const pattern = regexSpecialForLine;
    if (pattern.test(event.key)) {
      event.preventDefault();
    }
  }
  getManageCategory(lcoId: number, level:number, id:number|undefined) {
    this.loading = true;
    const request = {
      lcoIds: lcoId,
      page: 0,
      size: maxInt32,
      lvaParentId: id ? id : ''
    };

    this.categoryService.listCategory(request).subscribe(data => {
      const lcoChild = this.listLookupCode.find(item => item.lcoId === lcoId);
      if(level===0){
        this.listManageCategory = data.data.content;
        if(lcoChild){
          this.listLookupCodeChlid1 = this.listLookupCode.filter(item => item.lcoId === lcoChild.childId);
          this.isShowLevel1 = this.listLookupCodeChlid1 ? false : true;
          this.isShowLevel2 = this.isShowLevel1;
          this.categoryForm.controls['categoryChild1'].setValue(this.listLookupCodeChlid1[0]?.lcoId);
          this.onChangeType(this.categoryForm.controls['categoryChild1'].value, 1);
        } else {
          this.listLookupCodeChlid1 = [];
          this.isShowLevel1 = false;
        }
      } else {
        this.listCategoryChild = data.data.content;
        if(lcoChild){
          this.listLookupCodeChlid2 = this.listLookupCode.filter(item => item.lcoId === lcoChild.childId);
          this.isShowLevel2 = this.listLookupCodeChlid2 ? false : true;
          this.categoryForm.controls['categoryChild2'].setValue(this.listLookupCodeChlid2[0]?.lcoId);
        } else {
          this.listLookupCodeChlid2 = [];
          this.isShowLevel2 = false;
        }
      }
      this.loading = false;
    },error => {
      this.loading = false;
    })
  }
  changeValue(event:string) {
    const pattern = regexSpecialForLine;
    if (pattern.test(event)) {
      setTimeout(() => {
        this.categoryForm.controls['parentValue'].setValue("");
      }, 500);
    }
  }

  onChangeType(e:any, level:number) {

    let id:number|undefined;

    if(level === 1){
      const parent =  this.categoryForm.controls['valueManager'].value;
      if(parent){
        const category = this.listManageCategory.filter(item => item.lvaValue === parent);
        id = category.length ? category[0].lvaId : undefined;
      }
    }


    if (e) {
      this.getManageCategory(e, level, id);
    } else {
      if(level === 0){
        this.listManageCategory = [];
      } else {
        this.listCategoryChild = [];
      }
    }
  }

  onChangeLva(e:string, lcoId:number){

    if(!lcoId){
      return;
    }
    // const id = this.listManageCategory.filter(item => item.lvaValue === e)[0].lvaId;
    // if(!id){
    //   return;
    // }
    this.loading = true;
    const request = {
      lcoIds: lcoId,
      page: 0,
      size: maxInt32,
      lvaParentId: e ? e : ''
    };

    this.categoryService.listCategory(request).subscribe(data => {
      this.listCategoryChild = data.data.content;
      if(this.isShowLevel2){
        this.categoryForm.controls['valueChild1'].setValue('');
      }
      this.loading = false;
    },error => {
      this.loading = false;
    })
  }

  changeSw(e:boolean, level:number){
    if(level === 1){
      this.categoryForm.controls['valueManager'].setValue('');
      this.categoryForm.controls['valueChild1'].setValue('');
      if(!e){
        this.isShowLevel2 = false
      }
    } else {
      this.categoryForm.controls['valueChild1'].setValue('');
    }
  }

  override ngOnDestroy(): void {
    this.closeEvent.complete()
  }

  closeModal() {
    this.closeEvent.emit(false);
  }
}
