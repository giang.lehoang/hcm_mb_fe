import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { RouterModule } from '@angular/router';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { CategoryComponent } from './category/category.component';
import { CategoryInitComponent } from './category-init/category-init.component';
import { SharedUiMbDatePickerModule } from '@hcm-mfe/shared/ui/mb-date-picker';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { SharedUiMbSelectCheckAbleModule } from '@hcm-mfe/shared/ui/mb-select-check-able';

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, ReactiveFormsModule, NzGridModule, RouterModule.forChild([
    {
      path: '',
      component: CategoryComponent
    }]),
    SharedUiMbSelectModule, TranslateModule, SharedUiMbInputTextModule, SharedUiMbButtonModule, NzGridModule,
    FormsModule, NzTableModule, NzToolTipModule, NzTagModule, NzPaginationModule, SharedUiMbDatePickerModule, NzSwitchModule, SharedUiMbSelectCheckAbleModule],
  declarations: [CategoryComponent,CategoryInitComponent],
  exports: [CategoryComponent,CategoryInitComponent]
})
export class ModelOrganizationFeatureCategoryModule {}
