import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { TranslateModule } from '@ngx-translate/core';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { RouterModule } from '@angular/router';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { ModelOrganizationFeatureRegionCategoryInitModule } from '@hcm-mfe/model-organization/feature/region-category-init';
import { UnitApprovedComponent } from './unit-approved/unit-approved.component';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { SharedDirectivesPaddingDirectivesModule } from '@hcm-mfe/shared/directives/padding-directives';

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, SharedUiMbButtonModule, TranslateModule, NzCardModule, ReactiveFormsModule, FormsModule,
    NzGridModule, SharedUiMbSelectModule, FormsModule, SharedUiMbInputTextModule, NzTableModule, NzPaginationModule,
    RouterModule.forChild([
      {
        path: '',
        component: UnitApprovedComponent
      }]), NzToolTipModule, NzModalModule, NzTagModule, ModelOrganizationFeatureRegionCategoryInitModule, NzIconModule, SharedDirectivesPaddingDirectivesModule
  ],
  declarations: [UnitApprovedComponent],
  exports: [UnitApprovedComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ModelOrganizationFeatureUnitApprovedModule {}
