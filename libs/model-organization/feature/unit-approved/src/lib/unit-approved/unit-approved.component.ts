import { Component, Injector, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { InitScreenSearch } from '@hcm-mfe/shared/data-access/models';
import { ListSearchOrganization, OrgApprove, ParamSubmit } from '@hcm-mfe/model-organization/data-access/models';
import { FunctionCode, functionUri, types } from '@hcm-mfe/shared/common/enums';
import { UnitApprovedService } from '@hcm-mfe/model-organization/data-access/services';
import { DataService } from '@hcm-mfe/shared/data-access/services';
import { FormModalShowTreeUnitComponent } from '@hcm-mfe/shared/ui/form-modal-show-tree-unit';
@Component({
  selector: 'app-unit-approved',
  templateUrl: './unit-approved.component.html',
  styleUrls: ['./unit-approved.component.scss'],
})
export class UnitApprovedComponent extends BaseComponent implements OnInit {
  listUnitApprove: Array<ListSearchOrganization> = [];
  orgName = '';
  listSelectedOrg = [];
  textSearchDesc = '';
  selectedType:any;
  objectSearch: InitScreenSearch = {
    orgId: '',
    flagStatus: '',
    inputType: '',
    page: 0,
    size: 15,
  };
  paramsSubmit: ParamSubmit = {
    flagStatus: 2,
  };
  setOfCheckedId = new Set<number>();
  mapItem: Map<number, OrgApprove> = new Map();
  checked = false;
  params = {
    id: '',
    page: 0,
    size: 10,
  };
  isLoadingPage = false;
  commentForm: FormGroup;
  total = 0;
  submit =false;
  listType = types;

  constructor(
    injector: Injector,
    private readonly unitApprovedService: UnitApprovedService,
    private readonly dataService: DataService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ORGANIZATION_APPROVE_ORG}`);
    if(!this.objFunction.view){
      this.router.navigateByUrl(functionUri.access_denied);
    }
    this.commentForm = new FormGroup({
      comment: new FormControl('', Validators.required),
    });
  }

  ngOnInit(): void {
    this.submit = false;
    this.orgName = '';
    this.textSearchDesc = '';
    this.isLoadingPage = false;
    this.callList();
  }

  callList() {
    this.isLoadingPage = true;
    this.unitApprovedService.getListorganizationViewApprove(this.paramsSubmit).subscribe(
      (data) => {
        this.total = data.data.totalElements;
        this.listUnitApprove = data.data.content;
        this.buildTree();
        this.resetValidate();
        this.isLoadingPage = false;
      },
      (error) => {
        this.isLoadingPage = false;
        this.toastrCustom.error(error.error.message);
      }
    );
  }

  buildTree() {
    const mapContent = new Map<number, ListSearchOrganization>();
    const listChild: ListSearchOrganization[] = [];
    const newList: ListSearchOrganization[] = [];
    this.listUnitApprove.forEach(item => {
      mapContent.set(item.orgId, item)
    })
    this.listUnitApprove.forEach(item => {
      if(!mapContent.has(item.parentId)){
        item.subs = []
        item.style = 0;
        newList.push(item)
      } else {
        listChild.push(item)
      }
    })

    listChild.sort((a, b) => {
      return a.orgLevel > b.orgLevel ? 1 : -1;
    });

    listChild.forEach((item) => {
      if (mapContent.has(item.parentId)) {
        const parent:any = mapContent.get(item.parentId);
        const indexParent = newList.indexOf(parent);
        item.style = parent.style + 3;
        newList.splice(indexParent + 1, 0, item);
        if (parent.subs) {
          parent.subs.push(item);
        } else {
          parent.subs = [];
          parent.subs.push(item);
        }
      }
    });

    this.listUnitApprove = newList;
  }

  updateCheckedSet(id: number, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }

  onItemChecked(id: number, item:any, checked: boolean): void {
    if (checked) {
      this.mapItem.set(id, {
        id: id,
        type: item.inputType,
      });
    } else {
      this.checked = false;
      this.mapItem.delete(id);
    }

    this.updateCheckedSet(id, checked);
    this.checkSubs(item,checked);
    this.checkParents(item, checked);
  }
  checkParents(item:any,checked:any){
    const parent = this.listUnitApprove.find(p => item.parentId === p.orgId);
    if(parent && !checked){
      this.mapItem.delete(parent?.orgId);
      this.updateCheckedSet(parent?.orgId, false);
      this.checkParents(parent,false);
    }
  }
  checkSubs(item:any,checked:any){
    if(item.subs?.length > 0) {
      item.subs.forEach((unit:any) => {
        if(checked) {
          this.mapItem.set(unit.orgId, {
            id: unit.orgId,
            type: item.inputType,
          });
        } else {
          this.checked = false;
          this.mapItem.delete(unit.orgId);
        }
      this.updateCheckedSet(unit.orgId, checked);
      this.checkSubs(unit,checked);
      })
    }
  }

  onAllChecked(checked: boolean): void {
    if (checked) {
      this.checked = true;
      this.listUnitApprove.forEach((element) => {
        this.updateCheckedSet(element.orgId, checked);
        this.mapItem.set(element.orgId, {
          id: element.orgId,
          type: element.inputType,
        });
      });
    } else {
      this.checked = false;
      this.setOfCheckedId.clear();
      this.mapItem.clear();
    }
  }

  renderType(value: string) {
    if (value === 'KT') {
      return 'Tạo mới';
    } else if (value === 'DC') {
      return 'Điều chỉnh';
    } else {
      return 'Hiệu chỉnh';
    }
  }

  searchOrganizationList() {
    if (this.orgName === '') {
      delete this.paramsSubmit.orgId;
    } else {
      this.paramsSubmit.orgId = this.objectSearch.orgId;
    }
    if (!this.selectedType) {
      delete this.paramsSubmit.inputType;
    } else {
      this.paramsSubmit.inputType = this.selectedType;
    }
    if (this.textSearchDesc === '') {
      delete this.paramsSubmit.reason;
    } else {
      this.paramsSubmit.reason = this.textSearchDesc;
    }
    this.onAllChecked(false);
    this.callList();
  }

  approveItems() {
    this.submit = false;
    if (this.setOfCheckedId.size === 0) {
      this.toastrCustom.error(this.translate.instant('modelOrganization.message.chooseUnit'));
    } else {
      this.isLoadingPage = true;
      const objectSubmit:any = {
        organizationDTOS: [],
        description: this.commentForm.get('comment').value,
      };
      objectSubmit.organizationDTOS = Array.from(this.mapItem.values());
      this.unitApprovedService.approveOrg(objectSubmit).subscribe(
        (data) => {
          this.isLoadingPage = false;
          this.toastrCustom.success(this.translate.instant('modelOrganization.message.approved'));
          this.setOfCheckedId.clear();
          this.mapItem.clear();
          this.callList();
        },
        (error) => {
          this.isLoadingPage = false;
          this.toastrCustom.error(error.error.message);
        }
      );
    }
  }

  approveAllItems() {
    if(!this.listUnitApprove.length){
      this.toastrCustom.success(this.translate.instant('modelOrganization.message.approved'));
      return;
    }
    this.listUnitApprove.forEach(item => {
      this.mapItem.set(item.orgId, {
        id: item.orgId,
        type: item.inputType
      });
      this.setOfCheckedId.add(item.orgId);
    })
    this.approveItems();
  }

  rejectItem() {
    this.commentForm.controls['comment'].setValue(this.commentForm.value.comment.trim());
    if (this.setOfCheckedId.size === 0) {
      this.toastrCustom.error(this.translate.instant('modelOrganization.message.chooseUnitReject'));
      return;
    }

    if (this.commentForm.invalid) {
      this.submit = true;
      return;
    }
    const objectSubmit:any = {
      organizationDTOS: [],
      description: null,
    };
    objectSubmit.description = this.commentForm.get('comment').value;
    objectSubmit.organizationDTOS = Array.from(this.mapItem.values());

    this.isLoadingPage = true;
    this.unitApprovedService.rejectedOrg(objectSubmit).subscribe(
      (data) => {
        this.isLoadingPage = false;
        this.toastrCustom.success(this.translate.instant('modelOrganization.message.rejected'));
        this.resetValidate();
        this.mapItem.clear();
        this.setOfCheckedId.clear();
        this.callList();
        this.submit = false;
      },
      (error) => {
        this.isLoadingPage = false;
        this.toastrCustom.error(error.error.message);
      }
    );
  }

  redrirectToDetail(item:any) {
    localStorage.setItem('itemDetail', JSON.stringify(item));
    this.router.navigateByUrl(functionUri.organization_approve_detail);
  }

  resetValidate() {
    this.commentForm.controls['comment'].setValue('');
  }

  clearOrgInput() {
    this.orgName = '';
    this.paramsSubmit.orgId = '';
  }

  showPopup() {
    const modal = this.modal.create({
      nzWidth: '80%',
      nzTitle: ' ',
      nzContent: FormModalShowTreeUnitComponent,
      nzFooter: null,
    });

    modal.afterClose.subscribe((result) => {
      if (result?.orgName) {
        this.orgName = result?.orgName;
        this.objectSearch.orgId = result?.orgId;
      }
    });
  }

  trackByFn(index:any, item: ListSearchOrganization) {
    return item.orgId;
  }

  override triggerSearchEvent(): void {
    this.searchOrganizationList()
  }

}
