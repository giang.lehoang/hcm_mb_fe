import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UnitApprovedComponent } from './unit-approved.component';

describe('UnitApprovedComponent', () => {
  let component: UnitApprovedComponent;
  let fixture: ComponentFixture<UnitApprovedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UnitApprovedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UnitApprovedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
