import { Component, DoCheck, Injector, OnInit } from '@angular/core';
import * as moment from 'moment';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {DataService, ProjectService} from "@hcm-mfe/model-organization/data-access/services";
import {functionUri} from "@hcm-mfe/shared/common/constants";
import { FunctionCode } from '@hcm-mfe/shared/common/enums';
import {CriteriaRes, ProjectDetailResponse} from "@hcm-mfe/model-organization/data-access/models";

@Component({
  selector: 'app-view-project',
  templateUrl: './view-project.component.html',
  styleUrls: ['./view-project.component.scss'],
})
export class ViewProjectComponent extends BaseComponent implements OnInit, DoCheck {
  detailProject:ProjectDetailResponse | undefined;
  id:string | null;
  listCriteria:CriteriaRes[] = [];
  description: string;
  isValid: boolean;
  file: File | undefined;
  isVisible: boolean;
  pdfSrc: string;
  isLoadingPage: boolean;
  constructor(readonly projectService: ProjectService,
    injector: Injector,
    readonly dataService: DataService,

  ) {
    super(injector);
    this.description = '';
    this.isLoadingPage = false;
    this.pdfSrc = '';
    this.isValid = false;
    this.isVisible = false;
    this.id = localStorage.getItem('selectedId');
  }
  ngOnInit(): void {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.APPROVE_PROJECT_DETAIL}`);
    console.log(this.objFunction)
    if (!this.objFunction || !this.objFunction.view) {
      this.router.navigateByUrl(functionUri.access_denied)
    }
    if (this.id) {
      this.isLoadingPage = true;
      this.projectService.getDetailProject(this.id).subscribe(
        (data) => {
          this.detailProject = data.data;
          this.detailProject.fromDate = this.detailProject.fromDate ?
            moment(new Date(this.detailProject.fromDate)).format('DD/MM/YYYY') : '';
          this.detailProject.toDate =this.detailProject.toDate
            ? moment(new Date(this.detailProject.toDate)).format('DD/MM/YYYY') : '';
          this.isLoadingPage = false;
        },
      );
      this.projectService.getAllListCriteria().subscribe((data: any) => {
        this.listCriteria = data.data;

      });
    }
  }

  override ngOnDestroy(): void {
    localStorage.removeItem('selectedId');
  }

  ngDoCheck(): void {
    this.detailProject && this.detailProject.projectAttributes.forEach(element => {
      this.listCriteria && this.listCriteria.forEach((item, index) => {
        if (item.lvaValue) {
          item.attributes && item.attributes.forEach(itemOne => {
            if (element.attrCode === item.lvaValue) {
              item.selectedValue = element.attrValueMean
            }
          })
        }

      })
    })
  }

  reject(): void {
    if (!this.description) {
      this.isValid = true;
      return;
    } else {
      this.isValid = false;
    }
    const objectSubmit = {
      ids: [this.id],
      description: this.description ? this.description.trim() : "",
      type: 'TC',
    };
    this.projectService.submitListApprove(objectSubmit).subscribe((data) => {
      this.toastrCustom.success('Từ chối thành công');
      this.router.navigateByUrl(`/organization/project/project-list`).then(null);
    }, error => {
      this.toastrCustom.error(error.error.message);
    });
  }

  handleApprovedProject() {
    const objectSubmit = {
      ids: [this.id],
      description: this.description ? this.description.trim() : "",
      type: 'PD',
    };
    this.projectService.submitListApprove(objectSubmit).subscribe(
      (data) => {
        this.toastrCustom.success('Phê duyệt thành công');
        this.router.navigateByUrl(`/organization/project/project-list`);
      },
      (error) => {
        this.toastrCustom.error(error.error.message);
      }
    );

  }

  onChangeInput(event:any) {
    if (!event.target.value) {
      this.isValid = true;
    } else {
      this.isValid = false;
    }
  }
  downloadFile() {
    if(!this.detailProject?.document.docId){
      return;
    }
    this.dataService.downloadFile(this.detailProject?.document.docId).subscribe((data: any) => {
      const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.pdf' });
      this.file = new File([blob], String(this.detailProject?.document.fileName ? this.detailProject.document.fileName : ''));
      this.onFileSelected();
    });
  }

  onFileSelected() {
    const reader = new FileReader();
    reader.onload = (e: any) => {
      this.pdfSrc = e.target.result;
    };
    if(this.file) {
      reader.readAsArrayBuffer(this.file);
    }
    this.isVisible = true;
  }
  handleCancel(): void {
    this.isVisible = false;
  }
}
