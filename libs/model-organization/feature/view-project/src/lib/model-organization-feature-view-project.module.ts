import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {ViewProjectComponent} from "./view-project/view-project.component";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {NzFormModule} from "ng-zorro-antd/form";
import {NzModalModule} from "ng-zorro-antd/modal";
import {PdfViewerModule} from "ng2-pdf-viewer";
import {RouterModule} from "@angular/router";
import {NzDividerModule} from "ng-zorro-antd/divider";
import {FormsModule} from "@angular/forms";
import {NzCardModule} from "ng-zorro-antd/card";
import { TranslateModule } from '@ngx-translate/core';
import { NzIconModule } from 'ng-zorro-antd/icon';

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, SharedUiMbButtonModule, NzDividerModule, NzCardModule,
    SharedUiMbInputTextModule, NzFormModule, NzModalModule, PdfViewerModule, RouterModule.forChild([
      {
        path: '',
        component: ViewProjectComponent
      }]), FormsModule, TranslateModule, NzIconModule
  ],
  declarations: [ViewProjectComponent],
  exports: [ViewProjectComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ModelOrganizationFeatureViewProjectModule {}


