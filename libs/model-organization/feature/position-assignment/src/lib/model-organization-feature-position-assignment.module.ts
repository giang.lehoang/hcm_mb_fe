import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PositionAssignmentComponent} from "./position-assignment/position-assignment.component";
import {TranslateModule} from "@ngx-translate/core";
import {NzTabsModule} from "ng-zorro-antd/tabs";
import {NzTableModule} from "ng-zorro-antd/table";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {FormsModule} from "@angular/forms";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {NzEmptyModule} from "ng-zorro-antd/empty";
import {NzSwitchModule} from "ng-zorro-antd/switch";
import {ModelOrganizationUiJobSelectModule} from "@hcm-mfe/model-organization/ui/job-select";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzGridModule} from "ng-zorro-antd/grid";
import {RouterModule} from "@angular/router";
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";


@NgModule({
  imports: [CommonModule, TranslateModule, NzTabsModule, NzTableModule, SharedUiMbInputTextModule, FormsModule, NzGridModule,
    NzDropDownModule, NzEmptyModule, NzSwitchModule, ModelOrganizationUiJobSelectModule, SharedUiLoadingModule,
    SharedUiMbButtonModule ,RouterModule.forChild([
      {
        path: '',
        component: PositionAssignmentComponent,
      }]), NzCheckboxModule],
  declarations: [PositionAssignmentComponent],
  exports: [PositionAssignmentComponent]
})
export class ModelOrganizationFeaturePositionAssignmentModule {}
