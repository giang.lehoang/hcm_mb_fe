import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PositionAssignmentComponent } from './position-assignment.component';

describe('PositionAssignmentComponent', () => {
  let component: PositionAssignmentComponent;
  let fixture: ComponentFixture<PositionAssignmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PositionAssignmentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PositionAssignmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
