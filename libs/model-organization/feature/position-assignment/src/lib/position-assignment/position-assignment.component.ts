import { Component, ElementRef, Injector, OnInit, ViewChild } from '@angular/core';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {
  AssignPosRes,
  JobLine,
  OrgLine,
  PositionDetail,
  PositionTable
} from "@hcm-mfe/model-organization/data-access/models";
import {FunctionCode, functionUri} from '@hcm-mfe/shared/common/enums';
import {
  DataService,
  JobService,
  LineService,
  OrganizationService
} from "@hcm-mfe/model-organization/data-access/services";

@Component({
  selector: 'app-position-assignment',
  templateUrl: './position-assignment.component.html',
  styleUrls: ['./position-assignment.component.scss'],
})
export class PositionAssignmentComponent extends BaseComponent implements OnInit {
  orgList: OrgLine[] = [];
  orgListFilter: OrgLine[] = [];
  listJobCn: JobLine[] = [];
  listJobCnSelect: JobLine[] = [];
  dataTableGroup: PositionTable[] = [];
  selectTab: number;
  isSubmitted = false;
  mapOrgSelect: Map<number | null, PositionTable | OrgLine> = new Map();
  mapOrgSelectDetail: Map<number | null, PositionTable> = new Map();
  isVisible: boolean;
  dataSearch: string;
  positionDetail: PositionDetail | undefined;
  id: string;
  type:string;
  name = '';
  selectAll: boolean;
  stackCall: number[] = [];
  scollStack: PositionTable[] = [];
  loadItemNumber = 15
  generateState:boolean;
  submited:boolean;
  isOpenPopup = false;
  deleteIds:Array<number|null> = [];
  @ViewChild('scoll') scoll:ElementRef | undefined;

  constructor(
    injector: Injector,
    readonly lineService: LineService,
    readonly dataService: DataService,
    readonly orgService:OrganizationService,
    readonly jobService: JobService
  ) {
    super(injector);
    const pos = localStorage.getItem('posId');
    const item = JSON.parse(pos ? pos : '');
    this.objFunction = this.sessionService.getSessionData(
      `FUNCTION_${FunctionCode.ORGANIZATION_POSITION_ASSIGN}`
    );
    this.id = item ? item.id : '';
    this.type = item ? item.type : '';
    this.isLoading = false;
    this.isVisible = false;
    this.dataSearch = '';
    this.submited = false;
    if(!this.id){
      this.router.navigateByUrl(functionUri.organization_position_category);
    }
    this.selectAll = false;
    this.generateState = false;
    this.selectTab = 0;
  }

  ngOnInit(): void {
    this.onSearch();
  }

  fetchJobCN() {
    this.lineService.findAllByOrgGroupCNAssign({orgId: -1, name:this.name, type:this.type}).subscribe((data) => {
      const setPos = new Set<number>();
      if(this.type !== "LINE"){
        data.data.forEach(item => {
          if(item.assign){
            setPos.add(item.jobId);
          }
        })
      }
      const listJobUN =  data.data.filter(item => {
        if(!item.assign || this.type === "LINE"){
          if(setPos.has(item.jobId)){
            return false;
          } else {
            setPos.add(item.jobId);
            return true;
          }
        }
        return false;
      });
      this.listJobCn.push(...listJobUN);
    });
  }

  changeTab(e:number) {
    this.selectTab = e;
  }

  getListJob(org:PositionTable) {
    if (!org.orgId) {
      return;
    }
    this.stackCall.push(1)
    this.lineService.getListJobByIdUnassign({orgId: org.orgId, name:this.name, type:this.type}).subscribe(
      (data) => {
        if(this.type !== "LINE"){
          org.jobs = data.data.filter(item => !item.assign);
          if(org.jobSelect.length){
            org.jobs.push(...org.jobSelect);
          }
        } else {
          org.jobs = data.data;
        }

        org.loadJob = true;
        this.stackCall.pop();
        if(this.stackCall.length === 0){
          this.isLoading = false;
        }
      },
      (error) => {
        this.stackCall = []
        this.isLoading = false;
        this.toastrCustom.error(error.message);
      }
    );
    return org;
  }

  onSearch(): void {
    this.isLoading = true;

    if (this.id) {
      this.jobService.detailPositionAssign(this.id).subscribe((data) => {
        const mapPos = new Map<number | null, JobLine[]>();
        const mapJob = new Map<number, JobLine>();
        const mapJobCN = new Map<number, JobLine>();
        this.positionDetail = data.data;
        this.name = data.data.name;
        this.fetchJobCN();
        this.positionDetail.lstJobDTOHOs?.forEach((job) => {
          mapJob.set(job.jobId, <JobLine>job);
        });
        this.positionDetail.lstPositionHO?.forEach((item) => {
          const job = mapJob.get(item.jobId);
          if(job?.jobId){
            job.id = item.id;
            if (mapPos.has(item.orgId)) {
              mapPos.get(item.orgId)?.push(job);
            } else {
              mapPos.set(item.orgId, [job]);
            }
          }
        });

        this.positionDetail.lstOrgDTOHOs?.forEach((org) => {
          const pos = mapPos.get(org.orgId);
          if(pos) org.jobSelect = pos;
          org.pathName = org.fullName;
          this.mapOrgSelect.set(org.orgId, org);
          this.mapOrgSelectDetail.set(org.orgId, org);
        });

        this.positionDetail.lstJobDTOCNs?.forEach(jobCN => {
          jobCN.posIds = [];
          mapJobCN.set(jobCN.jobId, <JobLine>jobCN);
        })

        this.positionDetail.lstPositionCN?.forEach(pos => {
          mapJobCN.get(pos.jobId)?.posIds?.push(pos.id);
        })

        this.listJobCnSelect = Array.from(mapJobCN.values());
        if(this.type !== "LINE"){
          this.listJobCn.push(...this.listJobCnSelect);
        }

        this.selectOrg();
        if (!this.dataTableGroup.length) {
          this.addOrg();
        }
      });
    }
    this.orgService.getListOrg().subscribe(
      (data) => {
        this.orgList = data.data.filter((org) => org.orgName !== 'MB-HO');
        this.orgListFilter = this.orgList;
        this.isLoading = false;
      },
      (_) => {
        this.isLoading = false;
      }
    );
  }

  changeValue(value:string) {
    this.orgListFilter = this.orgList.filter(
      (item) => {
        if(item.pathName) return item.pathName.toLowerCase().indexOf(value.toLowerCase().trim()) > -1
        return false;
      }
    );
  }

  onSelect(item:OrgLine) {
    if (this.mapOrgSelect.has(item.orgId ? item.orgId : -1)) {
      this.mapOrgSelect.delete(item.orgId ? item.orgId : -1);
    } else {
      this.mapOrgSelect.set(item.orgId ? item.orgId : -1, item);
    }
  }

  addOrg() {
    const item:PositionTable  = {
      orgId: null,
      orgName: '',
      jobs: [],
      jobSelect: [],
      loadJob: false,
      isVisible: false,
    }

    if(this.dataTableGroup.length === 0){
      this.dataTableGroup = [item]
    } else {
      this.dataTableGroup.push(item);
    }
  }

  deleteOrg(index: number) {
    debugger;
    const id = this.dataTableGroup[index].orgId;
    this.dataTableGroup[index]?.jobSelect.forEach(item => {
      this.deleteIds.push(item.id);
    })
    this.mapOrgSelect.delete(id);
    if(this.mapOrgSelectDetail.has(id)){
      this.mapOrgSelectDetail.delete(id);
    }
    this.dataTableGroup.splice(index, 1);
  }

  selectOrg(){
    this.submited = false;
    this.scollStack = [];
    this.dataTableGroup = [];
    if(this.mapOrgSelect.size === 0){
      this.addOrg()
    }
    this.mapOrgSelect.forEach((value) => {
      this.scollStack.push(<PositionTable>value);
    })
    this.genOrg();
  }

  genOrg() {
    if(this.scollStack.length === 0){
      return;
    }
    this.isLoading = true;
    if(this.dataTableGroup.length === 1 && !this.dataTableGroup[0].orgName){
      this.dataTableGroup = [];
    }

    let runNum = this.loadItemNumber;

    while(runNum > 0 && this.scollStack.length > 0){
      const value = this.scollStack.shift();
      if(value){
        if (!value?.loadJob) {
          this.getListJob(value);
        }
        value.jobSelect = value?.jobSelect ? value.jobSelect : [];
        value.isVisible = false;
        this.dataTableGroup.push(value);
        runNum -= 1;
      }
    }
    if(this.stackCall.length === 0){
      this.isLoading = false;
    }
  }

  change(value:boolean) {
    if (value) {
      this.orgListFilter.forEach((org) => {
        if(!this.mapOrgSelect.has(org.orgId)){
          if(this.mapOrgSelectDetail.has(org.orgId)){
            const orgSelect = this.mapOrgSelectDetail.get(org.orgId);
            if(orgSelect) this.mapOrgSelect.set(org.orgId, orgSelect);
          } else {
            this.mapOrgSelect.set(org.orgId, org);
          }
        }
      });
    } else {
      this.mapOrgSelect.clear();
    }
  }

  changePopup(value:boolean){
    if(value){
      this.mapOrgSelect.clear();
      this.dataTableGroup.forEach(org => {
        this.mapOrgSelect.set(org.orgId, org)
      })
    }
  }

  assignPosition() {
    this.submited = true;
    const req:AssignPosRes = {
      positionGroupId: this.positionDetail?.pgrId ? this.positionDetail.pgrId : null,
      positionIds: [],
      deletePositionIds:this.deleteIds
    };

    for(const org of this.dataTableGroup){
      if(org.jobSelect?.length === 0){
        if(this.mapOrgSelect.size > 0){
          return;
        }
      }
      const jobSelect = org.jobSelect.map((item) => item.id);
      req.positionIds.push(...jobSelect);
    }

    this.listJobCnSelect.forEach((job) => {
      req.positionIds.push(...job.posIds);
    });

    if(req.positionIds.length === 0){
      this.toastrCustom.error(this.translate.instant('modelOrganization.message.job'));
      return;
    }
    this.isLoading = true;
    this.jobService.assignPosition(req).subscribe(
      (data) => {
        this.isLoading = false;
        this.toastrCustom.success();
        this.router.navigateByUrl(functionUri.organization_position_category);
      },
      (error) => {
        this.isLoading = false;
        this.toastrCustom.error(error.message);
      }
    );
  }

  trackByFn(index:number, item:OrgLine|PositionTable) {
    return item.orgId;
  }

  onScroll(event:Event){
    const el = this.scoll?.nativeElement;
    if(el.scrollHeight === el.clientHeight + el.scrollTop){
      if(this.scollStack.length > 0){
        this.genOrg();
      }
    }
  }
  onClickOrg(isOpen:boolean) {
    this.isOpenPopup = isOpen;
  }
}
