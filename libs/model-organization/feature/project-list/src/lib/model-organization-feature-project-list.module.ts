import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {ProjectListComponent} from "./project-list/project-list.component";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NzGridModule} from "ng-zorro-antd/grid";
import {NzFormModule} from "ng-zorro-antd/form";
import {NzDatePickerModule} from "ng-zorro-antd/date-picker";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {NzModalModule} from "ng-zorro-antd/modal";
import {PdfViewerModule} from "ng2-pdf-viewer";
import {NzTableModule} from "ng-zorro-antd/table";
import {SharedPipesConvertArrayStringModule} from "@hcm-mfe/shared/pipes/convert-array-string";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import {RouterModule} from "@angular/router";
import {NzCardModule} from "ng-zorro-antd/card";
import {NzButtonModule} from "ng-zorro-antd/button";
import {TranslateModule} from "@ngx-translate/core";
import {NzTagModule} from "ng-zorro-antd/tag";

@NgModule({
    imports: [CommonModule, SharedUiLoadingModule, SharedUiMbButtonModule, ReactiveFormsModule, NzGridModule, NzFormModule, NzButtonModule,
        NzDatePickerModule, SharedUiMbInputTextModule, TranslateModule, SharedUiMbSelectModule, NzModalModule,
        PdfViewerModule, FormsModule, NzTableModule, SharedPipesConvertArrayStringModule, NzPaginationModule, RouterModule.forChild([
            {
                path: '',
                component: ProjectListComponent,
            }]), NzCardModule, NzTagModule],
  declarations: [ProjectListComponent],
  exports: [ProjectListComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ModelOrganizationFeatureProjectListModule {}
