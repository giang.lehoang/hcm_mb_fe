import { Component, Injector, OnInit } from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd/modal';
import * as FileSaver from 'file-saver';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {DataService, ProjectService} from "@hcm-mfe/model-organization/data-access/services";
import {functionUri} from "@hcm-mfe/shared/common/constants";
import { FunctionCode } from '@hcm-mfe/shared/common/enums';
import {LookupValue, PRJResponse, SearchLinePRJ, SearchPRJ} from "@hcm-mfe/model-organization/data-access/models";
import {renderFlagStatus} from "../../../../../helper/common.function";

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.scss'],
})
export class ProjectListComponent extends BaseComponent implements OnInit {
  confirmModal?: NzModalRef;
  isExpand = true;
  searchLine:SearchLinePRJ[] = [];
  listCriteria:LookupValue[] = [];
  listAttribute = [];
  liststatusProject = [
    { lable: 'Phê duyệt', value: '1' },
    { lable: 'Chờ duyệt', value: '2' },
    { lable: 'Từ chối', value: '3' },
    { lable: 'Tạo mới', value: '5' },
  ];
  statusProject = {
    '1': 'Phê duyệt',
    '2': 'Chờ duyệt',
    '3': 'Từ chối',
    '5': 'Tạo mới'
  };
  renderFlagStatus = renderFlagStatus;

  objectSearch:SearchPRJ = {
    prjName: null,
    orgName: null,
    flagStatus: null,
    page: 0,
    size: 20,
    attrs: [],
  };
  listSettingPaginationCustom = {
    size: 0,
    total: 0,
    pageIndex: 0,
    numberOfElements: 0
  };
  dataTable:PRJResponse[] = [];

  constructor(
    readonly projectService: ProjectService,
    readonly dataService:DataService,
    injector: Injector
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.MANAGE_PROJECT}`);
    if (!this.objFunction || !this.objFunction.view) {
      this.router.navigateByUrl(functionUri.access_denied)
    }
    this.getListProject();
    this.projectService.getCriteriaProject().subscribe((data) => {
      if(data.data) {
        this.listCriteria = data.data;
      }
    });
  }

  createProject(): void {
    this.router.navigateByUrl('/organization/project/init-project');
  }

  editProject(id:number): void {
    localStorage.setItem('selectedId',String(id));
    this.router.navigateByUrl('/organization/project/init-project');
  }

  viewProject(id:number): void {
    localStorage.setItem('selectedId',String(id));
    this.router.navigateByUrl('/organization/project/view-project');
  }

  deleteProject(id:number): void {
    this.deletePopup.showModal(() => this.showConfirm(id));
  }

  changeProperty(id:string, index:number) {
    this.listAttribute = [];
    this.dataService.getLookupValue(id).subscribe((data: any) => {
      this.listAttribute = data.data;
      this.searchLine[index].listPrjAttributeValues = this.listAttribute;
    });
  }
  showConfirm(id:number): void {
    this.projectService.deleteProject(id).subscribe(
      (data) => {
        this.toastrCustom.success('Xóa dự án thành công');
        this.getListProject();
      },
      (error) => {
        this.toastrCustom.error('Xóa dự án thất bại');
      }
    );
  }
  getListProject(): void {
    this.projectService.getProjectList(this.objectSearch).subscribe((data) => {
      this.dataTable = data.data.content;
      this.listSettingPaginationCustom.size = data.data.size;
      this.listSettingPaginationCustom.total = data.data.totalElements;
      this.listSettingPaginationCustom.pageIndex = data.data.pageable.pageNumber;
      this.listSettingPaginationCustom.numberOfElements = data.data.numberOfElements;
      this.isLoading = false;
    }, () => {
      this.isLoading = false;
    });
  }
  exportExcel(): void {
    this.objectSearch.size = this.listSettingPaginationCustom.total;
    this.isLoading = true;
    this.projectService.exportProjectList(this.objectSearch).subscribe((data: any) => {
      this.isLoading = false;
      const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      FileSaver.saveAs(blob, 'Danh sach du an.xls');
    }, () => {
      this.isLoading = false;
    });
  }
  searchProject(): void {
    const map = new Map<string, any>();
    this.searchLine.forEach(el => {
      if (el.prjAttributeCode && map.has(el.prjAttributeCode)) {
        const item = map.get(el.prjAttributeCode)
        if (item) {
          if (item.length > 0 && el.prjAttributeValues) {
            item.push(...el.prjAttributeValues)
          }
        } else {
          map.set(el.prjAttributeCode, el.prjAttributeValues ? [el.prjAttributeValues] : null)
        }
      } else if (el.prjAttributeCode) {
        map.set(el.prjAttributeCode, el.prjAttributeValues ? [el.prjAttributeValues] : null)
      }
    })
    this.objectSearch.page = 0;
    const attributes = Array.from(map, ([prjAttributeCode, prjAttributeValues]) => ({ prjAttributeCode, prjAttributeValues }));
    this.objectSearch.attrs = attributes;
    this.getListProject();
  }
  expandSearch(): void {
    this.isExpand = !this.isExpand;
    if (!this.isExpand) {
      this.searchLine = [
        {
          prjAttributeCode: null,
          listPrjAttributeValues: [],
        },
      ];
    } else {
      this.searchLine = [];
    }
  }
  getListProjectBasePagination(value:number): void {
    this.objectSearch.page = value - 1;
    this.projectService.getProjectList(this.objectSearch).subscribe((data) => {
      if (data.data.content.length > 0) {
        this.dataTable = data.data.content;
        this.listSettingPaginationCustom.size = data.data.size;
        this.listSettingPaginationCustom.total = data.data.totalElements;
        this.listSettingPaginationCustom.pageIndex = data.data.pageable.pageNumber;
        this.listSettingPaginationCustom.numberOfElements = data.data.numberOfElements;
      } else {
        this.dataTable = [];
      }
    });
  }
  addSearchLine(index:number): void {
    this.searchLine.splice(index + 1, 0, {
      prjAttributeCode: null,
      listPrjAttributeValues: [],
    });
    this.searchLine = [...this.searchLine];
  }
  removeSearchLine(index:number): void {
    const listCheck = [...this.searchLine];
    if (listCheck.length === 1) {
      return;
    }
    this.searchLine.splice(index, 1);
    this.searchLine = [...this.searchLine];
  }
  goBackPage(): void {
    this.location.back();
  }

  override triggerSearchEvent(): void {
    this.searchProject();
  }
}
