
import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import {DataService} from "@hcm-mfe/model-organization/data-access/services";
import {
  Area,
  FileData,
  Job,
  Organization,
  OrganizationObject,
  ShareData,
  Type
} from "@hcm-mfe/model-organization/data-access/models";

@Component({
  selector: 'app-review-step',
  templateUrl: './review-step.component.html',
  styleUrls: ['./review-step.component.scss'],
})
export class ReviewStepComponent implements OnInit {
  @Input() createOrganization: FormGroup;
  @Input() listData: Organization[] = [];
  @Input() jobs: Job[] = [];
  @Input() shareData: ShareData;
  fileName: string;
  @Input() areas: Array<Area>;
  @Input() types: Array<Type>;
  @Input() orgVal: OrganizationObject;
  @Input() file: FileData;
  showComment: boolean;
  isVisible: boolean;
  loadingFile:boolean;
  pdfSrc: string;
  constructor(private readonly unitApproveService: DataService) {
    this.showComment = false;
  }
  ngOnInit(): void {
    this.isVisible = false;
    this.loadingFile = true;
  }


  showModal(): void {
    if(!this.file.status && this.loadingFile){
      const id =this.shareData.data?.sheetApproval?.mpDoc.docId;
      if(id){
        this.downloadFile(id);
      }
    } else {
      this.onFileSelected();
    }
  }

  handleCancel(): void {
    this.isVisible = false;
  }

  onFileSelected() {
    const reader = new FileReader();

    reader.onload = (e: any) => {
      this.pdfSrc = e.target.result;
    };

    if(this.file.file){
      reader.readAsArrayBuffer(this.file.file);
    }
    this.loadingFile = false;
    this.isVisible = true
  }

  downloadFile(id:number) {
    this.unitApproveService.downloadFile(String(id)).subscribe((data) => {
      const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.pdf' });

      this.file.file = new File([blob], this.file.fileName ? this.file.fileName : '');
      this.file.status = false;
      this.onFileSelected();
    });
  }
}
