
import {Component, OnInit, Input, Injector} from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import {
  Area, AttributesCorrect, FileData,
  Job,
  Organization,
  RegionType,
  ShareData,
  Type,
  ValidateObject
} from '@hcm-mfe/model-organization/data-access/models';
import {DataService} from "@hcm-mfe/model-organization/data-access/services";
import {REGEX} from "@hcm-mfe/shared/common/constants";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";


@Component({
  selector: 'app-correction-step',
  templateUrl: './correction-step.component.html',
  styleUrls: ['./correction-step.component.scss'],
})
export class CorrectionStepComponent extends BaseComponent implements OnInit {
  @Input() createOrganization: FormGroup;
  @Input() listData: Organization[] = [];
  @Input() shareData: ShareData;
  @Input() jobs: Job[] = [];
  @Input() areas: Array<Area>;
  @Input() types: Array<Type>;
  @Input() legalBranchs: Array<Type>;
  @Input() validateObject: ValidateObject;
  @Input() isSubmit: boolean;
  @Input('file') fileData: FileData;
  legalBranch: string;
  @Input() modified: boolean;

  constructor(
    injector:Injector,
    readonly dataService: DataService,
    ) {
    super(injector);
  }

  ngOnInit(): void {
    this.legalBranch = RegionType.LB;
  }


  removeChip() {
    this.fileData.fileName = '';
    this.fileData.status = true;
    this.createOrganization.controls['attach'].setErrors({
      require: true
    })
  }

  selectBranch(event:string) {
    if (event === RegionType.LB) {
      this.createOrganization.controls['legalBranch'].setValidators(Validators.required);
    } else {
      this.createOrganization.controls['legalBranch'].setValue(null)
      this.createOrganization.controls['legalBranch'].clearValidators();
    }
  }
  findDuplicate(formArray:AttributesCorrect[]) {
    const arr = [];
    const myArray = formArray;
    for (const item of myArray) {
      if (item.control?.value) {
        arr.push(item.control.value.trim());
      }
    }
    for (const item of myArray) {
      const arrTemp = arr.filter((m) => m.trim() === item.control?.value.trim());
      if (arrTemp.length > 1) {
        item.control?.setErrors({ 'duplicate': true });
      } else {
        item.control?.setErrors(null);
      }
    }
  }
  handleFileInput(event: Event) {
    const target = event.target as HTMLInputElement;
    if (!target.files?.item(0)) {
      return;
    }
    this.fileData.file = target.files[0];
    this.fileData.blob = null;
    this.fileData.fileName = target.files[0].name;
    this.fileData.status = true;
    const fileExtend = this.fileData.fileName.split('.').pop();
    this.createOrganization.controls['fileName'].setValue(this.fileData.fileName);
    if (fileExtend !== 'pdf') {
      this.createOrganization.controls['attach'].setErrors({ extend: true });
    }
    if (target.files[0].size > 10000000) {
      this.createOrganization.controls['attach'].setErrors({ size: true });
    }
  }


  addFun(arr:FormControl) {
    if(this.shareData.data?.orgAttributes.find(item => !item.control['value'])){
      this.toastrCustom.error(this.translate.instant("modelOrganization.validate.orgName"));
      return;
    }
    const control = new FormControl('', [ Validators.pattern(REGEX.SPECIAL_TEXT_CODE)]);
    (this.createOrganization.controls['attributes'] as FormArray).push(control);
    this.shareData.data?.orgAttributes.push({
      orgId: null,
      orgaValue: '',
      control: control,
      orgaId: null,
      positions: [],
    });
  }

  deleleFun(index: number, arr:AttributesCorrect[]) {
    if (this.shareData.data?.orgAttributes.length === 1) {
      return;
    }
    const control = this.shareData.data?.orgAttributes[index].control;
    const indexControl = this.getIndexControl(control);
    (this.createOrganization.controls['attributes'] as FormArray).removeAt(indexControl);
    this.shareData.data?.orgAttributes.splice(index, 1);
    setTimeout(() => {
      this.findDuplicate(arr);
    }, 0);
  }

  getIndexControl(controlEl: FormControl | undefined): number {
    if(!controlEl){
      return -1;
    }
    const controls = (this.createOrganization.controls['attributes'] as FormArray).controls;
    return controls.findIndex((control) => {
      return control === controlEl;
    });
  }

  updateJob(value:boolean){
    if(value){
      while(this.jobs.length){
        this.jobs.pop()
      }
      this.callGetJob()
    }
  }

  callGetJob(){
    this.dataService.getJobsByType().subscribe((data) => {
      const listJob = data.data.content;
      listJob.forEach((element) => {
        if (!element.jobName) {
          element.jobName = '';
        }
        this.jobs.push({
          id: element.jobId,
          value: element.jobName,
          jobId: element.jobId,
          jobName: element.jobName
        });
      });
    });
  }

}
