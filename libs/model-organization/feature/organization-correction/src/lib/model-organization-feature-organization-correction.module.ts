import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {CorrectionOrganizationComponent} from "./correction-organization/correction-organization.component";
import {CorrectionStepComponent} from "./correction-step/correction-step.component";
import {ReviewStepComponent} from "./review-step/review-step.component";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {MatStepperModule} from "@angular/material/stepper";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {NzGridModule} from "ng-zorro-antd/grid";
import {NzFormModule} from "ng-zorro-antd/form";
import {ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {MatChipsModule} from "@angular/material/chips";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
import {NzTableModule} from "ng-zorro-antd/table";
import {ModelOrganizationPipesWidthPipeModule} from "@hcm-mfe/model-organization/pipes/width-pipe";
import {ModelOrganizationUiJobSelectModule} from "@hcm-mfe/model-organization/ui/job-select";
import {NzModalModule} from "ng-zorro-antd/modal";
import {PdfViewerModule} from "ng2-pdf-viewer";
import {RouterModule} from "@angular/router";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzStepsModule} from "ng-zorro-antd/steps";

@NgModule({
    imports: [CommonModule, SharedUiLoadingModule, MatStepperModule, SharedUiMbInputTextModule, TranslateModule, SharedUiMbButtonModule,
        RouterModule.forChild([
            {
                path: '',
                component: CorrectionOrganizationComponent,
            }]), NzIconModule,
        SharedUiMbSelectModule, NzGridModule, NzFormModule, ReactiveFormsModule, SharedUiMbDatePickerModule, MatChipsModule,
        NzToolTipModule, NzTableModule, ModelOrganizationPipesWidthPipeModule, ModelOrganizationUiJobSelectModule, NzModalModule, PdfViewerModule, NzStepsModule],
  declarations: [CorrectionOrganizationComponent, CorrectionStepComponent, ReviewStepComponent],
  exports: [CorrectionOrganizationComponent, CorrectionStepComponent, ReviewStepComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ModelOrganizationFeatureOrganizationCorrectionModule {}
