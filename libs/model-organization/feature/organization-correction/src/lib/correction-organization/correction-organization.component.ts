import { forkJoin } from 'rxjs';
import { Component, OnInit, Injector, OnDestroy } from '@angular/core';
import {FormGroup, FormControl, Validators, FormArray, FormBuilder, AbstractControl} from '@angular/forms';
import * as moment from 'moment';
import { MatStepper } from '@angular/material/stepper';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {DataService, OrganizationService} from "@hcm-mfe/model-organization/data-access/services";
import {
  Area,
  Job,
  ShareData,
  Organization,
  RegionType,
  Type,
  FileData,
  ValidateObject,
  OrganizationObject,
  ScreenType,
  ResponseEntity,
  LookupValue,
  ResponseEntityData,
  OrganizationDetailCorrection,
  PositionDetailOrg,
  AttributesCorrect, FunctionOrg, AdjustOrgRequest
} from '@hcm-mfe/model-organization/data-access/models';
import { functionUri, REGEX, FunctionCode, maxInt32 } from '@hcm-mfe/shared/common/enums';

@Component({
  selector: 'app-correction-organization',
  templateUrl: './correction-organization.component.html',
  styleUrls: ['./correction-organization.component.scss'],
})
export class CorrectionOrganizationComponent extends BaseComponent implements OnInit, OnDestroy {
  isSubmit: boolean | undefined;
  createOrganization: FormGroup;
  viewDetail: boolean;
  listData: Organization[] = [];
  shareData: ShareData = {};
  loading: boolean | undefined;
  mapJobs: Map<number, Job>;
  jobs: Job[] = [];
  fileName: string;
  areas: Array<Area> = [];
  types: Array<Type> = [];
  legalBranchs: Array<Type> = [];
  changeMode: boolean | undefined;
  dataDetail: OrganizationDetailCorrection;
  type: string;
  sapId: number | undefined;
  disable: boolean | undefined;
  modified: boolean;
  orgVal: OrganizationObject | undefined;
  validateObject: ValidateObject | undefined;
  file: FileData | undefined;
  stepper:number;


  constructor(
    injector: Injector,
    private readonly orgService: OrganizationService,
    private readonly dataService: DataService,
    private readonly _formBuilder: FormBuilder,
  ) {
    super(injector);
    this.stepper = 0;
    this.fileName = '';
    this.viewDetail = JSON.parse(localStorage.getItem('viewDetail'));
    this.type = this.route.snapshot.data['type'];
    this.validateObject = {
      attach: false,
    };
    this.file = {
      status: false
    };
    this.shareData.loading = true;
    this.loading = false;
    this.mapJobs = new Map<number, Job>();
    this.modified = true;
    this.shareData = {
      type: 0,
      width: '30%',
    };
    this.initForm();
  }


  ngOnInit(): void {
    const id = JSON.parse(localStorage.getItem('org_selected')).orgId;
    this.shareData.id = id;
    if (this.type === ScreenType.CORRECTION) {
      this.shareData.type = 1;
      this.shareData.width = '25%';
    }
    if (!id || !this.type || (this.type !== ScreenType.ADJUSTED && this.type !== ScreenType.CORRECTION)) {
      this.router.navigateByUrl('/');
    }

    if (this.type === ScreenType.ADJUSTED) {
      this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ORGANIZATION_ADJUSTED_ORG}`);
    } else {
      this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ORGANIZATION_CORRECTION_ORG}`);
    }
    if (!this.objFunction || !this.objFunction.view) {
      this.router.navigateByUrl(functionUri.access_denied);
    }


    if (!this.shareData.type) {
      this.createOrganization.controls['sapNo'].addValidators([
        Validators.required,
      ]);
    }

    this.getDetail(id);
  }

  initForm(): void {
    this.createOrganization = this._formBuilder.group({
      description: ['', [Validators.required]],
      dateVisible: ['', Validators.required],
      orgName: ['', [Validators.required, Validators.pattern(REGEX.SPECIAL_TEXT_CODE)]],
      sapNo: [''],
      fileName: [null, Validators.required],
      type: [''],
      legalBranch: [''],
      area: [''],
      attach: [''],
      parent: [''],
      parentId: [''],
      expirationDate: [''],
      correctionDate: [new Date()],
      toDate: [''],
      legalBranchName: [''],
      regionView: [''],
      branchView: [''],
      attributes: new FormArray([]),
    });
  }
  getDetail(id:number): void {
    this.shareData.loading = true;
    forkJoin([
      this.dataService.getLookupValue(RegionType.KV, 1),
      this.dataService.getLookupValue(RegionType.LH, 1),
      this.orgService.getListLegalBranch(),
      this.dataService.getJobsByType('',0,maxInt32,['ORG','DV']),
      this.orgService.getOrgDetail(Number(id), this.type),
    ]).subscribe(
      ([areas, types, legalBranch, jobs, orgDetail]: [
        ResponseEntity<LookupValue[]>,
        ResponseEntity<LookupValue[]>,
        ResponseEntity<LookupValue[]>,
        ResponseEntityData<Job[]>,
        ResponseEntity<OrganizationDetailCorrection>
      ]) => {
        this.areas = areas.data;
        this.types = types.data;
        this.legalBranchs = legalBranch.data;
        this.dataDetail = orgDetail.data;
        const listJob = jobs.data.content;
        listJob.forEach((element) => {
          this.mapJobs.set(element.jobId, element);
          this.jobs.push({
            id: element.jobId,
            value: element.jobName,
            jobId:  element.jobId,
            jobName: element.jobName
          });
        });

        this.sapId = this.dataDetail.sheetApproval?.sapId;
        this.createOrganization.controls['sapNo'].setValue(this.dataDetail.sheetApproval?.sapNo || '');
        this.createOrganization.controls['type'].setValue(this.dataDetail.branchType);
        this.createOrganization.controls['legalBranch'].setValue(
          this.dataDetail.legalBranchId ? this.dataDetail.legalBranchId : ''
        );
        this.createOrganization.controls['area'].setValue(this.dataDetail.region);
        this.createOrganization.controls['parent'].setValue(this.dataDetail.parentName);
        this.createOrganization.controls['parentId'].setValue(this.dataDetail.parentId);
        this.createOrganization.controls['dateVisible'].setValue(this.dataDetail.fromDate);
        this.createOrganization.controls['fileName'].setValue(this.dataDetail.sheetApproval?.mpDoc?.fileName);
        if(this.file){
          this.file.fileName = this.dataDetail.sheetApproval?.mpDoc?.fileName;
        }
        this.createOrganization.controls['orgName'].setValue(this.dataDetail.orgName);
        this.createOrganization.controls['toDate'].setValue(this.dataDetail.toDate);
        this.createOrganization.controls['description'].setValue(this.dataDetail.sheetApproval?.reason);
        this.shareData.disable = this.dataDetail.orgLevel !== 2;
        this.shareData.data = this.dataDetail;
        this.shareData.sapNo = this.dataDetail.sheetApproval?.sapNo.toUpperCase();
        this.shareData.modify = false;
        this.shareData.loading = false;
        this.buildTable();
        this.getNameByValue();
      },
      (error) => {
        this.shareData.data = {
          orgAttributes: [],
        };
        this.shareData.loading = false;
        this.toastrCustom.error(error.message);
      }
    );
  }
  buildTable() {
    const mapJob = new Map<number, PositionDetailOrg>();
    const mapAt = new Map<number | null, AttributesCorrect>();
    this.dataDetail.positions?.forEach((item) => {
      item.value = this.mapJobs.get(item.jobId)?.jobName;
      item.posId = item.id;
      item.id = item.jobId;
      mapJob.set(item.posId, item);
    });

    this.dataDetail.orgAttributes.forEach((item) => {
      mapAt.set(item.orgaId, item);
      item.positions = [];
      const control = new FormControl(item.orgaValue, [Validators.pattern(REGEX.SPECIAL_TEXT_CODE)]);
      item.control = control;
      (this.createOrganization.controls['attributes'] as FormArray).push(control);
    });
    this.dataDetail.positionDetailDTOS?.forEach((item) => {
      if (item.orgaId === null && !mapAt.get(null)) {
        const control = new FormControl('', [Validators.pattern(REGEX.SPECIAL_TEXT_CODE)]);
        (this.createOrganization.controls['attributes'] as FormArray).push(control);
        const newAtb:AttributesCorrect = {
          orgId: null,
          orgaValue: '',
          orgaId: null,
          control: control,
          positions: [],
        };
        this.dataDetail.orgAttributes.push(newAtb);
        mapAt.set(null, newAtb);
      }
      mapAt.size > 0 && mapAt.get(item.orgaId)?.positions?.push(mapJob.get(item.posId) as any);
    });

    if (!this.dataDetail.orgAttributes.length) {
      const control = new FormControl('', [Validators.pattern(REGEX.SPECIAL_TEXT_CODE)]);
      (this.createOrganization.controls['attributes'] as FormArray).push(control);
      this.dataDetail.orgAttributes.push({
        orgId: null,
        orgaValue: '',
        control: control,
        orgaId: null,
        positions: [],
      });
    }
  }


  goBackPage() {
    this.location.back();
  }

  filterValue(steper: MatStepper) {
    this.isSubmit = true;
    this.createOrganization.controls['sapNo'].setValue(this.createOrganization.value.sapNo.replaceAll(' ', '').trim());
    if (!this.createOrganization.value.fileName) {
      this.createOrganization.controls['attach'].setErrors({ required: true })
    }
    this.getNameByValue();
    if (this.createOrganization.valid) {
      this.stepper = 1;
      steper.next();
    } else {
      const error = this.createOrganization.controls['attach'].errors;
      this.processControl(this.createOrganization);

      this.createOrganization.controls['attach'].setErrors(error);
    }
  }

  processControl(formControl:FormGroup) {
    Object.values(formControl.controls).forEach((control: AbstractControl) => {
      if (control.invalid) {
        control.markAsDirty();
        control.updateValueAndValidity({ onlySelf: true });
      }
    });
  }


  buildRequest() {
    const funs: FunctionOrg[] = [];
    const formatDate = 'DD/MM/YYYY';
    this.dataDetail.orgAttributes.forEach((item) => {
      const jobs:number[] = [];
      item.positions?.forEach((pos) => {
        jobs.push(pos.id);
      });

      const adFun: FunctionOrg = {
        orgaId: item.orgaId,
        orgaValue: item.control?.value,
        jobIds: jobs,
      };

      funs.push(adFun);
    });

    const rq: AdjustOrgRequest = {
      orgId: this.dataDetail.orgId,
      parentId: this.createOrganization.value.parentId,
      branchType: this.createOrganization.value.type,
      region: this.createOrganization.value.area,
      orgName: this.createOrganization.value.orgName,
      inputType: this.type,
      orgLevel: this.dataDetail.orgLevel,
      fromDate: moment(this.createOrganization.value.dateVisible).format(formatDate),
      legalBranchId: null,
      sheetApproval: {
        reason: this.createOrganization.value.description,
        sapId: '',
        sapNo: this.createOrganization.value.sapNo,
      },
      adjustedFunctions: funs,
    };

    if (rq.branchType === RegionType.LB) {
      rq.legalBranchId = this.createOrganization.value.legalBranch;
    }

    if (this.shareData.type) {
      rq.sheetApproval.sapDate = moment(this.createOrganization.value.correctionDate).format(formatDate);
      rq.toDate = this.createOrganization.value.toDate
        ? moment(this.createOrganization.value.toDate).format(formatDate)
        : null;
    }
    const formData: FormData = new FormData();
    formData.append(
      'request',
      new Blob([JSON.stringify(rq)], {
        type: 'application/json',
      })
    );

    //edit file
    if (this.file?.status) {
      if(this.file?.file){
        formData.append('sapFile', this.file.file, this.file.fileName);
      }
    }

    this.shareData.loading = true;

    this.orgService.adjustedOrg(formData).subscribe(
      (data) => {
        this.shareData.loading = false;
        this.toastrCustom.success();
        this.router.navigateByUrl(functionUri.organization_init_screen);
      },
      (error) => {
        this.shareData.loading = false;
        this.toastrCustom.error(error.message);
      }
    );
  }

  getMeanByValue(value:string, list: LookupValue[] | Area[] | Type[]) {
    return list.find(item => item.lvaValue === value)?.lvaMean;
  }

  getIndexControl(controlEl: FormControl): number {
    const controls = (this.createOrganization.controls['attributes'] as FormArray).controls;
    return controls.findIndex((control) => {
      return control === controlEl;
    });
  }
  getNameByValue() {
    if (this.createOrganization.value.type === RegionType.LB) {
      if (this.legalBranchs.length && this.createOrganization.value.legalBranch) {
        const orgSelected = this.legalBranchs.find(item => String(item.orgId) === String(this.createOrganization.value.legalBranch));
        this.createOrganization.controls['legalBranchName'].setValue(orgSelected?.orgName);
      }
    }

    if (this.createOrganization.value.area) {
      this.createOrganization.controls['regionView'].setValue(
        this.getMeanByValue(this.createOrganization.value.area, this.areas)
      );
    }

    if (this.createOrganization.value.type) {
      this.createOrganization.controls['branchView'].setValue(
        this.getMeanByValue(this.createOrganization.value.type, this.types)
      );
    }
  }

  override ngOnDestroy() {
    localStorage.removeItem('viewDetail');
    localStorage.removeItem('org_selected');
  }

  backPrevious(stepper:MatStepper){
    this.stepper = 0;
    stepper.previous();
  }
}
