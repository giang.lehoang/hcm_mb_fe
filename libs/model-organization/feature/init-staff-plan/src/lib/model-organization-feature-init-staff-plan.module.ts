import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InitStaffPlanComponent } from './init-staff-plan/init-staff-plan.component';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { FormsModule } from '@angular/forms';
import {RouterModule} from "@angular/router";
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzIconModule } from 'ng-zorro-antd/icon';

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, FormsModule, SharedUiMbButtonModule,
    TranslateModule, SharedUiMbInputTextModule, SharedUiMbSelectModule, NzTableModule, NzPaginationModule,
    RouterModule.forChild([
      {
        path: '',
        component: InitStaffPlanComponent
      }]), NzGridModule, NzIconModule],
  declarations: [InitStaffPlanComponent],
  exports: [InitStaffPlanComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ModelOrganizationFeatureInitStaffPlanModule {}
