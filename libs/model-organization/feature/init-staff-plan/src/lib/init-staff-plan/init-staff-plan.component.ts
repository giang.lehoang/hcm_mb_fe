import { Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import {
  AproveModel,
  InitStaffSearch,
  Job, PaginationPlan, QUANTITY_DEFAULT, REGEX_NUMBER,
  StaffData, StaffOrganizationNode,
  Year
} from '@hcm-mfe/model-organization/data-access/models';
import {
  InitStaffPlanService,
  StaffPlanService,
  UploadFileService
} from '@hcm-mfe/model-organization/data-access/services';
import { DataService } from '@hcm-mfe/shared/data-access/services';
import { MICRO_SERVICE } from '@hcm-mfe/shared/common/constants';
import { FunctionCode } from '@hcm-mfe/shared/common/enums';
import { forkJoin } from 'rxjs';
import { FormModalShowTreeUnitComponent } from '@hcm-mfe/shared/ui/form-modal-show-tree-unit';
import * as FileSaver from 'file-saver';

@Component({
  selector: 'app-init-staff-plan',
  templateUrl: './init-staff-plan.component.html',
  styleUrls: ['./init-staff-plan.component.scss'],
  providers: [UploadFileService]
})
export class InitStaffPlanComponent extends BaseComponent implements OnInit {
  objectSearch: InitStaffSearch;
  checked = false;
  dataTable: StaffData[] = [];
  quantityMap: Map<number, StaffData> = new Map<number, StaffData>();
  dataApproveAll: Map<number, StaffData> = new Map<number, StaffData>();
  params: InitStaffSearch;
  pagination: PaginationPlan;
  isLoadingPage = false;
  listYears: Year[] = [];
  listJob: Job[] = [];
  messageSuccess ='';
  curentYear: string;

  constructor(
    injector: Injector,
    public uploadFile: UploadFileService,
    public planInitSevice: InitStaffPlanService,
    public dataService: DataService,
    public staffPlanService: StaffPlanService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(
      `FUNCTION_${FunctionCode.INIT_STAFF_PLAN}`
    );
     this.curentYear = String(new Date().getFullYear());


    let org: StaffOrganizationNode = JSON.parse(localStorage.getItem('org_selected'));

    if (!org) {
      org = {
        id: '',
        name: '',
      };
    }
    this.objectSearch = {
      jobId: '',
      periodCode: this.curentYear,
      page: 0,
      size: 15,
      orgId: org?.id,
      orgName: org?.name,
    };
    this.params = {
      ...this.objectSearch,
    };
    this.pagination = {
      currentPage: 1,
      totalElement: 0,
      size: 15,
      totalPage: 0,
      numberOfElements: 0,
    };
  }

  ngOnInit(): void {
    this.checked = false;

    this.isLoadingPage = false;

    this.fetchData(true);

    this.messageSuccess = this.translate.instant('modelOrganization.message.tranferSuccess');
  }
  override ngOnDestroy() {
    localStorage.removeItem('org_selected');
  }

  fetchData(init: boolean) {
    this.isLoadingPage = true;
    const request = [];
    if (init) {
      request.push(this.staffPlanService.getListYear());
      request.push(this.dataService.getJobsByType());
    }

    request.push(this.planInitSevice.search(this.params));

    forkJoin(request).subscribe(
      (data: any) => {
        let plans: any;
        if (init) {
          (data[0]).data.periodCode.forEach((item:any) => {
            this.listYears.push({ value: item });
          });

          this.listJob = data[1].data.content;
          plans = data[2];
        } else {
          plans = data[0];
        }
        this.dataTable = plans.data.content;
        this.pagination.totalElement = plans.data.totalElements;
        this.pagination.totalPage = plans.data.totalPages;
        this.pagination.numberOfElements = plans.data.numberOfElements;
        this.pagination.currentPage = plans.data.pageable.pageNumber + 1;
        this.checked = true;
        this.setData();
        if (!this.dataTable.length) {
          this.checked = false;
        }

        this.caculatePaging();
        this.isLoadingPage = false;
      },
      (error) => {
        this.isLoadingPage = false;
        this.dataTable = [];
        this.listYears = [];
        this.listJob = [];
        this.toastrCustom.error(error.message);
      }
    );
  }
  setData(){
    this.dataTable.forEach((item) => {
      if (!this.quantityMap.has(item.hcpId)) {
        this.checked = false;
      } else {
        this.quantityMap.set(item.hcpId, item);
      }

      if (this.dataApproveAll.has(item.hcpId)) {
        item.quantity = this.dataApproveAll.get(item.hcpId).quantity;
      } else {
        item.quantity = item.headCount ? item.headCount : '';
      }

      this.dataApproveAll.set(item.hcpId, item);
    });
  }

  updateCheckedSet(item: StaffData, checked: boolean): void {
    if (checked) {
      this.quantityMap.set(item.hcpId, item);
    } else {
      this.quantityMap.delete(item.hcpId);
    }
  }

  onCurrentPageDataChange(listOfCurrentPageData: StaffData[]): void {
    this.dataTable = listOfCurrentPageData;
    this.refreshCheckedStatus();
  }

  refreshCheckedStatus(): void {
    this.checked = this.dataTable.every(({ hcpId }) => this.quantityMap.has(hcpId));
  }

  onItemChecked(item: StaffData, checked: boolean): void {
    this.updateCheckedSet(item, checked);
    this.refreshCheckedStatus();
  }

  onAllChecked(checked: boolean): void {
    this.dataTable.forEach((item) => this.updateCheckedSet(item, checked));
    this.refreshCheckedStatus();
  }

  changePage() {
    this.params.page = this.pagination.currentPage - 1;
    this.fetchData(false);
  }

  onSearch() {
    this.dataApproveAll.clear();
    this.params.jobId = this.objectSearch.jobId ? this.objectSearch.jobId : '';
    this.params.orgId = this.objectSearch.orgId ? this.objectSearch.orgId : '';
    this.params.periodCode = this.objectSearch.periodCode ? this.objectSearch.periodCode : '';
    this.params.page = 0;
    this.fetchData(false);
  }

  showUpload() {
    const requestInfo = {
      url: this.planInitSevice.getUrlUpload(),
      file: null,
      service:MICRO_SERVICE.MODEL_PLAN
    };
    this.uploadFile.showModal(
      () => {
        this.dataApproveAll.clear();
        this.onSearch();
      },
      requestInfo,
      this.translate.instant('modelOrganization.message.uploadSuccess')
    );
  }

  showPopup() {
    const width = window.innerWidth / 1.5 > 1100 ? 1368 : window.innerWidth / 1.5;
    const modal = this.modal.create({
      nzWidth:
        window.innerWidth > 767 ? width : window.innerWidth,
      nzTitle: ' ',
      nzComponentParams: {
        isAuthor: true
      },
      nzContent: FormModalShowTreeUnitComponent,
      nzFooter: null,
    });

    modal.afterClose.subscribe((result) => {
      if (result?.orgName) {
        this.objectSearch.orgName = result?.orgName;
        this.objectSearch.orgId = result?.orgId;
      }
    });
  }

  getIndex(i: number) {
    return this.pagination.size * (this.pagination.currentPage - 1) + (i + 1);
  }

  caculatePaging() {
    this.pagination.result = this.pagination.totalElement;
    this.pagination.min = this.getIndex(0);
    this.pagination.max = this.pagination.size * (this.pagination.currentPage - 1) + this.pagination.numberOfElements;
  }

  approveTranferAll() {
    const approveAll: AproveModel[] = [];
    this.dataApproveAll.forEach((value) => {
      if (value.quantity) {
        approveAll.push({
          hcpId: value.hcpId,
          headCount: value.quantity ? value.quantity : QUANTITY_DEFAULT,
        });
      }
    });
    const filterRequest = {
      periodCode: this.objectSearch.periodCode,
      orgId: this.objectSearch.orgId,
      jobId: this.objectSearch.jobId
    }
    const requestBody = {
      request: approveAll,
      filterRequest: !this.objectSearch.periodCode && !this.objectSearch.orgId && this.objectSearch.jobId
        ? null : filterRequest
    }
    this.isLoadingPage = true;
    this.planInitSevice.approveTranferAll(requestBody).subscribe(
      (data) => {
        this.isLoadingPage = false;
        this.quantityMap.clear();
        this.dataApproveAll.clear();
        this.toastrCustom.success(this.messageSuccess);
        this.clearOrgInput();
        this.onSearch();
      },
      (error) => {
        this.isLoadingPage = false;
        this.toastrCustom.error(error.message);
      }
    );
  }

  showComfirm() {
    const onClickUpload = () => {
      this.approveTranferAll();
    };
    this.deletePopup.showApprove(onClickUpload);
  }

  approveTranfer() {
    if (!this.quantityMap.size) {
      this.toastrCustom.error(this.translate.instant('modelOrganization.planYear.chooseMin'));
      return;
    }
    const approve: AproveModel[] = [];
    this.quantityMap.forEach((value) => {
      approve.push({
        hcpId: value.hcpId,
        headCount: value.quantity ? value.quantity : QUANTITY_DEFAULT,
      });
    });
    this.isLoadingPage = true;
    this.planInitSevice.approveTranfer(approve).subscribe(
      (data) => {
        this.isLoadingPage = false;
        this.quantityMap.clear();
        this.toastrCustom.success(this.messageSuccess);
        this.onSearch();
      },
      (error) => {
        this.isLoadingPage = false;
        this.toastrCustom.error(error.message);
      }
    );
  }

  exportExcel() {
    const excelExport = {
      ...this.params,
    };

    delete excelExport.page;
    delete excelExport.size;
    this.planInitSevice.exportExcelInitStaff(excelExport).subscribe(
      (data) => {
        const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
        FileSaver.saveAs(blob, 'Template.xlsx');
      },
      (error) => {
        this.toastrCustom.error(error.message);
      }
    );
  }

  onKeypress(event:any) {
    const pattern = REGEX_NUMBER;
    if (!pattern.test(event.key)) {
      event.preventDefault();
      return;
    }
    const value = parseInt(event.target.value + event.key);

    if (value > 999) {
      event.preventDefault();
      return;
    }

    if (+value === 0 && event.target.value.length === 1) {
      event.preventDefault();
    }
  }

  clearOrgInput() {
    this.objectSearch.orgName = '';
    this.objectSearch.orgId = '';
  }

  onClick(event:any) {
    if (+event.target.value === 0) {
      event.target.select();
    }
  }

  changeQuantity(value: string, data: StaffData) {
    let valueAssign: string;

    if (value === '0' || value === '') {
      return;
    }
   const valuePat = value.length > 3 ? value.substring(0, 3) : value;

    if (this.isInDesiredForm(valuePat)) {
      valueAssign = String(parseInt(valuePat));
    } else {
      valueAssign = '0';
    }

    if (!this.isInDesiredForm(valuePat) || value.length > 3) {
      setTimeout(() => {
        data.quantity = valueAssign;
      }, 500);
    }
  }

  isInDesiredForm(str:any) {
    var n = Math.floor(Number(str));
    return n !== Infinity && String(n) === str && n >= 0;
  }

  override triggerSearchEvent(): void {
    this.onSearch();
  }
}
