import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InitStaffPlanComponent } from './init-staff-plan.component';

describe('InitStaffPlanComponent', () => {
  let component: InitStaffPlanComponent;
  let fixture: ComponentFixture<InitStaffPlanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InitStaffPlanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InitStaffPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
