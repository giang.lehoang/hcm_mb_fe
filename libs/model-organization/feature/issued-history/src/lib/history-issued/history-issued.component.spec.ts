import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryIssuedComponent } from './history-issued.component';

describe('HistoryIssuedComponent', () => {
  let component: HistoryIssuedComponent;
  let fixture: ComponentFixture<HistoryIssuedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistoryIssuedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryIssuedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
