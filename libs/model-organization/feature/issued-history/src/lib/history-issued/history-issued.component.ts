import { Component, Injector, OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {IssuedService} from "../../../../../data-access/services/src/lib/issued-service";
import {HistoryOrg, Pagination, ParamHis} from "@hcm-mfe/model-organization/data-access/models";
import {userConfig} from "@hcm-mfe/shared/common/constants";

@Component({
  selector: 'app-history-issued',
  templateUrl: './history-issued.component.html',
  styleUrls: ['./history-issued.component.scss'],
})
export class HistoryIssuedComponent extends BaseComponent implements OnInit, OnDestroy {
  createOrganization: FormGroup;
  historyIssuedList: HistoryOrg[] = [];
  id: number;
  infoIssueName: string;
  pagination: Pagination = new Pagination(userConfig.pageSize);
  params: ParamHis
  constructor( injector:Injector, readonly orgService: IssuedService) {
    super(injector);
    this.createOrganization = new FormGroup({});
    this.id = JSON.parse(localStorage.getItem('selectedItem')).dataId;
    this.infoIssueName = JSON.parse(localStorage.getItem('selectedItem')).dataName
    if (!this.id) {
      this.router.navigateByUrl('/');
    }

    const dataType = localStorage.getItem('dataType');

    this.params = {
      objId: this.id,
      page: 0,
      size: userConfig.pageSize,
      dataType: dataType ? dataType : ''
    }
    this.getListHistory();
  }

  ngOnInit(): void {
    this.id = 0;
  }

  getListHistory(): void {
    this.orgService.getListHistoryIssued(this.params).subscribe((res) => {
      this.historyIssuedList = res.data.content;
      this.pagination.totalElement = res.data.totalElements
    });
  }

  changePage() {
    if(this.pagination.pageIndex){
      this.params.page = this.pagination.pageIndex - 1;
    }
    this.getListHistory();
  }

  override ngOnDestroy() {
    localStorage.removeItem('dataType');
    localStorage.removeItem('selectedItem');
  }
}
