import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HistoryIssuedComponent} from "./history-issued/history-issued.component";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzGridModule} from "ng-zorro-antd/grid";
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [CommonModule, NzPaginationModule, NzTableModule, NzGridModule, RouterModule.forChild([
    {
      path: '',
      component: HistoryIssuedComponent,
    }])],
  declarations: [HistoryIssuedComponent],
  exports: [HistoryIssuedComponent]
})
export class ModelOrganizationFeatureIssuedHistoryModule {}
