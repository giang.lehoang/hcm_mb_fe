import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PositionGroupAssignComponent } from './position-group-assign.component';

describe('PositionGroupAssignComponent', () => {
  let component: PositionGroupAssignComponent;
  let fixture: ComponentFixture<PositionGroupAssignComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PositionGroupAssignComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PositionGroupAssignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
