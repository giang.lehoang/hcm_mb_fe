import {Component, ElementRef, Injector, OnInit, ViewChild} from '@angular/core';
import {
  AssignPosRequest,
  BaseSelect,
  JobPosUnAssign,
  OrgLine,
  PosUnAssignDetail, PosUnAssignTable
} from "@hcm-mfe/model-organization/data-access/models";
import {
  DataService,
  JobService,
  OrganizationService
} from "@hcm-mfe/model-organization/data-access/services";
import {FunctionCode, functionUri} from "@hcm-mfe/shared/common/enums";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {forkJoin} from "rxjs";

@Component({
  selector: 'app-position-group-assign',
  templateUrl: './position-group-assign.component.html',
  styleUrls: ['./position-group-assign.component.scss']
})
export class PositionGroupAssignComponent extends BaseComponent implements OnInit {

  orgList: OrgLine[] = [];
  orgListFilter: OrgLine[] = [];
  listJobCn: JobPosUnAssign[] = [];
  listJobCnSelect: JobPosUnAssign[] = [];
  dataTableGroup: PosUnAssignTable[] = [];
  selectTab: number;
  isSubmitted = false;
  mapOrg = new Map<number, PosUnAssignTable>();
  mapOrgSelect: Map<number, OrgLine> = new Map();
  isVisible: boolean;
  dataSearch: string;
  selectAll: boolean;
  generateState:boolean;
  submited:boolean;
  groupType = '';
  posDetail: PosUnAssignDetail;
  listGroup:BaseSelect[] = [];
  pgrId:number | undefined;
  @ViewChild('scoll') scoll:ElementRef | undefined;

  constructor(
    injector: Injector,
    readonly dataService: DataService,
    readonly orgService:OrganizationService,
    readonly jobService: JobService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(
      `FUNCTION_${FunctionCode.ORGANIZATION_POSITION_ASSIGN}`
    );
    this.isLoading = false;
    this.isVisible = false;
    this.dataSearch = '';
    this.submited = false;
    this.selectAll = false;
    this.generateState = false;
    this.selectTab = 0;
    const item = localStorage.getItem('posUnassign');
    if(!item){
      this.router.navigate([functionUri.organization_pos_list_unassign]);
    }

    if(item){
      this.posDetail = JSON.parse(item);
      this.groupType = this.posDetail.pgrType;
      this.pgrId = Number(this.posDetail.pgrId);
    }
  }

  ngOnInit(): void {
    this.onSearch();
  }


  changeTab(e:number) {
    this.selectTab = e;
  }


  onSearch(): void {
    this.isLoading = true;

    forkJoin([ this.jobService.getDetailPosUnAssign(this.posDetail), this.orgService.getListOrg(),
    this.jobService.listPosGroupName(this.posDetail.pgrType)]).subscribe(data => {
      data[0].data[0].lstJobHO?.forEach(pos => {
        if(Number(pos.orgLevel) >= 2){
          if(this.mapOrg.has(pos.orgId)){
            const item = this.mapOrg.get(pos.orgId);
            item?.listAllJob.push(pos);
            item?.listSelectJob.push(pos);
          } else {
            const item:PosUnAssignTable = {
              orgId: pos.orgId,
              orgName: '',
              listAllJob: [],
              listSelectJob: [],
              isVisible: false
            }
            item.listAllJob.push(pos);
            item.listSelectJob.push(pos);
            this.mapOrg.set(pos.orgId, item);
            this.dataTableGroup.push(item);
          }
        }
      });
      this.listGroup = data[2].data;
      if(this.listGroup.length > 0 && !this.pgrId){
        this.pgrId = this.listGroup[0].id;
      }

      data[0].data[0].lstJobCN?.forEach(cn => {
          this.listJobCn.push(cn);
          this.listJobCnSelect.push(cn);
      });

      data[1].data.forEach(org => {
        if(this.mapOrg.has(org.orgId)){
          const itemTable = this.mapOrg.get(org.orgId);
          if(itemTable){
            itemTable.orgName = org.pathName;
          }
          this.mapOrgSelect.set(org.orgId, org);
          this.orgList.push(org);
          this.orgListFilter.push(org);
        }
      });
      if(this.dataTableGroup.length){
        this.selectTab = 0;
      } else {
        this.selectTab = 1;
      }

      this.isLoading = false;
    }, error => {
      this.isLoading = false;
      this.toastrCustom.error(error.message);
    });
  }

  changeValue(value:string) {
    this.orgListFilter = this.orgList.filter(
      (item) => {
        if(item.pathName) return item.pathName.toLowerCase().indexOf(value.toLowerCase().trim()) > -1
        return false;
      }
    );
  }

  onSelect(item:OrgLine) {
    if (this.mapOrgSelect.has(item.orgId ? item.orgId : -1)) {
      this.mapOrgSelect.delete(item.orgId ? item.orgId : -1);
    } else {
      this.mapOrgSelect.set(item.orgId ? item.orgId : -1, item);
    }
  }


  deleteOrg(index: number) {
    const id = this.dataTableGroup[index].orgId;
    this.dataTableGroup[index].isVisible = false;
    this.mapOrgSelect.delete(id);
    this.dataTableGroup.splice(index, 1);
  }

  selectOrg(){
    this.genOrg();
  }

  genOrg() {
    this.dataTableGroup = [];
    this.mapOrgSelect.forEach(values => {
      const item = this.mapOrg.get(values.orgId);
      if(item){
        this.dataTableGroup.push(item);
      }
    })
  }

  change(value:boolean) {
    if (value) {
      this.orgListFilter.forEach((org) => {
        if(!this.mapOrgSelect.has(org.orgId)){
          this.mapOrgSelect.set(org.orgId, org);
        }
      });
    } else {
      this.mapOrgSelect.clear();
    }
  }

  changePopup(value:boolean){
    if(value){
      this.dataTableGroup.forEach(org => {
        const item = this.orgList.find(orgL => orgL.orgId === org.orgId);
        if(item){
          this.mapOrgSelect.set(org.orgId, item)
        }
      })
    }
  }

  assignPosition() {
    this.submited = true;
    const pgrName = this.listGroup.find(item => item.id === this.pgrId);
    const req:AssignPosRequest = {
      pgrId:this.pgrId ? Number(this.pgrId) : 0,
      pgrName: pgrName?.name ? pgrName.name : '',
      pgrType: this.groupType,
      posId: [],
      type: this.posDetail.type
    };

    for(const org of this.dataTableGroup){
      if(org.listSelectJob.length === 0){
        return;
      }
      const listId = org.listSelectJob.map(item => item.posId);
      req.posId.push(...listId);
    }

    this.listJobCnSelect.forEach((job) => {
      req.posId.push(job.posId);
    });

    if(req.posId.length === 0){
      this.toastrCustom.error(this.translate.instant('modelOrganization.message.job'));
      return;
    }
    this.isLoading = true;
    this.jobService.assignPosUnassign(req).subscribe(
      (data) => {
        this.isLoading = false;
        this.toastrCustom.success();
        this.router.navigateByUrl(functionUri.organization_pos_list_unassign);
      },
      (error) => {
        this.isLoading = false;
        this.toastrCustom.error(error.message);
      }
    );
  }

  trackByFn(index:number, item:OrgLine|PosUnAssignTable) {
    return item.orgId;
  }


  onScroll(event:Event){
    const el = this.scoll?.nativeElement;
    if(el.scrollHeight === el.clientHeight + el.scrollTop){

    }
  }

}
