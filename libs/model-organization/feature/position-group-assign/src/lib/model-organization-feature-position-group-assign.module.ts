import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PositionGroupAssignComponent} from "./position-group-assign/position-group-assign.component";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {ModelOrganizationUiJobSelectModule} from "@hcm-mfe/model-organization/ui/job-select";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NzTabsModule} from "ng-zorro-antd/tabs";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzEmptyModule} from "ng-zorro-antd/empty";
import {RouterModule} from "@angular/router";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {NzGridModule} from "ng-zorro-antd/grid";
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";
import {NzSwitchModule} from "ng-zorro-antd/switch";

@NgModule({
    imports: [CommonModule, SharedUiLoadingModule, SharedUiMbButtonModule, TranslateModule, NzCheckboxModule,
        RouterModule.forChild([
            {
                path: '',
                component: PositionGroupAssignComponent,
            }]),
        SharedUiMbInputTextModule, ModelOrganizationUiJobSelectModule, NzDropDownModule, FormsModule, ReactiveFormsModule,
        NzTabsModule, NzTableModule, NzEmptyModule, SharedUiMbSelectModule, NzGridModule, NzSwitchModule],
  declarations: [PositionGroupAssignComponent],
  exports: [PositionGroupAssignComponent]
})
export class ModelOrganizationFeaturePositionGroupAssignModule {}
