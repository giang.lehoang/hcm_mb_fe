import { Component, OnInit, Injector, TemplateRef } from '@angular/core';
import * as FileSaver from 'file-saver';
import { forkJoin } from 'rxjs';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {
  Attributes,
  Job,
  LookupParam,
  LookupValue,
  listStatus,
  JDsModel, Pagination, ParamsJDSearch, ResponseEntity, ResponseEntityData, SelectObject, Issues
} from "@hcm-mfe/model-organization/data-access/models";
import { MICRO_SERVICE, userConfig, DATA_TYPE, Mode } from '@hcm-mfe/shared/common/constants';
import {FunctionCode, functionUri} from '@hcm-mfe/shared/common/enums';
import {DataService, UploadFileService} from "@hcm-mfe/model-organization/data-access/services";
import {JDService} from "../../../../../data-access/services/src/lib/jd.service";
import {FormModalShowTreeUnitComponent} from "@hcm-mfe/shared/ui/form-modal-show-tree-unit";
import {renderFlagStatus} from "../../../../../helper/common.function";
import {IssuedService} from "../../../../../data-access/services/src/lib/issued-service";
import { ActionDocumentAttachedFileComponent } from '@hcm-mfe/goal-management/ui/form-input';
import { NzModalRef } from 'ng-zorro-antd/modal';
@Component({
  selector: 'app-job-description',
  templateUrl: './job-description.component.html',
  styleUrls: ['./job-description.component.scss'],
  providers: [
    UploadFileService,
  ],
})
export class JobDescriptionComponent extends BaseComponent implements OnInit {
  orgName: string;
  searchParams: ParamsJDSearch = new ParamsJDSearch();
  params: ParamsJDSearch = new ParamsJDSearch();
  listJob: Job[] = [];
  listAttribute: Attributes[] = [];
  lookupParram: LookupParam = new LookupParam();
  listJobGroup: LookupValue[] = [];
  listCareerGroup: LookupValue[] = [];
  listStatus:SelectObject<number,string>[];
  dataTable: JDsModel[] = [];
  pagination: Pagination = new Pagination(userConfig.pageSize);
  isLoadingPage: boolean;
  isSeach: boolean;
  modalRef: NzModalRef | undefined;
  renderFlagStatus = renderFlagStatus;
  constructor(
    injector: Injector,
    private readonly jdServicce: JDService,
    private  readonly dataService: DataService,
    private readonly issuedService: IssuedService,
    public uploadFile: UploadFileService,
  ) {
    super(injector)
    this.isSeach = false;
    this.orgName = '';
    this.isLoadingPage = false;
    this.listStatus = listStatus.filter(item => item.id !== 6);
    this.objFunction = this.sessionService.getSessionData(
      `FUNCTION_${FunctionCode.JOB_DESCRIPTION}`
    );
  }

  ngOnInit(): void {
    this.searchParams.flagStatus = 7;
    this.fetchData(true)
  }

  fetchData(init: boolean) {
    this.isLoadingPage = true;
    const request = [];
    if (init) {
      request.push(this.dataService.getLookupValue(this.lookupParram.lookupCode));
      this.lookupParram.lookupCode = 'NHOM_NGHE_NGHIEP';
      request.push(this.dataService.getLookupValue(this.lookupParram.lookupCode));
    }
    const params = this.checkParams();
    request.push(this.jdServicce.getJDs(params));

    forkJoin(request).subscribe(
      (data: Array<any>) => {
        let jds;
        if (init) {
          this.listJobGroup = (<ResponseEntity<Array<LookupValue>>>data[0]).data;
          this.listCareerGroup = (<ResponseEntity<Array<LookupValue>>>data[1]).data;
          jds = (<ResponseEntityData<Array<JDsModel>>>data[2]).data;
        } else {
          jds = (<ResponseEntityData<Array<JDsModel>>>data[0]).data;
        }
        this.dataTable = jds.content;
        this.pagination.totalElements = jds.totalElements;
        this.pagination.totalPages = jds.totalPages;
        this.pagination.numberOfElements = jds.numberOfElements;
        this.pagination.currentPage = jds.pageable.pageNumber;
        this.isLoadingPage = false;
        this.isSeach = false;
      },
      (error) => {
        this.isLoadingPage = false;
        this.dataTable = [];
        this.toastrCustom.error(error.message);
      }
    );
  }

  showModalOrg(event:string): void {
    const modal = this.modal.create({
      nzWidth: '85vw',
      nzTitle: '',
      nzContent: FormModalShowTreeUnitComponent,
      nzFooter: null,
    });
    modal.afterClose.subscribe((result) => {
      if (result?.orgName) {
        this.orgName = result?.orgName;
        this.searchParams.orgId = result?.orgId;
        this.getJob();
        this.getAttributeJds();
      }
    });
  }
  clearOrgInput() {
    this.orgName = '';
    this.searchParams.orgId = null;
    this.searchParams.jobId = null;
    this.searchParams.orgaId = null;
  }

  getJob() {
    if(!this.searchParams.orgId){
      return;
    }
    this.jdServicce.getJobByOrgId(this.searchParams.orgId).subscribe((data) => {
      this.listJob = data.data;
    }, (error) => {
      this.toastrCustom.error(error.message);
    })
  }

  getAttributeJds() {
    if (this.searchParams.orgId) {
      this.jdServicce.getAttribute(this.searchParams.orgId).subscribe((data: ResponseEntity<Array<Attributes>>) => {
        this.listAttribute = data.data?.filter(item => item.orgaValue);
      },
        (error) => {
        this.toastrCustom.error(error.message);
      })
    }
  }

  onSearch() {
    this.isSeach = true;
    this.fetchData(false);
  }
  changePage(value: number) {
    this.searchParams.page = value - 1;
    this.fetchData(false);
  }
  checkParams() {
    const paramsTmp = { ...this.searchParams };
    if (!paramsTmp.flagStatus && paramsTmp.flagStatus !== 0) {
      delete paramsTmp.flagStatus
    }
    if (!paramsTmp.orgId) {
      delete paramsTmp.orgId
    }
    if (!paramsTmp.jobId) {
      delete paramsTmp.jobId;
    }
    if (!paramsTmp.lvaIdJob) {
      delete paramsTmp.lvaIdJob;
    }
    if (!paramsTmp.lvaIdProfession) {
      delete paramsTmp.lvaIdProfession
    }
    if (!paramsTmp.orgaId) {
      delete paramsTmp.orgaId
    }
    delete paramsTmp.description;
    if (this.isSeach) {
      delete paramsTmp.page;
    }
    return paramsTmp;
  }
  deleteJd(data: JDsModel) {
    const params = {
      "psdId": data.psdId,
    }
    this.deletePopup.showModal(() => {
      this.isLoadingPage = true;
      this.jdServicce.deleteJDs(params).subscribe(data => {
        this.isLoadingPage = false;
        this.toastrCustom.success();
        this.onSearch();
      }, error => {
        this.isLoadingPage = false;
        this.toastrCustom.error(error.message)
      })
    })
  }
  addJds(data?: JDsModel) {
    if (data) {
      localStorage.setItem('posInfo', JSON.stringify(data));
    } else {
      localStorage.removeItem('posInfo');
    }
    this.router.navigate([functionUri.organization_jd_init], { state: { data: data } });
  }
  editJds(data: JDsModel) {
    localStorage.setItem('posInfo', JSON.stringify(data));
    this.router.navigate([functionUri.organization_jd_init], { state: { data: data } });
  }
  onClickRow(data: JDsModel) {
    if (+data.flagStatus === 7 || data.flagStatus === null) {
      return;
    } else {
      data.view = true;
      localStorage.setItem('posInfo', JSON.stringify(data));
      this.router.navigate([functionUri.organization_jd_init], { state: { data: data } });
    }
  }
  exportExcel() {
    const params = this.checkParams()
    this.jdServicce.exportExcel(params).subscribe(
      (data) => {
        const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
        FileSaver.saveAs(blob, 'Template.xlsx');
      },
      (error) => {
        this.toastrCustom.error(error.error.message);
      }
    );
  }
  exportFile() {
    // const params = this.checkParams()
    // this.jdServicce.exportTemplate(params).subscribe(
    //   (data) => {
    //     const blob = new Blob([data], { type: 'application/octet-stream'});
    //     FileSaver.saveAs(blob, 'Template.xlsx');
    //   },
    //   (error) => {
    //     this.toastrCustom.error(error.error.message);
    //   }
    // );
  }

  issuedJD(jd:JDsModel){
    const issued:Issues = {
      dataType: DATA_TYPE.jd,
      description: '',
      issuedDate: '',
      dataName: jd.jdName,
      dataId: jd.jdId
    }
    this.issuedService.setListIdSelected([issued]);
    this.router.navigateByUrl('/organization/issued/issued-transfer');
  }
  showUpload(footerTmpl: TemplateRef<any>) {
    this.modalRef = this.modal.create({
      nzWidth: this.getInnerWidth(3.5),
      nzTitle: this.translate.instant('common.label.uploadFileExel'),
      nzContent: ActionDocumentAttachedFileComponent,
      nzComponentParams: {
        mode: Mode.ADD,
        url: this.jdServicce.importTemplate(),
        microService: MICRO_SERVICE.MODEL_PLAN,
        fileName: 'TEMPLATE',
        module: 'MODEL_PLAN',
        urlDownload:this.jdServicce.exportTemplate(),
        param: this.checkParams()
      },
      nzFooter: footerTmpl,
    });
    this.modalRef.afterClose.subscribe((result) => (result?.refresh ? this.onSearch() : ''));
  }
  getInnerWidth(point: number): number {
    if (window.innerWidth > 767) {
      return window.innerWidth / point > 1100 ? 1100 : window.innerWidth / point;
    }
    return window.innerWidth;
  }
  override triggerSearchEvent(): void {
    this.onSearch();
  }
}
