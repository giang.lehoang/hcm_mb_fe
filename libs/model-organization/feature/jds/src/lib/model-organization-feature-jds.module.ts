import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {JobDescriptionComponent} from "./jds/job-description.component";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzCardModule} from "ng-zorro-antd/card";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {FormsModule} from "@angular/forms";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import {NzTagModule} from "ng-zorro-antd/tag";
import {RouterModule} from "@angular/router";
import {TranslateModule} from "@ngx-translate/core";
import { NzGridModule } from 'ng-zorro-antd/grid';
import {NzIconModule} from "ng-zorro-antd/icon";
import {MatMenuModule} from "@angular/material/menu";
import {MatIconModule} from "@angular/material/icon";
@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, SharedUiMbButtonModule, TranslateModule, NzCardModule, NzIconModule,
    SharedUiMbInputTextModule, FormsModule, SharedUiMbSelectModule, NzTableModule, NzToolTipModule, NzPaginationModule,
    RouterModule.forChild([
      {
        path: '',
        component: JobDescriptionComponent
      }]),
    NzTagModule, NzGridModule, MatMenuModule, MatIconModule],
  declarations: [JobDescriptionComponent],
  exports: [JobDescriptionComponent]
})
export class ModelOrganizationFeatureJdsModule {}
