
import { Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import {
  Area, InfoOrganization, ListLegalBranch,
  Pagination, RegionType,
  Type
} from '@hcm-mfe/model-organization/data-access/models';
import { DataService } from '@hcm-mfe/shared/data-access/services';
import { OrganizationalModelTemplateService } from '@hcm-mfe/model-organization/data-access/services';
import { FunctionCode } from '@hcm-mfe/shared/common/enums';
import * as FileSaver from 'file-saver';
import {listOrgLevel} from '@hcm-mfe/model-organization/data-access/models';

@Component({
  selector: 'app-list-info-organization',
  templateUrl: './list-info-organization.component.html',
  styleUrls: ['./list-info-organization.component.scss'],
})
export class ListInfoOrganizationComponent extends BaseComponent implements OnInit {
  isLoadingPage: boolean;
  objectSearch: any = {
    orgName: '',
    branchType: '',
    region: '',
    orgLevel: '',
    page: 0,
    size: 20,
  }
  listSettingPaginationCustom: Pagination = {
    currentPage: 0,
    size: 0,
    total: 0,
    pageIndex: 0,
    numberOfElements: 0
  };
  listOrgLevel = listOrgLevel;
  listBranch: Array<Type>  = [];
  listLegalBranch: ListLegalBranch[]= [];
  listRegion: Array<Area>= [];
  listDataInfoOrg: InfoOrganization[] = [];
  isReset = false;
  tableConfigVisible = false;
  tableConfigHeaderShow = [];
  allTableFieldChecked:any;
  allTableFieldIndeterminate = false;
  checkedAll:any;
  columnSearch ='';
  tableConfigHeader = {
    headers: [],
  };
  tableConfigHeaderSearch:any = {
    headers:[]
  }
  initTableConfig = {
    headers: [
      {
        title: 'Mã',
        field: 'orgCode',
        nameSubmit:' orgCodeColumn',
        show: true,
        width: 120,
      },
      {
        title: 'Đơn vị quản lý',
        field: 'orgNameManage',
        nameSubmit:'orgNameManageColumn',
        show: true,
      },
      {
        title: 'Đơn vị Trực Thuộc 1',
        field: 'orgNameLevel1',
        nameSubmit:'orgNameLevel1Column',
        show: true,
      },
      {
        title: 'Đơn vị Trực Thuộc 2',
        field: 'orgNameLevel2',
        nameSubmit:'orgNameLevel2Column',
        show: true,
      },
      {
        title: 'Đơn vị Trực Thuộc 3',
        field: 'orgNameLevel3',
        nameSubmit:'orgNameLevel3Column',
        show: true,
      },
      {
        title: 'Tên viết tắt',
        field: 'orgAbbreviation',
        nameSubmit:'orgAbbreviationColumn',
        show: true,
      },
      {
        title: 'Tên đầy đủ',
        field: 'orgFullName',
        nameSubmit:'orgFullNameColumn',
        show: true,
      },
      {
        title: 'Cấp đơn  vị',
        field: 'orgLevel',
        nameSubmit:'orgLevelColumn',
        show: false,
      },
      {
        title: 'Loại chi nhánh',
        field: 'orgTypeValue',
        nameSubmit:'orgTypeValueColumn',
        show: true,
      },
      {
        title: 'Khu vực',
        field: 'regionValue',
        nameSubmit:'regionValueColumn',
        show: true,
      },
      {
        title: 'Tiểu vùng',
        field: 'subRegion',
        nameSubmit:'subRegionColumn',
        show: false,
      },
      {
        title: 'Chi nhánh pháp lý',
        field: 'branchName',
        nameSubmit:'legalBranchColumn',
        show: true,
      },
      {
        title: 'Địa chỉ',
        field: 'address',
        nameSubmit: 'addressColumn',
        show: false,
      },
      {
        title: 'Điện thoại',
        field: 'telephone',
        nameSubmit: 'telephoneColumn',
        show: false,
      },
      {
        title: 'Fax',
        field: 'fax',
        nameSubmit: 'faxColumn',
        show: false,
      },
      {
        title: 'Mã T24',
        field: 't24Code',
        nameSubmit: 't24CodeColumn',
        show: true,
      },
      {
        title: 'Người đại diện',
        field: 'surrogate',
        nameSubmit: 'surrogateColumn',
        show: false,
      },
    ],
  };
  count = 0;
  constructor(
    injector: Injector,
    readonly dataService: DataService,
    readonly orgService: OrganizationalModelTemplateService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ORGANIZATION_LIST_INFO}`);
    this.isLoadingPage = true;
    this.dataService.getRegionCategoryList(RegionType.KV, 1).subscribe((data: any) => {
      this.listRegion = data.data;
    });
    this.dataService.getRegionCategoryList(RegionType.LH, 1).subscribe((data: any) => {
      this.listBranch = data.data;
    });
    this.getListBranch();
  }
  ngOnInit(): void {
    const cloneArray = JSON.parse(JSON.stringify(this.initTableConfig));
    this.tableConfigHeader = cloneArray;
    this.tableConfigHeaderShow = cloneArray.headers.filter((item:any) => item.show === true);
    this.allTableFieldIndeterminate = true;
    this.tableConfigHeaderSearch.headers = this.initTableConfig.headers;
  }

  getListInfo(): void {
    const params = this.checkParams();
    this.orgService.getListInfoOrganization(params).subscribe(
      (data: any) => {
        this.listDataInfoOrg = data.data.content;
        this.listDataInfoOrg.forEach(org => {
          org.orgNameLevel1 = '';
          org.orgNameLevel2 = '';
          org.orgNameLevel3 = '';
          org.orgNameLevel4 = '';
          const nameSp = org.fullName?.split('-');
          org.orgNameManage = nameSp[0];
          if(nameSp.length >= 2){
            org.orgNameLevel1 = nameSp[1]
          }
          if(nameSp.length >= 3){
            org.orgNameLevel2 = nameSp[2]
          }
          if(nameSp.length >= 4){
            org.orgNameLevel3 = nameSp[3]
          }
          if(nameSp.length >= 5){
            org.orgNameLevel4 = nameSp[4]
          }
        })
        this.listSettingPaginationCustom.size = data.data.size;
        this.listSettingPaginationCustom.total = data.data.totalElements;
        this.listSettingPaginationCustom.pageIndex = data.data.pageable.pageNumber;
        this.listSettingPaginationCustom.numberOfElements = data.data.numberOfElements;
        this.isLoadingPage = false;
        this.listLegalBranch &&
          this.listLegalBranch.forEach((itemBranch) => {
            this.listDataInfoOrg.forEach((itemInfo) => {
              if (itemInfo.legalBranchId && itemInfo.legalBranchId === itemBranch.orgId) {
                itemInfo.branchName = itemBranch.orgName;
              }
            });
          });
      },
      () => {
        this.isLoadingPage = false;
      }
    );
  }

  getListBranch() {
    this.orgService.getListLegalBranch().subscribe(
      (data: any) => {
        this.listLegalBranch = data.data;
        this.getListInfo();
      },
      () => {
        this.isLoadingPage = false;
      }
    );
  }
  searchInfoOrganization(): void {
    this.objectSearch.size = 20;
    if (!this.objectSearch.region) {
      this.objectSearch.region = '';
    }
    if (!this.objectSearch.branchType) {
      this.objectSearch.branchType = '';
    }
    if (!this.objectSearch.orgLevel) {
      this.objectSearch.orgLevel = '';
    }
    this.objectSearch.hiddenColumn = [];
    this.isLoadingPage = true;
    this.getListInfo();
  }
  changeOrgName(event:any): void {
    if (event.target.value !== '') {
      this.objectSearch.orgName = event.target.value;
    } else {
      // @ts-ignore
      delete this.objectSearch.orgName;
    }
  }
  getListTemplateBasePagination(value:any): void {
    this.objectSearch.page = value - 1;
    this.isLoadingPage = true;
    this.getListInfo();
  }
  editInfoOrg(id:any): void {
    localStorage.setItem('selectedId', JSON.stringify(id));
    this.router.navigateByUrl(`/organization/init/info`);
  }

  exportExcel(): void {
    const resultHeader = this.tableConfigHeaderSearch.headers.length > 0 ? this.tableConfigHeaderSearch.headers : this.initTableConfig.headers;
    const resultFilter = resultHeader.filter((item:any) => item.show === false).map((item:any) => item.nameSubmit);
    this.objectSearch.size = this.listSettingPaginationCustom.total;
    this.objectSearch.hiddenColumn = resultFilter;
    this.objectSearch.page = 0;
    this.orgService.exportListOrganization(this.objectSearch).subscribe((data: any) => {
      const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      FileSaver.saveAs(blob, 'Danh sach thong tin don vi.xls');
    });
  }
  goBackPage(): void {
    this.location.back();
  }

  override triggerSearchEvent(): void {
    this.searchInfoOrganization();
  }

  changeSignalCheck(e: any, item: any): void {
    item.show = e;
    this.tableConfigHeaderShow = this.tableConfigHeaderSearch.headers.filter((item:any) => item.show === true);
    this.allTableFieldChecked = this.tableConfigHeader.headers.every((item:any) => item.show === true);
    const allUnChecked = this.tableConfigHeader.headers.every((item:any) => !item.show);
    this.allTableFieldIndeterminate = !this.allTableFieldChecked && !allUnChecked;
  }

  searchColumn(event:any) {
    if (event.target.value) {
      let cloneArray = JSON.parse(JSON.stringify(this.initTableConfig.headers));

      cloneArray = cloneArray.filter((item:any) => item.title.toLowerCase().includes(event.target.value.toLowerCase()));
      if(this.allTableFieldChecked === true && this.checkedAll === true && this.isReset === false) {
        cloneArray.forEach((item:any) => {
          item.show = true
        })
      }else if(this.allTableFieldChecked === false && this.checkedAll === false && this.isReset === false) {
        cloneArray.forEach((item:any) => {
          item.show = false
        })
      }
      this.tableConfigHeaderSearch.headers = cloneArray;
    } else {
      if (this.allTableFieldChecked === false && this.checkedAll === false && this.isReset === false) {
        const cloneArray = JSON.parse(JSON.stringify(this.initTableConfig.headers));
        cloneArray.forEach((item:any) => {
          item.show = false;
        });
        this.tableConfigHeaderSearch.headers = cloneArray;
      } else if (this.allTableFieldChecked === true && this.checkedAll === true && this.isReset === false) {
        const cloneArray = JSON.parse(JSON.stringify(this.initTableConfig.headers));
        cloneArray.forEach((item:any) => {
          item.show = true;
        });
        this.tableConfigHeaderSearch.headers = cloneArray;
      } else {
        this.tableConfigHeaderSearch.headers = this.initTableConfig.headers;
      }
    }
  }

  changeAllTableTableConfigShow(e: boolean): void {
    this.isReset = false;
    if (e) {
      this.allTableFieldChecked = e;
      this.allTableFieldIndeterminate = false;
      this.checkedAll = true;
    } else {
      this.checkedAll = false;
    }
    this.tableConfigHeader.headers.forEach((item:any) => (item.show = e));
    this.tableConfigHeaderSearch = this.tableConfigHeader;
    this.tableConfigHeaderShow = this.tableConfigHeader.headers.filter((item:any) => item.show === true);
  }

  reset(): void {
    const cloneArray = JSON.parse(JSON.stringify(this.initTableConfig));
    this.tableConfigHeader.headers = cloneArray.headers;
    this.tableConfigHeaderShow = this.tableConfigHeader.headers.filter((item:any) => item.show === true);
    this.tableConfigHeaderSearch.headers = this.tableConfigHeader.headers;
    this.allTableFieldChecked = false;
    this.allTableFieldIndeterminate = true;
    this.isReset = true;
  }
  checkParams(){
    return {
      page: this.objectSearch.page,
      size: 20,
      orgName: this.objectSearch.orgName,
      branchType: this.objectSearch.branchType,
      region: this.objectSearch.region,
      orgLevel: this.objectSearch.orgLevel
    }
  }

}
