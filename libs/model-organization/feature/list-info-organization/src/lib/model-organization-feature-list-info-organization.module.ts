import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { TranslateModule } from '@ngx-translate/core';
import { NzCardModule } from 'ng-zorro-antd/card';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { RouterModule } from '@angular/router';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { ModelOrganizationFeatureRegionCategoryInitModule } from '@hcm-mfe/model-organization/feature/region-category-init';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { SharedDirectivesPaddingDirectivesModule } from '@hcm-mfe/shared/directives/padding-directives';
import { ListInfoOrganizationComponent } from './list-info-organization/list-info-organization.component';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { NzSpaceModule } from 'ng-zorro-antd/space';

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, SharedUiMbButtonModule, TranslateModule, NzCardModule, ReactiveFormsModule, FormsModule,
    NzGridModule, SharedUiMbSelectModule, FormsModule, SharedUiMbInputTextModule, NzTableModule, NzPaginationModule,
    RouterModule.forChild([
      {
        path: '',
        component: ListInfoOrganizationComponent
      }]),
    NzToolTipModule, NzModalModule, NzTagModule, ModelOrganizationFeatureRegionCategoryInitModule, NzIconModule,
    SharedDirectivesPaddingDirectivesModule, NzCheckboxModule, NzPopoverModule, DragDropModule, NzSpaceModule
  ],
  declarations: [ListInfoOrganizationComponent],
  exports: [ListInfoOrganizationComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ModelOrganizationFeatureListInfoOrganizationModule {}
