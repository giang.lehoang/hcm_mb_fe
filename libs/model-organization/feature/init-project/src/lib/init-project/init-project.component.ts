import { Component, Injector, OnDestroy, OnInit } from '@angular/core';
import {FormGroup, Validators, FormBuilder, FormArray, AbstractControl} from '@angular/forms';
import * as moment from 'moment';
import { forkJoin } from 'rxjs';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import { FunctionCode } from '@hcm-mfe/shared/common/enums';
import {
  DataService,
  ProjectService,
  JobService,
  OrganizationService
} from "@hcm-mfe/model-organization/data-access/services";
import {
   CriteriaRes,
  JobResponse,
  OrganizationDetail, PJAtb,
  ProjectDetailResponse
} from "@hcm-mfe/model-organization/data-access/models";

@Component({
  selector: 'app-init-project',
  templateUrl: './init-project.component.html',
  styleUrls: ['./init-project.component.scss'],
})
export class InitProjectComponent extends BaseComponent implements OnInit, OnDestroy {
  initProjectForm: FormGroup;
  fileName: string;
  errorMessageMaxFileSize: string;
  errorMessageQuantity: string;
  errorDate: string;
  hideMinus: boolean;
  listProject:OrganizationDetail[] = [];
  listJob: JobResponse[] = [];
  listCriteria:CriteriaRes[] = [];
  listProjectAttributesSelected:PJAtb[] = [];
  multipleValue:number[] = [];
  id: string | null;
  errorUnit: boolean;
  listFlagStatus = {
    PENDING: '2',
    NEW: '5'
  };
  checkRequestApprove: boolean;
  projectAttributesTemp: {attrCheckBox?:boolean, attrValue:string, criteria:any}[] | null = [];
  file: File | undefined;
  isVisible: boolean;
  pdfSrc: string;
  detailProject: ProjectDetailResponse | undefined;
  checkLoaded: boolean;

  constructor(
    injector: Injector,
    readonly formBuilder: FormBuilder,
    readonly projectService: ProjectService,
    readonly jobService: JobService,
    readonly dataService: DataService,
    readonly orgService:OrganizationService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(
      `FUNCTION_${FunctionCode.INIT_PROJECT}`
    );
    this.checkRequestApprove = false;
    this.isVisible = false;
    this.pdfSrc = '';
    this.checkLoaded = false;
    this.errorUnit = false;
    this.isLoading = false;
    this.fileName = '';
    this.errorMessageMaxFileSize = '';
    this.errorMessageQuantity = '';
    this.errorDate = '';
    this.hideMinus = false;
    this.id = localStorage.getItem('selectedId');

    this.initProjectForm = this.formBuilder.group({
      prjName: ['', Validators.required],
      sapNo: ['', Validators.required],
      file: [''],
      fileName: ['', Validators.required],
      sapFile: [''],
      flagStatus: [''],
      fromDate: ['', Validators.required],
      toDate: [''],
      projectPositions: this.formBuilder.array([
        this.formBuilder.group({
          jobId: [''],
          headCount: [''],
        }),
      ]),
      projectAttributes: this.formBuilder.array([]),
      prjCode: [''],
      scopeContents: [''],
      organizations: this.formBuilder.array([
        this.formBuilder.group({
          orgId: ['']
        }),
      ]),
      unit: [],
    });
  }

  ngOnInit(): void {

    this.orgService.getListOrgDetail().subscribe((data) => {
      if(data.data?.content){
        this.listProject = data.data.content;
      }
    });
    this.jobService.getJobType('PRJ').subscribe((data) => {
      if(data.data?.content){
        this.listJob = data.data.content;
      }
    });

    if (this.id) {
      this.isLoading = true;
      forkJoin([
        this.projectService.getDetailProject(this.id),
        this.projectService.getAllListCriteria()
      ]).subscribe(res => {
        this.isLoading = false;
        if (res[0].data) {
          this.setData(res)
        }

        if (res[1].data.length > 0) {
          this.listCriteria = res[1].data;
          this.initTargetForm()
        }
      },
        (error) => {
          this.isLoading = false;
          this.toastrCustom.error(error.message);
        }
      );
    } else {
      this.isLoading = true;
      this.projectService.getAllListCriteria().subscribe((data: any) => {
        this.isLoading = false;
        this.listCriteria = data.data;
        this.initTargetForm()
      });
    }
  }

  override ngOnDestroy() {
  localStorage.removeItem('selectedId');
 }

 setData(res:any){
   const data = res[0];
   this.detailProject = res[0].data;
   this.initProjectForm.controls['prjName'].setValue(data.data?.prjName);
   this.initProjectForm.controls['sapNo'].setValue(data.data?.sheetApproval.sapNo);
   this.initProjectForm.controls['fromDate'].setValue(data.data?.fromDate);
   this.initProjectForm.controls['toDate'].setValue(data.data?.toDate);
   this.initProjectForm.controls['prjCode'].setValue(data.data?.prjCode);
   this.initProjectForm.controls['flagStatus'].setValue(data.data?.flagStatus);
   if (data.data?.document && data.data.document.fileName) {
     this.fileName = data.data.document.fileName;
     this.initProjectForm.controls['fileName'].setValue(this.fileName);
   }
   if(data.data?.projectAttributes) {
     this.listProjectAttributesSelected = data.data.projectAttributes;
   }
   const projectPositions = this.initProjectForm.get('projectPositions') as FormArray;
   data.data?.projectPositions.forEach((item: any) => {
     projectPositions.push(
       this.formBuilder.group({
         headCount: item.headCount,
         jobId: item.jobId,
       })
     );
   });
   projectPositions.removeAt(0);
   this.initProjectForm.controls['scopeContents'].setValue(data.data?.scopeContents);
   const resultArray:number[] = [];
   data.data?.organizations.forEach((item:any) => {
     resultArray.push(item.orgId);
   });
   this.multipleValue = resultArray;
 }

  transformBeforeSave() {
    const arraySelect:{orgId:number}[] = [];
    this.multipleValue.forEach((item) => {
      arraySelect.push({ orgId: parseInt(String(item), 10) });
    });
    delete this.initProjectForm.value.file;
    this.initProjectForm.value.sheetApproval = [];
    const objectSubmit = {
      sapNo: this.initProjectForm.value.sapNo,
      sapId: null,
    };
    this.initProjectForm.value.sheetApproval = objectSubmit;
    delete this.initProjectForm.value.sapNo;
    this.initProjectForm.value.organizations = arraySelect;
    this.projectAttributesTemp = JSON.parse(JSON.stringify(this.initProjectForm.value.projectAttributes));

    this.projectAttributesTemp && this.projectAttributesTemp.forEach((element) => {
      delete element.criteria;
    });
    this.projectAttributesTemp = this.projectAttributesTemp &&
      this.projectAttributesTemp.filter(
        (item) => item.attrCheckBox === true && item.attrValue !== ''
      );
    this.projectAttributesTemp && this.projectAttributesTemp.forEach((element) => {
      delete element.attrCheckBox;
    });
    this.projectAttributesTemp && this.initProjectForm.value.projectPositions.forEach((element: {jobId:number}) => {
      element.jobId = parseInt(String(element.jobId), 10);
    });
    if (this.projectAttributesTemp && this.projectAttributesTemp.length === 0) {
      this.projectAttributesTemp = null;
    }
  }

  validateForm() {
    this.projectPositions.value &&
      this.projectPositions.value.forEach((item:{jobId:string}, index:number) => {
        if (!item.jobId) {
          this.projectPositions.controls[index]?.get('controls')?.get('jobId')?.setErrors({ required: true });
        }
      });
    this.projectAttributes?.value &&
      this.projectAttributes.value.forEach((item:{attrCheckBox?:boolean, attrValue:string}, index:number) => {
        if (item.attrCheckBox && !item.attrValue) {
          this.projectPositions.controls[index]?.get('controls')?.get('attrValue')?.setErrors({ required: true });
        }
      });
    if (this.initProjectForm.value.organizations.length <= 0) {
      this.errorUnit = true;
    }
  }

  validateRequest(type: string) {
    if (type === 'SAVE') {
      if (this.id && this.checkRequestApprove) {
        return;
      } else if (!this.id) {
        this.initProjectForm.controls['flagStatus'].setValue(this.listFlagStatus.NEW);
      }
    }
    if (type === 'SAVE_AND_SEND_APPROVE') {
      this.initProjectForm.controls['flagStatus'].setValue(this.listFlagStatus.PENDING);
    }
    if (this.id) {
      this.initProjectForm.value.prjId = this.id;
    }
    this.transformBeforeSave();
    this.validateForm();

    let checkDate = true;
    if (this.fromDate.value && this.toDate.value && this.compareDate(new Date(this.fromDate.value), new Date(this.toDate.value)) >= 0) {
      this.errorDate = 'Ngày hiệu lực phải nhỏ hơn ngày hết hiệu lực';
      checkDate = false;
    } else {
      checkDate = true;
      this.errorDate = '';
    }

    if (!this.initProjectForm.valid || !checkDate) {
      this.setFormControlDirty();
    } else {
      this.id ? this.editProject() : this.createProject();
    }
  }

  setFormControlDirty() {
    Object.values(this.initProjectForm.controls).forEach((control) => {
      if (control.invalid) {
        control.markAsDirty();
        control.updateValueAndValidity({ onlySelf: true });
      }
    });
  }

  public compareDate(date1: Date, date2: Date): number | boolean {
    let result: number | boolean = false;
    date1.setHours(0);
    date1.setMinutes(0);
    date1.setSeconds(0);
    date1.setMilliseconds(0);
    date2.setHours(0);
    date2.setMinutes(0);
    date2.setSeconds(0);
    date2.setMilliseconds(0);
    if (moment(date1).isAfter(date2)) {
      result = 1;
    }
    if (moment(date1).isSame(date2)) {
      result = 0;
    }
    if (moment(date1).isBefore(date2)) {
      result = -1;
    }
    return result;
  }

  createProject() {
    const request = new FormData();
    const dataCreate = JSON.parse(JSON.stringify(this.initProjectForm.value));
    dataCreate.projectAttributes = this.projectAttributesTemp;
    request.append(
      'form',
      new Blob([JSON.stringify(dataCreate)], {
        type: 'application/json',
      })
    );
    request.append('sapFile', this.initProjectForm.controls['sapFile'].value);
    this.isLoading = true;
    this.projectService.createProject(request).subscribe(
      (data) => {
        this.isLoading = false;
        this.toastrCustom.success('Thêm mới thành công');
        this.router.navigateByUrl('/organization/project');
      },
      (error) => {
        this.isLoading = false;
        this.toastrCustom.error(error.error.description);
      }
    );
  }

  editProject() {
    const request = new FormData();
    const dataEdit = JSON.parse(JSON.stringify(this.initProjectForm.value));
    dataEdit.projectAttributes = this.projectAttributesTemp;
    request.append(
      'form',
      new Blob([JSON.stringify(dataEdit)], {
        type: 'application/json',
      })
    );
    if (this.initProjectForm.controls['sapFile'].value) {
      request.append('sapFile', this.initProjectForm.controls['sapFile'].value);
    }
    this.isLoading = true;
    this.projectService.editProject(request).subscribe(
      (data) => {
        this.isLoading = false;
        this.toastrCustom.success('Sửa thành công');
        this.router.navigateByUrl('/organization/project');
      },
      (error) => {
        this.isLoading = false;
        this.toastrCustom.error(error.error.description);
      }
    );
  }

  handleFileInput(files:any) {
    files = files.target.files
    this.onCheckRequestApprove();
    this.initProjectForm.controls['sapFile'].setValue(files[0]);
    this.fileName = files[0].name;
    this.initProjectForm.controls['fileName'].setValue(this.fileName);
    const fileSize = files[0].size;
    if (Math.round(fileSize / 1024 / 1024) > 3) {
      this.errorMessageMaxFileSize = 'Khồng được upload file có dung lượng lớn hơn 3MB';
    } else {
      this.errorMessageMaxFileSize = '';
    }
  }

  removeChip() {
    this.onCheckRequestApprove();
    this.initProjectForm.controls['file'].setValue(null);
    this.initProjectForm.controls['fileName'].setValue(null);
    this.initProjectForm.controls['sapFile'].setValue(null);
    this.fileName = '';
  }

  get projectPositions():FormArray {
    return this.initProjectForm.get('projectPositions') as FormArray;
  }

  get projectAttributes():FormArray | null {
    if (this.initProjectForm.get('projectAttributes')) {
      return this.initProjectForm.get('projectAttributes') as FormArray;
    }
    return null;
  }

  get fromDate() {
    return this.initProjectForm.get('fromDate') as FormArray;
  }

  get toDate() {
    return this.initProjectForm.get('toDate') as FormArray;
  }

  addJob() {
    this.projectPositions.push(
      this.formBuilder.group({
        jobId: [''],
        headCount: [''],
      })
    );
    if (this.projectPositions.length === 1) {
      this.hideMinus = true;
    } else {
      this.hideMinus = false;
    }
  }

  removeJob(index:number) {
    if (this.projectPositions.length > 1) {
      this.projectPositions.removeAt(index);
    }
    if (this.projectPositions.length === 1) {
      this.hideMinus = true;
    } else {
      this.hideMinus = false;
    }
  }

  initTargetForm() {
    this.listCriteria.forEach((element) => {
      const temp = this.listProjectAttributesSelected.find(m => m.attrCode === element.lvaValue);
      this.projectAttributes?.push(
        this.formBuilder.group({
          criteria: element.lvaMean,
          attrValue: [temp ? temp.attrValue : null],
          attrCode: element.lvaValue,
          attrCheckBox: [temp ? true : false],
        })
      );
    });
    setTimeout(() => {
      this.checkLoaded = true;
      this.checkRequestApprove = false;
    }, 1000);
  }


  onChangeUnit(event:string) {
    this.onCheckRequestApprove();
    if (this.multipleValue.length <= 0 && this.checkLoaded) {
      this.errorUnit = true;
    } else {
      this.errorUnit = false;
    }
  }

  onCheckRequestApprove() {
    this.checkRequestApprove = true;
  }

  onChangeAttributes(event:string, formGroup: AbstractControl) {
    if (this.checkLoaded) {
      formGroup.get('controls')?.get('attrCheckBox')?.setValue(true);
      this.onCheckRequestApprove();
    }
  }

  downloadFile() {
    if (this.detailProject && this.detailProject?.document.fileName === this.fileName) {
      this.isLoading = true;
      this.dataService.downloadFile(this.detailProject?.document.docId).subscribe((data: any) => {
        const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.pdf' });
        this.file = new File([blob], String(this.detailProject?.document.fileName ? this.detailProject?.document.fileName : ''));
        this.onFileSelected();
        this.isLoading = false;
      }, (err) => {
        this.isLoading = false;
      });
    } else {
      const blob = new Blob([this.initProjectForm.controls['sapFile'].value], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.pdf' });
      this.file = new File([blob], String(this.detailProject?.document.fileName ? this.detailProject?.document.fileName: ''));
      this.onFileSelected();
    }
  }

  onFileSelected() {
    const reader = new FileReader();
    reader.onload = (e: any) => {
      this.pdfSrc = e.target.result;
    };
    if(this.file) {
      reader.readAsArrayBuffer(this.file);
    }
    this.isVisible = true;
  }
  handleCancel(): void {
    this.isVisible = false;
  }

}
