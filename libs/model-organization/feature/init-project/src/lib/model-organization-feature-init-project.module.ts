import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {InitProjectComponent} from "./init-project/init-project.component";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NzGridModule} from "ng-zorro-antd/grid";
import {NzFormModule} from "ng-zorro-antd/form";
import {NzDatePickerModule} from "ng-zorro-antd/date-picker";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {NzModalModule} from "ng-zorro-antd/modal";
import {PdfViewerModule} from "ng2-pdf-viewer";
import {NzTableModule} from "ng-zorro-antd/table";
import {SharedPipesConvertArrayStringModule} from "@hcm-mfe/shared/pipes/convert-array-string";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import {RouterModule} from "@angular/router";
import {NzCardModule} from "ng-zorro-antd/card";
import {MatChipsModule} from "@angular/material/chips";
import {SharedUiMbSelectCheckAbleModule} from "@hcm-mfe/shared/ui/mb-select-check-able";
import {TranslateModule} from "@ngx-translate/core";
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";
import {NzInputModule} from "ng-zorro-antd/input";
import { SharedDirectivesAutofocusModule } from '@hcm-mfe/shared/directives/autofocus';

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, SharedUiMbButtonModule, ReactiveFormsModule, NzGridModule, NzFormModule,
    NzDatePickerModule, SharedUiMbInputTextModule, TranslateModule, SharedUiMbSelectModule, NzModalModule,
    PdfViewerModule, FormsModule, NzTableModule, SharedPipesConvertArrayStringModule, NzPaginationModule, RouterModule.forChild([
      {
        path: '',
        component: InitProjectComponent
      }]), NzCardModule, MatChipsModule, SharedUiMbSelectCheckAbleModule, NzCheckboxModule, NzInputModule, SharedDirectivesAutofocusModule],
  declarations: [InitProjectComponent],
  exports: [InitProjectComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ModelOrganizationFeatureInitProjectModule {}
