import { Component, Injector, OnInit } from '@angular/core';
import * as FileSaver from 'file-saver';
import { NzResizeEvent } from 'ng-zorro-antd/resizable';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {functionUri, userConfig} from "@hcm-mfe/shared/common/constants";
import { FunctionCode } from '@hcm-mfe/shared/common/enums';
import {DataService, OrganizationService} from "@hcm-mfe/model-organization/data-access/services";
import {ContentOrg, InitScreenSearch} from "@hcm-mfe/model-organization/data-access/models";
import {FormModalShowTreeUnitComponent} from "@hcm-mfe/shared/ui/form-modal-show-tree-unit";
import {renderFlagStatus} from "../../../../../helper/common.function";

@Component({
  selector: 'app-init-screen',
  templateUrl: './init-screen.component.html',
  styleUrls: ['./init-screen.component.scss'],
})
export class InitScreenComponent extends BaseComponent implements OnInit {
  colMatDraw: number;
  id: number;
  orgName: string | undefined;
  params = {
    id: '',
    page: 0,
    size: userConfig.pageSize,
  };
  isVisible = false;
  listType = [
    {
      value: 'KT',
      label: 'Tạo mới',
    },
    {
      value: 'DC',
      label: 'Điều chỉnh',
    },
    {
      value: 'HC',
      label: 'Hiệu chỉnh',
    },
  ];
  listStatus = [
    {
      value: '0',
      label: 'Không hoạt động',
    },
    {
      value: '1',
      label: 'Đang hoạt động',
    },
    {
      value: '2',
      label: 'Chờ duyệt',
    },
    {
      value: '3',
      label: 'Từ chối',
    },
    {
      value: '4',
      label: 'Phê duyệt',
    },
    {
      value: '6',
      label: 'Xác nhận',
    }
  ];
  listSettingPaginationCustom = {
    size: 0,
    total: 0,
    pageIndex: 0,
    numberOfElements: 0,
  };

  objectSearch: InitScreenSearch = {
    orgId: '',
    flagStatus: '',
    inputType: '',
    page: 0,
    size: userConfig.pageSize,
  };

  dataTable: ContentOrg[] = [];

  renderFlagStatus = renderFlagStatus;

  constructor(
    injector: Injector,
    readonly dataService: DataService,
    readonly orgService: OrganizationService
  ) {
    super(injector);
    this.colMatDraw = 5;
    this.id = -1;
    this.isLoading = true;
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ORGANIZATION_MANGEMENT}`);
    if (!this.objFunction.view) {
      this.router.navigateByUrl(functionUri.access_denied);
    }
  }


  ngOnInit(): void {
    this.getListOrganization();
  }

  goBackPage() {
    this.location.back();
  }

  getListOrganization(): void {
    this.orgService.getOrganizationList(this.objectSearch).subscribe(
      (data) => {
        if (data.data.content.length > 0) {
          this.dataTable = data.data.content;
          this.listSettingPaginationCustom.size = data.data.size;
          this.listSettingPaginationCustom.total = data.data.totalElements;
          this.listSettingPaginationCustom.pageIndex = data.data.pageable.pageNumber;
          this.listSettingPaginationCustom.numberOfElements = data.data.numberOfElements;
        } else {
          this.dataTable = [];
          this.listSettingPaginationCustom.size = data.data.size;
          this.listSettingPaginationCustom.total = data.data.totalElements;
          this.listSettingPaginationCustom.pageIndex = data.data.pageable.pageNumber;
          this.listSettingPaginationCustom.numberOfElements = data.data.numberOfElements;
        }
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }
  getListTemplateBasePagination(value:number) {
    this.isLoading = true;
    this.objectSearch.page = value - 1;
    this.getListOrganization();
  }

  selectStatus(value:string) {
    if (value) {
      this.objectSearch.flagStatus = value;
    } else {
      this.objectSearch.flagStatus = null;
    }
  }

  selectType(value:string) {
    if (value) {
      this.objectSearch.inputType = value;
    } else {
      this.objectSearch.inputType = null;
    }
  }
  onResizeMatDraw({ col }: NzResizeEvent): void {
    cancelAnimationFrame(this.id);
    this.id = requestAnimationFrame(() => {
      if(col) {
        this.colMatDraw = col;
      }
    });
  }
  searchOrganization() {
    this.isLoading = true;
    this.objectSearch.page = 0;
    if (this.objectSearch.inputType === null) {
      this.objectSearch.inputType = '';
    }
    if (this.objectSearch.flagStatus === null) {
      this.objectSearch.flagStatus = '';
    }
    this.getListOrganization();
  }

  showModalOrg(event:boolean): void {
    const modal = this.modal.create({
      nzWidth: '85vw',
      nzTitle: '',
      nzContent: FormModalShowTreeUnitComponent,
      nzFooter: null,
    });
    modal.afterClose.subscribe((result) => {
      if (result?.orgName) {
        this.orgName = result?.orgName;
        this.objectSearch.orgId = result?.orgId;
      }
    });
  }

  renderType(value:string) {
    if (value === 'KT') {
      return 'Tạo mới';
    } else if (value === 'DC') {
      return 'Điều chỉnh';
    } else if (value === 'HC') {
      return 'Hiệu chỉnh';
    }
    return null;
  }

  deleteOrg(id:number) {
    this.deletePopup.showModal(() => {
      this.isLoading = true;
      this.orgService.deleteOrg(id).subscribe(
        (data) => {
          this.toastrCustom.success(this.translate.instant('common.notification.deleteSuccess'));
          this.params.page = 0;
          this.getListOrganization();
        },
        (error) => {
          this.toastrCustom.error(this.translate.instant('common.notification.deleteError'));
        }
      );
    });
  }
  clearOrgInput() {
    this.orgName = '';
    this.objectSearch.orgId = '';
  }

  exportExcel() {
    this.orgService.exportExcelOrg(this.objectSearch).subscribe((data: any) => {
      const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      FileSaver.saveAs(blob, 'Danh sach mo hinh to chuc.xls');
    });
  }


  redirectToDetail(id:number): void {
    localStorage.setItem('selectedId', String(id));
    this.router.navigateByUrl(`/organization/init/info`);
  }

  clickInit(): void {
    this.router.navigateByUrl('organization/init/mangement');
    localStorage.setItem('viewInit', 'true');
  }
  redrirectTo(data:ContentOrg, isView:boolean, isViewEdit?:boolean) {
    if (isView) {
      localStorage.setItem('viewDetail', 'true');
    }
    localStorage.setItem('org_selected', JSON.stringify(data));
    if (data.inputType === 'DC') {
      this.router.navigateByUrl(`/organization/init/correction/DC`);
    } else if (data.inputType === 'HC') {
      this.router.navigateByUrl(`/organization/init/correction/HC`);
    } else {
      if (isViewEdit) {
        this.redrirectToEditOrg(data);
        return;
      }
      this.setDataAndRedirect(data);
    }
  }

  redrirectToEditOrg(data:ContentOrg) {
    localStorage.setItem('viewEdit', 'true');
    this.setDataAndRedirect(data);
  }

  setDataAndRedirect(data:ContentOrg) {
    this.router.navigateByUrl('organization/init/mangement');
    localStorage.setItem('org_selected', JSON.stringify(data));
  }

  override triggerSearchEvent(): void {
      this.searchOrganization()
  }
}
