import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {InitScreenComponent} from "./components/init-screen.component";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzResizableModule} from "ng-zorro-antd/resizable";
import {NzCardModule} from "ng-zorro-antd/card";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
import {NzTagModule} from "ng-zorro-antd/tag";
import {MatMenuModule} from "@angular/material/menu";
import {MatIconModule} from "@angular/material/icon";
import {MatDividerModule} from "@angular/material/divider";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import {NzGridModule} from "ng-zorro-antd/grid";
import {FormsModule} from "@angular/forms";
import {SharedUiMbTreeModule} from "@hcm-mfe/shared/ui/mb-tree";
import {RouterModule} from "@angular/router";
import {MatButtonModule} from "@angular/material/button";


@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, SharedUiMbButtonModule, SharedUiMbTreeModule, NzGridModule, RouterModule.forChild([
    {
      path: '',
      component: InitScreenComponent,
    }]), MatButtonModule,
    NzResizableModule, NzCardModule, SharedUiMbInputTextModule, TranslateModule, SharedUiMbSelectModule, NzTableModule,
    NzToolTipModule, NzTagModule, MatMenuModule, MatIconModule, MatDividerModule, NzPaginationModule, NzGridModule, FormsModule],
  declarations: [InitScreenComponent],
  exports: [InitScreenComponent]
})
export class ModelOrganizationFeatureOrganizationManagementModule {}
