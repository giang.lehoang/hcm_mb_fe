import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { TranslateModule } from '@ngx-translate/core';
import { NzCardModule } from 'ng-zorro-antd/card';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { RouterModule } from '@angular/router';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { SharedDirectivesPaddingDirectivesModule } from '@hcm-mfe/shared/directives/padding-directives';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { InfoOrganizationComponent } from './info-organization/info-organization.component';
import { ModelOrganizationUiDeputySearchCustomModalModule } from '@hcm-mfe/model-organization/ui/deputy-search-custom-modal';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { SharedUiMbDatePickerModule } from '@hcm-mfe/shared/ui/mb-date-picker';

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, SharedUiMbButtonModule, TranslateModule, NzCardModule, ReactiveFormsModule, FormsModule,
    NzGridModule, SharedUiMbSelectModule, FormsModule, SharedUiMbInputTextModule, NzTableModule, NzPaginationModule,
    RouterModule.forChild([
      {
        path: '',
        component: InfoOrganizationComponent
      }]),
    NzToolTipModule, NzModalModule, NzTagModule, NzIconModule, SharedDirectivesPaddingDirectivesModule,
    NzCheckboxModule, ModelOrganizationUiDeputySearchCustomModalModule, NzDividerModule,
    NzFormModule, NzInputModule, SharedUiMbDatePickerModule
  ],
  declarations: [InfoOrganizationComponent],
  exports: [InfoOrganizationComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ModelOrganizationFeatureInfoOrganizationModule {}
