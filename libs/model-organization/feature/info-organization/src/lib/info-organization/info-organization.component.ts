import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { InfoOrganization } from '@hcm-mfe/model-organization/data-access/models';
import {
  OrganizationalModelTemplateService,
  UnitApprovedService
} from '@hcm-mfe/model-organization/data-access/services';
import { DomSanitizer } from '@angular/platform-browser';
import { FunctionCode } from '@hcm-mfe/shared/common/enums';
import { CustomValidators } from '@hcm-mfe/shared/common/validators';
import * as moment from 'moment';
import { arrayBufferToBase64 } from '@hcm-mfe/shared/common/utils';

@Component({
  selector: 'app-info-organization',
  templateUrl: './info-organization.component.html',
  styleUrls: ['./info-organization.component.scss'],
})
export class InfoOrganizationComponent extends BaseComponent implements OnInit {
  imagePath:any;
  imageValid = false;
  dateFormat = 'dd/MM/yyyy';
  orgId  ='';
  fileName:any;
  isVisible = false;
  infoOrganization: InfoOrganization|undefined;
  listSubregion = [];
  infoOrgForm: FormGroup;
  loading = false;
  constructor(
    injector: Injector,
    readonly orgService: OrganizationalModelTemplateService,
    readonly _sanitizer: DomSanitizer,
    readonly unitApproveService: UnitApprovedService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ORGANIZATION_INFO}`);
    this.infoOrgForm=  this.infoOrgForm = this.fb.group({
      address: [''],
      telephone: [null],
      fax: [null],
      t24Code: [null],
      subRegion: [null],
      initialDate: [''],
      surrogateId: [null],
      fullName: [''],
      unitLogo: [
        null,
        [
          CustomValidators.imageSizeValidator(300000),
          CustomValidators.imageExtensionValidator(['image/jpeg', 'image/png']),
        ],
      ],
      image: [null, []],
      orgAbbreviation: [''],
      orgFullName: [''],
    });
  }

  ngOnInit(): void {
    this.initForm();
    this.loading = false;
    this.imageValid = false;
    this.fileName = '';
    this.orgId = JSON.parse(localStorage.getItem('selectedId'));
    if (!this.orgId) {
      this.router.navigateByUrl('/');
    }
    this.getInfoModelOrganization();
    this.orgService.getSubRegion(undefined, 1).subscribe((data: any) => {
      this.listSubregion = data.data;
    });
  }

  override ngOnDestroy() {
    localStorage.removeItem('selectedId');
  }

  initForm(): void {
    this.infoOrgForm = this.fb.group({
      address: [''],
      telephone: [null],
      fax: [null],
      t24Code: [null],
      subRegion: [null],
      initialDate: [''],
      surrogateId: [null],
      fullName: [''],
      unitLogo: [
        null,
        [
          CustomValidators.imageSizeValidator(300000),
          CustomValidators.imageExtensionValidator(['image/jpeg', 'image/png']),
        ],
      ],
      image: [null, []],
      orgAbbreviation: [''],
      orgFullName: [''],
    });
  }

  redirectListInfo(): void {
    this.router.navigateByUrl('/organization/init/list-info');
  }
  submitForm(): void {
    if (this.infoOrgForm.invalid || this.imageValid) {
      Object.values(this.infoOrgForm.controls).forEach((control) => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
      return;
    }

    const data = {
      orgId: this.infoOrganization?.orgId,
      address: this.infoOrgForm.value.address,
      telephone: this.infoOrgForm.value.telephone,
      fax: this.infoOrgForm.value.fax,
      t24Code: this.infoOrgForm.value.t24Code,
      subRegion: this.infoOrgForm.value.subRegion,
      initialDate: this.infoOrgForm.value.initialDate
        ? moment(this.infoOrgForm.value.initialDate).format('YYYY-MM-DD')
        : '',
      surrogateId: this.infoOrgForm.value.surrogateId,
      logoRef: this.infoOrgForm.value.image ? this.infoOrgForm.value.image.docId : '',
      orgAbbreviation: this.infoOrgForm.value.orgAbbreviation,
      orgFullName: this.infoOrgForm.value.orgFullName,
    };

    const formData: FormData = new FormData();
    formData.append(
      'request',
      new Blob([JSON.stringify(data)], {
        type: 'application/json',
      })
    );

    //edit file
    // @ts-ignore
    if (this.infoOrgForm.get('unitLogo').value) {
      formData.append('sapFile', this.infoOrgForm.value.unitLogo, this.infoOrgForm.value.unitLogo.name);
    }
    this.loading = true;
    this.orgService.editInfoModelOrganization(formData).subscribe((response): void => {
      this.loading = false;
      this.router.navigate(['organization/init/init-screen']).then();
      this.toastrCustom.success('Lưu thành công');
    }, error => {
      this.toastrCustom.error(error.error.message)
    });
  }
  getInfoModelOrganization(): void {
    this.loading = true;
    this.orgService.getInfoModelOrganization(this.orgId).subscribe((data: any) => {
      this.infoOrganization = data.data;
      debugger
      this.infoOrgForm.patchValue(data.data);
      // @ts-ignore
      this.infoOrgForm.get('initialDate').setValue(data.data.fromDate ? data.data.fromDate : new Date());
      this.loading = false;
      if (!this.infoOrganization?.docId) {
        return;
      }
      this.downloadLogo(this.infoOrganization?.docId, this.infoOrganization.fileName);
    }, () => {
      this.loading = false;
    });
  }

  downloadLogo(logoRef:any, fileName:string): void {
    this.unitApproveService.downloadFile(logoRef).subscribe((data) => {
      this.imagePath = this._sanitizer.bypassSecurityTrustResourceUrl(
        'data:image/jpg;base64,' + arrayBufferToBase64(data)
      );
      this.fileName = fileName;
    });
  }


  showModal(event: any): void {
    this.isVisible = true;
  }

  getDeputyData(event:any): void {
    // @ts-ignore
    this.infoOrgForm.get('surrogateId').setValue(event.employeeId);
    // @ts-ignore
    this.infoOrgForm.get('fullName').setValue(event.fullName);
    this.isVisible = false;
  }

  async readImageUrl(event: Event): Promise<void> {
    this.imagePath = null;
    const getBase64 = (file: File): Promise<string | ArrayBuffer | null> =>
      new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = (error) => reject(error);
      });
    const allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
    const target:any = event.target as HTMLInputElement;
    this.fileName = target.files[0].name;
    // @ts-ignore
    this.infoOrgForm.get('unitLogo').setValue(target.files[0], { emitModelToViewChange: false });
    if (allowedExtensions.exec(target.value)) {
      this.imageValid = false;
      this.imagePath = await getBase64(target.files[0]);
    } else {
      this.imageValid = true;
    }
  }

  uploadImage(file: File): void {
    const formData = new FormData();
    formData.append('files', file);
    formData.append('objId', this.orgId);
    formData.append('dgrCode', 'LOGO_ORG');
    formData.append('resourceId', '');
    this.orgService.uploadLogo(formData).subscribe((res: any): void => {
      // @ts-ignore
      this.infoOrgForm.get('image').setValue(res.data[0]);
      this.orgService.downloadLogo(res.data[0].docId).subscribe((data) => {
        this.imagePath = this._sanitizer.bypassSecurityTrustResourceUrl(
          'data:image/jpg;base64,' + arrayBufferToBase64(data)
        );
      });
    });
  }
  handleCancel(): void {
    this.isVisible = false;
  }
  deleteFile(): void {
    this.fileName = null;
    this.imageValid = false;
    // @ts-ignore
    this.infoOrgForm.get('image').setValue(null);
    // @ts-ignore
    this.infoOrgForm.get('unitLogo').setValue(null);
    this.imagePath = null
  }
  goBackPage(): void {
    this.location.back();
  }
}
