import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JdApprovedComponent } from './jd-approved.component';

describe('JdApprovedComponent', () => {
  let component: JdApprovedComponent;
  let fixture: ComponentFixture<JdApprovedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JdApprovedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JdApprovedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
