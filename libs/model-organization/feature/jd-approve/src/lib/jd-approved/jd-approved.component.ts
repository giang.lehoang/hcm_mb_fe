import { Component, Injector, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';
import * as FileSaver from 'file-saver';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {
  Attributes,
  JDsModel, Job, listStatus,
  LookupParam,
  LookupValue, Pagination, ParamsJDSearch, ResponseEntity, ResponseEntityData, SelectObject
} from "@hcm-mfe/model-organization/data-access/models";
import {FormModalShowTreeUnitComponent} from "@hcm-mfe/shared/ui/form-modal-show-tree-unit";
import { maxInt32} from "@hcm-mfe/shared/common/constants";
import { FunctionCode } from '@hcm-mfe/shared/common/enums';
import {JDService} from "../../../../../data-access/services/src/lib/jd.service";
import {DataService} from "@hcm-mfe/model-organization/data-access/services";

@Component({
  selector: 'app-jd-approved',
  templateUrl: './jd-approved.component.html',
  styleUrls: ['./jd-approved.component.scss']
})
export class JdApprovedComponent extends BaseComponent implements OnInit {

  orgName: string;
  searchParams: ParamsJDSearch = new ParamsJDSearch();
  params: ParamsJDSearch = new ParamsJDSearch();
  listJob: Job[] = [];
  listAttribute: Attributes[] = [];
  lookupParram: LookupParam = new LookupParam();
  listJobGroup: LookupValue[] = [];
  listCareerGroup: LookupValue[] = [];
  listStatus:SelectObject<number,string>[];
  dataTable: JDsModel[] = [];
  pagination: Pagination = new Pagination(maxInt32);
  isLoadingPage: boolean;
  isValid: boolean;
  isCheckAll: boolean;
  constructor(
    injector: Injector,
    private readonly jdServicce: JDService,
    private readonly dataService: DataService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(
      `FUNCTION_${FunctionCode.JOB_DESCRIPTION_APPROVE}`
    );
    this.orgName = '';
    this.isCheckAll = false;
    this.isLoadingPage = false;
    this.isValid = false;
    this.listStatus = listStatus.filter(item => item.id !== 6);
  }

  ngOnInit(): void {
    this.fetchData(true);
  }

  fetchData(init: boolean) {
    this.isLoadingPage = true;
    const request = [];
    if (init) {
      request.push(this.dataService.getLookupValue(this.lookupParram.lookupCode));
      this.lookupParram.lookupCode = 'NHOM_NGHE_NGHIEP';
      request.push(this.dataService.getLookupValue(this.lookupParram.lookupCode));
    }
    const params = this.checkParams();
    request.push(this.jdServicce.getListApprove(params));

    forkJoin(request).subscribe(
      (data: Array<any>) => {
        let jds;
        if (init) {
          this.listJobGroup = (<ResponseEntity<Array<LookupValue>>>data[0]).data;
          this.listCareerGroup = (<ResponseEntity<Array<LookupValue>>>data[1]).data;
          jds = (<ResponseEntityData<Array<JDsModel>>>data[2]).data;
        } else {
          jds = (<ResponseEntityData<Array<JDsModel>>>data[0]).data;
        }
        this.dataTable = jds.content;
        this.pagination.totalElements = jds.totalElements;
        this.pagination.totalPages = jds.totalPages;
        this.pagination.numberOfElements = jds.numberOfElements;
        this.pagination.currentPage = jds.pageable.pageNumber;
        this.selectAll(false);
        this.isLoadingPage = false;
        this.searchParams.description = '';
      },
      (error) => {
        this.isLoadingPage = false;
        this.dataTable = [];
        this.toastrCustom.error(error.message);
      }
    );
  }

  showModalOrg(event:string): void {
    const modal = this.modal.create({
      nzWidth: '85vw',
      nzTitle: '',
      nzContent: FormModalShowTreeUnitComponent,
      nzFooter: null,
    });
    modal.afterClose.subscribe((result) => {
      if (result?.orgName) {
        this.orgName = result?.orgName;
        this.searchParams.orgId = result?.orgId;
        this.getJob();
        this.getAttributeJds();
      }
    });
  }
  clearOrgInput() {
    this.orgName = '';
    this.searchParams.orgId = null;
  }

  getJob() {
    if(this.searchParams.orgId) this.jdServicce.getJobByOrgId(this.searchParams.orgId).subscribe((data: ResponseEntity<Array<Job>>) => {
      this.listJob = data.data;
    },
      (error) => {
      this.toastrCustom.error(error.message);
    })
  }

  getAttributeJds() {
    if(this.searchParams.orgId)
    this.jdServicce.getAttribute(this.searchParams.orgId).subscribe((data: ResponseEntity<Array<Attributes>>) => {
      this.listAttribute = data.data;
    },
      (error) => {
      this.toastrCustom.error(error.message);
    })
  }

  onSearch() {
    this.fetchData(false);
  }
  changePage(value: number) {
    this.searchParams.page = value - 1;
    this.fetchData(false);
  }
  checkParams() {
    const paramsTmp = { ...this.searchParams };
    if (!paramsTmp.flagStatus && paramsTmp.flagStatus !== 0) {
      delete paramsTmp.flagStatus
    }
    if (!paramsTmp.orgId) {
      delete paramsTmp.orgId
    }
    if (!paramsTmp.jobId) {
      delete paramsTmp.jobId;
    }
    if (!paramsTmp.lvaIdJob) {
      delete paramsTmp.lvaIdJob;
    }
    if (!paramsTmp.lvaIdProfession) {
      delete paramsTmp.lvaIdProfession
    }
    if (!paramsTmp.orgaId) {
      delete paramsTmp.orgaId
    }
    delete paramsTmp.page;
    return paramsTmp;
  }

  onClickRow(data: JDsModel) {
    localStorage.setItem('jdApprove', JSON.stringify(data));
    this.router.navigate(['/organization/job-description/jd-approve'], { state: { data: data } });
  }
  selectAll(isChecked: boolean) {
    this.dataTable.forEach(item => item.isChecked = isChecked);
  }

  onItemChecked(item:JDsModel, isChecked: boolean): void {
    item.isChecked = isChecked;
    this.isCheckAll = this.dataTable.length > 0 && this.dataTable.every(data => data.isChecked);
  }
  trackByFn(index: number, item: JDsModel) {
    return item.psdId;
  }
  reject() {
    if (this.dataTable.filter(item => item.isChecked).length === 0) {
      this.toastrCustom.error(this.translate.instant("modelOrganization.approvedStaff.notification.noSelected"));
      return;
    } else if (!this.searchParams.description) {
      this.isValid = true;
      return;
    } else {
      this.isValid = false;
    }
    const params = {
      jdId: this.dataTable.filter(item => item.isChecked).map(value => value.jdId),
      description: this.searchParams.description ? this.searchParams.description.trim() : ''
    }
    this.jdServicce.rejectJDs(params).subscribe(
      (data) => {
        this.toastrCustom.success();
        this.onSearch();
      },
      (error) => {
        this.toastrCustom.error(error.error.message);
      }
    );
  }

  approve() {
    if (this.dataTable.filter(item => item.isChecked).length === 0) {
      this.toastrCustom.error(this.translate.instant("modelOrganization.approvedStaff.notification.noSelected"));
      return;
    }
    const params = {
      jdId: this.dataTable.filter(item => item.isChecked).map(value => value.jdId),
      description: this.searchParams.description ? this.searchParams.description.trim() : ''
    }
    this.jdServicce.approveJDs(params).subscribe(
      (data) => {
        this.toastrCustom.success();
        this.onSearch();
      },
      (error) => {
        this.toastrCustom.error(error.error.message);
      }
    );
  }

  approvedAll() {
    const params = this.checkParams();
    this.jdServicce.approvedAll(params).subscribe(
      (data) => {
        this.toastrCustom.success();
        this.onSearch();
      },
      (error) => {
        this.toastrCustom.error(error.error.message);
      }
    );
  }

  exportExcel() {
    const params = this.checkParams()
    this.jdServicce.exportApprove(params).subscribe(
      (data) => {
        const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
        FileSaver.saveAs(blob, 'Template.xlsx');
      },
      (error) => {
        this.toastrCustom.error(error.error.message);
      }
    );
  }

  showPopupApproved() {
    this.deletePopup.showApprove(() => {
      this.approvedAll();
    })
  }
  override triggerSearchEvent(): void {
    this.onSearch();
  }
}
