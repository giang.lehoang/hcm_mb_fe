import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {JdApprovedComponent} from "./jd-approved/jd-approved.component";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {NzGridModule} from "ng-zorro-antd/grid";
import {FormsModule} from "@angular/forms";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
import {RouterModule} from "@angular/router";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {TranslateModule} from "@ngx-translate/core";
import {NzIconModule} from "ng-zorro-antd/icon";

@NgModule({
  imports: [CommonModule, SharedUiMbButtonModule, SharedUiMbInputTextModule, NzGridModule, TranslateModule, NzIconModule,
    FormsModule, SharedUiMbSelectModule, NzTableModule, NzToolTipModule, RouterModule.forChild([
      {
        path: '',
        component: JdApprovedComponent,
      }]), SharedUiLoadingModule],
  declarations: [JdApprovedComponent],
  exports: [JdApprovedComponent]
})
export class ModelOrganizationFeatureJdApproveModule {}
