import {RouterModule} from '@angular/router';
import {FunctionCode, Scopes} from '@hcm-mfe/shared/common/enums';
import {NgModule} from '@angular/core';

const router = [
  {
    path: '',
    redirectTo: "init",
  },
  {
    path: 'staff-plan',
    data: {
      breadcrumb: 'modelOrganization.breadcrumb.staffPlan'
    },
    children: [
      {
        path: '',
        redirectTo: "plan-year",
      },
      {
        path: 'plan-year',
        children: [
          {
            path: '',
            loadChildren: () => import('@hcm-mfe/model-organization/feature/plan-year').then((m) => m.ModelOrganizationFeaturePlanYearModule),
            data: {
              code: FunctionCode.ORGANIZATION_STAFF_PLAN,
              pageName: 'modelOrganization.pageName.planYear',
              breadcrumb: 'modelOrganization.breadcrumb.planYear'
            }          },
          {
            path: 'plan-year',
            loadChildren: () => import('@hcm-mfe/model-organization/feature/plan-year').then((m) => m.ModelOrganizationFeaturePlanYearModule),
            data: {
              code: FunctionCode.ORGANIZATION_STAFF_PLAN,
              pageName: 'modelOrganization.pageName.planYear',
              breadcrumb: 'modelOrganization.breadcrumb.planYear'
            }
          },
          {
            path: 'init-staff',
            loadChildren: () => import('@hcm-mfe/model-organization/feature/init-staff-plan').then((m) => m.ModelOrganizationFeatureInitStaffPlanModule),
            data: {
              code: FunctionCode.ORGANIZATION_STAFF_PLAN_INIT,
              pageName: 'modelOrganization.pageName.initStaffPlan',
              breadcrumb: 'modelOrganization.breadcrumb.initStaffPlan'
            }
          },
          {
            path: 'adjusted-staff',
            loadChildren: () => import('@hcm-mfe/model-organization/feature/adjusted-staff').then((m) => m.ModelOrganizationFeatureAdjustedStaffModule),
            data: {
              code: FunctionCode.ORGANIZATION_STAFF_PLAN_ADJUST,
              pageName: 'modelOrganization.pageName.adjustedStaff',
              breadcrumb: 'modelOrganization.breadcrumb.adjustedStaff'
            }
          },
          {
            path: 'correction-staff',
            loadChildren: () => import('@hcm-mfe/model-organization/feature/correction-staff').then((m) => m.ModelOrganizationFeatureCorrectionStaffModule),
            data: {
              code: FunctionCode.CORRECTION_STAFF,
              pageName: 'modelOrganization.pageName.correctionStaff',
              breadcrumb: 'modelOrganization.breadcrumb.correctionStaff'
            }
          },
        ]
      },{
        path: 'confirm-staff',
        loadChildren: () => import('@hcm-mfe/model-organization/feature/confirm-staff').then((m) => m.ModelOrganizationFeatureConfirmStaffModule),
        data: {
          code: FunctionCode.CONFIRM_STAFF,
          pageName: 'modelOrganization.pageName.confirmStaff',
          breadcrumb: 'modelOrganization.breadcrumb.confirmStaff'
        }
      },
      {
        path: 'approved-staff',
        loadChildren: () => import('@hcm-mfe/model-organization/feature/approved-staff').then((m) => m.ModelOrganizationFeatureApprovedStaffModule),
        data: {
          code: FunctionCode.ORGANIZATION_STAFF_PLAN_APPROVE,
          pageName: 'modelOrganization.pageName.approvedStaff',
          breadcrumb: 'modelOrganization.breadcrumb.approvedStaff'
        }
      },
      {
        path: 'report-employee',
        loadChildren: () => import('@hcm-mfe/model-organization/feature/report-employee').then((m) => m.ModelOrganizationFeatureReportEmployeeModule),
        data: {
          code: FunctionCode.ORGANIZATION_STAFF_PLAN_APPROVE,
          pageName: 'modelOrganization.pageName.reportEmployee',
          breadcrumb: 'modelOrganization.breadcrumb.reportEmployee'
        }
      }
    ]
  },
  {
    path: 'issued',
    data: {breadcrumb: 'modelOrganization.breadcrumb.issued'},
    children: [{
      path: 'issued-list',
      loadChildren: () => import('@hcm-mfe/model-organization/feature/issued-list').then((m) => m.ModelOrganizationFeatureIssuedListModule),
      data: {
        code: FunctionCode.ORGANIZATION_ISSUED_LIST,
        scope: Scopes.VIEW,
        pageName: 'modelOrganization.pageName.issuedList',
        breadcrumb: 'modelOrganization.breadcrumb.issuedList',
      }
    },
      {
        path: 'issued-transfer',
        code: FunctionCode.ORGANIZATION_ISSUED_TRANFER,
        scope: Scopes.CREATE,
        loadChildren: () => import('@hcm-mfe/model-organization/feature/transfer-issued').then((m) => m.ModelOrganizationFeatureTransferIssuedModule),
        data: {
          pageName: 'modelOrganization.pageName.issuedTranfer',
          breadcrumb: 'modelOrganization.breadcrumb.issuedTranfer'
        }
      },
      {
        path: 'issued-history',
        data: {
          pageName: 'modelOrganization.pageName.issuedHistory',
          breadcrumb: 'modelOrganization.breadcrumb.issuedHistory',
        },
        loadChildren: () => import('@hcm-mfe/model-organization/feature/issued-history').then((m) => m.ModelOrganizationFeatureIssuedHistoryModule),
      },
    ]
  },
  {
    path: 'init',
    data: {breadcrumb: 'modelOrganization.breadcrumb.modelOrganization'},
    children:[
      {
        path: 'region',
        children: [
          {
            path: 'kv',
            loadChildren: () => import('@hcm-mfe/model-organization/feature/region-category').then((m) => m.ModelOrganizationFeatureRegionCategoryModule),
            data: {
              code: 'kv',
              pageName: 'modelOrganization.pageName.regionCategory',
              breadcrumb: 'modelOrganization.breadcrumb.regionCategory',
            },
          },
          {
            path: 'lh',
            loadChildren: () => import('@hcm-mfe/model-organization/feature/region-category').then((m) => m.ModelOrganizationFeatureRegionCategoryModule),
            data: {
              code: 'lh',
              pageName: 'modelOrganization.pageName.branchCategory',
              breadcrumb: 'modelOrganization.breadcrumb.branchCategory',
            },
          },
          {
            path: 'tv',
            loadChildren: () => import('@hcm-mfe/model-organization/feature/region-category').then((m) => m.ModelOrganizationFeatureRegionCategoryModule),
            data: {
              code: 'tv',
              pageName: 'modelOrganization.pageName.typeCategory',
              breadcrumb: 'modelOrganization.breadcrumb.typeCategory',
            },
          },
        ],
      },
      {
        path: '',
        redirectTo: 'init-screen',
      },
      {
        path: 'init-screen',
        loadChildren: () => import('@hcm-mfe/model-organization/feature/organization-management')
          .then((m) => m.ModelOrganizationFeatureOrganizationManagementModule),
        data: {
          code: FunctionCode.ORGANIZATION_MANGEMENT,
          scope: Scopes.VIEW,
          pageName: 'modelOrganization.pageName.organizationList',
          breadcrumb: 'modelOrganization.breadcrumb.organizationList',
        },
      },
      {
        path: 'mangement',
        loadChildren: () => import('@hcm-mfe/model-organization/feature/organization-init')
          .then((m) => m.ModelOrganizationFeatureOrganizationInitModule),
        data: {
          code: FunctionCode.ORGANIZATION_MANGEMENT_INIT,
          scope: Scopes.VIEW,
          pageName: 'modelOrganization.pageName.initOrganization',
          breadcrumb: 'modelOrganization.breadcrumb.initOrganization',
        },
      },
      {
        path: 'correction',
        children: [
          {
            path: 'DC',
            loadChildren: () => import('@hcm-mfe/model-organization/feature/organization-correction')
              .then((m) => m.ModelOrganizationFeatureOrganizationCorrectionModule),
            data: {
              code: FunctionCode.ORGANIZATION_ADJUSTED_ORG,
              scope: Scopes.VIEW,
              type: 'DC',
              pageName: 'modelOrganization.pageName.adjustedOrganization',
              breadcrumb: 'modelOrganization.breadcrumb.adjustedOrganization',
            },
          },
          {
            path: 'HC',
            loadChildren: () => import('@hcm-mfe/model-organization/feature/organization-correction')
              .then((m) => m.ModelOrganizationFeatureOrganizationCorrectionModule),
            data: {
              code: FunctionCode.ORGANIZATION_CORRECTION_ORG,
              scope: Scopes.VIEW,
              type: 'HC',
              pageName: 'modelOrganization.pageName.correctionOrganization',
              breadcrumb: 'modelOrganization.breadcrumb.correctionOrganization',
            },
          }
        ],
      },
        {
        path: 'unit-approve',
        children: [
          {
            path: '',
            loadChildren: () => import('@hcm-mfe/model-organization/feature/unit-approved').then((m) => m.ModelOrganizationFeatureUnitApprovedModule),
            data: {
              code: FunctionCode.ORGANIZATION_APPROVE_ORG,
              pageName: 'modelOrganization.pageName.approveOrganization',
              breadcrumb: 'modelOrganization.breadcrumb.approveOrganization'
            },
          },
          {
            path: 'detail/:id',
            loadChildren: () => import('@hcm-mfe/model-organization/feature/unit-approved-detail').then((m) => m.ModelOrganizationFeatureUnitApprovedDetailModule),
            data: {
              scope: Scopes.VIEW,
              pageName: 'modelOrganization.pageName.approveOrganizationDetail',
              breadcrumb: 'modelOrganization.breadcrumb.approveOrganizationDetail'
            },
          },
          {
            path: 'detail',
            loadChildren: () => import('@hcm-mfe/model-organization/feature/unit-approved-detail').then((m) => m.ModelOrganizationFeatureUnitApprovedDetailModule),
            data: {
              scope: Scopes.VIEW,
              pageName: 'modelOrganization.pageName.approveOrganizationDetail',
              breadcrumb: 'modelOrganization.breadcrumb.approveOrganizationDetail'
            },
          },
        ],
      },
      {
        path: 'list-info',
        children: [
          {
            path: '',
            loadChildren: () => import('@hcm-mfe/model-organization/feature/list-info-organization').then((m) => m.ModelOrganizationFeatureListInfoOrganizationModule),
            data: {
              code: FunctionCode.ORGANIZATION_INFO,
              pageName: 'modelOrganization.pageName.infoOrg',
              breadcrumb: 'modelOrganization.breadcrumb.infoOrg'
            }
          },
        ],
      },
      {
        path: 'info',
        loadChildren: () => import('@hcm-mfe/model-organization/feature/info-organization').then((m) => m.ModelOrganizationFeatureInfoOrganizationModule),
        data: {
          code: FunctionCode.ORGANIZATION_INFO,
          pageName: 'modelOrganization.pageName.infoOrg',
          breadcrumb: 'modelOrganization.breadcrumb.infoOrg'
        }
      },
      {
        path: 'sort-tree',
        loadChildren: () => import('@hcm-mfe/model-organization/feature/sort-tree')
          .then((m) => m.ModelOrganizationFeatureSortTreeModule),
        data: {
          code: FunctionCode.ORGANIZATION_INFO,
          pageName: 'modelOrganization.pageName.sortOrg',
          breadcrumb: 'modelOrganization.breadcrumb.sortOrg'
        }
      },
    ],
  },
  {
    path: 'project',
    children: [
      {
        path: '',
        loadChildren: () => import('@hcm-mfe/model-organization/feature/project-list')
          .then((m) => m.ModelOrganizationFeatureProjectListModule),
        data: {
          code: FunctionCode.MANAGE_PROJECT,
          scope: Scopes.VIEW,
          pageName: 'modelOrganization.pageName.manageProject',
          breadcrumb: 'modelOrganization.breadcrumb.manageProject'
        }
      },
      {
        path: 'list-project-approved',
        loadChildren: () => import('@hcm-mfe/model-organization/feature/project-list-approved')
          .then((m) => m.ModelOrganizationFeatureProjectListApprovedModule),
        data: {
          code: FunctionCode.APPROVE_PROJECT,
          scope: Scopes.VIEW,
          pageName: 'modelOrganization.pageName.approvedProject',
          breadcrumb: 'modelOrganization.breadcrumb.approvedProject'
        }
      },
      {
        path: 'project-list',
        loadChildren: () => import('@hcm-mfe/model-organization/feature/project-list')
          .then((m) => m.ModelOrganizationFeatureProjectListModule),
        data: {
          code: FunctionCode.MANAGE_PROJECT,
          scope: Scopes.VIEW,
          pageName: 'modelOrganization.pageName.manageProject',
          breadcrumb: 'modelOrganization.breadcrumb.manageProject'
        }
      },
      {
        path: 'init-project',
        loadChildren: () => import('@hcm-mfe/model-organization/feature/init-project')
          .then((m) => m.ModelOrganizationFeatureInitProjectModule),
        data: {
          pageName: 'modelOrganization.pageName.project',
          breadcrumb: 'modelOrganization.breadcrumb.project'
        }
      },
      {
        path: 'view-project',
        loadChildren: () => import('@hcm-mfe/model-organization/feature/view-project')
          .then((m) => m.ModelOrganizationFeatureViewProjectModule),
        data: {
          pageName: 'modelOrganization.pageName.project',
          breadcrumb: 'modelOrganization.breadcrumb.project'
        }
      },
      {
        path: 'criteria-project',
        loadChildren: () => import('@hcm-mfe/model-organization/feature/project-criteria-category')
          .then((m) => m.ModelOrganizationFeatureProjectCriteriaCategoryModule),
        data: {
          pageName: 'modelOrganization.pageName.projectCriteriaCategory',
          breadcrumb: 'modelOrganization.breadcrumb.projectCriteriaCategory'
        },
      },
      {
        path: 'project-implement',
        loadChildren: () => import('@hcm-mfe/model-organization/feature/project-implement')
          .then((m) => m.ModelOrganizationFeatureProjectImplementModule),
        data: {
          code: FunctionCode.IMPLEMENT_PROJECT,
          scope: Scopes.VIEW,
          pageName: 'modelOrganization.pageName.projectImplement',
          breadcrumb: 'modelOrganization.breadcrumb.projectImplement'
        }
      },
      {
        path: 'employee-assignment',
        loadChildren: () => import('@hcm-mfe/model-organization/feature/employee-assignment')
          .then((m) => m.ModelOrganizationFeatureEmployeeAssignmentModule),
        data: {
          pageName: 'modelOrganization.pageName.projectAssign',
          breadcrumb: 'modelOrganization.breadcrumb.projectAssign'
        }
      },
      {
        path: 'employee-assignment/view',
        loadChildren: () => import('@hcm-mfe/model-organization/feature/employee-assignment')
          .then((m) => m.ModelOrganizationFeatureEmployeeAssignmentModule),
        data: {
          pageName: 'modelOrganization.pageName.projectAssign',
          breadcrumb: 'modelOrganization.breadcrumb.projectAssign',
          mode: 'view'
        }
      },
    ],
    data: {
      pageName: 'modelOrganization.pageName.modelProject',
      breadcrumb: 'modelOrganization.breadcrumb.modelProject'
    }
  },
  {
    path: 'job-description',
    children: [
      {
        path: '',
        data: {
          pageName: 'modelOrganization.pageName.listJds',
          breadcrumb: 'modelOrganization.breadcrumb.listJds',
        },
        children: [
          {
            path:'',
            redirectTo: 'job-description',
          },
          {
            path: 'job-description',
            loadChildren: () => import('@hcm-mfe/model-organization/feature/jds')
              .then((m) => m.ModelOrganizationFeatureJdsModule),
          },
          {
            path: 'jd-init',
            data: {
              pageName: 'modelOrganization.pageName.jdInit',
              breadcrumb: 'modelOrganization.breadcrumb.jdInit',
            },
            loadChildren: () => import('@hcm-mfe/model-organization/feature/jd-init')
              .then((m) => m.ModelOrganizationFeatureJdInitModule),
          },
          {
            path: 'jd-approve',
            data: {
              pageName: 'modelOrganization.pageName.jdInit',
              breadcrumb: 'modelOrganization.breadcrumb.jdInit',
              mode: "approve"
            },
            loadChildren: () => import('@hcm-mfe/model-organization/feature/jd-init')
              .then((m) => m.ModelOrganizationFeatureJdInitModule),
          }
        ]
      },
      {
        path: 'jd-approved',
        loadChildren: () => import('@hcm-mfe/model-organization/feature/jd-approve')
          .then((m) => m.ModelOrganizationFeatureJdApproveModule),
        data: {
          pageName: 'modelOrganization.pageName.approveJDs',
          breadcrumb: 'modelOrganization.breadcrumb.approveJDs',
        },
      },
    ],
    data: {
      breadcrumb: 'modelOrganization.breadcrumb.manageJDs'
    }
  },
  {
    path: 'job',
    children: [
      {
        path: '',
        redirectTo: 'list-job',
      },
      {
        path: 'list-job',
        loadChildren: () => import('@hcm-mfe/model-organization/feature/list-job')
          .then((m) => m.ModelOrganizationFeatureListJobModule),
        data: {
          breadcrumb: 'modelOrganization.breadcrumb.listJob',
          pageName: "modelOrganization.pageName.listJob",
        },
      },
      {
        path: 'approve-assign-job',
        loadChildren: () => import('@hcm-mfe/model-organization/feature/approve-assign-job')
          .then((m) => m.ModelOrganizationFeatureApproveAssignJobModule),
        data: {
          breadcrumb: 'modelOrganization.breadcrumb.approveAssignJob',
          pageName: "modelOrganization.pageName.approveAssignJob",
        },
      },
      {
        path: 'job-assign-manage',
        loadChildren: () => import('@hcm-mfe/model-organization/feature/job-assign-manage')
          .then((m) => m.ModelOrganizationFeatureJobAssignManageModule),
        data: {
          breadcrumb: 'modelOrganization.breadcrumb.assignJob',
          pageName: "modelOrganization.pageName.assignJob",
        },
      },
      {
        path: 'attach-job',
        loadChildren: () => import('@hcm-mfe/model-organization/feature/attach-job')
          .then((m) => m.ModelOrganizationFeatureAttachJobModule),
        data: {
          breadcrumb: 'modelOrganization.breadcrumb.attachJob',
          pageName: "modelOrganization.pageName.attachJob",
        },
      },
      {
        path: 'list-position',
        loadChildren: () => import('@hcm-mfe/model-organization/feature/list-position')
          .then((m) => m.ModelOrganizationFeatureListPositionModule),
        data: {
          breadcrumb: 'modelOrganization.breadcrumb.listPosition',
          pageName: "modelOrganization.pageName.listPosition",
        },
      },
      {
        path: 'position-group',
        data: {
          breadcrumb: 'modelOrganization.breadcrumb.positionGroup',
          pageName: "modelOrganization.pageName.positionGroup",
        },
        children: [{
          path: '',
          loadChildren: () => import('@hcm-mfe/model-organization/feature/position-group-category')
            .then((m) => m.ModelOrganizationFeaturePositionGroupCategoryModule),
        },
          {
            path: 'position-assignment',
            code: FunctionCode.ORGANIZATION_POSITION_ASSIGN,
            scope: Scopes.VIEW,
            loadChildren: () => import('@hcm-mfe/model-organization/feature/position-assignment')
              .then((m) => m.ModelOrganizationFeaturePositionAssignmentModule),
            data: {
              breadcrumb: 'modelOrganization.breadcrumb.positionGroupAssign',
              pageName: "modelOrganization.pageName.positionGroupAssign",
            },
          },
        ]
      },
      {
        path: 'list-position-unassigned',
        data: {
          breadcrumb: 'modelOrganization.breadcrumb.posListUnassign',
          pageName: "modelOrganization.pageName.posListUnassign",
        },
        children: [
          {
            path: '',
            loadChildren: () => import('@hcm-mfe/model-organization/feature/position-group-assign-list')
              .then((m) => m.ModelOrganizationFeaturePositionGroupAssignListModule),
          },
          {
            path: 'position-unassigned',
            loadChildren: () => import('@hcm-mfe/model-organization/feature/position-group-assign')
              .then((m) => m.ModelOrganizationFeaturePositionGroupAssignModule),
            data: {
              breadcrumb: 'modelOrganization.breadcrumb.positionGroupAssign',
              pageName: "modelOrganization.pageName.positionGroupAssign",
            }
          }
        ]
      }
    ],
    data: {
      breadcrumb: 'modelOrganization.breadcrumb.manageJob'
    }
  },
  {
    path: 'model-organization-template',
    data : {
      breadcrumb: 'modelOrganization.breadcrumb.templateOrganization',
    },
    children: [
      {
        path: '',
        loadChildren: () => import('@hcm-mfe/model-organization/feature/list-model-template')
          .then((m) => m.ModelOrganizationFeatureListModelTemplateModule),
        data: {
          breadcrumb: 'modelOrganization.breadcrumb.templateOrganizationList',
          pageName: "modelOrganization.pageName.templateOrganizationList",
        },
      },
      {
        path: 'init-template',
        loadChildren: () => import('@hcm-mfe/model-organization/feature/intit-model-template')
          .then((m) => m.ModelOrganizationFeatureIntitModelTemplateModule),
        data: {
          breadcrumb: 'modelOrganization.breadcrumb.initTemplate',
          pageName: "modelOrganization.pageName.initTemplate",
        },
      },
      {
        path: 'init-template/:id',
        loadChildren: () => import('@hcm-mfe/model-organization/feature/intit-model-template')
          .then((m) => m.ModelOrganizationFeatureIntitModelTemplateModule),
        data: {
          breadcrumb: 'modelOrganization.breadcrumb.initTemplate',
          pageName: "modelOrganization.pageName.initTemplate",
        },
      },
    ],
  },
  {
    path: 'line-group',
    children: [  {
      path: 'list-line',
      loadChildren: () => import('@hcm-mfe/model-organization/feature/list-line')
        .then((m) => m.ModelOrganizationFeatureListLineModule),
      data : {
        pageName: 'modelOrganization.pageName.lines',
        breadcrumb: 'modelOrganization.breadcrumb.lines'
      }
    },
      {
        path: 'init-line',
        loadChildren: () => import('@hcm-mfe/model-organization/feature/init-line')
          .then((m) => m.ModelOrganizationFeatureInitLineModule),
        data : {
          pageName: 'modelOrganization.pageName.initLine',
          breadcrumb: 'modelOrganization.breadcrumb.initLine'
        }
      },
      {
        path: 'manage',
        loadChildren: () => import('@hcm-mfe/model-organization/feature/manage-line')
          .then((m) => m.ModelOrganizationFeatureManageLineModule),
        data : {
          pageName: 'modelOrganization.pageName.lineManager',
          breadcrumb: 'modelOrganization.breadcrumb.lineManager'
        }
      },
      {
        path: 'approve-line',
        loadChildren: () => import('@hcm-mfe/model-organization/feature/approve-line')
          .then((m) => m.ModelOrganizationFeatureApproveLineModule),
        data : {
          pageName: 'modelOrganization.pageName.lineApprove',
          breadcrumb: 'modelOrganization.breadcrumb.lineApprove'
        }
      }
    ],
    data : {
      breadcrumb: 'modelOrganization.breadcrumb.line'
    }
  },
  {
    path: 'category',
    loadChildren: () => import('@hcm-mfe/model-organization/feature/category')
      .then((m) => m.ModelOrganizationFeatureCategoryModule),
    data : {
      pageName: 'modelOrganization.pageName.category',
      breadcrumb: 'modelOrganization.breadcrumb.category'
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(router)],
  exports: [RouterModule],
})

export class ModelOrganizationFeatureShellRoutingModule {
}
