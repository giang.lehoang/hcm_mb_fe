import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NzMessageModule} from "ng-zorro-antd/message";
import {NzModalModule} from "ng-zorro-antd/modal";
import {SharedUiPopupModule} from "@hcm-mfe/shared/ui/popup";
import {OverlayModule} from "@angular/cdk/overlay";
import {SystemFeatureShellRoutingModule} from "../../../../../system/feature/shell/src/lib/system-feature-shell.routing.module";
import {ToastrModule} from "ngx-toastr";
import {FormatCurrencyPipe} from "@hcm-mfe/shared/pipes/format-currency";
import {ModelOrganizationFeatureShellRoutingModule} from "./model-organization-feature-shell.routing.module";

@NgModule({
  imports: [CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NzMessageModule,
    NzModalModule,
    SharedUiPopupModule,
    OverlayModule,
    ModelOrganizationFeatureShellRoutingModule,
    ToastrModule.forRoot({})],
  providers: [DatePipe, FormatCurrencyPipe],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class ModelOrganizationFeatureShellModule {}
