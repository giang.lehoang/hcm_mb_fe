import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectListApprovedComponent } from './project-list-approved.component';

describe('ProjectListApprovedComponent', () => {
  let component: ProjectListApprovedComponent;
  let fixture: ComponentFixture<ProjectListApprovedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectListApprovedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectListApprovedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
