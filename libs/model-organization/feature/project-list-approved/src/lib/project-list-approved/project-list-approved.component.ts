import { Component, DoCheck, Injector, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {OrganizationService, ProjectService} from "@hcm-mfe/model-organization/data-access/services";
import {functionUri} from "@hcm-mfe/shared/common/constants";
import { FunctionCode } from '@hcm-mfe/shared/common/enums';
import { KNHS, OrganizationDetail, PRJResponse, SearchPRJ } from '@hcm-mfe/model-organization/data-access/models';

@Component({
  selector: 'app-project-list-approved',
  templateUrl: './project-list-approved.component.html',
  styleUrls: ['./project-list-approved.component.scss'],
})
export class ProjectListApprovedComponent extends BaseComponent implements OnInit, DoCheck {
  @ViewChild('formReject', { static: false }) formReject: NgForm | undefined;

  isLoadingPage: boolean;
  initData = [
    { id: 0, nameProject: 'CRM Next Gen', unitManagement: 'Ban Kế hoạch và MKT', flagStatus: 2 },
    { id: 1, nameProject: 'CMV', unitManagement: 'Khối Thẩm định', flagStatus: 2 },
    { id: 2, nameProject: 'App MBBank', unitManagement: KNHS, flagStatus: 2 },
    { id: 3, nameProject: 'Digital Lending', unitManagement: KNHS, flagStatus: 2 },
    { id: 4, nameProject: 'Nhà máy số', unitManagement: KNHS, flagStatus: 2 },
  ];
  arrayId:number[] = [];
  setOfCheckedId = new Set<number>();
  checked = false;
  objectSearch:SearchPRJ = {
    prjName: null,
    orgName: null,
    flagStatus: 2,
    page: 0,
    size: 20,
    attrs: null,
    description: ''
  };
  listSettingPaginationCustom = {
    size: 0,
    total: 0,
    pageIndex: 0,
    numberOfElements: 0
  };
  listProjectApproved:PRJResponse[] = [];
  listUnit:OrganizationDetail[] = [];
  count = 0;
  isSubmittedReject: boolean;
  showReason: boolean;

  constructor(
    readonly projectService: ProjectService,
    readonly orgService: OrganizationService,
    injector: Injector
  ) {
    super(injector);
    this.isLoadingPage = false;
    this.isSubmittedReject = false;
    this.showReason = false;
  }

  ngOnInit(): void {
    this.isLoadingPage = true;
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.APPROVE_PROJECT}`);
    console.log(this.objFunction)
    if (!this.objFunction || !this.objFunction.view) {
      this.router.navigateByUrl(functionUri.access_denied)
    }
    this.searchListProjectApproved();
    this.getProjectsUnit();
  }

  ngDoCheck(): void {
    if (this.arrayId.length === this.listProjectApproved.length && this.arrayId.length > 0) {
      this.checked = true;
    } else {
      this.checked = false;
    }
  }

  getProjectsUnit() {
    this.orgService.getListOrgDetail().subscribe((data) => {
      if(data.data?.content) {
        this.listUnit = data.data.content;
      }
    });
  }

  updateCheckedSet(id: number, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }

  onItemChecked(id: number, checked: boolean): void {
    if (checked === true && !this.setOfCheckedId.has(id)) {
      this.arrayId.push(id);
    } else if (this.setOfCheckedId.has(id)) {
      this.arrayId = this.arrayId.filter((element) => element !== id);
    }

    this.updateCheckedSet(id, checked);
  }

  onAllChecked(checked: boolean): void {
    this.listProjectApproved.forEach((element) => {
      this.updateCheckedSet(element.prjId, checked);
      if (checked === false) {
        this.arrayId = [];
      } else {
        this.arrayId.push(element.prjId);
      }
    });
  }

  getListAprrovedProjectPagination(value:number) {
    this.objectSearch.page = value - 1;
    this.searchListProjectApproved();

  }


  searchListProjectApproved() {
    this.isLoadingPage = true;
    this.projectService.getProjectList(this.objectSearch).subscribe((data) => {
      this.listSettingPaginationCustom.size = data.data?.size;
      this.listSettingPaginationCustom.total = data.data.totalElements;
      this.listSettingPaginationCustom.pageIndex = data.data.pageable.pageNumber;
      this.listSettingPaginationCustom.numberOfElements = data.data.numberOfElements;
      this.listProjectApproved = data.data.content;
      this.isLoadingPage = false;
    }, () => {
      this.isLoadingPage = false;
    });
  }

  resetFormReject() {
    this.showReason = false;
    this.isSubmittedReject = false;
    this.objectSearch.description = null;
  }

  handleApprovedProject() {
    this.resetFormReject();
    const objectSubmit = {
      ids: this.arrayId,
      description: this.objectSearch.description ? this.objectSearch.description.trim() : "",
      type: 'PD',
    };
    if (this.arrayId.length === 0) {
      this.toastrCustom.error('Bạn chưa chọn dự án muốn phê duyệt');
    } else {
      this.isLoadingPage=true;
      this.projectService.submitListApprove(objectSubmit).subscribe(
        (data) => {
          this.isLoadingPage=false;
          this.searchListProjectApproved();
          this.toastrCustom.success('Phê duyệt thành công');
        },
        (error) => {
          this.isLoadingPage=false;
          this.toastrCustom.error(error.error.description);
        }
      );
    }
  }

  showModal() {
    if (this.arrayId.length === 0) {
      this.toastrCustom.error('Bạn chưa chọn dự án muốn từ chối');
    } else {
      this.handleOk();
    }
  }

  handleCancel() {
    this.arrayId.forEach((element) => {
      this.setOfCheckedId.delete(element);
      this.arrayId = this.arrayId.filter((item) => item !== element);
    });
  }

  handleOk(): void {
    if (!this.showReason) {
      this.showReason = true;
      return;
    }

    this.isSubmittedReject = true;
    if (!this.showReason || !this.formReject?.valid) {
      return;
    }

    const objectSubmit = {
      ids: this.arrayId,
      description: this.objectSearch.description ? this.objectSearch.description.trim() : "",
      type: 'TC',
    };
    this.isLoadingPage=true;
    this.projectService.submitListApprove(objectSubmit).subscribe((data) => {
      this.isLoadingPage=false;
      this.resetFormReject();
      this.searchListProjectApproved();
      this.toastrCustom.success('Từ chối thành công');
      this.arrayId.forEach((element) => {
        this.setOfCheckedId.delete(element);
        this.arrayId = this.arrayId.filter((item) => item !== element);
      });
    }, error => {
      this.isLoadingPage=false;
      this.toastrCustom.error(error.error.description);
    });
  }

  redrirectToDetailProject(id:number) {
    localStorage.setItem("selectedId", String(id))
    this.router.navigateByUrl(`/organization/project/view-project`).then();
  }


  override triggerSearchEvent(): void {
    this.searchListProjectApproved();
  }
}
