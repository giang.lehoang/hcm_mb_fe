import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ProjectListApprovedComponent} from "./project-list-approved/project-list-approved.component";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzCardModule} from "ng-zorro-antd/card";
import {NzGridModule} from "ng-zorro-antd/grid";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {FormsModule} from "@angular/forms";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import {RouterModule} from "@angular/router";
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, SharedUiMbButtonModule, TranslateModule, NzCardModule,
    NzGridModule, SharedUiMbSelectModule, FormsModule, SharedUiMbInputTextModule, NzTableModule, NzPaginationModule,
    RouterModule.forChild([
      {
        path: '',
        component: ProjectListApprovedComponent,
      }])
  ],
  declarations: [ProjectListApprovedComponent],
  exports: [ProjectListApprovedComponent]
})
export class ModelOrganizationFeatureProjectListApprovedModule {}
