import { Component, Injector, OnInit } from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { PositionGroupInitComponent } from '../position-group-init/position-group-init.component';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {
  listStatusJob,
  Pagination,
  PositionGroup,
  PositionGroupSearch,
  TypePosition
} from "@hcm-mfe/model-organization/data-access/models";
import {functionUri, maxInt32, userConfig} from "@hcm-mfe/shared/common/constants";
import {JobService} from "@hcm-mfe/model-organization/data-access/services";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import { renderUseStatus } from '../../../../../helper/common.function';
@Component({
  selector: 'app-position-group-category',
  templateUrl: './position-group-category.component.html',
  styleUrls: ['./position-group-category.component.scss']
})
export class PositionGroupCategoryComponent extends BaseComponent implements OnInit {
  loading: boolean;
  modalRef: NzModalRef | undefined;
  posId: string | null;
  positionGroupSearch: PositionGroupSearch;
  dataTable: PositionGroup[] = [];
  allPosition: PositionGroup[] = [];
  pagination: Pagination = new Pagination(userConfig.pageSize);
  listStatus = listStatusJob;
  listTypePosition: Array<TypePosition> = [];
  isInit: boolean;
  renderFlagStatus = renderUseStatus;
  constructor(injector: Injector, readonly jobService: JobService) {
    super(injector)
    this.isInit = true;
    this.positionGroupSearch = new PositionGroupSearch();
    this.loading = false;
    this.posId = '';
    this.objFunction = this.sessionService.getSessionData(
      `FUNCTION_${FunctionCode.ORGANIZATION_POSITION_GROUP}`
    );
  }

  ngOnInit(): void {
    this.getAllPosSelect();
    this.getListType();
  }

  getListType() {
    this.jobService.getListTypePosition().subscribe(data => {
      this.listTypePosition = data.data;
    })
  }

  getAllPosSelect() {
    const objTmp = { ...this.positionGroupSearch };
    objTmp.size = maxInt32;
    this.jobService.getListPositionGroup(objTmp).subscribe(data => {
      this.allPosition = data.data.content;
      if (this.isInit) {
        this.callGetPosition();
      }
    })
  }

  callGetPosition() {
    this.loading = true;
    this.jobService.getListPositionGroup(this.positionGroupSearch).subscribe(data => {
      this.loading = false
      this.dataTable = data.data.content;
      this.pagination.currentPage = data.data.pageable.pageNumber + 1
      this.pagination.numberOfElements = data.data.numberOfElements
      this.pagination.totalElements = data.data.totalElements
      this.pagination.totalPages = data.data.totalPages
      this.pagination.size = data.data.size
      this.dataTable.forEach(item => {
        const pos = this.allPosition.find(value => value.id === item.parentId);
        item.parentName = item.parentId && pos ? pos.name : ""
      });
      this.caculatePaging();
      this.isInit = false;
    }, error => {
      this.loading = false
      this.toastrCustom.error(error.message)
    });
  }

  showModal(id?:number) {
    this.modalRef = this.modal.create({
      nzWidth: '604px',
      nzTitle: id ? "Chỉnh sửa" : "Thêm mới",
      nzComponentParams: {
        allPosition: this.allPosition,
        id: id,
        listTypePosition: this.listTypePosition
      },
      nzBodyStyle: {
        padding: '0px 24px 24px'
      },
      nzFooter: null,
      nzContent: PositionGroupInitComponent,
    });
    this.modalRef.componentInstance.closeEvent.subscribe((data:boolean) => {
      if (data) {
        this.posId = null;
        this.search();
        this.getAllPosSelect();
      }
      this.modalRef?.destroy();
    })
  }

  search() {
    if(this.posId) {
      this.positionGroupSearch.id = this.posId
    }
    this.positionGroupSearch.page = 0;
    this.callGetPosition();
  }

  deletePositionGroup(id:number) {
    this.deletePopup.showModal(() => {
      this.loading = true;
      this.jobService.deletePositionGroup(id).subscribe(data => {
        this.loading = false;
        this.toastrCustom.success();
        this.search();
        this.getAllPosSelect();
      }, error => {
        this.loading = false;
        this.toastrCustom.error(error.message)
      })
    })
  }

  caculatePaging() {
    this.pagination.result = this.pagination.totalElements;
    this.pagination.min = this.getIndex(0);
    this.pagination.max = this.pagination.size * (this.pagination.currentPage - 1) + this.pagination.numberOfElements;
  }

  getIndex(i: number) {
    return this.pagination.size * (this.pagination.currentPage - 1) + (i + 1);
  }

  changePage() {
    this.positionGroupSearch.page = this.pagination.currentPage - 1;
    this.callGetPosition();
  }

  override triggerSearchEvent(): void {
    this.search();
  }

  override ngOnDestroy(): void {
    this.modalRef?.destroy()
  }

  trackByFn(index:number, item: PositionGroup) {
    return item.id;
  }

  assignPosition(id:number, type:string) {
    localStorage.setItem('posId', JSON.stringify({id:id, type:type}));
    this.router.navigateByUrl(functionUri.organization_assign_position);
  }

}
