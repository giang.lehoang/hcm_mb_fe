import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PositionGroupCategoryComponent } from './position-group-category.component';

describe('PositionGroupCategoryComponent', () => {
  let component: PositionGroupCategoryComponent;
  let fixture: ComponentFixture<PositionGroupCategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PositionGroupCategoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PositionGroupCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
