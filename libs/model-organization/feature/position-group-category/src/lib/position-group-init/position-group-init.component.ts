import { Component, EventEmitter, Injector, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {
  listStatusJob,
  PositionGroup,
  RequestPosition,
  TypePosition
} from "@hcm-mfe/model-organization/data-access/models";
import {JobService} from "@hcm-mfe/model-organization/data-access/services";
import {regexSpecialForLine} from "@hcm-mfe/shared/common/constants";


@Component({
  selector: 'app-position-group-init',
  templateUrl: './position-group-init.component.html',
  styleUrls: ['./position-group-init.component.scss']
})
export class PositionGroupInitComponent extends BaseComponent implements OnInit, OnDestroy {

  isSubmitted: boolean;
  positionForm: FormGroup;
  loading: boolean;
  listPositionTmp: Array<PositionGroup> = [];
  @Input() id: number | undefined;
  @Input() listTypePosition: Array<TypePosition> = [];
  @Input() allPosition: PositionGroup[] = [];
  @Output() closeEvent = new EventEmitter<boolean>();
  listStatus = listStatusJob;
  constructor(readonly jobService: JobService, injector: Injector) {
    super(injector)
    this.listPositionTmp = [...this.allPosition];
    this.loading = false;
    this.isSubmitted = false;
    this.positionForm = new FormGroup({
      posManager: new FormControl(''),
      posGroupName: new FormControl('', Validators.required),
      typePositionGroup: new FormControl('', Validators.required),
      status: new FormControl('')
    })
  }

  ngOnInit(): void {

    if (this.id) {
      this.loading = true;

      this.jobService.detailPositionGroup(this.id).subscribe(data => {
        this.loading = false;
        this.positionForm.controls['posManager'].setValue(data.data.parentId);
        this.positionForm.controls['posGroupName'].setValue(data.data.name);
        this.positionForm.controls['typePositionGroup'].setValue(data.data.type);
        this.positionForm.controls['status'].setValue(''+data.data.status);

      }, error => {
        this.loading = false
      })
      this.allPosition = this.allPosition.filter(
        item => item.id !== this.id
      )
    } else {
      this.positionForm.controls['status'].setValue('1');
      this.positionForm.controls['status'].disable();
    }
  }

  // changeData(value) {
  //   this.isSubmitted = false;
  //   if (this.positionForm.value.fromDate && this.positionForm.value.toDate) {
  //     const fromDate = new Date(this.positionForm.value.fromDate).setHours(0, 0, 0, 0);
  //     const toDate = new Date(this.positionForm.value.toDate).setHours(0, 0, 0, 0);
  //     if (fromDate >= toDate) {
  //       this.positionForm.controls.toDate.setErrors({ date: true });
  //     } else {
  //       this.positionForm.controls.toDate.setErrors(null);
  //     }
  //   }
  // }

  submit() {
    this.isSubmitted = true;
    this.positionForm.controls['posGroupName'].setValue(this.positionForm.value.posGroupName.trim())
    if (this.positionForm.invalid) {
      return;
    }

    const req: RequestPosition = {
      parentId: this.positionForm.value.posManager,
      pgrId: this.id ? this.id : null,
      pgrName: this.positionForm.value.posGroupName,
      status: !this.id ? 1 : this.positionForm.value.status,
      type: this.positionForm.value.typePositionGroup
    }
    this.loading = true;
    if (this.id) {
      this.updatePosition(req);
    } else {
      this.createPosition(req);
    }

  }
  updatePosition(req:any){
    this.jobService.updatePositionGroup(req).subscribe(data => {
      this.loading = false;
      this.toastrCustom.success(this.translate.instant('modelOrganization.message.update'))
      this.closeEvent.emit(true)
    }, error => {
      const message = error.message
      if (message === this.translate.instant('modelOrganization.validate.duplicatePos')) {
        this.positionForm.controls['posGroupName'].setErrors({ duplicatePos: true })
      } else {
        this.toastrCustom.error(message)
      }
      this.loading = false;
    })
  }
  createPosition(req:any){
    this.jobService.createPositionGroup(req).subscribe(data => {
        this.loading = false;
        this.toastrCustom.success(this.translate.instant('modelOrganization.message.create'))
        this.closeEvent.emit(true)
      },
      (error) => {
        const message = error.message
        if (message === this.translate.instant('modelOrganization.validate.duplicatePos')) {
          this.positionForm.controls['posGroupName'].setErrors({ duplicatePos: true })
        } else {
          this.toastrCustom.error(message)
        }
        this.loading = false;
      })
  }

  onKeypress(event: KeyboardEvent) {
    const pattern = regexSpecialForLine;
    if (pattern.test(event.key)) {
      event.preventDefault();
    }
  }

  changeValue(event:string) {
    const pattern = regexSpecialForLine;
    if (pattern.test(event)) {
      setTimeout(() => {
        this.positionForm.controls['posGroupName'].setValue("");
      }, 500);
    }
  }
  onChangeType(e:any) {
    if (e.itemSelected) {
      this.listPositionTmp = [...this.allPosition].filter(item => item.type === e.itemSelected.meaning)
    } else {
      this.listPositionTmp = [...this.allPosition];
    }
  }

  override ngOnDestroy(): void {
    this.closeEvent.complete()
  }

  closeModal() {
    this.closeEvent.emit(false);
  }

}
