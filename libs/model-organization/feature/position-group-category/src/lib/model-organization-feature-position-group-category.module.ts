import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PositionGroupCategoryComponent} from "./position-group-category/position-group-category.component";
import {PositionGroupInitComponent} from "./position-group-init/position-group-init.component";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NzGridModule} from "ng-zorro-antd/grid";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
import {NzTagModule} from "ng-zorro-antd/tag";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, ReactiveFormsModule, NzGridModule, RouterModule.forChild([
    {
      path: '',
      component: PositionGroupCategoryComponent,
    }]),
    SharedUiMbSelectModule, TranslateModule, SharedUiMbInputTextModule, SharedUiMbButtonModule, NzGridModule,
    FormsModule, NzTableModule, NzToolTipModule, NzTagModule, NzPaginationModule],
  declarations: [PositionGroupCategoryComponent, PositionGroupInitComponent],
  exports: [PositionGroupCategoryComponent, PositionGroupInitComponent]
})
export class ModelOrganizationFeaturePositionGroupCategoryModule {}
