import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PositionGroupAssignListComponent } from './position-group-assign-list.component';

describe('PositionGroupAssignListComponent', () => {
  let component: PositionGroupAssignListComponent;
  let fixture: ComponentFixture<PositionGroupAssignListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PositionGroupAssignListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PositionGroupAssignListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
