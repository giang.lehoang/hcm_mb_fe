import {ChangeDetectorRef, Component, Injector, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {FormModalShowTreeUnitComponent} from "@hcm-mfe/shared/ui/form-modal-show-tree-unit";
import {TableConfig} from "@hcm-mfe/shared/data-access/models";
import {JobService} from "@hcm-mfe/model-organization/data-access/services";
import {
  Job,
  PosNunAssign,
  PosNunAssignRequest,
  PosUnAssignDetail
} from "@hcm-mfe/model-organization/data-access/models";
import {userConfig} from "@hcm-mfe/shared/common/constants";
import * as FileSaver from "file-saver";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {FunctionCode, functionUri} from "@hcm-mfe/shared/common/enums";

@Component({
  selector: 'app-position-group-assign-list',
  templateUrl: './position-group-assign-list.component.html',
  styleUrls: ['./position-group-assign-list.component.scss']
})
export class PositionGroupAssignListComponent extends BaseComponent implements OnInit {
  @ViewChild('actionTmpl', {static: true}) action!: TemplateRef<NzSafeAny>;
  orgName:string;
  listJob:Job[] = [];
  tableConfig:TableConfig = new TableConfig;
  nzWidthConfig: string[] = ['80px','850px', '294px', '200px', '250px', '80px'];
  dataTable:PosNunAssign[] = [];
  objectSearch:PosNunAssignRequest = new PosNunAssignRequest();
  param:PosNunAssignRequest;

  constructor(injector: Injector, private readonly jobService:JobService) {
    super(injector);
    this.orgName = '';
    this.param = this.objectSearch;
    this.objFunction = this.sessionService.getSessionData(
      `FUNCTION_${FunctionCode.ORGANIZATION_POSITION_ASSIGN}`
    );
  }

  ngOnInit(): void {
    localStorage.removeItem('posUnassign');
    this.initTable();
    this.onSearch();
    this.jobService.listJobPosUnassign().subscribe(data => {
      this.listJob = data.data;
    }, error => {
      this.toastrCustom.error(error.message);
    })
  }

  callList(){
    this.isLoading = true;
    this.param = {...this.objectSearch}

    if(!this.param.orgId){
      this.param.orgId = '';
    }

    if(!this.param.jobId){
      this.param.jobId = '';
    }

    this.jobService.listPosUnassign(this.param).subscribe(data => {
      this.isLoading = false;
      this.dataTable = data.data.content;
      this.tableConfig.total = data.data.totalElements;
    },error => {
      this.isLoading = false;
      this.toastrCustom.error(error.message);
    })
  }

  search(pageNumber: number) {
    this.tableConfig.pageIndex = pageNumber;
    this.objectSearch.page = pageNumber - 1;
    this.callList();
  }
  onSearch() {
    this.search(1);
  }

  clearOrgInput():void {
    this.orgName = "";
    this.objectSearch.orgId = "";
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: "STT",
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          rowspan: 2,
          fixed: window.innerWidth > 1024,
          width: 50,
          show: true,
          fixedDir: 'left',
        },
        {
          title: 'Position',
          colspan: 2,
          width:400,
          thClassList: ['border-bottom', 'border-left', 'border-right'],
          child: [
            {
              title: 'modelOrganization.table.unit',
              field: 'fullName',
              thClassList: ['border-left', 'border-right'],
              fixed: window.innerWidth > 1024,
              show: true,
            },
            {
              title: 'modelOrganization.table.job',
              field: 'jobName',
              thClassList: ['border-right'],
              fixed: window.innerWidth > 1024,
              show: true
            },

          ]
        },
        {
          title: 'modelOrganization.table.posType',
          width:200,
          rowspan: 2,
          field: 'pgrType',
          fixed: window.innerWidth > 1024,
          show: true
        },
        {
          title: 'modelOrganization.table.posGroup',
          field: 'pgrName',
          rowspan: 2,
          width: 100,
          fixed: window.innerWidth > 1024,
          show: true
        }
        ,        {
          title: '',
          rowspan: 2,
          width: 50,
          tdTemplate: this.action,
          fixed: window.innerWidth > 1024,
          show: true
        }
      ],
      needScroll: true,
      total: 10,
      loading: false,
      size: 'small',
      pageIndex: 1,
      pageSize: userConfig.pageSize,
      showFrontPagination: false,
    };
  }

  showPopup():void {
    const modal = this.modal.create({
      nzWidth: '80%',
      nzTitle: ' ',
      nzContent: FormModalShowTreeUnitComponent,
      nzFooter: null,
    });

    modal.afterClose.subscribe((result) => {
      if (result?.orgName) {
        this.orgName = result?.orgName;
        this.objectSearch.orgId = result?.orgId;
      }
    });
  }

  exportExcel(){
    this.param = {...this.objectSearch}
    if(!this.param.orgId){
      this.param.orgId = '';
    }

    if(!this.param.jobId){
      this.param.jobId = '';
    }
    this.jobService.exportPositionUnAssign(this.param).subscribe(data => {
      const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      FileSaver.saveAs(blob, 'Position chưa gán nhóm position.xlsx');
    })
  }

  assignPositon(pgrType:string, parentId:number, orgId:number, pgrId:number, pgrName:string){
    const item:PosUnAssignDetail = {
      orgId: orgId,
      type: this.param.type,
      pgrId: pgrId,
      parentId: parentId,
      pgrType:pgrType,
      pgrName: pgrName ? pgrName : ''
    }
    localStorage.setItem('posUnassign', JSON.stringify(item));
    this.router.navigate([functionUri.organization_pos_unassign]);
  }

}
