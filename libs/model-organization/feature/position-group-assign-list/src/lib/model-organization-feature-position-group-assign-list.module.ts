import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PositionGroupAssignListComponent} from "./position-group-assign-list/position-group-assign-list.component";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {NzGridModule} from "ng-zorro-antd/grid";
import {TranslateModule} from "@ngx-translate/core";
import {FormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {NzRadioModule} from "ng-zorro-antd/radio";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiMbTableMergeCellWrapModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell-wrap";
import {SharedUiMbTableMergeCellModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell";
import {NzIconModule} from "ng-zorro-antd/icon";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";

@NgModule({
  imports: [CommonModule, SharedUiMbInputTextModule, NzGridModule, TranslateModule, FormsModule, RouterModule.forChild([
    {
      path: '',
      component: PositionGroupAssignListComponent,
    }]), NzRadioModule, SharedUiMbButtonModule, SharedUiMbSelectModule, SharedUiMbTableMergeCellWrapModule,
    SharedUiMbTableMergeCellModule, NzIconModule, SharedUiLoadingModule, NzToolTipModule],
  declarations: [PositionGroupAssignListComponent],
  exports: [PositionGroupAssignListComponent]
})
export class ModelOrganizationFeaturePositionGroupAssignListModule {}
