import { Component, Injector, Input, OnInit } from '@angular/core';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { JobModelRes,
  JobModelTmp, JobModelUnit,
  ListSearchOrganization, OrgAttributeModel,
  PositionDetailModel, PositionDTOSModel, RegionType
} from '@hcm-mfe/model-organization/data-access/models';
import {
  OrganizationalModelTemplateService,
  UnitApprovedService
} from '@hcm-mfe/model-organization/data-access/services';
import { CustomToastrService } from '@hcm-mfe/shared/core';
import { DataService } from '@hcm-mfe/shared/data-access/services';

@Component({
  selector: 'app-correct-unit',
  templateUrl: './correct-unit.component.html',
  styleUrls: ['./correct-unit.component.scss']
})
export class CorrectUnitComponent extends BaseComponent implements OnInit {

  @Input() orgSelect: ListSearchOrganization|undefined;

  params = '';
  data: any;
  isValid = false;
  description ='';
  typeInput ='';
  orgId: any;
  arrTmp: Array<JobModelTmp> = [];
  positionDetailDTOS: Array<PositionDetailModel> = [];
  positionDTOS: Array<PositionDTOSModel> = [];
  orgAttribute: Array<OrgAttributeModel> = [];
  listJob: Array<JobModelRes> = [];
  jobDTO: Array<JobModelUnit> = [];
  isLoadData = false;
  listUnit: Array<string> = [];
  sheetApproval: any;
  listBranType: any;
  legalBranchId = 0;
  branchType = '';
  type = '';
  area = '';
  file: any;
  isVisible = false;
  pdfSrc = '';
  constructor(readonly unitApprovedService: UnitApprovedService,
    readonly organizationService: OrganizationalModelTemplateService,
    readonly dataService: DataService,
    readonly toastr: CustomToastrService,
    injector: Injector,
  ) {
    super(injector)
  }

  ngOnInit(): void {
    this.isValid = false;
    this.description = '';
    this.typeInput = '';
    this.branchType = '';
    this.type = '';
    this.area = '';
    this.orgId = 0;
    this.isLoadData = false;
    this.sheetApproval = null;
  }
  ngOnChanges(changes: any) {
    if (changes.orgSelect) {
      this.getUnitInitDetail();
    }

  }
  getUnitInitDetail() {
    this.unitApprovedService.getDetailOrgApprove(this.orgSelect?.orgId).subscribe((data: any) => {
      this.orgAttribute = data.data.organization.orgAttributeDTOS;
      this.positionDetailDTOS = data.data.organization.positionDetailDTOS;
      this.positionDTOS = data.data.organization.positionDTOS;
      this.jobDTO = data.data.organization.jobDTO;
      this.sheetApproval = data.data.sheetApproval;
      this.data = data.data;
      this.legalBranchId = data.data.organization.legalBranchId;
      this.getBranchType();
      this.getArea();
      this.getType();
      this.groupData();
    })
  }
  groupData() {
    let jobTmp:any = [];
    this.arrTmp = [];
    this.listJob = [];
    this.listUnit = [];
    this.positionDetailDTOS.forEach(item => {
      this.orgAttribute.forEach(value => {
        if (item.orgaId === value.orgaId) {
          this.arrTmp.push({
            orgaId: item.orgaId,
            orgaValue: value.orgaValue,
            posId: item.posId
          });
        }
      });
      if (!item.orgaId) {
        this.arrTmp.push({
          orgaId: null,
          orgaValue: "",
          posId: item.posId
        });
      }
    });
    this.positionDTOS.forEach(item => {
      this.arrTmp.forEach(value => {
        if (item.id === value.posId) {
          jobTmp.push({ ...value, jobId: item.jobId });
        }
      });
    });
    this.arrTmp = [...jobTmp];
    jobTmp = [];
    this.arrTmp.forEach(item => {
      // @ts-ignore
      jobTmp.push({ ...item, jobName: this.jobDTO.find(value => value.jobId === item.jobId).jobName });
    });
    const arrOrgTmp = jobTmp.filter((item:any) => !item.orgaId)
    if (arrOrgTmp.length > 0) {
      this.orgAttribute.push({
        orgaId: null,
        orgaValue: "",
      });
    }
    this.arrTmp = [...jobTmp];
    this.orgAttribute.forEach(item => {
      this.listJob.push({
        orgaId: item.orgaId,
        orgaValue: item.orgaValue,
        subs: this.arrTmp.filter(v => v.orgaId === item.orgaId)
      });
    });
    this.listUnit.push(this.data?.organization?.orgName);
    this.isLoadData = true;
  }

  getBranchType() {
    this.dataService.getBranchType().subscribe((data:any) => {
      this.listBranType = data.data;
      this.branchType = this.listBranType?.find((item:any) => Number(item?.orgId) === this.legalBranchId)?.orgName;
    });
  }

  getType() {
    this.dataService.getRegionCategoryList(RegionType.LH, 1).subscribe((data: any) => {
      this.type = this.data.branchType ? data.data.find((item:any) => item.lvaValue === this.data.branchType).lvaMean : "";
    });
  }

  getArea() {
    this.dataService.getRegionCategoryList(RegionType.KV, 1).subscribe((data: any) => {
      this.area = this.data.region ? data.data.find((item:any) => item.lvaValue === this.data.region).lvaMean : "";
    });
  }
  handleReject() {
    if (this.description === "") {
      this.isValid = true;
    } else {
      this.rejectUnit();
      this.isValid = false;
    }
  }
  handleApprove() {
    this.approveUnit();
  }
  approveUnit() {
    this.organizationService.approveInitOrganization(this.orgSelect?.orgId, this.orgSelect?.inputType, this.description).subscribe(data => {
      this.toastr.success(this.translate.instant('modelOrganization.approveDetail.notif.approveSuccess'));
      this.router.navigateByUrl('organization/init/unit-approve');
    }, error => {
      this.toastr.error(this.translate.instant('modelOrganization.approveDetail.notif.approveFail'));
    })
  }

  rejectUnit() {
    this.organizationService.rejectedInitOrganization(this.orgSelect?.orgId, this.orgSelect?.inputType, this.description).subscribe(data => {
      this.toastr.success(this.translate.instant('modelOrganization.approveDetail.notif.rejectSuccess'));
      this.router.navigateByUrl('organization/init/unit-approve');
    }, error => {
      this.toastr.error(this.translate.instant('modelOrganization.approveDetail.notif.rejectFail'));
    })
  }

  downloadFile(id:any) {
    this.unitApprovedService.downloadFile(id).subscribe(data => {
      const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.pdf' });
      this.file = new File([blob], String(this.sheetApproval?.mpDoc?.fileName));
      this.onFileSelected();
    })
  }
  onFileSelected() {
    const reader = new FileReader();
    reader.onload = (e: any) => {
      this.pdfSrc = e.target.result;
    };
    reader.readAsArrayBuffer(this.file);
    this.isVisible = true;
  }

  handleCancel(): void {
    this.isVisible = false;
  }

  calHeight(length:any) {
    if (length > 0) {
      return (length * 22) + 18;
    } else {
      return 44;
    }
  }
  calHeightJob(index:any, length:any) {
    let a = 0;
    if (index === length - 1) {
      a = 40;
    } else if (index === 0 && length === 0) {
      a = 44;
    }
    return a;
  }
}
