import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CorrectUnitComponent } from './correct-unit.component';

describe('CorrectUnitComponent', () => {
  let component: CorrectUnitComponent;
  let fixture: ComponentFixture<CorrectUnitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CorrectUnitComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CorrectUnitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
