import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { TranslateModule } from '@ngx-translate/core';
import { NzCardModule } from 'ng-zorro-antd/card';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { ModelOrganizationFeatureRegionCategoryInitModule } from '@hcm-mfe/model-organization/feature/region-category-init';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { SharedDirectivesPaddingDirectivesModule } from '@hcm-mfe/shared/directives/padding-directives';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { CorrectUnitComponent } from './correct-unit/correct-unit.component';

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, SharedUiMbButtonModule, TranslateModule, NzCardModule, ReactiveFormsModule, FormsModule,
    NzGridModule, SharedUiMbSelectModule, FormsModule, SharedUiMbInputTextModule, NzTableModule,
    NzToolTipModule, NzModalModule, NzTagModule, ModelOrganizationFeatureRegionCategoryInitModule, NzIconModule, SharedDirectivesPaddingDirectivesModule, PdfViewerModule, NzFormModule, NzInputModule
  ],
  declarations: [CorrectUnitComponent],
  exports: [CorrectUnitComponent],
})
export class ModelOrganizationFeatureCorrectUnitModule {}
