import { forkJoin } from 'rxjs';
import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators, FormArray, AbstractControl } from '@angular/forms';
import * as moment from 'moment';
import { MatStepper } from '@angular/material/stepper';
import {DataService, OrganizationService} from "@hcm-mfe/model-organization/data-access/services";
import { FunctionCode, functionUri, maxInt32 } from '@hcm-mfe/shared/common/enums';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {
  Job,
  Organization,
  Type,
  Area,
  RequestBodyInitOrg,
  AdjustOrgRequest,
  OrgNode,
  ValidateObject,
  ListLegalBranch,
  RegionType,
  ResponseEntity,
  LookupValue,
  ResponseEntityData,
  PositionDetailOrg,
  FunctionOrg,
  OrgDetailResponse,
  AttributesOrg,
  OrganizationCreate,
  SelectObject
} from "@hcm-mfe/model-organization/data-access/models";
import {REGEX} from "@hcm-mfe/shared/common/constants";

@Component({
  selector: 'function-management-screen',
  templateUrl: './function-management.component.html',
  styleUrls: ['./function-management.component.scss'],
})
export class FunctionMangementComponent extends BaseComponent implements OnInit{
  @ViewChild('stepper') private readonly myStepper: MatStepper | undefined;
  isSubmit: boolean | undefined;
  viewDetail: boolean;
  viewInit: boolean;
  orgId: string;
  fileName: string;
  orgLevel: string;
  createOrganization: FormGroup;
  initPosition: FormGroup | undefined;
  nameSelectedLegalBranch: string;
  review: FormGroup | undefined;
  dataDetail: OrgDetailResponse | Record<string, never> = {};
  listData: Organization[] = [];
  listView: Organization[] = [];
  mapJobs: Map<number, Job>;
  areas: Area[] = [];
  types: Type[] = [];
  allFruits: SelectObject<number, string>[] = [];
  change: boolean;
  requestBody: RequestBodyInitOrg | undefined;
  requestBodyEdit: AdjustOrgRequest | undefined;
  file: {
    file?: File;
    status?: boolean;
  };
  count: number | undefined;
  parentData: OrgNode | undefined;
  validateObject: ValidateObject;
  listLegalBranch: Array<ListLegalBranch> = [];
  steper:number;

  constructor(
    injector: Injector,
    private readonly _formBuilder: FormBuilder,
    private readonly orgService: OrganizationService,
    private readonly dataService: DataService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ORGANIZATION_MANGEMENT_INIT}`);
    if (!this.objFunction.view) {
      this.router.navigateByUrl(functionUri.access_denied);
    }
    this.steper = 0;
    this.change = false;
    this.validateObject = {
      attach: false,
    };
    this.orgId = JSON.parse(localStorage.getItem('org_selected'))?.orgId;
    this.orgLevel = JSON.parse(localStorage.getItem('org_selected'))?.orgLevel;
    this.viewDetail = JSON.parse(localStorage.getItem('viewDetail'));
    this.viewInit = JSON.parse(localStorage.getItem('viewInit'));


    this.file = {};
    this.mapJobs = new Map<number, Job>();
    this.fileName = '';
    this.nameSelectedLegalBranch = '';

    if (!this.viewInit) {
      this.parentData = JSON.parse(localStorage.getItem('org_selected'));
    }

    this.createOrganization = this._formBuilder.group({
      sapNo: ['', [Validators.required]],
      dateVisible: ['', Validators.required],
      parentUnit: this.parentData ? [this.parentData.name] : [''],
      parentId: [''],
      branch: [''],
      area: [''],
      regionView: [''],
      branchView: [''],
      attach: [''],
      fileName: ['', Validators.required],
      legalBranch: [''],
      orgSelected: [''],
      informationForm: this._formBuilder.group({
        attach: [''],
        fileName: [''],
      }),
      sheetApproval: [''],
      formArr: new FormArray([]),
      fun: new FormArray([]),
    });
  }


  ngOnInit() {
    this.fetchData();
  }

  fetchData() {
    this.isLoading = true;
    forkJoin([
      this.dataService.getLookupValue(RegionType.KV, 1),
      this.dataService.getLookupValue(RegionType.LH, 1),
      this.orgService.getListLegalBranch(),
      this.dataService.getJobsByType('',0,maxInt32,['ORG','DV']),
    ]).subscribe(
      ([areas, types, branchs, jobs]: [
        ResponseEntity<LookupValue[]>,
        ResponseEntity<LookupValue[]>,
        ResponseEntity<ListLegalBranch[]>,
        ResponseEntityData<Job[]>
      ]) => {
        this.areas = areas.data;
        this.types = types.data;
        this.listLegalBranch = branchs.data;
        const listJob = jobs.data.content;
        this.processListJob(listJob);
        if (this.orgId) {
          this.getDetailOrg();
        }
        this.isLoading = false;
      },
      (error) => {
        this.toastrCustom.error(error.error?.message);
        this.isLoading = false;
      }
    );
  }
  getDetailOrg() {
    if (this.viewInit) {
      return;
    }
    this.orgService.getDetailOrg(this.orgId).subscribe(
      (data: any) => {
        this.buildTable(data.data);
        this.dataDetail = data.data;
        this.createOrganization.controls['area'].setValue(data.data.region);
        this.createOrganization.controls['parentUnit'].setValue(data.data.parentName);
        this.createOrganization.controls['parentId'].setValue(data.data.parentId);
        this.createOrganization.controls['sapNo'].setValue(data.data.sheetApproval?.sapNo);
        this.createOrganization.controls['dateVisible'].setValue(data.data.fromDate && data.data.fromDate !== 'null' ? data.data.fromDate : '');
        this.createOrganization.controls['branch'].setValue(data.data.branchType);
        this.createOrganization.controls['legalBranch'].setValue(data.data.legalBranchId);
        this.createOrganization.controls['attach'].setValue(data.data.sheetApproval?.mpDoc.fileName);
        this.createOrganization.controls['fileName'].setValue(data.data.sheetApproval?.mpDoc.fileName, {
          emitModelToViewChange: false,
        });
        this.fileName = data.data.sheetApproval?.mpDoc.fileName;
        this.createOrganization.controls['sheetApproval'].setValue(data.data.sheetApproval);
        this.getValueShow();
      },
      (error) => {
        this.toastrCustom.error(error.error?.message);
      }
    );
  }
  callGetJob() {
    this.dataService.getJobsByType().subscribe((data) => {
      const listJob = data.data.content;
      this.processListJob(listJob);
    });
  }
  processListJob(listJob:Job[]) {
    listJob.forEach((element:Job) => {
      this.mapJobs.set(element.jobId, element);
      if (!element.jobName) {
        element.jobName = '';
      }
      this.allFruits.push({
        id: element.jobId,
        value: element.jobName,
      });
    });
  }
  buildTable(dataDetail:OrgDetailResponse) {
    const mapJob = new Map<number, PositionDetailOrg | Job>();
    const mapAt = new Map<number | null | undefined, AttributesOrg>();
    const functionAt = [];
    dataDetail.positionDTOS.forEach((item) => {
      item.value = this.mapJobs.get(item.jobId)?.jobName;
      item.posId = item.id;
      item.id = item.jobId;
      mapJob.set(item.posId, item);
    });

    dataDetail.orgAttributeDTOS.forEach((item) => {
      mapAt.set(item.orgaId, item);
      item.positions = [];
      const control = new FormControl(item.orgaValue, [Validators.pattern(REGEX.SPECIAL_TEXT_CODE)]);
      (this.createOrganization.controls['fun'] as FormArray).push(control);
      item.control = control;
      functionAt.push({
        job: item.positions,
        value: item.control,
      });
    });
    dataDetail.positionDetailDTOS.forEach((item) => {
      if (item.orgaId === null && !mapAt.get(null)) {
        const newAtb:AttributesOrg = {
          orgId: null,
          orgaValue: '',
          orgaId: null,
          positions: [],
        };
        dataDetail.orgAttributeDTOS.push(newAtb);
        mapAt.set(null, newAtb);
      }
      mapAt.size > 0 && mapAt.get(item.orgaId)?.positions.push(mapJob.get(item.posId) as Job);
    });

    if(functionAt.length === 0){
      functionAt.push({
        value: new FormControl(null, [Validators.pattern(REGEX.SPECIAL_TEXT_CODE)]),
        job: dataDetail.orgAttributeDTOS[0]?.positions ? dataDetail.orgAttributeDTOS[0]?.positions : []
      })
    }

    const formControl: FormControl = new FormControl(dataDetail.orgName, [
      Validators.pattern(REGEX.SPECIAL_TEXT_CODE),
      Validators.required,
    ]);
    (this.createOrganization.controls['formArr'] as FormArray).push(formControl);

    const org: Organization = {
      job: [],
      branchType: dataDetail.branchType,
      function: functionAt,
      children: [],
      orgLevel: 1,
      region: dataDetail.region,
      control: formControl,
    };
    this.listData = [org];
    this.listView = [org];
  }

  filterValue(steper: MatStepper) {
    this.isSubmit = true;
    if (!JSON.parse(localStorage.getItem('viewEdit')) && this.checkDuplicateOrg(null)) {
      this.toastrCustom.error('Tên đơn vị ngang cấp cùng cấp trên không được trùng nhau');
      return;
    }
    this.createOrganization.controls['sapNo'].setValue(this.createOrganization.value.sapNo.replaceAll(' ', '').trim());
    if (this.createOrganization.valid) {
      this.getValueShow();
      this.steper = 1;
      steper.next();
    } else {
      if (this.listData.length === 1 && !this.listData[0].control?.value && this.listData[0].children?.length === 0) {
        this.listData[0].control?.setErrors(Validators.required);
      }
      const errorSize = this.createOrganization.controls['fileName'].errors;
      Object.values(this.createOrganization.controls).forEach((control) => {
        this.validateObject.attach = true;
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });

      this.createOrganization.controls['fileName'].setErrors(errorSize);

      this.markAsDirty((this.createOrganization.get('formArr') as FormArray).controls);
    }
  }

  markAsDirty(arr: AbstractControl[]) {
    Object.values(arr).forEach((control: AbstractControl) => {
      if (control.invalid) {
        control.markAsDirty();
        control.updateValueAndValidity({ onlySelf: true });
      }
    });
  }

  getValueShow() {
    this.listView.splice(0, this.listView.length);
    this.listView.push(...this.listData);
    this.change = true;
    if (this.createOrganization.value.branch === 'PGD Online' || this.createOrganization.value.branch === 'TX8') {
      if (this.listLegalBranch.length > 0) {
        this.listLegalBranch.forEach((item) => {
          if (item.orgId === parseInt(this.createOrganization.value.legalBranch, 10)) {
            this.nameSelectedLegalBranch = item.orgName;
          }
        });
      }
    }
    if (this.createOrganization.value.area) {
      this.createOrganization.controls['regionView'].setValue(
        this.getMeanByValue(this.createOrganization.value.area, this.areas)
      );
    }

    if (this.createOrganization.value.branch) {
      this.createOrganization.controls['branchView'].setValue(
        this.getMeanByValue(this.createOrganization.value.branch, this.types)
      );
    }
  }


  submit() {
    this.isLoading = true;
    if (JSON.parse(localStorage.getItem('viewEdit'))) {
      this.updateOrganization(this.buildRequestEdit());
    } else {
      this.initOrganization(this.processRequest());
    }
  }

  initOrganization(data:FormData) {
    this.orgService.createOrganization(data).subscribe(
      (response) => {
        this.processBrowseOrg();
      },
      (error) => {
        this.toastrCustom.error(error.error?.message);
        this.isLoading = false;
      }
    );
  }
  updateOrganization(data:FormData) {
    this.orgService.updateOrganization(data).subscribe(
      (response) => {
        this.processBrowseOrg();
      },
      (error) => {
        this.toastrCustom.error(error.error?.message);
        this.isLoading = false;
      }
    );
  }
  processBrowseOrg() {
    this.toastrCustom.success('Chuyển duyệt thành công');
    this.isLoading = false;
    this.router.navigateByUrl(functionUri.organization_init_screen);
    localStorage.removeItem('org_selected');
  }
  processRequest():FormData {
    this.requestBody = {
      parentId: this.parentData?.id ? this.parentData.id : +localStorage.getItem('parentUnitId'),
      branchType: this.createOrganization.value.branch,
      region: this.createOrganization.value.area,
      fromDate: moment(this.createOrganization.value.dateVisible).format('YYYY-MM-DD'),
      legalBranchId: this.createOrganization.value.legalBranch,
      sapNo: this.createOrganization.value.sapNo,
      orgReqDetails: [],
    };
    this.buildRequest(this.listView, this.requestBody);
    const formData: FormData = new FormData();
    if(this.file.file){
      formData.append('sapFile', this.file.file, this.createOrganization.value.attach);
    }
    formData.append(
      'requestDTO',
      new Blob([JSON.stringify(this.requestBody)], {
        type: 'application/json',
      })
    );
    return formData;
  }
  buildRequestEdit() {
    const funs: FunctionOrg[] = [];

    this.dataDetail.orgAttributeDTOS?.forEach((item) => {
      let jobs: Job[] | undefined = [];
      item.positions.forEach((pos:any) => {
        jobs?.push(pos.id);
      });
      jobs = this.listData[0].function?.find(atr => atr.value.value === item?.control?.value)?.job;

      const adFun: FunctionOrg = {
        orgaId: item.orgaId,
        orgaValue: item?.control?.value,
        jobIds: jobs?.map(job => job.id),
      };

      funs.push(adFun);
    });

    this.requestBodyEdit = {
      orgId: +this.orgId,
      orgName: this.listData[0].control?.value,
      branchType: this.createOrganization.value.branch,
      parentId: this.createOrganization.value.parentId,
      region: this.createOrganization.value.area,
      fromDate: moment(this.createOrganization.value.dateVisible).format('YYYY-MM-DD'),
      legalBranchId: this.createOrganization.value.legalBranch,
      sheetApproval: {
        sapId: this.dataDetail.sheetApproval?.sapId,
        sapNo: this.createOrganization.value.sapNo,
      },
      orgLevel: +this.orgLevel,
      adjustedFunctions: funs,
    };
    const formData: FormData = new FormData();
    if (this.file.status) {
      if(this.file.file){
        formData.append('sapFile', this.file.file, this.createOrganization.value.attach);
      }
    }
    formData.append(
      'request',
      new Blob([JSON.stringify(this.requestBodyEdit)], {
        type: 'application/json',
      })
    );
    return formData;
  }
  buildRequest(list:Organization[], requestObject:RequestBodyInitOrg | OrganizationCreate) {
    if (!requestObject.orgReqDetails) {
      requestObject.subs = [];
    }
    if (!list || list.length === 0) {
      return;
    }
    for (const item of list) {
      const newItem = this.processObject(item);
      requestObject.subs?.push(newItem);
      requestObject.orgReqDetails?.push(newItem);
      if(item.children){
        this.buildRequest(item.children, newItem);
      }
    }
  }

  processObject(item: Organization) {
    const newItem: OrganizationCreate = {
      orgName: item?.control?.value.trim(),
      templateOrgId: item.orgId ? item.orgId : null,
      posDetails: [],
      subs: [],
    };

    const funs = item.function ? item.function : [];
    for (const fun of funs) {
      const posL = fun.job ? fun.job : [];
      const posIds:number[] = [];
      posL.forEach(el => {
        if(el.id){
          posIds.push(el.id)
        }
      })
      newItem.posDetails.push({
        orgAttrValue: fun.value.value?.trim(),
        jobIds: posIds,
      });
    }
    return newItem;
  }

  getMeanByValue(value:string, list:LookupValue[] | Area[] | Type[]) {
    return list.find(item => item.lvaValue === value)?.lvaMean;
  }

  checkDuplicateOrg(org: Organization): boolean {
    const map = new Map<string, Organization>();

    const listCheck = org ? org.children : this.listData;

    for (const item of listCheck ? listCheck : []) {
      if (this.checkDuplicateOrg(item)) {
        return true;
      }

      if (map.has(item.control?.value)) {
        item.control?.setErrors({ dup: true });
        map.get(item.control?.value)?.control?.setErrors({ dup: true });
        return true;
      } else {
        if (!item.control?.errors?.['required']) {
          item.control?.setErrors(null);
        }
        map.set(item.control?.value, item);
      }
    }
    return false;
  }


  backPrevious(stepper:MatStepper){
    this.steper = 0;
    stepper.previous();
  }
}
