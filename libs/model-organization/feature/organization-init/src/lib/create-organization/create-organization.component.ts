import { AbstractControl, Validators, FormControl, FormArray, FormGroup } from '@angular/forms';
import { Component, Injector, Input, OnDestroy, OnInit } from '@angular/core';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {
  Area,  FunctionOrgInit,
  ListLegalBranch,
  Organization,
  SelectObject, Template,
  Type
} from "@hcm-mfe/model-organization/data-access/models";
import {DataService, OrganizationService} from "@hcm-mfe/model-organization/data-access/services";
import {REGEX} from "@hcm-mfe/shared/common/constants";
import {FormModalShowTreeUnitComponent} from "@hcm-mfe/shared/ui/form-modal-show-tree-unit";

@Component({
  selector: 'app-create-organization',
  templateUrl: './create-organization.component.html',
  styleUrls: ['./create-organization.component.scss'],
})
export class CreateOrganizationComponent extends BaseComponent implements OnInit, OnDestroy {
  @Input() createOrganization: FormGroup;
  disable: boolean;
  parentName: string;
  showLegalBranch: boolean;
  viewEdit: boolean;
  viewInit: boolean;
  orgLevel: number | undefined;
  orgId: number | undefined;
  @Input() listData: Organization[];
  @Input() listView: Organization[];
  @Input() isSubmit: boolean;
  @Input() areas: Array<Area>;
  @Input() types: Array<Type>;
  @Input() validateObject;
  @Input() file;
  @Input() fileName: string;
  type = 'TM';
  showButtonCloseFile: boolean;
  @Input() allFruits: SelectObject<number, string>[] = [];
  listTemplate: Template[] = [];
  @Input() listLegalBranch: Array<ListLegalBranch> = [];
  @Input() dataDetail: any = {};
  orgSelected: number |undefined;

  constructor(
    injector: Injector,
    private readonly dataService: DataService,
    private readonly organizationService: OrganizationService,
  ) {
    super(injector);
    this.organizationService.getListTemplate().subscribe(data => {
      this.listTemplate = data.data;
    });
    this.disable = false;
    this.parentName = '';
    this.showLegalBranch = false;
    this.showButtonCloseFile = false;
    this.viewEdit = JSON.parse(localStorage.getItem('viewEdit'));
    this.viewInit = JSON.parse(localStorage.getItem('viewInit'));
    this.orgLevel = JSON.parse(localStorage.getItem('org_selected'))?.level;
    this.orgId = JSON.parse(localStorage.getItem('org_selected'))?.id;
    console.log(JSON.parse(localStorage.getItem('org_selected'))?.id)
  }

  ngOnInit(): void {

    if (this.orgId && !this.viewInit) {
      this.getDetailOrg();
    }
    if (this.viewInit) {
      this.orgLevel = undefined;
      localStorage.removeItem('org_selected');
      this.createOrganization.controls['parentUnit'].addValidators([Validators.required]);
    }

    if (!this.viewEdit) {
      const formControl: FormControl = new FormControl('', [
        Validators.pattern(REGEX.SPECIAL_TEXT_CODE),
        Validators.required,
      ]);
      (this.createOrganization.controls['formArr'] as FormArray).push(formControl);
      const formControlFun: FormControl = new FormControl('', [Validators.pattern(REGEX.SPECIAL_TEXT)]);
      (this.createOrganization.controls['fun'] as FormArray).push(formControlFun);
      const unit = {
        control: formControl,
        orgId: null,
        orgLevel: 2,
        function: [
          {
            value: formControlFun,
            job: [],
          },
        ],
        job: [],
        children: [],
        status: true,
      };
      this.listData.push(unit);
    }
  }

  callGetJob() {
    this.dataService.getJobsByType().subscribe((data) => {
      const listJob = data.data.content;
      listJob.forEach((element) => {
        if (!element.jobName) {
          element.jobName = '';
        }
        this.allFruits.push({
          id: element.jobId,
          value: element.jobName,
        });
      });
    });
  }
  getDetailOrg() {
    if(!this.orgId){
      return;
    }
    this.organizationService.getDetailOrg(this.orgId).subscribe(
      (data: any) => {
        this.createOrganization.controls['area'].setValue(data.data.region);
        this.createOrganization.controls['branch'].setValue(data.data.branchType);
        this.createOrganization.controls['legalBranch'].setValue(data.data.legalBranchId);
        this.selectBranch(this.createOrganization.value.branch);
        this.isLoading = false;
      },
      (error) => {
        this.isLoading = false;
      }
    );
  }
  handleFileInput(event: Event) {
    const target = event.target as HTMLInputElement;
    if (!target.files?.item(0) || this.fileName) {
      this.createOrganization.controls['fileName'].setErrors(null);
      return;
    }
    this.file.file = target.files[0];
    this.fileName = target.files[0].name;
    this.file.status = true;
    const fileExtend = this.fileName.split('.').pop();
    if (fileExtend !== 'pdf') {
      this.createOrganization.controls['fileName'].markAsDirty();
      this.createOrganization.controls['fileName'].updateValueAndValidity({ onlySelf: true });
      const error = this.createOrganization.controls['fileName'].errors;
      if (error) {
        error['extend'] = true;
        this.createOrganization.controls['fileName'].setErrors(error);
      } else {
        this.createOrganization.controls['fileName'].setErrors({ extend: true });
      }
    }
    if (target.files[0].size > 10000000) {
      this.createOrganization.controls['fileName'].markAsDirty();
      this.createOrganization.controls['fileName'].updateValueAndValidity({ onlySelf: true });
      const error = this.createOrganization.controls['fileName'].errors;
      if (error) {
        error['size'] = true;
        this.createOrganization.controls['fileName'].setErrors(error);
      } else {
        this.createOrganization.controls['fileName'].setErrors({ size: true });
      }
    }
    this.createOrganization.controls['attach'].setValue(this.fileName);
    this.showButtonCloseFile = true;
  }

  removeFile() {
    this.createOrganization.controls['fileName'].setValue(null);
    this.file.status = true;
    this.showButtonCloseFile = false;
  }

  removeChip() {
    this.createOrganization.controls['fileName'].setValue(null);
    this.fileName = '';
  }

  addOrg(parentOrg: Organization) {
    const formControl: FormControl = new FormControl('', [
      Validators.pattern(REGEX.SPECIAL_TEXT_CODE),
      Validators.required,
    ]);
    (this.createOrganization.controls['formArr'] as FormArray).push(formControl);
    const formControlFun: FormControl = new FormControl('', [Validators.pattern(REGEX.SPECIAL_TEXT)]);
    (this.createOrganization.controls['fun'] as FormArray).push(formControlFun);
    const item = {
      control: formControl,
      orgLevel: parentOrg.orgLevel + 1,
      function: [
        {
          value: formControlFun,
          job: [],
        },
      ],
      job: [],
      children: [],
      status: true,
    };
    if (!parentOrg.children) {
      parentOrg.children = [item as Organization];
    } else {
      parentOrg.children.push(item as Organization);
    }
  }

  addOrgLevel(list:Organization[], level:number) {
    const formControl: FormControl = new FormControl('', [
      Validators.pattern(REGEX.SPECIAL_TEXT_CODE),
      Validators.required,
    ]);
    (this.createOrganization.controls['formArr'] as FormArray).push(formControl);
    const formControlFun: FormControl = new FormControl('', [Validators.pattern(REGEX.SPECIAL_TEXT)]);
    (this.createOrganization.controls['fun'] as FormArray).push(formControlFun);
    const item = {
      control: formControl,
      orgLevel: level,
      function: [
        {
          value: formControlFun,
          job: [],
        },
      ],
      job: [],
      children: [],
      status: true,
    };
    list.push(item);
  }

  coppyOrg(list:Organization[], item:Organization) {
    const formControl: FormControl = new FormControl(item.control?.value, [
      Validators.pattern(REGEX.SPECIAL_TEXT_CODE),
      Validators.required,
    ]);
    (this.createOrganization.controls['formArr'] as FormArray).push(formControl);
    const newItem = { ...item };
    newItem.children = [];
    newItem.function = [];
    newItem.control = formControl;
    for (const fun of item.function ? item.function : []) {
      const formControlFun: FormControl = new FormControl(fun.value.value, [Validators.pattern(REGEX.SPECIAL_TEXT)]);
      (this.createOrganization.controls['fun'] as FormArray).push(formControlFun);
      if(!fun.job){
        fun.job = [];
      }
      newItem.function.push({
        value: formControlFun,
        job: [...fun.job],
      });
    }
    setTimeout(() => {
      this.findDuplicate(newItem.function);
    }, 0);
    list.push(newItem);
  }

  deleteOrg(list:Organization[], index:number) {
    if (list.length === 1) {
      return;
    }

    const item = list[index];
    const indexOfControl: number = this.getIndexControl(item.control);
    for (const fun of item.function ? item.function : []) {
      (this.createOrganization.controls['fun'] as FormArray).removeAt(this.getIndexControl(fun.value, 'FUN'));
    }
    list.splice(index, 1);
    (this.createOrganization.controls['formArr'] as FormArray).removeAt(indexOfControl);
  }

  deleteOrgChild(list:Organization[], index:number) {
    const item = list[index];
    const indexOfControl: number = this.getIndexControl(item.control);
    for (const fun of item.function ? item.function : []) {
      (this.createOrganization.controls['fun'] as FormArray).removeAt(this.getIndexControl(fun.value, 'FUN'));
    }
    list.splice(index, 1);
    (this.createOrganization.controls['formArr'] as FormArray).removeAt(indexOfControl);
  }

  createOrg() {
    if (!this.createOrganization.value.orgSelected) {
      return;
    }
    let dataOrg;
    while (this.listData.length) {
      this.listData.pop();
    }
    this.organizationService.getTemplateDetail(this.createOrganization.value.orgSelected).subscribe((data: any) => {
      dataOrg = data.data;
      this.buildOrgObject(dataOrg);
      this.toastrCustom.success('Sao chép thành công');
    });
  }

  addFunction(arr:any) {
    if (arr.filter((item:any) => item.value.value.trim() === '').length >= 1) {
      this.toastrCustom.error(this.translate.instant('modelOrganization.validate.orgName'));
      return;
    }
    const formControlFun: FormControl = new FormControl('', [Validators.pattern(REGEX.SPECIAL_TEXT)]);
    (this.createOrganization.controls['fun'] as FormArray).push(formControlFun);
    arr.push({
      value: formControlFun,
      job: [],
    });
    this.dataDetail.orgAttributeDTOS?.push({
      orgaValue: '',
      control: formControlFun,
      orgaId: null,
      positions: [],
    });
  }


  findDuplicate(formArray:FunctionOrgInit[] | undefined) {
    if(!formArray) {
      return;
    }
    const arr = [];
    const myArray = formArray;
    for (const item of myArray) {
      if (item.value.value) {
        arr.push(item.value.value.trim());
      }
    }
    for (const item of myArray) {
      const arrTemp = arr.filter((m) => m.trim() === item.value.value.trim());
      if (arrTemp.length > 1) {
        item.value.setErrors({ 'duplicate': true });
      } else {
        item.value.setErrors(null);
      }
    }
  }

  deleteFunction(arr:FunctionOrgInit[], index:number) {
    if (arr.length === 1) {
      return;
    }
    const item = arr[index];
    arr.splice(index, 1);
    this.dataDetail.orgAttributeDTOS.splice(index, 1);
    (this.createOrganization.controls['fun'] as FormArray).removeAt(this.getIndexControl(item.value, 'FUN'));
    setTimeout(() => {
      this.findDuplicate(arr);
    }, 0);
  }

  buildOrgObject(dataOrg : any) {
    (this.createOrganization.controls['fun'] as FormArray).clear();
    (this.createOrganization.controls['formArr'] as FormArray).clear();
    dataOrg.sort((a:any, b:any) => {
      if (a.orgLevel < b.orgLevel) {
        return -1;
      }
      if (a.orgLevel > b.orgLevel) {
        return 1;
      }
      return 0;
    });

    const map = new Map<string, any>();
    for (const item of dataOrg) {
      const formControlFun: FormControl = new FormControl('', [Validators.pattern(REGEX.SPECIAL_TEXT)]);
      (this.createOrganization.controls['fun'] as FormArray).push(formControlFun);

      const formControl: FormControl = new FormControl(item.orgName, [Validators.pattern(REGEX.SPECIAL_TEXT)]);
      (this.createOrganization.controls['fun'] as FormArray).push(formControlFun);
      item.control = formControl;
      item.job = [];
      if (item.posL) {
        for (const pos of item.posL) {
          if (!pos.jobId) {
            continue;
          }
          item.job.push({
            id: pos.jobId,
            value: this.getPosName(pos.jobId),
          });
        }
      }
      item.function = [
        {
          value: formControlFun,
          job: item.job,
        },
      ];
      map.set(item.orgId, item);
      if (item.parentId && item.orgLevel > 1) {
        const parent = map.get(item.parentId);
        if (parent.children) {
          parent.children.push(item);
        } else {
          parent.children = [];
          parent.children.push(item);
        }
      }
    }
    dataOrg = dataOrg.filter((item:any) => item.orgLevel === 2);
    this.listData.push(...dataOrg);
  }

  getPosName(jobId:number) {
    for (const item of this.allFruits) {
      if (item.id === jobId) {
        return item.value;
      }
    }
    return null;
  }
  showModalOrg(event:any): void {
    const modal = this.modal.create({
      nzWidth: '80vw',
      nzTitle: '',
      nzContent: FormModalShowTreeUnitComponent,
      nzFooter: null,
    });
    modal.afterClose.subscribe((result) => {
      if (result) {
        this.createOrganization.controls['parentUnit'].setValue(result?.orgName);
        localStorage.setItem('parentUnitId', result?.orgId);
        this.orgLevel = result?.orgLevel;
        if (this.orgLevel !== 0 && this.orgLevel !== 1 && this.orgLevel !== undefined) {
          this.isLoading = true;
          this.orgId = result.orgId;
          this.getDetailOrg();
        } else {
          this.disable = true;
          this.setValueNull();
        }
      }
    });
  }
  clearOrgInput() {
    localStorage.removeItem('parentUnitId');
    this.createOrganization.controls['parentUnit'].setValue(null);
    this.orgLevel = undefined;
    this.orgId = undefined;
    this.setValueNull();
  }
  setValueNull() {
    this.createOrganization.controls['area'].setValue(null);
    this.createOrganization.controls['branch'].setValue(null);
    this.createOrganization.controls['legalBranch'].setValue(null);
    this.selectBranch(this.createOrganization.value.branch);
  }
  selectBranch(event:string) {
    const template = this.listTemplate.find(el => el.branchType === event)
    if (!event) {
      this.createOrganization.controls['orgSelected'].setValue(null);
      return;
    }
    if (template) {
      this.createOrganization.controls['orgSelected'].setValue(template.orgId)
    } else {
      this.createOrganization.controls['orgSelected'].setValue(null)
    }
    if (event === 'PGD Online' || event === 'TX8') {
      this.showLegalBranch = true;
      this.createOrganization.controls['legalBranch'].setValidators(Validators.required);
    } else {
      this.showLegalBranch = false;
      this.createOrganization.controls['legalBranch'].clearValidators();
    }
  }

  getIndexControl(controlEl: FormControl | undefined, type?: string): number {
    if(!controlEl){
      return -1;
    }
    let controls: AbstractControl[];
    if (type === 'FUN') {
      controls = (this.createOrganization.controls['fun'] as FormArray).controls;
    } else {
      controls = (this.createOrganization.controls['formArr'] as FormArray).controls;
    }
    const indexOfControl: number = controls.findIndex((control) => control === controlEl);
    return indexOfControl;
  }

  checkDuplicate(control: FormControl, event?: any, dataList?: Organization[]) {
    if (event?.key === 'Shift' || event?.key === 'Unidentified' || event?.key === 'Backspace') {
      return;
    }
    const index = this.getIndexControl(control);
    const value = control.value.toUpperCase();
    const setValue: Set<string> = new Set<string>();
    for (let i = 0; i < (this.createOrganization.controls['formArr'] as FormArray).length; i++) {
      if (i === index) {
        continue;
      }
      setValue.add((this.createOrganization.controls['formArr'] as FormArray).controls[i].value.toUpperCase());
    }

    if (setValue.has(value)) {
      for (const item of dataList ? dataList : []) {
        if (item.control !== control && item.control?.value === control.value) {
          return;
        }
      }
      this.toastrCustom.warning('Trùng tên đơn vị');
    }
  }

  updateJob(value:boolean) {
    if (value) {
      while (this.allFruits.length) {
        this.allFruits.pop();
      }
      this.callGetJob();
    }
  }

  override ngOnDestroy() {
    localStorage.removeItem('parentUnitId');
    localStorage.removeItem('viewInit');
    localStorage.removeItem('viewEdit');
    localStorage.removeItem('org_selected');
  }
}
