
export interface InformationForm {
    reason: string,
    dateVisible: string,
    attach: string,
    fileName: string
}
export interface InformationOrganization {
    parentUnit: string,
    nameDependUnit: string,
    dateCreated: string,
    area: string,
    branch: string,
    quantity:string,
    address:string,
    phone:string,
    fax:string,
    codeTS24:string,
    datePublish:string,
    dateOfFoundation:string,
    unitLevel:string,
    dateExpiration:string
}
export interface InitPosition{
    parentOrg:string,
    orgName:string,
    function: Array<string>,
    job:[]

}

export interface JobInitPosition {
    job?:[] | any,
    function: Array<string>
}



export interface ResquestOrganization {
    sap: {
        sapDate: string,
        reason: string,
    },
    org: {
        orgCode: string,
        orgName: string,
        parentId: string,
        fromDate: string,
        toDate: string,
        t24Code: string,
        orgLevel: number,
        address: string,
        telephone: string,
        fax: string,
        regionId: number,
        orgTypeId:number,
        issuedDate: string
    },
    posL: PositionRequest[]
}

export interface PositionRequest {
    code: string,
    name:string,
    pgrId:number,
    jobId: number,
    psd : {
        psdCode: string,
        psdName: string,
        jdId: number,
        orgaId: number
    }
}


