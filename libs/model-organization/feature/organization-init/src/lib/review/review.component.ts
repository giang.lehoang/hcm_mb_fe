import { Component,Input, OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import {DataService} from "@hcm-mfe/model-organization/data-access/services";
import {Area, ListLegalBranch, Organization, Type} from "@hcm-mfe/model-organization/data-access/models";

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.scss'],
})
export class ReviewComponent implements OnInit, OnDestroy {
  showLegalBranch: boolean | undefined;
  loadingFile :boolean | undefined;
  @Input() nameSelectedLegalBranch: string | undefined;
  orgLevel: number | undefined;
  isVisible:boolean | undefined;
  pdfSrc:string | undefined;

  constructor(private readonly unitApproveService: DataService) {
    this.fileName = '';
  }

  fileName: string;
  @Input() areas: Array<Area> = [];
  @Input() types: Array<Type> = [];
  @Input() createOrganization: FormGroup | undefined;
  @Input() listData: Organization[] = [];
  @Input() listLegalBranch: Array<ListLegalBranch> = [];
  @Input() file:any;

  ngOnInit(): void {
    this.showLegalBranch = false;
    this.isVisible =false;
    this.loadingFile = true;
    this.orgLevel = JSON.parse(localStorage.getItem('org_selected'))?.level;
  }



  // handleFileInput(files) {
  //   const data = new FormData();
  //   this.createOrganization.controls['informationForm'].value.attach = data;
  //   this.createOrganization.controls['informationForm'].value.fileName = files[0].name;
  //   this.fileName = files[0].name;
  // }

  createDate(date:string) {
    return new Date(date);
  }
  downloadFile(id:number) {
    this.unitApproveService.downloadFile(String(id)).subscribe((data) => {
      const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.pdf' });
      this.file.file = new File([blob], this.file.fileName);
      this.file.status = false;
      this.onFileSelected();
    });
  }
  onFileSelected(): void {
    const reader = new FileReader();

    reader.onload = (e: any) => {
      this.pdfSrc = e.target.result;
    };

    reader.readAsArrayBuffer(this.file.file);
    this.loadingFile = false;
    this.isVisible = true
  }
  showPopup(){
    if(!this.file.status && this.loadingFile){
      this.downloadFile(this.createOrganization?.value.sheetApproval.mpDoc?.docId);
      console.log('down')
    } else {
      this.onFileSelected();
    }
  }

  handleCancel(): void {
    this.isVisible = false;
  }
  ngOnDestroy(): void {
    localStorage.removeItem('viewDetail');
    localStorage.removeItem('org_selected');
  }
}
