import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {FunctionMangementComponent} from "./function-management/function-management.component";
import {CreateOrganizationComponent} from "./create-organization/create-organization.component";
import {ReviewComponent} from "./review/review.component";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {MatStepperModule} from "@angular/material/stepper";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {ReactiveFormsModule} from "@angular/forms";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzModalModule} from "ng-zorro-antd/modal";
import {PdfViewerModule} from "ng2-pdf-viewer";
import {RouterModule} from "@angular/router";
import {MatButtonModule} from "@angular/material/button";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {ModelOrganizationUiJobSelectModule} from "@hcm-mfe/model-organization/ui/job-select";
import {MatIconModule} from "@angular/material/icon";
import {NzGridModule} from "ng-zorro-antd/grid";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import { CdkStepperModule } from '@angular/cdk/stepper';
import { MatChipsModule } from '@angular/material/chips';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import {NzStepsModule} from "ng-zorro-antd/steps";
import {MatMenuModule} from "@angular/material/menu";

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule,MatMenuModule, MatStepperModule, MatButtonModule, TranslateModule, RouterModule.forChild([
    {
      path: '',
      component: FunctionMangementComponent,
    }]), SharedUiMbInputTextModule, NzStepsModule, NzDividerModule,
    SharedUiMbButtonModule, ReactiveFormsModule, NzToolTipModule, NzTableModule, NzModalModule, PdfViewerModule, SharedUiMbSelectModule,
    ModelOrganizationUiJobSelectModule, MatIconModule, NzGridModule, SharedUiMbDatePickerModule,
    CdkStepperModule, MatChipsModule, NzIconModule, NzTypographyModule, NzFormModule, NzDividerModule],
  declarations: [FunctionMangementComponent, CreateOrganizationComponent, ReviewComponent],
  exports: [FunctionMangementComponent, CreateOrganizationComponent, ReviewComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ModelOrganizationFeatureOrganizationInitModule {}
