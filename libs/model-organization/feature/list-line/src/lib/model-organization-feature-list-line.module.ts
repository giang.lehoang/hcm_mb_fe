import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ListLineComponent} from "./list-line/list-line.component";
import {SharedUiMbSelectCheckAbleModule} from "@hcm-mfe/shared/ui/mb-select-check-able";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {NzCardModule} from "ng-zorro-antd/card";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {NzGridModule} from "ng-zorro-antd/grid";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import {FormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {NzTagModule} from "ng-zorro-antd/tag";

@NgModule({
    imports: [CommonModule, SharedUiMbSelectCheckAbleModule, SharedUiLoadingModule, NzGridModule,
        RouterModule.forChild([
            {
                path: '',
                component: ListLineComponent,
            }]),
        NzCardModule, TranslateModule, SharedUiMbDatePickerModule, NzGridModule, SharedUiMbSelectModule, SharedUiMbButtonModule,
        NzTableModule, NzPaginationModule, FormsModule, NzTagModule],
  declarations: [ListLineComponent],
  exports: [ListLineComponent]
})
export class ModelOrganizationFeatureListLineModule {}
