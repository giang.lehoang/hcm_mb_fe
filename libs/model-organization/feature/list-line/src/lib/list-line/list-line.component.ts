import { Component, Injector, OnInit } from '@angular/core';
import * as moment from 'moment';
import { NzModalRef } from 'ng-zorro-antd/modal';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {Pagination} from "@hcm-mfe/model-organization/data-access/models";
import {userConfig} from "@hcm-mfe/shared/common/constants";
import { FunctionCode } from '@hcm-mfe/shared/common/enums';
import {DataLine, SearchLineObj} from "../../../../../data-access/models/src/lib/line.model";
import {LineService} from "@hcm-mfe/model-organization/data-access/services";
import {renderFlagStatus} from "../../../../../helper/common.function";
@Component({
  selector: 'app-list-line',
  templateUrl: './list-line.component.html',
  styleUrls: ['./list-line.component.scss'],
})
export class ListLineComponent extends BaseComponent implements OnInit {
  confirmModal?: NzModalRef;
  searchObj: SearchLineObj = {
    name: null,
    flagStatus: null,
    fromDate: null,
    page: 0,
    size: 20,
    ids: [],
  };

  loadingPage: boolean;
  isLoadingMore: boolean;

  renderFlagStatus = renderFlagStatus;

  status = [
    { value: 1, name: 'Đang hoạt động' },
    { value: 2, name: 'Chờ duyệt' },
    { value: 3, name: 'Từ chối' },
    { value: 5, name: 'Tạo mới' },
]

  pagination: Pagination = new Pagination(userConfig.pageSize);

  lineList: Array<DataLine> = [];
  initData: Array<DataLine> = [];
  constructor(
    injector: Injector,
    readonly lineService: LineService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(
      `FUNCTION_${FunctionCode.LIST_LINE}`
    );

    this.isLoading = false;
    this.loadingPage = true;
    this.isLoadingMore = false;

    this.search();
  }

  ngOnInit(): void {
    this.isLoading = false;
    this.onSearch();
  }
  callAPISearch(obj:any){
    this.lineService.searchListLineView(obj).subscribe(
      (data) => {
        this.initData = data.data.content;
        this.pagination.totalElements = data.data.totalElements;
        this.pagination.size = data.data.size;
        this.loadingPage = false;
      },
      (error) => {
        this.toastrCustom.error(error?.error.description);
      }
    );
  }

  changePage() {
    this.loadingPage = true;
    this.searchObj.page = this.pagination.currentPage - 1;
    const objTmp = { ...this.searchObj }
    objTmp.fromDate = objTmp.fromDate ? moment(objTmp.fromDate).format('YYYY-MM-DD') : null;
    this.callAPISearch(objTmp);
  }

  onSearch() {
    const objTmp = { ...this.searchObj };
     objTmp.page = 0;
     objTmp.size = 99999;
    this.lineService.searchListLineView(objTmp).subscribe((data) => {
      this.lineList = data.data.content;
      this.isLoading = false;
    });
  }


  search() {
    this.loadingPage = true;
    this.searchObj.page = 0;
    const objTmp = { ...this.searchObj }
    objTmp.fromDate = objTmp.fromDate ? moment(objTmp.fromDate).format('YYYY-MM-DD') : null;
    this.callAPISearch(objTmp);
  }

  deleteLine(id:number, index:number) {
    this.deletePopup.showModal(() => {
      this.lineService.deleteLine(id).subscribe(
        (_) => {
          this.toastrCustom.success('Xóa line thành công');
          this.initData.splice(index, 1);
          this.initData = [...this.initData];
        },
        (_) => {
          this.toastrCustom.error('Xóa line thất bại');
        }
      );
    });
  }

  showConfirm(idLine:number, index:number): void {
    const message = 'Bạn có muốn xóa line?';
    this.confirmModal = this.modal.confirm({
      nzTitle: message,
      nzContent: '',
      nzOnOk: () => {
        const id = this.message.loading('Action in progress..', { nzDuration: 0 }).messageId;
        this.lineService.deleteLine(idLine).subscribe(
          (_) => {
            this.message.remove(id);
            this.toastrCustom.success('Xóa line thành công');
            this.initData.splice(index, 1);
            this.initData = [...this.initData];
          },
          (_) => {
            this.message.remove(id);
            this.toastrCustom.error('Xóa line thất bại');
          }
        );
      },
    });
  }

  viewDetail(id:number) {
    localStorage.setItem('selectedIdLine',String(id));
    this.router.navigateByUrl('/organization/line-group/manage');
  }


  onSelectLine(e:any) {
    this.searchObj.ids = [...e.listOfSelected]
  }

  displaStatus(status:any) {
   return this.status.find(item => item.value === status)?.name;
  }

  override triggerSearchEvent(): void {
      this.search();
  }
}
