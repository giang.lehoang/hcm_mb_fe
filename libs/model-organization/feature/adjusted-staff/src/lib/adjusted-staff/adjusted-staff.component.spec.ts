import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdjustedStaffComponent } from './adjusted-staff.component';

describe('AdjustedStaffComponent', () => {
  let component: AdjustedStaffComponent;
  let fixture: ComponentFixture<AdjustedStaffComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdjustedStaffComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdjustedStaffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
