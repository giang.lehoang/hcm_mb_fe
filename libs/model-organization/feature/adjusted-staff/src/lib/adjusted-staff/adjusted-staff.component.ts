import { Component, Injector, OnInit } from '@angular/core';
import {
  FillModel,
  InitStaffSearch,
  Job,
  PaginationPlan, QUANTITY_DEFAULT, REGEX_NUMBER_NEGATIVE,
  StaffData,
  StaffOrganizationNode,
  Year
} from '@hcm-mfe/model-organization/data-access/models';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { AdjustedStaffService, UploadFileService } from '@hcm-mfe/model-organization/data-access/services';
import { CustomToastrService } from '@hcm-mfe/shared/core';
import { DataService } from '@hcm-mfe/shared/data-access/services';
import { FunctionCode } from '@hcm-mfe/shared/common/enums';
import { forkJoin } from 'rxjs';
import { FormModalShowTreeUnitComponent } from '@hcm-mfe/shared/ui/form-modal-show-tree-unit';
import * as FileSaver from 'file-saver';
import { MICRO_SERVICE } from '@hcm-mfe/shared/common/constants';

@Component({
  selector: 'app-adjusted-staff',
  templateUrl: './adjusted-staff.component.html',
  styleUrls: ['./adjusted-staff.component.scss'],
  providers: [UploadFileService]

})
export class AdjustedStaffComponent extends BaseComponent implements OnInit {

  objectSearch: InitStaffSearch;
  checked = false;
  indeterminate = false;
  dataTable: StaffData[] = [];
  quantityMap: Map<number, StaffData> = new Map<number, StaffData>();
  dataApproveAll: Map<number, StaffData> = new Map<number, StaffData>();
  params: InitStaffSearch;
  pagination: PaginationPlan;
  isLoadingPage = false;
  listYears: Year[] = [];
  listJob: Job[] = [];
  messageSuccess = '';
  isFillQuick = false;
  objFill: FillModel = {
    adjustedNumber: "",
    reason: ""
  };
  curentYear: string;
  constructor(
    injector: Injector,
    public uploadFile: UploadFileService,
    public toast: CustomToastrService,
    public dataService: DataService,
    public adjustedStaffService: AdjustedStaffService,
  ) {
    super(injector);
    this.curentYear = String(new Date().getFullYear());
    this.objFunction = this.sessionService.getSessionData(
      `FUNCTION_${FunctionCode.ADJUSTED_STAFF}`
    );
    const org: StaffOrganizationNode = JSON.parse(localStorage.getItem('org_selected'));

    this.objectSearch = {
      jobId: '',
      periodCode: this.curentYear,
      page: 0,
      size: 15,
      orgId: org?.id,
      orgName: org?.name,
    };
    this.pagination = {
      currentPage: 1,
      totalElement: 0,
      size: 15,
      totalPage: 0,
      numberOfElements: 0,
    };
    this.params = {
      ...this.objectSearch,
    };
  }

  ngOnInit(): void {

    console.log(this.objFunction);
    this.params.inputType = "DC";

    this.isLoadingPage = false;

    this.fetchData(true);

    this.messageSuccess = this.translate.instant('modelOrganization.message.tranferSuccess');
  }
  override ngOnDestroy() {
    localStorage.removeItem('org_selected');
  }

  fetchData(init: boolean) {
    this.isLoadingPage = true;
    const request = [];
    if (init) {
      request.push(this.dataService.getJobsByType());
    }

    request.push(this.adjustedStaffService.getListAdjusted(this.params));

    forkJoin(request).subscribe(
      (data: any) => {
        let plans;
        if (init) {
          this.listJob = data[0].data.content;
          plans = data[1].data;
        } else {
          plans = data[0].data;
        }
        this.dataTable = plans.content;
        this.pagination.totalElement = plans.totalElements;
        this.pagination.totalPage = plans.totalPages;
        this.pagination.numberOfElements = plans.numberOfElements;
        this.pagination.currentPage = plans.pageable.pageNumber;
        this.checked = false;
        this.setData();
        if (!this.dataTable.length) {
          this.checked = false;
        }
        this.isLoadingPage = false;
        this.checkValidation(false);
      },
      (error) => {
        this.isLoadingPage = false;
        this.dataTable = [];
        this.listYears = [];
        this.listJob = [];
        this.toast.error(error?.message);
      }
    );
  }

  updateCheckedSet(item: StaffData, checked: boolean): void {
    if (checked) {
      this.quantityMap.set(item.hcpId, item);
    } else {
      this.quantityMap.delete(item.hcpId);
    }
  }

  onCurrentPageDataChange(listOfCurrentPageData: StaffData[]): void {
    this.dataTable = listOfCurrentPageData;
    this.refreshCheckedStatus();
  }

  refreshCheckedStatus(): void {
    this.checked = this.dataTable.every(({ hcpId }) => this.quantityMap.has(hcpId));
  }

  onItemChecked(item: StaffData, checked: boolean): void {
    this.updateCheckedSet(item, checked);
    this.refreshCheckedStatus();
  }

  onAllChecked(checked: boolean): void {
    this.dataTable.forEach((item) => this.updateCheckedSet(item, checked));
    this.refreshCheckedStatus();
  }

  changePage(value:any) {
    this.params.page = value - 1;
    this.fetchData(false);
  }

  onSearch() {
    this.params.jobId = this.objectSearch.jobId ? this.objectSearch.jobId : '';
    this.params.orgId = this.objectSearch.orgId ? this.objectSearch.orgId : '';
    this.params.periodCode = this.objectSearch.periodCode ? this.objectSearch.periodCode : '';
    this.params.page = 0;
    this.fetchData(false);
    this.quantityMap.clear();
    this.clearFill();
  }

  showUpload() {
    const requestInfo = {
      url: this.adjustedStaffService.getUrlUpload('DC'),
      file: null,
      service:MICRO_SERVICE.MODEL_PLAN
  };
    this.uploadFile.showModal(
      () => {
        this.dataApproveAll.clear();
        this.onSearch();
      },
      requestInfo,
      this.translate.instant('modelOrganization.message.uploadSuccess')
    );
  }

  showPopup() {
    const widthTmp = window.innerWidth / 1.5 > 1100 ? 1368 : window.innerWidth / 1.5;
    const modal = this.modal.create({
      nzWidth:
        window.innerWidth > 767 ? widthTmp : window.innerWidth,
      nzTitle: ' ',
      nzComponentParams: {
        isAuthor: true
      },
      nzContent: FormModalShowTreeUnitComponent,
      nzFooter: null,
    });

    modal.afterClose.subscribe((result) => {
      if (result?.orgName) {
        this.objectSearch.orgName = result?.orgName;
        this.objectSearch.orgId = result?.orgId;
      }
    });
  }

  approveTranfer() {
    this.objFill.reason = this.objFill.reason ? this.objFill.reason.trim() : ""
    if (!this.quantityMap.size) {
      this.dataTable.forEach(item => item.isChecked = false);
      this.toast.error(this.translate.instant('modelOrganization.planYear.chooseMin'));
      return;
    }
    if (!this.checkValidation(true)) {
      return;
    }
    const approve:any = [];
    this.quantityMap.forEach((value) => {
      approve.push({
        hcpId: value.hcpId,
        adjustedNumber: value.adjustedNumber ? value.adjustedNumber : QUANTITY_DEFAULT,
        reason: value.reason ? value.reason : "",
      });
    });
    const request = {
      headCountPlanDTOS: approve,
      inputType: "DC"
    }
    this.isLoadingPage = true;
    this.adjustedStaffService.approveTranfer(request).subscribe(
      (data) => {
        this.isLoadingPage = false;
        this.quantityMap.clear();
        this.toast.success(this.messageSuccess);
        this.onSearch();
      },
      (error) => {
        this.isLoadingPage = false;
        this.toast.error(error?.message);
      }
    );
  }

  exportExcel() {
    const excelExport = {
      orgId: this.params.orgId ? Number(this.params.orgId) : "",
      jobId: this.params.jobId ? Number(this.params.jobId) : "",
      inputType: "DC"
    };
    this.adjustedStaffService.downloadExcel(excelExport).subscribe(
      (data) => {
        const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
        FileSaver.saveAs(blob, 'Template.xlsx');
      },
      (error) => {
        this.toast.error(error?.error?.message);
      }
    );
  }

  onKeypress(event:any) {
    const pattern = REGEX_NUMBER_NEGATIVE;
    if (!pattern.test(event.key)) {
      event.preventDefault();
      return;
    }

    const value = parseInt(event.target.value + event.key);

    if (value > 999 || value < -999) {
      event.preventDefault();
      return;
    }

    if (value === 0 && event.target.value.length === 1) {
      event.preventDefault();
    }
  }
  onKeyUp(event:any) {
    const value = event.target.value
    if (value.substring(1).includes("-")) {
      event.target.value = "";
    }
  }

  clearOrgInput() {
    this.objectSearch.orgName = '';
    this.objectSearch.orgId = '';
  }

  onClick(event:any) {
    if (+event.target.value === 0) {
      event.target.select();
    }
  }
  changeFill(e:any) {
    this.isFillQuick = e;
  }
  assignInfo() {
    this.objFill.reason = this.objFill.reason ? this.objFill.reason.trim() : ""
    if (this.quantityMap.size <= 0) {
      this.toast.error(this.translate.instant('modelOrganization.planYear.chooseMin'));
      return;
    }
    const arrHpcId:any = [];
    this.quantityMap.forEach((value, key) => {
      value.adjustedNumber = Number(this.objFill.adjustedNumber);
      value.reason = this.objFill.reason ? this.objFill.reason : "";
      arrHpcId.push(key);
    });
    this.dataTable.forEach((data) => {
      if (arrHpcId.indexOf(data.hcpId) >= 0) {
        data.adjustedNumber = Number(this.objFill.adjustedNumber);
        data.reason = this.objFill.reason ? this.objFill.reason : "";
      }
    });

  }
  checkValidation(isTransfer:any) {
    const arrHpcId:any[] = [];
    const arrSelect:any[] = [];
    let rowError;
    this.quantityMap.forEach((value, key) => {
      arrHpcId.push(key);
      arrSelect.push(value);
    });
    this.dataTable.forEach((data) => {
      if (arrHpcId.indexOf(data.hcpId) >= 0) {
        let itemSelected;
        data.isChecked = true;
        itemSelected = arrSelect.find((v:StaffData) => v.hcpId === data.hcpId);
        data.adjustedNumber = itemSelected.adjustedNumber ? itemSelected.adjustedNumber : "";
        data.reason = itemSelected.reason ? itemSelected.reason : "";
      }
    })
    let arrError ;
    arrError = arrSelect.filter((item:StaffData) => !item.adjustedNumber || !item.reason);
    rowError = arrError[0];
    if (arrError.length > 0 && !isTransfer) {
      this.toast.warning(this.translate.instant('modelOrganization.adjustedStaff.row') + rowError.index
        + this.translate.instant('modelOrganization.adjustedStaff.page') + rowError.page + this.translate.instant('modelOrganization.adjustedStaff.notEmpty'));
      return false;
    } else if (isTransfer && arrError.length > 0) {
      this.toast.error(this.translate.instant('modelOrganization.adjustedStaff.errorMessage'));
      return false;
    } else {
      return true;
    }
  }
  onPaste(event:any) {
    const value = event.clipboardData.getData('text');
    if (!Number(value) || Number(value) > 999 || Number(value) < - 999) {
      event.preventDefault();
    }
  }
  clearFill() {
    this.objFill.adjustedNumber = "";
    this.objFill.reason = "";
  }
  setData(){
    this.dataTable.forEach((item, index) => {
      item.page = this.pagination.currentPage + 1;
      item.index = index + 1;
      if (!this.quantityMap.has(item.hcpId)) {
        this.checked = false;
      } else {
        this.quantityMap.set(item.hcpId, <StaffData>this.quantityMap.get(item.hcpId));
      }

      if (this.dataApproveAll.has(item.hcpId)) {
        item.quantity = this.dataApproveAll.get(item.hcpId).quantity;
      } else {
        item.quantity = item.headCount ? item.headCount : '';
      }
      this.dataApproveAll.set(item.hcpId, item);
    });

  }

  override triggerSearchEvent(): void {
    this.onSearch();
  }
}
