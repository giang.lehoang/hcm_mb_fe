import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompetencyTableComponent } from './competency-table.component';

describe('CompetencyTableComponent', () => {
  let component: CompetencyTableComponent;
  let fixture: ComponentFixture<CompetencyTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompetencyTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompetencyTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
