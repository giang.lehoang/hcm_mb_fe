import { Component, Injector, Input } from '@angular/core';
import { AbstractControl, FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {Competency, DetailTable, MajorLevel, Submit, SubTask, TaskTable} from "@hcm-mfe/model-organization/data-access/models";

@Component({
  selector: 'app-competency-table',
  templateUrl: './competency-table.component.html',
  styleUrls: ['./competency-table.component.scss'],
})
export class CompetencyTableComponent extends BaseComponent  {
  @Input() taskTable: TaskTable[] = [];
  @Input() degreeTable: DetailTable[] = [];
  @Input() certTable: DetailTable[] = [];
  @Input() languageTable: DetailTable[] = [];
  @Input() competencyList: Competency[] = [];
  @Input() competencyTable: DetailTable[] = [];
  @Input() listMajorLevel: MajorLevel[] = [];
  @Input() formJD: FormGroup | undefined;
  @Input() submited:Submit | undefined;
  @Input() disable:boolean | undefined;
  @Input() mode: 'create' |'select' | 'view' | undefined;

  constructor(injector: Injector) {
    super(injector);
  }


  addTask(index: number): void {
    if(this.disable){
      return;
    }
    const taskTable: TaskTable = {
      taskName: new FormControl('', Validators.required),
      sub: [],
    };
    this.taskTable.splice(index + 1, 0, taskTable);
    this.addSubTask(0, taskTable.sub);
    (this.formJD?.controls['task'] as FormArray).push(taskTable.taskName);
  }

  addSubTask(index: number, subs: SubTask[]): void {
    if(this.disable){
      return;
    }
    const subTask: SubTask = {
      taskName: new FormControl('', Validators.required),
      result: new FormControl('', Validators.required),
    };
    subs.splice(index + 1, 0, subTask);
    (this.formJD?.controls['subTask'] as FormArray).push(subTask.taskName);
    (this.formJD?.controls['result'] as FormArray).push(subTask.result);
  }

  addJdDetail(index: number, list: DetailTable[], type: number): void {
    if(this.disable){
      return;
    }
    const detailTable: DetailTable = {
      value: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
    };

    list.splice(index + 1, 0, detailTable);

    switch (type) {
      case 1: {
        (this.formJD?.controls['degree'] as FormArray).push(detailTable.value);
        (this.formJD?.controls['degreeDes'] as FormArray).push(detailTable.description);
        return;
      }
      case 2: {
        (this.formJD?.controls['cert'] as FormArray).push(detailTable.value);
        (this.formJD?.controls['certDes'] as FormArray).push(detailTable.description);
        return;
      }
      case 3: {
        (this.formJD?.controls['fLang'] as FormArray).push(detailTable.value);
        (this.formJD?.controls['fLangDes'] as FormArray).push(detailTable.description);
        return;
      }
      case 4: {
        (this.formJD?.controls['competency'] as FormArray).push(detailTable.value);
        (this.formJD?.controls['capacity'] as FormArray).push(detailTable.description);
        return;
      }
    }
  }

  deleteItem(index: number, list: TaskTable[] | DetailTable[] | SubTask[]): void {
    if (list.length === 1) {
      return;
    }
    list.splice(index, 1);
  }

  deleteTask(index: number, item: TaskTable): void {
    if(this.taskTable.length === 1 || this.disable){
      return;
    }
    this.deleteFormControl(item.taskName, this.formJD?.controls['task'] as FormArray);
    for (let i = 0; i < item.sub.length; i++) {
      this.deleteFormControl(item.sub[i].taskName, this.formJD?.controls['subTask'] as FormArray);
      this.deleteFormControl(item.sub[i].result, this.formJD?.controls['result'] as FormArray);
      this.deleteItem(i, item.sub);
    }
    this.deleteItem(index, this.taskTable);
  }

  deleteSubTask(index: number, item: TaskTable):void{
    if(item.sub.length === 1 || this.disable){
      return;
    }
    const sub = item.sub[index];
    this.deleteFormControl(sub.taskName, this.formJD?.controls['subTask'] as FormArray);
    this.deleteFormControl(sub.result, this.formJD?.controls['result'] as FormArray);
    this.deleteItem(index, item.sub);
  }

  deleteDetail(index: number, list: DetailTable[], type:number){
    if(list.length === 1 || this.disable){
      return;
    }
    switch (type) {
      case 1: {
        this.deleteFormControl(list[index].value, this.formJD?.controls['degree'] as FormArray);
        this.deleteFormControl(list[index].description, this.formJD?.controls['degreeDes'] as FormArray);
        break;
      }
      case 2: {
        this.deleteFormControl(list[index].value, this.formJD?.controls['cert'] as FormArray);
        this.deleteFormControl(list[index].description, this.formJD?.controls['certDes'] as FormArray);
        break;
      }
      case 3: {
        this.deleteFormControl(list[index].value, this.formJD?.controls['fLang'] as FormArray);
        this.deleteFormControl(list[index].description, this.formJD?.controls['fLang'] as FormArray);
        break;
      }
      case 4: {
        this.deleteFormControl(list[index].value, this.formJD?.controls['competency'] as FormArray);
        this.deleteFormControl(list[index].description, this.formJD?.controls['capacity'] as FormArray);
        break;
      }
    }
    this.deleteItem(index, list);
  }

  deleteFormControl(control: FormControl, list: FormArray): void {
    const controls: AbstractControl[] = list.controls;
    for (let i = 0; i < controls.length; i++) {
      if (controls[i] === control) {
        list.removeAt(i);
        break;
      }
    }
  }

  selectCompetency(value: string, compen: DetailTable): void {
    for (const item of this.competencyList) {
      if (item.competenceFrames === value) {
        compen.selectTemp = item.competenceJobGroupList;
        return;
      }
    }
  }

  selectCapacity($event: any, data: DetailTable) {
    this.competencyTable.forEach(com => {
      if(data !== com && com.description.value === data.description.value && com.value.value === data.value.value){
        setTimeout(() => {
          data.value.setValue('');
        }, 500);
        this.toastrCustom.error("Trùng khung năng lực");
      }
    })

  }
}
