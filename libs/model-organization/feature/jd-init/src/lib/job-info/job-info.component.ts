import { Component, EventEmitter, Injector, Input, OnInit, Output } from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {
  ApproveJD,
  JDsModel,
  Job,
  LookupValue,
  ASSIGNMENT_TYPE,
  Submit,
  FileInfo
} from "@hcm-mfe/model-organization/data-access/models";
import {JDService} from "../../../../../data-access/services/src/lib/jd.service";
import {PositionPopupComponent} from "../position-popup/position-popup.component";
import {DataService} from "@hcm-mfe/model-organization/data-access/services";
@Component({
  selector: 'app-job-info',
  templateUrl: './job-info.component.html',
  styleUrls: ['./job-info.component.scss'],
})
export class JobInfoComponent extends BaseComponent implements OnInit {
  @Input() jobOrgs: Job[] = [];
  @Input() formJD: FormGroup | undefined;
  @Input() listRole: LookupValue[] = [];
  @Input() submited:Submit | undefined;
  @Output() competency: EventEmitter<JDsModel> = new EventEmitter();
  @Input() mode: 'create' |'select' | 'view' | undefined;
  @Input() disable: boolean | undefined;
  @Input() approveJd:ApproveJD | undefined;
  JDSelect: JDsModel | null | undefined;
  assignType = ASSIGNMENT_TYPE;
  @Input() fileInfo:FileInfo = new FileInfo();
  pdfSrc:string;
  isVisible:boolean;

  constructor(injector: Injector, readonly jdService: JDService, private readonly dataService:DataService) {
    super(injector);
    const item = localStorage.getItem('posInfo');
    if(item){
      this.JDSelect = JSON.parse(item);
    }
    console.log(item);
    this.pdfSrc = '';
    this.isVisible = false;
  }

  ngOnInit(): void {
    this.formJD?.controls['assignBy'].setValue(this.assignType[0].key);

    if(this.JDSelect){
      this.setForm();
    }
  }

  showPosition(): void {
    const modalRef = this.modal.create({
      nzTitle: 'Position',
      nzContent: PositionPopupComponent,
      nzWidth: this.getInnerWidth(1.5),
      nzFooter: null,
    });

    modalRef.afterClose.subscribe((data) => {
      if (data?.objectData?.jobGroup) {
        this.JDSelect = data?.objectData;
        this.setForm(true);
      }
    });
  }

  setForm(select?:boolean):void{
    this.formJD?.controls['function'].setValue(this.JDSelect?.orgaValue);
    if(!this.JDSelect?.orgaValue){
        if(this.JDSelect?.orgId) this.jdService.getAttribute(this.JDSelect.orgId).subscribe((data) => {
        const fun = data.data.map(item => item.orgaValue);
        if(this.JDSelect) this.JDSelect.orgaValue = fun.join('/');
        this.updateValue();
      }, (error) => {
        this.toastrCustom.error(error.message);
      })
    } else {
      this.updateValue();
    }

    if(select){
      this.competency.emit(this.JDSelect ? this.JDSelect : undefined);
    }
  }

  updateValue():void{
    const value = this.formJD?.value.function ? ' - ' + this.formJD?.value.function : '';
    this.formJD?.controls['careerGroup'].setValue(this.JDSelect?.lvaMeanProfession);
    this.formJD?.controls['jobGroup'].setValue(this.JDSelect?.lvaMeanJob);
    this.formJD?.controls['posName'].setValue(this.JDSelect?.jobName + ' - ' + this.JDSelect?.fullName + value);
    this.formJD?.controls['function'].setValue(this.JDSelect?.orgaValue);
    this.formJD?.controls['jdName'].setValue('JD ' + this.formJD.value.posName);
    this.formJD?.controls['posId'].setValue(this.JDSelect?.posId);
    this.formJD?.controls['psdId'].setValue(this.JDSelect?.psdId);
    this.formJD?.controls['jobGroupId'].setValue(this.JDSelect?.jobGroup);
    this.formJD?.controls['jdId'].setValue(this.JDSelect?.jdId ? this.JDSelect.jdId : '');
  }

  getInnerWidth(point: number): number {
    if (window.innerWidth > 767) {
      return window.innerWidth / point > 1100 ? 1100 : window.innerWidth / point;
    }
    return window.innerWidth;
  }

  selectJob(job:AbstractControl):void{
    if((this.formJD?.value.dReport && this.formJD.value.iReport) && (this.formJD.value.dReport === this.formJD.value.iReport)){
      this.toastrCustom.error(this.translate.instant('modelOrganization.validate.selectReporter'));
      setTimeout(() => {
        job.setValue('');
      }, 500)
    }
  }

  onChangeDate(control:AbstractControl):void {
    if(this.formJD?.value.fromDate && this.formJD.value.toDate){

      const fromDate = new Date(this.formJD.value.fromDate).setHours(0, 0, 0, 0);
      const toDate = new Date(this.formJD.value.toDate).setHours(0, 0, 0, 0);

      if(fromDate >= toDate){
        this.toastrCustom.error(this.translate.instant('modelOrganization.message.toDate'));
        setTimeout(() => {
          control.setValue('');
        })
      }
    }
  }

  selectFile(event: { file: File; fileName: string }) {
    if(event){
      this.fileInfo.fileName = event.fileName;
      this.fileInfo.file = event.file;
      this.fileInfo.docId = null;
    } else {
      this.fileInfo.fileName = '';
      this.fileInfo.file = null;
    }
  }

  onFileSelected():void {
    const reader = new FileReader();
    reader.onload = (e: any) => {
      this.pdfSrc = e.target.result;
    };
    if(this.fileInfo.file) reader.readAsArrayBuffer(this.fileInfo.file);
    this.isVisible = true;
  }

  viewFile():void {
      if(!this.fileInfo.file && this.fileInfo.docId) {
        this.downloadFile(this.fileInfo.docId, this.fileInfo.fileName);
      } else this.onFileSelected();

  }

  downloadFile(docId:string | number, docName:string){
    this.dataService.downloadFile(docId).subscribe((data) => {
      const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.pdf' });
      this.fileInfo.file = new File([blob], docName);
      this.onFileSelected();
    });
  }

  handleCancel(){
    this.isVisible = false;
  }
}
