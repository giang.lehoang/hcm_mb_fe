import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JdInitComponent } from './jd-init.component';

describe('JdInitComponent', () => {
  let component: JdInitComponent;
  let fixture: ComponentFixture<JdInitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JdInitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JdInitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
