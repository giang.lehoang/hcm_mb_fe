import { Component, Injector, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { forkJoin } from 'rxjs';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {
  DetailTable,
  Job,
  JDEdit,
  TaskTable,
  LookupValue,
  LookupParam,
  MajorLevel,
  LOOKUP_CODE,
  Competency,
  Submit,
  JDsModel,
  JDsApproveModel,
  ApproveJD,
  jdDetailDTOMapObj,
  ASSIGNMENT_TYPE, JDCreate, JDDetail, JDDetailType, JDDetailObject, JDRCreate, FileInfo
} from "@hcm-mfe/model-organization/data-access/models";
import {DataService} from "@hcm-mfe/model-organization/data-access/services";
import {functionUri} from "@hcm-mfe/shared/common/constants";
import { FunctionCode } from '@hcm-mfe/shared/common/enums';
import {JDService} from "../../../../../data-access/services/src/lib/jd.service";

@Component({
  selector: 'app-jd-init',
  templateUrl: './jd-init.component.html',
  styleUrls: ['./jd-init.component.scss'],
})
export class JdInitComponent extends BaseComponent implements OnInit {
  jobOrgs: Job[] = [];
  taskTable: TaskTable[] = [];
  degreeTable: DetailTable[] = [];
  certTable: DetailTable[] = [];
  languageTable: DetailTable[] = [];
  competencyTable: DetailTable[] = [];
  competencyList: Competency[] = [];
  listMajorLevel: MajorLevel[] = [];
  listRole: LookupValue[] = [];
  lookupParam: LookupParam;
  formJD: FormGroup;
  submited: Submit | undefined;
  disable: boolean | undefined;
  JDSelect: JDsModel | JDsApproveModel | undefined;
  approveJd: ApproveJD | undefined;
  mode: 'create' | 'select' | 'view' = 'create';
  showButton: boolean | undefined;
  fileInfo = new FileInfo();

  constructor(injector: Injector, readonly dataService: DataService, readonly jdService: JDService) {
    super(injector);
    this.lookupParam = new LookupParam();
    this.formJD = new FormGroup({
      posName: new FormControl('', Validators.required),
      assignBy: new FormControl(''),
      jdName: new FormControl('', Validators.required),
      careerGroup: new FormControl(''),
      jobGroup: new FormControl(''),
      fromDate: new FormControl(new Date(), Validators.required),
      toDate: new FormControl(''),
      role: new FormControl(''),
      dReport: new FormControl('', Validators.required),
      iReport: new FormControl(''),
      rReport: new FormControl(''),
      function: new FormControl(''),
      purposeOfWork: new FormControl('', Validators.required),
      experience: new FormControl('', Validators.required),
      task: new FormArray([]),
      subTask: new FormArray([]),
      result: new FormArray([]),
      degree: new FormArray([]),
      degreeDes: new FormArray([]),
      cert: new FormArray([]),
      certDes: new FormArray([]),
      fLang: new FormArray([]),
      fLangDes: new FormArray([]),
      competency: new FormArray([]),
      capacity: new FormArray([]),
      posId: new FormControl(''),
      psdId: new FormControl(''),
      jobGroupId: new FormControl(''),
      jdId: new FormControl(''),
    });

    this.submited = {
      isSubmit: false,
    };

  }

  ngOnInit(): void {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ORGANIZATION_JD_INIT}`);
    this.checkModeJd();
    if (!this.objFunction || !this.objFunction.view) {
      this.router.navigateByUrl(functionUri.access_denied)
    }

    const lookupCarrer = { ...this.lookupParam };
    const lookupRole = { ...this.lookupParam };
    lookupRole.lookupCode = LOOKUP_CODE.ROLE;
    lookupCarrer.lookupCode = LOOKUP_CODE.CARRER_GROUP;

    const request:any[] = [
      this.dataService.getJobsByType(),
      this.dataService.getLookupValue(lookupRole.lookupCode),
      this.jdService.getMajorLevel(),
    ];

    if (this.JDSelect) {
      request.push(this.jdService.getCompetentcy(
        { jobGroup: this.JDSelect.jobGroup, positionId: this.JDSelect.posId ? this.JDSelect.posId : ''}));
    }

    if (this.JDSelect?.jdId) {
      request.push(this.jdService.getDetailJD(this.JDSelect?.jdId));
    }

    this.fetchData(request);

    if (!this.JDSelect?.jdId) {
      this.initTable();
    }

    this.showButton = this.mode !== 'view';
  }

  checkModeJd(){
    const item = localStorage.getItem('posInfo');
    if(item){
      this.JDSelect = JSON.parse(item ? item : '');
    }
    this.disable = this.JDSelect && (this.JDSelect.flagStatus === 0 || this.JDSelect.flagStatus === 1);
    if (this.JDSelect) {
      this.mode = this.JDSelect.view ? 'view' : 'select';
    }

    if (this.route.snapshot.data['mode'] === 'approve') {
      this.mode = 'view';
      const jd = localStorage.getItem('jdApprove');
      this.JDSelect = JSON.parse(jd ? jd : '');
      this.approveJd = {
        jdId: this.JDSelect?.jdId ? [this.JDSelect.jdId] : [],
        description: '',
      };
      this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ORGANIZATION_JD_APPROVE}`);
    }
  }

  fetchData(request:any[]):void{
    this.isLoading = true;
    forkJoin(request).subscribe(
      (data: any[]
      ) => {
        this.jobOrgs = data[0].data.content;
        this.jobOrgs.sort((a, b) => a.jobName.localeCompare(b.jobName));
        this.listRole = data[1].data;
        this.listRole.sort((a, b) => a.lvaMean.localeCompare(b.lvaMean));
        this.listMajorLevel = data[2].data;
        this.listMajorLevel.sort((a, b) => a.name.localeCompare(b.name));

        if (this.JDSelect) {
          this.competencyList = data[3].data;
          this.competencyList.forEach(item => {
            item.competenceJobGroupList.sort((a, b) => a.lvaMean.localeCompare(b.lvaMean));
          })
          this.competencyList.sort((a, b) => a.competenceFrames.localeCompare(b.competenceFrames));
        }

        if (this.JDSelect?.jdId) {
          this.setValueForm(data[4].data);
        } else {
          this.updateDefault();
        }

        if (this.mode === 'view') {
          this.convertToViewData();
        }
        this.isLoading = false;
      },
      (error) => {
        this.isLoading = false;
        this.toastrCustom.error();
      }
    );
  }

  setValueForm(item: JDEdit): void {
    if(item.docId){
      this.fileInfo.hasFile = true;
      this.fileInfo.fileName = item.fileName;
      this.fileInfo.docId = item.docId;
    }
    this.formJD.controls['jdId'].setValue(item.jdId);
    this.formJD.controls['jdName'].setValue(item.jdName);
    this.formJD.controls['assignBy'].setValue(item.assignmentType);
    this.formJD.controls['fromDate'].setValue(item.fromDate);
    this.formJD.controls['toDate'].setValue(item.toDate);
    this.formJD.controls['role'].setValue(item.lvaValueCompetenceRole);

    if(this.mode === "view"){
      this.formJD.controls['careerGroup'].setValue(item.positionDetailDTO.organizationDTO.lvaMeanProfession);
      this.formJD.controls['jobGroup'].setValue(item.positionDetailDTO.organizationDTO.lvaMeanJob);
    }

    if (!item.positionDetailDTO?.orgAttributeDTO?.orgaValue) {
      if(this.JDSelect?.orgId) this.jdService.getAttribute(this.JDSelect.orgId).subscribe(
        (data) => {
          const fun = data.data.map((att) => att.orgaValue);
          this.formJD.controls['function'].setValue(fun.join('/'));
        },
        (error) => {
          this.toastrCustom.error(error.message);
        }
      );
    }

    item.jdReporterDTOS.forEach((jdr) => {
      switch (jdr.jrpType) {
        case 'BCTT': {
          this.formJD.controls['dReport'].setValue(this.mode === 'view' ? jdr.jrpValue : jdr.jrpJobId);
          break;
        }
        case 'BCGT': {
          this.formJD.controls['iReport'].setValue(this.mode === 'view' ? jdr.jrpValue : jdr.jrpJobId);
          break;
        }
        case 'NNBC': {
          this.formJD.controls['rReport'].setValue(this.mode === 'view' ? jdr.jrpValue : jdr.jrpJobId);
          break;
        }
      }
    });
    this.setValueTable(item);
    if(item.jdDetailDTOMap?.MUC_DICH_CONG_VIEC) {
      this.formJD.controls['purposeOfWork'].setValue(item.jdDetailDTOMap.MUC_DICH_CONG_VIEC[0].targetDesc);
    }
    if(item.jdDetailDTOMap?.KINH_NGHIEM){
      this.formJD.controls['experience'].setValue(item.jdDetailDTOMap.KINH_NGHIEM[0].targetDesc);
    }
  }

  setValueTable(item: JDEdit){
    item.jdDetailDTOMap?.BANG_CAP?.forEach((degree) => {
      this.pushItem(degree, this.degreeTable, 'degree', 'degreeDes');
    });

    item.jdDetailDTOMap?.CHUNG_CHI?.forEach((cert) => {
      this.pushItem(cert, this.certTable, 'cert', 'certDes');
    });

    if(this.certTable?.length === 0 && this.mode !== 'view'){
      this.certTable.push(this.createDetail());
      (this.formJD.controls['cert'] as FormArray).push(this.certTable[0].value);
      (this.formJD.controls['certDes'] as FormArray).push(this.certTable[0].description);
    }

    item.jdDetailDTOMap?.NGOAI_NGU?.forEach((lang) => {
      this.pushItem(lang, this.languageTable, 'fLang', 'fLangDes');
    });

    if(this.languageTable?.length === 0 && this.mode !== 'view'){
      this.languageTable.push(this.createDetail());
      (this.formJD.controls['fLang'] as FormArray).push(this.languageTable[0].value);
      (this.formJD.controls['fLangDes'] as FormArray).push(this.languageTable[0].description);
    }

    item.jdDetailDTOMap?.KHUNG_NANG_LUC?.forEach((comp) => {
      comp.targetVal = comp.targetDescParent;
      this.pushItem(comp, this.competencyTable, 'competency', 'capacity');
    });

    this.competencyTable.forEach((comp) => {
      const major = this.competencyList.find((item) => item.competenceFrames === comp.description.value);
      comp.selectTemp = major?.competenceJobGroupList;
    });

    const mapTask = new Map<number, jdDetailDTOMapObj>();

    const task = item.jdDetailDTOMap?.NHIEM_VU?.sort(function (a, b) {
      if (a.parentId && !b.parentId) return 1;
      if (!a.parentId && b.parentId) return -1;
      return 0;
    });

    item.jdDetailDTOMap.NHIEM_VU = task;

    item.jdDetailDTOMap?.NHIEM_VU?.forEach((taskItem) => {
      mapTask.set(taskItem.jddId, taskItem);
      if (taskItem.parentId) {
        const parent = mapTask.get(taskItem.parentId);
        if (parent?.sub) {
          parent.sub.push(taskItem);
        } else {
          if(parent) {
            parent.sub = [];
            parent.sub.push(taskItem);
          }
        }
      }
    });

    this.pushItemTaskTable(item.jdDetailDTOMap?.NHIEM_VU ? item.jdDetailDTOMap.NHIEM_VU : []);
  }

  pushItem(ddt: jdDetailDTOMapObj, list: DetailTable[], value: string, des: string): void {
    const item = this.createDetail(ddt);
    list.push(item);
    (this.formJD.get(value) as FormArray).push(item.value);
    (this.formJD.get(des) as FormArray).push(item.description);
  }

  pushItemTaskTable(list: jdDetailDTOMapObj[]): void {
    for (const task of list) {
      if (task.parentId) {
        break;
      }
      const newTask: TaskTable = {
        taskName: new FormControl(task.targetDesc, Validators.required),
        sub: [],
      };
      task?.sub?.forEach((subTask) => {
        const sub = {
          taskName: new FormControl(subTask.targetDesc, Validators.required),
          result: new FormControl(subTask.targetVal, Validators.required),
        };
        newTask.sub.push(sub);
        (this.formJD.controls['subTask'] as FormArray).push(sub.taskName);
        (this.formJD.controls['result'] as FormArray).push(sub.result);
      });
      (this.formJD.controls['task'] as FormArray).push(newTask.taskName);
      this.taskTable.push(newTask);
    }
  }

  convertToViewData() {
    const roleValue = this.formJD.value.role;
    for (const role of this.listRole) {
      if (role.lvaValue === roleValue) {
        this.formJD.controls['role'].setValue(role.lvaMean);
        break;
      }
    }

    const assBy = this.formJD.value.assignBy;
    for (const ab of ASSIGNMENT_TYPE) {
      if (assBy === ab.key) {
        this.formJD.controls['assignBy'].setValue(ab.value);
        break;
      }
    }

    for (const comp of this.competencyTable) {
      if(comp.selectTemp) {
        for (const item of comp.selectTemp) {
          if (comp.value.value === item.lvaValue) {
            comp.value.setValue(item.lvaMean);
          }
        }
      }
    }
  }

  initTable(): void {
    const newTask: TaskTable = {
      taskName: new FormControl('', Validators.required),
      sub: [
        {
          taskName: new FormControl('', Validators.required),
          result: new FormControl('', Validators.required),
        },
      ],
    };
    this.taskTable.push(newTask);

    (this.formJD.controls['task'] as FormArray).push(newTask.taskName);
    (this.formJD.controls['subTask'] as FormArray).push(newTask.sub[0].taskName);
    (this.formJD.controls['result'] as FormArray).push(newTask.sub[0].result);

    this.degreeTable.push(this.createDetail());
    (this.formJD.controls['degree'] as FormArray).push(this.degreeTable[0].value);
    (this.formJD.controls['degreeDes'] as FormArray).push(this.degreeTable[0].description);

    this.certTable.push(this.createDetail());
    (this.formJD.controls['cert'] as FormArray).push(this.certTable[0].value);
    (this.formJD.controls['certDes'] as FormArray).push(this.certTable[0].description);

    this.languageTable.push(this.createDetail());
    (this.formJD.controls['fLang'] as FormArray).push(this.languageTable[0].value);
    (this.formJD.controls['fLangDes'] as FormArray).push(this.languageTable[0].description);

    this.competencyTable.push(this.createDetail());
    (this.formJD.controls['competency'] as FormArray).push(this.competencyTable[0].value);
    (this.formJD.controls['capacity'] as FormArray).push(this.competencyTable[0].description);
  }

  createDetail(jdd?: jdDetailDTOMapObj): DetailTable {
    const detailTable: DetailTable = {
      value: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
    };

    if (jdd) {
      detailTable.value.setValue(jdd.targetDesc);
      detailTable.description.setValue(jdd.targetVal);
    }
    return detailTable;
  }

  updateCompetency(value: JDsModel) {
    this.JDSelect = value;
    this.disable =
      this.JDSelect &&
      this.JDSelect.flagStatus !== 7 &&
      this.JDSelect.flagStatus !== null;
    if (this.disable) {
      this.showButton = false;
      this.toastrCustom.error(this.translate.instant('modelOrganization.message.posHasJd'));
    }
    const params = {
      jobGroup: value.jobGroup,
      positionId: this.JDSelect.posId ? this.JDSelect.posId : ''
    };
    this.jdService.getCompetentcy(params).subscribe((data) => {
      this.competencyList = data.data;
      this.updateDefault();
    });
  }

  updateDefault(){
    const tempTable:DetailTable[] = [];
    this.competencyList.forEach(parent => {
      parent.competenceJobGroupList.forEach(child => {
        if(child.isDefault === 'Y'){
          if(!tempTable.length){
            (this.formJD.get('competency') as FormArray).clear();
            (this.formJD.get('capacity') as FormArray).clear();
          }
          const item = this.createDetail();
          item.value.setValue(child.lvaValue);
          item.description.setValue(parent.competenceFrames);
          item.selectTemp = parent.competenceJobGroupList;
          tempTable.push(item);
          (this.formJD.get('competency') as FormArray).push(item.description);
          (this.formJD.get('capacity') as FormArray).push(item.value);
        }
      })
    });
    if(tempTable.length){
      this.competencyTable = tempTable;
    }
  }

  submit(type: number): void {
    this.trimValue(this.formJD);
    let emtyCer = false;
    let emtyLang = false;
    //remove req cert
    if(this.certTable.length === 1){
      if(!this.certTable[0].value.value){
        this.certTable[0].value.clearValidators();
        this.certTable[0].description.clearValidators();
        this.certTable[0].value.updateValueAndValidity();
        this.certTable[0].description.updateValueAndValidity();
        emtyCer = true;
        this.formJD.updateValueAndValidity();
      }
    }

    //remove req lang
    if(this.languageTable.length === 1){
      if(!this.languageTable[0].value.value){
        this.languageTable[0].value.clearValidators();
        this.languageTable[0].description.clearValidators();
        this.languageTable[0].value.updateValueAndValidity();
        this.languageTable[0].description.updateValueAndValidity();
        emtyLang = true;
        this.formJD.updateValueAndValidity();
      }
    }

    if(this.submited) {
      this.submited.isSubmit = true;
    }
    if (this.formJD.invalid) {
      return;
    }

    if(emtyCer){
      this.certTable = [];
    }

    if(emtyLang) {
      this.languageTable = [];
    }

    const request: JDCreate = {
      jdName: this.formJD.value.jdName,
      fromDate: this.formJD.value.fromDate,
      toDate: this.formJD.value.toDate,
      assignmentType: this.formJD.value.assignBy,
      competenceRole: this.formJD.value.role,
      posId: this.formJD.value.posId,
      psdId: this.formJD.value.psdId,
      targetValue: this.formJD.value.purposeOfWork,
      jdDetails: this.createJdDetail(),
      jdRpts: this.createJdReport(),
      jobGroup: this.formJD.value.jobGroupId,
      id: '',
      save: true,
    };

    if (this.formJD.value.jdId) {
      request.id = this.formJD.value.jdId;
      if(this.fileInfo.hasFile && !this.fileInfo.file){
        request.deleteFile = true;
      }
    }

    if (!type) {
      request.save = false;
    }
   this.callInit(request);
  }

  callInit(request: JDCreate){
    const formData = new FormData();
    formData.append("requestDTO", new Blob([JSON.stringify(request)], {
      type: 'application/json',
    }));
    this.isLoading = true;

    if (request.id) {
      if(this.fileInfo.file && !this.fileInfo.docId){
        formData.append('file',this.fileInfo.file);
      }
      this.jdService.updateJDs(formData).subscribe(
        (data) => {
          this.isLoading = false;
          this.toastrCustom.success();
          this.router.navigateByUrl(functionUri.organization_jd_list);
        },
        (error) => {
          this.isLoading = false;
          this.toastrCustom.error(error.message);
        }
      );
    } else {
      if(this.fileInfo.file){
        formData.append('file',this.fileInfo.file);
      }
      this.jdService.initJd(formData).subscribe(
        (data) => {
          this.isLoading = false;
          this.toastrCustom.success();
          this.router.navigateByUrl(functionUri.organization_jd_list);
        },
        (error) => {
          this.isLoading = false;
          this.toastrCustom.error(error.message);
        }
      );
    }
  }

  createJdDetail(): JDDetail[] {
    const jdDetail: JDDetail[] = [];

    const jdD: JDDetail = {
      jddType: JDDetailType.TASK,
      jddTypeObjects: [],
    };

    jdDetail.push(jdD);

    for (let i = 0; i < this.taskTable.length; i++) {
      const task = this.taskTable[i];
      const jdDObject: JDDetailObject = {
        fakeId: i,
        targetItem: i + 1,
        targetDescription: task.taskName.value,
      };
      jdD.jddTypeObjects.push(jdDObject);

      for (let iSub = 0; iSub < task.sub.length; iSub++) {
        const subTask = task.sub[iSub];
        const jdDObjectSub: JDDetailObject = {
          fakeIdParent: i,
          targetItem: iSub + 1,
          targetDescription: subTask.taskName.value,
          targetVal: subTask.result.value,
        };
        jdD.jddTypeObjects.push(jdDObjectSub);
      }
    }

    jdDetail.push(this.createJdDetailObj(this.degreeTable, JDDetailType.DEGREE));
    if(this.certTable.length !== 0){
      jdDetail.push(this.createJdDetailObj(this.certTable, JDDetailType.CERTIFICATE));
    }
    jdDetail.push(this.createJdDetailObj(this.languageTable, JDDetailType.LANGUAGE));
    jdDetail.push(this.createJdDetailObj(this.competencyTable, JDDetailType.COMPETENCY));

    jdDetail.push({
      jddType: JDDetailType.EXPERIENCE,
      jddTypeObjects: [
        {
          targetDescription: this.formJD.value.experience,
        },
      ],
    });

    jdDetail.push({
      jddType: JDDetailType.TARGET_JOB,
      jddTypeObjects: [
        {
          targetDescription: this.formJD.value.purposeOfWork,
        },
      ],
    });
    return jdDetail;
  }

  createJdDetailObj(list: DetailTable[], type: string): JDDetail {
    const jdD: JDDetail = {
      jddType: type,
      jddTypeObjects: [],
    };

    for (let i = 0; i < list.length; i++) {
      const detail = list[i];
      const jdd: JDDetailObject = {
        targetItem: i + 1,
        targetDescription: detail.value.value,
        targetVal: detail.description.value,
      };
      jdD.jddTypeObjects.push(jdd);
    }

    return jdD;
  }

  createJdReport(): JDRCreate[] {
    const jdR: JDRCreate[] = [];
    jdR.push({ jobId: this.formJD.value.dReport, jrpType: 'BCTT' });
    if (this.formJD.value.iReport) {
      jdR.push({ jobId: this.formJD.value.iReport, jrpType: 'BCGT' });
    }
    if (this.formJD.value.rReport) {
      jdR.push({ jobId: this.formJD.value.rReport, jrpType: 'NNBC' });
    }
    return jdR;
  }

  approve(): void {
    this.isLoading = true;
    if(!this.approveJd) return;
    this.jdService.approveJDs(this.approveJd).subscribe(
      (data) => {
        this.isLoading = false;
        this.router.navigateByUrl(functionUri.organization_jd_list);
      },
      (error) => {
        this.toastrCustom.error(error.message);
      }
    );
  }

  reject(): void {
    if(!this.approveJd || !this.submited) return;
    this.approveJd.description = this.approveJd.description.trim();
    if (!this.approveJd.description) {
      this.submited.isSubmit = true;
      return;
    }

    this.isLoading = true;
    this.jdService.rejectJDs(this.approveJd).subscribe(
      (data) => {
        this.isLoading = false;
        this.router.navigateByUrl(functionUri.organization_jd_list);
      },
      (error) => {
        this.isLoading = false;
        this.toastrCustom.error(error.message);
      }
    );
  }

  trimValue(arr:FormArray | FormGroup):void{
    Object.keys(arr.controls).forEach((key) => {
      const value = arr.get(key);
      if(value){
        if(value instanceof FormArray){
          this.trimValue(value);
        } else if(value.value && typeof value.value === 'string') {
          value.setValue(value.value.trim());
        }
      }
    });
  }
}
