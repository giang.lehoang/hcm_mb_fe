import { Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { userConfig } from '@hcm-mfe/shared/common/constants';
import { JDService } from 'libs/model-organization/data-access/services/src/lib/jd.service';
import { NzModalRef } from 'ng-zorro-antd/modal';
import {JDsModel, ParamsJDSearch} from "@hcm-mfe/model-organization/data-access/models";

@Component({
  selector: 'app-position-popup',
  templateUrl: './position-popup.component.html',
  styleUrls: ['./position-popup.component.scss'],
})
export class PositionPopupComponent extends BaseComponent implements OnInit {
  listData: Array<JDsModel> = [];
  listSettingPaginationCustom = {
    size: 0,
    total: 0,
    pageIndex: 0,
    numberOfElements: 0,
  };
  objectSearch:ParamsJDSearch = {
    orgName: '',
    jobName: '',
    orgaValue: '',
    page: 0,
    size: userConfig.pageSize,
  };
  isLoadingPage: boolean;
  constructor(readonly jdService: JDService,  readonly modalRef: NzModalRef, injector: Injector
    ) {
    super(injector);
    this.isLoadingPage = false;
  }

  ngOnInit(): void {
    this.getListJds(this.objectSearch);
  }

  getListJds(params:ParamsJDSearch) {
    this.isLoadingPage = true;
    this.jdService.getJDs(params).subscribe(
      (data) => {
        if (data.data.content.length > 0) {
          this.listData = data.data.content?.filter((item:JDsModel) => item.flagStatus !== 0);
          this.listSettingPaginationCustom.size = data.data.size;
          this.listSettingPaginationCustom.total = data.data.totalElements;
          this.listSettingPaginationCustom.pageIndex = data.data.pageable.pageNumber;
          this.listSettingPaginationCustom.numberOfElements = data.data.numberOfElements;
          this.isLoadingPage = false;
        } else {
          this.listData = [];
          this.listSettingPaginationCustom.size = data.data.size;
          this.listSettingPaginationCustom.total = data.data.totalElements;
          this.listSettingPaginationCustom.pageIndex = data.data.pageable.pageNumber;
          this.listSettingPaginationCustom.numberOfElements = data.data.numberOfElements;
          this.isLoadingPage = false;
        }
      },
      (err) => {
        this.toastrCustom.error(err.message)
      }
    );
  }

  getListTemplateBasePagination(value:number) {
    this.objectSearch.page = value - 1;
    this.getListJds(this.objectSearch);
  }

  searchPositon() {
    this.objectSearch.page = 0;
    this.getListJds(this.objectSearch);
  }

  chooseOrg(object:JDsModel) {
    this.modalRef.destroy({ objectData: object });
  }

  override triggerSearchEvent(): void {
    this.searchPositon();
  }
}
