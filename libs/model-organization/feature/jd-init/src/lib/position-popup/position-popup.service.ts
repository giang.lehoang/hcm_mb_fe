import { EventEmitter, Injectable, OnDestroy,OnInit } from '@angular/core';
import { NzModalService, NzModalRef } from 'ng-zorro-antd/modal';
import { PositionPopupComponent } from './position-popup.component';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
    providedIn: 'root',
  })

  export class PositionPopupService implements OnDestroy {
    onClick: EventEmitter<boolean> = new EventEmitter<boolean>();
    modalDelete: NzModalRef | undefined;
    constructor(private modal: NzModalService, readonly translate:TranslateService) {}
    ngOnDestroy(): void {
      this.onClick.unsubscribe();
    }

    showModal() {
      this.modalDelete = this.modal.create({
       nzTitle: 'Position',

        nzContent: PositionPopupComponent,
        nzFooter: null,
      });
    }

    getInnerWidth(point: number): number {
      if (window.innerWidth > 767) {
        return window.innerWidth / point > 1100 ? 1100 : window.innerWidth / point;
      }
      return window.innerWidth;
    }
  }
