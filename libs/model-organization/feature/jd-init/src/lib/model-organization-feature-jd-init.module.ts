import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {JobInfoComponent} from "./job-info/job-info.component";
import {JdInitComponent} from "./jd-init/jd-init.component";
import {CompetencyTableComponent} from "./competency-table/competency-table.component";
import {PositionPopupComponent} from "./position-popup/position-popup.component";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {NzGridModule} from "ng-zorro-antd/grid";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {RouterModule} from "@angular/router";
import {NzDividerModule} from "ng-zorro-antd/divider";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbInputFileModule} from "@hcm-mfe/shared/ui/mb-input-file";
import {NzModalModule} from "ng-zorro-antd/modal";
import {PdfViewerModule} from "ng2-pdf-viewer";
import { SharedDirectivesAutofocusModule } from '@hcm-mfe/shared/directives/autofocus';

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, NzGridModule, SharedUiMbInputTextModule, FormsModule,
    NzTableModule, TranslateModule, NzToolTipModule, NzPaginationModule, ReactiveFormsModule,
    SharedUiMbButtonModule, SharedUiMbSelectModule, RouterModule.forChild([
      {
        path: '',
        component: JdInitComponent
      }]), NzDividerModule, SharedUiMbDatePickerModule, SharedUiMbInputFileModule, NzModalModule, PdfViewerModule, SharedDirectivesAutofocusModule],
  declarations: [JobInfoComponent, JdInitComponent, CompetencyTableComponent, PositionPopupComponent],
  exports: [JobInfoComponent, JdInitComponent, CompetencyTableComponent, PositionPopupComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ModelOrganizationFeatureJdInitModule {}
