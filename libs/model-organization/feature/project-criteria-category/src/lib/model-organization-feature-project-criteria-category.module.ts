import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ProjectCriteriaCategoryComponent} from "./project-criteria-category/project-criteria-category.component";
import {ProjectCriteriaInitComponent} from "./project-criteria-init/project-criteria-init.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NzFormModule} from "ng-zorro-antd/form";
import {NzSelectModule} from "ng-zorro-antd/select";
import {NzTableModule} from "ng-zorro-antd/table";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
import {NzModalModule} from "ng-zorro-antd/modal";
import {NzCardModule} from "ng-zorro-antd/card";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {RouterModule} from "@angular/router";
import {NzInputModule} from "ng-zorro-antd/input";
import {TranslateModule} from "@ngx-translate/core";
import { SharedDirectivesAutofocusModule } from '@hcm-mfe/shared/directives/autofocus';

@NgModule({
  imports: [CommonModule, ReactiveFormsModule, NzFormModule, NzSelectModule, NzTableModule, FormsModule, SharedUiMbButtonModule,
    TranslateModule, NzPaginationModule, NzDropDownModule, NzToolTipModule, NzModalModule,
    RouterModule.forChild([
      {
        path: '',
        component: ProjectCriteriaCategoryComponent
      }]),
    NzCardModule, SharedUiLoadingModule, NzInputModule, SharedDirectivesAutofocusModule],
  declarations: [ProjectCriteriaCategoryComponent, ProjectCriteriaInitComponent],
  exports: [ProjectCriteriaCategoryComponent, ProjectCriteriaInitComponent]
})
export class ModelOrganizationFeatureProjectCriteriaCategoryModule {}
