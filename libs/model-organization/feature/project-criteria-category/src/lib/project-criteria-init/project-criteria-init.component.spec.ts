import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectCriteriaInitComponent } from './project-criteria-init.component';

describe('ProjectCriteriaInitComponent', () => {
  let component: ProjectCriteriaInitComponent;
  let fixture: ComponentFixture<ProjectCriteriaInitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectCriteriaInitComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectCriteriaInitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
