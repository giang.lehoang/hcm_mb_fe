import { Component, OnInit, Injector, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormArray, FormControl, Validators, AbstractControl } from '@angular/forms';
import { NzModalRef } from 'ng-zorro-antd/modal';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {Criteria, RequestCriteria} from "@hcm-mfe/model-organization/data-access/models";
import {ProjectService} from "@hcm-mfe/model-organization/data-access/services";
import {REGEX} from "@hcm-mfe/shared/common/constants";


@Component({
  selector: 'app-project-criteria-init',
  templateUrl: './project-criteria-init.component.html',
  styleUrls: [
    './project-criteria-init.component.scss',
    '../project-criteria-category/project-criteria-category.component.scss',
  ],
})
export class ProjectCriteriaInitComponent extends BaseComponent implements OnInit {
  confirmModal?: NzModalRef;
  criteria: Criteria | undefined;
  @Input() id: number | undefined;
  @Input() view: boolean | undefined;
  disable: boolean;
  criteriaForm: FormGroup;
  jobDataTable: Criteria[]= [];
  @Output() submitClick = new EventEmitter<boolean>();

  constructor(
    injector: Injector,
    public projectService: ProjectService,
  ) {
    super(injector);
    this.criteria = {
      meaning: '',
      flagStatus: 1,
      value: '',
      parentValue: '',
      subs: null,
      id: null,
    };
    this.criteriaForm = new FormGroup({
      value: new FormControl(this.criteria.meaning, [Validators.required, Validators.maxLength(500), Validators.pattern(REGEX.SPECIAL_TEXT)]),
      flagStatus: new FormControl(this.criteria.flagStatus, Validators.required),
      attributeName: new FormArray([]),
    });

    this.disable = false;
  }

  ngOnInit(): void {

    const attributeName = this.criteriaForm.controls['attributeName'] as FormArray;
    if (this.id) {
      this.projectService.getCriteriaCategoryDetail(this.id).subscribe(
        (data) => {
          this.criteria = data?.data;
          if(data.data?.subs) {
            this.jobDataTable = data.data?.subs;
          }
          this.criteriaForm.controls['value'].setValue(this.criteria?.meaning);
          this.criteriaForm.controls['flagStatus'].setValue(this.criteria?.flagStatus);
          for (const item of this.jobDataTable) {
            const input = new FormControl(item.meaning, [Validators.required, Validators.maxLength(500), Validators.pattern(REGEX.SPECIAL_TEXT)]);
            item.validate = input;
            if (this.view) {
              input.disable();
            }
            attributeName.push(input);
          }
        },
        (error) => {
          this.message.error(error.error.message);
        }
      );
    } else {
      this.addCriteria()
    }
  }

  get attributeNameFormArray() {
    return this.criteriaForm.get('attributeName') as FormArray;
  }

  closeModal() {
    this.submitClick.emit(true);
  }

  getErrorMessageForm(control: AbstractControl) {
    const error = control.errors;
    let message;
    if (error?.['pattern']) {
      message = 'Tên tiêu chí không hợp lệ';
    } else if (error?.['required']) {
      message = 'Tên tiêu chí bắt buộc nhập'
    } else if (error?.['maxlength']) {
      message = `Độ dài vượt quá ${error['maxlength'].requiredLength} ký tự`;
    }
    return message;
  }

  getErrorMessage(i:number) {
    const error = this.attributeNameFormArray.controls[i].errors;
    let message;
    if (error?.['duplicate']) {
      message = 'Tên thuộc tính đã tồn tại';
    } else if (error?.['maxlength']) {
      message = `Độ dài vượt quá ${error['maxlength'].requiredLength} ký tự`;
    } else if (error?.['pattern']) {
      message = 'Tên thuộc tính không hợp lệ'
    }
    else {
      message = 'Tên thuộc tính bắt buộc nhập';
    }
    return message;
  }

  validateRequest() {
    const values: Set<string> = new Set<string>();

    this.criteriaForm.controls['value'].setValue(this.criteriaForm.value.value.trim())

    Object.values(this.criteriaForm.controls).forEach((control: AbstractControl) => {
      if (control.invalid) {
        control.markAsDirty();
        control.updateValueAndValidity({ onlySelf: true });
      }
    });
    Object.values(this.attributeNameFormArray.controls).forEach((control: AbstractControl) => {
      if (control.invalid) {
        control.markAsDirty();
        control.updateValueAndValidity({ onlySelf: true });
      }

      if (control.value) {
        const value = control.value.trim();
        control.setValue(value)
        if (values.has(value)) {
          control.setErrors({ duplicate: true });
        } else {
          values.add(value);
        }
      }
    });

    if (!this.criteriaForm.valid) {
      return;
    }

    const listSub: RequestCriteria[] = [];
    for (const item of this.jobDataTable) {
      listSub.push({
        id: item.id,
        lvaMean: item.validate?.value,
        flagStatus: item.flagStatus,
        meaning: item.validate?.value,
      });
    }

    const request: RequestCriteria = {
      id: this.criteria?.id,
      lvaMean: this.criteriaForm.value.value.trim(),
      flagStatus: this.criteriaForm.value.flagStatus,
      meaning: this.criteriaForm.value.value.trim(),
      subs: listSub,
    };

    this.disable = true;
    if (this.id) {
      this.update(request);
    } else {
      this.create(request);
    }
  }
  update(request:any){
    this.projectService.updateCriteria(request).subscribe(
      (data) => {
        this.disable = false;
        this.toastrCustom.success()
        this.closeModal();
      },
      (error) => {
        this.disable = false;
        this.toastrCustom.error(error?.error.message)
        this.closeModal();
      }
    );
  }
  create(request:any){
    this.projectService.createCriteria(request).subscribe(
      (data) => {
        this.disable = false;
        this.toastrCustom.success()
        this.closeModal();
      },
      (error) => {
        this.disable = false;
        this.toastrCustom.error(error?.error.message)
        this.closeModal();
      }
    );
  }
  addCriteria() {
    const control = new FormControl(null, [Validators.required, Validators.maxLength(500), Validators.pattern(REGEX.SPECIAL_TEXT)]);
    this.attributeNameFormArray.push(control);
    this.jobDataTable.push({
      meaning: '',
      flagStatus: 1,
      value: '',
      parentValue: '',
      subs: null,
      id: null,
      validate: control,
    });
  }

  deleteCriteria(index:number) {
    this.jobDataTable.splice(index, 1);
    this.attributeNameFormArray.removeAt(index);
  }

  showDelete(index:number) {
    this.deletePopup.showModal(() => {
      this.jobDataTable.splice(index, 1);
      this.attributeNameFormArray.removeAt(index);
    });
  }
}
