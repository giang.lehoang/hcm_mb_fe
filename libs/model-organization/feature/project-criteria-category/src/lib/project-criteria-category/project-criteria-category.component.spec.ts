import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectCriteriaCategoryComponent } from './project-criteria-category.component';

describe('ProjectCriteriaCategoryComponent', () => {
  let component: ProjectCriteriaCategoryComponent;
  let fixture: ComponentFixture<ProjectCriteriaCategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectCriteriaCategoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectCriteriaCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
