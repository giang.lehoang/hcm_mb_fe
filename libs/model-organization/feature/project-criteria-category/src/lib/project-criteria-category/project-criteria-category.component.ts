import { Component, Injector, OnInit } from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd/modal';
import {CriteriaSearch, LookupValue, Pagination} from "@hcm-mfe/model-organization/data-access/models";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {userConfig} from "@hcm-mfe/shared/common/constants";
import { FunctionCode } from '@hcm-mfe/shared/common/enums';
import {DataService, ProjectService} from "@hcm-mfe/model-organization/data-access/services";
import {SearchService} from "@hcm-mfe/shared/core";

@Component({
  selector: 'app-project-criteria-category',
  templateUrl: './project-criteria-category.component.html',
  styleUrls: ['./project-criteria-category.component.scss'],
})
export class ProjectCriteriaCategoryComponent extends BaseComponent implements OnInit {
  jobDataTable: LookupValue[] = [];
  isExist: boolean;
  idSelected: number | null;
  isLoadingPage: boolean;
  isVisible: boolean;
  objectSearch: CriteriaSearch;
  requestBody: CriteriaSearch;
  pagination: Pagination = new Pagination(userConfig.pageSize);
  confirmModal?: NzModalRef;
  view: boolean;
  disable: boolean;
  dataSelectCriteria: LookupValue[] = [];
  dataSelectCriteriaSearch: LookupValue[] = [];
  dataSelectAttribute: LookupValue[] = [];
  dataSelectAttributeSearch: LookupValue[] = [];

  constructor(
    injector: Injector,
    readonly projectService: ProjectService,
    readonly searchService: SearchService,
    readonly dataService: DataService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.PROJECT_CRITERIA_CATEGORY}`);
    this.view = false;
    this.isExist = false;
    this.idSelected = null;
    this.disable = false;
    this.isVisible = false;
    this.isLoadingPage = false;
    this.objectSearch = {
      criteria: '',
      attribute: '',
      page: null,
      size: 15,
    };
    this.pagination = {
      currentPage: 1,
      totalElement: 50,
      size: 15,
      totalPage: 0,
      numberOfElements: 0,
    };
    this.requestBody = this.objectSearch;
  }

  ngOnInit(): void {


    this.callSearch();
    this.searchAll();
  }

  handleCancel() {
    this.isVisible = false;
  }

  searchAll() {
    const body: CriteriaSearch = {
      attribute: null,
      criteria: null,
      page: null,
      size: null,
    };
    this.dataService.searchLookup(body).subscribe(
      (data) => {
        if(data.data?.content){
          this.filterDuplicate(data.data.content);
        }
      },
      (error) => {
        this.message.error(error?.error.message);
      }
    );
  }

  filterDuplicate(list: LookupValue[]) {
    const setC: Set<string> = new Set<string>();
    const setA: Set<string> = new Set<string>();
    for (const item of list) {
      if (!setC.has(item.criteria)) {
        setC.add(item.criteria);
        this.dataSelectCriteria.push(item);
      }

      if (!setA.has(item.attribute)) {
        setA.add(item.attribute);
        this.dataSelectAttribute.push(item);
      }
    }
  }

  callSearch() {
    this.isLoadingPage = true;
    this.dataService.searchLookup(this.requestBody).subscribe(
      (data) => {
        if(data.data?.content){
          this.jobDataTable = data.data.content;
          this.pagination.totalElements = data.data.totalElements ? data.data.totalElements : 0;
          this.pagination.totalPages = data.data.totalPages ?  data.data.totalPages : 0;
          this.pagination.numberOfElements = data.data.numberOfElements ? data.data.numberOfElements : 0;
          this.isLoadingPage = false;
        }
      },
      (error) => {
        this.isLoadingPage = false;
        this.message.error(error?.error.message);
      }
    );
  }

  onSearch() {
    this.requestBody = {
      ...this.objectSearch,
    };
    this.callSearch();
  }

  changePage() {
    this.requestBody.page = this.pagination.currentPage - 1;
    this.callSearch();
  }

  openModal() {
    this.idSelected = null;
    this.view = false;
    this.isVisible = true;
  }

  closeModal(value:any) {
    this.isVisible = false;
    this.callSearch();
  }

  onEdit(id:number) {
    this.idSelected = id;
    this.isVisible = true;
    this.view = false;
  }

  onView(id:number) {
    this.idSelected = id;
    this.isVisible = true;
    this.view = true;
  }
  checkCriteriaProject(id:number): void {
    this.projectService.checkCriteriaProject(id).subscribe((data) => {
      if (data.data) {
        this.toastrCustom.error(this.translate.instant('common.notification.criteriaProjectExist'));
      } else {
        this.deletePopup.showModal(() => {
          const idMess = this.message.loading('Action in progress..', { nzDuration: 0 }).messageId;
          this.projectService.deleteCriteriaCategory(id).subscribe(
            (res) => {
              this.message.remove(idMess);
              this.toastrCustom.success();
              this.callSearch();
            },
            (error) => {
              this.message.remove(idMess);
              this.toastrCustom.error(error?.error.message);
            }
          );
        });
      }
    });
  }

  showConfirm(id:number): void {
    this.checkCriteriaProject(id);
  }

  onSearchCriteria(e:any) {
    this.dataSelectCriteriaSearch = [];
    const value = e?.target?.value;
    if (value) {
      this.dataSelectCriteria.forEach(item => {
        if (this.searchService.replaceUnicode(item.criteria.toLowerCase()).includes(this.searchService.replaceUnicode(value.toLowerCase()))) {
          this.dataSelectCriteriaSearch.push(item);
        }
      });
    } else {
      this.dataSelectCriteriaSearch = [];
    }
  }

  chooseSearchCriteria(value:string) {
    this.objectSearch.criteria = value;
  }

  onSearchAttribute(e:any) {
    this.dataSelectAttributeSearch = [];
    const value = e.target.value;
    if (value) {
      this.dataSelectAttribute.forEach(item => {
        if (this.searchService.replaceUnicode(item.attribute.toLowerCase()).includes(this.searchService.replaceUnicode(value.toLowerCase()))) {
          this.dataSelectAttributeSearch.push(item);
        }
      });
    } else {
      this.dataSelectAttributeSearch = [];
    }
  }

  chooseSearchAttribute(value:string) {
    this.objectSearch.attribute = value;
  }

  override triggerSearchEvent(): void {
      this.onSearch();
  }

}
