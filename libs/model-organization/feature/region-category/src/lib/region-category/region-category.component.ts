import { Component, Injector, OnInit } from '@angular/core';
import { NavigationEnd } from '@angular/router';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { RegionCategory } from '@hcm-mfe/shared/data-access/models';
import { filter, Subscription } from 'rxjs';
import { DataService } from '@hcm-mfe/shared/data-access/services';
import { CustomToastrService } from '@hcm-mfe/shared/core';
import { FunctionCode, functionUri } from '@hcm-mfe/shared/common/enums';
import { RegionType } from '@hcm-mfe/model-organization/data-access/models';
import { RegionService } from '@hcm-mfe/model-organization/data-access/services';

@Component({
  selector: 'app-region-category',
  templateUrl: './region-category.component.html',
  styleUrls: ['./region-category.component.scss'],
})
export class RegionCategoryComponent extends BaseComponent implements OnInit {
  jobDataTable: RegionCategory[] = [];
  isLoadingPage = false;
  isVisible = false;
  titlePopup ='';
  title ='';
  type ='';
  shareData: RegionCategory | undefined;
  id: any;
  view = false;
  count: number =0;
  subsciption: Subscription;

  constructor(injector: Injector, readonly dataService: DataService, readonly toastr: CustomToastrService,
              private readonly regionService: RegionService) {
    super(injector);
    this.subsciption = this.router.events.pipe(filter((event:any) => event instanceof NavigationEnd)).subscribe((e) => {
      this.handleChangeRouter();
    });
  }

  ngOnInit(): void {
    this.count = 0;
  }

  override ngOnDestroy(): void {
    this.subsciption.unsubscribe();
  }

  handleChangeRouter() {
    let code: string = this.route.snapshot.data['code'];
    code = code.toUpperCase();
    let funCode;
    switch (code) {
      case 'KV':
        this.type = RegionType.KV;
        this.title = 'DANH MỤC KHU VỰC';
        funCode = FunctionCode.ORGANIZATION_REGION_CATEGORY;
        break;
      case 'TV':
        this.type = RegionType.TV;
        this.title = 'DANH MỤC TIỂU VÙNG';
        funCode = FunctionCode.ORGANIZATION_SUB_REGION_CATEGORY;
        break;
      case 'LH':
        this.type = RegionType.LH;
        this.title = 'DANH MỤC LOẠI HÌNH CHI NHÁNH';
        funCode = FunctionCode.ORGANIZATION_BRANCH_CATEGORY;
        break;
    }

    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${funCode}`);
    console.log(this.objFunction)
    if(!this.objFunction.view){
      this.router.navigateByUrl(functionUri.access_denied)
    }
    this.isVisible = false;
    this.callSearch();
  }
  handleCancel() {
    this.isVisible = false;
  }

  callSearch() {
    this.isLoadingPage = true;
    this.dataService.getRegionCategoryList(this.type).subscribe(
      (data: any) => {
        this.jobDataTable = data.data;
        this.isLoadingPage = false;
      },
      (error) => {
        this.message.error(error?.message);
        this.isLoadingPage = false;
      }
    );
  }

  onDelete(id:any) {
    this.deletePopup.showModal(() => {
      this.regionService.deleteCategory(id).subscribe(
        (data) => {
          this.toastr.success();
          this.callSearch();
        },
        (error) => {
          this.toastr.error(error?.message);
        }
      );
    });
  }

  onEdit(id:any) {
    if(!this.objFunction.edit){
      return;
    }
    this.id = id;
    this.titlePopup = 'Chỉnh sửa';
    this.isVisible = true;
    this.view = false;
  }

  onView(id:any) {
    this.id = id;
    this.titlePopup = 'Xem';
    this.view = true;
    this.isVisible = true;
  }

  openModal() {
    this.id = null;
    this.titlePopup = 'Thêm mới';
    this.isVisible = true;
    this.view = false;
  }

  closeModal(value:any) {
    this.callSearch();
    this.isVisible = false;
  }
}
