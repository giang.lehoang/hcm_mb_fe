import { Component, Injector, OnInit } from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd/modal';
import * as FileSaver from 'file-saver';
import * as moment from 'moment';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {maxInt32} from "@hcm-mfe/shared/common/enums";
import {Issues, JobSearch, Pagination, SearchIssued} from "@hcm-mfe/model-organization/data-access/models";
import {userConfig} from "@hcm-mfe/shared/common/constants";
import {IssuedService} from "../../../../../data-access/services/src/lib/issued-service";

@Component({
  selector: 'app-issued-project-list',
  templateUrl: './issued-project-list.component.html',
  styleUrls: ['./issued-project-list.component.scss'],
})
export class IssuedProjectListComponent extends BaseComponent implements OnInit {
  jobDataTable:Issues[] = [];
  pagination: Pagination;
  objSearch: SearchIssued;
  params: JobSearch;
  confirmModal?: NzModalRef;
  showDelete: boolean;
  isVisible: boolean;
  indeterminate: boolean;
  checked: boolean;
  listSelect:Issues[] = [];
  constructor(
    injector: Injector,
    readonly issuedService: IssuedService
  ) {
    super(injector);
    this.checked = false;
    this.isLoading = false;
    this.showDelete = true;
    this.indeterminate = false;
    this.pagination = new Pagination(userConfig.pageSize);

    this.objSearch = {
      dataType: '',
      dataName: '',
      issuedDate: '',
      description: '',
    };

    this.params = {
      dataType: '',
      page: 0,
      size: 15,
    };

    this.isVisible = false;
  }

  ngOnInit(): void {
    this.callProjectFilter();
  }

  setCheckedAll() {
    this.checked = true;
    for (const item of this.jobDataTable) {
      if (!this.listSelect.some(m => m.dataId === item.dataId)) {
        this.checked = false;
        break;
      }
    }
  }

  changePage() {
    this.params.page = this.pagination.currentPage - 1;
    this.callProjectFilter();
  }

  callProjectFilter() {
    this.isLoading = true;
    this.issuedService.filterIssuedProject(this.params).subscribe(
      (data: any) => {
        this.isLoading = false;
        this.jobDataTable = data.data.content;
        this.pagination.totalElements = data.data.totalElements;
        this.pagination.totalPages = data.data.totalPages;
        this.pagination.numberOfElements = data.data.numberOfElements;
        this.setCheckedAll();
      },
      (error) => {
        this.message.error(error.error.message);
        this.isLoading = false;
        this.jobDataTable = [];
      }
    );
  }

  handleCancel() {
    this.isVisible = false;
  }

  onSearch() {
    this.params = {
      dataType: '',
      dataName: this.objSearch.dataName ? this.objSearch.dataName : '',
      description: this.objSearch.description ? this.objSearch.description : '',
      page: 0,
      size: 15,
    };

    if (this.objSearch.issuedDate) {
      this.params.issuedDate = moment(this.objSearch.issuedDate).format('DD/MM/YYYY');
    }

    this.callProjectFilter();
  }

  exportExcel() {
    this.params.page = 0;
    this.params.size = maxInt32;
    this.issuedService.exportExcelIssued(this.params).subscribe(
      (data: any) => {
        const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
        FileSaver.saveAs(blob, 'Danh sach thong tin ban hanh.xlsx');
      },
      (error) => {
        this.message.error(error.error.message);
      }
    );
  }

  onItemChecked(data:Issues, checked: boolean): void {
    if (checked) {
      if (!this.listSelect.some(m => m.dataId === data.dataId)) {
        this.listSelect.push(data);
      }
    } else {
      this.listSelect = this.listSelect.filter(m => m.dataId !== data.dataId);
    }
  }

  onAllChecked(checked: boolean): void {
    this.checked = checked
    this.jobDataTable.forEach((element: any) => {
      if (checked) {
        if (!this.listSelect.some(m => m.dataId === element.dataId)) {
          this.listSelect.push(element);
        }
      } else {
        this.listSelect = this.listSelect.filter(m => m.dataId !== element.dataId);
      }
    });
  }

  setListSelected(listId:Issues[]) {
    this.issuedService.setListIdSelected(listId);
  }

  naviageToHistory(value:Issues) {
    localStorage.setItem('selectedItem', JSON.stringify(value));
    this.router.navigateByUrl('/organization/issued/issued-history');
    localStorage.setItem('dataType', value?.dataType)
  }

  navigatePage(item:Issues) {
    if (item) {
      this.listSelect = [];
      this.listSelect.push(item);
      this.setListSelected(this.listSelect);
    } else {
      this.setListSelected(this.listSelect);
    }
    this.router.navigateByUrl('/organization/issued/issued-transfer');
  }

  itemChecked(item:Issues) {
    let result: boolean;
    if (this.listSelect.some(m => m.dataId === item.dataId)) {
      result = true;
    } else {
      result = false;
    }
    return result;
  }

  override triggerSearchEvent(): void {
      this.onSearch();
  }

}
