import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IssuedProjectListComponent } from './issued-project-list.component';

describe('IssuedProjectListComponent', () => {
  let component: IssuedProjectListComponent;
  let fixture: ComponentFixture<IssuedProjectListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IssuedProjectListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IssuedProjectListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
