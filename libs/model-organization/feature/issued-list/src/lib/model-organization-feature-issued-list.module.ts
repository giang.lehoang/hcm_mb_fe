import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IssuedProjectListComponent} from "./issued-project-list/issued-project-list.component";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {FormsModule} from "@angular/forms";
import {TranslateModule} from "@ngx-translate/core";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import {RouterModule} from "@angular/router";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {NzGridModule} from "ng-zorro-antd/grid";

@NgModule({
  imports: [CommonModule,SharedUiLoadingModule, SharedUiMbButtonModule, SharedUiMbSelectModule, SharedUiMbInputTextModule, FormsModule,
    NzTableModule, NzGridModule , NzCheckboxModule, NzToolTipModule, NzPaginationModule, TranslateModule, RouterModule.forChild([
      {
        path: '',
        component: IssuedProjectListComponent,
      }]), SharedUiMbDatePickerModule],
  declarations: [IssuedProjectListComponent],
  exports: [IssuedProjectListComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ModelOrganizationFeatureIssuedListModule {
}
