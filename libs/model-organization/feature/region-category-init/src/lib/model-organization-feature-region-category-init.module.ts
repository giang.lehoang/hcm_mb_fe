import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { TranslateModule } from '@ngx-translate/core';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { RegionCategoryInitComponent } from './region-category-init/region-category-init.component';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { SharedDirectivesAutofocusModule } from '@hcm-mfe/shared/directives/autofocus';

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, SharedUiMbButtonModule, TranslateModule, NzCardModule, FormsModule, NzFormModule, ReactiveFormsModule,
    NzGridModule, SharedUiMbSelectModule, FormsModule, SharedUiMbInputTextModule, NzTableModule, NzPaginationModule,
    NzToolTipModule, NzModalModule, NzTagModule, NzSelectModule, NzTypographyModule, NzFormModule, NzInputModule, SharedDirectivesAutofocusModule
  ],
  declarations: [RegionCategoryInitComponent],
  exports: [RegionCategoryInitComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ModelOrganizationFeatureRegionCategoryInitModule {}
