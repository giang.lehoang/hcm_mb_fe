import { Component, OnInit, Injector, Output, EventEmitter, Input } from '@angular/core';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { RegionCategory } from '@hcm-mfe/shared/data-access/models';
import { DataService } from '@hcm-mfe/shared/data-access/services';
import { CustomToastrService } from '@hcm-mfe/shared/core';
import { RegionType, RequestRegionModel } from '@hcm-mfe/model-organization/data-access/models';
import { REGEX } from '@hcm-mfe/shared/common/enums';
import { RegionService } from '@hcm-mfe/model-organization/data-access/services';

@Component({
  selector: 'app-region-category-init',
  templateUrl: './region-category-init.component.html',
  styleUrls: ['./region-category-init.component.scss'],
})
export class RegionCategoryInitComponent extends BaseComponent implements OnInit {
  confirmModal?: NzModalRef;
  disable = false;
  loadingSelect  = false;
  regionForm: FormGroup;
  data: any;
  @Input() type = '';
  @Input() id: any;
  @Input() view:any;
  dataSelect: RegionCategory[] = [];
  @Output() submitClick = new EventEmitter<boolean>();

  constructor(injector: Injector, readonly dataService: DataService, readonly toastr: CustomToastrService,
              private readonly regionService: RegionService) {
    super(injector);

  }

  ngOnInit(): void {
  
    if (this.type === RegionType.TV) {
      this.regionForm = new FormGroup({
        meaning: new FormControl('', [Validators.required, Validators.pattern(REGEX.SPECIAL_TEXT_SHIFT)]),
        flagStatus: new FormControl(1, Validators.required),
        parentValue: new FormControl('', Validators.required),
      });
    } else {
      this.regionForm = new FormGroup({
        meaning: new FormControl('', [Validators.required, Validators.pattern(REGEX.SPECIAL_TEXT_SHIFT)]),
        flagStatus: new FormControl(1, Validators.required),
      });
    }
    if (this.view) {
      this.regionForm.controls['meaning'].disable();
    }

    this.disable = false;
    if (this.type === RegionType.TV) {
      this.dataService.getRegionCategoryList(RegionType.KV, 1).subscribe((data: any) => {
        this.dataSelect = data.data;
      });
    }

    if (this.id) {
      this.regionService.regionCategoryDetail(this.id).subscribe((data: any) => {
        this.data = data.data;
        this.regionForm.controls['meaning'].setValue(this.data.lvaMean);
        this.regionForm.controls['flagStatus'].setValue(this.data.flagStatus);
        if (this.type === RegionType.TV) {
          this.regionForm.controls['parentValue'].setValue(this.data.parentValue);
        }
      });
    }
  }

  closeModal() {
    this.submitClick.emit(true);
  }

  getErrorMessage(control:any, message?: string): string | null {
    const error = control?.errors;
    if (error?.pattern) {
      return 'Dữ liệu không đúng định dạng';
    } else if (error?.required) {
      return String(message);
    }
    return null;
  }

  validateRequest() {
    this.regionForm.controls['meaning'].setValue(this.regionForm.value.meaning.trim());
    if (this.type === RegionType.TV) {
      this.regionForm.controls['parentValue'].setValue(this.regionForm.value.parentValue.trim());
    }
    Object.values(this.regionForm.controls).forEach((control) => {
      if (control.invalid) {
        control.markAsDirty();
        control.updateValueAndValidity({ onlySelf: true });
      }
    });
    if (!this.regionForm.valid) {
      return;
    }

    const request: RequestRegionModel = {
      flagStatus: this.regionForm.value.flagStatus,
      meaning: this.regionForm.value.meaning,
      type: this.type,
    };

    if (this.type === RegionType.TV) {
      request.parentValue = this.regionForm.value.parentValue;
    }
    this.disable = true;
    const idMess = this.message.loading('Action in progress..', { nzDuration: 0 }).messageId;
    if (this.id) {
      request.id = this.id;
      this.updateCategory(request, idMess);
    } else {
      this.createCategory(request, idMess);
    }
  }

  createCategory(request:any, idMess:any) {
    this.regionService.createRegionCategory(request).subscribe(
      () => {
        this.successCreateOrUpdate(idMess);
      },
      (err) => {
        this.errorCreateOrUpdate(err, idMess);
      }
    );
  }

  updateCategory(request:any, idMess:any) {
    this.regionService.updateRegionCategory(request).subscribe(
      () => {
        this.successCreateOrUpdate(idMess);
      },
      (err) => {
        this.errorCreateOrUpdate(err, idMess);
      }
    );
  }

  successCreateOrUpdate(idMess:any) {
    this.toastr.success();
    this.message.remove(idMess);
    this.disable = false;
    this.closeModal();
  }

  errorCreateOrUpdate(err:any, idMess:any) {
    this.toastr.error(err?.error.message);
    this.message.remove(idMess);
    this.disable = false;
  }
}
