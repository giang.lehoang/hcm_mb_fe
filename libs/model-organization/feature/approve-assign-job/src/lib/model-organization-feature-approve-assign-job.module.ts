import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ApproveAssignJobComponent} from "./approve-assign-job/approve-assign-job.component";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {FormsModule} from "@angular/forms";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import {RouterModule} from "@angular/router";
import {NzGridModule} from "ng-zorro-antd/grid";

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, SharedUiMbButtonModule, TranslateModule, SharedUiMbSelectModule, NzGridModule,
    RouterModule.forChild([
      {
        path: '',
        component: ApproveAssignJobComponent,
      }]),
    SharedUiMbInputTextModule, NzDropDownModule, FormsModule, NzTableModule, NzPaginationModule],
  declarations: [ApproveAssignJobComponent],
  exports: [ApproveAssignJobComponent]
})
export class ModelOrganizationFeatureApproveAssignJobModule {}
