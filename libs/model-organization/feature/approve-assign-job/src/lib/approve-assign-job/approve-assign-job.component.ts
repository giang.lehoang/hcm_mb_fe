import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import { FunctionCode } from '@hcm-mfe/shared/common/enums';
import {ApproveJob, JobPosition, JobView, Pagination, SearchJob} from "@hcm-mfe/model-organization/data-access/models";
import {userConfig} from "@hcm-mfe/shared/common/constants";
import {JobService} from "@hcm-mfe/model-organization/data-access/services";
import {SearchService} from "@hcm-mfe/shared/core";

@Component({
  selector: 'app-approve-assign-job',
  templateUrl: './approve-assign-job.component.html',
  styleUrls: ['./approve-assign-job.component.scss'],
})
export class ApproveAssignJobComponent extends BaseComponent implements OnInit {
  @ViewChild('formReject', { static: false }) formReject: NgForm | undefined;

  isLoadingPage: boolean;
  isSubmittedReject: boolean;
  checked = false;
  objSearch: SearchJob;
  params: SearchJob;
  pagination: Pagination = new Pagination(userConfig.pageSize);
  indeterminate = false;
  modelJobs: unknown[];
  modelJob = {
    ORG: 'Mô hình cứng',
    DV: 'Mô hình dịch vụ',
  };
  statusJob = {
    '1': 'Phê duyệt',
    '2': 'Chờ duyệt',
    '3': 'Từ chối',
  };
  setOfCheckedId = new Set<number>();
  arrayId:number[] = [];
  showComment = false;
  comment: string | null;
  initData: JobPosition[] = [];
  listJob:JobView[] = [];
  listJobSearch:JobView[] = [];

  constructor(
    injector: Injector,
    readonly jobService: JobService,
    readonly searchService: SearchService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.APPROVE_ASSIGN_JOB}`);
    this.modelJobs = [
      { value: 'ORG', label: 'Mô hình cứng' },
      { value: 'DV', label: 'Mô hình dịch vụ' },
    ];
    this.objSearch = {
      flagStatus: 2,
      jobType: 'ORG',
      jobName: '',
      page: 0,
      size: 50,
    };

    this.params = {
      page: 0,
      size: 15,
      jobType: '',
      jobName: '',
    };
    this.isLoadingPage = false;
    this.isSubmittedReject = false;
    this.comment = ''
  }

  ngOnInit(): void {
    this.onSearch();
    this.getAllJob();
  }

  getAllJob() {
    this.objSearch.jobType = this.objSearch.jobType ? this.objSearch.jobType : '';
    this.jobService.getListJob(this.objSearch.jobType).subscribe(
      (data) => {
        this.listJob = data.data;
      },
      (error) => {
        this.toastrCustom.error(error.message);
      }
    );
  }

  showRejectForm(): void {
    if (this.arrayId.length === 0) {
      this.toastrCustom.error('Vui lòng chọn Job để từ chối');
      return;
    }

    if (!this.showComment) {
      this.showComment = true;
      return;
    }

    this.isSubmittedReject = true;
    if (this.showComment && this.formReject?.valid) {
      const lstPosDTO:{id:number}[] = [];
      this.arrayId.forEach((el) => {
        lstPosDTO.push({
          id: el,
        });
      });

      const body:ApproveJob = {
        lstPosDTO: lstPosDTO,
        description: this.comment ? this.comment : '',
      };

      this.isLoadingPage = true;
      this.jobService.rejectJob(body).subscribe(
        (data) => {
          this.isLoadingPage = false;
          this.arrayId = [];
          this.resetFormReject();
          this.toastrCustom.success('Từ chối thành công');
          this.callJobFilter();
        },
        (error) => {
          this.isLoadingPage = false;
          this.toastrCustom.error('Từ chối thất bại');
        }
      );
    }
  }

  updateCheckedSet(posId: number, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(posId);
    } else {
      this.setOfCheckedId.delete(posId);
    }
  }

  onItemChecked(posId: number, checked: boolean): void {
    if (checked === true && !this.setOfCheckedId.has(posId)) {
      this.arrayId.push(posId);
    } else if (this.setOfCheckedId.has(posId)) {
      this.arrayId = this.arrayId.filter((element) => element !== posId);
    }

    this.updateCheckedSet(posId, checked);
  }

  goBackPage(): void {
    this.location.back();
  }

  onAllChecked(checked: boolean): void {
    this.initData.forEach((element: JobPosition) => {
      this.updateCheckedSet(element.posId, checked);
      if (checked === false) {
        this.arrayId = [];
      } else {
        this.arrayId.push(element.posId);
      }
    });
  }
  submit() {
    this.resetFormReject();

    if (this.arrayId.length === 0) {
      this.toastrCustom.error('Vui lòng chọn Job để duyệt');
      return;
    }

    const lstPosDTO:{id:number}[] = [];
    this.arrayId.forEach((num) => {
      lstPosDTO.push({
        id: num,
      });
    });

    const body = {
      lstPosDTO: lstPosDTO,
      description: '',
    };

    this.approveJob(body);
  }

  approveJob(body:ApproveJob): void {
    this.isLoadingPage = true;
    this.jobService.approveJob(body).subscribe(
      (data) => {
        this.isLoadingPage = false;
        this.arrayId = [];
        this.checked = true;
        this.toastrCustom.success('Duyệt thành công');
        this.callJobFilter();
      },
      (error) => {
        this.isLoadingPage = false;
        this.toastrCustom.error('Duyệt thất bại');
      }
    );
  }

  submitAll(): void {
    this.resetFormReject();

    const params:SearchJob = {
      jobName: "",
      jobType: "",
      page: 0,
      size: this.pagination.totalElements,
      flagStatus: 2
    };
    const body:ApproveJob = {
      lstPosDTO: [],
      description: '',
    };
    this.jobService.getListJobApprove(params).subscribe((data) => {
      if (!data.data.content) {
        return;
      }
      data.data.content.forEach((el) => {
        if (el.flagStatus === 2) {
          body.lstPosDTO.push({
            id: el.posId,
          });
        }
      });
      this.approveJob(body);
    });
  }

  callJobFilter() {
    this.isLoadingPage = true;
    this.jobService.getListJobApprove(this.params).subscribe(
      (data) => {
        this.initData = data.data.content;
        this.pagination.totalElements = data.data.totalElements;
        this.pagination.totalPages = data.data.totalPages;
        this.pagination.numberOfElements = data.data.numberOfElements;
        this.isLoadingPage = false;
        this.checked = false;
      },
      (error) => {
        this.message.error(error.error.message);
        this.initData = [];
        this.isLoadingPage = false;
      }
    );
  }

  onSearch() {
    this.params = {
      page: 0,
      size: 15,
      jobName: this.objSearch.jobName ? this.objSearch.jobName : '',
      jobType: this.objSearch.jobType ? this.objSearch.jobType : '',
      flagStatus: this.objSearch.flagStatus ? this.objSearch.flagStatus : '',
    };
    this.pagination.currentPage = 1;
    this.callJobFilter();
  }

  changePage() {
    this.params.page = this.pagination.currentPage - 1;
    this.callJobFilter();
  }

  resetFormReject() {
    this.showComment = false;
    this.isSubmittedReject = false;
    this.comment = null;
  }
  onSearchJob(e: KeyboardEvent) {
    this.listJobSearch= [];
    const value = (e.target as HTMLTextAreaElement).value;
    if (value) {
      if (!this.objSearch.jobType) {
        this.listJob.forEach((item) => {
          if (
            this.searchService
              .replaceUnicode(item.jobName.toLowerCase())
              .includes(this.searchService.replaceUnicode(value.toLowerCase()))
          ) {
            this.listJobSearch.push(item);
          }
        });
      } else {
        this.listJob
          .filter((job) => job.jobType === this.objSearch.jobType)
          .forEach((v) => {
            if (
              this.searchService
                .replaceUnicode(v.jobName.toLowerCase())
                .includes(this.searchService.replaceUnicode(value.toLowerCase()))
            ) {
              this.listJobSearch.push(v);
            }
          });
      }
    } else {
      this.listJobSearch = [];
    }
  }
  onClickJob(value:string) {
    this.objSearch.jobName = value;
  }

  override triggerSearchEvent(): void {
    this.onSearch();
  }
}
