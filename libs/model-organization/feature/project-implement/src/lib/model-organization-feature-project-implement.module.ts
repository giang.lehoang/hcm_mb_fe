import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ProjectImplementComponent} from "./project-implement/project-implement.component";
import {NzGridModule} from "ng-zorro-antd/grid";
import {NzCardModule} from "ng-zorro-antd/card";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {FormsModule} from "@angular/forms";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
import {RouterModule} from "@angular/router";
import {NzButtonModule} from "ng-zorro-antd/button";
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
    imports: [CommonModule, NzGridModule, NzCardModule, SharedUiLoadingModule, SharedUiMbSelectModule, FormsModule,
      SharedUiMbButtonModule, TranslateModule, NzTableModule, NzDropDownModule, NzButtonModule,
      NzPaginationModule, NzToolTipModule, RouterModule.forChild([
        {
          path: '',
          component: ProjectImplementComponent,
        }])],
  declarations: [ProjectImplementComponent],
  exports: [ProjectImplementComponent]
})
export class ModelOrganizationFeatureProjectImplementModule {}
