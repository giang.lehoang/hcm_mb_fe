import { Component, Injector, OnInit } from '@angular/core';
import * as FileSaver from 'file-saver';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {functionUri, userConfig} from "@hcm-mfe/shared/common/constants";
import { FunctionCode } from '@hcm-mfe/shared/common/enums';
import {
  Pagination,
  PRJImplResponse,
  ProjectInfo,
  SearchPRJ, PRJResponse
} from "@hcm-mfe/model-organization/data-access/models";
import {ProjectService} from "@hcm-mfe/model-organization/data-access/services";

@Component({
  selector: 'app-project-implement',
  templateUrl: './project-implement.component.html',
  styleUrls: ['./project-implement.component.scss'],
})
export class ProjectImplementComponent extends BaseComponent implements OnInit {
  isLoadingPage: boolean;
  searchObj: SearchPRJ = {
    prjId: '',
    prjName: '',
    page: 0,
    size: userConfig.pageSize,
  };
  pagination: Pagination = new Pagination(userConfig.pageSize);
  listProject: Array<PRJResponse> = [];
  listProjectInfo: Array<PRJImplResponse> = [];

  constructor(
    readonly projectService: ProjectService,
    injector: Injector
  ) {
    super(injector);
    this.isLoadingPage = false;
  }

  ngOnInit(): void {
    this.isLoadingPage = true;
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.IMPLEMENT_PROJECT}`);
    if (!this.objFunction || !this.objFunction.view) {
      this.router.navigateByUrl(functionUri.access_denied)
    }
    this.getListProject();
    this.getListData();
  }

  getListProject() {
    const params:SearchPRJ = {
      prjName: '',
      prjId: '',
      orgName: '',
      flagStatus: 1,
    };
    this.projectService.getProjectList(params).subscribe((data) => {
      this.listProject = data.data.content || [];
    });
  }

  getListData() {
    this.searchObj.prjId = this.searchObj.prjId ? this.searchObj.prjId : '';
    this.isLoadingPage = true;
    this.projectService.getListProjectIplm(this.searchObj).subscribe((data) => {
      this.isLoadingPage = false;
      this.listProjectInfo = data.data.content || [];
      this.pagination.totalElements = data.data.totalElements;
      this.pagination.size = data.data.size;
      this.pagination.currentPage = data.data.number;
      this.pagination.numberOfElements = data.data.numberOfElements;
    }, () => {
      this.isLoadingPage = false;
    });
  }

  viewDetail(id:string) {
    this.router.navigateByUrl('/organization/line-group/manage/' + id);
  }

  assignStaff(project: PRJImplResponse) {
    const projectInfo: ProjectInfo = {
      prjId: project.prjId,
      prjName: project.prjName,
      empNum: project.countEmp,
      fromDate: new Date(project.fromDate).setHours(0,0,0,0),
      toDate: project.toDate ? new Date(project.toDate).setHours(0,0,0,0) : null,
    };
    this.projectService.setProjectInfo(projectInfo);
    this.router.navigateByUrl(functionUri.project_assign_staff);
  }

  viewProject(project: PRJImplResponse) {
    const projectInfo: ProjectInfo = {
      prjId: project.prjId,
      prjName: project.prjName,
      empNum: project.countEmp,
      fromDate: new Date(project.fromDate).setHours(0,0,0,0),
      toDate: project.toDate ? new Date(project.toDate).setHours(0,0,0,0) : null,
    };
    this.projectService.setProjectInfo(projectInfo);
    this.router.navigateByUrl(functionUri.project_assign_staff + '/view');
  }

  changePage(value:number) {
    this.searchObj.page = value - 1;
    this.getListData();
  }

  search() {
    this.searchObj.page = 0;
    this.getListData();
  }
  downloadExcel() {
    const objTmp = { ...this.searchObj };
    delete objTmp.page;
    delete objTmp.size;
    delete objTmp.prjName;
    this.projectService.downloadExcelImpl(objTmp).subscribe((data) => {
      const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      FileSaver.saveAs(blob, 'Triển khai dự án.xls');
    });
  }
  goBackPage() {
    this.location.back();
  }

  trackByFn(index:number, item: PRJImplResponse) {
    return item.prjId;
  }

  override triggerSearchEvent(): void {
      this.search()
  }
}
