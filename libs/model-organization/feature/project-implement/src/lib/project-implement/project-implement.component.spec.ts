import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectImplementComponent } from './project-implement.component';

describe('ProjectImplementComponent', () => {
  let component: ProjectImplementComponent;
  let fixture: ComponentFixture<ProjectImplementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectImplementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectImplementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
