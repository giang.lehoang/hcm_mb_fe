import { Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { Pagination, Template } from '@hcm-mfe/model-organization/data-access/models';
import { FunctionCode, functionUri } from '@hcm-mfe/shared/common/enums';
import { OrganizationalModelTemplateService } from '@hcm-mfe/model-organization/data-access/services';
import {MBTableConfig} from "@hcm-mfe/shared/data-access/models";


@Component({
  selector: 'app-list-model-template',
  templateUrl: './list-model-template.component.html',
  styleUrls: ['./list-model-template.component.scss'],
})
export class ListModelTemplateComponent extends BaseComponent implements OnInit  {
  listTemplate:Template[] = [];
  pagination: Pagination;
  loading = false;
  objectSearch: any;
  tableConfig = new MBTableConfig();
  constructor(
    injector:Injector,
    private readonly organizationalModelTemplateService: OrganizationalModelTemplateService,
  ) {
    super(injector)
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ORGANIZATION_ORG_TEMPLATE}`);
    if(!this.objFunction.view){
      this.router.navigateByUrl(functionUri.access_denied);
    }
    console.log(this.objFunction)
    this.pagination = {
      currentPage: 1,
      totalElement: 0,
      size: 15,
      totalPage: 0,
      numberOfElements:0
    };

    this.objectSearch = {
      page: 0,
      size: 15,
      orgName: '',
      flagStatus: ''
    };
  }

  ngOnInit(): void {

    this.tableConfig.showFrontPagination = true;
    this.tableConfig = {
      headers: [
        // {
        //   title: 'polManagement.table.employeeCode',
        //   field: 'employeeCode',
        //   width: 80,
        //   fixed: window.innerWidth > 1024,
        //   fixedDir: 'left',
        // },
        // {
        //   title: 'polManagement.table.fullName',
        //   field: 'fullName',
        //   width: 100,
        //   fixed: window.innerWidth > 1024,
        //   fixedDir: 'left',
        // },
        // {
        //   title: 'polManagement.table.org',
        //   field: 'lastName',
        //   needEllipsis:true,
        //   fixed: window.innerWidth > 1024,
        //   width: 150,
        // },
        // {
        //   title: 'polManagement.table.jobName',
        //   field: 'jobName',
        //   fixed: window.innerWidth > 1024,
        //   width: 100
        // },
        {
          title: 'polManagement.table.fromDate',
          field: 'fromDate',
          pipe:'date',
          fixed: window.innerWidth > 1024,
          width: 80,
        },
        {
          title: 'polManagement.table.toDate',
          field: 'toDate',
          pipe:'date',
          fixed: window.innerWidth > 1024,
          width: 80,
        },
        {
          title: 'polManagement.table.salaryCommitmentLevel',
          field: 'amount',
          pipe:'currencyNumber',
          width: 100,
          fixed: window.innerWidth > 1024,
          tdClassList: ['text-right'],
        },
        {
          title: 'polManagement.table.curCode',
          field: 'curCode',
          fixed: window.innerWidth > 1024,
          width: 60,
        },
        {
          title: 'polManagement.table.bonus',
          field: 'bonusName',
          fixed: window.innerWidth > 1024,
          width: 60,
        },
        {
          title: 'polManagement.table.status',
          field: 'flagStatus',
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          // tdTemplate: this.flagStatus,
          fixed: window.innerWidth > 1024,
          width: 100,
        },
        {
          title: 'polManagement.table.action',
          // tdTemplate: this.action,
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          width: 80,
          fixed: window.innerWidth > 1024,
          fixedDir: 'right',
        }
      ],
      total: 0,
      loading: false,
      size: 'small',
      pageSize: this.pagination.size,
      pageIndex: 1,
      widthIndex: '30px'
    };
    this.callSearch()
  }

  goBackPage() {
    this.location.back();
  }

  changeOrgName(event:any) {
    if (event.target.value !== '') {
      this.objectSearch.orgName = event.target.value;
    } else {
      delete this.objectSearch.orgName;
    }
  }

  selectStatus(event:any) {
    this.objectSearch.flagStatus = event;
  }

  redrirectToAddModelTemplate() {
    this.router.navigateByUrl(functionUri.template_init);
  }

  changePage() {
    this.objectSearch.page = this.pagination.currentPage - 1;
    this.callSearch();
  }

  redrirectToInitTemaplatePage(value:any) {
    if(!this.objFunction.edit){
      return
    }
    localStorage.setItem('selectedId', value.orgId);
    this.router.navigateByUrl(functionUri.template_init);
  }


  callSearch(){
    if(this.objectSearch.flagStatus === null){
      this.objectSearch.flagStatus = ''
    }
    this.organizationalModelTemplateService.getListTemplate(this.objectSearch).subscribe((data:any) => {
      this.loading = false
      this.listTemplate = data.data.content;
      this.pagination.totalElement = data.data.totalElements;
      this.pagination.totalPage = data.data.totalPages;
      this.pagination.numberOfElements = data.data.numberOfElements
      this.pagination.size = data.data.size
    }, (error:any) => {
      this.loading = false
      this.toastrCustom.error(error.error?.message)
    });
  }

  searchListTemplate() {
    this.callSearch()
  }

  onDelete(id:any){
    this.deletePopup.showModal(() => {
      this.loading = true
      this.organizationalModelTemplateService.deleteTemplates(id).subscribe(
        (data:any) => {
          if(data.data){
            this.toastrCustom.success("Xóa thành công");
          } else {
            this.toastrCustom.error("Đã gán dữ liệu, không được xóa");
          }
          this.callSearch();
        },
        (error:any) => {
          this.toastrCustom.error(error.error.message);
          this.loading = false
        }
      );
    })
  }

  override triggerSearchEvent(): void {
      this.searchListTemplate()
  }
}
