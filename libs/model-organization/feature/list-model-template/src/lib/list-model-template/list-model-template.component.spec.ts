import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListModelTemplateComponent } from './list-model-template.component';

describe('ListModelTemplateComponent', () => {
  let component: ListModelTemplateComponent;
  let fixture: ComponentFixture<ListModelTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListModelTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListModelTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
