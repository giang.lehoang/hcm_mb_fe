import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { FormsModule } from '@angular/forms';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { RouterModule } from '@angular/router';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { SharedUiMbDatePickerModule } from '@hcm-mfe/shared/ui/mb-date-picker';
import { ListModelTemplateComponent } from './list-model-template/list-model-template.component';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";

@NgModule({
    imports: [CommonModule, SharedUiLoadingModule, FormsModule, SharedUiMbButtonModule,
        TranslateModule, SharedUiMbInputTextModule, SharedUiMbSelectModule, NzTableModule, NzPaginationModule,
        RouterModule.forChild([
            {
                path: '',
                component: ListModelTemplateComponent
            }]), NzGridModule, NzSwitchModule, NzToolTipModule, SharedUiMbDatePickerModule, NzSelectModule, NzTagModule, NzInputModule, NzTypographyModule, SharedUiMbTableModule],
  declarations: [ListModelTemplateComponent],
  exports: [ListModelTemplateComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ModelOrganizationFeatureListModelTemplateModule {}
