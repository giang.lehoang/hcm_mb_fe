import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { FormsModule } from '@angular/forms';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { RouterModule } from '@angular/router';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { SharedUiMbDatePickerModule } from '@hcm-mfe/shared/ui/mb-date-picker';
import { ApprovedStaffComponent } from './approved-staff/approved-staff.component';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzIconModule } from 'ng-zorro-antd/icon';
import {PdfViewerModule} from "ng2-pdf-viewer";

@NgModule({
    imports: [CommonModule, SharedUiLoadingModule, FormsModule, SharedUiMbButtonModule,
        TranslateModule, SharedUiMbInputTextModule, SharedUiMbSelectModule, NzTableModule, NzPaginationModule,
        RouterModule.forChild([
            {
                path: '',
                component: ApprovedStaffComponent
            }]), NzGridModule, NzSwitchModule, NzToolTipModule, SharedUiMbDatePickerModule, NzModalModule, NzInputModule, NzIconModule, PdfViewerModule],
  declarations: [ApprovedStaffComponent],
  exports: [ApprovedStaffComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ModelOrganizationFeatureApprovedStaffModule {}
