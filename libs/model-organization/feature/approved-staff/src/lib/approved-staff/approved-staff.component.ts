import { Component, HostListener, Injector, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { forkJoin } from 'rxjs';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import {
  Job,
  MB_BANK,
  ObjectSearch,
  REGEX_NUMBER,
  StaffData,
  Year
} from '@hcm-mfe/model-organization/data-access/models';
import { FunctionCode, types } from '@hcm-mfe/shared/common/enums';
import { CustomToastrService } from '@hcm-mfe/shared/core';
import { DataService } from '@hcm-mfe/shared/data-access/services';
import { ApproveStaffService, StaffPlanService } from '@hcm-mfe/model-organization/data-access/services';
import { userConfig } from '@hcm-mfe/shared/common/constants';
import { PopupService } from '@hcm-mfe/shared/ui/popup';
import { arrayBufferToBase64 } from '@hcm-mfe/shared/common/utils';
import { FormModalShowTreeUnitComponent } from '@hcm-mfe/shared/ui/form-modal-show-tree-unit';
import * as moment from 'moment';
@Component({
  selector: 'app-approved-staff',
  templateUrl: './approved-staff.component.html',
  styleUrls: ['./approved-staff.component.scss']
})
export class ApprovedStaffComponent extends BaseComponent implements OnInit {
  isOpenPopup: boolean = false;
  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.key === "Enter" && !this.isOpenPopup) {
      this.onSearch();
    }
  }
  objectSearch: ObjectSearch;
  checked = false;
  loading = false;
  indeterminate = false;
  dataTable: StaffData[] = [];
  quantityMap: Map<number, StaffData> = new Map<number, StaffData>();
  isLoadingPage = false;
  listYears: Year[] = [];
  listJob: Job[] = [];
  modalRef: NzModalRef| undefined;
  listType = types;
  isValid = false;
  file: any;
  isVisible = false;
  pdfSrc = '';
  imagePath: any;
  isPdfFile = false;
  dataTableTmp: StaffData[] = [];
  listDataApproveAll:StaffData[] = [];
  constructor(
    private readonly toast: CustomToastrService,
    private readonly  dataService: DataService,
    private readonly staffPlanService: StaffPlanService,
    private readonly approvedService: ApproveStaffService,
    private readonly popupService: PopupService,
    readonly _sanitizer: DomSanitizer,
    injector: Injector
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.APPROVED_STAFF}`);
    this.objectSearch = {
      jobId: '',
      periodCode: new Date().getFullYear().toString(),
      page: 0,
      size: userConfig.pageSize,
      orgId: '2',
      orgName: MB_BANK
    };
  }

  ngOnInit(): void {
    this.isLoadingPage = false;
    this.fetchData(true);
  }

  fetchData(init: boolean) {
    this.listDataApproveAll= [];
    this.isLoadingPage = true;
    const request = [];
    if (init) {
      request.push(this.staffPlanService.getListYear());
      request.push(this.dataService.getJobsByType());
    }
    this.checkParams();
    request.push(this.approvedService.getListStaffApprove(this.objectSearch));

    forkJoin(request).subscribe(
      (data: any) => {
        let plans: any;
        if (init) {
          data[0].data.periodCode.forEach((item:any) => {
            this.listYears.push({ value: item });
          });

          this.listJob = data[1].data.content;
          plans = data[2];
        } else {
          plans = data[0];
        }
        this.dataTable = plans.data;
        this.dataTable.length > 0 && this.dataTable.forEach((item:StaffData)  => {
          item.latchNumber= this.latchNumber(item);
          item.adjustedNumberTmp = item.adjustedNumber;
        });
        this.dataTableTmp = JSON.parse(JSON.stringify(this.dataTable));
        this.quantityMap.clear();
        this.checked = false;
        this.isLoadingPage = false;
      },
      (error) => {
        this.isLoadingPage = false;
        this.dataTable = [];
        this.listYears = [];
        this.listJob = [];
        this.toast.error(error.message);
      }
    );
  }

  updateCheckedSet(item: StaffData, checked: boolean): void {
    if (checked) {
      this.quantityMap.set(item.hcpId, item);
    } else {
      this.quantityMap.delete(item.hcpId);
    }
  }

  onCurrentPageDataChange(listOfCurrentPageData: StaffData[]): void {
    this.dataTable = listOfCurrentPageData;
    this.refreshCheckedStatus();
  }

  refreshCheckedStatus(): void {
    this.checked = this.dataTable.every(({ hcpId }) => this.quantityMap.has(hcpId));
  }

  onItemChecked(item: StaffData, checked: boolean): void {
    this.updateCheckedSet(item, checked);
    this.refreshCheckedStatus();
  }

  onAllChecked(checked: boolean): void {
    if (checked) {
      this.dataTable.forEach((item) => this.updateCheckedSet(item, checked));
    } else {
      this.quantityMap.clear();
    }

    this.refreshCheckedStatus();
  }

  onSearch() {
    this.checkParams();
    this.objectSearch.page = 0;
    this.fetchData(false);
  }

  checkParams() {
    this.objectSearch.jobId = this.objectSearch.jobId ? this.objectSearch.jobId : '';
    this.objectSearch.orgId = this.objectSearch.orgId ? this.objectSearch.orgId : '';
    this.objectSearch.periodCode = this.objectSearch.periodCode ? this.objectSearch.periodCode : '';
    this.objectSearch.inputType = this.objectSearch.inputType ? this.objectSearch.inputType : '';
    this.objectSearch.lastUpdateDate = this.objectSearch.lastUpdateDateTmp?
      moment(this.objectSearch.lastUpdateDateTmp).format('YYYY-MM-DD'):'';
    delete this.objectSearch.page;
    delete this.objectSearch.size;
  }

  showPopup() {
    this.isOpenPopup = true;
    this.modalRef = this.modal.create({
      nzWidth: '80%',
      nzTitle: undefined,
      nzComponentParams: {
        isAuthor: true
      },
      nzContent: FormModalShowTreeUnitComponent,
      nzBodyStyle: {
        padding: '24px',
        border: 'none',
        'border-radius': '20px',
      },
      nzFooter: null,
    });

    this.modalRef.afterClose.subscribe(result => {
      if (result?.orgName) {
        this.objectSearch.orgName = result.orgName;
        this.objectSearch.orgId = result.orgId;
      }
      this.isOpenPopup = false;
    });
  }

  showPopupApproved() {
    this.popupService.showApprove(() => {
      this.approvedAll();
    })
  }

  approvedAll() {
    this.objectSearch.description = this.objectSearch.description ? this.objectSearch.description.trim() : "";
    const headCountPlanDTOS:any = [];
    this.listDataApproveAll.forEach(item => {
      headCountPlanDTOS.push({
        id: item.hcpId,
        adjustNumber: item.adjustedNumber
      })
    })
    const requestBody = {
      ... this.objectSearch,
      headCountPlanDTOS: headCountPlanDTOS
    }
    if(!this.objectSearch.orgId && !this.objectSearch.periodCode && !this.objectSearch.jobId && !this.objectSearch.inputType ){
      delete requestBody.orgId;
      delete requestBody.periodCode;
      delete requestBody.jobId;
      delete requestBody.inputType;
    }
    delete requestBody.orgName;
    this.isLoadingPage = true;
    this.approvedService.transferAll(requestBody).subscribe(
      (data) => {
        this.isLoadingPage = false;
        this.fetchData(false);
        this.toast.success();
        this.objectSearch.description = "";
        this.isValid = false;
      },
      (error) => {
        this.isLoadingPage = false;
        this.toast.error(error?.message);
      }
    );
  }

  approveTranfer() {
    this.checkSize();
    const approve: any = [];
    const headCountPlanDTOS:any = [];
    this.quantityMap.forEach((value, key) => {
      approve.push({ key });
      headCountPlanDTOS.push({
        id: key,
        adjustNumber: this.checkAdjustNumber(value)
      });
    });
    const requestBody = {
      ids: approve.map((value:any) => value.key),
      description: this.objectSearch.description ? this.objectSearch.description.trim() : "",
      headCountPlanDTOS: headCountPlanDTOS
    }
    if (this.quantityMap.size > 0) {
      this.isLoadingPage = true;
      this.approvedService.transferApproved(requestBody).subscribe(
        (data) => {
          this.isLoadingPage = false;
          this.fetchData(false);
          this.objectSearch.description = "";
          this.toast.success();
          this.isValid = false;
        },
        (error) => {
          this.isLoadingPage = false;
          this.toast.error(error?.message);
        }
      );
    }
  }

  reject() {
    this.objectSearch.description = this.objectSearch.description ? this.objectSearch.description.trim() : "";
    if (!this.objectSearch.description) {
      this.isValid = true
      return;
    } else {
      this.isValid = false
    }
    this.checkSize();
    const approve:any[] = [];
    this.quantityMap.forEach((value, key) => {
      approve.push({ key });
    });
    const requestBody = {
      ids: approve.map(value => value.key),
      description: this.objectSearch.description ? this.objectSearch.description.trim() : "",
    }
    if (this.objectSearch.description && this.quantityMap.size > 0) {
      this.isLoadingPage = true;
      this.approvedService.reject(requestBody).subscribe(
        (data) => {
          this.isLoadingPage = false;
          this.objectSearch.description = "";
          this.fetchData(false);
          this.toast.success();
          this.isValid = false;
        },
        (error) => {
          this.isLoadingPage = false;
          this.toast.error(error?.message);
        }
      );
    }
  }

  clearOrgInput() {
    this.objectSearch.orgName = '';
    this.objectSearch.orgId = '';
  }
  checkSize() {
    if (this.quantityMap.size === 0) {
      this.toast.error(this.translate.instant("modelOrganization.approvedStaff.notification.noSelected"));
      return;
    }
  }

  latchNumber(data:any) {
    if (data.inputType === "KT") {
      return data.headCount;
    } else {
      return Number(data.headCount) + Number(data.adjustedNumber);
    }
  }

  trackByFn(index:any, item: StaffData) {
    return item.hcpId;
  }
  downloadFile(item:any) {
    this.approvedService.downloadFile(item.docId).subscribe((data) => {
      if(item.docName.includes('.pdf')){
        this.isPdfFile = true;
        const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.pdf' });
        this.file = new File([blob], String(item.docName));
        this.onFileSelected();
      } else if(item.docName.includes('.png') || item.docName.includes('.jpg') || item.docName.includes('.jpeg') ) {
        this.isPdfFile = false;
        this.imagePath = this._sanitizer.bypassSecurityTrustResourceUrl(
      'data:image/jpg;base64,' + arrayBufferToBase64(data));
      this.isVisible = true;
      }
    });
  }

  onFileSelected() {
    const reader = new FileReader();
    reader.onload = (e: any) => {
      this.pdfSrc = e.target.result;
    };
    reader.readAsArrayBuffer(this.file);
    this.isVisible = true;
  }

  handleCancel(): void {
    this.isVisible = false;
  }
  checkAdjustNumber(data: StaffData) {
    let adjustedNumber = 0;
    const checkValue = Number(data.adjustedNumberTmp) + Number(data.headCount);
    adjustedNumber = Number(data.latchNumber) - checkValue;
    return adjustedNumber;
  }
  checkLatchNumber(data:StaffData,index:number){
    const oldObj:any = this.dataTableTmp.find(item => item.hcpId === data.hcpId);
    if(Number(data.latchNumber) !== Number(oldObj.latchNumber)){
      this.listDataApproveAll.push(data);
    } else {
      this.listDataApproveAll.splice(index,1)
    }
    this.listDataApproveAll = [...new Set(this.listDataApproveAll)];
  }
  onKeypress(event: any) {
    const pattern = REGEX_NUMBER;
    if (!pattern.test(event.key)) {
      event.preventDefault();
      return;
    }
  }
}
