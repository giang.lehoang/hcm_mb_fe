import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprovedStaffComponent } from './approved-staff.component';

describe('ApprovedStaffComponent', () => {
  let component: ApprovedStaffComponent;
  let fixture: ComponentFixture<ApprovedStaffComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApprovedStaffComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprovedStaffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
