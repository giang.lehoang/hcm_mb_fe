import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AttachJobComponent } from './attach-job.component';

describe('AttachJobComponent', () => {
  let component: AttachJobComponent;
  let fixture: ComponentFixture<AttachJobComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AttachJobComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AttachJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
