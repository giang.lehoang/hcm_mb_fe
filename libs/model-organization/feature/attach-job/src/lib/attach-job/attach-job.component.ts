import { Component, DoCheck, Injector, OnInit } from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd/modal';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {functionUri} from "@hcm-mfe/shared/common/constants";
import { FunctionCode } from '@hcm-mfe/shared/common/enums';
import {JobService, OrganizationService} from "@hcm-mfe/model-organization/data-access/services";
import {
  DetailListTemp,
  OrgInfo,
  OrgType, ResponseEntity, ResponseError,
  SearchJob,
  TreeOrganization,
  TYPE_JOB
} from "@hcm-mfe/model-organization/data-access/models";
import { MultiSelectUnitPopupComponent } from '@hcm-mfe/shared/ui/multi-select-unit-popup';
import { IDataResponse, ITreeOrganization, MBTableConfig } from '@hcm-mfe/shared/data-access/models';
import { DataService } from '@hcm-mfe/shared/data-access/services';

@Component({
  selector: 'app-attach-job',
  templateUrl: './attach-job.component.html',
  styleUrls: ['./attach-job.component.scss'],
})
export class AttachJobComponent extends BaseComponent implements OnInit, DoCheck {
  checked: boolean | undefined;
  arrayId: Array<number> = [];
  setOfCheckedId = new Set<number>();
  numberTab: number;
  mapOfExpandedData:any[] = [];
  modalRef: NzModalRef | undefined;
  orgName: string;
  orgId: number | undefined;
  isLoadingPage: boolean;
  radioValue: string = TYPE_JOB;
  showTableForms: boolean;
  isVisible: boolean;
  valueSelect: string;
  initData: Array<string> = [];
  setOfCheckedIdModal = new Set<number>();
  options: Array<{
    label: string;
    value: string;
  }> = [
    { label: 'Mô hình cứng', value: 'ORG' },
    { label: 'Mô hình dịch vụ', value: 'DV' },
  ];

  listJob: Array<SearchJob> = [
    {
      page: null,
      size: null,
      jobCode: '',
      jobName: '',
      jobType: '',
      flagStatus: null,
    },
  ];
  listTemplate: Pick<OrgInfo, 'orgId' | 'orgName'>[] = [];
  lisTemplateDetail: Pick<DetailListTemp, OrgType>[] = [
    {
      orgId: null,
      orgName: '',
      orgLevel: null,
      subs: [],
    },
  ];
  selectedJob: string;
  pageableSession = {
    size: 0,
    totalElements: 0,
    currentPage: 0,
    totalPages: 0,
    recordStart: 0,
    recordTo: 0
  };
  dataDisplay: ITreeOrganization[]|undefined = [];
  tableConfig:MBTableConfig;

  recordTo: number | undefined;
  orgIdList: Array<number> = [];
  heightTable = { x: '70vw', y: '45vh'};
  params:any

  quantityMap: Map<number, TreeOrganization> = new Map<number, TreeOrganization>();

  collapse(array:any[], data:any, $event: boolean): void {
    if (!$event) {
      if (data.children) {
        data.children.forEach((d:any) => {
          const target = array.find((a) => a.key === d.key);
          target.expand = false;
          this.collapse(array, target, false);
        });
      } else {
        return;
      }
    }
  }

  convertTreeToList(root:any) {
    const stack = [];
    const array:any[] = [];
    const hashMap = {};
    stack.push({ ...root, level: 0, expand: false });

    while (stack.length !== 0) {
      const node = stack.pop();
      this.visitNode(node, hashMap, array);
      if (node.subs) {
        for (let i = node.subs.length - 1; i >= 0; i--) {
          stack.push({ ...node.subs[i], level: node.level + 1, expand: true, parent: node });
        }
      }
    }
    return array;
  }

  visitNode(node:any, hashMap: any, array:any[]): void {
    if (!hashMap[node.orgId]) {
      hashMap[node.orgId] = true;
      array.push(node);
    }
  }


  constructor(
    injector: Injector,
    readonly jobService: JobService,
    private readonly orgService:OrganizationService,
    readonly dataService: DataService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ASSIGN_JOB_FOR_UNIT}`);
    if (!this.objFunction || !this.objFunction.view) {
      this.router.navigateByUrl(functionUri.access_denied).then();
    }
    this.selectedJob = '';
    this.orgName = '';
    this.numberTab = 0;
    this.isLoadingPage = false;
    this.showTableForms = false;
    this.isVisible = false;
    this.valueSelect = '';
    this.tableConfig = new MBTableConfig();

  }

  ngDoCheck(): void {
    if (this.numberTab === 1) {
      this.showTableForms = true;
    } else {
      this.showTableForms = false;
    }
  }

  ngOnInit(): void {

    console.log(this.objFunction)
    this.jobService.getJobType(this.radioValue).subscribe(
      (data) => {
        this.listJob = data.data.content;
      },
      (error: ResponseError) => {
        this.toastrCustom.error(error.error.message);
      }
    );
    this.orgService.getListTemplate().subscribe((data: ResponseEntity<Pick<OrgInfo, 'orgId' | 'orgName'>[]>) => {
      this.listTemplate = data.data;
    },
      (error: ResponseError) => {
        this.toastrCustom.error(error.error.message);
      });
    this.initTable();
  }

  changeRadio(value:string) {
    this.selectedJob = '';
    this.jobService.getJobType(value).subscribe( (data) => {
      this.listJob = data.data.content;
    }, (error: ResponseError) => {
      this.toastrCustom.error(error.error.message);
    });
  }

  updateCheckedSetModal(id: number, checked: boolean): void {
    if (checked) {
      this.setOfCheckedIdModal.add(id);
    } else {
      this.setOfCheckedIdModal.delete(id);
    }
  }

  showModal(): void {
    this.modalRef = this.modal.create({
      nzWidth: '90vw',
      nzTitle: "",
      nzCentered: true,
      nzFooter: null,
      nzComponentParams: {
        orgIds: this.orgIdList
      },
      nzContent: MultiSelectUnitPopupComponent,
    });
    this.modalRef.afterClose.subscribe((result) => {
      if(!result){
        return;
      }
      if(result?.orgIds && result?.orgIds.length === 0){
        this.dataDisplay=[];
        return ;
      }
      this.orgIdList = result?.orgIds;
      this.params = {
        orgIds: result?.orgIds,
        isAuthor:result?.isAuthor,
        scope:result?.scope,
        resourceCode:result?.resourceCode,
        isMulti: true,
        isSelected:result?.isSelected,
        page: 0
      }
      this.tableConfig.pageIndex=1;
      this.getDataTable(this.params);
    });
  }

  selectForms(event:string) {
    this.valueSelect = event;
    if (event) {
      this.orgService
        .getListTemplateDetail(event)
        .subscribe((data: ResponseEntity<Pick<DetailListTemp, OrgType>[]>) => {
          this.lisTemplateDetail = data.data;
          data.data.forEach((item) => {
            if(item.orgId){
              this.mapOfExpandedData[item.orgId] = this.convertTreeToList(item);
            }
          });
        }, (error: ResponseError) => {
          this.toastrCustom.error(error.error.message);
        });
    } else {
      this.lisTemplateDetail = [];
      this.arrayId = [];
      this.setOfCheckedId.clear();
    }

    this.showTableForms = true;
  }

  updateCheckedSet(id: number, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }

  onItemChecked(id: number, checked: boolean): void {
    if (checked === true && !this.setOfCheckedId.has(id)) {
      this.arrayId.push(id);
    } else if (this.setOfCheckedId.has(id)) {
      this.arrayId = this.arrayId.filter((element) => element !== id);
    }

    this.updateCheckedSet(id, checked);
  }

  changeTab(event: number) {
    this.numberTab = event;
    if(this.numberTab === 0){
      this.initTable();
    }
  }

  submitJobForUnit() {
    const objectSubmit = {
      jobId: this.selectedJob,
      attrType: this.numberTab === 0 ? 'ORG' : 'TMP',
      orgIds: this.numberTab === 0 ? this.orgIdList : this.arrayId,
    };
    this.isLoadingPage = true;
    this.jobService.setJobForProject(objectSubmit).subscribe(
      (_: ResponseEntity<null>) => {
        this.router.navigateByUrl('/organization/job/job-assign-manage');
        this.toastrCustom.success(this.translate.instant('common.notification.sendSuccess'));
        this.isLoadingPage = false;
      },
      (error) => {
        this.toastrCustom.error(error?.message);
        this.isLoadingPage = false;
      }
    );
  }
  getDataTable(params){
    this.isLoadingPage = true;
    this.dataService.getOrgPopupMulti(params).subscribe((data: IDataResponse<ITreeOrganization[]>) => {
      this.dataDisplay = data?.content;
      this.tableConfig.total = data.totalElements;
      this.isLoadingPage = false;
    }, (error) => {
      this.toastrCustom.error(error.error?.message);
      this.isLoadingPage = false;
    });
  }
  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'common.label.org',
          field: 'orgName',
          width: 300,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left',
          show: true
        },
        {
          title: 'common.label.orgManage',
          field: 'pathName',
          width: 400,
          show: true
        },
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: 10,
      pageIndex: 1,
      showCheckbox: false,
      showFrontPagination: false
    };
  }
  changePage(page:number){
    this.params.page = page - 1;
    this.getDataTable(this.params);
  }
}
