import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AttachJobComponent} from "./attach-job/attach-job.component";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {FormsModule} from "@angular/forms";
import {TranslateModule} from "@ngx-translate/core";
import {NzTabsModule} from "ng-zorro-antd/tabs";
import {NzCardModule} from "ng-zorro-antd/card";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzRadioModule} from "ng-zorro-antd/radio";
import {RouterModule} from "@angular/router";
import {NzGridModule} from "ng-zorro-antd/grid";
import {NzInputModule} from "ng-zorro-antd/input";
import { SharedUiMbTableWrapModule } from '@hcm-mfe/shared/ui/mb-table-wrap';
import { SharedUiMbTableModule } from '@hcm-mfe/shared/ui/mb-table';


@NgModule({
  imports: [CommonModule, SharedUiMbSelectModule, FormsModule, TranslateModule, NzTabsModule, NzCardModule, NzGridModule,
    RouterModule.forChild([
      {
        path: '',
        component: AttachJobComponent
      }]),
    NzTableModule, NzPaginationModule, SharedUiLoadingModule, SharedUiMbButtonModule, NzRadioModule, NzInputModule, SharedUiMbTableWrapModule, SharedUiMbTableModule],
  declarations: [AttachJobComponent],
  exports: [AttachJobComponent]
})
export class ModelOrganizationFeatureAttachJobModule {}
