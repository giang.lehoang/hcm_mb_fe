import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InitUnitComponent } from './init-unit.component';

describe('InitUnitComponent', () => {
  let component: InitUnitComponent;
  let fixture: ComponentFixture<InitUnitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InitUnitComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InitUnitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
