import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EmployeeAssignmentComponent} from "./employee-assignment/employee-assignment.component";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzRadioModule} from "ng-zorro-antd/radio";
import {TranslateModule} from "@ngx-translate/core";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {ModelOrganizationUiSearchInputModule} from "@hcm-mfe/model-organization/ui/search-input";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {RouterModule} from "@angular/router";
import { NzIconModule } from 'ng-zorro-antd/icon';
import {NzFormModule} from "ng-zorro-antd/form";

@NgModule({
    imports: [CommonModule, SharedUiMbButtonModule, SharedUiLoadingModule, NzTableModule, NzRadioModule, TranslateModule,
        FormsModule, SharedUiMbSelectModule, ModelOrganizationUiSearchInputModule, NzToolTipModule, SharedUiMbDatePickerModule,
        SharedUiMbInputTextModule, ReactiveFormsModule, RouterModule.forChild([
            {
                path: '',
                component: EmployeeAssignmentComponent
            }]), NzIconModule, NzFormModule],
  declarations: [EmployeeAssignmentComponent],
  exports: [EmployeeAssignmentComponent]
})
export class ModelOrganizationFeatureEmployeeAssignmentModule {}
