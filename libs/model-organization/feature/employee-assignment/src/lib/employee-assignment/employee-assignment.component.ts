import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { Component, Injector, OnInit } from '@angular/core';
import { Subscription, forkJoin } from 'rxjs';
import * as moment from 'moment';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {toLowerCaseNonAccentVietnamese} from "@hcm-mfe/shared/common/utils";
import {
  Employee, EmployeeError,
  Job, ProjectAssignRequest, ProjectDetail,
  ProjectEmployeeTable,
  ProjectInfo,
  REGEX_NUMBER, ResponseEntity,
} from "@hcm-mfe/model-organization/data-access/models";
import {functionUri} from "@hcm-mfe/shared/common/constants";
import {ProjectService} from "@hcm-mfe/model-organization/data-access/services";

@Component({
  selector: 'employee-assignment',
  templateUrl: './employee-assignment.component.html',
  styleUrls: ['./employee-assignment.component.scss'],
})
export class EmployeeAssignmentComponent extends BaseComponent implements OnInit {
  form: FormGroup | undefined;
  dataTable: ProjectEmployeeTable[] = [];
  dataDelete: ProjectAssignRequest[] = [];
  projectInfo: ProjectInfo | undefined;
  jobs: Job[] = [];
  request: Subscription | null;
  isSubmit: boolean;
  pm: ProjectEmployeeTable | undefined;
  isLoadingPage: boolean;
  loadingSearch: boolean;
  options: Employee[] = [];
  jobPm: number | undefined;
  view: boolean;
  mapEmpError: Map<string, EmployeeError> = new Map<string, EmployeeError>();

  constructor(
    private readonly projectImplementService: ProjectService,
    injector: Injector
  ) {
    super(injector);
    this.view = this.route.snapshot.data['mode'] === 'view';
    this.projectInfo = this.projectImplementService.getProjectInfo();
    this.isSubmit = false;
    this.isLoadingPage = false;
    this.loadingSearch = false;
    this.request = null;
    if (!this.projectInfo) {
      this.projectInfo = {};
      this.router.navigateByUrl(functionUri.project_implement_list);
      return;
    }
    this.form = new FormGroup({
      jobs: new FormArray([]),
      emps: new FormArray([]),
      fromDate: new FormArray([]),
      toDate: new FormArray([]),
      scale: new FormArray([]),
    });
  }

  ngOnInit(): void {
    this.fetchData();
    if (!this.projectInfo?.empNum && !this.view) {
      this.addRow();
    }
  }

  onInput(event:string): void {
    if (this.request) {
      this.request.unsubscribe();
    }
    this.loadingSearch = true;
    this.request = this.projectImplementService.getEmployee({ keyWord: event }).subscribe(
      (data) => {
        this.options = data.data;
        this.request = null;
        this.loadingSearch = false;
        this.options.forEach((item) => {
          if (item.nick_name) {
            item.nick_name = item.nick_name.split('@')[0];
          } else {
            const names: string[] = toLowerCaseNonAccentVietnamese(item.full_name).split(' ');
            const lastName = names.pop();
            item.nick_name = lastName + names.map((element) => element[0]).join('');
          }
        });
      },
      (error) => {
        this.request = null;
        this.loadingSearch = false;
      }
    );
  }

  get jobsFormArray() {
    return this.form?.controls['jobs'] as FormArray;
  }
  get empsFormArray() {
    return this.form?.controls['emps'] as FormArray;
  }
  get fromDateFormArray() {
    return this.form?.controls['fromDate'] as FormArray;
  }
  get toDateFormArray() {
    return this.form?.controls['toDate'] as FormArray;
  }
  get scaleFormArray() {
    return this.form?.controls['scale'] as FormArray;
  }

  addRow(projectDetail?: ProjectDetail) {
    this.isSubmit = false;
    const jobIdControl = new FormControl(projectDetail ? projectDetail.jobId : '', Validators.required);
    const empIdControl = new FormControl(projectDetail ? projectDetail.employeeId : '', Validators.required);
    const fromDateControl = new FormControl(projectDetail ? projectDetail.formDate : '', Validators.required);
    const toDateControl = new FormControl(projectDetail ? projectDetail.toDate : '', Validators.required);
    const scale = new FormControl(projectDetail ? projectDetail.scale + '%' : '', Validators.required);
    let orgName = '';

    this.jobsFormArray.push(jobIdControl);
    this.empsFormArray.push(empIdControl);
    this.fromDateFormArray.push(fromDateControl);
    this.toDateFormArray.push(toDateControl);
    this.scaleFormArray.push(scale);

    if (projectDetail) {
      orgName =
        projectDetail.lateName.length > 50 ? projectDetail.lateName.substring(0, 50) + '...' : projectDetail.lateName;
    }

    const jobTemp = this.jobs.find((job) => job.jobId === projectDetail?.jobId);
    const item: ProjectEmployeeTable = {
      pjmId: projectDetail ? projectDetail.pjmId : null,
      prjId: projectDetail ? this.projectInfo?.prjId : null,
      jobId: jobIdControl,
      fromDate: fromDateControl,
      toDate: toDateControl,
      empId: empIdControl,
      isDeleted: false,
      scale: scale,
      pm: false,
      empName: projectDetail ? projectDetail.fullName : '',
      orgName: orgName,
      orgNameTooltip: projectDetail ? projectDetail.lateName : '',
      isSelectEmp: false,
      jobName: jobTemp ? jobTemp.jobName : '',
    };

    this.dataTable.push(item);

    if (projectDetail && projectDetail.isMaster === 'Y') {
      this.pm = item;
    }
  }

  deleteRow(index: number) {
    if (this.dataTable.length === 1) {
      return;
    }

    if (this.dataTable[index].prjId) {
      const item = this.dataTable[index];
      item.isDeleted = true;
      this.dataDelete.push(this.createItemToRequest(item));
    }
    this.jobsFormArray.removeAt(index);
    this.empsFormArray.removeAt(index);
    this.fromDateFormArray.removeAt(index);
    this.toDateFormArray.removeAt(index);
    this.scaleFormArray.removeAt(index);
    this.dataTable.splice(index, 1);
  }

  fetchData() {
    this.isLoadingPage = true;
    const request = [];
    request.push(this.projectImplementService.getJobProject());

    if (this.projectInfo?.empNum) {
      if(this.projectInfo.prjId){
        request.push(this.projectImplementService.getProjectDetail(this.projectInfo.prjId));
      }
    }
    forkJoin(request).subscribe(
      (data: any[]) => {
        this.isLoadingPage = false;
        this.jobs = data[0].data;
        if (this.projectInfo?.empNum) {
          this.createTableByDetail(data[1].data);
        }
      },
      (error) => {
        this.isLoadingPage = false;
        this.toastrCustom.error(error.error.message);
      }
    );
  }

  onChange(emp: Employee, data: ProjectEmployeeTable): void {
    data.empId.setValue(emp.employee_id);
    data.empName = emp.full_name;
    data.orgName = emp.late_name.length > 50 ? emp.late_name.substring(0, 50) + '...' : emp.late_name;
    data.orgNameTooltip = emp.late_name;
    this.changePm(data);
  }

  createTableByDetail(projectDetail:ProjectDetail[]) {
    projectDetail.forEach((item) => {
      this.addRow(item);
    });

    if (this.dataTable.length < 1) {
      this.addRow();
    }
  }

  validateTable() {
    this.isSubmit = true;
    if (this.form?.invalid) {
      this.toastrCustom.error(this.translate.instant('modelOrganization.message.variableRequired'));
      return;
    }
    if (!this.pm) {
      this.toastrCustom.error(this.translate.instant('modelOrganization.message.pmRequired'));
      return;
    }
    this.buildObjectRequest();
  }

  buildObjectRequest() {
    const objectRequest: ProjectAssignRequest[] = [];
    this.dataTable.forEach((item) => {
      objectRequest.push(this.createItemToRequest(item));
    });

    objectRequest.push(...this.dataDelete);

    this.mapEmpError.clear();
    this.isLoadingPage = true;
    this.projectImplementService.createProjectMember(objectRequest).subscribe(
      (data: ResponseEntity<EmployeeError[]>) => {
        this.isLoadingPage = false;
        const dataError = data.data;
        if (dataError.length) {
          this.toastrCustom.error(this.translate.instant('modelOrganization.message.scaleScreen'));
          this.setErrorEmp(dataError);
        } else {
          this.toastrCustom.success();
          this.router.navigateByUrl(functionUri.project_implement_list);
        }
      },
      (error) => {
        this.toastrCustom.error(error.error.message);
        this.isLoadingPage = false;
      }
    );
  }

  setErrorEmp(list: EmployeeError[]) {
    list.forEach((item) => {
      this.mapEmpError.set(`${item.empId}-${item.jobId}`, item);
    });
  }

  createItemToRequest(item: ProjectEmployeeTable): ProjectAssignRequest {
    const scale = item.scale.value.substring(0, item.scale.value.length - 1);
    const itemCreate: ProjectAssignRequest = {
      pjmId: item.pjmId,
      prjId: this.projectInfo?.prjId,
      empId: item.empId.value,
      fromDate: moment(item.fromDate.value).format('YYYY-MM-DD'),
      toDate: moment(item.toDate.value).format('YYYY-MM-DD'),
      isMaster: item === this.pm ? 'Y' : 'N',
      scale: scale,
      isDeleted: item.isDeleted,
      jobId: item.jobId.value,
    };
    return itemCreate;
  }

  onKeypress(event:KeyboardEvent) {
    const pattern = REGEX_NUMBER;
    if (!pattern.test(event.key)) {
      event.preventDefault();
      return;
    }

    const valueText: string = (event.target as HTMLTextAreaElement).value;

    const value = parseInt(valueText + event.key);

    if (value > 100) {
      event.preventDefault();
      return;
    }

    if (value === 0 && (event.target as HTMLTextAreaElement).value.length === 1) {
      event.preventDefault();
    }
  }

  onFocus(data: ProjectEmployeeTable) {
    if (this.mapEmpError.has(data.empId.value)) {
      this.mapEmpError.delete(data.empId.value);
    }
    this.isSubmit = false;
    let value = data.scale.value;
    if (value.indexOf('%') > -1) {
      value = value.substring(0, value.length - 1);
      data.scale.setValue(value);
    }
  }

  onBlur(data: ProjectEmployeeTable) {
    const value = data.scale.value;
    if (value.length) {
      data.scale.setValue(parseInt(value) + '%');
    } else {
      setTimeout(() => {
        data.scale.setValue('');
      }, 500);
    }
  }

  onChangeDate(event:string, value: ProjectEmployeeTable, data: FormControl) {
    let fromDate = 0;
    let toDate = 0;
    let error= '';

    if (!event) {
      return;
    }

    if (value.fromDate.value) {
      fromDate = new Date(value.fromDate.value).setHours(0, 0, 0, 0);
      error = this.checkFromDate(fromDate);
    }

    if (value.toDate.value && !error) {
      toDate = new Date(value.toDate.value).setHours(0, 0, 0, 0);
      error = this.checkToDate(toDate);
    }

    if (fromDate && toDate && !error) {
      if (fromDate > toDate) {
        error = this.translate.instant('modelOrganization.message.dateEnd');
      }
    }

    if (error) {
      this.toastrCustom.error(error);
      setTimeout(() => {
        data.setValue('');
      }, 500);
    } else {
      if (!this.changePm(value)) {
        return;
      }
    }
  }

  checkFromDate(fromDate:number) {
    let error = null;
    if (this.projectInfo?.fromDate && fromDate < this.projectInfo.fromDate) {
      error = this.translate.instant('modelOrganization.message.startDateLessThanProject');
    }

    if (this.projectInfo?.toDate && fromDate > this.projectInfo.toDate) {
      error = this.translate.instant('modelOrganization.message.startDateMoreThanProject');
    }
    return error;
  }

  checkToDate(toDate:number) {
    let error = null;
    if (this.projectInfo?.toDate && toDate > this.projectInfo.toDate) {
      error = this.translate.instant('modelOrganization.message.endDateLessThanProject');
    }

    if (this.projectInfo?.fromDate && toDate < this.projectInfo.fromDate) {
      error = this.translate.instant('modelOrganization.message.endDateMoreThanProject');
    }
    return error;
  }

  trackByFn(index:number, item: ProjectEmployeeTable) {
    return item.pjmId;
  }

  changePm(emp: ProjectEmployeeTable): boolean {
    if (!emp.fromDate.value || !emp.toDate.value) {
      return true;
    }

    let err = false;

    for (const item of this.dataTable) {
      if (emp !== item && item.empId.value === emp.empId.value && item !== emp && item.toDate.value && item.fromDate) {
        const numItemFromDate = new Date(item.fromDate.value).setHours(0, 0, 0, 0);
        const numItemToDate = new Date(item.toDate.value).setHours(0, 0, 0, 0);
        const numMasterFromDate = new Date(emp.fromDate.value).setHours(0, 0, 0, 0);
        const numMasterToDate = new Date(emp.toDate.value).setHours(0, 0, 0, 0);
        const valid =
          (numItemFromDate < numMasterFromDate && numItemToDate < numMasterFromDate) ||
          (numMasterFromDate < numItemFromDate && numMasterToDate < numItemFromDate);

        if (!valid) {
          err = true;
          setTimeout(() => {
            emp.empId.setValue('');
            emp.empName = '';
          }, 500);
          break;
        }
      }
    }

    if (err) {
      this.toastrCustom.error(this.translate.instant('modelOrganization.message.dulicateJobPm'));
      return false;
    }

    return true;
  }
}
