import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmStaffComponent } from './confirm-staff.component';

describe('ConfirmStaffComponent', () => {
  let component: ConfirmStaffComponent;
  let fixture: ComponentFixture<ConfirmStaffComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfirmStaffComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmStaffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
