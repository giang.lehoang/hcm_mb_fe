import { Component, Injector, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import {
  FillModel,
  InitStaffSearch,
  Job, MB_BANK,
  PaginationPlan, REGEX_NUMBER,
  StaffData
} from '@hcm-mfe/model-organization/data-access/models';
import {
  ApproveStaffService,
  ConfirmStaffService,
  UploadFileService
} from '@hcm-mfe/model-organization/data-access/services';
import { CustomToastrService } from '@hcm-mfe/shared/core';
import { DataService } from '@hcm-mfe/shared/data-access/services';
import { FunctionCode } from '@hcm-mfe/shared/common/enums';
import { FormModalShowTreeUnitComponent } from '@hcm-mfe/shared/ui/form-modal-show-tree-unit';
import { arrayBufferToBase64 } from '@hcm-mfe/shared/common/utils';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-confirm-staff',
  templateUrl: './confirm-staff.component.html',
  styleUrls: ['./confirm-staff.component.scss'],
  providers: [UploadFileService]
})
export class ConfirmStaffComponent extends BaseComponent implements OnInit {
  orgName = '';
  fileName = '';
  file: any;
  objectSearch: any;
  checked = false;
  indeterminate = false;
  dataTable: StaffData[] = [];
  quantityMap: Map<number, StaffData> = new Map<number, StaffData>();
  params: InitStaffSearch;
  pagination: PaginationPlan;
  isLoadingPage = false;
  listJob: Job[] = [];
  messageSuccess = '';
  isFillQuick = false;
  objFill: FillModel = {
    consultation: '',
  };
  isVisible = false;
  pdfSrc = '';
  imagePath: any;
  isPdfFile = false;

  constructor(
    injector: Injector,
    public uploadFile: UploadFileService,
    public toast: CustomToastrService,
    public dataService: DataService,
    public confirmStaffService: ConfirmStaffService,
    private readonly approvedService: ApproveStaffService,
    readonly _sanitizer: DomSanitizer,
  ) {
    super(injector);
    const curentYear = String(new Date().getFullYear());
    this.orgName = MB_BANK;
    this.objectSearch = {
      jobId: '',
      periodCode: curentYear,
      orgId: '2',
    };

    this.params = {
      ...this.objectSearch,
    };

    this.pagination = {
      currentPage: 1,
      totalElement: 0,
      size: 15,
      totalPage: 0,
      numberOfElements: 0,
    };
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CONFIRM_STAFF}`);
  }

  ngOnInit(): void {
    this.isLoadingPage = false;
    this.fetchData(true);
    this.messageSuccess = this.translate.instant('modelOrganization.message.tranferSuccess');
  }

  fetchData(init: boolean) {
    this.isLoadingPage = true;
    const request = [];
    if (init) {
      request.push(this.dataService.getJobsByType());
    }

    request.push(this.confirmStaffService.getListConfirm(this.params));

    forkJoin(request).subscribe(
      (data: any) => {
        let plans;
        if (init) {
          this.listJob = data[0].data.content;
          plans = data[1].data;
        } else {
          plans = data[0].data;
        }
        this.dataTable = plans;
        this.dataTable.length > 0 && this.dataTable.forEach((item: StaffData) => {
          item.latchNumber = this.latchNumber(item);
          item.adjustedNumberTmp = item.adjustedNumber;
        });
        this.file = null;
        this.fileName = '';
        this.checked = false;
        if (!this.dataTable.length) {
          this.checked = false;
        }
        this.isLoadingPage = false;
        this.checkValidation(false);
      },
      (error) => {
        this.isLoadingPage = false;
        this.dataTable = [];
        this.listJob = [];
        this.toast.error(error.message);
      }
    );
  }

  updateCheckedSet(item: StaffData, checked: boolean): void {
    if (checked) {
      this.quantityMap.set(item.hcpId, item);
    } else {
      this.quantityMap.delete(item.hcpId);
    }
  }

  onCurrentPageDataChange(listOfCurrentPageData: StaffData[]): void {
    this.dataTable = listOfCurrentPageData;
    this.refreshCheckedStatus();
  }

  refreshCheckedStatus(): void {
    this.checked = this.dataTable.every(({ hcpId }) => this.quantityMap.has(hcpId));
  }

  onItemChecked(item: StaffData, checked: boolean): void {
    this.updateCheckedSet(item, checked);
    this.refreshCheckedStatus();
  }

  onAllChecked(checked: boolean): void {
    this.dataTable.forEach((item) => this.updateCheckedSet(item, checked));
    this.refreshCheckedStatus();
  }

  changePage(value: any) {
    this.params.page = value - 1;
    this.fetchData(false);
  }

  onSearch() {
    this.params.jobId = this.objectSearch.jobId ? this.objectSearch.jobId : '';
    this.params.orgId = this.objectSearch.orgId ? this.objectSearch.orgId : '';
    this.params.periodCode = this.objectSearch.periodCode ? this.objectSearch.periodCode : '';
    this.params.page = 0;
    this.fetchData(false);
    this.quantityMap.clear();
    this.clearFill();
  }

  showPopup() {
    const width = window.innerWidth / 1.5 > 1100 ? 1368 : window.innerWidth / 1.5;
    const modal = this.modal.create({
      nzWidth:
        window.innerWidth > 767 ? width : window.innerWidth,
      nzTitle: ' ',
      nzComponentParams: {
        isAuthor: true
      },
      nzContent: FormModalShowTreeUnitComponent,
      nzFooter: null,
    });

    modal.afterClose.subscribe((result) => {
      if (result?.orgName) {
        this.orgName = result?.orgName;
        this.objectSearch.orgId = result?.orgId;
      }
    });
  }

  processStaff(reject: any) {
    const headCountPlanDTOS: any[] = [];
    this.quantityMap.forEach((value) => {
      headCountPlanDTOS.push({
        hcpId: value.hcpId,
        consultation: value.consultation,
        adjustNumber: this.checkAdjustNumber(value)
      });
    });
    const requestDTO = {
      headCountPlanDTOS: headCountPlanDTOS,
      description: this.objFill.reason ? this.objFill.reason.trim() : '',
      action: reject ? 'TC' : 'XN',
    };
    const formData: FormData = new FormData();
    formData.append(
      'requestDTO',
      new Blob([JSON.stringify(requestDTO)], {
        type: 'application/json',
      })
    );
    formData.append('file', this.file);
    if (!this.quantityMap.size) {
      this.dataTable.forEach((item) => (item.isChecked = false));
      this.toast.error(this.translate.instant('modelOrganization.planYear.chooseMin'));
      return;
    }
    if (!this.checkValidation(true)) {
      return;
    }
    this.isLoadingPage = true;
    this.confirmStaffService.confirmStaff(formData).subscribe((res) => {
      this.isLoadingPage = false;
      this.quantityMap.clear();
      if (reject) {
        this.toast.success(this.translate.instant('modelOrganization.confirmStaff.rejectSucess'));
      } else {
        this.toast.success(this.translate.instant('modelOrganization.confirmStaff.successMessage'));
      }
      this.objFill.reason = "";
      this.fetchData(true);
    }, (error) => {
      this.toast.error(error.message);
      this.isLoadingPage = false;
    });
  }

  clearOrgInput() {
    this.orgName = '';
    this.objectSearch.orgId = '';
  }

  onClick(event: any) {
    if (+event.target.value == 0) {
      event.target.select();
    }
  }

  changeFill(e: any) {
    this.isFillQuick = e;
  }

  assignInfo() {
    this.objFill.consultation = this.objFill.consultation ? this.objFill.consultation.trim() : '';
    if (this.quantityMap.size <= 0) {
      this.toast.error(this.translate.instant('modelOrganization.planYear.chooseMin'));
      return;
    }
    const arrHpcId: number[] = [];
    this.quantityMap.forEach((value, key) => {
      value.consultation = this.objFill.consultation ? this.objFill.consultation : '';
      arrHpcId.push(key);
    });
    this.dataTable.forEach((data) => {
      if (arrHpcId.indexOf(data.hcpId) >= 0) {
        data.consultation = this.objFill.consultation ? this.objFill.consultation : '';
      }
    });
  }

  checkValidation(isTransfer: any) {
    const arrHpcId: number[] = [];
    this.quantityMap.forEach((value, key) => {
      arrHpcId.push(key);
    });
    this.dataTable.forEach((data) => {
      if (arrHpcId.indexOf(data.hcpId) >= 0) {
        data.isChecked = true;
      }
    });
    const arrError = this.dataTable.filter((item) => !item.consultation && item.isChecked);
    if (isTransfer && arrError.length > 0) {
      this.toast.error(this.translate.instant('modelOrganization.confirmStaff.errorMessage'));
      return false;
    } else {
      return true;
    }
  }

  clearFill() {
    this.objFill.consultation = '';
  }

  latchNumber(data: any) {
    if (data.inputType === 'KT') {
      return 0;
    } else {
      return Number(data.headCount) + Number(data.adjustedNumber);
    }
  }

  validateFileSize(file: any): boolean {
    const fileSize = file?.size;
    const fileSizeInMB = Math.round(fileSize / (1024 * 1024));
    if (fileSizeInMB > 3) {
      this.toast.error(this.translate.instant('modelOrganization.confirmStaff.error.exceedFile'));
      return true
    } else {
      return false;
    }
  }

  validateFileType(file: any): boolean {
    let fileType;
    if (file?.name.includes('.msg')) {
      fileType = '.msg'
    } else {
      fileType = file?.type;
    }
    const fileTypeRequired = ['image/jpeg', 'image/jpg', 'image/png', 'application/pdf', '.msg']
    if (!fileTypeRequired.includes(fileType)) {
      this.toast.error(this.translate.instant('modelOrganization.confirmStaff.error.invalidType'));
      return true;
    } else {
      return false;
    }
  }

  handleFileInput(event: any) {
    if (this.validateFileType(event.target.files[0]) || this.validateFileSize(event.target.files[0])) {
      return;
    }
    this.file = event.target.files[0];
    this.fileName = this.file.name;
  }

  override triggerSearchEvent(): void {
    this.onSearch();
  }

  clearFileName() {
    this.fileName = '';
    this.file = null;
  }

  downloadFile(item: any) {
    let fileCorrection: any;
    this.approvedService.downloadFile(item.adjustDocId).subscribe((data: any) => {
      if (item.adjustDocName.includes('.pdf')) {
        this.isPdfFile = true;
        const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.pdf' });
        fileCorrection = new File([blob], String(item.adjustDocName));
        this.onFileSelected(fileCorrection);
      }
      if (item.adjustDocName.includes('.png') || item.adjustDocName.includes('.jpg') || item.adjustDocName.includes('.jpeg')) {
        this.isPdfFile = false;
        this.imagePath = this._sanitizer.bypassSecurityTrustResourceUrl(
          'data:image/jpg;base64,' + arrayBufferToBase64(data));
        this.isVisible = true;
      }
    });
  }

  onFileSelected(fileCorrection: any) {
    const reader = new FileReader();
    reader.onload = (e: any) => {
      this.pdfSrc = e.target.result;
    };
    reader.readAsArrayBuffer(fileCorrection);
    this.isVisible = true;
  }

  handleCancel(): void {
    this.isVisible = false;
  }

  checkAdjustNumber(data: StaffData) {
    let adjustedNumber = 0;
    const checkValue = Number(data.adjustedNumberTmp) + Number(data.headCount);
    adjustedNumber = Number(data.latchNumber) - checkValue;
    return adjustedNumber;
  }

  onKeypress(event: any) {
    const pattern = REGEX_NUMBER;
    if (!pattern.test(event.key)) {
      event.preventDefault();
      return;
    }
  }
}
