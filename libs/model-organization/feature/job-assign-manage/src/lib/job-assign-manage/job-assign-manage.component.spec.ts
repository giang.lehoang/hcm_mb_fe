import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobAssignManageComponent } from './job-assign-manage.component';

describe('JobAssignManageComponent', () => {
  let component: JobAssignManageComponent;
  let fixture: ComponentFixture<JobAssignManageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobAssignManageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobAssignManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
