import { Component, Injector, OnInit } from '@angular/core';
import * as FileSaver from 'file-saver';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {
  JobAssignManage, JobAssignRes,
  JobView,
  OptionSelect,
  Pagination,
  SearchJob
} from "@hcm-mfe/model-organization/data-access/models";
import {functionUri, userConfig} from "@hcm-mfe/shared/common/constants";
import { FunctionCode } from '@hcm-mfe/shared/common/enums';
import {JobService} from "@hcm-mfe/model-organization/data-access/services";
import {SearchService} from "@hcm-mfe/shared/core";
import {renderFlagStatus} from "../../../../../helper/common.function";
@Component({
  selector: 'app-job-assign-manage',
  templateUrl: './job-assign-manage.component.html',
  styleUrls: ['./job-assign-manage.component.scss'],
})
export class JobAssignManageComponent extends BaseComponent implements OnInit {
  jobDataTable: JobAssignRes[] = [];
  pagination: Pagination = new Pagination(userConfig.pageSize);
  isLoadingPage: boolean;
  objSearch: SearchJob;
  params: SearchJob;
  jobStatus = {
    waiting: {
      name: 'Chờ duyệt',
      style: 'orange',
    },
    approved: {
      name: 'Phê duyệt',
      style: 'green',
    },
    rejected: {
      name: 'Từ chối',
      style: 'red',
    },
  };
  renderFlagStatus = renderFlagStatus;
  jobType: string;
  listJob:JobView[] = [];
  listJobSearch:JobView[] = [];
  modelJobs: OptionSelect[];
  status: OptionSelect[];

  constructor(injector: Injector, readonly jobService: JobService,
    readonly searchService: SearchService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.DEPLOY_JOB}`);
    if (!this.objFunction || !this.objFunction.view) {
      this.router.navigateByUrl(functionUri.access_denied)
    }
    this.modelJobs = [
      { value: 'ORG', label: 'Mô hình cứng' },
      { value: 'DV', label: 'Mô hình dịch vụ' },
    ];
    this.status = [
      { value: '2', label: this.translate.instant('modelOrganization.assignJob.pending') },
      { value: '1', label: this.translate.instant('modelOrganization.assignJob.approve') },
      { value: '3', label: this.translate.instant('modelOrganization.assignJob.reject') }
    ]
    this.objSearch = {
      jobName: '',
      jobType: '',
      flagStatus: '',
    };

    this.params = {
      page: 0,
      size: 15,
      flagStatus: '',
      jobName: '',
      jobType: ''
    };
    this.jobType = '';
    this.isLoadingPage = false;
    this.pagination.currentPage = 1;
  }

  ngOnInit(): void {
    this.callGetJob();
    this.getAllJob();
  }

  onSearch() {
    this.params = {
      page: 0,
      size: 15,
      jobName: this.objSearch.jobName ? this.objSearch.jobName : '',
      jobType: this.objSearch.jobType ? this.objSearch.jobType : '',
      flagStatus: this.objSearch.flagStatus ? this.objSearch.flagStatus : '',
    };

    this.callGetJob();
  }

  getAllJob() {
    this.objSearch.jobType = this.objSearch.jobType ? this.objSearch.jobType : ''
    this.jobService.getListJob(this.objSearch.jobType).subscribe(
      (data) => {
        this.listJob = data.data;
      },
      (error) => {
        this.toastrCustom.error(error.message);
      }
    );
  }

  exportExcel() {
    this.jobService.exportAssignExcel(this.objSearch).subscribe(
      (data: any) => {
        const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
        FileSaver.saveAs(blob, 'Danh sach trien khai job.xlsx');
      },
      (error) => {
        this.toastrCustom.error(error.error.message);
      }
    );
  }

  callGetJob() {
    this.isLoadingPage = true;
    this.jobService.getJobPosition(this.params).subscribe(
      (data) => {
        this.isLoadingPage = false;
        this.jobDataTable = data.data.content;
        this.pagination.totalElements = data.data.totalElements;
        this.pagination.totalPages = data.data.totalPages;
        this.pagination.numberOfElements = data.data.numberOfElements;
      },
      (error) => {
        this.toastrCustom.error(error.error.message);
        this.isLoadingPage = false;
        this.jobDataTable = [];
      }
    );
  }

  changePage() {
    this.params.page = this.pagination.currentPage - 1;
    this.callGetJob();
  }


  navigatePage() {
    this.router.navigateByUrl("/organization/job/attach-job")
  }

  showConfirm(id:number|string): void {
    this.deletePopup.showModal(() => this.deleteJob(id))
  }

  deleteJob(id:number|string) {
    this.jobService.deleteJobPos(id).subscribe(
      (data) => {
        this.toastrCustom.success(this.translate.instant('modelOrganization.assignJob.notification.deleteSuccess'));
        this.callGetJob();
      },
      (error) => {
        this.toastrCustom.error(this.translate.instant('modelOrganization.assignJob.notification.deleteFail'));
      }
    );
  }
  onSearchJob(e:string) {
    this.listJobSearch = [];
    const value = e;
    if (value) {
      if (!this.objSearch.jobType) {
        this.listJob.forEach(v => {
          if (this.searchService.replaceUnicode(v.jobName.toLowerCase()).includes(this.searchService.replaceUnicode(value.toLowerCase()))) {
            this.listJobSearch.push(v);
          }
        });
      } else {
        this.listJob.filter(job => job.jobType === this.objSearch.jobType).forEach(item => {
          if (this.searchService.replaceUnicode(item.jobName.toLowerCase()).includes(this.searchService.replaceUnicode(value.toLowerCase()))) {
            this.listJobSearch.push(item);
          }
        });
      }
    } else {
      this.listJobSearch = this.listJob;
    }
  }
  onClickJob(value:string) {
    this.objSearch.jobName = value;
  }

  override triggerSearchEvent(): void {
    this.onSearch();
  }
}
