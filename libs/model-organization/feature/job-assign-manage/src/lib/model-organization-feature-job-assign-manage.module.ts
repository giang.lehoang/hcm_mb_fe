import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {JobAssignManageComponent} from "./job-assign-manage/job-assign-manage.component";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {ModelOrganizationPipesTransformJobTypeModule} from "@hcm-mfe/model-organization/pipes/transform-job-type";
import {NzTagModule} from "ng-zorro-antd/tag";
import {NzTableModule} from "ng-zorro-antd/table";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {FormsModule} from "@angular/forms";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {RouterModule} from "@angular/router";
import {NzGridModule} from "ng-zorro-antd/grid";

@NgModule({
  imports: [CommonModule, NzDropDownModule, NzPaginationModule, TranslateModule, SharedUiMbButtonModule,RouterModule.forChild([
    {
      path: '',
      component: JobAssignManageComponent,
    }]),
    ModelOrganizationPipesTransformJobTypeModule, NzTagModule, NzTableModule, NzGridModule,
    SharedUiMbSelectModule, FormsModule, SharedUiLoadingModule],
  declarations: [JobAssignManageComponent],
  exports: [JobAssignManageComponent]
})
export class ModelOrganizationFeatureJobAssignManageModule {}
