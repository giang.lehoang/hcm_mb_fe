import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TransferIssuedComponent} from "./transfer-issued/transfer-issued.component";
import {RouterModule} from "@angular/router";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {NzGridModule} from "ng-zorro-antd/grid";
import {TranslateModule} from "@ngx-translate/core";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";
import {NzFormModule} from "ng-zorro-antd/form";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {NzListModule} from "ng-zorro-antd/list";
import {NzDividerModule} from "ng-zorro-antd/divider";
import {ModelOrganizationUiSearchInputModule} from "@hcm-mfe/model-organization/ui/search-input";
import {SelectEmployeeComponent} from "./select-employee/select-employee.component";
import {NzAvatarModule} from "ng-zorro-antd/avatar";
import {SharedUiEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/employee-data-picker";
import {NzIconModule} from "ng-zorro-antd/icon";

@NgModule({
  imports: [CommonModule, NzDividerModule, NzListModule, ModelOrganizationUiSearchInputModule, NzAvatarModule,
    SharedUiLoadingModule, NzFormModule, NzIconModule,
    ReactiveFormsModule, FormsModule, NzCheckboxModule, NzTableModule, SharedUiMbSelectModule,
    SharedUiMbButtonModule, NzGridModule, TranslateModule, SharedUiMbInputTextModule, RouterModule.forChild([
      {
        path: '',
        component: TransferIssuedComponent,
      }]), SharedUiEmployeeDataPickerModule,],
  declarations: [TransferIssuedComponent, SelectEmployeeComponent],
  exports: [TransferIssuedComponent,SelectEmployeeComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class ModelOrganizationFeatureTransferIssuedModule {
}
