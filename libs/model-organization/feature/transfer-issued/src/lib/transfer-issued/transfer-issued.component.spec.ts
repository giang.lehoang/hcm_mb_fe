import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransferIssuedComponent } from './transfer-issued.component';

describe('TransferIssuedComponent', () => {
  let component: TransferIssuedComponent;
  let fixture: ComponentFixture<TransferIssuedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransferIssuedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransferIssuedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
