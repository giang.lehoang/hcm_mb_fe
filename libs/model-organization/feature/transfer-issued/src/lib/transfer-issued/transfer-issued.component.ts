import {Component, Injector, OnInit} from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {
  Group,
  IssuedRequest,
  Issues,
  IssuesDetail,
  Pagination,
  SearchJob,
  User
} from "@hcm-mfe/model-organization/data-access/models";
import {DATA_TYPE, STORAGE_NAME, userConfig} from "@hcm-mfe/shared/common/constants";
import {Subscription} from "rxjs";
import {IssuedService} from "../../../../../data-access/services/src/lib/issued-service";
import {EmployeeDetail} from "@hcm-mfe/shared/data-access/models";
import {JobService} from "../../../../../data-access/services/src/lib/job.service";
import {FunctionCode, functionUri} from "@hcm-mfe/shared/common/enums";
import {StorageService} from "@hcm-mfe/shared/common/store";
import { SelectEmployeeComponent } from '../select-employee/select-employee.component';

@Component({
  selector: 'app-transfer-issued',
  templateUrl: './transfer-issued.component.html',
  styleUrls: ['./transfer-issued.component.scss'],
})
export class TransferIssuedComponent extends BaseComponent implements OnInit {
  tranferForm: FormGroup;
  isVisible: boolean;
  listPos: SearchJob[] | undefined = [];
  setPostSelected: SearchJob[] = [];
  ds = [];
  inforIssueList = Array<Issues>();
  groupMembers: Group[] = [];
  itemSelected = new Set<number>();
  loading: boolean;
  showItems: string[] = [];
  params: SearchJob;
  pagination: Pagination;
  request: Subscription | undefined;
  isSubmit: boolean;

  constructor(
    injector: Injector,
    readonly issuedService: IssuedService,
    readonly jobService: JobService
  ) {
    super(injector);
    this.request = undefined;
    this.isSubmit = false;
    this.isVisible = false;
    this.loading = false;
    this.pagination = new Pagination(userConfig.pageSize);
    this.params = {
      page: 0,
      size: 15,
      jobCode: '',
      jobName: '',
      jobType: '',
      flagStatus: '1',
    };
    this.tranferForm = new FormGroup({
      title: new FormControl('', Validators.required),
      content: new FormControl('', Validators.required),
      date: new FormControl(new Date()),
    });
  }

  ngOnInit(): void {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ORGANIZATION_ISSUED_TRANFER}`);
    if (!this.objFunction || !this.objFunction.view) {
      this.router.navigateByUrl(functionUri.access_denied);
    }
    this.inforIssueList = this.issuedService.getListIdSelected();
    if (!this.inforIssueList || this.inforIssueList.length < 1) {
      this.router.navigateByUrl('/organization/issued/issued-list');
      this.toastrCustom.error('Vui lòng chọn bản ghi');
      return;
    }
    this.pagination = new Pagination(userConfig.pageSize);
    this.showItems = Array.from(this.inforIssueList).map((item) => item.dataName);
    this.isSubmit = false;
    console.log(this.inforIssueList, StorageService.get(STORAGE_NAME.USER_LOGIN));
    this.getGroup();
  }

  getGroup() {
    this.loading = true;
    this.issuedService
      .getGroupsAndMembersByUser(StorageService.get(STORAGE_NAME.USER_LOGIN).username)
      .subscribe((data: any) => {
        this.loading = false;
        this.groupMembers = data.data;
      }, error => {
        this.loading = false;
        this.toastrCustom.error(error.error.message)
      });
  }

  showPopup(group?: Group) {
    const member: EmployeeDetail[] = [];
    if (group) {
      group.members.forEach(item => {
        member.push({
          employeeId: item.gmeId,
          fullName: item.fullName,
          email: item.gmeMail,
          orgName: item.department,
          employeeCode: '',
          dateOfBirth: '',
          jobName: '',
          personalId: ''
        })
      });
    }

    const modalUpload = this.modal.create({
      nzTitle: '',
      nzStyle: {
        width: '80%',
      },
      nzComponentParams: {
        group: group,
        ds: member,
      },

      nzContent: SelectEmployeeComponent,
      nzBodyStyle: {
        padding: '56px 0px 0px 0px',
        border: 'none',
        background: '#f4f6fa',
      },
      nzFooter: null,
    });

    modalUpload.componentInstance?.addGroup?.subscribe((data: boolean) => {
      if (data) {
        this.getGroup();
      }
      modalUpload.destroy();
    });
  }

  handleCancel() {
    this.isVisible = false;
  }

  addGroup(value: string) {
    if (value) {
      this.issuedService
        .getGroupsAndMembersByUser(StorageService.get(STORAGE_NAME.USER_LOGIN).username)
        .subscribe((data: any) => {
          this.isVisible = false;
          this.groupMembers = data.data;
          if (data.data && data.data.length > 0) {
            let id = this.groupMembers[0].gdeId;
            for (let i = 0; i < data.data.length; i++) {
              if (this.groupMembers[i].gdeId > id) {
                id = this.groupMembers[i].gdeId;
              }
            }
          }
        });
    }
    this.isVisible = false;
  }

  removeGroup(item: Group) {
    this.deletePopup.showModal(() => {
      this.loading = true;
      this.issuedService.deleteGroupMember(item.gdeId).subscribe(data => {
        this.toastrCustom.success();
        this.loading = false;
        this.getGroup();
      }, error => {
        this.toastrCustom.error(error.error.message);
        this.loading = false;
      })
    })
  }

  removePos(item: SearchJob, index: number) {
    if (item.jobId) {
      this.itemSelected.delete(item.jobId);
      this.setPostSelected.splice(index, 1);
    }
  }

  checkValid(): boolean {
    let check = true;
    if (this.tranferForm.invalid) {
      check = false;
    }
    if (this.setOfCheckedId.size === 0) {
      this.toastrCustom.error('Bạn chưa chọn nhóm');
      check = false;
    }
    return check;
  }

  submit() {
    this.isSubmit = true;
    this.tranferForm.controls['title'].setValue(this.tranferForm.value.title.trim());
    this.tranferForm.controls['title'].setValue(this.tranferForm.value.title.trim());

    if (!this.checkValid()) {
      return;
    }

    const orgId = [];
    const prjId = [];
    const jdId = [];
    const jobOrg: number[] = [];
    const jobPrj: number[] = [];
    const user: User[] = [];
    let org: IssuesDetail | null = null;
    let prj: IssuesDetail | null = null;
    let jd:IssuesDetail | null = null;
    for (const item of this.inforIssueList) {
      if (item.dataType === DATA_TYPE.org) {
        orgId.push(item.dataId);
      } else if (item.dataType === DATA_TYPE.prj) {
        prjId.push(item.dataId);
      } else if(item.dataType === DATA_TYPE.jd){
        jdId.push(item.dataId);
      }
    }

    for (const item of this.setPostSelected) {
      if (item.jobType === DATA_TYPE.org) {
        if (item.jobId) {
          jobOrg.push(item.jobId);
        }
      } else if (item.jobType === DATA_TYPE.prj) {
        if (item.jobId) {
          jobPrj.push(item.jobId);
        }
      }
    }

    const emailEmp = new Set<string>();
    for (const item of this.setOfCheckedId) {
      const gr = this.groupMembers.find((group) => group.gdeId === item);
      gr?.members?.forEach((member) => {
        if (!emailEmp.has(member.gmeMail)) {
          user.push({
            fullName: member.fullName,
            email: member.gmeMail,
          });
          emailEmp.add(member.gmeMail);
        }
      });
    }

    if (orgId.length) {
      org = {
        jobIds: jobOrg,
        objIds: orgId
      }
    }

    if (prjId.length) {
      prj = {
        jobIds: jobPrj,
        objIds: prjId
      }

    }

    if(jdId.length){
      jd = {
        jobIds: jobOrg,
        objIds: jdId
      }
    }

    const requesObject: IssuedRequest = {
      title: this.tranferForm.value.title,
      content: this.tranferForm.value.content,
      users: user,
      org: org,
      prj: prj,
      jd: jd
    };

    this.loading = true;
    this.issuedService.tranfer(requesObject).subscribe(
      (data) => {
        this.loading = false;
        this.toastrCustom.success('Chuyển thành công');
        this.router.navigateByUrl('/organization/project/issued-project');
      },
      (error) => {
        this.loading = false;
        this.toastrCustom.error('Chuyển thất bại');
      }
    );
    console.log(requesObject)
  }

  goBackPage() {
    this.location.back();
  }

  checked = false;
  setOfCheckedId = new Set<number>();

  updateCheckedSet(id: number, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }

  onItemChecked(id: number, checked: boolean): void {
    this.updateCheckedSet(id, checked);
  }

  onAllChecked(value: boolean): void {
    this.groupMembers.forEach((item) => this.updateCheckedSet(item.gdeId, value));
  }

  onInput(event: string): void {
    if (this.request) {
      this.request.unsubscribe();
    }
    this.params.jobName = event;
    this.params.page = 0;
    this.callJob();
  }

  callJob(load?: boolean) {
    this.pagination.numCaculate = 0;
    this.request = this.jobService.filterJobs(this.params).subscribe(
      (data) => {
        if (data.data) {
          if (load && data.data?.content && this.listPos) {
            this.listPos = [...this.listPos, ...data.data.content];
          } else {
            this.listPos = data.data?.content;
          }
          if(data.data.totalElements){
            this.pagination.totalElements = data.data.totalElements;
          }
          if(data.data.totalPages){
            this.pagination.totalPages = data.data.totalPages;
          }
          if( data.data.numberOfElements){
            this.pagination.numberOfElements = data.data.numberOfElements
          }
          if(this.listPos && this.pagination.totalElements){
            this.pagination.numCaculate = this.pagination?.totalElements - this.listPos.length;
          }
          this.request = undefined;
        }
      },
      (error) => {
        this.message.error(error.error.message);
        this.request = undefined;
        this.listPos = [];
      }
    );
  }

  loadMoreEvent(value: boolean) {
    if (this.request) {
      return;
    }
    if (this.params?.page) {
      this.params.page = this.params.page + 1;
    }
    this.callJob(true);
  }
}
