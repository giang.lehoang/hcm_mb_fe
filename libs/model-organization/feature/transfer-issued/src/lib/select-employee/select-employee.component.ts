import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, EventEmitter, Injector, Input, OnDestroy, OnInit, Output } from '@angular/core';
// @ts-ignore
import * as FileSaver from 'file-saver';
import { NzModalRef } from 'ng-zorro-antd/modal';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {
  EmployeeGroupCreate,
  Group,
  ImportEmployee, RequestFileInfo,
  ResponseEntity
} from "@hcm-mfe/model-organization/data-access/models";
import {EmployeeDetail} from "@hcm-mfe/shared/data-access/models";
import {Subscription} from "rxjs";
import {FunctionCode, functionUri} from "@hcm-mfe/shared/common/enums";
import {IssuedService} from "../../../../../data-access/services/src/lib/issued-service";
import {UploadFileService} from "@hcm-mfe/model-organization/data-access/services";
import {IFileUploadInfo} from "@hcm-mfe/shared/ui/upload-file-organization";
import { MICRO_SERVICE } from '@hcm-mfe/shared/common/constants';

@Component({
  selector: 'app-select-employee',
  templateUrl: './select-employee.component.html',
  styleUrls: ['./select-employee.component.scss'],
  providers: [UploadFileService]
})
export class SelectEmployeeComponent extends BaseComponent implements OnInit, OnDestroy {
  formName: FormGroup | null;
  fileUploadInfo: IFileUploadInfo | null;
  error: string;
  select: number;
  @Input() ds: EmployeeDetail[] = [];
  tempData = [];
  url:string;
  showInput = false;
  @Output() addGroup = new EventEmitter<unknown>();
  confirmModal?: NzModalRef;
  request: Subscription | null;
  isVisible = false;
  mapEmployee: Map<number, EmployeeDetail> = new Map<number, EmployeeDetail>();
  employeeId: number;
  loading: boolean;
  isSubmitted: boolean;
  @Input() group: Group | null = null;

  constructor(injector: Injector, readonly issuedService: IssuedService, readonly uploadFile: UploadFileService) {
    super(injector);
    this.url = issuedService.getUrlUploadFile();
    this.request = null;
    this.isSubmitted = false;
    this.loading = false;
    this.employeeId = 0;
    this.formName = null;
    this.error = '';
    this.select = 0;
    this.fileUploadInfo = null;
  }

  ngOnInit(): void {
    this.select = 0;
    let groupName = '';
    if (this.group) {
      groupName = this.group.gdeName;
      this.ds.forEach((item) => {
        this.mapEmployee.set(item.employeeId, item);
      });
    }
    this.formName = new FormGroup({
      name: new FormControl(groupName, Validators.required),
    });
    this.isSubmitted = false;
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ORGANIZATION_ISSUED_TRANFER}`);
    if(!this.objFunction || !this.objFunction.view){
      this.router.navigateByUrl(functionUri.access_denied);
    }
  }

  dowloadTeamplate() {
    this.issuedService.dowloadExampleFile().subscribe(
      (data: any) => {
        const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
        FileSaver.saveAs(blob, 'Template.xlsx');
      },
      (error) => {
        this.toastrCustom.error(error?.error.description);
      }
    );
  }



  deleteEmp(index:number) {
    this.mapEmployee.delete(this.ds[index].employeeId);
    this.ds.splice(index, 1);
  }

  onSelect(value:any) {
    if (value.type) {
      if (!this.mapEmployee.has(value.value.employeeId)) {
        this.ds.push(value.value);
        this.mapEmployee.set(value.value.employeeId, value.value);
      }
    } else {
      if (this.mapEmployee.has(value.value.employeeId)) {
        const item:any = this.mapEmployee.get(value.value.employeeId);
        const index = this.ds.indexOf(item);
        if (index) {
          this.ds.splice(index, 1);
          this.mapEmployee.delete(value.value.employeeId);
        }
      }
    }
  }

  cancel() {
    this.addGroup.emit(false);
  }

  submit() {
    this.isSubmitted = true;
    if (this.formName?.invalid) {
      return;
    }

    if (!this.formName?.valid) {
      if (!this.showInput) {
        this.showInput = true;
      }
      return;
    }

    const gdeMembers = [];

    for (const item of this.ds) {
      gdeMembers.push({
        gmeUser: item.email?.split('@')[0],
        gmeMail: item.email,
      });
    }

    if (gdeMembers.length < 1) {
      this.toastrCustom.error('Vui lòng thêm nhân viên');
      return;
    }

    const object:EmployeeGroupCreate = {
      gdeId: this.group ? this.group.gdeId : null,
      gdeName: this.formName?.value.name,
      gdeMembers: gdeMembers,
    };

    this.callAPI(object);
  }
  showModal(): void {
    this.fileUploadInfo = null;
    this.tempData = [];
    this.isVisible = true;
  }

  handleCancel(): void {
    console.log('Button cancel clicked!');
    this.isVisible = false;
  }
  callAPI(object:EmployeeGroupCreate) {
    this.loading = true;
    if (this.group) {
      this.issuedService.updateGroupMember(object).subscribe(
        (data) => {
          this.loading = false;
          this.toastrCustom.success('Sửa nhóm thành công');
          this.addGroup.emit(true);
        },
        (error) => {
          this.loading = false;
          this.toastrCustom.error(error.error.message);
        }
      );
      return;
    }

    this.issuedService.createGroupMember(object).subscribe(
      (data) => {
        this.toastrCustom.success('Tạo nhóm thành công');
        this.addGroup.emit(true);
      },
      (error) => {
        this.toastrCustom.error(error.error.message);
      }
    );
  }

  onChange(event: EmployeeDetail) {
    if (!this.mapEmployee.has(event.employeeId)) {
      this.ds.push(event);
      this.mapEmployee.set(event.employeeId, event);
    }
  }

  showUpload() {
    const requestInfo:RequestFileInfo = {
      url: this.url,
      file: null,
      service:MICRO_SERVICE.MODEL_PLAN
    };
    this.uploadFile.showModal(
      () => { console.log("close") },
      requestInfo,
      this.translate.instant('modelOrganization.message.uploadSuccess'),
      true
    );

    this.uploadFile.eventClose?.subscribe((data: ResponseEntity<ImportEmployee>) => {
      const body = data.data;
      if(body){
        this.ds = body.employeeInfoDTOs;
        this.mapEmployee.clear();
        this.ds.forEach((item) => {
          item.orgName = item.department;
          this.mapEmployee.set(item.employeeId, item);
        });
      }
    });
  }

  override ngOnDestroy(): void {
    this.uploadFile.eventClose?.unsubscribe();
    this.mapEmployee.clear();
  }
}
