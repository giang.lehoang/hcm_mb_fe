import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UnitApproveDetailComponent } from './unit-approve-detail.component';

describe('UnitApproveDetailComponent', () => {
  let component: UnitApproveDetailComponent;
  let fixture: ComponentFixture<UnitApproveDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UnitApproveDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UnitApproveDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
