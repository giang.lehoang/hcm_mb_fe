import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ListSearchOrganization } from '@hcm-mfe/model-organization/data-access/models';

@Component({
  selector: 'app-unit-approve-detail',
  templateUrl: './unit-approve-detail.component.html',
  styleUrls: ['./unit-approve-detail.component.scss']
})
export class UnitApproveDetailComponent implements OnInit {
  listUnitApprove: Array<ListSearchOrganization> = [];
  id  = 0;
  tabsFind: Array<ListSearchOrganization> = [];
  orgIdSelect = 0;
  tabSelect =  0;
  possition: any;
  orgSelect: ListSearchOrganization|undefined;
  parentOrg: ListSearchOrganization|undefined;
  textInput = '';

  constructor(
    readonly route: ActivatedRoute,
    readonly location: Location
  ) {
  }

  ngOnInit(): void {
    this.getListTabs();
    this.possition = "bottomRight";
    this.orgIdSelect = 0;
    this.tabSelect = 0;
    this.textInput = "";
  }
  changeTab(event:any) {
    this.tabSelect = event;
    this.orgIdSelect = this.listUnitApprove[event].orgId;
    this.orgSelect = this.listUnitApprove[event];
  }
  onNextTab() {
    if (this.tabSelect < this.listUnitApprove.length - 1) {
      this.tabSelect = this.tabSelect + 1;
    }
    this.orgIdSelect = this.listUnitApprove[this.tabSelect].orgId;
    this.orgSelect = this.listUnitApprove[this.tabSelect];
  }
  onBackTab() {
    if (this.tabSelect > 0) {
      this.tabSelect = this.tabSelect - 1;
    }
    this.orgIdSelect = this.listUnitApprove[this.tabSelect].orgId;
    this.orgSelect = this.listUnitApprove[this.tabSelect];
  }

  handleClick() {
    this.textInput = "";
    this.tabsFind = this.listUnitApprove;
  }

  handleChangeText(event:any) {
    const listTmp:any = [];
    const value = event.target.value;
    if (event.target.value === "") {
      this.tabsFind = this.listUnitApprove;
    } else {
      this.listUnitApprove.forEach(item => {
        if (item.orgName.toLowerCase().includes(value.toLowerCase())) {
          listTmp.push(item);
        }
      });
      this.tabsFind = listTmp;
    }
  }

  handleChangeRadio(e:any) {
    this.tabSelect = this.listUnitApprove.map(value => value.orgId).indexOf(e);
    this.orgSelect = this.listUnitApprove[this.tabSelect];
  }

  getListTabs() {
    this.listUnitApprove = [];
    this.id = JSON.parse(localStorage.getItem('itemDetail')).orgId;
    this.parentOrg = JSON.parse(localStorage.getItem('itemDetail'));
    if (this.parentOrg) {
      this.listUnitApprove.push(this.parentOrg);
    }
    if (this.parentOrg?.subs && this.parentOrg.subs.length > 0) {
      this.parentOrg.subs.forEach(item => {
        this.listUnitApprove.push(item);
        if (item.subs && item.subs.length > 0) {
          item.subs.forEach(value => {
            this.listUnitApprove.push(value);
            if (value.subs && value.subs.length > 0) {
              value.subs.forEach(v => {
                this.listUnitApprove.push(v);
              })
            }
          })
        }
      });
    }
    if (this.id) {
      this.tabsFind = this.listUnitApprove;
      this.orgSelect = this.listUnitApprove[0];
      this.orgIdSelect = this.listUnitApprove[0].orgId;
    }
  }
  goBackPage() {
    this.location.back();
  }
}
