import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { TranslateModule } from '@ngx-translate/core';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { FormsModule } from '@angular/forms';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { RouterModule } from '@angular/router';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { ModelOrganizationFeatureRegionCategoryInitModule } from '@hcm-mfe/model-organization/feature/region-category-init';
import { UnitApproveDetailComponent } from './unit-approve-detail/unit-approve-detail.component';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { ModelOrganizationFeatureInitUnitModule } from '@hcm-mfe/model-organization/feature/init-unit';
import { NzInputModule } from 'ng-zorro-antd/input';
import { ModelOrganizationFeatureAdjusUnitModule } from '@hcm-mfe/model-organization/feature/adjus-unit';
import { ModelOrganizationFeatureCorrectUnitModule } from '@hcm-mfe/model-organization/feature/correct-unit';

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, SharedUiMbButtonModule, TranslateModule, NzCardModule,
    NzGridModule, SharedUiMbSelectModule, FormsModule, SharedUiMbInputTextModule, NzTableModule, NzPaginationModule,
    RouterModule.forChild([
      {
        path: '',
        component: UnitApproveDetailComponent
      }]), NzToolTipModule, NzModalModule, NzTagModule, ModelOrganizationFeatureRegionCategoryInitModule, NzDropDownModule, NzRadioModule,
    NzTabsModule, ModelOrganizationFeatureInitUnitModule, NzInputModule,
    ModelOrganizationFeatureAdjusUnitModule, ModelOrganizationFeatureCorrectUnitModule
  ],
  declarations: [UnitApproveDetailComponent],
  exports: [UnitApproveDetailComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ModelOrganizationFeatureUnitApprovedDetailModule {}
