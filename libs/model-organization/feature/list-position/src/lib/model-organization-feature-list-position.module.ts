import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ListPositionComponent} from "./list-position/list-position.component";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {FormsModule} from "@angular/forms";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
import {NzTagModule} from "ng-zorro-antd/tag";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import {RouterModule} from "@angular/router";
import {NzGridModule} from "ng-zorro-antd/grid";
import { NzIconModule } from 'ng-zorro-antd/icon';

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, TranslateModule, SharedUiMbButtonModule, RouterModule.forChild([
    {
      path: '',
      component: ListPositionComponent
    }]), NzGridModule,
    SharedUiMbInputTextModule, FormsModule, SharedUiMbSelectModule, NzTableModule, NzToolTipModule, NzTagModule, NzPaginationModule, NzIconModule],
  declarations: [ListPositionComponent],
  exports: [ListPositionComponent]
})
export class ModelOrganizationFeatureListPositionModule {}
