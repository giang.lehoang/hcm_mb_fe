import { Component, Injector, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';
import * as FileSaver from 'file-saver';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {
  Job,
  ListPositionModel,
  listStatusPosition,
  Pagination,  ResponseEntityData, SavePosRes,
  SearchPosModel, typePosition
} from "@hcm-mfe/model-organization/data-access/models";
import {userConfig} from "@hcm-mfe/shared/common/constants";
import { DataService, JobService} from "@hcm-mfe/model-organization/data-access/services";
import {FormModalShowTreeUnitComponent} from "@hcm-mfe/shared/ui/form-modal-show-tree-unit";
import {renderFlagStatus} from "../../../../../helper/common.function";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";

@Component({
  selector: 'app-list-position',
  templateUrl: './list-position.component.html',
  styleUrls: ['./list-position.component.scss']
})
export class ListPositionComponent extends BaseComponent implements OnInit {

  searchParam: SearchPosModel = new SearchPosModel();
  indeterminate = false;
  dataTable: ListPositionModel[] = [];
  params: SearchPosModel;
  pagination: Pagination = new Pagination(userConfig.pageSize) ;
  isLoadingPage: boolean;
  listJob: Job[] = [];
  listWidth = ['5%', '30%', '25%', '20%', '100px', '10%', '5%'];
  listStatus=listStatusPosition.filter(status => status.id !== 5 && status.id !== 7 && status.id !== 6);
  listTypePosition;
  orgName: string;
  isSearch: boolean;
  renderFlagStatus = renderFlagStatus;
  constructor(
    injector: Injector,
    readonly dataService: DataService,
    readonly jobService: JobService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(
      `FUNCTION_${FunctionCode.ORGANIZATION_POSITION_LIST}`
    );

    this.params = {
      ...this.searchParam,
    };
    this.isSearch = true;
    this.listTypePosition = typePosition;
    this.isLoadingPage = false;
    this.orgName = '';
  }

  ngOnInit(): void {
    this.fetchData(true);

  }

  fetchData(init: boolean) {
    this.isLoadingPage = true;
    const request = [];
    if (init) {
      request.push(this.dataService.getJobsByType());
    }

    request.push(this.jobService.getListPosition(this.params));

    forkJoin(request).subscribe(
      (data: Array<ResponseEntityData<ListPositionModel[]> | ResponseEntityData<Job[]>>) => {
        let positions;
        if (init) {
          this.listJob = (<ResponseEntityData<Job[]>>data[0]).data.content;
          positions = (<ResponseEntityData<ListPositionModel[]>>data[1]).data;
        } else {
          positions = (<ResponseEntityData<ListPositionModel[]>>data[0]).data;
        }
        this.dataTable = positions.content;
        this.pagination.totalElements = positions.totalElements;
        this.pagination.totalPages = positions.totalPages;
        this.pagination.numberOfElements = positions.numberOfElements;
        this.pagination.currentPage = positions.pageable.pageNumber;
        this.isLoadingPage = false;
      },
      (error) => {
        this.isLoadingPage = false;
        this.dataTable = [];
        this.listJob = [];
        this.toastrCustom.error(error.message);
      }
    );
  }
  onItemChecked(data: ListPositionModel, isChecked: boolean) {
    if (isChecked) {
      this.dataTable.filter(item => item.orgId === data.orgId).forEach(value =>  value.isHead = 'N' );
      data.isHead = 'Y';
      data.isDisable = false;
    } else {
      this.dataTable.filter(item => item.orgId === data.orgId).forEach(value =>  value.isHead = 'N' );
    }

  }

  onCurrentPageDataChange(listOfCurrentPageData: ListPositionModel[]): void {
    this.dataTable = listOfCurrentPageData;
  }

  changePage(value:number) {
    this.params.page = value - 1;
    this.fetchData(false);
  }

  onSearch() {
    this.isSearch = true;
    this.setParams();
    this.params.page = 0;
    this.fetchData(false);
  }

  showPopup() {
    const width = window.innerWidth / 1.5 > 1100 ? 1368 : window.innerWidth / 1.5;
    const modal = this.modal.create({
      nzWidth:
        window.innerWidth > 767 ? width : window.innerWidth,
      nzTitle: ' ',
      nzContent: FormModalShowTreeUnitComponent,
      nzFooter: null,
    });

    modal.afterClose.subscribe((result) => {
      if (result?.orgName) {
        this.orgName = result?.orgName;
        this.searchParam.orgId = result?.orgId;
      }
    });
  }

  exportExcel() {
    this.jobService.exportPosition(this.params).subscribe(
      (data) => {
        const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
        FileSaver.saveAs(blob, 'Template.xlsx');
      },
      (error) => {
        this.toastrCustom.error(error.error.message);
      }
    );
  }

  clearOrgInput() {
    this.orgName = '';
    this.searchParam.orgId = '';
  }

  override triggerSearchEvent(): void {
    this.onSearch();
  }

  setParams() {
    this.params.jobId = this.searchParam.jobId ? this.searchParam.jobId : '';
    this.params.orgId = this.searchParam.orgId ? this.searchParam.orgId : '';
    this.params.type = this.searchParam.type ? this.searchParam.type : 'POSITION';
    this.params.status = [null, undefined, ''].indexOf(String(this.searchParam.status)) > 0 ? null : this.searchParam.status;
  }

  deletePosition(data: ListPositionModel) {
    const params = {
      "posId": data.posId,
    }
    this.deletePopup.showModal(() => {
      this.isLoadingPage = true;
      this.jobService.deletePosition(params).subscribe(value => {
        this.isLoadingPage = false;
        this.toastrCustom.success();
        this.onSearch();
      }, error => {
        this.isLoadingPage = false;
        this.toastrCustom.error(error.message)
      })
    })
  }
  checkData() {
    this.dataTable.forEach(item => {
      if (item.position && item.position.every(value => value.isHead === 'N' || !value.isHead)) {
        item.isDisable = false;
      } else {
        if (item.isHead === 'Y') {
          item.isDisable = false;
        } else {
          item.isDisable = true;
        }
      }
    });
  }
  savePosition() {
    const params:SavePosRes[] = [];
    this.dataTable.forEach(item => {
      params.push({
        posId: item.posId,
        isHead: item.isHead
      })
    });
    this.jobService.savePosition(params).subscribe(data => {
      this.toastrCustom.success();
      this.onSearch();
    }, (error) => {
      this.toastrCustom.error(error.message);
    });
  }
}
