import {Component, Injector, OnInit} from "@angular/core";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {FunctionCode, functionUri, listStatus, types} from "@hcm-mfe/shared/common/enums";
import {NzResizeEvent} from "ng-zorro-antd/resizable";
import {userConfig} from "@hcm-mfe/shared/common/constants";
import * as moment from 'moment';
import {
  Job,
  ListPlanModel,
  MB_BANK,
  ObjectSearch,
  Pagination, PlanModel, ResponseEntity,
  Year, YearModel
} from "@hcm-mfe/model-organization/data-access/models";
import {DataService, StaffPlanService} from "@hcm-mfe/model-organization/data-access/services";
import * as FileSaver from 'file-saver';
import { FormModalShowTreeUnitComponent } from '@hcm-mfe/shared/ui/form-modal-show-tree-unit';
import {renderFlagStatus} from "../../../../../helper/common.function";

@Component({
  selector: 'app-plan-year',
  templateUrl: './plan-year.component.html',
  styleUrls: ['./plan-year.component.scss'],
})
export class PlanYearComponent extends BaseComponent implements OnInit {
  type = "PY";
  colMatDraw = 5;
  id = -1;
  orgName = MB_BANK;
  listJob: Array<Job> = [];
  params = {
    id: '',
    page: 0,
    size: userConfig.pageSize,
  };
  isVisible = false;
  listSettingPaginationCustom: Pagination = new Pagination(userConfig.pageSize);

  objectSearch: ObjectSearch = {
    orgId: '2',
    flagStatus: 1,
    inputType: '',
    page: 0,
    size: userConfig.pageSize,
    jobId: '',
    fromDate: '',
    year: new Date().getFullYear().toString(),
  };

  listType = types;
  listStatus = listStatus;
  listYears: Array<Year> = [];
  listData: Array<ListPlanModel> = [];
  renderFlagStatus = renderFlagStatus;
  constructor(
    injector: Injector,
    private readonly dataService: DataService,
    private readonly staffPlanService: StaffPlanService
  ) {
    super(injector);

    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ORGANIZATION_STAFF_PLAN}`);
    console.log(this.objFunction)
    if (!this.objFunction.view) {
      this.router.navigateByUrl(functionUri.access_denied);
    }
  }

  ngOnInit(): void {
    this.getDataPlanYear();
    this.getListJob();
    this.getYears();
  }

  getListJob() {
    this.dataService.getJobsByTypePlanYear().subscribe((data) => {
      this.listJob = data.data.content || [];
    });
  }
  getYears() {
    this.staffPlanService.getListYear().subscribe((data: YearModel) => {
      data.data.periodCode.forEach((item:any) => {
        this.listYears.push({ value: item });
      });
    });
  }

  getDataPlanYear() {
    let objTmp = { ...this.objectSearch };
    objTmp = this.checkParams(objTmp, true);
    this.staffPlanService.getListPlan(objTmp).subscribe((data: ResponseEntity<PlanModel>) => {
      this.listData = data.data.content || [];
      this.listSettingPaginationCustom.pageSize = data.data.pageable.pageSize || 0;
      this.listSettingPaginationCustom.totalElements = data.data.totalElements || 0;
      this.listSettingPaginationCustom.pageNumber = data.data.pageable.pageNumber || 0;
      this.listSettingPaginationCustom.numberOfElements = data.data.numberOfElements || 0;
    });
  }

  checkParams(objTmp:any, isSearch:any) {
    if (isSearch) {
      objTmp.flagStatus = [undefined, null, ''].indexOf(objTmp.flagStatus) > 0
        ? '' : objTmp.flagStatus;
      objTmp.jobId = objTmp.jobId ? objTmp.jobId : '';
      objTmp.orgId = objTmp.orgId ? objTmp.orgId : '';
    } else {
      objTmp.flagStatus = [undefined, null, ''].indexOf(objTmp.flagStatus) > 0
        ? null : objTmp.flagStatus;
      objTmp.jobId = objTmp.jobId ? objTmp.jobId : null;
      objTmp.orgId = objTmp.orgId ? objTmp.orgId : null;
    }
    objTmp.inputType = objTmp.inputType ? objTmp.inputType : '';
    objTmp.fromDate = objTmp.fromDate ? moment(objTmp.fromDate).format('YYYY-MM-DD') : '';
    objTmp.year = objTmp.year ? objTmp.year : '';
    return objTmp;
  }

  goBackPage() {
    this.location.back();
  }

  getListTemplateBasePagination(value:any) {
    this.objectSearch.page = value - 1;
    this.getDataPlanYear();
  }

  onResizeMatDraw({ col }: NzResizeEvent): void {
    cancelAnimationFrame(this.id);
    this.id = requestAnimationFrame(() => {
      if(col){
        this.colMatDraw = col;
      }
    });
  }

  showModalOrg(event:any): void {
    const modal = this.modal.create({
      nzWidth: '80vw',
      nzTitle: '',
      nzComponentParams: {
        isAuthor: true
      },
      nzContent: FormModalShowTreeUnitComponent,
      nzFooter: null,
    });
    modal.afterClose.subscribe((result) => {
      if (result?.orgName) {
        this.orgName = result?.orgName;
        this.objectSearch.orgId = result?.orgId;
      }
    });
  }

  clearOrgInput() {
    this.orgName = '';
    this.objectSearch.orgId = '';
  }
  onSearch() {
    this.objectSearch.page = 0;
    this.getDataPlanYear();
  }

  exportExcel() {
    const objTmp = { ...this.objectSearch };
    delete objTmp.page;
    delete objTmp.size;
    this.checkParams(objTmp, false);
    this.staffPlanService.downloadExcel(objTmp).subscribe((data:any) => {
      const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      FileSaver.saveAs(blob, 'Kế hoạch năm.xls');
    });
  }

  genderFlagStatus(flagStatus:any) {
    let status = '';
    switch (flagStatus) {
      case 0:
        status = this.translate.instant('modelOrganization.planYear.inactive');
        break;
      case 1:
        status = this.translate.instant('modelOrganization.planYear.active');
        break;
      case 2:
        status = this.translate.instant('modelOrganization.planYear.transApproved');
        break;
      case 3:
        status = this.translate.instant('modelOrganization.planYear.reject');
        break;
      case 5:
        status = this.translate.instant('modelOrganization.planYear.create');
        break;
      case 6:
        status = this.translate.instant('modelOrganization.planYear.confirm');
        break;
      case 7:
        status = this.translate.instant('modelOrganization.planYear.notCreate');
        break;
      default:
        status = '';
    }
    return status;
  }

  override triggerSearchEvent(): void {
    this.onSearch();
  }
}
