import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {NzFormModule} from "ng-zorro-antd/form";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedDirectivesUppercaseInputModule} from "@hcm-mfe/shared/directives/uppercase-input";
import {NzInputModule} from "ng-zorro-antd/input";
import {SharedDirectivesTrimInputModule} from "@hcm-mfe/shared/directives/trim-input";
import {RouterModule} from "@angular/router";
import {PlanYearComponent} from "./plan-year/plan-year.component";
import {ModelOrganizationDataAccessModelsModule} from "@hcm-mfe/model-organization/data-access/models";
import {NzResizableModule} from "ng-zorro-antd/resizable";
import {NzCardModule} from "ng-zorro-antd/card";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {NzWaveModule} from "ng-zorro-antd/core/wave";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import { SharedUiMbTreeModule } from '@hcm-mfe/shared/ui/mb-tree';
import { NzIconModule } from 'ng-zorro-antd/icon';
import {NzTagModule} from "ng-zorro-antd/tag";

@NgModule({
    imports: [CommonModule, SharedUiMbButtonModule, TranslateModule, SharedUiLoadingModule, NzFormModule, FormsModule,
        ReactiveFormsModule, SharedUiMbInputTextModule, SharedDirectivesUppercaseInputModule, NzInputModule, SharedDirectivesTrimInputModule,
        ModelOrganizationDataAccessModelsModule,
        RouterModule.forChild([
            {
                path: '',
                component: PlanYearComponent
            }
        ]), NzResizableModule, NzCardModule, SharedUiMbSelectModule, NzWaveModule, NzTableModule, NzToolTipModule,
      NzPaginationModule, SharedUiMbDatePickerModule, SharedUiMbTreeModule, NzIconModule, NzTagModule
    ],
  declarations: [PlanYearComponent],
  exports: [PlanYearComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ModelOrganizationFeaturePlanYearModule {}
