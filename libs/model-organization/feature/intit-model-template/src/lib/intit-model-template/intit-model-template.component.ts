import { forkJoin } from 'rxjs';
import { FormGroup, FormControl, Validators, FormArray, AbstractControl } from '@angular/forms';
import { Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { FunctionCode, functionUri, maxInt32, REGEX } from '@hcm-mfe/shared/common/enums';
import { Job, RegionType } from '@hcm-mfe/model-organization/data-access/models';
import { DataService } from '@hcm-mfe/shared/data-access/services';
import { OrganizationalModelTemplateService } from '@hcm-mfe/model-organization/data-access/services';
export interface Organization {
  level: number;
  orgId: string;
  jobDTOS: Job[];
  subs: Organization[];
  orgName: string;
  control?: FormControl;
}
@Component({
  selector: 'app-intit-model-template',
  templateUrl: './intit-model-template.component.html',
  styleUrls: ['./intit-model-template.component.scss'],
})
export class IntitModelTemplateComponent extends BaseComponent implements OnInit {
  listBranch = [];
  allFruits: any[] = [];
  request: Organization[]= [];
  loading = false;
  objectSubmit: {
    orgName: string;
    subs: Organization[];
    flagStatus: number;
    orgId?: string;
    branchType?:  {
      lvaValue: string,
      lcoId: number
    };
  } = {
    orgName: '',
    subs: [],
    flagStatus: 0,
  };
  id ='';
  formInput: FormGroup;
  constructor(
    injector:Injector,
    private organizationalModelTemplateService: OrganizationalModelTemplateService,
    private dataService: DataService,
  ) {
    super(injector)
    this.formInput = new FormGroup({
      orgName: new FormControl('', [Validators.required, Validators.pattern(REGEX.SPECIAL_TEXT)]),
      flagStatus: new FormControl("1"),
      branchType: new FormControl(''),
      orgNames: new FormArray([]),
    });
  }
  listData: Organization[] = [];

  ngOnInit(): void {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ORGANIZATION_ORG_TEMPLATE_INIT}`);
    console.log(this.objFunction)
    if(!this.objFunction.view){
      this.router.navigateByUrl(functionUri.access_denied);
    }


    this.id = localStorage.getItem('selectedId')||'';

    if (this.id) {
      this.initData(true)
    } else {
      this.initData(false)
      const control = new FormControl('', [Validators.pattern(REGEX.SPECIAL_TEXT), Validators.required]);
      (<FormArray>this.formInput.controls['orgNames']).push(control);
      const initData = {
        level: 0,
        orgName: '',
        jobDTOS: [],
        subs: [],
        control: control,
      };
      this.listData.push(<any>initData);
    }
  }

  override ngOnDestroy(): void {
    localStorage.removeItem('selectedId');
  }

  initData(edit:boolean){
    this.loading = true;
    const request = [this.dataService.getJobsByType('',0,maxInt32,['ORG','DV']), this.organizationalModelTemplateService.getSubRegion(RegionType.LH, this.formInput.controls['flagStatus'].value)]
    if(edit){
      request.push(this.organizationalModelTemplateService.getTemplateDetail(this.id))
    }
    forkJoin(request).subscribe((data:any[]) => {
      this.listBranch = data[1].data;
      const listJob = data[0].data.content;
      listJob.forEach((element:any) => {
        this.allFruits.push({
          id: element.jobId,
          value: element.jobName,
        });
      });
      if(edit){
        const dataOrg = data[2].data;
        data[2].data.forEach((item:any) => {
          if (item.orgLevel === 1) {
            this.formInput.controls['orgName'].setValue(item.orgName);
            this.formInput.controls['branchType'].setValue(item.branchType)
            this.formInput.controls['flagStatus'].setValue(item.flagStatus!==null ? String(item.flagStatus) : '')
          }
        });
        this.buildOrgObject(dataOrg);
      }
      this.loading = false;
    }, error => {
      this.loading = false;
      this.toastrCustom.error(error.error?.message)
    });
  }

  goBackPage() {
    this.location.back();
  }

  addOrg(parentOrg: Organization) {
    const control = new FormControl('', [Validators.pattern(REGEX.SPECIAL_TEXT), Validators.required]);
    (<FormArray>this.formInput.controls['orgNames']).push(control);
    const item = {
      level: parentOrg.level + 1,
      jobDTOS: [],
      subs: [],
      control: control,
    };
    if(!parentOrg.subs) {
      parentOrg.subs = [];
    }
    parentOrg.subs.push(<any>item);
  }

  addOrgLevel(list:any, level:any) {
    const control = new FormControl('', [Validators.pattern(REGEX.SPECIAL_TEXT), Validators.required]);
    (<FormArray>this.formInput.controls['orgNames']).push(control);
    const item = {
      orgId:null,
      level: level,
      jobDTOS: [],
      subs: [],
      control: control,
    };
    list.push(<any>item);
  }

  coppyOrg(list:any, item: Organization, level:any) {
    const control = new FormControl(item.control?.value, [Validators.pattern(REGEX.SPECIAL_TEXT), Validators.required]);
    (<FormArray>this.formInput.controls['orgNames']).push(control);
    const newItem = {
      orgName: item.orgName,
      orgId: null,
      level: level,
      jobDTOS: [...item.jobDTOS],
      control: control,
    };

    list.push(<any>newItem);
  }

  deleteOrg(list:any, index:any, parent?:any) {
    if (parent) {
      if (list.length === 1) {
        this.toastrCustom.error();
        return;
      }
    }
    (<FormArray>this.formInput.controls['orgNames']).removeAt(this.getIndexControl(list[index].control));
    list.splice(index, 1);
  }

  submit() {
    if (this.formInput.invalid) {
      console.log(this.formInput)
      Object.values(this.formInput.controls).forEach((control) => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });

      Object.values((<FormArray>this.formInput.get('orgNames')).controls).forEach((control: any) => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
      return;
    }
    const branch = this.getBranchName(this.formInput.value.branchType)
    this.objectSubmit.orgName = this.formInput.value.orgName.trim();
    this.objectSubmit.flagStatus = this.formInput.value.flagStatus;
    this.objectSubmit.branchType = branch ? {
      lcoId: branch?.lcoId,
      lvaValue: branch?.lvaValue
    } : null;
    this.trimValue(this.listData);

    const listOrgReq:any = [];
    this.builtRequest(this.listData, listOrgReq)
    this.objectSubmit.subs = listOrgReq;
    this.loading = true
    if(this.id){
      this.updateTemplate()
    } else {
      this.createTemplate();
    }

  }

  builtRequest(source:Organization[], destination:Organization[]){

    for(let item of source){
      let jobs= []
      for(let job of item.jobDTOS){
        jobs.push(job.id)
      }

      const newItem:Organization = {
        orgName: item.orgName,
        orgId: item.orgId,
        jobDTOS: item.jobDTOS,
        level: item.level,
        subs: []
      }
      destination.push(newItem);
      if(!item.subs){
        item.subs = []
      }
      this.builtRequest(item.subs, newItem.subs)
    }
  }

  createTemplate(){
    this.loading = true;
    this.organizationalModelTemplateService.createTemalpate(this.objectSubmit).subscribe(
      (data) => {
        this.loading = false
        this.toastrCustom.success("Lưu thành công");
        this.router.navigateByUrl('/organization/model-organization-template');
      },
      (error) => {
        this.loading = false
        this.toastrCustom.error(error.message);
      }
    );
  }

  updateTemplate() {
    this.loading = true;
    this.objectSubmit.orgId = this.id;
    this.organizationalModelTemplateService.updateTemplate(this.objectSubmit).subscribe(
      (data) => {
        this.loading = false
        this.toastrCustom.success("Lưu thành công");
        this.router.navigateByUrl('/organization/model-organization-template');
      },
      (error) => {
        this.loading = false
        this.toastrCustom.error(error.message);
      }
    );
  }

  buildOrgObject(dataOrg:any) {
    dataOrg.sort((a:any, b:any) => {
      if (a.orgLevel < b.orgLevel) {
        return -1;
      }
      if (a.orgLevel > b.orgLevel) {
        return 1;
      }
      return 0;
    });

    const map = new Map<string, any>();
    for (const item of dataOrg) {
      item.jobDTOS = [];
      item.subs = [];
      const control = new FormControl(item.orgName, [Validators.pattern(REGEX.SPECIAL_TEXT), Validators.required]);
      (<FormArray>this.formInput.controls['orgNames']).push(control);
      item.control = control;
      if (item.posL) {
        for (let pos of item.posL) {
          if (!pos.jobId) {
            continue;
          }
          item.jobDTOS.push({
            id: pos.jobId,
            value: this.getPosName(pos.jobId),
          });
        }
      }
      map.set(item.orgId, item);
      if (item.parentId && item.orgLevel > 1) {
        let parent = map.get(item.parentId);
        if (parent.subs) {
          parent.subs.push(item);
        } else {
          parent.subs = [];
          parent.subs.push(item);
        }
      }
    }
    dataOrg = dataOrg.filter((item:any) => item.orgLevel === 2);
    this.listData.push(...dataOrg);
  }

  getPosName(jobId:any) {
    for (let item of this.allFruits) {
      if (item.id === jobId) {
        return item.value;
      }
    }
  }

  trimValue(list: Organization[]) {
    if (!list) {
      return;
    }

    for (let item of list) {
      item.orgName = item.control?.value.trim();
      this.trimValue(item.subs);
    }
  }

  getIndexControl(controlEl: FormControl): number {
    let controls: AbstractControl[];
    controls = (<FormArray>this.formInput.controls['orgNames']).controls;
    const indexOfControl: number = controls.findIndex((control) => {
      return control === controlEl;
    });
    return indexOfControl;
  }

  getBranchName(id:any){
    if(!id){
      return null;
    }
    for(let i = 0; i < this.listBranch.length; i++){
      let item = this.listBranch[i]
      if(id == item?.lvaValue){
        return item;
      }
    }
    return null;
  }
  updateJob(value:any) {
    if (value) {
      while (this.allFruits.length) {
        this.allFruits.pop()
      }
      this.callGetJob()
    }
  }
  callGetJob() {
    this.dataService.getJobsByType().subscribe((data) => {
      const listJob = data.data.content;
      listJob.forEach((element:any) => {
        if (!element.jobName) {
          element.jobName = '';
        }
        this.allFruits.push({
          id: element.jobId,
          value: element.jobName,
        });
      });
    });
  }
}
