import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntitModelTemplateComponent } from './intit-model-template.component';

describe('IntitModelTemplateComponent', () => {
  let component: IntitModelTemplateComponent;
  let fixture: ComponentFixture<IntitModelTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntitModelTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntitModelTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
