import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ManageLineComponent} from "./manage-line/manage-line.component";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {NzResizableModule} from "ng-zorro-antd/resizable";
import {NzInputModule} from "ng-zorro-antd/input";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {MatTreeModule} from "@angular/material/tree";
import {MatMenuModule} from "@angular/material/menu";
import {MatDividerModule} from "@angular/material/divider";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzGridModule} from "ng-zorro-antd/grid";
import {NzCardModule} from "ng-zorro-antd/card";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {NzTabsModule} from "ng-zorro-antd/tabs";
import {NzTableModule} from "ng-zorro-antd/table";
import {ModelOrganizationUiJobSelectModule} from "@hcm-mfe/model-organization/ui/job-select";
import {NzEmptyModule} from "ng-zorro-antd/empty";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
import {NzSwitchModule} from "ng-zorro-antd/switch";
import {RouterModule} from "@angular/router";
import {MatIconModule} from "@angular/material/icon";
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatButtonModule} from "@angular/material/button";
import {MatDialogModule} from "@angular/material/dialog";
import {MatBottomSheetModule} from "@angular/material/bottom-sheet";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatInputModule} from "@angular/material/input";
import {MatListModule} from "@angular/material/list";
import {MatChipsModule} from "@angular/material/chips";
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatProgressBarModule} from "@angular/material/progress-bar";
import {MatSelectModule} from "@angular/material/select";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatStepperModule} from "@angular/material/stepper";
import {MatExpansionModule} from "@angular/material/expansion";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {MatRadioModule} from "@angular/material/radio";
import {MatSliderModule} from "@angular/material/slider";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {MatBadgeModule} from "@angular/material/badge";
import {MatGridListModule} from "@angular/material/grid-list";
import {MatSortModule} from "@angular/material/sort";
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatTableModule} from "@angular/material/table";
import {MatTabsModule} from "@angular/material/tabs";
import {MatTooltipModule} from "@angular/material/tooltip";
import {CdkTreeModule} from '@angular/cdk/tree';
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";
import {NzTreeModule} from "ng-zorro-antd/tree";
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';


@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, NzResizableModule, NzInputModule, NzTreeModule,
    MatSidenavModule, ReactiveFormsModule, MatButtonModule, NzCheckboxModule,
    RouterModule.forChild([
      {
        path: '',
        component: ManageLineComponent
      }]),
    NzDropDownModule, MatTreeModule, MatMenuModule, MatDividerModule, SharedUiMbButtonModule, NzGridModule, MatIconModule,
    NzCardModule, ReactiveFormsModule, TranslateModule, SharedUiMbInputTextModule, SharedUiMbDatePickerModule,
    NzTabsModule, NzTableModule, ModelOrganizationUiJobSelectModule, FormsModule, NzEmptyModule, NzToolTipModule, NzSwitchModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonToggleModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatInputModule,
    MatListModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatAutocompleteModule,
    MatRadioModule,
    MatSelectModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    CdkTreeModule, SharedUiMbSelectModule
  ],
  declarations: [ManageLineComponent],
  exports: [ManageLineComponent]
})
export class ModelOrganizationFeatureManageLineModule {}
