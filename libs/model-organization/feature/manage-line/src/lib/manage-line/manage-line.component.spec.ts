import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageLineComponent } from './manage-line.component';

describe('ManageLineComponent', () => {
  let component: ManageLineComponent;
  let fixture: ComponentFixture<ManageLineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageLineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
