import {AfterViewInit, Component, ElementRef, Injector, OnDestroy, OnInit, Renderer2, ViewChild} from '@angular/core';
import { forkJoin, fromEvent } from 'rxjs';
import { filter, debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import * as moment from 'moment';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NzResizeEvent } from 'ng-zorro-antd/resizable';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {
  AssignPosRes,
  ExampleFlatNode,
  Job, JobLine, OrgLine,
  PositionTable,
  ResponseEntity
} from "@hcm-mfe/model-organization/data-access/models";
import {
  ArrayOrganizationLineDTOS,
  DataLineInit, LineInit,
  ManageLine, SearchLineInitList, TreeLine
} from '../../../../../data-access/models/src/lib/line.model';
import {
  DataService,
  JobService,
  LineService,
  OrganizationService
} from "@hcm-mfe/model-organization/data-access/services";
import { FunctionCode, maxInt32 } from '@hcm-mfe/shared/common/enums';
import {regexSpecialForLine} from "@hcm-mfe/shared/common/constants";

@Component({
  selector: 'app-manage-line',
  templateUrl: './manage-line.component.html',
  styleUrls: ['./manage-line.component.scss'],
})
export class ManageLineComponent extends BaseComponent implements OnInit, AfterViewInit {
  @ViewChild('scoll') scoll:ElementRef | undefined;
  @ViewChild('input', { static: true }) input: ElementRef | undefined;
  hideListDropdown: boolean | undefined;
  id = -1;
  col = 5;
  listJobCn: Job[] = [];
  listJobCnSelect: Job[] = [];
  dataTableGroup: (PositionTable|ArrayOrganizationLineDTOS)[] = [];
  lineForm: FormGroup;
  orgNameDisplay: string | undefined;
  lineNm: string | undefined;
  isDupLine = false;
  lineTmp: string | undefined;
  selectTab: number | undefined;
  isSubmitted = false;
  orgList: OrgLine[] = [];
  orgListFilter: OrgLine[] = [];
  dataSearch: string | undefined;
  selectAll: boolean | undefined;
  stackCall: number[] = [];
  scollStack: PositionTable[] = [];
  mapOrgSelect: Map<number, PositionTable | ArrayOrganizationLineDTOS | OrgLine> = new Map();
  mapOrgSelectDetail: Map<number, PositionTable | ArrayOrganizationLineDTOS> = new Map();
  loadItemNumber = 15;
  treeLine:TreeLine[] = [];
  dataTableLine: Array<DataLineInit> = [
    {
      nameManagerLine: '',
      nameLine: '',
      effectiveDate: '',
      expirationDate: '',
      pgrId: '',
      parentId: '',
    },
  ];
  dataDropdown:ManageLine[] = [];
  lineList: Array<SearchLineInitList> = [];
  lineListTmp: Array<SearchLineInitList> = [];
  isOpenOrg: boolean = false;
  hasChild = (_: number, node: ExampleFlatNode) => node.expanded;

  constructor(
    injector: Injector,
    readonly lineService: LineService,
    readonly dataService: DataService,
    readonly orgService: OrganizationService,
    readonly jobService: JobService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(
      `FUNCTION_${FunctionCode.MANAGE_LINE}`
    );
    this.isLoading = false;
    const id = localStorage.getItem('selectedIdLine');
    if (id) {
      this.assignInfoLine(Number(id));
    }

    forkJoin([this.lineService.getListTreeLine(), this.lineService.findAllByOrgGroupCN()]).subscribe(
      ([treeLine, jobCN]: [ResponseEntity<TreeLine[]>, ResponseEntity<JobLine[]>]) => {
        const response = treeLine.data;
        this.treeLine = response;
        this.addKey(this.treeLine,'');
        if (!jobCN.data) {
          this.listJobCn = [];
          this.isLoading = false;
          return;
        }
        for (const item of jobCN.data) {
          item.id = item.jobId;
          item.value = item.jobName;
        }
        this.listJobCn = jobCN.data;
        this.isLoading = false;
      },
      (error) => {
        this.toastrCustom.error(error.message);
        this.isLoading = false;
      }
    );
    this.lineForm = new FormGroup({
      lineManage: new FormControl({ value: ''}),
      name: new FormControl('', Validators.required),
      fromDate: new FormControl('', Validators.required),
      toDate: new FormControl(''),
      parentId: new FormControl('')
    });
  }

  ngOnInit(): void {
    localStorage.removeItem('selectedIdLine');
    this.hideListDropdown = false;
    this.orgNameDisplay = "";
    this.lineNm = "";
    this.isDupLine = false;
    this.lineTmp = "";
    this.selectTab = 0;
    this.listLine();
    this.onSearch("", null);
  }

  ngAfterViewInit() {
    fromEvent(this.input?.nativeElement, 'keyup')
      .pipe(
        filter(Boolean),
        debounceTime(500),
        <any>distinctUntilChanged(),
        tap((event: KeyboardEvent) => {
          if (this.input?.nativeElement.value !== '') {
            this.hideListDropdown = false;
            this.lineService
              .getListNameSuggest(this.input?.nativeElement.value.toUpperCase())
              .subscribe((data) => {
                this.dataDropdown = data.data;
              });
          } else {
            this.hideListDropdown = true;
            this.lineService.getListTreeLine().subscribe((data) => {
              const response = data.data;
              this.treeLine = response;
            });
          }
        })
      )
      .subscribe();
  }

  addKey(data: TreeLine[] | undefined, keyParent?: any) {
    if (!data) {
      return
    }
    for (let i = 0; i < data.length; i++) {
      data[i].title = data[i].name;
      data[i].key = keyParent + '-' + i;
      this.addKey(data[i].subs, data[i].key)
      data[i].children = data[i].subs ? data[i].subs : [];
    }
  }

  redrirectToAddLine(fromTree:boolean, node:any) {
    const line = { ...this.dataTableLine[0] };
    if (fromTree) {
      line.parentId = node.id;
      line.nameManagerLine = node.name;
    } else {
      line.parentId = '';
      line.nameManagerLine = '';
    }
    line.nameLine = '';
    line.effectiveDate = '';
    line.expirationDate = '';

    this.lineService.setDataLine(line);
    localStorage.removeItem('selectedIdLine');
    this.router.navigateByUrl('/organization/line-group/init-line');
  }

  redrirectToEditLine(fromTree:boolean, id:string) {
    const pgrId = this.dataTableLine[0].pgrId;
    if (!pgrId && !fromTree) {
      this.toastrCustom.error('Vui lòng chọn line');
      return;
    }
    localStorage.setItem('selectedIdLine',id);
    this.router.navigateByUrl('/organization/line-group/init-line');
  }

  assignInfoLine(id:number) {
    this.mapOrgSelect.clear();
    this.mapOrgSelectDetail.clear();
    this.dataTableGroup = [];
    this.isLoading = true;
    this.listJobCnSelect = [];
    this.lineService.getLineDetailById(id).subscribe(
      (data) => {
        this.isLoading = false;
        const line = {
          nameManagerLine: data.data.parentName,
          nameLine: data.data.pgrName,
          effectiveDate: data.data.fromDate,
          expirationDate: data.data.toDate,
          pgrId: data.data.pgrId,
          parentId: data.data.parentId,
        };
        this.lineForm.controls['lineManage'].setValue(line.parentId);
        this.lineForm.controls['name'].setValue(line.nameLine);
        this.lineForm.controls['fromDate'].setValue(line.effectiveDate);
        this.lineForm.controls['toDate'].setValue(line.expirationDate);
        this.lineForm.controls['parentId'].setValue(line.parentId);
        this.dataTableLine = [line];
        this.listJobCnSelect = data.data.jobs ? data.data.jobs : [];
        this.lineListTmp = this.lineList.filter(item => item.id !== data.data.pgrId);
        data.data.organizationLineDTOS?.map(org => {
          org.jobSelect = org.positionLineDTOS;
          org.pathName = org.orgName;
          this.mapOrgSelect.set(org.orgId, org);
          this.mapOrgSelectDetail.set(org.orgId, org);
          this.getListJob(org);
          return org;
        })

        this.dataTableGroup = data.data.organizationLineDTOS;

         if (!this.dataTableGroup || this.dataTableGroup.length === 0) {
          this.dataTableGroup = [
            {
              orgId: null,
              orgName: '',
              type: 'HO',
              nameBranch: '',
              jobs : [],
              jobSelect : [],
            },
          ];
          return;
        }

      },
      (error) => {
        this.toastrCustom.error(error.message);
        this.isLoading = false;
      }
    );
  }

  addOrgLine() {
    const item  = {
      orgId: null,
      orgName: '',
      jobs: [],
      jobSelect: [],
      loadJob: false,
      isVisible: false,
    }

    if(this.dataTableGroup.length === 0){
      this.dataTableGroup = [item]
    } else {
      this.dataTableGroup.push(item);
    }
  }


  removeOrgLine(index:number) {
    const id = this.dataTableGroup[index].orgId;
    if(id){
      this.mapOrgSelect.delete(id);
      if(this.mapOrgSelectDetail.has(id)){
        this.mapOrgSelectDetail.delete(id);
      }
    }
    this.dataTableGroup.splice(index, 1);
  }

  onSearch(value: string, _:string|null): void {
    this.isLoading = true;
    this.orgService.getListOrg().subscribe(
      data => {
        this.orgList = data.data.filter(org => org.orgName !== "MB-HO");
        this.orgListFilter = this.orgList;
        this.isLoading = false;
      },
      (error) => {
        this.isLoading = false;
      }
    );
  }

  selectedNodeId(id:number) {
    this.lineService.getListTreeLineById(id).subscribe(
      (data) => {
        const response = data.data;
        this.treeLine = response;
      },
    );
  }

  submitLine() {
    const pgrId = this.dataTableLine[0].pgrId;
    let posList:any = [];
    for (const jobCn of this.listJobCnSelect) {
      posList.push(this.listJobCn?.find(item => item.jobId === jobCn.jobId)?.posIds);
    }
    for (const org of this.dataTableGroup) {
      for (const jobCn of org.jobSelect) {
        posList.push(jobCn.id ? jobCn.id : jobCn.posId);
      }
    }
    const result = posList.reduce((accumulator:any, value:string) => accumulator.concat(value), []);
    posList = result;
    const requestObject:AssignPosRes = {
      positionGroupId: pgrId,
      positionIds: posList,
    };
    this.showConfirm(requestObject);
  }


  goToListPage() {
    this.router.navigateByUrl('/organization/line-group/list-line');
  }

  showConfirm(body:AssignPosRes): void {
    this.isLoading = true;
    this.lineService.tranferLine(body).subscribe(
      (data) => {
        this.isLoading = false;
        localStorage.removeItem('selectedIdLine');
        this.toastrCustom.success('Chuyển duyệt thành công');
        this.router.navigateByUrl('/organization/line-group/list-line');
      },
      (_) => {
        this.isLoading = false;
        this.toastrCustom.error('Chuyển duyệt thất bại');
      }
    );
  }

  changeTab(e:number) {
    this.selectTab = e;
  }

  validateRequest() {
    let checkValid = false;
    this.isSubmitted = true;

    if (!this.lineForm.valid) {
      checkValid = true;
    }

    if (this.lineForm.value.fromDate && this.lineForm.value.toDate) {
      if (moment(new Date(this.lineForm.value.fromDate)) >= moment(new Date(this.lineForm.value.toDate))) {
        this.toastrCustom.error(this.translate.instant("common.manageLine.errorDate"));
        return;
      }
    }

    if (checkValid) {
      Object.values(this.lineForm.controls).forEach((control) => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
      return;
    }
    const pgrId = this.dataTableLine[0].pgrId;
    if (!pgrId) {
      this.toastrCustom.error('Vui lòng chọn line');
      return;
    }
    for (const org of this.dataTableGroup) {
      if(org.jobSelect.length === 0){
        if(this.mapOrgSelect.size > 0){
          return;
        }
      }
    }

    if (this.mapOrgSelect.size === 0 && this.listJobCnSelect.length === 0) {
      this.toastrCustom.error(this.translate.instant('modelOrganization.message.job'));
      return;
    }

    this.lineForm.controls['name'].setValue(this.lineForm.value.name.trim());

    const reqObject:LineInit = {
      id: this.dataTableLine[0].pgrId,
      parentId: this.lineForm.value.lineManage,
      name: this.lineForm.value.name,
      fromDate: this.lineForm.value.fromDate ? moment(this.lineForm.value.fromDate).format('YYYY-MM-DD') : null,
      toDate: this.lineForm.value.toDate ? moment(this.lineForm.value.toDate).format('YYYY-MM-DD') : null,
    };
    this.updateLine(reqObject);
  }
  updateLine(reqObject:LineInit) {
    this.isLoading = true;
    this.lineService.updateLine(reqObject).subscribe(
      (data) => {
        this.isLoading = false
        this.submitLine();
      },
      (error) => {
        this.isLoading = false
        if (error.error.message === "Data already exist exception") {
          this.isDupLine = true;
          this.lineTmp = this.lineForm.value.name;
        } else {
          this.toastrCustom.error('Chuyển duyệt thất bại');
        }
      }
    );
  }
  onClickLine(nameLine:string) {
    this.lineNm = nameLine;
  }
  onKeypress(event: KeyboardEvent) {
    const pattern = regexSpecialForLine;
    if (pattern.test(event.key)) {
      event.preventDefault();
    }
  }
  handleChangeLine(event:Event) {
    if ((event.target as HTMLTextAreaElement).value !== this.lineTmp) {
      this.isDupLine = false;
    }

  }

  change(value:boolean) {
    if (value) {
      this.orgListFilter.forEach((org) => {
        if(!this.mapOrgSelect.has(org.orgId)){
          if(this.mapOrgSelectDetail.has(org.orgId)){
            const detail = this.mapOrgSelectDetail.get(org.orgId);
            if(detail){
              this.mapOrgSelect.set(org.orgId, detail);
            }
          } else {
            this.mapOrgSelect.set(org.orgId, org);
          }
        }
      });
    } else {
      this.mapOrgSelect.clear();
    }
  }

  changePopup(value:boolean){
    if(value){

      this.mapOrgSelect.clear();
      this.dataTableGroup.forEach(org => {
        if(org.orgId){
          this.mapOrgSelect.set(org.orgId, org)
        }
      })
    }
  }

  selectOrg(){
    this.isSubmitted = false;
    this.scollStack = [];
    this.dataTableGroup = [];
    if(this.mapOrgSelect.size === 0){
      this.addOrgLine()
    }
    this.mapOrgSelect.forEach((value) => {
      this.scollStack.push(<PositionTable>value);
    })
    this.genOrg();
  }

  genOrg() {
    if(this.scollStack.length === 0){
      return;
    }
    this.isLoading = true;
    if(this.dataTableGroup.length === 1 && !this.dataTableGroup[0].orgName){
      this.dataTableGroup = [];
    }

    let runNum = this.loadItemNumber;

    while(runNum > 0 && this.scollStack.length > 0){
      const value = this.scollStack.shift();

      if(value){
        if (!value.loadJob) {
          this.getListJob(value);
        }
        value.jobSelect = value.jobSelect ? value.jobSelect : [];
        value.isVisible = false;
        this.dataTableGroup.push(value);
        runNum -= 1;
      }

    }
    if(this.stackCall.length === 0){
      this.isLoading = false;
    }
  }

  getListJob(org:PositionTable | ArrayOrganizationLineDTOS) {
    if (!org.orgId) {
      return;
    }
    this.stackCall.push(1)
    this.lineService.getListJobById(org.orgId).subscribe(
      (data: any) => {
        org.jobs = data.data;
        org.loadJob = true;
        this.stackCall.pop();
        if(this.stackCall.length === 0){
          this.isLoading = false;
        }
      },
      (error) => {
        this.stackCall = []
        this.isLoading = false;
        this.toastrCustom.error(error.message);
      }
    );
    return org;
  }

  changeValue(value:string) {
    this.orgListFilter = this.orgList.filter(
      (item) => item.pathName.toLowerCase().indexOf(value.toLowerCase().trim()) > -1
    );
  }

  onSelect(item:OrgLine) {
    if (this.mapOrgSelect.has(item.orgId)) {
      this.mapOrgSelect.delete(item.orgId);
    } else {
      this.mapOrgSelect.set(item.orgId, item);
    }
  }

  onResize({ col }: NzResizeEvent): void {
    cancelAnimationFrame(this.id);
    this.id = requestAnimationFrame(() => {
      if(col) this.col = col;
    });
  }

  trackByFn(index:number, item:PositionTable | OrgLine) {
    return item.orgId;
  }


  onScroll(event:Event){
    const el = this.scoll?.nativeElement;
    const hight = el.clientHeight + el.scrollTop;
    if(el.scrollHeight - el.scrollHeight * 0.1  < hight){
      if(this.scollStack.length > 0){
        this.genOrg();
      }
    }
  }

  onClickOrg(isClick: boolean){
    this.isOpenOrg = isClick;
  }
  listLine() {
    const objTmp = { name: '',
      page: 0,
      size: maxInt32
    };
    this.lineService.searchLine(objTmp).subscribe(data => {
        this.lineList = data.data.content;
        this.lineListTmp = data.data.content;
    });
  }

}
