import { Component, Injector, Input, OnInit } from '@angular/core';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import {
  JobModelRes,
  JobModelTmp,
  JobModelUnit,
  ListSearchOrganization,
  OrgAttributeModel,
  PositionDetailModel,
  PositionDTOSModel, RegionType,
  Type
} from '@hcm-mfe/model-organization/data-access/models';
import {
  OrganizationalModelTemplateService,
  UnitApprovedService
} from '@hcm-mfe/model-organization/data-access/services';
import { DataService } from '@hcm-mfe/shared/data-access/services';
import { FunctionCode, functionUri } from '@hcm-mfe/shared/common/enums';
@Component({
  selector: 'app-adjust-unit',
  templateUrl: './adjust-unit.component.html',
  styleUrls: ['./adjust-unit.component.scss'],
})
export class AdjustUnitComponent extends BaseComponent implements OnInit {
  @Input() orgSelect: ListSearchOrganization |undefined;
  id: any;
  arrTmp: Array<JobModelTmp> = [];
  dataJob: string[] = [];
  listJob: Array<JobModelRes> = [];
  isValid = false;
  description = '';
  dataOrg: any;
  dataApprovalOrg: any;
  orgAttribute: Array<OrgAttributeModel> = [];
  positionDetailDTOS: Array<PositionDetailModel> = [];
  jobDTO: Array<JobModelUnit> = [];
  positionDTOS: Array<PositionDTOSModel> = [];
  sheetApproval: any;
  isLoadData = false;
  listUnit: Array<string> = [];
  listBranType: Array<Type> = [];
  legalBranchId = 0;
  branchType ='';
  type = '';
  area = '';
  file: any;
  isVisible = false;
  pdfSrc = '';
  constructor(
    injector: Injector,
    readonly unitApproveService: UnitApprovedService,
    readonly organizationService: OrganizationalModelTemplateService,
    readonly dataService: DataService,
  ) {
    super(injector)
  }

  ngOnInit(): void {
    this.isValid = false;
    this.description = '';
    this.sheetApproval = null;
    this.branchType = '';
    this.type = '';
    this.area = '';
    this.isLoadData = false;

    this.id = this.route.snapshot.paramMap.get('id');

    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ORGANIZATION_APPROVE_ORG}`);
    if (!this.objFunction.view) {
      this.router.navigateByUrl(functionUri.access_denied)
    }
  }

  ngOnChanges(changes: any) {
    if (changes.orgSelect) {
      this.getListOrgDetail(this.orgSelect?.orgId);
    }
  }

  getListOrgDetail(id:any): void {
    this.unitApproveService.getListOrgDetail(id).subscribe((data: any) => {
      this.dataOrg = data.data;
      this.dataApprovalOrg = data.data.organization;
      this.orgAttribute = this.dataApprovalOrg.orgAttributeDTOS;
      this.positionDetailDTOS = this.dataApprovalOrg.positionDetailDTOS;
      this.positionDTOS = data.data.organization.positionDTOS;
      this.jobDTO = data.data.organization.jobDTO;
      this.sheetApproval = data.data.sheetApproval;
      this.legalBranchId = data.data.organization.legalBranchId;
      this.getBranchType();
      this.getArea();
      this.getType();
      this.groupData();
    });
  }
  groupData() {
    let jobTmp:any = [];
    this.arrTmp = [];
    this.listJob = [];
    this.listUnit = [];
    this.positionDetailDTOS.forEach((item) => {
      this.orgAttribute.forEach((value) => {
        if (item.orgaId === value.orgaId) {
          this.arrTmp.push({
            orgaId: item.orgaId,
            orgaValue: value.orgaValue,
            posId: item.posId,
          });
        }
      });
      if (!item.orgaId) {
        this.arrTmp.push({
          orgaId: null,
          orgaValue: '',
          posId: item.posId,
        });
      }
    });
    this.positionDTOS.forEach((item) => {
      this.arrTmp.forEach((value) => {
        if (item.id === value.posId) {
          jobTmp.push({ ...value, jobId: item.jobId });
        }
      });
    });
    this.arrTmp = [...jobTmp];
    jobTmp = [];
    this.arrTmp.forEach((item) => {
      // @ts-ignore
      jobTmp.push({ ...item, jobName: this.jobDTO.find((value) => value.jobId === item.jobId).jobName });
    });
    const arrOrgTmp = jobTmp.filter((item:any) => !item.orgaId);
    if (arrOrgTmp.length > 0) {
      this.orgAttribute.push({
        orgaId: null,
        orgaValue: '',
      });
    }
    this.arrTmp = [...jobTmp];
    this.orgAttribute.forEach((item) => {
      this.listJob.push({
        orgaId: item.orgaId,
        orgaValue: item.orgaValue,
        subs: this.arrTmp.filter((v) => v.orgaId === item.orgaId),
      });
    });
    this.listUnit.push(this.dataOrg?.organization.orgName);
    this.isLoadData = true;
  }
  getBranchType() {
    this.dataService.getBranchType().subscribe((data: any) => {
      this.branchType = this.legalBranchId
        ? data.data.find((item:any) => Number(item.orgId) === this.legalBranchId)?.orgName
        : '';
    });
  }

  getType() {
    this.dataService.getRegionCategoryList(RegionType.LH, 1).subscribe((data:any) => {
      this.type = this.dataOrg.branchType
        ? data.data.find((item:any) => item.lvaValue === this.dataOrg.branchType).lvaMean
        : '';
    });
  }

  getArea() {
    this.dataService.getRegionCategoryList(RegionType.KV, 1).subscribe((data: any) => {
      this.area = this.dataOrg.region ? data.data.find((item:any) => item.lvaValue === this.dataOrg.region).lvaMean : '';
    });
  }

  handleReject() {
    if (this.description === '') {
      this.isValid = true;
    } else {
      this.rejectUnit();
      this.isValid = false;
    }
  }
  handleApprove() {
    this.approveUnit();
  }
  approveUnit() {
    this.organizationService
      .approveInitOrganization(this.orgSelect?.orgId, this.orgSelect?.inputType, this.description)
      .subscribe(
        (data) => {
          this.toastrCustom.success(this.translate.instant('modelOrganization.approveDetail.notif.approveSuccess'));
          this.router.navigateByUrl('organization/init/unit-approve');
        },
        (error) => {
          this.toastrCustom.error(this.translate.instant('modelOrganization.approveDetail.notif.approveFail'));
        }
      );
  }

  rejectUnit() {
    this.organizationService
      .rejectedInitOrganization(this.orgSelect?.orgId, this.orgSelect?.inputType, this.description)
      .subscribe(
        (data) => {
          this.toastrCustom.success(this.translate.instant('modelOrganization.approveDetail.notif.rejectSuccess'));
          this.router.navigateByUrl('organization/init/unit-approve');
        },
        (error) => {
          this.toastrCustom.error(this.translate.instant('modelOrganization.approveDetail.notif.rejectFail'));
        }
      );
  }

  downloadFile(id:any) {
    this.unitApproveService.downloadFile(id).subscribe((data) => {
      const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.pdf' });
      this.file = new File([blob], String(this.sheetApproval?.mpDoc?.fileName));
      this.onFileSelected();
    });
  }

  onFileSelected() {
    const reader = new FileReader();
    reader.onload = (e: any) => {
      this.pdfSrc = e.target.result;
    };
    reader.readAsArrayBuffer(this.file);
    this.isVisible = true;
  }

  handleCancel(): void {
    this.isVisible = false;
  }

  calHeight(length:any) {
    if (length > 0) {
      return length * 22 + 18;
    } else {
      return 44;
    }
  }
  calHeightJob(index:any, length:any) {
    if (index === length - 1) {
      return 40;
    } else if (index === 0 && length === 0) {
      return 44;
    }
    return null;
  }
}
