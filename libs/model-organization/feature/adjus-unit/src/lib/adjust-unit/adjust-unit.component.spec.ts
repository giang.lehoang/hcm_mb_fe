import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdjustUnitComponent } from './adjust-unit.component';

describe('AdjustUnitComponent', () => {
  let component: AdjustUnitComponent;
  let fixture: ComponentFixture<AdjustUnitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdjustUnitComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdjustUnitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
