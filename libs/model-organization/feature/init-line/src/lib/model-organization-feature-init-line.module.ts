import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {InitLineComponent} from "./init-line/init-line.component";
import {ReactiveFormsModule} from "@angular/forms";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzCardModule} from "ng-zorro-antd/card";
import {RouterModule} from "@angular/router";
import {NzGridModule} from "ng-zorro-antd/grid";

@NgModule({
  imports: [CommonModule, ReactiveFormsModule, SharedUiLoadingModule, SharedUiMbSelectModule, TranslateModule, NzGridModule,
    RouterModule.forChild([
      {
        path: '',
        component: InitLineComponent,
      }]),
    SharedUiMbInputTextModule, SharedUiMbDatePickerModule, SharedUiMbButtonModule, NzCardModule],
  declarations: [InitLineComponent],
  exports: [InitLineComponent]
})
export class ModelOrganizationFeatureInitLineModule {}
