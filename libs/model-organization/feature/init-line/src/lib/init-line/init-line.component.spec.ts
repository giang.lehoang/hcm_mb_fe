import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InitLineComponent } from './init-line.component';

describe('InitLineComponent', () => {
  let component: InitLineComponent;
  let fixture: ComponentFixture<InitLineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InitLineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InitLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
