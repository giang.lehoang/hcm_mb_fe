import { FormGroup, FormControl, Validators } from '@angular/forms';
import {Component, Injector, OnDestroy, OnInit} from '@angular/core';
import * as moment from 'moment';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {LineService} from "@hcm-mfe/model-organization/data-access/services";
import { FunctionCode } from '@hcm-mfe/shared/common/enums';
import {
  DataLineInit,
  LineInit,
  SearchLineInit,
  SearchLineInitList
} from "../../../../../data-access/models/src/lib/line.model";
import {regexSpecialForLine} from "@hcm-mfe/shared/common/constants";

@Component({
  selector: 'app-init-line',
  templateUrl: './init-line.component.html',
  styleUrls: ['./init-line.component.scss'],
})
export class InitLineComponent extends BaseComponent implements OnInit, OnDestroy {
  title = '';
  lineForm: FormGroup;
  data: DataLineInit;
  id: number;
  mode: boolean;
  loading: boolean;
  searchObject: SearchLineInit;
  lineList: Array<SearchLineInitList> = [];
  isDupLine: boolean;
  lineTmp: string;
  isSubmitted: boolean;
  constructor(
    injector: Injector,
    readonly lineService: LineService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(
      `FUNCTION_${FunctionCode.INIT_LINE}`
    );

    this.id = Number(localStorage.getItem('selectedIdLine'));
    this.data = this.lineService.getDataLine();
    this.isLoading = false;
    this.isDupLine = false;
    this.lineTmp = '';
    this.isSubmitted = false;
    this.loading = false;
    this.lineForm = new FormGroup({
      parentId: new FormControl(''),
      name: new FormControl('', Validators.required),
      fromDate: new FormControl('', Validators.required),
      toDate: new FormControl(''),
    });
    if (this.id) {
      this.title = 'Cập nhật line';
      this.mode = true;
      this.lineService.getLineDetailById(this.id).subscribe(lineDetail => {
          const line = {
            nameManagerLine: lineDetail.data.parentName,
            nameLine: lineDetail.data.pgrName,
            effectiveDate: lineDetail.data.fromDate,
            expirationDate: lineDetail.data.toDate,
            pgrId: lineDetail.data.pgrId,
            parentId: lineDetail.data.parentId,
          };
          this.lineForm.controls['parentId'].setValue(line.parentId);
          this.lineForm.controls['name'].setValue(line.nameLine);
          this.lineForm.controls['fromDate'].setValue(line.effectiveDate);
          this.lineForm.controls['toDate'].setValue(line.expirationDate);
        },
        (error) => {
          this.toastrCustom.error(error?.error.description);
        }
      );
    } else {
      this.mode = false;
      this.title = 'Thêm mới line';
      if (this.data && this.data.parentId) {
        this.loading = true;
        this.lineService
          .searchLine({
            name: this.data.nameManagerLine,
            page: null,
            size: null,
          })
          .subscribe((data: any) => {
            this.loading = false;
            this.lineList = data.data.content;
          });
      }

      this.lineForm = new FormGroup({
        parentId: new FormControl(this.data?.parentId ? this.data?.parentId : ''),
        name: new FormControl(this.data?.nameLine ? this.data?.nameLine : '', Validators.required),
        fromDate: new FormControl(
          this.data?.effectiveDate ? this.data?.effectiveDate : new Date(),
          Validators.required
        ),
        toDate: new FormControl(this.data?.expirationDate ? this.data?.expirationDate : ''),
      });
    }

    this.searchObject = {
      name: '',
      page: 0,
      size: 50,
    };
  }

  ngOnInit(): void {
    this.isDupLine = false;
    this.lineTmp = "";
    this.isSubmitted = false;
    this.onSearch();
   }

  onSearch() {
    const objTmp = {...this.searchObject};
    objTmp.page = 0;
    objTmp.size = 99999;
    this.lineService.searchLine(objTmp).subscribe(data => {
      if(this.id){
        this.lineList = data.data.content.filter(item => item.id !== this.id);
      } else {
        this.lineList = data.data.content;
      }
      this.isLoading = false;
    });
  }

  loadMore() {
    this.isLoading = true;
    if(this.searchObject.page){
      this.searchObject.page = this.searchObject.page + 1;
    }
    this.lineService.searchLine(this.searchObject).subscribe(data => {
      this.lineList = [...this.lineList, ...data.data.content];
      this.isLoading = false;
    });
  }

  validateRequest() {
    this.isSubmitted = true
    let checkValid = false;

    if (!this.lineForm.valid) {
      if (!this.mode) {
        checkValid = true;
      }
    }
    if (this.lineForm.value.fromDate && this.lineForm.value.toDate) {
      const diffDate = moment(new Date(this.lineForm.value.fromDate)).diff(moment(new Date(this.lineForm.value.toDate)),'days');
      if (diffDate >= 0) {
        this.toastrCustom.error(this.translate.instant("common.manageLine.errorDate"));
        return;
      }
    }

    if (checkValid) {
      Object.values(this.lineForm.controls).forEach((control) => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
      return;
    }

    this.lineForm.controls['name'].setValue(this.lineForm.value.name.trim());
    const reqObject:LineInit = {
      id: this.id,
      parentId: this.lineForm.value.parentId ? this.lineForm.value.parentId : null,
      name: this.lineForm.value.name,
      fromDate: this.lineForm.value.fromDate ? moment(this.lineForm.value.fromDate).format('YYYY-MM-DD') : null,
      toDate: this.lineForm.value.toDate ? moment(this.lineForm.value.toDate).format('YYYY-MM-DD') : null,
    };

    this.showConfirm(reqObject);
  }

  showConfirm(reqObject:LineInit): void {
    if (this.mode) {
      this.lineService.updateLine(reqObject).subscribe(
        (data) => {
          this.toastrCustom.success('Chỉnh sửa line thành công');
          localStorage.setItem('selectedIdLine',String(this.id));
          this.router.navigateByUrl('/organization/line-group/manage');
        },
        (error) => {
          if (error.message === "Line này đã tồn tại") {
            this.isDupLine = true;
            this.lineTmp = this.lineForm.value.name;
          } else {
            this.toastrCustom.error('Chỉnh sửa line thất bại');
          }
        }
      );
    } else {
      this.lineService.createLine(reqObject).subscribe(
        (data: any) => {
          this.toastrCustom.success('Tạo line thành công');
          localStorage.setItem('selectedIdLine',data.data.id);
          this.router.navigateByUrl('/organization/line-group/manage');
        },
        (error) => {
          if (error.message === "Line này đã tồn tại") {
            this.isDupLine = true;
            this.lineTmp = this.lineForm.value.name;
          } else {
            this.toastrCustom.error('Tạo line thất bại');
          }
        }
      );
    }
  }

  onKeypress(event: KeyboardEvent) {
    const pattern = regexSpecialForLine;
    if (pattern.test(event.key)) {
      event.preventDefault();
    }
  }

  handleChangeLine(event:Event) {
    if ((event.target as HTMLTextAreaElement).value !== this.lineTmp) {
      this.isDupLine = false;
    }
  }

  override ngOnDestroy() {
    this.lineService.setDataLine(null);
  }
}
