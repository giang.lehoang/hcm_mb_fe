import { Component, Injector, OnInit } from "@angular/core";
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import {
  ObjectSearch
} from '@hcm-mfe/model-organization/data-access/models';
import { NzModalRef } from "ng-zorro-antd/modal";
import { FormModalShowTreeUnitComponent } from '@hcm-mfe/shared/ui/form-modal-show-tree-unit';
import { orgGroup } from '@hcm-mfe/shared/common/enums';
import {
  IParamReportEmployee
} from '@hcm-mfe/model-organization/data-access/models';
import {StaffPlanService} from "@hcm-mfe/model-organization/data-access/services";
import * as moment from "moment";
import * as FileSaver from 'file-saver';


@Component({
  selector: 'app-report-employee',
  templateUrl: './report-employee.component.html',
  styleUrls: ['./report-employee.component.scss'],
})
export class ReportEmployeeComponent extends BaseComponent implements OnInit {
  isOpenPopup = false;
  objectSearch: ObjectSearch = {};
  modalRef: NzModalRef| undefined;
  listOrgGroups = orgGroup;
  orgName = '';
  fromDate = new Date();
  params:IParamReportEmployee={
    orgId: '',
    orgGroup: '',
    fromDate: ''
  };

  constructor(injector: Injector, readonly staffPlanService:StaffPlanService) {
    super(injector);
  }

  ngOnInit() {
    console.log('')
  }

  showPopup() {
    this.isOpenPopup = true;
    this.modalRef = this.modal.create({
      nzWidth: '80%',
      nzTitle: undefined,
      nzComponentParams: {
        isAuthor: true
      },
      nzContent: FormModalShowTreeUnitComponent,
      nzBodyStyle: {
        padding: '24px',
        border: 'none',
        'border-radius': '20px',
      },
      nzFooter: null,
    });

    this.modalRef.afterClose.subscribe(result => {
      if (result) {
        this.params.orgId = result.orgId;
        this.orgName = result.orgName;
      }
      this.isOpenPopup = false;
    });
  }

  clearOrgInput() {
    this.params.orgId = '';
    this.orgName = '';
  }

  exportExcel() {
    this.params.fromDate = moment(this.fromDate).format("DD/MM/yyyy");
    this.staffPlanService.reportEmployee(this.params).subscribe(
      (data) => {
        const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
        FileSaver.saveAs(blob, 'Bao_cao_co_cau_nhan_su.xlsx');
      },
      (error) => {
        this.toastrCustom.error(error.error.message);
      }
    );
    console.log(this.params);
  }
}
