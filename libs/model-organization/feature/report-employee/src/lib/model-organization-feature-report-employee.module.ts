import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportEmployeeComponent } from './report-employee/report-employee.component';
import { RouterModule } from '@angular/router';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { SharedUiMbDatePickerModule } from '@hcm-mfe/shared/ui/mb-date-picker';
import { FormsModule } from '@angular/forms';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzIconModule } from 'ng-zorro-antd/icon';

@NgModule({
  imports: [CommonModule, SharedUiMbButtonModule, SharedUiLoadingModule, TranslateModule, SharedUiMbSelectModule, SharedUiMbInputTextModule, SharedUiMbDatePickerModule,
    FormsModule, NzGridModule,
    RouterModule.forChild([
      {
        path: '',
        component: ReportEmployeeComponent
      }]), NzIconModule
  ],
  exports: [ReportEmployeeComponent],
  declarations: [ReportEmployeeComponent],
})
export class ModelOrganizationFeatureReportEmployeeModule {}
