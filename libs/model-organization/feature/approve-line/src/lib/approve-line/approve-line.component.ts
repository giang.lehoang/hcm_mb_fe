
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, DoCheck, Injector, OnInit } from '@angular/core';
import * as moment from 'moment';
import { NzModalRef } from 'ng-zorro-antd/modal';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {DataLineApprove, SearchLineObj} from "../../../../../data-access/models/src/lib/line.model";
import {Pagination} from "@hcm-mfe/model-organization/data-access/models";
import {userConfig} from "@hcm-mfe/shared/common/constants";
import {LineService} from "@hcm-mfe/model-organization/data-access/services";
import { FunctionCode } from '@hcm-mfe/shared/common/enums';


@Component({
  selector: 'app-approve-line',
  templateUrl: './approve-line.component.html',
  styleUrls: ['./approve-line.component.scss'],
})
export class ApproveLineComponent extends BaseComponent implements OnInit, DoCheck {
  checked: boolean;
  isVisibleReject: boolean;
  indeterminate: boolean;
  confirmModal?: NzModalRef;
  commentForm: FormGroup;
  isValid: boolean;
  description: string;
  searchObj: SearchLineObj = {
    name: null,
    fromDate: null,
    flagStatus: null,
    page: 0,
    size: 20,
    ids: [],
  };
  isLoadingMore: boolean;
  loadingPage: boolean;
  status = {
    null: 'Chưa có',
    '1': 'Đang hoạt động',
    '2': 'Đang chờ',
    '3': 'Từ chối',
    '4': 'Phê duyệt',
    '5': 'Tất cả',
  };
  pagination: Pagination = new Pagination(userConfig.pageSize);
  searchLineObj: SearchLineObj = {
    name: null,
    fromDate: null,
    flagStatus: null,
    page: 0,
    size: 30,
    ids: [],
  };
  lineList: Array<DataLineApprove> = [];
  initData: Array<DataLineApprove> = [];
  arrayId: Array<number> = [];
  setOfCheckedId = new Set<number>();

  constructor(
    injector: Injector,
    public lineService: LineService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(
      `FUNCTION_${FunctionCode.APPROVE_LINE}`
    );

    this.isVisibleReject = false;
    this.loadingPage = true;
    this.isLoadingMore = false;
    this.commentForm = new FormGroup({
      comment: new FormControl('', Validators.required),
    });
    this.checked = false;
    this.indeterminate = false;
    this.isValid= false;
    this.description = "";

  }

  ngOnInit(): void {
    this.search();
    this.onSearch();
   }

  ngDoCheck(): void {
    if (this.arrayId.length === this.initData.length && this.arrayId.length > 0) {
      this.checked = true;
    } else {
      this.checked = false;
    }
  }

  updateCheckedSet(id: number, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }

  handleCancelReject() {
    this.isVisibleReject = false;
  }

  viewDetail(id:number) {
    localStorage.setItem('selectedIdLine',String(id));
    this.router.navigateByUrl('/organization/line-group/manage');
  }

  onItemChecked(id: number, checked: boolean): void {
    if (checked === true && !this.setOfCheckedId.has(id)) {
      this.arrayId.push(id);
    } else if (this.setOfCheckedId.has(id)) {
      this.arrayId = this.arrayId.filter((element: number) => element !== id);
    }

    this.updateCheckedSet(id, checked);
  }

  onAllChecked(checked: boolean): void {
    this.initData.forEach((element) => {
      this.updateCheckedSet(element.id, checked);
      if (checked === false) {
        this.arrayId = [];
      } else {
        this.arrayId.push(element.id);
      }
    });
  }

  changePage() {
    this.loadingPage = true;
    this.searchObj.page = this.pagination.currentPage - 1;
    const objTmp = { ...this.searchObj }
    objTmp.fromDate = objTmp.fromDate ? moment(objTmp.fromDate).format('YYYY-MM-DD') : null;
    this.lineService.searchListLineViewPending(objTmp).subscribe(
      (data) => {
        this.initData = data.content;
        this.pagination.totalElements = data.totalElements;
        this.pagination.size = data.size;
        this.loadingPage = false;
      },
      (error) => {
        this.toastrCustom.error(error?.error.description);
      }
    );
  }

  onSearch() {
    const objTmp = { ...this.searchObj};
    objTmp.page = 0;
    objTmp.size = 99999;
    this.lineService.searchListLineViewPending(objTmp).subscribe((data) => {
      this.lineList = data.content;
      this.isLoading = false;
    });
  }

  loadMore() {
    this.isLoadingMore = true;
    this.searchLineObj.page = this.searchLineObj.page + 1;
    this.lineService.searchListLineViewPending(this.searchLineObj).subscribe((data) => {
      this.lineList = [...this.lineList, ...data.content];
      this.isLoadingMore = false;
    });
  }

  search() {
    this.loadingPage = true;
    this.searchObj.page = 0;
    const objTmp = { ...this.searchObj }
    objTmp.fromDate = objTmp.fromDate ? moment(objTmp.fromDate).format('YYYY-MM-DD') : null;

    this.lineService.searchListLineViewPending(objTmp).subscribe(
      (data: any) => {
        this.initData = data.content;
        this.pagination.totalElements = data.numberOfElements;
        this.pagination.size = data.size;
        this.loadingPage = false;
      },
      (error) => {
        this.toastrCustom.error(error?.error.description);
      }
    );
  }



  showConfirm(idLine:number, index:number): void {
    const message = 'Bạn có muốn xóa line?';
    this.confirmModal = this.modal.confirm({
      nzTitle: message,
      nzContent: '',
      nzOnOk: () => {
        this.lineService.deleteLine(idLine).subscribe(
          (data) => {
            this.toastrCustom.success('Xóa line thành công');
            this.initData.splice(index, 1);
            this.initData = [...this.initData];
          },
          (error) => {
            this.toastrCustom.error('Xóa line thất bại');
          }
        );
      },
    });
  }

  showRejectForm() {
    if (this.arrayId.length < 1) {
      this.toastrCustom.error('Vui lòng chọn line để thực hiện');
      return;
    }
    if (this.description === "") {
      this.isValid = true;
    } else {
      this.isValid = false;
      this.rejectLineAPI();
    }
  }

  handleOk() {
    if (!this.commentForm.valid) {
      Object.values(this.commentForm.controls).forEach((control) => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    } else {
      this.isVisibleReject = false;
      this.approveLine(false);
    }
  }

  approveLine(status:boolean) {
    if (this.arrayId.length < 1) {
      this.toastrCustom.error('Vui lòng chọn line để thực hiện');
      return;
    }
    this.loadingPage = true;
    this.lineService
      .approvedLine({
        ids: this.arrayId,
        description: this.description ? this.description : null,
      })
      .subscribe(
        (data) => {
          this.loadingPage = false;
          this.toastrCustom.success('Phê duyệt line thành công');
          this.onAllChecked(false);
          this.changePage();
          this.arrayId = [];
          this.searchObj.ids = [];
          this.isValid = false;
          this.description = "";
        },
        (error) => {
          this.loadingPage = false;
          this.toastrCustom.error('Phê duyệt thất bại');
        }
      );
  }
  rejectLineAPI() {
    this.loadingPage = true;
    this.lineService
      .rejectedLine({
        ids: this.arrayId,
        description: this.description,
      })
      .subscribe(
        (data) => {
          this.loadingPage = false;
          this.toastrCustom.success('Từ chối line thành công');
          this.onAllChecked(false);
          this.changePage();
          this.arrayId = [];
          this.description = "";
        },
        (error) => {
          this.loadingPage = false;
          this.toastrCustom.error('Từ chối thất bại');
        }
      );
  }

  goBackPage() {
    this.location.back();
  }
  onSelectLine(e:any){
    this.searchObj.ids = [...e.listOfSelected]
  }

  override triggerSearchEvent(): void {
      this.search();
  }
}
