import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApproveLineComponent } from './approve-line.component';

describe('ApproveLineComponent', () => {
  let component: ApproveLineComponent;
  let fixture: ComponentFixture<ApproveLineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApproveLineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApproveLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
