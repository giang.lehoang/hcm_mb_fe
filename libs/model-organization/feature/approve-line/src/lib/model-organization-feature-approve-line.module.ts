import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ApproveLineComponent} from "./approve-line/approve-line.component";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzCardModule} from "ng-zorro-antd/card";
import {SharedUiMbSelectCheckAbleModule} from "@hcm-mfe/shared/ui/mb-select-check-able";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import {NzModalModule} from "ng-zorro-antd/modal";
import {NzGridModule} from "ng-zorro-antd/grid";
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, SharedUiMbButtonModule, NzCardModule,
    RouterModule.forChild([
      {
        path: '',
        component: ApproveLineComponent,
      }]),
    SharedUiMbSelectCheckAbleModule, TranslateModule, SharedUiMbDatePickerModule, FormsModule,
    SharedUiMbInputTextModule, NzTableModule, NzPaginationModule, NzModalModule, ReactiveFormsModule, NzGridModule],
  declarations: [ApproveLineComponent],
  exports: [ApproveLineComponent]
})
export class ModelOrganizationFeatureApproveLineModule {}
