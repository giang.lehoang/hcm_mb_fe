import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CorrectionStaffComponent } from './correction-staff.component';

describe('CorrectionStaffComponent', () => {
  let component: CorrectionStaffComponent;
  let fixture: ComponentFixture<CorrectionStaffComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CorrectionStaffComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CorrectionStaffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
