import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SortTreeComponent } from './sort-tree.component';

describe('SortTreeComponent', () => {
  let component: SortTreeComponent;
  let fixture: ComponentFixture<SortTreeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SortTreeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SortTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
