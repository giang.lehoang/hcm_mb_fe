import {Component, Injector, OnInit} from '@angular/core';
import {CdkDragDrop} from '@angular/cdk/drag-drop';
import {OrganizationService} from "@hcm-mfe/model-organization/data-access/services";
import {
  OrganizationTreeNode,
  RequestOrgName,
  TreeSeqUpdate,
  OrganizationByName,
  MODE_DRAG
} from "@hcm-mfe/model-organization/data-access/models";
import {BaseComponent} from "../../../../../../shared/common/base-component/src";
import {Subscription} from "rxjs";
import { maxInt32 } from '@hcm-mfe/shared/common/constants';
import {FunctionCode} from "@hcm-mfe/shared/common/enums";

@Component({
  selector: 'app-sort-tree',
  templateUrl: './sort-tree.component.html',
  styleUrls: ['./sort-tree.component.scss']
})
export class SortTreeComponent extends BaseComponent implements OnInit {

  listTree: OrganizationTreeNode[] = [];
  mapChildren: Map<number, Array<OrganizationTreeNode>>;
  mapOfExpandedDataTree: { [key: string]: OrganizationTreeNode[] };
  mapChangeIndex: Map<number, OrganizationTreeNode>;
  orgByNames:OrganizationByName[] = [];
  idOrg = -1;
  loading:boolean;
  searching: boolean;
  request: Subscription | null;

  constructor(private readonly orgService:OrganizationService,
              injector:Injector) {
    super(injector);
    this.mapChangeIndex = new Map();
    this.mapChildren = new Map();
    this.mapOfExpandedDataTree = {};
    this.loading = false;
    this.request = null;
    this.searching = false;
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ORGANIZATION_SORT_TREE}`);
  }

  ngOnInit(): void {
    this.fetchData();
  }

  fetchData(){
    this.loading = true;
    this.orgService.getSubTree(true).subscribe(data => {
      this.listTree = data.data;
      this.mapOfExpandedDataTree['0'] = this.convertOrgTree(this.listTree);
      this.loading = false;
    }, error => {
      this.loading = false;
      this.toastrCustom.error(error.message);
    })
  }

  convertOrgTree(orgTree:OrganizationTreeNode[], parent?:OrganizationTreeNode):OrganizationTreeNode[]{
    for(let i = 0; i < orgTree.length; i++){
      orgTree[i].expand = false;
      orgTree[i].key = parent ?  `${parent?.key} - ${i}` : '0';
      orgTree[i].level = parent ? parent.level + 1 : 0;
      orgTree[i].parent = parent;
      orgTree[i].minChange = -1;
      orgTree[i].maxChange = 0;
      orgTree[i].lengthChildren = 0;
    }
    return orgTree;
  }

  collapseTree(index:number, data: OrganizationTreeNode, $event: boolean): void {
    if(!$event){
        data.expand = false;
        this.closeTree(data.orgId);
    } else if(!data.loadChildren) {
      this.loading = true;
      this.orgService.getSubTree(true ,data.orgId).subscribe(res => {
        data.loadChildren = true;
        const listChildren = this.convertOrgTree(res.data, data);
        this.mapChildren.set(data.orgId, res.data);
        data.lengthChildren = listChildren.length;
        this.mapOfExpandedDataTree['0'].splice(index + 1, 0, ...listChildren);
        this.loading = false;
      }, error => {
        this.loading = false;
        this.toastrCustom.error(error.message);
      })
    }
  }

  closeTree(orgId:number):void{
    const listData = this.mapChildren.get(orgId);
    if(listData){
      listData.forEach(item => {
        item.expand = false;
        this.closeTree(item.orgId);
      })
    }
  }

  drop(event: CdkDragDrop<string[]>): void {
    this.findRealIndex(event);
    const list = this.mapOfExpandedDataTree['0'];
    const item = list[event.previousIndex];

    if(item.parent){
      const indexOfParent = list.indexOf(item.parent);
      const mode = this.checkCorrectLocation(event, indexOfParent);
      if(mode === MODE_DRAG.OUTSIDE_MIN){
        this.moveOutsideMin(item, indexOfParent, event);
      } else if(mode === MODE_DRAG.INSIDE) {
        this.moveInside(item, indexOfParent, event, list);
      } else if(mode === MODE_DRAG.OUTSIDE_MAX) {
        this.moveItem(event.previousIndex, indexOfParent + item.parent.lengthChildren, item.level);
        if(item.parent.lengthChildren > item.parent.maxChange){
          item.parent.maxChange = item.parent.lengthChildren;
        }
        if((event.previousIndex < item.parent.minChange) || item.parent.minChange === -1){
          item.parent.minChange = event.previousIndex;
        }

      }

      if(!this.mapChangeIndex.has(item.parent.orgId)){
        this.mapChangeIndex.set(item.parent.orgId, item.parent);
      }

    } else {return}
  }

  moveOutsideMin(item:OrganizationTreeNode, indexOfParent:number, event:CdkDragDrop<string[]>){
    if(item.parent){
      this.moveItem(event.previousIndex, indexOfParent + 1, item.level);
      if(event.previousIndex - indexOfParent > item.parent.maxChange){
        item.parent.maxChange = event.previousIndex - indexOfParent;
      }

      if((event.currentIndex - indexOfParent < item.parent.minChange) || item.parent.minChange === -1){
        item.parent.minChange = 1;
      }
    }
  }

  moveInside(item:OrganizationTreeNode, indexOfParent:number, event:CdkDragDrop<string[]>, list:OrganizationTreeNode[]){
    if(item.parent){
      if(event.currentIndex > event.previousIndex){
        if(list[event.currentIndex].lengthChildren){
          this.caculateMaxlength(event.currentIndex, list);
          event.currentIndex += list[event.currentIndex].lengthChildren;
        }
      }

      this.moveItem(event.previousIndex, event.currentIndex, item.level);
      let max = event.currentIndex > event.previousIndex ? event.currentIndex : event.previousIndex;
      let min = event.currentIndex > event.previousIndex ? event.previousIndex : event.currentIndex;
      if(max - indexOfParent > item.parent.maxChange){
        item.parent.maxChange = max - indexOfParent;
      }

      if((min - indexOfParent < item.parent.minChange) || item.parent.minChange === -1){
        item.parent.minChange = min - indexOfParent;
      }
    }
  }

  findRealIndex(event:CdkDragDrop<string[]>){
    const list = this.mapOfExpandedDataTree['0'];
    let count = 0;
    let pre = 0;
    let cur = 0;
    for (let i = 0; i < list.length; i++){

      if (count === event.previousIndex){
        pre = i;
      }

      if(count === event.currentIndex){
        cur = i
      }

      if((list[i].parent && list[i].parent?.expand) || !list[i].parent){
        count++;
      }
    }
    event.currentIndex = cur;
    event.previousIndex = pre;
  }

  checkCorrectLocation(event: CdkDragDrop<string[]>, parentIndex:number):number{
    const list = this.mapOfExpandedDataTree['0'];
    const item = list[event.previousIndex];
    const itemNext = list[event.currentIndex];
    if(item.parent === itemNext.parent){
      return MODE_DRAG.INSIDE;
    } else {
      if(itemNext.level > item.level){
        return MODE_DRAG.NOTHING;
      }
      if(event.currentIndex < event.previousIndex){
        return MODE_DRAG.OUTSIDE_MIN;
      } else {
        this.caculateMaxlength(parentIndex, list);
        return MODE_DRAG.OUTSIDE_MAX
      }
    }
  }

  caculateMaxlength(index:number, list:OrganizationTreeNode[]){
    const parent = list[index];
    index++;
    let count = 0;
    while (list[index].level !== parent.level && index < list.length){
      count++;
      index++;
    }
    parent.lengthChildren = count;
  }

  moveItem(oldIndex:number, newIndex:number, level:number){
    const list = this.mapOfExpandedDataTree['0'];
    let count = 0;
    const stack = [];
    stack.push(list[oldIndex]);
    list.splice(oldIndex, 1);
    while (list[oldIndex]?.level > level && oldIndex < list.length - 1){
      count++;
      stack.push(list[oldIndex]);
      list.splice(oldIndex, 1);
    }
    if(newIndex > oldIndex){
      newIndex -= count;
    }
    list.splice(newIndex, 0, ...stack);
  }


  buildRequest():void{
    const rq:TreeSeqUpdate[] = [];
    const list = this.mapOfExpandedDataTree['0'];
    this.mapChangeIndex.forEach((value) => {
      const index = list.indexOf(value) + 1;
      let count = value.minChange;
      for(let i = value.minChange - 1; i < value.maxChange; i++){
        if(list[index + i].parent === value){
          rq.push({
            orgId: list[index + i].orgId,
            displaySeq: count
          });
          count++;
        }
      }
    });
    this.loading = true;
    this.orgService.updateTreeSeq(rq).subscribe(value => {
      this.loading = false;
      this.toastrCustom.success();
      this.fetchData();
    }, error => {
      this.loading = false;
      this.toastrCustom.error(error.message);
    })
  }

  trackBy(index:number, item:OrganizationTreeNode){
    return item.orgId;
  }

  searchOrgName($event: string) {
    if(!$event){
      return;
    }

    if(this.request){
      this.request.unsubscribe();
    }
    const param:RequestOrgName = {
      name: $event,
      is_author: false,
      size: maxInt32
    }

    this.searching = true;
    this.request = this.orgService.getOrganizationTreeByNameL(param).subscribe(data => {
      this.request = null;
      this.searching = false;
      this.orgByNames = data.content;
      this.orgByNames.forEach((item) => {
        const arrPath = item.pathName?.split('-');
        if(arrPath){
          item.orgName = `${arrPath.pop()} - ${item.orgName}`;
        }
      });
    }, error => {
      this.searching = false;
      this.request = null;
    })
  }

  changeValue() {
    if(!this.idOrg){
      this.fetchData();
      return;
    }

    for(const item of this.orgByNames){
      if(item.orgId === this.idOrg){
        item.loadChildren = false;
        this.listTree = [item];
        this.mapOfExpandedDataTree['0'] = this.convertOrgTree(this.listTree);
        this.mapChangeIndex.clear();
      }
    }
  }
}
