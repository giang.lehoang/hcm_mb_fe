import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SortTreeComponent} from "./sort-tree/sort-tree.component";
import {NzTableModule} from "ng-zorro-antd/table";
import {RouterModule} from "@angular/router";
import { DragDropModule } from '@angular/cdk/drag-drop';
import {NzGridModule} from "ng-zorro-antd/grid";
import {SharedUiMbInputTextModule} from "../../../../../shared/ui/mb-input-text/src";
import {FormsModule} from "@angular/forms";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbButtonModule} from "../../../../../shared/ui/mb-button/src";
import {SharedUiLoadingModule} from "../../../../../shared/ui/loading/src";
import {SharedUiMbSelectModule} from "../../../../../shared/ui/mb-select/src";

@NgModule({
  imports: [CommonModule, NzTableModule, RouterModule.forChild([
    {
      path: '',
      component: SortTreeComponent,
    }]), DragDropModule, NzGridModule, SharedUiMbInputTextModule, FormsModule, TranslateModule, SharedUiMbButtonModule, SharedUiLoadingModule, SharedUiMbSelectModule],
  declarations: [SortTreeComponent],
  exports: [SortTreeComponent]
})
export class ModelOrganizationFeatureSortTreeModule {}
