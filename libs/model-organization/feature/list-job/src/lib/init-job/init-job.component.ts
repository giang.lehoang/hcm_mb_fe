import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, EventEmitter, Injector, Input, OnInit, Output } from '@angular/core';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {JobShare} from "@hcm-mfe/model-organization/data-access/models";
import {JobService} from "@hcm-mfe/model-organization/data-access/services";


@Component({
  selector: 'app-init-job',
  templateUrl: './init-job.component.html',
  styleUrls: ['./init-job.component.scss'],
})
export class InitJobComponent extends BaseComponent implements OnInit {
  title = '';
  jobForm: FormGroup;
  disable: boolean;
  disabeJobType: boolean
  disableFlagStatus: boolean;
  disableBtn: boolean
  @Output() closeEvent = new EventEmitter<boolean>();
  @Input() jobShare:JobShare | undefined;

  constructor(injector: Injector, readonly jobService: JobService) {
    super(injector);
    this.disable = false;
    this.disabeJobType= false;
    this.disableFlagStatus= false;
    this.disableBtn= false;
    this.jobForm = new FormGroup({
      jobName: new FormControl('', Validators.required),
      flagStatus: new FormControl('', Validators.required),
      description: new FormControl(''),
      jobType: new FormControl('', Validators.required),
    });
  }

  ngOnInit(): void {

    if (this.jobShare?.action) {
      const job = this.jobShare.job
      if(job?.flagStatus === null){
        job.flagStatus = ''
      }
      this.jobForm.controls['jobName'].setValue(job?.jobName ? job.jobName : '');
          this.jobForm.controls['flagStatus'].setValue(String(job?.flagStatus));
          this.jobForm.controls['description'].setValue(job?.jobDescription ? job.jobDescription : '');
          this.jobForm.controls['jobType'].setValue(job?.jobType ? job.jobType : '');
      if (this.jobShare.action === 'edit') {
        this.title = 'Cập nhật job';
      } else {
        this.disable = true;
        this.jobForm.controls['jobName'].disable();
        this.jobForm.controls['description'].disable();
        this.title = 'View job';
      }
    } else {
      this.title = 'Thêm mới job';
      this.disableFlagStatus = true;
      if (this.jobShare?.type === "TM") {
        this.disabeJobType = true;
        this.jobForm.controls['jobType'].setValue('ORG');
      }
      this.jobForm.controls['flagStatus'].setValue('1');
    }
  }


  closeModal(status?: boolean) {
    this.closeEvent.emit(status ? status : false)
  }

  validateRequest() {

    this.jobForm.controls['jobName'].setValue(this.jobForm.controls['jobName'].value.trim());
    this.jobForm.controls['description'].setValue(this.jobForm.controls['description'].value.trim());
    if (!this.jobForm.valid) {
      Object.values(this.jobForm.controls).forEach((control) => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
      return;
    }
    const count = new Blob([this.jobForm.controls['description'].value.trim()]).size;
    if(count > 250 ){
      this.toastrCustom.error('Mô tả quá dài');
      return;
    }

    const id = this.message.loading('Action in progress..', { nzDuration: 0 }).messageId;
    this.disableBtn = true;
    if (this.jobShare?.action) {
      const request = this.jobForm.value;
      request.jobId = this.jobShare.job?.jobId;
      this.jobService.updateJob(request).subscribe(
        (data) => {
          this.message.remove(id);
          this.toastrCustom.success('Sửa thành công');
          this.closeModal(true)
        },
        (error) => {
          this.message.remove(id);
          this.toastrCustom.error(error.error?.message);
          this.disableBtn = false;
        }
      );
    } else {
      this.jobService.createJob(this.jobForm.value).subscribe(
        (data) => {
          this.message.remove(id);
          this.toastrCustom.success('Lưu thành công');
          this.closeModal(true)
        },
        (error) => {
          this.message.remove();
          this.toastrCustom.error(error.error?.message);
          this.disableBtn = false;
        }
      );
    }
  }
}
