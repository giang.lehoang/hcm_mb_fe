import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {ListJobComponent} from "./list-job/list-job.component";
import {InitJobComponent} from "./init-job/init-job.component";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {NzModalModule} from "ng-zorro-antd/modal";
import {NzTagModule} from "ng-zorro-antd/tag";
import {NzTableModule} from "ng-zorro-antd/table";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ModelOrganizationPipesTransformJobTypeModule} from "@hcm-mfe/model-organization/pipes/transform-job-type";
import {NzCardModule} from "ng-zorro-antd/card";
import {NzFormModule} from "ng-zorro-antd/form";
import {NzSelectModule} from "ng-zorro-antd/select";
import {RouterModule} from "@angular/router";
import {TranslateModule} from "@ngx-translate/core";
import {NzInputModule} from "ng-zorro-antd/input";
import {NzGridModule} from "ng-zorro-antd/grid";
import { SharedDirectivesAutofocusModule } from '@hcm-mfe/shared/directives/autofocus';

@NgModule({
  imports: [CommonModule, NzToolTipModule, TranslateModule, NzInputModule, NzGridModule,
    RouterModule.forChild([
      {
        path: '',
        component: ListJobComponent
      }]),
    NzPaginationModule, NzDropDownModule, NzModalModule, NzTagModule, NzTableModule, SharedUiMbButtonModule,
    SharedUiMbSelectModule, SharedUiMbInputTextModule, SharedUiLoadingModule, FormsModule, ModelOrganizationPipesTransformJobTypeModule,
    NzCardModule, ReactiveFormsModule, NzFormModule, NzSelectModule, SharedDirectivesAutofocusModule],
  declarations: [ListJobComponent, InitJobComponent],
  exports: [ListJobComponent, InitJobComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ModelOrganizationFeatureListJobModule {}
