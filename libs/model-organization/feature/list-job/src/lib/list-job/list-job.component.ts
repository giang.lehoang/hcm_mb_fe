import { Component, Injector, OnInit } from '@angular/core';
import * as FileSaver from 'file-saver';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {
  JobShare,
  listJobType,
  Pagination,
  SearchJob, JobView, listStatusJob
} from "@hcm-mfe/model-organization/data-access/models";
import {userConfig} from "@hcm-mfe/shared/common/constants";
import { FunctionCode } from '@hcm-mfe/shared/common/enums';
import { SearchService } from '@hcm-mfe/shared/core';
import {JobService} from "@hcm-mfe/model-organization/data-access/services";
import {renderFlagStatus} from "../../../../../helper/common.function";

@Component({
  selector: 'app-list-job',
  templateUrl: './list-job.component.html',
  styleUrls: ['./list-job.component.scss'],
})
export class ListJobComponent extends BaseComponent implements OnInit {
  jobDataTable: SearchJob[] = [];
  pagination: Pagination = new Pagination(userConfig.pageSize);
  isLoadingPage: boolean;
  objSearch: SearchJob;
  params: SearchJob;
  isVisible: boolean;
  jobShare: JobShare;
  listJobSearch:JobView[] = [];
  listJob:JobView[] = [];
  listJobType  = listJobType;
  listStatus = listStatusJob;
  renderFlagStatus = renderFlagStatus;

  constructor(readonly jobService: JobService, injector: Injector,
    readonly searchService: SearchService,

  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(
      `FUNCTION_${FunctionCode.LIST_JOB}`
    );
    console.log(this.objFunction)
    this.isLoadingPage = false;
    this.objSearch = {
      jobCode: '',
      jobName: '',
      jobType: '',
      flagStatus: '1',
    };

    this.jobShare = { action: '', ...this.objSearch }

    this.params = {
      page: 0,
      size: 15,
      jobCode: '',
      jobName: '',
      jobType: '',
      flagStatus: '',
    };
    this.isVisible = false
  }

  ngOnInit(): void {
    this.onSearch();
    this.getAllJob();
  }

  changePage() {
    this.params.page = this.pagination.currentPage - 1;
    this.callJobFilter();
  }

  callJobFilter() {
    this.isLoadingPage = true;
    this.jobService.filterJobs(this.params).subscribe(
      (data) => {
        this.isLoadingPage = false;
        this.jobDataTable = data.data.content;
        this.pagination.totalElements = data.data.totalElements;
        this.pagination.totalPages = data.data.totalPages;
        this.pagination.numberOfElements = data.data.numberOfElements
      },
      (error) => {
        this.message.error(error.error.message);
        this.isLoadingPage = false;
        this.jobDataTable = [];
      }
    );
  }

  handleCancel() {
    this.isVisible = false
  }

  onSearch() {
    this.params = {
      page: 0,
      size: 15,
      jobCode: this.objSearch.jobCode ? this.objSearch.jobCode : '',
      jobName: this.objSearch.jobName ? this.objSearch.jobName : '',
      jobType: this.objSearch.jobType ? this.objSearch.jobType : '',
      flagStatus: this.objSearch.flagStatus ? this.objSearch.flagStatus : '',
    };
    this.pagination.currentPage = 1;
    this.callJobFilter();
  }

  deleteJob(id:number) {
      this.isLoadingPage = true;
      this.jobService.checkJobUsed(id).subscribe((res) => {
          this.isLoadingPage = false;
          if (res && res.data) {
            this.toastrCustom.error(this.translate.instant('modelOrganization.listJob.notification.usedNotallowedDelete'));
          } else {
            this.deletePopup.showModal(() => this.showConfirm(id));
          }
        },
        (error) => {
          this.isLoadingPage = false;
          this.message.error(error.error.message);
        }
      );


  }

  createJob() {
    this.jobShare.action = '';
    this.jobShare.job = {};
    this.isVisible = true
  }

  navigatePage(action:string, item:SearchJob) {
    if (action === 'edit' && !this.objFunction.edit) {
      return;
    }
    this.jobShare.action = action;
    this.jobShare.job = item
    this.isVisible = true
  }

  exportExcel() {
    this.jobService.exportExcel(this.objSearch).subscribe(
      (data: any) => {
        const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
        FileSaver.saveAs(blob, 'Danh sach job.xlsx');
      },
      (error) => {
        this.message.error(error.error.message);
      }
    );
  }

  showConfirm(id:number): void {
    this.jobService.deleteJob(id).subscribe(
      (data) => {
        this.toastrCustom.success(this.translate.instant('modelOrganization.listJob.notification.deleteSuccess'));
        this.callJobFilter();
      },
      (error) => {
        this.toastrCustom.error(this.translate.instant('modelOrganization.listJob.notification.deleteFail'));
      }
    );
  }

  closeModal(value:boolean) {
    this.isVisible = false
    this.callJobFilter()
  }
  getAllJob() {
    this.objSearch.jobType = this.objSearch.jobType ? this.objSearch.jobType : ''
    this.jobService.getListJob(this.objSearch.jobType).subscribe(
      (data) => {
        this.listJob = data.data;
      },
      (error) => {
        this.toastrCustom.error(error.message);
      }
    );
  }
  onSearchJob(e: KeyboardEvent) {
    this.listJobSearch = [];
    const value = (e.target as HTMLTextAreaElement).value;
    if (value) {
      if(!this.objSearch.jobType) {
        this.listJob.forEach(item => {
          if (this.searchService.replaceUnicode(item.jobName.toLowerCase()).includes(this.searchService.replaceUnicode(value.toLowerCase()))) {
            this.listJobSearch.push(item);
          }
        });
      } else {
        this.listJob.filter(job => job.jobType === this.objSearch.jobType)
          .forEach(item => {
          if (this.searchService.replaceUnicode(item.jobName.toLowerCase()).includes(this.searchService.replaceUnicode(value.toLowerCase()))) {
            this.listJobSearch.push(item);
          }
        });
      }
    } else {
      this.listJobSearch = [];
    }
  }
  onClickJob(value:string) {
    this.objSearch.jobName = value;
  }

  override triggerSearchEvent(): void {
      this.onSearch();
  }
}
