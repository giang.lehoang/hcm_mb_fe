export const REGEX_NUMBER = /^[0-9]+/
export const MB_BANK = "MB Bank";
export const QUANTITY_DEFAULT = '0';
export const REGEX_NUMBER_NEGATIVE = /-|^[0-9]+/;
export const SUCCESS = "Thành công";
export const TYPE_JOB = "ORG";
export  const KNHS = "Khối ngân hàng số"
export const listOrgLevel = [
    { id: 2, value: 'Đơn vị quản lý' },
    { id: 3, value: 'Đơn vị trực thuộc 1' },
    { id: 4, value: 'Đơn vị trực thuộc 2' },
    { id: 5, value: 'Đơn vị trực thuộc 3' },
  ];
