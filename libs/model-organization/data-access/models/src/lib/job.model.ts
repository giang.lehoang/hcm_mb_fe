import {BaseSelect} from "./common.model";
import {userConfig} from "@hcm-mfe/shared/common/constants";

export interface SearchJob extends JobResponse{
  page?: number | null;
  size?: number | null;
}

export interface JobResponse {
  jobId?: number,
  jobDescription?: string,
  jobCode?: string,
  jobName: string,
  jobType: string,
  flagStatus?: string | number | null,
  isAssign?: boolean,
}

export interface JobView {
  jobName:string;
  jobType:string;
}

export interface JobShare {
  action?: string | null,
  job?: SearchJob | Record<string, never> | null,
  type?: string
}

export interface ApproveJob {
  lstPosDTO: {id:number}[],
  description: string,
}

export interface JobCreate {
  jobName: string,
  flagStatus: string | number,
  description: string,
  jobType: string,
  jobId?:string
}

export interface JobSearch {
  dataType: string,
  page: number,
  size: number,
  dataName?: string,
  description?: string,
  issuedDate?: string
}

export interface JobAssignRes {
  flagStatus: number;
  jobName: string;
  jobType: string;
  orgName: string;
  posDesc: null;
  posId: number;
}

export interface JobAssignManage {
  code: string;
  displayName: string;
  iconUri: string;
  id: string;
  indexNumber: number;
  isMenu: string;
  name: string;
  parentId: string;
  type: string;
  uri: string;
}

export interface OptionSelect {
  value: string,
  label: string
}

export class PositionGroupSearch {
  id:string | null;
  name:string;
  status?:number;
  type:string;
  page:number;
  size:number;
  constructor(
  ) {
    this.id = null;
    this.name = "";
    this.type = "";
    this.page = 0;
    this.size = 15
  }
}

export interface PositionGroup {
  fromDate: string;
  name: string;
  toDate: string;
  id: number
  parentName: string;
  parentId: number;
  type?: string;
  flagStatus?: number
}

export interface RequestPosition {
  id?: number,
  pgrId: number | null,
  pgrName: string,
  type?: string,
  fromDate?: string,
  toDate?: string,
  description?: string,
  parentId?: number,
  name?: string,
  status?: number;
}
export interface PositionDetail {
  pgrId: number,
  name: string,
  parentName: string,
  fromDate: string,
  toDate: string,
  lstPositionHO: Position[],
  lstOrgDTOHOs: PositionTable[],
  lstJobDTOHOs: Job[] | JobLine[],
  lstPositionCN: Position[],
  lstOrgDTOCNs: PositionTable[],
  lstJobDTOCNs: Job[] | JobLine[]
}

export interface Position {
  id: number,
  orgId: number,
  jobId: number
}


export interface Job {
  id?: number,
  posId?: number,
  jobId: number,
  jobName: string,
  posIds?: number[],
  jobType?: string,
  flagStatus?: string,
  value?:string
}

export interface JobLine extends BaseSelect{
  posId: number,
  jobId: number,
  jobName: string,
  posIds: number[],
  assign:string
}

export interface JobPosition {
  flagStatus?: number | string;
  jobId: number;
  jobName: string;
  jobType: string;
  orgName: string;
  posId: number;
}

export interface PositionTable {
  type?: string;
  nameBranch?: string;
  orgGroup?: string;
  orgId: number | null;
  orgName?: string;
  fullName?: string;
  orgType?: string;
  parentId?: number;
  pathName?: string;
  isVisible?: boolean;
  jobs: JobLine[],
  jobSelect: JobLine[],
  loadJob?: boolean,
}


export interface AssignJob {
  jobId: number | string | undefined,
  attrType: string,
  orgIds: number[],
}


export interface TypePosition {
  lcoId: number,
  meaning: string,
}

export class SearchModel {
  orgId = '';
  jobId = '';
  status = 1;
  type = 'POSITION';
  page = 0;
  size = 15;
}

export interface ListPositionModel {
  flagStatus: number;
  isHead: string;
  jobId: number;
  jobName: string;
  lastName: string;
  orgId: number;
  orgaValue: string;
  pathId: string;
  posId: number;
  isDisable: boolean;
  inputType:string;
  position: Array<ListPositionModel>;
}

export const listJobType  = [
  {value:'ORG', name:'MH cứng'},
  {value:'PRJ', name:'MH dự án'},
  {value:'DV', name:'MH dịch vụ'},
];
export const listStatusJob = [
  {value: '1', name:'Sử dụng'},
  {value: '0', name:'Không sử dụng'}
]

export class SearchPosModel {
  orgId: string;
  jobId: string;
  status: number | null;
  type: string;
  page: number;
  size: number;

  constructor() {
    this.orgId = '';
    this.jobId = '';
    this.status = 1;
    this.type = 'POSITION';
    this.page = 0;
    this.size = 15;
  }
}


export const listStatusPosition = [
  { id: 7, value: 'Chưa tạo' },
  { id: 5, value: 'Tạo mới' },
  { id: 2, value: 'Chờ duyệt' },
  { id: 3, value: 'Từ chối' },
  { id: 4, value: 'Phê duyệt' },
  { id: 1, value: 'Đang hoạt động' },
  { id: 0, value: 'Không hoạt động' },
  { id: 6, value: 'Xác nhận' },
];

export interface SavePosRes {
  posId: number,
    isHead: string
}

export const typePosition = [
  { id: 'POSITION', value: 'Position' },
  { id: 'POSITION DETAIL', value: 'Chi tiết Position' },
];

const typeOrg = ["orgId", "orgName", "orgLevel","subs"] as const;
export type OrgType = typeof typeOrg[number];

export interface AssignPosRes {
  positionGroupId: number | null | string,
  positionIds: number[]
  deletePositionIds?:Array<number|null>;
}

export interface PosNunAssign {
  id?:number;
  posId: number,
  orgId: number,
  jobId: number,
  fullName: string,
  jobName: string,
  pgrId: number,
  pgrType: string,
  flagStatus: number,
  pgrName: string,
  lastUpdateDate: string
}

export interface PosUnAssignDetail {
  pgrType: string,
  orgId: number,
  pgrName: string,
  parentId: number,
  pgrId?:number,
  type: "UNASSIGN" | "ASSIGN"
}

export interface PosUnAssignDetailResponse {
  posId: number,
  orgId: number,
  lstJobHO: JobPosUnAssign[] | null,
  lstJobCN: JobPosUnAssign[] | null ,
  pgrType: string,
  pgrName: null | string,
  orgLevel: number
}

export interface PosUnAssignTable {
  orgId: number,
  orgName: string,
  listAllJob: JobPosUnAssign[],
  listSelectJob: JobPosUnAssign[],
  isVisible: boolean
}

export interface JobPosUnAssign {
  jobId: number;
  jobName: string;
  orgId: number;
  orgLevel: string;
  pgrName: string;
  pgrType: string;
  posId: number
}

export class PosNunAssignRequest {
  orgId: number | string;
  jobId: number | string;
  type: "ASSIGN" | "UNASSIGN";
  page: number;
  size: number;

  constructor() {
    this.orgId = '';
    this.jobId = '';
    this.type = "UNASSIGN";
    this.page = 0;
    this.size = userConfig.pageSize
  }
}

export interface AssignPosRequest {
  pgrId: number,
  pgrName: string,
  pgrType: string,
  posId: number[],
  type: "ASSIGN" | "UNASSIGN"
}
