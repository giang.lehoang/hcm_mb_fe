export interface ResponseEntity<T> {
  data: T;
  content: T;
  size: number;
  totalElements: number;
  totalPages: number;
  numberOfElements: number;
  number: number;
  pageable: {
    pageNumber: number;
  };
}

export interface ResponseEntityData<T> {
  data: ResponseEntity<T>;
}

export class Pagination {
  constructor(pageSize:number) {
    this.pageSize = pageSize;
    this.size = pageSize;
  }
  currentPage: number = 1;
  size? = 0;
  totalPages? = 0;
  pageSize? = 0;
  pageNumber? = 0;
  numberOfElements? = 0;
  totalElements?= 0;
  result?:number = 0;
  numCaculate?:number = 0;
  min?:number = 0;
  max?:number = 0;
  totalElement?: number = 0;
  totalPage?: number = 0;
  total?:number= 0;
  pageIndex?: number = 0;
}

export interface PaginationInterface {
  currentPage: number,
  totalElement: number,
  size?: number,
  totalPage: number,
  numberOfElements: number;
  numCaculate?: number;
  min?:number;
  max?:number;
}

export interface RequestFileInfo {
  file: File | null;
  url: string;
  service?:string
}

export interface SelectObject<T, V> {
  key?: T,
  id?: T,
  value: V
}

export interface LookupValue {
  lvaMean: string;
  lvaId: number;
  lvaValue: string;
  parentValue: string;
  criteriaId: number;
  attribute: string;
  criteria: string;
  flagStatus: number;
  lcoCode: string;
  lcoId: number;
  regionMean: string;
  orgId : number,
  branchType : string
}

export const LOOKUP_CODE = {
  JOB_GROUP : 'NHOM_CONG_VIEC',
  CARRER_GROUP: 'NHOM_NGHE_NGHIEP',
  ROLE: 'VAI_TRO_NANG_LUC'
}

export interface ResponseError {
  error: MessageError
}

export interface MessageError {
  message: string
}
export const RegionType = {
  KV: 'KHU_VUC',
  TV: 'TIEU_VUNG',
  CN: 'CHI_NHANH',
  LH: 'LOAI_HINH',
  LB: 'TX8',
  NCV: 'NHOM_CONG_VIEC',
  NNN: 'NHOM_NGHE_NGHIEP'
};

export interface BaseSelect {
  id: number,
  value: string,
  name?:string
}

export interface ApproveRequest {
  ids: number[],
  description: string | null,
}

export const ScreenType = {
  ADJUSTED: 'DC',
  CORRECTION: 'HC',
};

export class FileInfo {
  fileName:string;
  file: File | null;
  docId: number | null;
  hasFile: boolean;

  constructor() {
    this.fileName = '';
    this.file = null;
    this.docId = null;
    this.hasFile = false;
  }
}

