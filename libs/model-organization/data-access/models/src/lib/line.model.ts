import * as moment from "moment/moment";
import {Job} from "@hcm-mfe/model-organization/data-access/models";
import {NzTreeNodeOptions} from "ng-zorro-antd/core/tree/nz-tree-base-node";

export interface SearchLineObj {
  flagStatus: number | null;
  fromDate: string | null;
  page: number;
  size: number;
  ids: Array<string>;
  pgrName?:string | null;
  name?:string | null;
}

export interface DataLine {
  content: string;
  createDate: string;
  flagStatus: number;
  fromDate: string;
  pgrId: number;
  pgrName: string;
  toDate: string;
  listOfSelected: string[]
}

export interface DataLineApprove extends DataLine{
  id:number;
  name:string;
}

export interface DataLineInit {
  effectiveDate: string;
  expirationDate: string;
  nameLine: string;
  nameManagerLine: string;
  parentId?: number | string;
  pgrId: string,
}


export interface SearchLineInit {
  name: string;
  page: number | null;
  size: number | null;
}

export interface SearchLineInitList {
  flagStatus?: number;
  id: number | string;
  name: string;
}

export interface LineInit {
  id?: number | string,
  parentId: number,
  name: string,
  fromDate: string | null,
  toDate: string | null,
}

export interface ManageLine {
  id: number;
  name: string;
  parentId: number;
  pathName: string;
}

export interface ObjectLineDetail {
  fromDate: string;
  organizationLineDTOS: Array<ArrayOrganizationLineDTOS>;
  parentId: string;
  parentName: string;
  pgrId: string;
  pgrName: string;
  toDate: string;
  jobs: Array<ArrayPositionLineDTOS>
}

export interface ArrayOrganizationLineDTOS {
  jobSelect: Job[];
  jobs: Job[];
  orgGroup: string;
  orgId: number;
  orgName: string;
  pathName?:string;
  positionLineDTOS: Array<ArrayPositionLineDTOS>;
  loadJob?: boolean;
  isVisible?: boolean;
}

export interface ArrayPositionLineDTOS {
  posId: number;
  jobId: number;
  jobName: string;
}

export interface ParamsLine {
  name: string,
  orgGroup: string,
};

export interface TreeLine extends NzTreeNodeOptions {
  expanded: boolean,
  id: number,
  name: string,
  parentId: null | number,
  subs: TreeLine[] | undefined
}

export interface JobByOrg {
  orgId: number,
  type: string,
  name: string
}




