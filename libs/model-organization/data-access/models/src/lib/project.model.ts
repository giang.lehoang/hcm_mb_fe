import { FormControl } from '@angular/forms';
import {LookupValue} from "./common.model";
export interface Criteria {
  meaning: string;
  flagStatus: number;
  value: string;
  parentValue: string;
  subs: Criteria[] | null;
  id: number | null;
  validate?: FormControl;
}


export interface CriteriaSearch {
  criteria: string | null;
  attribute: string | null;
  page: number | null;
  size: number | null;
}

export interface CriteriaDetail {
  meaning: string;
  flagStatus: number;
  value: string;
  parentValue: string;
  subs: CriteriaDetail[];
  id: number;
}


export interface RequestCriteria {
  id?: number | null;
  meaning?: string;
  lvaMean?: string;
  flagStatus: number;
  subs?: RequestCriteria[];
}

export interface ObjectSearchProject {
  orgId?: number;
  name?: string;
  fromDate?: string;
  page: number;
  size: number;
  ids?: Array<string>;
  status?: number;
  jobId?: number;
  flagStatus?: string;
  inputType?: string;
  year?: number;
  date?: string;
}

export interface AssignEmployee {
  id: number;
  job: string;
  name: string;
  idEm: string;
  orgName: string;
  fromDate: string;
  toDate: string;
  density: number;
}

export interface SearchParam {
  prjId: string;
  prjName: string;
  page: number;
  size: number;
}


export interface ProjectMemberRequest {
  pjmId: number;
  prjId: number;
  empId?: number;
  isMaster?: string;
  fromDate: string;
  toDate: string;
  scale: number;
  jobId: number;
  isDeleted: boolean;
  pm: boolean;
  empName: string;
}

export interface ProjectInfo {
  prjId?: number;
  prjName?: string;
  empNum?: number;
  fromDate?: number;
  toDate?: number | null;
}

//


export interface Employee {
  organization_id?: number;
  full_name: string;
  employee_id: number;
  nick_name: string;
  employee_code: string;
  late_name: string;
  valueShow?: string;
}

export interface ProjectAssignRequest {
  pjmId: number | null;
  prjId: number | undefined;
  empId: number;
  isMaster: string;
  fromDate: string;
  toDate: string;
  scale: number;
  jobId: number;
  isDeleted: boolean | undefined;
}

export interface ProjectDetail {
  pjmId: number;
  empId: number;
  employeeCode: string;
  employeeId: number;
  flagStatus: number;
  formDate: string;
  fullName: string;
  isMaster: string;
  jobId: number;
  jobName: string;
  lateName: string;
  nickName: string;
  orgId: number;
  prjId: number;
  prjName: string;
  scale: number;
  toDate: string;
  treeLevel: number;
}


export interface EmployeeError {
  empId: number;
  scale: string;
  empName: string;
  jobId: number;
}

export interface ProjectDetailResponse {
  description: string;
  document: {docId: string, fileName: string};
  flagStatus: number;
  fromDate: string;
  organizations: PJOrg[];
  prjCode: null | string;
  prjId: number;
  prjName: string;
  projectAttributes: PJAtb[]
  projectPositions: PJPos[]
  scopeContents: null
  sheetApproval: {sapId: number, sapNo: string}
  toDate: string
}

export interface PJOrg {
  orgId: number,
  orgName: string
}

export interface PJPos {
  pjpId: number,
  jobId: number,
  jobName: string,
  headCount: number
}
export interface PJAtb {
  attrCode: string;
  attrCodeMean: string;
  attrValue: string;
  attrValueMean: string;
  pjaId: number;
}

export interface CriteriaRes {
  lvaId: number;
  lvaValue: string;
  lvaMean: string;
  selectedValue?:string;
  attributes: Criteria[]
}

export interface SearchLinePRJ {
  prjAttributeCode:string | null,
  listPrjAttributeValues:LookupValue[],
  prjAttributeValues?:string | null
}

export interface SearchPRJ {
  prjId?: string;
  prjName?: string | null,
  orgName?: string | null,
  flagStatus?: number | null,
  page?: number,
  size?: number,
  attrs?: any[] | null,
  description?: string | null
}

export interface PRJResponse {
  content: string;
  flagStatus: number;
  orgNames: string[];
  orgNamesE: string | null;
  prjId: number;
  prjName: string;
  statusE: string | null
}

export interface SearchPRJImpl {
  prjId: string;
  prjName?: string;
  orgName?:string
  page?: number;
  size?: number;
}

export interface PRJImplResponse {
  prjId: number;
  prjName: string;
  countEmp: number;
  fromDate: string;
  fullName: string;
  toDate: string;
  isMaster?:string;
}

export interface ProjectEmployeeTable {
  pjmId: number | null;
  prjId: number | null | undefined;
  jobId: FormControl;
  pm: boolean;
  empId: FormControl;
  fromDate: FormControl;
  toDate: FormControl;
  scale: FormControl;
  isDeleted?: boolean;
  empName: string;
  orgName: string;
  orgNameTooltip?: string;
  isSelectEmp: boolean;
  jobName?:string
}
