export interface LookupCode {
  createBy: string;
  createDate: number;
  lcoDesc: string;
  lastUpdatedBy: string;
  lastUpdatedDate: number;
  name: string;
  status: string;
  lcoId:number;
  flagStatus: number;
  childId: number;
  parentId: number;
}
export class ParamsModel {
  lvaId?: string = '';
  lcoId: string = '';
  lvaMean: string = '';
  parentValue: string = '';
  flagStatus: string|number = '';
  parentMean?: string = '';
  lvaParentId?: string = '';
  childId?:string = '';
  page?: number = 0;
  size?: number = 15
  fromDate?: string ='';
  toDate?: string ='';
  lcoIds? = [];
}
export interface ManageCategory {
  lcoDesc: string;
  lvaMean:string;
  lvaMeanAttribute:string;
  flagStatus:number;
  parentMean: string;
  lvaId?: number;
  lcoId?: string;
  lvaParentId: string;
  lvaValue:string;
  parentValue:string;
  childId:string;
  toDate:string;
  fromDate:string;
}

export interface CategoryRequest {
  lvaId: number,
  lcoId: number,
  lvaMean: string,
  parentValue: string,
  flagStatus: number,
  fromDate: string,
  toDate: string,
  sub1: SubCategory | null,
  "sub2": SubCategory | null
}

export interface SubCategory {
  subLcoId: number,
  subLvaId: number | string,
  subLvaMean: string,
  subParentValue: string
}


