export interface PaginationPlan {
  currentPage: number,
  totalElement: number,
  size: number,
  totalPage?:number,
  numberOfElements:number,
  result?:number,
  min?:number,
  max?:number
}
export interface StaffOrganizationNode {
  expanded?: boolean
  id: string
  isExpanded?: boolean
  level?: number
  name: string
}
