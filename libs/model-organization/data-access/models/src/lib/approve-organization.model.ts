import {Type} from "./organization.model";

export interface ListSearchOrganization {
  approveDate: string;
  style?:number;
  reason: string;
  flagStatus: number;
  inputType: string;
  orgId: number;
  parentId: number;
  pathName: string;
  orgLevel: number;
  orgName:string;
  hideRootBorder?: boolean;
  key?: string;
  subs?: Array<ListSearchOrganization>;
  approveLevel2: boolean;
  show: boolean;
}

export interface ParamSubmit {
  flagStatus: number;
  orgId?: string;
  inputType?: string;
  content?: string;
  reason?: string;
}
export interface OrgApprove {
  id: number;
  type: string;
}
export interface DataModel {
  branchType: string;
  fromDate: string;
  organization: OrganizationModel;
  parentId: number;
  parentName: string;
  region: string;
  sheetApproval: SheetApprovalModel;
  toDate: string;
}
export interface OrganizationModel {
  jobDTO: Array<JobModelUnit>;
  orgId: number;
  orgName: string;
  positionDTOS: Array<PositionDTOSModel>;
  orgAttributeDTOS: Array<OrgAttributeModel>;
  positionDetailDTOS: Array<PositionDetailModel>;
  legalBranchId: number;
}
export interface JobModelUnit {
  jobId: number;
  jobName: string;
}
export interface PositionDTOSModel {
  code: string;
  id: number;
  jobId: number;
  orgId: number;
  name: string;
}

export interface SheetApprovalModel {
  sapId: number;
  sapNo: string;
  mpDoc: DocuModel;
  reason: string;
}
export interface OrgAttributeModel {
  orgaValue: string;
  orgaId: number;
}
export interface PositionDetailModel {
  orgaId: number;
  posId: number;
}
export interface JobTmpModel {
  orgaValue: string;
  orgaId: number;
  posId: number;
  jobId: number;
  jobName: string;
}
export interface DocuModel {
  fileName: number|string;
  docId: string;
}
export interface JobModelTmp {
  orgaId: number;
  orgaValue: string;
  posId?: number;
  jobId?: number;
  jobName?: string;
}
export interface JobModelRes {
  orgaId: number;
  orgaValue: string;
  subs: Array<JobModelTmp>;
}
export interface BranchTypeModel {
  data: Array<Type>;
}
export interface PositionDTOSModel {
  code: string;
  id: number;
  jobId: number;
  orgId: number;
  name: string;
}

