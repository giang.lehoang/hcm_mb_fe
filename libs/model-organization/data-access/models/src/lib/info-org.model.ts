
export interface InfoOrganization {
  address: string;
  fax: string;
  orgLevel: string;
  fromDate: string;
  fullName: string;
  initialDate: string;
  logoRef?: string;
  orgCode: string;
  orgId: number;
  orgName: string;
  orgParentName: string;
  orgTypeId: number;
  orgTypeName: string;
  parentId: number;
  regionId: number;
  regionName: string;
  subRegion: string;
  subRegionName: string;
  surrogateId: string;
  t24Code: string;
  telephone: string;
  toDate: string;
  docId: number;
  fileName: string;
  legalBranchId?:number;
  branchName?: string;
  orgNameLevel1: string;
  orgNameLevel2: string;
  orgNameLevel3: string;
  orgNameLevel4: string;
  orgNameManage: string;
  orgAbbreviatation: String;
  orgFullName: String
}

