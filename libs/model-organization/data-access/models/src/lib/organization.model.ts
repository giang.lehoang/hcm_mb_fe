import {FormControl} from "@angular/forms";
import {Job} from "./job.model";

export interface OrganizationDetail {
  address: string;
  branchType: string;
  fax: string;
  flagStatus: number;
  legalBranch: string;
  legalBranchId: number | null;
  orgCode: string;
  orgId: number;
  orgLevel: number;
  orgName: string;
  orgNameLevel1: string;
  orgNameLevel2: string;
  orgNameLevel3: string;
  orgNameLevel4: string;
  orgNameManage: string;
  orgType: string;
  orgTypeValue: string;
  region: string;
  regionValue: string;
  subRegion: string;
  surrogate: string;
  surrogateId: number | null;
  t24Code: string | null;
  telephone: string | null;
}

export interface FunctionOrgInit {
  value: FormControl;
  job?: Job[];
}

export interface Organization {
  control?: FormControl;
  sequent?: number;
  organization?: Organization;
  id?: number;
  orgId?: number | null;
  orgName?: string;
  orgLevel: number;
  function?: FunctionOrgInit[];
  job: Job[];
  children?: Organization[];
  subs?: Organization[];
  posL?: PositionOrg[];
  status?: boolean;
  parentName?: string;
  positions?: PositionOrg[];
  orgAttributes?: object[];
  orgTypeId?: number;
  legalBranchId?: number;
  regionId?: number;
  date?: string;
  sheetApproval?: {
    mpDoc: Mpdoc;
    sapNo: string;
    reason: string;
    sapDate: string;
    sapId: string;
  };
  fromDate?: string;
  toDate?: string;
  flagEdited?: string;
  flagStatus?: string;
  region?: string;
  branchType?: string;
}


export interface Mpdoc {
  docId: number;
  fileName: string;
}

export interface PositionOrg {
  id: number;
  jobId: number;
  name?: string;
  code?: string;
}

export interface OrgInfo {
  orgId: number;
  orgName: string;
  path: string;
  parentId: number;
  parentName: string;
}

export interface TreeOrganization {
  orgName: string;
  pathName: string;
  orgId: number;
  expanded: boolean;
  pageNumber?: number;
  treeLevel: number;
  pathId: string;
}


export interface Template {
  orgId: number;
  orgName: string;
  branchType: string;
  flagStatus: number;
  createdDate: string;
}

export interface SearchTemplate {
  page?: number,
  size?: number,
  orgName?: string,
  flagStatus?: string,
}

export interface DetailListTemp {
  orgId: number | null;
  orgLevel: number | null;
  orgName: string;
  subs: DetailListTemp[];
}

export interface OrgLine {
  flagStatus: number;
  isExpanded: boolean;
  orgGroup: string;
  orgId: number;
  orgName: string;
  orgType: string;
  parentId: number;
  pathId: string;
  pathName: string;
  rootId: number;
  treeLevel: number;
}

export interface Ijob {
  jobId: number;
  jobCode: string;
  jobName: string;
  flagStatus: number;
  createdBy: string;
  createdDate: string;
  lastUpdatedBy: string;
  lastUpdatedDate: string;
  toDate: string;
  fromDate: string;
  jobType: string;
  jobDescription: string;
}

export interface ExampleFlatNode {
  expanded: boolean;
  name: string;
  level: number;
  isExpanded: boolean;
}

export interface InitScreenSearch {
  orgId: string;
  flagStatus: string | null;
  inputType: string | null;
  page: number;
  size: number;
  jobId?: number;
  year?: number;
  date?: string;
}

export interface ContentOrg {
  orgId: number;
  orgName: string;
  flagStatus: number;
  createdDate: number;
  approved: string;
  inputType: string;
  pathName: string;
  approveDate: string;
  content: string
}

export interface Area {
  lvaValue: string;
  lvaId: number;
  lcoCode: string;
  lvaMean: string;
}

export interface Type {
  lvaValue: string;
  lvaId: number;
  orgId: number;
  orgName?: string;
  branchType: string;
  lcoCode: string;
  lvaMean: string;
}

export interface RequestBodyInitOrg {
  orgId?: string;
  parentId?: number;
  branchType: string;
  region: string;
  fromDate: string;
  legalBranchId: number;
  sapNo: string;
  orgReqDetails?: OrganizationCreate[];
  subs?: OrganizationCreate[]
}

export interface OrganizationCreate {
  orgName: string;
  templateOrgId: number | null;
  posDetails: PositionDetailCreate[];
  subs: OrganizationCreate[];
  orgReqDetails?: OrganizationCreate[];
}

export interface PositionDetailCreate {
  orgAttrValue: string;
  jobIds: number[];
}

export interface PositionDetailOrg {
  id: number;
  name: string;
  orgId: number;
  jobId: number;
  value?: string;
  posId: number;
}

export interface AdjustOrgRequest {
  orgId: number | undefined;
  parentId?: number;
  orgName: string;
  region: string;
  branchType: string;
  legalBranchId: number | null;
  orgLevel: number | undefined;
  inputType?: string;
  toDate?: string | null;
  fromDate?: string;
  flagStatus?: number;
  sheetApproval: {
    sapId: string | undefined;
    sapNo: string;
    reason?: string;
    sapDate?: string;
  };
  adjustedFunctions?: FunctionOrg[];
  orgReqDetails?: OrganizationCreate[];
}

export interface FunctionOrg {
  orgaId: number | null;
  orgaValue: string;
  jobIds: (number | undefined)[] | undefined;
}

export interface OrgNode {
  name: string;
  id: number;
}

export interface ValidateObject {
  attach: boolean;
}

export interface ListLegalBranch {
  orgId: number;
  orgName: string;
}


export interface OrgDetailResponse extends Organization {
  orgName: string,
  parentName: string,
  parentId: number,
  fromDate: string,
  orgLevel: number,
  positionDTOS: PositionDetailOrg[],
  jobDTO: [],
  orgAttributeDTOS: AttributesOrg[],
  positionDetailDTOS: { orgaId: number, posId: number }[],
}

export interface AttributesOrg {
  orgId: number | null;
  orgaId: number | null;
  orgaValue: string;
  control?: FormControl;
  positions: Job[];
}

export interface OrganizationObject {
  branchType: string;
  area: string;
  legalBranch: string;
}

export interface FileData {
  file?: File;
  blob?: Blob | null;
  fileName?: string;
  status?: boolean;
}

export interface ShareData {
  id?: string | number;
  type?: number;
  disable?: boolean;
  width?: string;
  loading?: boolean;
  data?: OrganizationDetailCorrection;
  sapNo?: string;
  modify?: boolean;
}

export interface OrganizationDetailCorrection {
  branchType?: string;
  fromDate?: string;
  flagEdited?: string;
  flagStatus?: number;
  legalBranchId?: string;
  inputType?: string;
  jobDTO?: Job[];
  orgAttributes: AttributesCorrect[];
  orgId?: number;
  orgLevel?: number | undefined;
  orgName?: string;
  parentId?: number;
  parentName?: string;
  positions?: PositionDetailOrg[];
  region?: string;
  sapId?: string;
  sheetApproval?: SheetApproval;
  toDate?: string;
  positionDetailDTOS?: PositionDetailDTOS[];
}


export interface SheetApproval {
  sapId: number;
  mpDoc: { docId: number; fileName: string };
  sapNo: string;
  reason: string;
}

export interface AttributesCorrect {
  orgaId: number | null;
  orgaValue: string;
  orgId: number | null;
  control: FormControl;
  positions?: PositionDetailOrg[];
}

export interface PositionDetailDTOS {
  posId: number;
  orgaId: number;
}

export interface TreeRoot<T> {
  key:string;
  title?:string;
  children: Array<T>;
  level: number;
  expand?: boolean;
  parent?: T;

}

export interface OrganizationTreeNode extends TreeRoot<OrganizationTreeNode>{
  branchType?: null | string;
  fullName?: string;
  orgCode?: string;
  orgId: number;
  orgName: string;
  parentId: null | number;
  pathId: string;
  region: null | string;
  subRegion: null | string;
  loadChildren?:boolean;
  lengthChildren:number;
  maxChange:number;
  minChange: number;
  index: number;
  label: string
}

export interface TreeSeqUpdate {
  orgId: number,
  displaySeq: number
}

export interface RequestOrgName {
  name: string
  is_author?: boolean
  size?:number
}

export interface OrganizationByName extends OrganizationTreeNode{
  flagStatus: number;
  isExpanded: boolean;
  orgGroup: string;
  orgId: number;
  orgName: string;
  orgType: string;
  parentId: number;
  pathId: string;
  pathName: string;
  rootId: number;
  tmpInputType: string;
  tmpStatus: string;
  treeLevel: number;
}

export enum MODE_DRAG {
  NOTHING,
  INSIDE,
  OUTSIDE_MIN,
  OUTSIDE_MAX
}



