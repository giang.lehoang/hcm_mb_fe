import {SelectObject} from "./common.model";

export class ParamsJDSearch {
  orgId?: number | null;
  jobId?: number | null;
  jobGroup?: string;
  orgName: string;
  jobName: string;
  orgaValue: string;
  jdName?: string;
  flagStatus?: number | undefined;
  lvaIdJob?: number | undefined;
  lvaIdProfession?: number | undefined;
  orgaId?: number | undefined | null;
  page?: number;
  size: number ;
  description?: string;

  constructor() {
    this.orgId = null;
    this.jobId = null;
    this.jobGroup = '';
    this.orgName = '';
    this.jobName = '';
    this.orgaValue = '';
    this.jdName = '';
    this.page = 0;
    this.size = 15;
  }
}
export interface Attributes {
  orgaId: number;
  orgaValue: string;
}
export class LookupParam {
  lookupCode: string;
  flagStatus: number;

  constructor() {
    this.lookupCode = 'NHOM_CONG_VIEC';
    this.flagStatus = 1;
  }
}

export interface JDsModel {
  orgId: number;
  orgName: string;
  flagStatus: number;
  jdId: number;
  jdName: string;
  jobGroup: string;
  jobId: number;
  jobName: string;
  lvaIdJob: number;
  lvaIdProfession: number;
  lvaMeanJob: string;
  lvaMeanProfession: string;
  regionMean: string;
  orgaId: number;
  orgaValue: string;
  psdId: number;
  fullName: string;
  view?:boolean;
  posId?:number;
  isChecked?: boolean;
}
export interface JDsApproveModel {
  orgId: number;
  orgName: string;
  flagStatus: number;
  jdId: number;
  jdName: string;
  jobGroup: string;
  jobId: number;
  jobName: string;
  lvaIdJob: number;
  lvaIdProfession: number;
  lvaMeanJob: string;
  lvaMeanProfession: string;
  regionMean: string;
  orgaId: number;
  orgaValue: string;
  psdId: number;
  fullName: string;
  isChecked: boolean;
  view?:boolean;
  posId?:number;
}

export interface ListJd {
  content: Array<ArrayListJds>;
}

export interface ListMangementUnit {
  data: Array<ArrayListMangementUnit>;
}

export interface ArrayListJds {
  dvqlId: number;
  dvqlName: string;
  dvtt1Id: string;
  dvtt1Name: string;
  dvtt2Id: number;
  dvtt2Name: string;
  dvtt3Id: number;
  dvtt3Name: string;
  flagStatus: number;
  jdId: number;
  jdName: string;
  orgCode: string;
  orgGroup: string;
  orgId: number;
  orgName: string;
  posId: number;
  posName: string;
  psdId: number;
  psdName: string;
  stt: number;
}

export interface ArrayListMangementUnit {
  orgName: string;
  orgId: string;
}

export interface ListPosition {
  data: Array<ArrayListPosition>;
}

export interface ArrayListPosition {
  id: number;
  name: string;
}

export interface ObjectSearchJD {
  dvqlId: number;
  dvtt1Id: string;
  dvtt2Id: number;
  dvtt3Id: number;
  flagStatus: number;
  jdIds: number;
  posIds: number;
  orgGroup: string;
}

export const listStatus:SelectObject<number,string>[] = [
  { id: 7, value: 'Chưa tạo' },
  { id: 5, value: 'Tạo mới' },
  { id: 2, value: 'Chờ duyệt' },
  { id: 3, value: 'Từ chối' },
  { id: 1, value: 'Đang hoạt động' },
  { id: 0, value: 'Dừng hoạt động' },
  { id: 6, value: 'Xác nhận' },
];

import { FormControl } from "@angular/forms";


export interface PositionJD {
  id: number;
  name: string;
}

export interface MultiSelect {
  directReports?: Array<ReportItem>;
  otherReports?: Array<ReportItem>;
  indirectReports?: Array<ReportItem>;
  ex?: {
    targetId: number,
    targetDescription: string,
    targetVal: string,
    jddType: string,
    rank: number,
    jddChildren: [],
  }

  type?:string;
  target?:string;

  parent_id?: number;
  competence_name?: string;
  cde_id?: number;
  value?: string;
  children?: [string];
}

export interface ReportItem {
  posId: string,
  orderNumber:number,
}

export interface Task {
  taskTitle: string;
  content: [
    {
      task: string;
      result: string;
    }
  ];
}

export interface Knowledge {
  certificate: string;
  explain: string;
}

interface LisPosition {
  id: number,
  name: string
}

export interface  ListPositionById {
  data: Array<LisPosition>
}

export interface Person {
  key: string;
  name: string;
  age: number;
  address: string;
}

export interface TaskTable {
  taskName:FormControl,
  sub: SubTask[]
}

export interface SubTask {
  taskName:FormControl,
  result:FormControl,
}

export interface DetailTable {
  value: FormControl,
  description: FormControl,
  selectTemp?: CompetenceJobGroupList[]
}

export interface Competency {
  competenceControl: FormControl;
  competenceFrames: string;
  competenceJobGroupList: CompetenceJobGroupList[]
}

export interface CompetenceJobGroupList {
  lvaMeanControl: FormControl;
  lvaMean: string;
  lvaValue:string;
  parentValue: string;
  isDefault: 'Y' | 'N';
}

export interface Submit {
  isSubmit:boolean;
}

export interface Report {
  dReport:number[];
  iReport:number[];
  rReport:number[];
}

export const ASSIGNMENT_TYPE = [
  {key: '0', value:'Position chi tiết'},
  {key: '1', value:'Position'},
  {key: '2', value:'Nhóm Position'}
]


export interface JDCreate {
  id: string,
  save: boolean,
  jdName: string,
  toDate: string,
  fromDate: string,
  assignmentType: string,
  targetValue: string,
  jobGroup: string,
  competenceRole: string,
  psdId: number,
  posId: number,
  jdRpts: JDRCreate[],
  jdDetails: JDDetail[],
  deleteFile?: boolean
}

export interface JDPending {
  jdId: number,
  initializeJdDetailRequest: JDCreate;
}

export interface JDRCreate {
  jobId: number,
  jrpType: "BCTT" | "BCGT" | "NNBC"
}

export interface JDDetail {
  jddType: string,
  jddTypeObjects: JDDetailObject[]
}

export interface JDDetailObject {
  targetItem?: string | number,
  targetDescription: string,
  targetVal?: string,
  fakeIdParent?: number,
  fakeId?:number,
  jddId?:number
}

export interface MajorLevel {
  majorLevelId: number,
  code: string,
  name: string
}

export enum JDDetailType {
  EXPERIENCE="KINH_NGHIEM",
  DEGREE="BANG_CAP",
  CERTIFICATE="CHUNG_CHI",
  LANGUAGE="NGOAI_NGU",
  COMPETENCY="KHUNG_NANG_LUC",
  TARGET_JOB="MUC_DICH_CONG_VIEC",
  TASK="NHIEM_VU"
}

export interface JDEdit {
  jdId: number,
  jdName: string,
  assignmentType: string,
  fromDate: string,
  toDate: string,
  lvaValueCompetenceRole: string,
  positionDetailDTO: PositionDetailDTO,
  jdReporterDTOS: jdReporterDTOS[],
  jdDetailDTOMap: jdDetailDTOMap
  docId: number;
  fileName: string
}

export interface PositionDetailDTO {
  psdId: number,
  psdName: string,
  organizationDTO: OrganizationDTO,
  orgAttributeDTO: OrgAttributeDTO,
}

export interface OrganizationDTO {
  orgId: number,
  lvaMeanProfession: string,
  lvaMeanJob: string
}

export interface OrgAttributeDTO {
  orgaId: number,
  orgaValue: string
}

export interface jdReporterDTOS {
  jrpId: number,
  jrpType: "BCTT" | "BCGT" | "NNBC",
  jrpValue: string,
  jrpJobId: number
}

export interface jdDetailDTOMap {
  KINH_NGHIEM?: jdDetailDTOMapObj[];
  BANG_CAP?: jdDetailDTOMapObj[];
  CHUNG_CHI?: jdDetailDTOMapObj[];
  NGOAI_NGU?: jdDetailDTOMapObj[];
  KHUNG_NANG_LUC?: jdDetailDTOMapObj[];
  MUC_DICH_CONG_VIEC?:jdDetailDTOMapObj[];
  NHIEM_VU?: jdDetailDTOMapObj[]
}

export interface jdDetailDTOMapObj {
  jddId: number,
  parentId: number,
  targetItem: string,
  targetDesc: string,
  targetVal: string,
  targetDescParent: string,
  sub?: jdDetailDTOMapObj[]
}

export interface ApproveJD {
  jdId: number[],
  description: string
}




