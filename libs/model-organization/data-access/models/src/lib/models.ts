import {Pagination} from "./common.model";
import {Job} from "./job.model";

export interface InitStaffSearch {
  periodCode?: string;
  orgName?: string;
  jobId?: string;
  page?: number;
  size?: number;
  orgId?: string;
  inputType?: string;
}

export interface AproveModel {
  hcpId: number;
  quantity?: string;
  headCount: string;
}
export interface JobModel {
  data: Array<Job>;
}

export interface ObjectSearch {
  orgId?: string;
  name?: string;
  fromDate?: string;
  page?: number;
  size?: number;
  ids?: Array<string>;
  status?: string;
  jobId?: string;
  flagStatus?: number;
  inputType?: string;
  year?: string;
  date?: string;
  orgName?: string;
  periodCode?: string;
  description?: string;
  lastUpdateDate?:string;
  lastUpdateDateTmp?:string
}

export interface PlanModel {
  content: Array<ListPlanModel>;
  pageable: Pagination;
  numberOfElements: number;
  totalElements: number;
}
export interface ListPlanModel {
  flagStatus: number;
  fromDate: string;
  headCount: string;
  inputType: string;
  jobId: number;
  jobName: string;
  orgId: number;
  orgName: string;
  pathName: string;
  periodCode: string;
  posId: string;
  contents: string;
  dateTime: string;
}

export interface YearModel {
  data: Years;
}
export interface Years {
  periodCode: Array<string>;
}
export interface Year {
  value: string;
}

export interface StaffData {
  hcpId: number;
  periodCode: string;
  pathName: string;
  jobName: string;
  quantity?: string;
  headCount?:string;
  latchNumber?: number;
  adjustedNumber?: number;
  reason?: string;
  isChecked?: boolean;
  index?: number;
  page?: number;
  fromDate?: string;
  effectiveDate?: string;
  consultation?: string;
  docName?: string;
  isValidDate?: boolean;
  inputType?: string;
  adjustDocName?: string;
  adjustDocId?: number;
  lastUpdateDate?:string;
  adjustedNumberTmp?:number;
}

export interface OrganizationNode {
  expanded?: boolean;
  id: string | number;
  isExpanded?: boolean;
  level?: number;
  name: string;
  subs:OrganizationNode[]
}

export interface FillModel {
  adjustedNumber?: string;
  reason?: string;
  fromDate?: string;
  consultation?: string;
}
export interface RequestRegionModel {
  id?: number;
  meaning: string;
  flagStatus: number;
  type: string;
  parentValue?: string;
}

