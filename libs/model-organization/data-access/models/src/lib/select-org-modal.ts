export interface Params {
    id: string,
    page: number,
    size: number,
}

export interface FlatNode {
    expanded: boolean;
    name: string;
    level: number;
    isExpanded: boolean;
  }

  export interface OrganizationNodeTree {
    orgId: number;
    orgName: string;
    subs?: OrganizationNodeTree[];
    expanded: boolean;
  }

  export interface TreeOrganizationModal {
    orgName: string;
    pathName: string;
    orgId: number;
    expanded: boolean;
  }

  export interface DataResponse<T> {
    content?: T[];
    number: number;
    numberOfElements: number;
    size: number;
    totalElements: number;
    totalPages: number;
    pageable?: {
      pageNumber: number;
    };
  }

  export interface OrgTreeSearch {
    orgId: string;
    flagStatus: string;
    inputType: string;
    page: number;
    size: number;
    jobId?: number;
    year?: number;
    date?: string;
  }
