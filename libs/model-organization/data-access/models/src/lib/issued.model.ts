export interface IUserInfo {
  email: string;
  email_verified: boolean;
  family_name: string;
  given_name: string;
  locale: string;
  name: string;
  preferred_username: string;
  sub: string;
}

export interface IIssuedOrganization {
  orgId: number;
  orgName: string;
  description: string;
  issuedDate: string;
  inputType: string;
  orgType: string;
  flagStatus: number;
  inforIssue: string;
}

export interface IssuedRequest {
  title: string;
  content: string;
  users: User[];
  org: IssuesDetail | null;
  prj: IssuesDetail | null;
  jd: IssuesDetail | null;
}

export interface IssuesDetail {
  objIds: number[];
  jobIds: number[];
}

export interface User {
  fullName: string;
  email: string;
}

export interface Issues {
  dataId: number;
  dataName: string;
  description: string;
  issuedDate: string;
  dataType: string;
}

export interface Member {
  department: string;
  fullName: string;
  gdeId: number;
  gmeId: number;
  gmeMail: string;
  gmeUser: string;
  nickName: string;
}

export interface Group {
  gdeId: number;
  gdeName: string;
  gdeUser: string;
  members: Member[];
}

export interface SearchIssued {
  dataType: string;
  dataName: string;
  issuedDate: string;
  description: string;
}

export interface EmployeeGroupCreate {
  gdeId: number | null,
  gdeName: string,
  gdeMembers: MemberCreate[],
}

export interface MemberCreate {
  gmeUser: string,
  gmeMail: string
}

export interface ImportEmployee {
  employeeInfoDTOs: EmployeeDetail[];
}


export interface EmployeeDetail {
  employeeId: number;
  employeeCode: string;
  fullName: string; //Tên nhân viên
  jobName: string; //Nghề nghiệp
  dateOfBirth: string; //Ngày sinh
  personalId: string; //Số CMND
  email: string; //Email nhân viên
  positionName?: string; // Chức danh
  orgName?: string; // Đơn vị
  imagePath?: string; // Đường dẫn avatar
  flagStatus?: number; //Trạng thái làm việc. 1: Đang làm việc, 0: Đã nghỉ việc
  currentAddress?: string;
  department?:string;
}

export interface HistoryOrg {
  actionType: string;
  delId: number;
  infoPublicized: string;
  orgId: number;
  position: string;
  receivedEmail: string;
  receivedFullName: string;
  receivedUser: string;
  sendTime: string;
}

export interface ParamHis {
  objId: number;
  page: number;
  size: number;
  dataType: string;
}


