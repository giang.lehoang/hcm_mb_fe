export interface IParamReportEmployee {
    orgId?: number | undefined | string;
    orgGroup?: string;
    fromDate?: string;
}
  