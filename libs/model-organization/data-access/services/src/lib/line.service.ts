import {Injectable} from "@angular/core";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {HttpClient} from "@angular/common/http";
import {MICRO_SERVICE, SERVICE_NAME} from "@hcm-mfe/shared/common/constants";
import {URL_LINE} from "./constant/line.url";
import {Observable} from "rxjs";
import {
  ApproveRequest,
  AssignPosRes,
  JobLine,
  OrganizationNode,
  ResponseEntity,
  ResponseEntityData
} from "@hcm-mfe/model-organization/data-access/models";
import {
  DataLine, DataLineApprove, JobByOrg,
  LineInit,
  ManageLine,
  ObjectLineDetail, SearchLineInit,
  SearchLineInitList,
  SearchLineObj, TreeLine
} from "../../../models/src/lib/line.model";

@Injectable({
  providedIn: 'root',
})
export class LineService extends BaseService {

  private dataLine:any;

  constructor(readonly http: HttpClient) {
    super(http);
  }
  getListTreeLine():Observable<ResponseEntity<TreeLine[]>> {
    return this.get(SERVICE_NAME.MODEL_PLAN + URL_LINE.POS_GROUP_LIST, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  getListTreeLineById(id:number):Observable<ResponseEntity<TreeLine[]>> {
    return this.get(SERVICE_NAME.MODEL_PLAN + URL_LINE.POS_GROUP_LIST, {params:{id:id}}, MICRO_SERVICE.MODEL_PLAN);
  }

  getListNameSuggest(name:string):Observable<ResponseEntity<ManageLine[]>> {
    return this.get(SERVICE_NAME.MODEL_PLAN + URL_LINE.POS_GROUP_SEARCH, {params:{name:name}}, MICRO_SERVICE.MODEL_PLAN);
  }

  getLineDetailById(id:number):Observable<ResponseEntity<ObjectLineDetail>>{
    return this.get(SERVICE_NAME.MODEL_PLAN + URL_LINE.LINE_DETAIL + '/' + id, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  createLine(body:LineInit){
    return this.post(SERVICE_NAME.MODEL_PLAN + URL_LINE.CREATE_LINE, body,{}, MICRO_SERVICE.MODEL_PLAN);
  }

  updateLine(body:LineInit){
    return this.put(SERVICE_NAME.MODEL_PLAN + URL_LINE.UPDATE_LINE, body, MICRO_SERVICE.MODEL_PLAN);
  }

  // searchOrgLine(params): Observable<IOrganizationModel>{
  //   return this.http.get<IOrganizationModel>(environment.baseUrl + url.url_enpoint_line_search_org, {params:params})
  // }

  getListJobById(id:number): Observable<ResponseEntity<JobLine[]>>{
    return this.get(SERVICE_NAME.MODEL_PLAN + URL_LINE.JOB_BY_ORG_ID + '/' + id, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  getListJobByIdUnassign(request:JobByOrg): Observable<ResponseEntity<JobLine[]>>{
    return this.get(SERVICE_NAME.MODEL_PLAN + URL_LINE.JOB_BY_ORG_ID_ASSIGN, {params:request}, MICRO_SERVICE.MODEL_PLAN);
  }

  findAllByOrgGroupCN():Observable<ResponseEntity<JobLine[]>>{
    return this.get(SERVICE_NAME.MODEL_PLAN + URL_LINE.JOB_BY_ORG, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  searchLine(body:SearchLineInit):Observable<ResponseEntityData<SearchLineInitList[]>>{
    return this.post(SERVICE_NAME.MODEL_PLAN + URL_LINE.SEARCH_LINE, body,{}, MICRO_SERVICE.MODEL_PLAN);
  }

  searchListLineView(body:SearchLineObj):Observable<ResponseEntityData<DataLine[]>>{
    return this.post(SERVICE_NAME.MODEL_PLAN + URL_LINE.LINE_VIEW, body, {},MICRO_SERVICE.MODEL_PLAN);
  }

  searchListLineViewPending(body:SearchLineObj):Observable<ResponseEntity<DataLineApprove[]>>{
    return this.post(SERVICE_NAME.MODEL_PLAN + URL_LINE.LINE_VIEW_PENDING, body, {},MICRO_SERVICE.MODEL_PLAN);
  }

  approvedLine(body:ApproveRequest){
    return this.post(SERVICE_NAME.MODEL_PLAN + URL_LINE.LINE_APPROVE, body, {},MICRO_SERVICE.MODEL_PLAN);
  }

  rejectedLine(body:ApproveRequest){
    return this.post(SERVICE_NAME.MODEL_PLAN + URL_LINE.LINE_REJECT, body, {},MICRO_SERVICE.MODEL_PLAN);
  }

  deleteLine(id:number){
    return this.delete(SERVICE_NAME.MODEL_PLAN + URL_LINE.DELETE_LINE + '/' + id, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  tranferLine(body:AssignPosRes){
    return this.post(SERVICE_NAME.MODEL_PLAN + URL_LINE.LINE_PENDING, body,{}, MICRO_SERVICE.MODEL_PLAN);
  }

  setDataLine(data:any){
    this.dataLine = data
  }

  getDataLine(){
    return this.dataLine
  }

  findAllByOrgGroupCNAssign(request:JobByOrg):Observable<ResponseEntity<JobLine[]>>{
    return this.get(SERVICE_NAME.MODEL_PLAN + URL_LINE.JOB_BY_CN_ASSIGN, {params:request}, MICRO_SERVICE.MODEL_PLAN);
  }

}
