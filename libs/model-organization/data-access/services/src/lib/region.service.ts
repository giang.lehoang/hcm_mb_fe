import { Injectable } from "@angular/core";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {MICRO_SERVICE, SERVICE_NAME} from "@hcm-mfe/shared/common/constants";
import { REGION_URL } from './constant/region.url';



@Injectable({
  providedIn: 'root',
})
export class RegionService extends BaseService {
  deleteCategory(id:any){
    return this.delete(SERVICE_NAME.MODEL_PLAN + REGION_URL.lookupValue + '/' + id,{},MICRO_SERVICE.MODEL_PLAN);
  }
  regionCategoryDetail(id:any){
    return this.get(SERVICE_NAME.MODEL_PLAN + REGION_URL.lookupValue + '/' + id,{},MICRO_SERVICE.MODEL_PLAN);
  }
  createRegionCategory(body:any){
    return this.post(SERVICE_NAME.MODEL_PLAN + REGION_URL.lookupValue, body,{},MICRO_SERVICE.MODEL_PLAN)
  }
  updateRegionCategory(body:any){
    return this.put(SERVICE_NAME.MODEL_PLAN + REGION_URL.lookupValue, body,MICRO_SERVICE.MODEL_PLAN)
  }
}
