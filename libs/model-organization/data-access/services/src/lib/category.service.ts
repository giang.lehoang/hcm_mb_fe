import { Injectable } from "@angular/core";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {MICRO_SERVICE, SERVICE_NAME} from "@hcm-mfe/shared/common/constants";
import { COMMON_URL } from './constant/common.url';
import { CATEGORY_URL } from './constant/category.url';
import {CategoryRequest, ParamsModel} from '../../../models/src/lib/category.model';


@Injectable({
  providedIn: 'root',
})
export class CategoryService extends BaseService {

  getLookupCode() {
    return this.get(SERVICE_NAME.MODEL_PLAN + CATEGORY_URL.CATEGORY_WITH_CONFIG, {}, MICRO_SERVICE.MODEL_PLAN);
  }
  getCategoryManage(lcoId: number) {
    const params = {
      lcoId: lcoId
    }
    return this.get(SERVICE_NAME.MODEL_PLAN + CATEGORY_URL.CATEGORY_MANAGE, {params:params}, MICRO_SERVICE.MODEL_PLAN);
  }
  createCategory(body: CategoryRequest) {
    return this.post(SERVICE_NAME.MODEL_PLAN + CATEGORY_URL.UPDATE_CATEGORY,body, {}, MICRO_SERVICE.MODEL_PLAN);
  }
  detailCategory(id:number){
    return this.get(SERVICE_NAME.MODEL_PLAN + CATEGORY_URL.DETAIL_CATEGORY+'/'+id, {}, MICRO_SERVICE.MODEL_PLAN);
  }
  deleteCategory(id:number){
    return this.delete(SERVICE_NAME.MODEL_PLAN + CATEGORY_URL.DETAIL_CATEGORY+'/'+id, {}, MICRO_SERVICE.MODEL_PLAN);
  }
  listCategory(params:any){
    return this.get(SERVICE_NAME.MODEL_PLAN + CATEGORY_URL.LIST_CATEGORY, {params:params}, MICRO_SERVICE.MODEL_PLAN);
  }
  exportCategory(params: ParamsModel) {
    return this.get(SERVICE_NAME.MODEL_PLAN + CATEGORY_URL.EXPORT_CATEGORY, {params:params,responseType: 'arraybuffer'}, MICRO_SERVICE.MODEL_PLAN);
  }

  updateCategory(body:CategoryRequest){
    return this.put(SERVICE_NAME.MODEL_PLAN + CATEGORY_URL.EDIT_CATEGORY,body, MICRO_SERVICE.MODEL_PLAN);
  }
  importExcel() {
    return `${SERVICE_NAME.MODEL_PLAN }${CATEGORY_URL.IMPORT_EXCEL}`;
  }
  templateExcel() {
    return `${SERVICE_NAME.MODEL_PLAN }${CATEGORY_URL.DOWNLOAD_TEMP}`;
  }
}
