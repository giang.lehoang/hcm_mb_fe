import { Injectable } from '@angular/core';
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {MICRO_SERVICE, SERVICE_NAME, url} from "@hcm-mfe/shared/common/constants";
import {urlStaffModule} from "./url/url.const";
import {environment} from "@hcm-mfe/shared/environment";
import { UrlConstant } from '@hcm-mfe/policy-management/data-access/service';

@Injectable({
  providedIn: 'root',
})
export class InitStaffPlanService extends BaseService {

  search(params:any) {
    return this.get(SERVICE_NAME.MODEL_PLAN + urlStaffModule.initSearch, { params: params }, MICRO_SERVICE.MODEL_PLAN);
  }

  approveTranfer(body:any) {
    return this.put(SERVICE_NAME.MODEL_PLAN + urlStaffModule.initApproval, body, MICRO_SERVICE.MODEL_PLAN);
  }

  approveTranferAll(body:any) {
    return this.put(SERVICE_NAME.MODEL_PLAN + urlStaffModule.initApprovalAll, body, MICRO_SERVICE.MODEL_PLAN);
  }

  exportExcelInitStaff(body:any) {
    return this.post(SERVICE_NAME.MODEL_PLAN + urlStaffModule.initExport, body, {
      responseType: 'arraybuffer',
    }, MICRO_SERVICE.MODEL_PLAN);
  }

  getUrlUpload(): string {
    return `${SERVICE_NAME.MODEL_PLAN}${urlStaffModule.uploadFile}`;
  }
}
