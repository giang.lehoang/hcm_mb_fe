import { Injectable } from '@angular/core';
import {
  Job,
  ResponseEntity,
  ResponseEntityData,
  LookupValue,
  CriteriaSearch
} from "@hcm-mfe/model-organization/data-access/models";
import {Observable} from "rxjs";
import {maxInt32} from "@hcm-mfe/shared/common/enums";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {MICRO_SERVICE, SERVICE_NAME} from "@hcm-mfe/shared/common/constants";
import {urlStaffModule} from "./url/url.const";
import {COMMON_URL} from "./constant/common.url";
import {JOB_URLS} from "./constant/job.url";

@Injectable({
  providedIn: 'root',
})
export class DataService extends BaseService {

  getJobsByType(jobType?:any, page?:any, size?:any,jobTypes?:any):Observable<ResponseEntityData<Job[]>>{
    const params = {
      page: page ? page : 0,
      size: size ? size : maxInt32,
      jobType: jobType ? jobType : 'ORG',
      jobTypes:jobTypes?jobTypes:''
    }
    return this.get(SERVICE_NAME.MODEL_PLAN + JOB_URLS.LIST_JOB_TYPE, { params: params }, MICRO_SERVICE.MODEL_PLAN);
  }

  getJobsByTypePlanYear(jobType?:any, page?:any, size?:any):Observable<ResponseEntityData<Job[]>>{
    const params = {
      page: page ? page : 0,
      size: size ? size : maxInt32,
      jobType: jobType ? jobType : 'ORG'
    }
    return this.get(SERVICE_NAME.MODEL_PLAN + urlStaffModule.listPlanYear, { params: params }, MICRO_SERVICE.MODEL_PLAN);
  }

  downloadFile(docId:string|number){
    return this.get(SERVICE_NAME.MODEL_PLAN + COMMON_URL.FILE_DOWNLOAD + docId, {responseType: 'arraybuffer'}, MICRO_SERVICE.MODEL_PLAN)
  }

  getLookupValue(lookupCode:string, flagStatus:number | '' = ''):Observable<ResponseEntity<LookupValue[]>>{
    const objSearch = {
      lookupCode: lookupCode,
      flagStatus:flagStatus
    };
    return this.get(SERVICE_NAME.MODEL_PLAN + COMMON_URL.LOOKUP_VALUE, { params: objSearch }, MICRO_SERVICE.MODEL_PLAN);
  }

  searchLookup(body:CriteriaSearch):Observable<ResponseEntityData<LookupValue[]>>{
    return this.post(SERVICE_NAME.MODEL_PLAN + COMMON_URL.LOOKUP_VALUE_SEARCH, body, {}, MICRO_SERVICE.MODEL_PLAN);
  }


}
