import { Injectable } from "@angular/core";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {urlStaffModule} from "./url/url.const";
import {MICRO_SERVICE, SERVICE_NAME} from "@hcm-mfe/shared/common/constants";



@Injectable({
  providedIn: 'root',
})
export class StaffPlanService extends BaseService {
  getListPlan(params?:any) {
    return this.get(SERVICE_NAME.MODEL_PLAN + urlStaffModule.listPlanYear, { params: params }, MICRO_SERVICE.MODEL_PLAN);
  }
  getListYear() {
    return this.get(SERVICE_NAME.MODEL_PLAN + urlStaffModule.getYear, undefined, MICRO_SERVICE.MODEL_PLAN);
  }
  downloadExcel(body:any) {
    return this.post(SERVICE_NAME.MODEL_PLAN + urlStaffModule.exportExcel, body, { responseType: 'arraybuffer' }, MICRO_SERVICE.MODEL_PLAN);
  }
  reportEmployee(params:any){
    return this.get(SERVICE_NAME.MODEL_PLAN + urlStaffModule.reportEmployee, {responseType: 'arraybuffer',params:params }, MICRO_SERVICE.MODEL_PLAN);
  }
}
