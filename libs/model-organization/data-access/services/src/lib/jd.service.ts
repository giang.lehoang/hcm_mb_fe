import {Injectable} from "@angular/core";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {HttpClient} from "@angular/common/http";
import {MICRO_SERVICE, SERVICE_NAME} from "@hcm-mfe/shared/common/constants";
import {
  ApproveJD,
  Attributes,
  Competency, JDCreate, JDEdit,
  JDsModel,
  Job, MajorLevel,
  ParamsJDSearch,
  ResponseEntity,
  ResponseEntityData
} from "@hcm-mfe/model-organization/data-access/models";
import {JD_URL} from "./constant/jd.url";
import {Observable} from "rxjs";
import {environment} from "@hcm-mfe/shared/environment";

@Injectable({
  providedIn: 'root',
})
export class JDService extends BaseService{
  constructor(readonly http: HttpClient) {
    super(http);
  }

  getJobByOrgId(orgId:number | string):Observable<ResponseEntity<Array<Job>>> {
    return this.get(SERVICE_NAME.MODEL_PLAN + JD_URL.GET_JOB_ORG + orgId, {}, MICRO_SERVICE.MODEL_PLAN);
  }
  getAttribute(orgId:number):Observable<ResponseEntity<Attributes[]>> {
    const requestParam = {
      orgId: orgId,
    }
    return this.get(SERVICE_NAME.MODEL_PLAN + JD_URL.GET_ATRIB, { params: requestParam }, MICRO_SERVICE.MODEL_PLAN);
  }

  getJDs(params:ParamsJDSearch) {
    return this.get(SERVICE_NAME.MODEL_PLAN + JD_URL.GET_LIST_JD, { params: params }, MICRO_SERVICE.MODEL_PLAN);
  }

  getCompetentcy(params:{jobGroup:string, positionId: string|number}): Observable<ResponseEntity<Competency[]>> {
    return this.get(SERVICE_NAME.MODEL_PLAN + JD_URL.getCompetency, { params: params }, MICRO_SERVICE.MODEL_PLAN);
  }

  deleteJDs(params:{
    psdId: number
  }) {
    return this.delete(SERVICE_NAME.MODEL_PLAN + JD_URL.JD_ROOT, { params: params }, MICRO_SERVICE.MODEL_PLAN);
  }

  exportExcel(body:ParamsJDSearch) {
    return this.post(SERVICE_NAME.MODEL_PLAN + JD_URL.EXPORT_EXCEL, body, { responseType: 'arraybuffer' }, MICRO_SERVICE.MODEL_PLAN);
  }
  getMajorLevel(): Observable<ResponseEntity<MajorLevel[]>> {
    return this.get(SERVICE_NAME.MODEL_PLAN + JD_URL.majorLevel, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  initJd(body: FormData) {
    return this.post(SERVICE_NAME.MODEL_PLAN + JD_URL.jdInit, body, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  // pendingJd(body:JDCreate){
  //   return this.put(SERVICE_NAME.MODEL_PLAN + urlJDsModule.jdPending, body, MICRO_SERVICE.MODEL_PLAN);
  // }

  getListApprove(params:any) {
    return this.get(SERVICE_NAME.MODEL_PLAN + JD_URL.approveList, { params: params }, MICRO_SERVICE.MODEL_PLAN);
  }

  exportApprove(body:any) {
    return this.post(SERVICE_NAME.MODEL_PLAN + JD_URL.approveExport, body, { responseType: 'arraybuffer' }, MICRO_SERVICE.MODEL_PLAN);
  }

  rejectJDs(body:ApproveJD) {
    return this.post(SERVICE_NAME.MODEL_PLAN + JD_URL.rejectJDs, body, {}, MICRO_SERVICE.MODEL_PLAN);
  }
  approveJDs(body:ApproveJD) {
    return this.post(SERVICE_NAME.MODEL_PLAN + JD_URL.approveJDs, body, {}, MICRO_SERVICE.MODEL_PLAN);
  }
  approvedAll(body:any) {
    return this.post(SERVICE_NAME.MODEL_PLAN + JD_URL.approvedAll, body, {}, MICRO_SERVICE.MODEL_PLAN);
  }
  getDetailJD(id: number): Observable<JDEdit> {
    return this.get(SERVICE_NAME.MODEL_PLAN + JD_URL.JD_ROOT + `/${id}`, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  updateJDs(body: FormData) {
    return this.put(SERVICE_NAME.MODEL_PLAN + JD_URL.jdUpdate, body, MICRO_SERVICE.MODEL_PLAN);
  }

  exportTemplate() {
    return `${SERVICE_NAME.MODEL_PLAN}${JD_URL.EXPORT_TEMP}`;
  }

  importTemplate() {
    return `${SERVICE_NAME.MODEL_PLAN}${JD_URL.IMPORT_TEMP}`;
  }

}
