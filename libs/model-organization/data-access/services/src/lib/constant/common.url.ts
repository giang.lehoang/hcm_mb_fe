export const COMMON_URL = {
  FILE_DOWNLOAD: '/v1.0/mpl/files/download/',
  LOOKUP_VALUE: '/v1.0/lookup-values',
  LOOKUP_VALUE_SEARCH : '/v1.0/lookup-values/search',
  LOOKUP_CODE: '/v1.0/lookup-codes/all'
}
