export const CATEGORY_URL = {
  CATEGORY_MANAGE: '/v1.0/lookup-values/category-manage',
  UPDATE_CATEGORY: '/v1.0/lookup-values/common',
  DETAIL_CATEGORY: '/v1.0/lookup-values/common',
  LIST_CATEGORY: '/v1.0/lookup-values/search-category',
  EXPORT_CATEGORY: '/v1.0/lookup-values/category-excel',
  CATEGORY_WITH_CONFIG: '/v1.0/lookup-codes/list/get-by-config',
  EDIT_CATEGORY: '/v1.0/lookup-values/common-update',
  IMPORT_EXCEL: '/v1.0/lookup-value/import',
  DOWNLOAD_TEMP: '/v1.0/lookup-values/download-template'
}
