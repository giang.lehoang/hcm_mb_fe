export const URL_LINE = {
  JOB_BY_ORG : '/jobs/findAllByOrgGroupCN',
  JOB_BY_ORG_ID : '/positions/findAllAndJobsByOrgId',
  LINE_VIEW: '/line-view/filter',
  DELETE_LINE: '/position-groups/delete-with-child',
  LINE_DETAIL: '/v1.0/position-groups/line-details',
  SEARCH_LINE: '/position-groups/search-lines',
  UPDATE_LINE: '/position-groups/update-line',
  CREATE_LINE: '/position-groups/create-line',
  POS_GROUP_LIST: '/position-groups/list',
  POS_GROUP_SEARCH: '/position-groups/search',
  LINE_PENDING: '/v1.0/position-groups/waiting-approval',
  LINE_VIEW_PENDING: '/position-groups/filter-pending-status',
  LINE_APPROVE: '/position-groups/approved',
  LINE_REJECT: '/position-groups/rejected',
  JOB_BY_CN_ASSIGN: '/v1.0/position-group/job-cn',
  JOB_BY_ORG_ID_ASSIGN : '/positions/findAllAndJobsByOrgIds'
}
