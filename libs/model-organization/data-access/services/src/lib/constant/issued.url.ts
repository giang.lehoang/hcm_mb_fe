export const ISSUED_URLS = {
  LIST_ISSUED : '/v1.0/organizations/issued-organizations',
  EXPORT_ISSUED: '/v1.0/organizations/export-excell',
  IMPORT_EMPLOYEE: 'hcm-model-plan/v1.0/employees/import-excel',
  CREATE_GROUP_MEMBER: '/group-deliveries/create-delivery',
  UPDATE_GROUP_MEMBER: '/group-deliveries/update-delivery',
  DELETE_GROUP_MEMBER: '/group-deliveries/delete-delivery/',
  EMPLOYEE_TEMPLATE: '/files/template',
  LIST_GROUP_AND_MEMBER: '/group-deliveries/getGroupsAndMembersByUser',
  PUBLICIZE_ISSUED: '/v1.0/deliveries/publicize',
  FILTER_ISSUED: '/v1.0/deliveries-view/filter',
  EXPORT_EXCEL_ISSUED: '/v1.0/projects/excel',
  HIS_ISSUED: '/v1.0/organizations/publicized-history'
}
