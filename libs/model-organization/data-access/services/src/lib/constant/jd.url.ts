export const JD_URL = {
  GET_JOB_ORG: '/positions/findAllAndJobsByOrgId/',
  GET_ATRIB: '/v1.0/organization_attributes',
  GET_LIST_JD: '/v1.0/jds/list',
  JD_ROOT: '/v1.0/jds',
  getCompetency: '/v1.0/competences',
  EXPORT_EXCEL: '/v1.0/jds/list/export-excel',
  majorLevel: '/v1.0/major-level',
  jdInit: '/v1.0/jds/initialize',
  jdPending: '/v1.0/jds/pending',
  approveList: '/v1.0/jds/list/approved/search',
  approveExport: '/v1.0/jds/list/excel-approved',
  rejectJDs: '/v1.0/list/reject-jds',
  approveJDs: '/v1.0/list/approved-jds',
  approvedAll: '/v1.0/list/approved/all',
  jdUpdate: '/v1.0/jds/update-initialize',
  EXPORT_TEMP: '/v1.0/jds/template-excel',
  IMPORT_TEMP: '/v1.0/jds/import-excel'
}
