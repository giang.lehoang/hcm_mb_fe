export const PROJECT_URL = {
  LIST_CRITERIA: '/v1.0/lookup-values/criteria',
  LIST_CRITERIA_PRJ: '/v1.0/lookup-values/list-criteria',
  PRJ_ROOT: '/v1.0/projects',
  PROJECT_SEARCH_LIST: '/v1.0/projects/search',
  EXPORT_PROJECT: '/v1.0/projects/export',
  PROJECT_IMPL_SEARCH: '/v1.0/projects/implementation-search',
  CHECK_CRITERIA : '/v1.0/lookup-values/check-criteria-project-used',
  CRITERIA_CATEGORY : '/v1.0/lookup-values/criteria-category',
  PRJ_APPROVE : '/v1.0/projects/approve',
  EMPLOYEE_SEARCH : '/v1.0/plans/all/employee',
  JOB_PRJ : '/v1.0/plans/jobs',
  CREATE_PRJ_MEMBER : '/v1.0/plans/create-project-member',
  PRJ_IMPL_DETAIL : '/v1.0/plans/detail'
}
