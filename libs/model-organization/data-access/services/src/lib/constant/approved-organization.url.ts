export const APPROVE_ORGANIZATION = {
  ORG_SEARCH: '/v1.0/organization-view/search',
  ORG_APPROVE_LIST:'/v1.0/organizations/approval/list',
  ORG_INIT_DETAIL: '/v1.0/organizations/init-organization-details',
  LIST_ORG_DETAIL:'/v1.0/organizations/organization-details-approval/',
  APPROVE_ALL_ORG: '/v1.0/organizations/approved/all',
  APPROVE_DETAIL: '/v1.0/organizations/organization-details-approval/',
  DOWNLOAD_FILE: '/v1.0/mpl/files/download',
  APPROVE_ORG: '/v1.0/organizations/approved',
  REJECT_ORG: '/v1.0/organizations/rejected'
}
