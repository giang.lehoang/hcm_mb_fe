import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {
  ApproveJob,
  AssignJob, AssignPosRequest,
  AssignPosRes, BaseSelect,
  Job,
  JobAssignRes,
  JobCreate,
  JobPosition,
  JobResponse,
  JobView,
  PositionDetail,
  PositionGroup,
  PositionGroupSearch,
  PosNunAssign,
  PosNunAssignRequest, PosUnAssignDetail, PosUnAssignDetailResponse,
  RequestPosition,
  ResponseEntity,
  ResponseEntityData,
  SavePosRes,
  SearchJob,
  SearchPosModel
} from "@hcm-mfe/model-organization/data-access/models";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {Observable} from "rxjs";
import {maxInt32, MICRO_SERVICE, SERVICE_NAME} from "@hcm-mfe/shared/common/constants";
import {JOB_URLS} from "./constant/job.url";

@Injectable({
  providedIn: 'root',
})
export class JobService extends BaseService {
  constructor(readonly http: HttpClient) {
    super(http);
  }

  filterJobs(params:SearchJob): Observable<ResponseEntityData<SearchJob[]>> {
    return this.get(SERVICE_NAME.MODEL_PLAN + JOB_URLS.FILTER_JOB, {params:params}, MICRO_SERVICE.MODEL_PLAN)
  }

  getJobPosition(body:SearchJob): Observable<ResponseEntityData<JobAssignRes[]>> {
    return this.post(SERVICE_NAME.MODEL_PLAN + JOB_URLS.JOB_POS_ALL, body,{}, MICRO_SERVICE.MODEL_PLAN)
  }

  exportExcel(body:SearchJob) {
    return this.post(SERVICE_NAME.MODEL_PLAN + JOB_URLS.JOB_EXCEL, body, {
      responseType: 'arraybuffer'},  MICRO_SERVICE.MODEL_PLAN)
  }

  exportAssignExcel(body:SearchJob) {
    return this.post(SERVICE_NAME.MODEL_PLAN + JOB_URLS.JOB_EXPORT, body, {
      responseType: 'arraybuffer'},  MICRO_SERVICE.MODEL_PLAN)
  }

  rejectJob(body:ApproveJob) {
    return this.post(SERVICE_NAME.MODEL_PLAN + JOB_URLS.JOB_REJECT, body, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  getListJobApprove(body:SearchJob): Observable<ResponseEntityData<JobPosition[]>> {
    return this.post(SERVICE_NAME.MODEL_PLAN + JOB_URLS.LIST_JOB_POS, body, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  approveJob(body:ApproveJob) {
    return this.post(SERVICE_NAME.MODEL_PLAN + JOB_URLS.JOB_APPROVE, body, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  createJob(body:JobCreate) {
    return this.post(SERVICE_NAME.MODEL_PLAN + JOB_URLS.JOB_ROOT, body,{}, MICRO_SERVICE.MODEL_PLAN);
  }

  // getJobDetail(id) {
  //   return this.http.get(`${environment.baseUrl}${url.url_enpoint_job_create}/${id}`);
  // }

  updateJob(body:JobCreate) {
    return this.put(SERVICE_NAME.MODEL_PLAN + JOB_URLS.JOB_ROOT, body, MICRO_SERVICE.MODEL_PLAN);
  }

  deleteJob(id:number) {
    const params = {jobId: id};
    return this.delete(SERVICE_NAME.MODEL_PLAN + JOB_URLS.JOB_ROOT + '/' + id, {params: params}, MICRO_SERVICE.MODEL_PLAN);
  }


  deleteJobPos(id:number|string) {
    return this.delete(SERVICE_NAME.MODEL_PLAN + JOB_URLS.JOB_POS + '/' + id, {}, MICRO_SERVICE.MODEL_PLAN)
  }

  getJobType(type:string, page?:number, size?:number):Observable<ResponseEntityData<JobResponse[]>> {
    const objectSubmit = {
      page: page ? page : 0,
      size: size ? size : maxInt32,
      jobType: type
    }
    return this.get(SERVICE_NAME.MODEL_PLAN + JOB_URLS.LIST_JOB_TYPE, {params: objectSubmit}, MICRO_SERVICE.MODEL_PLAN);
  }

  setJobForProject(request:AssignJob) {
    return this.post(SERVICE_NAME.MODEL_PLAN + JOB_URLS.JOB_POS, request, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  getListJob(type:string):Observable<ResponseEntity<JobView[]>> {
    const objectSubmit = {
      jobType: type
    }
    return this.get(SERVICE_NAME.MODEL_PLAN + JOB_URLS.JOB_SEARCH_VIEW, {params: objectSubmit}, MICRO_SERVICE.MODEL_PLAN);
  }

  checkJobUsed(id:number): Observable<ResponseEntity<boolean>> {
    const params = { jobId: id };
    return this.get(SERVICE_NAME.MODEL_PLAN + JOB_URLS.CHECK_JOB_USED, {params: params}, MICRO_SERVICE.MODEL_PLAN);
  }

  getListPositionGroup(params:PositionGroupSearch): Observable<ResponseEntityData<PositionGroup[]>> {
    return this.post(SERVICE_NAME.MODEL_PLAN + JOB_URLS.positionGroupList, params, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  deletePositionGroup(id:number) {
    return this.delete(SERVICE_NAME.MODEL_PLAN + JOB_URLS.positionGroupDelete + id, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  createPositionGroup(body:RequestPosition) {
    return this.post(SERVICE_NAME.MODEL_PLAN + JOB_URLS.positionGroupCreate, body, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  updatePositionGroup(body:RequestPosition) {
    return this.put(SERVICE_NAME.MODEL_PLAN + JOB_URLS.positionGroupUpdate, body, MICRO_SERVICE.MODEL_PLAN);
  }

  detailPositionGroup(id:number): Observable<ResponseEntity<RequestPosition>> {
    return this.get(SERVICE_NAME.MODEL_PLAN + JOB_URLS.positionGroupDetail + id, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  detailPositionAssign(id:number | string): Observable<ResponseEntity<PositionDetail>> {
    return this.get(SERVICE_NAME.MODEL_PLAN + JOB_URLS.positionAssignDetail + id, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  assignPosition(body:AssignPosRes) {
    return this.post(SERVICE_NAME.MODEL_PLAN + JOB_URLS.positionAssign, body, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  getListTypePosition() {
    return this.get(SERVICE_NAME.MODEL_PLAN + JOB_URLS.listTypePosition, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  getListPosition(body:SearchPosModel) {
    return this.post(SERVICE_NAME.MODEL_PLAN + JOB_URLS.positionList, body, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  deletePosition(body: { posId:number }) {
    return this.put(SERVICE_NAME.MODEL_PLAN + JOB_URLS.deletePosition, body, MICRO_SERVICE.MODEL_PLAN);
  }

  savePosition(body:SavePosRes[]) {
    return this.post(SERVICE_NAME.MODEL_PLAN + JOB_URLS.savePosition, body, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  exportPosition(body:SearchPosModel) {
    return this.post(SERVICE_NAME.MODEL_PLAN + JOB_URLS.exportPosition, body, {
      responseType: 'arraybuffer'
    }, MICRO_SERVICE.MODEL_PLAN);
  }

  listJobPosUnassign():Observable<ResponseEntity<Job[]>>{
    return this.get(SERVICE_NAME.MODEL_PLAN + JOB_URLS.JOB_NON_ASSIGN, {}, MICRO_SERVICE.MODEL_PLAN)
  }

  listPosUnassign(request:PosNunAssignRequest): Observable<ResponseEntityData<PosNunAssign[]>>{
    return this.get(SERVICE_NAME.MODEL_PLAN + JOB_URLS.LIST_POS_UNASSIGN, {params: request}, MICRO_SERVICE.MODEL_PLAN)
  }

  exportPositionUnAssign(request:PosNunAssignRequest) {
    return this.post(SERVICE_NAME.MODEL_PLAN + JOB_URLS.POS_UN_EXCEL, request, {
      responseType: 'arraybuffer'
    }, MICRO_SERVICE.MODEL_PLAN);
  }

  getDetailPosUnAssign(body:PosUnAssignDetail):Observable<ResponseEntity<PosUnAssignDetailResponse[]>>{
    return this.post(SERVICE_NAME.MODEL_PLAN + JOB_URLS.POS_UN_DETAIL, body, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  listPosGroupName(type:string):Observable<ResponseEntity<BaseSelect[]>>{
    const request = {
      type:type
    }
    return this.get(SERVICE_NAME.MODEL_PLAN + JOB_URLS.LIST_POS_GROUP_NAME, {params: request}, MICRO_SERVICE.MODEL_PLAN)
  }

  assignPosUnassign(body:AssignPosRequest){
    return this.post(SERVICE_NAME.MODEL_PLAN + JOB_URLS.POS_UN_ASSIGN, body, {}, MICRO_SERVICE.MODEL_PLAN)
  }

}
