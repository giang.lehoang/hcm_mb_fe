import { Injectable } from "@angular/core";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {MICRO_SERVICE, SERVICE_NAME, url} from "@hcm-mfe/shared/common/constants";
import {urlStaffModule} from "./url/url.const";


@Injectable({
  providedIn: 'root',
})
export class AdjustedStaffService extends BaseService {
  getListAdjusted(params: any) {
    const requestParam = {
      orgId: params.orgId,
      jobId: params.jobId,
      page: params.page,
      size: params.size,
      inputType: params.inputType
    }
    return this.get(SERVICE_NAME.MODEL_PLAN + urlStaffModule.planList, { params: requestParam }, MICRO_SERVICE.MODEL_PLAN);
  }

  downloadExcel(body: any) {
    return this.post(SERVICE_NAME.MODEL_PLAN + urlStaffModule.export, body, {
      responseType: 'arraybuffer',
    }, MICRO_SERVICE.MODEL_PLAN);
  }

  getUrlUpload(type:string): string {
    const url =  type === 'DC'? `${SERVICE_NAME.MODEL_PLAN}${urlStaffModule.uploadAdjusted}`:`${SERVICE_NAME.MODEL_PLAN}${urlStaffModule.uploadCorrection}`
    return url;
  }

  approveTranfer(body: any) {
    return this.post(SERVICE_NAME.MODEL_PLAN + urlStaffModule.waitingApproval, body,{},MICRO_SERVICE.MODEL_PLAN);
  }
  approveTranferCorection(body: FormData) {
    return this.post(SERVICE_NAME.MODEL_PLAN + urlStaffModule.tranferApproveCorrection, body,{},MICRO_SERVICE.MODEL_PLAN);
  }
}
