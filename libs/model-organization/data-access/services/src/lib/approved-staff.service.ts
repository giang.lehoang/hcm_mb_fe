import { Injectable } from "@angular/core";
import { urlStaffModule } from "./url/url.const";
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { MICRO_SERVICE, SERVICE_NAME } from '@hcm-mfe/shared/common/constants';

@Injectable({
  providedIn: 'root',
})
export class ApproveStaffService extends BaseService {

  getListStaffApprove(params:any) {
    return this.get(SERVICE_NAME.MODEL_PLAN + urlStaffModule.approveStaffList, { params: params }, MICRO_SERVICE.MODEL_PLAN);
  }

  transferApproved(body:any) {
    return this.post(SERVICE_NAME.MODEL_PLAN + urlStaffModule.transferApproved, body, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  reject(body:any) {
    return this.post(SERVICE_NAME.MODEL_PLAN + urlStaffModule.rejectApproved, body, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  transferAll(body:any) {
    return this.post(SERVICE_NAME.MODEL_PLAN + urlStaffModule.transferApprovedAll, body, {}, MICRO_SERVICE.MODEL_PLAN);
  }
  downloadFile(id:string) {
    return this.get(`${SERVICE_NAME.MODEL_PLAN + urlStaffModule.downloadFile}/${id}`, {responseType: 'arraybuffer'},MICRO_SERVICE.MODEL_PLAN);
  }
}
