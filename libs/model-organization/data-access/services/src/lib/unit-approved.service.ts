import { Injectable } from '@angular/core';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { MICRO_SERVICE, SERVICE_NAME } from '@hcm-mfe/shared/common/constants';
import { APPROVE_ORGANIZATION } from './constant/approved-organization.url';

@Injectable({
  providedIn: 'root',
})
export class UnitApprovedService extends BaseService{

  getListorganizationView(params:any) {
    return this.get(
      SERVICE_NAME.MODEL_PLAN + APPROVE_ORGANIZATION.ORG_SEARCH,
      { params: params },MICRO_SERVICE.MODEL_PLAN
    );
  }

  getListorganizationViewApprove(params:any) {
    return this.get(
      SERVICE_NAME.MODEL_PLAN + APPROVE_ORGANIZATION.ORG_APPROVE_LIST,
      { params: params },MICRO_SERVICE.MODEL_PLAN
    );
  }

  getUnitInitDetail(id:any) {
    return this.get(`${SERVICE_NAME.MODEL_PLAN}${APPROVE_ORGANIZATION.ORG_INIT_DETAIL}/${id}`,{},MICRO_SERVICE.MODEL_PLAN);
  }

  getListOrgDetail(id:any) {
    return this.get(SERVICE_NAME.MODEL_PLAN + APPROVE_ORGANIZATION.LIST_ORG_DETAIL+ id,{},MICRO_SERVICE.MODEL_PLAN);
  }
  approveALLOrganization() {
    return this.post(SERVICE_NAME.MODEL_PLAN + APPROVE_ORGANIZATION.LIST_ORG_DETAIL, {},MICRO_SERVICE.MODEL_PLAN);
  }
  getDetailOrgApprove(id:any) {
    return this.get(SERVICE_NAME.MODEL_PLAN + APPROVE_ORGANIZATION.APPROVE_DETAIL+ id,{},MICRO_SERVICE.MODEL_PLAN);
  }

  downloadFile(id:any) {
    return this.get(`${SERVICE_NAME.MODEL_PLAN}${APPROVE_ORGANIZATION.DOWNLOAD_FILE}/${id}`, {responseType: 'arraybuffer'},MICRO_SERVICE.MODEL_PLAN)
  }
  approveOrg(request:any) {
    return this.post(SERVICE_NAME.MODEL_PLAN + APPROVE_ORGANIZATION.APPROVE_ORG, request,{},MICRO_SERVICE.MODEL_PLAN);
  }
  rejectedOrg(request:any) {
    return this.post(SERVICE_NAME.MODEL_PLAN + APPROVE_ORGANIZATION.REJECT_ORG, request,{},MICRO_SERVICE.MODEL_PLAN);
  }
}
