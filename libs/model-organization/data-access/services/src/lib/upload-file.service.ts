import {EventEmitter, Injectable, OnDestroy} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {OnClickCallback} from "ng-zorro-antd/modal/modal-types";
import {CustomToastrService} from "@hcm-mfe/shared/core";
import {RequestFileInfo} from "@hcm-mfe/model-organization/data-access/models";
import { UploadFileComponent } from '@hcm-mfe/shared/ui/upload-file-organization';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
@Injectable({
  providedIn: 'root'
})
export class UploadFileService implements OnDestroy {
  modalUpload: NzModalRef | null;
  eventClose: EventEmitter<unknown> | null;

  constructor(
    private modal: NzModalService,
    private translate: TranslateService,
    private toastr: CustomToastrService
  ) {
    this.modalUpload = null;
    this.eventClose = null;
  }

  ngOnDestroy(): void {
    this.eventClose?.complete();
  }

  showModal(fun: EventEmitter<UploadFileComponent> | OnClickCallback<UploadFileComponent>, httpRequest: RequestFileInfo, message?: string, data?: boolean) {
    this.eventClose = new EventEmitter<unknown>();
    this.modalUpload = this.modal.create({
      nzTitle: this.translate.instant('shared.uploadFile.label.title'),
      nzStyle: {
        width: '500px',
        height: '321px',
      },
      nzComponentParams: {
        fileType: ['XLSX'],
        request: httpRequest,
        isData: data,
      },
      nzWrapClassName: 'delete-popup-container',
      nzContent: UploadFileComponent,
      nzBodyStyle: {
        padding: '0px',
        border: 'none',
      },
      nzOnOk: fun,
      nzFooter: null,
    });

    this.modalUpload.componentInstance.eventClose.subscribe((data: boolean) => {
      if (data) {
        this.toastr.success(message);
        this.eventClose?.emit(data);
        this.modalUpload?.triggerOk();
      }
      this.modalUpload?.destroy();
      this.eventClose?.complete();
    });
  }
}
