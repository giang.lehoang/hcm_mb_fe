import {Injectable} from "@angular/core";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {MICRO_SERVICE, SERVICE_NAME} from "@hcm-mfe/shared/common/constants";
import {ORGANIZATION_URL} from "./constant/organization.url";
import {
  ContentOrg,
  DetailListTemp, InitScreenSearch,
  OrganizationDetail, OrganizationTreeNode, OrgLine, RequestOrgName,
  ResponseEntity,OrganizationByName,
  ResponseEntityData, SearchTemplate,
  Template
} from "@hcm-mfe/model-organization/data-access/models";
import {Observable} from "rxjs";
import {UrlConstant} from "@hcm-mfe/goal-management/data-access/services";

@Injectable({
  providedIn: 'root',
})
export class OrganizationService extends BaseService {
  constructor(readonly http: HttpClient) {
    super(http);
  }

  getListOrgDetail(orgLevel:number = 3, orgGroup:string = 'ho'):Observable<ResponseEntityData<OrganizationDetail[]>>{
    const objSearch = {
      orgLevel: orgLevel,
      orgGroup: orgGroup,
    };
    return this.get(SERVICE_NAME.MODEL_PLAN + ORGANIZATION_URL.ORG_LIST_DT, {params: objSearch}, MICRO_SERVICE.MODEL_PLAN);
  }

  getListTemplate(param?:SearchTemplate):Observable<ResponseEntity<Template[]>> {
    const pr = param ? param : {};
    return this.get(SERVICE_NAME.MODEL_PLAN + ORGANIZATION_URL.ORG_TEMP, {params:pr}, MICRO_SERVICE.MODEL_PLAN);
  }

  getListTemplateDetail(id:number|string):Observable<ResponseEntity<DetailListTemp[]>> {
    return this.get(SERVICE_NAME.MODEL_PLAN + ORGANIZATION_URL.ORG_ROOT + `/${id}/hierarchical`, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  getListOrg():Observable<ResponseEntity<OrgLine[]>>{
    return this.get(SERVICE_NAME.MODEL_PLAN + ORGANIZATION_URL.ORG_LIST, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  getOrganizationList(param:InitScreenSearch):Observable<ResponseEntityData<ContentOrg[]>> {
    return this.get(SERVICE_NAME.MODEL_PLAN + ORGANIZATION_URL.ORG_VIEW_SEARCH, {params: param}, MICRO_SERVICE.MODEL_PLAN);
  }

  deleteOrg(id:number) {
    const params = {orgId: id}
    return this.delete(SERVICE_NAME.MODEL_PLAN + ORGANIZATION_URL.ORG_DELETE, {params: params}, MICRO_SERVICE.MODEL_PLAN);
  }

  exportExcelOrg(request:InitScreenSearch) {
    return this.post(SERVICE_NAME.MODEL_PLAN + ORGANIZATION_URL.ORG_VIEW_EXPORT, request,
      {responseType: 'arraybuffer'}, MICRO_SERVICE.MODEL_PLAN);
  }

  getListLegalBranch() {
    return this.get(SERVICE_NAME.MODEL_PLAN + ORGANIZATION_URL.LEGAL_BRANCH, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  getDetailOrg(id:number | string) {
    return this.get(SERVICE_NAME.MODEL_PLAN + ORGANIZATION_URL.DETAIL_ORG + '/' + id, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  createOrganization(requestBody:FormData) {
    return this.post(SERVICE_NAME.MODEL_PLAN + ORGANIZATION_URL.CREATE_ORG, requestBody,
      { }, MICRO_SERVICE.MODEL_PLAN);
  }

  updateOrganization(requestBody:FormData) {
    return this.post(SERVICE_NAME.MODEL_PLAN + ORGANIZATION_URL.UPDATE_ORG, requestBody,
      { }, MICRO_SERVICE.MODEL_PLAN);
  }

  getTemplateDetail(templateId:number) {
    const params = {
      id: templateId,
    };
    return this.get(SERVICE_NAME.MODEL_PLAN + ORGANIZATION_URL.TEMPLATE_DETAIL, {params: params}, MICRO_SERVICE.MODEL_PLAN);
  }

  getOrgDetail(id:string | number, type:string){
    return this.post(SERVICE_NAME.MODEL_PLAN + ORGANIZATION_URL.TREE_ORG_DETAIL, {
      orgId: id,
        inputType: type
    }, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  adjustedOrg(body:FormData){
    return this.post(SERVICE_NAME.MODEL_PLAN + ORGANIZATION_URL.ADJUST_ORG, body, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  getSubTree(active:boolean ,orgId?:number):Observable<ResponseEntity<OrganizationTreeNode[]>>{
    const params = {
      orgId: orgId ? orgId : ''
    }
    let url:string;
    if(active){
      url = ORGANIZATION_URL.SUB_TREE_ACTIVE
    } else {
      url = ORGANIZATION_URL.SUB_TREE_INACTIVE
    }
    return this.get(SERVICE_NAME.MODEL_PLAN + url, {params: params}, MICRO_SERVICE.MODEL_PLAN);
  }

  updateTreeSeq(request:any){
    return this.post(SERVICE_NAME.MODEL_PLAN + ORGANIZATION_URL.UPDATE_TREE, request,
      {}, MICRO_SERVICE.MODEL_PLAN);
  }

  getOrganizationTreeByNameL(obj:RequestOrgName):Observable<ResponseEntity<OrganizationByName[]>> {
    if (!obj.is_author) {
      delete obj.is_author;
    }
    return this.get(`${UrlConstant.END_POINT_MODEL_PLAN.url_enpoint_organization_tree_get_by_name}`, {params: obj}, MICRO_SERVICE.MODEL_PLAN);
  }

}
