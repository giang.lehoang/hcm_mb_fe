import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import {
  EmployeeGroupCreate, HistoryOrg,
  IIssuedOrganization, IssuedRequest,
  Issues, JobSearch, ParamHis,
  ResponseEntity, ResponseEntityData,
  SearchIssued
} from "@hcm-mfe/model-organization/data-access/models";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {MICRO_SERVICE, SERVICE_NAME} from "@hcm-mfe/shared/common/constants";
import {ISSUED_URLS} from "./constant/issued.url";
import {environment} from "@hcm-mfe/shared/environment";

@Injectable({
  providedIn: 'root',
})
export class IssuedService extends BaseService{
  public listIdSelected:Array<Issues> = [];

  constructor(readonly http: HttpClient) {
    super(http);
  }

  searchListIssed(body:SearchIssued): Observable<ResponseEntity<IIssuedOrganization[]>> {
    return this.post(SERVICE_NAME.MODEL_PLAN + ISSUED_URLS.LIST_ISSUED, body, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  exportListIssed(body:SearchIssued) {
    return this.post(SERVICE_NAME.MODEL_PLAN + ISSUED_URLS.EXPORT_ISSUED, body, {
      responseType: 'arraybuffer',
    }, MICRO_SERVICE.MODEL_PLAN);
  }

  getUrlUploadFile(){
      return environment.baseUrl + ISSUED_URLS.IMPORT_EMPLOYEE;
  }

  createGroupMember(body:EmployeeGroupCreate){
    return this.post(SERVICE_NAME.MODEL_PLAN + ISSUED_URLS.CREATE_GROUP_MEMBER, body, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  updateGroupMember(body:EmployeeGroupCreate){
    return this.put(SERVICE_NAME.MODEL_PLAN + ISSUED_URLS.UPDATE_GROUP_MEMBER, body, MICRO_SERVICE.MODEL_PLAN);
  }

  deleteGroupMember(id:number){
    return this.delete(SERVICE_NAME.MODEL_PLAN + `${ISSUED_URLS.DELETE_GROUP_MEMBER}${id}`, {}, MICRO_SERVICE.MODEL_PLAN );
  }

  getListIdSelected():Issues[] {
    return this.listIdSelected;
  }

  dowloadExampleFile(){
    return this.get(SERVICE_NAME.MODEL_PLAN + ISSUED_URLS.EMPLOYEE_TEMPLATE, {
      responseType: 'arraybuffer'}, MICRO_SERVICE.MODEL_PLAN);
  }

  getGroupsAndMembersByUser(user:string){
      const param = {
        gdeUser : user
      }
      return this.get(SERVICE_NAME.MODEL_PLAN + ISSUED_URLS.LIST_GROUP_AND_MEMBER, {params: param}, MICRO_SERVICE.MODEL_PLAN);
  }

  tranfer(body:IssuedRequest){
    return this.post(SERVICE_NAME.MODEL_PLAN + ISSUED_URLS.PUBLICIZE_ISSUED, body, {} ,MICRO_SERVICE.MODEL_PLAN)
  }

  filterIssuedProject(params:JobSearch) {
    return this.get(SERVICE_NAME.MODEL_PLAN + ISSUED_URLS.FILTER_ISSUED, {params: params}, MICRO_SERVICE.MODEL_PLAN);
  }

  exportExcelIssued(body:JobSearch) {
    return this.post(SERVICE_NAME.MODEL_PLAN + ISSUED_URLS.EXPORT_EXCEL_ISSUED, body, {
      responseType: 'arraybuffer',
    }, MICRO_SERVICE.MODEL_PLAN);
  }

  setListIdSelected(listIdSelected:Issues[]) {
    this.listIdSelected = listIdSelected;
  }

  getListHistoryIssued(params:ParamHis): Observable<ResponseEntityData<HistoryOrg[]>>{
    return this.get(SERVICE_NAME.MODEL_PLAN + ISSUED_URLS.HIS_ISSUED, {params: params}, MICRO_SERVICE.MODEL_PLAN);
  }

}
