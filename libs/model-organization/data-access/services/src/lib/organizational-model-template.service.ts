import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Area, Type } from '@hcm-mfe/model-organization/data-access/models';
import { maxInt32 } from '@hcm-mfe/shared/common/enums';
import { MICRO_SERVICE, SERVICE_NAME } from '@hcm-mfe/shared/common/constants';
import { TEMPLATE_ORGANIZATION } from './constant/organization-model-template.url';
import { BaseService } from '@hcm-mfe/shared/common/base-service';

@Injectable({
  providedIn: 'root',
})
export class OrganizationalModelTemplateService extends BaseService{
  private idTemplate:any;
  private areas: Array<Area> = [];
  private types: Array<Type> = [];
  private jobs: any;

  getListTemplate(params:any) {
    return this.get(SERVICE_NAME.MODEL_PLAN + TEMPLATE_ORGANIZATION.TEMPLATE_SEARCH, { params: params },MICRO_SERVICE.MODEL_PLAN);
  }

  getTemplateDetail(templateId:any) {
    const params = {
      id: templateId,
    };
    return this.get(SERVICE_NAME.MODEL_PLAN + TEMPLATE_ORGANIZATION.TEMPLATE_DETAILS, { params: params },MICRO_SERVICE.MODEL_PLAN);
  }

  getAllTemplates() {
    const params = {
      page: '0',
      size: String(maxInt32),
    };
    return this.get(SERVICE_NAME.MODEL_PLAN + TEMPLATE_ORGANIZATION.GET_ALL, {params:params},MICRO_SERVICE.MODEL_PLAN);
  }
  deleteTemplates(id: any) {
    return this.delete(SERVICE_NAME.MODEL_PLAN + TEMPLATE_ORGANIZATION.DELETE_TEMPLATE + `/${id}`,{},MICRO_SERVICE.MODEL_PLAN);
  }

  updateTemplate(request:any) {
    return this.put(SERVICE_NAME.MODEL_PLAN + TEMPLATE_ORGANIZATION.UPDATE_TEMPALTE, request,MICRO_SERVICE.MODEL_PLAN);
  }

  adjustedOrg(body:any){
    const header = new HttpHeaders()
    return this.post(SERVICE_NAME.MODEL_PLAN + TEMPLATE_ORGANIZATION.ADJUSTED_ORG, body, {
      headers: header,
    },MICRO_SERVICE.MODEL_PLAN);
  }

  createTemalpate(request:any) {
    return this.post(SERVICE_NAME.MODEL_PLAN + TEMPLATE_ORGANIZATION.CREATRE_TEMPALTE, request,{},MICRO_SERVICE.MODEL_PLAN);
  }

  createOrganization(requestBody:any) {
    const header = new HttpHeaders()
    return this.post(SERVICE_NAME.MODEL_PLAN + TEMPLATE_ORGANIZATION.CREATE_ORG, requestBody, {
      headers: header,
    },MICRO_SERVICE.MODEL_PLAN);
  }
  updateOrganization(requestBody:any) {
    const header = new HttpHeaders()
    return this.post(SERVICE_NAME.MODEL_PLAN + TEMPLATE_ORGANIZATION.UPDATE_ORG, requestBody, {
      headers: header,
    },MICRO_SERVICE.MODEL_PLAN);
  }
  getOrgDetail(id:any, type:any){
    return this.post(SERVICE_NAME.MODEL_PLAN + TEMPLATE_ORGANIZATION.DETAILS_ORG, {
      orgId: id,
      inputType: type
    },MICRO_SERVICE.MODEL_PLAN);
  }

  deleteOrg(id:any) {
    const params = {orgId: id}
    return this.delete(SERVICE_NAME.MODEL_PLAN + TEMPLATE_ORGANIZATION.DELETE_ORG, {params: params},MICRO_SERVICE.MODEL_PLAN);
  }

  getDetailOrgApprove(id:any, type:any){
    if(type=="TM"){
      return this.get(SERVICE_NAME.MODEL_PLAN + TEMPLATE_ORGANIZATION.INIT_ORG_DETAIL + id,{},MICRO_SERVICE.MODEL_PLAN);
    } else {
      return this.get(SERVICE_NAME.MODEL_PLAN + TEMPLATE_ORGANIZATION.DETAIL_APPROVAL + id,{},MICRO_SERVICE.MODEL_PLAN);
    }

  }
  getDetailOrg(id:any) {
    return this.get(SERVICE_NAME.MODEL_PLAN + TEMPLATE_ORGANIZATION.GET_DETAIL_ORG + id,{},MICRO_SERVICE.MODEL_PLAN);
  }
  getDetailOrgApproveCorrection(id:any){
    return this.get(SERVICE_NAME.MODEL_PLAN + TEMPLATE_ORGANIZATION.DETAIL_ORG_CORRECTION + id,{},MICRO_SERVICE.MODEL_PLAN);
  }

  getListHistoryIssued(params:any): Observable<any>{
    return this.get(SERVICE_NAME.MODEL_PLAN + TEMPLATE_ORGANIZATION.HISTORY_ISSUED, {params: params},MICRO_SERVICE.MODEL_PLAN);
  }

  exportAsExcelFile(id:any, page:any, size:any){
    const param = {
      orgId: id,
      page: page,
      size: size
    }
    return this.post(SERVICE_NAME.MODEL_PLAN + TEMPLATE_ORGANIZATION.HISTORY_ISSUED_EXPORT,
      {}, {responseType: 'arraybuffer', params:param},MICRO_SERVICE.MODEL_PLAN);
  }
  exportListOrganization(param:any) {
    return this.post(
      SERVICE_NAME.MODEL_PLAN + TEMPLATE_ORGANIZATION.EXPORT_LIST_INFO_ORG, param,
      { responseType: 'arraybuffer'},MICRO_SERVICE.MODEL_PLAN
    );
  }
  getListInfoOrganization(param:any): Observable<any> {
    return this.get(SERVICE_NAME.MODEL_PLAN + TEMPLATE_ORGANIZATION.ORG_INFO, { params: param },MICRO_SERVICE.MODEL_PLAN);
  }

  uploadLogo(requestBody:any) {
    return this.post(SERVICE_NAME.MODEL_PLAN + TEMPLATE_ORGANIZATION.UPLOAD_LOGO, requestBody,{},MICRO_SERVICE.MODEL_PLAN)
  }
  downloadLogo(id:any) {
    return this.get('http://10.1.16.181:10427/' + TEMPLATE_ORGANIZATION.DOWNLOAD_LOGO + `/${id}`, {responseType: 'arraybuffer'})
  }
  approveInitOrganization(id:any, type:any,comment?:any){
    const requestBody = {
      "organizationDTOS": [
        {
          "id": id,
          "type": type
        }
      ],
      "description": comment ? comment:""
    }
    return this.post(SERVICE_NAME.MODEL_PLAN + TEMPLATE_ORGANIZATION.APPROVE_INIT_ORG, requestBody,{},MICRO_SERVICE.MODEL_PLAN)
  }

  rejectedInitOrganization(id:any, type:any, comment:any){
    const requestBody ={
      "organizationDTOS": [
        {
          "id": id,
          "type": type
        },
      ],
      "description": comment
    }
    return this.post(SERVICE_NAME.MODEL_PLAN + TEMPLATE_ORGANIZATION.REJECT_INIT_ORG, requestBody,{},MICRO_SERVICE.MODEL_PLAN)
  }

  getInfoModelOrganization(id:any){
    return this.get(SERVICE_NAME.MODEL_PLAN + TEMPLATE_ORGANIZATION.GET_INFO_MODEL_ORG + `/${id}`,{},MICRO_SERVICE.MODEL_PLAN)
  }
  editInfoModelOrganization(requestBody:any){
    return this.put(SERVICE_NAME.MODEL_PLAN + TEMPLATE_ORGANIZATION.EDIT_INFO_MODEL_ORG, requestBody,MICRO_SERVICE.MODEL_PLAN)
  }
  getListSurrogate(param:any){
    return this.get(SERVICE_NAME.MODEL_PLAN + TEMPLATE_ORGANIZATION.LIST_SUR, {params: param},MICRO_SERVICE.MODEL_PLAN)
  }
  public setIdDetailTemplate(id: number): void {
    this.idTemplate = id
  }
  getSubRegion(lookUpcode?:string, flagStatus?: number) {
    const objSearch = {
      lookupCode: lookUpcode ? lookUpcode : 'TIEU_VUNG',
      flagStatus: flagStatus ? flagStatus : ''
    };
    return this.get(SERVICE_NAME.MODEL_PLAN + TEMPLATE_ORGANIZATION.CRITERIA, {
      params: objSearch,
    },MICRO_SERVICE.MODEL_PLAN);
  }
  public getIdDetailTemplate() {
    return this.idTemplate;
  }

  public getAreas(): Area[]{
    if(!this.areas){
      return []
    }
    return this.areas
  }

  public getTypes(): Type[]{
    if(!this.types){
      return []
    }
    return this.types
  }

  public setAreas(areas:any){
    this.areas = areas
  }

  public setTypes(types:any){
    this.types = types
  }

  public sẹtJobs(jobs:any){
    this.jobs = jobs
  }

  public gẹtJob(){
    return this.jobs
  }
  getBranchType () {
    return this.get(SERVICE_NAME.MODEL_PLAN + TEMPLATE_ORGANIZATION.CRITERIA,{},MICRO_SERVICE.MODEL_PLAN);
  }
  getListLegalBranch() {
    return this.get(SERVICE_NAME.MODEL_PLAN + TEMPLATE_ORGANIZATION.GET_BRANCH_TYPE,{},MICRO_SERVICE.MODEL_PLAN);
  }
}
