import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {MICRO_SERVICE, SERVICE_NAME} from "@hcm-mfe/shared/common/constants";
import {PROJECT_URL} from "./constant/project.url";
import {
  ProjectDetailResponse,
  ResponseEntity,
  LookupValue,
  SearchPRJ,
  SearchPRJImpl,
  ProjectInfo,
  Criteria,
  RequestCriteria,
  PRJResponse,
  ResponseEntityData,
  Employee,
  JobResponse, ProjectDetail, Job
} from "@hcm-mfe/model-organization/data-access/models";

@Injectable({
  providedIn: 'root',
})
export class ProjectService extends BaseService {

  private projectInfo:ProjectInfo | undefined;

  constructor(readonly http: HttpClient) {
    super(http);
  }

  setProjectInfo(projectInfo:ProjectInfo){
    this.projectInfo = projectInfo;
  }

  getProjectInfo(){
    return this.projectInfo
  }

  // getListProject() {
  //   const objSearch = {
  //     orgLevel: 3,
  //     orgGroup: 'ho',
  //   };
  //   return this.http.get(environment.baseUrl + url.url_enpoint_project_list, {
  //     params: { orgLevel: objSearch.orgLevel, orgGroup: objSearch.orgGroup },
  //   });
  // }
  //
  // getListJob(params?): Observable<any> {
  //   return this.http.get(environment.baseUrl + url.url_enpoint_project_list_job, { params: this.toParams(params) });
  // }
  //
  //
  getCriteriaProject():Observable<ResponseEntity<LookupValue[]>> {
    return this.get(SERVICE_NAME.MODEL_PLAN + PROJECT_URL.LIST_CRITERIA_PRJ, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  getAllListCriteria(): Observable<any> {
    return this.get(SERVICE_NAME.MODEL_PLAN + PROJECT_URL.LIST_CRITERIA, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  createProject(request:FormData) {
    return this.post(SERVICE_NAME.MODEL_PLAN + PROJECT_URL.PRJ_ROOT, request, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  editProject(request:FormData) {
    return this.put(SERVICE_NAME.MODEL_PLAN + PROJECT_URL.PRJ_ROOT, request, MICRO_SERVICE.MODEL_PLAN);
  }

  getDetailProject(id:string): Observable<ResponseEntity<ProjectDetailResponse>> {
    return this.get(SERVICE_NAME.MODEL_PLAN + PROJECT_URL.PRJ_ROOT + '/' + id, {}, MICRO_SERVICE.MODEL_PLAN);
  }
  // filterJobs(params) {
  //   return this.http.get(environment.baseUrl + url.url_enpoint_job_filter, { params: params });
  // }
  //
  //
  //
  getProjectList(param:SearchPRJ): Observable<ResponseEntityData<PRJResponse[]>> {
    return this.post(SERVICE_NAME.MODEL_PLAN + PROJECT_URL.PROJECT_SEARCH_LIST, param, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  deleteProject(id:number) {
    return this.delete(SERVICE_NAME.MODEL_PLAN + PROJECT_URL.PRJ_ROOT + '/' + id, {}, MICRO_SERVICE.MODEL_PLAN);
  }
  //
  // listProjectApprove(request) {
  //   return this.http.post(environment.baseUrl + url.url_endpoint_project_approve_search_post, request)
  // }
  //
  submitListApprove(request: any) {
    return this.post(SERVICE_NAME.MODEL_PLAN + PROJECT_URL.PRJ_APPROVE, request, {}, MICRO_SERVICE.MODEL_PLAN)
  }
  exportProjectList(body:SearchPRJ) {
    return this.post(SERVICE_NAME.MODEL_PLAN + PROJECT_URL.EXPORT_PROJECT, body, {responseType: 'arraybuffer'}, MICRO_SERVICE.MODEL_PLAN);
  }
  //
  // searchCriteria(body) {
  //   return this.http.post(environment.baseUrl + url.url_endpoint_lookup_values_search, body)
  // }
  //
  deleteCriteriaCategory(id:number) {
    return this.delete(SERVICE_NAME.MODEL_PLAN + PROJECT_URL.LIST_CRITERIA + '/' + id, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  getCriteriaCategoryDetail(id:number):Observable<ResponseEntity<Criteria>> {
    return this.get(SERVICE_NAME.MODEL_PLAN + PROJECT_URL.LIST_CRITERIA + '/' + id, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  createCriteria(body:RequestCriteria) {
    return this.post(SERVICE_NAME.MODEL_PLAN + PROJECT_URL.CRITERIA_CATEGORY, body, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  updateCriteria(body:RequestCriteria) {
    return this.put(SERVICE_NAME.MODEL_PLAN + PROJECT_URL.CRITERIA_CATEGORY, body,  MICRO_SERVICE.MODEL_PLAN);
  }
  //
  // getListProjectIplm(params) {
  //   return this.http.get(environment.baseUrl + url.url_endpoint_projects_implement, { params: params })
  // }
  // downloadExcel(body) {
  //   return this.http.post(environment.baseUrl + url.url_endpoint_download_excel_project_implement, body, {
  //     responseType: 'arraybuffer',
  //   })
  // }
  checkCriteriaProject(id:number) {
    const params = { lvaId: id }
    return this.get(SERVICE_NAME.MODEL_PLAN + PROJECT_URL.CHECK_CRITERIA, {params: params}, MICRO_SERVICE.MODEL_PLAN);
  }

  getListProjectIplm(params:SearchPRJ):Observable<ResponseEntity<any>> {
    return this.get(SERVICE_NAME.MODEL_PLAN + PROJECT_URL.PROJECT_IMPL_SEARCH, {params: params}, MICRO_SERVICE.MODEL_PLAN);
  }

  getEmployee(params:{ keyWord: string }):Observable<ResponseEntity<Employee[]>>{
    return this.get(SERVICE_NAME.MODEL_PLAN + PROJECT_URL.EMPLOYEE_SEARCH, {params: params}, MICRO_SERVICE.MODEL_PLAN);
  }

  downloadExcelImpl(body:SearchPRJ){
    return this.post(SERVICE_NAME.MODEL_PLAN + PROJECT_URL.PROJECT_IMPL_SEARCH, body, {responseType: 'arraybuffer'}, MICRO_SERVICE.MODEL_PLAN);
  }

  getJobProject(): Observable<ResponseEntityData<Job[]>> {
    return this.get(SERVICE_NAME.MODEL_PLAN + PROJECT_URL.JOB_PRJ, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  createProjectMember(body:any){
    return this.post(SERVICE_NAME.MODEL_PLAN + PROJECT_URL.CREATE_PRJ_MEMBER, body, {}, MICRO_SERVICE.MODEL_PLAN);
  }

  getProjectDetail(prjId: number):Observable<ResponseEntityData<ProjectDetail[]>> {
    const params = {
      prjId: prjId,
    };
    return this.get(SERVICE_NAME.MODEL_PLAN + PROJECT_URL.PRJ_IMPL_DETAIL, {params:params}, MICRO_SERVICE.MODEL_PLAN);
  }

}
