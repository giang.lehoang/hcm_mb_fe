import { Injectable } from "@angular/core";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {MICRO_SERVICE, SERVICE_NAME} from "@hcm-mfe/shared/common/constants";
import {urlStaffModule} from "./url/url.const";


@Injectable({
  providedIn: 'root',
})
export class ConfirmStaffService extends BaseService {

  getListConfirm(params: any) {
    return this.get(SERVICE_NAME.MODEL_PLAN + urlStaffModule.confirmList, { params: params }, MICRO_SERVICE.MODEL_PLAN);
  }
  confirmStaff(body: any) {
    return this.post(SERVICE_NAME.MODEL_PLAN + urlStaffModule.confirm, body, {}, MICRO_SERVICE.MODEL_PLAN);
  }
}
