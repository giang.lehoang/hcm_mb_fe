import { Injectable } from '@angular/core';
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";

@Injectable({
  providedIn: "root"
})
export class DownloadFileAttachService extends BaseService{
  readonly baseUrl = UrlConstant.API_VERSION + UrlConstant.DOWNLOAD_FILE_ATTACH;

  // dowload file attach by docId
  doDownloadAttachFile(docId: number) {
    const url = this.baseUrl + `${docId}`;
    return this.getRequestFile(url);
  }

  doDownloadAttachFileWithSecurity(docId: number, security: string | any) {
    const url = this.baseUrl + `${docId}&security=${security}`;
    return this.getRequestFile(url);
  }
}
