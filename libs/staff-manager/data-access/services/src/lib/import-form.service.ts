import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";

@Injectable({
  providedIn: 'root'
})
export class ImportFormService extends BaseService {

  public downloadTemplate(urlEndpoint: string) {
    const url = UrlConstant.API_VERSION + urlEndpoint;
    return this.getRequestFile(url);
  }
  public doDownloadFileByName(fileName: string | any) {
    const url = UrlConstant.API_VERSION + '/download/temp-file?fileName=' + fileName;
    return this.getRequestFile(url);
  }

  public doImport(endpoint: string, data: FormData): Observable<BaseResponse> {
    const url = UrlConstant.API_VERSION + endpoint;
    return this.post(url, data);
  }


}
