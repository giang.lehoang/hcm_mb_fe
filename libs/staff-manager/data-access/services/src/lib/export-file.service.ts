import { Injectable } from '@angular/core';
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";

@Injectable({
  providedIn: "root"
})
export class ExportFileService extends BaseService{
  readonly baseUrl = UrlConstant.API_VERSION;

  doExport(strUrl: string) {
    const url = this.baseUrl + strUrl;
    return this.get(url, {responseType: 'blob'})
  }
}
