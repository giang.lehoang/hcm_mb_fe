import {Injectable} from '@angular/core';
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {HttpParams} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class AcceptanceService extends BaseService{

  uploadAttachFile(formData: FormData) {
    const url = UrlConstant.API_VERSION + UrlConstant.ACCEPTANCE.UPLOAD_FILE;
    return this.post(url, formData);
  }

  downloadFileAttach(params: HttpParams) {
    const url = UrlConstant.API_VERSION + UrlConstant.DOWNLOAD_FILE;
    return this.getRequestFile(url, {params: params});
  }

}
