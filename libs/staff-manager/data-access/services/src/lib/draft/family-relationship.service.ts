import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";
import {environment} from "@hcm-mfe/shared/environment";
import {UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {SearchForm} from "@hcm-mfe/staff-manager/data-access/models/draft";

@Injectable({
    providedIn: 'root',
})
export class FamilyRelationshipService {

    constructor(private http: HttpClient) {
    }

    approveFamilyRelationship(draftFamilyRelationshipId: number): Observable<BaseResponse> {
        return this.http.post<BaseResponse>(`${environment.backend.employeeServiceBackend}` + UrlConstant.API_VERSION + UrlConstant.APPROVE_DRAFT.FAMILY_RELATIONSHIP, {
            id: draftFamilyRelationshipId
        });
    }

    approveAllFamilyRelationship(searchParam: SearchForm): Observable<BaseResponse> {
        return this.http.post<BaseResponse>(`${environment.backend.employeeServiceBackend}` + UrlConstant.API_VERSION + UrlConstant.APPROVE_ALL_DRAFT.FAMILY_RELATIONSHIP, searchParam);
    }

    deleteDraftFamilyRelationshipById(draftId: number): Observable<BaseResponse> {
        return this.http.delete<BaseResponse>(`${environment.backend.employeeServiceBackend}` + UrlConstant.API_VERSION + UrlConstant.DELETE_DRAFT.FAMILY_RELATIONSHIP + draftId, {});
    }

    approveFamilyRelationshipByListId(listId: number[]): Observable<BaseResponse> {
        return this.http.post<BaseResponse>(`${environment.backend.employeeServiceBackend}` + UrlConstant.API_VERSION + UrlConstant.APPROVE_DRAFT.FAMILY_RELATIONSHIP_LIST,
            {
                listId: listId
            }
        );
    }

}
