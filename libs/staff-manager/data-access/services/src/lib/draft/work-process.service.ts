import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";
import {environment} from "@hcm-mfe/shared/environment";
import {UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {SearchForm} from "@hcm-mfe/staff-manager/data-access/models/draft";


@Injectable({
    providedIn: 'root'
})
export class WorkProcessService {

    constructor(private http: HttpClient) {
    }

    approveWorkProcess(draftWorkProcessId: number): Observable<BaseResponse> {
        return this.http.post<BaseResponse>(`${environment.backend.employeeServiceBackend}` + UrlConstant.API_VERSION + UrlConstant.APPROVE_DRAFT.WORK_PROCESS, {
            id: draftWorkProcessId
        });
    }

    refuseApproveWorkProcess(draftWorkProcessId: number): Observable<BaseResponse> {
        return this.http.post<BaseResponse>(`${environment.backend.employeeServiceBackend}` + UrlConstant.API_VERSION + UrlConstant.REFUSE_APPROVE_DRAFT.WORK_PROCESS, {
            id: draftWorkProcessId
        });
    }

    approveAllWorkProcess(searchParam: SearchForm): Observable<BaseResponse> {
        return this.http.post<BaseResponse>(`${environment.backend.employeeServiceBackend}` + UrlConstant.API_VERSION + UrlConstant.APPROVE_ALL_DRAFT.WORK_PROCESS, searchParam);
    }

    deleteDraftWorkProcessById(draftId: number): Observable<BaseResponse> {
        return this.http.delete<BaseResponse>(`${environment.backend.employeeServiceBackend}` + UrlConstant.API_VERSION + UrlConstant.DELETE_DRAFT.WORK_PROCESS + draftId, {});
    }

    approveWorkProcessByListId(listId: number[]): Observable<BaseResponse> {
        return this.http.post<BaseResponse>(`${environment.backend.employeeServiceBackend}` + UrlConstant.API_VERSION + UrlConstant.APPROVE_DRAFT.WORK_PROCESS_LIST,
            {
                listId: listId
            }
        );
    }

}
