import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";
import {environment} from "@hcm-mfe/shared/environment";
import {UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {SearchForm} from "@hcm-mfe/staff-manager/data-access/models/draft";


@Injectable({
    providedIn: 'root'
})
export class DependentPersonService {

    constructor(private http: HttpClient) {
    }

    approveDependentPerson(draftDependentPersonId: number): Observable<BaseResponse> {
        return this.http.post<BaseResponse>(`${environment.backend.employeeServiceBackend}` + UrlConstant.API_VERSION + UrlConstant.APPROVE_DRAFT.DEPENDENT_PERSON, {
            id: draftDependentPersonId
        });
    }

    refuseApproveDependentPerson(draftDependentPersonId: number): Observable<BaseResponse> {
        return this.http.post<BaseResponse>(`${environment.backend.employeeServiceBackend}` + UrlConstant.API_VERSION + UrlConstant.REFUSE_APPROVE_DRAFT.DEPENDENT_PERSON, {
            id: draftDependentPersonId
        });
    }

    approveAllDependentPerson(searchParam: SearchForm): Observable<BaseResponse> {
        return this.http.post<BaseResponse>(`${environment.backend.employeeServiceBackend}` + UrlConstant.API_VERSION + UrlConstant.APPROVE_ALL_DRAFT.DEPENDENT_PERSON, searchParam);
    }

    deleteDraftDependentPersonById(draftId: number): Observable<BaseResponse> {
        return this.http.delete<BaseResponse>(`${environment.backend.employeeServiceBackend}` + UrlConstant.API_VERSION + UrlConstant.DELETE_DRAFT.DEPENDENT_PERSON + draftId, {});
    }

    approveDependentPersonByListId(listId: number[]): Observable<BaseResponse> {
        return this.http.post<BaseResponse>(`${environment.backend.employeeServiceBackend}` + UrlConstant.API_VERSION + UrlConstant.APPROVE_DRAFT.DEPENDENT_PERSON_LIST,
            {
                listId: listId
            }
        );
    }

    refuseApproveDependentPersonByListId(listId: number[]): Observable<BaseResponse> {
        return this.http.post<BaseResponse>(`${environment.backend.employeeServiceBackend}` + UrlConstant.API_VERSION + UrlConstant.REFUSE_APPROVE_DRAFT.DEPENDENT_PERSON_LIST,
            {
                listId: listId
            }
        );
    }

}
