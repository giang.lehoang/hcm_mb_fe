import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";
import {environment} from "@hcm-mfe/shared/environment";
import {SearchForm} from "@hcm-mfe/staff-manager/data-access/models/draft";
import {UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";

@Injectable({
    providedIn: 'root',
})
export class ConcurrentProcessService {

    constructor(private http: HttpClient) {
    }

    approveConcurrentProcess(draftConcurrentProcessId: number): Observable<BaseResponse> {
        return this.http.post<BaseResponse>(`${environment.backend.employeeServiceBackend}` + UrlConstant.API_VERSION + UrlConstant.APPROVE_DRAFT.CONCURRENT_PROCESS, {
            id: draftConcurrentProcessId
        });
    }

    approveAllConcurrentProcess(searchParam: SearchForm): Observable<BaseResponse> {
        return this.http.post<BaseResponse>(`${environment.backend.employeeServiceBackend}` + UrlConstant.API_VERSION + UrlConstant.APPROVE_ALL_DRAFT.CONCURRENT_PROCESS, searchParam);
    }

    deleteDraftConcurrentProcessById(draftId: number): Observable<BaseResponse> {
        return this.http.delete<BaseResponse>(`${environment.backend.employeeServiceBackend}` + UrlConstant.API_VERSION + UrlConstant.DELETE_DRAFT.CONCURRENT_PROCESS.replace("{id}", draftId.toString()), {});
    }

    approveConcurrentProcessByListId(listId: number[]): Observable<BaseResponse> {
        return this.http.post<BaseResponse>(`${environment.backend.employeeServiceBackend}` + UrlConstant.API_VERSION + UrlConstant.APPROVE_DRAFT.CONCURRENT_PROCESS_LIST,
            {
                listId: listId
            }
        );
    }

}
