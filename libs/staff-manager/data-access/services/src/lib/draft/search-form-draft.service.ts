import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";
import {UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {environment} from "@hcm-mfe/shared/environment";

@Injectable({
  providedIn: 'root'
})
export class SearchFormDraftService extends BaseService {

  // public getFilterResearch(modelFilter: SearchForm, urlEndpoint: string,  pagination?: {startRecord: number , pageSize: number}): Observable<BaseResponse> {
  //   let params = new HttpParams();
  //   if (modelFilter != undefined) {
  //     if (modelFilter.fullName != undefined)
  //       params = params.set('fullName', modelFilter.fullName);
  //     if (modelFilter.employeeCode != undefined)
  //       params = params.set('employeeCode', modelFilter.employeeCode);
  //     if (modelFilter.flagStatus != undefined)
  //       params = params.set('flagStatus', modelFilter.flagStatus);
  //     if (modelFilter.organizationId != undefined)
  //       params = params.set('organizationId', modelFilter.organizationId);
  //   }
  //   if (pagination) params = params.appendAll(pagination);
  //   const url = UrlConstant.API_VERSION + urlEndpoint;
  //   return this.get(url, { params: params });
  // }

  public getFilterResearch(urlEndpoint: string, searchParam: HttpParams, pagination: {startRecord: number , pageSize: number}): Observable<BaseResponse> {
    if (pagination) searchParam = searchParam.appendAll(pagination);
    const url = UrlConstant.API_VERSION + urlEndpoint;
    return this.get(url, { params: searchParam });
  }

  public getUrlImportFile(){
    return environment.backend.employeeServiceBackend + UrlConstant.API_VERSION + UrlConstant.IMPORT_FORM.DRAFT_DECPLINE_FORM;
  }

  public getEmpTypeList() {
      return this.get(UrlConstant.API_VERSION + UrlConstant.SEARCH_FORM.LOOKUP_EMP_TYPE)
  }
}
