import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";
import {environment} from "@hcm-mfe/shared/environment";
import {UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {SearchForm} from "@hcm-mfe/staff-manager/data-access/models/draft";

@Injectable({
    providedIn: 'root',
})
export class PersonalIdentityService {

    constructor(private http: HttpClient) {
    }

    approveIdentity(draftPersonalIdentityId: number): Observable<BaseResponse> {
        return this.http.post<BaseResponse>(`${environment.backend.employeeServiceBackend}` + UrlConstant.API_VERSION + UrlConstant.APPROVE_DRAFT.IDENTITY,
            {
                id: draftPersonalIdentityId
            }
        );
    }

    refuseApproveIdentity(draftPersonalIdentityId: number): Observable<BaseResponse> {
        return this.http.post<BaseResponse>(`${environment.backend.employeeServiceBackend}` + UrlConstant.API_VERSION + UrlConstant.REFUSE_APPROVE_DRAFT.IDENTITY,
            {
                id: draftPersonalIdentityId,
            }
        );
    }

    approveIdentityByListId(listId: number[]): Observable<BaseResponse> {
        return this.http.post<BaseResponse>(`${environment.backend.employeeServiceBackend}` + UrlConstant.API_VERSION + UrlConstant.APPROVE_DRAFT.IDENTITY_LIST,
            {
                listId: listId
            }
        );
    }

    refuseApproveIdentityByListId(listId: number[]): Observable<BaseResponse> {
        return this.http.post<BaseResponse>(`${environment.backend.employeeServiceBackend}` + UrlConstant.API_VERSION + UrlConstant.REFUSE_APPROVE_DRAFT.IDENTITY_LIST,
            {
                listId: listId
            }
        );
    }

    approveAllIdentity(searchParam: SearchForm): Observable<BaseResponse> {
        return this.http.post<BaseResponse>(`${environment.backend.employeeServiceBackend}` + UrlConstant.API_VERSION + UrlConstant.APPROVE_ALL_DRAFT.IDENTITY, searchParam);
    }

    deleteDraftIdentityById(draftId: number): Observable<BaseResponse> {
        return this.http.delete<BaseResponse>(`${environment.backend.employeeServiceBackend}` + UrlConstant.API_VERSION + UrlConstant.DELETE_DRAFT.IDENTITY + draftId, {});
    }

}
