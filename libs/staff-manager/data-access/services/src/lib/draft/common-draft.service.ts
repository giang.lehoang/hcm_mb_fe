import { Injectable } from '@angular/core';
import {FormGroup} from '@angular/forms';
import {HttpParams} from "@angular/common/http";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";

@Injectable({
  providedIn: 'root',
})
export class CommonDraftService extends BaseService{
  public approveById(id:number, urlEndpoint:string) {
    const data = {id}
    const url = UrlConstant.API_VERSION + urlEndpoint;
    return this.post(url, data);
  }

  public approveByList(listId:number[], urlEndpoint:string) {
    const data = {listId}
    const url = UrlConstant.API_VERSION + urlEndpoint;
    return this.post(url, data);
  }

  public rejectById(id:number, urlEndpoint:string, rejectReason:string) {
    const data = {id,rejectReason}
    const url = UrlConstant.API_VERSION + urlEndpoint;
    return this.post(url,data);
  }
  public rejectByList(listId:number[], urlEndpoint:string, rejectReason?:string) {
    const data = {listId,rejectReason}
    const url = UrlConstant.API_VERSION + urlEndpoint;
    return this.post(url,data);
  }

  public approveAll(urlEndpoint:string, form: FormGroup | any) {
    const formControl = form.controls;
    const data = {
      employeeCode: formControl['employeeCode'].value,
      fullName: formControl['fullName'].value,
      empTypeCode: formControl['employeeType'].value,
      organizationId: formControl['organizationId'].value,
    }
    const url = UrlConstant.API_VERSION + urlEndpoint;
    return this.post(url, data);
  }

  public deleteDraft(id:number, urlEndpoint:string){
    const url = UrlConstant.API_VERSION + urlEndpoint.replace('{id}',id.toString());
    return this.delete(url);
  }

  getNumberBadge(type: string | any, urlEndpoint: string) {
    const url = UrlConstant.API_VERSION + urlEndpoint;
    const params = new HttpParams().set("type", type);
    return this.get(url, {params: params});
  }
}
