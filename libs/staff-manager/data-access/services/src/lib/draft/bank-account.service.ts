import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";
import {environment} from "@hcm-mfe/shared/environment";
import {UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {SearchForm} from "@hcm-mfe/staff-abs/data-access/models";

@Injectable({
    providedIn: 'root',
})
export class BankAccountService {

    constructor(private http: HttpClient) {
    }

    approveBankAccount(draftBankAccountId: number): Observable<BaseResponse> {
        return this.http.post<BaseResponse>(`${environment.backend.employeeServiceBackend}` + UrlConstant.API_VERSION + UrlConstant.APPROVE_DRAFT.BANK_ACCOUNT, {
            id: draftBankAccountId
        });
    }

    approveAllBankAccount(searchParam: SearchForm): Observable<BaseResponse> {
        return this.http.post<BaseResponse>(`${environment.backend.employeeServiceBackend}` + UrlConstant.API_VERSION + UrlConstant.APPROVE_ALL_DRAFT.BANK_ACCOUNT, searchParam);
    }

    deleteDraftBankAccountById(draftId: number): Observable<BaseResponse> {
        return this.http.delete<BaseResponse>(`${environment.backend.employeeServiceBackend}` + UrlConstant.API_VERSION + UrlConstant.DELETE_DRAFT.BANK_ACCOUNT + draftId, {});
    }

    approveBankAccountByListId(listId: number[]): Observable<BaseResponse> {
        return this.http.post<BaseResponse>(`${environment.backend.employeeServiceBackend}` + UrlConstant.API_VERSION + UrlConstant.APPROVE_DRAFT.BANK_ACCOUNT_LIST,
            {
                listId: listId
            }
        );
    }

}
