import {Injectable} from '@angular/core';
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {HttpParams} from "@angular/common/http";

export interface ApprovalDTO {
  id?: number;
  status?: number;
  listId?: number[];
}

@Injectable({
  providedIn: 'root'
})
export class RecruitmentService extends BaseService{

  public saveWorkProcess(request: FormData) {
    const url = UrlConstant.API_VERSION + UrlConstant.RECRUITMENT.SAVA_DATA;
    return this.post(url, request);
  }

  public saveRecruitNew(request: FormData) {
    const url = UrlConstant.API_VERSION + UrlConstant.RECRUITMENT.SAVE_RECRUIT_NEW;
    return this.post(url, request);
  }

  public getDataById(id: number, urlEnd: string) {
    const url = UrlConstant.API_VERSION + urlEnd.replace("{id}", id.toString());
    return this.get(url);
  }

  public approvalRecruitById(id: number, urlEnd: string) {
    const data = {id};
    const url = UrlConstant.API_VERSION + urlEnd;
    return this.post(url, data);
  }

  public approvalRecruitByList(listId: number[], urlEnd: string, status?: number) {
    const data = {listId, status};
    const url = UrlConstant.API_VERSION + urlEnd;
    return this.post(url, data);
  }

  public approvalRecruitAll(params: HttpParams, urlEnd: string) {
    const url = UrlConstant.API_VERSION + urlEnd;
    return this.post(url, null, {params: params});
  }

  public deleteDataById(id: number, urlEnd: string) {
    const url = UrlConstant.API_VERSION + urlEnd.replace("{id}", id.toString());
    return this.delete(url);
  }

  public confirmRecruit(data: ApprovalDTO[], urlEnd: string) {
    const url = UrlConstant.API_VERSION + urlEnd;
    return this.post(url, data);
  }

  public sendBrowsingOrCancel(data: ApprovalDTO, urlEnd: string) {
    const url = UrlConstant.API_VERSION + urlEnd;
    return this.post(url, data);
  }

  getCatalog(typeCode: string) {
    const url = UrlConstant.API_VERSION + UrlConstant.CATALOGS.PREFIX;
    return this.get(url, { params: {typeCode} });
  }

}
