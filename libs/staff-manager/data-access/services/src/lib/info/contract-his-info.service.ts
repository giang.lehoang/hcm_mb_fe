import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {HttpParams} from '@angular/common/http';
import {CurrentPage} from "@hcm-mfe/shared/data-access/models";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";
import {NzSafeAny} from "ng-zorro-antd/core/types";

@Injectable({
  providedIn: 'root'
})
export class ContractHisInfoService extends BaseService {
  readonly baseUrl = UrlConstant.API_VERSION + UrlConstant.CONTRACT_HISTORY_INFO.PREFIX;

  // Lấy danh sách
  public getList(employeeId: number | NzSafeAny, param: CurrentPage): Observable<BaseResponse> {
    const url = this.baseUrl + UrlConstant.CONTRACT_HISTORY_INFO.LIST.replace('{employeeId}', employeeId.toString())
    return this.get(url, {params: param});
  }

  // Lưu bản ghi
  public saveRecord(request: FormData) {
    const url = this.baseUrl + UrlConstant.CONTRACT_HISTORY_INFO.SAVE;
    return this.post(url, request);
  }

  public getDataById(id: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_HISTORY_INFO.GET_BY_ID.replace("{id}", id.toString());
    return this.get(url);
  }

  public getDraftDataById(id: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_HISTORY_INFO.GET_DRAFT_BY_ID.replace("{id}", id.toString());
    return this.get(url);
  }

  // Xóa bản ghi
  public deleteRecord(contractProcessId: number) {
    const url = this.baseUrl + UrlConstant.CONTRACT_HISTORY_INFO.DELETE.replace('{contractProcessId}', contractProcessId.toString())
    return this.delete(url);
  }

  // Xóa bản ghi trong draft
  public deleteRecordInDraft(id: number) {
    const url = this.baseUrl + UrlConstant.CONTRACT_HISTORY_INFO.DELETE_DRAFT.replace('{id}', id.toString())
    return this.delete(url);
  }

  // Lấy thông tin loại hợp đồng
  public getContractTypes(classifyCode:string): Observable<BaseResponse> {
    const params = new HttpParams().set('classifyCode', classifyCode);
    const url = UrlConstant.API_VERSION + UrlConstant.CATALOGS.CONTRACT_TYPES;
    return this.get(url, {params:params})
  }

  // Lấy danh mục loại hợp đồng
  public getVLookupContract(typeCode:string): Observable<BaseResponse> {
    const params = new HttpParams().set('typeCode', typeCode);
    const url = UrlConstant.API_VERSION + UrlConstant.CATALOGS.PREFIX;
    return this.get(url,{params:params});
  }

  public getManagerInfo(managerId:number) {
    const url = UrlConstant.API_VERSION + UrlConstant.EMPLOYEES.PREFIX + "/" + managerId + UrlConstant.EMPLOYEES.PERSONAL_INFO;
    return this.get(url);
  }
}
