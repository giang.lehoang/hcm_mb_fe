import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {BaseResponse, CurrentPage} from "@hcm-mfe/shared/data-access/models";
import {WorkBeforeInfo} from "@hcm-mfe/staff-manager/data-access/models/info";

@Injectable({
  providedIn: 'root'
})
export class WorkBeforeHisService extends BaseService{
  readonly baseUrl = UrlConstant.API_VERSION + UrlConstant.WORK_BEFORE_HISTORY.PREFIX;

  // Lấy danh sách
  public getList(employeeId: number | any, param: CurrentPage): Observable<BaseResponse> {
    const url = this.baseUrl + UrlConstant.WORK_BEFORE_HISTORY.LIST.replace('{employeeId}', employeeId.toString())
    return this.get(url, {params: param});
  }

  public getWorkedOutside(id: number) {
    const url = this.baseUrl + UrlConstant.WORK_BEFORE_HISTORY.GET_BY_ID.replace('{id}', id.toString())
    return this.get(url);
  }

  // Lưu bản ghi
  public saveRecord(request: WorkBeforeInfo) {
    const url = this.baseUrl + UrlConstant.WORK_BEFORE_HISTORY.SAVE;
    return this.post(url, request);
  }

  // Xóa bản ghi
  public deleteRecord(workOutsideId: number) {
    const url = this.baseUrl + UrlConstant.WORK_BEFORE_HISTORY.DELETE.replace('{workOutsideId}', workOutsideId.toString())
    return this.delete(url);
  }
}
