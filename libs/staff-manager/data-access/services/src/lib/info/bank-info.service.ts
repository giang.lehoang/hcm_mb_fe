import {Injectable} from "@angular/core";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {ParamBankAccount} from "@hcm-mfe/staff-manager/data-access/models/info";

@Injectable({
  providedIn: 'root'
})
export class BankInfoService extends BaseService {

  public getBankInfo(employeeId: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.EMPLOYEES.PREFIX + '/' + employeeId + UrlConstant.EMPLOYEES.BANK;
    return this.get(url);
  }

  public saveBankInfo(employeeId: number, bankInfo: ParamBankAccount) {
    const url = UrlConstant.API_VERSION + UrlConstant.EMPLOYEES.PREFIX + '/' + employeeId + UrlConstant.EMPLOYEES.BANK;
    return this.post(url, bankInfo);
  }
}
