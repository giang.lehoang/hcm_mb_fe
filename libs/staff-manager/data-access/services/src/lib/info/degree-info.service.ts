import {Injectable} from "@angular/core";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {CurrentPage} from "@hcm-mfe/shared/data-access/models";

@Injectable({
  providedIn: 'root'
})
export class DegreeInfoService extends BaseService {

  public getDegreeInfos(employeeId: number, param: CurrentPage) {
    const url = UrlConstant.API_VERSION + UrlConstant.DEGREE_INFOS.PREFIX + UrlConstant.DEGREE_INFOS.LIST + '/' + employeeId;
    return this.get(url, {params: param});
  }

  public getDegreeInfoDetail(educationDegreeId: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.DEGREE_INFOS.PREFIX  + '/' + educationDegreeId;
    return this.get(url);
  }

  public saveDegreeInfo(formData: FormData) {
    const url = UrlConstant.API_VERSION + UrlConstant.DEGREE_INFOS.PREFIX;
    return this.post(url, formData);
  }

  public deleteDegreeInfo(educationDegreeId: number | any) {
    const url = UrlConstant.API_VERSION + UrlConstant.DEGREE_INFOS.PREFIX + '/' + educationDegreeId;
    return this.delete(url);
  }
}
