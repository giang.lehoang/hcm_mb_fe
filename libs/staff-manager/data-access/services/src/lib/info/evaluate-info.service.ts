import { Injectable } from '@angular/core';
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";
import { UrlConstant } from '@hcm-mfe/staff-manager/data-access/common';

@Injectable({
  providedIn: 'root'
})
export class EvaluateInfoService extends BaseService{
  readonly baseUrl = 'goa' + UrlConstant.API_VERSION;

  getEvaluateInfo(param: NzSafeAny) {
    const url = this.baseUrl + UrlConstant.EVALUATE_INFO;
    return this.get(url, {params: param}, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
}
