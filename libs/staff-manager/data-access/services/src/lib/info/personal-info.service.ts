import {Injectable} from "@angular/core";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {PersonalInfo} from "@hcm-mfe/staff-manager/data-access/models/info";

@Injectable({
  providedIn: 'root'
})
export class PersonalInfoService extends BaseService {

  public getPersonalInfo(employeeId: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.EMPLOYEES.PREFIX + '/' + employeeId + UrlConstant.EMPLOYEES.PERSONAL_INFO;
    return this.get(url);
  }

  public savePersonalInfo(employeeId: number, personalInfo: PersonalInfo) {
    const url = UrlConstant.API_VERSION + UrlConstant.EMPLOYEES.PREFIX + '/' + employeeId + UrlConstant.EMPLOYEES.PERSONAL_INFO;
    return this.post(url, personalInfo);
  }

  public getIdentities(employeeId: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.EMPLOYEES.PREFIX + '/' + employeeId + UrlConstant.EMPLOYEES.IDENTITIES;
    return this.get(url);
  }

  public getAvatar(employeeId: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.EMPLOYEES.PREFIX + '/' + employeeId + UrlConstant.EMPLOYEES.AVATAR;
    return this.get(url);
  }

  public uploadAvatar(employeeId: number | any, formData: any) {
    const url = UrlConstant.API_VERSION + UrlConstant.EMPLOYEES.PREFIX + '/' + employeeId + UrlConstant.EMPLOYEES.AVATAR;
    return this.post(url, formData);
  }

  public deleteAvatar(employeeId: number | any) {
    const url = UrlConstant.API_VERSION + UrlConstant.EMPLOYEES.PREFIX + '/' + employeeId + UrlConstant.EMPLOYEES.AVATAR;
    return this.delete(url);
  }

  public getPersonalAvatar(employeeId: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.PERSONAL.PREFIX + '/' + employeeId + UrlConstant.PERSONAL.AVATAR;
    return this.get(url);
  }

  public uploadPersonalAvatar(employeeId: number, formData: any) {
    const url = UrlConstant.API_VERSION + UrlConstant.PERSONAL.PREFIX + '/' + employeeId + UrlConstant.PERSONAL.AVATAR;
    return this.post(url, formData);
  }

  public deletePersonalAvatar(employeeId: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.PERSONAL.PREFIX + '/' + employeeId + UrlConstant.PERSONAL.AVATAR;
    return this.delete(url);
  }
}
