import {Injectable} from "@angular/core";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";

@Injectable({
  providedIn: 'root'
})
export class UserConfigService extends BaseService {
  public getListUserRole(params: any){
    const url = `users`;
    return this.get(url, {params}, MICRO_SERVICE.URL_ENDPOIN_CATEGORY);
  }
  public searchRole(params: any){
    const url = `roles`;
    return this.get(url, {params}, MICRO_SERVICE.URL_ENDPOIN_CATEGORY);
  }
}
