import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {BaseResponse, CurrentPage} from "@hcm-mfe/shared/data-access/models";
import {RelativeInfo} from "@hcm-mfe/staff-manager/data-access/models/info";
import {NzSafeAny} from "ng-zorro-antd/core/types";

@Injectable({
  providedIn: 'root'
})
export class RelativesInfoService extends BaseService {
  readonly baseUrl = UrlConstant.API_VERSION + UrlConstant.RELATIVES_INFO.PREFIX;

  // Lấy danh sách thân nhân
  public getList(employeeId: number | NzSafeAny, param: CurrentPage): Observable<BaseResponse> {
    const url = this.baseUrl + UrlConstant.RELATIVES_INFO.LIST.replace('{employeeId}', employeeId.toString())
    return this.get(url, {params: param});
  }

  // Lưu bản ghi
  public saveRecord(request: RelativeInfo) {
    const url = this.baseUrl + UrlConstant.RELATIVES_INFO.SAVE;
    return this.post(url, request);
  }

  // Xóa bản ghi
  public deleteRecord(familyRelationshipId: number) {
    const url = this.baseUrl + UrlConstant.RELATIVES_INFO.DELETE.replace('{familyRelationshipId}', familyRelationshipId.toString())
    return this.delete(url);
  }

  // Lấy bản ghi
  public getRecord(familyRelationshipId: number) {
    const url = this.baseUrl + UrlConstant.RELATIVES_INFO.DETAIL.replace('{familyRelationshipId}', familyRelationshipId.toString())
    return this.get(url);
  }

}
