import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {BaseResponse, CurrentPage} from "@hcm-mfe/shared/data-access/models";

@Injectable({
    providedIn: "root"
})
export class ConcurrentProcessInfoService extends BaseService {
    readonly baseUrl = UrlConstant.API_VERSION + UrlConstant.CONCURRENT_PROCESS_INFO.PREFIX;

    // Lấy danh sách
    public getList(employeeId: number, param: CurrentPage): Observable<BaseResponse> {
        const url = this.baseUrl + UrlConstant.CONCURRENT_PROCESS_INFO.LIST.replace('{employeeId}', employeeId.toString());
        return this.get(url, { params: param });
    }

    // Lưu bản ghi
    public saveRecord(request: FormData) {
        const url = this.baseUrl + UrlConstant.CONCURRENT_PROCESS_INFO.SAVE;
        return this.post(url, request);
    }

    // Xóa bản ghi
    public deleteRecord(concurrentProcessId: number) {
        const url = this.baseUrl + UrlConstant.CONCURRENT_PROCESS_INFO.DELETE.replace('{concurrentProcessId}', concurrentProcessId.toString());
        return this.delete(url);
    }

    // Xóa bản ghi trong draft
    public deleteRecordInDraft(id: number) {
        const url = this.baseUrl + UrlConstant.CONCURRENT_PROCESS_INFO.DELETE_DRAFT.replace('{concurrentProcessId}', id.toString())
        return this.delete(url);
    }

    // Lấy danh mục chức danh
    public getMBPositions(orgId: number) {
        const url = UrlConstant.API_VERSION + UrlConstant.CATALOGS.MB_POSITIONS.replace('{orgId}', orgId?.toString());
        return this.get(url)
    }

    // Lấy thông tin bản ghi
    public getRecord(id: number) {
        const url = this.baseUrl + UrlConstant.CONCURRENT_PROCESS_INFO.DETAIL.replace('{concurrentProcessId}', id.toString())
        return this.get(url);
    }

    public getDraftDataById(id: number) {
        const url = this.baseUrl + UrlConstant.CONCURRENT_PROCESS_INFO.GET_DRAFT_BY_ID.replace("{concurrentProcessId}", id.toString());
        return this.get(url);
    }

}
