import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {BaseResponse, CurrentPage} from "@hcm-mfe/shared/data-access/models";
import {RewardHistory} from "@hcm-mfe/staff-manager/data-access/models/info";

@Injectable({
  providedIn: 'root'
})
export class RewardHisInfoService extends BaseService{
  readonly baseUrl = UrlConstant.API_VERSION + UrlConstant.REWARD_HISTORY_INFO.PREFIX;

  // Lấy danh sách
  public getList(employeeId: number | any, param: CurrentPage): Observable<BaseResponse> {
    const url = this.baseUrl + UrlConstant.REWARD_HISTORY_INFO.LIST.replace('{employeeId}', employeeId.toString())
    return this.get(url, {params: param});
  }

// Lưu bản ghi
  public saveRecord(request: RewardHistory) {
    const url = this.baseUrl + UrlConstant.REWARD_HISTORY_INFO.SAVE;
    return this.post(url, request);
  }


// Xóa bản ghi
  public deleteRecord(rewardRecordId: number) {
    const url = this.baseUrl + UrlConstant.REWARD_HISTORY_INFO.DELETE.replace('{rewardRecordId}', rewardRecordId.toString())
    return this.delete(url);
  }

  // lấy thông tin bản ghi
  public getRecord(rewardRecordId: number) {
    const url = this.baseUrl + UrlConstant.REWARD_HISTORY_INFO.DETAIL.replace('{rewardRecordId}', rewardRecordId.toString())
    return this.get(url);
  }
}
