import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {BaseResponse, CurrentPage} from "@hcm-mfe/shared/data-access/models";
import {AllowanceHistory} from "@hcm-mfe/staff-manager/data-access/models/info";

@Injectable({
  providedIn: 'root'
})
export class AllowanceHisInfoService extends BaseService{
  readonly baseUrl = UrlConstant.API_VERSION + UrlConstant.ALLOWANCE_HISTORY_INFO.PREFIX;

  // Lấy danh sách
  public getList(employeeId: number | any, param: CurrentPage): Observable<BaseResponse> {
    const url = this.baseUrl + UrlConstant.ALLOWANCE_HISTORY_INFO.LIST.replace('{employeeId}', employeeId.toString())
    return this.get(url, {params: param});
  }

  public getRecord(id: number) {
    const url = this.baseUrl + UrlConstant.ALLOWANCE_HISTORY_INFO.GET_BY_ID.replace('{id}', id.toString())
    return this.get(url);
  }

  // Lưu bản ghi
  public saveRecord(request: AllowanceHistory) {
    const url = this.baseUrl + UrlConstant.ALLOWANCE_HISTORY_INFO.SAVE;
    return this.post(url, request);
  }

  // Xóa bản ghi
  public deleteRecord(allowanceProcessId: number) {
    const url = this.baseUrl + UrlConstant.ALLOWANCE_HISTORY_INFO.DELETE.replace('{allowanceProcessId}', allowanceProcessId.toString())
    return this.delete(url);
  }
}
