import {Injectable} from '@angular/core';
import {BehaviorSubject, Subject} from 'rxjs';
import {BankInfo, IdentityInfo, PersonalInfo} from "@hcm-mfe/staff-manager/data-access/models/info";

@Injectable({
  providedIn: 'root',
})
export class ShareDataService {

  private personalInfoSource = new Subject<PersonalInfo>();
  personalInfo$ = this.personalInfoSource.asObservable();
  changePersonalInfo(personalInfo: PersonalInfo ) {
    this.personalInfoSource.next(personalInfo);
  }

  private employee = new BehaviorSubject<{employeeId: number | any}>({employeeId: null});
  public employee$ = this.employee.asObservable();
  changeEmployee(employee: {employeeId: number}) {
    this.employee.next(employee);
  }
  getEmployee() { return this.employee.getValue() }

  private identityInfoSource = new Subject<IdentityInfo>();
  identityInfo$ = this.identityInfoSource.asObservable();
  changeIdentityInfo(identityInfo: IdentityInfo) {
    this.identityInfoSource.next(identityInfo)
  }

  private bankInfoSource = new Subject<BankInfo>();
  bankInfo$ = this.bankInfoSource.asObservable();
  changeBankInfo(bankInfo: BankInfo) {
    this.bankInfoSource.next(bankInfo)
  }

  private emitChangeSource = new Subject<boolean>();
  changeEmitted$ = this.emitChangeSource.asObservable();
  emitChange(change: boolean) {
    this.emitChangeSource.next(change);
  }

}
