import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {BaseResponse, CurrentPage} from "@hcm-mfe/shared/data-access/models";

@Injectable({
  providedIn: 'root'
})
export class WorkInfoService extends BaseService{
  readonly baseUrl = UrlConstant.API_VERSION + UrlConstant.WORK_HISTORY.PREFIX;

  // Lấy danh sách
  public getList(employeeId: number | any, param: CurrentPage): Observable<BaseResponse> {
    const url = this.baseUrl + UrlConstant.WORK_HISTORY.LIST.replace('{employeeId}', employeeId.toString())
    return this.get(url, {params: param});
  }

  // Lưu bản ghi
  public saveRecord(request: FormData) {
    const url = this.baseUrl + UrlConstant.WORK_HISTORY.SAVE;
    return this.post(url, request);
  }

  public getDataById(id: number) {
    const url = this.baseUrl + UrlConstant.WORK_HISTORY.GET_BY_ID.replace("{id}", id.toString());
    return this.get(url);
  }

  public getDraftDataById(id: number) {
    const url = this.baseUrl + UrlConstant.WORK_HISTORY.GET_DRAFT_BY_ID.replace("{id}", id.toString());
    return this.get(url);
  }

  // Xóa bản ghi
  public deleteRecord(workProcessId: number) {
    const url = this.baseUrl + UrlConstant.WORK_HISTORY.DELETE.replace('{workProcessId}', workProcessId.toString())
    return this.delete(url);
  }

  // Xóa bản ghi
  public deleteDraftRecord(id: number) {
    const url = this.baseUrl + UrlConstant.WORK_HISTORY.DELETE_DRAFT.replace('{id}', id.toString())
    return this.delete(url);
  }

  // Lấy thông tin bản ghi
  public getRecord(workProcessId: number) {
    const url = this.baseUrl + UrlConstant.WORK_HISTORY.DETAIL.replace('{workProcessId}', workProcessId.toString())
    return this.get(url);
  }

  // Lấy danh mục loại QĐ
  public getDocumentTypes() {
    const url = UrlConstant.API_VERSION + UrlConstant.CATALOGS.DOCUMENT_TYPES;
    return this.get(url)
  }

  // Lấy danh mục chức danh
  public getMBPositions(orgId: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.CATALOGS.MB_POSITIONS.replace('{orgId}', orgId?.toString());
    return this.get(url)
  }
}
