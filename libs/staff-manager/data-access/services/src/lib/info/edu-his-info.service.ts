import {Injectable} from "@angular/core";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {CurrentPage} from "@hcm-mfe/shared/data-access/models";
import {EducationProcess} from "@hcm-mfe/staff-manager/data-access/models/info";

@Injectable({
  providedIn: 'root'
})
export class EduHisInfoService extends BaseService {

  public getEduHisInfo(employeeId: number, param: CurrentPage) {
    const url = UrlConstant.API_VERSION + UrlConstant.EDU_HIS_INFO.PREFIX + UrlConstant.EDU_HIS_INFO.LIST + '/' + employeeId;
    return this.get(url, {params: param});
  }

  public getEduHisInfoDetail(educationDegreeId: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.EDU_HIS_INFO.PREFIX  + '/' + educationDegreeId;
    return this.get(url);
  }

  public saveEduHisInfo(eduHisInfo: EducationProcess) {
    const url = UrlConstant.API_VERSION + UrlConstant.EDU_HIS_INFO.PREFIX;
    return this.post(url, eduHisInfo);
  }

  public deleteEduHisInfo(educationDegreeId: number | any) {
    const url = UrlConstant.API_VERSION + UrlConstant.EDU_HIS_INFO.PREFIX + '/' + educationDegreeId;
    return this.delete(url);
  }
}
