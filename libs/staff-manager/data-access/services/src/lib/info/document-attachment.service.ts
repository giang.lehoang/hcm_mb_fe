import { Injectable } from '@angular/core';
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {CurrentPage} from "@hcm-mfe/shared/data-access/models";

@Injectable({
  providedIn: 'root'
})
export class DocumentAttachmentService extends BaseService {

  public getDocumentAttachment(employeeId: number, param: CurrentPage) {
    const url = UrlConstant.API_VERSION + UrlConstant.DOCUMENT_ATTACHMENT.PREFIX + UrlConstant.DOCUMENT_ATTACHMENT.LIST + employeeId;
    return this.get(url, {params: param});
  }

  public getRecord(id: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.DOCUMENT_ATTACHMENT.GET_BY_ID.replace("{id}", id.toString());
    return this.get(url);
  }

  public saveDocumentAttachment(data: FormData) {
    const url = UrlConstant.API_VERSION + UrlConstant.DOCUMENT_ATTACHMENT.PREFIX;
    return this.post(url, data);
  }

  public deleteDocumentAttachment(documentTypeId: any) {
    const url = UrlConstant.API_VERSION + UrlConstant.DOCUMENT_ATTACHMENT.PREFIX + '/' + documentTypeId;
    return this.delete(url);
  }
}
