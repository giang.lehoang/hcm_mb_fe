import {Injectable} from '@angular/core';
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";

@Injectable({
  providedIn: 'root'
})
export class CategoriesService extends BaseService{

  public saveData(url: string,request: FormData) {
    return this.post(url, request);
  }

  public getDataById(id: number, url: string) {
    url = url.replace("{id}", id.toString());
    return this.get(url);
  }


  public deleteDataById(id: number, url: string) {
    url = url.replace("{id}", id.toString());
    return this.delete(url);
  }


}
