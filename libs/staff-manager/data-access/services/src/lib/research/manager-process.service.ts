import {Injectable} from '@angular/core';
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {NzSafeAny} from "ng-zorro-antd/core/types";

@Injectable({
  providedIn: 'root'
})
export class ManagerProcessService extends BaseService {

  public getMangerProcessById(id: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.MANAGER_PROCESS.GET_BY_ID.replace('{id}', id.toString());
    return this.get(url)
  }

  public deleteMangerProcessById(id: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.MANAGER_PROCESS.GET_BY_ID.replace('{id}', id.toString());
    return this.delete(url)
  }

  public getMangerInfo(employeeId: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.EMPLOYEES.MANAGER_INFO.replace('{employeeId}', employeeId.toString());
    return this.get(url);
  }

  public saveManagerProcess(data: NzSafeAny) {
    const url =UrlConstant.API_VERSION + UrlConstant.MANAGER_PROCESS.SAVE;
    return this.post(url, data);
  }
}
