import {Injectable} from "@angular/core";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {Bookmark} from "@hcm-mfe/staff-manager/data-access/models/research";

@Injectable({
  providedIn: 'root'
})
export class BookmarkFormService extends BaseService {

  public getExtendField(moduleCode: string) {
    const url = UrlConstant.API_VERSION + UrlConstant.BOOK_MARK_RESEARCH.EXTEND_FIELD + moduleCode;
    return this.get(url);
  }

  public getBookmark(type: string) {
    const url = UrlConstant.API_VERSION + UrlConstant.BOOK_MARK_RESEARCH.USER_BOOKMARK;
    return this.get(url, {params: {type: type}})
  }

  public saveBookmark(data: Bookmark) {
    const url = UrlConstant.API_VERSION + UrlConstant.BOOK_MARK_RESEARCH.USER_BOOKMARK;
    return this.post(url, data )
  }

  public deleteBookmark(id: number | any) {
    const url = UrlConstant.API_VERSION + UrlConstant.BOOK_MARK_RESEARCH.DELETE_BOOKMARK.replace('{id}', id.toString());
    return this.delete(url)
  }
}
