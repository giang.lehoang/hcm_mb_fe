import {Injectable} from '@angular/core';
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {Observable} from 'rxjs';
import {HttpParams} from '@angular/common/http';
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";
import {environment} from "@hcm-mfe/shared/environment";
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";
import {NzSafeAny} from "ng-zorro-antd/core/types";

@Injectable({
  providedIn: 'root'
})
export class SearchFormService extends BaseService {

  public getFilterResearch(urlEndpoint: string, searchParam: HttpParams, pagination: {startRecord: number, pageSize: number}): Observable<BaseResponse> {
    if (pagination) searchParam = searchParam.appendAll(pagination);
    const url = UrlConstant.API_VERSION + urlEndpoint;
    return this.get(url, {params: searchParam});
  }

  public getUrlImportFile() {
    return environment.backend.employeeServiceBackend + UrlConstant.API_VERSION + UrlConstant.IMPORT_FORM.DECPLINE_FORM;
  }

  doExportFile(urlEndpoint: string, searchParam: HttpParams) {
    const url = UrlConstant.API_VERSION + urlEndpoint;
    return this.getRequestFile(url, {params: searchParam});
  }

  public getListValue(urlEndpoint: string, params: HttpParams) {
    const url = UrlConstant.API_VERSION + urlEndpoint;
    return this.get(url, {params: params});
  }

  public getUrlImportFileDraft(){
    return environment.backend.employeeServiceBackend + UrlConstant.API_VERSION + UrlConstant.IMPORT_FORM.DRAFT_DECPLINE_FORM;
  }

  public getEmpTypeList() {
    return this.get(UrlConstant.API_VERSION + UrlConstant.SEARCH_FORM.LOOKUP_EMP_TYPE)
  }

  public getListLine(pgrType: string) {
    const url = UrlConstant.API_VERSION + UrlConstant.SEARCH_FORM.LINE.replace("{pgrType}", pgrType);
    return this.get(url, {}, MICRO_SERVICE.ABS_MANAGEMENT)
  }

  public getPositionGroup(pgrType: string) {
    const url = UrlConstant.API_VERSION + UrlConstant.SEARCH_FORM.LINE.replace('{pgrType}', pgrType);
    return this.get(url, {}, MICRO_SERVICE.ABS_MANAGEMENT);
  }

  public exportReport(searchParam: NzSafeAny, data: NzSafeAny): Observable<any> {
    return this.getRequestFileD2T(data.url, {params: searchParam}, MICRO_SERVICE.REPORT);
  }
  
   public categoriesResearch(url: string, searchParam: HttpParams, pagination: {startRecord: number, pageSize: number}): Observable<BaseResponse> {
    if (pagination) searchParam = searchParam.appendAll(pagination);
    return this.get(url, {params: searchParam});
  }

  doExportCategories(url: string, searchParam: HttpParams) {
    return this.getRequestFile(url, {params: searchParam});
  }
}
