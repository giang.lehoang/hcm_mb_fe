import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {EmployeeInfo} from "@hcm-mfe/staff-manager/data-access/models/draft";
import {FormGroup} from "@angular/forms";
import {SearchFormComponent} from "@hcm-mfe/staff-manager/ui/search-form-research";
import {NzSafeAny} from "ng-zorro-antd/core/types";

export function validateRejectByList<T extends EmployeeInfo>(setDraftId: Set<number>, mapEmployeeIsNotApproved: Map<number, T> | any, translateService: TranslateService, toastrService: ToastrService): boolean {
  let isValid = true;
  let listEmployeeIsNotApproved: T[] = [];
  setDraftId.forEach(draftId => {
    if (mapEmployeeIsNotApproved.has(draftId)) {
      listEmployeeIsNotApproved.push(mapEmployeeIsNotApproved.get(draftId));
      isValid = false;
    }
  });
  if (!isValid) {
    let message = translateService.instant('common.notification.rejectByListFailed');
    let employeeCodes = '';
    // filter duplicate by employeeCode
    listEmployeeIsNotApproved = listEmployeeIsNotApproved.reduce((accumulator: any[], currentValue) => {
      if (!accumulator.some((item: any) => item.employeeCode === currentValue.employeeCode)) {
        accumulator.push(currentValue);
      }
      return accumulator;
    }, []);
    listEmployeeIsNotApproved.forEach(draft => {
      if (employeeCodes === '') {
        employeeCodes = employeeCodes.concat(draft.employeeCode);
      } else {
        employeeCodes = employeeCodes.concat(', ' + draft.employeeCode);
      }
    });
    message = message.replace('%s', employeeCodes);
    toastrService.error(message);
    return false;
  }
  return true;
}

export function getPreviousForm(searchForm: SearchFormComponent, form: string) {
  const redirectByBackBtn = localStorage.getItem("redirectByBackBtn");
  const fields: NzSafeAny[] = [];
  if (redirectByBackBtn) {
    const oldForm = localStorage.getItem(form);
    if (redirectByBackBtn === 'true' && oldForm) {
      const oldFormObj = JSON.parse(oldForm);
      searchForm.form.patchValue(oldFormObj);

      // fill nhung truong tim kiem bo sung
      oldFormObj?.extendField?.filter((item: string) => item !== 'flagStatus')
        .forEach((item: string) => {
          const itemField = searchForm.listExtendField[searchForm.listExtendField.findIndex(x => x.code === item)];
          if (itemField) {
            fields.push(itemField.code);
            searchForm.form.addControl(itemField.code, searchForm.fb.control(null));
            const option = searchForm.getOption(itemField, oldFormObj);
            searchForm.doAddOrRemoveFieldExtend({listOfSelected: [itemField.code]}, false, true, itemField, option);
          }
        })
      searchForm.form.patchValue({extendField: fields});


      localStorage.removeItem("redirectByBackBtn");
    }
  }
}

export function saveSearchFormToLocalStorage(searchForm: FormGroup, form: string) {
  localStorage.setItem(form, JSON.stringify(searchForm.value));
}
