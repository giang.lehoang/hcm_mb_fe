import {environment} from "@hcm-mfe/shared/environment";
import {NzSafeAny} from "ng-zorro-antd/core/types";

export class Constant {
  public static readonly OFFICIAL_CODE = '1';
  public static readonly EMP_OS_CODE = '3';
  public static readonly EMP_STATUS_OUT = 'OUT';
  public static readonly DOCUMENT_TYPE_RECRUIT_AGAIN = 'RI';
  public static readonly NATION_CODE_VN = '27';


  public static readonly SALARY_ACCOUNT_CODE = '01';


  public static readonly ACTION = {
    EDIT: 'EDIT',
    DETAIL: 'DETAIL',
    ADD: 'ADD'
  };

  public static readonly PAPERS_CODE = environment?.production ?
    {ID_NO: 'CMT', CITIZEN_ID: 'CCCD'} : (environment?.isMbBank ? {ID_NO: '1010', CITIZEN_ID: '2080'} : {ID_NO: '03', CITIZEN_ID: '05'})


  public static readonly KEEP_SENIOR = {
    YES: 1,
    NO: 0
  };

  public static readonly LIST_DOCUMENT_TYPE = [
    {label: 'IN', value: 'IN'},
    {label: 'OUT', value: 'OUT'}
  ];

  public static readonly APPROVAL_STATUSES = [   // Trạng thái phê duyệt
    {label: 'staffManager.contractProcess.statuses.suggestChange', value: 'UPDATE', color: '#F99600', bgColor: '#FFF2DA'},
    {label: 'staffManager.contractProcess.statuses.browsed', value: 1, color: '#06A561', bgColor: '#DAF9EC'},
    {label: 'staffManager.contractProcess.statuses.suggestDelete', value: 'DELETE', color: '#FA0B0B', bgColor: '#FDE7EA'},
    {label: 'staffManager.contractProcess.statuses.suggestCreate', value: 'INSERT', color: '#F99600', bgColor: '#FFF2DA'},
  ];

  public static readonly SUGGEST_TYPE = [   // Loại đề nghị
    {label: 'staffManager.staffDraft.suggestType.deleteSuggest', value: 'DELETE', color: '#FA0B0B', bgColor: '#FDE7EA'},
    {label: 'staffManager.staffDraft.suggestType.editSuggest', value: 'UPDATE', color: '#F99600', bgColor: '#FFF2DA'},
    {label: 'staffManager.staffDraft.suggestType.createSuggest', value: 'INSERT', color: '#F99600', bgColor: '#FFF2DA'},
  ];

  public static readonly EXTEND_FIELD_TYPE = {
    TEXT: 'text',
    DATE: 'date',
    CHECKBOX: 'checkbox',
    RADIO: 'radio',
    NUMBER: 'number'
  }

  public static readonly MODULE_NAME = {
    BASIC_RESEARCH: "THONG_TIN_CO_BAN",
    ALLOW_HISTORY: "PHU_CAP",
    BANK: "TAI_KHOAN_NGAN_HANG",
    CONTRACT: "HOP_DONG",
    DEGREE: "BANG_CAP_CHUNG_CHI",
    EDU: "QUA_TRINH_DAO_TAO",
    FAMILY_RELATIONSHIPS: "QUAN_HE_GIA_DINH",
    IDENTITY: "DINH_DANH",
    PROJECT: "DU_AN",
    DEPENDENT_PERSONS: "GIAM_TRU_GIA_CANH",
    SALARY: "DIEN_BIEN_LUONG",
    WORK: "QUA_TRINH_CONG_TAC",
    WORK_OUT: "QUA_TRINH_CONG_TAC_NGOAI",
    DECPLINE: "KY_LUAT",
    REWARD: "KHEN_THUONG",
    FAMILY_DEDUCTIONS: "THONG_TIN_CO_BAN",
    CONCURRENT_PROCESS: "THONG_TIN_CO_BAN",
    MANAGER_PROCESS: 'NGUOI_QUAN_LY_TRUC_TIEP'
  }

  public static readonly EXCEL_TYPE_1 = 'xls';
  public static readonly EXCEL_MIME_1 = 'application/vnd.ms-excel';

  public static readonly EXCEL_TYPE_2 = 'xlsx';
  public static readonly EXCEL_MIME_2 = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';

  public static readonly PDF_TYPE = 'pdf';
  public static readonly PDF_MIME = 'application/pdf';

  public static readonly APPROVE = {
    IS_INITIALIZATION: 0, //Khởi tạo
    IS_APPROVE: 1, // Phê duyệt
    IS_NOT_APPROVE: 2 // Từ chối phê duyệt
  };

  public static readonly FLAG_STATUS = {
    RETIRED: 0,
    IS_WORKING: 1
  };

  public static readonly IDENTITIES = {
      IS_MAIN: 1, // Là số định danh chính
  };

  public static readonly BANK_ACCOUNT = {
    IS_PAYMENT_ACCOUNT: 1, // Là tài khoản chi lương
  };

  public static readonly CATALOGS = {
    GIOI_TINH: 'GIOI_TINH',
    DAN_TOC: 'DAN_TOC',
    TON_GIAO: 'TON_GIAO',
    TINH_TRANG_HON_NHAN: 'TINH_TRANG_HON_NHAN',
    TINH: 'TINH',
    HUYEN: 'HUYEN',
    XA: 'XA',
    LOAI_TAI_KHOAN: 'LOAI_TAI_KHOAN',
    LOAI_GIAY_TO: 'LOAI_GIAY_TO',
    MA_VUNG_DIEN_THOAI: 'MA_VUNG_DIEN_THOAI',
    QUOC_GIA: 'QUOC_GIA',
    DOI_TUONG_CV: 'DOI_TUONG_CV',
    CAP_BAC_QUAN_HAM: 'CAP_BAC_QUAN_HAM',
    LEVEL_NV: 'LEVEL_NV',
    PHAN_LOAI_HOP_DONG: 'PHAN_LOAI_HOP_DONG',
    LOAI_HINH_DT: 'LOAI_HINH_DT',
    LOAI_BANG_CAP: 'LOAI_BANG_CAP',
    HE_DT: 'HE_DT',
    HINHTHUC_DAOTAO: 'HINHTHUC_DAOTAO',
    XEP_LOAI_DT: 'XEP_LOAI_DT',
    XEP_LOAI_TOT_NGHIEP: 'XEP_LOAI_TOT_NGHIEP',
    MOI_QUAN_HE_NT: 'MOI_QUAN_HE_NT',
    TINH_TRANG_NT: 'TINH_TRANG_NT',
    DOITUONG_CHINHSACH: 'DOITUONG_CHINHSACH',
    CAP_QD_KHENTHUONG: 'CAP_QD_KHENTHUONG',
    HINHTHUC_KHENTHUONG: 'HINHTHUC_KHENTHUONG',
    LOAI_KHENTHUONG: 'LOAI_KHENTHUONG',
    HINHTHUC_KYLUAT: 'HINHTHUC_KYLUAT',
    LOAI_HO_SO: 'LOAI_HO_SO',
    LOAI_PHU_CAP: 'LOAI_PHU_CAP',
    CAP_QD_KYLUAT: 'CAP_QD_KYLUAT',
    NOI_CAP_CCCD: "NOI_CAP_CCCD",
    NOI_CAP_CMND: "NOI_CAP_CMND"
  }

  public static readonly CONTRACT_TYPE_CODE = {
    HOP_DONG: 'HD',
    PHU_HOP_DONG: 'PHD',
    PHU_LUC_TAM_HOAN: 'PLTH'
  }

  public static readonly DOCUMENT_TYPE = {
    TYPE_OUT: 'OUT',
    TYPE_IN: 'IN',
  }


  public static readonly TYPE_CODE = {
    DOI_TUONG_CV: 'DOI_TUONG_CV',
    KHU_VUC: 'KHU_VUC',
    LINE: 'LINE',
    POSITION: 'POSITION'
  }

  public static readonly JOB_TYPE = {
    ORG: 'ORG'
  }

  public static readonly STATUS_EMP = [
    { value: '1', label: 'staffManager.label.workIn' , color: '#06A561', bgColor: '#DAF9EC'},
    { value: '2', label: 'staffManager.label.contractPending' , color: '#F99600', bgColor: '#FFF2DA'},
    { value: '3', label: 'staffManager.label.workOut' , color: '#FA0B0B', bgColor: '#FDE7EA'}
  ];

  public static readonly UPLOAD_STATUS = [
    { value: '1', label: 'staffManager.acceptance.notUpload' , color: '#FA0B0B', bgColor: '#FDE7EA'},
    { value: '2', label: 'staffManager.acceptance.uploaded' , color: '#06A561', bgColor: '#DAF9EC'}
  ];

  public static readonly APPROVAL_STATUS = [
    { value: '0', label: 'staffManager.label.initial', color: '#F99600', bgColor: '#FFF2DA'},
    { value: '1', label: 'staffManager.label.waitConfirm', color: '#F99600', bgColor: '#FFF2DA'},
    { value: '2', label: 'staffManager.label.waitApproval', color: '#F99600', bgColor: '#FFF2DA'},
    { value: '3', label: 'staffManager.label.isApprove', color: '#06A561', bgColor: '#DAF9EC'},
    { value: '4', label: 'staffManager.label.isNotApprove', color: '#FA0B0B', bgColor: '#FFC9C9'},
  ]

  public static readonly RECRUIT_TYPE = [
    { value: 'NEW', label: 'staffManager.recruit.recruitmentNew'},
    { value: 'OS', label: 'staffManager.recruit.recruitmentOS'},
    { value: 'REHIRE', label: 'staffManager.recruit.recruitmentAgain'}
  ]

  public static readonly RECRUIT_TYPE_NUMBER = {
    REHIRE: 1,
    NEW: 2
  };

  public static readonly SEND_REQUEST = {
    YES: 1,
    CANCEL: 0
  };

  public static readonly SCREEN_MODE = {
    CREATE: 'CREATE',
    APPROVAL: 'APPROVAL'
  }

  public static readonly DUPLICATE_TYPE = [
    {value: 1, label: 'staffManager.recruit.duplicateIdNo'},
    {value: 2, label: 'staffManager.recruit.duplicateDateOfBirth'},
  ]

  public static readonly SALARY_GRADE = [
    {value: '1', label: '1'},
    {value: '2', label: '2'},
    {value: '3', label: '3'},
    {value: '4', label: '4'},
    {value: '5', label: '5'},
    {value: '6', label: '6'},
    {value: '7', label: '7'},
    {value: '8', label: '8'},
    {value: '9', label: '9'},
    {value: '10', label: '10'},
    {value: '11A', label: '11A'},
    {value: '11B', label: '11B'},
    {value: '12A', label: '12A'},
    {value: '12B', label: '12B'},
    {value: '13', label: '13'},
    {value: '14', label: '14'},
    {value: '15', label: '15'},
    {value: '16', label: '16'},
    {value: '17', label: '17'}
  ]

  public static readonly SALARY_RANK = [
    {value: '1', label: 'Bậc 1'},
    {value: '2', label: 'Bậc 2'},
    {value: '3', label: 'Bậc 3'},
    {value: '4', label: 'Bậc 4'},
    {value: '5', label: 'Bậc 5'},
    {value: '6', label: 'Bậc 6'},
    {value: '7', label: 'Bậc 7'},
    {value: '8', label: 'Bậc 8'},
    {value: '9', label: 'Bậc 9'},
    {value: '10', label: 'Bậc 10'},
  ]

  public static readonly TREATMENT_ALLOWANCE = [
    {value: 'BQL kho tiền CN', label: 'staffManager.recruit.treatment1'},
    {value: 'BQL kho tiền PGD', label: 'staffManager.recruit.treatment2'},
    {value: 'Lái xe kho tiền', label: 'staffManager.recruit.treatment3'},
    {value: 'Lái xe chi nhánh', label: 'staffManager.recruit.treatment4'},
  ]

  public static readonly MENU_STAFF_REPORT: NzSafeAny = [{
    "id": 7,
    "url": "/staff/staff-report/",
    "name": "staffManager.breadcrumb.staffReport",
    "parentId": 0,
    "icon": "/assets/icon/button/file.png",
    "iconActive": "/assets/icon/button/file.png",
    "showIcon": false,
    "childrens": [
      {
        "id": 71,
        "code": "EMPLOYEE_REPORT_SUMMARY",
        "url": "/staff/staff-report/employee_report_summary",
        "name": "staffManager.breadcrumb.staffReportSummary",
        "parentId": 1,
        "showIcon": false
      },
      {
        "id": 72,
        "code": "EMPLOYEE_REPORT_EMP_FULL",
        "url": "/staff/staff-report/employee_report_emp_full",
        "name": "staffManager.breadcrumb.staffReportFull",
        "parentId": 1,
        "showIcon": false
      },
      {
        "id": 73,
        "code": "EMPLOYEE_REPORT_TERMINATION",
        "url": "/staff/staff-report/employee_report_termination",
        "name": "staffManager.breadcrumb.staffReportEmpTermination",
        "parentId": 1,
        "showIcon": false
      },
      {
        "id": 74,
        "code": "EMPLOYEE_REPORT_TRANSFER",
        "url": "/staff/staff-report/employee_report_transfer",
        "name": "staffManager.breadcrumb.staffReportEmpTransfer",
        "parentId": 1,
        "showIcon": false
      },
      {
        "id": 75,
        "code": "EMPLOYEE_REPORT_LEAVE",
        "url": "/staff/staff-report/employee_report_leave",
        "name": "staffManager.breadcrumb.staffReportEmpLeave",
        "parentId": 1,
        "showIcon": false
      },
      {
        "id": 75,
        "code": "EMPLOYEE_REPORT_LEAVE",
        "url": "/staff/staff-report/employee_report_resume",
        "name": "staffManager.breadcrumb.staffReportEmpResume",
        "parentId": 1,
        "showIcon": false
      }
    ]
  }]

  public static readonly LOCAL_STORAGE_KEY = {
    PREVIOUS_SEARCH_FORM: {
      BASIC_SEARCH: 'staff-manager__research--basic__previousSearchForm',
      IDENTITY_SEARCH: 'staff-manager__research--identity__previousSearchForm',
      BANK_ACCOUNT_SEARCH: 'staff-manager__research--bankAccount__previousSearchForm',
      FAMILY_RELATIONSHIP_SEARCH: 'staff-manager__research--familyRelationship__previousSearchForm',
      DEPENDENT_PERSON_SEARCH: 'staff-manager__research--dependentPerson__previousSearchForm',
      WORK_HIS_BEFORE_SEARCH: 'staff-manager__research--workHisBefore__previousSearchForm',
      WORK_HIS_SEARCH: 'staff-manager__research--workHis__previousSearchForm',
      PROJECT_HIS_SEARCH: 'staff-manager__research--projectHis__previousSearchForm',
      CONTRACT_HIS_SEARCH: 'staff-manager__research--contractHis__previousSearchForm',
      MANAGER_PROCESS_SEARCH: 'staff-manager__research--managerProcess__previousSearchForm',
      DEGREE_SEARCH: 'staff-manager__research--degree__previousSearchForm',
      EDU_HIS_SEARCH: 'staff-manager__research--eduHis__previousSearchForm',
      SALARY_HIS_SEARCH: 'staff-manager__research--salaryHis__previousSearchForm',
      ALLOWANCE_HIS_SEARCH: 'staff-manager__research--allowanceHis__previousSearchForm',
      REWARD_RECORD_SEARCH: 'staff-manager__research--rewardRecord__previousSearchForm',
      DISCIPLINE_SEARCH: 'staff-manager__research--discipline__previousSearchForm',
    }
  }

}

export class ReportFunctionCode {
  public static readonly EMPLOYEE_REPORT_SUMMARY = 'EMPLOYEE_REPORT_SUMMARY';
  public static readonly EMPLOYEE_REPORT_EMP_FULL = 'EMPLOYEE_REPORT_EMP_FULL';
  public static readonly EMPLOYEE_REPORT_TERMINATION = 'EMPLOYEE_REPORT_TERMINATION';
  public static readonly EMPLOYEE_REPORT_TRANSFER = 'EMPLOYEE_REPORT_TRANSFER';
  public static readonly EMPLOYEE_REPORT_LEAVE = 'EMPLOYEE_REPORT_LEAVE';
  public static readonly EMPLOYEE_REPORT_RESUME = 'EMPLOYEE_REPORT_RESUME';
}
