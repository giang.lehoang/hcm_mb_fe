export class UrlConstant {
  public static readonly API_VERSION = '/v1.0';

  public static readonly EVALUATE_INFO = '/share/score_employee';
  
  public static readonly CATEGORIES = {
    SAVE_DOCUMENT_TYPE: '/v2.0/document-types',
    GET_DOCUMENT_TYPE_BY_ID: '/v2.0/document-types/{id}',
    GET_FACULTY_BY_ID: '/v3.0/faculties/{id}',
    SAVE_FACULTY: '/v3.0/faculties',
    SAVE_SCHOOL: '/v2.0/schools',
    GET_SCHOOL_BY_ID: '/v2.0/schools/{id}',
    SAVE_MAJOR_LEVEL: '/v2.0/major-levels',
    GET_MAJOR_LEVEL_BY_ID: '/v2.0/major-levels/{id}'
  }

  public static readonly MANAGER_PROCESS = {
    GET_BY_ID: '/manager-processes/{id}',
    SAVE: '/manager-processes'
  }

  public static readonly BOOK_MARK_RESEARCH = {
    EXTEND_FIELD: '/search-item?moduleCode=',
    USER_BOOKMARK: '/user-bookmark',
    DELETE_BOOKMARK: '/user-bookmark/{id}'
  };

  public static readonly DOWNLOAD_FILE_ATTACH = '/download/file?docId=';
  public static readonly DOWNLOAD_FILE = '/download/file';

  public static readonly SEARCH_FORM = {
    BASIC_FORM: '/employees/personal-information',
    EDU_HIS: '/education-process',
    IDENTITY: '/identities',
    SALARY_HIS: '/salary-processes',
    ALLOWANCE_HIS: '/allowance-processes',
    WORK_HIS: '/work-processes',
    PROJECT_HIS: '/project-members',
    BANK: '/bank-accounts',
    FAMILY_RELATIONSHIPS: '/family-relationships',
    DEPENDENT_PERSONS: '/dependent-persons',
    CONTRACT: '/contract-processes',
    DEGREE:'/education-degree',
    DECPLINE:'/decpline-records',
    REWARD_RECORD: '/reward-records',
    DRAFT_REWARD_RECORD: '/reward-records/draft',
    DRAFT_DECPLINE: '/decpline-records/draft',
    LOOKUP_EMP_TYPE: '/lookup-values?typeCode=DOI_TUONG_CV',
    DRAFT_PROJECT_MEMBER: '/project-members/draft',
    DRAFT_EDUCATION_PROCESS: '/education-process/draft',
    DRAFT_EDUCATION_REGREE: '/education-degree/draft',
    DRAFT_ALLOWANCE_PROCESS: '/allowance-processes/draft',
    DRAFT_MANAGER_PROCESS: '/work-processes/manager-process/draft',
    DRAFT_CONTRACT_PROCESS: '/contract-processes/search-draft',
    DRAFT_SALARY_PROCESS: '/salary-processes/search-draft',
    WORK_HIS_BEFORE: '/worked-outsides',
    RECRUITMENT_NEW_SEARCH: '/candidates/employee/search',
    RECRUITMENT_OS_SEARCH: '/rehire-os-employees/employee/search',
    ACCEPTANCE_RECORDS: '/acceptance-records',
    MANAGER_PROCESS: '/manager-processes',
    APPROVE_RECRUITMENT: '/candidates',
    REHIRE_OS: '/rehire-os-employees',
    LINE: '/mp-position-groups/{pgrType}',
    DOCUMENT_TYPES: '/v2.0/document-types',
    FACULTIES: '/v3.0/faculties',
    SCHOOLS: '/v2.0/schools',
    MAJOR_LEVEL: '/v2.0/major-levels'
  };

    public static readonly SEARCH_FORM_DRAFT = {
        IDENTITY: '/identities/search-draft',
        BANK: '/bank-accounts/search-draft',
        FAMILY_RELATIONSHIP: '/family-relationships/search-draft',
        DEPENDENT_PERSON: '/dependent-persons/search-draft',
        WORK_PROCESS: '/work-processes/draft',
        CONCURRENT_PROCESS: '/concurrent-process/draft'
    };

  public static readonly IMPORT_FORM = {
    BASIC_TEMPLATE: '/employees/export-template-add',
    BASIC_FORM: '/employees/import-add',
    DRAFT_DECPLINE_FORM : '/decpline-records/import-process',
    DECPLINE_TEMPLATE: '/decpline-records/import-template',
    REWARD_RECORD_TEMPLATE: '/reward-records/import-template',
    DRAFT_REWARD_RECORD_FORM: '/reward-records/import-process',
    PERSONAL_IDENTITIES_TEMPLATE: '/identities/import-template',
    PROJECT_MEMBER_TEMPLATE: '/project-members/import-template',
    DRAFT_PROJECT_MEMBER_FORM: '/project-members/import-process',
    EDUCATION_PROCESS_TEMPLATE: '/education-process/import-template',
    DRAFT_EDUCATION_PROCESS_FORM: '/education-process/import-process',
    EDUCATION_DEGREE_TEMPLATE: '/education-degree/import-template',
    DRAFT_EDUCATION_DEGREE_FORM: '/education-degree/import-process',
    ALLOWANCE_PROCESS_TEMPLATE: '/allowance-processes/import-template',
    DRAFT_ALLOWANCE_PROCESS_FORM: '/allowance-processes/import-process',
    MANAGER_PROCESS_TEMPLATE: '/work-processes/manager-process/import-template',
    DRAFT_MANAGER_PROCESS_FORM: '/work-processes/manager-process/import-process',
    BANK_ACCOUNTS_TEMPLATE: '/bank-accounts/import-template',
    FAMILY_RELATIONSHIP_TEMPLATE: '/family-relationships/import-template',
    DEPENDENT_PERSON_TEMPLATE: '/dependent-persons/import-template',
    WORK_PROCESS_TEMPLATE: '/work-processes/import-template',
    CONCURRENT_PROCESS_TEMPLATE: '/concurrent-process/import-template',
    DRAFT_CONTRACT_PROCESS_FORM: '/contract-processes/import-process',
    CONTRACT_PROCESS_TEMPLATE: '/contract-processes/import-template',
    DRAFT_SALARY_PROCESS_FORM: '/salary-processes/import-process',
    SALARY_PROCESS_TEMPLATE: '/salary-processes/import-template',
    DECPLINE_FORM : '/decpline-records/import-add',
    ADD_NEW_EMPLOYEE: '/candidates/import-process',
    ADD_NEW_EMPLOYEE_TEMPLATE: '/candidates/export-template',
    RECRUIT_FROM_OS_TEMPLATE: '/rehire-os-employees/os/export-template',
    RECRUIT_FROM_OS: '/rehire-os-employees/os/import-process',
    MANAGER_PROCESS: '/manager-processes/import-template',
    MANAGER_PROCESS_FORM: '/manager-processes/import-process',
    MANAGER_PROCESS_IMPORT_TEMPLATE: '/manager-processes/import-template',
    MANAGER_PROCESS_IMPORT: '/manager-processes/import-process',
    MANAGER_DIRECT_IMPORT: '/manager-processes/import-data',

  };

    public static readonly IMPORT_EXCEL = {
        PERSONAL_IDENTITIES: '/identities/import-process',
        BANK_ACCOUNT: '/bank-accounts/import-process',
        FAMILY_RELATIONSHIP: '/family-relationships/import-process',
        DEPENDENT_PERSON: '/dependent-persons/import-process',
        WORK_PROCESS: '/work-processes/import-process',
        CONCURRENT_PROCESS: '/concurrent-process/import-process'
    };

  public static readonly EXPORT_REPORT = {
    DECPLINE:'/decpline-records/export',
    REWARD_RECORD: '/reward-records/export',
    BASIC_FORM:'/employees/export-personal-information',
    ALLOWACE_FORM:'/allowance-processes/export',
    BANK_FORM:'/bank-accounts/export',
    CONTRACT_FORM:'/contract-processes/export',
    DEGREE_FORM:'/education-degree/export',
    EDU_HIS_FORM:'/education-process/export',
    FAMILY_FORM:'/dependent-persons/export',
    IDENTITY_FORM:'/identities/export',
    PROJECT_HIS:'/project-members/export',
    RELATIVES_FORM:'/family-relationships/export',
    SALARY_FORM:'/salary-processes/export',
    WORK_HIS:'/work-processes/export',
    WORK_HIS_BEFORE:'/worked-outsides/export',
    RECRUITMENT_EXPORT: '/employee-infos/recruitment/export',
    ACCEPTANCE_RECORDS: '/acceptance-records/export',
    MANAGER_PROCESS: '/manager-processes/export',
    REHIRE_OS: '/rehire-os-employees/export',
    RECRUIT_NEW: '/candidates/export',
    DOCUMENT_TYPES: '/v2.0/document-types/export',
    FACULTIES: '/v3.0/faculties/export',
    SCHOOLS: '/v2.0/schools/export',
    MAJOR_LEVEL: '/v2.0/major-levels/export'
  };
  public static readonly APPROVE_ALL_DRAFT = {
    IDENTITY: '/identities/approve-all',
    ALLOWANCE_PROCESS: '/allowance-processes/approve-all',
    DECPLINE_RECORD: '/decpline-records/approve-all',
    EDUCATION_DEGREE: '/education-degree/approve-all',
    EDUCATION_PROCESS: '/education-process/approve-all',
    PROJECT_MEMBER: '/project-members/approve-all',
    REWARD_RECORD: '/reward-records/approve-all',
    MANAGER_PROCESS: '/work-processes/manager-process/approve-all',
    BANK_ACCOUNT: '/bank-accounts/approve-all',
    FAMILY_RELATIONSHIP: '/family-relationships/approve-all',
    DEPENDENT_PERSON: '/dependent-persons/approve-all',
    WORK_PROCESS: '/work-processes/approve-all',
    CONTRACT_PROCESS: '/contract-processes/approve-all',
    SALARY_PROCESS: '/salary-processes/approve-all',
    CONCURRENT_PROCESS: '/concurrent-process/approve-all'
  };

    public static readonly APPROVE_DRAFT = {
        IDENTITY: '/identities/approve-by-id',
        IDENTITY_LIST: '/identities/approve-by-list',
        REWARD_RECORD: '/reward-records/approve-by-id',
        DECPLINE_RECORD: '/decpline-records/approve-by-id',
        PROJECT_MEMBER: '/project-members/approve-by-id',
        EDUCATION_PROCESS: '/education-process/approve-by-id',
        EDUCATION_DEGREE: '/education-degree/approve-by-id',
        ALLOWANCE_PROCESS: '/allowance-processes/approve-by-id',
        MANAGER_PROCESS: '/work-processes/manager-process/approve-by-id',
        BANK_ACCOUNT: '/bank-accounts/approve-by-id',
        BANK_ACCOUNT_LIST: '/bank-accounts/approve-by-list',
        FAMILY_RELATIONSHIP: '/family-relationships/approve-by-id',
        FAMILY_RELATIONSHIP_LIST: '/family-relationships/approve-by-list',
        DEPENDENT_PERSON: '/dependent-persons/approve-by-id',
        DEPENDENT_PERSON_LIST: '/dependent-persons/approve-by-list',
        WORK_PROCESS: '/work-processes/approve-by-id',
        WORK_PROCESS_LIST: '/work-processes/approve-by-list',
        CONTRACT_PROCESS: '/contract-processes/approve-by-id',
        SALARY_PROCESS: '/salary-processes/approve-by-id',
        CONCURRENT_PROCESS: '/concurrent-process/approve-by-id',
        CONCURRENT_PROCESS_LIST: '/concurrent-process/approve-by-list',
    };

    public static readonly APPROVE_LIST_DRAFT = {
      ALLOWANCE_PROCESS: '/allowance-processes/approve-by-list',
      DECPLINE_RECORD: '/decpline-records/approve-by-list',
      EDUCATION_DEGREE: '/education-degree/approve-by-list',
      EDUCATION_PROCESS: '/education-process/approve-by-list',
      PROJECT_MEMBER: '/project-members/approve-by-list',
      REWARD_RECORD: '/reward-records/approve-by-list',
      MANAGER_PROCESS: '/work-processes/manager-process/approve-by-list',
      CONTRACT_PROCESS: '/contract-processes/approve-by-list',
      SALARY_PROCESS: '/salary-processes/approve-by-list'
    }

    public static readonly REJECT_LIST_DRAFT = {
      ALLOWANCE_PROCESS: '/allowance-processes/reject-by-list',
      DECPLINE_RECORD: '/decpline-records/reject-by-list',
      EDUCATION_DEGREE: '/education-degree/reject-by-list',
      EDUCATION_PROCESS: '/education-process/reject-by-list',
      PROJECT_MEMBER: '/project-members/reject-by-list',
      REWARD_RECORD: '/reward-records/reject-by-list',
      MANAGER_PROCESS: '/work-processes/manager-process/reject-by-list',
      CONTRACT_PROCESS: '/contract-processes/reject-by-list',
      SALARY_PROCESS: '/salary-processes/reject-by-list'
    }

    public static readonly REFUSE_APPROVE_DRAFT = {
        IDENTITY: '/identities/reject-by-id',
        IDENTITY_LIST: '/identities/reject-by-list',
        BANK_ACCOUNT: '/bank-accounts/reject-by-id',
        BANK_ACCOUNT_LIST: '/bank-accounts/reject-by-list',
        CONCURRENT_PROCESS: '/concurrent-process/reject-by-id',
        CONCURRENT_PROCESS_LIST: '/concurrent-process/reject-by-list',
        FAMILY_RELATIONSHIP: '/family-relationships/reject-by-id',
        FAMILY_RELATIONSHIP_LIST: '/family-relationships/reject-by-list',
        DEPENDENT_PERSON: '/dependent-persons/reject-by-id',
        DEPENDENT_PERSON_LIST: '/dependent-persons/reject-by-list',
        WORK_PROCESS: '/work-processes/reject-by-id',
        WORK_PROCESS_LIST: '/work-processes/reject-by-list',
        REWARD_RECORD: '/reward-records/reject-by-id',
        DECPLINE_RECORD: '/decpline-records/reject-by-id',
        PROJECT_MEMBER: '/project-members/reject-by-id',
        EDUCATION_PROCESS: '/education-process/reject-by-id',
        EDUCATION_DEGREE: '/education-degree/reject-by-id',
        ALLOWANCE_PROCESS: '/allowance-processes/reject-by-id',
        MANAGER_PROCESS: '/work-processes/manager-process/reject-by-id',
        CONTRACT_PROCESS: '/contract-processes/reject-by-id',
        SALARY_PROCESS: '/salary-processes/reject-by-id'
    };

  public static readonly DELETE_DRAFT = {
    ALLOWANCE_PROCESS: '/allowance-processes/draft/{id}',
    REWARD_RECORD: '/reward-records/draft/{id}',
    DECPLINE_RECORD: '/decpline-records/draft/{id}',
    PROJECT_MEMBER: '/project-members/draft/{id}',
    EDUCATION_PROCESS: '/education-process/draft/{id}',
    EDUCATION_DEGREE: '/education-degree/draft/{id}',
    MANAGER_PROCESS: '/work-processes/manager-process/draft/{id}',
    CONTRACT_PROCESS: '/contract-processes/draft/{id}',
    SALARY_PROCESS: '/salary-processes/draft/{id}',
    IDENTITY: '/identities/delete-identity/',
    BANK_ACCOUNT: '/bank-accounts/delete-bank-account/',
    FAMILY_RELATIONSHIP: '/family-relationships/delete-family-relationship/',
    DEPENDENT_PERSON: '/dependent-persons/delete-dependent-person/',
    WORK_PROCESS: '/work-processes/delete-work-process/',
    CONCURRENT_PROCESS: '/concurrent-process/draft/{id}',
  }

  public static readonly CATEGORY = {
    NUMBER_BADGE: '/category/draft-number-badge',
    GROUP_NUMBER_BADGE: '/category/group-draft-number-badge',
    LOOKUP_VALUE: '/lookup-values',
    MP_JOB: '/category/mp-jobs'

  }

  public static readonly CATALOG = '/lookup-values';

  public static readonly BANKS = '/cat-banks';

  public static readonly SCHOOLS = '/schools';

  public static readonly FACULTIES = '/faculties';

  public static readonly MAJOR_LEVELS = '/major_levels';

  public static readonly CATALOGS = {
    PREFIX: '/lookup-values',
    CONTRACT_TYPES: '/contract-types',
    JOBS: '/mp-jobs',
    PROJECTS: '/mp-projects',
    SALARY_GRADES: '/salary-grades',
    SALARY_RANKS: '/salary-ranks',
    DOCUMENT_TYPES: '/document-types',
    MB_POSITIONS: '/mp-positions/org/{orgId}'
  }

  public static readonly DEGREE_INFOS = {
    PREFIX: '/education-degree',
    LIST: '/employees'
  }

  public static readonly EDU_HIS_INFO = {
    PREFIX: '/education-process',
    LIST: '/employees'
  }

  public static readonly EMPLOYEES = {
    PREFIX: '/employees',
    DATA_PICKER: '/data-picker',
    PERSONAL_INFO: '/personal-information',
    IDENTITIES: '/identities',
    CONTACT: '/contact-info',
    BANK: '/bank-accounts',
    PARTY: '/party-army',
    AVATAR: '/avatar',
    MANAGER_INFO: '/employees/{employeeId}/personal-information'
  }

  public static readonly PERSONAL = {
    PREFIX: '/personal',
    DATA_PICKER: '/data-picker',
    PERSONAL_INFO: '/personal-information',
    IDENTITIES: '/identities',
    CONTACT: '/contact-info',
    BANK: '/bank-accounts',
    PARTY: '/party-army',
    AVATAR: '/avatar',
  }

  // Danh sách thân nhân
  public static readonly RELATIVES_INFO = {
    PREFIX: '/family-relationships',
    LIST: '/employees/{employeeId}',
    SAVE: '',
    DELETE: '/{familyRelationshipId}',
    DETAIL: '/{familyRelationshipId}'
  }

  // Danh sách giảm trừ gia cảnh
  public static readonly FAMILY_DEDUCTION = {
    PREFIX: '/dependent-persons',
    LIST: '/employees/{employeeId}',
    RELATIVE_INFO: '/employees/{employeeId}/family-relationships',
    SAVE: '',
    DELETE: '/{dependentPersonId}',
    DETAIL: '/{dependentPersonId}'
  }

  // Danh sách làm việc trước MB
  public static readonly WORK_BEFORE_HISTORY = {
    PREFIX: '/worked-outsides',
    LIST: '/employees/{employeeId}',
    SAVE: '',
    DELETE: '/{workOutsideId}',
    GET_BY_ID: '/{id}',
    DETAIL: '/{workOutsideId}'
  }

  // Danh sách làm việc tại MB
  public static readonly WORK_HISTORY = {
    PREFIX: '/work-processes',
    LIST: '/employees/{employeeId}',
    SAVE: '',
    DELETE: '/{workProcessId}',
    DELETE_DRAFT: '/delete-work-process/{id}',
    DETAIL: '/{workProcessId}',
    GET_BY_ID: '/{id}',
    GET_DRAFT_BY_ID: '/draft/{id}'
  }

  // Quá trình lương
  public static readonly SALARY_PROGRESS = {
    PREFIX: '/salary-processes',
    LIST: '/employees/{employeeId}',
    SAVE: '',
    DELETE: '/{salaryProcessId}',
    GET_BY_ID: '/{id}',
    DETAIL: '/{salaryProcessId}'
  }

  // Quá trình tham gia dự án
  public static readonly PROJECT_HISTORY_INFO = {
    PREFIX: '/project-members',
    LIST: '/employees/{employeeId}',
    SAVE: '',
    DELETE: '/{projectMemberId}',
    DETAIL: '/{projectMemberId}'
  }

  // Quá trình hợp đồng
  public static readonly CONTRACT_HISTORY_INFO = {
    PREFIX: '/contract-processes',
    LIST: '/employees/{employeeId}',
    SAVE: '',
    GET_BY_ID: '/contract-processes/{id}',
    GET_DRAFT_BY_ID: '/contract-processes/draft/{id}',
    DELETE: '/{contractProcessId}',
    DELETE_DRAFT: '/draft/{id}',
    DETAIL: '/{contractProcessId}'
  }

  // Quá trình kiêm nhiệm
  public static readonly CONCURRENT_PROCESS_INFO = {
    PREFIX: '/concurrent-process',
    LIST: '/employees/{employeeId}',
    SAVE: '',
    DELETE: '/{concurrentProcessId}',
    DETAIL: '/{concurrentProcessId}',
    DELETE_DRAFT: '/draft/{concurrentProcessId}',
    GET_DRAFT_BY_ID: '/draft/{concurrentProcessId}'
  }

  // Quá trình phụ cấp
  public static readonly ALLOWANCE_HISTORY_INFO = {
    PREFIX: '/allowance-processes',
    LIST: '/employees/{employeeId}',
    SAVE: '',
    DELETE: '/{allowanceProcessId}',
    GET_BY_ID: '/{id}',
    DETAIL: '/{allowanceProcessId}'
  }

  // Quá trình khen thưởng
  public static readonly REWARD_HISTORY_INFO = {
    PREFIX: '/reward-records',
    LIST: '/employees/{employeeId}',
    SAVE: '',
    DELETE: '/{rewardRecordId}',
    DETAIL: '/{rewardRecordId}'
  }

  // Quá trình kỷ luật
  public static readonly DISCIPINARY_HISTORY_INFO = {
    PREFIX: '/decpline-records',
    LIST: '/employees/{employeeId}',
    SAVE: '',
    DELETE: '/{decplineRecordId}',
    GET_BY_ID: '/{id}',
    DETAIL: '/{decplineRecordId}'
  }

  // Hồ sơ đính kèm
  public static readonly DOCUMENT_ATTACHMENT = {
    PREFIX: '/employee-profiles',
    LIST: '/employees/',
    SAVE: '',
    GET_BY_ID: '/employee-profiles/{id}',
    DELETE: '/{employee-profile-id}',
    DETAIL: '/{employee-profile-id}'
  }

  public static readonly RECRUITMENT = {
    SAVA_DATA: '/rehire-os-employees',
    SAVE_RECRUIT_NEW: '/candidates',
    GET_CANDIDATE_BY_ID: '/candidates/{id}',
    GET_REHIRE_OS_BY_ID: '/rehire-os-employees/{id}',
    APPROVAL_RECRUIT_NEW_BY_ID: '/candidates/approve-by-id',
    APPROVAL_REHIRE_OS_BY_ID: '/rehire-os-employees/approve-by-id',
    APPROVAL_RECRUIT_NEW_BY_LIST: '/candidates/approve-by-list',
    APPROVAL_RECRUIT_NEW_ALL: '/candidates/approve-all',
    APPROVAL_REHIRE_OS_BY_LIST: '/rehire-os-employees/approve-by-list',
    APPROVAL_REHIRE_OS_ALL: '/rehire-os-employees/approve-all',
    REJECT_RECRUIT_NEW_BY_ID: '/candidates/reject-by-id',
    REJECT_RECRUIT_NEW_BY_LIST: '/candidates/reject-by-list',
    REJECT_REHIRE_OS_BY_ID: '/rehire-os-employees/reject-by-id',
    REJECT_REHIRE_OS_BY_LIST: '/rehire-os-employees/reject-by-list',
    CONFIRM_BY_LIST: '/candidates/confirm-by-list',
    SEND_REQUEST_NEW: '/candidates/request-or-cancel-approve-by-list',
    SEND_REQUEST_OS: '/rehire-os-employees/request-or-cancel-approve-by-list',
    SEND_REQUEST_NEW_ALL: '/candidates/request-waiting-approve-all',
    SEND_REQUEST_OS_ALL: '/rehire-os-employees/request-waiting-approve-all',
  }

  public static readonly ACCEPTANCE = {
    UPLOAD_FILE: '/acceptance-records/upload-file'
  }

  public static readonly URL_NAVIGATE = {
    'PERSONAL_INFO': '/staff/personal-info',
    'EDIT_APPROVAL_CANDIDATE_NEW': '/staff/recruit/approval-candidate/edit-recruitment-new?id=',
    'DETAIL_APPROVAL_CANDIDATE_NEW': '/staff/recruit/approval-candidate/detail-recruitment-new?id=',
    'EDIT_PROPOSAL_CANDIDATE_NEW': '/staff/recruit/proposal-candidate/edit-recruitment-new?id=',
    'ADD_PROPOSAL_CANDIDATE_NEW': '/staff/recruit/proposal-candidate/add-recruitment-new',
    'DETAIL_PROPOSAL_CANDIDATE_NEW': '/staff/recruit/proposal-candidate/detail-recruitment-new?id=',
    'DETAIL_RECRUITMENT_OS': '/staff/recruit/proposal-candidate/detail-recruitment-os?id=',
    'DETAIL_APPROVAL_RECRUITMENT_OS': '/staff/recruit/approval-candidate/detail-recruitment-os?id=',
    'EDIT_RECRUITMENT_OS': '/staff/recruit/proposal-candidate/edit-recruitment-os?id=',
    'ADD_RECRUITMENT_OS': '/staff/recruit/proposal-candidate/add-recruitment-os',
    'DETAIL_RECRUITMENT_REHIRE': '/staff/recruit/proposal-candidate/detail-recruitment-rehire?id=',
    'DETAIL_APPROVAL_RECRUITMENT_REHIRE': '/staff/recruit/approval-candidate/detail-recruitment-rehire?id=',
    'EDIT_RECRUITMENT_REHIRE': '/staff/recruit/proposal-candidate/edit-recruitment-rehire?id=',
    'ADD_RECRUITMENT_REHIRE': '/staff/recruit/proposal-candidate/add-recruitment-rehire'
  }

  // employee report
  public static readonly URL_EMPLOYEE_REPORT = {
    SUMMARY: 'v1.0/employees/export-summary',
    EMP_FULL: 'v1.0/employees/export-full',
    TERMINATION: 'v1.0/employees/export-emp-termination',
    TRANSFER: 'v1.0/employees/export-emp-transfer',
    LEAVE: 'v1.0/employees/export-emp-leave',
    RESUME: 'v1.0/employees/export-emp-resume/{reportType}',
  }

}
