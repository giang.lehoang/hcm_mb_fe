export class CandidateWorkedOutside {
  organizationName?: string;
  positionName?: string;
  fromDate?: string | null;
  toDate?: string | null;
}
