export class CandidateSalary {
  candidateSalaryId: number | null = null;
  salaryGrade: string | null = null;
  salaryRank: string | null = null;
  salaryAmount: number | null = null;
  salaryPercent: number | null = null;
  thsFactor: number | null = null;
  thsAmount: number | null = null;
  thsPercent: number | null = null;
  treatmentAllowance: string | null = null;
  monthlyBonus: number | null = null;
  salaryMonth: number | null = null;
  salaryMonthByRate: number | null = null;
  baseProposal: string | null = null;
  durationFactor: number | null = null;
  rate: string | null = null;
  natureOfWork: string | null = null;
  monthlyBonusDate: string | null = null;
  commitmentAmount: number | null = null;
  commitmentFromDate: string | null = null;
  commitmentToDate: string | null = null;
  allowanceAmount: number | null = null;
  allowanceFromDate: string | null = null;
  allowanceToDate: string | null = null;
}
