export class SearchForm {
    employeeCode: string;
    fullName: string;
    empTypeCode: string;
    organizationId: string;
    status: number;
}
