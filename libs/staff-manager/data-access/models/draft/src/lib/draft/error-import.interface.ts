export interface ErrorImport {
    row: number;
    column: number;
    columnLabel: string;
    description: string;
    content: string;
}
