export class Degree {
  degreeId: number;
  employeeCode: string;
  fullName: string;
  orgName: string;
  fromDate: string;
  toDate: string;
  courseName: string;
  eduMethodTypeName: string;
  courseContent: string;
  signedDate: string;
  flagStatus: number;
}
