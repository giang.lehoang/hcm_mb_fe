export class MenusDraft {
  icon: string;
  iconActive: string;
  id: number;
  code: string;
  name: string;
  parentId: number;
  showIcon: boolean;
  url: string;
  type?:string;
  numberBadge?:number;
  childrens?: MenusDraft[];
}
