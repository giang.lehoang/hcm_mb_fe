export class ContractHis {
  contractProcessId?: number;
  employeeCode?: string;
  fullName?: string;
  orgName?: string;
  fromDate?: string;
  toDate?: string;
  contractTypeName?: string;
  contractNumber?: string;
  signedDate?: string;
  flagStatus?: number;
}
