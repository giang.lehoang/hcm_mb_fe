export class DependentPerson {
    dependentPersonId: number;
    draftDependentPersonId: number;
    employeeId: number;
    employeeCode: string;
    familyRelationshipId: number;
    fromDate: Date
    toDate: Date
    taxOrganizationId: number;
    provinceCode: string;
    districtCode: string;
    wardCode: string;
    codeNo: string;
    bookNo: string;
    flagStatus: number;
    status: number;
    createdBy: string;
    createDate: Date
    lastUpdatedBy: string;
    lastUpdateDate: Date
    personalId: string;
    passportNo: string;
    nationCode: string;
    note: string;
}
