export class BankAccount {
    bankAccountId: number;
    draftBankAccountId: number;
    employeeId: number;
    employeeCode: string;
    accountNo: string;
    bankId: number;
    bankBranch: string;
    isPaymentAccount: number;
    accountTypeCode: string;
    fromDate: Date;
    toDate: Date;
    status: number;
    flagStatus: number;
    createdBy: string;
    createDate: Date;
    lastUpdatedBy: Date;
    lastUpdateDate: Date;
}
