export class Decpline {
    employeeId: number;
    employeeCode: string;
    fullName: string;
    orgName: string;
    amount: string;
    reason: string;
    signedDate: string;
    disciplineLevelName: string;
    disciplineMethodName: string;
    flagStatus: number;
  }