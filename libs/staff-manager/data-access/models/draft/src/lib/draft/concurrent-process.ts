export class ConcurrentProcess {
    concurrentProcessId?: number;
    draftConcurrentProcessId?: number;
    employeeId?: number;
    employeeCode: string;
    workProcessId?: number;
    positionId?: number;
    posName?: string;
    organizationId?: number;
    orgName?: string;
    documentNo?: string;
    path?: string;
    parentId?: number;
    note?: string;
    fromDate?: string;
    toDate?: string;
    signedDate?: string;
    empTypeCode?: string;
    empTypeName?: string;
    startRecord?: number;
    pageSize?: number;
    org?: OrgDTO;
    isTrial?: number;
    managerId?: number;
    status?: number;
    planToDate1?: string;
    planToDate2?: string;
    attachFileList?: DocumentsDTO[];
    docIdsDelete?: number[];
};

export interface OrgDTO {
  orgId?: number;
  orgName?: string;
  path?: string;
};

export interface DocumentsDTO {
  docId?: number;
  security?: string;
  fileName?: string;
  mimeType?: string;
};
