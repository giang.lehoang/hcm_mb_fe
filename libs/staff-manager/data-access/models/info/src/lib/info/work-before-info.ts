export class WorkBeforeInfo {
  workedOutsideId: number; //ID bản ghi.
  employeeId: number; //ID nhân viên.
  fromDate: string | null; //Từ ngày
  toDate: string | null; // Đến ngày
  organizationName: string; // Tên đơn vị
  positionName: string; // Vị trí
  referPerson: string; // Người liên hệ
  referPersonPosition: string; // Chức danh người liên hệ
  mission: string; // Nhiệm vụ chính
  reasonLeave: string; // Lý do nghỉ việc
  rewardInfo: string; // Thông tin khen thưởng
  decplineInfo: string; // Thông tin kỷ luật
}
