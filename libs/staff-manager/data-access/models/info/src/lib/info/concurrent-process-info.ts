export interface ConcurrentProcessInfo {
    concurrentProcessId?: number;
    draftConcurrentProcessId?: number;
    employeeId?: number;
    workProcessId?: number;
    positionId?: number;
    organizationId?: number;
    orgName?: string;
    path?: string;
    parentId?: number;
    fromDate?: string | null;
    toDate?: string | null;
    signedDate?: string | null;
    documentNo?: string;
    note?: string;
    empTypeCode?: string;
    startRecord?: number;
    pageSize?: number;
    org?: OrgDTO;
    posName?: string;
    listManager?: Manager[];
    attachFileList?: Array<ListFileAttach>;
    docIdsDelete?: number[];
    positionGroupId?: number;
    positionLevel?: string;
    status: number;
    inputType: string;
    isCloneFile?: number;
    pgrName?: string;
    positionLevelStr?: string;
}

interface ListFileAttach {
    docId: string;
    fileName: string;
    security?: string;
}

interface Manager {
    managerProcessId?: number;
    positionId: number;
    fromDate: string | null;
    managerId: number;
    managerName: string
}

interface OrgDTO {
    orgId?: number;
    orgName?: string;
    path?: string;
}
