export interface EducationDegree {
    educationDegreeId?: number;
    degreeName?: string;
    employeeId?: number;
    degreeTypeCode?: string;
    degreeTypeName?: string;
    issueYear?: string;
    schoolId?: number;
    schoolName?: string;
    facultyId?: number;
    facultyName?: string;
    majorLevelId?: number;
    majorLevelName?: string;
    eduRankCode?: string;
    eduRankName?: string;
    isHighest?: number;
    note?: string;
    fromDate?: Date;
    toDate?: Date;
    startRecord?: number;
    pageSize?: number;
    flagStatus?: number;
    orgName?: string;
    fullName?: string;
    employeeCode?: string;
    status?: number;
    draftEducationDegreeId?: number;
}
