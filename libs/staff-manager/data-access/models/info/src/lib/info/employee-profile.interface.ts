import {ListFileAttach} from "./list-file-attach";

export interface EmployeeProfile {
    employeeProfileId: number;
    employeeId: number;
    profileTypeCode: string;
    profileTypeName: string;
    note: string;
    isHardDocument: number;
    saveDocumentOrigin: string;
    createdBy: string;
    createDate: string;
    startRecord: number;
    pageSize: number;
    listFileAttach: Array<ListFileAttach>;
    attachFileList: Array<ListFileAttach>;
    docIdsDelete: [];
}

