export interface BankAccount {
    bankAccountId?: number;
    draftBankAccountId?: number;
    employeeId?: number;
    accountNo?: string;
    bankId?: number;
    bankName?: string;
    bankBranch?: string;
    isPaymentAccount?: number;
    accountTypeCode?: string;
    accountTypeName?: string;
    fromDate?: Date;
    toDate?: Date;
    startRecord?: number;
    pageSize?: number;
    fullName?: string;
    employeeCode?: string;
    orgId?: number;
    orgName?: string;
    label?: string;
    empTypeCode?: string;
    flagStatus?: number;
    status?: number;
}
