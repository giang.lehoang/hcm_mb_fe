export interface FacultyInfo {
    facultyId?: number;
    code?: string;
    name?: string;
    isTextManual?: number;
}
