import { EmployeeProfile } from './employee-profile.interface';

export interface EmployeeProfiles {
    files?: File[];
    data?: EmployeeProfile;
}
