export interface AllowanceProcess {
    allowanceProcessId?: number;
    employeeId?: number;
    allowanceTypeCode?: string;
    allowanceTypeName?: string;
    amountMoney?: number;
    fromDate?: Date;
    toDate?: Date;
    note?: string;
    startRecord?: number;
    pageSize?: number;
    employeeCode?: string;
    fullName?: string;
    orgName?: string;
    flagStatus?: number;
    status?: number;
    draftAllowanceProcessId?: number;
}
