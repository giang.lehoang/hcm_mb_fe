export class DependentPerson {
  dependentPersonId: number; // Id bản ghi
  employeeId: number; // Id nhân viên
  familyRelationshipId: number; // Id quan hệ gia đình
  relationTypeName: string; // Tên quan hệ gia đình
  fullName: string;
  taxNumber: string; // Mã số thuế
  personalIdNumber: string; // Số CCCD NPT
  codeNo: string; // Số GKS
  bookNo: string; // QUyển số GKS
  national: string; // Quốc gia
  provinceCode: string; // Mã tỉnh / thành phố
  districtCode: string; // Mã Quận / Huyện
  wardCode: string; // Mã Phường / Xã
  fromDate: string; // Giảm trừ từ tháng
  toDate: string; // Giảm trừ tới tháng
  note: string; // Ghi chú
}
