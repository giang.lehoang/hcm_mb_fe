export class AllowanceHistory {
  allowanceProcessId: number; // ID Bản ghi
  employeeId: number; // ID Nhân viên
  fromDate: string | null; // Từ ngày
  toDate: string | null; // Đến ngày
  allowanceTypeCode: string; // Loại phụ cấp .
  allowanceTypeName: string; // Tên Loại phụ cấp .
  amountMoney: number; // Mức hưởng.
  note: string;
}
