import { EduHisInfo } from './edu-his-info';

export class EduHisInfoGroup {
    employeeId: number;
    data: EduHisInfo;
}
