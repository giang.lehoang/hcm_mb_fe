export interface PersonalIdentity {
    personalIdentityId?: number;
    draftPersonalIdentityId?: number;
    employeeId?: number;
    idTypeCode?: string;
    idTypeName?: string;
    idNo?: string;
    idIssueDate?: Date;
    idIssuePlace?: string;
    isMain?: number;
    fromDate?: Date;
    toDate?: Date;
    startRecord?: number;
    pageSize?: number;
    resultSqlExecute?: string;
    orgId?: number;
    flagStatus?: number;
    status?: number;
    empTypeCode?: string;
    label?: string;
    orgName?: string;
    employeeCode?: string;
    fullName?: string;
}
