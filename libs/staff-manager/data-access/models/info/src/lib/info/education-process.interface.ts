export interface EducationProcess {
    educationProcessId?: number;
    employeeId?: number;
    courseName?: string;
    eduMethodTypeName?: string;
    eduMethodTypeCode?: string;
    courseContent?: string;
    note?: string;
    fromDate?: Date;
    toDate?: Date;
    startRecord?: number;
    pageSize?: number;
    fullName?: string;
    employeeCode?: string;
    flagStatus?: number;
    orgName?: string;
    status?: number;
    draftEducationProcessId?: number;
}
