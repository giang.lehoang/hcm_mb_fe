export interface CatBankInfo {
    bankId?: number;
    code?: string;
    name?: string;
}
