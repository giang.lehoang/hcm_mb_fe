import { PersonalIdentity } from './personal-identity.interface';

export interface PersonalIde {
    idDelete?: number[];
    data?: PersonalIdentity[];
}
