export class RewardHistory {
  rewardRecordId: number; // ID Bản ghi
  employeeId: number; // ID Nhân viên
  year: string; // Năm
  signedDate: string | null; // Ngày ký QĐ
  methodCode: string; // Mã Hình thức khen thưởng.
  methodName: string; // Hình thức khen thưởng.
  titleName: string; // Danh hiệu khen thưởng.
  titleCode: string; // Mã Danh hiệu khen thưởng.
  decisionLevelName: string; //Cấp ký QĐ
  decisionLevelCode: string; //Mã Cấp ký QĐ
  reason: string; // Lý do.
  note: string;
  documentNumber:string; // Số quyết định
  signerPosition:string;
  signer:string;
  fromDate: string | null;
}
