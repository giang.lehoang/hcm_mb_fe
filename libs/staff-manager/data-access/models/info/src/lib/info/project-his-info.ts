import {ListFileAttach} from "./list-file-attach";

export class ProjectHistory {
  projectMemberId: number; // ID Bản ghi
  employeeId: number; // ID Nhân viên
  fromDate: string | null; // Từ ngày
  toDate: string | null; // Đến ngày
  projectName: string; // Tên dự án tham gia - API danh mục tả ra thiếu
  jobName: string; //Job - API danh mục trả ra thiếu
  projectId: number;
  jobId: number;
  note: string;
  managerName: string;
  managerId: number;
  listFileAttach: Array<ListFileAttach>;
  attachFileList: Array<ListFileAttach>;
  docIdsDelete: number[];
}

