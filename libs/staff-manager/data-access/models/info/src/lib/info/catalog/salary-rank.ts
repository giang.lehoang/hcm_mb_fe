// Danh mục bậc lương
export interface SalaryRank {
  salaryRankId: number,
  salaryRankCode: string,
  salaryRankName: string,
  displaySeq: number
}
