import { BankAccount } from './bank-account.interface';

export interface ParamBankAccount {
    idDelete?: number[];
    data?: BankAccount[];
}
