import {ListFileAttach} from "./list-file-attach";

export class ContractHistory {
  contractProcessId: number; // ID Bản ghi
  draftContractProcessId: number; // ID Bản ghi draft
  employeeId: number; // ID Nhân viên
  fromDate: string | null; // Từ ngày
  toDate: string | null; // Đến ngày
  signedDate: string | null; // Ngày ký.
  contractTypeName: string; // Tên hợp đồng
  contractTypeId: number; //Loại HĐ
  contractNumber: string; //Số HĐ.
  note: string;
  classifyCode:string;
  classifyName:string; // Tên phân loại hợp đồng
  signerId:number;
  signerName:string;
  signerPosition:string;
  listFileAttach: Array<ListFileAttach>;
  attachFileList: Array<ListFileAttach>;
  docIdsDelete: number[];
  status: number;
  inputType: string;
  isCloneFile?: number;
}


export class ContractTypes {
  contractTypeId: number;
  code: string;
  name: string;
}
