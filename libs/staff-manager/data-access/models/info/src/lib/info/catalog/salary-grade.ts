// Danh mục dải lương
export interface SalaryGrade {
  salaryGradeId: number,
  salaryGradeCode: string,
  salaryGradeName: string,
  displaySeq: number
}
