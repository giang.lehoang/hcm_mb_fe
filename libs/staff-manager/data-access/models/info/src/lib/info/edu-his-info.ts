export interface EduHisInfo {
  educationProcessId?: number, // ID Bản ghi
  employeeId?: number, // ID Nhân viên
  courseName?: string, //Tên khóa học
  eduMethodTypeCode?: string, //Mã hình thức đào tạo
  eduMethodTypeName?: string, //Tên hình thức đào tạo
  courseContent?: string, //Nội dung đào tạo
  fromDate?: string, //Thời gian đào tạo từ ngày
  toDate?: string, //Thời gian đào tạo đến ngày
  result?: string, // Kết quả đào tạo
  refundAmount: number, // Số tiền bồi hoàn
  commitmentDate: string, // Thời gian cam kết
  note?: string
}
