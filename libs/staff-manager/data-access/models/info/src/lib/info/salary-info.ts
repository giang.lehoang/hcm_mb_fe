export class Salary {
  salaryProcessId: number; //ID bản ghi.
  employeeId: number; //ID nhân viên.
  salaryGradeName: string; //Dải lương.
  salaryGradeCode: string; // Mã dải lương - API đang trả thiếu
  salaryRankName: string; // Bậc lương
  salaryRankCode: string; // Mã Bậc lương - API đang trả ra thiếu
  salaryAmount: number; // Mức lương
  salaryPercent: string; // Phần trăm thưởng
  fromDate: string | null;
  toDate: string;
  note: string;
  performanceFactor: number;
  performancePercent: string;
}
