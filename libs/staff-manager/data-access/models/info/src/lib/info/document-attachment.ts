export class DocumentAttachment {
  employeeId: number;
  employeeProfileId: number;
  profileTypeCode: string;
  profileTypeName: string;
  note: string;
  isHardDocument: number;
  saveDocumentOrigin: string;
  createdBy: string;
  createDate: string;
  listFileAttach: Array<ListFileAttach>;
  attachFileList: Array<ListFileAttach>;
  docIdsDelete: number[];
}

export class ListFileAttach {
  docId: string;
  fileName: string;
  security?: string;
}
