import { ContactInfo } from './contact-info';

export class ContactInfoGroup {
    employeeId: number;
    data: ContactInfo;
}
