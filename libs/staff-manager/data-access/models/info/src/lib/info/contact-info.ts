export interface ContactInfo {
  employeeId?: number,
  placeOfBirth?: string, //Nơi sinh
  pernamentAddress?: string, //Hộ khẩu thường trú
  currentAddress?: string, //Chỗ ở hiện tại(Đã xử lý ghép chuỗi).
  originalAddress?: string, //Nguyên quán.
  pernamentProvinceCode?: string, //Hộ khẩu thường trú - Mã tỉnh
  pernamentProvinceName?: string, //Hộ khẩu thường trú - Tên tỉnh
  pernamentDistrictCode?: string, //Hộ khẩu thường trú - Mã Huyện
  pernamentDistrictName?: string, //Hộ khẩu thường trú - Tên Huyện
  pernamentWardCode?: string, //Hộ khẩu thường trú - Mã Xã
  pernamentWardName?: string, //Hộ khẩu thường trú - Tên Xã
  pernamentDetail?: string, //Hộ khẩu thường trú - Chi tiết
  currentProvinceCode?: string, //Chỗ ở hiện tại - Mã tỉnh
  currentProvinceName?: string, //Chỗ ở hiện tại - Tên tỉnh
  currentDistrictCode?: string, //Chỗ ở hiện tại - Mã Huyện
  currentDistrictName?: string, //Chỗ ở hiện tại - Tên Huyện
  currentWardCode?: string, //Chỗ ở hiện tại - Mã Xã
  currentWardName?: string, //Chỗ ở hiện tại - Tên Xã
  currentDetail?: string, //Chỗ ở hiện tại - Chi tiết
  workAddress?: string, //Dai chi lam viec
}
