// Vai trò tham gia dự án
export interface Job {
  jobId: number,
  jobCode: string,
  jobName: string,
  jobDescription: string,
  jobType: string
}
