import { IdentityInfo } from './identity-info';

export class IdentityInfoGroup {
    employeeId: number;
    data: IdentityInfo;
}
