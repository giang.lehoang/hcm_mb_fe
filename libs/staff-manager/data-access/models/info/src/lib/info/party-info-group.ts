import { PartyInfo } from './party-info';

export class PartyInfoGroup {
    employeeId: number;
    data: PartyInfo;
}
