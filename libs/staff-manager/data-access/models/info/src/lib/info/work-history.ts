import {ListFileAttach} from "./list-file-attach";

export class WorkHistory {
  workProcessId: number; //ID bản ghi.
  draftWorkProcessId: number;
  employeeId: number; //ID nhân viên.
  fromDate: string | null; //Từ ngày
  // toDate: string; // Đến ngày

  empTypeName: string; // Đối tượng
  documentTypeName: string; // Loại QĐ
  positionName: string; // Chức danh
  jobName: string;
  organizationName: string; // Đơn vị
  positionLevel: string; // Bậc chức danh
  positionLevelName: string; // Bậc chức danh
  positionGroupId: number; // Vị trí chức danh


  empTypeCode: number | string; // Mã Đối tượng
  documentTypeId: number; // ID Loại QĐ
  positionId: number; // ID Chức danh
  organizationId: number; //ID Đơn vị
  listManager: Manager[] = [];
  otherOrgId: number; //ID đơn vị kiêm nhiệm
  orgOtherName: string; //ID đơn vị kiêm nhiệm
  otherPositionId: number; //ID chức danh kiêm nhiệm
  otherPositionName: string; //ID chức danh kiêm nhiệm
  documentNo: string; // Số QĐ
  signedDate: string | null; // Ngày ký quyết định
  note: string; // Ghi chú
  isTrial: number; //Thử thách
  toDate: string | null; // Đến ngày
  planToDate1: string | null; // Ngày kết thúc 1
  planToDate2: string | null // Ngày kết thúc 2
  isKeepSenior:number; // Bảo lưu thâm niên
  listFileAttach: Array<ListFileAttach>

  hrDocumentType: string | undefined; // Loai quyet dinh

  listConcurrentProcessDTO: ConcurrentProcess[] = [];

  attachFileList: Array<ListFileAttach>;
  docIdsDelete: number[];
  managerId?: number;
  type?: string;
  status: number;
  inputType: string;
  parentName: string;
  isCloneFile?: number;
  pgrName?: string;
  isTrialStr?: string;
  isKeepSeniorStr?: string;
}


export class Manager {
  managerProcessId?: number;
  positionId: number;
  fromDate: string | null;
  managerId: number;
  managerName: string
}

export class ConcurrentProcess {
  concurrentProcessId?: number;
  employeeId?: number;
  workProcessId?: number;
  positionId?: number;
  organizationId?: number;
  empTypeCode?: string;
  managerId?: number;
  isTrial?: number;
  planToDate1?: string | null;
  planToDate2?: string | null;
}

export interface Document {
  documentTypeId: number;
  name: string;
  code: string;
  type: string;
}

export interface Position {
  posId: number,
  posCode: string;
  posName: string;
}
