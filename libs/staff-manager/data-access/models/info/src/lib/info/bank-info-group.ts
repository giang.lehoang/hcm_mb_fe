import { BankInfo } from './bank-info';

export class BankInfoGroup {
    employeeId: number;
    data: BankInfo;
}
