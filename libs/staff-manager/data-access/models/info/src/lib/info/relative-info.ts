export class RelativeInfo {
  familyRelationshipId: number;
  employeeId: number; // ID Nhân viên
  isInCompany: number; // Là nhân sự MB
  isInCompanyStr: string; // Là nhân sự MB
  referenceEmployeeId: string | number; // id nhân viên của thân nhân - API đang trả ra thiếu (string or number ???)
  relationTypeCode: string; //Tình trạng thân nhân
  relationTypeName: string;
  fullName: string;
  relationStatusCode: string;
  relationStatusName: string;
  policyTypeCode: string;
  policyTypeName: string;
  personalIdNumber: string;
  dateOfBirth: string | null;
  workOrganization: string;
  job: string;
  currentAddress: string;
  phoneNumber: string;
  note: string; // Ghi chú
  isHouseholdOwner: number; // Là chủ hộ
  isHouseholdOwnerStr: string; // Là chủ hộ
}
