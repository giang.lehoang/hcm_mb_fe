export interface PartyInfo {
  partyDate?: string, //Ngày vào đảng
  employeeId?: number,
  partyOfficialDate?: string, //Ngày vào đảng chính thức
  partyPlace?: string, //Nơi kết nạp đảng
  armyJoinDate?: string, //Ngày vào quân nhân
  armyLevelCode?: string, //Mã Cấp bậc quân hàm
  armyLevelName?: string, //Tên Cấp bậc quân hàm
}
