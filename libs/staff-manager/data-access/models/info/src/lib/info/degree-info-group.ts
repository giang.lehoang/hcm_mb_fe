import { DegreeInfo } from './degree-info';

export class DegreeInfoGroup {
    employeeId: number | null;
    data: DegreeInfo;
}
