export class DisciplinaryHistory {
  disciplineRecordId: number; // ID Bản ghi
  employeeId: number; // ID Nhân viên
  signedDate: string | null; // Ngày ký.
  disciplineMethodName: string; // Hình thức kỷ luật
  disciplineMethodCode: number; // Mã hình thức kỷ luật
  reason: string; // Lý do
  disciplineLevelName: string; // Cấp QĐ
  disciplineLevelCode: string; // Mã cấp QĐ
  amount: number;
  amountRemedied:number;
  note: string;
  fromDate: string | null;
  toDate: string | null;
  caseName: string;
  signerPosition: string;
  signer: string;
}
