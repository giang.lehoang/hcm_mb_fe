export class BaseResponse {
  code: number;
  message: string;
  timestamp: string;
  clientMessageId: string;
  transactionId: string;
  path: string;
  status: number;
  data: any;
}
