export interface BankInfo {
  bankAccountId?: number,
  employeeId?: number,
  accountTypeCode?: string, //Mã Loại tài khoản.
  accountTypeName?: string, //Loại tài khoản.
  accountNo?: string, //Số tài khoản
  bankId?: number, //ID ngân hàng.
  bankName?: string, //Tên ngân hàng.
  bankBranch?: string, //Chi nhánh
  isPaymentAccount?: number, //Là tài khoản chi lương
    index?: number,
}
