export interface SchoolInfo {
    schoolId?: number;
    code?: string;
    name?: string;
    isTextManual?: number;
}
