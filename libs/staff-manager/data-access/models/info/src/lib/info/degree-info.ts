export interface DegreeInfo<T = any> {
  educationDegreeId?: number,
  degreeName?: string, //Tên bằng cấp
  employeeId?: number,
  degreeTypeName?: string, //Loại bằng cấp
  degreeTypeCode?: string, //Mã loại bằng cấp
  schoolName?: string, //Trường đào tạo
  facultyName?: string, //Chuyên ngành
  majorLevelName?: string, //Trình độ
  eduRankName?: string //Xếp loại,
  issueYear?: string, // Năm cấp
  schoolId?: number, //Id trường đào tạo,
  facultyId?: number, //Id chuyên ngành
  majorLevelId?: number, //Id trình độ
  eduRankCode?: string, //Mã xếp loại
  isRelatedJob?: number, //Liên quan đến công việc
  isHighest?: number, //Là trình độ cao nhất
  note?: string, //Ghi chú
  isHighestStr?: string,
  data?: T
}
