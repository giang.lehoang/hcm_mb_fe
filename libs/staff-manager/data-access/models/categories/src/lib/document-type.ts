export interface DocumentType {
  documentTypeId?: number;
  code?: string;
  name?: string;
  type?: string;
  isRequiredEndDate?: number | string;
  isRequiredTrial?: number | string;
  isShowEndDate?: number | string;
  isShowTrial?: number | string;
  isShowKeepSenior?: number | string;
}
