export interface Faculties {
  facultyId?: number;
  code?: string;
  name?: string;
  isTextManual?: number | string;
  schoolId?: number;
  majorLevelId?: number;
}
