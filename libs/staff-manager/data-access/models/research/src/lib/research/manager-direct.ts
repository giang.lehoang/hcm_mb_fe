export interface ManagerModel {
  employeeCode: string;
  employeeId: number;
  fullName: string;
  jobName: string;
  orgName: string;
  posName: string;
  orgFullName:string;
  managerProcessId:number;
  dataTmp:any;
  status:number;
  classify:string;
  isMainPosition?:string;
  userName:string;
  positionId?:string;
  positionName?:string;
  objectId:string;
  isEnable?:number;
  fromDate?:string;
}
export interface TreeRoot<T> {
  key?:string;
  title?:string;
  children?: Array<T>;
  level?: number;
  expand?: boolean;
  parent?: T;

}

export interface EmployeeDetail extends TreeRoot<EmployeeDetail>{
  empCode: string;
  empId: number;
  employeeCode: string;
  employeeId: number;
  fullName: string;
  jobName: string;
  levelName: string;
  orgName: string;
  positionName?: string;
  positionId?: number | string;
  orgCurrentName?: string
}
export interface EmployeeInfo extends TreeRoot<EmployeeInfo>{
  empCode?: string;
  empId?: number;
  employeeCode?: string;
  employeeId: number;
  fullName?: string;
  jobName?: string;
  levelName?: string;
  orgName?: string;
  positionName?: string;
  positionId?: number | string;
  orgCurrentName?: string
  managerFullName?: string;
  managerOrgFullName?:string;
  managerJobName?:string;
  indirectManagerFullName?:string;
  indirectManagerOrgFullName?:string;
  indirectManagerJobName?:string;
  loadChildren?:boolean
  label?:string;
  orgFullName?:string;
}

export interface AssignRequest {
  managerProcessId: string | number | null,
  employeeId?: string | number,
  employeeFullName?: string | number,
  managerId: string | number,
  indirectManagerId: string | number,
  positionId?: string | number,
  fromDate?: string,
  toDate: string,
  hasIndirectManager: string | boolean
  jobName?:string;
  orgName?:string;
  posName?:string;
  isMainPosition?:string;
  managerD?: ManagerModel;
  managerI?: ManagerModel;
  objectId?: string;
}
export class ParamsModel {
  employeeId = '';
  orgId = '';
  jobId = '';
  isAuto = '';
  managerId = '';
  indirectManagerId = '';
  fromDate= '';
  isLasted = '0';
  isAssign = '0';
  orgName = '';
  page = 0;
  size = 15;
  isMainPosition?:string= '1';
}

export interface ProcessManagerDetail {
  employeeCode: string;
  employeeId: number;
  employeeLevel: string;
  fromDate: string;
  fullName: string;
  indirectManagerId: number;
  isAuto: string;
  jobId: number;
  jobName: string;
  managerId: number;
  managerProcessId: number;
  orgId: number;
  orgName: string;
  posId: number;
  posName: string
  toDate: string;
  orgFullName:string;
}
