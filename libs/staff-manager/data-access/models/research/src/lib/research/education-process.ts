export class EducationProcess {
  courseContent: string;
  courseName: string;
  eduMethodTypeName: string;
  employeeCode: string;
  employeeId: number;
  flagStatus: number;
  fromDate: Date;
  fullName: string;
  orgName: string;
  toDate: Date;
  result: string;
  refundAmount: number;
  commitmentDate: Date;
}
