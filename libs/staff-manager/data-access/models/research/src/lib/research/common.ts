export class MANAGER_DIRECT {
 public static readonly LIST_FORM_ASSIGN = [
    {value:'N',label:'Thủ công'},
    {value:'Y',label:'Tự động'}
  ]
  public static readonly DISPLAY = [
    {value:'0',label:'Tất cả'},
    {value:'1',label:'Mới nhất'},
  ]
  public static readonly LIST_CLASSIFY = [
    {value:'0',label:'Quá trình kiêm nhiệm'},
    {value:'1',label:'Quá trình công tác'},
  ]
};
