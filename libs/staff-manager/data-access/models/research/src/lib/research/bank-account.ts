export class BankAccount {
  accountNo: string;
  bankBranch: string;
  bankName: string;
  employeeCode: string;
  employeeId: number;
  flagStatus: number;
  fullName: string;
  isPaymentAccount: number;
  label: string;
  orgName: string;
}
