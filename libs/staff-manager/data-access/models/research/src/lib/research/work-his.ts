export class WorkHis {
  workProcessId: number;
  employeeCode: string;
  fullName: string;
  orgName: string;
  fromDate: string;
  toDate: string;
  empTypeName: string;
  organizationName: string;
  flagStatus: string;
  employeeId:number;
}
