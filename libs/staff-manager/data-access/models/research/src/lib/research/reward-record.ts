export class RewardRecord {
  decisionLevelName: string;
  documentNumber: string;
  employeeCode: string;
  employeeId: number;
  flagStatus: number;
  fromDate: Date;
  fullName: string;
  methodName: string;
  orgName: string;
  reason: string;
  signedDate: Date;
  titleName: string;
  year: number;
}
