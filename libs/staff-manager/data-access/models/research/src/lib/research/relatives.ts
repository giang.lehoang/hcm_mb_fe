export class Relatives {
  dateOfBirth: Date;
  employeeCode: string;
  employeeId: number;
  employeeName: string;
  flagStatus: number;
  fullName: string;
  orgName: string;
  relationStatusCode: string;
  relationTypeCode: string;
  workOrganization: string;
}
