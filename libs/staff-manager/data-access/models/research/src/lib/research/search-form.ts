export class SearchForm {
  employeeCode: string;
  fullName: string;
  flagStatus: number;
  organizationId: string;
}
