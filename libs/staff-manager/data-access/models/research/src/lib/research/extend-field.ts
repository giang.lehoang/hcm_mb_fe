export class ExtendField {
  code: string;
  inputType: string;
  inputLabel: string;
  inputPlaceholder: string;
  dataSourceType: string;
  dataSourceValue: string;
  dataSource?: DataSource[] = [];
  minValue: number;
  maxValue: number;
  loadingDataSource = false;
}
export class DataSource {
  value: 'number' | 'string';
  label: string;
}
