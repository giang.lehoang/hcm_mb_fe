export class Identity {
  employeeCode: string;
  employeeId: number;
  flagStatus: number
  fullName: string;
  idIssueDate: Date;
  idNo: string;
  isMain: number;
  label: string;
  orgName: string;
}
