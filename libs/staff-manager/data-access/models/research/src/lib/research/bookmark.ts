export class Bookmark {
  userBookmarkId: number;
  userName: string;
  type: string;
  name: string;
  options: Option[];
}
export class Option {
  code: string;
  values: string[];
  valueFrom: string | null;
  valueTo: string
}
