export class WorkHisBefore {
  workedOutSideId: number;
  fromDate: string;
  toDate: string;
  organizationName: string;
  positionName: string;
  referPerson: string;
  referPersonPosition: string;
  reasonLeave: string;
  rewardInfo: string;
  decplineInf: string;
}
