export class ProjectHis {
  projectProcessId: number;
  employeeCode: string;
  fullName: string;
  orgName: string;
  projectName: string;
  jobName: string;
  note: string;
  fromDate: string;
  toDate: string;
  flagStatus: number;
  employeeId: number;
}
