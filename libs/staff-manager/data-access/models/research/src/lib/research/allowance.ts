export class Allowance {
  allowanceTypeName: string;
  amountMoney: number;
  employeeCode: string;
  employeeId: number;
  flagStatus: number;
  fromDate: Date;
  fullName: string;
  orgName: string;
  toDate: Date;
}
