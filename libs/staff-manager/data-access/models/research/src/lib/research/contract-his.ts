export class ContractHis {
  contractProcessId: number;
  employeeCode: string;
  employeeId: number;
  fullName: string;
  orgName: string;
  fromDate: string;
  toDate: string;
  contractTypeName: string;
  contractNumber: string;
  signedDate: string;
  flagStatus: number;
  signerName: string;
  signerPosition: string;
}
