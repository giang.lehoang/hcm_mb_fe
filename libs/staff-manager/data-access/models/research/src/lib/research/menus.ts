export class Menus {
  icon: string;
  iconActive: string;
  id: number;
  code: string;
  name: string;
  parentId: number;
  showIcon: boolean;
  url: string;
  childrens?: Menus[];
}
