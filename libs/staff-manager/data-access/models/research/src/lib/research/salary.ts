export class Salary {
  employeeCode: string;
  employeeId: number;
  flagStatus: number;
  fromDate: Date;
  fullName: string;
  orgName: string;
  performanceFactor: number;
  performancePercent: number;
  salaryAmount: number;
  salaryGradeName: string;
  salaryProcessId: number;
  salaryRankName: string;
  toDate: Date;
}
