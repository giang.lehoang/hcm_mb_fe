export class PersonalInformation {
  employeeId: number;
  employeeCode: string;
  fullName: string;
  empTypeName: string;
  positionName: string;
  orgName: string;
  flagStatus: number;
  email: string;
  mobileNumber: string;
  dateOfBirth: string;
  genderName: string; // Gioi tinh
  ethnicName: string; // Dan toc
  religionName: string; // Ton giao
  personalId: string; // so can cuoc cong dan
  insuranceNo: string; // so BHXH
  taxNo: string; // MST
  isInsuranceMb: string; // Co tham gia bao hiem XH ko
  placeOfBirth: string; // Noi sinh
  maritalStatusName: string; // Tinh trang hon nhan
  pernamentAddress: string; // Ho khau thuong tru
  currentAddress: string; // dia chi hien tai
  originalAddress: string; // Que quan
  partyDate: string; // Ngay vao Dang
  partyOfficialDate: string; // Ngay vao dang chinh thuc
  yearOfBirth: number;
}
