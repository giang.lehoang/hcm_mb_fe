export class FamilyDeductions {
  employeeCode: string;
  employeeId: number;
  employeeName: string;
  flagStatus: number;
  fromDate: Date;
  fullName: string;
  orgName: string;
  relationTypeName: string;
  toDate: Date;
}
