import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { saveAs } from 'file-saver';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import {ErrorImport} from "@hcm-mfe/staff-manager/data-access/models/research";
import {ImportFormService} from "@hcm-mfe/staff-manager/data-access/services";
import {beforeUploadFile, getTypeExport} from "@hcm-mfe/shared/common/utils";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {NzUploadFile} from "ng-zorro-antd/upload";

@Component({
  selector: 'app-import-employee',
  templateUrl: './import-employee.component.html',
  styleUrls: ['./import-employee.component.scss']
})
export class ImportEmployeeComponent implements OnInit {

  @Input() showContent = false;
  @Input() closeModalWhenClick = true;
  @Input() urlApiImport = '';
  @Input() urlApiDownloadTemp = '';
  @Output() onCloseModal: EventEmitter<boolean> = new EventEmitter<boolean>();

  isModalError = false;
  errorList: ErrorImport[] = [];
  fileList: NzUploadFile[] = [];
  fileName?: string;

  isExistFileImport = false;
  isLoading = false;
  nzWidth: number;
  nzWidthError: number;

  constructor(private importFormService: ImportFormService,
              private toastrService: ToastrService, private translate: TranslateService, private cdRed: ChangeDetectorRef) {
    this.nzWidth = window.innerWidth > 767 ? window.innerWidth / 3 : window.innerWidth / 2.5;
    this.nzWidthError = window.innerWidth > 767 ? window.innerWidth / 2 : window.innerWidth / 1.5;
  }

  ngOnInit(): void {
  }

  doClose() {
    this.showContent = false;
    this.fileList = [];
    this.onCloseModal.emit(false);
  }

  doDownloadTemplate() {
    this.importFormService.downloadTemplate(this.urlApiDownloadTemp).subscribe(res => {
      const reportFile = new Blob([res.body], { type: getTypeExport('xlsx') });
      saveAs(reportFile, 'template.xlsx');
    });
  }

  doCloseModal() {
    this.isModalError = false;
  }

  doDownloadFile() {
    this.importFormService.doDownloadFileByName(this.fileName).subscribe(res => {
      const reportFile = new Blob([res.body], { type: getTypeExport('xlsx') });
      saveAs(reportFile, 'file_error_information.xlsx');
    });
  }

  beforeUpload = (file: NzUploadFile): boolean => {
    this.fileList = [];
    this.fileList =  beforeUploadFile(file, this.fileList, 3000000, this.toastrService, this.translate, 'staffManager.notification.upload.limitSize')
    this.isExistFileImport = true;
    return false;
  };

  doImportFile() {
    this.isLoading = true;
    const formData = new FormData();
    this.fileList.forEach((file: NzUploadFile) => {
      formData.append('file', file as any);
    });
    this.importFormService.doImport(this.urlApiImport, formData).subscribe(res => {
      if (res != undefined && res.code != HTTP_STATUS_CODE.OK) {
        this.errorList = res.data.errorList;
        if (this.errorList != undefined && this.errorList.length > 0) {
          this.isModalError = true;
          this?.cdRed?.markForCheck();
          this.fileName = res.data.errorFile;
          this.toastrService.error(this.translate.instant('staffManager.notification.upload.error'));
        } else {
          this.isModalError = false;
          this.errorList = [];
          this.fileName = undefined;
          this.toastrService.error(res.message);
        }
        this.isLoading = false;
      } else {
        this.isModalError = false;
        this.errorList = [];
        this.fileName = undefined;
        this.isLoading = false;
        this.toastrService.success(this.translate.instant('staffManager.notification.upload.success'));
      }
    });
  }

  doRemoveFile = (): boolean => {
    this.fileList = [];
    this.isExistFileImport = false;
    return true;
  };
}
