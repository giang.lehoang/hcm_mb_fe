import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ImportEmployeeComponent} from "./import-employee/import-employee.component";
import {NzModalModule} from "ng-zorro-antd/modal";
import {NzFormModule} from "ng-zorro-antd/form";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzUploadModule} from "ng-zorro-antd/upload";
import {TranslateModule} from "@ngx-translate/core";
import {NzTableModule} from "ng-zorro-antd/table";
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';

@NgModule({
  imports: [CommonModule, NzModalModule, NzFormModule, SharedUiMbButtonModule, NzUploadModule, TranslateModule, NzTableModule, SharedUiLoadingModule],
  declarations: [ImportEmployeeComponent],
  exports: [ImportEmployeeComponent],
})
export class StaffManagerUiImportEmployeeResearchModule {}
