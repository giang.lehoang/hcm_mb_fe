import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {Category} from "@hcm-mfe/shared/data-access/models";
import {HttpParams} from "@angular/common/http";
import {StringUtils} from "@hcm-mfe/shared/common/utils";
import {Constant} from "@hcm-mfe/staff-manager/data-access/common";

@Component({
  selector: 'search-form-categories',
  templateUrl: './search-form-categories.component.html',
  styleUrls: ['./search-form-categories.component.scss']
})
export class SearchFormCategoriesComponent implements OnInit {
  @Input() showType = false;

  form!: FormGroup;
  listType: Category[] = [];

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.initForm();
    this.initDataSelect();
  }

  initForm() {
    this.form = this.fb.group({
      code: null,
      name: null,
      listType: null
    })
  }

  initDataSelect() {
    this.listType = Constant.LIST_DOCUMENT_TYPE;
  }


  get formControl() {
    return this.form.controls;
  }


  parseOptions() {
    let params = new HttpParams();

    if (!StringUtils.isNullOrEmpty(this.formControl['code'].value)) {
      params = params.set('code', this.formControl['code'].value.trim());
    }

    if (!StringUtils.isNullOrEmpty(this.formControl['name'].value)) {
      params = params.set('name', this.formControl['name'].value.trim());
    }

    if (this.formControl['listType'].value !== null && this.showType)
      params = params.set('listType', this.formControl['listType'].value.join(','));

    return params;
  }
}
