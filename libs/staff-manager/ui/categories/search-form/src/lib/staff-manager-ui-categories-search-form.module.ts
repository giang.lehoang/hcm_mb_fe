import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ReactiveFormsModule} from "@angular/forms";
import {SharedUiOrgDataPickerModule} from "@hcm-mfe/shared/ui/org-data-picker";
import {SharedUiMbSelectCheckAbleModule} from "@hcm-mfe/shared/ui/mb-select-check-able";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SearchFormCategoriesComponent} from "./search-form-categories/search-form-categories.component";
import {TranslateModule} from "@ngx-translate/core";
import {CoreModule} from "@hcm-mfe/shared/ui/core";
import {SharedUiLoadingPageModule} from "@hcm-mfe/shared/ui/loading-page";

@NgModule({
    imports: [CommonModule, ReactiveFormsModule, SharedUiOrgDataPickerModule, SharedUiMbSelectCheckAbleModule, SharedUiMbInputTextModule, SharedUiMbDatePickerModule, SharedUiMbSelectModule, TranslateModule, CoreModule, SharedUiLoadingPageModule],
  declarations: [SearchFormCategoriesComponent],
  exports: [SearchFormCategoriesComponent]
})

export class StaffManagerUiCategoriesSearchFormModule {}
