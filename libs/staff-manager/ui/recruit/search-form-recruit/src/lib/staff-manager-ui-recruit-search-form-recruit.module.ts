import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SearchFormRecruitComponent} from "./search-form-recruit/search-form-recruit.component";
import {ReactiveFormsModule} from "@angular/forms";
import {SharedUiOrgDataPickerModule} from "@hcm-mfe/shared/ui/org-data-picker";
import {CoreModule} from "@hcm-mfe/shared/ui/core";
import {SharedUiMbSelectCheckAbleModule} from "@hcm-mfe/shared/ui/mb-select-check-able";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";

@NgModule({
    imports: [CommonModule, ReactiveFormsModule, SharedUiOrgDataPickerModule, CoreModule, SharedUiMbSelectCheckAbleModule, SharedUiMbInputTextModule, SharedUiMbDatePickerModule, SharedUiMbSelectModule],
  declarations: [SearchFormRecruitComponent],
  exports: [SearchFormRecruitComponent]
})
export class StaffManagerUiRecruitSearchFormRecruitModule {}
