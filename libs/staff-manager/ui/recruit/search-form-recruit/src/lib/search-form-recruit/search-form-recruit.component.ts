import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {HttpParams} from '@angular/common/http';
import {Subscription} from 'rxjs';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {CatalogModel} from "@hcm-mfe/shared/data-access/models";
import {SearchFormService} from "@hcm-mfe/staff-manager/data-access/services";
import {Constant, UrlConstant} from '@hcm-mfe/staff-manager/data-access/common';
import {HTTP_STATUS_CODE, Scopes} from "@hcm-mfe/shared/common/constants";
import {StringUtils} from "@hcm-mfe/shared/common/utils";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import * as moment from "moment";

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form-recruit.component.html',
  styleUrls: ['./search-form-recruit.component.scss']
})

export class SearchFormRecruitComponent implements OnInit {
  @Input() defaultListStatus = [Constant.STATUS_EMP[0].value];
  @Input() defaultListEmpType?: string[];
  @Input() showApprovalStatus = false;
  @Input() isApproval = false;
  @Input() functionCode?: string;
  @Input() scope = Scopes.VIEW;

  form!: FormGroup;
  listEmpTypeCode: CatalogModel[] = [];
  listPosition: CatalogModel[] = [];
  subscriptions: Subscription[] = [];
  statusEmployeeList: CatalogModel[] = [];
  listApprovalStatus: CatalogModel[] = [];
  listOrgRegions: CatalogModel[] = [];
  listLine: CatalogModel[] = [];
  listRecruitType: CatalogModel[] = [];

  constructor(private fb: FormBuilder,
              private toastrService: ToastrService,
              private translateService: TranslateService,
              private searchFormService: SearchFormService) { }

  ngOnInit(): void {
    this.initFormGroup();
    this.initDataSelect();
  }

  initFormGroup() {
    this.form = this.fb.group({
      organizationId: null,
      listEmpTypeCode: [this.defaultListEmpType],
      listPositionId: null,
      keyword: null,
      listStatus: [this.defaultListStatus],
      fromDate: [null],
      toDate: [null],
      type: [Constant.RECRUIT_TYPE[0].value],
      listApprovalStatus: [null],
      listOrgRegions: [null],
      listLineId: [null]
    });
  }

  getListEmpTypeCode() {
    const params = new HttpParams().set('typeCode', Constant.TYPE_CODE.DOI_TUONG_CV);
    this.subscriptions.push(
      this.searchFormService.getListValue(UrlConstant.CATEGORY.LOOKUP_VALUE, params).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listEmpTypeCode = res.data;
        }
      }, error => {
        this.toastrService.error(error.message);
        this.listEmpTypeCode = [];
      })
    );
  }

  initDataSelect() {
    this.statusEmployeeList = Constant.STATUS_EMP.map(item => {
      item.label = this.translateService.instant(item.label);
      return item;
    });

    this.listApprovalStatus = Constant.APPROVAL_STATUS.reduce((accumulator: NzSafeAny[], currentValue, index) => {
      currentValue.label = this.translateService.instant(currentValue.label);
      if (this.isApproval) {
        if (index > 1 && index < 5) {
          accumulator.push(currentValue);
        }
      } else {
        accumulator.push(currentValue);
      }
      return accumulator;
    }, [])

    if (this.isApproval) {
      this.form.controls['listApprovalStatus'].setValue([Constant.APPROVAL_STATUS[2].value]);
    }

    this.listRecruitType = Constant.RECRUIT_TYPE.map(item => {
      item.label = this.translateService.instant(item.label);
      return item;
    })

    this.getListEmpTypeCode();
    this.getListPosition();
    this.getListOrgRegions();
    this.getListLine();
  }


    getListPosition() {
    const params = new HttpParams().set('jobType', Constant.JOB_TYPE.ORG);
    this.subscriptions.push(
      this.searchFormService.getListValue(UrlConstant.CATEGORY.MP_JOB, params).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listPosition = res.data;
        } else {
          this.toastrService.error(res.message);
        }
      }, error => {
        this.toastrService.error(error.message);
        this.listPosition = [];
      })
    );
  }

  getListOrgRegions() {
    const params = new HttpParams().set('typeCode', Constant.TYPE_CODE.KHU_VUC);
    this.subscriptions.push(
      this.searchFormService.getListValue(UrlConstant.CATEGORY.LOOKUP_VALUE, params).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listOrgRegions = res.data;
        }
      }, error => {
        this.toastrService.error(error.message);
        this.listEmpTypeCode = [];
      })
    );
  }

  getListLine() {
    this.subscriptions.push(
      this.searchFormService.getListLine(Constant.TYPE_CODE.LINE).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listLine = res.data.map((item: NzSafeAny) => {
            item.value = item.pgrId?.toString();
            item.label = item.pgrName;
            return item;
          });
        }
      }, error => {
        this.toastrService.error(error.message);
        this.listLine = [];
      })
    );
  }


  setFormValue(event: NzSafeAny, formName: string) {
    if ((formName !== 'listStatus' && formName !== 'listEmpTypeCode' && formName !== 'listApprovalStatus') ||
      event.listOfSelected?.length > 0) {
      this.form.controls[formName].setValue(event.listOfSelected);
    }
  }

  get formControl() {
    return this.form.controls;
  }

  parseOptions() {
    let params = new HttpParams();

    if (!StringUtils.isNullOrEmpty(this.formControl['keyword'].value))
      params = params.set('keyword', this.formControl['keyword'].value.trim());

    if (this.formControl['organizationId'].value)
      params = params.set('organizationId', this.formControl['organizationId'].value['orgId']);

    if (this.formControl['listEmpTypeCode'].value){
      params = params.set('listEmpTypeCode', this.formControl['listEmpTypeCode'].value.join(','));
    }
    if (this.formControl['listPositionId'].value){
      params = params.set('listPositionId', this.formControl['listPositionId'].value.join(','));
    }

    if (this.formControl['listStatus'].value !== null && !this.showApprovalStatus)
      params = params.set('listStatus', this.formControl['listStatus'].value.join(','));

    if (this.formControl['listOrgRegions'].value !== null)
      params = params.set('listOrgRegions', this.formControl['listOrgRegions'].value.join(','));

    if (this.formControl['listLineId'].value !== null)
      params = params.set('listLineId', this.formControl['listLineId'].value.join(','));

    if (this.formControl['listApprovalStatus'].value !== null && this.showApprovalStatus)
      params = params.set('listApprovalStatus', this.formControl['listApprovalStatus'].value.join(','));

    if (this.formControl['type'].value !== null && this.showApprovalStatus)
      params = params.set('type', this.formControl['type'].value);

    if (this.formControl['fromDate'].value !== null)
      params = params.set('fromDate', moment(this.formControl['fromDate'].value).format('DD/MM/YYYY'));

    if (this.formControl['toDate'].value !== null)
      params = params.set('toDate', moment(this.formControl['toDate'].value).format('DD/MM/YYYY'));

    return params;
  }

  ngOnDestroy(): void {
    this.subscriptions?.forEach(sub => sub?.unsubscribe());
  }
}
