import {Component, EventEmitter, Input, Output} from '@angular/core';
import {saveAs} from 'file-saver';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {NzUploadFile} from 'ng-zorro-antd/upload';
import {NzSafeAny} from 'ng-zorro-antd/core/types';
import {Subscription} from 'rxjs';
import {ErrorImport} from "@hcm-mfe/staff-manager/data-access/models/draft";
import {ImportFormService, ShareDataService} from "@hcm-mfe/staff-manager/data-access/services";
import {beforeUploadFile, getTypeExport} from "@hcm-mfe/shared/common/utils";
import {HTTP_STATUS_CODE} from '@hcm-mfe/shared/common/constants';

@Component({
    selector: 'app-import-file',
    templateUrl: './import-file.component.html',
    styleUrls: ['./import-file.component.scss']
})
export class ImportFileComponent {

  @Input() showContent = false;
  @Input() showAttachFile = false;
  @Input() requiredAttachFile = false;
  @Input() closeModalWhenClick = true;
  @Input() urlApiImport = '';
  @Input() urlApiDownloadTemp = '';
  @Output() onCloseModal: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() loadPage: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() sendListConfirm = new EventEmitter<NzSafeAny[]>();

  isModalError = false;
  errorList: ErrorImport[] = [];
  fileList: NzUploadFile[] = [];
  fileName?: string;
  fileImportName = '';
  fileImportSize?: string;
  subscriptions: Subscription[] = [];

  isExistFileImport = false;
  nzWidth: number;
  nzWidthError: number;

  isHiddenDownloadErrorFile = true;
  fileListAttach: NzUploadFile[] = [];

  constructor(private importFormService: ImportFormService,
              private toastrService: ToastrService,
              private translate: TranslateService,
              private shareDataService: ShareDataService
  ) {
    this.nzWidth = window.innerWidth > 767 ? window.innerWidth / 3 : window.innerWidth / 2.5;
    this.nzWidthError = window.innerWidth > 767 ? window.innerWidth / 2 : window.innerWidth / 1.5;
  }

  doClose(refresh: boolean = false) {
    this.showContent = false;
    this.onCloseModal.emit(false);
    this.fileImportName = '';
    this.isExistFileImport = false;
    this.onCloseModal.emit(refresh);
    this.fileListAttach = [];
  }

  doDownloadTemplate() {
    this.loadPage.emit(true);
    this.subscriptions.push(
      this.importFormService.downloadTemplate(this.urlApiDownloadTemp).subscribe(res => {
        const arr = res.headers.get("Content-Disposition")?.split(';');
        const fileName:string = arr[arr.length - 1].replace('filename=','').trim();
        const reportFile = new Blob([res.body], { type: getTypeExport('xlsx') });
        saveAs(reportFile, fileName);
        this.loadPage.emit(false);
      }, error => {
        this.toastrService.error(error.message);
        this.loadPage.emit(false);
      })
    );
  }

  doCloseModal() {
    this.isModalError = false;
  }

  doDownloadFile() {
    this.subscriptions.push(
      this.importFormService.doDownloadFileByName(this.fileName).subscribe(res => {
        const reportFile = new Blob([res.body], { type: getTypeExport('xlsx') });
        saveAs(reportFile, 'file_error_information.xlsx');
        this.isHiddenDownloadErrorFile = false;
      }, error => {
        this.toastrService.error(error.message);
      })
    );
  }

  beforeUpload = (file: NzSafeAny): boolean => {
    this.fileList = [];
    this.fileList =  beforeUploadFile(file, this.fileList, 3000000, this.toastrService, this.translate, 'staffManager.notification.upload.errorFileImport', ".XLS", ".XLSX")
    this.isExistFileImport = true;
    this.fileImportName = file.name;
    this.fileImportSize = (file.size/1000000).toFixed(2);
    return false;
  };

  beforeUploadAttach = (file: NzUploadFile): boolean => {
    this.fileListAttach =  beforeUploadFile(file, this.fileListAttach, 15000000, this.toastrService, this.translate, 'common.notification.errorAttachFile', ".ZIP", ".PDF")
    return false;
  }

  doImportFile() {
    this.isHiddenDownloadErrorFile = true;
    if (this.fileListAttach.length === 0 && this.requiredAttachFile) {
      this.toastrService.error(this.translate.instant('staffManager.notification.upload.errorRequiredFile'));
      return;
    }
    const formData = new FormData();
    this.fileList.forEach((file: NzUploadFile) => {
      formData.append('file', file as NzSafeAny);
    });

    this.fileListAttach.forEach((file: NzUploadFile) => {
      formData.append('fileExtends', file as any);
    });

    this.loadPage.emit(true);
    this.subscriptions.push(
      this.importFormService.doImport(this.urlApiImport, formData).subscribe(res => {
        if (res != undefined && res.code != HTTP_STATUS_CODE.OK) {
          this.loadPage.emit(false);
          this.errorList = res.data?.errorList;
          if (this.errorList != undefined && this.errorList.length > 0) {
            this.isModalError = true;
            this.fileName = res.data.errorFile;
            this.toastrService.error(this.translate.instant('staffManager.notification.upload.error'));
          } else {
            this.isModalError = false;
            this.errorList = [];
            this.fileName = undefined;
            this.toastrService.error(res.message);
          }
        } else {
          this.isModalError = false;
          this.errorList = [];
          this.fileName = undefined;
          this.shareDataService.emitChange(true);
          this.doClose(true);
          this.toastrService.success(this.translate.instant('staffManager.notification.upload.success'));
          this.sendListConfirm.emit(res.data);
        }
      }, error => {
        this.loadPage.emit(false);
        this.toastrService.error(error.message);
      })
    );
  }

  doRemoveFile = (): boolean => {
    this.fileList = [];
    this.isExistFileImport = false;
    return true;
  };

  ngOnDestroy(): void {
    this.subscriptions?.forEach(sub => sub?.unsubscribe());
  }
}
