import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ImportFileComponent} from "./import-file/import-file.component";
import {CoreModule} from "@hcm-mfe/shared/ui/core";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";

@NgModule({
  imports: [CommonModule, CoreModule, SharedUiMbButtonModule],
  declarations: [ImportFileComponent],
  exports: [ImportFileComponent]
})
export class StaffManagerUiRecruitImportFileModule {}
