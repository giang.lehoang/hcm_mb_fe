import { Component, Input, OnInit } from '@angular/core';
import {ImportFormService} from "@hcm-mfe/staff-manager/data-access/services";

@Component({
  selector: 'app-import-employee',
  templateUrl: './import-employee.component.html',
  styleUrls: ['./import-employee.component.scss']
})
export class ImportEmployeeComponent implements OnInit {

  @Input() showContent = false;
  @Input() closeModalWhenClick = true;
  @Input() urlApiImport = '';
  @Input() urlApiDownloadTemp = '';
  nzWidth: number;


  constructor(private importFormService: ImportFormService) {
    this.nzWidth = window.innerWidth > 767 ? window.innerWidth / 3.5 : window.innerWidth / 3;
  }

  ngOnInit(): void {
  }

  doClose() {
    this.showContent = false;
  }

  doDownloadTemplate() {
    this.importFormService.downloadTemplate(this.urlApiDownloadTemp).subscribe(() => {
    });
  }
}
