import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ImportEmployeeComponent} from "./import-employee/import-employee.component";
import {NzModalModule} from "ng-zorro-antd/modal";
import {TranslateModule} from "@ngx-translate/core";
import {NzFormModule} from "ng-zorro-antd/form";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiMbImportExcelModule} from "@hcm-mfe/shared/ui/mb-import-excel";

@NgModule({
  imports: [CommonModule, NzModalModule, TranslateModule, NzFormModule, SharedUiMbButtonModule, SharedUiMbImportExcelModule],
  declarations: [ImportEmployeeComponent],
  exports: [ImportEmployeeComponent],
})
export class StaffManagerUiImportEmployeeModule {}
