import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SearchFormComponent} from "./search-form/search-form.component";
import {NzFormModule} from "ng-zorro-antd/form";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbSelectCheckAbleModule} from "@hcm-mfe/shared/ui/mb-select-check-able";
import {SharedUiOrgDataPickerModule} from "@hcm-mfe/shared/ui/org-data-picker";
import {SharedUiMbNumbericRangeInputModule} from "@hcm-mfe/shared/ui/mb-numberic-range-input";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiMbRangDatePickerModule} from "@hcm-mfe/shared/ui/mb-rang-date-picker";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzPopoverModule} from "ng-zorro-antd/popover";
import {SharedDirectivesSwitchCaseModule} from "@hcm-mfe/shared/directives/switch-case";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
    imports: [CommonModule, NzFormModule, ReactiveFormsModule, SharedUiMbInputTextModule, TranslateModule,
        SharedUiMbSelectCheckAbleModule, SharedUiOrgDataPickerModule, SharedUiMbNumbericRangeInputModule, SharedUiMbSelectModule,
        SharedUiMbRangDatePickerModule, SharedUiMbButtonModule, NzPopoverModule, FormsModule, SharedDirectivesSwitchCaseModule, SharedUiLoadingModule],
  declarations: [SearchFormComponent],
  exports: [SearchFormComponent],
})
export class StaffManagerUiSearchFormResearchModule {}
