import {ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import * as moment from 'moment';
import * as _ from 'lodash';
import {HttpParams} from '@angular/common/http';
import {Scopes} from "@hcm-mfe/shared/common/enums";
import {Bookmark, ExtendField, Option} from "@hcm-mfe/staff-manager/data-access/models/research";
import {BaseResponse, CatalogModel, UserLogin} from "@hcm-mfe/shared/data-access/models";
import {BookmarkFormService, SearchFormService} from "@hcm-mfe/staff-manager/data-access/services";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {ValidateService} from "@hcm-mfe/shared/core";
import {HTTP_STATUS_CODE, STORAGE_NAME} from "@hcm-mfe/shared/common/constants";
import {Constant, UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {SelectCheckAbleModal} from "@hcm-mfe/system/data-access/models";
import {StringUtils} from "@hcm-mfe/shared/common/utils";
import {StorageService} from '@hcm-mfe/shared/common/store';
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {forkJoin} from "rxjs";

@Component({
  selector: 'search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {
  @Input() moduleName = '';
  @Input() scope: string = Scopes.VIEW;
  @Input() functionCode = '';
  @Output() submitEvent: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() isDataSelectReadyEvent: EventEmitter<boolean> = new EventEmitter<boolean>();
  isSubmitted = false;
  isLoadingPage = false;
  form: FormGroup;
  formConfig: ExtendField[] = [];
  listExtendField: ExtendField[] = [];
  listEmpTypeCode: CatalogModel[] = [];
  listPosition: CatalogModel[] = [];
  statusList: CatalogModel[] = [
    {
      value: '1',
      label: this.translate.instant('staffManager.label.workIn')
    },
    {
      value: '3',
      label: this.translate.instant('staffManager.label.workOut')
    },
    {
      value: '2',
      label: this.translate.instant('staffManager.label.contractPending')
    }
  ];
  bookmarks: Bookmark[] = [];
  userLogin: UserLogin = new UserLogin();
  saveBookmarkName?: string = undefined;
  saveBookmarkVisible = false;
  choosedBookmarkId?: number;
  chooseBookmarkVisible = false;

  constructor(
    private bookmarkFormService: BookmarkFormService,
    private baseService: BaseService,
    private router: Router,
    private toastService: ToastrService,
    private translate: TranslateService,
    private searchFormService: SearchFormService,
    public validateService: ValidateService,
    public fb: FormBuilder,
    private cdRef: ChangeDetectorRef
  ) {
    this.form = fb.group({
      employeeCode: [null],
      fullName: [null],
      flagStatus: [null],
      listStatus: [['1']],
      organizationId: [null],
      listEmpTypeCode: [null],
      listPositionId: [null],
      extendField: null
    });
  }

  ngOnInit(): void {
    this.userLogin = StorageService.get(STORAGE_NAME.USER_LOGIN);
    this.initDataSelect();
  }

  initDataSelect() {
    const params1 = new HttpParams().set('typeCode', Constant.TYPE_CODE.DOI_TUONG_CV);
    const params2 = new HttpParams().set('jobType', Constant.JOB_TYPE.ORG);
    forkJoin([
      this.searchFormService.getListValue(UrlConstant.CATEGORY.LOOKUP_VALUE, params1),
      this.searchFormService.getListValue(UrlConstant.CATEGORY.MP_JOB, params2),
      this.bookmarkFormService.getBookmark(this.moduleName),
      this.bookmarkFormService.getExtendField(this.moduleName)
    ]).subscribe((res) => {
      this.listEmpTypeCode = res[0].data;
      this.listPosition = res[1].data;
      this.bookmarks = res[2].data.filter((item: NzSafeAny) => item.code !== 'flagStatus');
      this.listExtendField = res[3].data;
      this.isDataSelectReadyEvent.emit(true);
    });
  }

  getBookmark() {
    this.bookmarkFormService.getBookmark(this.moduleName).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.bookmarks = res.data.filter((item: NzSafeAny) => item.code !== 'flagStatus');
      }
    });
  }

  doAddOrRemoveFieldExtend(items: SelectCheckAbleModal, addFormControl: boolean = true, setFormValue?: boolean, itemField?: ExtendField, option?: Option) {
    if (addFormControl) {
      this.formConfig = [];
    }
    items.listOfSelected?.forEach(item => {
      const itemField = this.listExtendField[this.listExtendField.findIndex(x => x.code === item)];
      if (itemField) {
        if (addFormControl) {
          this.form.addControl(item, this.fb.control(null));
        }
        if (itemField.dataSourceType === 'STATIC') {
          itemField.dataSource = JSON.parse(itemField.dataSourceValue);
        }
        if (itemField.dataSourceType === 'REST_URL') {
          itemField.loadingDataSource = true;
          this.baseService.get(itemField.dataSourceValue).subscribe(data => {
            if (data.code === HTTP_STATUS_CODE.OK) {
              if (data.data.length > 0) {
                const first = data.data[0];
                if (Object.prototype.hasOwnProperty.call(first, 'majorLevelId')) {
                  itemField.dataSource = data.data.map((d: NzSafeAny) => new CatalogModel(d.name, d.code));
                } else if (Object.prototype.hasOwnProperty.call(first, 'posId')) {
                  itemField.dataSource = data.data.map((d: NzSafeAny) => new CatalogModel(d.posName, d.posCode));
                } else if (Object.prototype.hasOwnProperty.call(first, 'schoolId')) {
                  itemField.dataSource = data.data.map((d: NzSafeAny) => new CatalogModel(d.name, d.code));
                } else if (Object.prototype.hasOwnProperty.call(first, 'bankId')) {
                  itemField.dataSource = data.data.map((d: NzSafeAny) => new CatalogModel(d.name, d.code));
                } else if (Object.prototype.hasOwnProperty.call(first, 'salaryRankId')) {
                  itemField.dataSource = data.data.map((d: NzSafeAny) => new CatalogModel(d.salaryRankName, d.salaryRankCode));
                } else if (Object.prototype.hasOwnProperty.call(first, 'salaryGradeId')) {
                  itemField.dataSource = data.data.map((d: NzSafeAny) => new CatalogModel(d.salaryGradeName, d.salaryGradeCode));
                } else {
                  itemField.dataSource = data.data
                }
                if (setFormValue) {
                  this.setFormValueChooseBookmark(itemField, option);
                  this.cdRef.markForCheck();
                }
              }
              itemField.loadingDataSource = false;
            }
          }, () => itemField.loadingDataSource = false);
        }
        if (setFormValue) {
          this.setFormValueChooseBookmark(itemField, option);
          this.cdRef.markForCheck();
        }
        this.formConfig.push(itemField);
      }
    });
  }

  get formControl() {
    return this.form.controls;
  }

  saveBookmark(id?: number) {
    const bm: Bookmark | NzSafeAny = new Bookmark();
    bm.userBookmarkId = id;
    bm.userName = this.userLogin.employeeCode;
    bm.type = this.moduleName;
    bm.name = id ? (this.saveBookmarkName ?? this.bookmarks.find(b => b.userBookmarkId === id)?.name) : this.saveBookmarkName;
    bm.options = this.getOptions();
    this.bookmarkFormService.saveBookmark(bm).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.toastService.success(this.translate.instant('common.notification.updateSuccess'));
        this.getBookmark();
        this.saveBookmarkName = undefined;
        this.saveBookmarkVisible = false;
        this.cdRef.markForCheck();
      } else this.toastService.error(this.translate.instant('common.notification.updateError') + ':' + res.message);
    });
  }

  chooseBookmark() {
    this.formConfig = [];
    const fields: NzSafeAny[] = [];
    const bookmark = this.bookmarks.find(bm => bm.userBookmarkId === this.choosedBookmarkId);
    bookmark?.options.forEach((option) => {
      if (option.code === 'employeeCode') {
        this.form.patchValue({employeeCode: option.values[0]});
        return;
      } else if (option.code === 'fullName') {
        this.form.patchValue({fullName: option.values[0]});
        return;
      } else if (option.code === 'listStatus') {
        this.form.patchValue({listStatus: option.values});
        return;
      } else if (option.code === 'listEmpTypeCode') {
        this.form.patchValue({listEmpTypeCode: option.values});
        return;
      } else if (option.code === 'listPositionId') {
        this.form.patchValue({listPositionId: option.values});
        return;
      } else if (option.code === 'organizationId') {
        this.form.patchValue({
          organizationId: option.values[0] ? {
            orgId: option.values[0].split('|')[0],
            orgName: option.values[0].split('|')[1],
            parentName: option.values[0].split('|')[2]
          } : null
        });
        return;
      }
      const itemField = this.listExtendField[this.listExtendField.findIndex(x => x.code === option.code)];
      if (itemField) {
        fields.push(itemField.code);
        this.form.addControl(itemField.code, this.fb.control(null));
        this.doAddOrRemoveFieldExtend({listOfSelected: [itemField.code]}, false, true, itemField, option);
      }
    });


    this.form.patchValue({extendField: fields});
    this.chooseBookmarkVisible = false;
  }

  setFormValueChooseBookmark(itemField: ExtendField, option: Option | NzSafeAny) {
    switch (itemField.inputType) {
      case 'date':
        this.formControl[itemField.code].setValue([
            option.valueFrom ? moment(option.valueFrom, 'DD/MM/YYYY').toDate() : null,
            option.valueTo ? moment(option.valueTo, 'DD/MM/YYYY').toDate() : null
          ]);
        break;
      case 'text':
      case 'combobox':
      case 'radiobox':
        this.formControl[itemField.code].setValue(option.values[0]);
        break;
      case 'number':
        this.formControl[itemField.code].setValue(
          [option.valueFrom ? Number(option.valueFrom) : null,
            option.valueTo ? Number(option.valueTo) : null
          ]);
        break;
      case 'multi-combobox':
        this.formControl[itemField.code].setValue(option.values);
        break;
      case 'checkbox':
        this.formControl[itemField.code].setValue(option.values);
        break;
      default:
        break;
    }
  }

  deleteBookmark() {
    this.bookmarkFormService.deleteBookmark(this.choosedBookmarkId).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.toastService.success(this.translate.instant('common.notification.updateSuccess'));
        this.choosedBookmarkId = undefined;
        this.getBookmark();
      } else this.toastService.error(this.translate.instant('common.notification.updateError') + ':' + res.message);
    });
  }

  getOptions(): Option[] {
    const options: Option[] = [];

    const option = new Option();

    option.code = 'employeeCode';
    option.values = this.formControl['employeeCode'].value ? [this.formControl['employeeCode'].value] : [];
    options.push(_.clone(option));

    option.code = 'fullName';
    option.values = this.formControl['fullName'].value ? [this.formControl['fullName'].value] : [];
    options.push(_.clone(option));

    option.code = 'listStatus';
    option.values = this.formControl['listStatus'].value ? this.formControl['listStatus'].value : [];
    options.push(_.clone(option));

    option.code = 'listEmpTypeCode';
    option.values = this.formControl['listEmpTypeCode'].value ? this.formControl['listEmpTypeCode'].value : [];
    options.push(_.clone(option));

    option.code = 'listPositionId';
    option.values = this.formControl['listPositionId'].value ? this.formControl['listPositionId'].value : [];
    options.push(_.clone(option));

    option.code = 'organizationId';
    option.values = this.formControl['organizationId'].value ? [this.formControl['organizationId'].value['orgId'] + '|' + (this.formControl['organizationId'].value['orgName'] + '|' + this.formControl['organizationId'].value['parentName'])] : [];
    options.push(_.clone(option));

    this.formConfig.forEach(field => {
      const option: NzSafeAny = new Option();
      option.code = field.code;
      switch (field.inputType) {
        case 'date':
          option.valueFrom = this.formControl[field.code].value ? (this.formControl[field.code].value[0] ? moment(this.formControl[field.code].value[0]).format('DD/MM/YYYY') : null) : null;
          option.valueTo = this.formControl[field.code].value ? (this.formControl[field.code].value[1] ? moment(this.formControl[field.code].value[1]).format('DD/MM/YYYY') : null) : null;
          break;
        case 'number':
          option.valueFrom = this.formControl[field.code].value[0];
          option.valueTo = this.formControl[field.code].value[1];
          break;
        case 'text':
        case 'combobox':
        case 'radiobox':
          option.values = this.formControl[field.code].value ? [this.formControl[field.code].value] : [];
          break;
        case 'multi-combobox':
          option.values = this.formControl[field.code].value ? this.formControl[field.code].value : [];
          break;
        case 'checkbox':
          option.values = this.formControl[field.code].value ?? [];
          break;
        default:
          option.values = [];
          break;
      }
      options.push(option);
    });
    return options;
  }

  parseOptions() {
    let params = new HttpParams();
    if (!StringUtils.isNullOrEmpty(this.formControl['employeeCode'].value))
      params = params.set('employeeCode', this.formControl['employeeCode'].value);
    if (!StringUtils.isNullOrEmpty(this.formControl['fullName'].value))
      params = params.set('fullName', this.formControl['fullName'].value);
    if (this.formControl['organizationId'].value)
      params = params.set('organizationId', this.formControl['organizationId'].value['orgId']);
    if (this.formControl['listStatus'].value) {
      params = params.set('listStatus', this.formControl['listStatus'].value.join(','));
    }
    if (this.formControl['listEmpTypeCode'].value) {
      params = params.set('listEmpTypeCode', this.formControl['listEmpTypeCode'].value.join(','));
    }
    if (this.formControl['listPositionId'].value) {
      params = params.set('listPositionId', this.formControl['listPositionId'].value.join(','));
    }

    this.formConfig?.forEach(item => {
      const formName = item.code;
      const itemField = this.listExtendField[this.listExtendField.findIndex(x => x.code === formName)];
      switch (itemField.inputType) {
        case 'date':
          if (this.formControl[formName].value && (this.formControl[formName].value[0] || this.formControl[formName].value[1])) {
            const startDate = this.formControl[formName].value ? moment(this.formControl[formName].value[0]).format('DD/MM/YYYY') : ""
            const endDate = this.formControl[formName].value ? moment(this.formControl[formName].value[1]).format('DD/MM/YYYY') : ""
            const param = [startDate, endDate];
            params = params.set(formName, param.join(','));
          }
          break;
        case 'number':
          if (this.formControl[formName].value && (this.formControl[formName].value[0] || this.formControl[formName].value[1])) {
            const min = this.formControl[formName].value[0] ? Number(this.formControl[formName].value[0]) : null
            const max = this.formControl[formName].value[1] ? Number(this.formControl[formName].value[1]) : null
            const param = [min, max];
            params = params.set(formName, param.join(','));
          }
          break;
        case 'text':
        case 'combobox':
        case 'radiobox':
          if (!StringUtils.isNullOrEmpty(this.formControl[formName].value))
            params = params.set(formName, this.formControl[formName].value);
          break;
        case 'multi-combobox':
          if (this.formControl[formName].value && this.formControl[formName].value.length > 0) {
            params = params.set(formName, this.formControl[formName].value.join(","));
          }
          break;
        case 'checkbox':
          if (this.formControl[formName].value && this.formControl[formName].value.length > 0)
            params = params.set(formName, this.formControl[formName].value);
          break;
        default:
          break;
      }
    });

    return params;
  }

  setFormValue(event: NzSafeAny, formName: string) {
    if (formName !== 'listStatus' || event.listOfSelected.length > 0) {
      this.formControl[formName].setValue(event.listOfSelected);
    }
  }

  validateDate(date: NzSafeAny, code: NzSafeAny) {
    if (date && date[1] && date[0] > date[1]) {
      this.formControl[code].setErrors({[code]: true});
    } else {
      this.formControl[code].setErrors(null);
    }
  }

  validateDateNumber(date: NzSafeAny, code: NzSafeAny) {
    if (date && date[1] && parseInt(date[0]) > parseInt(date[1])) {
      this.formControl[code].setErrors({[code]: true});
    } else {
      this.formControl[code].setErrors(null);
    }
  }

  submitSearch = () => this.submitEvent.emit(true);

  getOption(itemField: ExtendField, form: NzSafeAny): Option {
    const option: Option = new Option();
    option.code = itemField.code;
    switch (itemField.code) {
      case 'listMonthOfBirth':
      case 'listYearOfBirth':
        if (form[itemField.code][0]) {
          option.valueFrom = form[itemField.code][0];
        }
        if (form[itemField.code][1]) {
          option.valueTo = form[itemField.code][1];
        }
        break;
      case 'listDateOfBirth':
      case 'listJoinCompanyDate':
        if (form[itemField.code][0]) {
          option.valueFrom = moment(form[itemField.code][0]).format('DD/MM/YYYY').toString();
        }
        if (form[itemField.code][1]) {
          option.valueTo = moment(form[itemField.code][1]).format('DD/MM/YYYY').toString();
        }
        break;
      case 'listMajorLevelId':
      case 'listOrgRegions':
      case 'listReligionCode':
      case 'listEthnicCode':
      case 'listMaritalStatusCode':
      case 'listGender':
        option.values = form[itemField.code];
        break;
      case 'personalId':
      case 'flagStatus':
      case 'taxNo':
      case 'insuranceNo':
      case 'mobileNumber':
      case 'email':
        option.values = [form[itemField.code]];
        break;
    }
    return option;
  }

}
