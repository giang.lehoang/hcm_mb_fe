import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RejectCommonComponent} from "./reject-common/reject-common.component";
import {NzModalModule} from "ng-zorro-antd/modal";
import {TranslateModule} from "@ngx-translate/core";
import {NzFormModule} from "ng-zorro-antd/form";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
    imports: [CommonModule, NzModalModule, TranslateModule, NzFormModule, SharedUiMbInputTextModule, ReactiveFormsModule, FormsModule, SharedUiLoadingModule],
  declarations: [RejectCommonComponent],
  exports: [RejectCommonComponent],
})
export class StaffManagerUiRejectCommonModule {}
