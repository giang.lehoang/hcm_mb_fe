import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {CommonDraftService} from "@hcm-mfe/staff-manager/data-access/services";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";

@Component({
  selector: 'app-reject-common',
  templateUrl: './reject-common.component.html',
  styleUrls: ['./reject-common.component.scss']
})
export class RejectCommonComponent implements OnInit {
  @Input() isReject = false;
  @Input() urlRejectById = '';
  @Input() urlRejectByList = '';
  @Input() isRejectList = false;
  @Input() listDraftId: number[] = [];
  @Input() id?:number;
  @Output() onCloseModal: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() resetListDraftId: EventEmitter<boolean> = new EventEmitter<boolean>();

  form: FormGroup;
  isSubmitted = false;
  isLoadingPage = false;

  constructor(private commonDraftService: CommonDraftService,
              private toastrService: ToastrService,
              private fb: FormBuilder,
              private translateService: TranslateService) {
    this.form = this.fb.group({
      rejectReason: [null, [Validators.required, Validators.maxLength(500)]]
    })
  }

  ngOnInit(): void {
  }

  doClose(refresh= false) {
    this.isReject = false;
    this.isSubmitted = false;
    this.onCloseModal.emit(refresh);
    this.form.controls['rejectReason'].setValue(null);
  }

  doReject(id:number) {
    this.isSubmitted = true;
    if(this.isRejectList) {
      this.rejectByList();
      this.resetListDraftId.emit(false);
    } else {
      this.rejectById(id);
    }
  }

  rejectByList() {
    if(this.form.valid) {
      this.isLoadingPage = true;
      const rejectReason = this.form.value['rejectReason'];
      this.commonDraftService.rejectByList(this.listDraftId, this.urlRejectByList, rejectReason).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.toastrService.success(this.translateService.instant('common.notification.isNotApprove'));
          this.doClose(true);
        }
        this.isLoadingPage = false;
      }, error => {
        this.isLoadingPage = false;
        this.toastrService.error(error.message);
      });
    }
  }

  rejectById(id:number) {
    if(this.form.valid) {
      const rejectReason = this.form.value['rejectReason'];
      this.isLoadingPage = true;
      this.commonDraftService.rejectById(id,this.urlRejectById, rejectReason).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.toastrService.success(this.translateService.instant('common.notification.isNotApprove'));
          this.doClose(true);
        }
        if (res.code === HTTP_STATUS_CODE.BAD_REQUEST) {
          this.toastrService.error(res.message);
        }
        this.isLoadingPage = false;
      }, error => {
        this.toastrService.error(error.message);
        this.isLoadingPage = false;
      });
    }
  }

}
