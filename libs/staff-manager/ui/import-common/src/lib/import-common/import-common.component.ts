import {Component, EventEmitter, Input, Output} from '@angular/core';
import {saveAs} from 'file-saver';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {ErrorImport} from "@hcm-mfe/staff-manager/data-access/models/draft";
import {ImportFormService, ShareDataService} from "@hcm-mfe/staff-manager/data-access/services";
import {beforeUploadFile, getTypeExport} from "@hcm-mfe/shared/common/utils";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {NzUploadFile} from "ng-zorro-antd/upload";
import {NzSafeAny} from "ng-zorro-antd/core/types";

@Component({
    selector: 'app-import-common',
    templateUrl: './import-common.component.html',
    styleUrls: ['./import-common.component.scss']
})
export class ImportCommonComponent {

  @Input() showContent = false;
  @Input() requiredAttachFile = false;
  @Input() closeModalWhenClick = true;
  @Input() urlApiImport = '';
  @Input() urlApiDownloadTemp = '';
  @Input() showAttachFile = true;
  @Output() onCloseModal: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() loadPage = new EventEmitter<boolean>();

  isModalError = false;
  errorList: ErrorImport[] = [];
  fileList: NzUploadFile[] = [];
  fileListAttach: NzUploadFile[] = [];
  fileName?: string;
  fileImportName = '';
  fileImportSize?: string;

  isExistFileImport = false;
  nzWidth: number;
  nzWidthError: number;

  isHiddenDownloadErrorFile = true;
  isLoadingPage = false;

  constructor(private importFormService: ImportFormService,
              private toastrService: ToastrService,
              private translate: TranslateService,
              private shareDataService: ShareDataService
  ) {
    this.nzWidth = window.innerWidth > 767 ? window.innerWidth / 3 : window.innerWidth / 2.5;
    this.nzWidthError = window.innerWidth > 767 ? window.innerWidth / 2 : window.innerWidth / 1.5;
  }

  doClose(refresh = false) {
    this.showContent = false;
    this.onCloseModal.emit(refresh);
    this.fileImportName = '';
    this.isExistFileImport = false;
    this.fileListAttach = [];
  }

  doDownloadTemplate() {
    this.isLoadingPage = true;
    this.importFormService.downloadTemplate(this.urlApiDownloadTemp).subscribe(res => {
      const arr = res.headers.get("Content-Disposition")?.split(';');
      const fileName:string = arr[arr.length - 1].replace('filename=','').trim();
      const reportFile = new Blob([res.body], { type: getTypeExport('xlsx') });
      saveAs(reportFile, fileName);
      this.isLoadingPage = false;
    }, error => {
      this.toastrService.error(error.message);
      this.isLoadingPage = false;
    });
  }

  doCloseModal() {
    this.isModalError = false;
  }

  doDownloadFile() {
    this.isLoadingPage = true;
    this.importFormService.doDownloadFileByName(this.fileName).subscribe(res => {
      const reportFile = new Blob([res.body], { type: getTypeExport('xlsx') });
      saveAs(reportFile, 'file_error_information.xlsx');
      this.isHiddenDownloadErrorFile = false;
      this.isLoadingPage = false;
    }, error => {
      this.toastrService.error(error.message);
      this.isLoadingPage = false;
    });
  }

  beforeUpload = (file: NzUploadFile): boolean => {
    this.fileList = [];
    this.fileList =  beforeUploadFile(file, this.fileList, 3000000, this.toastrService, this.translate, 'staffManager.notification.upload.errorFileImport', ".XLS", ".XLSX")
    this.isExistFileImport = true;
    this.fileImportName = file.name;
    this.fileImportSize = ((file.size ?? 1)/1000000).toFixed(2);
    return false;
  };

  doImportFile(): NzSafeAny {
    this.isHiddenDownloadErrorFile = true; // refresh
    if(this.fileListAttach.length === 0 && this.requiredAttachFile) {
      this.toastrService.error(this.translate.instant('staffManager.notification.upload.errorRequiredFile'));
      return false;
    }
    const formData = new FormData();
    this.fileList.forEach((file: NzUploadFile) => {
      formData.append('file', file as NzSafeAny);
    });

    this.fileListAttach.forEach((file: NzUploadFile) => {
      formData.append('fileExtends', file as NzSafeAny);
    });
    this.isLoadingPage = true;
    this.importFormService.doImport(this.urlApiImport, formData).subscribe(res => {
      if (res != undefined && res.code != HTTP_STATUS_CODE.OK) {
        this.errorList = res.data?.errorList;
        if (this.errorList != undefined && this.errorList.length > 0) {
          this.isModalError = true;
          this.fileName = res.data.errorFile;
          this.toastrService.error(this.translate.instant('staffManager.notification.upload.error'));
        } else {
          this.isModalError = false;
          this.errorList = [];
          this.fileName = undefined;
          this.toastrService.error(res.message);
        }
      } else {
        this.isModalError = false;
        this.errorList = [];
        this.fileName = undefined;
        this.shareDataService.emitChange(true);
        this.doClose(true);
        this.toastrService.success(this.translate.instant('staffManager.notification.upload.success'));
      }
      this.isLoadingPage = false;
    }, error => {
      this.toastrService.error(error.message);
      this.isLoadingPage = false;
    });
  }

  doRemoveFile = (): boolean => {
    this.fileList = [];
    this.isExistFileImport = false;
    return true;
  };

  beforeUploadAttach = (file: NzUploadFile): boolean => {
    this.fileListAttach =  beforeUploadFile(file, this.fileListAttach, 15000000, this.toastrService, this.translate, 'common.notification.errorAttachFile', ".ZIP", ".PDF")
    return false;
  }
}

