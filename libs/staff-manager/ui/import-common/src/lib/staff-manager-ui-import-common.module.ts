import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ImportCommonComponent} from "./import-common/import-common.component";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzFormModule} from "ng-zorro-antd/form";
import {TranslateModule} from "@ngx-translate/core";
import {NzUploadModule} from "ng-zorro-antd/upload";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzModalModule} from "ng-zorro-antd/modal";
import {NzTableModule} from "ng-zorro-antd/table";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
    imports: [CommonModule, SharedUiMbButtonModule, NzFormModule, TranslateModule, NzUploadModule, NzIconModule, NzModalModule, NzTableModule, SharedUiLoadingModule],
  declarations: [ImportCommonComponent],
  exports: [ImportCommonComponent],
})
export class StaffManagerUiImportCommonModule {}
