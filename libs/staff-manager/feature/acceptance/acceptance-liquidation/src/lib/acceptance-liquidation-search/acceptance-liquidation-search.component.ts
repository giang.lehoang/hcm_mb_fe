import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {AppFunction, MBTableConfig, Pagination, PersonalInformation} from "@hcm-mfe/shared/data-access/models";
import {Constant, UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {Subscription} from "rxjs";
import {NzModalRef} from "ng-zorro-antd/modal";
import {SearchFormRecruitComponent} from "@hcm-mfe/staff-manager/ui/recruit/search-form-recruit";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {AcceptanceService, SearchFormService} from "@hcm-mfe/staff-manager/data-access/services";
import {TranslateService} from "@ngx-translate/core";
import {ToastrService} from "ngx-toastr";
import {HttpParams} from "@angular/common/http";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {getTypeExport} from "@hcm-mfe/shared/common/utils";
import {saveAs} from "file-saver";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {SessionService} from "@hcm-mfe/shared/common/store";

@Component({
  selector: 'hcm-mfe-acceptance-liquidation-search',
  templateUrl: './acceptance-liquidation-search.component.html',
  styleUrls: ['./acceptance-liquidation-search.component.scss']
})
export class AcceptanceLiquidationSearchComponent implements OnInit {
  isLoadingPage = false;
  tableConfig!: MBTableConfig;
  pagination = new Pagination();
  searchResult!: PersonalInformation[];
  constant = Constant;
  subs: Subscription[] = [];
  modal!: NzModalRef;
  nzLgSearchForm = 8;
  isImportData = false;
  functionCode = FunctionCode.HR_ACCEPTANCE_RECORD;
  objFunction!: AppFunction;

  @ViewChild('searchForm') searchForm!: SearchFormRecruitComponent;
  @ViewChild('empStatusTmpl', {static: true}) empStatus!: TemplateRef<NzSafeAny>;
  @ViewChild('attachFileTmpl', {static: true}) attachFileList!: TemplateRef<NzSafeAny>;
  @ViewChild('uploadStatusTmpl', {static: true}) uploadStatus!: TemplateRef<NzSafeAny>;

  constructor(private searchFormService: SearchFormService,
              private translateService: TranslateService,
              private toastrService: ToastrService,
              private acceptanceService: AcceptanceService,
              private sessionService: SessionService) {
      this.objFunction = this.sessionService.getSessionData(`FUNCTION_${this.functionCode}`);
  }

  ngOnInit(): void {
    this.initTable();
  }

  ngAfterViewInit() {
    this.doSearch(1);
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'staffManager.staffResearch.personalInformation.table.employeeCode',
          field: 'employeeCode',
          width: 120,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left'
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.fullName',
          field: 'fullName',
          width: 150,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left'
        },
        {
          title: 'staffManager.acceptance.acceptanceDate',
          field: 'strDate',
          width: 100,
          tdClassList: ['text-center']
        },
        {
          title: 'staffManager.acceptance.report',
          field: 'attachFileList',
          width: 250,
          tdTemplate: this.attachFileList
        },
        {
          title: 'staffManager.acceptance.uploadStatus',
          field: 'uploadStatus',
          thClassList: ['text-center'],
          width: 150,
          tdTemplate: this.uploadStatus
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.orgName',
          field: 'orgName',
          width: 200
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.positionName',
          field: 'positionName',
          width: 120
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.email',
          field: 'email',
          width: 180,
          show: false
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.flagStatus',
          field: 'empStatus',
          width: 120,
          tdTemplate: this.empStatus
        }
      ],
      total: 0,
      needScroll: false,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  doSearch(pageIndex: number) {
    this.pagination.pageNumber = pageIndex;
    let searchParam = this.searchForm ? this.searchForm.parseOptions() : new HttpParams();
    this.tableConfig.loading = true;
    this.isLoadingPage = true;
    this.subs.push(
      this.searchFormService.getFilterResearch(UrlConstant.SEARCH_FORM.ACCEPTANCE_RECORDS, searchParam, this.pagination.getCurrentPage()).subscribe(res => {
        this.isLoadingPage = false;
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.searchResult = res.data.listData;
          this.tableConfig.total = res.data.count;
        }
        this.tableConfig.loading = false;
      }, error => {
        this.toastrService.error(error.message);
        this.isLoadingPage = false;
        this.tableConfig.loading = false;
      })
    );
  }

  doExportData() {
    let searchParam = this.searchForm ? this.searchForm.parseOptions() : new HttpParams();
    this.isLoadingPage = true;
    this.subs.push(
      this.searchFormService.doExportFile(UrlConstant.EXPORT_REPORT.ACCEPTANCE_RECORDS, searchParam).subscribe(res => {
        if (res?.headers?.get('Excel-File-Empty')) {
          this.toastrService.error(this.translateService.instant("common.notification.exportFail"));
          return;
        } else {
          const reportFile = new Blob([res.body], {type: getTypeExport('xlsx')});
          saveAs(reportFile, 'danh_sach_bien_ban-nghiem_thu.xlsx');
        }
        this.isLoadingPage = false;
      }, error => {
        this.toastrService.error(error.message);
        this.isLoadingPage = false;
      })
    );
  }

  downloadFile(file: NzSafeAny) {
    const params = new HttpParams().set('docId', file?.docId).set('security', file?.security);
    this.isLoadingPage = true;
    this.subs.push(
      this.acceptanceService.downloadFileAttach(params).subscribe(res => {
        const reportFile = new Blob([res.body], { type: getTypeExport(file.fileName.split('.').pop()) });
        saveAs(reportFile, file.fileName);
        this.isLoadingPage = false;
      }, error => {
        this.toastrService.error(this.translateService.instant('staffManager.acceptance.downloadFileError'))
        this.isLoadingPage = false;
      })
    );
  }

  doImportData() {
    this.isImportData = true;
  }

  doCloseImport(refresh: boolean) {
    this.isImportData = false;
    if (refresh) {
      this.doSearch(this.pagination.pageNumber)
    }
  }

  ngOnDestroy(): void {
    this.subs?.forEach(sub => sub?.unsubscribe());
  }
}
