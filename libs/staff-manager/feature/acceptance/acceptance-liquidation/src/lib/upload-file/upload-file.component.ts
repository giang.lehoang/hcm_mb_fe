import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {CatalogModel} from "@hcm-mfe/partnership/ui/pns-contract/search-form";
import {Constant} from "@hcm-mfe/partnership/data-access/pns-contract/common";
import {beforeUploadFile} from "@hcm-mfe/shared/common/utils";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {Subscription} from "rxjs";
import {NzUploadFile} from "ng-zorro-antd/upload";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {AcceptanceService} from "@hcm-mfe/staff-manager/data-access/services";

@Component({
  selector: 'acceptance-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.scss']
})
export class UploadFileComponent implements OnInit {

  @Input() showContent = false;
  @Output() onCloseModal = new EventEmitter<boolean>();


  fileList: NzUploadFile[] = [];
  fileName: string | undefined;

  nzWidth: number;
  listType: CatalogModel[] = [];
  acceptanceDate?: Date;
  isExistFileImport = false;
  subs: Subscription[] = [];
  isLoadingPage = false;

  constructor(private toastrService: ToastrService,
              private translate: TranslateService,
              private acceptanceService: AcceptanceService) {
    this.nzWidth = window.innerWidth > 767 ? window.innerWidth / 3 : window.innerWidth / 2.5;
  }

  ngOnInit(): void {
    this.getListType();
  }

  getListType() {
    this.listType = Constant.CONTRACT_TYPE.map((item => {
      item.label = this.translate.instant(item.label);
      return item;
    }));
  }

  doClose(refresh: boolean = false) {
    this.onCloseModal.emit(refresh);
    this.fileList = [];
    this.acceptanceDate = undefined;
    this.isExistFileImport = false;
  }

  beforeUpload = (file: NzUploadFile): boolean => {
    this.fileList = beforeUploadFile(file, this.fileList, 1500000000, this.toastrService, this.translate, 'common.notification.errorAttachFile', '.ZIP', '.PDF');
    this.isExistFileImport = this.fileList.length > 0;

    return false;
  };

  doImportFile() {
    if (!this.acceptanceDate) {
      this.toastrService.error(this.translate.instant('staffManager.acceptance.dateRequired'));
    } else {
      const formData = new FormData();
      this.fileList.forEach((file: NzUploadFile) => {
        formData.append('files', file as NzSafeAny);
      });
      const data = {
        month: this.acceptanceDate.getMonth() + 1,
        year: this.acceptanceDate.getFullYear()
      }
      formData.append('data', new Blob([JSON.stringify(data)], { type: 'application/json' }));
      this.isLoadingPage = true;
      this.subs.push(
        this.acceptanceService.uploadAttachFile(formData).subscribe(res => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.doClose(true);
            this.toastrService.success(this.translate.instant('partnership.pns-contract.notification.importSuccess'));
          } else if (res.code === HTTP_STATUS_CODE.CREATED) {
            this.toastrService.error(res.message);
          }
          this.isLoadingPage = false;
        }, error => {
            this.isLoadingPage = false;
            this.toastrService.error(error.message)
        })
      );
    }
  }

  ngOnDestroy() {
    this.subs?.forEach((s) => s.unsubscribe());
  }
}

