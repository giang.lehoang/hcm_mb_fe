import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {CatalogModel} from "@hcm-mfe/shared/data-access/models";
import {Subscription} from "rxjs";
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";
import {SearchFormService} from "@hcm-mfe/staff-manager/data-access/services";
import {StringUtils} from "@hcm-mfe/shared/common/utils";
import {HttpParams} from "@angular/common/http";
import {Constant, UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {Scopes} from "@hcm-mfe/shared/common/enums";

@Component({
  selector: 'acceptance-liquidation-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {

  @Input() showEmpTypeCode = true;
  @Input() showEmpStatus = true;
  @Input() nzLg = 6;
  @Input() functionCode = '';
  @Input() scope = Scopes.VIEW;

  form!: FormGroup;
  listEmpTypeCode: CatalogModel[] = [];
  listPosition: CatalogModel[] = [];
  subscriptions: Subscription[] = [];
  statusEmployeeList: CatalogModel[] = [];
  uploadStatusList: CatalogModel[] = [];

  constructor(private fb: FormBuilder,
              private toastrService: ToastrService,
              private translateService: TranslateService,
              private searchFormService: SearchFormService) { }

  ngOnInit(): void {
    this.initDataSelect();
    this.initFormGroup();
  }

  initFormGroup() {
    this.form = this.fb.group({
      organizationId: null,
      listEmpTypeCode: null,
      listPositionId: null,
      keySearch: null,
      listEmpStatus: [['1']],
      uploadStatusCode: null,
      acceptanceDate: [new Date()]
    });
  }

  getListEmpTypeCode() {
    const params = new HttpParams().set('typeCode', Constant.TYPE_CODE.DOI_TUONG_CV);
    this.subscriptions.push(
      this.searchFormService.getListValue(UrlConstant.CATEGORY.LOOKUP_VALUE, params).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listEmpTypeCode = res.data;
        }
      }, error => {
        this.toastrService.error(error.message);
        this.listEmpTypeCode = [];
      })
    );
  }

  initDataSelect() {
    this.statusEmployeeList = Constant.STATUS_EMP.map(item => {
      item.label = this.translateService.instant(item.label);
      return item;
    });

    this.uploadStatusList = Constant.UPLOAD_STATUS.map(item => {
      item.label = this.translateService.instant(item.label);
      return item;
    })

    this.getListEmpTypeCode();
    this.getListPosition();
  }


  getListPosition() {
    const params = new HttpParams().set('jobType', Constant.JOB_TYPE.ORG);
    this.subscriptions.push(
      this.searchFormService.getListValue(UrlConstant.CATEGORY.MP_JOB, params).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listPosition = res.data;
        } else {
          this.toastrService.error(res.message);
        }
      }, error => {
        this.toastrService.error(error.message);
        this.listPosition = [];
      })
    );
  }


  setFormValue(event: NzSafeAny, formName: string) {
    if (formName !== 'listEmpStatus' || event.listOfSelected?.length > 0) {
      this.form.controls[formName].setValue(event.listOfSelected);
    }
  }

  get formControl() {
    return this.form.controls;
  }

  parseOptions() {
    let params = new HttpParams();

    if (!StringUtils.isNullOrEmpty(this.formControl['keySearch'].value))
      params = params.set('keySearch', this.formControl['keySearch'].value.trim());

    if (!StringUtils.isNullOrEmpty(this.formControl['uploadStatusCode'].value))
      params = params.set('uploadStatusCode', this.formControl['uploadStatusCode'].value.trim());

    if (this.formControl['organizationId'].value)
      params = params.set('organizationId', this.formControl['organizationId'].value['orgId']);

    if (this.formControl['listEmpTypeCode'].value){
      params = params.set('listEmpTypeCode', this.formControl['listEmpTypeCode'].value.join(','));
    }
    if (this.formControl['listPositionId'].value){
      params = params.set('listPositionId', this.formControl['listPositionId'].value.join(','));
    }

    if (this.formControl['listEmpStatus'].value !== null)
      params = params.set('listEmpStatus', this.formControl['listEmpStatus'].value.join(','));

    if (this.formControl['acceptanceDate'].value !== null) {
      const month = this.formControl['acceptanceDate'].value.getMonth() + 1;
      const year = this.formControl['acceptanceDate'].value.getFullYear();
      params = params.set('month', month);
      params = params.set('year', year);
    }

    return params;
  }

  ngOnDestroy(): void {
    this.subscriptions?.forEach(sub => sub?.unsubscribe());
  }

}
