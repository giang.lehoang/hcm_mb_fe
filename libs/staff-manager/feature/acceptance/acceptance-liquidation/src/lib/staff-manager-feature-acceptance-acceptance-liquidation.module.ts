import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  AcceptanceLiquidationSearchComponent
} from './acceptance-liquidation-search/acceptance-liquidation-search.component';
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {StaffManagerUiRecruitSearchFormRecruitModule} from "@hcm-mfe/staff-manager/ui/recruit/search-form-recruit";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {StaffManagerUiRecruitImportFileModule} from "@hcm-mfe/staff-manager/ui/recruit/import-file";
import {NzTagModule} from "ng-zorro-antd/tag";
import {TranslateModule} from "@ngx-translate/core";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
import {CoreModule} from "@hcm-mfe/shared/ui/core";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiOrgDataPickerModule} from "@hcm-mfe/shared/ui/org-data-picker";
import {SharedUiEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/employee-data-picker";
import {SharedUiMbUploadModule} from "@hcm-mfe/shared/ui/mb-upload";
import {SearchFormComponent} from "./search-form/search-form.component";
import {SharedUiMbSelectCheckAbleModule} from "@hcm-mfe/shared/ui/mb-select-check-able";
import {RouterModule} from "@angular/router";
import {UploadFileComponent} from "./upload-file/upload-file.component";

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, StaffManagerUiRecruitSearchFormRecruitModule, SharedUiMbButtonModule, SharedUiMbTableWrapModule, SharedUiMbTableModule, StaffManagerUiRecruitImportFileModule, NzTagModule, TranslateModule, NzToolTipModule, CoreModule, SharedUiMbSelectModule, SharedUiMbDatePickerModule, SharedUiMbInputTextModule, SharedUiOrgDataPickerModule, SharedUiEmployeeDataPickerModule, SharedUiMbUploadModule, SharedUiMbSelectCheckAbleModule,
    RouterModule.forChild([
      {
        path: '',
        component: AcceptanceLiquidationSearchComponent
      }
    ])],

  declarations: [
    AcceptanceLiquidationSearchComponent, SearchFormComponent, UploadFileComponent
  ],
  exports: [AcceptanceLiquidationSearchComponent, SearchFormComponent]
})
export class StaffManagerFeatureAcceptanceAcceptanceLiquidationModule {}
