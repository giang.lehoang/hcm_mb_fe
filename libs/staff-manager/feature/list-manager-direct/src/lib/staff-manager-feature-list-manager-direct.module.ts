import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { TranslateModule } from '@ngx-translate/core';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { RouterModule } from '@angular/router';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { SharedUiMbDatePickerModule } from '@hcm-mfe/shared/ui/mb-date-picker';
import { SharedUiMbEmployeeDataPickerModule } from '@hcm-mfe/shared/ui/mb-employee-data-picker';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { StaffManagerUiImportEmployeeResearchModule } from '@hcm-mfe/staff-manager/ui/import-employee-research';
import { SharedUiMbTableWrapModule } from '@hcm-mfe/shared/ui/mb-table-wrap';
import { SharedUiMbTableModule } from '@hcm-mfe/shared/ui/mb-table';
import { ListManagerDirectComponent } from './list-manager-direct/list-manager-direct.component';
import { PopupService } from '@hcm-mfe/shared/ui/popup';

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, SharedUiMbButtonModule, TranslateModule, NzCardModule, NzIconModule,
    ReactiveFormsModule,
    SharedUiMbInputTextModule, FormsModule, SharedUiMbSelectModule, NzTableModule, NzToolTipModule, NzPaginationModule,
    RouterModule.forChild([
      {
        path: '',
        component: ListManagerDirectComponent
      }]),
    NzTagModule, NzGridModule, MatMenuModule, MatIconModule, SharedUiMbDatePickerModule, SharedUiMbEmployeeDataPickerModule, NzRadioModule, StaffManagerUiImportEmployeeResearchModule, SharedUiMbTableWrapModule, SharedUiMbTableModule],
  declarations: [ListManagerDirectComponent],
  exports: [ListManagerDirectComponent],
  providers:[PopupService]
})
export class StaffManagerFeatureListManagerDirectModule {}
