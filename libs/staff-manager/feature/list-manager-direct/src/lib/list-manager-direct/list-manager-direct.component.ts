/*
  author: hannd.os
  create date: 13/10/2022
* */
import { Component, OnInit, Injector, ViewChild, TemplateRef } from '@angular/core';
import * as FileSaver from 'file-saver';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {
  Attributes,
  Job, Pagination
} from "@hcm-mfe/model-organization/data-access/models";
import { userConfig } from '@hcm-mfe/shared/common/constants';
import { FunctionCode, functionUri, maxInt32 } from '@hcm-mfe/shared/common/enums';
import { DataService } from '@hcm-mfe/model-organization/data-access/services';
import {FormModalShowTreeUnitComponent} from "@hcm-mfe/shared/ui/form-modal-show-tree-unit";
import { EmployeeDetail, MBTableConfig } from '@hcm-mfe/shared/data-access/models';
import { UrlConstant } from '@hcm-mfe/staff-manager/data-access/common';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import * as moment from 'moment';
import { MANAGER_DIRECT, ManagerModel, ParamsModel } from '@hcm-mfe/staff-manager/data-access/models/research';
import { ManagerDirectService } from '@hcm-mfe/staff-manager/data-access/services';

@Component({
  selector: 'app-list-manager-direct',
  templateUrl: './list-manager-direct.component.html',
  styleUrls: ['./list-manager-direct.component.scss']
})
export class ListManagerDirectComponent extends BaseComponent implements OnInit {
  @ViewChild('actionTmpl', {static: true}) action!: TemplateRef<NzSafeAny>;


  orgName: string;
  searchParams: ParamsModel = new ParamsModel();
  params: ParamsModel = new ParamsModel();
  listJob: Job[] = [];
  listAssign = MANAGER_DIRECT.LIST_FORM_ASSIGN;
  listDisplay = MANAGER_DIRECT.DISPLAY;
  listManager: ManagerModel[]= [];
  listAttribute: Attributes[] = [];
  tableConfig:MBTableConfig;
  dataTable: ManagerModel[] = [];
  pagination: Pagination = new Pagination(userConfig.pageSize);
  isLoadingPage: boolean;
  isSeach: boolean;
  employeeId = '';
  isImportData = false;
  urlApiImport: string = UrlConstant.IMPORT_FORM.MANAGER_DIRECT_IMPORT;
  urlApiDownloadTemp: string = UrlConstant.IMPORT_FORM.MANAGER_PROCESS_IMPORT_TEMPLATE;
  heightTable = { x: '100vw', y: '30vh'};
  listClassify = MANAGER_DIRECT.LIST_CLASSIFY;
  listManagerTmp: ManagerModel[]= [];

  constructor(
    injector: Injector,
    private readonly managerDirectService: ManagerDirectService,
    private readonly dataService: DataService
  ) {
    super(injector)
    this.isSeach = false;
    this.orgName = '';
    this.isLoadingPage = false;
    this.tableConfig = new MBTableConfig();
    this.objFunction = this.sessionService.getSessionData(
      `FUNCTION_${FunctionCode.MANAGER_DIREECT}`
    );
  }

  ngOnInit(): void {
    this.getListManager();
    this.getJob();
    this.getData();
    this.initTable();
  }

  getListManager(){
    const params = {
      keyword:'',
      size: maxInt32
    }
    this.managerDirectService.getListManager(params).subscribe(data => {
      this.listManager =  data.data.listData;
      this.listManager.forEach(item => {
        item.fullName = `${item.fullName}-${item.employeeCode}`;
        if(item.status === 3){
          item.fullName = item.fullName + ' (Nghỉ việc)';
        }
      });
    });
  }
  getData(){
    this.checkParam();
    this.isLoadingPage = true;
    this.managerDirectService.getListData(this.params).subscribe(data => {
      this.dataTable = data.data.listData;
      this.tableConfig.total = data.data.count;
      this.isLoadingPage = false;
      this.dataTable.forEach((item) => {
        if(String(item.isMainPosition) === "0"){
          item.classify = this.translate.instant("staffManager.managerDirect.label.processPartime")
        } else {
          item.classify = this.translate.instant("staffManager.managerDirect.label.processWork")
        }
        item.dataTmp = item;
      })
    },(error:any) => {
      this.isLoadingPage = false;
      this.toastrCustom.error(error.message);
      this.isLoadingPage = false;
    });
  }
  showModalOrg(event:string): void {
    const modal = this.modal.create({
      nzWidth: '85vw',
      nzTitle: '',
      nzContent: FormModalShowTreeUnitComponent,
      nzFooter: null,
      nzComponentParams: {
        resourceCode: FunctionCode.MANAGER_DIREECT,
        scope: "00_VIEW"
      },
    });
    modal.afterClose.subscribe((result) => {
      if (result?.orgName) {
        this.orgName = result?.orgName;
        this.searchParams.orgId = result?.orgId;
      }
    });
  }
  clearOrgInput() {
    this.orgName = '';
    this.searchParams.orgId = '';
  }

  getJob() {
    this.dataService.getJobsByType(this.searchParams.orgId).subscribe((data) => {
      this.listJob = data.data.content;
    }, (error:any) => {
      this.toastrCustom.error(error.message);
    })
  }

  search(page:number){
    this.searchParams.page = page -1
    this.getData();
  }
  checkParam() {
    this.params.employeeId = this.searchParams.employeeId ? this.searchParams.employeeId : '';
    this.params.orgId = this.searchParams.orgId ? this.searchParams.orgId : '';
    this.params.jobId = this.searchParams.jobId ? this.searchParams.jobId:'';
    this.params.managerId = this.searchParams.managerId ?this.searchParams.managerId:'';
    this.params.indirectManagerId = this.searchParams.indirectManagerId?this.searchParams.indirectManagerId:'';
    this.params.isLasted = this.searchParams.isLasted ? this.searchParams.isLasted:'1';
    this.params.isAuto = this.searchParams.isAuto?this.searchParams.isAuto:'';
    this.params.isAssign = this.searchParams.isAssign?this.searchParams.isAssign:''
    this.params.fromDate =  this.searchParams.fromDate?moment(this.searchParams.fromDate).format('DD/MM/YYYY'):'';
    this.params.isMainPosition = this.searchParams.isMainPosition?this.searchParams.isMainPosition:'';
    this.params.page = this.searchParams.page;
    this.params.size = this.searchParams.size;
  }

  onSearch() {
    this.isSeach = true;
    this.searchParams.page = 0;
    this.tableConfig.pageIndex = 1;
    this.getData();
  }

  exportExcel() {
    this.checkParam()
    this.managerDirectService.exportExcel(this.params).subscribe(
      (data) => {
        const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
        FileSaver.saveAs(blob, 'template_export_cbql truc tiep - gian tiep - news.xlsx');
      },
      (error) => {
        this.toastrCustom.error(error.error.message);
      }
    );
  }
  showUpload() {
    this.isImportData = true;
  }
  onChange(event: EmployeeDetail){
    if(event){
      this.searchParams.employeeId = String(event.employeeId);
    } else {
      this.searchParams.employeeId = ''
    }
  }
  doCloseImport() {
    this.isImportData = false;
  }
  override triggerSearchEvent(): void {
    this.onSearch();
  }
  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'staffManager.managerDirect.table.label.empCode',
          field: 'employeeCode',
          fixed: window.innerWidth > 1024,
          fixedDir: 'left',
          width: 60,
        },
        {
          title: 'staffManager.managerDirect.table.label.empName',
          field: 'fullName',
          fixed: window.innerWidth > 1024,
          fixedDir: 'left',
          width: 80,
        },
        {
          title: 'staffManager.managerDirect.label.fromDate',
          field: 'fromDate',
          width: 60,
          pipe:'date',
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'staffManager.managerDirect.table.label.org',
          field: 'orgFullName',
          needEllipsis: true,
          fixed: window.innerWidth > 1024,
          width: 120,
        },
        {
          title: 'Job',
          field: 'jobName',
          fixed: window.innerWidth > 1024,
          width: 100,
        },
        {
          title: 'staffManager.managerDirect.table.label.classify',
          field: 'classify',
          fixed: window.innerWidth > 1024,
          width: 100,
        },
        {
          title: 'staffManager.managerDirect.table.label.managerDirect',
          field: 'manager',
          width: 120,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'staffManager.managerDirect.table.label.managerIndirect',
          field: 'indirectManager',
          width: 120,
          fixed: window.innerWidth > 1024,
        },
        {
          title: ' ',
          tdTemplate: this.action,
          width: 60,
          fixed: window.innerWidth > 1024,
          fixedDir: 'right',
        }
      ],
      total: 0,
      loading: false,
      size: 'small',
      pageSize: this.params.size,
      pageIndex: 1,
      showFrontPagination: false,
      needScroll:true,
    };
  }
  redirectAssign(managerProcessId?:any){
    const data = {...managerProcessId}
    data.dataTmp = null;
    if(managerProcessId){
      localStorage.setItem('managerDirect',JSON.stringify(data));
    }
    this.router.navigate([functionUri.system_manager_direct_assign]);
  }
  dblClick(manager:ManagerModel){
    manager.dataTmp = null;
    if(manager){
      localStorage.setItem('managerDirect',JSON.stringify(manager));
    }
    this.router.navigate([functionUri.system_manager_direct_assign]);
  }
  deleteRecord(id: any,event:any){
    this.deletePopup.showModal(() => {
      this.managerDirectService.deleteManager(id).subscribe(data => {
        this.isLoadingPage = false;
        this.toastrCustom.success();
        this.getData();
      }, (error: any) => {
        this.isLoadingPage = false;
        this.toastrCustom.error(error.message)
      })
    })
  }
  onChangeType(event:any){
    this.searchParams.isAssign = event;
  }
  onSearchDirect(event:any){
    const listTmp: ManagerModel[]  = [];
    if(event){
      this.listManager.forEach(item => {
        if ((item.userName &&  item.userName.toLowerCase().includes(event.toLowerCase()))
          || item.fullName.toLowerCase().includes(event.toLowerCase())
          ||  item.employeeCode?.toLowerCase().includes(event.toLowerCase())
        ) {
          listTmp.push(item);
        }
      });
      this.listManagerTmp = [...listTmp];
    } else {
      this.listManagerTmp = [...this.listManager]
    }
  }
}
