import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LayoutDraftComponent} from "./layout-draft/layout-draft.component";
import {NzFormModule} from "ng-zorro-antd/form";
import {RouterModule} from "@angular/router";
import {StaffManagerFeatureDraftSideBarModule} from "@hcm-mfe/staff-manager/feature/draft/side-bar";

@NgModule({
  imports: [CommonModule, NzFormModule, RouterModule, StaffManagerFeatureDraftSideBarModule],
  declarations: [LayoutDraftComponent],
  exports: [LayoutDraftComponent]
})
export class StaffManagerFeatureDraftLayoutDraftModule {}
