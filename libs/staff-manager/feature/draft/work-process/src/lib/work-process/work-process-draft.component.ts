import {AfterViewInit, Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { Router } from '@angular/router';
import { HttpParams } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { saveAs } from 'file-saver';

import {Constant, ConstantColor, UrlConstant, validateRejectByList} from "@hcm-mfe/staff-manager/data-access/common";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {AppFunction, CatalogModel, MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {SearchFormComponent} from "@hcm-mfe/staff-manager/ui/search-form-draft";
import {
  DownloadFileAttachService,
  SearchFormService, WorkProcessService
} from "@hcm-mfe/staff-manager/data-access/services";
import {ValidateService} from "@hcm-mfe/shared/core";
import { SessionService } from "@hcm-mfe/shared/common/store";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {getTypeExport} from "@hcm-mfe/shared/common/utils";
import {WorkProcess} from "@hcm-mfe/staff-manager/data-access/models/draft";

@Component({
    selector: 'app-work-his-research',
    templateUrl: './work-process-draft.component.html',
    styleUrls: ['./work-process-draft.component.scss']
})
export class WorkProcessDraftComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('searchForm') searchForm!: SearchFormComponent;
    @ViewChild('flagStatusTmpl', { static: true }) flagStatus!: TemplateRef<NzSafeAny>;
    @ViewChild('actionTmpl', { static: true }) action!: TemplateRef<NzSafeAny>;
    @ViewChild('statusTmpl', { static: true }) status!: TemplateRef<NzSafeAny>;
    @ViewChild('selectTmpl', { static: true }) selectRecord!: TemplateRef<NzSafeAny>;
    @ViewChild('attachFileTmpl', {static: true}) attachFile!: TemplateRef<NzSafeAny>;
    @ViewChild('suggestTypeTpl', {static: true}) suggestTypeTpl!: TemplateRef<NzSafeAny>;

    public readonly MODULENAME = Constant.MODULE_NAME.FAMILY_DEDUCTIONS;
    functionCode: string = FunctionCode.HR_WORK_PROCESS;
    objFunction: AppFunction;
    tableConfig!: MBTableConfig;
    searchResult: WorkProcess[] = [];

    isImportData = false;
    urlApiImport: string = UrlConstant.IMPORT_EXCEL.WORK_PROCESS;
    urlApiDownloadTemp: string = UrlConstant.IMPORT_FORM.WORK_PROCESS_TEMPLATE;

    subs: Subscription[] = [];
    pagination = new Pagination();

    selectedCheckBoxNumber = 0;
    draftWorkProcessSet = new Set<number>();

    listDraftId: number[] = Array.from(this.draftWorkProcessSet);
    isRejectList = false;
    isReject = false;
    urlRejectById: string = UrlConstant.REFUSE_APPROVE_DRAFT.WORK_PROCESS;
    urlRejectByList: string = UrlConstant.REFUSE_APPROVE_DRAFT.WORK_PROCESS_LIST;
    id?: number;

    isDisabledApproveAllBtn = false;
    btnDisableColor: string = ConstantColor.COLOR.BG_PRIMARY_LIGHT;

    tagInitializeColor: string = ConstantColor.TAG.STATUS_INITIALIZE;
    tagApprovedColor: string = ConstantColor.TAG.STATUS_APPROVED;
    tagNotApprovedColor: string = ConstantColor.TAG.STATUS_NOT_APPROVED;
    disabledColor:string = ConstantColor.COLOR.DISABLED;

    mapEmployeeIsNotApproved = new Map<number, WorkProcess>();
    constant = Constant;

    CONSTANT_STATUS = Constant.APPROVE;
    CONSTANT_FLAG_STATUS = Constant.FLAG_STATUS;
    requiredAttachFile = true;
    isLoadingPage = false;
    statusList: CatalogModel[] = [
        {
            value: 1,
            label: this.translateService.instant('staffManager.label.workIn')
        },
        {
            value: 0,
            label: this.translateService.instant('staffManager.label.workOut')
        }
    ];

    statusApproveList: CatalogModel[] = [
        {
            value: 0,
            label: this.translateService.instant('staffManager.label.isInitial')
        },
        {
            value: 1,
            label: this.translateService.instant('staffManager.label.isApprove')
        },
        {
            value: 2,
            label: this.translateService.instant('staffManager.label.isNotApprove')
        }
    ];

    constructor(
                private router: Router,
                private translateService: TranslateService,
                private searchFormService: SearchFormService,
                public validateService: ValidateService,
                private workProcessService: WorkProcessService,
                private toastrService: ToastrService,
                public sessionService: SessionService,
                private downloadFileAttachService: DownloadFileAttachService
    ) {
        this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.HR_MODIFY_WORK_PROCESS}`);
    }

    ngOnInit(): void {
        this.initTable();
    }

    ngAfterViewInit() {
      this.doSearch(1);
    }

    ngOnDestroy() {
        this.subs.forEach(sub => sub.unsubscribe());
    }

    doImportData() {
        this.isImportData = true;
    }

    doCloseImport(event: boolean) {
      this.isImportData = false;
      if(event) {
        this.doSearch(this.pagination.pageNumber);
      }
    }

    doSearch(page: number) {
        this.pagination.pageNumber = page;
        this.isLoadingPage = true;
        const searchParam = this.searchForm ? this.searchForm.parseOptions() : new HttpParams();
        this.searchFormService.getFilterResearch(UrlConstant.SEARCH_FORM_DRAFT.WORK_PROCESS, searchParam, this.pagination.getCurrentPage()).subscribe(res => {
            if (res.code === HTTP_STATUS_CODE.OK) {
                this.isDisabledApproveAllBtn = res.data.listData.every((draft: NzSafeAny) => draft.status === Constant.APPROVE.IS_APPROVE || draft.status === Constant.APPROVE.IS_NOT_APPROVE);
                if (this.tableConfig.pageSize) {
                  if (Math.ceil(res.data.count/ this.tableConfig.pageSize) >= 2) {
                      this.isDisabledApproveAllBtn = false;
                  }
                }
                res.data.listData
                    .filter((draft: NzSafeAny) => draft.status === Constant.APPROVE.IS_NOT_APPROVE)
                    .forEach((draft: NzSafeAny) => this.mapEmployeeIsNotApproved.set(draft.draftWorkProcessId, draft));
                this.searchResult = res.data.listData;
                this.tableConfig.total = res.data.count;
            }
            this.isLoadingPage = false;
        }, () => this.isLoadingPage = false);
    }

    private initTable(): void {
        this.tableConfig = {
            headers: [
                {
                    title: 'staffManager.staffResearch.workProcess.table.select',
                    field: 'select',
                    tdClassList: ['text-center'], thClassList: ['text-center'],
                    tdTemplate: this.selectRecord,
                    width: 60,
                    show: this.objFunction.approve,
                    fixedDir: 'left'
                },
                {
                    title: 'staffManager.staffResearch.workProcess.table.employeeCode',
                    field: 'employeeCode',
                    width: 120,
                    thClassList: ['text-center'],
                    fixed: true,
                    fixedDir: 'left'
                },
                {
                    title: 'staffManager.staffResearch.workProcess.table.fullName',
                    field: 'fullName',
                    width: 150,
                    thClassList: ['text-center']
                },
                {
                    title: 'staffManager.staffResearch.workProcess.table.suggestType',
                    field: 'inputType',
                    tdTemplate: this.suggestTypeTpl,
                    tdClassList: ['text-center'], thClassList: ['text-center'],
                    width: 150,
                },
                {
                    title: 'staffManager.staffResearch.workProcess.table.status',
                    field: 'status',
                    tdTemplate: this.status,
                    tdClassList: ['text-center'], thClassList: ['text-center'],
                    show: true,
                    width: 100,
                },
                {
                    title: 'staffManager.staffResearch.workProcess.table.fromDate',
                    field: 'fromDate',
                    tdClassList: ['text-center'],
                    thClassList: ['text-center'],
                    width: 100
                },
                {
                    title: 'staffManager.staffResearch.workProcess.table.toDate',
                    field: 'toDate',
                    tdClassList: ['text-center'],
                    thClassList: ['text-center'],
                    width: 100
                },
                {
                    title: 'staffManager.staffResearch.workHis.table.planToDate1',
                    field: 'planToDate1',
                    tdClassList: ['text-center'],
                    thClassList: ['text-center'],
                    show: false,
                    width: 100
                },
                {
                    title: 'staffManager.staffResearch.workHis.table.planToDate2',
                    field: 'planToDate2',
                    tdClassList: ['text-center'],
                    thClassList: ['text-center'],
                    show: false,
                    width: 100
                },
                {
                    title: 'staffManager.staffResearch.workProcess.table.progressType',
                    field: 'documentTypeName',
                    width: 150
                },
                {
                    title: 'staffManager.staffResearch.workProcess.table.empTypeName',
                    field: 'empTypeName',
                    width: 150
                },
                {
                    title: 'staffManager.staffResearch.workProcess.table.positionName',
                    field: 'positionName',
                    width: 150
                },
                {
                    title: 'staffManager.staffResearch.workProcess.table.positionGroup',
                    field: 'pgrName',
                    width: 150
                },
                {
                  title: 'staffManager.staffResearch.workProcess.table.currentOrgName',
                  field: 'currentOrgName',
                  width: 200,
                  thClassList: ['text-center'],
                  show: false
                },
                {
                    title: 'staffManager.staffResearch.workProcess.table.proposalOrgName',
                    field: 'organizationName',
                    width: 200,
                    thClassList: ['text-center'],
                },
                {
                    title: 'staffManager.staffResearch.allowanceHis.table.attachFile',
                    field: 'attachFileList',
                    width: 200,
                    show: true,
                    tdTemplate: this.attachFile
                },
                {
                    title: ' ',
                    field: 'action',
                    tdTemplate: this.action,
                    tdClassList: ['text-center'],
                    thClassList: ['text-center'],
                    show: this.objFunction.approve || this.objFunction.delete,
                    width: 50,
                    fixed: true,
                    fixedDir: 'right'
                }
            ],
            total: 0,
            needScroll: true,
            loading: false,
            size: 'small',
            pageSize: this.pagination.pageSize,
            pageIndex: 1
        };
    }

    approveWorkProcess(draftWorkProcessId: number) {
        this.isLoadingPage = true;
        this.workProcessService.approveWorkProcess(draftWorkProcessId).subscribe(res => {
            if (res.code === HTTP_STATUS_CODE.OK) {
                this.doSearch(1);
                this.toastrService.success(this.translateService.instant('common.notification.isApprove'));
            }
            this.isLoadingPage = false;
        }, error => {
            this.toastrService.error(error.message);
            this.isLoadingPage = false;
        });
    }

    getFlagStatus(value: number) {
        return this.statusList.find(status => status.value === value)?.label;
    }

    getStatus(value: number) {
        return this.statusApproveList.find(status => status.value === value)?.label;
    }

    approveAll() {
        this.isLoadingPage = true;
        this.workProcessService.approveAllWorkProcess(this.searchForm.getSearchForm()).subscribe(res => {
            if (res.code === HTTP_STATUS_CODE.OK) {
                this.doSearch(1);
                this.toastrService.success(this.translateService.instant('common.notification.isApprove'));
            }
            this.isLoadingPage = false;
        }, error => {
            this.toastrService.error(error.message);
            this.isLoadingPage = false;
        });
    }

    onCheckBoxChange(event: boolean, draftPersonalWorkProcessId: number) {
        if (event) {
            this.selectedCheckBoxNumber++;
            this.draftWorkProcessSet.add(draftPersonalWorkProcessId);
        } else {
            this.selectedCheckBoxNumber--;
            if (this.draftWorkProcessSet.has(draftPersonalWorkProcessId)) {
                this.draftWorkProcessSet.delete(draftPersonalWorkProcessId);
            }
        }
    }

    approveWorkProcessList() {
        this.isLoadingPage = true;
        const listId = Array.from(this.draftWorkProcessSet);
        this.workProcessService.approveWorkProcessByListId(listId).subscribe(res => {
            if (res.code === HTTP_STATUS_CODE.OK) {
                this.doSearch(1);
                this.toastrService.success(this.translateService.instant('common.notification.isApprove'));
            }
            this.isLoadingPage = false;
        }, error => {
            this.toastrService.error(error.message);
            this.isLoadingPage = false;
        });
        this.draftWorkProcessSet.clear();
        this.selectedCheckBoxNumber = 0;
    }

    confirm(draftWorkProcessId: number) {
        this.isLoadingPage = true;
        this.workProcessService.deleteDraftWorkProcessById(draftWorkProcessId).subscribe(res => {
            if (res.code === HTTP_STATUS_CODE.OK) {
                this.doSearch(1);
                this.toastrService.success(this.translateService.instant('common.notification.deleteSuccess'));
            }
            this.isLoadingPage = false;
        }, error => {
            this.toastrService.error(error.message);
            this.isLoadingPage = false;
        });
    }

    doCloseReject(){
        this.isReject = false;
        this.doSearch(1);
    }

    resetListDraftId() {
        this.listDraftId = [];
    }

    openRejectById(draftWorkProcessId: number) {
        this.isReject = true;
        this.id = draftWorkProcessId;
    }

    openRejectByList() {
        if (validateRejectByList(this.draftWorkProcessSet, this.mapEmployeeIsNotApproved, this.translateService, this.toastrService)) {
            this.isReject = true;
            this.isRejectList = true;
        }
    }

    doDownloadAttach(file: NzSafeAny) {
      this.isLoadingPage = true;
      this.downloadFileAttachService.doDownloadAttachFileWithSecurity(~~file?.docId, file?.security).subscribe(res => {
        const reportFile = new Blob([res.body], { type: getTypeExport(file.fileName.split('.').pop()) });
        saveAs(reportFile, file.fileName);
        this.isLoadingPage = false;
      }, error => {
        this.toastrService.error(error.message);
        this.isLoadingPage = false;
      });
    }

    onLoadPage(event: boolean) {
      this.isLoadingPage = event;
    }

}
