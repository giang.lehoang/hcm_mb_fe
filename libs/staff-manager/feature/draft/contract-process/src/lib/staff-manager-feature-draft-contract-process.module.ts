import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ContractProcessDraftComponent} from "./contract-process/contract-process-draft.component";
import {StaffManagerUiSearchFormDraftModule} from "@hcm-mfe/staff-manager/ui/search-form-draft";
import {NzFormModule} from "ng-zorro-antd/form";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {StaffManagerUiImportCommonModule} from "@hcm-mfe/staff-manager/ui/import-common";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {StaffManagerUiRejectCommonModule} from "@hcm-mfe/staff-manager/ui/reject-common";
import {NzTagModule} from "ng-zorro-antd/tag";
import {RouterModule} from "@angular/router";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
    imports: [CommonModule, StaffManagerUiSearchFormDraftModule, NzFormModule, SharedUiMbButtonModule, TranslateModule,
        SharedUiMbTableWrapModule, SharedUiMbTableModule, StaffManagerUiImportCommonModule, NzIconModule, NzButtonModule,
        NzDropDownModule, StaffManagerUiRejectCommonModule, NzTagModule,
        RouterModule.forChild([
            {
                path: '',
                component: ContractProcessDraftComponent
            }
        ]), SharedUiLoadingModule
    ],
  declarations: [ContractProcessDraftComponent],
  exports: [ContractProcessDraftComponent],
})
export class StaffManagerFeatureDraftContractProcessModule {}
