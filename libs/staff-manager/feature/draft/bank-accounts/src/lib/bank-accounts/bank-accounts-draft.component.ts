import {AfterViewInit, Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { HttpParams } from '@angular/common/http';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { ToastrService } from 'ngx-toastr';
import { saveAs } from 'file-saver';
import {SearchFormComponent} from "@hcm-mfe/staff-manager/ui/search-form-draft";
import {Constant, ConstantColor, UrlConstant, validateRejectByList} from "@hcm-mfe/staff-manager/data-access/common";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {AppFunction, CatalogModel, MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {BankAccount} from "@hcm-mfe/staff-manager/data-access/models/draft";
import {TranslateService} from "@ngx-translate/core";
import {
  BankAccountService,
  DownloadFileAttachService,
  SearchFormService
} from "@hcm-mfe/staff-manager/data-access/services";
import {ValidateService} from "@hcm-mfe/shared/core";
import { SessionService } from "@hcm-mfe/shared/common/store";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {getTypeExport} from "@hcm-mfe/shared/common/utils";

@Component({
    selector: 'app-bank-research',
    templateUrl: './bank-accounts-draft.component.html',
    styleUrls: ['./bank-accounts-draft.component.scss']
})
export class BankAccountsDraftComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('searchForm') searchForm!: SearchFormComponent;
    @ViewChild('flagStatusTmpl', { static: true }) flagStatus!: TemplateRef<NzSafeAny>;
    @ViewChild('actionTmpl', { static: true }) action!: TemplateRef<NzSafeAny>;
    @ViewChild('statusTmpl', { static: true }) status!: TemplateRef<NzSafeAny>;
    @ViewChild('selectTmpl', { static: true }) selectRecord!: TemplateRef<NzSafeAny>;
    @ViewChild('isPaymentAccountTmpl', {static: true}) isPaymentAccount!: TemplateRef<NzSafeAny>;
    @ViewChild('attachFileTmpl', {static: true}) attachFile!: TemplateRef<NzSafeAny>;

    public readonly MODULENAME = Constant.MODULE_NAME.BANK;
    functionCode: string = FunctionCode.HR_ACCOUNT;
    objFunction: AppFunction;
    tableConfig!: MBTableConfig;
    searchResult: BankAccount[] = [];

    isImportData = false;
    urlApiImport: string = UrlConstant.IMPORT_EXCEL.BANK_ACCOUNT;
    urlApiDownloadTemp: string = UrlConstant.IMPORT_FORM.BANK_ACCOUNTS_TEMPLATE;

    subs: Subscription[] = [];
    pagination = new Pagination();

    selectedCheckBoxNumber = 0;
    draftBankAccountSet = new Set<number>();

    listDraftId: number[] = Array.from(this.draftBankAccountSet);
    isRejectList = false;
    isReject = false;
    urlRejectById: string = UrlConstant.REFUSE_APPROVE_DRAFT.BANK_ACCOUNT;
    urlRejectByList: string = UrlConstant.REFUSE_APPROVE_DRAFT.BANK_ACCOUNT_LIST;
    id?: number;

    isDisabledApproveAllBtn = false;
    btnDisableColor: string = ConstantColor.COLOR.BG_PRIMARY_LIGHT;

    tagInitializeColor: string = ConstantColor.TAG.STATUS_INITIALIZE;
    tagApprovedColor: string = ConstantColor.TAG.STATUS_APPROVED;
    tagNotApprovedColor: string = ConstantColor.TAG.STATUS_NOT_APPROVED;
    disabledColor:string = ConstantColor.COLOR.DISABLED;

    mapEmployeeIsNotApproved = new Map<number, BankAccount>();

    CONSTANT_STATUS = Constant.APPROVE;
    CONSTANT_BANK_ACCOUNT = Constant.BANK_ACCOUNT;
    CONSTANT_FLAG_STATUS = Constant.FLAG_STATUS;
    isLoadingPage = false;

    statusList: CatalogModel[] = [
        {
            value: 1,
            label: this.translateService.instant('staffManager.label.workIn')
        },
        {
            value: 0,
            label: this.translateService.instant('staffManager.label.workOut')
        }
    ];

    statusApproveList: CatalogModel[] = [
        {
            value: 0,
            label: this.translateService.instant('staffManager.label.isInitial')
        },
        {
            value: 1,
            label: this.translateService.instant('staffManager.label.isApprove')
        },
        {
            value: 2,
            label: this.translateService.instant('staffManager.label.isNotApprove')
        }
    ];

    constructor(
                private router: Router,
                private translateService: TranslateService,
                private searchFormService: SearchFormService,
                public validateService: ValidateService,
                private bankAccountService: BankAccountService,
                private toastrService: ToastrService,
                public sessionService: SessionService,
                private downloadFileAttachService: DownloadFileAttachService
    ) {
        this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.HR_MODIFY_ACCOUNT}`);
    }

    ngOnInit(): void {
        this.initTable();
    }

    ngAfterViewInit() {
      this.doSearch(1);
    }

    ngOnDestroy() {
        this.subs.forEach(sub => sub.unsubscribe());
    }

    doSearch(page: number) {
        this.pagination.pageNumber = page;
        const searchParam = this.searchForm ? this.searchForm.parseOptions() : new HttpParams();
        this.isLoadingPage = true;
        this.searchFormService.getFilterResearch(UrlConstant.SEARCH_FORM_DRAFT.BANK, searchParam, this.pagination.getCurrentPage()).subscribe(res => {
            if (res.code === HTTP_STATUS_CODE.OK) {
                this.isDisabledApproveAllBtn = res.data.listData.every((draft: NzSafeAny) => draft.status === Constant.APPROVE.IS_APPROVE || draft.status === Constant.APPROVE.IS_NOT_APPROVE);
                if (this.tableConfig.pageSize) {
                  if (Math.ceil(res.data.count/ this.tableConfig.pageSize) >= 2) {
                      this.isDisabledApproveAllBtn = false;
                  }
                }
                res.data.listData
                    .filter((draft: NzSafeAny) => draft.status === Constant.APPROVE.IS_NOT_APPROVE)
                    .forEach((draft: NzSafeAny) => this.mapEmployeeIsNotApproved.set(draft.draftBankAccountId, draft));
                this.searchResult = res.data.listData;
                this.tableConfig.total = res.data.count;
            }
          this.isLoadingPage = false;
        }, () => this.isLoadingPage = false);
    }

    doImportData() {
        this.isImportData = true;
    }

    doCloseImport(event: boolean) {
      this.isImportData = false;
      if(event) {
        this.doSearch(this.pagination.pageNumber);
      }
    }

    private initTable(): void {
        this.tableConfig = {
            headers: [
                {
                    title: 'staffManager.staffResearch.bank.table.select',
                    field: 'select',
                    tdClassList: ['text-center'], thClassList: ['text-center', 'text-nowrap'],
                    tdTemplate: this.selectRecord,
                    width: 70,
                    show: this.objFunction.approve,
                    fixed: true,
                    fixedDir: 'left'
                },
                {
                    title: 'staffManager.staffResearch.bank.table.employeeCode',
                    field: 'employeeCode',
                    tdClassList: ['text-left'], thClassList: ['text-center'],
                    fixed: true,
                    fixedDir: 'left',
                    width: 120
                },
                {
                    title: 'staffManager.staffResearch.bank.table.fullName',
                    field: 'fullName',
                    tdClassList: ['text-left'], thClassList: ['text-center'],
                    width: 150
                },
                {
                    title: 'staffManager.staffResearch.bank.table.status',
                    field: 'status',
                    tdTemplate: this.status,
                    tdClassList: ['text-center'], thClassList: ['text-center'],
                    show: true,
                    width: 100
                },
                {
                    title: 'staffManager.staffResearch.bank.table.orgName',
                    field: 'orgName',
                    tdClassList: ['text-left'], thClassList: ['text-center'],
                    width: 200
                },
                {
                    title: 'staffManager.staffResearch.bank.table.accountNo',
                    field: 'accountNo',
                    tdClassList: ['text-nowrap'], thClassList: ['text-center'],
                    width: 150
                },
                {
                    title: 'staffManager.staffResearch.bank.table.bankName',
                    field: 'bankName',
                    tdClassList: ['text-nowrap'], thClassList: ['text-center'],
                    show: true,
                    width: 150
                },
                {
                    title: 'staffManager.staffResearch.bank.table.bankBranch',
                    field: 'bankBranch',
                    tdClassList: ['text-left'], thClassList: ['text-center'],
                    width: 150
                },
                {
                    title: 'staffManager.staffResearch.bank.table.isPaymentAccount',
                    field: 'isPaymentAccount',
                    tdClassList: ['text-center'], thClassList: ['text-center'],
                    tdTemplate: this.isPaymentAccount,
                    show: true,
                    width: 100
                },
                {
                    title: 'staffManager.staffResearch.allowanceHis.table.attachFile',
                    field: 'attachFileList',
                    width: 200,
                    show: true,
                    tdTemplate: this.attachFile
                },
                {
                    title: ' ',
                    field: 'action',
                    tdTemplate: this.action,
                    tdClassList: ['text-center'],
                    thClassList: ['text-center'],
                    show: this.objFunction.approve || this.objFunction.delete,
                    width: 50,
                    fixed: true,
                    fixedDir: 'right'
                }
            ],
            total: 0,
            needScroll: true,
            loading: false,
            size: 'small',
            pageSize: this.pagination.pageSize,
            pageIndex: 1
        };
    }

    approveBankAccount(draftBankAccountId: number) {
        this.isLoadingPage = true;
        this.bankAccountService.approveBankAccount(draftBankAccountId).subscribe(res => {
            if (res.code === HTTP_STATUS_CODE.OK) {
                this.doSearch(1);
                this.toastrService.success(this.translateService.instant('common.notification.isApprove'));
            }
            this.isLoadingPage = false;
        }, error => {
            this.toastrService.error(error.message);
            this.isLoadingPage = false;
        });
    }

    getFlagStatus(value: number) {
        return this.statusList.find(status => status.value === value)?.label;
    }

    getStatus(value: number) {
        return this.statusApproveList.find(status => status.value === value)?.label;
    }

    approveAll() {
        this.isLoadingPage = true;
        this.bankAccountService.approveAllBankAccount(this.searchForm.getSearchForm()).subscribe(res => {
            if (res.code === HTTP_STATUS_CODE.OK) {
                this.doSearch(1);
                this.toastrService.success(this.translateService.instant('common.notification.isApprove'));
            }
            this.isLoadingPage = false;
        }, error => {
            this.toastrService.error(error.message);
            this.isLoadingPage = false;
        });
    }

    onCheckBoxChange(event: boolean, draftPersonalBankAccountId: number) {
        if (event) {
            this.selectedCheckBoxNumber++;
            this.draftBankAccountSet.add(draftPersonalBankAccountId);
        } else {
            this.selectedCheckBoxNumber--;
            if (this.draftBankAccountSet.has(draftPersonalBankAccountId)) {
                this.draftBankAccountSet.delete(draftPersonalBankAccountId);
            }
        }
    }

    approveBankAccountList() {
        const listId = Array.from(this.draftBankAccountSet);
        this.isLoadingPage = true;
        this.bankAccountService.approveBankAccountByListId(listId).subscribe(res => {
            if (res.code === HTTP_STATUS_CODE.OK) {
                this.doSearch(1);
                this.toastrService.success(this.translateService.instant('common.notification.isApprove'));
            }
            this.isLoadingPage = false;
        }, error => {
            this.toastrService.error(error.message);
            this.isLoadingPage = false;
        });
        this.draftBankAccountSet.clear();
        this.selectedCheckBoxNumber = 0;
    }

    confirm(draftBankAccountId: number) {
        this.isLoadingPage = true;
        this.bankAccountService.deleteDraftBankAccountById(draftBankAccountId).subscribe(res => {
            if (res.code === HTTP_STATUS_CODE.OK) {
                this.doSearch(1);
                this.toastrService.success(this.translateService.instant('common.notification.deleteSuccess'));
            }
            this.isLoadingPage = false;
        }, error => {
            this.toastrService.error(error.message);
            this.isLoadingPage = false;
        });
    }

    doCloseReject(){
        this.isReject = false;
        this.doSearch(1);
    }

    resetListDraftId() {
        this.listDraftId = [];
    }

    openRejectById(draftBankAccountId: number) {
        this.isReject = true;
        this.id = draftBankAccountId;
    }

    openRejectByList() {
        if (validateRejectByList(this.draftBankAccountSet, this.mapEmployeeIsNotApproved, this.translateService, this.toastrService)) {
            this.isReject = true;
            this.isRejectList = true;
        }
    }

    doDownloadAttach(file: NzSafeAny) {
      this.isLoadingPage = true;
      this.downloadFileAttachService.doDownloadAttachFileWithSecurity(~~file?.docId, file?.security).subscribe(res => {
        const reportFile = new Blob([res.body], { type: getTypeExport(file.fileName.split('.').pop()) });
        saveAs(reportFile, file.fileName);
        this.isLoadingPage = false;
      }, error => {
        this.toastrService.error(error.message);
        this.isLoadingPage = false;
      });
    }

    onLoadPage(event: boolean) {
      this.isLoadingPage = event;
    }

}
