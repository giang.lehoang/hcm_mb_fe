import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BankAccountsDraftComponent} from "./bank-accounts/bank-accounts-draft.component";
import {StaffManagerUiSearchFormDraftModule} from "@hcm-mfe/staff-manager/ui/search-form-draft";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {StaffManagerUiImportCommonModule} from "@hcm-mfe/staff-manager/ui/import-common";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {NzTagModule} from "ng-zorro-antd/tag";
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";
import {FormsModule} from "@angular/forms";
import {StaffManagerUiRejectCommonModule} from "@hcm-mfe/staff-manager/ui/reject-common";
import {RouterModule} from "@angular/router";
import {NzGridModule} from "ng-zorro-antd/grid";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
    imports: [CommonModule, StaffManagerUiSearchFormDraftModule, SharedUiMbButtonModule, TranslateModule, SharedUiMbTableWrapModule,
        SharedUiMbTableModule, StaffManagerUiImportCommonModule, NzIconModule, NzButtonModule, NzDropDownModule, NzTagModule,
        NzCheckboxModule, FormsModule, StaffManagerUiRejectCommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: BankAccountsDraftComponent
            }
        ]), NzGridModule, SharedUiLoadingModule
    ],
  declarations: [BankAccountsDraftComponent],
  exports: [BankAccountsDraftComponent],
})
export class StaffManagerFeatureDraftBankAccountsModule {}
