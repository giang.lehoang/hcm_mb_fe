import {AfterViewInit, Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { HttpParams } from '@angular/common/http';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { ToastrService } from 'ngx-toastr';
import { FormGroup } from '@angular/forms';
import { saveAs } from 'file-saver';

import {Constant, ConstantColor, UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {AppFunction, CatalogModel, MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {DraftObject, PersonalInformation} from "@hcm-mfe/staff-manager/data-access/models/draft";
import {SearchFormComponent} from "@hcm-mfe/staff-manager/ui/search-form-draft";
import {
  BookmarkFormService,
  CommonDraftService,
  DownloadFileAttachService,
  SearchFormService
} from "@hcm-mfe/staff-manager/data-access/services";
import {ValidateService} from "@hcm-mfe/shared/core";
import { SessionService } from "@hcm-mfe/shared/common/store";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {getTypeExport} from "@hcm-mfe/shared/common/utils";

@Component({
  selector: 'app-salary-his-research',
  templateUrl: './salary-process-draft.component.html',
  styleUrls: ['./salary-process-draft.component.scss']
})
export class SalaryProcessDraftComponent implements OnInit, AfterViewInit, OnDestroy {

  public readonly MODULENAME = Constant.MODULE_NAME.SALARY;
  functionCode: string = FunctionCode.HR_SALARY_PROCESS;
  objFunction: AppFunction;
  tableConfig!: MBTableConfig;
  searchResult: PersonalInformation[] = [];

  isImportData = false;
  urlApiImport: string = UrlConstant.IMPORT_FORM.DRAFT_SALARY_PROCESS_FORM;
  urlApiDownloadTemp: string = UrlConstant.IMPORT_FORM.SALARY_PROCESS_TEMPLATE;
  listDraftId:number[] = [];
  listDraftObject: DraftObject[] = [];
  isRejectList = false;
  isReject = false;
  urlRejectById: string = UrlConstant.REFUSE_APPROVE_DRAFT.SALARY_PROCESS;
  urlRejectByList: string = UrlConstant.REJECT_LIST_DRAFT.SALARY_PROCESS;
  id?:number;
  searchFormApprove?: FormGroup;
  isDisabled = false;
  mbButtonBgColor:string = ConstantColor.COLOR.BG_PRIMARY_LIGHT;
  requiredAttachFile = true;

  tagInitializeColor: string = ConstantColor.TAG.STATUS_INITIALIZE;
  tagApprovedColor: string = ConstantColor.TAG.STATUS_APPROVED;
  tagNotApprovedColor: string = ConstantColor.TAG.STATUS_NOT_APPROVED;
  disabledColor:string = ConstantColor.COLOR.DISABLED;
  CONSTANT_STATUS = Constant.APPROVE;
  isLoadingPage = false;
  statusList: CatalogModel[] = [
    {
      value: 1,
      label: this.translateService.instant('staffManager.label.workIn')
    },
    {
      value: 0,
      label: this.translateService.instant('staffManager.label.workOut')
    }
  ];

  statusApproveList: CatalogModel[] = [
    {
      value: 0,
      label: this.translateService.instant('staffManager.label.isInitial')
    },
    {
      value: 1,
      label: this.translateService.instant('staffManager.label.isApprove')
    },
    {
      value: 2,
      label: this.translateService.instant('staffManager.label.isNotApprove')
    }
  ];

  subs: Subscription[] = [];
  pagination = new Pagination();

  @ViewChild('searchForm') searchForm!: SearchFormComponent;
  @ViewChild('flagStatusTmpl', {static: true}) flagStatus!: TemplateRef<NzSafeAny>;
  @ViewChild('actionTmpl', {static: true}) action!: TemplateRef<NzSafeAny>;
  @ViewChild('statusTmpl', {static: true}) status!: TemplateRef<NzSafeAny>;
  @ViewChild('selectTmpl', {static: true}) select!: TemplateRef<NzSafeAny>;
  @ViewChild('attachFileTmpl', {static: true}) attachFile!: TemplateRef<NzSafeAny>;

  constructor(private bookmarkFormService: BookmarkFormService,
              private router: Router,
              private translateService: TranslateService,
              private searchFormService: SearchFormService,
              private commonDraftService: CommonDraftService,
              private toastrService: ToastrService,
              public validateService: ValidateService,
              public sessionService: SessionService,
              private downloadFileAttachService: DownloadFileAttachService
  ) {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.HR_MODIFY_SALARY_PROCESS}`);
  }


  ngOnInit(): void {
    this.initTable();
  }

  ngAfterViewInit() {
    this.doSearch(1);
  }

  ngOnDestroy() {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  doSearch(pageIndex:number) {
    this.pagination.pageNumber = pageIndex;
    this.isLoadingPage = true;
    const searchParam = this.searchForm? this.searchForm.parseOptions() : new HttpParams();

    this.searchFormService.getFilterResearch(UrlConstant.SEARCH_FORM.DRAFT_SALARY_PROCESS,searchParam, this.pagination.getCurrentPage()).subscribe(res => {
      if (res.code === HTTP_STATUS_CODE.OK) {
          this.isDisabled = res.data.listData.every((draft: NzSafeAny) => draft.status === Constant.APPROVE.IS_APPROVE || draft.status === Constant.APPROVE.IS_NOT_APPROVE);
          if (this.tableConfig.pageSize) {
            if (Math.ceil(res.data.count/ this.tableConfig.pageSize) >= 2) {
                this.isDisabled = false;
            }
          }
        this.searchResult = res.data.listData;
        this.tableConfig.pageIndex = pageIndex;
        this.tableConfig.total = res.data.count;
        this.searchFormApprove = this.searchForm.form;
      }
      this.isLoadingPage = false;
    }, () => this.isLoadingPage = false);
  }

  doImportData() {
    this.isImportData = true;
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'staffManager.staffResearch.degree.table.select',
          field: 'select',
          width: 60,
          tdTemplate: this.select,
          thClassList: ['text-nowrap','text-center'],
          tdClassList: ['text-nowrap','text-center'],
          show: this.objFunction.approve,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left'
        },
        {
          title: 'staffManager.staffResearch.salary.table.employeeCode',
          field: 'employeeCode', width: 130,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left'
        },
        {
          title: 'staffManager.staffResearch.salary.table.fullName',
          field: 'fullName', width: 150
        },
        {
          title: 'staffManager.staffResearch.allowanceHis.table.status',
          field: 'status',
          width: 100,
          tdTemplate: this.status,
          tdClassList: ['text-nowrap','text-center'],
          thClassList: ['text-nowrap','text-center'],
          show: true
        },
        {
          title: 'staffManager.staffResearch.salary.table.orgName',
          field: 'orgName',
          width: 250
        },
        {
          title: 'staffManager.table.percentReceive',
          field: 'salaryPercent',
          width: 130,
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        },
        {
          title: 'staffManager.table.performanceFactor',
          field: 'performanceFactor',
          width: 130,
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        },
        {
          title: 'staffManager.table.ratio',
          field: 'performancePercent',
          width: 130,
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        },
        {
          title: 'staffManager.staffResearch.salary.table.salaryRankName',
          field: 'salaryRankName',
          width: 200,
          tdClassList: ['text-nowrap'], thClassList: ['text-nowrap'],
          show: false
        },
        {
          title: 'staffManager.staffResearch.salary.table.fromDate',
          field: 'fromDate',
          width: 100,
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        },
        {
          title: 'staffManager.staffResearch.salary.table.toDate',
          field: 'toDate',
          width: 100,
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        }, {
          title: 'staffManager.staffResearch.salary.table.salaryGradeName',
          field: 'salaryGradeName',
          width: 200,
          needEllipsis: true,
          tdClassList: ['text-nowrap'], thClassList: ['text-nowrap']
        },
        {
          title: 'staffManager.staffResearch.allowanceHis.table.attachFile',
          field: 'attachFileList',
          width: 200,
          show: true,
          tdTemplate: this.attachFile
        },
        {
          title: ' ',
          field: 'action',
          tdClassList: ['text-nowrap','text-center'], thClassList: ['text-nowrap','text-center'],
          width: 50,
          show: this.objFunction.approve || this.objFunction.delete,
          tdTemplate: this.action,
          fixed: window.innerWidth > 1024,
          fixedDir: 'right'
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  doCloseImport(event: boolean) {
    this.isImportData = false;
    if(event) {
      this.doSearch(this.pagination.pageNumber);
    }
  }

  getStatus(value: number) {
    return this.statusApproveList.find(status => status.value === value)?.label;
  }

  openRejectById(id:number,status:number) {
    if(status !== Constant.APPROVE.IS_NOT_APPROVE) {
      this.isReject = true;
      this.id = id;
    }
  }
  openRejectByList() {
    const listRejected:string[] = [];
    let error = false;
    this.listDraftObject.forEach((data:DraftObject) => {
      if(data.status === Constant.APPROVE.IS_NOT_APPROVE) {
        listRejected.push(data.employeeCode);
        error = true;
      }
    })
    if(error){
      this.toastrService.error(this.translateService.instant('common.notification.employeeCode') + " {" + listRejected.toString() + "} " + this.translateService.instant('common.notification.rejected'));
    } else {
      this.isReject = true;
      this.isRejectList = true;
    }
  }

  doCloseReject(){
    this.isReject = false;
    this.doSearch(this.pagination.pageNumber);
  }
  resetListDraftId() {
    this.listDraftId = [];
  }

  approveById(id:number) {
    this.isLoadingPage = true;
    this.commonDraftService.approveById(id, UrlConstant.APPROVE_DRAFT.SALARY_PROCESS).subscribe(res => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.doSearch(this.pagination.pageNumber);
        this.toastrService.success(this.translateService.instant('common.notification.isApprove'));
      }
      this.isLoadingPage = false;
    }, error => {
      this.isLoadingPage = false;
      this.toastrService.error(error.message);
    })
  }

  approveByList() {
    this.isLoadingPage = true;
    this.commonDraftService.approveByList(this.listDraftId, UrlConstant.APPROVE_LIST_DRAFT.SALARY_PROCESS).subscribe(res => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.doSearch(this.pagination.pageNumber);
        this.toastrService.success(this.translateService.instant('common.notification.isApprove'));
        this.listDraftId = [];
      }
      this.isLoadingPage = false;
    }, error => {
      this.isLoadingPage = false;
      this.toastrService.error(error.message);
    })
  }

  approveAll() {
    this.isLoadingPage = true;
    this.commonDraftService.approveAll(UrlConstant.APPROVE_ALL_DRAFT.SALARY_PROCESS, this.searchFormApprove).subscribe(res => {
      if(res.code === HTTP_STATUS_CODE.OK) {
        this.doSearch(this.pagination.pageNumber);
        this.toastrService.success(this.translateService.instant('common.notification.isApprove'));
      }
      this.isLoadingPage = false;
    }, error => {
      this.isLoadingPage = false;
      this.toastrService.error(error.message);
    })
  }

  deleteDraft(id:number) {
    this.isLoadingPage = true;
    this.commonDraftService.deleteDraft(id,UrlConstant.DELETE_DRAFT.SALARY_PROCESS).subscribe(res => {
      if(res.code === HTTP_STATUS_CODE.OK) {
        this.doSearch(1);
        this.toastrService.success(this.translateService.instant('common.notification.deleteSuccess'));
      }
      this.isLoadingPage = false;
    }, error => {
      this.toastrService.error(error.message);
      this.isLoadingPage = false;
    })
  }

  onCheckChange(event: NzSafeAny, status:number, employeeCode:string){
    const id = parseInt(event.target.value);
    const data:DraftObject = {
      id: id,
      status: status,
      employeeCode: employeeCode
    }
    if(event.target.checked) {
      this.listDraftId.push(id);
      this.listDraftObject.push(data);
    } else {
      this.listDraftId.splice(this.listDraftId.indexOf(id),1);
      this.listDraftObject.forEach((value,index) => {
        if(value.id == id) {
          this.listDraftObject.splice(index,1);
        }
      })
    }
  }

  doDownloadAttach(file: NzSafeAny) {
    this.isLoadingPage = true;
    this.downloadFileAttachService.doDownloadAttachFileWithSecurity(~~file?.docId, file?.security).subscribe(res => {
      const reportFile = new Blob([res.body], { type: getTypeExport(file.fileName.split('.').pop()) });
      saveAs(reportFile, file.fileName);
      this.isLoadingPage = false;
    }, error => {
      this.toastrService.error(error.message);
      this.isLoadingPage = false;
    });
  }

  onLoadPage(event: boolean) {
    this.isLoadingPage = event;
  }

}
