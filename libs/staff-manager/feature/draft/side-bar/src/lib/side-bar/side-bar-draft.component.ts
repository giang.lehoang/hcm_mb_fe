import {Component, OnInit} from '@angular/core';
import {NavigationEnd, Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {MenusDraft} from "@hcm-mfe/staff-manager/data-access/models/draft";
import { SessionService } from "@hcm-mfe/shared/common/store";
import {CommonDraftService, ShareDataService} from "@hcm-mfe/staff-manager/data-access/services";
import {UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {environment} from "@hcm-mfe/shared/environment";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";

@Component({
  selector: 'app-side-bar-draft',
  templateUrl: './side-bar-draft.component.html',
  styleUrls: ['./side-bar-draft.component.scss']
})
export class SideBarDraftComponent implements OnInit {
  currentUrl?: string;
  menus: MenusDraft[] = [];
  openMenuMap: { [id: string]: boolean } = {};
  selectedMenu?: MenusDraft;

  constructor(
    private route: Router,
    private http: HttpClient,
    public sessionService: SessionService,
    private commonDraftService: CommonDraftService,
    private shareDataService: ShareDataService
  ) {
    this.getCurrentUrl(this.route);
    shareDataService.changeEmitted$.subscribe(loadMenu => {
      if(loadMenu) {
        this.getMenu().then();
      }
    })
  }

  ngOnInit(): void {
    this.getMenu().then();
  }

  async getMenu() {
    const res = await this.http.get<MenusDraft[]>('./assets/fake-data/menu_side_bar_draft.json').toPromise();
    res?.forEach(item => {
      if (item.childrens && item.childrens.length > 0) {
        item.childrens = item.childrens.filter(element => this.checkScopeView(element?.code));
      }
    });

    this.menus = res ?? [];
    this.setOpenMenuMap(this.menus);

    for (const item of this.menus) {
      const groupNumberBadge = await this.commonDraftService.getNumberBadge(item.type, UrlConstant.CATEGORY.GROUP_NUMBER_BADGE).toPromise();
      if(groupNumberBadge.code === HTTP_STATUS_CODE.OK) {
        item.numberBadge = groupNumberBadge.data;
      }
    }
    for (const item of this.menus) {
      if (item.childrens) {
        for (const child of item.childrens) {
          if(child.type) {
            const response = await this.commonDraftService.getNumberBadge(child.type, UrlConstant.CATEGORY.NUMBER_BADGE).toPromise();
            if(response.code === HTTP_STATUS_CODE.OK) {
              child.numberBadge = response.data;
            }
          }
        }
      }
    }
  }

  checkScopeView(resource: string): boolean {
    if(environment.isMbBank){
      return this.sessionService.getSessionData(`FUNCTION_${FunctionCode[resource as keyof typeof FunctionCode]}`)?.view;
    } else {
      return true;
    }
  }

  setOpenMenuMap(menus: MenusDraft[]) {
    return menus.some((menu: any) => {
      this.openMenuMap[menu.id] = false;
      if (menu.childrens && menu.childrens.length > 0) {
        this.openMenuMap[menu.id] = this.setOpenMenuMap(menu.childrens);
      }
      if (menu.url === this.currentUrl) {
        this.openMenuMap[menu.id] = true;
        return true;
      }
      return;
    });
  }

  getCurrentUrl(router: Router) {
    router.events.subscribe(e => {
      if (e instanceof NavigationEnd) {
        this.currentUrl = e.url;
      }
    });
  }

  openHandler(value: number): void {
    for (const key in this.openMenuMap) {
      if (key !== value.toString()) {
        this.openMenuMap[key] = false;
      }
    }
    this.setParentMenuOpen(value, this.menus);
  }

  setParentMenuOpen(value: number, menus: MenusDraft[]) {
    return menus.some((menu: any) => {
      this.openMenuMap[menu.id] = false;
      if (menu.childrens && menu.childrens.length > 0) {
        this.openMenuMap[menu.id] = this.setParentMenuOpen(value, menu.childrens);
      }
      if (menu.id === value) {
        this.openMenuMap[menu.id] = true;
        return true;
      }
      return;
    });
  }

}
