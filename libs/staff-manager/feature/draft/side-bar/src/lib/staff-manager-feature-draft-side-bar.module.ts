import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SideBarDraftComponent} from "./side-bar/side-bar-draft.component";
import {NzMenuModule} from "ng-zorro-antd/menu";
import {RouterModule} from "@angular/router";
import {NzBadgeModule} from "ng-zorro-antd/badge";

@NgModule({
  imports: [CommonModule, NzMenuModule, RouterModule, NzBadgeModule],
  declarations: [SideBarDraftComponent],
  exports: [SideBarDraftComponent],
})
export class StaffManagerFeatureDraftSideBarModule {}
