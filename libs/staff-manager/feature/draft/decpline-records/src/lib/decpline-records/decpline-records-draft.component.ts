import {Component, OnInit, ViewChild, TemplateRef, AfterViewInit, OnDestroy} from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { Subscription } from 'rxjs';
import * as FileSaver from 'file-saver';
import { HttpParams } from '@angular/common/http';
import {ToastrService} from 'ngx-toastr';
import {FormGroup} from '@angular/forms';
import { saveAs } from 'file-saver';

import {Constant, ConstantColor, UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {AppFunction, CatalogModel, MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {DraftObject, PersonalInformation} from "@hcm-mfe/staff-manager/data-access/models/draft";
import {SearchFormComponent} from "@hcm-mfe/staff-manager/ui/search-form-draft";
import {
  BookmarkFormService,
  CommonDraftService,
  DownloadFileAttachService, ExportFileService,
  SearchFormService
} from "@hcm-mfe/staff-manager/data-access/services";
import {ValidateService} from "@hcm-mfe/shared/core";
import { SessionService } from "@hcm-mfe/shared/common/store";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {getTypeExport} from "@hcm-mfe/shared/common/utils";

@Component({
  selector: 'app-decpline-research',
  templateUrl: './decpline-records-draft.component.html',
  styleUrls: ['./decpline-records-draft.component.scss']
})
export class DecplineRecordsDraftComponent implements OnInit, AfterViewInit, OnDestroy {

  public readonly MODULENAME = Constant.MODULE_NAME.DECPLINE;
  functionCode: string = FunctionCode.HR_DECPLINE;
  objFunction: AppFunction;
  tableConfig!: MBTableConfig;
  searchResult: PersonalInformation[] = [];

  isImportData = false;
  urlApiImport: string = UrlConstant.IMPORT_FORM.DRAFT_DECPLINE_FORM;
  urlApiDownloadTemp: string = UrlConstant.IMPORT_FORM.DECPLINE_TEMPLATE;
  urlRejectById:string = UrlConstant.REFUSE_APPROVE_DRAFT.DECPLINE_RECORD;
  urlRejectByList = UrlConstant.REJECT_LIST_DRAFT.DECPLINE_RECORD;
  listDraftId:number[] = [];
  listDraftObject: DraftObject[] = [];
  isRejectList = false;
  isReject = false;
  id?:number;
  searchFormApprove?: FormGroup;
  isDisabled = false;
  mbButtonBgColor:string = ConstantColor.COLOR.BG_PRIMARY_LIGHT;
  requiredAttachFile = true;

  tagInitializeColor: string = ConstantColor.TAG.STATUS_INITIALIZE;
  tagApprovedColor: string = ConstantColor.TAG.STATUS_APPROVED;
  tagNotApprovedColor: string = ConstantColor.TAG.STATUS_NOT_APPROVED;
  disabledColor:string = ConstantColor.COLOR.DISABLED;
  CONSTANT_STATUS = Constant.APPROVE;
  isLoadingPage = false;
  statusList: CatalogModel[] = [
    {
      value: 1,
      label: this.translateService.instant('staffManager.label.workIn')
    },
    {
      value: 0,
      label: this.translateService.instant('staffManager.label.workOut')
    }
  ];

  statusApproveList: CatalogModel[] = [
    {
      value: 0,
      label: this.translateService.instant('staffManager.label.isInitial')
    },
    {
      value: 1,
      label: this.translateService.instant('staffManager.label.isApprove')
    },
    {
      value: 2,
      label: this.translateService.instant('staffManager.label.isNotApprove')
    }
  ];
  subs: Subscription[] = [];
  pagination = new Pagination();

  @ViewChild('searchForm') searchForm!: SearchFormComponent;
  @ViewChild('flagStatusTmpl', {static: true}) flagStatus!: TemplateRef<NzSafeAny>;
  @ViewChild('actionTmpl', {static: true}) action!: TemplateRef<NzSafeAny>;
  @ViewChild('statusTmpl', {static: true}) status!: TemplateRef<NzSafeAny>;
  @ViewChild('selectTmpl', {static: true}) select!: TemplateRef<NzSafeAny>;
  @ViewChild('attachFileTmpl', {static: true}) attachFile!: TemplateRef<NzSafeAny>;

  constructor(private bookmarkFormService: BookmarkFormService,
              private exportFileService: ExportFileService,
              private router: Router,
              private translateService: TranslateService,
              private searchFormService: SearchFormService,
              private commonDraftService: CommonDraftService,
              private toastrService: ToastrService,
              public validateService: ValidateService,
              public sessionService: SessionService,
              private downloadFileAttachService: DownloadFileAttachService
  ) {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.HR_MODIFY_DECIPLINE}`);
  }

  ngOnInit(): void {
    this.initTable();
  }

  ngAfterViewInit() {
    this.doSearch(1);
  }

  ngOnDestroy() {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  doImportData() {
    this.isImportData = true;
  }

  openRejectById(id:number,status:number) {
    if(status !== Constant.APPROVE.IS_NOT_APPROVE) {
      this.isReject = true;
      this.id = id;
    }
  }

  openRejectByList() {
    const listRejected:string[] = [];
    let error = false;
    this.listDraftObject.forEach((data:DraftObject) => {
      if(data.status === Constant.APPROVE.IS_NOT_APPROVE) {
        listRejected.push(data.employeeCode);
        error = true;
      }
    })
    if(error){
      this.toastrService.error(this.translateService.instant('common.notification.employeeCode') + " {" + listRejected.toString() + "} " + this.translateService.instant('common.notification.rejected'));
    } else {
      this.isReject = true;
      this.isRejectList = true;
    }
  }

  doSearch(pageIndex: number) {
    this.pagination.pageNumber = pageIndex;
    this.isLoadingPage = true;
    const searchParam = this.searchForm? this.searchForm.parseOptions() : new HttpParams();
    this.searchFormService.getFilterResearch(UrlConstant.SEARCH_FORM.DRAFT_DECPLINE,searchParam, this.pagination.getCurrentPage()).subscribe(res => {
      if (res.code === HTTP_STATUS_CODE.OK) {
          this.isDisabled = res.data.listData.every((draft: NzSafeAny) => draft.status === Constant.APPROVE.IS_APPROVE || draft.status === Constant.APPROVE.IS_NOT_APPROVE);
          if (this.tableConfig.pageSize) {
            if (Math.ceil(res.data.count/ this.tableConfig.pageSize) >= 2) {
                this.isDisabled = false;
            }
          }
        this.searchResult = res.data.listData;
        this.tableConfig.total = res.data.count;
        this.tableConfig.pageIndex = pageIndex;
        this.searchFormApprove = this.searchForm.form;
      }
      this.isLoadingPage = false;
    }, () => this.isLoadingPage = false);
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'staffManager.staffResearch.degree.table.select',
          field: 'select',
          width: 60,
          tdTemplate: this.select,
          thClassList: ['text-nowrap','text-center'],
          tdClassList: ['text-nowrap','text-center'],
          show: this.objFunction.approve,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left'
        },
        {
          title: 'staffManager.staffResearch.decpline.table.employeeCode',
          field: 'employeeCode', width: 130,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left'
        },
        {
          title: 'staffManager.staffResearch.decpline.table.fullName',
          field: 'fullName', width: 150,
        },
        {
          title: 'staffManager.staffResearch.allowanceHis.table.status',
          field: 'status',
          width: 100,
          tdClassList: ['text-nowrap','text-center'],
          thClassList: ['text-nowrap','text-center'],
          tdTemplate: this.status,
          show: true
        },
        {
          title: 'staffManager.staffResearch.decpline.table.orgName',
          field: 'orgName',
          width: 250
        },
        {
          title: 'staffManager.staffResearch.decpline.table.signedDate',
          field: 'signedDate',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 100
        },
        {
          title: 'staffManager.staffResearch.decpline.table.effectiveDate',
          field: 'fromDate',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 100
        },
        {
          title: 'staffManager.staffResearch.decpline.table.expirationDate',
          field: 'toDate',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 100
        },
        {
          title: 'staffManager.staffResearch.decpline.table.disciplineMethodName',
          field: 'disciplineMethodName',
          width: 200
        },
        {
          title: 'staffManager.staffResearch.decpline.table.caseName',
          field: 'caseName',
          width: 200
        },
        {
          title: 'staffManager.staffResearch.decpline.table.reason',
          field: 'reason',
          width: 200,
          show: false
        },
        {
          title: 'staffManager.staffResearch.decpline.table.disciplineLevelName',
          field: 'disciplineLevelName',
          width: 180
        },
        {
          title: 'staffManager.staffResearch.decpline.table.amount',
          field: 'amount',
          width: 150,
          pipe: 'currency: VND',
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        },
        {
          title: 'staffManager.staffResearch.decpline.table.amountRemedied',
          field: 'amountRemedied',
          width: 150,
          pipe: 'currency: VND',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          show: false
        },
        {
          title: 'staffManager.staffResearch.decpline.table.signer',
          field: 'signer',
          width: 150,
          show: false
        },
        {
          title: 'staffManager.staffResearch.decpline.table.signerPosition',
          field: 'signerPosition',
          width: 150,
          show: false
        },
        {
          title: 'staffManager.staffResearch.allowanceHis.table.attachFile',
          field: 'attachFileList',
          width: 150,
          show: true,
          tdTemplate: this.attachFile
        },
        {
          title: ' ',
          width: 50,
          show: this.objFunction.approve || this.objFunction.delete,
          tdTemplate: this.action,
          fixed: window.innerWidth > 1024,
          fixedDir: 'right'
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  getFlagStatus(value: number) {
    return this.statusList.find(status => status.value === value)?.label;
  }

  getStatus(value: number) {
    return this.statusApproveList.find(status => status.value === value)?.label;
  }

  doExport() {
    this.exportFileService.doExport(UrlConstant.EXPORT_REPORT.DECPLINE).subscribe((res) => {
      const blob = new Blob([res.body], { type: 'application/vnd.ms.excel' });
      FileSaver.saveAs(blob, 'ThongTinKyLuat.xlsx');
    })
  }

  doCloseImport(event: boolean) {
    this.isImportData = false;
    if(event) {
      this.doSearch(this.pagination.pageNumber);
    }
  }

  doCloseReject(){
    this.isReject = false;
    this.doSearch(this.pagination.pageNumber);
  }
  resetListDraftId() {
    this.listDraftId = [];
  }

  approveById(id:number) {
    this.isLoadingPage = true;
    this.commonDraftService.approveById(id, UrlConstant.APPROVE_DRAFT.DECPLINE_RECORD).subscribe(res => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.doSearch(this.pagination.pageNumber);
        this.toastrService.success(this.translateService.instant('common.notification.isApprove'));
      }
      this.isLoadingPage = false;
    }, error => {
      this.toastrService.error(error.message);
      this.isLoadingPage = false;
    })
  }

  approveByList() {
    this.isLoadingPage = true;
    this.commonDraftService.approveByList(this.listDraftId, UrlConstant.APPROVE_LIST_DRAFT.DECPLINE_RECORD).subscribe(res => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.doSearch(this.pagination.pageNumber);
        this.toastrService.success(this.translateService.instant('common.notification.isApprove'));
        this.listDraftId = [];
      }
      this.isLoadingPage = false;
    }, error => {
      this.toastrService.error(error.message);
      this.isLoadingPage = false;
    })
  }

  approveAll() {
    this.isLoadingPage = true;
    this.commonDraftService.approveAll(UrlConstant.APPROVE_ALL_DRAFT.DECPLINE_RECORD, this.searchFormApprove).subscribe(res => {
      if(res.code === HTTP_STATUS_CODE.OK) {
        this.doSearch(this.pagination.pageNumber);
        this.toastrService.success(this.translateService.instant('common.notification.isApprove'));
      }
      this.isLoadingPage = false;
    }, error => {
      this.toastrService.error(error.message);
      this.isLoadingPage = false;
    })
  }

  deleteDraft(id:number) {
    this.isLoadingPage = true;
    this.commonDraftService.deleteDraft(id,UrlConstant.DELETE_DRAFT.DECPLINE_RECORD).subscribe(res => {
      if(res.code === HTTP_STATUS_CODE.OK) {
        this.doSearch(1);
        this.toastrService.success(this.translateService.instant('common.notification.deleteSuccess'));
      }
      this.isLoadingPage = false;
    }, error => {
      this.toastrService.error(error.message);
      this.isLoadingPage = false;
    })
  }

  onCheckChange(event: NzSafeAny,status:number,employeeCode:string){
    const id = parseInt(event.target.value);
    const data:DraftObject = {
      id: id,
      status: status,
      employeeCode: employeeCode
    }
    if(event.target.checked) {
      this.listDraftId.push(id);
      this.listDraftObject.push(data);
    } else {
      this.listDraftId.splice(this.listDraftId.indexOf(id),1);
      this.listDraftObject.forEach((value,index) => {
        if(value.id == id) {
          this.listDraftObject.splice(index,1);
        }
      })
    }
  }

  doDownloadAttach(file: NzSafeAny) {
    this.isLoadingPage = true;
    this.downloadFileAttachService.doDownloadAttachFileWithSecurity(~~file?.docId, file?.security).subscribe(res => {
      const reportFile = new Blob([res.body], { type: getTypeExport(file.fileName.split('.').pop()) });
      saveAs(reportFile, file.fileName);
      this.isLoadingPage = false;
    }, error => {
      this.toastrService.error(error.message);
      this.isLoadingPage = false;
    });
  }

  onLoadPage(event: boolean) {
    this.isLoadingPage = event;
  }

}
