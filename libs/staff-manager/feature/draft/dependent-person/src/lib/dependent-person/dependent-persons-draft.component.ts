import {AfterViewInit, Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { HttpParams } from '@angular/common/http';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { ToastrService } from 'ngx-toastr';
import { saveAs } from 'file-saver';

import {Constant, ConstantColor, UrlConstant, validateRejectByList} from "@hcm-mfe/staff-manager/data-access/common";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {AppFunction, CatalogModel, MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {SearchFormComponent} from "@hcm-mfe/staff-manager/ui/search-form-draft";
import {
  DependentPersonService,
  DownloadFileAttachService,
  SearchFormService
} from "@hcm-mfe/staff-manager/data-access/services";
import {ValidateService} from "@hcm-mfe/shared/core";
import { SessionService } from "@hcm-mfe/shared/common/store";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {getTypeExport} from "@hcm-mfe/shared/common/utils";
import {DependentPerson} from "@hcm-mfe/staff-manager/data-access/models/draft";

@Component({
    selector: 'app-dependent-person-draft-research',
    templateUrl: './dependent-persons-draft.component.html',
    styleUrls: ['./dependent-persons-draft.component.scss']
})
export class DependentPersonsDraftComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('searchForm') searchForm!: SearchFormComponent;
    @ViewChild('flagStatusTmpl', { static: true }) flagStatus!: TemplateRef<NzSafeAny>;
    @ViewChild('actionTmpl', { static: true }) action!: TemplateRef<NzSafeAny>;
    @ViewChild('statusTmpl', { static: true }) status!: TemplateRef<NzSafeAny>;
    @ViewChild('selectTmpl', { static: true }) selectRecord!: TemplateRef<NzSafeAny>;
    @ViewChild('attachFileTmpl', {static: true}) attachFile!: TemplateRef<NzSafeAny>;

    public readonly MODULENAME = Constant.MODULE_NAME.FAMILY_DEDUCTIONS;
    functionCode: string = FunctionCode.HR_DEPENDENT_PERSON;
    objFunction: AppFunction;
    tableConfig!: MBTableConfig;
    searchResult: DependentPerson[] = [];

    isImportData = false;
    urlApiImport: string = UrlConstant.IMPORT_EXCEL.DEPENDENT_PERSON;
    urlApiDownloadTemp: string = UrlConstant.IMPORT_FORM.DEPENDENT_PERSON_TEMPLATE;

    subs: Subscription[] = [];
    pagination = new Pagination();

    selectedCheckBoxNumber = 0;
    draftDependentPersonSet = new Set<number>();

    listDraftId: number[] = Array.from(this.draftDependentPersonSet);
    isRejectList = false;
    isReject = false;
    urlRejectById: string = UrlConstant.REFUSE_APPROVE_DRAFT.DEPENDENT_PERSON;
    urlRejectByList: string = UrlConstant.REFUSE_APPROVE_DRAFT.DEPENDENT_PERSON_LIST;
    id?: number;

    isDisabledApproveAllBtn = false;
    btnDisableColor: string = ConstantColor.COLOR.BG_PRIMARY_LIGHT;

    tagInitializeColor: string = ConstantColor.TAG.STATUS_INITIALIZE;
    tagApprovedColor: string = ConstantColor.TAG.STATUS_APPROVED;
    tagNotApprovedColor: string = ConstantColor.TAG.STATUS_NOT_APPROVED;
    disabledColor:string = ConstantColor.COLOR.DISABLED;

    mapEmployeeIsNotApproved = new Map<number, DependentPerson>();

    CONSTANT_STATUS = Constant.APPROVE;
    CONSTANT_FLAG_STATUS = Constant.FLAG_STATUS;
    isLoadingPage = false;

    statusList: CatalogModel[] = [
        {
            value: 1,
            label: this.translateService.instant('staffManager.label.workIn')
        },
        {
            value: 0,
            label: this.translateService.instant('staffManager.label.workOut')
        }
    ];

    statusApproveList: CatalogModel[] = [
        {
            value: 0,
            label: this.translateService.instant('staffManager.label.isInitial')
        },
        {
            value: 1,
            label: this.translateService.instant('staffManager.label.isApprove')
        },
        {
            value: 2,
            label: this.translateService.instant('staffManager.label.isNotApprove')
        }
    ];

    constructor(
                private router: Router,
                private translateService: TranslateService,
                private searchFormService: SearchFormService,
                public validateService: ValidateService,
                private dependentPersonService: DependentPersonService,
                private toastrService: ToastrService,
                public sessionService: SessionService,
                private downloadFileAttachService: DownloadFileAttachService
    ) {
        this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.HR_MODIFY_DEPENDENT}`);
    }

    ngOnInit(): void {
        this.initTable();
    }

    ngAfterViewInit() {
        this.doSearch(1);
    }

    ngOnDestroy() {
        this.subs.forEach(sub => sub.unsubscribe());
    }

    doSearch(page: number) {
        this.pagination.pageNumber = page;
        this.isLoadingPage = true;
        const searchParam = this.searchForm ? this.searchForm.parseOptions() : new HttpParams();
        this.searchFormService.getFilterResearch(UrlConstant.SEARCH_FORM_DRAFT.DEPENDENT_PERSON, searchParam, this.pagination.getCurrentPage()).subscribe(res => {
            if (res.code === HTTP_STATUS_CODE.OK) {
                this.isDisabledApproveAllBtn = res.data.listData.every((draft: NzSafeAny) => draft.status === Constant.APPROVE.IS_APPROVE || draft.status === Constant.APPROVE.IS_NOT_APPROVE);
                if (this.tableConfig.pageSize) {
                  if (Math.ceil(res.data.count/ this.tableConfig.pageSize) >= 2) {
                      this.isDisabledApproveAllBtn = false;
                  }
                }
                res.data.listData
                    .filter((draft: NzSafeAny) => draft.status === Constant.APPROVE.IS_NOT_APPROVE)
                    .forEach((draft: NzSafeAny) => this.mapEmployeeIsNotApproved.set(draft.draftDependentPersonId, draft));
                this.searchResult = res.data.listData;
                this.tableConfig.total = res.data.count;
            }
          this.isLoadingPage = false;
        }, () => this.isLoadingPage = false);
    }

    doImportData() {
        this.isImportData = true;
    }

    private initTable(): void {
        this.tableConfig = {
            headers: [
                {
                    title: 'staffManager.staffResearch.dependentPerson.table.select',
                    field: 'select',
                    tdClassList: ['text-center'], thClassList: ['text-center'],
                    tdTemplate: this.selectRecord,
                    width: 70,
                    fixed: true,
                    show: this.objFunction.approve,
                    fixedDir: 'left'
                },
                {
                    title: 'staffManager.staffResearch.dependentPerson.table.employeeCode',
                    field: 'employeeCode',
                    tdClassList: ['text-nowrap'], thClassList: ['text-center'],
                    width: 120,
                    fixed: true,
                    fixedDir: 'left'
                },
                {
                    title: 'staffManager.staffResearch.dependentPerson.table.employeeName',
                    field: 'employeeName',
                    tdClassList: ['text-nowrap'], thClassList: ['text-center'],
                    width: 150
                },
                {
                    title: 'staffManager.staffResearch.dependentPerson.table.status',
                    field: 'status',
                    tdTemplate: this.status,
                    tdClassList: ['text-center'], thClassList: ['text-center'],
                    show: true,
                    width: 100
                },
                {
                    title: 'staffManager.staffResearch.dependentPerson.table.orgName',
                    field: 'orgName',
                    tdClassList: ['text-left'], thClassList: ['text-center'],
                    width: 200
                },
                {
                    title: 'staffManager.staffResearch.dependentPerson.table.flagStatus',
                    field: 'flagStatus',
                    tdClassList: ['text-nowrap'], thClassList: ['text-center'],
                    show: false,
                    width: 100
                },
                {
                    title: 'staffManager.staffResearch.dependentPerson.table.dependentPersonCode',
                    field: 'dependentPersonCode',
                    tdClassList: ['text-left'], thClassList: ['text-center'],
                    width: 150
                },
                {
                    title: 'staffManager.staffResearch.dependentPerson.table.relationTypeName',
                    field: 'relationTypeName',
                    tdClassList: ['text-nowrap'], thClassList: ['text-center'],
                    width: 100
                },
                {
                    title: 'staffManager.staffResearch.dependentPerson.table.fullName',
                    field: 'fullName',
                    tdClassList: ['text-nowrap'], thClassList: ['text-center'],
                    width: 150
                },
                {
                    title: 'staffManager.staffResearch.dependentPerson.table.dateOfBirth',
                    field: 'dateOfBirth',
                    tdClassList: ['text-center'],
                    thClassList: ['text-center'],
                    width: 100
                },
                {
                    title: 'staffManager.staffResearch.dependentPerson.table.taxNumber',
                    field: 'taxNumber',
                    width: 150
                },
                {
                    title: 'staffManager.staffResearch.dependentPerson.table.fromDate',
                    field: 'fromDate',
                    tdClassList: ['text-center'], thClassList: ['text-center'],
                    width: 100
                },
                {
                    title: 'staffManager.staffResearch.dependentPerson.table.toDate',
                    field: 'toDate',
                    tdClassList: ['text-center'], thClassList: ['text-center'],
                    width: 100
                },
                {
                  title: 'staffManager.staffResearch.allowanceHis.table.attachFile',
                  field: 'attachFileList',
                  width: 200,
                  show: true,
                  tdTemplate: this.attachFile
                },
                {
                    title: ' ',
                    field: 'action',
                    tdTemplate: this.action,
                    tdClassList: ['text-center'],
                    thClassList: ['text-center'],
                    show: this.objFunction.approve || this.objFunction.delete,
                    width: 50,
                    fixed: true,
                    fixedDir: 'right',
                }
            ],
            total: 0,
            needScroll: true,
            loading: false,
            size: 'small',
            pageSize: this.pagination.pageSize,
            pageIndex: 1
        };
    }

    doCloseImport(event: boolean) {
      this.isImportData = false;
      if(event) {
        this.doSearch(this.pagination.pageNumber);
      }
    }

    approveDependentPerson(draftDependentPersonId: number) {
        this.isLoadingPage = true;
        this.dependentPersonService.approveDependentPerson(draftDependentPersonId).subscribe(res => {
            if (res.code === HTTP_STATUS_CODE.OK) {
                this.doSearch(1);
                this.toastrService.success(this.translateService.instant('common.notification.isApprove'));
            }
            this.isLoadingPage = false;
        }, error => {
            this.toastrService.error(error.message);
            this.isLoadingPage = false;
        });
    }

    getFlagStatus(value: number) {
        return this.statusList.find(status => status.value === value)?.label;
    }

    getStatus(value: number) {
        return this.statusApproveList.find(status => status.value === value)?.label;
    }

    approveAll() {
        this.isLoadingPage = true;
        this.dependentPersonService.approveAllDependentPerson(this.searchForm.getSearchForm()).subscribe(res => {
            if (res.code === HTTP_STATUS_CODE.OK) {
                this.doSearch(1);
                this.toastrService.success(this.translateService.instant('common.notification.isApprove'));
            }
            this.isLoadingPage = false;
        }, error => {
            this.toastrService.error(error.message);
            this.isLoadingPage = false;
        });
    }

    onCheckBoxChange(event: boolean, draftPersonalDependentPersonId: number) {
        if (event) {
            this.selectedCheckBoxNumber++;
            this.draftDependentPersonSet.add(draftPersonalDependentPersonId);
        } else {
            this.selectedCheckBoxNumber--;
            if (this.draftDependentPersonSet.has(draftPersonalDependentPersonId)) {
                this.draftDependentPersonSet.delete(draftPersonalDependentPersonId);
            }
        }
    }

    approveDependentPersonList() {
        const listId = Array.from(this.draftDependentPersonSet);
        this.isLoadingPage = true;
        this.dependentPersonService.approveDependentPersonByListId(listId).subscribe(res => {
            if (res.code === HTTP_STATUS_CODE.OK) {
                this.doSearch(1);
                this.toastrService.success(this.translateService.instant('common.notification.isApprove'));
            }
            this.isLoadingPage = false;
        }, error => {
            this.toastrService.error(error.message);
            this.isLoadingPage = false;
        });
        this.draftDependentPersonSet.clear();
        this.selectedCheckBoxNumber = 0;
    }

    confirm(draftDependentPersonId: number) {
        this.isLoadingPage = true;
        this.dependentPersonService.deleteDraftDependentPersonById(draftDependentPersonId).subscribe({
            next: (res) => {
                if (res.code === HTTP_STATUS_CODE.OK) {
                    this.doSearch(1);
                    this.toastrService.success(this.translateService.instant('common.notification.deleteSuccess'));
                }
                this.isLoadingPage = false;
            }, error: (err) => {
                this.toastrService.error(err.message);
                this.isLoadingPage = false;
          }
        });
    }

    doCloseReject(){
        this.isReject = false;
        this.doSearch(1);
    }

    resetListDraftId() {
        this.listDraftId = [];
    }

    openRejectById(draftDependentPersonId: number) {
        this.isReject = true;
        this.id = draftDependentPersonId;
    }

    openRejectByList() {
        if (validateRejectByList(this.draftDependentPersonSet, this.mapEmployeeIsNotApproved, this.translateService, this.toastrService)) {
            this.isReject = true;
            this.isRejectList = true;
        }
    }

    doDownloadAttach(file: NzSafeAny) {
      this.isLoadingPage = true;
      this.downloadFileAttachService.doDownloadAttachFileWithSecurity(~~file?.docId, file?.security).subscribe(res => {
        const reportFile = new Blob([res.body], { type: getTypeExport(file.fileName.split('.').pop()) });
        saveAs(reportFile, file.fileName);
        this.isLoadingPage = false;
      }, error => {
        this.toastrService.error(error.message);
        this.isLoadingPage = false;
      });
    }

    onLoadPage(event: boolean) {
      this.isLoadingPage = event;
    }

}
