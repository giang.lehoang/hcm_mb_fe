import {AfterViewInit, Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { HttpParams } from '@angular/common/http';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { ToastrService } from 'ngx-toastr';
import { FormGroup } from '@angular/forms';
import { saveAs } from 'file-saver';
import {Constant, ConstantColor, UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {AppFunction, CatalogModel, MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {DraftObject, PersonalInformation} from "@hcm-mfe/staff-manager/data-access/models/draft";
import {
  BookmarkFormService,
  CommonDraftService,
  DownloadFileAttachService,
  SearchFormService
} from "@hcm-mfe/staff-manager/data-access/services";
import {ValidateService} from "@hcm-mfe/shared/core";
import { SessionService } from "@hcm-mfe/shared/common/store";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {getTypeExport} from "@hcm-mfe/shared/common/utils";
import {SearchFormComponent} from "@hcm-mfe/staff-manager/ui/search-form-draft";

@Component({
  selector: 'app-allowance-his-research',
  templateUrl: './allowance-draft.component.html',
  styleUrls: ['./allowance-draft.component.scss']
})
export class AllowanceDraftComponent implements OnInit, AfterViewInit, OnDestroy {
  public readonly MODULENAME = Constant.MODULE_NAME.ALLOW_HISTORY;
  functionCode: string = FunctionCode.HR_ALLOWANCE_PROCESS;
  objFunction: AppFunction;
  isImportData = false;
  isReject = false;
  tableConfig!: MBTableConfig;
  searchResult: PersonalInformation[] = [];
  urlApiImport: string = UrlConstant.IMPORT_FORM.DRAFT_ALLOWANCE_PROCESS_FORM;
  urlApiDownloadTemp: string = UrlConstant.IMPORT_FORM.ALLOWANCE_PROCESS_TEMPLATE;
  urlRejectById: string = UrlConstant.REFUSE_APPROVE_DRAFT.ALLOWANCE_PROCESS;
  urlRejectByList: string = UrlConstant.REJECT_LIST_DRAFT.ALLOWANCE_PROCESS;
  id?:number;
  listDraftId:number[] = [];
  listDraftObject: DraftObject[] = [];
  isRejectList = false;
  searchFormApprove: FormGroup | NzSafeAny;
  isDisabled = false;
  mbButtonBgColor:string = ConstantColor.COLOR.BG_PRIMARY_LIGHT;

  tagInitializeColor: string = ConstantColor.TAG.STATUS_INITIALIZE;
  tagApprovedColor: string = ConstantColor.TAG.STATUS_APPROVED;
  tagNotApprovedColor: string = ConstantColor.TAG.STATUS_NOT_APPROVED;
  disabledColor:string = ConstantColor.COLOR.DISABLED;
  CONSTANT_STATUS = Constant.APPROVE;
  isLoadingPage = false;

  statusList: CatalogModel[] = [
    {
      value: 1,
      label: this.translateService.instant('staffManager.label.workIn')
    },
    {
      value: 0,
      label: this.translateService.instant('staffManager.label.workOut')
    }
  ];

  statusApproveList: CatalogModel[] = [
    {
      value: 0,
      label: this.translateService.instant('staffManager.label.isInitial')
    },
    {
      value: 1,
      label: this.translateService.instant('staffManager.label.isApprove')
    },
    {
      value: 2,
      label: this.translateService.instant('staffManager.label.isNotApprove')
    }
  ];

  subs: Subscription[] = [];
  pagination = new Pagination();

  @ViewChild('searchForm') searchForm!: SearchFormComponent;
  @ViewChild('actionTmpl', {static: true}) action!: TemplateRef<NzSafeAny>;
  @ViewChild('attachFileTmpl', {static: true}) attachFile!: TemplateRef<NzSafeAny>;
  @ViewChild('statusTmpl', {static: true}) status!: TemplateRef<NzSafeAny>;
  @ViewChild('selectTmpl', {static: true}) select!: TemplateRef<NzSafeAny>;

  constructor(private bookmarkFormService: BookmarkFormService,
              private router: Router,
              private translateService: TranslateService,
              private searchFormService: SearchFormService,
              private commonDraftService: CommonDraftService,
              private toastrService: ToastrService,
              public validateService: ValidateService,
              public sessionService: SessionService,
              private downloadFileAttachService: DownloadFileAttachService
  ) {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.HR_MODIFY_ALLOWANCE_PROCESS}`);
  }


  ngOnInit(): void {
    this.initTable();
  }

  ngAfterViewInit() {
    this.doSearch(1);
  }

  ngOnDestroy() {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  doSearch(pageIndex: number) {
    this.pagination.pageNumber = pageIndex;
    const searchParam = this.searchForm? this.searchForm.parseOptions() : new HttpParams();
    this.isLoadingPage = true;
    this.searchFormService.getFilterResearch(UrlConstant.SEARCH_FORM.DRAFT_ALLOWANCE_PROCESS, searchParam, this.pagination.getCurrentPage()).subscribe(res => {
      if (res.code === HTTP_STATUS_CODE.OK) {
          this.isDisabled = res.data.listData.every((draft: NzSafeAny) => draft.status === Constant.APPROVE.IS_APPROVE || draft.status === Constant.APPROVE.IS_NOT_APPROVE);
          if (this.tableConfig.pageSize) {
            if (Math.ceil(res.data.count/ this.tableConfig.pageSize) >= 2) {
                this.isDisabled = false;
            }
          }
        this.searchResult = res.data.listData;
        this.tableConfig.pageIndex = pageIndex;
        this.tableConfig.total = res.data.count;
        this.searchFormApprove = this.searchForm.form;
      }
      this.isLoadingPage = false;
    }, () => this.isLoadingPage = false);
  }

  doImportData() {
    this.isImportData = true;
  }

  openRejectById(id:number,status:number) {
    if(status !== Constant.APPROVE.IS_NOT_APPROVE) {
      this.isReject = true;
      this.id = id;
    }
  }

  openRejectByList() {
    const listRejected:string[] = [];
    let error = false;
    this.listDraftObject.forEach((data:DraftObject) => {
      if(data.status === Constant.APPROVE.IS_NOT_APPROVE) {
        listRejected.push(data.employeeCode);
        error = true;
      }
    })
    if(error){
      this.toastrService.error(this.translateService.instant('common.notification.employeeCode') + " {" + listRejected.toString() + "} " + this.translateService.instant('common.notification.rejected'));
    } else {
      this.isReject = true;
      this.isRejectList = true;
    }
  }

  doCloseReject(){
    this.isReject = false;
    this.doSearch(this.pagination.pageNumber);
  }

  resetListDraftId() {
    this.listDraftId = [];
  }


  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'staffManager.staffResearch.degree.table.select',
          field: 'select',
          width: 60,
          tdTemplate: this.select,
          thClassList: ['text-nowrap','text-center'],
          tdClassList: ['text-nowrap','text-center'],
          show: this.objFunction.approve,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left'
        },
        {
          title: 'staffManager.staffResearch.allowanceHis.table.employeeCode',
          field: 'employeeCode', width: 130,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left'
        },
        {
          title: 'staffManager.staffResearch.allowanceHis.table.fullName',
          field: 'fullName', width: 150,
        },
        {
          title: 'staffManager.staffResearch.allowanceHis.table.status',
          field: 'status',
          width: 100,
          tdTemplate: this.status,
          tdClassList: ['text-nowrap','text-center'],
          thClassList: ['text-nowrap','text-center'],
          show: true
        },
        {
          title: 'staffManager.staffResearch.allowanceHis.table.orgName',
          field: 'orgName', width: 250
        },
        {
          title: 'staffManager.staffResearch.allowanceHis.table.fromDate',
          field: 'fromDate',
          width: 100,
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        },
        {
          title: 'staffManager.staffResearch.allowanceHis.table.toDate',
          field: 'toDate',
          width: 100,
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        }, {
          title: 'staffManager.staffResearch.allowanceHis.table.allowanceTypeName',
          field: 'allowanceTypeName',
          width: 150,
        },
        {
          title: 'staffManager.staffResearch.allowanceHis.table.amountMoney',
          field: 'amountMoney',
          width: 120,
          show: false,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
        },
        {
          title: 'staffManager.staffResearch.allowanceHis.table.attachFile',
          field: 'attachFileList',
          width: 200,
          show: true,
          tdTemplate: this.attachFile
        },
        {
          title: ' ',
          field: 'action',
          tdClassList: ['text-nowrap','text-center'], thClassList: ['text-nowrap','text-center'],
          width: 50,
          show: this.objFunction.approve || this.objFunction.delete,
          tdTemplate: this.action,
          fixed: window.innerWidth > 1024,
          fixedDir: 'right'
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  doCloseImport(event: boolean) {
    this.isImportData = false;
    if(event) {
      this.doSearch(this.pagination.pageNumber);
    }
  }

  getStatus(value: number) {
    return this.statusApproveList.find(status => status.value === value)?.label;
  }

  approveById(id:number) {
    this.isLoadingPage = true;
    this.commonDraftService.approveById(id, UrlConstant.APPROVE_DRAFT.ALLOWANCE_PROCESS).subscribe(res => {
      if (res.code===HTTP_STATUS_CODE.OK) {
        this.doSearch(this.pagination.pageNumber);
        this.toastrService.success(this.translateService.instant('common.notification.isApprove'));
      }
      this.isLoadingPage = false;
    }, error => {
      this.toastrService.error(error.message);
      this.isLoadingPage = false;
    });
  }

  approveByList() {
    this.isLoadingPage = true;
    this.commonDraftService.approveByList(this.listDraftId, UrlConstant.APPROVE_LIST_DRAFT.ALLOWANCE_PROCESS).subscribe(res => {
      if (res.code===HTTP_STATUS_CODE.OK) {
        this.doSearch(this.pagination.pageNumber);
        this.toastrService.success(this.translateService.instant('common.notification.isApprove'));
        this.listDraftId = [];
      }
      this.isLoadingPage = false;
    },error => {
      this.toastrService.error(error.message);
      this.isLoadingPage = false;
    });
  }

  approveAll() {
    this.isLoadingPage = true;
    this.commonDraftService.approveAll(UrlConstant.APPROVE_ALL_DRAFT.ALLOWANCE_PROCESS, this.searchFormApprove).subscribe(res => {
      if(res.code===HTTP_STATUS_CODE.OK) {
        this.doSearch(this.pagination.pageNumber);
        this.toastrService.success(this.translateService.instant('common.notification.isApprove'));
      }
      this.isLoadingPage = false;
    }, error => {
      this.toastrService.error(error.message);
      this.isLoadingPage = false;
    });
  }

  deleteDraft(id:number) {
    this.isLoadingPage = true;
    this.commonDraftService.deleteDraft(id,UrlConstant.DELETE_DRAFT.ALLOWANCE_PROCESS).subscribe(res => {
      if(res.code===HTTP_STATUS_CODE.OK) {
        this.doSearch(1);
        this.toastrService.success(this.translateService.instant('common.notification.deleteSuccess'));
      }
      this.isLoadingPage = false;
    }, error => {
      this.toastrService.error(error.message);
      this.isLoadingPage = false;
    })
  }

  onCheckChange(event: NzSafeAny,status:number,employeeCode:string){
    const id = parseInt(event.target.value);
    const data:DraftObject = {
      id: id,
      status: status,
      employeeCode: employeeCode
    }
    if(event.target.checked) {
      this.listDraftId.push(id);
      this.listDraftObject.push(data);
    } else {
      this.listDraftId.splice(this.listDraftId.indexOf(id),1);
      this.listDraftObject.forEach((value,index) => {
        if(value.id == id) {
          this.listDraftObject.splice(index,1);
        }
      })
    }
  }

  doDownloadAttach(file: NzSafeAny) {
    this.isLoadingPage = true;
    this.downloadFileAttachService.doDownloadAttachFileWithSecurity(~~file?.docId, file?.security).subscribe(res => {
      const reportFile = new Blob([res.body], { type: getTypeExport(file.fileName.split('.').pop()) });
      saveAs(reportFile, file.fileName);
      this.isLoadingPage = false;
    }, error => {
      this.toastrService.error(error.message);
      this.isLoadingPage = false;
    });
  }

  onLoadPage(event: boolean) {
    this.isLoadingPage = event;
  }

}
