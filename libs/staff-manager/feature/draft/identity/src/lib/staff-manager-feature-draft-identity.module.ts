import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {StaffManagerUiSearchFormDraftModule} from "@hcm-mfe/staff-manager/ui/search-form-draft";
import {NzFormModule} from "ng-zorro-antd/form";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {StaffManagerUiImportCommonModule} from "@hcm-mfe/staff-manager/ui/import-common";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {StaffManagerUiRejectCommonModule} from "@hcm-mfe/staff-manager/ui/reject-common";
import {NzTagModule} from "ng-zorro-antd/tag";
import {IdentityDraftComponent} from "./identity/identity-draft.component";
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";
import {FormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
    imports: [CommonModule, StaffManagerUiSearchFormDraftModule, NzFormModule, SharedUiMbButtonModule, TranslateModule,
        SharedUiMbTableWrapModule, SharedUiMbTableModule, StaffManagerUiImportCommonModule, NzIconModule, NzButtonModule,
        NzDropDownModule, StaffManagerUiRejectCommonModule, NzTagModule, NzCheckboxModule, FormsModule,
        RouterModule.forChild([
            {
                path: '',
                component: IdentityDraftComponent
            }
        ]), SharedUiLoadingModule
    ],
  declarations: [IdentityDraftComponent],
  exports: [IdentityDraftComponent],
})
export class StaffManagerFeatureDraftIdentityModule {}
