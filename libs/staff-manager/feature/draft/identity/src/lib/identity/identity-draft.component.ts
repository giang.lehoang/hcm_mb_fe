import {AfterViewInit, Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { HttpParams } from '@angular/common/http';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { ToastrService } from 'ngx-toastr';
import { saveAs } from 'file-saver';

import {Constant, ConstantColor, UrlConstant, validateRejectByList} from "@hcm-mfe/staff-manager/data-access/common";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {AppFunction, CatalogModel, MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {PersonalInformation} from "@hcm-mfe/staff-manager/data-access/models/draft";
import {SearchFormComponent} from "@hcm-mfe/staff-manager/ui/search-form-draft";
import {
  DownloadFileAttachService, PersonalIdentityService,
  SearchFormService
} from "@hcm-mfe/staff-manager/data-access/services";
import {ValidateService} from "@hcm-mfe/shared/core";
import { SessionService } from "@hcm-mfe/shared/common/store";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {getTypeExport} from "@hcm-mfe/shared/common/utils";

@Component({
    selector: 'app-identity-research',
    templateUrl: './identity-draft.component.html',
    styleUrls: ['./identity-draft.component.scss']
})
export class IdentityDraftComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('actionTmpl', { static: true }) action!: TemplateRef<NzSafeAny>;
    @ViewChild('searchForm') searchForm!: SearchFormComponent;
    @ViewChild('flagStatusTmpl', {static: true}) flagStatus!: TemplateRef<NzSafeAny>;
    @ViewChild('statusTmpl', {static: true}) status!: TemplateRef<NzSafeAny>;
    @ViewChild('selectTmpl', {static: true}) selectRecord!: TemplateRef<NzSafeAny>;
    @ViewChild('isMainTmpl', {static: true}) isMain!: TemplateRef<NzSafeAny>;
    @ViewChild('attachFileTmpl', {static: true}) attachFile!: TemplateRef<NzSafeAny>;

    public readonly MODULENAME = Constant.MODULE_NAME.IDENTITY;
    functionCode: string = FunctionCode.HR_MODIFY_IDENTITY;
    objFunction: AppFunction;
    tableConfig!: MBTableConfig;
    searchResult: PersonalInformation[] = [];
    isImportData = false;
    urlApiImport: string = UrlConstant.IMPORT_EXCEL.PERSONAL_IDENTITIES;
    urlApiDownloadTemp: string = UrlConstant.IMPORT_FORM.PERSONAL_IDENTITIES_TEMPLATE;
    selectedCheckBoxNumber = 0;
    draftIdentitySet = new Set<number>();
    isDisabledApproveAllBtn = false;
    btnDisableColor: string = ConstantColor.COLOR.BG_PRIMARY_LIGHT;

    tagInitializeColor: string = ConstantColor.TAG.STATUS_INITIALIZE;
    tagApprovedColor: string = ConstantColor.TAG.STATUS_APPROVED;
    tagNotApprovedColor: string = ConstantColor.TAG.STATUS_NOT_APPROVED;
    disabledColor:string = ConstantColor.COLOR.DISABLED;

    listDraftId: number[] = Array.from(this.draftIdentitySet);
    isRejectList = false;
    isReject = false;
    urlRejectById: string = UrlConstant.REFUSE_APPROVE_DRAFT.IDENTITY;
    urlRejectByList: string = UrlConstant.REFUSE_APPROVE_DRAFT.IDENTITY_LIST;
    id?: number;
    mapEmployeeIsNotApproved = new Map<number, PersonalInformation>();

    subs: Subscription[] = [];
    pagination = new Pagination();
    CONSTANT_STATUS = Constant.APPROVE;
    CONSTANT_IDENTITIES = Constant.IDENTITIES;
    CONSTANT_FLAG_STATUS = Constant.FLAG_STATUS;
    requiredAttachFile = true;
    isLoadingPage = false;
    statusList: CatalogModel[] = [
        {
            value: 1,
            label: this.translateService.instant('staffManager.label.workIn')
        },
        {
            value: 0,
            label: this.translateService.instant('staffManager.label.workOut')
        }
    ];

    statusApproveList: CatalogModel[] = [
        {
            value: 0,
            label: this.translateService.instant('staffManager.label.isInitial')
        },
        {
            value: 1,
            label: this.translateService.instant('staffManager.label.isApprove')
        },
        {
            value: 2,
            label: this.translateService.instant('staffManager.label.isNotApprove')
        }
    ];

    constructor(
                private router: Router,
                private translateService: TranslateService,
                private searchFormService: SearchFormService,
                public validateService: ValidateService,
                private personalIdentityService: PersonalIdentityService,
                private toastrService: ToastrService,
                public sessionService: SessionService,
                private downloadFileAttachService: DownloadFileAttachService
    ) {
        this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.HR_MODIFY_IDENTITY}`);
    }

    ngOnInit(): void {
        this.initTable();
    }

    ngAfterViewInit() {
        this.doSearch(1);
    }

    ngOnDestroy() {
        this.subs.forEach(sub => sub.unsubscribe());
    }

    doSearch(page: number) {
        this.pagination.pageNumber = page;
        this.isLoadingPage = true;
        const searchParam = this.searchForm ? this.searchForm.parseOptions() : new HttpParams();
        this.searchFormService.getFilterResearch(UrlConstant.SEARCH_FORM_DRAFT.IDENTITY, searchParam, this.pagination.getCurrentPage()).subscribe(res => {
            if (res.code === HTTP_STATUS_CODE.OK) {
                this.isDisabledApproveAllBtn = res.data.listData.every((draft: NzSafeAny) => draft.status === Constant.APPROVE.IS_APPROVE || draft.status === Constant.APPROVE.IS_NOT_APPROVE);
                if (this.tableConfig.pageSize) {
                    if (Math.ceil(res.data.count/ this.tableConfig.pageSize) >= 2) {
                        this.isDisabledApproveAllBtn = false;
                    }
                }
                this.searchResult = res.data.listData;
                res.data.listData
                    .filter((draft: NzSafeAny) => draft.status === Constant.APPROVE.IS_NOT_APPROVE)
                    .forEach((draft: NzSafeAny) => this.mapEmployeeIsNotApproved.set(draft.draftPersonalIdentityId, draft));
                this.tableConfig.total = res.data.count;
            }
            this.isLoadingPage = false;
        }, () => this.isLoadingPage = false);
    }

    doImportData() {
        this.isImportData = true;
    }

    private initTable(): void {
        this.tableConfig = {
            headers: [
                {
                    title: 'staffManager.staffResearch.identity.table.select',
                    field: 'select',
                    tdClassList: ['text-center'], thClassList: ['text-center'],
                    tdTemplate: this.selectRecord,
                    width: 70,
                    show: this.objFunction.approve,
                    fixed: true,
                    fixedDir: 'left',
                },
                {
                    title: 'staffManager.staffResearch.identity.table.employeeCode',
                    field: 'employeeCode',
                    tdClassList: ['text-left'], thClassList: ['text-center'],
                    fixed: true,
                    fixedDir: 'left',
                    width: 100,
                },
                {
                    title: 'staffManager.staffResearch.identity.table.fullName',
                    field: 'fullName',
                    tdClassList: ['text-nowrap'], thClassList: ['text-center'],
                    width: 150
                },
                {
                    title: 'staffManager.staffResearch.identity.table.status',
                    field: 'status',
                    tdTemplate: this.status,
                    tdClassList: ['text-center'], thClassList: ['text-center'],
                    show: true,
                    width: 150
                },
                {
                    title: 'staffManager.staffResearch.identity.table.orgName',
                    field: 'orgName',
                    tdClassList: ['text-left'], thClassList: ['text-center'],
                    width: 150
                },
                {
                    title: 'staffManager.staffResearch.identity.table.label',
                    field: 'label',
                    tdClassList: ['text-left'], thClassList: ['text-center'],
                    width: 150
                },
                {
                    title: 'staffManager.staffResearch.identity.table.idNo',
                    field: 'idNo',
                    tdClassList: ['text-nowrap'], thClassList: ['text-center'],
                    width: 150
                },
                {
                    title: 'staffManager.staffResearch.identity.table.idIssuePlace',
                    field: 'idIssuePlace',
                    tdClassList: ['text-left'], thClassList: ['text-center'],
                    show: true,
                    width: 150
                },
                {
                    title: 'staffManager.staffResearch.identity.table.idIssueDate',
                    field: 'idIssueDate',
                    tdClassList: ['text-center'], thClassList: ['text-center'],
                    width: 150
                },
                {
                    title: 'staffManager.staffResearch.identity.table.isMain',
                    field: 'isMain',
                    tdClassList: ['text-center'], thClassList: ['text-center'],
                    tdTemplate: this.isMain,
                    show: true,
                    width: 150
                },
                {
                    title: 'staffManager.staffResearch.allowanceHis.table.attachFile',
                    field: 'attachFileList',
                    width: 200,
                    show: true,
                    tdTemplate: this.attachFile
                },
                {
                    title: ' ',
                    field: 'action',
                    tdTemplate: this.action,
                    tdClassList: ['text-center'],
                    thClassList: ['text-center'],
                    show: this.objFunction.approve || this.objFunction.delete,
                    width: 50,
                    fixed: true,
                    fixedDir: 'right'
                }
            ],
            total: 0,
            needScroll: true,
            loading: false,
            size: 'small',
            pageSize: this.pagination.pageSize,
            pageIndex: 1
        };
    }

    doCloseImport(event: boolean) {
      this.isImportData = false;
      if(event) {
        this.doSearch(this.pagination.pageNumber);
      }
    }

    getFlagStatus(value: number) {
        return this.statusList.find(status => status.value === value)?.label;
    }

    getStatus(value: number) {
        return this.statusApproveList.find(status => status.value === value)?.label;
    }

    doCloseReject(){
        this.isReject = false;
        this.doSearch(1);
    }

    resetListDraftId() {
        this.listDraftId = [];
    }

    approveIdentity(draftPersonalIdentityId: number) {
        this.isLoadingPage = true;
        this.personalIdentityService.approveIdentity(draftPersonalIdentityId).subscribe(res => {
            if (res.code === HTTP_STATUS_CODE.OK) {
                this.doSearch(1);
                this.toastrService.success(this.translateService.instant('common.notification.isApprove'));
            }
            this.isLoadingPage = false;
        }, error => {
            this.toastrService.error(error.message);
            this.isLoadingPage = false;
        });
    }

    approveAll() {
        this.isLoadingPage = true;
        this.personalIdentityService.approveAllIdentity(this.searchForm.getSearchForm()).subscribe(res => {
            if (res.code === HTTP_STATUS_CODE.OK) {
                this.doSearch(1);
                this.toastrService.success(this.translateService.instant('common.notification.isApprove'));
            }
            this.isLoadingPage = false;
        }, error => {
            this.toastrService.error(error.message);
            this.isLoadingPage = false;
        });
    }

    onCheckBoxChange(event: boolean, draftPersonalIdentityId: number) {
        if (event) {
            this.selectedCheckBoxNumber ++;
            this.draftIdentitySet.add(draftPersonalIdentityId);
        } else {
            this.selectedCheckBoxNumber --;
            if (this.draftIdentitySet.has(draftPersonalIdentityId)) {
                this.draftIdentitySet.delete(draftPersonalIdentityId);
            }
        }
    }

    approveIdentityList() {
        this.isLoadingPage = true;
        const listId = Array.from(this.draftIdentitySet);
        this.personalIdentityService.approveIdentityByListId(listId).subscribe(res => {
            if (res.code === HTTP_STATUS_CODE.OK) {
                this.doSearch(1);
                this.toastrService.success(this.translateService.instant('common.notification.isApprove'));
            }
            this.isLoadingPage = false;
        }, error => {
            this.toastrService.error(error.message);
            this.isLoadingPage = false;
        });
        this.draftIdentitySet.clear();
        this.selectedCheckBoxNumber = 0;
    }

    confirm(draftPersonalIdentityId: number) {
        this.isLoadingPage = true;
        this.personalIdentityService.deleteDraftIdentityById(draftPersonalIdentityId).subscribe(res => {
            if (res.code === HTTP_STATUS_CODE.OK) {
                this.doSearch(1);
                this.toastrService.success(this.translateService.instant('common.notification.deleteSuccess'));
            }
            this.isLoadingPage = false;
        }, error => {
            this.toastrService.error(error.message);
            this.isLoadingPage = false;
        });
    }

    openRejectById(draftPersonalIdentityId: number) {
        this.isReject = true;
        this.id = draftPersonalIdentityId;
    }

    openRejectByList() {
        if (validateRejectByList(this.draftIdentitySet, this.mapEmployeeIsNotApproved, this.translateService, this.toastrService)) {
            this.isReject = true;
            this.isRejectList = true;
        }
    }

    doDownloadAttach(file: NzSafeAny) {
      this.isLoadingPage = true;
      this.downloadFileAttachService.doDownloadAttachFileWithSecurity(~~file?.docId, file?.security).subscribe(res => {
        const reportFile = new Blob([res.body], { type: getTypeExport(file.fileName.split('.').pop()) });
        saveAs(reportFile, file.fileName);
        this.isLoadingPage = false;
      }, error => {
        this.toastrService.error(error.message);
        this.isLoadingPage = false;
      });
    }

    onLoadPage(event: boolean) {
      this.isLoadingPage = event;
    }

}
