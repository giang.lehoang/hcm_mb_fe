import {AfterViewInit, Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { Subscription } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import {SearchFormComponent} from "@hcm-mfe/staff-manager/ui/search-form-draft";
import {Constant, ConstantColor, UrlConstant, validateRejectByList} from "@hcm-mfe/staff-manager/data-access/common";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {AppFunction, CatalogModel, MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {ConcurrentProcess} from "@hcm-mfe/staff-manager/data-access/models/info";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {ConcurrentProcessService, DownloadFileAttachService, SearchFormService} from "@hcm-mfe/staff-manager/data-access/services";
import {ValidateService} from "@hcm-mfe/shared/core";
import {SessionService} from "@hcm-mfe/shared/common/store";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {getTypeExport} from "@hcm-mfe/shared/common/utils";
import {saveAs} from "file-saver";

@Component({
    selector: 'app-concurrent-process',
    templateUrl: './concurrent-process-draft.component.html',
    styleUrls: ['./concurrent-process-draft.component.scss']
})
export class ConcurrentProcessDraftComponent implements OnInit, OnDestroy, AfterViewInit {

    @ViewChild('searchForm') searchForm!: SearchFormComponent;
    @ViewChild('flagStatusTmpl', { static: true }) flagStatus!: TemplateRef<NzSafeAny>;
    @ViewChild('actionTmpl', { static: true }) action!: TemplateRef<NzSafeAny>;
    @ViewChild('statusTmpl', { static: true }) status!: TemplateRef<NzSafeAny>;
    @ViewChild('selectTmpl', { static: true }) selectRecord!: TemplateRef<NzSafeAny>;
    @ViewChild('suggestTypeTpl', {static: true}) suggestTypeTpl!: TemplateRef<NzSafeAny>;
    @ViewChild('attachFileTmpl', {static: true}) attachFile!: TemplateRef<NzSafeAny>;

    public readonly MODULENAME = Constant.MODULE_NAME.CONCURRENT_PROCESS;
    functionCode: string = FunctionCode.HR_CONCURRENT_PROCESS;
    objFunction: AppFunction;
    tableConfig!: MBTableConfig;

    searchResult: ConcurrentProcess[] = [];

    isImportData = false;
    urlApiImport: string = UrlConstant.IMPORT_EXCEL.CONCURRENT_PROCESS;
    urlApiDownloadTemp: string = UrlConstant.IMPORT_FORM.CONCURRENT_PROCESS_TEMPLATE;

    subs: Subscription[] = [];
    pagination = new Pagination();

    selectedCheckBoxNumber = 0;
    draftConcurrentProcessSet = new Set<number>();

    listDraftId: number[] = Array.from(this.draftConcurrentProcessSet);
    isRejectList = false;
    isReject = false;
    urlRejectById: string = UrlConstant.REFUSE_APPROVE_DRAFT.CONCURRENT_PROCESS;
    urlRejectByList: string = UrlConstant.REFUSE_APPROVE_DRAFT.CONCURRENT_PROCESS_LIST;
    id: number | undefined;

    isDisabledApproveAllBtn = false;
    btnDisableColor: string = ConstantColor.COLOR.BG_PRIMARY_LIGHT;

    tagInitializeColor: string = ConstantColor.TAG.STATUS_INITIALIZE;
    tagApprovedColor: string = ConstantColor.TAG.STATUS_APPROVED;
    tagNotApprovedColor: string = ConstantColor.TAG.STATUS_NOT_APPROVED;
    disabledColor:string = ConstantColor.COLOR.DISABLED;

    mapEmployeeIsNotApproved = new Map<number, ConcurrentProcess>();
    isLoadingPage = false;
    CONSTANT_STATUS = Constant.APPROVE;
    CONSTANT_FLAG_STATUS = Constant.FLAG_STATUS;
    constant = Constant;
    statusList: CatalogModel[] = [
        {
            value: 1,
            label: this.translateService.instant('staffManager.label.workIn')
        },
        {
            value: 0,
            label: this.translateService.instant('staffManager.label.workOut')
        }
    ];

    statusApproveList: CatalogModel[] = [
        {
            value: 0,
            label: this.translateService.instant('staffManager.label.isInitial')
        },
        {
            value: 1,
            label: this.translateService.instant('staffManager.label.isApprove')
        },
        {
            value: 2,
            label: this.translateService.instant('staffManager.label.isNotApprove')
        }
    ];

    constructor(
        private baseService: BaseService,
        private router: Router,
        private translateService: TranslateService,
        private searchFormService: SearchFormService,
        public validateService: ValidateService,
        private concurrentProcessService: ConcurrentProcessService,
        private toastrService: ToastrService,
        public sessionService: SessionService,
        private downloadFileAttachService: DownloadFileAttachService
    ) {
        this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.HR_DRAFT_CONCURRENT_PROCESS}`);
    }

    ngOnInit(): void {
        this.initTable();
    }

    ngAfterViewInit() {
      this.doSearch(1);
    }

    ngOnDestroy() {
        this.subs.forEach(sub => sub.unsubscribe());
    }

    doSearch(page: number) {
        this.pagination.pageNumber = page;
        const searchParam = this.searchForm ? this.searchForm.parseOptions() : new HttpParams();
        this.isLoadingPage = true;
        this.searchFormService.getFilterResearch(UrlConstant.SEARCH_FORM_DRAFT.CONCURRENT_PROCESS, searchParam, this.pagination.getCurrentPage()).subscribe(res => {
            if (res.code === HTTP_STATUS_CODE.OK) {
                this.isDisabledApproveAllBtn = res.data.listData.every((draft: NzSafeAny) => draft.status === Constant.APPROVE.IS_APPROVE || draft.status === Constant.APPROVE.IS_NOT_APPROVE);
                if (this.tableConfig.pageSize) {
                    if (Math.ceil(res.data.count/ this.tableConfig.pageSize) >= 2) {
                        this.isDisabledApproveAllBtn = false;
                    }
                }
                res.data.listData
                    .filter((draft: NzSafeAny) => draft.status === Constant.APPROVE.IS_NOT_APPROVE)
                    .forEach((draft: NzSafeAny) => this.mapEmployeeIsNotApproved.set(draft.draftConcurrentProcessId, draft));
                this.searchResult = res.data.listData;
                this.tableConfig.total = res.data.count;
            }
          this.isLoadingPage = false;
        }, () => this.isLoadingPage = false);
    }

    doImportData() {
        this.isImportData = true;
    }

    doCloseImport(event: boolean) {
      this.isImportData = false;
      if(event) {
        this.doSearch(this.pagination.pageNumber);
      }
    }

    private initTable(): void {
        this.tableConfig = {
            headers: [
                {
                    title: 'staffManager.staffResearch.bank.table.select',
                    field: 'select',
                    tdClassList: ['text-center'], thClassList: ['text-center', 'text-nowrap'],
                    tdTemplate: this.selectRecord,
                    width: 70,
                    show: this.objFunction.approve,
                    fixed: true,
                    fixedDir: 'left'
                },
                {
                    title: 'staffManager.staffResearch.bank.table.employeeCode',
                    field: 'employeeCode',
                    tdClassList: ['text-left'], thClassList: ['text-center'],
                    fixed: true,
                    fixedDir: 'left',
                    width: 120
                },
                {
                    title: 'staffManager.staffResearch.bank.table.fullName',
                    field: 'fullName',
                    tdClassList: ['text-left'], thClassList: ['text-center'],
                    width: 150
                },
                {
                    title: 'staffManager.staffResearch.workProcess.table.suggestType',
                    field: 'inputType',
                    tdTemplate: this.suggestTypeTpl,
                    tdClassList: ['text-center'], thClassList: ['text-center'],
                    width: 150,
                },
                {
                    title: 'staffManager.staffResearch.bank.table.status',
                    field: 'status',
                    tdTemplate: this.status,
                    tdClassList: ['text-center'], thClassList: ['text-center'],
                    show: true,
                    width: 100
                },
                {
                    title: 'staffManager.staffResearch.bank.table.orgName',
                    field: 'orgName',
                    tdClassList: ['text-left'], thClassList: ['text-center'],
                    width: 200
                },
                {
                    title: 'staffManager.staffResearch.concurrentProcess.table.fromDate',
                    field: 'fromDate',
                    tdClassList: ['text-center'], thClassList: ['text-center'],
                    width: 100
                },
                {
                    title: 'staffManager.staffResearch.concurrentProcess.table.toDate',
                    field: 'toDate',
                    tdClassList: ['text-center'], thClassList: ['text-center'],
                    width: 100
                },
                {
                    title: 'staffManager.staffResearch.concurrentProcess.table.position',
                    field: 'posName',
                    tdClassList: ['text-left'], thClassList: ['text-center'],
                    width: 150
                },
                {
                    title: 'staffManager.staffResearch.concurrentProcess.table.positionGroup',
                    field: 'pgrName',
                    thClassList: ['text-center'],
                    width: 150
                },
                {
                    title: 'staffManager.staffResearch.concurrentProcess.table.positionLevel',
                    field: 'positionLevel',
                    thClassList: ['text-center'],
                    width: 100,
                    show: false
                },
                {
                    title: 'staffManager.staffResearch.concurrentProcess.table.note',
                    field: 'note',
                    tdClassList: ['text-left'], thClassList: ['text-center'],
                    width: 150
                },
                {
                    title: 'staffManager.staffResearch.allowanceHis.table.attachFile',
                    field: 'attachFileList',
                    width: 200,
                    show: true,
                    tdTemplate: this.attachFile
                },
                {
                    title: ' ',
                    field: 'action',
                    tdTemplate: this.action,
                    tdClassList: ['text-center'],
                    thClassList: ['text-center'],
                    show: this.objFunction.approve || this.objFunction.delete,
                    width: 50,
                    fixed: true,
                    fixedDir: 'right'
                }
            ],
            total: 0,
            needScroll: true,
            loading: false,
            size: 'small',
            pageSize: this.pagination.pageSize,
            pageIndex: 1
        };
    }

    approveConcurrentProcess(draftConcurrentProcessId: number) {
        this.isLoadingPage = true;
        this.concurrentProcessService.approveConcurrentProcess(draftConcurrentProcessId).subscribe(res => {
            if (res.code === HTTP_STATUS_CODE.OK) {
                this.doSearch(1);
                this.toastrService.success(this.translateService.instant('common.notification.isApprove'));
            }
            this.isLoadingPage = false;
        }, error => {
            this.isLoadingPage = false;
            this.toastrService.error(error.message);
        });
    }

    getFlagStatus(value: number) {
        return this.statusList.find(status => status.value === value)?.label;
    }

    getStatus(value: number) {
        return this.statusApproveList.find(status => status.value === value)?.label;
    }

    approveAll() {
        this.isLoadingPage = true;
        this.concurrentProcessService.approveAllConcurrentProcess(this.searchForm.getSearchForm()).subscribe(res => {
            if (res.code === HTTP_STATUS_CODE.OK) {
                this.doSearch(1);
                this.toastrService.success(this.translateService.instant('common.notification.isApprove'));
            }
            this.isLoadingPage = false;
        }, error => {
            this.toastrService.error(error.message);
            this.isLoadingPage = false;
        });
    }

    onCheckBoxChange(event: boolean, draftPersonalConcurrentProcessId: number) {
        if (event) {
            this.selectedCheckBoxNumber++;
            this.draftConcurrentProcessSet.add(draftPersonalConcurrentProcessId);
        } else {
            this.selectedCheckBoxNumber--;
            if (this.draftConcurrentProcessSet.has(draftPersonalConcurrentProcessId)) {
                this.draftConcurrentProcessSet.delete(draftPersonalConcurrentProcessId);
            }
        }
    }

    approveConcurProcessList() {
        const listId = Array.from(this.draftConcurrentProcessSet);
        this.isLoadingPage = true;
        this.concurrentProcessService.approveConcurrentProcessByListId(listId).subscribe(res => {
            if (res.code === HTTP_STATUS_CODE.OK) {
                this.doSearch(1);
                this.toastrService.success(this.translateService.instant('common.notification.isApprove'));
            }
            this.isLoadingPage = false;
        }, error => {
            this.toastrService.error(error.message);
            this.isLoadingPage = false;
        });
        this.draftConcurrentProcessSet.clear();
        this.selectedCheckBoxNumber = 0;
    }

    confirm(draftConcurrentProcessId: number) {
        this.isLoadingPage = true;
        this.concurrentProcessService.deleteDraftConcurrentProcessById(draftConcurrentProcessId).subscribe(res => {
            if (res.code === HTTP_STATUS_CODE.OK) {
                this.doSearch(1);
                this.toastrService.success(this.translateService.instant('common.notification.deleteSuccess'));
            }
            this.isLoadingPage = false;
        }, error => {
            this.isLoadingPage = false;
            this.toastrService.error(error.message);
        });
    }

    doCloseReject(){
        this.isReject = false;
        this.doSearch(1);
    }

    resetListDraftId() {
        this.listDraftId = [];
    }

    openRejectById(draftConcurrentProcessId: number) {
        this.isReject = true;
        this.id = draftConcurrentProcessId;
    }

    openRejectByList() {
        if (validateRejectByList(this.draftConcurrentProcessSet, this.mapEmployeeIsNotApproved, this.translateService, this.toastrService)) {
            this.isReject = true;
            this.isRejectList = true;
        }
    }

    onLoadPage(event: boolean) {
      this.isLoadingPage = event;
    }

    doDownloadAttach(file: NzSafeAny) {
        this.isLoadingPage = true;
        this.downloadFileAttachService.doDownloadAttachFileWithSecurity(~~file?.docId, file?.security).subscribe(res => {
            const reportFile = new Blob([res.body], { type: getTypeExport(file.fileName.split('.').pop()) });
            saveAs(reportFile, file.fileName);
            this.isLoadingPage = false;
        }, error => {
            this.toastrService.error(error.message);
            this.isLoadingPage = false;
        });
    }

}
