import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ConcurrentProcessDraftComponent} from "./concurrent-process/concurrent-process-draft.component";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {StaffManagerUiSearchFormDraftModule} from "@hcm-mfe/staff-manager/ui/search-form-draft";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {StaffManagerUiImportCommonModule} from "@hcm-mfe/staff-manager/ui/import-common";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {NzTagModule} from "ng-zorro-antd/tag";
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";
import {FormsModule} from "@angular/forms";
import {StaffManagerUiRejectCommonModule} from "@hcm-mfe/staff-manager/ui/reject-common";
import {RouterModule} from "@angular/router";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzGridModule} from "ng-zorro-antd/grid";
import {NzButtonModule} from "ng-zorro-antd/button";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
    imports: [CommonModule, SharedUiMbButtonModule, StaffManagerUiSearchFormDraftModule, TranslateModule, SharedUiMbTableWrapModule, SharedUiMbTableModule, StaffManagerUiImportCommonModule, NzDropDownModule, NzTagModule, NzCheckboxModule, FormsModule, StaffManagerUiRejectCommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: ConcurrentProcessDraftComponent
            }
        ]), NzIconModule, NzGridModule, NzButtonModule, SharedUiLoadingModule
    ],
    declarations: [ConcurrentProcessDraftComponent]
})
export class StaffManagerFeatureDraftConcurrentProcessModule {}
