import {AfterViewInit, Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { HttpParams } from '@angular/common/http';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { ToastrService } from 'ngx-toastr';

import {Constant, ConstantColor, UrlConstant, validateRejectByList} from "@hcm-mfe/staff-manager/data-access/common";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {AppFunction, CatalogModel, MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {SearchFormComponent} from "@hcm-mfe/staff-manager/ui/search-form-draft";
import {
  FamilyRelationshipService,
  SearchFormService
} from "@hcm-mfe/staff-manager/data-access/services";
import {ValidateService} from "@hcm-mfe/shared/core";
import { SessionService } from "@hcm-mfe/shared/common/store";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {FamilyRelationship} from "@hcm-mfe/staff-manager/data-access/models/draft";

@Component({
    selector: 'app-family-deductions-research',
    templateUrl: './family-relationships-draft.component.html',
    styleUrls: ['./family-relationships-draft.component.scss']
})
export class FamilyRelationshipsDraftComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('searchForm') searchForm!: SearchFormComponent;
    @ViewChild('flagStatusTmpl', { static: true }) flagStatus!: TemplateRef<NzSafeAny>;
    @ViewChild('actionTmpl', { static: true }) action!: TemplateRef<NzSafeAny>;
    @ViewChild('statusTmpl', { static: true }) status!: TemplateRef<NzSafeAny>;
    @ViewChild('selectTmpl', { static: true }) selectRecord!: TemplateRef<NzSafeAny>;

    objFunction: AppFunction;
    tableConfig!: MBTableConfig;
    searchResult: FamilyRelationship[] = [];

    isImportData = false;
    urlApiImport: string = UrlConstant.IMPORT_EXCEL.FAMILY_RELATIONSHIP;
    urlApiDownloadTemp: string = UrlConstant.IMPORT_FORM.FAMILY_RELATIONSHIP_TEMPLATE;

    subs: Subscription[] = [];
    pagination = new Pagination();

    selectedCheckBoxNumber = 0;
    draftFamilyRelationshipSet = new Set<number>();

    listDraftId: number[] = Array.from(this.draftFamilyRelationshipSet);
    isRejectList = false;
    isReject = false;
    urlRejectById: string = UrlConstant.REFUSE_APPROVE_DRAFT.FAMILY_RELATIONSHIP;
    urlRejectByList: string = UrlConstant.REFUSE_APPROVE_DRAFT.FAMILY_RELATIONSHIP_LIST;
    id?: number;

    isDisabledApproveAllBtn = false;
    btnDisableColor: string = ConstantColor.COLOR.BG_PRIMARY_LIGHT;

    tagInitializeColor: string = ConstantColor.TAG.STATUS_INITIALIZE;
    tagApprovedColor: string = ConstantColor.TAG.STATUS_APPROVED;
    tagNotApprovedColor: string = ConstantColor.TAG.STATUS_NOT_APPROVED;

    mapEmployeeIsNotApproved = new Map<number, FamilyRelationship>();

    CONSTANT_STATUS = Constant.APPROVE;
    CONSTANT_FLAG_STATUS = Constant.FLAG_STATUS;
    isLoadingPage = false;
    statusList: CatalogModel[] = [
        {
            value: 1,
            label: this.translateService.instant('staffManager.label.workIn')
        },
        {
            value: 0,
            label: this.translateService.instant('staffManager.label.workOut')
        }
    ];

    statusApproveList: CatalogModel[] = [
        {
            value: 0,
            label: this.translateService.instant('staffManager.label.isInitial')
        },
        {
            value: 1,
            label: this.translateService.instant('staffManager.label.isApprove')
        },
        {
            value: 2,
            label: this.translateService.instant('staffManager.label.isNotApprove')
        }
    ];

    constructor(
                private router: Router,
                private translateService: TranslateService,
                private searchFormService: SearchFormService,
                public validateService: ValidateService,
                private familyRelationshipService: FamilyRelationshipService,
                private toastrService: ToastrService,
                public sessionService: SessionService,
    ) {
        this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.HR_PERSONAL_INFO}`);
    }

    ngOnInit(): void {
        this.initTable();
    }

    ngAfterViewInit() {
      this.doSearch(1);
    }

    ngOnDestroy() {
        this.subs.forEach(sub => sub.unsubscribe());
    }

    doSearch(page: number) {
        this.pagination.pageNumber = page;
        this.isLoadingPage = true;
        const searchParam = this.searchForm ? this.searchForm.parseOptions() : new HttpParams();
        this.searchFormService.getFilterResearch(UrlConstant.SEARCH_FORM_DRAFT.FAMILY_RELATIONSHIP, searchParam, this.pagination.getCurrentPage()).subscribe(res => {
            if (res.code === HTTP_STATUS_CODE.OK) {
                this.isDisabledApproveAllBtn = res.data.listData.every((draft: NzSafeAny) => draft.status === Constant.APPROVE.IS_APPROVE || draft.status === Constant.APPROVE.IS_NOT_APPROVE);
                if (this.tableConfig.pageSize) {
                  if (Math.ceil(res.data.count/ this.tableConfig.pageSize) >= 2) {
                    this.isDisabledApproveAllBtn = false;
                  }
                }
                res.data.listData
                    .filter((draft: NzSafeAny) => draft.status === Constant.APPROVE.IS_NOT_APPROVE)
                    .forEach((draft: NzSafeAny) => this.mapEmployeeIsNotApproved.set(draft.draftFamilyRelationshipId, draft));
                this.searchResult = res.data.listData;
                this.tableConfig.total = res.data.count;
            }
            this.isLoadingPage = false;
        }, () => this.isLoadingPage = false);
    }

    doImportData() {
        this.isImportData = true;
    }

    private initTable(): void {
        this.tableConfig = {
            headers: [
                {
                    title: 'staffManager.staffResearch.familyDeductions.table.select',
                    field: '',
                    tdClassList: ['text-center'], thClassList: ['text-center'],
                    tdTemplate: this.selectRecord,
                    show: this.objFunction.approve,
                    fixed: true,
                    fixedDir: 'left',
                    width: 70,
                },
                {
                    title: 'staffManager.staffResearch.familyDeductions.table.employeeCode',
                    field: 'employeeCode',
                    tdClassList: ['text-left'], thClassList: ['text-center'],
                    fixed: true,
                    fixedDir: 'left',
                    width: 150,
                },
                {
                    title: 'staffManager.staffResearch.familyDeductions.table.employeeName',
                    field: 'employeeName',
                    tdClassList: ['text-nowrap'], thClassList: ['text-center'],
                    width: 150
                },
                {
                    title: 'staffManager.staffResearch.familyDeductions.table.status',
                    field: 'status',
                    tdTemplate: this.status,
                    tdClassList: ['text-center'], thClassList: ['text-center'],
                    show: true,
                    width: 150
                },
                {
                    title: 'staffManager.staffResearch.familyDeductions.table.orgName',
                    field: 'orgName',
                    tdClassList: ['text-left'], thClassList: ['text-center'],
                    width: 150
                },
                {
                    title: 'staffManager.staffResearch.familyDeductions.table.flagStatus',
                    field: 'flagStatus',
                    tdClassList: ['text-nowrap'], thClassList: ['text-center'],
                    show: false,
                    width: 150
                },
                {
                    title: 'staffManager.staffResearch.familyDeductions.table.relationTypeName',
                    field: 'relationTypeCode',
                    tdClassList: ['text-nowrap'], thClassList: ['text-center'],
                    width: 150
                },
                {
                    title: 'staffManager.staffResearch.familyDeductions.table.fullName1',
                    field: 'fullName',
                    tdClassList: ['text-nowrap'], thClassList: ['text-center'],
                    width: 150
                },
                {
                    title: 'staffManager.staffResearch.familyDeductions.table.relationStatusCode',
                    field: 'relationStatusCode',
                    tdClassList: ['text-nowrap'], thClassList: ['text-center'],
                    width: 150
                },
                {
                    title: 'staffManager.staffResearch.familyDeductions.table.dateOfBirth',
                    field: 'dateOfBirth',
                    tdClassList: ['text-center'], thClassList: ['text-center'],
                    width: 150
                },
                {
                    title: 'staffManager.staffResearch.familyDeductions.table.workOrganization',
                    field: 'workOrganization',
                    tdClassList: ['text-left'], thClassList: ['text-center'],
                    width: 150
                },
                {
                    title: 'staffManager.staffResearch.familyDeductions.table.action',
                    field: 'action',
                    tdTemplate: this.action,
                    tdClassList: ['text-center'],
                    thClassList: ['text-center'],
                    show: this.objFunction.approve || this.objFunction.delete,
                    width: 150,
                    fixed: true,
                    fixedDir: 'right'
                }
            ],
            total: 0,
            needScroll: true,
            loading: false,
            size: 'small',
            pageSize: this.pagination.pageSize,
            pageIndex: 1
        };
    }

    doCloseImport(event: boolean) {
      this.isImportData = false;
      if(event) {
        this.doSearch(this.pagination.pageNumber);
      }
    }

    approveFamilyRelationship(draftFamilyRelationshipId: number) {
        this.isLoadingPage = true;
        this.familyRelationshipService.approveFamilyRelationship(draftFamilyRelationshipId).subscribe(res => {
            if (res.code === HTTP_STATUS_CODE.OK) {
                this.doSearch(1);
                this.toastrService.success(this.translateService.instant('common.notification.isApprove'));
            }
            this.isLoadingPage = false;
        }, error => {
            this.toastrService.error(error.message);
            this.isLoadingPage = false;
        });
    }

    getFlagStatus(value: number) {
        return this.statusList.find(status => status.value === value)?.label;
    }

    getStatus(value: number) {
        return this.statusApproveList.find(status => status.value === value)?.label;
    }

    approveAll() {
        this.isLoadingPage = true;
        this.familyRelationshipService.approveAllFamilyRelationship(this.searchForm.getSearchForm()).subscribe(res=> {
            if (res.code === HTTP_STATUS_CODE.OK) {
                this.doSearch(1);
                this.toastrService.success(this.translateService.instant('common.notification.isApprove'));
            }
            this.isLoadingPage = false;
        }, error => {
            this.toastrService.error(error.message);
            this.isLoadingPage = false;
        });
    }

    onCheckBoxChange(event: boolean, draftPersonalFamilyRelationshipId: number) {
        if (event) {
            this.selectedCheckBoxNumber++;
            this.draftFamilyRelationshipSet.add(draftPersonalFamilyRelationshipId);
        } else {
            this.selectedCheckBoxNumber--;
            if (this.draftFamilyRelationshipSet.has(draftPersonalFamilyRelationshipId)) {
                this.draftFamilyRelationshipSet.delete(draftPersonalFamilyRelationshipId);
            }
        }
    }

    approveFamilyRelationshipList() {
        this.isLoadingPage = true;
        const listId = Array.from(this.draftFamilyRelationshipSet);
        this.familyRelationshipService.approveFamilyRelationshipByListId(listId).subscribe(res => {
            if (res.code === HTTP_STATUS_CODE.OK) {
                this.doSearch(1);
            }
            this.isLoadingPage = false;
        }, error => {
            this.toastrService.error(error.message);
            this.isLoadingPage = false;
        });
        this.draftFamilyRelationshipSet.clear();
        this.selectedCheckBoxNumber = 0;
    }

    confirm(draftFamilyRelationshipId: number) {
        this.isLoadingPage = true;
        this.familyRelationshipService.deleteDraftFamilyRelationshipById(draftFamilyRelationshipId).subscribe(res => {
            if (res.code === HTTP_STATUS_CODE.OK) {
                this.doSearch(1);
                this.toastrService.success(this.translateService.instant('common.notification.deleteSuccess'));
            }
            this.isLoadingPage = false;
        }, error => {
            this.toastrService.error(error.message);
            this.isLoadingPage = false;
        });
    }

    doCloseReject(){
        this.isReject = false;
        this.doSearch(1);
    }

    resetListDraftId() {
        this.listDraftId = [];
    }

    openRejectById(draftFamilyRelationshipId: number) {
        this.isReject = true;
        this.id = draftFamilyRelationshipId;
    }

    openRejectByList() {
        if (validateRejectByList(this.draftFamilyRelationshipSet, this.mapEmployeeIsNotApproved, this.translateService, this.toastrService)) {
            this.isReject = true;
            this.isRejectList = true;
        }
    }

    onLoadPage(event: boolean) {
      this.isLoadingPage = event;
    }

}
