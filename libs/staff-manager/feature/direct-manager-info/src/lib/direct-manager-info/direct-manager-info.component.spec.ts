import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectManagerInfoComponent } from './direct-manager-info.component';

describe('DirectManagerInfoComponent', () => {
  let component: DirectManagerInfoComponent;
  let fixture: ComponentFixture<DirectManagerInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DirectManagerInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectManagerInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
