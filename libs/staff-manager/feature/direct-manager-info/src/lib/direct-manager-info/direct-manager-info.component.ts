import { Component, Injector, OnInit } from '@angular/core';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import { EmployeeInfo } from '@hcm-mfe/staff-manager/data-access/models/research';
import { ManagerDirectService } from '@hcm-mfe/staff-manager/data-access/services';
import { OrganizationService } from '@hcm-mfe/model-organization/data-access/services';
import { TableHeader } from '@hcm-mfe/shared/data-access/models';

@Component({
  selector: 'app-direct-manager-info',
  templateUrl: './direct-manager-info.component.html',
  styleUrls: ['./direct-manager-info.component.scss']
})
export class DirectManagerInfoComponent extends BaseComponent implements OnInit {
  employee:EmployeeInfo;
  listTree: EmployeeInfo[] = [];
  mapChildren: Map<number, Array<EmployeeInfo>>;
  mapOfExpandedDataTree: { [key: string]: EmployeeInfo[] };
  mapChangeIndex: Map<number, EmployeeInfo>;
  tableHeaders:TableHeader[] = [];
  constructor(injector:Injector,  private readonly managerDirectService: ManagerDirectService)
  {
    super(injector);
    this.employee = {
    empCode: '',
    empId: 0,
    employeeCode: '',
    employeeId: 0,
    jobName: '',
    fullName: '',
    levelName: '',
    orgName: '',
    positionName: '',
    positionId: ''
  };
    this.mapChangeIndex = new Map();
    this.mapChildren = new Map();
    this.mapOfExpandedDataTree = {};
  }

  ngOnInit(): void {
    this.getData();
    this.initTable();
  }
  initTable(){
    this.tableHeaders = [
      { title:'staffManager.table.unit',
        show: true,
        field:'orgFullName',
        width: 300,
        needEllipsis:true
      },
      { title:'Job',
        show: true,
        field:'jobName',
        width: 200,
      },
      { title:'staffManager.table.manager',
        show: true,
        field: 'managerFullName',
        width: 200
      },
      { title:'staffManager.table.unitManager',
        show: true,
        field:'managerOrgFullName',
        width: 300,
        needEllipsis:true
      },
      { title:'staffManager.table.jobManager',
        show: true,
        field:'managerJobName',
        width: 200,
      },
      { title:'staffManager.table.directManager',
        show: true,
        field:'indirectManagerFullName',
        width: 200
      },
      { title:'staffManager.table.unitDirectManager',
        show: true,
        field:'indirectManagerOrgFullName',
        width: 300,
        needEllipsis:true
      },
      { title:'staffManager.table.jobDirectManager',
        show: true,
        field:'indirectManagerJobName',
        needEllipsis:true,
        width: 200
      },
    ]
  }
  changeHeaders(event:any){
    this.tableHeaders = event.filter((item:any) => item.isChecked);
  }

  getData(){
    this.isLoading = true;
    this.managerDirectService.getDirectManagerInfo().subscribe(data => {
      this.employee = data.data?.employee;
      this.employee.positionName = data.data?.employee?.posName;
      this.listTree = data.data?.employeeLowers || [];
      if(this.listTree.length > 0 && this.listTree){
        this.listTree.forEach((item,index) => {
          item.expand = false;
          item.key = `${index}`;
          item.level = 0;
          this.mapOfExpandedDataTree[String(index)] =[item];
        });
        this.mapOfExpandedDataTree['0'][0].label= 'Nhân viên';
      }
      this.isLoading = false;
    }, error => {
      this.isLoading = false;
      this.toastrCustom.error(error.message);
    })
  }

  convertOrgTree(orgTree:EmployeeInfo[], parent?:EmployeeInfo):EmployeeInfo[]{
    for(let i = 0; i < orgTree.length; i++){
      orgTree[i].expand = false;
      orgTree[i].key = parent ?  `${parent?.key} - ${i}` : '0';
      orgTree[i].level = parent ? parent.level + 1 : 0;
      orgTree[i].parent = parent;
    }
    return orgTree;
  }
  collapseTree(index:number, data: EmployeeInfo, event: boolean): void {
    if(!event){
      data.expand = false;
    } else if(!data.loadChildren) {
      this.isLoading = true;
      this.managerDirectService.getSubEmp(data.employeeId).subscribe(res => {
        data.loadChildren = true;
        const listChildren = this.convertOrgTree(res.data, data);
        this.mapChildren.set(data.employeeId, res.data);
        if(res.data.length > 0) {
          this.mapOfExpandedDataTree[String(data.key)].splice(index + 1, 0, ...listChildren);
        }
        this.isLoading = false;
      }, error => {
        this.isLoading = false;
        this.toastrCustom.error(error.message);
      })
    }
  }
  trackBy(index:number, item:EmployeeInfo){
    return item.employeeId;
  }
}
