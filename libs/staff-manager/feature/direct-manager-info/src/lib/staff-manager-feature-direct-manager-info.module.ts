import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { SharedUiMbDatePickerModule } from '@hcm-mfe/shared/ui/mb-date-picker';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { DirectManagerInfoComponent } from './direct-manager-info/direct-manager-info.component';
import { PopupService } from '@hcm-mfe/shared/ui/popup';
import { NzTableModule } from 'ng-zorro-antd/table';
import { SharedUiMbHeaderFilterModule } from '@hcm-mfe/shared/ui/mb-header-filter';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';

@NgModule({
  imports: [CommonModule, NzGridModule, NzCheckboxModule, SharedUiMbHeaderFilterModule,
    RouterModule.forChild([
      {
        path: '',
        component: DirectManagerInfoComponent
      }]), TranslateModule, FormsModule, NzDividerModule, SharedUiMbDatePickerModule, SharedUiMbSelectModule, SharedUiMbButtonModule, SharedUiLoadingModule, NzTableModule, NzToolTipModule],
  exports: [DirectManagerInfoComponent],
  declarations: [DirectManagerInfoComponent],
  providers:[PopupService]
})
export class StaffManagerFeatureDirectManagerInfoModule {}
