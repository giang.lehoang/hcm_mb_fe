import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import {HTTP_STATUS_CODE, Mode} from "@hcm-mfe/shared/common/constants";
import {
  RewardHisInfoService,
  ShareDataService, StaffInfoService,
} from "@hcm-mfe/staff-manager/data-access/services";
import {Constant} from "@hcm-mfe/staff-manager/data-access/common";
import {BaseResponse, Category} from "@hcm-mfe/shared/data-access/models";
import {RewardHistory} from "@hcm-mfe/staff-manager/data-access/models/info";

@Component({
  selector: 'app-add-reward-his',
  templateUrl: './add-reward-his.component.html',
  styleUrls: ['./add-reward-his.component.scss'],
})
export class AddRewardHisComponent implements OnInit {
  data = new RewardHistory();
  mode = Mode.ADD;

  isSubmitted = false;
  form: FormGroup;

  listDecisionLevel: Category[] = [];
  listMethod: Category[] = [];
  listTitle: Category[] = [];
  isLoadingPage = false;

  constructor(
    private fb: FormBuilder,
    private modalRef: NzModalRef,
    private toastService: ToastrService,
    private modalService: NzModalService,
    private translate: TranslateService,
    private shareDataService: ShareDataService,
    private staffInfoService: StaffInfoService,
    private rewardHisInfoService: RewardHisInfoService
  ) {
    this.form = this.fb.group({
      rewardRecordId: [null],
      employeeId: [null],
      year: [null, Validators.required],
      signedDate: null,
      decisionLevelCode: [null, Validators.required],
      methodCode: [null, Validators.required],
      titleCode: [null, Validators.required],
      reason: [null, [Validators.maxLength(1000), Validators.required]],
      documentNumber: [null, [Validators.maxLength(50)]],
      fromDate: null,
      signer: null,
      signerPosition: null,
      note: [null, [Validators.maxLength(500)]] // TODO: API chưa trả về note
    });

    this.getDecisionLevelList();
    this.getMethodList();
    this.getTitleList();
  }

  get f() {
    return this.form.controls;
  }

  ngOnInit(): void {
    if (this.mode === Mode.EDIT)
      this.patchValueInfo();
    this.f['employeeId'].setValue(this.shareDataService.getEmployee().employeeId);
  }

  patchValueInfo() {
    this.isLoadingPage = true;
    this.rewardHisInfoService.getRecord(this.data.rewardRecordId).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.form.patchValue(res.data);
        if (this.data.year)
          this.f['year'].setValue(moment(res.data.year, 'YYYY').toDate());
        if (this.data.signedDate)
          this.f['signedDate'].setValue(moment(res.data.signedDate, 'DD/MM/YYYY').toDate());
        if (this.data.fromDate)
          this.f['fromDate'].setValue(moment(res.data.fromDate, 'DD/MM/YYYY').toDate());
      } else {
        this.toastService.error(res?.message);
      }
      this.isLoadingPage = false;
    }, () => {
      this.modalRef.close();
      this.isLoadingPage = false;
    });
  }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      this.isLoadingPage = true;
      const request: RewardHistory = this.form.value;
      request.year = moment(this.f['year'].value).format('YYYY');
      request.signedDate = this?.f?.['signedDate']?.value ? moment(this.f['signedDate'].value).format('DD/MM/YYYY') : null;
      request.fromDate = this?.f?.['fromDate']?.value ? moment(this.f['fromDate'].value).format('DD/MM/YYYY') : null;

      this.rewardHisInfoService.saveRecord(request).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.toastService.success(this.translate.instant('common.notification.updateSuccess'));
          this.modalRef.close({ refresh: true });
        } else {
          this.toastService.error(this.translate.instant('common.notification.updateError') + ':' + res.message);
        }
        this.isLoadingPage = false;
      });
    }
  }

  getDecisionLevelList() {
    this.staffInfoService.getCatalog(Constant.CATALOGS.CAP_QD_KHENTHUONG).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) this.listDecisionLevel = res.data;
    });
  }

  getMethodList() {
    this.staffInfoService.getCatalog(Constant.CATALOGS.HINHTHUC_KHENTHUONG).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) this.listMethod = res.data;
    });
  }

  getTitleList() {
    this.staffInfoService.getCatalog(Constant.CATALOGS.LOAI_KHENTHUONG).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) this.listTitle = res.data;
    });
  }

}
