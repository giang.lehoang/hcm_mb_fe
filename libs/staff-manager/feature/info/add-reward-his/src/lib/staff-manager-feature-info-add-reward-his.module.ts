import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AddRewardHisComponent} from "./add-reward-his/add-reward-his.component";
import {NzFormModule} from "ng-zorro-antd/form";
import {ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
    imports: [CommonModule, NzFormModule, ReactiveFormsModule, SharedUiMbDatePickerModule, TranslateModule, SharedUiMbSelectModule, SharedUiMbInputTextModule, SharedUiLoadingModule],
  declarations: [AddRewardHisComponent],
  exports: [AddRewardHisComponent]
})
export class StaffManagerFeatureInfoAddRewardHisModule {}
