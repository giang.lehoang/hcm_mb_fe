import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { saveAs } from 'file-saver';
import {HTTP_STATUS_CODE, Mode} from "@hcm-mfe/shared/common/constants";
import {
  DownloadFileAttachService, ProjectHisInfoService,
  ShareDataService,
} from "@hcm-mfe/staff-manager/data-access/services";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";
import {DateValidator} from "@hcm-mfe/shared/common/validators";
import {beforeUploadFile, getTypeExport, Utils} from "@hcm-mfe/shared/common/utils";
import {Job, Project, ProjectHistory} from "@hcm-mfe/staff-manager/data-access/models/info";
import {NzSafeAny} from "ng-zorro-antd/core/types";

@Component({
  selector: 'app-add-project-his-info',
  templateUrl: './add-project-his-info.component.html',
  styleUrls: ['./add-project-his-info.component.scss'],
})
export class AddProjectHisInfoComponent implements OnInit {
  data: ProjectHistory = new ProjectHistory();
  mode = Mode.ADD;

  isSubmitted = false;
  form: FormGroup;
  fileList: NzUploadFile[] = [];

  listProject: Project[] = [];

  listJob: Job[] = [];
  docIdsDelete: number[] = [];
  isLoadingPage = false;

  constructor(
    private fb: FormBuilder,
    private modalRef: NzModalRef,
    private toastService: ToastrService,
    private modalService: NzModalService,
    private translate: TranslateService,
    private shareDataService: ShareDataService,
    private projectHisInfoService: ProjectHisInfoService,
    private downloadFileAttachService: DownloadFileAttachService,

    ) {
    this.form = fb.group({
      projectMemberId: [null],
      employeeId: [null],
      fromDate: [null, [Validators.required]], // Từ ngày
      toDate: [null], // Đến ngày
      projectId: [null, [Validators.required]], // Dự án đã tham gia vào
      jobId: [null, [Validators.required]], //Vai trò của dự án
      note: [null, [Validators.maxLength(500)]], // Ghi chú
      managerId: [null] // Người quản lý
    }, {
      validators: [
        DateValidator.validateRangeDate('fromDate', 'toDate', 'rangeDateError')
      ]
    });
    this.getJobList();
    this.getProjectList();
  }

  get f() { return this.form.controls; }
  ngOnInit(): void {
    if (this.mode === Mode.EDIT)
      this.patchValueInfo().then();
   this.f['employeeId'].setValue(this.shareDataService.getEmployee().employeeId)
  }

  async patchValueInfo() {
    this.form.patchValue(this.data);
    this.f['fromDate'].setValue(Utils.convertDateToFillForm(this.data.fromDate));
    this.f['toDate'].setValue(Utils.convertDateToFillForm(this.data.toDate));
    let projectMemberDetail: NzSafeAny;
    this.isLoadingPage = true;
    await this.projectHisInfoService.getProjectMemberDetailWithAsync(this.data.projectMemberId).then(res => {
        projectMemberDetail = res;
    });
    this.projectHisInfoService.getManagerInfo(projectMemberDetail.data.managerId).subscribe(res => {
      if(res) {
        this.f['managerId'].setValue(res.data);
        this.isLoadingPage = false;
      }
    });
    if (this.data?.attachFileList && this.data?.attachFileList.length > 0) {
      this.data?.attachFileList.forEach((item) => {
        this.fileList.push({
          uid: item.docId,
          name: item.fileName,
          url: item.security,
          status: 'done'
        });
      });
      this.fileList = [...this.fileList];
    }
  }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      this.isLoadingPage = true;
      const request:ProjectHistory = this.form.value;
      request.fromDate = this?.f?.['fromDate']?.value ? moment(this.f['fromDate'].value).format('DD/MM/YYYY') : null;
      request.toDate = this?.f?.['toDate']?.value ? moment(this.f['toDate'].value).format('DD/MM/YYYY') : null;
      if(this.f['managerId'].value != null){
        request.managerId = this.f['managerId'].value.employeeId;
      }
      request.docIdsDelete = this.docIdsDelete;


      const formData = new FormData();
      formData.append("data", new Blob([JSON.stringify(request)], {
        type: 'application/json',
      }));

      this.fileList.forEach((nzFile: NzSafeAny) =>{
        formData.append('files', nzFile)
      });

      this.projectHisInfoService.saveRecord(formData).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.toastService.success(this.translate.instant('common.notification.updateSuccess'));
          this.modalRef.close({refresh:true})
        } else {
          this.toastService.error(this.translate.instant('common.notification.updateError') + ":" + res.message);
        }
        this.isLoadingPage = false;
      })
    }
  }

  getProjectList() {
    this.projectHisInfoService.getProjectList().subscribe((res: BaseResponse) =>{
      if (res.code === HTTP_STATUS_CODE.OK) this.listProject = res.data;
    })
  }

  getJobList() {
    this.projectHisInfoService.getJobsList().subscribe((res: BaseResponse) =>{
      if (res.code === HTTP_STATUS_CODE.OK) this.listJob = res.data;
    })
  }

  beforeUpload = (file: NzUploadFile): boolean => {
    this.fileList =  beforeUploadFile(file, this.fileList, 15000000, this.toastService, this.translate, 'common.notification.errorSizeFile')
    return false;
  };

  downloadFile = (file: NzUploadFile) => {
    if (file?.uid) {
      this.downloadFileAttachService.doDownloadAttachFileWithSecurity(~~file?.uid, file?.url).subscribe(res => {
        const reportFile = new Blob([res.body], { type: getTypeExport(file.name.split('.').pop()) });
        saveAs(reportFile, file.name);
      });
    }
  }

  removeFile = (file: NzUploadFile) => {
    this.docIdsDelete.push(Number(file.uid));
    const index = this.fileList.indexOf(file);
    this.fileList.splice(index, 1);
    return true;
  }
}
