import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AddProjectHisInfoComponent} from "./add-project-his-info/add-project-his-info.component";
import {NzFormModule} from "ng-zorro-antd/form";
import {ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/employee-data-picker";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {NzUploadModule} from "ng-zorro-antd/upload";
import {NzIconModule} from "ng-zorro-antd/icon";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
    imports: [CommonModule, NzFormModule, ReactiveFormsModule, SharedUiMbDatePickerModule, TranslateModule, SharedUiMbSelectModule, SharedUiEmployeeDataPickerModule, SharedUiMbInputTextModule, NzUploadModule, NzIconModule, SharedUiLoadingModule],
  declarations: [AddProjectHisInfoComponent],
  exports: [AddProjectHisInfoComponent],
})
export class StaffManagerFeatureInfoAddProjectHisInfoModule {}
