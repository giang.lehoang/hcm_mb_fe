import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {
  ContactInfoService,
  ShareDataService,
} from "@hcm-mfe/staff-manager/data-access/services";
import {AppFunction} from "@hcm-mfe/shared/data-access/models";
import { SessionService } from "@hcm-mfe/shared/common/store";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {BaseResponse, ContactInfo, ContactInfoGroup} from "@hcm-mfe/staff-manager/data-access/models/info";

@Component({
  selector: 'app-contact-information',
  templateUrl: './contact-information.component.html',
  styleUrls: ['./contact-information.component.scss']
})
export class ContactInformationComponent implements OnInit, OnDestroy {
  objFunction: AppFunction;
  items?: ContactInfoGroup;
  subs: Subscription[] = [];
  response: BaseResponse = new BaseResponse();
  contactInfo?: ContactInfo;
  employeeId: number = 1;

  constructor(
    private shareDataService: ShareDataService,
    private contactInfoServices: ContactInfoService,
    private sessionService: SessionService,
  ) {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.HR_ALLOWANCE_PROCESS}`);
  }

  ngOnInit(): void {
    this.subs.push(
      this.shareDataService.employee$.subscribe(employee => {
        this.employeeId = employee.employeeId;
        this.getContactInfo(this.employeeId);
      })
    );
  }

  ngOnDestroy() {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  getContactInfo(employeeId: number) {
    this.subs.push(
      this.contactInfoServices.getContactInfo(employeeId).subscribe(res => {
        this.response = res;
        this.contactInfo = this.response?.data;
        this.items = {employeeId: employeeId, data: this.response?.data};
      })
    );
  }

  refresh() {
    this.getContactInfo(this.employeeId);
  }

}
