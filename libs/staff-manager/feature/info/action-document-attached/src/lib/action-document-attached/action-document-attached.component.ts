import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {NzModalRef} from 'ng-zorro-antd/modal';
import {ToastrService} from 'ngx-toastr';
import {Subscription} from 'rxjs';
import {NzUploadFile} from "ng-zorro-antd/upload";
import {saveAs} from 'file-saver';
import {DataSource, DocumentAttachment, LookupValues} from "@hcm-mfe/staff-manager/data-access/models/info";
import {HTTP_STATUS_CODE, Mode} from "@hcm-mfe/shared/common/constants";
import {
  DocumentAttachmentService, DownloadFileAttachService,
  ShareDataService,
  StaffInfoService
} from "@hcm-mfe/staff-manager/data-access/services";
import {Constant} from "@hcm-mfe/staff-manager/data-access/common";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";
import {beforeUploadFile, getTypeExport} from "@hcm-mfe/shared/common/utils";
import {NzSafeAny} from "ng-zorro-antd/core/types";

@Component({
  selector: 'app-action-document-attached',
  templateUrl: './action-document-attached.component.html',
  styleUrls: ['./action-document-attached.component.scss']
})
export class ActionDocumentAttachedComponent implements OnInit {
  mode = Mode.ADD;
  data = new DocumentAttachment();
  isSubmitted = false;
  form: FormGroup;
  fileList: NzUploadFile[] = [];
  documentTypeList: LookupValues[] = [];
  subs: Subscription[] = [];
  employeeId = 1;
  hardDocumentList: DataSource[] = [
    {
      value: 0,
      label: this.translate.instant('staffManager.documentAttachedInformation.label.no')
    },
    {
      value: 1,
      label: this.translate.instant('staffManager.documentAttachedInformation.label.yes')
    }
  ];
  docIdsDelete: number[] = [];
  isLoadingPage = false;

  constructor(
    private fb: FormBuilder,
    private shareDataService: ShareDataService,
    private staffInfoService: StaffInfoService,
    private documentAttachmentService: DocumentAttachmentService,
    private toastService: ToastrService,
    private translate: TranslateService,
    private downloadFileAttachService: DownloadFileAttachService,
    private modalRef: NzModalRef
  ) {
    this.form = this.fb.group({
      profileTypeCode: [null, Validators.required],
      isHardDocument: 0,
      note: [null],
      employeeId: [null]
    });
    this.getDocumentTypeList();
  }

  get f() {
    return this.form.controls;
  }

  ngOnInit(): void {
    if (this.mode === Mode.EDIT)
      this.patchValueInfo();
    this.f['employeeId'].setValue(this.shareDataService.getEmployee().employeeId);
  }

  patchValueInfo() {
    this.isLoadingPage = true;
    this.documentAttachmentService.getRecord(this.data.employeeProfileId).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.data = res?.data;
        this.form.patchValue(this.data);
        if (this.data.profileTypeCode)
          this.f['profileTypeCode'].setValue(this.data.profileTypeCode);
        if (this.data.isHardDocument)
          this.f['isHardDocument'].setValue(this.data.isHardDocument);
        if (this.data.note)
          this.f['note'].setValue(this.data.note);

        if (this.data?.attachFileList && this.data?.attachFileList.length > 0) {
          this.data?.attachFileList.forEach((item) => {
            this.fileList.push({
              uid: item.docId,
              name: item.fileName,
              url: item.security,
              status: 'done'
            });
          });
        }
        this.fileList = [...this.fileList];
      } else {
        this.toastService.error(res?.message);
      }
      this.isLoadingPage = false;
    }, () => {
      this.modalRef.close();
      this.isLoadingPage = false;
    })
  }

  getDocumentTypeList() {
    this.staffInfoService.getCatalog(Constant.CATALOGS.LOAI_HO_SO).subscribe(res => {
      const response: BaseResponse = res;
      this.documentTypeList = response.data;
    });
  }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      this.isLoadingPage = true;
      const value: DocumentAttachment = Object.assign({}, this.data, this.form.value);
      value.docIdsDelete = this.docIdsDelete;
      const formData = new FormData();
      formData.append('data', new Blob([JSON.stringify(value)], {
        type: 'application/json'
      }));

      this.fileList.forEach((nzFile: NzSafeAny) => {
        formData.append('files', nzFile);
      });

      this.documentAttachmentService.saveDocumentAttachment(formData).subscribe(res => {
        if (res && res.code === 200) {
          this.toastService.success(this.translate.instant('common.notification.updateSuccess'));
          this.modalRef.close({refresh: true});
        } else {
          this.toastService.error(this.translate.instant('common.notification.updateError') + ': ' + res?.message);
          this.modalRef.close({refresh: false});
        }
        this.isLoadingPage = false;
      }, () => {
        this.toastService.error(this.translate.instant('common.notification.updateError'));
        this.modalRef.close({refresh: false});
        this.isLoadingPage = false;
      });
    }
  }

  beforeUpload = (file: NzUploadFile): boolean => {
    this.fileList = beforeUploadFile(file, this.fileList, 15000000, this.toastService, this.translate, 'common.notification.fileExtensionInvalidRarZipPdf', ".RAR", ".ZIP", ".PDF")
    return false;
  }

  downloadFile = (file: NzUploadFile) => {
    if (file?.uid) {
      this.downloadFileAttachService.doDownloadAttachFileWithSecurity(~~file?.uid, file?.url).subscribe(res => {
        const reportFile = new Blob([res.body], {type: getTypeExport(file.name.split('.').pop())});
        saveAs(reportFile, file.name);
      });
    }
  }

  removeFile = (file: NzUploadFile) => {
    this.docIdsDelete.push(Number(file.uid));
    const index = this.fileList.indexOf(file);
    this.fileList.splice(index, 1);
    return true;
  }

}
