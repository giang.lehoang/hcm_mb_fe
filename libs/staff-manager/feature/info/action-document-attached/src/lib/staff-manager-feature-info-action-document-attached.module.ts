import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ActionDocumentAttachedComponent} from "./action-document-attached/action-document-attached.component";
import {NzFormModule} from "ng-zorro-antd/form";
import {ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {NzUploadModule} from "ng-zorro-antd/upload";
import {NzIconModule} from "ng-zorro-antd/icon";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
    imports: [CommonModule, NzFormModule, ReactiveFormsModule, SharedUiMbSelectModule, TranslateModule, SharedUiMbInputTextModule, NzUploadModule, NzIconModule, SharedUiLoadingModule],
  declarations: [ActionDocumentAttachedComponent],
  exports: [ActionDocumentAttachedComponent],
})
export class StaffManagerFeatureInfoActionDocumentAttachedModule {}
