import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NzModalRef, NzModalService} from 'ng-zorro-antd/modal';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {NzUploadFile} from 'ng-zorro-antd/upload';
import {ConcurrentProcessInfo} from "@hcm-mfe/staff-manager/data-access/models/info";
import {HTTP_STATUS_CODE, Mode} from "@hcm-mfe/shared/common/constants";
import {BaseResponse, CatalogModel, Category, OrgInfo} from "@hcm-mfe/shared/data-access/models";
import {
    ConcurrentProcessInfoService,
    ShareDataService,
    StaffInfoService
} from "@hcm-mfe/staff-manager/data-access/services";
import {DateValidator} from "@hcm-mfe/shared/common/validators";
import {Constant} from "@hcm-mfe/staff-manager/data-access/common";
import {Utils} from "@hcm-mfe/shared/common/utils";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {Subscription} from "rxjs";

@Component({
    selector: 'app-edit-concurrent-process-info',
    templateUrl: './edit-concurrent-process-info.component.html',
    styleUrls: ['./edit-concurrent-process-info.component.scss'],
})
export class EditConcurrentProcessInfoComponent implements OnInit, OnDestroy {
    data: ConcurrentProcessInfo | undefined;
    mode = Mode.ADD;

    listEmpType: Category[] = [];
    listPosition = [];
    docIdsDelete: number[] = [];
    employeeId: number | undefined;

    fileList: NzUploadFile[] = [];
    isSubmitted = false;
    form: FormGroup = this.fb.group([]);
    isLoadingPage = false;
    listPositionGroup: CatalogModel[] = [];
    listPositionLevel: CatalogModel[] = [];
    isLoadEdit = false;
    toDateOfModelEdit = '';
    subs: Subscription[] = [];
    cloneFile!: number;


    constructor(
        private fb: FormBuilder,
        private modalRef: NzModalRef,
        private toastService: ToastrService,
        private modalService: NzModalService,
        private translate: TranslateService,
        private shareDataService: ShareDataService,
        private staffInfoService: StaffInfoService,
        private concurrentProcessInfoService: ConcurrentProcessInfoService
    ) {
    }

    ngOnInit(): void {
        this.employeeId = this.shareDataService.getEmployee().employeeId;
        this.initForm();
        this.getListEmployeeType();
        this.onChanges();
        if (this.mode === Mode.EDIT) {
            this.getModelEdit();
        }
        this.getPositionLevelList();
        this.getListPositionGroup();
    }

    get f() {
        return this.form?.controls;
    }

    initForm() {
        this.form = this.fb.group({
            fromDate: [null, Validators.required],
            toDate: [null],
            documentNo: [null],
            signedDate: [null],
            empTypeCode: [null, Validators.required],
            org: [null, Validators.required],
            positionId: [null, Validators.required],
            note: [null],
            positionLevel: [null], // Bậc chức danh
            positionGroupId: [null,  Validators.required], // Vị trí chức danh
        }, {
            validators: [
                DateValidator.validateRangeDate('fromDate', 'toDate', 'rangeDateError')
            ]
        });
    }

    getModelEdit() {
        if (this.data) {
            if (this.data.concurrentProcessId && this.data.concurrentProcessId > 0) {
                this.getDataById();
                this.cloneFile = 1;
            } else {
                this.getDraftDataById();
            }
        }
    }

    getDataById() {
        this.isLoadingPage = true;
        if (this.data?.concurrentProcessId) {
            this.subs.push(
                this.concurrentProcessInfoService.getRecord(this.data.concurrentProcessId).subscribe({
                    next: (res: BaseResponse) => {
                        if (res.code === HTTP_STATUS_CODE.OK) {
                            this.data = res?.data;
                            this.doSomeWorkAfterGetEditModel();
                        } else {
                            this.toastService.error(res?.message);
                        }
                        this.isLoadingPage = false;
                    },
                    error: (err) => {
                        this.toastService.error(err?.message);
                        this.isLoadingPage = false;
                    }
                })
            );
        }
    }

    getDraftDataById() {
        this.isLoadingPage = true;
        if (this.data?.draftConcurrentProcessId) {
            this.subs.push(
                this.concurrentProcessInfoService.getDraftDataById(this.data.draftConcurrentProcessId).subscribe({
                    next: (res: BaseResponse) => {
                        if (res.code === HTTP_STATUS_CODE.OK) {
                            this.data = res?.data;
                            this.doSomeWorkAfterGetEditModel();
                        } else {
                            this.toastService.error(res?.message);
                        }
                        this.isLoadingPage = false;
                    },
                    error: (err) => {
                        this.toastService.error(err?.message);
                        this.isLoadingPage = false;
                    }
                })
            );
        }
    }

    doSomeWorkAfterGetEditModel() {
        this.toDateOfModelEdit = this.data?.toDate ? this.data.toDate : '';
        this.patchValueInfo();
    }

    getListEmployeeType() {
        this.staffInfoService.getCatalog(Constant.CATALOGS.DOI_TUONG_CV).subscribe((res: BaseResponse) => {
            if (res.code === HTTP_STATUS_CODE.OK) this.listEmpType = res.data;
        });
    }

    getPositionLevelList() {
        this.staffInfoService.getCatalog(Constant.CATALOGS.LEVEL_NV).subscribe(
            (res: BaseResponse) => {
                if (res.code === HTTP_STATUS_CODE.OK) this.listPositionLevel = res.data;
            }
        );
    }

    getListPositionGroup() {
        this.staffInfoService.getPositionGroup(Constant.TYPE_CODE.POSITION).subscribe(res => {
            if (res.code === HTTP_STATUS_CODE.OK) {
                this.listPositionGroup = res.data.map((item: NzSafeAny) => {
                    item.value = item.pgrId;
                    item.label = item.pgrName;
                    return item;
                });
            } else {
              this.toastService.error(res?.message);
            }
        }, error => this.toastService.error(error.message));
    }

    onChanges() {
        if (this.f) {
            this.f['org'].valueChanges.subscribe((value: OrgInfo) => {
                if (value) {
                    if (this.f) {
                        this.f['positionId'].setValue(null, { emitEvent: false, onlySelf: true });
                    }
                    this.listPosition = [];
                    this.concurrentProcessInfoService.getMBPositions(value.orgId).subscribe((res: BaseResponse) => {
                        if (res.code === HTTP_STATUS_CODE.OK) this.listPosition = res.data;
                    });
                }
            });
        }
    }

    patchValueInfo() {
        const concurrentProcess: ConcurrentProcessInfo | undefined = this.data;
        this.form.patchValue({
            fromDate: Utils.convertDateToFillForm(concurrentProcess?.fromDate),
            toDate: Utils.convertDateToFillForm(concurrentProcess?.toDate),
            org: concurrentProcess?.org,
            documentNo: concurrentProcess?.documentNo,
            signedDate: Utils.convertDateToFillForm(concurrentProcess?.signedDate),
            empTypeCode: concurrentProcess?.empTypeCode,
            note: concurrentProcess?.note,
            positionLevel: concurrentProcess?.positionLevel,
            positionGroupId: concurrentProcess?.positionGroupId,
        });
        this.f['positionId']?.patchValue(concurrentProcess?.positionId);
        // file
        if (concurrentProcess?.attachFileList && concurrentProcess?.attachFileList.length > 0) {
            concurrentProcess?.attachFileList.forEach((item) => {
                this.fileList.push({
                    uid: item.docId,
                    name: item.fileName,
                    url: item.security,
                    status: 'done'
                });
            });
            this.fileList = [...this.fileList];
        }
        this.isLoadEdit = true;
    }

    changePosition(event: NzSafeAny) {
        if (event) {
            if (event.itemSelected?.positionGroupId && !this.isLoadEdit) {
                this.f['positionGroupId'].setValue(event.itemSelected?.positionGroupId);
            }
        }
    }

    save() {
        this.isSubmitted = true;
        if (this.form.valid) {
            this.isLoadingPage = true;
            const request: ConcurrentProcessInfo = this.form.value;
            request.concurrentProcessId = this?.data?.concurrentProcessId;
            request.employeeId = this.employeeId;
            request.fromDate = Utils.convertDateToSendServer(this.f['fromDate'].value);
            request.toDate = this.toDateOfModelEdit;
            request.toDate = Utils.convertDateToSendServer(this.f['toDate'].value);
            request.signedDate = Utils.convertDateToSendServer(this.f['signedDate'].value);
            request.organizationId = this.f?.['org']?.value ? this.f?.['org'].value['orgId'] : null;
            request.docIdsDelete = this.docIdsDelete;
            request.draftConcurrentProcessId = this.data?.draftConcurrentProcessId;
            request.isCloneFile = this.cloneFile;

            const formData = new FormData();
            formData.append('data', new Blob([JSON.stringify(request)], {
                type: 'application/json'
            }));

            this.fileList.forEach((nzFile: NzSafeAny) => {
                formData.append('files', nzFile);
            });

            this.concurrentProcessInfoService.saveRecord(formData).subscribe((res: BaseResponse) => {
                if (res.code === HTTP_STATUS_CODE.OK) {
                    this.toastService.success(this.translate.instant(this.mode === Mode.EDIT ? 'common.notification.updateSuccess' : 'common.notification.addSuccess'));
                    this.modalRef.close({ refresh: true });
                } else {
                    this.toastService.error(this.translate.instant(this.mode === Mode.EDIT ? 'common.notification.updateError' : 'common.notification.addError') + ': ' + res?.message);
                }
            }, error => {
                this.toastService.error(this.translate.instant(this.mode === Mode.EDIT ? 'common.notification.updateError' : 'common.notification.addError') + ': ' + error.message);
                this.isLoadingPage = false;
            });
        }
    }

    ngOnDestroy(): void {
        this.subs?.forEach(sub => sub?.unsubscribe());
    }

}
