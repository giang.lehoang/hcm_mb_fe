import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
    EditConcurrentProcessInfoComponent
} from "./edit-concurrent-process-info/edit-concurrent-process-info.component";
import {ReactiveFormsModule} from "@angular/forms";
import {NzGridModule} from "ng-zorro-antd/grid";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiOrgDataPickerModule} from "@hcm-mfe/shared/ui/org-data-picker";
import {SharedUiExtendFormItemModule} from "@hcm-mfe/shared/ui/extend-form-item";
import {SharedUiMbEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/mb-employee-data-picker";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiMbUploadModule} from "@hcm-mfe/shared/ui/mb-upload";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {NzFormModule} from "ng-zorro-antd/form";

@NgModule({
  imports: [CommonModule, ReactiveFormsModule, NzGridModule, SharedUiMbDatePickerModule, TranslateModule, SharedUiMbInputTextModule, SharedUiMbSelectModule, SharedUiOrgDataPickerModule, SharedUiExtendFormItemModule, SharedUiMbEmployeeDataPickerModule, SharedUiMbButtonModule, SharedUiMbUploadModule, SharedUiLoadingModule, NzFormModule],
  declarations: [EditConcurrentProcessInfoComponent]
})
export class StaffManagerFeatureInfoEditConcurrentProcessInfoModule {}
