import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import { Observable, Observer, Subscription } from 'rxjs';
import {NzUploadFile, NzUploadXHRArgs} from 'ng-zorro-antd/upload';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import {
  PersonalInfoService,
  ShareDataService,
} from "@hcm-mfe/staff-manager/data-access/services";
import {AppFunction} from "@hcm-mfe/shared/data-access/models";
import { SessionService } from "@hcm-mfe/shared/common/store";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {PersonalInfo} from "@hcm-mfe/staff-manager/data-access/models/info";

@Component({
  selector: 'app-brief-description',
  templateUrl: './brief-description.component.html',
  styleUrls: ['./brief-description.component.scss']
})
export class BriefDescriptionComponent implements OnInit, OnDestroy {
  objFunction: AppFunction;
  personalInfo?: PersonalInfo;
  subs: Subscription[] = [];
  employeeId?: number;
  avtBase64: string | any | ArrayBuffer = 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';
  avtIsLoading = true;
  fileAvt: NzUploadXHRArgs | any;
  formDataAvt: FormData | any;
  showTmp = false;

  @Output() isEmpInfoReady: EventEmitter<PersonalInfo> = new EventEmitter<PersonalInfo>();

  constructor(
    private shareDataService: ShareDataService,
    private personalInfoService: PersonalInfoService,
    private toastrService: ToastrService,
    private translate: TranslateService,
    private sessionService: SessionService,
  ) {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.HR_ALLOWANCE_PROCESS}`);
  }

  ngOnInit(): void {
    this.subs.push(
      this.shareDataService.employee$.subscribe(employee => {
        this.employeeId = employee.employeeId;
        this.getPersonalInfo();
        this.getAvatar(this.employeeId);
      })
    );
  }

  ngOnDestroy(): void {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  getPersonalInfo() {
    this.subs.push(
      this.shareDataService.personalInfo$.subscribe(value => {
        this.personalInfo = value;
        this.isEmpInfoReady.emit(value);
      })
    );
  }

  getAvatar(employeeId: number | any) {
    this.avtIsLoading = true;
    this.subs.push(
      this.personalInfoService.getAvatar(employeeId).subscribe(res => {
       this.avtBase64 = res?.data ? 'data:image/jpg;base64,' + res?.data : 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';
       this.avtIsLoading = false;
      })
    );
  }

  beforeUpload = (file: NzUploadFile) => {
    return new Observable((observer: Observer<boolean>) => {
      const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
      if (!isJpgOrPng) {
       this.toastrService.error(this.translate.instant('staffManager.validate.fileType', {fileType: 'png, jpg'}));
        observer.complete();
        return;
      }
      observer.next(isJpgOrPng);
      observer.complete();
    });
  };

  handleUpload = (item: NzUploadXHRArgs): any => {
    this.avtIsLoading = true;
    const formData = new FormData();
    formData.append('fileAvatar', item.file as unknown as File);
    this.formDataAvt = formData;
    this.fileAvt = item;
    const reader = new FileReader();
    reader.readAsDataURL(item.file as unknown as File);
    reader.onload = () => {
      this.avtBase64 = reader.result;
      this.avtIsLoading = false;
      this.showTmp = true;
    };
  }

  deleteAvt() {
    this.avtIsLoading = true;
    this.subs.push(
      this.personalInfoService.deleteAvatar(this.employeeId).subscribe(res => {
        this.avtBase64 = 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';
        this.avtIsLoading = false;
        this.toastrService.success(this.translate.instant('common.notification.updateSuccess'));
      }, error => {
        this.avtIsLoading = false;
      })
    );
  }

  saveAvt() {
    if (this.fileAvt && this.formDataAvt) {
      this.avtIsLoading = true;
      this.subs.push(
      this.personalInfoService.uploadAvatar(this.employeeId, this.formDataAvt).subscribe(res => {
        this.getAvatar(this.employeeId);
        this.avtIsLoading = false;
        this.showTmp = false;
        this.fileAvt = null;
        this.formDataAvt = null;
      })
    );
    }
  }
}
