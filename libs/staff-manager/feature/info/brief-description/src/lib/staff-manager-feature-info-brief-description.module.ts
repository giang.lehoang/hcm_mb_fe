import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BriefDescriptionComponent} from "./brief-description/brief-description.component";
import {NzFormModule} from "ng-zorro-antd/form";
import {NzSpinModule} from "ng-zorro-antd/spin";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzUploadModule} from "ng-zorro-antd/upload";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzPopconfirmModule} from "ng-zorro-antd/popconfirm";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbTextLabelModule} from "@hcm-mfe/shared/ui/mb-text-label";
import {NzMenuModule} from "ng-zorro-antd/menu";

@NgModule({
  imports: [CommonModule, NzFormModule, NzSpinModule, NzIconModule, NzUploadModule, SharedUiMbButtonModule, NzPopconfirmModule, TranslateModule, SharedUiMbTextLabelModule, NzMenuModule],
  declarations: [BriefDescriptionComponent],
  exports: [BriefDescriptionComponent],
})
export class StaffManagerFeatureInfoBriefDescriptionModule {}
