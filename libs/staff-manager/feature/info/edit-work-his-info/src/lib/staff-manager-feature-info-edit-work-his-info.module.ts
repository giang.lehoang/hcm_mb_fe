import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EditWorkHisInfoComponent} from "./edit-work-his-info/edit-work-his-info.component";
import {NzFormModule} from "ng-zorro-antd/form";
import {ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiOrgDataPickerModule} from "@hcm-mfe/shared/ui/org-data-picker";
import {SharedUiExtendFormItemModule} from "@hcm-mfe/shared/ui/extend-form-item";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";
import {SharedUiEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/employee-data-picker";
import {NzUploadModule} from "ng-zorro-antd/upload";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzDividerModule} from "ng-zorro-antd/divider";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
    imports: [CommonModule, NzFormModule, ReactiveFormsModule, SharedUiMbDatePickerModule, TranslateModule, SharedUiMbSelectModule, SharedUiOrgDataPickerModule, SharedUiExtendFormItemModule, SharedUiMbButtonModule, SharedUiMbInputTextModule, NzCheckboxModule, SharedUiEmployeeDataPickerModule, NzUploadModule, NzIconModule, NzDividerModule, SharedUiLoadingModule],
  declarations: [EditWorkHisInfoComponent],
  exports: [EditWorkHisInfoComponent],
})
export class StaffManagerFeatureInfoEditWorkHisInfoModule {}
