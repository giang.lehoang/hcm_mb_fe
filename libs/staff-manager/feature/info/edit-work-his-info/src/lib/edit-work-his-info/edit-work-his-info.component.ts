import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NzModalRef, NzModalService} from 'ng-zorro-antd/modal';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import * as moment from 'moment';
import {NzUploadFile} from 'ng-zorro-antd/upload';
import {saveAs} from 'file-saver';
import {
  DownloadFileAttachService,
  ShareDataService,
  StaffInfoService,
  WorkInfoService
} from "@hcm-mfe/staff-manager/data-access/services";
import {BaseResponse, CatalogModel, Category, OrgInfo} from "@hcm-mfe/shared/data-access/models";
import {Constant} from "@hcm-mfe/staff-manager/data-access/common";
import {DateValidator} from "@hcm-mfe/shared/common/validators";
import {ConcurrentProcess, Document, WorkHistory} from "@hcm-mfe/staff-manager/data-access/models/info";
import {HTTP_STATUS_CODE, Mode} from "@hcm-mfe/shared/common/constants";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {beforeUploadFile, getTypeExport, Utils} from "@hcm-mfe/shared/common/utils";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {Subscription} from "rxjs";

@Component({
    selector: 'app-edit-work-his-info',
    templateUrl: './edit-work-his-info.component.html',
    styleUrls: ['./edit-work-his-info.component.scss'],
})
export class EditWorkHisInfoComponent implements OnInit, OnDestroy{
    mode = Mode.ADD;
    data: WorkHistory = new WorkHistory();
    functionCode: string = FunctionCode.HR_WORK_PROCESS;
    isSubmitted = false;
    form: FormGroup;
    fileList: NzUploadFile[] = [];

    listDocumentType: Document[] = [];
    listEmpType: Category[] = [];
    listPosition = [];
    listPositionLevel = [];
    listOtherPosition = [];

    documentType: NzSafeAny;

    documentTypeOutSet = new Set();

    isHideWhenTypeOut = false;
    isShowConcurrentProcess = true;
    isHideConcurrentProcess = true;  // hide concurrent process form

    docIdsDelete: number[] = [];
    isLoadingPage = false;
    listPositionGroup: CatalogModel[] = [];
    isLoadEdit = false;
    toDateOfModelEdit = '';
    subs: Subscription[] = [];
    cloneFile!: number;

    constructor(
        private fb: FormBuilder,
        private modalRef: NzModalRef,
        private toastService: ToastrService,
        private modalService: NzModalService,
        private translate: TranslateService,
        private shareDataService: ShareDataService,
        private staffInfoService: StaffInfoService,
        private workInfoService: WorkInfoService,
        private downloadFileAttachService: DownloadFileAttachService,
        private cdRef: ChangeDetectorRef,
    ) {
        this.form = fb.group({
            workProcessId: [null],
            employeeId: [null],
            fromDate: [null, [Validators.required]], //Từ ngày.
            documentTypeId: [null, [Validators.required]], // ID Loại QĐ
            empTypeCode: [null, [Validators.required]], //Mã Đối tượng
            positionId: [null, [Validators.required]], //ID Chức danh
            organizationId: [null, [Validators.required]], //ID Đơn vị

            positionLevel: [null], // Bậc chức danh
            positionGroupId: [null, [Validators.required]], // Vị trí chức danh
            documentNo: [null], // Số QĐ
            signedDate: [null], // Ngày ký QĐ
            note: [null], //Ghi chú,
            isTrial: [null], //Thử thách
            planToDate1: [null], // Ngày kết thúc 1
            planToDate2: [null], // Ngày kết thúc 2
            isKeepSenior: [null], // Bảo lưu thâm niên
            listConcurrentProcessDTO: this.fb.array([]), // Đơn vị kiểm nghiệm
        }, {
            validators: [
                DateValidator.validateRangeDate('fromDate', 'planToDate1', 'rangeDateError1'),
                DateValidator.validateRangeDate('planToDate1', 'planToDate2', 'rangeDateError2')
            ]
        });
        this.getListDocumentTypes();
        this.getListEmployeeType();
        this.getPositionLevelList();
        this.getListPositionGroup();
    }

    ngOnInit(): void {
        this.onChanges();
        if (this.mode === Mode.EDIT) {
            this.removeConcurrentProcessDisplay();
            this.getModelEdit();
        } else {
            this.addConcurrentProcess(null);
        }
        this.f['employeeId'].setValue(this.shareDataService.getEmployee().employeeId);
    }

    get f() {
        return this.form.controls;
    }

    get concurrentProcesses(): NzSafeAny {
        return this.f['listConcurrentProcessDTO'] as FormArray;
    }

    getModelEdit() {
        if (this.data) {
            if (this.data.workProcessId && this.data.workProcessId > 0) {
                this.getDataById();
                this.cloneFile = 1;
            } else {
                this.getDraftDataById();
            }
        }
    }

    getDataById() {
        this.isLoadingPage = true;
        this.subs.push(
            this.workInfoService.getDataById(this.data.workProcessId).subscribe({
                next: (res: BaseResponse) => {
                    if (res.code === HTTP_STATUS_CODE.OK) {
                        this.data = res?.data;
                        this.doSomeWorkAfterGetEditModel();
                    } else {
                        this.toastService.error(res?.message);
                    }
                    this.isLoadingPage = false;
                },
                error: (err) => {
                    this.toastService.error(err?.message);
                    this.isLoadingPage = false;
                }
            })
        );
    }

    getDraftDataById() {
        this.isLoadingPage = true;
        this.subs.push(
            this.workInfoService.getDraftDataById(this.data.draftWorkProcessId).subscribe({
                next: (res: BaseResponse) => {
                    if (res.code === HTTP_STATUS_CODE.OK) {
                        this.data = res?.data;
                        this.doSomeWorkAfterGetEditModel();
                    } else {
                        this.toastService.error(res?.message);
                    }
                    this.isLoadingPage = false;
                },
                error: (err) => {
                    this.toastService.error(err?.message);
                    this.isLoadingPage = false;
                }
            })
        );
    }

    doSomeWorkAfterGetEditModel() {
        this.toDateOfModelEdit = this.data.toDate ? this.data.toDate : '';
        this.patchValueInfo();
    }

    removeConcurrentProcessDisplay() {
        this.isShowConcurrentProcess = false;
        this.form.removeControl("listConcurrentProcessDTO");
    }

    patchValueInfo() {
        const workHistory: WorkHistory = this.data;
        this.form.patchValue({
            workProcessId: workHistory?.workProcessId,
            employeeId: this.shareDataService.getEmployee().employeeId,
            documentTypeId: workHistory?.documentTypeId, // ID Loại QĐ
            empTypeCode: workHistory?.empTypeCode, //Mã Đối tượng
            positionLevel: workHistory?.positionLevel, // Bậc chức danh
            positionGroupId: workHistory?.positionGroupId,
            documentNo: workHistory?.documentNo, // Số QĐ
            note: workHistory?.note ?? null, //Ghi chú
            isTrial: workHistory?.isTrial === 1,
            isKeepSenior: workHistory?.isKeepSenior === 1
        });
        this.f['fromDate'].setValue(this.data?.fromDate ? moment(this.data?.fromDate, 'DD/MM/YYYY').toDate() : null);
        this.f['planToDate1'].setValue(this.data?.planToDate1 ? moment(this.data?.planToDate1, 'DD/MM/YYYY').toDate() : null);
        this.f['planToDate2'].setValue(this.data?.planToDate2 ? moment(this.data?.planToDate2, 'DD/MM/YYYY').toDate() : null);
        this.f['signedDate'].setValue(this.data?.signedDate ? moment(this.data?.signedDate, 'DD/MM/YYYY').toDate() : null);

        const org = new OrgInfo();
        org.orgId = this.data?.organizationId;
        org.orgName = this.data?.organizationName;
        org.parentName = this.data?.parentName;
        this.f['organizationId']?.patchValue(org);

        if (workHistory?.positionId) {
            this.form?.patchValue({
                positionId: workHistory?.positionId, //ID Chức danh
            });
        }

        // file
        if (workHistory?.attachFileList && workHistory?.attachFileList.length > 0) {
            workHistory?.attachFileList.forEach((item) => {
                this.fileList.push({
                    uid: item.docId,
                    name: item.fileName,
                    url: item.security,
                    status: 'done'
                });
            });
            this.fileList = [...this.fileList];
        }

        this.isLoadEdit = true;
    }

    validateOrgAndOrgOther(): boolean {

        if (this.isHideWhenTypeOut || this.isHideConcurrentProcess) { // if type = out || is closing concurProcessForm -> no need validate
            return true;
        }

        const org = this?.f['organizationId']?.value;
        const listOtherOrg = this?.f['listConcurrentProcessDTO'].value;

        // validate bổ trống đơn vị kiêm nhiệm hoặc chức danh kiêm nhiệm
        const isFieldEmpty = listOtherOrg.filter((item: NzSafeAny) => !!(item?.otherOrgId)).some((item: NzSafeAny) => item?.otherPositionId == null);
        if (isFieldEmpty) {
            this.toastService.error(this.translate.instant("staffManager.validate.workProcess.emptyOtherOrg"));
            return false;
        }

        // validate đơn vị kiêm nhiệm trùng với đơn vị.
        const isDuplicateOrg = listOtherOrg.some((item: NzSafeAny) => item?.otherOrgId?.orgId === org?.orgId);
        if (isDuplicateOrg) {
            this.toastService.error(this.translate.instant("staffManager.validate.workProcess.otherOrgDuplicateWithOrg"));
            return false;
        }

        // validate duplicate đơn vị kiêm nhiệm
        const otherOrgsIds = listOtherOrg.map((item: NzSafeAny) => item?.otherOrgId?.orgId);
        const isDuplicateOtherOrg = otherOrgsIds.some((orgId: NzSafeAny, index: NzSafeAny) => otherOrgsIds.indexOf(orgId) != index);

        if (isDuplicateOtherOrg) {
            this.toastService.error(this.translate.instant("staffManager.validate.workProcess.otherOrgDuplicate"));
            return false;
        }

        return true;
    }

    onChanges() {
        this.f['organizationId'].valueChanges.subscribe((value: OrgInfo) => {
            if (value) {
                this.f['positionId'].setValue(null, { emitEvent: false, onlySelf: true });
                this.listPosition = [];
                this.workInfoService.getMBPositions(value.orgId).subscribe((res: BaseResponse) => {
                    if (res.code === HTTP_STATUS_CODE.OK) this.listPosition = res.data;
                });
            }
        });
    }

    pushNewConcurrentProcess() {
        const concurProcess = this.fb.group({
            otherEmpTypeCode: [null, [Validators.required]],
            otherOrgId: [null, [Validators.required]],
            otherPositionId: [null, [Validators.required]],
            otherManagerId: [null],
            otherIsTrial: [null],
            otherPlanToDate1: [null],
            otherPlanToDate2: [null],
        }, {
            validators: [
                DateValidator.validateRangeDate('otherPlanToDate1', 'otherPlanToDate2', 'rangeDateErrorOther'),
            ]
        });
        this.concurrentProcesses?.push(concurProcess);
    }

    addConcurrentProcess(i: number | null) {
        if (i === 0 && this.isHideConcurrentProcess) {   // show concurrent form
            this.isHideConcurrentProcess = false;
            this.addConcurrentProcessFormValidator();
            this.setErrorConcurrentProcessForm();
            return;
        }
        const listOtherOrg = this?.f['listConcurrentProcessDTO'].value;
        const isFieldEmpty = listOtherOrg.filter((item: NzSafeAny) => !!(item?.otherOrgId)).some((item: NzSafeAny) => item?.otherPositionId == null);
        if (isFieldEmpty) {
            this.toastService.error(this.translate.instant("staffManager.validate.workProcess.emptyOtherOrg1"));
            return;
        }
        this.pushNewConcurrentProcess();
    }

    removeConcurrentProcess(index: number) {
        if (index === 0 && !this.isHideConcurrentProcess) {  // hide concurrentProcess form
            this.isHideConcurrentProcess = true;
            this.removeConcurrentProcessFormValidator();
            return;
        }

        if (this.concurrentProcesses.length > 1)
            this.concurrentProcesses.removeAt(index);
        else {
            this.concurrentProcesses.removeAt(index);
            this.addConcurrentProcess(null);
        }
    }

    addConcurrentProcessFormValidator() {
        this.concurrentProcesses?.at(0)['controls']?.otherEmpTypeCode.addValidators(Validators.required);
        this.concurrentProcesses?.at(0)['controls']?.otherOrgId.addValidators(Validators.required);
        this.concurrentProcesses?.at(0)['controls']?.otherPositionId.addValidators(Validators.required);
    }

    setErrorConcurrentProcessForm() {
        this.concurrentProcesses?.at(0)['controls']?.otherEmpTypeCode.setErrors({required: true});
        this.concurrentProcesses?.at(0)['controls']?.otherOrgId.setErrors({required: true});
        this.concurrentProcesses?.at(0)['controls']?.otherPositionId.setErrors({required: true});
    }

    removeConcurrentProcessFormValidator() {
        this.concurrentProcesses?.at(0)['controls']?.otherEmpTypeCode.clearValidators();
        this.concurrentProcesses?.at(0)['controls']?.otherOrgId.clearValidators();
        this.concurrentProcesses?.at(0)['controls']?.otherPositionId.clearValidators();
    }

    setNonErrorConcurrentProcessForm() {
        this.concurrentProcesses?.at(0)['controls']?.otherEmpTypeCode.setErrors(null);
        this.concurrentProcesses?.at(0)['controls']?.otherOrgId.setErrors(null);
        this.concurrentProcesses?.at(0)['controls']?.otherPositionId.setErrors(null);
    }

    getListDocumentTypes() {
        this.workInfoService.getDocumentTypes().subscribe((res: BaseResponse) => {
            if (res.code === HTTP_STATUS_CODE.OK) {
                this.listDocumentType = res.data;
                this.getListDocumentTypeOut(res.data);
            } else {
              this.toastService.error(res?.message);
            }
        });
    }

    getListDocumentTypeOut(documentTypes: Document[]) {
        this.documentTypeOutSet = new Set(documentTypes.filter((dt: NzSafeAny) => dt.type === Constant.DOCUMENT_TYPE.TYPE_OUT).map((dt: NzSafeAny) => dt.documentTypeId));
    }

    getListEmployeeType() {
        this.staffInfoService.getCatalog(Constant.CATALOGS.DOI_TUONG_CV).subscribe((res: BaseResponse) => {
            if (res.code === HTTP_STATUS_CODE.OK) this.listEmpType = res.data;
        });
    }

    getPositionLevelList() {
        this.staffInfoService.getCatalog(Constant.CATALOGS.LEVEL_NV).subscribe(
            (res: BaseResponse) => {
                if (res.code === HTTP_STATUS_CODE.OK) this.listPositionLevel = res.data;
            }
        );
    }

    getListPositionGroup() {
        this.staffInfoService.getPositionGroup(Constant.TYPE_CODE.POSITION).subscribe(res => {
            if (res.code === HTTP_STATUS_CODE.OK) {
                this.listPositionGroup = res.data.map((item: NzSafeAny) => {
                    item.value = item.pgrId;
                    item.label = item.pgrName;
                    return item;
                });
            } else {
              this.toastService.error(res?.message);
            }
        }, error => this.toastService.error(error.message));
    }

    changePosition(event: NzSafeAny) {
        if (event) {
            if (event.itemSelected?.positionGroupId && !this.isLoadEdit) {
                this.f['positionGroupId'].setValue(event.itemSelected?.positionGroupId);
            }
        }
    }

    beforeUpload = (file: NzUploadFile): boolean => {
      this.fileList =  beforeUploadFile(file, this.fileList, 3000000, this.toastService, this.translate, 'staffManager.notification.upload.limitSize')
      return false;
    };

    clearFormArray = (formArray: FormArray) => {
        if (formArray && formArray.length > 0) {
            while (formArray.length !== 0) {
                formArray.removeAt(0);
            }
        }
    }

    removeValidateWhenTypeOut() {
        const keys = ['empTypeCode', 'organizationId', 'positionId', 'positionGroupId', 'positionLevel'];
        keys.forEach(key => {
          this.f[key].setValidators(null);
          this.f[key].reset();
        })
        this.resetConcurrentProcessFormArray();
    }

    resetConcurrentProcessFormArray() {
        this.clearFormArray(this.concurrentProcesses);
        this.pushNewConcurrentProcess();
    }

    addValidateWhenTypeIn() {
        const keys = ['empTypeCode', 'organizationId', 'positionId', 'positionGroupId'];
        keys.forEach(key => {
          this.f[key].setValidators([Validators.required]);
        })
    }

    updateValueAndValidityForm() {
        const keys = ['empTypeCode', 'organizationId', 'positionId', 'positionGroupId'];
        keys.forEach(key => {
          this.f[key].updateValueAndValidity();
        })
    }

    changeValue(event: number) {
        this.isHideWhenTypeOut = false;
        if (this.documentTypeOutSet.has(event)) {
            this.isHideWhenTypeOut = true;
            this.removeValidateWhenTypeOut();
        } else {
            this.addValidateWhenTypeIn();
        }
        this.updateValueAndValidityForm();

        this.listDocumentType.forEach((value: NzSafeAny) => {
            if (value['documentTypeId'] === event) {
                this.documentType = value;
                this.form.controls['planToDate1'].setValidators(value['isRequiredEndDate'] ? [Validators.required] : []);
                this.form.controls['planToDate2'].setValidators(value['isRequiredEndDate'] ? [Validators.required] : []);
                this.cdRef.detectChanges();
            }
        });
    }

    onOtherOrgChange(org: OrgInfo) {
        const workInfoService = this.workInfoService.getMBPositions(org?.orgId);
        if (org?.orgId > 0) {
            workInfoService.subscribe({
                next: (res) => {
                    if (res?.data && res?.data?.length > 0) {
                        this.listOtherPosition = res.data;
                    }
                }
            });
        }
    }

    setConcurrentProcessToRequest(request: WorkHistory) {
        request.listConcurrentProcessDTO = [];
        if (this.concurrentProcesses) {
            for (const concurProcess of this.concurrentProcesses.value) {
                const o = new ConcurrentProcess();
                o.organizationId = concurProcess?.otherOrgId?.orgId;
                o.positionId = concurProcess?.otherPositionId;
                o.empTypeCode = concurProcess?.otherEmpTypeCode;
                o.managerId = concurProcess?.otherManagerId?.employeeId;
                o.isTrial = concurProcess?.otherIsTrial ? 1 : 0;
                o.planToDate1 = Utils.convertDateToSendServer(concurProcess?.otherPlanToDate1);
                o.planToDate2 = Utils.convertDateToSendServer(concurProcess?.otherPlanToDate2);
                if (o.organizationId == null || o.positionId == null) continue;
                request.listConcurrentProcessDTO.push(o);
            }
        }
    }

    save() {
        this.isSubmitted = true;
        if (this.isHideConcurrentProcess) {         // if concurProcessForm is closing
            this.removeConcurrentProcessFormValidator();
            this.setNonErrorConcurrentProcessForm();
        }

        if (this.validateOrgAndOrgOther() && this.form.valid) {
            this.isLoadingPage = true;
            const request: WorkHistory = this.form.value;
            request.fromDate = Utils.convertDateToSendServer(this.f?.['fromDate'].value);
            request.toDate = this.toDateOfModelEdit;
            request.signedDate = Utils.convertDateToSendServer(this.f?.['signedDate'].value);

            request.organizationId = this.f?.['organizationId']?.value ? this.f?.['organizationId'].value['orgId'] : null;
            request.otherOrgId = this.f?.['otherOrgId']?.value ? this.f?.['otherOrgId'].value['orgId'] : null;
            request.isKeepSenior = this.f?.['isKeepSenior']?.value ? 1 : 0;
            request.isTrial = this.f?.['isTrial']?.value ? 1 : 0;
            request.planToDate1 =  Utils.convertDateToSendServer(this.f?.['planToDate1'].value);
            request.planToDate2 = Utils.convertDateToSendServer(this.f?.['planToDate2'].value);

            request.hrDocumentType = this.listDocumentType?.find(dc => dc.documentTypeId === this.f?.['documentTypeId']?.value)?.type;
            request.docIdsDelete = this.docIdsDelete;
            request.draftWorkProcessId = this.data.draftWorkProcessId;
            request.isCloneFile = this.cloneFile;

            this.setConcurrentProcessToRequest(request);

            const formData = new FormData();
            formData.append('data', new Blob([JSON.stringify(request)], {
                type: 'application/json'
            }));

            this.fileList.forEach((nzFile: NzSafeAny) => {
                formData.append('files', nzFile);
            });

            this.workInfoService.saveRecord(formData).subscribe((res: BaseResponse) => {
                if (res.code === HTTP_STATUS_CODE.OK) {
                    this.toastService.success(this.translate.instant('common.notification.updateSuccess'));
                    this.modalRef.close({ refresh: true });
                } else {
                    this.toastService.error(this.translate.instant('common.notification.updateError') + ':' + res.message);
                }
                this.isLoadingPage = false;
            });
        }
    }

    downloadFile = (file: NzUploadFile) => {
        if (file?.uid) {
            this.downloadFileAttachService.doDownloadAttachFileWithSecurity(~~file?.uid, file?.url).subscribe(res => {
                const reportFile = new Blob([res.body], { type: getTypeExport(file.name.split('.').pop()) });
                saveAs(reportFile, file.name);
            });
        }
    }

    removeFile = (file: NzUploadFile) => {
        this.docIdsDelete.push(Number(file.uid));
        const index = this.fileList.indexOf(file);
        this.fileList.splice(index, 1);
        return true;
    }

    ngOnDestroy(): void {
        this.subs?.forEach(sub => sub?.unsubscribe());
    }

}
