import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NzModalRef, NzModalService} from "ng-zorro-antd/modal";
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";
import * as moment from 'moment';
import {HTTP_STATUS_CODE, Mode} from "@hcm-mfe/shared/common/constants";
import {ShareDataService, WorkBeforeHisService,} from "@hcm-mfe/staff-manager/data-access/services";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";
import {DateValidator} from "@hcm-mfe/shared/common/validators";
import {WorkBeforeInfo} from "@hcm-mfe/staff-manager/data-access/models/info";

@Component({
  selector: 'app-add-work-his-before',
  templateUrl: './add-work-his-before.component.html',
  styleUrls: ['./add-work-his-before.component.scss'],
})
export class AddWorkHisBeforeComponent implements OnInit {
  mode = Mode.ADD;
  data: WorkBeforeInfo = new WorkBeforeInfo();

  isSubmitted = false;
  form: FormGroup;
  isLoadingPage = false;

  constructor(
    private fb: FormBuilder,
    private modalRef: NzModalRef,
    private toastService: ToastrService,
    private modalService: NzModalService,
    private translate: TranslateService,
    private shareDataService: ShareDataService,
    private workBeforeHisService: WorkBeforeHisService
  ) {
    this.form = this.fb.group({
      workedOutsideId: [null],
      employeeId: [null],
      fromDate: [null, Validators.required],
      toDate: [null, Validators.required],
      organizationName: [null, [Validators.required, Validators.maxLength(200)]],
      positionName: [null, [Validators.required, Validators.maxLength(200)]],
      referPerson: [null, Validators.maxLength(200)],
      referPersonPosition: [null, Validators.maxLength(200)],
      mission: [null, Validators.maxLength(1000)],
      reasonLeave: [null, Validators.maxLength(500)],
      rewardInfo: [null, Validators.maxLength(1000)],
      decplineInfo: [null, Validators.maxLength(1000)],
    }, {
      validators: [
        DateValidator.validateRangeDate('fromDate', 'toDate', 'rangeDateError')
      ]
    });
  }

  get f() { return this.form.controls; }
  ngOnInit(): void {
    if (this.mode === Mode.EDIT)
      this.patchValueInfo();
    this.f['employeeId'].setValue(this.shareDataService.getEmployee().employeeId)
  }

  patchValueInfo() {
    this.isLoadingPage = true;
    this.workBeforeHisService.getWorkedOutside(this.data.workedOutsideId).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.data = res?.data;
        this.form.patchValue(this.data);
        this.f['fromDate'].setValue(this.data.fromDate ? moment(this.data.fromDate, 'MM/YYYY').toDate() : null);
        this.f['toDate'].setValue(this.data.toDate ? moment(this.data.toDate, 'MM/YYYY').toDate() : null);
      } else {
        this.toastService.error(res?.message);
      }
      this.isLoadingPage = false;
    }, () => {
      this.modalRef.close();
      this.isLoadingPage = false;
    })
  }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      this.isLoadingPage = true;
      const request:WorkBeforeInfo = this.form.value;
      request.fromDate = this.f['fromDate'].value ? moment(this.f['fromDate'].value).format('MM/YYYY') : null;
      request.toDate = this.f['toDate'].value ? moment(this.f['toDate'].value).format('MM/YYYY') : null;
      this.workBeforeHisService.saveRecord(request).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.toastService.success(this.translate.instant('common.notification.updateSuccess'));
          this.modalRef.close({refresh:true})
        } else {
          this.toastService.error(this.translate.instant('common.notification.updateError') + ":" + res.message);
        }
        this.isLoadingPage = false;
      }, error => {
        this.toastService.error(this.translate.instant('common.notification.updateError') + ":" + error.message);
        this.isLoadingPage = false;
      })
    }
  }
}
