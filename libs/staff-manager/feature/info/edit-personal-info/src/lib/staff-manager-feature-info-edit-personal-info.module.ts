import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EditPersonalInfoComponent} from "./edit-personal-info/edit-personal-info.component";
import {NzFormModule} from "ng-zorro-antd/form";
import {ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedDirectivesNumberInputModule} from "@hcm-mfe/shared/directives/number-input";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiMbInputCompactModule} from "@hcm-mfe/shared/ui/mb-input-compact";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
    imports: [CommonModule, NzFormModule, ReactiveFormsModule, SharedUiMbInputTextModule, SharedDirectivesNumberInputModule, TranslateModule, SharedUiMbSelectModule, SharedUiMbInputCompactModule, SharedUiMbDatePickerModule, SharedUiLoadingModule],
  declarations: [EditPersonalInfoComponent],
  exports: [EditPersonalInfoComponent],
})
export class StaffManagerFeatureInfoEditPersonalInfoModule {}
