import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subscription} from "rxjs";
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";
import {NzModalRef} from "ng-zorro-antd/modal";
import * as moment from 'moment';
import {ListMobileNumber, LookupValues, PersonalInfo} from "@hcm-mfe/staff-manager/data-access/models/info";
import {PersonalInfoService, ShareDataService, StaffInfoService} from "@hcm-mfe/staff-manager/data-access/services";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";
import {Constant} from "@hcm-mfe/staff-manager/data-access/common";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";

@Component({
  selector: 'app-edit-personal-info',
  templateUrl: './edit-personal-info.component.html',
  styleUrls: ['./edit-personal-info.component.scss']
})
export class EditPersonalInfoComponent implements OnInit, OnDestroy {
  @Input() data: NzSafeAny;
  isSubmitted = false;
  form: FormGroup;
  listSex: LookupValues[] = [];
  listMarital: LookupValues[] = [];
  listNational: LookupValues[] = [];
  listFolk: LookupValues[] = [];
  listReligion: LookupValues[] = [];
  validaInput = [Validators.required, Validators.pattern('^[0-9]{0,10}$')];
  personalInfo: PersonalInfo | NzSafeAny;
  subs: Subscription[] = [];
  listPhoneNumbers: ListMobileNumber[] = [];
  phoneAreas: LookupValues[] = [];
  isLoadingPage = false;

  constructor(
    private fb: FormBuilder,
    private shareDataService: ShareDataService,
    private personalInfoService: PersonalInfoService,
    private staffInfoService: StaffInfoService,
    private toastService: ToastrService,
    private translate: TranslateService,
    private modalRef: NzModalRef
  ) {
    this.form = fb.group({
      fullName: [null, [Validators.required, Validators.maxLength(200)]],
      dateOfBirth: [null, Validators.required],
      genderCode: [null, Validators.required],
      maritalStatusCode: [null, Validators.required],
      nationCode: [null, Validators.required],
      ethnicCode: [null, Validators.required],
      religionCode: [null, Validators.required],
      personalEmail: [null, Validators.email],
      companyPhone: [null, [Validators.minLength(10), Validators.maxLength(13), Validators.pattern('^[0-9]{0,50}$')]],
      listMobileNumber: [null],
      isInsuranceMb: [null, Validators.required],
      insuranceNo: [null, Validators.maxLength(10)],
      taxNo: [null, [Validators.minLength(10), Validators.maxLength(10)]],
      empTypeName: [null],
      positionName: [null],
      orgName: [null],
      empTypeCode: [null],
      positionCode: [null],
      orgCode: [null]
    });
  }

  ngOnInit(): void {
   this.personalInfo = this.data;
   this.getCatalogs();
   this.patchValueInfo();
  }

  ngOnDestroy() {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      this.isLoadingPage = true;
      const value: PersonalInfo = Object.assign({}, this.personalInfo, this.form.value);
      value.dateOfBirth = moment(this.form.value.dateOfBirth).format('DD/MM/YYYY');
      value.mobileNumber = value.listMobileNumber ? value.listMobileNumber[value.listMobileNumber.findIndex(item => item.isMain === 1)]?.phoneNumber : value.mobileNumber;
      this.personalInfoService.savePersonalInfo(this.personalInfo.employeeId, value).subscribe(res => {
        if (res && res.code === 200) {
          this.toastService.success(this.translate.instant('common.notification.updateSuccess'));
          this.modalRef.close({refresh: true});
        } else {
          this.toastService.error(this.translate.instant('common.notification.updateError') + ': ' + res?.message);
          this.modalRef.close({refresh: false});
        }
        this.isLoadingPage = false;
      }, error =>  {
        this.toastService.error(this.translate.instant('common.notification.updateError') + ': ' + error?.message);
        this.modalRef.close({refresh: false});
        this.isLoadingPage = false;
      });
    }
  }

   patchValueInfo() {
     this.isLoadingPage = true;
     this.personalInfoService.getPersonalInfo(this.personalInfo.employeeId).subscribe((res: BaseResponse) => {
       if (res.code === HTTP_STATUS_CODE.OK) {
         this.personalInfo = res?.data;
         this.listPhoneNumbers = this.personalInfo?.listMobileNumber;
         this.form.patchValue({
           fullName: this.personalInfo?.fullName,
           dateOfBirth: this.personalInfo?.dateOfBirth ? moment(this.personalInfo?.dateOfBirth, 'DD/MM/YYYY').toDate() : null,
           genderCode: this.personalInfo?.genderCode,
           maritalStatusCode: this.personalInfo?.maritalStatusCode,
           nationCode: this.personalInfo?.nationCode,
           ethnicCode: this.personalInfo?.ethnicCode,
           religionCode: this.personalInfo?.religionCode,
           personalEmail: this.personalInfo?.personalEmail,
           companyPhone: this.personalInfo?.companyPhone,
           listMobileNumber: this.personalInfo?.listMobileNumber,
           isInsuranceMb: this.personalInfo?.isInsuranceMb,
           insuranceNo: this.personalInfo?.insuranceNo,
           taxNo: this.personalInfo?.taxNo,
           positionName: this.personalInfo?.positionName,
           empTypeName: this.personalInfo?.empTypeName,
           orgName: this.personalInfo?.orgName,
           empTypeCode: this.personalInfo?.empTypeCode,
           positionCode: this.personalInfo?.positionCode,
           orgCode: this.personalInfo?.orgCode,
         });
       } else {
         this.toastService.error(res?.message);
       }
       this.isLoadingPage = false;
     }, () => {
       this.modalRef.close();
       this.isLoadingPage = false;
     })
  }

  getCatalogs() {
    this.getListGender();
    this.getListMarital();
    this.getNational();
    this.getEthnic();
    this.getReligion();
    this.getPhoneArea();
  }

  getListGender() {
    this.subs.push(
      this.staffInfoService.getCatalog(Constant.CATALOGS.GIOI_TINH).subscribe(res => {
        const response: BaseResponse = res;
        this.listSex = response.data;
      })
    );
  }

  getListMarital() {
    this.subs.push(
      this.staffInfoService.getCatalog(Constant.CATALOGS.TINH_TRANG_HON_NHAN).subscribe(res => {
        const response: BaseResponse = res;
        this.listMarital = response.data;
      })
    );
  }

  getNational() {
    this.subs.push(
      this.staffInfoService.getCatalog(Constant.CATALOGS.QUOC_GIA).subscribe(res => {
        const response: BaseResponse = res;
        this.listNational = response.data;
      })
    );
  }

  getEthnic() {
    this.subs.push(
      this.staffInfoService.getCatalog(Constant.CATALOGS.DAN_TOC).subscribe(res => {
        const response: BaseResponse = res;
        this.listFolk = response.data;
      })
    );
  }

  getReligion() {
    this.subs.push(
      this.staffInfoService.getCatalog(Constant.CATALOGS.TON_GIAO).subscribe(res => {
        const response: BaseResponse = res;
        this.listReligion = response.data;
      })
    );
  }

  getPhoneArea() {
    this.subs.push(
      this.staffInfoService.getCatalog(Constant.CATALOGS.MA_VUNG_DIEN_THOAI).subscribe(res => {
        const response: BaseResponse = res;
        this.phoneAreas = response.data;
      })
    );
  }
}
