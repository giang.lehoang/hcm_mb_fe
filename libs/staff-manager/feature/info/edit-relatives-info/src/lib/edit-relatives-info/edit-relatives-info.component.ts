import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { Subscription } from 'rxjs';
import {
  ContactInfoService,
  IdentityInfoService,
  PersonalInfoService, RelativesInfoService,
  ShareDataService,
  StaffInfoService
} from "@hcm-mfe/staff-manager/data-access/services";
import {BaseResponse, Category} from "@hcm-mfe/shared/data-access/models";
import {Constant} from "@hcm-mfe/staff-manager/data-access/common";
import {DateValidator} from "@hcm-mfe/shared/common/validators";
import {EmployeeDetail, PersonalInfo, RelativeInfo} from "@hcm-mfe/staff-manager/data-access/models/info";
import {HTTP_STATUS_CODE, Mode} from "@hcm-mfe/shared/common/constants";
import {Utils} from "@hcm-mfe/shared/common/utils";

@Component({
  selector: 'app-edit-relatives-info',
  templateUrl: './edit-relatives-info.component.html',
  styleUrls: ['./edit-relatives-info.component.scss'],
})
export class EditRelativesInfoComponent implements OnInit, OnDestroy {
  data = new RelativeInfo();
  mode = Mode.ADD;

  isSubmitted = false;
  form: FormGroup;

  listFamilyRelationship: Category[] = [];

  listRelationStatus: Category[] = [];

  listPolicyType: Category[] = [];

  subs: Subscription[] = [];
  isLoadingPage = false;

  constructor(
    private fb: FormBuilder,
    private modalRef: NzModalRef,
    private toastService: ToastrService,
    private modalService: NzModalService,
    private translate: TranslateService,
    private shareDataService: ShareDataService,
    private staffInfoService: StaffInfoService,
    private personalInfoService: PersonalInfoService,
    private identityInfoService: IdentityInfoService,
    private contactInfoService: ContactInfoService,
    private relativesInfoService: RelativesInfoService
  ) {
    this.form = fb.group({
      familyRelationshipId: [null],
      employeeId: [null],
      // Mã mối quan hệ
      relationTypeCode: [null, [Validators.required]], //Loại mối quan hệ
      relationStatusCode: [null, [Validators.required]], //Tình trạng thân nhân
      policyTypeCode: [null], // Đối tượng chính sách

      isInCompany: [false], //Có phải nhân viên trong công ty. truyền vào giá trị 1 nếu người dùng chọn checked
      referenceEmployeeId: [{ value: null, disabled: true }], //ID nhân viên của thân nhân

      // Chưa có GTTT/MST
      fullName: [null, [Validators.required,Validators.maxLength(200)]], //Họ tên thân nhâ

      personalIdNumber: [null, [Validators.minLength(6), Validators.maxLength(12)]], // CMT / CCCD
      dateOfBirth: [null, [DateValidator.maxCurrentDateValidator]], // Ngày sinh
      workOrganization: [null, [Validators.maxLength(200)]], // Đơn vị công tác
      job: [null, [Validators.maxLength(200)]], // Nghề nghiệp
      currentAddress: [null, Validators.maxLength(200)], // Chỗ ở hiện tại
      note: [null, [Validators.maxLength(500)]], // Ghi chú
      isHouseholdOwner: [false],
      phoneNumber: [null]
    });
    this.getFamilyRelationshipList();
    this.getRelationStatusList();
    this.getPolicyTypeList();
  }

  get f() {
    return this.form.controls;
  }

  ngOnInit(): void {
    this.onChange();
    if (this.mode === Mode.EDIT)
      this.patchValueInfo();

    this.f['employeeId'].setValue(this.shareDataService.getEmployee().employeeId);
  }

  onChange() {
    this.f['isInCompany'].valueChanges.subscribe(checked => {
      if (checked) {
        this.f['referenceEmployeeId'].disable();
        this.f['fullName'].disable();
        this.f['dateOfBirth'].disable();
        this.f['personalIdNumber'].disable();
        this.f['workOrganization'].disable();
        this.f['job'].disable();
        this.f['currentAddress'].disable();
      } else {
        this.f['referenceEmployeeId'].enable();
        this.f['fullName'].enable();
        this.f['dateOfBirth'].enable();
        this.f['personalIdNumber'].enable();
        this.f['workOrganization'].enable();
        this.f['job'].enable();
        this.f['currentAddress'].enable();
        this.f['referenceEmployeeId'].reset();

        this.f['fullName'].reset();
        this.f['dateOfBirth'].reset();
        this.f['personalIdNumber'].reset();
        this.f['workOrganization'].reset();
        this.f['job'].reset();
        this.f['currentAddress'].reset();
      }
    });
  }


  patchValueInfo() {
    this.isLoadingPage = true;
    this.relativesInfoService.getRecord(this.data.familyRelationshipId).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.form.patchValue({
          familyRelationshipId: res.data.familyRelationshipId,
          relationTypeCode: res.data.relationTypeCode,
          relationStatusCode: res.data.relationStatusCode,
          policyTypeCode:res.data.policyTypeCode,
          referenceEmployeeId: res.data.referenceEmployeeId,
          fullName: res.data.fullName,
          personalIdNumber: res.data.personalIdNumber,
          workOrganization: res.data.workOrganization,
          job: res.data.job,
          currentAddress: res.data.currentAddress,
          note: res.data.note,
          phoneNumber: res.data.phoneNumber,
          dateOfBirth: res.data.dateOfBirth ? moment(res.data.dateOfBirth, 'DD/MM/YYYY').toDate() : null
        });
        if (res.data.isInCompany) {
          if (res.data.isInCompany === 1) {
            this.form.patchValue({isInCompany: true})
          } else {
            this.form.patchValue({isInCompany: false})
          }
        }
        if (res.data.isHouseholdOwner) {
          if (res.data.isHouseholdOwner === 1) {
            this.form.patchValue({isHouseholdOwner: true})
          } else {
            this.form.patchValue({isHouseholdOwner: false})
          }
        }
      } else {
        this.toastService.error(res?.message);
      }
      this.isLoadingPage = false;
    }, () => {
      this.modalRef.close();
      this.isLoadingPage = false;
    })
  }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      this.isLoadingPage = true;
      const request: RelativeInfo = this.form.getRawValue();
      request.dateOfBirth = this.f['dateOfBirth'].value ? moment(this.f['dateOfBirth'].value).format('DD/MM/YYYY') : null;
      request.isInCompany = this.f['isInCompany'].value ? 1 : 0;
      request.referenceEmployeeId = this.f['isInCompany'].value ? this.f['referenceEmployeeId'].value : null;
      request.isHouseholdOwner = this.f['isHouseholdOwner'].value ? 1 : 0;

      this.relativesInfoService.saveRecord(request).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.toastService.success(this.translate.instant('common.notification.updateSuccess'));
          this.modalRef.close({refresh: true});
        } else if (res.code === HTTP_STATUS_CODE.ACCEPTED) {
          this.toastService.warning(this.translate.instant('common.notification.identificationCardWarning'));
          this.toastService.success(this.translate.instant('common.notification.updateSuccess'));
          this.modalRef.close({refresh: true});
        } else {
          this.toastService.error(this.translate.instant('common.notification.updateError') + ':' + res.message);
        }
        this.isLoadingPage = false;
        }, error => {
          this.toastService.error(this.translate.instant('common.notification.updateError') + ':' + error.message);
          this.isLoadingPage = false;
        }
      );
    }
  }

  getFamilyRelationshipList() {
    this.staffInfoService.getCatalog(Constant.CATALOGS.MOI_QUAN_HE_NT).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) this.listFamilyRelationship = res.data;
    });
  }

  getRelationStatusList() {
    this.staffInfoService.getCatalog(Constant.CATALOGS.TINH_TRANG_NT).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) this.listRelationStatus = res.data;
    });
  }

  getPolicyTypeList() {
    this.staffInfoService.getCatalog(Constant.CATALOGS.DOITUONG_CHINHSACH).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) this.listPolicyType = res.data;
    });
  }

  searchReferenceEmployee(ref: EmployeeDetail) {
    const employeeId = ref.employeeId;
    this.subs.push(
      this.personalInfoService.getPersonalInfo(employeeId).subscribe({
        next: (res) => {
          const personalInfo = Utils.processResponse(res, this.toastService, this.translate);
          this.patchValueToForm(ref, personalInfo);
        },
        error: (err) => {
          this.toastService.error(err?.message);
        }
      })
    );
  }

  patchValueToForm(ref: EmployeeDetail, personalInfo: PersonalInfo) {
    this.form.patchValue({
      referenceEmployeeId: ref.employeeId,
      fullName: ref.fullName,
      workOrganization: ref.orgName,

      personalIdNumber: personalInfo?.personalIdNumber ? personalInfo.personalIdNumber : null,
      dateOfBirth: personalInfo?.dateOfBirth ? Utils.convertDateToFillForm(personalInfo.dateOfBirth) : null,
      job: personalInfo?.jobName ? personalInfo.jobName : null,
      currentAddress: personalInfo?.currentAddress ? personalInfo.currentAddress : null,
    });
  }

  ngOnDestroy(): void {
    this.subs?.forEach(sub => sub?.unsubscribe());
  }

}
