import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EditRelativesInfoComponent} from "./edit-relatives-info/edit-relatives-info.component";
import {NzFormModule} from "ng-zorro-antd/form";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";
import {SharedUiEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/employee-data-picker";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedDirectivesNumberInputModule} from "@hcm-mfe/shared/directives/number-input";
import {SharedDirectivesAlphabeticInputModule} from "@hcm-mfe/shared/directives/alphabetic-input";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
    imports: [CommonModule, NzFormModule, ReactiveFormsModule, TranslateModule, SharedUiMbSelectModule, NzCheckboxModule, SharedUiEmployeeDataPickerModule, FormsModule, SharedUiMbInputTextModule, SharedDirectivesNumberInputModule, SharedDirectivesAlphabeticInputModule, SharedUiMbDatePickerModule, SharedUiLoadingModule],
  declarations: [EditRelativesInfoComponent],
  exports: [EditRelativesInfoComponent],
})
export class StaffManagerFeatureInfoEditRelativesInfoModule {}
