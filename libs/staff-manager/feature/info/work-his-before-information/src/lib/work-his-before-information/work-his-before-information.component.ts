import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ToastrService} from "ngx-toastr";
import {NzModalRef, NzModalService} from "ng-zorro-antd/modal";
import {TranslateService} from "@ngx-translate/core";
import {AppFunction, BaseResponse, MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {WorkBeforeInfo} from "@hcm-mfe/staff-manager/data-access/models/info";
import {ShareDataService, WorkBeforeHisService} from "@hcm-mfe/staff-manager/data-access/services";
import {SessionService} from "@hcm-mfe/shared/common/store";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {ValidateService} from "@hcm-mfe/shared/core";
import {HTTP_STATUS_CODE, Mode} from "@hcm-mfe/shared/common/constants";
import {AddWorkHisBeforeComponent} from "@hcm-mfe/staff-manager/feature/info/add-work-his-before";
import {NzSafeAny} from "ng-zorro-antd/core/types";

@Component({
  selector: 'app-work-his-before-information',
  templateUrl: './work-his-before-information.component.html',
  styleUrls: ['./work-his-before-information.component.scss'],
})
export class WorkHisBeforeInformationComponent implements OnInit {
  objFunction: AppFunction;
  tableConfig!: MBTableConfig;
  dataTable: WorkBeforeInfo[] = [];
  employeeId: number | null = null;
  modal?: NzModalRef;
  pagination = new Pagination();

  @ViewChild('actionTmpl', { static: true }) actionTmpl!: TemplateRef<NzSafeAny>;

  constructor(
    public validateService: ValidateService,
    private http: HttpClient,
    private toastService: ToastrService,
    private modalService: NzModalService,
    private translate: TranslateService,
    private shareDataService: ShareDataService,
    private workBeforeHisService: WorkBeforeHisService,
    private sessionService: SessionService,
  ) {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.HR_WORK_OUTSIDE}`);
  }

  ngOnInit(): void {
    this.shareDataService.employee$.subscribe(employee => {
      this.employeeId = employee.employeeId;
      this.initTable();
      this.doSearch(1);
    })
  }

  initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'staffManager.workHisBeforeInformation.label.table.fromDate',
          field: 'fromDate',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 100
        },
        {
          title: 'staffManager.workHisBeforeInformation.label.table.toDate',
          field: 'toDate',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 100
        },
        {
          title: 'staffManager.workHisBeforeInformation.label.table.organizationName',
          field: 'organizationName',
          width: 150
        },
        {
          title: 'staffManager.workHisBeforeInformation.label.table.positionName',
          field: 'positionName',
          width: 100
        },
        {
          title: 'staffManager.workHisBeforeInformation.label.table.reasonLeave',
          field: 'reasonLeave',
          width: 100
        },
        {
          title: 'staffManager.workHisBeforeInformation.label.table.rewardInfo',
          field: 'rewardInfo',
          width: 100
        },
        {
          title: 'staffManager.workHisBeforeInformation.label.table.decplineInfo',
          field: 'decplineInfo',
          width: 100
        },
        {
          title: 'staffManager.workHisBeforeInformation.label.attributes.referPerson',
          field: 'referPerson',
          width: 100,
          show: false
        },
        {
          title: 'staffManager.workHisBeforeInformation.label.attributes.referPersonPosition',
          field: 'referPersonPosition',
          width: 100,
          show: false
        },
        {
          title: 'staffManager.workHisBeforeInformation.label.attributes.mission',
          field: 'mission',
          width: 100,
          show: false
        },
        {
          title: ' ',
          field: 'action',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          fixed: true,
          fixedDir: "right",
          tdTemplate: this.actionTmpl,
          width: 80,
          show: this.objFunction?.delete || this.objFunction?.edit
        }
      ],
      total: 0,
      needScroll: true,
      loading: true,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  doSearch(pageNumber: number) {
    this.tableConfig = { ...this.tableConfig, loading: true };
    this.pagination.pageNumber = pageNumber;
    const param = this.pagination.getCurrentPage();
    this.workBeforeHisService.getList(this.employeeId, param).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.dataTable = res.data.listData;
        this.tableConfig.total = res.data.count;
      } else {
        this.toastService.error(res?.message);
      }
      this.tableConfig = { ...this.tableConfig, loading: false };
    }, () => {
      this.tableConfig = { ...this.tableConfig, loading: false };
      this.dataTable = []
    })
  }

  showModalUpdate(workedOutsideId: number, footerTmpl: TemplateRef<Record<string, never>>) {
    const data = this.dataTable.find(item => item.workedOutsideId === workedOutsideId);
    this.modal = this.modalService.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
      nzTitle: this.translate.instant('staffManager.workHisBeforeInformation.modal.update'),
      nzContent: AddWorkHisBeforeComponent,
      nzComponentParams: {
        mode: Mode.EDIT,
        data: data
      },
      nzFooter: footerTmpl
    });
    this.modal.afterClose.subscribe(result => result?.refresh ? this.doSearch(1): '')
  }

  deleteItem(workedOutsideId: number) {
    this.workBeforeHisService.deleteRecord(workedOutsideId).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.doSearch(1);
        this.toastService.success(this.translate.instant('common.notification.deleteSuccess'));
      } else {
        this.toastService.error(res?.message);
      }
    }, () => {
      this.toastService.error(this.translate.instant('common.notification.deleteError'));
    })
  }

  refresh() {
    this.doSearch(1);
  }

}
