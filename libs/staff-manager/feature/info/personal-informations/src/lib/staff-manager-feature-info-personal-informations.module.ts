import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PersonalInformationsComponent} from "./personal-informations/personal-informations.component";
import {SharedUiEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/employee-data-picker";
import {FormsModule} from "@angular/forms";
import {NzFormModule} from "ng-zorro-antd/form";
import {SharedDirectivesScrollSpyModule} from "@hcm-mfe/shared/directives/scroll-spy";
import {StaffManagerFeatureInfoBriefDescriptionModule} from "@hcm-mfe/staff-manager/feature/info/brief-description";
import {SharedUiMbScrollTabsModule} from "@hcm-mfe/shared/ui/mb-scroll-tabs";
import {SharedUiMbCollapseModule} from "@hcm-mfe/shared/ui/mb-collapse";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {TranslateModule} from "@ngx-translate/core";
import {RouterModule} from "@angular/router";

@NgModule({
    imports: [CommonModule, SharedUiEmployeeDataPickerModule, FormsModule, NzFormModule, SharedDirectivesScrollSpyModule,
        StaffManagerFeatureInfoBriefDescriptionModule, SharedUiMbScrollTabsModule, SharedUiMbCollapseModule, SharedUiMbInputTextModule,
        SharedUiMbButtonModule, TranslateModule,
        RouterModule.forChild([
            {
                path: '',
                component: PersonalInformationsComponent
            }
        ])
    ],
  declarations: [PersonalInformationsComponent],
  exports: [PersonalInformationsComponent],
})
export class StaffManagerFeatureInfoPersonalInformationsModule {}
