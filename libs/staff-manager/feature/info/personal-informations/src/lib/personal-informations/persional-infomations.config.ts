import { PanelOption } from '@hcm-mfe/shared/data-access/models';
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {PersonalInformationComponent} from "@hcm-mfe/staff-manager/feature/info/personal-information";
import {EditPersonalInfoComponent} from "@hcm-mfe/staff-manager/feature/info/edit-personal-info";
import {IdentityInformationComponent} from "@hcm-mfe/staff-manager/feature/info/identity-information";
import {EditIdentityInfoComponent} from "@hcm-mfe/staff-manager/feature/info/edit-identity-info";
import {ContactInformationComponent} from "@hcm-mfe/staff-manager/feature/info/contact-information";
import {EditContactInfoComponent} from "@hcm-mfe/staff-manager/feature/info/edit-contact-info";
import {BankInformationComponent} from "@hcm-mfe/staff-manager/feature/info/bank-information";
import {EditBankInfoComponent} from "@hcm-mfe/staff-manager/feature/info/edit-bank-info";
import {PartyInformationComponent} from "@hcm-mfe/staff-manager/feature/info/party-information";
import {EditPartyInfoComponent} from "@hcm-mfe/staff-manager/feature/info/edit-party-info";
import {RelativesInformationComponent} from "@hcm-mfe/staff-manager/feature/info/relatives-information";
import {EditRelativesInfoComponent} from "@hcm-mfe/staff-manager/feature/info/edit-relatives-info";
import {FamilyDeductionsInformationComponent} from "@hcm-mfe/staff-manager/feature/info/family-deductions-information";
import {AddFamilyDeductionsInfoComponent} from "@hcm-mfe/staff-manager/feature/info/add-family-deductions-info";
import {WorkHisBeforeInformationComponent} from "@hcm-mfe/staff-manager/feature/info/work-his-before-information";
import {AddWorkHisBeforeComponent} from "@hcm-mfe/staff-manager/feature/info/add-work-his-before";
import {WorkHisInformationComponent} from "@hcm-mfe/staff-manager/feature/info/work-his-information";
import {EditWorkHisInfoComponent} from "@hcm-mfe/staff-manager/feature/info/edit-work-his-info";
import {ProjectHisInformationComponent} from "@hcm-mfe/staff-manager/feature/info/project-his-information";
import {AddProjectHisInfoComponent} from "@hcm-mfe/staff-manager/feature/info/add-project-his-info";
import {ContractHisInformationComponent} from "@hcm-mfe/staff-manager/feature/info/contract-his-information";
import {AddContractHisInfoComponent} from "@hcm-mfe/staff-manager/feature/info/add-contract-his-info";
import {SalaryHisInformationComponent} from "@hcm-mfe/staff-manager/feature/info/salary-his-information";
import {AddSalaryInfoComponent} from "@hcm-mfe/staff-manager/feature/info/add-salary-info";
import {AllowanceHisInformationComponent} from "@hcm-mfe/staff-manager/feature/info/allowance-his-information";
import {AddAllowanceHisComponent} from "@hcm-mfe/staff-manager/feature/info/add-allowance-his";
import {DegreeInformationComponent} from "@hcm-mfe/staff-manager/feature/info/degree-information";
import {AddDegreeInfoComponent} from "@hcm-mfe/staff-manager/feature/info/add-degree-info";
import {EduHisInformationComponent} from "@hcm-mfe/staff-manager/feature/info/edu-his-infomation";
import {AddEduHisInfoComponent} from "@hcm-mfe/staff-manager/feature/info/add-edu-his-info";
import {RewardHisInformationComponent} from "@hcm-mfe/staff-manager/feature/info/reward-his-information";
import {AddRewardHisComponent} from "@hcm-mfe/staff-manager/feature/info/add-reward-his";
import {DisciplinaryHisInformationComponent} from "@hcm-mfe/staff-manager/feature/info/disciplinary-his-information";
import {AddDisciplinaryHisComponent} from "@hcm-mfe/staff-manager/feature/info/add-disciplinary-his";
import {DocumentAttachedInformationComponent} from "@hcm-mfe/staff-manager/feature/info/document-attached-information";
import {ActionDocumentAttachedComponent} from "@hcm-mfe/staff-manager/feature/info/action-document-attached";
import {
  ConcurrentProcessInformationComponent
} from "@hcm-mfe/staff-manager/feature/info/concurrent-process-information";
import {EditConcurrentProcessInfoComponent} from "@hcm-mfe/staff-manager/feature/info/edit-concurrent-process-info";
import {EvaluateInfoComponent} from "@hcm-mfe/staff-manager/feature/info/evaluate-info";

export const PANELS_INFORMATION: PanelOption[] = [
  {
    id: 'personal_information',
    active: true,
    disabled: false,
    icon: 'assets/icon/staff-manager/personal-info.svg',
    code: FunctionCode.HR_PERSONAL_INFO,
    name: 'staffManager.panel.personalInformation',
    panelComponent: PersonalInformationComponent,
    extraMode: [{
      type: 'EDIT_MODAL',
      contentHeader: 'staffManager.panel.personalInformation',
      content: EditPersonalInfoComponent
    }]
  },
  {
    id: 'identity_information',
    active: true,
    disabled: false,
    icon: 'assets/icon/staff-manager/identification.svg',
    code: FunctionCode.HR_IDENTITY,
    name: 'staffManager.panel.identityInformation',
    panelComponent: IdentityInformationComponent,
    extraMode: [{
      type: 'EDIT_MODAL',
      contentHeader: 'staffManager.panel.identityInformation',
      content: EditIdentityInfoComponent
    }]
  },
  {
    id: 'contact_information',
    active: true,
    disabled: false,
    icon: 'assets/icon/staff-manager/contact-info.svg',
    code: FunctionCode.HR_PERSONAL_INFO,
    name: 'staffManager.panel.contactInformation',
    panelComponent: ContactInformationComponent,
    extraMode: [{
      type: 'EDIT_MODAL',
      contentHeader: 'staffManager.panel.contactInformation',
      content: EditContactInfoComponent
    }]
  },
  {
    id: 'bank_information',
    active: true,
    disabled: false,
    icon: 'assets/icon/staff-manager/credit-card.svg',
    code: FunctionCode.HR_ACCOUNT,
    name: 'staffManager.panel.bankInformation',
    panelComponent: BankInformationComponent,
    extraMode: [{
      type: 'EDIT_MODAL',
      contentHeader: 'staffManager.panel.bankInformation',
      content: EditBankInfoComponent
    }]
  },
  {
    id: 'party_information',
    active: true,
    disabled: false,
    icon: 'assets/icon/staff-manager/collection-bookmark.svg',
    code: FunctionCode.HR_PERSONAL_INFO,
    name: 'staffManager.panel.partyInformation',
    panelComponent: PartyInformationComponent,
    extraMode: [{
      type: 'EDIT_MODAL',
      contentHeader: 'staffManager.panel.partyInformation',
      content: EditPartyInfoComponent
    }]
  },
  {
    id: 'relatives_information',
    active: true,
    disabled: false,
    icon: 'assets/icon/staff-manager/list-alt.svg',
    code: FunctionCode.HR_FAMILY_RELATIONSHIP,
    name: 'staffManager.panel.relativesInformation',
    panelComponent: RelativesInformationComponent,
    extraMode: [{
      type: 'ADD_MODAL',
      content: EditRelativesInfoComponent,
      contentHeader: 'staffManager.relativesInformation.modal.add'
    }]
  },
  {
    id: 'family_deductions_information',
    active: true,
    disabled: false,
    icon: 'assets/icon/staff-manager/list-alt.svg',
    code: FunctionCode.HR_DEPENDENT_PERSON,
    name: 'staffManager.panel.familyDeductionsInformation',
    panelComponent: FamilyDeductionsInformationComponent,
    extraMode: [{
      type: 'ADD_MODAL',
      content: AddFamilyDeductionsInfoComponent,
      contentHeader: 'staffManager.familyDeductionsInformation.modal.add'
    }]
  },
  {
    id: 'work_his_before_information',
    active: true,
    disabled: false,
    icon: 'assets/icon/staff-manager/history.svg',
    code: FunctionCode.HR_WORK_OUTSIDE,
    name: 'staffManager.panel.workHisBeforeInformation',
    panelComponent: WorkHisBeforeInformationComponent,
    extraMode: [{
      type: 'ADD_MODAL',
      content: AddWorkHisBeforeComponent,
      contentHeader: 'staffManager.workHisBeforeInformation.modal.add'
    }]
  },
  {
    id: 'work_his_information',
    active: true,
    disabled: false,
    icon: 'assets/icon/staff-manager/history.svg',
    code: FunctionCode.HR_WORK_PROCESS,
    name: 'staffManager.panel.workHisInformation',
    panelComponent: WorkHisInformationComponent,
    extraMode: [{
      type: 'ADD_MODAL',
      content: EditWorkHisInfoComponent,
      contentHeader: 'staffManager.workHisInformation.modal.add'
    }]
  },
  {
    id: 'concurrent_process_information',
    active: true,
    disabled: false,
    icon: 'assets/icon/staff-manager/history.svg',
    code: FunctionCode.HR_CONCURRENT_PROCESS,
    name: 'staffManager.panel.concurrentProcessInformation',
    panelComponent: ConcurrentProcessInformationComponent,
    extraMode: [{
      type: 'ADD_MODAL',
      content: EditConcurrentProcessInfoComponent,
      contentHeader: 'staffManager.concurrentProcessInformation.modal.add'
    }]
  },
  {
    id: 'project_his_information',
    active: true,
    disabled: false,
    icon: 'assets/icon/staff-manager/history.svg',
    code: FunctionCode.HR_PROJECT_MEMBER,
    name: 'staffManager.panel.projectHisInformation',
    panelComponent: ProjectHisInformationComponent,
    extraMode: [{
      type: 'ADD_MODAL',
      content: AddProjectHisInfoComponent,
      contentHeader: 'staffManager.projectHisInfo.modal.add'
    }]
  },
  {
    id: 'contract_his_information',
    active: true,
    disabled: false,
    icon: 'assets/icon/staff-manager/history.svg',
    code: FunctionCode.HR_CONTRACT_PROCESS,
    name: 'staffManager.panel.contractHisInformation',
    panelComponent: ContractHisInformationComponent,
    extraMode: [{
      type: 'ADD_MODAL',
      content: AddContractHisInfoComponent,
      contentHeader: 'staffManager.contractHisInfo.modal.add'
    }]
  },
  {
    id: 'evaluate_information',
    active: true,
    disabled: false,
    icon: 'assets/icon/self-staff-manager/history.svg',
    name: 'staffManager.panel.evaluateInformation',
    panelComponent: EvaluateInfoComponent
  },
  {
    id: 'salary_his_information',
    active: true,
    disabled: false,
    icon: 'assets/icon/staff-manager/history.svg',
    code: FunctionCode.HR_SALARY_PROCESS,
    name: 'staffManager.panel.salaryHisInformation',
    panelComponent: SalaryHisInformationComponent,
    extraMode: [{
      type: 'ADD_MODAL',
      content: AddSalaryInfoComponent,
      contentHeader: 'staffManager.salaryHisInformation.modal.add'
    }]
  },
  {
    id: 'allowance_his_information',
    active: true,
    disabled: false,
    icon: 'assets/icon/staff-manager/history.svg',
    code: FunctionCode.HR_ALLOWANCE_PROCESS,
    name: 'staffManager.panel.allowanceHisInformation',
    panelComponent: AllowanceHisInformationComponent,
    extraMode: [{
      type: 'ADD_MODAL',
      content: AddAllowanceHisComponent,
      contentHeader: 'staffManager.allowanceHisInformation.modal.add'
    }]
  },
  {
    id: 'degree_information',
    active: true,
    disabled: false,
    icon: 'assets/icon/staff-manager/list-alt.svg',
    code: FunctionCode.HR_EDU_DEGREE,
    name: 'staffManager.panel.degreeInformation',
    panelComponent: DegreeInformationComponent,
    extraMode: [{
      type: 'ADD_MODAL',
      content: AddDegreeInfoComponent,
      contentHeader: 'staffManager.degreeInformation.modal.add'
    }]
  },
  {
    id: 'edu_his_information',
    active: true,
    disabled: false,
    icon: 'assets/icon/staff-manager/list-alt.svg',
    code: FunctionCode.HR_EDU_PROCESS,
    name: 'staffManager.panel.eduHisInformation',
    panelComponent: EduHisInformationComponent,
    extraMode: [{
      type: 'ADD_MODAL',
      content: AddEduHisInfoComponent,
      contentHeader: 'staffManager.eduHisInformation.modal.add'
    }]
  },
  {
    id: 'reward_his_information',
    active: true,
    disabled: false,
    icon: 'assets/icon/staff-manager/list-alt.svg',
    code: FunctionCode.HR_REWARD,
    name: 'staffManager.panel.rewardHisInformation',
    panelComponent: RewardHisInformationComponent,
    extraMode: [{
      type: 'ADD_MODAL',
      content: AddRewardHisComponent,
      contentHeader: 'staffManager.rewardHisInformation.modal.add'
    }]
  },
  {
    id: 'disciplinary_his_information',
    active: true,
    disabled: false,
    icon: 'assets/icon/staff-manager/list-alt.svg',
    code: FunctionCode.HR_DECPLINE,
    name: 'staffManager.panel.disciplinaryHisInformation',
    panelComponent: DisciplinaryHisInformationComponent,
    extraMode: [{
      type: 'ADD_MODAL',
      content: AddDisciplinaryHisComponent,
      contentHeader: 'staffManager.disciplinaryHisInformation.modal.add'
    }]
  },
  {
    id: 'document_attached_information',
    active: true,
    disabled: false,
    icon: 'assets/icon/staff-manager/list-alt.svg',
    code: FunctionCode.HR_PROFILE,
    name: 'staffManager.panel.documentAttachedInformation',
    panelComponent: DocumentAttachedInformationComponent,
    extraMode: [
      {
        type: 'ADD_MODAL',
        content: ActionDocumentAttachedComponent,
        contentHeader: 'staffManager.documentAttachedInformation.label.add'
      },
      {
        type: 'SEARCH',
        content: "",
      },
    ]
  },
];
