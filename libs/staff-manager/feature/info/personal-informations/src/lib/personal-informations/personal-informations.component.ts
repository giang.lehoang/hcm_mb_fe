import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {NzModalRef, NzModalService} from 'ng-zorro-antd/modal';
import {PANELS_INFORMATION} from './persional-infomations.config';
import {TranslateService} from '@ngx-translate/core';
import {SCROLL_TABS_DATA} from './scroll-tab.config';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {FunctionCode, Scopes} from "@hcm-mfe/shared/common/enums";
import {MbCollapseComponent} from "@hcm-mfe/shared/ui/mb-collapse";
import {PersonalInfoService, ShareDataService} from "@hcm-mfe/staff-manager/data-access/services";
import {Mode, STORAGE_NAME} from "@hcm-mfe/shared/common/constants";
import {PersonalInfo} from "@hcm-mfe/staff-manager/data-access/models/info";
import {SessionService, StorageService} from '@hcm-mfe/shared/common/store';
import {PanelOption} from '@hcm-mfe/shared/data-access/models';
import {NzSafeAny} from "ng-zorro-antd/core/types";


@Component({
  selector: 'app-personal-infomations',
  templateUrl: './personal-infofmations.component.html',
  styleUrls: ['./personal-informations.component.scss']
})
export class PersonalInformationsComponent implements OnInit {
  public panels = PANELS_INFORMATION;
  public scrollTabs: NzSafeAny = SCROLL_TABS_DATA.filter(tab => this.sessionService.getSessionData(`FUNCTION_${tab.code}`).view);
  formSearch: FormGroup;
  modal?: NzModalRef;
  employeeId: number | NzSafeAny;
  employeeCode: string | NzSafeAny;
  scope: string = Scopes.VIEW;
  functionCode: string = FunctionCode.HR_PERSONAL_INFO;
  pageName: string;
  option: NzSafeAny;

  @ViewChild('collapse') collapse!: MbCollapseComponent;
  @ViewChild('footerTmpl') footerTmpl!: TemplateRef<Record<string, never>>;

  constructor(
    private modalService: NzModalService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private translate: TranslateService,
    private staffInfoService: PersonalInfoService,
    private shareService: ShareDataService,
    private fb: FormBuilder,
    private sessionService: SessionService,
  ) {
    const userLogin = StorageService.get(STORAGE_NAME.USER_LOGIN);
    this.pageName = this.translate.instant(this.activatedRoute.snapshot.data['pageName']);
    this.activatedRoute.queryParams.subscribe((params) => {
      this.employeeId = params['employeeId'] ?? userLogin.employeeId ?? 1;
    });
    this.formSearch = this.fb.group({
      formStaff: null,
      employeeCode: null
    });
  }

  ngOnInit(): void {
    this.shareService.changeEmployee({employeeId: this.employeeId});
  }

  searchStaff(staffInfo: PersonalInfo) {
    if (staffInfo)
      this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        this.router.navigate(['/staff/personal-info'], {queryParams: {employeeId: staffInfo.employeeId}})
      });
  }

  openEditModal(panel: PanelOption | any, type: string) {
    const extraOption = panel.extraMode.find((mode: any) => mode.type === type);
    const instance = this.collapse.getInstance(panel);
    this.modal = this.modalService.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
      nzTitle: this.translate.instant(extraOption.contentHeader),
      nzContent: extraOption.content,
      nzComponentParams: {
        mode: Mode.EDIT,
        data: instance.items
      },
      nzFooter: this.footerTmpl
    });

    this.modal.afterClose.subscribe(res => {
      if (res && res.refresh) {
        instance.refresh();
      }
    });
  }

  openAddModal(panel: PanelOption | any, type: string) {
    const extraOption = panel.extraMode.find((mode: any) => mode.type === type);
    const instance = this.collapse.getInstance(panel);

    this.modal = this.modalService.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
      nzTitle: this.translate.instant(extraOption.contentHeader),
      nzContent: extraOption.content,
      nzComponentParams: {
        mode: Mode.ADD
      },
      nzFooter: this.footerTmpl
    });
    this.modal.afterClose.subscribe(res => {
      if (res && res.refresh) {
        instance.refresh();
      }
    });
  }

  onSearchChange(panel: PanelOption, value: string) {
    const instance = this.collapse.getInstance(panel);
    instance.searchData(value);
  }

  checkExtraMode = (panel: PanelOption | NzSafeAny, type: string) => panel.extraMode.find((mode: NzSafeAny) => mode.type === type);

  checkShowAction = (panel: PanelOption) => this.sessionService.getSessionData(`FUNCTION_${panel.code}`).edit;

  cancel() {
    this.modal?.destroy();
  }

  save() {
    this.modal?.getContentComponent().save();
  }

  onEmpInfoReady(event: PersonalInfo) {
    this.option = {
      pageName: this.pageName,
      empCode: event.employeeCode,
      empName: event.fullName
    }
  }

}
