import {FunctionCode} from "@hcm-mfe/shared/common/enums";

export const SCROLL_TABS_DATA = [
  {
    title:'staffManager.tab.personalInformation',
    code: FunctionCode.HR_PERSONAL_INFO,
    scrollTo: 'personal_information'
  },
  {
    title:'staffManager.tab.workHisBeforeInformation',
    code: FunctionCode.HR_WORK_OUTSIDE,
    scrollTo: 'work_his_before_information'
  },
  {
    title:'staffManager.tab.workHisInformation',
    code: FunctionCode.HR_WORK_PROCESS,
    scrollTo: 'work_his_information'
  },
  {
    title:'staffManager.tab.eduHisInformation',
    code: FunctionCode.HR_EDU_PROCESS,
    scrollTo: 'edu_his_information'
  },
  {
    title:'staffManager.tab.rewardHisInformation',
    code: FunctionCode.HR_REWARD,
    scrollTo: 'reward_his_information'
  },
  {
    title:'staffManager.tab.documentAttachedInformation',
    code: FunctionCode.HR_PROFILE,
    scrollTo: 'document_attached_information',
  },

]
