import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {TranslateService} from '@ngx-translate/core';
import * as moment from 'moment';
import {NzModalRef} from 'ng-zorro-antd/modal';
import {ToastrService} from 'ngx-toastr';
import {Subscription} from 'rxjs';
import {NzUploadFile} from 'ng-zorro-antd/upload';
import {saveAs} from 'file-saver';
import {
  DegreeInfoService,
  DownloadFileAttachService,
  ShareDataService,
  StaffInfoService,
} from "@hcm-mfe/staff-manager/data-access/services";
import {Constant} from "@hcm-mfe/staff-manager/data-access/common";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";
import {beforeUploadFile, getTypeExport} from "@hcm-mfe/shared/common/utils";
import {DegreeInfo, FacultyInfo, LookupValues, SchoolInfo} from "@hcm-mfe/staff-manager/data-access/models/info";
import {NzSafeAny} from "ng-zorro-antd/core/types";

@Component({
  selector: 'app-add-degree-info',
  templateUrl: './add-degree-info.component.html',
  styleUrls: ['./add-degree-info.component.scss']
})
export class AddDegreeInfoComponent implements OnInit, OnDestroy {
  @Input() data: NzSafeAny;
  isSubmitted = false;
  form: FormGroup;
  subs: Subscription[] = [];
  degreeTypes: LookupValues[] = [];
  eduRankCodes: LookupValues[] = [];
  faculty: NzSafeAny;
  school: NzSafeAny;
  listSchool: SchoolInfo[] = [];
  listFaculty: FacultyInfo[] = [];
  listMajorLevel: LookupValues[] = [];
  fileList: NzUploadFile[] = [];
  docIdsDelete: number[] = [];
  isLoadingPage = false;

    constructor(
    private fb: FormBuilder,
    private degreeInfoService: DegreeInfoService,
    private staffInfoService: StaffInfoService,
    private toastService: ToastrService,
    private translate: TranslateService,
    private shareDataService: ShareDataService,
    private modalRef: NzModalRef,
    private downloadFileAttachService: DownloadFileAttachService,
    ) {
    this.form = this.fb.group({
      educationDegreeId: null,
      degreeTypeCode: [null, Validators.required],
      issueYear: null,
      schoolId: [null, Validators.required],
      schoolName: [null],
      facultyId: [null, Validators.required],
      facultyName: [null],
      majorLevelId: [null, Validators.required],
      eduRankCode: null,
      isHighest: null,
      note: null
    });
  }

  ngOnInit(): void {
    this.getEduRank();
    this.getDegreeType();
    this.getListSchool();
    this.getListFaculty();
    this.getListMajorLevel();
    this.pathValue().then();
  }

  getDegreeType() {
    this.subs.push(
      this.staffInfoService.getCatalog(Constant.CATALOGS.LOAI_BANG_CAP).subscribe(res => {
        const response: BaseResponse = res;
        this.degreeTypes = response?.data;
      })
    );
  }

  getEduRank() {
    this.subs.push(
      this.staffInfoService.getCatalog(Constant.CATALOGS.XEP_LOAI_TOT_NGHIEP).subscribe(res => {
        const response: BaseResponse = res;
        this.eduRankCodes = response?.data;
      })
    );
  }

  getListSchool() {
    this.subs.push(
      this.staffInfoService.getListSchool().subscribe(res => {
        const response: BaseResponse = res;
        this.listSchool = response?.data;
      })
    );
  }

  getListFaculty() {
    this.subs.push(
      this.staffInfoService.getListFaculty().subscribe(res => {
        const response: BaseResponse = res;
        this.listFaculty = response?.data;
      })
    );
  }

  getListMajorLevel() {
    this.subs.push(
      this.staffInfoService.getListMajorLevel().subscribe(res => {
        const response: BaseResponse = res;
        this.listMajorLevel = response?.data;
      })
    );
  }

  ngOnDestroy() {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  async pathValue() {
    if (this.data) {
      this.getListSchool();
      this.getListFaculty();
      this.isLoadingPage = true;
      const degreeInfoResponse = await this.degreeInfoService.getDegreeInfoDetail(this.data?.educationDegreeId).toPromise();
      const response: BaseResponse = degreeInfoResponse;
      if (response) {
        this.isLoadingPage = false;
      }
      const degreeInfo: DegreeInfo = response?.data;
      this.staffInfoService.getListFaculty().subscribe(res => {
        this.listFaculty = res?.data;
        this.listFaculty.forEach((data) => {
          if(data.facultyId === degreeInfo?.facultyId) {
            this.faculty = data;
            this.form.controls['facultyName'].setValidators(data.isTextManual == 1 ? [Validators.required, Validators.maxLength(200)]: []);
          }
        });
      });

      this.staffInfoService.getListSchool().subscribe(res => {
        this.listSchool = res?.data;
        this.listSchool.forEach((data) => {
          if(data.schoolId === degreeInfo?.schoolId) {
            this.school = data;
            this.form.controls['schoolName'].setValidators(data.isTextManual == 1 ? [Validators.required, Validators.maxLength(200)]: []);
          }
        })
      });
      this.form.patchValue({
        educationDegreeId: degreeInfo?.educationDegreeId,
        issueYear: degreeInfo?.issueYear ? moment(degreeInfo?.issueYear, 'YYYY').toDate() : null,
        degreeName: degreeInfo?.degreeName,
        schoolId: degreeInfo?.schoolId,
        facultyId: degreeInfo?.facultyId,
        degreeTypeCode: degreeInfo?.degreeTypeCode,
        majorLevelId: degreeInfo?.majorLevelId,
        eduRankCode: degreeInfo?.eduRankCode,
        isRelatedJob: degreeInfo?.isRelatedJob == 1,
        isHighest: degreeInfo?.isHighest == 1,
        note: degreeInfo?.note ? degreeInfo?.note : null,
        schoolName:degreeInfo?.schoolName,
        facultyName:degreeInfo?.facultyName,
      })

      if (this.data?.attachFileList && this.data?.attachFileList.length > 0) {
        this.data?.attachFileList.forEach((item: NzSafeAny) => {
          this.fileList.push({
            uid: item.docId,
            name: item.fileName,
            url: item.security,
            status: 'done'
          });
        });
        this.fileList = [...this.fileList];
      }

    }
  }

  beforeUpload = (file: NzUploadFile): boolean => {
    this.fileList =  beforeUploadFile(file, this.fileList, 3000000, this.toastService, this.translate, 'common.notification.errorFile', '.PDF')
    return false;
  };

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      this.isLoadingPage = true;
      const value = this.form.value;
      value.issueYear = value.issueYear ? + moment(value.issueYear).format('YYYY') : null;
      value.isHighest = value.isHighest ? 1 : 0;
      value.employeeId = this.shareDataService.getEmployee().employeeId;
      value.docIdsDelete = this.docIdsDelete;
      const formData = new FormData();
      formData.append('data', new Blob([JSON.stringify(value)], {
        type: 'application/json'
      }));
      this.fileList.forEach((nzFile: NzSafeAny) => {
        formData.append('files', nzFile);
      });
      this.degreeInfoService.saveDegreeInfo(formData).subscribe(res => {
        if (res && res.code === 200) {
          this.toastService.success(this.translate.instant('common.notification.updateSuccess'));
          this.modalRef.close({refresh: true});
        } else {
          this.toastService.error(this.translate.instant('common.notification.updateError') + ': ' + res?.message);
          this.modalRef.close({refresh: false});
        }
        this.isLoadingPage = false;
      }, () => {
        this.toastService.error(this.translate.instant('common.notification.updateError'));
        this.modalRef.close({refresh: false});
        this.isLoadingPage = false;
      });
    }
  }

  changeFaculty(event:number) {
    this.listFaculty.forEach((data) => {
      if(data.facultyId === event) {
        this.faculty = data;
        this.form.controls['facultyName'].setValue(null);
        this.form.controls['facultyName'].setValidators(data.isTextManual == 1 ? [Validators.required, Validators.maxLength(200)]: []);
      }
    })
  }

  changeSchool(event:number) {
    this.listSchool.forEach((data) => {
      if(data.schoolId === event) {
        this.school = data;
        this.form.controls['schoolName'].setValue(null);
        this.form.controls['schoolName'].setErrors(null);
        this.form.controls['schoolName'].setValidators(data.isTextManual == 1 ? [Validators.required, Validators.maxLength(200)]: []);
      }
    })
  }

    downloadFile = (file: NzUploadFile) => {
        if (file?.uid) {
            this.downloadFileAttachService.doDownloadAttachFileWithSecurity(~~file?.uid, file?.url).subscribe(res => {
                const reportFile = new Blob([res.body], { type: getTypeExport(file.name.split('.').pop()) });
                saveAs(reportFile, file.name);
            });
        }
    }

    removeFile = (file: NzUploadFile) => {
        this.docIdsDelete.push(Number(file.uid));
        const index = this.fileList.indexOf(file);
        this.fileList.splice(index, 1);
        return true;
    }
}
