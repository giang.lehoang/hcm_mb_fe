import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AddDegreeInfoComponent} from "./add-degree-info/add-degree-info.component";
import {SharedUiCoreModule} from "@hcm-mfe/shared/ui/core";
import {NzFormModule} from "ng-zorro-antd/form";
import {ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";
import {NzUploadModule} from "ng-zorro-antd/upload";
import {NzIconModule} from "ng-zorro-antd/icon";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
    imports: [CommonModule, NzFormModule, ReactiveFormsModule, SharedUiMbSelectModule, TranslateModule, SharedUiMbDatePickerModule, SharedUiMbInputTextModule, NzCheckboxModule, NzUploadModule, NzIconModule, SharedUiLoadingModule],
  declarations: [AddDegreeInfoComponent],
  exports: [AddDegreeInfoComponent],
})
export class StaffManagerFeatureInfoAddDegreeInfoModule {}
