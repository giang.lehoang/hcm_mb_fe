import {NzModalRef, NzModalService} from 'ng-zorro-antd/modal';
import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";
import {AppFunction, BaseResponse, MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {FamilyDeductionInfo} from "@hcm-mfe/staff-manager/data-access/models/info";
import {ValidateService} from "@hcm-mfe/shared/core";
import {FamilyDeductionInfoService, ShareDataService} from "@hcm-mfe/staff-manager/data-access/services";
import {SessionService} from "@hcm-mfe/shared/common/store";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {HTTP_STATUS_CODE, Mode} from "@hcm-mfe/shared/common/constants";
import {AddFamilyDeductionsInfoComponent} from "@hcm-mfe/staff-manager/feature/info/add-family-deductions-info";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {StringUtils} from "@hcm-mfe/shared/common/utils";

@Component({
  selector: 'app-family-deductions-information',
  templateUrl: './family-deductions-information.component.html',
  styleUrls: ['./family-deductions-information.component.scss'],
})
export class FamilyDeductionsInformationComponent implements OnInit {
  objFunction: AppFunction;
  tableConfig!: MBTableConfig;
  dataTable: FamilyDeductionInfo[] = [];
  employeeId: number | null = null;
  modal?: NzModalRef;
  pagination = new Pagination();

  @ViewChild('actionTmpl', { static: true }) actionTmpl!: TemplateRef<NzSafeAny>;

  constructor(
    public validateService: ValidateService,
    private http: HttpClient,
    private toastService: ToastrService,
    private modalService: NzModalService,
    private translate: TranslateService,
    private shareDataService: ShareDataService,
    private familyDeductionInfoService: FamilyDeductionInfoService,
    private sessionService: SessionService,
  ) {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.HR_DEPENDENT_PERSON}`);
  }

  ngOnInit(): void {
    this.shareDataService.employee$.subscribe(employee => {
      this.employeeId = employee.employeeId;
      this.initTable();
      this.doSearch(1);
    })
  }

  doSearch(pageNumber: number) {
    this.tableConfig = { ...this.tableConfig, loading: true };
    this.pagination.pageNumber = pageNumber;
    const param = this.pagination.getCurrentPage();
    this.familyDeductionInfoService.getList(this.employeeId, param).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.dataTable = res.data.listData.map((item: FamilyDeductionInfo) => {
          if (StringUtils.isNullOrEmpty(item.codeNo) && StringUtils.isNullOrEmpty(item.bookNo)) {
            item.hasPersonalId = 'X';
          }
          return item;
        });
        this.tableConfig.total = res.data.count;
      } else {
        this.toastService.error(res?.message);
      }
      this.tableConfig = { ...this.tableConfig, loading: false };
    }, () => {
      this.tableConfig = { ...this.tableConfig, loading: false };
      this.dataTable = []
    })
  }

  initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'staffManager.familyDeductionsInformation.label.table.relationTypeName',
          field: 'relationTypeName',
        },
        {
          title: 'staffManager.familyDeductionsInformation.label.table.fullName',
          field: 'fullName',
        },
        {
          title: 'staffManager.familyDeductionsInformation.label.table.taxNumber',
          field: 'taxNumber',
        },
        {
          title: 'staffManager.familyDeductionsInformation.label.table.personalIdNumber',
          field: 'personalId',
        },
        {
          title: 'staffManager.familyDeductionsInformation.label.table.codeNo',
          field: 'codeNo',
        },
        {
          title: 'staffManager.familyDeductionsInformation.label.table.fromDate',
          field: 'fromDate',
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        },
        {
          title: 'staffManager.familyDeductionsInformation.label.table.toDate',
          field: 'toDate',
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        },
        {
          title: 'staffManager.familyDeductionsInformation.label.attributes.hasPersonalId',
          field: 'hasPersonalId',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          show: false
        },
        {
          title: 'staffManager.familyDeductionsInformation.label.attributes.bookNo',
          field: 'bookNo',
          show: false
        },
        {
          title: 'staffManager.familyDeductionsInformation.label.attributes.national',
          field: 'nationName',
          show: false
        },
        {
          title: 'staffManager.label.province',
          field: 'provinceName',
          show: false
        },
        {
          title: 'staffManager.label.district',
          field: 'districtName',
          show: false
        },
        {
          title: 'staffManager.label.ward',
          field: 'wardName',
          show: false
        },
        {
          title: 'staffManager.familyDeductionsInformation.label.attributes.note',
          field: 'note',
          show: false
        },
        {
          title: ' ',
          field: 'action',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          fixed: true,
          fixedDir: "right",
          tdTemplate: this.actionTmpl,
          width: 80,
          show: this.objFunction?.delete || this.objFunction?.edit
        }
      ],
      total: 0,
      needScroll: true,
      loading: true,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  showModalUpdate(dependentPersonId: number, footerTmpl: TemplateRef<Record<string, never>>) {
    const data = this.dataTable.find(item => item.dependentPersonId === dependentPersonId);
    this.modal = this.modalService.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 2 : window.innerWidth,
      nzTitle: this.translate.instant("staffManager.familyDeductionsInformation.modal.update"),
      nzContent: AddFamilyDeductionsInfoComponent,
      nzComponentParams: {
        mode: Mode.EDIT,
        data: data
      },
      nzFooter: footerTmpl
    });
    this.modal.afterClose.subscribe(result => result?.refresh ? this.doSearch(1): '')
  }

  deleteItem(dependentPersonId: number) {
    this.familyDeductionInfoService.deleteRecord(dependentPersonId).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.doSearch(1);
        this.toastService.success(this.translate.instant('common.notification.deleteSuccess'));
      } else {
        this.toastService.error(res?.message);
      }
    })
  }

  refresh() {
    this.doSearch(1);
  }

}
