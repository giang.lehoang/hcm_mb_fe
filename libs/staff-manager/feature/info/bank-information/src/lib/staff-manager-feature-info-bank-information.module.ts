import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BankInformationComponent} from "./bank-information/bank-information.component";
import {SharedUiMbTextLabelModule} from "@hcm-mfe/shared/ui/mb-text-label";
import {TranslateModule} from "@ngx-translate/core";
import {NzFormModule} from "ng-zorro-antd/form";

@NgModule({
  imports: [CommonModule, SharedUiMbTextLabelModule, TranslateModule, NzFormModule],
  declarations: [BankInformationComponent],
  exports: [BankInformationComponent],
})
export class StaffManagerFeatureInfoBankInformationModule {}
