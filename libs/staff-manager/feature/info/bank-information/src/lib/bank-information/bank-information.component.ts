import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {
  BankInfoService,
  ShareDataService,
} from "@hcm-mfe/staff-manager/data-access/services";
import {AppFunction} from "@hcm-mfe/shared/data-access/models";
import { SessionService } from "@hcm-mfe/shared/common/store";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {BankInfo, BankInfoGroup, BaseResponse} from "@hcm-mfe/staff-manager/data-access/models/info";

@Component({
  selector: 'app-bank-information',
  templateUrl: './bank-information.component.html',
  styleUrls: ['./bank-information.component.scss']
})
export class BankInformationComponent implements OnInit, OnDestroy {
  objFunction: AppFunction;
  items?: BankInfoGroup;
  subs: Subscription[] = [];
  response: BaseResponse = new BaseResponse();
  bankInfos: BankInfo[] = [];
  bankInfoMains: BankInfo[] = [];
  employeeId: number = 1;

  constructor(
    private shareDataService: ShareDataService,
    private bankInfoService: BankInfoService,
    private sessionService: SessionService,
  ) {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.HR_ALLOWANCE_PROCESS}`);
  }

  ngOnInit(): void {
    this.subs.push(
      this.shareDataService.employee$.subscribe(employee => {
        this.employeeId = employee.employeeId;
        this.getBankInfo(this.employeeId);
      })
    );
  }

  ngOnDestroy() {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  getBankInfo(employeeId: number) {
    this.subs.push(
      this.bankInfoService.getBankInfo(employeeId).subscribe(res => {
        this.response = res;
        this.bankInfos = this.response?.data;
        this.bankInfoMains = this.bankInfos?.filter(item => item.isPaymentAccount);
        this.items = {employeeId: employeeId, data: this.response?.data};
      })
    );
  }

  refresh() {
    this.getBankInfo(this.employeeId);
  }
}
