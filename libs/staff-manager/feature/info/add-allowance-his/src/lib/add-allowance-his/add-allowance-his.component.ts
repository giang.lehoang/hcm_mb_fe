import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import {HTTP_STATUS_CODE, Mode} from "@hcm-mfe/shared/common/constants";
import {
  AllowanceHisInfoService,
  ShareDataService,
  StaffInfoService
} from "@hcm-mfe/staff-manager/data-access/services";
import {Constant} from "@hcm-mfe/staff-manager/data-access/common";
import {BaseResponse, Category} from "@hcm-mfe/shared/data-access/models";
import {AllowanceHistory} from "@hcm-mfe/staff-manager/data-access/models/info";
import {DateValidator} from "@hcm-mfe/shared/common/validators";

@Component({
  selector: 'app-add-allowance-his',
  templateUrl: './add-allowance-his.component.html',
  styleUrls: ['./add-allowance-his.component.scss'],
})
export class AddAllowanceHisComponent implements OnInit {
  data = new AllowanceHistory();
  mode = Mode.ADD;

  isSubmitted = false;
  form: FormGroup;

  listAllowanceType: Category[] = [];
  isLoadingPage = false;

  constructor(
    private fb: FormBuilder,
    private modalRef: NzModalRef,
    private toastService: ToastrService,
    private modalService: NzModalService,
    private translate: TranslateService,
    private shareDataService: ShareDataService,
    private staffInfoService: StaffInfoService,
    private allowanceHisInfoService: AllowanceHisInfoService
  ) {
    this.form = this.fb.group({
      allowanceProcessId: [null],
      employeeId: [null],
      fromDate: [null,
        [Validators.required]],
      toDate: null,
      allowanceTypeCode: [null, Validators.required],
      amountMoney: [null, Validators.required],
      note: [null, [Validators.maxLength(500)]]
    }, {
      validators: [
        DateValidator.validateRangeDate('fromDate', 'toDate', 'rangeDateError')
      ]
    });
    // Chờ confirm API
    this.getAllowanceTypeList();
  }

  get f() {
    return this.form.controls;
  }

  ngOnInit(): void {
    if (this.mode === Mode.EDIT)
      this.patchValueInfo();

    this.f['employeeId'].setValue(this.shareDataService.getEmployee().employeeId);
  }

  patchValueInfo() {
    this.isLoadingPage = true;
    this.allowanceHisInfoService.getRecord(this.data.allowanceProcessId).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.data = res?.data;
        this.form.patchValue(this.data);
        if (this.data.fromDate) {
          this.f['fromDate'].setValue(moment(this.data.fromDate, 'DD/MM/YYYY').toDate());
        }
        if (this.data.toDate) {
          this.f['toDate'].setValue(moment(this.data.toDate, 'DD/MM/YYYY').toDate());
        }
      } else {
        this.toastService.error(res?.message);
      }
      this.isLoadingPage = false;
    }, () => {
      this.modalRef.close();
      this.isLoadingPage = false;
    })
  }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      this.isLoadingPage = true;
      const request: AllowanceHistory = this.form.value;
      request.fromDate = this.f['fromDate'].value ? moment(this.f['fromDate'].value).format('DD/MM/YYYY') : null;
      request.toDate = this.f['toDate'].value ? moment(this.f['toDate'].value).format('DD/MM/YYYY') : null;

      this.allowanceHisInfoService.saveRecord(request).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.toastService.success(this.translate.instant(this.mode === Mode.EDIT ? 'common.notification.updateSuccess' : 'common.notification.addSuccess'));
          this.modalRef.close({ refresh: true });
        } else {
          this.toastService.error(this.translate.instant(this.mode === Mode.EDIT ? 'common.notification.updateError' : 'common.notification.addError') + ': ' + res?.message);
        }
        this.isLoadingPage = false;
      }, error => {
        this.toastService.error(this.translate.instant(this.mode === Mode.EDIT ? 'common.notification.updateError' : 'common.notification.addError') + ': ' + error.message);
        this.isLoadingPage = false;
      });
    }
  }

  getAllowanceTypeList() {
    this.staffInfoService.getCatalog(Constant.CATALOGS.LOAI_PHU_CAP).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) this.listAllowanceType = res.data;
    });
  }
}
