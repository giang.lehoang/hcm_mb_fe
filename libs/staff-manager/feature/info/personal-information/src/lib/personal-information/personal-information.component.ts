import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {ActivatedRoute} from "@angular/router";
import {AppFunction} from "@hcm-mfe/shared/data-access/models";
import {BaseResponse, PersonalInfo} from "@hcm-mfe/staff-manager/data-access/models/info";
import {ShareDataService} from "@hcm-mfe/staff-manager/data-access/services";
import { SessionService } from "@hcm-mfe/shared/common/store";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {PersonalInfoService} from "@hcm-mfe/personal-tax/data-access/services";

@Component({
  selector: 'app-personal-information',
  templateUrl: './personal-information.component.html',
  styleUrls: ['./personal-information.component.scss']
})
export class PersonalInformationComponent implements OnInit, OnDestroy {
  objFunction: AppFunction;
  items: PersonalInfo | any;
  showEdit = false;
  personalInfo: PersonalInfo | any;
  subs: Subscription[] = [];
  response: BaseResponse = new BaseResponse();
  employeeId: number = 1;

  constructor(
    private router: ActivatedRoute,
    private shareDataService: ShareDataService,
    private personalInfoService: PersonalInfoService,
    private sessionService: SessionService,
    ) {
      this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.HR_ALLOWANCE_PROCESS}`);
    }

  ngOnInit(): void {
    this.router.queryParams.subscribe((employee: any) => {
      this.employeeId = employee.employeeId;
      this.getPersonalInfo();
    })
    this.subs.push(
      this.shareDataService.employee$.subscribe(employee => {
        this.employeeId = employee.employeeId;
        this.getPersonalInfo();
      })
    );
  }

  ngOnDestroy() {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  getPersonalInfo() {
    this.subs.push(
      this.personalInfoService.getPersonalInfo(this.employeeId).subscribe(res => {
        this.response = res;
        this.personalInfo = this.response?.data;
        this.items = this.response?.data;
        this.shareDataService.changePersonalInfo(this.response?.data);
      })
    );
  }

  refresh() {
    this.getPersonalInfo();
  }

}
