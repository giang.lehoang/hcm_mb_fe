import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PartyInformationComponent} from "./party-information/party-information.component";
import {NzFormModule} from "ng-zorro-antd/form";
import {SharedUiMbTextLabelModule} from "@hcm-mfe/shared/ui/mb-text-label";
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  imports: [CommonModule, NzFormModule, SharedUiMbTextLabelModule, TranslateModule],
  declarations: [PartyInformationComponent],
  exports: [PartyInformationComponent],
})
export class StaffManagerFeatureInfoPartyInformationModule {}
