import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {AppFunction} from "@hcm-mfe/shared/data-access/models";
import {BaseResponse, PartyInfo, PartyInfoGroup} from "@hcm-mfe/staff-manager/data-access/models/info";
import {PartyInfoService, ShareDataService} from "@hcm-mfe/staff-manager/data-access/services";
import { SessionService } from "@hcm-mfe/shared/common/store";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";

@Component({
  selector: 'app-party-information',
  templateUrl: './party-information.component.html',
  styleUrls: ['./party-information.component.scss']
})
export class PartyInformationComponent implements OnInit, OnDestroy {
  objFunction: AppFunction;
  items: PartyInfoGroup | any;
  subs: Subscription[] = [];
  response: BaseResponse = new BaseResponse();
  partyInfo: PartyInfo | any;
  employeeId: number = 1;

  constructor(
    private shareDataService: ShareDataService,
    private partyInfoService: PartyInfoService,
    private sessionService: SessionService,
    ) {
      this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.HR_ALLOWANCE_PROCESS}`);
    }

  ngOnInit(): void {
    this.subs.push(
      this.shareDataService.employee$.subscribe(employee => {
        this.employeeId = employee.employeeId;
        this.getPartyInfo(this.employeeId);
      })
    );
  }

  ngOnDestroy() {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  getPartyInfo(employeeId: number) {
    this.subs.push(
      this.partyInfoService.getPartyInfo(employeeId).subscribe(res => {
        this.response = res;
        this.partyInfo = this.response?.data;
        this.items = {employeeId: employeeId, data: this.response?.data};
      })
    );
  }

  refresh() {
    this.getPartyInfo(this.employeeId);
  }

}
