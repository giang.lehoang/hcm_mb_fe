import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {NzModalRef, NzModalService} from 'ng-zorro-antd/modal';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {AppFunction, BaseResponse, MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {Salary} from "@hcm-mfe/staff-manager/data-access/models/info";
import {SalaryProgressService, ShareDataService} from "@hcm-mfe/staff-manager/data-access/services";
import {SessionService} from "@hcm-mfe/shared/common/store";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {ValidateService} from "@hcm-mfe/shared/core";
import {HTTP_STATUS_CODE, Mode} from "@hcm-mfe/shared/common/constants";
import {AddSalaryInfoComponent} from "@hcm-mfe/staff-manager/feature/info/add-salary-info";
import {NzSafeAny} from "ng-zorro-antd/core/types";


@Component({
  selector: 'app-salary-his-information',
  templateUrl: './salary-his-information.component.html',
  styleUrls: ['./salary-his-information.component.scss'],
})
export class SalaryHisInformationComponent implements OnInit {
  objFunction: AppFunction;
  tableConfig!: MBTableConfig;
  dataTable: Salary[] = [];
  employeeId: number | null = null;
  modal?: NzModalRef;
  pagination = new Pagination();

  @ViewChild('actionTmpl', {static: true}) actionTmpl!: TemplateRef<NzSafeAny>;

  constructor(
    public validateService: ValidateService,
    private http: HttpClient,
    private toastService: ToastrService,
    private modalService: NzModalService,
    private translate: TranslateService,
    private shareDataService: ShareDataService,
    private salaryProgressService: SalaryProgressService,
    private sessionService: SessionService,
  ) {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.HR_SALARY_PROCESS}`);
  }

  ngOnInit(): void {
    this.shareDataService.employee$.subscribe(employee => {
      this.employeeId = employee.employeeId;
      this.initTable();
      this.doSearch(1);
    })
  }

  doSearch(pageNumber: number) {
    this.tableConfig = {...this.tableConfig, loading: true};
    this.pagination.pageNumber = pageNumber;
    const param = this.pagination.getCurrentPage();
    this.salaryProgressService.getList(this.employeeId, param).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.dataTable = res.data.listData.map((item: Salary) => {
          item.salaryPercent = item.salaryPercent + ' %';
          item.performancePercent = item.performancePercent + ' %';
          return item;
        });
        this.tableConfig.total = res.data.count;
      } else {
        this.toastService.error(res?.message);
      }
      this.tableConfig = {...this.tableConfig, loading: false};
    }, () => {
      this.tableConfig = {...this.tableConfig, loading: false};
      this.dataTable = []
    })
  }

  initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'staffManager.table.fromDate',
          width: 100,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          field: 'fromDate',
        },
        {
          title: 'staffManager.table.toDate',
          width: 100,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          field: 'toDate',
        },
        {
          title: 'staffManager.table.salaryRange',
          field: 'salaryGradeName',
        },
        {
          title: 'staffManager.table.salaryLevel',
          field: 'salaryRankName',
        },
        {
          title: 'staffManager.table.salaryNumber',
          pipe: 'currency: VND',
          field: 'salaryAmount',
        },
        {
          title: 'staffManager.table.percentReceive',
          field: 'salaryPercent',
          tdClassList: ['text-right']
        },
        {
          title: 'staffManager.table.performanceFactor',
          field: 'performanceFactor',
          tdClassList: ['text-right']
        },
        {
          title: 'staffManager.table.ratio',
          field: 'performancePercent',
          tdClassList: ['text-right']
        },
        {
          title: 'staffManager.label.note',
          field: 'note',
          show: false
        },
        {
          title: ' ',
          field: 'action',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          fixed: true,
          fixedDir: "right",
          tdTemplate: this.actionTmpl,
          width: 80,
          show: this.objFunction?.delete || this.objFunction?.edit
        }
      ],
      total: 0,
      needScroll: true,
      loading: true,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  showModalUpdate(salaryProcessId: number, footerTmpl: TemplateRef<Record<string, never>>) {
    const data = this.dataTable.find(item => item.salaryProcessId === salaryProcessId);
    this.modal = this.modalService.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
      nzTitle: this.translate.instant("staffManager.salaryHisInformation.modal.update"),
      nzContent: AddSalaryInfoComponent,
      nzComponentParams: {
        mode: Mode.EDIT,
        data: data
      },
      nzFooter: footerTmpl
    });
    this.modal.afterClose.subscribe(result => result?.refresh ? this.doSearch(1) : '')
  }

  deleteItem(salaryProcessId: number) {
    this.salaryProgressService.deleteRecord(salaryProcessId).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.doSearch(1);
        this.toastService.success(this.translate.instant('common.notification.deleteSuccess'));
      } else {
        this.toastService.error(res?.message);
      }
    })
  }

  refresh() {
    this.doSearch(1);
  }

}
