import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';
import {HTTP_STATUS_CODE, Mode} from "@hcm-mfe/shared/common/constants";
import {
  DiscipinaryHisInfoService,
  ShareDataService, StaffInfoService,
} from "@hcm-mfe/staff-manager/data-access/services";
import {Constant} from "@hcm-mfe/staff-manager/data-access/common";
import {BaseResponse, Category} from "@hcm-mfe/shared/data-access/models";
import {DisciplinaryHistory} from "@hcm-mfe/staff-manager/data-access/models/info";

@Component({
  selector: 'app-add-disciplinary-his',
  templateUrl: './add-disciplinary-his.component.html',
  styleUrls: ['./add-disciplinary-his.component.scss'],
})
export class AddDisciplinaryHisComponent implements OnInit {
  data = new DisciplinaryHistory();
  mode = Mode.ADD;

  isSubmitted = false;
  form: FormGroup;

  listDisciplineLevel: Category[] = [];
  listDisciplineMethod: Category[] = [];
  isLoadingPage = false;


  constructor(
    private fb: FormBuilder,
    private modalRef: NzModalRef,
    private toastrService: ToastrService,
    private modalService: NzModalService,
    private translate: TranslateService,
    private shareDataService: ShareDataService,
    private staffInfoService: StaffInfoService,
    private discipinaryHisInfoService: DiscipinaryHisInfoService
  ) {
    this.form = this.fb.group({
      disciplineRecordId: [null],
      employeeId: [null],
      signedDate: [null, Validators.required],
      fromDate: [null],
      toDate: [null],
      disciplineLevelCode: [null, Validators.required],
      disciplineMethodCode: [null, Validators.required],
      reason: [null, Validators.required],
      amount: [null, [Validators.maxLength(10)]],
      amountRemedied: [null, [Validators.maxLength(10)]],
      signer: null,
      signerPosition: null,
      note: [null, [Validators.maxLength(500)]],
      caseName: [null, [Validators.maxLength(200)]]
    });

    // TODO: Chờ xác nhận API
    this.getDisciplineLevelList();
    this.getDisciplineMethodList();

  }

  get f() {
    return this.form.controls;
  }

  ngOnInit(): void {
    if (this.mode === Mode.EDIT)
      this.patchValueInfo();

    this.f['employeeId'].setValue(this.shareDataService.getEmployee().employeeId);
  }

  patchValueInfo() {
    this.isLoadingPage = true;
    this.discipinaryHisInfoService.getRecord(this.data.disciplineRecordId).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.data = res?.data;
        this.form.patchValue(this.data);
        if (this.data.signedDate)
          this.f['signedDate'].setValue(moment(this.data.signedDate, 'DD/MM/YYYY').toDate());
        if (this.data.fromDate)
          this.f['fromDate'].setValue(moment(this.data.fromDate, 'DD/MM/YYYY').toDate());
        if (this.data.toDate)
          this.f['toDate'].setValue(moment(this.data.toDate, 'DD/MM/YYYY').toDate());
      } else {
        this.toastrService.error(res?.message);
      }
      this.isLoadingPage = false;
    }, () => {
      this.modalRef.close();
      this.isLoadingPage = false;
    })
  }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      this.isLoadingPage = true;
      const request: DisciplinaryHistory = this.form.value;
      request.signedDate = this.f['signedDate'].value ? moment(this.f['signedDate'].value).format('DD/MM/YYYY') : null;
      request.fromDate = this.f['fromDate'].value ? moment(this.f['fromDate'].value).format('DD/MM/YYYY') : null;
      request.toDate = this.f['toDate'].value ? moment(this.f['toDate'].value).format('DD/MM/YYYY') : null;

      this.discipinaryHisInfoService.saveRecord(request).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.toastrService.success(this.translate.instant('common.notification.updateSuccess'));
          this.modalRef.close({ refresh: true });
        } else {
          this.toastrService.error(this.translate.instant('common.notification.updateError') + ':' + res.message);
        }
        this.isLoadingPage = false;
      });
    }
  }

  getDisciplineLevelList() {
    // TODO: Thiếu trong api danh mục
    this.staffInfoService.getCatalog(Constant.CATALOGS.CAP_QD_KYLUAT).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) this.listDisciplineLevel = res.data;
    });
  }

  getDisciplineMethodList() {
    this.staffInfoService.getCatalog(Constant.CATALOGS.HINHTHUC_KYLUAT).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) this.listDisciplineMethod = res.data;
    });
  }
}
