import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {NzModalRef, NzModalService} from 'ng-zorro-antd/modal';
import {HttpClient} from '@angular/common/http';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {AppFunction, BaseResponse, MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {ConcurrentProcessInfo} from "@hcm-mfe/staff-manager/data-access/models/info";
import {ValidateService} from "@hcm-mfe/shared/core";
import {
  ConcurrentProcessInfoService,
  DownloadFileAttachService,
  ShareDataService
} from "@hcm-mfe/staff-manager/data-access/services";
import {SessionService} from "@hcm-mfe/shared/common/store";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {HTTP_STATUS_CODE, Mode} from "@hcm-mfe/shared/common/constants";
import {EditConcurrentProcessInfoComponent} from "@hcm-mfe/staff-manager/feature/info/edit-concurrent-process-info";
import {Constant} from '@hcm-mfe/staff-manager/data-access/common';
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {saveAs} from "file-saver";
import {getTypeExport, StringUtils} from "@hcm-mfe/shared/common/utils";

@Component({
  selector: 'app-concurrent-process-information',
  templateUrl: './concurrent-process-information.component.html',
  styleUrls: ['./concurrent-process-information.component.scss']
})
export class ConcurrentProcessInformationComponent implements OnInit {

  objFunction: AppFunction;
  tableConfig!: MBTableConfig;
  dataTable: ConcurrentProcessInfo[] = [];
  employeeId: number | undefined;
  modal: NzModalRef | undefined;
  constant = Constant;
  pagination = new Pagination();

  @ViewChild('actionTmpl', { static: true }) actionTmpl!: TemplateRef<NzSafeAny>;
  @ViewChild('statusTmpl', { static: true }) statusTmpl!: TemplateRef<NzSafeAny>;
  @ViewChild('fileTmpl', { static: true }) fileTmpl!: TemplateRef<NzSafeAny>;

  constructor(
    public validateService: ValidateService,
    private http: HttpClient,
    private toastService: ToastrService,
    private modalService: NzModalService,
    private translate: TranslateService,
    private shareDataService: ShareDataService,
    private concurrentProcessService: ConcurrentProcessInfoService,
    private downloadFileAttachService: DownloadFileAttachService,
    private sessionService: SessionService
  ) {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.HR_PROJECT_MEMBER}`);
  }

  ngOnInit(): void {
    this.shareDataService.employee$.subscribe(employee => {
      this.employeeId = employee.employeeId;
      this.initTable();
      this.doSearch(1);
    });
  }

  initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'staffManager.staffResearch.concurrentProcess.table.fromDate',
          field: 'fromDate',
          width: 100,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
        },
        {
          title: 'staffManager.staffResearch.concurrentProcess.table.toDate',
          field: 'toDate',
          width: 100,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
        },
        {
          title: 'staffManager.staffResearch.concurrentProcess.table.position',
          field: 'posName',
          width: 150,
        },
        {
          title: 'staffManager.workHisInformation.label.attributes.positionGroup',
          field: 'pgrName',
          width: 150,
        },
        {
          title: 'staffManager.staffResearch.concurrentProcess.table.org',
          field: 'orgName',
          width: 150,
        },
        {
          title: 'staffManager.staffResearch.concurrentProcess.label.documentNo',
          field: 'documentNo',
          width: 150,
          show: false
        },
        {
          title: 'staffManager.staffResearch.concurrentProcess.label.signedDate',
          field: 'signedDate',
          width: 100,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          show: false
        },
        {
          title: 'staffManager.staffResearch.concurrentProcess.label.empTypeCode',
          field: 'empTypeName',
          width: 150,
          show: false
        },
        {
          title: 'staffManager.workHisInformation.label.attributes.positionLevel',
          field: 'positionLevelStr',
          width: 150,
          show: false
        },
        {
          title: 'staffManager.staffResearch.concurrentProcess.table.note',
          field: 'note',
          width: 150,
          show: false
        },
        {
          title: 'staffManager.label.file',
          width: 130,
          tdTemplate: this.fileTmpl,
          show: false
        },
        {
          title: 'staffManager.staffResearch.concurrentProcess.table.status',
          fixed: true,
          fixedDir: "right",
          width: 130,
          tdTemplate: this.statusTmpl
        },
        {
          title: ' ',
          field: 'action',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          fixed: true,
          fixedDir: "right",
          tdTemplate: this.actionTmpl,
          width: 80,
          show: this.objFunction?.delete || this.objFunction?.edit
        }
      ],
      total: 0,
      needScroll: true,
      loading: true,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  doSearch(pageNumber: number) {
    this.tableConfig = { ...this.tableConfig, loading: true };
    this.pagination.pageNumber = pageNumber;
    const param = this.pagination.getCurrentPage();
    if (this.employeeId) {
      this.concurrentProcessService.getList(this.employeeId, param).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.dataTable = res.data.listData.map((item: ConcurrentProcessInfo) => {
            if (item.positionLevel) {
              if (!StringUtils.isNullOrEmpty(item.positionLevel)) {
                item.positionLevelStr = 'Bậc ' + parseInt(item.positionLevel, 10);
              }
            }
            return item;
          });
          this.tableConfig.total = res.data.count;
        } else {
          this.toastService.error(res?.message);
        }
        this.tableConfig = { ...this.tableConfig, loading: false };
      }, () => {
        this.tableConfig = { ...this.tableConfig, loading: false };
        this.dataTable = [];
      });
    }
  }

  showModalUpdate(concurrentProcessId: number, draftConcurrentProcessId: number, footerTmpl: TemplateRef<Record<string, never>>) {
    const data = this.getRecord(concurrentProcessId, draftConcurrentProcessId);
    this.modal = this.modalService.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
      nzTitle: this.translate.instant('staffManager.concurrentProcessInformation.modal.update'),
      nzContent: EditConcurrentProcessInfoComponent,
      nzComponentParams: {
        mode: Mode.EDIT,
        data: data
      },
      nzFooter: footerTmpl
    });
    this.modal.afterClose.subscribe(result => result?.refresh ? this.doSearch(1) : '');
  }

  getRecord(concurrentProcessId: number, draftConcurrentProcessId: number): ConcurrentProcessInfo | undefined {
    if (concurrentProcessId) {
      return this.dataTable.find(item => item.concurrentProcessId === concurrentProcessId);
    }
    return this.dataTable.find(item => item.draftConcurrentProcessId === draftConcurrentProcessId);
  }

  deleteItem(concurrentProcessId: number, draftConcurrentProcessId: number) {
    if (concurrentProcessId && concurrentProcessId > 0) { // xoa trong bang chinh
      this.concurrentProcessService.deleteRecord(concurrentProcessId).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.doSearch(1);
          this.toastService.success(this.translate.instant('common.notification.deleteSuccess'));
        } else {
          this.toastService.error(res?.message);
        }
      })
    } else { // xoa trong draft
      if (draftConcurrentProcessId) {
        this.concurrentProcessService.deleteRecordInDraft(draftConcurrentProcessId).subscribe((res: BaseResponse) => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.doSearch(1);
            this.toastService.success(this.translate.instant('common.notification.deleteSuccess'));
          } else {
            this.toastService.error(res?.message);
          }
        })
      }
    }
  }

  doDownloadAttach(file: NzSafeAny) {
    this.downloadFileAttachService.doDownloadAttachFileWithSecurity(file?.docId, file?.security).subscribe(res => {
      const reportFile = new Blob([res.body], {type: getTypeExport(file.fileName.split(".").pop())});
      saveAs(reportFile, file.fileName);
    });
  }

  getStatus(param: NzSafeAny) {
    return Constant.APPROVAL_STATUSES.find(item => item.value === param)?.label;
  }

  refresh() {
    this.doSearch(1);
  }

}
