import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {NzModalRef, NzModalService} from 'ng-zorro-antd/modal';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from "ngx-toastr";
import {AppFunction, BaseResponse, MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {RelativeInfo} from "@hcm-mfe/staff-manager/data-access/models/info";
import {RelativesInfoService, ShareDataService} from "@hcm-mfe/staff-manager/data-access/services";
import {SessionService} from "@hcm-mfe/shared/common/store";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {ValidateService} from "@hcm-mfe/shared/core";
import {HTTP_STATUS_CODE, Mode} from "@hcm-mfe/shared/common/constants";
import {EditRelativesInfoComponent} from "@hcm-mfe/staff-manager/feature/info/edit-relatives-info";
import {NzSafeAny} from "ng-zorro-antd/core/types";


/**
 * Thông tin thân nhân
 */
@Component({
  selector: 'app-relatives-information',
  templateUrl: './relatives-information.component.html',
  styleUrls: ['./relatives-information.component.scss'],
})
export class RelativesInformationComponent implements OnInit {
  objFunction: AppFunction;
  tableConfig!: MBTableConfig;
  dataTable: RelativeInfo[] = [];
  employeeId: number | null = null;
  modal?: NzModalRef;
  pagination = new Pagination();

  @ViewChild('actionTmpl', { static: true }) actionTmpl!: TemplateRef<NzSafeAny>;

  constructor(
    public validateService: ValidateService,
    private toastService: ToastrService,
    private modalService: NzModalService,
    private translate: TranslateService,
    private shareDataService: ShareDataService,
    private relativesInfoService: RelativesInfoService,
    private sessionService: SessionService,
  ) {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.HR_FAMILY_RELATIONSHIP}`);
  }

  ngOnInit(): void {
    this.shareDataService.employee$.subscribe(employee => {
      this.employeeId = employee.employeeId;
      this.initTable();
      this.doSearch(1);
    })
  }

  doSearch(pageNumber: number) {
    this.tableConfig = { ...this.tableConfig, loading: true };
    this.pagination.pageNumber = pageNumber;
    const param = this.pagination.getCurrentPage();
    this.relativesInfoService.getList(this.employeeId, param).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.dataTable = res.data.listData.map((item: RelativeInfo) => {
          if(item.isInCompany === 1) {
            item.isInCompanyStr = 'X';
          }
          if (item.isHouseholdOwner === 1) {
            item.isHouseholdOwnerStr = 'X';
          }
          return item;
        });
        this.tableConfig.total = res.data.count;
      } else {
        this.toastService.error(res?.message);
      }
      this.tableConfig = { ...this.tableConfig, loading: false };
    }, () => {
      this.tableConfig = { ...this.tableConfig, loading: false };
      this.dataTable = []
    })
  }

  initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'staffManager.relativesInformation.label.table.relationTypeName',
          field: 'relationTypeName',
        },
        {
          title: 'staffManager.relativesInformation.label.table.fullName',
          field: 'fullName',
        },
        {
          title: 'staffManager.relativesInformation.label.table.relationStatus',
          field: 'relationStatusName',
        },
        {
          title: 'staffManager.relativesInformation.label.table.dateOfBirth',
          field: 'dateOfBirth',
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        },
        {
          title: 'staffManager.relativesInformation.label.table.workOrganization',
          field: 'workOrganization',
        },
        {
          title: 'staffManager.relativesInformation.label.table.job',
          field: 'job',
        },
        {
          title: 'staffManager.relativesInformation.label.table.currentAddress',
          field: 'currentAddress',
        },
        {
          title: 'staffManager.relativesInformation.label.table.phoneNumber',
          field: 'phoneNumber',
        },
        {
          title: 'staffManager.relativesInformation.label.attributes.isInCompany',
          field: 'isInCompanyStr',
          width: 80,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          show: false
        },
        {
          title: 'staffManager.relativesInformation.label.attributes.policyType',
          field: 'policyTypeName',
          show: false
        },
        {
          title: 'staffManager.relativesInformation.label.attributes.personalIdNumber',
          field: 'personalIdNumber',
          show: false
        },
        {
          title: 'staffManager.relativesInformation.label.attributes.isHeadHouse',
          field: 'isHouseholdOwnerStr',
          width: 80,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          show: false
        },
        {
          title: 'staffManager.relativesInformation.label.attributes.note',
          field: 'note',
          show: false
        },
        {
          title: ' ',
          field: 'action',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          fixed: true,
          fixedDir: "right",
          tdTemplate: this.actionTmpl,
          width: 80,
          show: this.objFunction?.delete || this.objFunction?.edit
        }
      ],
      total: 0,
      needScroll: true,
      loading: true,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  showModalUpdate(familyRelationshipId: number, footerTmpl: TemplateRef<Record<string, never>>) {
    const data = this.dataTable.find(item => item.familyRelationshipId === familyRelationshipId);
    this.modal = this.modalService.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
      nzTitle: this.translate.instant("staffManager.relativesInformation.modal.update"),
      nzContent: EditRelativesInfoComponent,
      nzComponentParams: {
        mode: Mode.EDIT,
        data: data
      },
      nzFooter: footerTmpl
    });
    this.modal.afterClose.subscribe(result => result?.refresh ? this.doSearch(1): '')
  }

  deleteItem(familyRelationshipId: number) {
    this.relativesInfoService.deleteRecord(familyRelationshipId).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.toastService.success(this.translate.instant('common.notification.deleteSuccess'));
        this.doSearch(1);
      } else {
        this.toastService.error(res?.message);
      }
    }, () => {
      this.toastService.error(this.translate.instant('common.notification.deleteError'));
    })
  }

  refresh() {
    this.doSearch(1);
  }

}
