import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RelativesInformationComponent} from "./relatives-information/relatives-information.component";
import {NzTableModule} from "ng-zorro-antd/table";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzPopconfirmModule} from "ng-zorro-antd/popconfirm";
import {SharedUiMbTableMergeCellModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell";
import {SharedUiMbTableMergeCellWrapModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell-wrap";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";

@NgModule({
  imports: [CommonModule, NzTableModule, TranslateModule, SharedUiMbButtonModule, NzPopconfirmModule, SharedUiMbTableMergeCellModule, SharedUiMbTableMergeCellWrapModule, SharedUiMbTableWrapModule, SharedUiMbTableModule],
  declarations: [RelativesInformationComponent],
  exports: [RelativesInformationComponent],
})
export class StaffManagerFeatureInfoRelativesInformationModule {}
