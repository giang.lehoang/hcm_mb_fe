import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EditIdentityInfoComponent} from "./edit-identity-info/edit-identity-info.component";
import {ReactiveFormsModule} from "@angular/forms";
import {NzTableModule} from "ng-zorro-antd/table";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedDirectivesNumberInputModule} from "@hcm-mfe/shared/directives/number-input";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {NzRadioModule} from "ng-zorro-antd/radio";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzPopconfirmModule} from "ng-zorro-antd/popconfirm";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
  imports: [CommonModule, ReactiveFormsModule, NzTableModule, TranslateModule, SharedUiMbSelectModule, SharedUiMbInputTextModule, SharedDirectivesNumberInputModule, SharedUiMbDatePickerModule, NzRadioModule, SharedUiMbButtonModule, NzPopconfirmModule, SharedUiLoadingModule],
  declarations: [EditIdentityInfoComponent],
  exports: [EditIdentityInfoComponent],
})
export class StaffManagerFeatureInfoEditIdentityInfoModule {}
