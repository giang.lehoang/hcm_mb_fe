import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Subscription} from "rxjs";
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";
import {NzModalRef} from "ng-zorro-antd/modal";
import * as moment from 'moment';
import {IdentityInfo, LookupValues} from "@hcm-mfe/staff-manager/data-access/models/info";
import {IdentityInfoService, ShareDataService, StaffInfoService} from "@hcm-mfe/staff-manager/data-access/services";
import {BaseResponse, User, UserLogin} from "@hcm-mfe/shared/data-access/models";
import {Constant} from "@hcm-mfe/staff-manager/data-access/common";
import {DateValidator} from "@hcm-mfe/shared/common/validators";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {HTTP_STATUS_CODE, STORAGE_NAME} from "@hcm-mfe/shared/common/constants";
import {StorageService} from "@hcm-mfe/shared/common/store";
import {SessionKey} from "@hcm-mfe/shared/common/enums";

@Component({
  selector: 'app-edit-identity-info',
  templateUrl: './edit-identity-info.component.html',
  styleUrls: ['./edit-identity-info.component.scss']
})
export class EditIdentityInfoComponent implements OnInit, OnDestroy {
  @Input() data: NzSafeAny;
  dataIdentities: IdentityInfo[] = [];
  form: FormGroup;
  identities = 'data';
  isSubmitted = false;
  personalIdentityId = 'personalIdentityId';
  identityType = 'idTypeCode';
  identityNumber = 'idNo';
  dateOfIdentity = 'idIssueDate';
  placeOfIdentity = 'idIssuePlace';
  documentMain = 'isMain';
  radioValue?: number;
  identityExpireDate = 'toDate';
  fromDate = 'fromDate';
  listIdPlace = 'listIdPlace';
  listIdentityType: LookupValues[] = [];
  subs: Subscription[] = [];
  listIdDelete: number[] = [];
  constant = Constant;
  isLoadingPage = false;

  constructor(
    private fb: FormBuilder,
    private shareDataService: ShareDataService,
    private identityInfoService: IdentityInfoService,
    private staffInfoService: StaffInfoService,
    private toastService: ToastrService,
    private translate: TranslateService,
    private modalRef: NzModalRef
  ) {
    const formControl = new FormGroup({});
    formControl.addControl(this.identities, this.fb.array([this.createFormGroup(null, null)]));
    this.form = this.fb.group(formControl.controls);
  }

  ngOnInit(): void {
    this.dataIdentities = this.data?.data;
    this.getIdentityType();
    this.patchValue();
  }

  ngOnDestroy() {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  createFormGroup(item: IdentityInfo | NzSafeAny, index: number | NzSafeAny): FormGroup {
    const formControl = new FormGroup({});
    formControl.addControl(this.identityType, new FormControl(item ? item[this.identityType] : null, Validators.required));
    formControl.addControl(this.identityNumber, new FormControl(item ? item[this.identityNumber] : null, [Validators.required, Validators.minLength(6), Validators.maxLength(12)]));
    formControl.addControl(this.dateOfIdentity, new FormControl(item ? item[this.dateOfIdentity] ? moment(item[this.dateOfIdentity], 'DD/MM/YYYY').toDate() : null : null));
    formControl.addControl(this.placeOfIdentity, new FormControl(item ? item[this.placeOfIdentity] : null, Validators.maxLength(199)));
    formControl.addControl(this.documentMain, new FormControl(item ? item[this.documentMain] : null));
    formControl.addControl(this.identityExpireDate, new FormControl(item ? item[this.identityExpireDate] ? moment(item[this.identityExpireDate], 'DD/MM/YYYY').toDate() : null : null));
    formControl.addControl(this.personalIdentityId, new FormControl(item ? item[this.personalIdentityId] : null));
    formControl.addControl(this.fromDate, new FormControl(item ? item[this.fromDate] : null));
    formControl.addControl('index', new FormControl(index ? index : null));
    if (!item) {
      formControl.addControl(this.listIdPlace, new FormControl([]));
    }
    return new FormGroup(formControl.controls, DateValidator.validateRangeDate(this.dateOfIdentity, this.identityExpireDate, 'rangeDateError'));
  }

  configValue() {
    (this.form.value[this.identities] as Array<IdentityInfo>)?.forEach((item: NzSafeAny, j) => {
      if (j === item.index) {
        item[this.documentMain] = 1;
      } else {
        item[this.documentMain] = 0;
      }
    });
  }

  addFormArray() {
    this.isSubmitted = true;
    let isValid = true;
    const idx = this.form.value[this.identities][0]?.index;
    this.radioValue = idx;
    for (let i = 0; i < (this.form.get(this.identities) as FormArray).controls.length; i++) {
      if (!(this.form.get(this.identities) as FormArray).controls[i].valid) {
        isValid = false;
        break;
      }
    }
    if (isValid) {
      this.isSubmitted = false;
      const form = this.form.get(this.identities) as FormArray;
      form.push(this.createFormGroup(null, idx));
    }
  }

  save() {
    this.isSubmitted = true;
    const value = this.form.value;
    this.bindingErrorDuplicate(value);
    if (this.form.valid) {
      this.isLoadingPage = true;
      const body: { [key: string]: object } = {};
      value?.data.forEach((item: NzSafeAny) => {
        item.idIssueDate = item.idIssueDate ? moment(item.idIssueDate).format('DD/MM/YYYY') : '';
        item.toDate = item.toDate ? moment(item.toDate).format('DD/MM/YYYY') : '';
      });
      body['idDelete'] = this.listIdDelete;
      body['data'] = value?.data;
      this.identityInfoService.saveIdentitiesInfo(this.data?.employeeId, body).subscribe(res => {
        if (res && res.code === 200) {
          this.toastService.success(this.translate.instant('common.notification.updateSuccess'));
          this.modalRef.close({refresh: true});
        } else {
          this.toastService.error(this.translate.instant('common.notification.updateError') + ': ' + res?.message);
        }
        this.isLoadingPage = false;
      }, error =>  {
        this.toastService.error(this.translate.instant('common.notification.updateError') + ': ' + error?.message);
        this.modalRef.close({refresh: false});
        this.isLoadingPage = false;
      });
    }
  }

  bindingErrorDuplicate(value: NzSafeAny) {
    const arrayDataValid: NzSafeAny[] = [];
    value?.data.forEach((item: NzSafeAny, i: number) => {
      const indexOf = arrayDataValid.indexOf(item[this.identityNumber]);
      if (indexOf === -1) {
        arrayDataValid.push(item[this.identityNumber]);
      } else {
        ((this.form.get(this.identities) as FormArray).controls[indexOf] as FormGroup).controls[this.identityNumber]?.setErrors({ 'accDuplicate': true });
        ((this.form.get(this.identities) as FormArray).controls[i] as FormGroup).controls[this.identityNumber]?.setErrors({ 'accDuplicate': true });
      }
    });
  }

  patchValue() {
    if (this.dataIdentities && this.dataIdentities.length > 0) {
      if (this.dataIdentities[0].employeeId) {
        this.isLoadingPage = true;
        this.identityInfoService.getIdentityInfo(this.dataIdentities[0].employeeId).subscribe((res: BaseResponse) => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.dataIdentities = res?.data;
            const form = this.form.get(this.identities) as FormArray;
            if (this.dataIdentities.length > 0) {
              form.clear();
              let idx: number;
              this.dataIdentities.forEach((item: NzSafeAny, index) => {
                if (item[this.documentMain]) {
                  idx = index;
                  this.radioValue = idx;
                }
                form.push(this.createFormGroup(item, idx));
              });
            }
            this.form.valueChanges.subscribe(() => {
              this.configValue();
            });
          } else {
            this.toastService.error(res?.message);
          }
          this.isLoadingPage = false;
        }, () => {
          this.modalRef.close();
          this.isLoadingPage = false;
        })
      }
    }
  }

  deleteItem(data: IdentityInfo, index: number) {
    const identities = this.form.get(this.identities) as FormArray;
    const idx = this.form.value[this.identities][0]?.index;
    this.radioValue = idx;
    (this.form.value[this.identities] as Array<IdentityInfo>).forEach((item: NzSafeAny) => {
      if (index < item.index) {
        item.index = idx - 1;
      }
    });
    if (identities.length > 1) {
      identities.removeAt(index);
      if (data?.personalIdentityId) {
        this.listIdDelete.push(data.personalIdentityId);
      }
    } else {
      identities.reset();
    }
  }

  getIdentityType() {
    this.subs.push(
      this.staffInfoService.getCatalog(Constant.CATALOGS.LOAI_GIAY_TO).subscribe(res => {
        const response: BaseResponse = res;
        this.listIdentityType = response.data;
      })
    );
  }

  selectIdentityType(event: NzSafeAny, index: number) {
    const identityType = event?.itemSelected?.value;
    let typeCode = null;
    if (identityType === Constant.PAPERS_CODE.ID_NO) {
      typeCode = Constant.CATALOGS.NOI_CAP_CMND;
    } else if (identityType === Constant.PAPERS_CODE.CITIZEN_ID) {
      typeCode = Constant.CATALOGS.NOI_CAP_CCCD;
    }
    if (typeCode) {
      this.subs.push(
        this.staffInfoService.getCatalog(typeCode).subscribe(res => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            const listData = res.data.map((item: NzSafeAny) => {
              item.value = item.label;
              return item;
            })
            const identities = this.form.get(this.identities)['controls'];
            if (!identities[index].controls[this.listIdPlace]) {
              identities[index].addControl(this.listIdPlace, new FormControl([]));
            }
            identities[index].controls[this.listIdPlace].setValue(listData);
          } else {
            this.toastService.error(res?.message);
          }
        }, error => this.toastService.error(error.message))
      );
    }

    if (this.isSubmitted) {
      this.bindingErrorDuplicate(this.form.value);
    }
  }


  checkDuplicate() {
    if (this.isSubmitted) {
      this.bindingErrorDuplicate(this.form.value);
    }
  }
}
