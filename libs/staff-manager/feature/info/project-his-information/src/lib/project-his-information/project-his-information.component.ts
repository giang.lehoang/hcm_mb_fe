import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ToastrService} from 'ngx-toastr';
import {NzModalRef, NzModalService} from 'ng-zorro-antd/modal';
import {TranslateService} from '@ngx-translate/core';
import {saveAs} from 'file-saver';
import {AppFunction, BaseResponse, MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {ProjectHistory} from "@hcm-mfe/staff-manager/data-access/models/info";
import {
  DownloadFileAttachService,
  ProjectHisInfoService,
  ShareDataService
} from "@hcm-mfe/staff-manager/data-access/services";
import {SessionService} from "@hcm-mfe/shared/common/store";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {ValidateService} from "@hcm-mfe/shared/core";
import {HTTP_STATUS_CODE, Mode} from "@hcm-mfe/shared/common/constants";
import {getTypeExport} from "@hcm-mfe/shared/common/utils";
import {AddProjectHisInfoComponent} from "@hcm-mfe/staff-manager/feature/info/add-project-his-info";
import {NzSafeAny} from "ng-zorro-antd/core/types";

@Component({
  selector: 'app-project-his-information',
  templateUrl: './project-his-information.component.html',
  styleUrls: ['./project-his-information.component.scss'],
})
export class ProjectHisInformationComponent implements OnInit {
  objFunction: AppFunction;
  tableConfig!: MBTableConfig;
  dataTable: ProjectHistory[] = [];
  employeeId: number | null = null;
  modal?: NzModalRef;
  pagination = new Pagination();

  @ViewChild('actionTmpl', { static: true }) actionTmpl!: TemplateRef<NzSafeAny>;
  @ViewChild('fileTmpl', { static: true }) fileTmpl!: TemplateRef<NzSafeAny>;

  constructor(
    public validateService: ValidateService,
    private http: HttpClient,
    private toastService: ToastrService,
    private modalService: NzModalService,
    private translate: TranslateService,
    private shareDataService: ShareDataService,
    private projectHisInfoService: ProjectHisInfoService,
    private downloadFileAttachService: DownloadFileAttachService,
    private sessionService: SessionService,
    ) {
      this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.HR_PROJECT_MEMBER}`);
    }

  ngOnInit(): void {
    this.shareDataService.employee$.subscribe(employee => {
      this.employeeId = employee.employeeId;
      this.initTable();
      this.doSearch(1);
    })
  }

  doSearch(pageNumber: number) {
    this.tableConfig = { ...this.tableConfig, loading: true };
    this.pagination.pageNumber = pageNumber;
    const param = this.pagination.getCurrentPage();
    this.projectHisInfoService.getList(this.employeeId, param).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.dataTable = res.data.listData;
        this.tableConfig.total = res.data.count;
      } else {
        this.toastService.error(res?.message);
      }
      this.tableConfig = { ...this.tableConfig, loading: false };
    }, () => {
      this.tableConfig = { ...this.tableConfig, loading: false };
      this.dataTable = []
    })
  }

  initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'staffManager.table.fromDate',
          field: 'fromDate',
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        },
        {
          title: 'staffManager.table.toDate',
          field: 'toDate',
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        },
        {
          title: 'staffManager.table.partiProject',
          field: 'projectName',
        },
        {
          title: 'staffManager.table.jobProject',
          field: 'jobName',
        },
        {
          title: 'staffManager.table.managerName',
          field: 'managerName'
        },
        {
          title: 'staffManager.label.note',
          field: 'note'
        },
        {
          title: 'staffManager.table.fileAttach',
          tdTemplate: this.fileTmpl,
        },
        {
          title: ' ',
          field: 'action',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          fixed: true,
          fixedDir: "right",
          tdTemplate: this.actionTmpl,
          width: 80,
          show: this.objFunction?.delete || this.objFunction?.edit
        }
      ],
      total: 0,
      needScroll: false,
      loading: true,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  showModalUpdate(projectMemberId: number, footerTmpl: TemplateRef<Record<string, never>>) {
    const data = this.dataTable.find(item => item.projectMemberId === projectMemberId);
    this.modal = this.modalService.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
      nzTitle: this.translate.instant('staffManager.projectHisInfo.modal.update'),
      nzContent: AddProjectHisInfoComponent,
      nzComponentParams: {
        mode: Mode.EDIT,
        data: data
      },
      nzFooter: footerTmpl
    });
    this.modal.afterClose.subscribe(result => result?.refresh ? this.doSearch(1): '')
  }

  deleteItem(projectMemberId: number) {
    this.projectHisInfoService.deleteRecord(projectMemberId).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.doSearch(1);
          this.toastService.success(this.translate.instant('common.notification.deleteSuccess'));
        } else {
          this.toastService.error(res?.message);
        }
      }, () => {
        this.toastService.error(this.translate.instant('common.notification.deleteError'));
      })
  }

  doDownloadAttach(file: NzSafeAny) {
    this.downloadFileAttachService.doDownloadAttachFileWithSecurity(file?.docId, file?.security).subscribe(res => {
      const reportFile = new Blob([res.body], {type: getTypeExport(file.fileName.split(".").pop())});
      saveAs(reportFile, file.fileName);
    });
  }

  refresh() {
    this.doSearch(1);
  }

}
