import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import {HTTP_STATUS_CODE, Mode} from "@hcm-mfe/shared/common/constants";
import {
  FamilyDeductionInfoService,
  ShareDataService, StaffInfoService,
} from "@hcm-mfe/staff-manager/data-access/services";
import {Constant} from "@hcm-mfe/staff-manager/data-access/common";
import {BaseResponse, Category} from "@hcm-mfe/shared/data-access/models";
import {DateValidator} from "@hcm-mfe/shared/common/validators";
import {FamilyDeductionInfo, LookupValues} from "@hcm-mfe/staff-manager/data-access/models/info";
import {StringUtils, Utils} from "@hcm-mfe/shared/common/utils";

@Component({
  selector: 'app-add-family-deductions-info',
  templateUrl: './add-family-deductions-info.component.html',
  styleUrls: ['./add-family-deductions-info.component.scss'],
})
export class AddFamilyDeductionsInfoComponent implements OnInit, AfterViewInit {
  data = new FamilyDeductionInfo();
  mode = Mode.ADD;

  isSubmitted = false;
  form: FormGroup;

  listFamilyRelationship: Category[] = [];

  listNational: LookupValues[] = [];

  provinces: LookupValues[] = [];
  districts: LookupValues[] = [];
  wards: LookupValues[] = [];
  modelEdit?: FamilyDeductionInfo;

  // Đã CMT / CCCD
  hasPersonalId = false;
  isLoadingPage = false;

  constructor(
    private fb: FormBuilder,
    private modalRef: NzModalRef,
    private toastService: ToastrService,
    private modalService: NzModalService,
    private translate: TranslateService,
    private shareDataService: ShareDataService,
    private staffInfoService: StaffInfoService,
    private familyDeductionInfoService: FamilyDeductionInfoService
  ) {
    this.form = fb.group({
      dependentPersonId: [null],
      employeeId: [null],
      familyRelationshipId: [null, [Validators.required]], //Loại mối quan hệ
      taxNumber: [null, [Validators.maxLength(20)]], // Mã số thuế

      // Đã có GTTT/MST
      personalId: [null, [Validators.maxLength(20)]],

      // Chưa có GTTT/MST
      codeNo: [null, [Validators.required, Validators.maxLength(20)]], // Giấy khai sinh
      bookNo: [null, [Validators.required, Validators.maxLength(20)]], // Quyển số

      nationCode: [null, [Validators.required]], // Quốc gia
      provinceCode: [null, [Validators.required]], // Tỉnh / Thành phố
      districtCode: [null, [Validators.required]], // Quận / Huyện
      wardCode: [null, [Validators.required]], // Phường / Xã
      fromDate: [null, [Validators.required]], // Giảm trừ từ tháng
      toDate: [null], // Giảm trừ đến tháng
      note: [null, Validators.maxLength(500)] // Ghi chú
    }, {
      validators: [
        DateValidator.validateRangeDate('fromDate', 'toDate', 'rangeDateError')
      ]
    });
    this.getNational();
    this.getProvinces();
    this.getListFamilyRelationship();
    this.onChanges();
  }

  get f() {
    return this.form.controls;
  }

  ngOnInit(): void {
    this.f['employeeId'].setValue(this.shareDataService.getEmployee().employeeId);
  }

  patchValueInfo() {
    this.isLoadingPage = true;
    this.familyDeductionInfoService.getRecord(this.data.dependentPersonId).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.modelEdit = res?.data;
        this.form.patchValue({
          dependentPersonId: res.data.dependentPersonId,
          familyRelationshipId: res.data.familyRelationshipId, //Loại mối quan hệ
          taxNumber: res.data.taxNumber, // Mã số thuế
          provinceCode: res.data.provinceCode, // Tỉnh / Thành phố
          districtCode: res.data.districtCode, // Quận / Huyện
          wardCode: res.data.wardCode, // Phường / Xã

          nationCode: res.data.nationCode, // Quốc gia
          note: res.data.note ? res.data.note : '' // Ghi chú
        });
        this.hasPersonalId = StringUtils.isNullOrEmpty(res.data.codeNo) && StringUtils.isNullOrEmpty(res.data.bookNo);
        this.checkHasPersonalId(this.hasPersonalId);
        this.form.patchValue({
          // Đã có GTTT/MST
          personalId: res.data.personalId,

          // Chưa có GTTT/MST
          codeNo: res.data.codeNo, // Giấy khai sinh
          bookNo: res.data.bookNo // Quyển số
        });
        if (this.data.fromDate)
          this.f['fromDate'].setValue(moment(res.data.fromDate, 'MM/YYYY').toDate());
        if (this.data.toDate)
          this.f['toDate'].setValue(moment(res.data.toDate, 'MM/YYYY').toDate());
      } else {
        this.toastService.error(res?.message);
      }
      this.isLoadingPage = false;
    }, () => {
      this.modalRef.close();
      this.isLoadingPage = false;
    });
  }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      this.isLoadingPage = true;
      const request: FamilyDeductionInfo = this.form.getRawValue();
      request.fromDate = this.f['fromDate'].value ? moment(this.f['fromDate'].value).format('MM/YYYY') : null;
      request.toDate = this.f['toDate'].value ? moment(this.f['toDate'].value).format('MM/YYYY') : null;

      this.familyDeductionInfoService.saveRecord(request).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.toastService.success(this.translate.instant('common.notification.updateSuccess'));
          this.modalRef.close({ refresh: true });
        } else {
          this.toastService.error(this.translate.instant('common.notification.updateError') + ':' + res.message);
        }
        this.isLoadingPage = false;
      });
    }
  }

  checkHasPersonalId(checked: boolean) {

    if (checked) {
      Utils.fullResetFormControl([this.f['codeNo'], this.f['bookNo'], this.f['nationCode'], this.f['provinceCode'], this.f['districtCode'], this.f['wardCode']]);
    } else {
      Utils.fullResetFormControl([this.f['taxNumber'], this.f['personalId']]);
      this.f['codeNo'].setValidators(Validators.required);
      this.f['bookNo'].setValidators(Validators.required);
      this.f['nationCode'].setValidators(Validators.required);
      this.f['provinceCode'].setValidators(Validators.required);
      this.f['districtCode'].setValidators(Validators.required);
      this.f['wardCode'].setValidators(Validators.required);
    }

    // if ( this.modelEdit && this.modelEdit?.dependentPersonId && this.modelEdit?.dependentPersonId > 0) {
    //   if (checked) {
    //
    //   } else {
    //
    //   }
    // } else {
    //
    // }
  }

  getListFamilyRelationship() {
    this.familyDeductionInfoService.getFamilyRelationships(this.shareDataService.getEmployee().employeeId).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.listFamilyRelationship = res.data.map((fr: any) => {
          const cat = new Category();
          cat.label = fr.relationTypeName + ' ' + fr.fullName;
          cat.value = fr.familyRelationshipId;
          return cat;
        });
      } else {
        this.toastService.error(res?.message);
      }
    });
  }

  getNational() {
    this.staffInfoService.getCatalog(Constant.CATALOGS.QUOC_GIA).subscribe((res: BaseResponse) => {
      this.listNational = res.data;
    });
  }

  getProvinces() {
    this.staffInfoService.getCatalog(Constant.CATALOGS.TINH).subscribe((res: BaseResponse) => {
      this.provinces = res.data;
    });
  }

  getDistricts(parentCode?: number) {
    if (parentCode) {
      this.staffInfoService.getCatalog(Constant.CATALOGS.HUYEN, parentCode).subscribe((res: BaseResponse) => {
        this.districts = res.data;
      });
    }
  }

  getWards(parentCode?: number) {
    if (parentCode) {
      this.staffInfoService.getCatalog(Constant.CATALOGS.XA, parentCode).subscribe((res: BaseResponse) => {
        this.wards = res.data;
      });
    }
  }

  onChanges() {
    this.f['provinceCode'].valueChanges.subscribe(value => {
      this.f['districtCode'].setValue(null, { emitEvent: false, onlySelf: true });
      this.f['wardCode'].setValue(null, { emitEvent: false, onlySelf: true });
      this.wards = [];
      this.getDistricts(value);
    });

    this.f['districtCode'].valueChanges.subscribe(value => {
      this.f['wardCode'].setValue(null, { emitEvent: false, onlySelf: true });
      this.getWards(value);
    });
  }

  ngAfterViewInit(): void {
    if (this.mode === Mode.EDIT)
      this.patchValueInfo();
  }
}
