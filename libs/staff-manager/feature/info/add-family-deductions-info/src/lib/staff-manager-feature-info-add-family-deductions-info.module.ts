import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AddFamilyDeductionsInfoComponent} from "./add-family-deductions-info/add-family-deductions-info.component";
import {NzFormModule} from "ng-zorro-antd/form";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {TranslateModule} from "@ngx-translate/core";
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
    imports: [CommonModule, NzFormModule, ReactiveFormsModule, SharedUiMbSelectModule, TranslateModule, NzCheckboxModule, FormsModule, SharedUiMbInputTextModule, SharedUiMbDatePickerModule, SharedUiLoadingModule],
  declarations: [AddFamilyDeductionsInfoComponent],
  exports: [AddFamilyDeductionsInfoComponent],
})
export class StaffManagerFeatureInfoAddFamilyDeductionsInfoModule {}
