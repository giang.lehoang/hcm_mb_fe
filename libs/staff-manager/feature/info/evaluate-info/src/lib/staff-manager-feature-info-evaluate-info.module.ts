import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EvaluateInfoComponent } from './evaluate-info/evaluate-info.component';
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";

@NgModule({
  imports: [CommonModule, SharedUiMbTableWrapModule, SharedUiMbTableModule],
  declarations: [
    EvaluateInfoComponent
  ],
})
export class StaffManagerFeatureInfoEvaluateInfoModule {}
