import {Component, OnDestroy, OnInit} from '@angular/core';
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {ActivatedRoute} from "@angular/router";
import {EvaluateInfoService} from "@hcm-mfe/staff-manager/data-access/services";
import {HttpParams} from "@angular/common/http";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {Subscription} from "rxjs";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'hcm-mfe-evaluate-info',
  templateUrl: './evaluate-info.component.html',
  styleUrls: ['./evaluate-info.component.scss']
})
export class EvaluateInfoComponent implements OnInit, OnDestroy {
  dataTable: NzSafeAny[] = [];
  resulSearch: NzSafeAny[] = [];
  tableConfig!: MBTableConfig;
  pagination = new Pagination();
  employeeCode!: string;
  subs: Subscription[] = [];


  constructor(
    private activeRoute: ActivatedRoute,
    private evaluateInfoService: EvaluateInfoService,
    private toastService: ToastrService
  ) { }

  ngOnInit(): void {
    this.employeeCode = this.activeRoute.snapshot.queryParams['employeeCode'];
    this.initTable();
    this.doSearch(1);
  }

  initTable() {
    this.tableConfig = {
      headers: [
        {
          title: 'staffManager.evaluateInfo.periodName',
          field: 'periodName'
        },
        {
          title: 'staffManager.evaluateInfo.score',
          field: 'score',
        },
        {
          title: 'staffManager.evaluateInfo.ratingBefore',
          field: 'ratingBefore',
        },
        {
          title: 'staffManager.evaluateInfo.ratingAfter',
          field: 'ratingAfter'
        }
      ],
      total: 0,
      needScroll: false,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  doSearch(pageIndex: number) {
    const params = new HttpParams().set("empCode", this.employeeCode).set("numPeriod", 10000);
    this.subs.push(
      this.evaluateInfoService.getEvaluateInfo(params).subscribe(res => {
        if (res.status === HTTP_STATUS_CODE.OK) {
          this.resulSearch = res.data.listScore;
          this.tableConfig.total = this.resulSearch?.length;
          this.changePage(pageIndex);
        }
      }, error => {
        this.toastService.error(error.message);
      })
    );
  }

  changePage(pageIndex: number) {
    this.tableConfig.pageIndex = pageIndex;
    const start = (pageIndex - 1)*this.pagination.pageSize;
    this.dataTable = this.resulSearch?.slice(start, start + this.pagination.pageSize);
  }

  ngOnDestroy() {
    this.subs.forEach((s) => s.unsubscribe());
  }
}
