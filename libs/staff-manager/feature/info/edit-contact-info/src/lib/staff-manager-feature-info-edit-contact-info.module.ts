import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EditContactInfoComponent} from "./edit-contact-info/edit-contact-info.component";
import {NzFormModule} from "ng-zorro-antd/form";
import {ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {NzMenuModule} from "ng-zorro-antd/menu";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
    imports: [CommonModule, NzFormModule, ReactiveFormsModule, SharedUiMbInputTextModule, SharedUiMbSelectModule, NzMenuModule, TranslateModule, SharedUiLoadingModule],
  declarations: [EditContactInfoComponent],
  exports: [EditContactInfoComponent],
})
export class StaffManagerFeatureInfoEditContactInfoModule {}
