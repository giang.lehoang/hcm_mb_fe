import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Subscription} from "rxjs";
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";
import {NzModalRef} from "ng-zorro-antd/modal";
import {ContactInfo, LookupValues} from "@hcm-mfe/staff-manager/data-access/models/info";
import {ContactInfoService, StaffInfoService} from "@hcm-mfe/staff-manager/data-access/services";
import {BaseResponse, SelectModal} from "@hcm-mfe/shared/data-access/models";
import {Constant} from "@hcm-mfe/staff-manager/data-access/common";
import {NzSafeAny} from "ng-zorro-antd/core/types";

@Component({
  selector: 'app-edit-contact-info',
  templateUrl: './edit-contact-info.component.html',
  styleUrls: ['./edit-contact-info.component.scss']
})
export class EditContactInfoComponent implements OnInit, OnDestroy {
  @Input() data: NzSafeAny;
  form: FormGroup;
  subs: Subscription[] = [];
  provinces: LookupValues[] = [];
  districts: LookupValues[] = [];
  wards: LookupValues[] = [];
  districtsCurrent: LookupValues[] = [];
  wardsCurrent: LookupValues[] = [];
  isSubmitted = false;
  contactInfo: ContactInfo | NzSafeAny;
  isLoadingProvinces = true;
  isLoadingDistricts = true;
  isLoadingWards = true;
  isLoadingProvincesCurrent = true;
  isLoadingDistrictsCurrent = true;
  isLoadingWardsCurrent = true;
  isLoadingPage = false;

  constructor(
    private fb: FormBuilder,
    private staffInfoService: StaffInfoService,
    private contactInfoService: ContactInfoService,
    private toastService: ToastrService,
    private translate: TranslateService,
    private modalRef: NzModalRef
  ) {
    this.form = this.fb.group({
      placeOfBirth: [null, Validators.maxLength(200)],
      originalAddress: [null, Validators.maxLength(200)],
      pernamentProvinceCode: null,
      pernamentDistrictCode: null,
      pernamentWardCode: null,
      pernamentDetail: [null, Validators.maxLength(200)],
      currentProvinceCode: null,
      currentDistrictCode: null,
      currentWardCode: null,
      currentDetail: [null, Validators.maxLength(200)],
    });
  }

  ngOnInit(): void {
    this.contactInfo = this.data?.data;
    this.getCatalogs();
    this.pathValue();
  }

  ngOnDestroy() {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  pathValue() {
    if (this.contactInfo) {
      this.form.patchValue({
        placeOfBirth: this.contactInfo.placeOfBirth,
        originalAddress: this.contactInfo.originalAddress,
        pernamentProvinceCode: this.contactInfo.pernamentProvinceCode,
        pernamentDistrictCode: this.contactInfo.pernamentDistrictCode,
        pernamentWardCode: this.contactInfo.pernamentWardCode,
        pernamentDetail: this.contactInfo.pernamentDetail,
        currentProvinceCode: this.contactInfo.currentProvinceCode,
        currentDistrictCode: this.contactInfo.currentDistrictCode,
        currentWardCode: this.contactInfo.currentWardCode,
        currentDetail: this.contactInfo.currentDetail,
      });
    }
  }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      this.isLoadingPage = true;
      const value = this.form.value;
      this.contactInfoService.saveContactInfo(this.data?.employeeId, value).subscribe(res => {
        if (res && res.code === 200) {
          this.toastService.success(this.translate.instant('common.notification.updateSuccess'));
          this.modalRef.close({refresh: true});
        } else {
          this.toastService.error(this.translate.instant('common.notification.updateError') + ': ' + res?.message);
          this.modalRef.close({refresh: false});
        }
        this.isLoadingPage = false;
      }, error =>  {
        this.toastService.error(this.translate.instant('common.notification.updateError') + ": " + error.message);
        this.modalRef.close({refresh: false});
        this.isLoadingPage = false;
      });
    }
  }

  getCatalogs() {
    this.getProvinces();
  }

  getProvinces() {
    this.subs.push(
      this.staffInfoService.getCatalog(Constant.CATALOGS.TINH).subscribe(res => {
        const response: BaseResponse = res;
        this.provinces = response.data;
        this.isLoadingProvinces = false;
        this.isLoadingProvincesCurrent = false;
      })
    );
  }

  getDistricts(parentCode?: number, type: 'HK' | 'HT' = 'HK') {
     if (parentCode) {
       if (type === 'HK') {
         this.isLoadingDistricts = true;
       } else {
         this.isLoadingDistrictsCurrent = true;
       }
      this.subs.push(
        this.staffInfoService.getCatalog(Constant.CATALOGS.HUYEN, parentCode).subscribe(res => {
          const response: BaseResponse = res;
          if (type === 'HK') {
            this.districts = response.data;
            this.isLoadingDistricts = false;
          } else {
            this.districtsCurrent = response.data;
            this.isLoadingDistrictsCurrent = false;
          }
        })
      );
    }
  }

  getWards(parentCode?: number, type: 'HK' | 'HT' = 'HK') {
    if (parentCode) {
      if (type === 'HK') {
        this.isLoadingWards = true;
      } else {
        this.isLoadingWardsCurrent = true;
      }
      this.subs.push(
        this.staffInfoService.getCatalog(Constant.CATALOGS.XA, parentCode).subscribe(res => {
          const response: BaseResponse = res;
          if (type === 'HK') {
            this.wards = response.data;
            this.isLoadingWards = false;
          } else {
            this.wardsCurrent = response.data;
            this.isLoadingWardsCurrent = false;
          }
        })
      );
    }
  }

  changeProvince($event: SelectModal, type: 'HK' | 'HT') {
    if (type === 'HK' && $event?.itemSelected?.value !== this.contactInfo.pernamentProvinceCode) {
      this.contactInfo.pernamentProvinceCode = $event?.itemSelected?.value;
      this.form.controls['pernamentDistrictCode'].reset();
      this.form.controls['pernamentWardCode'].reset();
    }
    if (type === 'HT' && $event?.itemSelected?.value !== this.contactInfo.currentProvinceCode) {
      this.contactInfo.currentProvinceCode = $event?.itemSelected?.value;
      this.form.controls['currentDistrictCode'].reset();
      this.form.controls['currentWardCode'].reset();
    }
    this.getDistricts($event?.itemSelected?.value, type);
  }

  changeDistrict($event: SelectModal, type: 'HK' | 'HT') {
    if (type === 'HK' && $event?.itemSelected?.value !== this.contactInfo.pernamentDistrictCode) {
      this.contactInfo.pernamentDistrictCode = $event?.itemSelected?.value;
      this.form.controls['pernamentWardCode'].reset();
    }
    if (type === 'HT' && $event?.itemSelected?.value !== this.contactInfo.currentDistrictCode) {
      this.contactInfo.currentDistrictCode = $event?.itemSelected?.value;
      this.form.controls['currentWardCode'].reset();
    }
    this.getWards($event?.itemSelected?.value, type);
  }
}
