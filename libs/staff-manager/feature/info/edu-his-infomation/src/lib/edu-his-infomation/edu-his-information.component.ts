import {Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {Subscription} from "rxjs";
import {NzModalRef, NzModalService} from "ng-zorro-antd/modal";
import {TranslateService} from "@ngx-translate/core";
import {ToastrService} from "ngx-toastr";
import {AppFunction, MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {BaseResponse, EduHisInfo} from "@hcm-mfe/staff-manager/data-access/models/info";
import {ValidateService} from "@hcm-mfe/shared/core";
import {EduHisInfoService, ShareDataService} from "@hcm-mfe/staff-manager/data-access/services";
import {SessionService} from "@hcm-mfe/shared/common/store";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {AddEduHisInfoComponent} from "@hcm-mfe/staff-manager/feature/info/add-edu-his-info";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";

@Component({
  selector: 'app-edu-his-information',
  templateUrl: './edu-his-information.component.html',
  styleUrls: ['./edu-his-information.component.scss']
})
export class EduHisInformationComponent implements OnInit, OnDestroy {
  objFunction: AppFunction;
  tableConfig!: MBTableConfig;
  dataTable: EduHisInfo[] = [];
  employeeId: number | null = null;
  modal?: NzModalRef;
  subs: Subscription[] = [];
  pagination = new Pagination();

  @ViewChild('actionTmpl', {static: true}) actionTmpl!: TemplateRef<NzSafeAny>;

  constructor(
    public validateService: ValidateService,
    private eduHisInfoService: EduHisInfoService,
    private modalService: NzModalService,
    private translate: TranslateService,
    private toastService: ToastrService,
    private shareDataService: ShareDataService,
    private sessionService: SessionService,
  ) {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.HR_EDU_PROCESS}`);
  }

  ngOnInit(): void {
    this.subs.push(
      this.shareDataService.employee$.subscribe(employee => {
        this.employeeId = employee.employeeId;
        this.initTable();
        this.doSearch(1);
      })
    );
  }

  doSearch(pageNumber: number) {
    this.tableConfig = {...this.tableConfig, loading: true};
    this.pagination.pageNumber = pageNumber;
    const param = this.pagination.getCurrentPage();
    if (this.employeeId) {
      this.subs.push(
        this.eduHisInfoService.getEduHisInfo(this.employeeId, param).subscribe(res => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.dataTable = res.data.listData;
            this.tableConfig.total = res.data.count;
          } else {
            this.toastService.error(res?.message);
          }
          this.tableConfig = { ...this.tableConfig, loading: false };
        }, () => {
          this.tableConfig = { ...this.tableConfig, loading: false };
          this.dataTable = [];
        })
      )
    }
  }

  initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'staffManager.table.courseName',
          field: 'courseName',
          width: 150
        },
        {
          title: 'staffManager.table.eduForm',
          field: 'eduMethodTypeName',
          width: 150
        },
        {
          title: 'staffManager.table.eduContent',
          field: 'courseContent',
          width: 150
        },
        {
          title: 'staffManager.table.eduFromDate',
          field: 'fromDate',
          width: 100,
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        },
        {
          title: 'staffManager.table.eduToDate',
          field: 'toDate',
          width: 100,
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        },
        {
          title: 'staffManager.table.result',
          field: 'result',
          width: 150
        },
        {
          title: 'staffManager.table.refundAmount',
          field: 'refundAmount',
          pipe: 'currency: VND',
          width: 150
        },
        {
          title: 'staffManager.table.commitmentDate',
          field: 'commitmentDate',
          width: 100,
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        },
        {
          title: 'staffManager.label.note',
          field: 'note',
          show: false,
          width: 150
        },
        {
          title: ' ',
          field: 'action',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          fixed: true,
          fixedDir: "right",
          tdTemplate: this.actionTmpl,
          width: 80,
          show: this.objFunction?.delete || this.objFunction?.edit
        }
      ],
      total: 0,
      needScroll: true,
      loading: true,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  showModalUpdate(educationProcessId: number, footerTmpl: TemplateRef<Record<string, never>>) {
    const data = this.dataTable.find(item => item.educationProcessId === educationProcessId);
    this.modal = this.modalService.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 2 : window.innerWidth,
      nzTitle: this.translate.instant('staffManager.eduHisInformation.modal.update'),
      nzContent: AddEduHisInfoComponent,
      nzComponentParams: {
        data: data
      },
      nzFooter: footerTmpl
    });
    this.modal.afterClose.subscribe(result => result?.refresh ? this.doSearch(1): '');
  }

  deleteItem(educationProcessId: number) {
    this.eduHisInfoService.deleteEduHisInfo(educationProcessId).subscribe((res: BaseResponse) => {
      if (res.code === 200) {
        this.doSearch(1);
        this.toastService.success(this.translate.instant('common.notification.deleteSuccess'));
      }
    })
  }

  ngOnDestroy() {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  refresh() {
    this.doSearch(1);
  }

}
