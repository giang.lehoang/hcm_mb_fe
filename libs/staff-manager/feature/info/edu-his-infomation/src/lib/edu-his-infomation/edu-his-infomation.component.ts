import {Component, OnInit, TemplateRef} from '@angular/core';
import {Subscription} from "rxjs";
import {NzModalRef, NzModalService} from "ng-zorro-antd/modal";
import {TranslateService} from "@ngx-translate/core";
import {ToastrService} from "ngx-toastr";
import {AppFunction, Pagination} from "@hcm-mfe/shared/data-access/models";
import {BaseResponse, EduHisInfo, EduHisInfoGroup} from "@hcm-mfe/staff-manager/data-access/models/info";
import {ValidateService} from "@hcm-mfe/shared/core";
import {EduHisInfoService, ShareDataService} from "@hcm-mfe/staff-manager/data-access/services";
import { SessionService } from "@hcm-mfe/shared/common/store";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {AddEduHisInfoComponent} from "@hcm-mfe/staff-manager/feature/info/add-edu-his-info";

@Component({
  selector: 'app-edu-his-infomation',
  templateUrl: './edu-his-infomation.component.html',
  styleUrls: ['./edu-his-infomation.component.scss']
})
export class EduHisInfomationComponent implements OnInit {
  objFunction: AppFunction;
  items: EduHisInfoGroup | any;
  loading = false;
  eduHisInfos: EduHisInfo[] = [];
  subs: Subscription[] = [];
  response: BaseResponse = new BaseResponse();
  employeeId: number = 1;
  modal?: NzModalRef;
  count = 0;
  pagination = new Pagination();

  constructor(
    public validateService: ValidateService,
    private eduHisInfoService: EduHisInfoService,
    private modalService: NzModalService,
    private translate: TranslateService,
    private toastrService: ToastrService,
    private shareDataService: ShareDataService,
    private sessionService: SessionService,
  ) {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.HR_EDU_PROCESS}`);
  }

  ngOnInit(): void {
    this.subs.push(
      this.shareDataService.employee$.subscribe(employee => {
        this.employeeId = employee.employeeId;
        this.pagination.pageNumber = 1;
        this.getEduHisInfos();
      })
    );
  }

  ngOnDestroy() {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  getEduHisInfos() {
    this.loading = true;
    let param = this.pagination.getCurrentPage();

    this.subs.push(
      this.eduHisInfoService.getEduHisInfo(this.employeeId, param).subscribe(res => {
        this.response = res;
        this.loading = false
        this.eduHisInfos = this.response?.data?.listData;
        this.count = this.response?.data?.count;
        this.items = {employeeId: this.employeeId, data: this.response?.data?.listData};
      }, () =>this.loading = false)
    );
  }

  refresh() {
    this.getEduHisInfos();
  }

  resetContextMenu() {

  }

  rightClickTable($event: MouseEvent, data: any) {

  }

  showModalUpdate(data: EduHisInfo, footerTmpl: TemplateRef<{}>) {
    const valueData = {employeeId: this.employeeId, data: data};
    this.modal = this.modalService.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 2 : window.innerWidth,
      nzTitle: this.translate.instant('staffManager.eduHisInformation.modal.update'),
      nzContent: AddEduHisInfoComponent,
      nzComponentParams: {
        data: data
      },
      nzFooter: footerTmpl
    });
    this.modal.afterClose.subscribe(result => result?.refresh ? this.refresh(): '')
  }

  deleteItem(data: EduHisInfo) {
    this.eduHisInfoService.deleteEduHisInfo(data.educationProcessId).subscribe((res: BaseResponse) => {
      if (res.code === 200) {
        this.pagination.pageNumber = 1;
        this.getEduHisInfos();
        this.toastrService.success(this.translate.instant('common.notification.deleteSuccess'));
      }
    })
  }
}
