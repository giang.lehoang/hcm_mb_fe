import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EduHisInformationComponent} from "./edu-his-infomation/edu-his-information.component";
import {NzTableModule} from "ng-zorro-antd/table";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzPopconfirmModule} from "ng-zorro-antd/popconfirm";
import {SharedPipesFormatCurrencyModule} from "@hcm-mfe/shared/pipes/format-currency";
import {SharedUiMbTableMergeCellWrapModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell-wrap";
import {SharedUiMbTableMergeCellModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";

@NgModule({
  imports: [CommonModule, NzTableModule, TranslateModule, SharedUiMbButtonModule, NzPopconfirmModule, SharedPipesFormatCurrencyModule, SharedUiMbTableMergeCellWrapModule, SharedUiMbTableMergeCellModule, SharedUiMbTableWrapModule, SharedUiMbTableModule],
  declarations: [EduHisInformationComponent],
  exports: [EduHisInformationComponent],
})
export class StaffManagerFeatureInfoEduHisInfomationModule {}
