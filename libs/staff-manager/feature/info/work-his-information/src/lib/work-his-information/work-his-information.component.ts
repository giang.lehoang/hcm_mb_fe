import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {NzModalRef, NzModalService} from "ng-zorro-antd/modal";
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";
import {saveAs} from 'file-saver';
import {AppFunction, BaseResponse, MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {WorkHistory} from "@hcm-mfe/staff-manager/data-access/models/info";
import {
  DownloadFileAttachService,
  ShareDataService,
  WorkInfoService
} from "@hcm-mfe/staff-manager/data-access/services";
import {SessionService} from "@hcm-mfe/shared/common/store";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {ValidateService} from "@hcm-mfe/shared/core";
import {HTTP_STATUS_CODE, Mode} from "@hcm-mfe/shared/common/constants";
import {EditWorkHisInfoComponent} from "@hcm-mfe/staff-manager/feature/info/edit-work-his-info";
import {getTypeExport} from "@hcm-mfe/shared/common/utils";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {Constant} from "@hcm-mfe/staff-manager/data-access/common";

@Component({
  selector: 'app-work-his-information',
  templateUrl: './work-his-information.component.html',
  styleUrls: ['./work-his-information.component.scss'],
})
export class WorkHisInformationComponent implements OnInit {
  objFunction: AppFunction;
  tableConfig!: MBTableConfig;
  dataTable: WorkHistory[] = [];
  employeeId: number | null = null;
  modal?: NzModalRef;
  pagination = new Pagination();
  constant = Constant;

  @ViewChild('actionTmpl', { static: true }) actionTmpl!: TemplateRef<NzSafeAny>;
  @ViewChild('statusTmpl', { static: true }) statusTmpl!: TemplateRef<NzSafeAny>;
  @ViewChild('fileTmpl', { static: true }) fileTmpl!: TemplateRef<NzSafeAny>;

  constructor(
    public validateService: ValidateService,
    private http: HttpClient,
    private toastService: ToastrService,
    private modalService: NzModalService,
    private translate: TranslateService,
    private shareDataService: ShareDataService,
    private workInfoService: WorkInfoService,
    private downloadFileAttachService: DownloadFileAttachService,
    private sessionService: SessionService,
  ) {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.HR_WORK_PROCESS}`);
  }

  ngOnInit(): void {
    this.shareDataService.employee$.subscribe(employee => {
      this.employeeId = employee.employeeId;
      this.initTable();
      this.doSearch(1);
    })
  }

  initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'staffManager.workHisInformation.label.table.fromDate',
          field: 'fromDate',
          width: 100,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
        },
        {
          title: 'staffManager.workHisInformation.label.table.toDate',
          field: 'toDate',
          width: 100,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
        },
        {
          title: 'staffManager.workHisInformation.label.table.empTypeName',
          field: 'empTypeName',
          width: 150,
        },
        {
          title: 'staffManager.workHisInformation.label.table.documentTypeName',
          field: 'documentTypeName',
          width: 150,
        },
        {
          title: 'staffManager.workHisInformation.label.table.positionName',
          field: 'jobName',
          width: 150,
        },
        {
          title: 'staffManager.workHisInformation.label.attributes.positionGroup',
          field: 'pgrName',
          width: 150,
        },
        {
          title: 'staffManager.workHisInformation.label.table.organizationName',
          field: 'organizationName',
          width: 150,
        },
        {
          title: 'staffManager.workHisInformation.label.table.positionLevelName',
          field: 'positionLevelName',
          width: 150,
        },
        {
          title: 'staffManager.workHisInformation.label.attributes.documentNo',
          field: 'documentNo',
          width: 150,
          show: false
        },
        {
          title: 'staffManager.workHisInformation.label.attributes.signedDate',
          field: 'signedDate',
          width: 100,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          show: false
        },
        {
          title: 'staffManager.workHisInformation.label.attributes.trial',
          field: 'isTrialStr',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 150,
          show: false
        },
        {
          title: 'staffManager.workHisInformation.label.attributes.toDate',
          field: 'planToDate1',
          width: 100,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          show: false
        },
        {
          title: 'staffManager.workHisInformation.label.attributes.toDate2',
          field: 'planToDate2',
          width: 100,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          show: false
        },
        {
          title: 'staffManager.workHisInformation.label.attributes.keepSenior',
          field: 'isKeepSeniorStr',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 150,
          show: false
        },
        {
          title: 'staffManager.workHisInformation.label.attributes.note',
          field: 'note',
          width: 150,
          show: false
        },
        {
          title: 'staffManager.label.file',
          tdTemplate: this.fileTmpl,
          width: 130,
          show: false
        },
        {
          title: 'staffManager.workHisInformation.label.table.status',
          fixed: true,
          fixedDir: "right",
          width: 130,
          tdTemplate: this.statusTmpl
        },
        {
          title: ' ',
          field: 'action',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          fixed: true,
          fixedDir: "right",
          tdTemplate: this.actionTmpl,
          width: 80,
          show: this.objFunction?.delete || this.objFunction?.edit
        }
      ],
      total: 0,
      needScroll: true,
      loading: true,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  doSearch(pageNumber: number) {
    this.tableConfig = { ...this.tableConfig, loading: true };
    this.pagination.pageNumber = pageNumber;
    const param = this.pagination.getCurrentPage();
    this.workInfoService.getList(this.employeeId, param).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.dataTable = res.data.listData.map((item: WorkHistory) => {
          if (item.isTrial === 1) {
            item.isTrialStr = 'X';
          }
          if (item.isKeepSenior === 1) {
            item.isKeepSeniorStr = 'X';
          }
          return item;
        });
        this.tableConfig.total = res.data.count;
      } else {
        this.toastService.error(res?.message);
      }
      this.tableConfig = { ...this.tableConfig, loading: false };
    }, () => {
      this.tableConfig = { ...this.tableConfig, loading: false };
      this.dataTable = []
    })
  }

  showModalUpdate(workProcessId: number, draftWorkProcessId: number, footerTmpl: TemplateRef<Record<string, never>>) {
    const data = this.getRecord(workProcessId, draftWorkProcessId);
    this.modal = this.modalService.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
      nzTitle: this.translate.instant('staffManager.workHisInformation.modal.update'),
      nzContent: EditWorkHisInfoComponent,
      nzComponentParams: {
        mode: Mode.EDIT,
        data: data
      },
      nzFooter: footerTmpl
    });
    this.modal.afterClose.subscribe(result => result?.refresh ? this.doSearch(1): '')
  }

  getRecord(workProcessId: number, draftWorkProcessId: number): WorkHistory | undefined {
    if (workProcessId) {
      return this.dataTable.find(item => item.workProcessId === workProcessId);
    }
    return this.dataTable.find(item => item.draftWorkProcessId === draftWorkProcessId);
  }

  deleteItem(workProcessId: number, draftWorkProcessId: number) {
    if (workProcessId && workProcessId > 0) {
      this.workInfoService.deleteRecord(workProcessId).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.doSearch(1);
          this.toastService.success(this.translate.instant('common.notification.deleteSuccess'));
        } else {
          this.toastService.error(res?.message);
        }
      }, () => {
        this.toastService.error(this.translate.instant('common.notification.deleteError'));
      })
    } else if (draftWorkProcessId && draftWorkProcessId > 0) {
      this.workInfoService.deleteDraftRecord(draftWorkProcessId).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.doSearch(1);
          this.toastService.success(this.translate.instant('common.notification.deleteSuccess'));
        } else {
          this.toastService.error(res?.message);
        }
      }, () => {
        this.toastService.error(this.translate.instant('common.notification.deleteError'));
      })
    }
  }

  doDownloadAttach(file: NzSafeAny) {
    this.downloadFileAttachService.doDownloadAttachFileWithSecurity(file?.docId, file?.security).subscribe(res => {
      const reportFile = new Blob([res.body], {type: getTypeExport(file.fileName.split(".").pop())});
      saveAs(reportFile, file.fileName);
    });
  }

  getStatus(param: NzSafeAny) {
    return Constant.APPROVAL_STATUSES.find((item) => item.value === param)?.label;
  }

  refresh() {
    this.doSearch(1);
  }

}
