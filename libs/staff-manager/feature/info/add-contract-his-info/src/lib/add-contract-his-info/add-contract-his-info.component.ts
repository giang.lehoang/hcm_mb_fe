import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NzModalRef, NzModalService} from 'ng-zorro-antd/modal';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import * as moment from 'moment';
import {NzUploadFile} from 'ng-zorro-antd/upload';
import {saveAs} from 'file-saver';
import {HTTP_STATUS_CODE, Mode} from "@hcm-mfe/shared/common/constants";
import {
  ContractHisInfoService,
  DownloadFileAttachService,
  ShareDataService,
} from "@hcm-mfe/staff-manager/data-access/services";
import {Constant} from "@hcm-mfe/staff-manager/data-access/common";
import {BaseResponse, Category} from "@hcm-mfe/shared/data-access/models";
import {DateValidator} from "@hcm-mfe/shared/common/validators";
import {ContractHistory} from "@hcm-mfe/staff-manager/data-access/models/info";
import {beforeUploadFile, getTypeExport} from "@hcm-mfe/shared/common/utils";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {concatMap, of, Subscription} from "rxjs";

@Component({
  selector: 'app-add-contract-his-info',
  templateUrl: './add-contract-his-info.component.html',
  styleUrls: ['./add-contract-his-info.component.scss'],
})
export class AddContractHisInfoComponent implements OnInit, OnDestroy {
  data = new ContractHistory();
  mode = Mode.ADD;

  isSubmitted = false;
  form: FormGroup;

  fileList: NzUploadFile[] = [];
  listContractType: Category[] = [];
  listVLookupContract: Category[] = [];
  typeCode: string = Constant.CONTRACT_TYPE_CODE.PHU_HOP_DONG;
  docIdsDelete: number[] = [];
  isLoadingPage = false;
  toDateOfModelEdit = '';
  subs: Subscription[] = [];
  cloneFile!: number;


  constructor(
    private fb: FormBuilder,
    private modalRef: NzModalRef,
    private toastService: ToastrService,
    private modalService: NzModalService,
    private translate: TranslateService,
    private shareDataService: ShareDataService,
    private contractHisInfoService: ContractHisInfoService,
    private downloadFileAttachService: DownloadFileAttachService,
  ) {
    this.form = fb.group({
      contractProcessId: [null],
      employeeId: [null],
      fromDate: [null, [Validators.required]], // Từ ngày
      toDate: [null], // Đến ngày
      signedDate: [null], // Ngày ký
      contractTypeId: [null, [Validators.required]], // Loại HĐ
      classifyCode: [null, [Validators.required]], //Phân Loại HĐ
      contractNumber: [null],
      signer: [null],
      signerPosition: [null],
      note: [null, [Validators.maxLength(500)]] // Ghi chú
    }, {
      validators: [
        DateValidator.validateRangeDate('fromDate', 'toDate', 'rangeDateError')
      ]
    });
  }

  get f() {
    return this.form.controls;
  }

  ngOnInit(): void {
    this.getVLookupContract();
    if (this.mode === Mode.EDIT) {
      this.getModelEdit();
    }
    this.f['employeeId'].setValue(this.shareDataService.getEmployee().employeeId);
  }

  getModelEdit() {
    if (this.data) {
      if (this.data.contractProcessId && this.data.contractProcessId > 0) {
        this.getDataById();
        this.cloneFile = 1;
      } else {
        this.getDraftDataById();
      }
    }
  }

  getDataById() {
    this.isLoadingPage = true;
    this.subs.push(
      this.contractHisInfoService.getDataById(this.data.contractProcessId).pipe(
        concatMap((res) => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.data = res?.data;
            return this.contractHisInfoService.getContractTypes(res?.data.classifyCode);
          } else {
            this.toastService.error(res?.message);
          }
          return of(null);
        })
      ).subscribe((res) => {
        if (res) {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.listContractType = res.data;
            this.doSomeWorkAfterGetEditModel();
          } else {
            this.toastService.error(res?.message);
          }
          this.isLoadingPage = false;
        }
      })
    );
  }

  getDraftDataById() {
    this.isLoadingPage = true;
    this.subs.push(
      this.contractHisInfoService.getDraftDataById(this.data.draftContractProcessId).pipe(
        concatMap((res) => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.data = res?.data;
            return this.contractHisInfoService.getContractTypes(res?.data.classifyCode);
          } else {
            this.toastService.error(res?.message);
          }
          return of(null);
        })
      ).subscribe((res) => {
        if (res) {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.listContractType = res.data;
            this.doSomeWorkAfterGetEditModel();
          } else {
            this.toastService.error(res?.message);
          }
          this.isLoadingPage = false;
        }
      })
    );
  }

  doSomeWorkAfterGetEditModel() {
    this.toDateOfModelEdit = this.data.toDate ? this.data.toDate : '';
    this.patchValueInfo();
  }

  patchValueInfo() {
    this.form.patchValue(this.data);
    if (this.data.contractTypeId)
      this.form.controls['contractTypeId'].setValue(this.data.contractTypeId);
    if (this.data.fromDate)
      this.f['fromDate'].setValue(moment(this.data.fromDate, 'DD/MM/YYYY').toDate());
    if (this.data.toDate)
      this.f['toDate'].setValue(moment(this.data.toDate, 'DD/MM/YYYY').toDate());
    if (this.data.signedDate)
      this.f['signedDate'].setValue(moment(this.data.signedDate, 'DD/MM/YYYY').toDate());
    this.contractHisInfoService.getManagerInfo(this.data.signerId).subscribe(res => {
      if (res) {
        this.f['signer'].setValue(res.data);
      }
    }, (err) => {
      this.toastService.error(err?.message);
    });

    if (this.data?.attachFileList && this.data?.attachFileList.length > 0) {
      this.data?.attachFileList.forEach((item) => {
        this.fileList.push({
          uid: item.docId,
          name: item.fileName,
          thumbUrl: item.security,
          status: 'done'
        });
      });
      this.fileList = [...this.fileList];
    }
  }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      this.isLoadingPage = true;
      const request: ContractHistory = this.form.value;
      request.fromDate = this.f['fromDate'].value ? moment(this.f['fromDate'].value).format('DD/MM/YYYY') : null;
      request.toDate = this.toDateOfModelEdit;
      request.toDate = this.f['toDate'].value ? moment(this.f['toDate'].value).format('DD/MM/YYYY') : null;
      request.signedDate = this.f['signedDate'].value ? moment(this.f['signedDate'].value).format('DD/MM/YYYY') : null;
      request.docIdsDelete = this.docIdsDelete;
      request.draftContractProcessId = this.data.draftContractProcessId;
      request.isCloneFile = this.cloneFile;
      if (this.f['signer'].value != null) {
        request.signerId = this.f['signer'].value.employeeId;
      }

      const formData = new FormData();
      formData.append('data', new Blob([JSON.stringify(request)], {
        type: 'application/json'
      }));

      this.fileList.forEach((nzFile: NzSafeAny) => {
        formData.append('files', nzFile);
      });

      this.contractHisInfoService.saveRecord(formData).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.toastService.success(this.translate.instant('common.notification.updateSuccess'));
          this.modalRef.close({ refresh: true });
        } else {
          this.toastService.error(this.translate.instant('common.notification.updateError') + ':' + res.message);
        }
        this.isLoadingPage = false;
      });
    }
  }

  getContractTypeList(classifyCode: string) {
    this.contractHisInfoService.getContractTypes(classifyCode).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) this.listContractType = res.data;
    });
  }

  getVLookupContract() {
    this.contractHisInfoService.getVLookupContract(Constant.CATALOGS.PHAN_LOAI_HOP_DONG).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) this.listVLookupContract = res.data;
    });
  }

  changeVLookupContract(data: string) {
    this.form.patchValue({
      contractTypeId: null
    });
    this.listContractType = [];
    this.getContractTypeList(data);
  }

  beforeUpload = (file: NzUploadFile): boolean => {
    this.fileList =  beforeUploadFile(file, this.fileList, 3000000, this.toastService, this.translate, null)
    return false;
  };

  downloadFile = (file: NzUploadFile) => {
    if (file?.uid) {
      this.downloadFileAttachService.doDownloadAttachFileWithSecurity(~~file?.uid, file?.thumbUrl).subscribe(res => {
        const reportFile = new Blob([res.body], { type: getTypeExport(file.name.split('.').pop()) });
        saveAs(reportFile, file.name);
      });
    }
  }

  removeFile = (file: NzUploadFile) => {
    this.docIdsDelete.push(Number(file.uid));
    const index = this.fileList.indexOf(file);
    this.fileList.splice(index, 1);
    return true;
  }

  ngOnDestroy(): void {
    this.subs?.forEach(sub => sub?.unsubscribe());
  }

}
