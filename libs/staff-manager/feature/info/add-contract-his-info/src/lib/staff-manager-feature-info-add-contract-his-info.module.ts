import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AddContractHisInfoComponent} from "./add-contract-his-info/add-contract-his-info.component";
import {NzFormModule} from "ng-zorro-antd/form";
import {ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/employee-data-picker";
import {NzUploadModule} from "ng-zorro-antd/upload";
import {NzIconModule} from "ng-zorro-antd/icon";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
    imports: [CommonModule, NzFormModule, ReactiveFormsModule, SharedUiMbSelectModule, TranslateModule, SharedUiMbDatePickerModule,
        SharedUiMbInputTextModule, SharedUiEmployeeDataPickerModule, NzUploadModule, NzIconModule, SharedUiLoadingModule],
  declarations: [AddContractHisInfoComponent],
  exports: [AddContractHisInfoComponent],
})
export class StaffManagerFeatureInfoAddContractHisInfoModule {}
