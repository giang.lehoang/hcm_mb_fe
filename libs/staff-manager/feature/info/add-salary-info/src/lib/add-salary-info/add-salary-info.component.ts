import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';
import {HTTP_STATUS_CODE, Mode} from "@hcm-mfe/shared/common/constants";
import {
  SalaryProgressService,
  ShareDataService,
} from "@hcm-mfe/staff-manager/data-access/services";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";
import {Salary, SalaryGrade, SalaryRank} from "@hcm-mfe/staff-manager/data-access/models/info";

@Component({
  selector: 'app-add-salary-info',
  templateUrl: './add-salary-info.component.html',
  styleUrls: ['./add-salary-info.component.scss'],
})
export class AddSalaryInfoComponent implements OnInit {
  mode = Mode.ADD;
  data: Salary = new Salary();

  isSubmitted = false;
  form: FormGroup;

  // Dải lương
  salaryRankList : SalaryRank[] = [];

  // Bậc Lương
  salaryGradeList: SalaryGrade[] = []
  isLoadingPage = false;

  constructor(
    private fb: FormBuilder,
    private modalRef: NzModalRef,
    private toastService: ToastrService,
    private modalService: NzModalService,
    private translate: TranslateService,
    private shareDataService: ShareDataService,
    private salaryProgressService: SalaryProgressService,
  ) {
    this.form = this.fb.group({
      salaryProcessId: [null],
      employeeId: [null],
      fromDate: [null, Validators.required],
      // toDate: [null],
      salaryRankId: [null, Validators.required],
      salaryGradeId: [null, Validators.required],
      salaryAmount: [null, Validators.required],
      salaryPercent: [100, Validators.required],
      performanceFactor: 0, // Hệ số lương hiệu suất
      performancePercent: 0, // Tỉ lệ hưởng lương hiệu suất
      note: [null, [Validators.maxLength(500)]],
    });
    this.getSalaryGradeList();
    this.getSalaryRankList();
  }

  get f() { return this.form.controls; }
  ngOnInit(): void {
    if (this.mode === Mode.EDIT)
      this.patchValueInfo();

      this.f['employeeId'].setValue(this.shareDataService.getEmployee().employeeId)
  }

  patchValueInfo() {
    this.isLoadingPage = true;
    this.salaryProgressService.getRecord(this.data.salaryProcessId).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.data = res?.data;
        this.form.patchValue(this.data);
        this.f['fromDate'].setValue(this.data.fromDate ? moment(this.data.fromDate, 'DD/MM/YYYY').toDate() : null);
      } else {
        this.toastService.error(res?.message);
      }
      this.isLoadingPage = false;
    }, () => {
      this.modalRef.close();
      this.isLoadingPage = false;
    })
  }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      this.isLoadingPage = true;
      const request:Salary = this.form.value;
      request.fromDate = this.f['fromDate'].value ? moment(this.f['fromDate'].value).format('DD/MM/YYYY') : null;
      this.salaryProgressService.saveRecord(request).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.toastService.success(this.translate.instant('common.notification.updateSuccess'));
          this.modalRef.close({refresh:true})
        } else {
          this.toastService.error(this.translate.instant('common.notification.updateError') + ":" + res.message);
        }
        this.isLoadingPage = false;
      }, error => {
        this.toastService.error(this.translate.instant('common.notification.updateError') + ":" + error.message);
        this.isLoadingPage = false;
      })
    }
  }

  getSalaryGradeList() {
    this.salaryProgressService.getSalaryGradeList().subscribe((res: BaseResponse) =>{
      if (res.code === HTTP_STATUS_CODE.OK) this.salaryGradeList = res.data;
    })
  }

  getSalaryRankList() {
    this.salaryProgressService.getSalaryRankList().subscribe((res: BaseResponse) =>{
      if (res.code === HTTP_STATUS_CODE.OK) this.salaryRankList = res.data;
    })
  }

}
