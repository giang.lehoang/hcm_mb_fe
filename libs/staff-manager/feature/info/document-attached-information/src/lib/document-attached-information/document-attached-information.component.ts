import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {Subscription} from 'rxjs';
import {NzModalRef, NzModalService} from 'ng-zorro-antd/modal';
import {TranslateService} from '@ngx-translate/core';
import {saveAs} from 'file-saver';
import {
  DocumentAttachmentService,
  DownloadFileAttachService,
  ShareDataService,
} from "@hcm-mfe/staff-manager/data-access/services";
import {AppFunction, MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {SessionService} from "@hcm-mfe/shared/common/store";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {ValidateService} from "@hcm-mfe/shared/core";
import {EmployeeProfile} from "@hcm-mfe/staff-manager/data-access/models/info";
import {HTTP_STATUS_CODE, Mode} from "@hcm-mfe/shared/common/constants";
import {ActionDocumentAttachedComponent} from "@hcm-mfe/staff-manager/feature/info/action-document-attached";
import {getTypeExport, StringUtils} from "@hcm-mfe/shared/common/utils";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-document-attached-information',
  templateUrl: './document-attached-information.component.html',
  styleUrls: ['./document-attached-information.component.scss']
})
export class DocumentAttachedInformationComponent implements OnInit {
  objFunction: AppFunction
  tableConfig!: MBTableConfig;
  items: EmployeeProfile[] = [];
  dataTable: EmployeeProfile[] = [];
  employeeId: number | null = null;
  modal?: NzModalRef;
  pagination = new Pagination();
  subs: Subscription[] = [];

  @ViewChild('actionTmpl', {static: true}) actionTmpl!: TemplateRef<NzSafeAny>;
  @ViewChild('fileTmpl', {static: true}) fileTmpl!: TemplateRef<NzSafeAny>;

  constructor(
    public validateService: ValidateService,
    private translate: TranslateService,
    private shareDataService: ShareDataService,
    private toastService: ToastrService,
    private modalService: NzModalService,
    public documentAttachmentService: DocumentAttachmentService,
    private downloadFileAttachService: DownloadFileAttachService,
    private sessionService: SessionService,
  ) {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.HR_PROFILE}`);
  }

  ngOnInit(): void {
    this.subs.push(
      this.shareDataService.employee$.subscribe(employee => {
        this.employeeId = employee.employeeId;
        this.initTable();
        this.doSearch(1);
      })
    );
  }

  doSearch(pageNumber: number) {
    this.tableConfig = {...this.tableConfig, loading: true};
    this.pagination.pageNumber = pageNumber;
    const param = this.pagination.getCurrentPage();
    if (this.employeeId) {
      this.documentAttachmentService.getDocumentAttachment(this.employeeId, param).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          const data = res.data.listData.map((item: EmployeeProfile) => {
            if (item?.isHardDocument && item?.isHardDocument === 1) {
              item.saveDocumentOrigin = 'Có';
            } else {
              item.saveDocumentOrigin = 'Không';
            }
            return item;
          });
          this.dataTable = data;
          this.items = data;
          this.tableConfig.total = res.data.count;
        } else {
          this.toastService.error(res?.message);
        }
        this.tableConfig = {...this.tableConfig, loading: false};
      }, () => {
        this.tableConfig = {...this.tableConfig, loading: false};
        this.dataTable = []
      });
    }
  }

  initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'staffManager.documentAttachedInformation.label.documentType',
          field: 'profileTypeName'
        },
        {
          title: 'staffManager.documentAttachedInformation.label.saveDocumentOrigin',
          field: 'saveDocumentOrigin',
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        },
        {
          title: 'staffManager.documentAttachedInformation.table.createdBy',
          field: 'createdBy',
        },
        {
          title: 'staffManager.documentAttachedInformation.table.createdDate',
          field: 'createDate',
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        },
        {
          title: 'staffManager.documentAttachedInformation.label.note',
          field: 'note'
        },
        {
          title: 'staffManager.table.fileAttach',
          tdTemplate: this.fileTmpl,
        },
        {
          title: ' ',
          field: 'action',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          fixed: true,
          fixedDir: "right",
          tdTemplate: this.actionTmpl,
          width: 80,
          show: this.objFunction?.delete || this.objFunction?.edit
        }
      ],
      total: 0,
      needScroll: false,
      loading: true,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  showModalUpdate(employeeProfileId: number, footerTmpl: TemplateRef<Record<string, never>>) {
    const data = this.dataTable.find(item => item.employeeProfileId === employeeProfileId);
    this.modal = this.modalService.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
      nzTitle: this.translate.instant('staffManager.contractHisInfo.modal.add'),
      nzContent: ActionDocumentAttachedComponent,
      nzComponentParams: {
        mode: Mode.EDIT,
        data: data
      },
      nzFooter: footerTmpl
    });
    this.modal.afterClose.subscribe(result => result?.refresh ? this.doSearch(1) : '');
  }

  deleteItem(employeeProfileId: number) {
    this.documentAttachmentService.deleteDocumentAttachment(employeeProfileId).subscribe(res => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.doSearch(1);
        this.toastService.success(this.translate.instant('common.notification.deleteSuccess'));
      } else {
        this.toastService.error(res?.message);
      }
    }, () => {
      this.toastService.error(this.translate.instant('common.notification.deleteError'));
    });
  }

  searchData(value: string) {
    value = value.toLowerCase();
    this.dataTable = StringUtils.isNullOrEmpty(value) ? [...this.items] : this.items.filter(item => item.profileTypeName.toLowerCase().startsWith(value));
    this.dataTable = [...this.dataTable];
  }

  doDownloadAttach(file: NzSafeAny) {
    this.downloadFileAttachService.doDownloadAttachFileWithSecurity(~~file?.docId, file?.security).subscribe(res => {
      const reportFile = new Blob([res.body], {type: getTypeExport(file.fileName.split('.').pop())});
      saveAs(reportFile, file.fileName);
    });
  }

  refresh() {
    this.doSearch(1);
  }

}
