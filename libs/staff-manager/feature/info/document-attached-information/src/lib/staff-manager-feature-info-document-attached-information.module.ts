import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  DocumentAttachedInformationComponent
} from "./document-attached-information/document-attached-information.component";
import {NzTableModule} from "ng-zorro-antd/table";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzPopconfirmModule} from "ng-zorro-antd/popconfirm";
import {SharedUiMbTableMergeCellWrapModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell-wrap";
import {SharedUiMbTableMergeCellModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";

@NgModule({
  imports: [CommonModule, NzTableModule, TranslateModule, SharedUiMbButtonModule, NzPopconfirmModule, SharedUiMbTableMergeCellWrapModule, SharedUiMbTableMergeCellModule, SharedUiMbTableWrapModule, SharedUiMbTableModule],
  declarations: [DocumentAttachedInformationComponent],
  exports: [DocumentAttachedInformationComponent],
})
export class StaffManagerFeatureInfoDocumentAttachedInformationModule {}
