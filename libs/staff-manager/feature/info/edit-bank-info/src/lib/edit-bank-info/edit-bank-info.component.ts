import {Component, Input, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {NzModalRef} from 'ng-zorro-antd/modal';
import {BankInfo, CatBankInfo, LookupValues} from "@hcm-mfe/staff-manager/data-access/models/info";
import {BankInfoService, StaffInfoService} from "@hcm-mfe/staff-manager/data-access/services";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";
import {Constant} from "@hcm-mfe/staff-manager/data-access/common";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";

@Component({
  selector: 'app-edit-bank-info',
  templateUrl: './edit-bank-info.component.html',
  styleUrls: ['./edit-bank-info.component.scss']
})
export class EditBankInfoComponent implements OnInit {
  @Input() data: NzSafeAny;
  form: FormGroup;
  banks = 'data';
  isSubmitted = false;

  bankType = 'accountTypeCode';
  accountNumber = 'accountNo';
  bankCode = 'bankId';
  branch = 'bankBranch';

  accountMain = 'isPaymentAccount';
  radioValue?: number;
  bankAccountId = 'bankAccountId';
  dataBankInfos: BankInfo[] = [];
  subs: Subscription[] = [];
  listBankInfo: CatBankInfo[] = [];
  listAccountType: LookupValues[] = [];
  listIdDelete: number[] = [];
  isLoadingPage = false;

  constructor(
    private fb: FormBuilder,
    private staffInfoService: StaffInfoService,
    private bankInfoService: BankInfoService,
    private toastService: ToastrService,
    private translate: TranslateService,
    private modalRef: NzModalRef
  ) {
    const formControl = new FormGroup({});
    formControl.addControl(this.banks, this.fb.array([this.createFormGroup(null, null)]));
    this.form = this.fb.group(formControl.controls);
  }

  ngOnInit(): void {
    this.dataBankInfos = this.data?.data;
    this.getBank();
    this.getAccountType();
    this.patchValue();
  }

  patchValue() {
    if (this.dataBankInfos && this.dataBankInfos.length > 0) {
      if (this.dataBankInfos[0].employeeId) {
        this.isLoadingPage = true;
        this.bankInfoService.getBankInfo(this.dataBankInfos[0].employeeId).subscribe((res: BaseResponse) => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.dataBankInfos = res?.data;
            const form = this.form.get(this.banks) as FormArray;
            if (this.dataBankInfos.length > 0) {
              form.clear();
              let idx: number;
              this.dataBankInfos.forEach((item: NzSafeAny, index) => {
                if (item[this.accountMain]) {
                  idx = index;
                  this.radioValue = idx;
                }
                form.push(this.createFormGroup(item, idx));
              });
            }
            this.form.valueChanges.subscribe(() => {
              this.configValue();
            });
          } else {
            this.toastService.error(res?.message);
          }
          this.isLoadingPage = false;
        }, () => {
          this.modalRef.close();
          this.isLoadingPage = false;
        })
      }
    }
  }

  getBank() {
    this.subs.push(
      this.staffInfoService.getBanks().subscribe(res => {
        const response: BaseResponse = res;
        this.listBankInfo = response.data;
      })
    );
  }

  getAccountType() {
    this.subs.push(
      this.staffInfoService.getCatalog(Constant.CATALOGS.LOAI_TAI_KHOAN).subscribe(res => {
        const response: BaseResponse = res;
        this.listAccountType = response.data;
      })
    );
  }

  createFormGroup(item: BankInfo | NzSafeAny, index: number | null): FormGroup {
    const formControl = new FormGroup({});
    formControl.addControl(this.bankType, new FormControl(item ? item[this.bankType] : null, Validators.required));
    formControl.addControl(this.accountNumber, new FormControl(item ? item[this.accountNumber] : null, [Validators.required, Validators.maxLength(20), Validators.pattern('^[0-9]{0,20}$')]));
    formControl.addControl(this.branch, new FormControl(item ? item[this.branch] : null, Validators.required));
    formControl.addControl(this.accountMain, new FormControl(item ? item[this.accountMain] : null));
    formControl.addControl(this.bankCode, new FormControl(item ? item[this.bankCode] : null, Validators.required));
    formControl.addControl(this.bankAccountId, new FormControl(item ? item[this.bankAccountId] : null));
    formControl.addControl('index', new FormControl(index ? index : null));
    return new FormGroup(formControl.controls);
  }

  configValue() {
    (this.form.value[this.banks] as Array<BankInfo>)?.forEach((item: NzSafeAny, j) => {
      if (j === item.index) {
        item[this.accountMain] = 1;
      } else {
        item[this.accountMain] = 0;
      }
    });
  }

  addFormArray() {
    this.isSubmitted = true;
    let isValid = true;
    const idx = this.form.value[this.banks][0]?.index;
    this.radioValue = idx;
    for (let i = 0; i < (this.form.get(this.banks) as FormArray).controls.length; i++) {
      if (!(this.form.get(this.banks) as FormArray).controls[i].valid) {
        isValid = false;
        break;
      }
    }
    if (isValid) {
      this.isSubmitted = false;
      const form = this.form.get(this.banks) as FormArray;
      form.push(this.createFormGroup(null, idx));
    }
  }

  save() {
    this.isSubmitted = true;
    const value = this.form.value;
    this.bindingErrorDuplicate(value);
    if (this.form.valid) {
      this.isLoadingPage = true;
      const body: { [key: string]: object } = {};
      body['idDelete'] = this.listIdDelete;
      body['data'] = value?.data;
      this.bankInfoService.saveBankInfo(this.data?.employeeId, body).subscribe(res => {
        if (res && res.code === 200) {
          this.toastService.success(this.translate.instant('common.notification.updateSuccess'));
          this.modalRef.close({ refresh: true });
        } else {
          this.toastService.error(this.translate.instant('common.notification.updateError') + ': ' + res?.message);
          this.modalRef.close({ refresh: false });
        }
        this.isLoadingPage = false;
      }, error => {
        this.toastService.error(this.translate.instant('common.notification.updateError') + ': ' + error.message);
        this.modalRef.close({ refresh: false });
        this.isLoadingPage = false;
      });
    }
  }

  bindingErrorDuplicate(value: NzSafeAny) {
    const arrayDataValid: NzSafeAny[] = [];
    const arraySalaryAccountDupl: NzSafeAny[] = [];
    const salaryAccountTypeCode = this.listAccountType.find((item) => {
      if(item && item.value) {
        return item.value === Constant.SALARY_ACCOUNT_CODE;
      }
      return null;
    })?.value;
    value?.data.forEach((item: NzSafeAny, i: number) => {
      const indexOf = arrayDataValid.indexOf(item[this.bankCode] + '_' + item[this.accountNumber]);
      let indexOfSalaryAccountType = -1;
      if (item[this.bankType] === salaryAccountTypeCode) {
        indexOfSalaryAccountType = arraySalaryAccountDupl.indexOf(item[this.bankType]);  // loai tk la tk luong
      }
      if (indexOf === -1) {
        arrayDataValid.push(item[this.bankCode] + '_' + item[this.accountNumber]);
        ((this.form.get(this.banks) as FormArray).controls[i] as FormGroup).controls[this.accountNumber]?.setErrors(null);
      } else {
        ((this.form.get(this.banks) as FormArray).controls[indexOf] as FormGroup).controls[this.accountNumber]?.setErrors({ 'accDuplicate': true });
        ((this.form.get(this.banks) as FormArray).controls[i] as FormGroup).controls[this.accountNumber]?.setErrors({ 'accDuplicate': true });
      }

      if (indexOfSalaryAccountType === -1) {
        arraySalaryAccountDupl.push(item[this.bankType]);
        ((this.form.get(this.banks) as FormArray).controls[i] as FormGroup).controls[this.bankType]?.setErrors(null);
      } else {
        ((this.form.get(this.banks) as FormArray).controls[indexOfSalaryAccountType] as FormGroup).controls[this.bankType]?.setErrors({ 'salaryAccountTypeDuplicate': true });
        ((this.form.get(this.banks) as FormArray).controls[i] as FormGroup).controls[this.bankType]?.setErrors({ 'salaryAccountTypeDuplicate': true });
      }

    });
  }

  deleteItem(data: BankInfo, index: number) {
    const banks = this.form.get(this.banks) as FormArray;
    const idx = this.form.value[this.banks][0]?.index;
    this.radioValue = idx;
    (this.form.value[this.banks] as Array<BankInfo>).forEach((item: NzSafeAny) => {
      if (index < item.index) {
        item.index = idx - 1;
      }
    });
    if (banks.length > 1) {
      banks.removeAt(index);
      if (data.bankAccountId) {
        this.listIdDelete.push(data.bankAccountId);
      }
    } else {
      banks.reset();
    }
  }

  checkDuplicate() {
    if (this.isSubmitted) {
      this.bindingErrorDuplicate(this.form.value);
    }
  }
}
