import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EditBankInfoComponent} from "./edit-bank-info/edit-bank-info.component";
import {ReactiveFormsModule} from "@angular/forms";
import {NzTableModule} from "ng-zorro-antd/table";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {NzRadioModule} from "ng-zorro-antd/radio";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzPopconfirmModule} from "ng-zorro-antd/popconfirm";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
    imports: [CommonModule, ReactiveFormsModule, NzTableModule, TranslateModule, SharedUiMbSelectModule, SharedUiMbInputTextModule, NzRadioModule, SharedUiMbButtonModule, NzPopconfirmModule, SharedUiLoadingModule],
  declarations: [EditBankInfoComponent],
  exports: [EditBankInfoComponent],
})
export class StaffManagerFeatureInfoEditBankInfoModule {}
