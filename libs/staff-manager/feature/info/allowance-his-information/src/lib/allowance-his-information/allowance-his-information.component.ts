import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ToastrService} from "ngx-toastr";
import {NzModalRef, NzModalService} from "ng-zorro-antd/modal";
import {TranslateService} from "@ngx-translate/core";
import {HTTP_STATUS_CODE, Mode} from "@hcm-mfe/shared/common/constants";
import {AllowanceHisInfoService, ShareDataService,} from "@hcm-mfe/staff-manager/data-access/services";
import {AppFunction, BaseResponse, MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {AllowanceHistory} from "@hcm-mfe/staff-manager/data-access/models/info";
import {ValidateService} from "@hcm-mfe/shared/core";
import {SessionService} from "@hcm-mfe/shared/common/store";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {AddAllowanceHisComponent} from "@hcm-mfe/staff-manager/feature/info/add-allowance-his";
import {NzSafeAny} from "ng-zorro-antd/core/types";

@Component({
  selector: 'app-allowance-his-information',
  templateUrl: './allowance-his-information.component.html',
  styleUrls: ['./allowance-his-information.component.scss'],
})
export class AllowanceHisInformationComponent implements OnInit {
  objFunction: AppFunction;
  tableConfig!: MBTableConfig;
  dataTable: AllowanceHistory[] = [];
  employeeId: number | null = null;
  modal?: NzModalRef;
  pagination = new Pagination();

  @ViewChild('actionTmpl', {static: true}) actionTmpl!: TemplateRef<NzSafeAny>;

  constructor(
    public validateService: ValidateService,
    private http: HttpClient,
    private toastService: ToastrService,
    private modalService: NzModalService,
    private translate: TranslateService,
    private shareDataService: ShareDataService,
    private allowanceHisInfoService: AllowanceHisInfoService,
    private sessionService: SessionService,
  ) {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.HR_ALLOWANCE_PROCESS}`);
  }

  ngOnInit(): void {
    this.shareDataService.employee$.subscribe(employee => {
      this.employeeId = employee.employeeId;
      this.initTable();
      this.doSearch(1);
    })
  }

  doSearch(pageNumber: number) {
    this.tableConfig = {...this.tableConfig, loading: true};
    this.pagination.pageNumber = pageNumber;
    const param = this.pagination.getCurrentPage();
    this.allowanceHisInfoService.getList(this.employeeId, param).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.dataTable = res.data.listData;
        this.tableConfig.total = res.data.count;
      } else {
        this.toastService.error(res?.message);
      }
      this.tableConfig = { ...this.tableConfig, loading: false };
    }, () => {
      this.tableConfig = { ...this.tableConfig, loading: false };
      this.dataTable = [];
    })
  }

  initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'staffManager.table.fromDate',
          field: 'fromDate',
          width: 100,
          thClassList: ['text-center'],
          tdClassList: ['text-center']
        },
        {
          title: 'staffManager.table.toDate',
          field: 'toDate',
          width: 100,
          thClassList: ['text-center'],
          tdClassList: ['text-center']
        },
        {
          title: 'staffManager.table.allowanceType',
          field: 'allowanceTypeName',
        },
        {
          title: 'staffManager.table.benefitRate',
          pipe: 'currency: VND',
          field: 'amountMoney',
        },
        {
          title: 'staffManager.label.note',
          field: 'note',
          show: false
        },
        {
          title: ' ',
          field: 'action',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          fixed: true,
          fixedDir: "right",
          tdTemplate: this.actionTmpl,
          width: 80,
          show: this.objFunction?.delete || this.objFunction?.edit
        }
      ],
      total: 0,
      needScroll: false,
      loading: true,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  showModalUpdate(allowanceProcessId: number, footerTmpl: TemplateRef<Record<string, never>>) {
    const data = this.dataTable.find(item => item.allowanceProcessId === allowanceProcessId);
    this.modal = this.modalService.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
      nzTitle: this.translate.instant('staffManager.allowanceHisInformation.modal.update'),
      nzContent: AddAllowanceHisComponent,
      nzComponentParams: {
        mode: Mode.EDIT,
        data: data
      },
      nzFooter: footerTmpl
    });
    this.modal.afterClose.subscribe(result => result?.refresh ? this.doSearch(1): '');
  }

  deleteItem(allowanceProcessId: number) {
    this.allowanceHisInfoService.deleteRecord(allowanceProcessId).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.doSearch(1);
        this.toastService.success(this.translate.instant('common.notification.deleteSuccess'));
      } else {
        this.toastService.error(res?.message);
      }
    })
  }

  refresh() {
    this.doSearch(1);
  }

}
