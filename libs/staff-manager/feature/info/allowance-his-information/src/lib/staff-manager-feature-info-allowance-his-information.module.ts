import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AllowanceHisInformationComponent} from "./allowance-his-information/allowance-his-information.component";
import {NzTableModule} from "ng-zorro-antd/table";
import {TranslateModule} from "@ngx-translate/core";
import {SharedPipesFormatCurrencyModule} from "@hcm-mfe/shared/pipes/format-currency";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzPopconfirmModule} from "ng-zorro-antd/popconfirm";
import {SharedUiMbTableMergeCellWrapModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell-wrap";
import {SharedUiMbTableMergeCellModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";

@NgModule({
  imports: [CommonModule, NzTableModule, TranslateModule, SharedPipesFormatCurrencyModule, SharedUiMbButtonModule, NzPopconfirmModule, SharedUiMbTableMergeCellWrapModule, SharedUiMbTableMergeCellModule, SharedUiMbTableWrapModule, SharedUiMbTableModule],
  declarations: [AllowanceHisInformationComponent],
  exports: [AllowanceHisInformationComponent],
})
export class StaffManagerFeatureInfoAllowanceHisInformationModule {}
