import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Subscription} from "rxjs";
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";
import {NzModalRef} from "ng-zorro-antd/modal";
import * as moment from 'moment';
import {LookupValues, PartyInfo} from "@hcm-mfe/staff-manager/data-access/models/info";
import {PartyInfoService, StaffInfoService} from "@hcm-mfe/staff-manager/data-access/services";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";
import {Constant} from "@hcm-mfe/staff-manager/data-access/common";
import {DateValidator} from "@hcm-mfe/shared/common/validators";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";


@Component({
  selector: 'app-edit-party-info',
  templateUrl: './edit-party-info.component.html',
  styleUrls: ['./edit-party-info.component.scss']
})
export class EditPartyInfoComponent implements OnInit, OnDestroy {
  @Input() data: NzSafeAny;
  form: FormGroup;
  armyLevels: LookupValues[] = [];
  subs: Subscription[] = [];
  partyInfo: PartyInfo | NzSafeAny;
  isSubmitted = false;
  isLoadingPage = false;

  constructor(
    private fb: FormBuilder,
    private staffInfoService: StaffInfoService,
    private partyInfoService: PartyInfoService,
    private toastService: ToastrService,
    private translate: TranslateService,
    private modalRef: NzModalRef
  ) {
    this.form = this.fb.group({
      partyDate: null,
      partyPlace: [null, Validators.maxLength(200)],
      partyOfficialDate: null,
      armyJoinDate: null,
      armyLevelCode: null,
    },{
      validators: [
        DateValidator.validateRangeDate('partyDate', 'partyOfficialDate', 'rangeDateError')
      ]
    });
  }

  ngOnInit(): void {
    this.partyInfo = this.data?.data;
    this.getCatalogs();
    this.patchValue();
  }

  ngOnDestroy() {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      this.isLoadingPage = true;
      const value = this.form.value;
      value.partyDate = value.partyDate ? moment(value.partyDate).format('DD/MM/YYYY') : '';
      value.partyOfficialDate = value.partyOfficialDate ? moment(value.partyOfficialDate).format('DD/MM/YYYY') : '';
      value.armyJoinDate = value.armyJoinDate ? moment(value.armyJoinDate).format('DD/MM/YYYY') : '';
      this.partyInfoService.savePartyInfo(this.data?.employeeId, value).subscribe(res => {
        if (res && res.code === 200) {
          this.toastService.success(this.translate.instant('common.notification.updateSuccess'));
          this.modalRef.close({refresh: true});
        } else {
          this.toastService.error(this.translate.instant('common.notification.updateError') + ': ' + res?.message);
          this.modalRef.close({refresh: false});
        }
        this.isLoadingPage = false;
      }, error =>  {
        this.toastService.error(this.translate.instant('common.notification.updateError') + ': ' + error?.message);
        this.modalRef.close({refresh: false});
        this.isLoadingPage = false;
      });
    }
  }

  patchValue() {
    this.isLoadingPage = true;
    this.partyInfoService.getPartyInfo(this.partyInfo.employeeId).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.partyInfo = res?.data;
        this.form.patchValue({
          partyDate: this.partyInfo.partyDate ? moment(this.partyInfo.partyDate, 'DD/MM/YYYY').toDate() : null,
          partyPlace: this.partyInfo.partyPlace,
          partyOfficialDate: this.partyInfo.partyOfficialDate ? moment(this.partyInfo.partyOfficialDate, 'DD/MM/YYYY').toDate() : null,
          armyJoinDate: this.partyInfo.armyJoinDate ? moment(this.partyInfo.armyJoinDate, 'DD/MM/YYYY').toDate() : null,
          armyLevelCode: this.partyInfo.armyLevelCode,
        });
      } else {
        this.toastService.error(res?.message);
      }
      this.isLoadingPage = false;
    }, () => {
      this.modalRef.close();
      this.isLoadingPage = false;
    })
  }

  getCatalogs() {
    this.getArmyLevels();
  }

  getArmyLevels() {
    this.subs.push(
      this.staffInfoService.getCatalog(Constant.CATALOGS.CAP_BAC_QUAN_HAM).subscribe(res => {
        const response: BaseResponse = res;
        this.armyLevels = response.data;
      })
    );
  }

}
