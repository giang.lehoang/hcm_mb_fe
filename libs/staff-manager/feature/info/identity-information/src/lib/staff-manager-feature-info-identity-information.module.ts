import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {IdentityInformationComponent} from "./identity-information/identity-information.component";
import {SharedUiMbTextLabelModule} from "@hcm-mfe/shared/ui/mb-text-label";
import {NzFormModule} from "ng-zorro-antd/form";
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  imports: [CommonModule, SharedUiMbTextLabelModule, NzFormModule, TranslateModule],
  declarations: [IdentityInformationComponent],
  exports: [IdentityInformationComponent],
})
export class StaffManagerFeatureInfoIdentityInformationModule {}
