import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {AppFunction} from "@hcm-mfe/shared/data-access/models";
import {BaseResponse, IdentityInfo, IdentityInfoGroup} from "@hcm-mfe/staff-manager/data-access/models/info";
import {IdentityInfoService, ShareDataService} from "@hcm-mfe/staff-manager/data-access/services";
import { SessionService } from "@hcm-mfe/shared/common/store";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";

@Component({
  selector: 'app-identity-information',
  templateUrl: './identity-information.component.html',
  styleUrls: ['./identity-information.component.scss']
})
export class IdentityInformationComponent implements OnInit, OnDestroy {
  objFunction: AppFunction;
  items: IdentityInfoGroup | any;
  subs: Subscription[] = [];
  response: BaseResponse = new BaseResponse();
  identities: IdentityInfo[] = [];
  identityMains: IdentityInfo[] = [];
  employeeId: number = 1;

  constructor(
    private shareDataService: ShareDataService,
    private identityInfoService: IdentityInfoService,
    private sessionService: SessionService,
    ) {
      this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.HR_ALLOWANCE_PROCESS}`);
    }

  ngOnInit(): void {
    this.subs.push(
      this.shareDataService.employee$.subscribe(employee => {
        this.employeeId = employee.employeeId;
        this.getIdentityInfo(this.employeeId);
      })
    );
  }

  ngOnDestroy() {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  getIdentityInfo(employeeId: number) {
    this.subs.push(
      this.identityInfoService.getIdentityInfo(employeeId).subscribe(res => {
        this.response = res;
        this.identities = this.response?.data;
        this.identityMains = this.identities?.filter(item => item.isMain === 1);
        this.items = {employeeId: employeeId, data: this.response?.data};
        this.shareDataService.changeIdentityInfo(this.response?.data);
      })
    );
  }

  refresh() {
    this.getIdentityInfo(this.employeeId);
  }

}
