import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ToastrService} from "ngx-toastr";
import {NzModalRef, NzModalService} from "ng-zorro-antd/modal";
import {TranslateService} from "@ngx-translate/core";
import {AppFunction, BaseResponse, MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {RewardHistory} from "@hcm-mfe/staff-manager/data-access/models/info";
import {RewardHisInfoService, ShareDataService} from "@hcm-mfe/staff-manager/data-access/services";
import {SessionService} from "@hcm-mfe/shared/common/store";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {ValidateService} from "@hcm-mfe/shared/core";
import {HTTP_STATUS_CODE, Mode} from "@hcm-mfe/shared/common/constants";
import {AddRewardHisComponent} from "@hcm-mfe/staff-manager/feature/info/add-reward-his";
import {NzSafeAny} from "ng-zorro-antd/core/types";

@Component({
  selector: 'app-reward-his-information',
  templateUrl: './reward-his-information.component.html',
  styleUrls: ['./reward-his-information.component.scss'],
})
export class RewardHisInformationComponent implements OnInit {
  objFunction: AppFunction;
  tableConfig!: MBTableConfig;
  dataTable: RewardHistory[] = [];
  employeeId: number | null = null;
  modal?: NzModalRef;
  pagination = new Pagination();

  @ViewChild('actionTmpl', { static: true }) actionTmpl!: TemplateRef<NzSafeAny>;

  constructor(
    public validateService: ValidateService,
    private http: HttpClient,
    private toastService: ToastrService,
    private modalService: NzModalService,
    private translate: TranslateService,
    private shareDataService: ShareDataService,
    private rewardHisInfoService: RewardHisInfoService,
    private sessionService: SessionService,
    ) {
      this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.HR_REWARD}`);
    }

  ngOnInit(): void {
    this.shareDataService.employee$.subscribe(employee => {
      this.employeeId = employee.employeeId;
      this.initTable();
      this.doSearch(1);
    })
  }

  doSearch(pageNumber: number) {
    this.tableConfig = { ...this.tableConfig, loading: true };
    this.pagination.pageNumber = pageNumber;
    const param = this.pagination.getCurrentPage();
    this.rewardHisInfoService.getList(this.employeeId, param).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.dataTable = res.data.listData;
        this.tableConfig.total = res.data.count;
      } else {
        this.toastService.error(res?.message);
      }
      this.tableConfig = { ...this.tableConfig, loading: false };
    }, () => {
      this.tableConfig = { ...this.tableConfig, loading: false };
      this.dataTable = [];
    })
  }

  initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'staffManager.table.year',
          field: 'year',
          width: 150,
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        },
        {
          title: 'staffManager.table.decisionDay',
          field: 'signedDate',
          width: 100,
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        },
        {
          title: 'staffManager.table.rewardForm',
          field: 'methodName',
          width: 150,
        },
        {
          title: 'staffManager.table.rewardTitle',
          field: 'titleName',
          width: 150,
        },
        {
          title: 'staffManager.table.effectiveDate',
          field: 'fromDate',
          width: 100,
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        },
        {
          title: 'staffManager.table.documentNumber',
          field: 'documentNumber',
          width: 150,
        },
        {
          title: 'staffManager.table.reason',
          field: 'reason',
          width: 150,
        },
        {
          title: 'staffManager.table.decisionLevel',
          field: 'decisionLevelName',
          width: 150,
        },
        {
          title: 'staffManager.table.signer',
          field: 'signer',
          width: 150,
        },
        {
          title: 'staffManager.table.signerPosition',
          field: 'signerPosition',
          width: 150,
        },
        {
          title: 'staffManager.label.note',
          field: 'note',
          width: 150,
          show: false
        },
        {
          title: ' ',
          field: 'action',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          fixed: true,
          fixedDir: "right",
          tdTemplate: this.actionTmpl,
          width: 80,
          show: this.objFunction?.delete || this.objFunction?.edit
        }
      ],
      total: 0,
      needScroll: true,
      loading: true,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  showModalUpdate(rewardRecordId: number, footerTmpl: TemplateRef<Record<string, never>>) {
    const data = this.dataTable.find(item => item.rewardRecordId === rewardRecordId);
    this.modal = this.modalService.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
      nzTitle: this.translate.instant('staffManager.rewardHisInformation.modal.update'),
      nzContent: AddRewardHisComponent,
      nzComponentParams: {
        mode: Mode.EDIT,
        data: data
      },
      nzFooter: footerTmpl
    });
    this.modal.afterClose.subscribe(result => result?.refresh ? this.doSearch(1): '')
  }

  deleteItem(rewardRecordId: number) {
    this.rewardHisInfoService.deleteRecord(rewardRecordId).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.doSearch(1);
        this.toastService.success(this.translate.instant('common.notification.deleteSuccess'));
      } else {
        this.toastService.error(res?.message);
      }
    })
  }

  refresh() {
    this.doSearch(1);
  }

}
