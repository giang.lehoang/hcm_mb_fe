import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ContractHisInformationComponent} from "./contract-his-information/contract-his-information.component";
import {NzTableModule} from "ng-zorro-antd/table";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzPopconfirmModule} from "ng-zorro-antd/popconfirm";
import {NzTagModule} from "ng-zorro-antd/tag";
import {SharedUiMbTableMergeCellWrapModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell-wrap";
import {SharedUiMbTableMergeCellModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";

@NgModule({
  imports: [CommonModule, NzTableModule, TranslateModule, SharedUiMbButtonModule, NzPopconfirmModule, NzTagModule, SharedUiMbTableMergeCellWrapModule, SharedUiMbTableMergeCellModule, SharedUiMbTableWrapModule, SharedUiMbTableModule],
  declarations: [ContractHisInformationComponent],
  exports: [ContractHisInformationComponent],
})
export class StaffManagerFeatureInfoContractHisInformationModule {}
