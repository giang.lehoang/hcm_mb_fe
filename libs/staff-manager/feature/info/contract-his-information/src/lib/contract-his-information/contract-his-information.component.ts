import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ToastrService} from "ngx-toastr";
import {NzModalRef, NzModalService} from "ng-zorro-antd/modal";
import {TranslateService} from "@ngx-translate/core";
import {saveAs} from 'file-saver';
import {
  ContractHisInfoService,
  DownloadFileAttachService,
  ShareDataService,
} from "@hcm-mfe/staff-manager/data-access/services";
import {AppFunction, BaseResponse, MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {SessionService} from "@hcm-mfe/shared/common/store";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {ContractHistory} from "@hcm-mfe/staff-manager/data-access/models/info";
import {ValidateService} from "@hcm-mfe/shared/core";
import {HTTP_STATUS_CODE, Mode} from "@hcm-mfe/shared/common/constants";
import {AddContractHisInfoComponent} from "@hcm-mfe/staff-manager/feature/info/add-contract-his-info";
import {getTypeExport} from "@hcm-mfe/shared/common/utils";
import {Constant} from "@hcm-mfe/staff-manager/data-access/common";
import {NzSafeAny} from "ng-zorro-antd/core/types";


@Component({
  selector: 'app-contract-his-information',
  templateUrl: './contract-his-information.component.html',
  styleUrls: ['./contract-his-information.component.scss'],
})
export class ContractHisInformationComponent implements OnInit {

  objFunction: AppFunction;
  tableConfig!: MBTableConfig;
  dataTable: ContractHistory[] = [];
  employeeId: number | null = null;
  modal?: NzModalRef;
  constant = Constant;
  pagination = new Pagination();

  @ViewChild('actionTmpl', { static: true }) actionTmpl!: TemplateRef<NzSafeAny>;
  @ViewChild('statusTmpl', { static: true }) statusTmpl!: TemplateRef<NzSafeAny>;
  @ViewChild('fileTmpl', { static: true }) fileTmpl!: TemplateRef<NzSafeAny>;

  constructor(
    public validateService: ValidateService,
    private http: HttpClient,
    private toastService: ToastrService,
    private modalService: NzModalService,
    private translate: TranslateService,
    private shareDataService: ShareDataService,
    private contractHisInfoService: ContractHisInfoService,
    private downloadFileAttachService: DownloadFileAttachService,
    private sessionService: SessionService,
  ) {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.HR_CONTRACT_PROCESS}`);
  }

  ngOnInit(): void {
    this.shareDataService.employee$.subscribe(employee => {
      this.employeeId = employee.employeeId;
      this.initTable();
      this.doSearch(1);
    })
  }

  initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'staffManager.table.effectiveDate',
          field: 'fromDate',
          width: 100,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
        },
        {
          title: 'staffManager.table.expirationDate',
          field: 'toDate',
          width: 100,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
        },
        {
          title: 'staffManager.table.signDay',
          field: 'signedDate',
          width: 100,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
        },
        {
          title: 'staffManager.table.classifyName',
          field: 'classifyName',
          width: 150,
        },
        {
          title: 'staffManager.table.contractType',
          field: 'contractTypeName',
          width: 150,
        },
        {
          title: 'staffManager.table.contractNumber',
          field: 'contractNumber',
          width: 150,
        },
        {
          title: 'staffManager.table.signer',
          field: 'signerName',
          width: 150,
        },
        {
          title: 'staffManager.table.signerPosition',
          field: 'signerPosition',
          width: 150,
        },
        {
          title: 'staffManager.contractHisInfo.label.attributes.note',
          field: 'note',
          width: 150,
          show: false
        },
        {
          title: 'staffManager.table.fileAttach',
          tdTemplate: this.fileTmpl,
          width: 130,
        },
        {
          title: 'staffManager.table.contractStatus',
          fixed: true,
          fixedDir: "right",
          width: 130,
          tdTemplate: this.statusTmpl
        },
        {
          title: ' ',
          field: 'action',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          fixed: true,
          fixedDir: "right",
          tdTemplate: this.actionTmpl,
          width: 80,
          show: this.objFunction?.delete || this.objFunction?.edit
        }
      ],
      total: 0,
      needScroll: true,
      loading: true,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  doSearch(pageNumber: number) {
    this.tableConfig = { ...this.tableConfig, loading: true };
    this.pagination.pageNumber = pageNumber;
    const param = this.pagination.getCurrentPage();
    this.contractHisInfoService.getList(this.employeeId, param).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.dataTable = res.data.listData;
        this.tableConfig.total = res.data.count;
      } else {
        this.toastService.error(res?.message);
      }
      this.tableConfig = { ...this.tableConfig, loading: false };
    }, () => {
      this.tableConfig = { ...this.tableConfig, loading: false };
      this.dataTable = [];
    })
  }

  showModalUpdate(contractProcessId: number, draftContractProcessId: number, footerTmpl: TemplateRef<Record<string, never>>) {
    const data = this.getRecord(contractProcessId, draftContractProcessId);
    this.modal = this.modalService.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
      nzTitle: this.translate.instant('staffManager.contractHisInfo.modal.update'),
      nzContent: AddContractHisInfoComponent,
      nzComponentParams: {
        mode: Mode.EDIT,
        data: data
      },
      nzFooter: footerTmpl
    });
    this.modal.afterClose.subscribe(result => result?.refresh ? this.doSearch(1): '');
  }

  getRecord(contractProcessId: number, draftContractProcessId: number): ContractHistory | undefined {
    if (contractProcessId) {
      return this.dataTable.find(item => item.contractProcessId === contractProcessId);
    }
    return this.dataTable.find(item => item.draftContractProcessId === draftContractProcessId);
  }

  deleteItem(contractProcessId: number, draftContractProcessId: number) {
    if (contractProcessId && contractProcessId > 0) { // xoa trong bang chinh
      this.contractHisInfoService.deleteRecord(contractProcessId).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.doSearch(1);
          this.toastService.success(this.translate.instant('common.notification.deleteSuccess'));
        } else {
          this.toastService.error(res?.message);
        }
      })
    } else { // xoa trong draft
      this.contractHisInfoService.deleteRecordInDraft(draftContractProcessId).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.doSearch(1);
          this.toastService.success(this.translate.instant('common.notification.deleteSuccess'));
        } else {
          this.toastService.error(res?.message);
        }
      })
    }
  }

  doDownloadAttach(file: NzSafeAny) {
    this.downloadFileAttachService.doDownloadAttachFileWithSecurity(file?.docId, file?.security).subscribe(res => {
      const reportFile = new Blob([res.body], {type: getTypeExport(file.fileName.split(".").pop())});
      saveAs(reportFile, file.fileName);
    });
  }

  getStatus(param: NzSafeAny) {
    return Constant.APPROVAL_STATUSES.find(item => item.value === param)?.label;
  }

  refresh() {
    this.doSearch(1);
  }

}
