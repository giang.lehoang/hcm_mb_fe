import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AddEduHisInfoComponent} from "./add-edu-his-info/add-edu-his-info.component";
import {NzFormModule} from "ng-zorro-antd/form";
import {ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {SharedDirectivesNumberInputModule} from "@hcm-mfe/shared/directives/number-input";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
    imports: [CommonModule, NzFormModule, ReactiveFormsModule, SharedUiMbInputTextModule, TranslateModule, SharedUiMbSelectModule, SharedUiMbDatePickerModule, SharedDirectivesNumberInputModule, SharedUiLoadingModule],
  declarations: [AddEduHisInfoComponent],
  exports: [AddEduHisInfoComponent],
})
export class StaffManagerFeatureInfoAddEduHisInfoModule {}
