import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { ToastrService } from 'ngx-toastr';
import {Subscription} from "rxjs";
import {
  EduHisInfoService,
  ShareDataService, StaffInfoService,
} from "@hcm-mfe/staff-manager/data-access/services";
import {Constant} from "@hcm-mfe/staff-manager/data-access/common";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";
import {EduHisInfo, LookupValues} from "@hcm-mfe/staff-manager/data-access/models/info";
import {NzSafeAny} from "ng-zorro-antd/core/types";

@Component({
  selector: 'app-add-edu-his-info',
  templateUrl: './add-edu-his-info.component.html',
  styleUrls: ['./add-edu-his-info.component.scss']
})
export class AddEduHisInfoComponent implements OnInit, OnDestroy {
  @Input() data: NzSafeAny;
  isSubmitted = false;
  form: FormGroup;
  subs: Subscription[] = [];
  listEduMethodType: LookupValues[] = [];
  isLoadingPage = false;

  constructor(
    private fb: FormBuilder,
    private eduHisInfoService: EduHisInfoService,
    private staffInfoService: StaffInfoService,
    private toastService: ToastrService,
    private translate: TranslateService,
    private shareDataService: ShareDataService,
    private modalRef: NzModalRef
  ) {
    this.form = this.fb.group({
      educationProcessId: null,
      employeeId: null,
      courseName: [null, [Validators.required, Validators.maxLength(200)]],
      eduMethodTypeCode: null,
      courseContent: null,
      fromDate: null,
      toDate: null,
      result: null,
      refundAmount: null,
      commitmentDate: null,
      note: null
    });
  }

  ngOnInit(): void {
    this.getListEduMethodType();
    this.pathValue();
  }

  getListEduMethodType() {
    this.subs.push(
      this.staffInfoService.getCatalog(Constant.CATALOGS.HINHTHUC_DAOTAO).subscribe(res => {
        const response: BaseResponse = res;
        this.listEduMethodType = response?.data;
      })
    );
  }

  ngOnDestroy() {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  pathValue() {
    if (this.data) {
      this.isLoadingPage = true;
      this.subs.push(
        this.eduHisInfoService.getEduHisInfoDetail(this.data?.educationProcessId).subscribe( res => {
          const response: BaseResponse = res;
          const eduHisInfo: EduHisInfo = response?.data;
          this.form.patchValue({
            educationProcessId: eduHisInfo?.educationProcessId,
            employeeId: eduHisInfo?.employeeId,
            courseName: eduHisInfo?.courseName,
            eduMethodTypeCode: eduHisInfo?.eduMethodTypeCode,
            courseContent: eduHisInfo?.courseContent,
            fromDate: eduHisInfo?.fromDate ? moment(eduHisInfo?.fromDate, 'DD/MM/YYYY').toDate() : null,
            toDate: eduHisInfo?.toDate ? moment(eduHisInfo?.toDate, 'DD/MM/YYYY').toDate() : null,
            result: eduHisInfo?.result,
            refundAmount: eduHisInfo?.refundAmount,
            commitmentDate: eduHisInfo?.commitmentDate ? moment(eduHisInfo?.commitmentDate, 'DD/MM/YYYY').toDate() : null,
            note: eduHisInfo?.note ?? ""
          })
          this.isLoadingPage = false;
        })
      );
    }
  }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      this.isLoadingPage = true;
      const value = this.form.value;
      value.fromDate = value.fromDate ? moment(value.fromDate).format('DD/MM/YYYY') : null;
      value.toDate = value.toDate ? moment(value.toDate).format('DD/MM/YYYY') : null;
      value.commitmentDate = value.commitmentDate ? moment(value.commitmentDate).format('DD/MM/YYYY') : null;
      value.employeeId = value.employeeId ? value.employeeId : this.shareDataService.getEmployee().employeeId;
      this.eduHisInfoService.saveEduHisInfo(value).subscribe(res => {
        if (res && res.code === 200) {
          this.toastService.success(this.translate.instant('common.notification.updateSuccess'));
          this.modalRef.close({refresh: true});
        } else {
          this.toastService.error(this.translate.instant('common.notification.updateError') + ': ' + res?.message);
          this.modalRef.close({refresh: false});
        }
        this.isLoadingPage = false;
      }, () => {
        this.toastService.error(this.translate.instant('common.notification.updateError'));
        this.modalRef.close({refresh: false});
        this.isLoadingPage = false;
      });
    }
  }
}
