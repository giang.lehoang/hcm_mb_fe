import {Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {Subscription} from "rxjs";
import {NzModalRef, NzModalService} from "ng-zorro-antd/modal";
import {TranslateService} from "@ngx-translate/core";
import {ToastrService} from "ngx-toastr";
import {
  DegreeInfoService,
  DownloadFileAttachService,
  ShareDataService,
} from "@hcm-mfe/staff-manager/data-access/services";
import {AppFunction, MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {SessionService} from "@hcm-mfe/shared/common/store";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {BaseResponse, DegreeInfo} from "@hcm-mfe/staff-manager/data-access/models/info";
import {ValidateService} from "@hcm-mfe/shared/core";
import {AddDegreeInfoComponent} from "@hcm-mfe/staff-manager/feature/info/add-degree-info";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {saveAs} from "file-saver";
import {getTypeExport} from "@hcm-mfe/shared/common/utils";

@Component({
  selector: 'app-degree-information',
  templateUrl: './degree-information.component.html',
  styleUrls: ['./degree-information.component.scss']
})
export class DegreeInformationComponent implements OnInit, OnDestroy {
  objFunction: AppFunction;
  tableConfig!: MBTableConfig;
  dataTable: DegreeInfo[] = [];
  employeeId: number | null = null;
  modal?: NzModalRef;
  pagination = new Pagination();
  subs: Subscription[] = [];

  @ViewChild('actionTmpl', {static: true}) actionTmpl!: TemplateRef<NzSafeAny>;
  @ViewChild('fileTmpl', { static: true }) fileTmpl!: TemplateRef<NzSafeAny>;

  constructor(
    public validateService: ValidateService,
    private degreeInfoService: DegreeInfoService,
    private modalService: NzModalService,
    private translate: TranslateService,
    private toastService: ToastrService,
    private shareDataService: ShareDataService,
    private sessionService: SessionService,
    private downloadFileAttachService: DownloadFileAttachService,
  ) {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.HR_EDU_DEGREE}`);
  }

  ngOnInit(): void {
    this.subs.push(
      this.shareDataService.employee$.subscribe(employee => {
        this.employeeId = employee.employeeId;
        this.initTable();
        this.doSearch(1);
      })
    );
  }

  doSearch(pageNumber: number) {
    this.tableConfig = {...this.tableConfig, loading: true};
    this.pagination.pageNumber = pageNumber;
    const param = this.pagination.getCurrentPage();
    if (this.employeeId) {
      this.subs.push(
        this.degreeInfoService.getDegreeInfos(this.employeeId, param).subscribe(res => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.dataTable = res.data.listData.map((item: DegreeInfo) => {
              if (item.isHighest === 1) {
                item.isHighestStr = 'X';
              }
              return item;
            });
            this.tableConfig.total = res.data.count;
          } else {
            this.toastService.error(res?.message);
          }
          this.tableConfig = { ...this.tableConfig, loading: false };
        }, () => {
          this.tableConfig = { ...this.tableConfig, loading: false };
          this.dataTable = [];
        })
      )
    }
  }

  initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'staffManager.table.degreeType',
          field: 'degreeTypeName',
          width: 150,
        },
        {
          title: 'staffManager.table.eduSchool',
          field: 'schoolName',
          width: 150,
        },
        {
          title: 'staffManager.table.level',
          field: 'majorLevelName',
          width: 150,
        },
        {
          title: 'staffManager.table.specialized',
          field: 'facultyName',
          width: 150,
        },
        {
          title: 'staffManager.table.graduationGrade',
          field: 'eduRankName',
          width: 150,
        },
        {
          title: 'staffManager.table.yearC',
          field: 'issueYear',
          width: 150,
          show: false
        },
        {
          title: 'staffManager.table.isLevelMax',
          field: 'isHighestStr',
          width: 150,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          show: false
        },
        {
          title: 'staffManager.label.note',
          field: 'note',
          width: 150,
          show: false
        },
        {
          title: 'staffManager.label.file',
          tdTemplate: this.fileTmpl,
          width: 130,
          show: false
        },
        {
          title: ' ',
          field: 'action',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          fixed: true,
          fixedDir: "right",
          tdTemplate: this.actionTmpl,
          width: 80,
          show: this.objFunction?.delete || this.objFunction?.edit
        }
      ],
      total: 0,
      needScroll: false,
      loading: true,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  showModalUpdate(educationDegreeId: number, footerTmpl: TemplateRef<Record<string, never>>) {
    const data = this.dataTable.find(item => item.educationDegreeId === educationDegreeId);
    this.modal = this.modalService.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 2 : window.innerWidth,
      nzTitle: this.translate.instant('staffManager.degreeInformation.modal.update'),
      nzContent: AddDegreeInfoComponent,
      nzComponentParams: {
        data: data
      },
      nzFooter: footerTmpl
    });
    this.modal.afterClose.subscribe(result => result?.refresh ? this.doSearch(1) : '');
  }

  deleteItem(educationDegreeId: number) {
    this.degreeInfoService.deleteDegreeInfo(educationDegreeId).subscribe((res: BaseResponse) => {
      if (res.code === 200) {
        this.doSearch(1);
        this.toastService.success(this.translate.instant('common.notification.deleteSuccess'));
      }
    })
  }

  doDownloadAttach(file: NzSafeAny) {
    this.downloadFileAttachService.doDownloadAttachFileWithSecurity(file?.docId, file?.security).subscribe(res => {
      const reportFile = new Blob([res.body], {type: getTypeExport(file.fileName.split(".").pop())});
      saveAs(reportFile, file.fileName);
    });
  }

  ngOnDestroy() {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  refresh() {
    this.doSearch(1);
  }

}
