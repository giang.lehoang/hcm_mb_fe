import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SideBarReportComponent } from './side-bar-report.component';

describe('SideBarReportComponent', () => {
  let component: SideBarReportComponent;
  let fixture: ComponentFixture<SideBarReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SideBarReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SideBarReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
