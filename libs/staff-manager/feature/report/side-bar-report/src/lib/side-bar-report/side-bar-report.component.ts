import {Component, OnInit} from '@angular/core';
import {Menus} from "@hcm-mfe/staff-manager/data-access/models/research";
import {NavigationEnd, Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {SessionService} from "@hcm-mfe/shared/common/store";
import {environment} from "@hcm-mfe/shared/environment";
import {Constant} from "@hcm-mfe/staff-manager/data-access/common";

@Component({
  selector: 'hcm-mfe-side-bar-report',
  templateUrl: './side-bar-report.component.html',
  styleUrls: ['./side-bar-report.component.scss']
})
export class SideBarReportComponent implements OnInit {

  currentUrl?: string;
  menus: Menus[] = [];
  openMenuMap: { [id: string]: boolean } = {};
  selectedMenu?: Menus;

  constructor(
    private route: Router,
    private http: HttpClient,
    public sessionService: SessionService,
  ) {
    this.getCurrentUrl(this.route);
  }

  ngOnInit(): void {
    this.getMenu();
  }

  getMenu() {
    this.menus = Constant.MENU_STAFF_REPORT;
    this.menus.forEach((item: any) => {
      if (item.childrens && item.childrens.length > 0) {
        item.childrens.forEach((el: Menus) => el.url.toLowerCase());
        item.childrens = item.childrens.filter((element: any) => this.checkScopeView(element.code));
      }
    })
    this.setOpenMenuMap(this.menus);
  }

  checkScopeView(resource: string): boolean {
    if (environment.isMbBank) {
      return this.sessionService.getSessionData(`FUNCTION_${resource}`)?.view;
    } else {
      return true;
    }
  }

  setOpenMenuMap(menus: Menus[]) {
    return menus.some(menu => {
      this.openMenuMap[menu.id] = false;
      if (menu.childrens && menu.childrens.length > 0) {
        this.openMenuMap[menu.id] = this.setOpenMenuMap(menu.childrens);
      }
      if (menu.url === this.currentUrl) {
        this.openMenuMap[menu.id] = true;
        return true;
      }
      return;
    });
  }

  getCurrentUrl(router: Router) {
    router.events.subscribe(e => {
      if (e instanceof NavigationEnd) {
        this.currentUrl = e.url;
      }
    });
  }

  openHandler(value: number): void {
    for (const key in this.openMenuMap) {
      if (key !== value.toString()) {
        this.openMenuMap[key] = false;
      }
    }
    this.setParentMenuOpen(value, this.menus);
  }

  setParentMenuOpen(value: number, menus: Menus[]) {
    return menus.some(menu => {
      this.openMenuMap[menu.id] = false;
      if (menu.childrens && menu.childrens.length > 0) {
        this.openMenuMap[menu.id] = this.setParentMenuOpen(value, menu.childrens);
      }
      if (menu.id === value) {
        this.openMenuMap[menu.id] = true;
        return true;
      }
      return;
    });
  }

}
