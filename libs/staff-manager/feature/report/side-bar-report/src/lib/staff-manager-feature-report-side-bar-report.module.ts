import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideBarReportComponent } from './side-bar-report/side-bar-report.component';
import {NzMenuModule} from "ng-zorro-antd/menu";
import {RouterModule} from "@angular/router";
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
    imports: [CommonModule, NzMenuModule, RouterModule, TranslateModule],
  declarations: [
    SideBarReportComponent
  ],
  exports: [
    SideBarReportComponent
  ]
})
export class StaffManagerFeatureReportSideBarReportModule {}
