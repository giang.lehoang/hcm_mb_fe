import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutReportComponent } from './layout-report/layout-report.component';
import {NzFormModule} from "ng-zorro-antd/form";
import {RouterModule} from "@angular/router";
import {StaffManagerFeatureReportSideBarReportModule} from "@hcm-mfe/staff-manager/feature/report/side-bar-report";

@NgModule({
  imports: [CommonModule, NzFormModule, RouterModule, StaffManagerFeatureReportSideBarReportModule],
  declarations: [
    LayoutReportComponent
  ],
  exports: [LayoutReportComponent]
})
export class StaffManagerFeatureReportLayoutReportModule {}
