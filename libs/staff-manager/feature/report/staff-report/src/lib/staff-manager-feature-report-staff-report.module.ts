import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {StaffReportComponent} from './staff-report/staff-report.component';
import {RouterModule} from "@angular/router";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzFormModule} from "ng-zorro-antd/form";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {TranslateModule} from "@ngx-translate/core";
import {ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {SharedUiMbSelectCheckAbleModule} from "@hcm-mfe/shared/ui/mb-select-check-able";
import {SharedUiOrgDataPickerModule} from "@hcm-mfe/shared/ui/org-data-picker";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";

@NgModule({
  imports: [CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: StaffReportComponent
      }
    ]), SharedUiMbButtonModule, NzFormModule, SharedUiMbInputTextModule, TranslateModule, ReactiveFormsModule, SharedUiMbDatePickerModule, SharedUiMbSelectCheckAbleModule, SharedUiOrgDataPickerModule, SharedUiLoadingModule, SharedUiMbSelectModule
  ],
  declarations: [
    StaffReportComponent
  ],
})
export class StaffManagerFeatureReportStaffReportModule {
}
