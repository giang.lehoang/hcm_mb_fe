import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {TranslateService} from "@ngx-translate/core";
import {ToastrService} from "ngx-toastr";
import {ActivatedRoute} from "@angular/router";
import {CatalogModel} from "@hcm-mfe/shared/data-access/models";
import {HttpParams} from "@angular/common/http";
import {Constant, ReportFunctionCode, UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {SearchFormService} from "@hcm-mfe/staff-manager/data-access/services";
import {HTTP_STATUS_CODE, MICRO_SERVICE, Scopes} from "@hcm-mfe/shared/common/constants";
import {Subscription} from "rxjs";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import * as moment from "moment/moment";
import {getTypeExport} from "@hcm-mfe/shared/common/utils";
import {saveAs} from "file-saver";
import {DateValidator} from "@hcm-mfe/shared/common/validators";
import { environment } from '@hcm-mfe/shared/environment';

@Component({
  selector: 'hcm-mfe-staff-report',
  templateUrl: './staff-report.component.html',
  styleUrls: ['./staff-report.component.scss']
})
export class StaffReportComponent implements OnInit, OnDestroy {

  isSubmitted = false;
  form: FormGroup;
  isLoadingPage = false;
  subscriptions: Subscription[] = [];
  listEmpTypeCode: CatalogModel[] = [];
  statusEmployeeList: CatalogModel[] = [];
  listPositionGroup: CatalogModel[] = [];
  listOrgRegions: CatalogModel[] = [];
  listLine: CatalogModel[] = [];
  code?: string;
  subs: Subscription[] = [];
  scope = Scopes.VIEW;
  constructor(
    public fb: FormBuilder,
    private translate: TranslateService,
    private toastService: ToastrService,
    private searchFormService: SearchFormService,
    route: ActivatedRoute
  ) {
    this.code = route.snapshot.data['code'];
    this.form = fb.group({
      org: [null],
      empTypeCodes: [null],
      positionGroupIds: [null],
      regionCodes: [null],
      lineIds: [null],
      empStatus: [null],
      fromDate: [null, Validators.required],
      toDate: [null, Validators.required]
    },{
      validators: [DateValidator.validateRangeDate('fromDate', 'toDate', 'rangeDateError')],
    });
  }

  ngOnInit(): void {
    this.getListEmpTypeCode();
    this.getListOrgRegions();
    this.getListLine();
    this.getListPositionGroup();
  }

  getListEmpTypeCode() {
    const params = new HttpParams().set('typeCode', Constant.TYPE_CODE.DOI_TUONG_CV);
    this.searchFormService.getListValue(UrlConstant.CATEGORY.LOOKUP_VALUE, params).subscribe(res => {
      this.listEmpTypeCode = res.data;
    }, () => {
      this.listEmpTypeCode = [];
    })
  }

  getListOrgRegions() {
    const params = new HttpParams().set('typeCode', Constant.TYPE_CODE.KHU_VUC);
    this.subscriptions.push(
      this.searchFormService.getListValue(UrlConstant.CATEGORY.LOOKUP_VALUE, params).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listOrgRegions = res.data;
        }
      }, error => {
        this.toastService.error(error.message);
        this.listEmpTypeCode = [];
      })
    );
  }

  getListLine() {
    this.subscriptions.push(
      this.searchFormService.getListLine(Constant.TYPE_CODE.LINE).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listLine = res.data.map((item: NzSafeAny) => {
            item.value = item.pgrId?.toString();
            item.label = item.pgrName;
            return item;
          });
        }
      }, error => {
        this.toastService.error(error.message);
        this.listEmpTypeCode = [];
      })
    );
  }

  getListPositionGroup() {
    this.searchFormService.getPositionGroup(Constant.TYPE_CODE.POSITION).subscribe(res => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.listPositionGroup = res.data.map((item: NzSafeAny) => {
          item.value = item.pgrId;
          item.label = item.pgrName;
          return item;
        });
      }
    }, error => this.toastService.error(error.message));
  }


  setFormValue(event: any, formName: string) {
    if(event.listOfSelected.length > 0) {
      this.formControl[formName].setValue(event.listOfSelected);
    }
  }

  get formControl() {
    return this.form.controls;
  }

  parseOptions() {
    const params: NzSafeAny = {};
    if (this.formControl['org'].value) {
      params.organizationId = this.formControl['org'].value['orgId'];
    }
    if (this.formControl['empTypeCodes'].value) {
      params.empTypeCodes = this.formControl['empTypeCodes'].value;
    }
    if (this.formControl['positionGroupIds'].value) {
      params.positionGroupIds = this.formControl['positionGroupIds'].value;
    }
    if (this.formControl['regionCodes'].value) {
      params.regionCodes = this.formControl['regionCodes'].value;
    }
    if (this.formControl['lineIds'].value) {
      params.lineIds = this.formControl['lineIds'].value;
    }
    if (this.formControl['empStatus'].value) {
      params.empStatus = this.formControl['empStatus'].value;
    }
    if (this.formControl['fromDate'].value) {
      params.fromDate = moment(this.formControl['fromDate'].value).format('DD/MM/YYYY');
    }
    if (this.formControl['toDate'].value) {
      params.toDate = moment(this.formControl['toDate'].value).format('DD/MM/YYYY');
    }
    return params;
  }

  doExport() {
    const searchParam: HttpParams = this.parseOptions();
    this.isSubmitted = true;
    if(this.code && this.form.valid) {
      this.isLoadingPage = true;
      const data = this.getUrlAndServiceName(this.code);
      this.subs.push(
        this.searchFormService.exportReport(searchParam, data).subscribe(res => {
          const arr = res.headers.get("Content-Disposition")?.split(';');
          const fileName:string = arr[arr.length - 1].replace('filename=','').trim();
          const reportFile = new Blob([res.body], {type: getTypeExport('xlsx')});
          saveAs(reportFile, fileName);
          this.isLoadingPage = false;
        }, error => {
          this.toastService.error(error.message);
          this.isLoadingPage = false;
        })
      );
    }
  }

  getUrlAndServiceName(code: string) {
    let data: NzSafeAny = {};
    switch (code) {
      case ReportFunctionCode.EMPLOYEE_REPORT_SUMMARY:
        data = {url: UrlConstant.URL_EMPLOYEE_REPORT.SUMMARY}
        break;
      case ReportFunctionCode.EMPLOYEE_REPORT_EMP_FULL:
        data = {url: UrlConstant.URL_EMPLOYEE_REPORT.EMP_FULL}
        break;
      case ReportFunctionCode.EMPLOYEE_REPORT_TERMINATION:
        data = {url: UrlConstant.URL_EMPLOYEE_REPORT.TERMINATION}
        break;
      case ReportFunctionCode.EMPLOYEE_REPORT_TRANSFER:
        data = {url: UrlConstant.URL_EMPLOYEE_REPORT.TRANSFER}
        break;
      default:
        data = {url: UrlConstant.URL_EMPLOYEE_REPORT.LEAVE}
        break;
    }
    return data;
  }

  checkCode(): boolean {
    if(this.code === ReportFunctionCode.EMPLOYEE_REPORT_TRANSFER || this.code === ReportFunctionCode.EMPLOYEE_REPORT_LEAVE) {
      return false
    }
    return true;
  }

  ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
