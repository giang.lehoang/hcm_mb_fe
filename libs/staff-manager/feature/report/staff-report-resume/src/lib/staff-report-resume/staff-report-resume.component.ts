import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {Subscription} from "rxjs";
import {TranslateService} from "@ngx-translate/core";
import {ToastrService} from "ngx-toastr";
import {SearchFormService} from "@hcm-mfe/staff-manager/data-access/services";
import {ActivatedRoute} from "@angular/router";
import {HttpParams} from "@angular/common/http";
import {UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {Scopes} from "@hcm-mfe/shared/common/constants";
import {getTypeExport} from "@hcm-mfe/shared/common/utils";
import {saveAs} from "file-saver";

@Component({
  selector: 'hcm-mfe-staff-report-resume',
  templateUrl: './staff-report-resume.component.html',
  styleUrls: ['./staff-report-resume.component.scss']
})
export class StaffReportResumeComponent implements OnInit, OnDestroy {

  isSubmitted = false;
  form: FormGroup;
  isLoadingPage = false;
  subscriptions: Subscription[] = [];
  code?: string;
  subs: Subscription[] = [];
  functionCode = this.code;
  scope = Scopes.VIEW;
  constructor(
    public fb: FormBuilder,
    private translate: TranslateService,
    private toastService: ToastrService,
    private searchFormService: SearchFormService,
    route: ActivatedRoute
  ) {
    this.code = route.snapshot.data['code'];
    this.form = fb.group({
      employee: [null],
      empCodes: [null],
      emails: [null],
    })
  }

  ngOnInit(): void {

  }

  get formControl() {
    return this.form.controls;
  }

  parseOptions() {
    let params = new HttpParams();
    if (this.formControl['employee'].value)
      params = params.set('employeeId', this.form.controls['employee'].value?.employeeId);

    if (this.formControl['empCodes'].value)
      params = params.set('empCodes', this.form.controls['empCodes'].value);

    if (this.formControl['emails'].value)
      params = params.set('emails', this.form.controls['emails'].value);

    return params;
  }

  doExport(reportType: 'docx' | 'zip' | 'pdf') {
    const searchParam: HttpParams = this.parseOptions();
    if (!this.formControl['employee'].value && !this.formControl['empCodes'].value && !this.formControl['emails'].value) {
      this.toastService.error(this.translate.instant('staffManager.report.error'));
      return;
    }
    this.isSubmitted = true;
    if(this.code && this.form.valid) {
      this.isLoadingPage = true;
      const data = {url: UrlConstant.URL_EMPLOYEE_REPORT.RESUME.replace('{reportType}', reportType)}
      this.subs.push(
        this.searchFormService.exportReport(searchParam, data).subscribe(res => {
          if ((res?.headers?.get('Excel-File-Empty'))) {
            this.toastService.error(this.translate.instant("common.notification.exportFail"));
          } else {
            const arr = res.headers.get("Content-Disposition")?.split(';');
            const fileName:string = arr[arr.length - 1].replace('filename=','').trim();
            const reportFile = new Blob([res.body], {type: getTypeExport('xlsx')});
            saveAs(reportFile, fileName);
          }
          this.isLoadingPage = false;
        }, error => {
          this.toastService.error(error.message);
          this.isLoadingPage = false;
        })
      );
    }
  }

  ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }

}
