import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffReportResumeComponent } from './staff-report-resume.component';

describe('StaffReportResumeComponent', () => {
  let component: StaffReportResumeComponent;
  let fixture: ComponentFixture<StaffReportResumeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StaffReportResumeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffReportResumeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
