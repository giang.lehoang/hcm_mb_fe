import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StaffReportResumeComponent } from './staff-report-resume/staff-report-resume.component';
import {RouterModule} from "@angular/router";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzFormModule} from "ng-zorro-antd/form";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {TranslateModule} from "@ngx-translate/core";
import {ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {SharedUiMbSelectCheckAbleModule} from "@hcm-mfe/shared/ui/mb-select-check-able";
import {SharedUiOrgDataPickerModule} from "@hcm-mfe/shared/ui/org-data-picker";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/employee-data-picker";
import {NzMenuModule} from "ng-zorro-antd/menu";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {NzButtonModule} from "ng-zorro-antd/button";

@NgModule({
  imports: [CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: StaffReportResumeComponent
      }
    ]), SharedUiMbButtonModule, NzFormModule, SharedUiMbInputTextModule, TranslateModule, ReactiveFormsModule, SharedUiMbDatePickerModule, NzButtonModule,
    SharedUiMbSelectCheckAbleModule, SharedUiOrgDataPickerModule, SharedUiLoadingModule, SharedUiMbSelectModule, SharedUiEmployeeDataPickerModule, NzMenuModule, NzIconModule, NzDropDownModule
  ],
  declarations: [
    StaffReportResumeComponent
  ],
})
export class StaffManagerFeatureReportStaffReportResumeModule {}
