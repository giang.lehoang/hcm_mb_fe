import { Component, Injector, OnDestroy, OnInit } from '@angular/core';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {functionUri, maxInt32} from "@hcm-mfe/shared/common/enums";
import * as moment from "moment/moment";
import {forkJoin} from "rxjs";
import {
  AssignRequest,
  EmployeeDetail,
  ManagerModel,
  ProcessManagerDetail
} from '@hcm-mfe/staff-manager/data-access/models/research';
import { ManagerDirectService } from '@hcm-mfe/staff-manager/data-access/services';

@Component({
  selector: 'app-assign-manager-init',
  templateUrl: './assign-manager-init.component.html',
  styleUrls: ['./assign-manager-init.component.scss']
})
export class AssignManagerInitComponent extends BaseComponent implements OnInit, OnDestroy {

  employee:EmployeeDetail;
  listManager: ManagerModel[]= [];
  listManagerTmp: ManagerModel[]= [];
  managerD: ManagerModel | undefined;
  managerI: ManagerModel | undefined;
  detailManage:ManagerModel | null;
  checked:boolean;
  request: AssignRequest;
  submit:boolean;
  fromDate:string | undefined;
  toDate:string | undefined;
  error: string | undefined;
  isAuto: boolean = false;
  listAssignRequest: AssignRequest[] = [];

  constructor(injector:Injector,  private readonly managerDirectService: ManagerDirectService) {
    super(injector);
    this.employee = {
      empCode: '',
      empId: 0,
      employeeCode: '',
      employeeId: 0,
      jobName: '',
      fullName: '',
      levelName: '',
      orgName: '',
      positionName: '',
      positionId: '',
      orgCurrentName: ''
    };

    this.request =
    {
      managerProcessId: "",
      employeeId:"",
      employeeFullName:"",
      managerId:"",
      indirectManagerId:"",
      positionId:"",
      fromDate:"",
      toDate:"",
      hasIndirectManager: true
    }
    this.checked = false;
    this.submit = false;
    const localS = localStorage.getItem('managerDirect');
    this.detailManage = localS ?  JSON.parse(localS ) : undefined;
  }

  ngOnInit(): void {
    if(this.detailManage){
      this.request.isMainPosition = this.detailManage.isMainPosition;
      if(!this.detailManage.managerProcessId){
        this.managerDirectService.getDetailEmployeeInfo(this.detailManage.employeeId).subscribe(data => {
          this.employee = data.data;
          if (this.employee.orgCurrentName) {
            this.employee.orgName = this.employee.orgCurrentName;
          }
          if(this.detailManage?.employeeId){
            this.request.employeeId = this.detailManage.employeeId;
          }
          this.request.employeeFullName = this.employee.fullName;
        })
      } else {
        this.request.managerProcessId = this.detailManage.managerProcessId;
      }
      this.employee.positionId = this.detailManage.positionId;
      this.employee.positionName = this.detailManage.positionName;
      this.request.positionId = this.detailManage.positionId;
      this.request.objectId = this.detailManage.objectId;
    }
    this.getListManager();
  }

  onChange(event: EmployeeDetail){
    this.employee = event;
    if(event){
      this.request.employeeId = event.employeeId;
      this.request.employeeFullName = event.fullName;
      this.request.positionId = event.positionId;
      if (this.employee.orgCurrentName) {
        this.employee.orgName = this.employee.orgCurrentName;
      }
    } else {
      this.request.employeeId = '';
      this.request.employeeFullName = '';
      this.request.positionId = '';
    }
  }

  getListManager(){
    const params = {
      keyword:'',
      size: maxInt32
    }
    this.isLoading = true;
    const request = [this.managerDirectService.getListManager(params)];
    if(this.request.managerProcessId){
      request.push( this.managerDirectService.assignManagerDetail(this.request.managerProcessId));
    }

    forkJoin(request).subscribe(data => {
      this.listManager =  data[0].data.listData;
      let detail:ProcessManagerDetail;
      if(this.request.managerProcessId){
        detail = data[1].data;
          this.employee = {
            empCode: detail.employeeCode,
            empId: detail.employeeId,
            employeeCode: detail.employeeCode,
            employeeId: detail.employeeId,
            jobName: detail.jobName,
            fullName: detail.fullName,
            levelName: detail.employeeLevel,
            orgName: detail.orgFullName,
          };
          this.request.employeeId = this.employee.employeeId;
          this.request.employeeFullName = this.employee.fullName;
          this.request.managerId = detail.managerId;
          this.request.indirectManagerId = detail.indirectManagerId;
          this.request.hasIndirectManager = detail.indirectManagerId ? true : false;
          this.isAuto = detail.isAuto === 'Y';
          this.request.fromDate = new Date(detail.fromDate).toString();
          if(detail.toDate){
            this.request.toDate = new Date(detail.toDate).toString();
          }
      }
      this.request.fromDate = this.request.fromDate?this.request.fromDate:moment(this.detailManage?.fromDate).toString();
      this.listManager.forEach(item => {
        item.fullName = `${item.fullName}-${item.employeeCode}`;
        if(item.status === 3){
          item.fullName = item.fullName + ' (Nghỉ việc)';
        }
        if(this.request.managerProcessId && detail.managerId === item.employeeId ){
          this.request.managerD = item;
        }

        if(this.request.managerProcessId && detail.indirectManagerId === item.employeeId){
          this.request.managerI = item;
        }
      });
      this.listManagerTmp = [...this.listManager];
      this.listAssignRequest.push(this.request);
      this.isLoading = false;
    }, error => {
      this.isLoading = false;
      this.toastrCustom.error(error?.message);
    });
  }

  findManager(id:string|number):ManagerModel | undefined {
    const data = this.listManager.find(item => item.employeeId === id);
    return data;
  }

  changeEmp($event: string, isDe: boolean,assignRequest:AssignRequest) {
    const item = this.findManager($event);
    if(isDe){
      assignRequest.managerD= item;
    } else {
      assignRequest.managerI = item;
    }

    if(assignRequest.managerId === assignRequest.indirectManagerId){
      this.toastrCustom.error(this.translate.instant('staffManager.managerDirect.message.duplicateManager'));
      if(isDe){
        setTimeout(() => {
          assignRequest.managerId = '';
          assignRequest.managerD = undefined;
        }, 500);
      } else {
        setTimeout(() => {
          assignRequest.indirectManagerId = '';
          assignRequest.managerI = undefined;
        }, 500);
      }
    }
  }

  assignManager(){
    this.submit = true;
    const objCheck = this.checkParams();
    if(!objCheck?.isValid){
      return;
    }
    const params  = {
      employeeId: this.request.employeeId,
      employeeFullName: this.request.employeeFullName,
      positionId: this.request.positionId,
      isMainPosition: this.request.isMainPosition,
      objectId: this.request.objectId,
      managerProcessDTOS: objCheck?.managerProcessDTOS.length > 0? objCheck?.managerProcessDTOS:[]
    }
    this.isLoading = true;
    this.managerDirectService.assignManager(params).subscribe(data => {
      this.toastrCustom.success();
      this.isLoading = false;
      this.router.navigate([functionUri.system_manager_direct])
    }, error => {
      this.isLoading = false;
      if(error?.code === 400){
        this.error = error?.message;
      }
      this.toastrCustom.error(error?.message);
    });
  }

  changeDate($event: any, fDate:boolean) {
    if(fDate){
      this.error = '';
    }

    if(this.fromDate && this.toDate){
      const fromDateNum = new Date(this.fromDate).setHours(0, 0, 0, 0);
      const toDateNum = new Date(this.toDate).setHours(0, 0, 0, 0);

      if(fromDateNum >= toDateNum){
        this.toastrCustom.error(this.translate.instant('staffManager.managerDirect.message.assignCompare'));
        setTimeout(() => {
          if (fDate){
            this.fromDate = ''
          } else {
            this.toDate = ''
          }
        })
      }
    }
  }

  override ngOnDestroy() {
    localStorage.removeItem('managerDirect');
  }
  onSearch(event:any){
    const listTmp: ManagerModel[]  = [];
    if(event){
      this.listManager.forEach(item => {
        if ((item.userName &&  item.userName.toLowerCase().includes(event.toLowerCase()))
          || item.fullName.toLowerCase().includes(event.toLowerCase())
          ||  item.employeeCode?.toLowerCase().includes(event.toLowerCase())
        ) {
          listTmp.push(item);
        }
      });
      this.listManagerTmp = [...listTmp];
    } else {
      this.listManagerTmp = [...this.listManager]
    }

  }
  addManager(){
    this.submit  = false;
    const newManager =
      {
        managerProcessId: "",
        managerId:"",
        indirectManagerId:"",
        fromDate:"",
        toDate:"",
        hasIndirectManager: true
      }
    this.listAssignRequest.push(newManager);
  }
  deleteManager(index: number){
    this.listAssignRequest.splice(index,1);
  }
  checkParams(){
    const managerProcessDTOS:AssignRequest[] = [];
    let isValid = true;
    this.listAssignRequest.forEach(item =>  {
      if(!item.fromDate || !this.request.managerId){
        isValid = false;
      }
      if(item.hasIndirectManager){
        if(!item.indirectManagerId){
          isValid = false;
        }
      } else {
        item.indirectManagerId = '';
      }
      managerProcessDTOS.push({
        managerProcessId: item.managerProcessId,
        managerId: item.managerId,
        indirectManagerId: item.indirectManagerId,
        fromDate: item.fromDate?moment(item.fromDate).format('DD/MM/YYYY'):'',
        toDate: item.toDate?moment(item.toDate).format('DD/MM/YYYY'):'',
        hasIndirectManager: item.hasIndirectManager
      });
    });
    const obj =  {
      isValid : isValid,
      managerProcessDTOS: managerProcessDTOS
    }
      return obj;
  }
}
