import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignManagerInitComponent } from './assign-manager-init.component';

describe('AssignManagerInitComponent', () => {
  let component: AssignManagerInitComponent;
  let fixture: ComponentFixture<AssignManagerInitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AssignManagerInitComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignManagerInitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
