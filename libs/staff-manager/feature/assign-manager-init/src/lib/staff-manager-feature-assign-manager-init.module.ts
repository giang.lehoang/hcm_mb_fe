import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { RouterModule } from '@angular/router';
import { SharedUiMbEmployeeDataPickerModule } from '@hcm-mfe/shared/ui/mb-employee-data-picker';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { SharedUiMbDatePickerModule } from '@hcm-mfe/shared/ui/mb-date-picker';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { AssignManagerInitComponent } from './assign-manager-init/assign-manager-init.component';
import { PopupService } from '@hcm-mfe/shared/ui/popup';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';

@NgModule({
  imports: [CommonModule, NzGridModule, NzCheckboxModule,
    RouterModule.forChild([
      {
        path: '',
        component: AssignManagerInitComponent
      }]), SharedUiMbEmployeeDataPickerModule, TranslateModule, FormsModule, NzDividerModule, SharedUiMbDatePickerModule, SharedUiMbSelectModule, SharedUiMbButtonModule, SharedUiLoadingModule, NzToolTipModule],
  exports: [AssignManagerInitComponent],
  declarations: [AssignManagerInitComponent],
  providers:[PopupService]
})
export class StaffManagerFeatureAssignManagerInitModule {}
