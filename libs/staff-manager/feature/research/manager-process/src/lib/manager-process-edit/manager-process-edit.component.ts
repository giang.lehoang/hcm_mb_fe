import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HTTP_STATUS_CODE } from "@hcm-mfe/shared/common/constants";
import { CustomValidators } from "@hcm-mfe/shared/common/validators";
import { ManagerProcessService } from "@hcm-mfe/staff-manager/data-access/services";
import { TranslateService } from '@ngx-translate/core';
import * as moment from "moment";
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';

@Component({
  selector: 'manager-process-edit',
  templateUrl: './manager-process-edit.component.html',
  styleUrls: ['./manager-process-edit.component.scss']
})
export class ManagerProcessEditComponent implements OnInit {

  form!: FormGroup;
  modal!: NzModalRef;
  managerProcessId!: number;
  isSubmitted = false;

  subscriptions: Subscription[] = [];
  employeeId!: number;

  constructor(
    private fb: FormBuilder,
    private toastrService: ToastrService,
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private modalService: NzModalService,
    private modalRef: NzModalRef,
    private managerProcessService: ManagerProcessService
  ) { }

  ngOnInit(): void {
    this.initFormGroup();
    this.pathValueForm();
  }

  initFormGroup() {
    this.form = this.fb.group({
      employeeCode: [null],
      fullName: [null],
      manager: [null, [Validators.required]],
      indirectManager: [null],
      fromDate: [null, [Validators.required]],
      toDate: [null],
      note: [null]
    }, {
      validator: [
        CustomValidators.toDateAfterFromDate('fromDate', 'toDate'),
      ]
    });
  }

  async pathValueForm() {
    if (this.managerProcessId) {
      const res = await this.managerProcessService.getMangerProcessById(this.managerProcessId).toPromise();
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.employeeId = res.data?.employeeId;
        this.form.patchValue(res.data);
        this.form.controls['fromDate'].setValue(res.data.fromDate ? moment(res.data.fromDate,'DD/MM/yyyy').toDate() : null);
        this.form.controls['toDate'].setValue(res.data.toDate ? moment(res.data.toDate,'DD/MM/yyyy').toDate() : null);
        this.subscriptions.push(
          this.managerProcessService.getMangerInfo(res.data?.managerId).subscribe(mangerRes => {
            if (mangerRes.code === HTTP_STATUS_CODE.OK) {
              this.form.controls['manager'].setValue(mangerRes.data);
            }
          }, error => this.toastrService.error(error.message))
        );

        this.subscriptions.push(
          this.managerProcessService.getMangerInfo(res.data?.indirectManagerId).subscribe(res => {
            if (res.code === HTTP_STATUS_CODE.OK) {
              this.form.controls['indirectManager'].setValue(res.data);
            }
          }, error => this.toastrService.error(error.message))
        )
      }
    }
  }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      const request = this.form.value;
      request.managerProcessId = this.managerProcessId;
      request.fromDate = this.form.controls['fromDate'].value ? moment(this.form.controls['fromDate'].value).format('DD/MM/YYYY') : null;
      request.toDate = this.form.controls['toDate'].value ? moment(this.form.controls['toDate'].value).format('DD/MM/YYYY') : null;
      request.managerId = this.form.controls['manager'].value?.employeeId;
      request.indirectManagerId = this.form.controls['indirectManager'].value?.employeeId;

      if (this.employeeId === request.managerId || this.employeeId === request.indirectManagerId) {
        this.toastrService.error(this.translate.instant('staffManager.staffResearch.manager.label.managerError'));
        return;
      }

      this.subscriptions.push(
        this.managerProcessService.saveManagerProcess(request).subscribe(res => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.toastrService.success(this.translate.instant('common.notification.updateSuccess'));
            this.modalRef.close({refresh: true});
          } else {
            this.toastrService.error(res.message)
          }
        }, error => this.toastrService.error(error.message))
      );
    }
  }


  closeModalAdd() {
    this.modalService.closeAll();
  }

  ngOnDestroy(): void {
    this.subscriptions?.forEach(sub => sub.unsubscribe());
  }

}
