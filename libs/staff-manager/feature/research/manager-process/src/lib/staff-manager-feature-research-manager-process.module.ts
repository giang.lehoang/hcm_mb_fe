import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {NzTagModule} from "ng-zorro-antd/tag";
import {TranslateModule} from "@ngx-translate/core";
import {RouterModule} from "@angular/router";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
import {CoreModule} from "@hcm-mfe/shared/ui/core";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiOrgDataPickerModule} from "@hcm-mfe/shared/ui/org-data-picker";
import {SharedUiEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/employee-data-picker";
import {SharedUiMbUploadModule} from "@hcm-mfe/shared/ui/mb-upload";
import {ManagerProcessResearchComponent} from "./manager-process-research/manager-process-research.component";
import {ManagerProcessEditComponent} from "./manager-process-edit/manager-process-edit.component";
import {StaffManagerUiSearchFormResearchModule} from "@hcm-mfe/staff-manager/ui/search-form-research";
import {StaffManagerUiImportEmployeeResearchModule} from "@hcm-mfe/staff-manager/ui/import-employee-research";
import {StaffManagerUiImportCommonModule} from "@hcm-mfe/staff-manager/ui/import-common";

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, SharedUiMbButtonModule, SharedUiMbTableWrapModule, SharedUiMbTableModule, NzTagModule, TranslateModule, NzToolTipModule,
    RouterModule.forChild([
      {
        path: '',
        component: ManagerProcessResearchComponent
      }
    ]), CoreModule, SharedUiMbSelectModule, SharedUiMbDatePickerModule, SharedUiMbInputTextModule, SharedUiOrgDataPickerModule, SharedUiEmployeeDataPickerModule, SharedUiMbUploadModule, StaffManagerUiSearchFormResearchModule, StaffManagerUiImportEmployeeResearchModule, StaffManagerUiImportCommonModule],
  declarations: [ManagerProcessResearchComponent, ManagerProcessEditComponent],
  exports: [ManagerProcessResearchComponent, ManagerProcessEditComponent]
})
export class StaffManagerFeatureResearchManagerProcessModule {}
