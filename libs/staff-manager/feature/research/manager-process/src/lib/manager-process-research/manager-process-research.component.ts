import {AfterViewInit, Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {Subscription} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {NzSafeAny} from 'ng-zorro-antd/core/types';
import {Router} from '@angular/router';
import {HttpParams} from '@angular/common/http';
import {ToastrService} from 'ngx-toastr';
import {saveAs} from 'file-saver';

import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {
  Constant,
  ConstantColor,
  getPreviousForm,
  saveSearchFormToLocalStorage,
  UrlConstant
} from "@hcm-mfe/staff-manager/data-access/common";
import {AppFunction, CatalogModel, MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {SearchFormComponent} from "@hcm-mfe/staff-manager/ui/search-form-research";
import {
  BookmarkFormService,
  ManagerProcessService,
  SearchFormService
} from "@hcm-mfe/staff-manager/data-access/services";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {ValidateService} from "@hcm-mfe/shared/core";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {getTypeExport} from "@hcm-mfe/shared/common/utils";
import {PersonalInformation} from "@hcm-mfe/staff-manager/data-access/models/research";
import {SessionService} from "@hcm-mfe/shared/common/store";
import {NzModalRef, NzModalService} from "ng-zorro-antd/modal";
import {ManagerProcessEditComponent} from "../manager-process-edit/manager-process-edit.component";

@Component({
  selector: 'app-manager-process-research',
  templateUrl: './manager-process-research.component.html',
  styleUrls: ['./manager-process-research.component.scss']
})
export class ManagerProcessResearchComponent implements OnInit {
  public readonly MODULENAME = Constant.MODULE_NAME.MANAGER_PROCESS;
  functionCode = FunctionCode.HR_MANAGER_PROCESS;
  isLoadingPage = false;
  tableConfig!: MBTableConfig;
  searchResult: PersonalInformation[] = [];
  subs: Subscription[] = [];
  isImportData = false;
  urlApiImport = UrlConstant.IMPORT_FORM.MANAGER_PROCESS_FORM;
  urlApiDownloadTemp = UrlConstant.IMPORT_FORM.MANAGER_PROCESS;
  objFunction: AppFunction;
  statusList: CatalogModel[] = []
  pagination = new Pagination();
  constant = Constant;
  modal!: NzModalRef;

  @ViewChild('empStatusTmpl', {static: true}) empStatus!: TemplateRef<NzSafeAny>;
  @ViewChild('actionTmpl', {static: true}) action!: TemplateRef<NzSafeAny>;
  @ViewChild('searchForm') searchForm!: SearchFormComponent;

    tagIsWorkingColor: string = ConstantColor.TAG.STATUS_IS_WORKING;
    tagSuspenseColor: string = ConstantColor.TAG.STATUS_SUSPENSE;
    tagRetiredColor: string = ConstantColor.TAG.STATUS_RETIRED;

  constructor(private bookmarkFormService: BookmarkFormService,
              private baseService: BaseService,
              private router: Router,
              private toastrService: ToastrService,
              private translateService: TranslateService,
              private searchFormService: SearchFormService,
              public validateService: ValidateService,
              public sessionService: SessionService,
              private modalService: NzModalService,
              private managerProcessService: ManagerProcessService
  ) {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.HR_MANAGER_PROCESS}`);
  }

  onDataSelectReady($event: boolean) {
    if ($event) {
      this.getPreviousSearchForm();
      this.doSearch();
    }
  }

  getPreviousSearchForm() {
    getPreviousForm(this.searchForm, Constant.LOCAL_STORAGE_KEY.PREVIOUS_SEARCH_FORM.MANAGER_PROCESS_SEARCH);
  }

  ngOnInit(): void {
    this.initDataSelect();
    this.initTable();
  }

  initDataSelect() {
    this.statusList = Constant.STATUS_EMP.map(item => {
      item.label = this.translateService.instant(item.label);
      return item;
    })
  }

  doSearch() {
    if (this.searchForm?.form.valid) {
      saveSearchFormToLocalStorage(this.searchForm.form, Constant.LOCAL_STORAGE_KEY.PREVIOUS_SEARCH_FORM.MANAGER_PROCESS_SEARCH);
      const searchParam = this.searchForm ? this.searchForm.parseOptions() : new HttpParams();
      this.searchForm.isLoadingPage = true;
      this.subs.push(
        this.searchFormService.getFilterResearch(UrlConstant.SEARCH_FORM.MANAGER_PROCESS, searchParam, this.pagination.getCurrentPage()).subscribe(res => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.searchResult = res.data.listData;
            this.tableConfig.total = res.data.count;
          }
          this.searchForm.isLoadingPage = false;
        }, error => {
          this.searchForm.isLoadingPage = false;
          this.toastrService.error(error.message);
        })
      );
    }
  }

  search(pageNumber: number) {
    this.pagination.pageNumber = pageNumber;
    this.doSearch();
  }

  doImportData() {
    this.isImportData = true;
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'staffManager.staffResearch.personalInformation.table.employeeCode',
          field: 'employeeCode',
          width: 120,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left'
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.fullName',
          field: 'fullName',
          width: 150,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left'
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.empTypeName',
          field: 'empTypeName',
          width: 120
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.positionName',
          field: 'jobName',
          width: 120
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.orgName',
          field: 'organizationName',
          width: 200
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.dateOfBirth',
          field: 'dateOfBirth',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 100,
          show: false
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.email',
          field: 'email',
          width: 150,
          show: false
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.mobileNumber',
          field: 'mobileNumber',
          width: 100,
          show: false
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.personalId',
          field: 'personalId',
          width: 100,
          show: false
        },
        {
          title: 'staffManager.staffResearch.manager.table.managerCode',
          field: 'managerCode',
          width: 100
        },
        {
          title: 'staffManager.staffResearch.manager.table.managerName',
          field: 'managerName',
          width: 140
        },
        {
          title: 'staffManager.staffResearch.manager.table.indirectManagerCode',
          field: 'indirectManagerCode',
          width: 100
        },
        {
          title: 'staffManager.staffResearch.manager.table.indirectManagerName',
          field: 'indirectManagerName',
          width: 140
        },
        {
          title: 'staffManager.staffResearch.manager.table.fromDate',
          field: 'fromDate',
          width: 100,
          tdClassList: ['text-center']
        },
        {
          title: 'staffManager.staffResearch.manager.table.toDate',
          field: 'toDate',
          width: 100,
          tdClassList: ['text-center']
        },
        {
          title: 'staffManager.staffResearch.manager.table.note',
          field: 'note',
          width: 180,
          show: false
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.flagStatus',
          field: 'status',
          tdTemplate: this.empStatus,
          width: 100,
          show: false
        },
        {
          title: ' ',
          field: 'action',
          tdClassList: ['text-center'],
          width: 60,
          tdTemplate: this.action,
          fixed: window.innerWidth > 1024,
          fixedDir: 'right',
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  onClickItem(employee: PersonalInformation) {
    this.router.navigate([UrlConstant.URL_NAVIGATE.PERSONAL_INFO], {
      queryParams: {employeeId: employee.employeeId, employeeCode: employee.employeeCode}
    }).then();
  }

  doCloseImport() {
    this.isImportData = false;
  }

  doExportData() {
    const searchParam = this.searchForm ? this.searchForm.parseOptions() : new HttpParams();
    this.searchForm.isLoadingPage = true;
    this.subs.push(
      this.searchFormService.doExportFile(UrlConstant.EXPORT_REPORT.MANAGER_PROCESS, searchParam).subscribe(res => {
        this.searchForm.isLoadingPage = false;
        if (res?.headers?.get('Excel-File-Empty')) {
          this.toastrService.error(this.translateService.instant("common.notification.exportFail"));
          return;
        } else {
          const reportFile = new Blob([res.body], {type: getTypeExport('xlsx')});
          saveAs(reportFile, 'thong_tin_nguoi_quan_ly.xlsx');
        }
      }, error => {
        this.toastrService.error(error.message);
        this.searchForm.isLoadingPage = false;
      })
    );
  }

  onOpenEdit(managerProcessId: number) {
    this.modal = this.modalService.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
      nzTitle: this.translateService.instant('staffManager.staffResearch.manager.label.editManagerProcess'),
      nzContent: ManagerProcessEditComponent,
      nzComponentParams: {
        managerProcessId: managerProcessId
      },
      nzFooter: null
    });
    this.subs.push(
      this.modal.afterClose.subscribe(res => {
        if (res && res.refresh) {
          this.doSearch();
        }
      })
    );
  }

  deleteManagerProcess(managerProcessId: number) {
    this.subs.push(
      this.managerProcessService.deleteMangerProcessById(managerProcessId).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.toastrService.success(this.translateService.instant('common.notification.deleteSuccess'))
          this.doSearch();
        } else {
          this.toastrService.error(res.message)
        }
      }, error => this.toastrService.error(error.message))
    );
  }

  ngOnDestroy() {
    this.subs?.forEach(sub => sub.unsubscribe());
  }

}
