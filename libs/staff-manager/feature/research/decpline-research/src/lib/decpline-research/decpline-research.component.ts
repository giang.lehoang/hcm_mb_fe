import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { Subscription } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { saveAs } from 'file-saver';
import { ToastrService } from 'ngx-toastr';

import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {
  Constant,
  ConstantColor,
  getPreviousForm,
  saveSearchFormToLocalStorage,
  UrlConstant
} from "@hcm-mfe/staff-manager/data-access/common";
import {CatalogModel, MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {SearchFormComponent} from "@hcm-mfe/staff-manager/ui/search-form-research";
import {BookmarkFormService, SearchFormService} from "@hcm-mfe/staff-manager/data-access/services";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {ValidateService} from "@hcm-mfe/shared/core";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {getTypeExport} from "@hcm-mfe/shared/common/utils";
import {Decpline} from "@hcm-mfe/staff-manager/data-access/models/research";

@Component({
  selector: 'app-decpline-research',
  templateUrl: './decpline-research.component.html',
  styleUrls: ['./decpline-research.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DecplineResearchComponent implements OnInit {

  functionCode: string = FunctionCode.HR_DECPLINE;

  public readonly MODULENAME = Constant.MODULE_NAME.DECPLINE;
  tableConfig!: MBTableConfig;
  searchResult: Decpline[] = [];

  isImportData = false;
  urlApiImport: string = UrlConstant.IMPORT_FORM.BASIC_FORM;
  statusList: CatalogModel[] = [
    {
      value: 1,
      label: this.translateService.instant('staffManager.label.workIn')
    },
    {
      value: 3,
      label: this.translateService.instant('staffManager.label.workOut')
    },
    {
      value: 2,
      label: this.translateService.instant('staffManager.label.contractPending')
    }
  ];
  subs: Subscription[] = [];
  pagination = new Pagination();

  @ViewChild('searchForm') searchForm!: SearchFormComponent;

  @ViewChild('flagStatusTmpl', {static: true}) flagStatus!: TemplateRef<NzSafeAny>;

    tagIsWorkingColor: string = ConstantColor.TAG.STATUS_IS_WORKING;
    tagSuspenseColor: string = ConstantColor.TAG.STATUS_SUSPENSE;
    tagRetiredColor: string = ConstantColor.TAG.STATUS_RETIRED;

  constructor(private bookmarkFormService: BookmarkFormService,
              private baseService: BaseService,
              private cdr: ChangeDetectorRef,
              private router: Router,
              private translateService: TranslateService,
              private searchFormService: SearchFormService,
              private toastrService: ToastrService,
              public validateService: ValidateService) {
  }

  onDataSelectReady($event: boolean) {
    if ($event) {
      this.getPreviousSearchForm();
      this.doSearch();
    }
  }

  getPreviousSearchForm() {
    getPreviousForm(this.searchForm, Constant.LOCAL_STORAGE_KEY.PREVIOUS_SEARCH_FORM.DISCIPLINE_SEARCH);
  }

  ngOnInit(): void {
    this.initTable();
  }

  ngOnDestroy() {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  doImportData() {
    this.isImportData = true;
  }

  doSearch() {
    if(this.searchForm?.form.valid) {
      saveSearchFormToLocalStorage(this.searchForm.form, Constant.LOCAL_STORAGE_KEY.PREVIOUS_SEARCH_FORM.DISCIPLINE_SEARCH);
      this.searchForm.isLoadingPage = true;
      this.cdr.detectChanges();
      const searchParam = this.searchForm ? this.searchForm.parseOptions() : new HttpParams();

      this.searchFormService.getFilterResearch(UrlConstant.SEARCH_FORM.DECPLINE, searchParam, this.pagination.getCurrentPage()).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.searchResult = res.data.listData;
          this.tableConfig.total = res.data.count;
        }
        this.searchForm.isLoadingPage = false;
        this.cdr.detectChanges();
      }, () => {
        this.searchForm.isLoadingPage = false;
        this.cdr.detectChanges();
      });
    }
  }

  search(pageNumber: number) {
    this.pagination.pageNumber = pageNumber;
    this.doSearch();
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'staffManager.staffResearch.decpline.table.employeeCode',
          field: 'employeeCode',
          width: 120,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left'
        },
        {
          title: 'staffManager.staffResearch.decpline.table.fullName',
          field: 'fullName',
          width: 150,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left'
        },
        {
          title: 'staffManager.staffResearch.decpline.table.orgName',
          field: 'orgName',
          width: 200
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.empTypeName',
          field: 'empTypeName',
          width: 120
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.positionName',
          field: 'jobName',
          width: 120,
        },
        {
          title: 'staffManager.staffResearch.decpline.table.signedDate',
          field: 'signedDate',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 100
        },
        {
          title: 'staffManager.staffResearch.decpline.table.disciplineMethodName',
          field: 'disciplineMethodName',
          width: 180
        },
        {
          title: 'staffManager.staffResearch.decpline.table.reason',
          field: 'reason',
          width: 150,
        },
        {
          title: 'staffManager.staffResearch.decpline.table.disciplineLevelName',
          field: 'disciplineLevelName',
          width: 150
        },
        {
          title: 'staffManager.staffResearch.decpline.table.amount',
          field: 'amount',
          width: 150,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          pipe: 'currency: VND'
        },
        {
          title: 'staffManager.staffResearch.decpline.table.amountRemedied',
          field: 'amountRemedied',
          width: 150,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          pipe: 'currency: VND',
          show: false
        },
        {
          title: 'staffManager.staffResearch.decpline.table.effectiveDate',
          field: 'fromDate',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 100
        },
        {
          title: 'staffManager.staffResearch.decpline.table.expirationDate',
          field: 'toDate',
          width: 100,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          show: false
        },
        {
          title: 'staffManager.staffResearch.decpline.table.caseName',
          field: 'caseName',
          width: 150
        },
        {
          title: 'staffManager.staffResearch.decpline.table.signer',
          field: 'signer',
          width: 120,
          show: false
        },
        {
          title: 'staffManager.staffResearch.decpline.table.signerPosition',
          field: 'signerPosition',
          width: 120,
          show: false
        },
        {
          title: 'staffManager.staffResearch.decpline.table.flagStatus',
          field: 'flagStatus',
          tdTemplate: this.flagStatus,
          width: 100,
          show: false
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  getFlagStatus(value: number) {
    return this.statusList.find(status => status.value === value)?.label;
  }

  onClickItem(employee: Decpline) {
    this.router.navigate(['/staff/personal-info'], {
      queryParams: { employeeId: employee.employeeId }
    });
  }
  doExportData() {
    const searchParam = this.searchForm ? this.searchForm.parseOptions() : new HttpParams();
    this.searchForm.isLoadingPage = true;
    this.searchFormService.doExportFile(UrlConstant.EXPORT_REPORT.DECPLINE, searchParam).subscribe(res => {
      if (res?.headers?.get('Excel-File-Empty')) {
        this.toastrService.error(this.translateService.instant("common.notification.exportFail"));
      } else {
        const reportFile = new Blob([res.body], {type: getTypeExport('xlsx')});
        saveAs(reportFile, 'thong_tin_ky_luat.xlsx');
      }
      this.searchForm.isLoadingPage = false;
      this.cdr.detectChanges();
    },(error) => {
      this.searchForm.isLoadingPage = false;
      this.toastrService.error(error.message);
      this.cdr.detectChanges();
    });
  }
}
