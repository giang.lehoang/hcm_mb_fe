import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LayoutResearchComponent} from "./layout-research/layout-research.component";
import {NzFormModule} from "ng-zorro-antd/form";
import {RouterModule} from "@angular/router";
import {StaffManagerFeatureResearchSideBarModule} from "@hcm-mfe/staff-manager/feature/research/side-bar";

@NgModule({
  imports: [CommonModule, NzFormModule, RouterModule, StaffManagerFeatureResearchSideBarModule],
  declarations: [LayoutResearchComponent],
  exports: [LayoutResearchComponent],
})
export class StaffManagerFeatureResearchLayoutResearchModule {}
