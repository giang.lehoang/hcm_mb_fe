import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { Subscription } from 'rxjs';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { HttpParams } from '@angular/common/http';
import { saveAs } from 'file-saver';
import { ToastrService } from 'ngx-toastr';

import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {
  Constant,
  ConstantColor,
  getPreviousForm,
  saveSearchFormToLocalStorage,
  UrlConstant
} from "@hcm-mfe/staff-manager/data-access/common";
import {CatalogModel, MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {SearchFormComponent} from "@hcm-mfe/staff-manager/ui/search-form-research";
import {BookmarkFormService, SearchFormService} from "@hcm-mfe/staff-manager/data-access/services";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {ValidateService} from "@hcm-mfe/shared/core";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {getTypeExport} from "@hcm-mfe/shared/common/utils";
import {Degree} from "@hcm-mfe/staff-manager/data-access/models/research";

@Component({
  selector: 'app-degree-research',
  templateUrl: './degree-research.component.html',
  styleUrls: ['./degree-research.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DegreeResearchComponent implements OnInit {

  functionCode: string = FunctionCode.HR_EDU_DEGREE;

  public readonly MODULENAME = Constant.MODULE_NAME.DEGREE;
  tableConfig!: MBTableConfig;
  searchResult: Degree[] = [];

  isImportData = false;
  urlApiImport: string = UrlConstant.IMPORT_FORM.BASIC_FORM;
  statusList: CatalogModel[] = [
    {
      value: 1,
      label: this.translateService.instant('staffManager.label.workIn')
    },
    {
      value: 3,
      label: this.translateService.instant('staffManager.label.workOut')
    },
    {
      value: 2,
      label: this.translateService.instant('staffManager.label.contractPending')
    }
  ];
  subs: Subscription[] = [];
  pagination = new Pagination();

  @ViewChild('searchForm') searchForm!: SearchFormComponent;

  @ViewChild('flagStatusTmpl', {static: true}) flagStatus!: TemplateRef<NzSafeAny>;

    tagIsWorkingColor: string = ConstantColor.TAG.STATUS_IS_WORKING;
    tagSuspenseColor: string = ConstantColor.TAG.STATUS_SUSPENSE;
    tagRetiredColor: string = ConstantColor.TAG.STATUS_RETIRED;

  constructor(private bookmarkFormService: BookmarkFormService,
              private baseService: BaseService,
              private router: Router,
              private cdr: ChangeDetectorRef,
              private translateService: TranslateService,
              private searchFormService: SearchFormService,
              private toastrService: ToastrService,
              public validateService: ValidateService) {
  }

  onDataSelectReady($event: boolean) {
    if ($event) {
      this.getPreviousSearchForm();
      this.doSearch();
    }
  }

  getPreviousSearchForm() {
    getPreviousForm(this.searchForm, Constant.LOCAL_STORAGE_KEY.PREVIOUS_SEARCH_FORM.DEGREE_SEARCH);
  }

  ngOnInit(): void {
    this.initTable();
  }

  ngOnDestroy() {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  doImportData() {
    this.isImportData = true;
  }

  doSearch() {
    if(this.searchForm?.form.valid) {
      saveSearchFormToLocalStorage(this.searchForm.form, Constant.LOCAL_STORAGE_KEY.PREVIOUS_SEARCH_FORM.DEGREE_SEARCH);
      const searchParam = this.searchForm ? this.searchForm.parseOptions() : new HttpParams();
      this.searchForm.isLoadingPage = true;
      this.cdr.detectChanges();
      this.searchFormService.getFilterResearch(UrlConstant.SEARCH_FORM.DEGREE, searchParam, this.pagination.getCurrentPage()).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.searchResult = res.data.listData;
          this.tableConfig.total = res.data.count;
        }
        this.searchForm.isLoadingPage = false;
        this.cdr.detectChanges();
      }, error => {
        this.searchForm.isLoadingPage = false;
        this.cdr.detectChanges();
      });
    }
  }

  search(pageNumber: number) {
    this.pagination.pageNumber = pageNumber;
    this.doSearch();
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'staffManager.staffResearch.degree.table.employeeCode',
          field: 'employeeCode',
          width: 120,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left'
        },
        {
          title: 'staffManager.staffResearch.degree.table.fullName',
          field: 'fullName',
          width: 150,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left'
        },
        {
          title: 'staffManager.staffResearch.degree.table.orgName',
          field: 'orgName',
          width: 200
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.empTypeName',
          field: 'empTypeName',
          width: 120
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.positionName',
          field: 'jobName',
          width: 120,
        },
        {
          title: 'staffManager.staffResearch.degree.table.issueYear',
          field: 'issueYear',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 80
        },
        {
          title: 'staffManager.staffResearch.degree.table.schoolName',
          field: 'schoolName',
          width: 150
        },
        {
          title: 'staffManager.staffResearch.degree.table.facultyName',
          field: 'facultyName',
          width: 150
        },
        {
          title: 'staffManager.staffResearch.degree.table.majorLevelName',
          field: 'majorLevelName',
          width: 100
        },
        {
          title: 'staffManager.staffResearch.degree.table.degreeTypeName',
          field: 'degreeTypeName',
          width: 100,
          show: false
        },
        {
          title: 'staffManager.staffResearch.degree.table.eduRankName',
          field: 'eduRankName',
          width: 100,
          show: false
        },
        {
          title: 'staffManager.staffResearch.degree.table.flagStatus',
          field: 'flagStatus',
          tdTemplate: this.flagStatus,
          show: false,
          width: 100
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  getFlagStatus(value: number) {
    return this.statusList.find(status => status.value === value)?.label;
  }

  onClickItem(employee: Degree) {
    this.router.navigate(['/staff/personal-info'], {
      queryParams: { employeeId: employee.employeeId }
    });
  }

  doExportData() {
    const searchParam = this.searchForm ? this.searchForm.parseOptions() : new HttpParams();
    this.searchForm.isLoadingPage = true;
    this.searchFormService.doExportFile(UrlConstant.EXPORT_REPORT.DEGREE_FORM, searchParam).subscribe(res => {
      if ((res?.headers?.get('Excel-File-Empty'))) {
        this.toastrService.error(this.translateService.instant("common.notification.exportFail"));
      } else {
        const reportFile = new Blob([res.body], {type: getTypeExport('xlsx')});
        saveAs(reportFile, 'bang_cap_chung_chi.xlsx');
      }
      this.searchForm.isLoadingPage = false;
      this.cdr.detectChanges();
    }, (error) => {
      this.searchForm.isLoadingPage = false;
      this.toastrService.error(error.message);
      this.cdr.detectChanges();
    });
  }

}
