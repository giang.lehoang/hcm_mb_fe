import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { Router } from '@angular/router';
import { HttpParams } from '@angular/common/http';
import { saveAs } from 'file-saver';
import { ToastrService } from 'ngx-toastr';

import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {
  Constant,
  ConstantColor,
  getPreviousForm,
  saveSearchFormToLocalStorage,
  UrlConstant
} from "@hcm-mfe/staff-manager/data-access/common";
import {CatalogModel, MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {SearchFormComponent} from "@hcm-mfe/staff-manager/ui/search-form-research";
import {BookmarkFormService, SearchFormService} from "@hcm-mfe/staff-manager/data-access/services";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {ValidateService} from "@hcm-mfe/shared/core";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {getTypeExport} from "@hcm-mfe/shared/common/utils";
import {WorkHis} from "@hcm-mfe/staff-manager/data-access/models/research";

@Component({
  selector: 'app-work-his-research',
  templateUrl: './work-his-research.component.html',
  styleUrls: ['./work-his-research.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorkHisResearchComponent implements OnInit {

  functionCode: string = FunctionCode.HR_WORK_PROCESS;

  public readonly MODULENAME = Constant.MODULE_NAME.WORK;
  tableConfig!: MBTableConfig;
  searchResult: WorkHis[] = [];

  isImportData = false;
  urlApiImport: string = UrlConstant.IMPORT_FORM.BASIC_FORM;
  statusList: CatalogModel[] = [
    {
      value: 1,
      label: this.translateService.instant('staffManager.label.workIn')
    },
    {
      value: 3,
      label: this.translateService.instant('staffManager.label.workOut')
    },
    {
      value: 2,
      label: this.translateService.instant('staffManager.label.contractPending')
    }
  ];
  subs: Subscription[] = [];
  pagination = new Pagination();

  @ViewChild('searchForm') searchForm!: SearchFormComponent;

  @ViewChild('flagStatusTmpl', { static: true }) flagStatus!: TemplateRef<NzSafeAny>;

    tagIsWorkingColor: string = ConstantColor.TAG.STATUS_IS_WORKING;
    tagSuspenseColor: string = ConstantColor.TAG.STATUS_SUSPENSE;
    tagRetiredColor: string = ConstantColor.TAG.STATUS_RETIRED;

  constructor(private bookmarkFormService: BookmarkFormService,
              private baseService: BaseService,
              private router: Router,
              private cdr: ChangeDetectorRef,
              private translateService: TranslateService,
              private searchFormService: SearchFormService,
              private toastrService: ToastrService,
              public validateService: ValidateService) {
  }

  onDataSelectReady($event: boolean) {
    if ($event) {
      this.getPreviousSearchForm();
      this.doSearch();
    }
  }

  ngOnInit(): void {
    this.initTable();
  }

  ngOnDestroy() {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  getPreviousSearchForm() {
    getPreviousForm(this.searchForm, Constant.LOCAL_STORAGE_KEY.PREVIOUS_SEARCH_FORM.WORK_HIS_SEARCH);
  }

  doImportData() {
    this.isImportData = true;
  }

  doSearch() {
    if(this.searchForm?.form.valid) {
      saveSearchFormToLocalStorage(this.searchForm.form, Constant.LOCAL_STORAGE_KEY.PREVIOUS_SEARCH_FORM.WORK_HIS_SEARCH);
      this.searchForm.isLoadingPage = true;
      const searchParam = this.searchForm ? this.searchForm.parseOptions() : new HttpParams();
      this.cdr.detectChanges();
      this.searchFormService.getFilterResearch(UrlConstant.SEARCH_FORM.WORK_HIS, searchParam, this.pagination.getCurrentPage()).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.searchResult = res.data.listData;
          this.tableConfig.total = res.data.count;
        }
        this.searchForm.isLoadingPage = false;
        this.cdr.detectChanges();
      }, () => {
        this.searchForm.isLoadingPage = false;
        this.cdr.detectChanges();
      });
    }
  }

  search(pageNumber: number) {
    this.pagination.pageNumber = pageNumber;
    this.doSearch();
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'staffManager.staffResearch.workHis.table.employeeCode',
          field: 'employeeCode',
          width: 75,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left'
        },
        {
          title: 'staffManager.staffResearch.workHis.table.fullName',
          field: 'fullName',
          width: 110,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left'
        },
        {
          title: 'staffManager.staffResearch.workHis.table.fromDate',
          field: 'fromDate',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 75
        },
        {
          title: 'staffManager.staffResearch.workHis.table.toDate',
          field: 'toDate',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 75
        },
        {
          title: 'staffManager.staffResearch.workHis.table.documentType',
          field: 'documentTypeName',
          width: 100
        },
        {
          title: 'staffManager.staffResearch.workHis.table.trial',
          field: 'trial',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          show:false,
          width: 30
        },
        {
          title: 'staffManager.staffResearch.workHis.table.keepSenior',
          field: 'keepSenior',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          show:false,
          width: 30
        },
        {
          title: 'staffManager.staffResearch.workHis.table.processOrgName',
          field: 'processOrgName',
          width: 150
        },
        {
          title: 'staffManager.staffResearch.workHis.table.processEmpTypeName',
          field: 'processEmpTypeName',
          width: 100
        },
        {
          title: 'staffManager.staffResearch.workHis.table.processJobName',
          field: 'processJobName',
          width: 100
        },
        {
          title: 'staffManager.staffResearch.workHis.table.processLevel',
          field: 'processLevel',
          width: 75
        },
        {
          title: 'staffManager.staffResearch.workHis.table.processGroup',
          field: 'processGroup',
          width: 100
        },

        {
          title: 'staffManager.staffResearch.workHis.table.planToDate1',
          field: 'planToDate1',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 75,
          show: false
        },
        {
          title: 'staffManager.staffResearch.workHis.table.planToDate2',
          field: 'planToDate2',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 75,
          show: false
        },
        {
          title: 'staffManager.staffResearch.workHis.table.documentNo',
          field: 'documentNo',
          width: 100,
          show: false
        },
        {
          title: 'staffManager.staffResearch.workHis.table.signedDate',
          field: 'signedDate',
          width: 75,
          show: false
        },
        {
          title: 'staffManager.staffResearch.workHis.table.orgName',
          field: 'organizationName',
          show: false,
          width: 200,
        },
        {
          title: 'staffManager.staffResearch.workHis.table.empTypeName',
          show: false,
          field: 'empTypeName',
          width: 120
        },
        {
          title: 'staffManager.staffResearch.workHis.table.positionName',
          field: 'jobName',
          show: false,
          width: 120,
        },
        {
          title: 'staffManager.staffResearch.workHis.table.flagStatus',
          field: 'flagStatus',
          tdTemplate: this.flagStatus,
          width: 100,
          show: false
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  getFlagStatus(value: number) {
    return this.statusList.find(status => status.value === value)?.label;
  }

  onClickItem(employee: WorkHis) {
    this.router.navigate(['/staff/personal-info'], {
      queryParams: { employeeId: employee.employeeId }
    });
  }

  doExportData() {
    const searchParam = this.searchForm ? this.searchForm.parseOptions() : new HttpParams();
    this.searchForm.isLoadingPage = true;
    this.searchFormService.doExportFile(UrlConstant.EXPORT_REPORT.WORK_HIS, searchParam).subscribe(res => {
      if ((res?.headers?.get('Excel-File-Empty'))) {
        this.toastrService.error(this.translateService.instant("common.notification.exportFail"));
      } else {
        const reportFile = new Blob([res.body], {type: getTypeExport('xlsx')});
        saveAs(reportFile, 'qua_trinh_cong_tac.xlsx');
      }
      this.searchForm.isLoadingPage = false;
      this.cdr.detectChanges();
    }, (error) => {
      this.searchForm.isLoadingPage = false;
      this.toastrService.error(error.message);
      this.cdr.detectChanges();
    });
  }
}
