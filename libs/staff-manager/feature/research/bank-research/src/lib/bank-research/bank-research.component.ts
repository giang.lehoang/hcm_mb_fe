import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { HttpParams } from '@angular/common/http';
import { saveAs } from 'file-saver';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { ToastrService } from 'ngx-toastr';

import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {
  Constant,
  ConstantColor,
  getPreviousForm,
  saveSearchFormToLocalStorage,
  UrlConstant
} from "@hcm-mfe/staff-manager/data-access/common";
import {CatalogModel, MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {SearchFormComponent} from "@hcm-mfe/staff-manager/ui/search-form-research";
import {BookmarkFormService, SearchFormService} from "@hcm-mfe/staff-manager/data-access/services";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {ValidateService} from "@hcm-mfe/shared/core";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {getTypeExport} from "@hcm-mfe/shared/common/utils";
import {BankAccount} from "@hcm-mfe/staff-manager/data-access/models/research";

@Component({
  selector: 'app-bank-research',
  templateUrl: './bank-research.component.html',
  styleUrls: ['./bank-research.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BankResearchComponent implements OnInit, OnDestroy {

  functionCode: string = FunctionCode.HR_ACCOUNT;

  public readonly MODULENAME = Constant.MODULE_NAME.BANK;
  tableConfig!: MBTableConfig;
  searchResult: BankAccount[] = [];

  isImportData = false;
  urlApiImport: string = UrlConstant.IMPORT_FORM.BASIC_FORM;
  statusList: CatalogModel[] = [
    {
      value: 1,
      label: this.translateService.instant('staffManager.label.workIn')
    },
    {
      value: 3,
      label: this.translateService.instant('staffManager.label.workOut')
    },
    {
      value: 2,
      label: this.translateService.instant('staffManager.label.contractPending')
    }
  ];
  subs: Subscription[] = [];
  pagination = new Pagination();

  @ViewChild('searchForm') searchForm!: SearchFormComponent;
  @ViewChild('flagStatusTmpl', {static: true}) flagStatus!: TemplateRef<NzSafeAny>;

    tagIsWorkingColor: string = ConstantColor.TAG.STATUS_IS_WORKING;
    tagSuspenseColor: string = ConstantColor.TAG.STATUS_SUSPENSE;
    tagRetiredColor: string = ConstantColor.TAG.STATUS_RETIRED;

  constructor(private bookmarkFormService: BookmarkFormService,
              private cdr: ChangeDetectorRef,
              private baseService: BaseService,
              private router: Router,
              private translateService: TranslateService,
              private searchFormService: SearchFormService,
              private toastrService: ToastrService,
              public validateService: ValidateService) {
  }

  onDataSelectReady($event: boolean) {
    if ($event) {
      this.getPreviousSearchForm();
      this.doSearch();
    }
  }

  ngOnInit(): void {
    this.initTable();
  }

  ngOnDestroy() {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  getPreviousSearchForm() {
    getPreviousForm(this.searchForm, Constant.LOCAL_STORAGE_KEY.PREVIOUS_SEARCH_FORM.BANK_ACCOUNT_SEARCH);
  }

  doSearch() {
    if(this.searchForm?.form.valid) {
      saveSearchFormToLocalStorage(this.searchForm.form, Constant.LOCAL_STORAGE_KEY.PREVIOUS_SEARCH_FORM.BANK_ACCOUNT_SEARCH);
      const searchParam = this.searchForm ? this.searchForm.parseOptions() : new HttpParams();
      this.searchForm.isLoadingPage = true;
      this.cdr.detectChanges();
      this.searchFormService.getFilterResearch(UrlConstant.SEARCH_FORM.BANK, searchParam, this.pagination.getCurrentPage()).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.searchResult = res.data.listData;
          this.tableConfig.total = res.data.count;
        }
        this.searchForm.isLoadingPage = false;
        this.cdr.detectChanges();
      }, () => {
        this.searchForm.isLoadingPage = false;
        this.cdr.detectChanges();
      });
    }
  }

  search(pageNumber: number) {
    this.pagination.pageNumber = pageNumber;
    this.doSearch();
  }

  getFlagStatus(value: number) {
    return this.statusList.find(status => status.value === value)?.label;
  }
  doImportData() {
    this.isImportData = true;
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'staffManager.staffResearch.bank.table.employeeCode',
          field: 'employeeCode',
          fixed: window.innerWidth > 1024,
          width: 75,
          fixedDir: 'left'
        },
        {
          title: 'staffManager.staffResearch.bank.table.fullName',
          field: 'fullName',
          fixed: window.innerWidth > 1024,
          width: 110,
          fixedDir: 'left'
        },
        {
          title: 'staffManager.staffResearch.bank.table.orgName',
          field: 'orgName',
          width: 200,
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.empTypeName',
          field: 'empTypeName',
          width: 75
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.positionName',
          field: 'jobName',
          width: 120,
        },
        {
          title: 'staffManager.staffResearch.bank.table.accountType',
          field: 'label',
          width: 80,
        },
        {
          title: 'staffManager.staffResearch.bank.table.accountNo',
          field: 'accountNo',
          width: 80,
        },
        {
          title: 'staffManager.staffResearch.bank.table.bankName',
          field: 'bankName',
          width: 100,
          show: false
        },
        {
          title: 'staffManager.staffResearch.bank.table.bankBranch',
          field: 'bankBranch',
          width: 150,
          show: false
        },
        {
          title: 'staffManager.staffResearch.bank.table.isPaymentAccount',
          field: 'isPaymentAccount',
          width: 45,
          pipe: 'yn',
          show: false
        },
        {
          title: 'staffManager.staffResearch.bank.table.flagStatus',
          field: 'flagStatus',
          tdTemplate: this.flagStatus,
          width: 75,
          show: false
        },
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  onClickItem(employee: BankAccount) {
    this.router.navigate(['/staff/personal-info'], {
      queryParams: { employeeId: employee.employeeId }
    });
  }

  doExportData() {
    const searchParam = this.searchForm ? this.searchForm.parseOptions() : new HttpParams();
    this.searchForm.isLoadingPage = true;
    this.searchFormService.doExportFile(UrlConstant.EXPORT_REPORT.BANK_FORM, searchParam).subscribe(res => {
      if (res?.headers?.get('Excel-File-Empty')) {
        this.toastrService.error(this.translateService.instant("common.notification.exportFail"));
      } else {
        const reportFile = new Blob([res.body], {type: getTypeExport('xlsx')});
        saveAs(reportFile, 'thong_tin_tai_khoan.xlsx');
      }
      this.searchForm.isLoadingPage = false;
      this.cdr.detectChanges()
    }, (error) => {
      this.searchForm.isLoadingPage = false;
      this.toastrService.error(error.message);
      this.cdr.detectChanges();
    });
  }
}
