import {Component, OnInit} from '@angular/core';
import {NavigationEnd, Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {Menus} from "@hcm-mfe/staff-manager/data-access/models/research";
import { SessionService } from "@hcm-mfe/shared/common/store";
import {environment} from "@hcm-mfe/shared/environment";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent implements OnInit {
  currentUrl?: string;
  menus: Menus[] = [];
  openMenuMap: { [id: string]: boolean } = {};
  selectedMenu?: Menus;

  constructor(
    private route: Router,
    private http: HttpClient,
    public sessionService: SessionService,
  ) {
    this.getCurrentUrl(this.route);
  }

  ngOnInit(): void {
    this.getMenu();
  }

  getMenu() {
    this.http.get('./assets/fake-data/menu_side_bar.json').subscribe((res: Menus[] | any) => {
      res.forEach((item: any) => {
        if(item.childrens && item.childrens.length > 0) {
          item.childrens = item.childrens.filter((element: any) => this.checkScopeView(element.code));
        }
      });
      this.menus = res;
      this.setOpenMenuMap(this.menus);
    });
  }

  checkScopeView(resource: string): boolean {
    if(environment.isMbBank){
        return this.sessionService.getSessionData(`FUNCTION_${resource}`)?.view;
    } else {
        return true;
    }
  }

  setOpenMenuMap(menus: Menus[]) {
    return menus.some(menu => {
      this.openMenuMap[menu.id] = false;
      if (menu.childrens && menu.childrens.length > 0) {
        this.openMenuMap[menu.id] = this.setOpenMenuMap(menu.childrens);
      }
      if (menu.url === this.currentUrl) {
        this.openMenuMap[menu.id] = true;
        return true;
      }
      return;
    });
  }

  getCurrentUrl(router: Router) {
    router.events.subscribe(e => {
      if (e instanceof NavigationEnd) {
        this.currentUrl = e.url;
      }
    });
  }

  openHandler(value: number): void {
    for (const key in this.openMenuMap) {
      if (key !== value.toString()) {
        this.openMenuMap[key] = false;
      }
    }
    this.setParentMenuOpen(value, this.menus);
  }

  setParentMenuOpen(value: number, menus: Menus[]) {
    return menus.some(menu => {
      this.openMenuMap[menu.id] = false;
      if (menu.childrens && menu.childrens.length > 0) {
        this.openMenuMap[menu.id] = this.setParentMenuOpen(value, menu.childrens);
      }
      if (menu.id === value) {
        this.openMenuMap[menu.id] = true;
        return true;
      }
      return;
    });
  }

}
