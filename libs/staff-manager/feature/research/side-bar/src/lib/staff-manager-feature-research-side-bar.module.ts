import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SideBarComponent} from "./side-bar/side-bar.component";
import {NzMenuModule} from "ng-zorro-antd/menu";
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [CommonModule, NzMenuModule, RouterModule],
  declarations: [SideBarComponent],
  exports: [SideBarComponent],
})
export class StaffManagerFeatureResearchSideBarModule {}
