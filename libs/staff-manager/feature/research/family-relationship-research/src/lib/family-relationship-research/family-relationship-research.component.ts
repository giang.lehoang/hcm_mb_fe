import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { HttpParams } from '@angular/common/http';
import { saveAs } from 'file-saver';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { ToastrService } from 'ngx-toastr';

import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {
  Constant,
  ConstantColor,
  getPreviousForm,
  saveSearchFormToLocalStorage,
  UrlConstant
} from "@hcm-mfe/staff-manager/data-access/common";
import {CatalogModel, MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {SearchFormComponent} from "@hcm-mfe/staff-manager/ui/search-form-research";
import {BookmarkFormService, SearchFormService} from "@hcm-mfe/staff-manager/data-access/services";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {ValidateService} from "@hcm-mfe/shared/core";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {getTypeExport} from "@hcm-mfe/shared/common/utils";
import {Relatives} from "@hcm-mfe/staff-manager/data-access/models/research";

@Component({
  selector: 'app-relatives-research',
  templateUrl: './family-relationship-research.component.html',
  styleUrls: ['./family-relationship-research.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FamilyRelationshipResearchComponent implements OnInit {

  functionCode: string = FunctionCode.HR_FAMILY_RELATIONSHIP;

  public readonly MODULENAME = Constant.MODULE_NAME.FAMILY_RELATIONSHIPS;
  tableConfig!: MBTableConfig;
  searchResult: Relatives[] = [];

  isImportData = false;
  urlApiImport: string = UrlConstant.IMPORT_FORM.BASIC_FORM;
  statusList: CatalogModel[] = [
    {
      value: 1,
      label: this.translateService.instant('staffManager.label.workIn')
    },
    {
      value: 3,
      label: this.translateService.instant('staffManager.label.workOut')
    },
    {
      value: 2,
      label: this.translateService.instant('staffManager.label.contractPending')
    }
  ];
  subs: Subscription[] = [];
  pagination = new Pagination();

  @ViewChild('searchForm') searchForm!: SearchFormComponent;
  @ViewChild('flagStatusTmpl', {static: true}) flagStatus!: TemplateRef<NzSafeAny>;
  @ViewChild('isInCompanyTmpl', {static: true}) isInCompany!: TemplateRef<NzSafeAny>;

    tagIsWorkingColor: string = ConstantColor.TAG.STATUS_IS_WORKING;
    tagSuspenseColor: string = ConstantColor.TAG.STATUS_SUSPENSE;
    tagRetiredColor: string = ConstantColor.TAG.STATUS_RETIRED;

  constructor(private bookmarkFormService: BookmarkFormService,
              private baseService: BaseService,
              private router: Router,
              private cdr: ChangeDetectorRef,
              private translateService: TranslateService,
              private searchFormService: SearchFormService,
              private toastrService: ToastrService,
              public validateService: ValidateService) {}

  onDataSelectReady($event: boolean) {
    if ($event) {
      this.getPreviousSearchForm();
      this.doSearch();
    }
  }

  ngOnInit(): void {
    this.initTable();
  }

  ngOnDestroy() {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  getPreviousSearchForm() {
    getPreviousForm(this.searchForm, Constant.LOCAL_STORAGE_KEY.PREVIOUS_SEARCH_FORM.FAMILY_RELATIONSHIP_SEARCH);
  }

  doSearch() {
    if(this.searchForm?.form.valid) {
      saveSearchFormToLocalStorage(this.searchForm.form, Constant.LOCAL_STORAGE_KEY.PREVIOUS_SEARCH_FORM.FAMILY_RELATIONSHIP_SEARCH);
      this.searchForm.isLoadingPage = true;
      const searchParam = this.searchForm ? this.searchForm.parseOptions() : new HttpParams();

      this.searchFormService.getFilterResearch(UrlConstant.SEARCH_FORM.FAMILY_RELATIONSHIPS, searchParam, this.pagination.getCurrentPage()).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.searchResult = res.data.listData;
          this.tableConfig.total = res.data.count;
        }
        this.searchForm.isLoadingPage = false;
        this.cdr.detectChanges();
      }, error => {
        this.searchForm.isLoadingPage = false;
        this.cdr.detectChanges();
      });
    }
  }

  search(pageNumber: number) {
    this.pagination.pageNumber = pageNumber;
    this.doSearch();
  }

  doImportData() {
    this.isImportData = true;
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'staffManager.staffResearch.relatives.table.employeeCode',
          field: 'employeeCode',
          width: 120,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left'
        },
        {
          title: 'staffManager.staffResearch.relatives.table.employeeName',
          field: 'employeeName',
          width: 150,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left'
        },
        {
          title: 'staffManager.staffResearch.relatives.table.orgName',
          field: 'orgName',
          width: 200
        },
        {
          title: 'staffManager.staffResearch.relatives.table.empTypeName',
          field: 'empTypeName',
          width: 120
        },
        {
          title: 'staffManager.staffResearch.relatives.table.jobName',
          field: 'jobName',
          width: 120
        },
        {
          title: 'staffManager.staffResearch.relatives.table.relationTypeCode',
          field: 'relationTypeCode',
          width: 120
        },
        {
          title: 'staffManager.staffResearch.relatives.table.fullName',
          field: 'fullName',
          width: 150
        },
        {
          title: 'staffManager.staffResearch.relatives.table.dateOfBirth',
          field: 'dateOfBirth',
          width: 100,
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        },
        {
          title: 'staffManager.staffResearch.relatives.table.isEmpMB',
          field: 'referenceEmployeeCode',
          width: 100
        },
        {
          title: 'staffManager.staffResearch.relatives.table.relationStatusCode',
          field: 'relationStatusCode',
          width: 130
        },
        {
          title: 'staffManager.staffResearch.relatives.table.policyTypeCode',
          field: 'policyTypeName',
          width: 130
        },
        {
          title: 'staffManager.staffResearch.relatives.table.personalIdNumber',
          field: 'personalIdNumber',
          width: 120,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          show: false
        },
        {
          title: 'staffManager.staffResearch.relatives.table.phoneNumber',
          field: 'phoneNumber',
          width: 100,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          show: false
        },
        {
          title: 'staffManager.staffResearch.relatives.table.workOrganization',
          field: 'workOrganization',
          width: 150,
          show: false
        },
        {
          title: 'staffManager.staffResearch.relatives.table.flagStatus',
          field: 'flagStatus',
          tdTemplate: this.flagStatus,
          width: 100,
          show: false
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  getFlagStatus(value: number) {
    return this.statusList.find(status => status.value === value)?.label;
  }

  onClickItem(employee: Relatives) {
    this.router.navigate(['/staff/personal-info'], {
      queryParams: { employeeId: employee.employeeId }
    });
  }

  doExportData() {
    const searchParam = this.searchForm ? this.searchForm.parseOptions() : new HttpParams();
    this.searchForm.isLoadingPage = true;
    this.searchFormService.doExportFile(UrlConstant.EXPORT_REPORT.RELATIVES_FORM, searchParam).subscribe(res => {
      if ((res?.headers?.get('Excel-File-Empty'))) {
        this.toastrService.error(this.translateService.instant("common.notification.exportFail"));
      } else {
        const reportFile = new Blob([res.body], {type: getTypeExport('xlsx')});
        saveAs(reportFile, 'thong_tin_nhan_than.xlsx');
      }
      this.searchForm.isLoadingPage = false;
      this.cdr.detectChanges();
    }, (error) => {
      this.searchForm.isLoadingPage = false;
      this.toastrService.error(error.message);
      this.cdr.detectChanges();
    });
  }
}
