import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {StaffManagerUiSearchFormResearchModule} from "@hcm-mfe/staff-manager/ui/search-form-research";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {NzTagModule} from "ng-zorro-antd/tag";
import {NzFormModule} from "ng-zorro-antd/form";
import {BasicResearchComponent} from "./basic-research/basic-research.component";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {StaffManagerUiImportEmployeeResearchModule} from "@hcm-mfe/staff-manager/ui/import-employee-research";
import {RouterModule} from "@angular/router";
import {NzButtonModule} from "ng-zorro-antd/button";

@NgModule({
    imports: [CommonModule, StaffManagerUiSearchFormResearchModule, SharedUiMbButtonModule, TranslateModule,
        SharedUiMbTableWrapModule, SharedUiMbTableModule, NzTagModule, NzFormModule, SharedUiLoadingModule, StaffManagerUiImportEmployeeResearchModule,
        RouterModule.forChild([
            {
                path: '',
                component: BasicResearchComponent
            }
        ]), NzButtonModule
    ],
  declarations: [BasicResearchComponent],
  exports: [BasicResearchComponent],
})
export class StaffManagerFeatureResearchBasicResearchModule {}
