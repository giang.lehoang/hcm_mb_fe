import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild
} from '@angular/core';
import {Subscription} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {NzSafeAny} from 'ng-zorro-antd/core/types';
import {Router} from '@angular/router';
import {HttpParams} from '@angular/common/http';
import {ToastrService} from 'ngx-toastr';
import {saveAs} from 'file-saver';

import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {
  Constant,
  ConstantColor,
  getPreviousForm,
  saveSearchFormToLocalStorage,
  UrlConstant
} from "@hcm-mfe/staff-manager/data-access/common";
import {AppFunction, CatalogModel, MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {SearchFormComponent} from "@hcm-mfe/staff-manager/ui/search-form-research";
import {BookmarkFormService, SearchFormService} from "@hcm-mfe/staff-manager/data-access/services";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {ValidateService} from "@hcm-mfe/shared/core";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {getTypeExport} from "@hcm-mfe/shared/common/utils";
import {PersonalInformation} from "@hcm-mfe/staff-manager/data-access/models/research";
import {SessionService} from "@hcm-mfe/shared/common/store";

@Component({
  selector: 'app-basic-research',
  templateUrl: './basic-research.component.html',
  styleUrls: ['./basic-research.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BasicResearchComponent implements OnInit, OnDestroy {
  objFunction: AppFunction;
  public readonly MODULE_NAME = Constant.MODULE_NAME.BASIC_RESEARCH;
  functionCode: string = FunctionCode.HR_PERSONAL_INFO;
  isLoadingPage = false;
  tableConfig!: MBTableConfig;
  searchResult: PersonalInformation[] = [];
  subs: Subscription[] = [];
  isImportData = false;
  urlApiImport: string = UrlConstant.IMPORT_FORM.BASIC_FORM;
  urlApiDownloadTemp: string = UrlConstant.IMPORT_FORM.BASIC_TEMPLATE;
  statusList: CatalogModel[] = [
    {
      value: 1,
      label: this.translateService.instant('staffManager.label.workIn')
    },
    {
      value: 3,
      label: this.translateService.instant('staffManager.label.workOut')
    },
    {
      value: 2,
      label: this.translateService.instant('staffManager.label.contractPending')
    }
  ];
  pagination = new Pagination();

  @ViewChild('flagStatusTmpl', {static: true}) flagStatus!: TemplateRef<NzSafeAny>;
  @ViewChild('searchForm') searchForm!: SearchFormComponent;

  tagIsWorkingColor: string = ConstantColor.TAG.STATUS_IS_WORKING;
  tagSuspenseColor: string = ConstantColor.TAG.STATUS_SUSPENSE;
  tagRetiredColor: string = ConstantColor.TAG.STATUS_RETIRED;

  constructor(
    private bookmarkFormService: BookmarkFormService,
    private cdr: ChangeDetectorRef,
    private baseService: BaseService,
    private router: Router,
    private toastService: ToastrService,
    private translateService: TranslateService,
    private searchFormService: SearchFormService,
    public validateService: ValidateService,
    public sessionService: SessionService,
  ) {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.HR_PERSONAL_INFO}`);
  }

  ngOnInit(): void {
    this.initTable();
  }

  ngOnDestroy() {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  onDataSelectReady($event: boolean) {
    if ($event) {
      this.getPreviousSearchForm();
      this.doSearch();
    }
  }

  getPreviousSearchForm() {
    getPreviousForm(this.searchForm, Constant.LOCAL_STORAGE_KEY.PREVIOUS_SEARCH_FORM.BASIC_SEARCH);
  }

  doSearch() {
    if (this.searchForm?.form.valid) {
      saveSearchFormToLocalStorage(this.searchForm.form, Constant.LOCAL_STORAGE_KEY.PREVIOUS_SEARCH_FORM.BASIC_SEARCH);
      const searchParam = this.searchForm ? this.searchForm.parseOptions() : new HttpParams();
      this.searchForm.isLoadingPage = true;
      this.cdr.detectChanges();
      this.isLoadingPage = true;
      this.searchFormService.getFilterResearch(UrlConstant.SEARCH_FORM.BASIC_FORM, searchParam, this.pagination.getCurrentPage()).subscribe(res => {
        this.isLoadingPage = false;
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.searchResult = res.data.listData;
          this.tableConfig.total = res.data.count;
        }
        this.searchForm.isLoadingPage = false;
        this.cdr.detectChanges()
      }, () => {
        this.searchForm.isLoadingPage = false;
        this.cdr.detectChanges()
      });
    }
  }

  search(pageNumber: number) {
    this.pagination.pageNumber = pageNumber;
    this.doSearch();
  }

  doImportData() {
    this.isImportData = true;
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'staffManager.staffResearch.personalInformation.table.employeeCode',
          field: 'employeeCode',
          width: 120,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left'
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.oldEmpCode',
          field: 'oldEmpCode',
          width: 120,
          show:false
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.fullName',
          field: 'fullName',
          width: 150,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left'
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.empTypeName',
          field: 'empTypeName',
          width: 120
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.positionName',
          field: 'jobName',
          width: 120,
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.orgName',
          field: 'orgName',
          width: 200
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.dateOfBirth',
          field: 'dateOfBirth',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 100
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.email',
          field: 'email',
          width: 150
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.mobileNumber',
          field: 'mobileNumber',
          width: 100
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.genderName',
          field: 'genderName',
          width: 80,
          show: false
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.ethnicName',
          field: 'ethnicName',
          width: 100,
          show: false
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.religionName',
          field: 'religionName',
          width: 100,
          show: false
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.personalId',
          field: 'personalId',
          width: 100,
          show: false
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.insuranceNo',
          field: 'insuranceNo',
          width: 100,
          show: false
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.religionName',
          field: 'religionName',
          width: 100,
          show: false
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.taxNo',
          field: 'taxNo',
          width: 100,
          show: false
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.isInsuranceMb',
          field: 'isInsuranceMb',
          width: 100,
          pipe: 'yn',
          show: false
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.placeOfBirth',
          field: 'placeOfBirth',
          width: 100,
          show: false
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.maritalStatusName',
          field: 'maritalStatusName',
          width: 100,
          show: false
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.pernamentAddress',
          field: 'pernamentAddress',
          width: 200,
          show: false
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.currentAddress',
          field: 'currentAddress',
          width: 200,
          show: false
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.originalAddress',
          field: 'originalAddress',
          width: 200,
          show: false
        },
        {
          title: 'staffManager.label.joinCompanyDate',
          field: 'joinCompanyDate',
          width: 100,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          show: false
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.partyDate',
          field: 'partyDate',
          width: 100,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          show: false
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.partyOfficialDate',
          field: 'partyOfficialDate',
          width: 100,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          show: false
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.flagStatus',
          field: 'flagStatus',
          tdTemplate: this.flagStatus,
          width: 100,
          show: false
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  getFlagStatus(value: number) {
    return this.statusList.find(status => status.value === value)?.label;
  }

  onClickItem(employee: PersonalInformation) {
    this.router.navigate(['/staff/personal-info'], {
      queryParams: {employeeId: employee.employeeId, employeeCode: employee.employeeCode}
    }).then();
  }

  doCloseImport() {
    this.isImportData = false;
  }

  doExportData() {
    const searchParam = this.searchForm ? this.searchForm.parseOptions() : new HttpParams();
    this.searchForm.isLoadingPage = true;
    this.searchFormService.doExportFile(UrlConstant.EXPORT_REPORT.BASIC_FORM, searchParam).subscribe(res => {
      if (res?.headers?.get('Excel-File-Empty')) {
        this.toastService.error(this.translateService.instant("common.notification.exportFail"));
      } else {
        const reportFile = new Blob([res.body], {type: getTypeExport('xlsx')});
        saveAs(reportFile, 'thong_tin_co_ban.xlsx');
      }
      this.searchForm.isLoadingPage = false;
      this.cdr.detectChanges();
    }, (error) => {
      this.searchForm.isLoadingPage = false;
      this.cdr.detectChanges();
      this.toastService.error(error.message);
    });
  }

}
