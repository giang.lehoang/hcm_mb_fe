import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, EventEmitter,
  OnInit,
  Output,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import {MBTableConfig} from "@hcm-mfe/shared/data-access/models";

export class HeaderTable {
  width?: string;
  title?: string;
  titleValue?: string;
  key?: string;
  type?: 'string' | 'date' | 'currency' | 'number' | 'boolean' | 'stt' | null;
  class?: string;
  formatDate?: string = 'dd/MM/yyyy';
  formatCurrency?: string = 'VND';
  hidden? = false;
}

@Component({
  selector: 'app-result-table',
  templateUrl: './result-table.component.html',
  styleUrls: ['./result-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResultTableComponent implements OnInit {
  @Output() onClickItem: EventEmitter<number> = new EventEmitter<number>();
  dataList: NzSafeAny[] = [];
  tableConfig!: MBTableConfig;

  @ViewChild('highLightTpl', { static: true }) highLightTpl!: TemplateRef<NzSafeAny>;
  @ViewChild('operationTpl', { static: true }) operationTpl!: TemplateRef<NzSafeAny>;

  constructor(
    private cdr: ChangeDetectorRef
  ) {
  }

  ngOnInit(): void {
    this.initTable();
    this.getDataList();
  }

  changePageSize(e: number): void {
    this.tableConfig.pageSize = e;
  }

  getDataList(): void {
    // console.log(e);
    this.tableConfig.loading = true;
    this.dataList = [];
    const index = (this.tableConfig.pageIndex - 1) * this.tableConfig.pageSize + 1;
    setTimeout(() => {
      this.dataList = [
        {
          id: index,
          vitri: 'Giám đốc',
          ma_nv: '0001.12321.444',
          ten_nv: 'Nguyễn Văn A',
          don_vi: 'Khối CNTT',
          ngay_sinh: '1650176126164',
          luong: 80000000,
          staff: { info: { email: 'tranvana@gmail.com' } }
        },
        {
          id: index + 1,
          vitri: 'Nhân viên',
          ma_nv: '0001.12321.223',
          ten_nv: 'Nguyễn Văn B',
          don_vi: 'Khối tài chính',
          ngay_sinh: '1650176126164',
          luong: 70000000,
          staff: { info: { email: 'levanc@gmail.com' } }
        },
        {
          id: index + 2,
          vitri: 'Nhân viên',
          ma_nv: '0001.21123.444',
          ten_nv: 'Nguyễn Văn C',
          don_vi: 'Khối ngân hàng',
          ngay_sinh: '1650176126164',
          luong: 60000000,
          staff: { info: { email: 'kieuhoi@gmail.com' } }
        },
        {
          id: index + 3,
          vitri: 'Thư ký',
          ma_nv: '0101.11123.444',
          ten_nv: 'Nguyễn Văn D',
          don_vi: 'Khối CNTT',
          luong: 40000000,
          ngay_sinh: '1650176126164',
          staff: { info: { email: 'dantri@gmail.com' } }
        },
        {
          id: index + 4,
          vitri: 'Nhân viên',
          ma_nv: '0001.12321.444',
          ten_nv: 'Nguyễn Văn E',
          don_vi: 'Khối ATTT',
          ngay_sinh: '1650176126164',
          luong: 20000000,
          staff: { info: { email: 'intructable@gmail.com' } }
        },
        {
          id: index + 5,
          vitri: 'Bảo vệ',
          ma_nv: '0001.12331.444',
          ten_nv: 'Nguyễn Văn F',
          don_vi: 'Khối Nhân sự',
          // ngay_sinh: '1650176126164',
          luong: 'asdasd',
          staff: { info: { email: 'tranvana@gmail.com' } }
        }
      ];
      this.tableConfig.total = 18;
      // this.tableConfig.pageIndex = e;
      // this.checkedCashArray = [...this.checkedCashArray];
      this.dataList = [...this.dataList];
      this.tableConfig.loading = false;
      this.cdr.detectChanges();
    });
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'Chức vụ',
          field: 'vitri',
          show: false
        },
        {
          title: 'Mã nhân viên',
          field: 'ma_nv',
          tdClassList: ['text-nowrap']
        },
        {
          title: 'Tên nhân viên',
          field: 'ten_nv',
          // notNeedEllipsis: true,
          tdClassList: ['text-nowrap']
        },
        {
          title: 'Email',
          field: 'staff.info.email',
          tdClassList: ['text-nowrap']
        },
        {
          title: 'Ngày sinh',
          field: 'ngay_sinh',
          pipe: 'date',
          tdClassList: ['text-nowrap', 'text-right'],
          thClassList: ['text-nowrap', 'text-right']
        },
        {
          title: 'Tiền lương',
          field: 'luong',
          pipe: 'currency: VND',
          tdClassList: ['text-nowrap', 'text-right'],
          thClassList: ['text-nowrap', 'text-right']
        },
        {
          title: 'Đơn vị',
          field: 'don_vi',
          tdTemplate: this.highLightTpl
        },
        {
          title: 'Hành động',
          tdTemplate: this.operationTpl,
          fixed: window.innerWidth > 1024,
          fixedDir: 'right'
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: 6,
      pageIndex: 1
    };
  }

  test($event: string) {
    // console.log($event);
  }

  onClickNode(employeeId: number) {
    this.onClickItem.emit(employeeId);
  }
}
