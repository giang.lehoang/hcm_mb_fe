import { Component, OnInit } from '@angular/core';
import {NzModalRef} from "ng-zorro-antd/modal";
import {TranslateService} from "@ngx-translate/core";
import { BaseResponse } from '../../../../../core/models/base-response';
import { Pagination } from '../../../../../shared/model/pagination';
import { ValidateService } from '../../../../../shared/services/common-utils.service';

export class HeaderTable {
  width?: string;
  title?: string;
  titleValue?: string;
  key?: string;
  type?: 'string' | 'date' | 'currency' | 'number' | 'boolean' | 'stt' | null;
  class?: string;
  formatDate?: string = 'dd/MM/yyyy';
  formatCurrency?: string = 'VND';
  hidden? = false;
}

@Component({
  selector: 'app-result-table',
  templateUrl: './result-table.component.html',
  styleUrls: ['./result-table.component.scss']
})
export class ResultTableComponent implements OnInit {
  loading: boolean | undefined;
  response: BaseResponse = new BaseResponse();
  employeeId = 1;
  modal: NzModalRef | undefined;
  count = 2;
  pagination = new Pagination();
  isVisibleFilter = false;
  headerTable: HeaderTable[] = [];
  headerTableShow: HeaderTable[] = [];

  constructor(
    public validateService: ValidateService,
    private translate: TranslateService
  ) { }

  ngOnInit(): void {
    this.headerTable = [
      {
        class: 'text-center',
        title: 'staffManager.table.stt',
        titleValue: this.translate.instant('staffManager.table.stt'),
        type: "stt",
        key: 'stt',
        width: '80px'
      },
      {
        title: 'staffManager.table.courseName',
        titleValue: this.translate.instant('staffManager.table.courseName'),
        type: "string",
        key: 'courseName',
        hidden: false
      },
      {
        title: 'staffManager.table.eduForm',
        titleValue: this.translate.instant('staffManager.table.eduForm'),
        type: "string",
        key: 'eduForm',
        hidden: false
      },
      {
        title: 'staffManager.table.eduContent',
        titleValue: this.translate.instant('staffManager.table.eduContent'),
        type: "string",
        key: 'eduContent',
        hidden: false
      },
      {
        title: 'staffManager.table.eduFromDate',
        titleValue: this.translate.instant('staffManager.table.eduFromDate'),
        type: "date",
        key: 'eduFromDate'
      },
      {
        title: 'staffManager.table.eduToDate',
        titleValue: this.translate.instant('staffManager.table.eduToDate'),
        type: "date",
        key: 'eduToDate',
        formatDate: 'yyyy-MM-dd',
        hidden: true
      }
    ];
    this.translate.onLangChange.subscribe(res => {
     this.headerTable.forEach(item => {
       item.titleValue = this.translate.instant(item.title);
     });
    });
    this.headerTableShow = this.headerTable.filter(item => (item.hidden == undefined || item.hidden === false));
  }

  getDataTable() {

  }

  test($event: string) {
    // console.log($event);
  }
}
