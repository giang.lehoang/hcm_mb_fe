import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {NzSpaceModule} from "ng-zorro-antd/space";
import {ResultTableComponent} from "./result-table/result-table.component";
import {NzBadgeModule} from "ng-zorro-antd/badge";

@NgModule({
  imports: [CommonModule, SharedUiMbTableWrapModule, SharedUiMbTableModule, NzSpaceModule, NzBadgeModule],
  declarations: [ResultTableComponent],
  exports: [ResultTableComponent],
})
export class StaffManagerFeatureResearchResultTableModule {}
