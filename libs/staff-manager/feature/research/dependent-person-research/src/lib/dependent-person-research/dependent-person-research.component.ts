import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { HttpParams } from '@angular/common/http';
import { saveAs } from 'file-saver';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { ToastrService } from 'ngx-toastr';

import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {
  Constant,
  ConstantColor,
  getPreviousForm,
  saveSearchFormToLocalStorage,
  UrlConstant
} from "@hcm-mfe/staff-manager/data-access/common";
import {CatalogModel, MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {SearchFormComponent} from "@hcm-mfe/staff-manager/ui/search-form-research";
import {BookmarkFormService, SearchFormService} from "@hcm-mfe/staff-manager/data-access/services";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {ValidateService} from "@hcm-mfe/shared/core";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {getTypeExport} from "@hcm-mfe/shared/common/utils";
import {FamilyDeductions} from "@hcm-mfe/staff-manager/data-access/models/research";

@Component({
  selector: 'app-family-deductions-research',
  templateUrl: './dependent-person-research.component.html',
  styleUrls: ['./dependent-person-research.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DependentPersonResearchComponent implements OnInit {

  functionCode: string = FunctionCode.HR_DEPENDENT_PERSON;

  public readonly MODULENAME = Constant.MODULE_NAME.DEPENDENT_PERSONS;
  tableConfig!: MBTableConfig;
  searchResult: FamilyDeductions[] = [];

  isImportData = false;
  urlApiImport: string = UrlConstant.IMPORT_FORM.BASIC_FORM;
  statusList: CatalogModel[] = [
    {
      value: 1,
      label: this.translateService.instant('staffManager.label.workIn')
    },
    {
      value: 3,
      label: this.translateService.instant('staffManager.label.workOut')
    },
    {
      value: 2,
      label: this.translateService.instant('staffManager.label.contractPending')
    }
  ];
  subs: Subscription[] = [];
  pagination = new Pagination();

  @ViewChild('searchForm') searchForm!: SearchFormComponent;
  @ViewChild('flagStatusTmpl', {static: true}) flagStatus!: TemplateRef<NzSafeAny>;

    tagIsWorkingColor: string = ConstantColor.TAG.STATUS_IS_WORKING;
    tagSuspenseColor: string = ConstantColor.TAG.STATUS_SUSPENSE;
    tagRetiredColor: string = ConstantColor.TAG.STATUS_RETIRED;

  constructor(private bookmarkFormService: BookmarkFormService,
              private baseService: BaseService,
              private router: Router,
              private cdr: ChangeDetectorRef,
              private translateService: TranslateService,
              private searchFormService: SearchFormService,
              private toastrService: ToastrService,
              public validateService: ValidateService) {
  }

  onDataSelectReady($event: boolean) {
    if ($event) {
      this.getPreviousSearchForm();
      this.doSearch();
    }
  }

  ngOnInit(): void {
    this.initTable();
  }

  ngOnDestroy() {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  getPreviousSearchForm() {
    getPreviousForm(this.searchForm, Constant.LOCAL_STORAGE_KEY.PREVIOUS_SEARCH_FORM.DEPENDENT_PERSON_SEARCH);
  }

  doSearch() {
    if(this.searchForm?.form.valid) {
      saveSearchFormToLocalStorage(this.searchForm.form, Constant.LOCAL_STORAGE_KEY.PREVIOUS_SEARCH_FORM.DEPENDENT_PERSON_SEARCH);
      this.searchForm.isLoadingPage = true;
      const searchParam = this.searchForm ? this.searchForm.parseOptions() : new HttpParams();
      this.cdr.detectChanges();
      this.searchFormService.getFilterResearch(UrlConstant.SEARCH_FORM.DEPENDENT_PERSONS, searchParam, this.pagination.getCurrentPage()).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.searchResult = res.data.listData;
          this.tableConfig.total = res.data.count;
        }
        this.searchForm.isLoadingPage = false;
        this.cdr.detectChanges();
      }, () => {
        this.searchForm.isLoadingPage = false;
        this.cdr.detectChanges();
      });
    }
  }

  search(pageNumber: number) {
    this.pagination.pageNumber = pageNumber;
    this.doSearch();
  }

  doImportData() {
    this.isImportData = true;
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'staffManager.staffResearch.familyDeductions.table.employeeCode',
          field: 'employeeCode',
          fixed: window.innerWidth > 1024,
          width: 120,
          fixedDir: 'left'
        },
        {
          title: 'staffManager.staffResearch.familyDeductions.table.employeeName',
          field: 'employeeName',
          fixed: window.innerWidth > 1024,
          width: 150,
          fixedDir: 'left'
        },
        {
          title: 'staffManager.staffResearch.familyDeductions.table.orgName',
          field: 'orgName',
          width: 200
        },
        {
          title: 'staffManager.staffResearch.familyDeductions.table.empTypeName',
          field: 'empTypeName',
          width: 120
        },
        {
          title: 'staffManager.staffResearch.familyDeductions.table.jobName',
          field: 'jobName',
          width: 120
        },
        {
          title: 'staffManager.staffResearch.familyDeductions.table.relationTypeName',
          field: 'relationTypeName',
          width: 90
        },
        {
          title: 'staffManager.staffResearch.familyDeductions.table.fullName',
          field: 'fullName',
          width: 150
        },
        {
          title: 'staffManager.staffResearch.familyDeductions.table.dependentPersonalId',
          field: 'dependentPersonalId',
          width: 120,
          show: false
        },
        {
          title: 'staffManager.staffResearch.familyDeductions.table.nationName',
          field: 'nationName',
          width: 100,
          show: false
        },
        {
          title: 'staffManager.staffResearch.familyDeductions.table.taxNumber',
          field: 'taxNumber',
          width: 100,
          show: false
        },
        {
          title: 'staffManager.staffResearch.familyDeductions.table.codeNo',
          field: 'codeNo',
          width: 100,
          show: false
        },
        {
          title: 'staffManager.staffResearch.familyDeductions.table.bookNo',
          field: 'bookNo',
          width: 100,
          show: false
        },
        {
          title: 'staffManager.staffResearch.familyDeductions.table.dateOfBirth',
          field: 'dateOfBirth',
          width: 100,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
        },
        {
          title: 'staffManager.staffResearch.familyDeductions.table.fromDate',
          field: 'fromDate',
          width: 100,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
        },
        {
          title: 'staffManager.staffResearch.familyDeductions.table.toDate',
          field: 'toDate',
          width: 100,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
        },
        {
          title: 'staffManager.staffResearch.familyDeductions.table.flagStatus',
          field: 'flagStatus',
          width: 100,
          tdTemplate: this.flagStatus,
          show: false
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  onClickItem(employee: FamilyDeductions) {
    this.router.navigate(['/staff/personal-info'], {
      queryParams: { employeeId: employee.employeeId }
    });
  }

  doExportData() {
    const searchParam = this.searchForm ? this.searchForm.parseOptions() : new HttpParams();
    this.searchForm.isLoadingPage = true;
    this.searchFormService.doExportFile(UrlConstant.EXPORT_REPORT.FAMILY_FORM, searchParam).subscribe(res => {
      if ((res?.headers?.get('Excel-File-Empty'))) {
        this.toastrService.error(this.translateService.instant("common.notification.exportFail"));
      } else {
        const reportFile = new Blob([res.body], {type: getTypeExport('xlsx')});
        saveAs(reportFile, 'giam_tru_gia_canh.xlsx');
      }
      this.searchForm.isLoadingPage = false;
      this.cdr.detectChanges();
    }, (error) => {
      this.searchForm.isLoadingPage = false;
      this.toastrService.error(error.message);
      this.cdr.detectChanges();
    });
  }

  getFlagStatus(value: number) {
    return this.statusList.find(status => status.value === value)?.label;
  }
}
