import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {StaffManagerUiSearchFormResearchModule} from "@hcm-mfe/staff-manager/ui/search-form-research";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {NzTagModule} from "ng-zorro-antd/tag";
import {NzFormModule} from "ng-zorro-antd/form";
import {DependentPersonResearchComponent} from "./dependent-person-research/dependent-person-research.component";
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [CommonModule, StaffManagerUiSearchFormResearchModule, SharedUiMbButtonModule, TranslateModule,
    SharedUiMbTableWrapModule, SharedUiMbTableModule, NzTagModule, NzFormModule,
    RouterModule.forChild([
      {
        path: '',
        component: DependentPersonResearchComponent
      }
    ])
  ],
  declarations: [DependentPersonResearchComponent],
  exports: [DependentPersonResearchComponent],
})
export class StaffManagerFeatureResearchDependentPersonResearchModule {}
