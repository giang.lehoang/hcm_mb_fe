import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {StaffDetailsComponent} from "./staff-details/staff-details.component";
import {NzFormModule} from "ng-zorro-antd/form";
import {SharedUiMbTextLabelModule} from "@hcm-mfe/shared/ui/mb-text-label";
import {TranslateModule} from "@ngx-translate/core";
import {NzTagModule} from "ng-zorro-antd/tag";

@NgModule({
  imports: [CommonModule, NzFormModule, SharedUiMbTextLabelModule, TranslateModule, NzTagModule],
  declarations: [StaffDetailsComponent],
  exports: [StaffDetailsComponent],
})
export class StaffManagerFeatureDetailsModule {}
