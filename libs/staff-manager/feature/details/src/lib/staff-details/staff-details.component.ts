import {Component, OnInit} from "@angular/core";
import {TranslateService} from "@ngx-translate/core";
import {PersonalInformation} from "@hcm-mfe/staff-manager/data-access/models/research";
import {EmployeesService} from "@hcm-mfe/shared/core";
import {CatalogModel} from "@hcm-mfe/shared/data-access/models";

@Component({
  selector: 'staff-details-component',
  templateUrl: './staff-details.component.html',
  styleUrls: ['./staff-details.component.scss']
})
export class StaffDetailsComponent implements OnInit {
  data = new PersonalInformation();
  avtBase64: string | ArrayBuffer = 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';
  qrCodeBase64: string | any;
  statusList: CatalogModel[] = [
    {
      value: 1,
      label: this.translateService.instant('staffManager.label.workIn')
    },
    {
      value: 0,
      label: this.translateService.instant('staffManager.label.workOut')
    }
  ];
  constructor(
    private employeesService: EmployeesService,
    private translateService: TranslateService,
  ) {
  }
  ngOnInit(): void {
    this.getAvatar(this.data.employeeId).then(r => this.avtBase64 = r);
    this.getQRCode(this.data.employeeId, 200, 200).then(r => this.qrCodeBase64 = r);
  }

  getFlagStatus(value: number) {
    return this.statusList.find(status => status.value === value)?.label;
  }

  async getAvatar(employeeId: number) {
    let res = await this.employeesService.getAvatar(employeeId).toPromise();
    return res?.data ? 'data:image/jpg;base64,' + res?.data : this.avtBase64;
  }

  async getQRCode(employeeId: number, width: number, height: number) {
    let res = await this.employeesService.getQrCode(employeeId, width, height).toPromise();
    return res?.data ? 'data:image/png;base64,' + res?.data : null;
  }
}
