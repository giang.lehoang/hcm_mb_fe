import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {StaffManagerUiRecruitSearchFormRecruitModule} from "@hcm-mfe/staff-manager/ui/recruit/search-form-recruit";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {StaffManagerUiRecruitImportFileModule} from "@hcm-mfe/staff-manager/ui/recruit/import-file";
import {NzTagModule} from "ng-zorro-antd/tag";
import {TranslateModule} from "@ngx-translate/core";
import {RouterModule} from "@angular/router";
import {ApprovalRecruitmentSearchComponent} from "./approval-recruitment-search/approval-recruitment-search.component";
import {SharedUiMbTableMergeCellWrapModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell-wrap";
import {SharedUiMbTableMergeCellModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell";
import {EditRecruitmentNewComponent} from './edit-recruitment-new/edit-recruitment-new.component';
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiOrgDataPickerModule} from "@hcm-mfe/shared/ui/org-data-picker";
import {SharedUiMbUploadModule} from "@hcm-mfe/shared/ui/mb-upload";
import {SharedDirectivesNumberInputModule} from "@hcm-mfe/shared/directives/number-input";
import {StaffManagerUiRejectCommonModule} from "@hcm-mfe/staff-manager/ui/reject-common";
import { EditRehireOsEmployeeComponent } from './edit-rehire-os-employee/edit-rehire-os-employee.component';
import {SharedUiMbEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/mb-employee-data-picker";
import { ConfirmRecruitmentComponent } from './confirm-recruitment/confirm-recruitment.component';
import {NzModalModule} from "ng-zorro-antd/modal";
import {SharedUiMbTextLabelModule} from "@hcm-mfe/shared/ui/mb-text-label";
import {SharedDirectivesNumbericModule} from "@hcm-mfe/shared/directives/numberic";
import {SharedUiPopupModule} from "@hcm-mfe/shared/ui/popup";
import {Constant} from "@hcm-mfe/staff-manager/data-access/common";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzFormModule} from "ng-zorro-antd/form";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzPopconfirmModule} from "ng-zorro-antd/popconfirm";
import {NzDividerModule} from "ng-zorro-antd/divider";
import {NzSwitchModule} from "ng-zorro-antd/switch";

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, StaffManagerUiRecruitSearchFormRecruitModule, SharedUiMbButtonModule, SharedUiMbTableWrapModule, SharedUiMbTableModule, StaffManagerUiRecruitImportFileModule, NzTagModule, TranslateModule, NzModalModule,
    RouterModule.forChild([
      {
        path: '',
        redirectTo: 'search'
      },
      {
        path: 'search',
        component: ApprovalRecruitmentSearchComponent
      },
      {
        path: 'edit-recruitment-new',
        component: EditRecruitmentNewComponent,
        data: {
          pageName: 'staffManager.pageName.editRecruitmentNew'
        }
      },
      {
        path: 'add-recruitment-new',
        component: EditRecruitmentNewComponent,
        data: {
          pageName: 'staffManager.pageName.addRecruitmentNew'
        }
      },
      {
        path: 'detail-recruitment-new',
        component: EditRecruitmentNewComponent,
        data: {
          pageName: 'staffManager.pageName.detailRecruitmentNew',
          isDetail: true
        }
      },
      {
        path: 'add-recruitment-os',
        component: EditRehireOsEmployeeComponent,
        data: {
          pageName: 'staffManager.pageName.addRecruitmentOS',
          type: Constant.RECRUIT_TYPE[1].value
        }
      },
      {
        path: 'edit-recruitment-os',
        component: EditRehireOsEmployeeComponent,
        data: {
          pageName: 'staffManager.pageName.editRecruitmentOS',
          isEdit: true,
          type: Constant.RECRUIT_TYPE[1].value
        }
      },
      {
        path: 'detail-recruitment-os',
        component: EditRehireOsEmployeeComponent,
        data: {
          pageName: 'staffManager.pageName.detailRecruitmentOS',
          isDetail: true,
          type: Constant.RECRUIT_TYPE[1].value
        }
      },
      {
        path: 'add-recruitment-rehire',
        component: EditRehireOsEmployeeComponent,
        data: {
          pageName: 'staffManager.pageName.addRecruitmentRehire',
          type: Constant.RECRUIT_TYPE[2].value
        }
      },
      {
        path: 'edit-recruitment-rehire',
        component: EditRehireOsEmployeeComponent,
        data: {
          pageName: 'staffManager.pageName.editRecruitmentRehire',
          isEdit: true,
          type: Constant.RECRUIT_TYPE[2].value
        }
      },
      {
        path: 'detail-recruitment-rehire',
        component: EditRehireOsEmployeeComponent,
        data: {
          pageName: 'staffManager.pageName.detailRecruitmentRehire',
          isDetail: true,
          type: Constant.RECRUIT_TYPE[2].value
        }
      }
    ]), SharedUiMbTableMergeCellWrapModule, SharedUiPopupModule, SharedUiMbTableMergeCellModule, SharedUiMbSelectModule, SharedUiMbDatePickerModule, ReactiveFormsModule, SharedUiMbInputTextModule, SharedUiOrgDataPickerModule, SharedUiMbUploadModule, SharedDirectivesNumberInputModule, StaffManagerUiRejectCommonModule, SharedUiMbEmployeeDataPickerModule, SharedUiMbTextLabelModule, SharedDirectivesNumbericModule, NzDropDownModule, NzCheckboxModule, NzIconModule, NzFormModule, NzButtonModule, NzPopconfirmModule, NzDividerModule, NzSwitchModule],
  declarations: [ApprovalRecruitmentSearchComponent, EditRecruitmentNewComponent, EditRehireOsEmployeeComponent, ConfirmRecruitmentComponent],
  exports: [ApprovalRecruitmentSearchComponent]
})
export class StaffManagerFeatureRecruitApprovalRecruitmentModule {}
