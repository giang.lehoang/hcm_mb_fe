import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {AppFunction, BaseResponse, CatalogModel, OrgInfo} from "@hcm-mfe/shared/data-access/models";
import {Constant, UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {Subscription} from "rxjs";
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";
import {ActivatedRoute, Router} from "@angular/router";
import {
  RecruitmentService,
  SearchFormService,
  StaffInfoService,
  WorkInfoService
} from "@hcm-mfe/staff-manager/data-access/services";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {Utils} from "@hcm-mfe/shared/common/utils";
import * as moment from "moment";
import {SessionService} from "@hcm-mfe/shared/common/store";
import {FunctionCode, Scopes} from "@hcm-mfe/shared/common/enums";
import {CandidateSalary, CandidateWorkedOutside} from "@hcm-mfe/staff-manager/data-access/models/recruit";
import {NzUploadFile} from "ng-zorro-antd/upload";
import {ModelUpload} from "@hcm-mfe/shared/ui/mb-upload";

@Component({
  selector: 'hcm-mfe-edit-recruitment-new',
  templateUrl: './edit-recruitment-new.component.html',
  styleUrls: ['./edit-recruitment-new.component.scss']
})
export class EditRecruitmentNewComponent implements OnInit, OnDestroy {

  form!: FormGroup;
  isDetail = false;

  listPosition: CatalogModel[] = [];
  listEmpTypeCode: CatalogModel[] = [];
  listLevel: CatalogModel[] = [];
  listDocumentType: CatalogModel[] = [];
  isSubmitted = false;

  constant = Constant;
  docIdsDelete: number[] = [];
  subscriptions: Subscription[] = [];

  listGender: CatalogModel[] = [];
  listMarital: CatalogModel[] = [];
  listNation: CatalogModel[] = [];
  listEthnic: CatalogModel[] = [];
  listReligion: CatalogModel[] = [];
  listProvince: CatalogModel[] = [];
  listEduRanks: CatalogModel[] = [];
  listSchool: CatalogModel[] = [];
  listMajorLevel: CatalogModel[] = [];
  listFaculty: CatalogModel[] = [];
  listPersonalIdType: CatalogModel[] = [];
  listEmpType: CatalogModel[] = [];
  listContractType: CatalogModel[] = [];
  listPernamentDistrict: CatalogModel[] = [];
  listPernamentWard: CatalogModel[] = [];
  listcurrentDistrict: CatalogModel[] = [];
  listcurrentWard: CatalogModel[] = [];
  listDegreeType: CatalogModel[] = [];
  candidateId!: number;
  positionId!: number;
  statusApproval!: number;
  functionCode: string = FunctionCode.HR_RECRUITMENT;
  objFunction: AppFunction;
  status!: number;
  pageName!: string;
  isLoadingPage = false;
  isReject = false;
  urlReject = UrlConstant.RECRUITMENT.REJECT_RECRUIT_NEW_BY_ID;
  listSalaryGrade: CatalogModel[] = Constant.SALARY_GRADE;
  listSalaryRank: CatalogModel[] = Constant.SALARY_RANK;
  listTreatmentAllowance: CatalogModel[] = [];
  fileList: NzUploadFile[] = [];
  configUploadFile!: ModelUpload;
  listCitizenPlace: CatalogModel[] = [];
  isLoadEdit = false;
  showConfirmRecruit = false;
  listConfirmRecruit: NzSafeAny[] = [];
  functionCodeOrg = FunctionCode.PROPOSAL_CANDIDATE;
  scope = Scopes.VIEW;
  hideSalaryInfo = false;
  listPositionGroup: CatalogModel[] = [];
  isRequiredAddress = false;

  constructor(
    private fb: FormBuilder,
    private toastrService: ToastrService,
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private searchFormService: SearchFormService,
    private workInfoService: WorkInfoService,
    private staffInfoService: StaffInfoService,
    private recruitmentService: RecruitmentService,
    public sessionService: SessionService,
  ) {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${this.functionCode}`);
  }

  ngOnInit(): void {
    this.getRouterUrlInfo();
    this.initDataSelect();
    this.initFormGroup();
    this.onChanges();
    this.pathFormValue();
    this.configUploadFile = {
      viewDetailStr: this.translate.instant('staffManager.recruit.descriptionUpload'),
      viewDetail: true
    }
  }

  getRouterUrlInfo() {
    this.candidateId = this.activatedRoute.snapshot.queryParams['id'];
    this.pageName = this.activatedRoute.snapshot?.data['screenMode'];
    this.isDetail = this.activatedRoute.snapshot?.data['isDetail'];
  }

  initFormGroup() {
    this.form = this.fb.group({
      fullName: [null, [Validators.required]],
      genderCode: [null, [Validators.required]],
      dateOfBirth: [null, [Validators.required]],
      maritalStatusCode: [null, [Validators.required]],
      nationCode: [null, [Validators.required]],
      ethnicCode: [null, [Validators.required]],
      mobileNumber: [null, [Validators.required]],
      rejectReason: [null],
      personalEmail: [null],
      religionCode: [null],
      studentInfo: [null],
      criminalRecord: [null],
      schoolId: [null, [Validators.required]],
      facultyId: [null, [Validators.required]],
      degreeTypeCode: [null, [Validators.required]],
      majorLevelId: [null, [Validators.required]],
      eduRankCode: [null],
      issueYear: [null],
      personalIdTypeCode: [null, [Validators.required]],
      personalId: [null, [Validators.required]],
      personalIdDate: [null, [Validators.required]],
      personalIdPlace: [null, [Validators.required]],
      joinCompanyDate: [null, [Validators.required]],
      empTypeCode: [null, [Validators.required]],
      documentTypeId: [null, [Validators.required]],
      documentNo: [null, [Validators.required]],
      signedDate: [null],
      organization: [null,[Validators.required]],
      positionId: [null, [Validators.required]],
      positionLevel: [null],
      positionGroupId: [null],
      contractTypeId: [null, [Validators.required]],
      currentProvinceCode: [null],
      currentDistrictCode: [null],
      currentWardCode: [null],
      currentDetail: [null],
      pernamentProvinceCode: [null],
      pernamentDistrictCode: [null],
      pernamentWardCode: [null],
      pernamentDetail: [null],
      duplicateTypeName: [null],
      duplicateOrgName: [null],
      duplicateDateOfBirth: [null],
      duplicateFullName: [null],
      duplicateEmployeeCode: [null],
      amountFee: [null],
      salaryGrade: [null, [Validators.required]],
      salaryRank: [null, [Validators.required]],
      salaryAmount: [null, [Validators.min(1), Validators.required]],
      salaryPercent: [null, [Validators.min(1), Validators.max(100), Validators.required]],
      thsFactor: [null, [Validators.max(100)]],
      thsAmount: [null, [Validators.min(1)]],
      thsPercent: [null, [Validators.min(1), Validators.max(100)]],
      treatmentAllowance: [null],
      monthlyBonus: [null, [Validators.min(1)]],
      salaryMonth: [null, [Validators.min(1)]],
      salaryMonthByRate: [null, [Validators.min(1)]],
      baseProposal: [null],
      durationFactor: [null, [Validators.max(100)]],
      rate: [null],
      natureOfWork: [null],
      monthlyBonusDate: [null],
      commitmentAmount: [null, [Validators.min(1)]],
      commitmentFromDate: [null],
      commitmentToDate: [null],
      allowanceAmount: [null, [Validators.min(1)]],
      allowanceFromDate: [null],
      allowanceToDate: [null],
      candidateSalaryId: [null],
      groupOrg: this.fb.array([[null]]),
      groupPosition: this.fb.array([[null]]),
      groupFromDate: this.fb.array([[null]]),
      groupToDate: this.fb.array([[null]]),
    });
  }

  get f() {
    return this.form.controls;
  }

  get groupOrg(): FormArray {
    return this.form.get('groupOrg') as FormArray;
  }

  get groupPosition(): FormArray {
    return this.form.get('groupPosition') as FormArray;
  }

  get groupFromDate(): FormArray {
    return this.form.get('groupFromDate') as FormArray;
  }

  get groupToDate(): FormArray {
    return this.form.get('groupToDate') as FormArray;
  }

  onPersonalIdTypeChange(event: NzSafeAny) {
    if (event) {
        this.getCitizenPlace(event.itemSelected?.value);
        if (!this.isLoadEdit) {
          this.form.controls['personalIdPlace'].reset();
        }
        this.isLoadEdit = false;
    }
  }

  getCitizenPlace(paperType: string) {
    let typeCode = '';
    if (paperType === Constant.PAPERS_CODE.CITIZEN_ID) {
      typeCode =  Constant.CATALOGS.NOI_CAP_CCCD;
    } else if (paperType === Constant.PAPERS_CODE.ID_NO) {
      typeCode = Constant.CATALOGS.NOI_CAP_CMND;
    }
    this.subscriptions.push(
      this.recruitmentService.getCatalog(typeCode).subscribe({
        next: (res: BaseResponse) => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.listCitizenPlace = res?.data?.map((item: NzSafeAny) => {
              item.value = item.label?.trim();
              return item;
            });
          } else {
            this.toastrService.error(res?.message);
          }
        },
        error: (err) => {
          this.toastrService.error(err?.message);
        }
      })
    );
  }


  initDataSelect() {
    this.getListGender();
    this.getListDegreeType();
    this.getListDocumentTypes();
    this.getPositionLevelList();
    this.getListPositionGroup();
    this.getListMarital();
    this.getListNation();
    this.getListEthnic();
    this.getListReligion();
    this.getListProvince();
    this.getListEduRank();
    this.getListSchool();
    this.getListFaculty();
    this.getListMajorLevel();
    this.getListPersonalIdType();
    this.getListEmpType();
    this.getContractTypeList();
    this.getListTreatmentAllowance();
  }

  getListTreatmentAllowance() {
    this.listTreatmentAllowance = Constant.TREATMENT_ALLOWANCE.map(item => {
      item.label = this.translate.instant(item.label);
      return item;
    })
  }

  onChanges() {
    this.subscriptions.push(
      this.f['organization'].valueChanges?.subscribe((value: OrgInfo) => {
        if (value) {
          this.f['positionId'].setValue(null, { emitEvent: false, onlySelf: true });
          this.listPosition = [];
          this.workInfoService.getMBPositions(value.orgId).subscribe((res: BaseResponse) => {
            if (res.code === HTTP_STATUS_CODE.OK) {
              this.listPosition = res.data?.map((item: NzSafeAny) => {
                item.value = item.posId;
                item.label = item.jobName;
                return item;
              });
              if (this.positionId) {
                this.form.controls['positionId'].setValue(this.positionId);
              }
            }
          });
        }
      })
    );
  }

  getListDocumentTypes() {
    this.subscriptions.push(
      this.workInfoService.getDocumentTypes().subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listDocumentType = res.data.map((item: NzSafeAny) => {
            item.label = item.name;
            item.value = item.documentTypeId;
            return item;
          });
        }
      }, error => this.toastrService.error(error.message))
    );
  }

  getPositionLevelList() {
    this.subscriptions.push(
      this.staffInfoService.getCatalog(Constant.CATALOGS.LEVEL_NV).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listLevel = res.data;
        }
      }, error => this.toastrService.error(error.message))
    );
  }

  getListPositionGroup() {
    this.staffInfoService.getPositionGroup(Constant.TYPE_CODE.POSITION).subscribe(res => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.listPositionGroup = res.data.map((item: NzSafeAny) => {
          item.value = item.pgrId;
          item.label = item.pgrName;
          return item;
        });
      }
    }, error => this.toastrService.error(error.message));
  }

  changePosition(event: NzSafeAny) {
    if (event) {
      if (event.itemSelected?.positionGroupId) {
        this.f['positionGroupId'].setValue(event.itemSelected?.positionGroupId);
      }
    }
  }

  getListGender() {
    this.subscriptions.push(
      this.staffInfoService.getCatalog(Constant.CATALOGS.GIOI_TINH).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listGender = res.data;
        }
      }, error => this.toastrService.error(error.message))
    );
  }

  getListDegreeType() {
    this.subscriptions.push(
      this.staffInfoService.getCatalog(Constant.CATALOGS.LOAI_BANG_CAP).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listDegreeType = res.data;
        }
      }, error => this.toastrService.error(error.message))
    );
  }

  getListMarital() {
    this.subscriptions.push(
      this.staffInfoService.getCatalog(Constant.CATALOGS.TINH_TRANG_HON_NHAN).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listMarital = res.data;
        }
      }, error => this.toastrService.error(error.message))
    );
  }

  getListNation() {
    this.subscriptions.push(
      this.staffInfoService.getCatalog(Constant.CATALOGS.QUOC_GIA).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listNation = res.data;
        }
      }, error => this.toastrService.error(error.message))
    );
  }

  getListEthnic() {
    this.subscriptions.push(
      this.staffInfoService.getCatalog(Constant.CATALOGS.DAN_TOC).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listEthnic = res.data;
        }
      }, error => this.toastrService.error(error.message))
    );
  }

  getListReligion() {
    this.subscriptions.push(
      this.staffInfoService.getCatalog(Constant.CATALOGS.TON_GIAO).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listReligion = res.data;
        }
      }, error => this.toastrService.error(error.message))
    );
  }

  getListPersonalIdType() {
    this.subscriptions.push(
      this.staffInfoService.getCatalog(Constant.CATALOGS.LOAI_GIAY_TO).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listPersonalIdType = res.data;
        }
      }, error => this.toastrService.error(error.message))
    );
  }

  getListEmpType() {
    this.subscriptions.push(
      this.staffInfoService.getCatalog(Constant.CATALOGS.DOI_TUONG_CV).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listEmpType = res.data;
        }
      }, error => this.toastrService.error(error.message))
    );
  }

  getContractTypeList() {
    this.subscriptions.push(
      this.staffInfoService.getContractTypes().subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listContractType = res.data;
        }
      }, error => this.toastrService.error(error.message))
    );
  }

  getListProvince() {
    this.subscriptions.push(
      this.staffInfoService.getCatalog(Constant.CATALOGS.TINH).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listProvince = res.data;
        }
      }, error => this.toastrService.error(error.message))
    );
  }

  getListEduRank() {
    this.subscriptions.push(
      this.staffInfoService.getCatalog(Constant.CATALOGS.XEP_LOAI_TOT_NGHIEP).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listEduRanks = res.data;
        }
      }, error => this.toastrService.error(error.message))
    );
  }

  getListSchool() {
    this.subscriptions.push(
      this.staffInfoService.getListSchool().subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listSchool = res.data;
        }
      }, error => this.toastrService.error(error.message))
    );
  }

  getListFaculty() {
    this.subscriptions.push(
      this.staffInfoService.getListFaculty().subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listFaculty = res.data;
        }
      }, error => this.toastrService.error(error.message))
    );
  }


  getListMajorLevel() {
    this.subscriptions.push(
      this.staffInfoService.getListMajorLevel().subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listMajorLevel = res.data;
        }
      }, error => this.toastrService.error(error.message))
    );
  }

  pathFormValue() {
    if (this.candidateId) {
      this.isLoadingPage = true;
      this.subscriptions.push(
        this.recruitmentService.getDataById(this.candidateId, UrlConstant.RECRUITMENT.GET_CANDIDATE_BY_ID).subscribe(res => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.isLoadEdit = true;
            const org = new OrgInfo();
            org.orgId = res.data.organizationId;
            org.orgName = res.data.orgName;
            this.f['organization'].setValue(org);

            this.status = res.data.status;
            this.positionId = res.data.positionId;
            this.form.patchValue(res.data);

            if (res.data.candidateSalaryDTO) { // lương và phụ cấp
              const candidateSalary: CandidateSalary = res.data.candidateSalaryDTO;
              for (const key in candidateSalary) {
                this.f[key]?.setValue(candidateSalary[key as keyof CandidateSalary]);
              }

              this.f['allowanceFromDate'].setValue(Utils.convertDateToFillForm(candidateSalary.allowanceFromDate));
              this.f['allowanceToDate'].setValue(Utils.convertDateToFillForm(candidateSalary.allowanceToDate));
              this.f['commitmentFromDate'].setValue(Utils.convertDateToFillForm(candidateSalary.commitmentFromDate));
              this.f['commitmentToDate'].setValue(Utils.convertDateToFillForm(candidateSalary.commitmentToDate));
              this.f['monthlyBonusDate'].setValue(Utils.convertDateToFillForm(candidateSalary.monthlyBonusDate));

              if (this.isDetail) {
                this.convertNumberToMoney('salaryAmount', candidateSalary?.salaryAmount);
                this.convertNumberToMoney('thsAmount', candidateSalary?.thsAmount);
                this.convertNumberToMoney('monthlyBonus', candidateSalary?.monthlyBonus);
                this.convertNumberToMoney('salaryMonth', candidateSalary?.salaryMonth);
                this.convertNumberToMoney('salaryMonthByRate', candidateSalary?.salaryMonthByRate);
                this.convertNumberToMoney('commitmentAmount', candidateSalary?.commitmentAmount);
                this.convertNumberToMoney('allowanceAmount', candidateSalary?.allowanceAmount);
              }
             }

            res.data.candidateWorkedOutsidesDTO?.forEach((item: CandidateWorkedOutside, index: number) => {// Công tác trước Mb
              if (index === 0) {
                this.f['groupOrg'].setValue([item.organizationName]);
                this.f['groupPosition'].setValue([item.positionName]);
                this.f['groupFromDate'].setValue([Utils.convertDateToFillForm(item.fromDate)]);
                this.f['groupToDate'].setValue([Utils.convertDateToFillForm(item.toDate)]);
              } else {
                this.groupOrg.push(new FormControl(item.organizationName));
                this.groupPosition.push(new FormControl(item.positionName));
                this.groupFromDate.push(new FormControl(Utils.convertDateToFillForm(item.fromDate)));
                this.groupToDate.push(new FormControl(Utils.convertDateToFillForm(item.toDate)));
              }
            });

            this.f['dateOfBirth'].setValue(Utils.convertDateToFillForm(res.data.dateOfBirth));
            this.f['personalIdDate'].setValue(Utils.convertDateToFillForm(res.data.personalIdDate));
            this.f['joinCompanyDate'].setValue(Utils.convertDateToFillForm(res.data.joinCompanyDate));
            this.f['signedDate'].setValue(Utils.convertDateToFillForm(res.data.signedDate));
            this.f['issueYear'].setValue(res.data.issueYear ? moment(res.data.issueYear,'yyyy').toDate() : null);

            const duplicateTypeName = Constant.DUPLICATE_TYPE.find(item => item.value === res.data.duplicateType)?.label;
            if (duplicateTypeName) {
              this.f['duplicateTypeName'].setValue(this.translate.instant(duplicateTypeName));
            }
            this.statusApproval = res.data.status;

            res.data.attachFileList?.forEach((item: NzSafeAny)=> {
              this.fileList.push({
                uid: item.docId?.toString() ?? "",
                name: item.fileName ?? "",
                thumbUrl: item.security,
                status: 'done'
              });
            });
            this.fileList = [...this.fileList];

            if (this.isDetail) {
              this.convertNumberToMoney('amountFee', res.data?.amountFee);
              this.form.disable();
            }
            this.isLoadingPage = false;
          }
        })
      );
    }
  }

  convertNumberToMoney(key: string, data: number | null) {
    if (data) {
      this.f[key].setValue(new Intl.NumberFormat('de-DE').format(data));
    }
  }

  changeProvince(value: number, type: 'HK' | 'HT') {
    if (value) {
      if (type === 'HK') {
        this.subscriptions.push(
          this.staffInfoService.getCatalog(Constant.CATALOGS.HUYEN, value).subscribe((res: BaseResponse) => {
            if (res.code === HTTP_STATUS_CODE.OK) {
              this.listPernamentDistrict = res.data;
            }
          }, error => this.toastrService.error(error.message))
        );
      } else if (type === 'HT') {
        this.subscriptions.push(
          this.staffInfoService.getCatalog(Constant.CATALOGS.HUYEN, value).subscribe((res: BaseResponse) => {
            if (res.code === HTTP_STATUS_CODE.OK) {
              this.listcurrentDistrict = res.data;
            }
          }, error => this.toastrService.error(error.message))
        );
      }
    }
  }

  changeDistrict(value: number, type: 'HK' | 'HT') {
    if (value) {
      if (type === 'HK') {
        this.subscriptions.push(
          this.staffInfoService.getCatalog(Constant.CATALOGS.XA, value).subscribe((res: BaseResponse) => {
            if (res.code === HTTP_STATUS_CODE.OK) {
              this.listPernamentWard = res.data;
            }
          }, error => this.toastrService.error(error.message))
        );
      } else if (type === 'HT') {
        this.subscriptions.push(
          this.staffInfoService.getCatalog(Constant.CATALOGS.XA, value).subscribe((res: BaseResponse) => {
            if (res.code === HTTP_STATUS_CODE.OK) {
              this.listcurrentWard = res.data;
            }
          }, error => this.toastrService.error(error.message))
        );
      }
    }
  }

  changeDate(date: Date, key: string) {
    const errorStr = key + 'Error'
    if (date > new Date()) {
      this.f[key].setErrors({[errorStr]: true});
    } else {
      if (!this.f[key].value) {
        this.f[key].setErrors({required: true});
      } else {
        this.f[key].setErrors(null);
      }
    }
  }

  changeAllowanceDate(key: string) {
    const errorStr = key + "Error";
    let fromDate;
    let toDate;
    if (key === 'allowanceFromDate') {
      fromDate = this.f['allowanceFromDate'].value;
      toDate = this.f['allowanceToDate'].value;
    } else if (key === 'commitmentFromDate') {
      fromDate = this.f['commitmentFromDate'].value;
      toDate = this.f['commitmentToDate'].value;
    }
    try {
      fromDate = new Date(fromDate?.getFullYear(), fromDate?.getMonth(), fromDate?.getDate());
      toDate = new Date(toDate?.getFullYear(), toDate?.getMonth(), toDate?.getDate());
    } catch (error) {}
    if (fromDate && toDate && fromDate > toDate) {
      this.f[key].setErrors({[errorStr]: true})
    } else {
      this.f[key].setErrors(null);
    }
  }

  changeIssueYear(year: NzSafeAny) {
    const dateNow = new Date();
    if (isNaN(parseInt(year)) && year?.getFullYear() > dateNow.getFullYear()) {
      this.f['issueYear'].setErrors({'issueYearError': true})
    } else {
      this.f['issueYear'].setErrors(null);
    }
  }

  save() {
    this.isSubmitted = true;
    if (this.f['empTypeCode'].value === Constant.EMP_OS_CODE) {
      const candidateSalary = new CandidateSalary();
      for (const key in candidateSalary) {
        if (key !== 'candidateSalaryId') {
          this.f[key].setValue(null);
        }
      }
    } else {
      this.f['amountFee'].setValue(null);
    }
    if (this.form.valid) {
      if (this.fileList.length === 0) {
        this.toastrService.error(this.translate.instant('staffManager.recruit.requiredFileAttach'))
        return;
      }
      const request = this.form.value;
      request.dateOfBirth = Utils.convertDateToSendServer(this.form.controls['dateOfBirth'].value);
      request.personalIdDate = Utils.convertDateToSendServer(this.form.controls['personalIdDate'].value);
      request.joinCompanyDate = Utils.convertDateToSendServer(this.form.controls['joinCompanyDate'].value);
      request.signedDate = Utils.convertDateToSendServer(this.form.controls['signedDate'].value);
      if (this.f['issueYear'].value) {
        request.issueYear = this.f['issueYear'].value.getFullYear();
      }
      request.organizationId = this.form.value.organization?.orgId;
      request.candidateId = this.candidateId;
      request.pageName = this.pageName;
      request.candidateSalary = this.getCandidateSalary(this.form);
      const errorWorkOutside = this.validateCandidateWorkOutside();
      if (errorWorkOutside) {
        this.toastrService.error(this.translate.instant('staffManager.recruit.errorWorkOutside'));
        return;
      } else {
        request.candidateWorkedOutsides = this.getCandidateWorkedOutside();
      }

      request.docIdsDelete = this.docIdsDelete;

      const formData = new FormData();
      formData.append('data', new Blob([JSON.stringify(request)], {
        type: 'application/json'
      }));

      this.fileList.forEach((nzFile: NzUploadFile) => {
        formData.append('files', nzFile as NzSafeAny);
      });

      this.isLoadingPage = true;
      this.subscriptions.push(
        this.recruitmentService.saveRecruitNew(formData).subscribe(res => {
          this.isLoadingPage = false;
          if (res.code === HTTP_STATUS_CODE.OK) {
            const notification = this.candidateId ? 'common.notification.editSuccess' : 'common.notification.addSuccess'
            this.toastrService.success(this.translate.instant(notification));
            if (res.data) {
              this.listConfirmRecruit = [res.data];
              this.showConfirmRecruit = true;
            } else {
              this.closeCancel();
            }
          } else {
            this.toastrService.error(res.message);
          }
        }, error => {
          this.isLoadingPage = false;
          this.toastrService.error(error.message)
        })
      );
    }
  }

  getCandidateWorkedOutside(): CandidateWorkedOutside[] | null {
    const data: CandidateWorkedOutside[] = [];
    this.f['groupOrg'].value?.forEach((item: NzSafeAny, index: number) => {
      if (item) {
        data.push(
          {
            organizationName: item,
            positionName: this.f['groupPosition'].value[index],
            fromDate: Utils.convertDateToSendServer(this.f['groupFromDate'].value[index]),
            toDate: Utils.convertDateToSendServer(this.f['groupToDate'].value[index])
          }
        );
      }
    })
    return data.length > 0 ? data : null;
  }

  getCandidateSalary(form: FormGroup): CandidateSalary | null {
    const candidateSalary = new CandidateSalary();
    let isNull = true;
    for (const key in candidateSalary) {
        candidateSalary[key] = form.controls[key].value;
        if (form.controls[key].value) {
          isNull = false;
        }
    }
    candidateSalary.allowanceFromDate = Utils.convertDateToSendServer(form.controls['allowanceFromDate'].value);
    candidateSalary.allowanceToDate = Utils.convertDateToSendServer(form.controls['allowanceToDate'].value);
    candidateSalary.commitmentFromDate = Utils.convertDateToSendServer(form.controls['commitmentFromDate'].value);
    candidateSalary.commitmentToDate = Utils.convertDateToSendServer(form.controls['commitmentToDate'].value);
    candidateSalary.monthlyBonusDate = Utils.convertDateToSendServer(form.controls['monthlyBonusDate'].value);
    return !isNull ? candidateSalary : null;
  }

  doCloseReject(refresh: boolean){
    this.isReject = false;
    if (refresh) {
      this.closeCancel();
    }
  }

  openRejectForm() {
    this.isReject = true;
  }

  approvalById(id: number) {
    this.isLoadingPage = true;
    this.subscriptions.push(
      this.recruitmentService.approvalRecruitById(id, UrlConstant.RECRUITMENT.APPROVAL_RECRUIT_NEW_BY_ID).subscribe((res) => {
        this.isLoadingPage = false;
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.toastrService.success(this.translate.instant('common.notification.isApprove'));
          this.closeCancel()
        } else {
          this.toastrService.error(res?.message)
        }
      }, error => {
        this.isLoadingPage = false;
        this.toastrService.error(error.message)
      })
    );
  }

  validateCandidateWorkOutside(): boolean {
    const keys = ['groupOrg', 'groupPosition', 'groupFromDate', 'groupToDate'];
    const length = this.f['groupOrg'].value.length;
    let error = false;
    for (let i = 0; i < length; i++) {
      const isValue: boolean = !!this.f['groupOrg'].value[i];
      error = keys.some(key => {
        return (!!this.f[key].value[i]) !== isValue;
      })
      if (error) {
        break;
      }
    }
    return error;
  }

  addWorkOutside() {
    const index = this.f['groupOrg'].value.length - 1;
    if (this.f['groupOrg'].value[index] && this.f['groupPosition'].value[index]
        && this.f['groupFromDate'].value[index] && this.f['groupToDate'].value[index]) {
      this.groupOrg.push(new FormControl(null));
      this.groupPosition.push(new FormControl(null));
      this.groupFromDate.push(new FormControl(null));
      this.groupToDate.push(new FormControl(null));
    } else {
      this.toastrService.error(this.translate.instant('staffManager.recruit.validateAddWO'))
    }
  }

  removeWorkOutside (index: number) {
    this.groupOrg.removeAt(index);
    this.groupPosition.removeAt(index);
    this.groupFromDate.removeAt(index);
    this.groupToDate.removeAt(index);
  }

  selectWODate(index: number) {
    if (this.f['groupFromDate'].value[index] && this.f['groupToDate'].value[index]
        && this.f['groupFromDate'].value[index] > this.f['groupToDate'].value[index]) {
      this.groupFromDate.controls[index].setErrors({woDateError: true})
    } else {
      this.groupFromDate.controls[index].setErrors(null)
    }
  }

  closeCancel() {
    localStorage.setItem('isBackUrl', 'true');
    history.back();
  }

  onFileListChange(event: NzUploadFile[]) {
    this.fileList = event;
  }

  removeFile(event: number[]) {
    if (event && event.length > 0) {
      this.docIdsDelete = event.filter((docId: number) => !isNaN(docId));
    }
  }

  selectEmpType(event: string) {
    if (event === Constant.EMP_OS_CODE) {
      this.hideSalaryInfo = true;
      this.resetValidateRequiredSalary();
    } else {
      this.hideSalaryInfo = false;
      this.setValidateSalary();
    }
    this.updateValueAndValiditySalary();
  }

  setValidateSalary() {
    this.f['salaryGrade'].setValidators(Validators.required);
    this.f['salaryRank'].setValidators(Validators.required);
    this.f['salaryAmount'].setValidators([Validators.required, Validators.min(1)]);
    this.f['salaryPercent'].setValidators([Validators.required, Validators.min(1), Validators.max(100)]);
    this.f['amountFee'].setValidators(null);
  }

  updateValueAndValiditySalary() {
    this.f["salaryGrade"].updateValueAndValidity();
    this.f["salaryRank"].updateValueAndValidity();
    this.f["salaryAmount"].updateValueAndValidity();
    this.f["salaryPercent"].updateValueAndValidity();
    this.f["amountFee"].updateValueAndValidity();
  }


  resetValidateRequiredSalary() {
    this.f['salaryGrade'].setValidators(null);
    this.f['salaryRank'].setValidators(null);
    this.f['salaryAmount'].setValidators(Validators.min(1));
    this.f['salaryPercent'].setValidators([Validators.min(1), Validators.max(100)]);
    this.f['amountFee'].setValidators([Validators.min(1), Validators.required]);
  }

  ngOnDestroy(): void {
    this.subscriptions?.forEach(sub => sub.unsubscribe());
  }

  changeNation(event: string) {
    const keyAddress = ['currentProvinceCode', 'currentDistrictCode', 'currentWardCode', 'pernamentProvinceCode', 'pernamentDistrictCode', 'pernamentWardCode'];
    if (event === Constant.NATION_CODE_VN) {
      this.isRequiredAddress = true;
      keyAddress.forEach(key => {
        this.f[key].setValidators([Validators.required])
      })
    } else {
      this.isRequiredAddress = false;
      keyAddress.forEach(key => {
        this.f[key].setValidators(null)
      })
    }
    keyAddress.forEach(key => {
      this.f[key].updateValueAndValidity()
    })
  }

  getLabelAddress(key: string, isRequiredAddress: boolean): string {
    let labelText = this.translate.instant(key);
    if (isRequiredAddress) {
      labelText += "<span class='label__required'> *</span>"
    } else {
      labelText = this.translate.instant(key);
    }
    return labelText;
  }
}
