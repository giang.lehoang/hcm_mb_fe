import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {AppFunction, BaseResponse, CatalogModel, OrgInfo} from "@hcm-mfe/shared/data-access/models";
import {Constant, UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {NzUploadFile} from "ng-zorro-antd/upload";
import {Subscription} from "rxjs";
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";
import {ActivatedRoute, Router} from "@angular/router";
import {
  RecruitmentService,
  SearchFormService,
  StaffInfoService,
  WorkInfoService
} from "@hcm-mfe/staff-manager/data-access/services";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {Utils} from "@hcm-mfe/shared/common/utils";
import {HttpParams} from "@angular/common/http";
import {ModelUpload} from "@hcm-mfe/shared/ui/mb-upload";
import {CandidateSalary} from "@hcm-mfe/staff-manager/data-access/models/recruit";
import {FunctionCode, Scopes} from "@hcm-mfe/shared/common/enums";

@Component({
  selector: 'hcm-mfe-edit-rehire-os-employee',
  templateUrl: './edit-rehire-os-employee.component.html',
  styleUrls: ['./edit-rehire-os-employee.component.scss']
})
export class EditRehireOsEmployeeComponent implements OnInit, OnDestroy {

  form!: FormGroup;

  listPosition: CatalogModel[] = [];
  listEmpTypeCode: CatalogModel[] = [];
  listLevel: CatalogModel[] = [];
  listDocumentType: CatalogModel[] = [];
  isSubmitted = false;

  constant = Constant;
  employeeId!: number;
  fileList: NzUploadFile[] = [];
  docIdsDelete: number[] = [];
  subscriptions: Subscription[] = [];
  empStatus: 'WORKING' | 'OUT' = 'WORKING';
  empTypeCode!: string;
  isDetail = false;
  isEdit = false;
  recordId!: number | null;
  positionId!: number;
  isRecruitFromOs = false;
  type!: string;
  action!: string;
  pageName!: string;
  objFunction!: AppFunction;
  status!: number;
  isLoadingPage = false;
  isReject = false;
  urlReject = UrlConstant.RECRUITMENT.REJECT_REHIRE_OS_BY_ID;
  configUploadFile!: ModelUpload;
  listSalaryGrade: CatalogModel[] = Constant.SALARY_GRADE;
  listSalaryRank: CatalogModel[] = Constant.SALARY_RANK;
  listTreatmentAllowance: CatalogModel[] = [];
  hideSalaryInfo = false;
  functionCodeOrg = FunctionCode.PROPOSAL_CANDIDATE;
  functionCodeEmp: string = FunctionCode.PROPOSAL_CANDIDATE;
  scope = Scopes.VIEW;
  listContractType: CatalogModel[] = [];
  listPositionGroup: CatalogModel[] = [];

  constructor(
    private fb: FormBuilder,
    private toastrService: ToastrService,
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private searchFormService: SearchFormService,
    private workInfoService: WorkInfoService,
    private staffInfoService: StaffInfoService,
    private recruitmentService: RecruitmentService,
  ) { }

  ngOnInit(): void {
    this.initFormGroup();
    this.getRouterUrlInfo();
    this.getListDocumentTypes();
    this.getContractTypeList();
    this.getListTreatmentAllowance();
    this.getListEmpTypeCode();
    this.getPositionLevelList();
    this.getListPositionGroup();
    this.onChanges();
    this.pathFormValue();
    this.configUploadFile = {
      viewDetailStr: this.translate.instant('staffManager.recruit.descriptionUpload'),
      viewDetail: true
    }
  }

  getListPositionGroup() {
    this.staffInfoService.getPositionGroup(Constant.TYPE_CODE.POSITION).subscribe(res => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.listPositionGroup = res.data.map((item: NzSafeAny) => {
          item.value = item.pgrId;
          item.label = item.pgrName;
          return item;
        });
      }
    }, error => this.toastrService.error(error.message));
  }

  changePosition(event: NzSafeAny) {
    if (event) {
      if (event.itemSelected?.positionGroupId) {
        this.f['positionGroupId'].setValue(event.itemSelected?.positionGroupId);
      }
    }
  }

  getListTreatmentAllowance() {
    this.listTreatmentAllowance = Constant.TREATMENT_ALLOWANCE.map(item => {
      item.label = this.translate.instant(item.label);
      return item;
    })
  }

  getRouterUrlInfo() {
    this.type = this.activatedRoute.snapshot?.data['type'];
    if (this.type === Constant.RECRUIT_TYPE[1].value) {
      this.isRecruitFromOs = true;
      this.empTypeCode = Constant.EMP_OS_CODE;
    } else {
      this.empStatus = Constant.EMP_STATUS_OUT;
      this.functionCodeEmp = '';
    }
    this.recordId = this.activatedRoute.snapshot.queryParams['id'];
    this.isDetail = this.activatedRoute.snapshot?.data['isDetail'];
    this.isEdit = this.activatedRoute.snapshot?.data['isEdit'];
    if (!this.isRecruitFromOs) {
      this.form.addControl('empTypeCode', new FormControl(null, Validators.required));
    }
  }

  initFormGroup() {
    this.form = this.fb.group({
      fromDate: [null, [Validators.required]],
      documentTypeId: [null, [Validators.required]],
      documentNo: [null, [Validators.required]],
      signedDate: [null],
      rejectReason: [null],
      organization: [null,[Validators.required]],
      positionId: [null, [Validators.required]],
      positionGroupId: [null],
      positionLevel: [null],
      contractTypeId: [null, [Validators.required]],
      amountFee: [null, [Validators.min(1)]],
      note: [null],
      isKeepSenior: [null],
      employee: [null, [Validators.required]],
      salaryGrade: [null, [Validators.required]],
      salaryRank: [null, [Validators.required]],
      salaryAmount: [null, [Validators.min(1), Validators.required]],
      salaryPercent: [null, [Validators.min(1), Validators.max(100), Validators.required]],
      thsFactor: [null, [Validators.max(100)]],
      thsAmount: [null, [Validators.min(1)]],
      thsPercent: [null, [Validators.min(1), Validators.max(100)]],
      treatmentAllowance: [null],
      monthlyBonus: [null, [Validators.min(1)]],
      salaryMonth: [null, [Validators.min(1)]],
      salaryMonthByRate: [null, [Validators.min(1)]],
      baseProposal: [null],
      durationFactor: [null, [Validators.max(100)]],
      rate: [null],
      natureOfWork: [null],
      monthlyBonusDate: [null],
      commitmentAmount: [null, [Validators.min(1)]],
      commitmentFromDate: [null],
      commitmentToDate: [null],
      allowanceAmount: [null, [Validators.min(1)]],
      allowanceFromDate: [null],
      allowanceToDate: [null],
      candidateSalaryId: [null],
    });
  }

  get f() {
    return this.form.controls;
  }

  onChanges() {
    this.subscriptions.push(
      this.f['organization'].valueChanges?.subscribe((value: OrgInfo) => {
        if (value) {
          this.f['positionId'].setValue(null, { emitEvent: false, onlySelf: true });
          this.listPosition = [];
          this.workInfoService.getMBPositions(value.orgId).subscribe((res: BaseResponse) => {
            if (res.code === HTTP_STATUS_CODE.OK) {
              this.listPosition = res.data?.map((item: NzSafeAny) => {
                item.value = item.posId;
                item.label = item.jobName;
                return item;
              });
              if (this.positionId) {
                this.f['positionId'].setValue(this.positionId);
              }
            }
          });
        }
      })
    );
  }

  getListDocumentTypes() {
    this.subscriptions.push(
      this.workInfoService.getDocumentTypes().subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listDocumentType = res.data.reduce((accumulator: CatalogModel[], currentValue: NzSafeAny) => {
            if (currentValue.type === 'IN') {
              currentValue.label = currentValue.name;
              currentValue.value = currentValue.documentTypeId;
              accumulator.push(currentValue);
            }
            return accumulator;
          }, [])
        }
      }, error => this.toastrService.error(error.message))
    );
  }

  getContractTypeList() {
    this.subscriptions.push(
      this.staffInfoService.getContractTypes().subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listContractType = res.data;
        }
      }, error => this.toastrService.error(error.message))
    );
  }

  getListEmpTypeCode() {
    const params = new HttpParams().set('typeCode', Constant.TYPE_CODE.DOI_TUONG_CV);
    this.subscriptions.push(
      this.searchFormService.getListValue(UrlConstant.CATEGORY.LOOKUP_VALUE, params).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listEmpTypeCode = res.data;
        }
      }, error => {
        this.toastrService.error(error.message);
      })
    );
  }

  getPositionLevelList() {
    this.subscriptions.push(
      this.staffInfoService.getCatalog(Constant.CATALOGS.LEVEL_NV).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listLevel = res.data;
        }
      }, error => this.toastrService.error(error.message))
    );
  }

  onFileListChange(event: NzUploadFile[]) {
    this.fileList = event;
  }

  removeFile(event: number[]) {
    if (event && event.length > 0) {
      this.docIdsDelete = event.filter((docId: number) => !isNaN(docId));
    }
  }

  pathFormValue() {
    if (this.recordId) {
      this.isLoadingPage = true;
      this.subscriptions.push(
        this.recruitmentService.getDataById(this.recordId,UrlConstant.RECRUITMENT.GET_REHIRE_OS_BY_ID).subscribe(res => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.status = res.data?.status;
            this.form.patchValue(res.data);
            this.f['fromDate'].setValue(Utils.convertDateToFillForm(res.data.fromDate));
            this.f['signedDate'].setValue(Utils.convertDateToFillForm(res.data.signedDate));
            const org = {orgId: res.data.organizationId, orgName: res.data.orgName}
            this.f['organization'].setValue(org);
            const employee = {employeeId: res.data.employeeId, fullName: res.data.fullName, employeeCode: res.data.oldEmployeeCode}
            this.f['employee'].setValue(employee);
            this.positionId = res.data.positionId;
            this.employeeId = res.data.employeeId;

            if (res.data.candidateSalaryDTO) { // lương và phụ cấp
              const candidateSalary: CandidateSalary = res.data.candidateSalaryDTO;
              for (const key in candidateSalary) {
                this.f[key]?.setValue(candidateSalary[key]);
              }

              this.f['allowanceFromDate'].setValue(Utils.convertDateToFillForm(candidateSalary.allowanceFromDate));
              this.f['allowanceToDate'].setValue(Utils.convertDateToFillForm(candidateSalary.allowanceToDate));
              this.f['commitmentFromDate'].setValue(Utils.convertDateToFillForm(candidateSalary.commitmentFromDate));
              this.f['commitmentToDate'].setValue(Utils.convertDateToFillForm(candidateSalary.commitmentToDate));
              this.f['monthlyBonusDate'].setValue(Utils.convertDateToFillForm(candidateSalary.monthlyBonusDate));

              if (this.isDetail) {
                this.convertNumberToMoney('salaryAmount', candidateSalary.salaryAmount);
                this.convertNumberToMoney('thsAmount', candidateSalary.thsAmount);
                this.convertNumberToMoney('monthlyBonus', candidateSalary.monthlyBonus);
                this.convertNumberToMoney('salaryMonth', candidateSalary.salaryMonth);
                this.convertNumberToMoney('salaryMonthByRate', candidateSalary.salaryMonthByRate);
                this.convertNumberToMoney('commitmentAmount', candidateSalary.commitmentAmount);
                this.convertNumberToMoney('allowanceAmount', candidateSalary.allowanceAmount);
              }
            }

            res.data.attachFileList?.forEach((item: NzSafeAny)=> {
              this.fileList.push({
                uid: item.docId?.toString() ?? "",
                name: item.fileName ?? "",
                thumbUrl: item.security,
                status: 'done'
              });
            });
            this.fileList = [...this.fileList];
            this.f['isKeepSenior'].setValue(res.data.isKeepSenior === Constant.KEEP_SENIOR.YES);
            if (this.isDetail) {
              this.convertNumberToMoney('amountFee', res.data?.amountFee);
              this.form.disable();
            }
            this.isLoadingPage = false;
          }
        })
      );
    }
  }

  convertNumberToMoney(key: string, data: number | null) {
    if (data) {
      this.f[key].setValue(new Intl.NumberFormat('de-DE').format(data));
    }
  }

  changeAllowanceDate(key: string) {
    const errorStr = key + "Error";
    let fromDate;
    let toDate;
    if (key === 'allowanceFromDate') {
      fromDate = this.f['allowanceFromDate'].value;
      toDate = this.f['allowanceToDate'].value;
    } else if (key === 'commitmentFromDate') {
      fromDate = this.f['commitmentFromDate'].value;
      toDate = this.f['commitmentToDate'].value;
    }
    try {
      fromDate = new Date(fromDate?.getFullYear(), fromDate?.getMonth(), fromDate?.getDate());
      toDate = new Date(toDate?.getFullYear(), toDate?.getMonth(), toDate?.getDate());
    } catch (error) {}
    if (fromDate && toDate && fromDate > toDate) {
      this.f[key].setErrors({[errorStr]: true})
    } else {
      this.f[key].setErrors(null);
    }
  }

  save() {
    this.isSubmitted = true;
    if (this.f['empTypeCode']?.value === Constant.EMP_OS_CODE) {
      const candidateSalary = new CandidateSalary();
      for (const key in candidateSalary) {
        if (key !== 'candidateSalaryId') {
          this.f[key].setValue(null);
        }
      }
    } else {
      this.f['amountFee'].setValue(null);
    }
    if (this.form.valid) {
      if (this.fileList.length === 0) {
        this.toastrService.error(this.translate.instant('staffManager.recruit.requiredFileAttach'))
        return;
      }
      const request = this.form.value;
      request.employeeId = this.f['employee'].value?.employeeId;
      request.fromDate = Utils.convertDateToSendServer(this.f['fromDate'].value);
      request.signedDate = Utils.convertDateToSendServer(this.f['signedDate'].value);
      request.organizationId = this.f['organization'].value?.orgId;
      request.type = this.type;
      request.rehireOsEmployeeId = this.recordId;
      request.isKeepSenior = this.f['isKeepSenior'].value ? Constant.KEEP_SENIOR.YES : Constant.KEEP_SENIOR.NO;
      request.pageName = this.pageName;
      request.docIdsDelete = this.docIdsDelete;
      request.candidateSalary = this.getCandidateSalary(this.form);

      const formData = new FormData();
      formData.append('data', new Blob([JSON.stringify(request)], {
        type: 'application/json'
      }));

      this.fileList.forEach((nzFile: NzUploadFile) => {
        formData.append('files', nzFile as NzSafeAny);
      });

      this.isLoadingPage = true;
      this.subscriptions.push(
        this.recruitmentService.saveWorkProcess(formData).subscribe(res => {
          this.isLoadingPage = false;
          if (res.code === HTTP_STATUS_CODE.OK) {
            const notification = this.recordId ? 'common.notification.editSuccess' : 'common.notification.addSuccess'
            this.toastrService.success(this.translate.instant(notification));
            this.onCloseModal();
          } else {
            this.toastrService.error(res.message);
          }
        }, error => {
          this.isLoadingPage = false;
          this.toastrService.error(error.message)
        })
      );
    }
  }

  getCandidateSalary(form: FormGroup): CandidateSalary | null {
    const candidateSalary = new CandidateSalary();
    let isNull = true;
    for (const key in candidateSalary) {
      candidateSalary[key] = form.controls[key].value;
      if (form.controls[key].value) {
        isNull = false;
      }
    }
    candidateSalary.allowanceFromDate = Utils.convertDateToSendServer(form.controls['allowanceFromDate'].value);
    candidateSalary.allowanceToDate = Utils.convertDateToSendServer(form.controls['allowanceToDate'].value);
    candidateSalary.commitmentFromDate = Utils.convertDateToSendServer(form.controls['commitmentFromDate'].value);
    candidateSalary.commitmentToDate = Utils.convertDateToSendServer(form.controls['commitmentToDate'].value);
    candidateSalary.monthlyBonusDate = Utils.convertDateToSendServer(form.controls['monthlyBonusDate'].value);
    return !isNull ? candidateSalary : null;
  }

  doCloseReject(refresh: boolean){
    this.isReject = false;
    if (refresh) {
      this.onCloseModal();
    }
  }

  openRejectForm() {
    this.isReject = true;
  }

  approvalById(id: number) {
    this.isLoadingPage = true;
    this.subscriptions.push(
      this.recruitmentService.approvalRecruitById(id, UrlConstant.RECRUITMENT.APPROVAL_REHIRE_OS_BY_ID).subscribe((res) => {
        this.isLoadingPage = false;
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.toastrService.success(this.translate.instant('common.notification.isApprove'));
          this.onCloseModal();
        } else {
          this.toastrService.error(res?.message)
        }
      }, error => {
        this.isLoadingPage = false;
        this.toastrService.error(error.message)
      })
    );
  }

  onCloseModal() {
    localStorage.setItem('isBackUrl', 'true');
    history.back();
  }

  selectEmpType(event: string) {
    if (!this.isRecruitFromOs && event === Constant.EMP_OS_CODE) {
      this.hideSalaryInfo = true;
      this.resetValidateRequiredSalary();
    } else {
      this.hideSalaryInfo = false;
      this.setValidateSalary();
    }
    this.updateValueAndValiditySalary();
  }

  setValidateSalary() {
    this.f['salaryGrade'].setValidators(Validators.required);
    this.f['salaryRank'].setValidators(Validators.required);
    this.f['salaryAmount'].setValidators([Validators.required, Validators.min(1)]);
    this.f['salaryPercent'].setValidators([Validators.required, Validators.min(1), Validators.max(100)]);
    this.f['amountFee'].setValidators(null);
  }

  updateValueAndValiditySalary() {
    this.f["salaryGrade"].updateValueAndValidity();
    this.f["salaryRank"].updateValueAndValidity();
    this.f["salaryAmount"].updateValueAndValidity();
    this.f["salaryPercent"].updateValueAndValidity();
    this.f["amountFee"].updateValueAndValidity();
  }


  resetValidateRequiredSalary() {
    this.f['salaryGrade'].setValidators(null);
    this.f['salaryRank'].setValidators(null);
    this.f['salaryAmount'].setValidators(Validators.min(1));
    this.f['salaryPercent'].setValidators([Validators.min(1), Validators.max(100)]);
    this.f['amountFee'].setValidators([Validators.min(1), Validators.required]);
  }

  ngOnDestroy(): void {
    this.subscriptions?.forEach(sub => sub.unsubscribe());
  }

}
