import {AfterViewInit, Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {HttpParams} from '@angular/common/http';
import {ToastrService} from 'ngx-toastr';
import {NzSafeAny} from 'ng-zorro-antd/core/types';
import {Subscription} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {saveAs} from 'file-saver';
import {AppFunction, Pagination, TableConfig} from "@hcm-mfe/shared/data-access/models";
import {Constant, UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {
  ApprovalDTO,
  DownloadFileAttachService,
  RecruitmentService,
  SearchFormService
} from "@hcm-mfe/staff-manager/data-access/services";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {getTypeExport, StringUtils} from "@hcm-mfe/shared/common/utils";
import {SearchFormRecruitComponent} from "@hcm-mfe/staff-manager/ui/recruit/search-form-recruit";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {SessionService} from "@hcm-mfe/shared/common/store";
import {ActivatedRoute, Router} from "@angular/router";
import {NzModalRef, NzModalService} from "ng-zorro-antd/modal";
import {EditRehireOsEmployeeComponent} from "../edit-rehire-os-employee/edit-rehire-os-employee.component";
import {PopupService} from "@hcm-mfe/shared/ui/popup";

@Component({
  selector: 'app-approval-recruitment-search',
  templateUrl: './approval-recruitment-search.component.html',
  styleUrls: ['./approval-recruitment-search.component.scss']
})
export class ApprovalRecruitmentSearchComponent implements OnInit, OnDestroy, AfterViewInit {
  isLoadingPage = false;
  isImportData = false;
  modal!: NzModalRef;
  tableConfig!: TableConfig;
  pagination = new Pagination();
  searchResult!: NzSafeAny[];
  constant = Constant;
  urlApiImport!: string;
  urlApiDownloadTemp!: string;
  subs: Subscription[] = [];
  recruitNewType = Constant.RECRUIT_TYPE[0].value;
  recruitOSType = Constant.RECRUIT_TYPE[1].value;
  recruitType = '';
  functionCode: string = FunctionCode.HR_RECRUITMENT;
  functionCodeOrg!: string;
  objFunction: AppFunction;
  isReject = false;
  isRejectList = false;
  recordId!: number;
  listRecordId: number[] = [];
  checkAll: boolean[] = [];
  listDraftObject: NzSafeAny[] = [];
  isShowSendBrowsingAll = false;
  pageName!: string;
  showConfirmRecruit = false;
  listConfirmRecruit: NzSafeAny[] = [];
  showAttachFile = false;
  requiredAttachFile = false;

  @ViewChild('searchForm') searchForm!: SearchFormRecruitComponent;
  @ViewChild('actionTmpl', { static: true }) action!: TemplateRef<NzSafeAny>;
  @ViewChild('approvalStatusTmpl', { static: true }) approvalStatus!: TemplateRef<NzSafeAny>;
  @ViewChild('selectTmpl', { static: true }) select!: TemplateRef<NzSafeAny>;
  @ViewChild('selectHeaderTmpl', { static: true }) selectHeader!: TemplateRef<NzSafeAny>;
  @ViewChild('attachFileTmpl', {static: true}) attachFile!: TemplateRef<NzSafeAny>;

  constructor(private searchFormService: SearchFormService,
              private translateService: TranslateService,
              private toastrService: ToastrService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private recruitmentService: RecruitmentService,
              private modalService: NzModalService,
              private downloadFileAttachService: DownloadFileAttachService,
              public sessionService: SessionService,
              private deletePopup: PopupService) {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${this.functionCode}`);
  }

  ngOnInit(): void {
    this.initTable();
    this.pageName = this.activatedRoute.snapshot?.data['screenMode'];
    this.functionCodeOrg = this.pageName === Constant.SCREEN_MODE.CREATE ? FunctionCode.PROPOSAL_CANDIDATE : FunctionCode.APPROVAL_CANDIDATE;
  }

  ngAfterViewInit() {
    this.getOldSearchForm();
    this.doSearch(1, true);
  }

  getOldSearchForm() {
    if (localStorage.getItem("oldSearchForm") && localStorage.getItem("isBackUrl")) {
      const oldSearchForm = localStorage.getItem("oldSearchForm");
      if (oldSearchForm) {
        this.searchForm.form.setValue(JSON.parse(oldSearchForm));
      }
      this.doSearch(1, true);
      localStorage.removeItem("oldSearchForm");
      localStorage.removeItem("isBackUrl");
    }
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: ' ',
          field: 'select',
          width: 60,
          tdTemplate: this.select,
          thTemplate: this.selectHeader,
          thClassList: ['text-nowrap','text-center'],
          tdClassList: ['text-nowrap','text-center'],
          fixed: window.innerWidth > 1024,
          fixedDir: 'left',
          rowspan: 2,
        },
        {
          title: 'STT',
          width: 60,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left',
          rowspan: 2,
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
        },
        {
          title: this.recruitType === this.recruitOSType ? 'staffManager.recruit.oldEmployeeCode' :'staffManager.recruit.employeeCode',
          field: 'oldEmployeeCode',
          width: 120,
          rowspan: 2,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left',
          thClassList: ['text-center'],
          remove: this.recruitType === this.recruitNewType
        },
        {
          title: 'staffManager.recruit.newEmployeeCode',
          field: 'newEmployeeCode',
          width: 120,
          rowspan: 2,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left',
          thClassList: ['text-center'],
          remove: this.recruitType !== this.recruitOSType
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.fullName',
          field: 'fullName',
          width: 140,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left',
          rowspan: 2,
          thClassList: ['text-center']
        },
        {
          title: 'staffManager.recruit.approvalStatus',
          field: 'status',
          tdTemplate: this.approvalStatus,
          width: 120,
          fixed: window.innerWidth > 1024 && this.recruitType === this.recruitNewType,
          fixedDir: 'left',
          rowspan: 2,
          thClassList: ['text-center'],
          tdClassList: ['text-center']
        },
        {
          title: 'staffManager.recruit.recruitmentType',
          field: 'typeName',
          width: 120,
          rowspan: 2,
          thClassList: ['text-center']
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.genderName',
          field: 'genderName',
          width: 80,
          rowspan: 2,
          thClassList: ['text-center'],
          remove: this.recruitType !== this.recruitNewType
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.dateOfBirth',
          field: 'dateOfBirth',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 100,
          rowspan: 2,
          remove: this.recruitType !== this.recruitNewType
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.mobileNumber',
          field: 'mobileNumber',
          width: 100,
          rowspan: 2,
          thClassList: ['text-center'],
          remove: this.recruitType !== this.recruitNewType
        },
        {
          title: 'staffManager.recruit.degreeInfo',
          colspan: 5,
          remove: this.recruitType !== this.recruitNewType,
          child: [
            {
              title: 'staffManager.recruit.schoolName',
              field: 'schoolName',
              width: 180,
              thClassList: ['text-center']
            },
            {
              title: 'staffManager.recruit.facultyName',
              field: 'facultyName',
              width: 100,
              thClassList: ['text-center']
            },
            {
              title: 'staffManager.recruit.majorLevelName',
              field: 'majorLevelName',
              width: 100,
              thClassList: ['text-center']
            },
            {
              title: 'staffManager.recruit.eduRankName',
              field: 'eduRankName',
              width: 100,
              thClassList: ['text-center']
            },
            {
              title: 'staffManager.recruit.issueYear',
              field: 'issueYear',
              width: 80,
              thClassList: ['text-center']
            }
          ]
        },
        {
          title: 'staffManager.recruit.identityInfo',
          colspan: 4,
          remove: this.recruitType !== this.recruitNewType,
          child: [
            {
              title: 'staffManager.recruit.personalIdTypeName',
              field: 'personalIdTypeName',
              width: 100,
              thClassList: ['text-center']
            },
            {
              title: 'staffManager.recruit.personalId',
              field: 'personalId',
              width: 100,
              thClassList: ['text-center']
            },
            {
              title: 'staffManager.recruit.personalIdDate',
              field: 'personalIdDate',
              width: 100,
              thClassList: ['text-center'],
              tdClassList: ['text-center']
            },
            {
              title: 'staffManager.recruit.personalIdPlace',
              field: 'personalIdPlace',
              width: 100,
              thClassList: ['text-center']
            }
          ]
        },
        {
          title: 'staffManager.recruit.workProcessInfo',
          colspan: 10,
          child: [
            {
              title: this.recruitType === this.recruitNewType ?
                    'staffManager.recruit.joinCompanyDate': 'staffManager.recruit.fromDate',
              field: this.recruitType === this.recruitNewType ? 'joinCompanyDate': 'fromDate',
              width: 150,
              thClassList: ['text-center'],
              tdClassList: ['text-center']
            },
            {
              title: 'staffManager.staffResearch.personalInformation.table.empTypeName',
              field: 'empTypeName',
              width: 150,
              thClassList: ['text-center']
            },
            {
              title: 'staffManager.staffResearch.personalInformation.table.positionName',
              field: 'jobName',
              width: 150,
              thClassList: ['text-center']
            },
            {
              title: 'staffManager.workHisInformation.label.attributes.positionGroup',
              field: 'pgrName',
              width: 150,
              thClassList: ['text-center']
            },
            {
              title: 'staffManager.staffResearch.personalInformation.table.orgName',
              field: 'orgName',
              width: 300,
              thClassList: ['text-center']
            },
            {
              title: 'staffManager.recruit.documentTypeName',
              field: 'documentTypeName',
              width: 150,
              thClassList: ['text-center']
            },
            {
              title: 'staffManager.recruit.documentNo',
              field: 'documentNo',
              width: 150,
              thClassList: ['text-center']
            },
            {
              title: 'staffManager.recruit.signedDate',
              field: 'signedDate',
              width: 150,
              thClassList: ['text-center'],
              tdClassList: ['text-center']
            },
            {
              title: 'staffManager.recruit.positionLevel',
              field: 'positionLevel',
              width: 150,
              thClassList: ['text-center']
            },
            {
              title: 'staffManager.recruit.contractTypeName',
              field: 'contractTypeName',
              width: 150,
              thClassList: ['text-center']
            }
          ]
        },
        {
          title: 'staffManager.recruit.rejectReason',
          field: 'rejectReason',
          width: 150,
          show: true,
          rowspan: 2,
        },
        {
          title: 'staffManager.recruit.attachFile',
          field: 'attachFileList',
          width: 250,
          show: true,
          tdTemplate: this.attachFile,
          rowspan: 2,
        },
        {
          title: ' ',
          field: 'action',
          rowspan: 2,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 50,
          tdTemplate: this.action,
          fixed: window.innerWidth > 1024,
          fixedDir: 'right'
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: this.pagination.pageNumber
    };
  }

  doSearch(pageIndex: number, changeHeader = false) {
    this.pagination.pageNumber = pageIndex;
    this.isShowSendBrowsingAll = false;
    let searchParam = new HttpParams();
    if (this.searchForm) {
      if (StringUtils.isNullOrEmpty(this.searchForm.form.controls['type'].value)) {
        this.toastrService.error(this.translateService.instant('staffManager.recruit.action.typeIsRequired'))
        return;
      }
      searchParam =  this.searchForm.parseOptions();
      searchParam = searchParam.append('pageName', this.pageName);
      if (changeHeader) {
        this.recruitType = this.searchForm.form.controls['type'].value;
        this.initTable();
        this.checkAll[pageIndex] = false;
        this.listRecordId = [];
      }
    }
    this.isLoadingPage = true;
    const url = this.recruitType === Constant.RECRUIT_TYPE[0].value ? UrlConstant.SEARCH_FORM.APPROVE_RECRUITMENT : UrlConstant.SEARCH_FORM.REHIRE_OS;
    this.subs.push(
      this.searchFormService.getFilterResearch(url, searchParam, this.pagination.getCurrentPage()).subscribe(res => {
        this.isLoadingPage = false;
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.searchResult = res.data.listData.map((item: NzSafeAny) => {
            const typeName = Constant.RECRUIT_TYPE.filter(data => data.value === this.recruitType)[0]?.label
            if (typeName) {
              item.typeName = this.translateService.instant(typeName);
            }
            return item;
          });
          if (this.objFunction?.create) {
            this.isShowSendBrowsingAll = this.searchResult.some(item => item.status === ~~Constant.APPROVAL_STATUS[0].value) || res.data.count > this.pagination.pageSize;
          }
          this.tableConfig.total = res.data.count;
        }
      }, error => {
        this.toastrService.error(error?.message);
        this.isLoadingPage = false;
      })
    );
  }


  onLoadPage(isLoadPage: boolean) {
    this.isLoadingPage = isLoadPage;
  }

  doExportData() {
    const url = this.recruitType === this.recruitNewType ? UrlConstant.EXPORT_REPORT.RECRUIT_NEW : UrlConstant.EXPORT_REPORT.REHIRE_OS;
    let searchParam = this.searchForm ? this.searchForm.parseOptions() : new HttpParams();
    searchParam = searchParam.set('type', this.recruitType);
    this.isLoadingPage = true;
    this.subs.push(
      this.searchFormService.doExportFile(url, searchParam).subscribe(res => {
        this.isLoadingPage = false;
        if (res?.headers?.get('Excel-File-Empty')) {
          this.toastrService.error(this.translateService.instant("common.notification.exportFail"));
          return;
        } else {
          const reportFile = new Blob([res.body], {type: getTypeExport('xlsx')});
          saveAs(reportFile, 'danh_sach_tuyen_dung.xlsx');
        }
      }, error => {
        this.toastrService.error(error.message);
        this.isLoadingPage = false;
      })
    );
  }

  doImportData(type: string) {
    this.isImportData = true;
    this.showAttachFile = true;
    this.requiredAttachFile = true;
    if (type === Constant.RECRUIT_TYPE[0].value) {
      this.urlApiDownloadTemp = UrlConstant.IMPORT_FORM.ADD_NEW_EMPLOYEE_TEMPLATE;
      this.urlApiImport = UrlConstant.IMPORT_FORM.ADD_NEW_EMPLOYEE;
    } else {
      this.urlApiDownloadTemp = UrlConstant.IMPORT_FORM.RECRUIT_FROM_OS_TEMPLATE;
      this.urlApiImport = UrlConstant.IMPORT_FORM.RECRUIT_FROM_OS;
    }
  }

  doCloseImport(refresh: boolean) {
    this.isImportData = false;
    if (refresh) {
      this.doSearch(this.pagination.pageNumber)
    }
  }

  openEditRecruitment(recordId: number, type : string, action: string) {
    let url: string;
    switch (type) {
      case this.recruitNewType:
        if (this.pageName === Constant.SCREEN_MODE.APPROVAL) {
          url = action === Constant.ACTION.DETAIL ? (UrlConstant.URL_NAVIGATE.DETAIL_APPROVAL_CANDIDATE_NEW + recordId) : (UrlConstant.URL_NAVIGATE.EDIT_APPROVAL_CANDIDATE_NEW + recordId);
        } else {
          url = action === Constant.ACTION.DETAIL ? (UrlConstant.URL_NAVIGATE.DETAIL_PROPOSAL_CANDIDATE_NEW + recordId) : (UrlConstant.URL_NAVIGATE.EDIT_PROPOSAL_CANDIDATE_NEW + recordId);
        }
        break;
      case this.recruitOSType:
        if (this.pageName === Constant.SCREEN_MODE.APPROVAL) {
          url = action === Constant.ACTION.DETAIL ? (UrlConstant.URL_NAVIGATE.DETAIL_APPROVAL_RECRUITMENT_OS + recordId) : '';
        } else {
          url = action === Constant.ACTION.DETAIL ? (UrlConstant.URL_NAVIGATE.DETAIL_RECRUITMENT_OS + recordId) : (UrlConstant.URL_NAVIGATE.EDIT_RECRUITMENT_OS + recordId);
        }
        break;
      default :
        if (this.pageName === Constant.SCREEN_MODE.APPROVAL) {
          url = action === Constant.ACTION.DETAIL ? (UrlConstant.URL_NAVIGATE.DETAIL_APPROVAL_RECRUITMENT_REHIRE + recordId) : '';
        } else {
          url = action === Constant.ACTION.DETAIL ? (UrlConstant.URL_NAVIGATE.DETAIL_RECRUITMENT_REHIRE + recordId) : (UrlConstant.URL_NAVIGATE.EDIT_RECRUITMENT_REHIRE + recordId);
        }
        break;
    }
    localStorage.setItem("oldSearchForm", JSON.stringify(this.searchForm.form.value));
    this.router.navigateByUrl(url).then();
  }

  openAddRecruit(type: string) {
    let url = UrlConstant.URL_NAVIGATE.ADD_PROPOSAL_CANDIDATE_NEW;
    if (type === Constant.RECRUIT_TYPE[1].value) {
      url = UrlConstant.URL_NAVIGATE.ADD_RECRUITMENT_OS;
    } else if (type === Constant.RECRUIT_TYPE[2].value) {
      url = UrlConstant.URL_NAVIGATE.ADD_RECRUITMENT_REHIRE;
    }
    this.router.navigateByUrl(url).then();
  }

  onDblClickRow(event: NzSafeAny) {
    this.openEditRecruitment(event.recordId, this.recruitType, Constant.ACTION.DETAIL);
  }


  openModal(recordId: number | null, type: string, action: string, title: string) {
    this.modal = this.modalService.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.2 > 1100 ? 1100 : window.innerWidth / 1.2 : window.innerWidth,
      nzTitle: this.translateService.instant(title),
      nzContent: EditRehireOsEmployeeComponent,
      nzComponentParams: {
        recordId: recordId,
        type: type,
        action: action,
        pageName: this.pageName,
        objFunction: this.objFunction
      },
      nzFooter: null
    });
    this.subs.push(
      this.modal.afterClose.subscribe(res => {
        if (res && res.refresh) {
          this.doSearch(1);
        }
      })
    );
  }

  onApprovalById(id: number) {
    this.isLoadingPage = true;
    const url = this.recruitNewType === this.recruitType ?
      UrlConstant.RECRUITMENT.APPROVAL_RECRUIT_NEW_BY_ID : UrlConstant.RECRUITMENT.APPROVAL_REHIRE_OS_BY_ID;
    this.subs.push(
      this.recruitmentService.approvalRecruitById(id, url).subscribe((res) => {
        this.isLoadingPage = false;
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.toastrService.success(this.translateService.instant('common.notification.isApprove'));
          this.doSearch(this.pagination.pageNumber, true);
        } else {
          this.toastrService.error(res?.message)
        }
      }, error => {
        this.isLoadingPage = false;
        this.toastrService.error(error.message)
      })
    );
  }

  approveByList(type: string) {
    const url = type === this.recruitNewType ?
      UrlConstant.RECRUITMENT.APPROVAL_RECRUIT_NEW_BY_LIST : UrlConstant.RECRUITMENT.APPROVAL_REHIRE_OS_BY_LIST;
    this.isLoadingPage = true;
    this.subs.push(
      this.recruitmentService.approvalRecruitByList(this.listRecordId, url).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.toastrService.success(this.translateService.instant('common.notification.isApprove'));
          this.doSearch(this.pagination.pageNumber, true);
        } else {
          this.toastrService.error(res?.message);
        }
        this.isLoadingPage = false;
      }, error => {
        this.toastrService.error(error?.message);
        this.isLoadingPage = false;
      })
    );
  }

  onRejectById(id: number) {
    this.isReject = true;
    this.recordId = id;
  }

  getUrlReject(type: string, isList: boolean): string {
    let url: string;
    if (type !== this.recruitNewType) {
      url = isList ? UrlConstant.RECRUITMENT.REJECT_REHIRE_OS_BY_LIST : UrlConstant.RECRUITMENT.REJECT_REHIRE_OS_BY_ID;
    } else {
      url = isList ? UrlConstant.RECRUITMENT.REJECT_RECRUIT_NEW_BY_LIST : UrlConstant.RECRUITMENT.REJECT_RECRUIT_NEW_BY_ID;
    }
    return url;
  }

  doCloseReject(refresh: boolean){
    this.isReject = false;
    if (refresh) {
      this.doSearch(this.pagination.pageNumber, true);
    }
  }

  resetListCandidateId() {
    this.listRecordId = [];
  }

  onCheckChange(event: boolean, id: number){
    if (event) {
      this.listRecordId.push(id);
      const data = this.searchResult.filter(item => item.recordId === id)[0];
      this.listDraftObject.push({id: id, status: data.status, employeeName: data.fullName}
      );
    } else {
      this.listRecordId.splice(this.listRecordId.indexOf(id),1);
      this.listDraftObject = this.listDraftObject.filter(item => item.id !== id);
    }
    const notCheckAll = this.searchResult.some(item => {
      return this.listRecordId.indexOf(item.recordId) < 0;
    });
    this.checkAll[this.pagination.pageNumber] = !notCheckAll;
  }

  onCheckAll(event: boolean) {
    if (event) {
      this.searchResult.forEach(item => {
        if ((this.pageName === Constant.SCREEN_MODE.APPROVAL && item.status === ~~Constant.APPROVAL_STATUS[2].value)
          || (this.pageName === Constant.SCREEN_MODE.CREATE && item.status === ~~Constant.APPROVAL_STATUS[0].value)) {
          const id = item.recordId;
          const data = {
            id: id,
            status: item.status,
            employeeName: item.fullName
          }
          if (this.listRecordId.indexOf(id) < 0) {
            this.listRecordId.push(id);
            this.listDraftObject.push(data);
          }
        }
      })
    } else {
      this.searchResult.forEach(item => {
        const id = item.recordId;
        this.listRecordId = this.listRecordId.filter(recordId => recordId != id);
        this.listDraftObject = this.listDraftObject.filter(data => data.id != id);
      });
    }
  }

  onDelete(id: number, type: string) {
    const url = type === this.recruitNewType ? UrlConstant.RECRUITMENT.GET_CANDIDATE_BY_ID : UrlConstant.RECRUITMENT.GET_REHIRE_OS_BY_ID;
    this.deletePopup.showModal(() => {
      this.isLoadingPage = true;
      this.subs.push(
        this.recruitmentService.deleteDataById(id, url).subscribe(res => {
          this.isLoadingPage = false;
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.toastrService.success(this.translateService.instant('common.notification.deleteSuccess'));
            this.doSearch(this.pagination.pageNumber);
          } else {
            this.toastrService.error(res?.message);
          }
        }, error => {
          this.isLoadingPage = false;
          this.toastrService.error(error.message)
        })
      );
    });
  }

  sendBrowsingAll() {
    const url = this.recruitType === this.recruitNewType ? UrlConstant.RECRUITMENT.SEND_REQUEST_NEW_ALL : UrlConstant.RECRUITMENT.SEND_REQUEST_OS_ALL;
    const searchParam = this.searchForm ? this.searchForm.parseOptions() : new HttpParams();
    this.isLoadingPage = true;
    this.subs.push(
      this.recruitmentService.approvalRecruitAll(searchParam, url).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.toastrService.success(this.translateService.instant('common.notification.sendBrowseSuccess'));
          this.doSearch(this.pagination.pageNumber, true);
        } else {
          this.toastrService.error(res?.message);
        }
        this.isLoadingPage = false;
      }, error => {
        this.toastrService.error(error?.message);
        this.isLoadingPage = false;
      })
    );
  }

  sendBrowsingByList(type: string) {
    const url = type === this.recruitNewType ?
      UrlConstant.RECRUITMENT.SEND_REQUEST_NEW : UrlConstant.RECRUITMENT.SEND_REQUEST_OS;
    this.isLoadingPage = true;
    this.subs.push(
      this.recruitmentService.approvalRecruitByList(this.listRecordId, url, Constant.SEND_REQUEST.YES).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.toastrService.success(this.translateService.instant('common.notification.sendBrowseSuccess'));
          this.doSearch(this.pagination.pageNumber, true);
        } else {
          this.toastrService.error(res?.message);
        }
        this.isLoadingPage = false;
      }, error => {
        this.toastrService.error(error?.message);
        this.isLoadingPage = false;
      })
    );
  }

  openRejectByList() {
    const listRejected:string[] = [];
    let error = false;
    this.listDraftObject.forEach(data => {
      if(data.status === ~~Constant.APPROVAL_STATUS[4].value) {
        listRejected.push(data.employeeName);
        error = true;
      }
    })
    if(error){
      this.toastrService.error(this.translateService.instant('staffManager.recruit.employee') + " {" + listRejected.toString() + "} " + this.translateService.instant('common.notification.rejected'));
    } else {
      this.isReject = true;
      this.isRejectList = true;
    }
  }

  confirmRecruit(id: number, status: number) {
    this.isLoadingPage = true;
    const data: ApprovalDTO[] = [{id, status}];
    this.subs.push(
      this.recruitmentService.confirmRecruit(data, UrlConstant.RECRUITMENT.CONFIRM_BY_LIST).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.toastrService.success(this.translateService.instant('staffManager.notification.confirmSuccess'));
          this.doSearch(this.pagination.pageNumber, true);
        } else {
          this.toastrService.error(res?.message);
        }
        this.isLoadingPage = false;
      }, error => {
        this.toastrService.error(error?.message);
        this.isLoadingPage = false;
      })
    );
  }

  sendBrowsingOrCancel(id: number, type: string, status: number) {
    const data: ApprovalDTO = {listId: [id], status};
    const url = type === this.recruitNewType ? UrlConstant.RECRUITMENT.SEND_REQUEST_NEW : UrlConstant.RECRUITMENT.SEND_REQUEST_OS;
    this.isLoadingPage = true;
    this.subs.push(
      this.recruitmentService.sendBrowsingOrCancel(data, url).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          if (status === Constant.SEND_REQUEST.YES) {
            this.toastrService.success(this.translateService.instant('common.notification.sendBrowseSuccess'));
          } else {
            this.toastrService.success(this.translateService.instant('common.notification.cancelRequestSuccess'));
          }
          this.doSearch(this.pagination.pageNumber, true);
        } else {
          this.toastrService.error(res?.message);
        }
        this.isLoadingPage = false;
      }, error => {
        this.toastrService.error(error?.message);
        this.isLoadingPage = false;
      })
    );
  }

  getIsEdit(status: number) : boolean {
    const listStatusIsEdit: number[] = [~~Constant.APPROVAL_STATUS[0].value];
    if (this.pageName === Constant.SCREEN_MODE.CREATE) {
      listStatusIsEdit.push(~~Constant.APPROVAL_STATUS[4].value)
    } else {
      listStatusIsEdit.push(~~Constant.APPROVAL_STATUS[2].value)
    }
    return listStatusIsEdit.includes(status) && this.objFunction.edit && this.pageName === Constant.SCREEN_MODE.CREATE;
  }

  doDownloadAttach(file: NzSafeAny) {
    this.isLoadingPage = true;
    this.subs.push(
      this.downloadFileAttachService.doDownloadAttachFileWithSecurity(~~file?.docId, file?.security).subscribe(res => {
        this.isLoadingPage = false;
        if (res) {
          const reportFile = new Blob([res.body], { type: getTypeExport(file.fileName.split('.').pop()) });
          saveAs(reportFile, file.fileName);
        }
      }, () => {
        this.isLoadingPage = false;
        this.toastrService.error(this.translateService.instant('staffManager.acceptance.downloadFileError'));
      })
    );
  }

  checkListConfirm(data: NzSafeAny[]) {
    if (data?.length > 0) {
      this.listConfirmRecruit = data;
      this.showConfirmRecruit = true;
    }
  }

  onCloseModalConfirm() {
    this.showConfirmRecruit = false;
    this.doSearch(this.pagination.pageNumber);
  }

  ngOnDestroy(): void {
    this.subs?.forEach(sub => sub?.unsubscribe());
  }

  getDisabledCheckBox(status: number): boolean {
    if (this.pageName === Constant.SCREEN_MODE.CREATE) {
      return status !== ~~Constant.APPROVAL_STATUS[0].value;
    } else {
     return status !== ~~Constant.APPROVAL_STATUS[2].value;
    }
  }
}

