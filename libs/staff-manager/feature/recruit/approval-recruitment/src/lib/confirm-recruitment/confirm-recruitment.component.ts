import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, TemplateRef, ViewChild} from '@angular/core';
import {TableConfig} from "@hcm-mfe/shared/data-access/models";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {Constant, UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {ApprovalDTO, RecruitmentService} from "@hcm-mfe/staff-manager/data-access/services";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";
import {Subscription} from "rxjs";

@Component({
  selector: 'hcm-mfe-confirm-recruitment',
  templateUrl: './confirm-recruitment.component.html',
  styleUrls: ['./confirm-recruitment.component.scss']
})
export class ConfirmRecruitmentComponent implements OnInit, OnDestroy {
  @Input() showContent = false;
  @Input() listConfirmRecruit: NzSafeAny[] = [];
  @Output() closeModal = new EventEmitter<boolean>();
  @Output() loadPage = new EventEmitter<boolean>();

  nzWidth = window.innerWidth/ 1.2;
  tableConfig!: TableConfig;
  constant = Constant;
  isLoadingPage = false;
  subs: Subscription[] = [];

  @ViewChild('actionTmpl', { static: true }) action!: TemplateRef<NzSafeAny>;
  @ViewChild('statusTmpl', { static: true }) status!: TemplateRef<NzSafeAny>;

  constructor(private recruitmentService: RecruitmentService,
              private toastrService: ToastrService,
              private translateService: TranslateService) {
  }

  ngOnInit(): void {
    this.initTable();
  }

  initTable() {
    this.tableConfig = {
      headers: [
        {
          title: 'STT',
          width: 50,
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
        },
        {
          title: 'staffManager.recruit.candidateName',
          field: 'fullName',
          width: 130,
          thClassList: ['text-center']
        },
        {
          title: 'staffManager.recruit.personalId',
          field: 'personalId',
          width: 100,
          thClassList: ['text-center']
        },
        {
          title: 'staffManager.recruit.empCodeDuplicate',
          field: 'duplicateEmployeeCode',
          width: 100,
          thClassList: ['text-center']
        },
        {
          title: 'staffManager.recruit.empNameDuplicate',
          field: 'duplicateFullName',
          width: 120,
          thClassList: ['text-center']
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.dateOfBirth',
          field: 'duplicateDateOfBirth',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 100,
        },
        {
          title: 'staffManager.staffResearch.personalInformation.table.orgName',
          field: 'duplicateOrgName',
          width: 200,
          thClassList: ['text-center']
        },
        {
          title: 'staffManager.recruit.statusDuplicate',
          field: 'duplicateType',
          tdTemplate: this.status,
          width: 140,
          thClassList: ['text-center']
        },
        {
          title: ' ',
          field: 'action',
          tdTemplate: this.action,
          tdClassList: ['text-nowrap','text-center'], thClassList: ['text-nowrap','text-center'],
          width: 50,
          fixed: window.innerWidth > 1024,
          fixedDir: 'right'
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: 1000,
      pageIndex: 1
    };
  }

  confirmRecruit(id: number, status: number) {
    this.loadPage.emit(true);
    const data: ApprovalDTO[] = [{id, status}];
    this.subs.push(
      this.recruitmentService.confirmRecruit(data, UrlConstant.RECRUITMENT.CONFIRM_BY_LIST).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.toastrService.success(this.translateService.instant('staffManager.notification.confirmSuccess'));
          this.listConfirmRecruit = this.listConfirmRecruit.filter(item => item.candidateId !== id);
        } else {
          this.toastrService.error(res?.message);
        }
        this.loadPage.emit(false);
      }, error => {
        this.toastrService.error(error?.message);
        this.loadPage.emit(false);
      })
    );
  }

  onDelete(id: number) {
    this.loadPage.emit(true);
    this.subs.push(
      this.recruitmentService.deleteDataById(id, UrlConstant.RECRUITMENT.GET_CANDIDATE_BY_ID).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.toastrService.success(this.translateService.instant('common.notification.deleteSuccess'));
          this.listConfirmRecruit = this.listConfirmRecruit.filter(item => item.candidateId !== id);
        } else {
          this.toastrService.error(res?.message);
        }
        this.loadPage.emit(false);
      }, error => {
        this.loadPage.emit(false);
        this.toastrService.error(error.message)
      })
    );
  }

  doClose() {
    this.showContent = false;
    this.closeModal.emit(false);
  }

  ngOnDestroy(): void {
    this.subs?.forEach(sub => sub?.unsubscribe());
  }

}
