import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {LayoutResearchComponent} from "@hcm-mfe/staff-manager/feature/research/layout-research";
import {LayoutDraftComponent} from "@hcm-mfe/staff-manager/feature/draft/layout-draft";
import {RoleGuardService} from "@hcm-mfe/shared/core";
import {FunctionCode, Scopes} from "@hcm-mfe/shared/common/enums";
import {Constant, ReportFunctionCode} from "@hcm-mfe/staff-manager/data-access/common";
import {LayoutReportComponent} from "@hcm-mfe/staff-manager/feature/report/layout-report";
import {LayoutCategoriesComponent} from "@hcm-mfe/staff-manager/feature/categories/layout-categories";

const routes: Routes = [
  {
    path: '',
    data: {
      pageName: 'staffManager.pageName.staffInfo',
      breadcrumb: 'staffManager.breadcrumb.staffInfo'
    },
    children: [
      {
        path: 'personal-info',
        data: {
          pageName: 'staffManager.pageName.personalInfo',
          breadcrumb: 'staffManager.breadcrumb.personalInfo',
          isNeedShowBackBtn: true
        },
        loadChildren: () => import('@hcm-mfe/staff-manager/feature/info/personal-informations').then(m => m.StaffManagerFeatureInfoPersonalInformationsModule),
      }
    ]
  },
  {
    path: 'staff-research',
    component: LayoutResearchComponent,
    data: {
      pageName: 'staffManager.pageName.staffResearch',
      breadcrumb: 'staffManager.breadcrumb.staffResearch'
    },
    children: [
      {
        path: 'basic',
        canActivateChild: [RoleGuardService],
        children: [{
          path: '',
          loadChildren: () => import('@hcm-mfe/staff-manager/feature/research/basic-research').then(m => m.StaffManagerFeatureResearchBasicResearchModule),
          data: {
            code: FunctionCode.HR_PERSONAL_INFO,
            scope: Scopes.VIEW,
            pageName: 'staffManager.pageName.basicInformation',
            breadcrumb: 'staffManager.breadcrumb.basicInformation'
          }
        }],
      },
      {
        path: 'edu-his',
        canActivateChild: [RoleGuardService],
        children: [{
          path: '',
          loadChildren: () => import('@hcm-mfe/staff-manager/feature/research/edu-his-research').then(m => m.StaffManagerFeatureResearchEduHisResearchModule),
          data: {
            code: FunctionCode.HR_EDU_PROCESS,
            scope: Scopes.VIEW,
            pageName: 'staffManager.pageName.eduHisResearch',
            breadcrumb: 'staffManager.breadcrumb.eduHisResearch'
          }
        }],
      },
      {
        path: 'identity-info',
        canActivateChild: [RoleGuardService],
        children: [{
          path: '',
          loadChildren: () => import('@hcm-mfe/staff-manager/feature/research/identity-research').then(m => m.StaffManagerFeatureResearchIdentityResearchModule),
          data: {
            code: FunctionCode.HR_IDENTITY,
            scope: Scopes.VIEW,
            pageName: 'staffManager.pageName.identityResearch',
            breadcrumb: 'staffManager.breadcrumb.identityResearch'
          }
        }],
      },
      {
        path: 'salary-his',
        canActivateChild: [RoleGuardService],
        children: [{
          path: '',
          loadChildren: () => import('@hcm-mfe/staff-manager/feature/research/salary-his-research').then(m => m.StaffManagerFeatureResearchSalaryHisResearchModule),
          data: {
            code: FunctionCode.HR_SALARY_PROCESS,
            scope: Scopes.VIEW,
            pageName: 'staffManager.pageName.salaryHisResearch',
            breadcrumb: 'staffManager.breadcrumb.salaryHisResearch'
          }
        }],
      },
      {
        path: 'allowance-his',
        canActivateChild: [RoleGuardService],
        children: [{
          path: '',
          loadChildren: () => import('@hcm-mfe/staff-manager/feature/research/allowance-his-research').then(m => m.StaffManagerFeatureResearchAllowanceHisResearchModule),
          data: {
            code: FunctionCode.HR_ALLOWANCE_PROCESS,
            scope: Scopes.VIEW,
            pageName: 'staffManager.pageName.allowanceHisResearch',
            breadcrumb: 'staffManager.breadcrumb.allowanceHisResearch'
          }
        }],
      },
      {
        path: 'work-his-before',
        canActivateChild: [RoleGuardService],
        children: [{
          path: '',
          loadChildren: () => import('@hcm-mfe/staff-manager/feature/research/work-his-before-research').then(m => m.StaffManagerFeatureResearchWorkHisBeforeResearchModule),
          data: {
            code: FunctionCode.HR_WORK_OUTSIDE,
            scope: Scopes.VIEW,
            pageName: 'staffManager.pageName.workHisBeforeResearch',
            breadcrumb: 'staffManager.breadcrumb.workHisBeforeResearch'
          }
        }],
      },
      {
        path: 'work-his',
        canActivateChild: [RoleGuardService],
        children: [{
          path: '',
          loadChildren: () => import('@hcm-mfe/staff-manager/feature/research/work-his-research').then(m => m.StaffManagerFeatureResearchWorkHisResearchModule),
          data: {
            code: FunctionCode.HR_WORK_PROCESS,
            scope: Scopes.VIEW,
            pageName: 'staffManager.pageName.workHisResearch',
            breadcrumb: 'staffManager.breadcrumb.workHisResearch'
          }
        }],
      },
      {
        path: 'project-his',
        canActivateChild: [RoleGuardService],
        children: [{
          path: '',
          loadChildren: () => import('@hcm-mfe/staff-manager/feature/research/project-his-research').then(m => m.StaffManagerFeatureResearchProjectHisResearchModule),
          data: {
            code: FunctionCode.HR_PROJECT_MEMBER,
            scope: Scopes.VIEW,
            pageName: 'staffManager.pageName.projectHisResearch',
            breadcrumb: 'staffManager.breadcrumb.projectHisResearch'
          }
        }],
      },
      {
        path: 'contract-his',
        canActivateChild: [RoleGuardService],
        children: [{
          path: '',
          loadChildren: () => import('@hcm-mfe/staff-manager/feature/research/contract-his-research').then(m => m.StaffManagerFeatureResearchContractHisResearchModule),
          data: {
            code: FunctionCode.HR_CONTRACT_PROCESS,
            scope: Scopes.VIEW,
            pageName: 'staffManager.pageName.contractHisResearch',
            breadcrumb: 'staffManager.breadcrumb.contractHisResearch'
          }
        }],
      },
      {
        path: 'degree',
        canActivateChild: [RoleGuardService],
        children: [{
          path: '',
          loadChildren: () => import('@hcm-mfe/staff-manager/feature/research/degree-research').then(m => m.StaffManagerFeatureResearchDegreeResearchModule),
          data: {
            code: FunctionCode.HR_EDU_DEGREE,
            scope: Scopes.VIEW,
            pageName: 'staffManager.pageName.educationDegree',
            breadcrumb: 'staffManager.breadcrumb.educationDegree'
          }
        }],
      },
      {
        path: 'bank-info',
        canActivateChild: [RoleGuardService],
        children: [{
          path: '',
          loadChildren: () => import('@hcm-mfe/staff-manager/feature/research/bank-research').then(m => m.StaffManagerFeatureResearchBankResearchModule),
          data: {
            code: FunctionCode.HR_ACCOUNT,
            scope: Scopes.VIEW,
            pageName: 'staffManager.pageName.bankResearch',
            breadcrumb: 'staffManager.breadcrumb.bankResearch'
          }
        }],
      },
      {
        path: 'relative-info',
        canActivateChild: [RoleGuardService],
        children: [{
          path: '',
          loadChildren: () => import('@hcm-mfe/staff-manager/feature/research/family-relationship-research').then(m => m.StaffManagerFeatureResearchFamilyRelationshipResearchModule),
          data: {
            code: FunctionCode.HR_FAMILY_RELATIONSHIP,
            scope: Scopes.VIEW,
            pageName: 'staffManager.pageName.relativeResearch',
            breadcrumb: 'staffManager.breadcrumb.relativeResearch'
          }
        }],
      },
      {
        path: 'family-deductions-info',
        canActivateChild: [RoleGuardService],
        children: [{
          path: '',
          loadChildren: () => import('@hcm-mfe/staff-manager/feature/research/dependent-person-research').then(m => m.StaffManagerFeatureResearchDependentPersonResearchModule),
          data: {
            code: FunctionCode.HR_DEPENDENT_PERSON,
            scope: Scopes.VIEW,
            pageName: 'staffManager.pageName.familyDeductionsResearch',
            breadcrumb: 'staffManager.breadcrumb.familyDeductionsResearch'
          }
        }],
      },
      {
        path: 'decpline-info',
        canActivateChild: [RoleGuardService],
        children: [{
          path: '',
          loadChildren: () => import('@hcm-mfe/staff-manager/feature/research/decpline-research').then(m => m.StaffManagerFeatureResearchDecplineResearchModule),
          data: {
            code: FunctionCode.HR_DECPLINE,
            scope: Scopes.VIEW,
            pageName: 'staffManager.pageName.decplineResearch',
            breadcrumb: 'staffManager.breadcrumb.decplineResearch'
          }
        }],
      },
      {
        path: 'reward-info',
        canActivateChild: [RoleGuardService],
        children: [{
          path: '',
          loadChildren: () => import('@hcm-mfe/staff-manager/feature/research/reward-record-research').then(m => m.StaffManagerFeatureResearchRewardRecordResearchModule),
          data: {
            code: FunctionCode.HR_REWARD,
            scope: Scopes.VIEW,
            pageName: 'staffManager.pageName.rewardRecordResearch',
            breadcrumb: 'staffManager.breadcrumb.rewardRecordResearch'
          }
        }],
      },
      {
        path: 'manager-process',
        canActivateChild: [RoleGuardService],
        children: [{
          path: '',
          loadChildren: () => import('@hcm-mfe/staff-manager/feature/research/manager-process').then(m => m.StaffManagerFeatureResearchManagerProcessModule),
          data: {
            code: FunctionCode.HR_MANAGER_PROCESS,
            scope: Scopes.VIEW,
            pageName: 'staffManager.pageName.managerProcessResearch',
            breadcrumb: 'staffManager.breadcrumb.managerProcessResearch'
          }
        }],
      }
    ]
  },
  {
    path: 'staff-report',
    component: LayoutReportComponent,
    data: {
      pageName: 'staffManager.pageName.staffReport',
      breadcrumb: 'staffManager.breadcrumb.staffReport'
    },
    children: [
      {
        path: ReportFunctionCode.EMPLOYEE_REPORT_SUMMARY.toLowerCase(),
        loadChildren: () => import('@hcm-mfe/staff-manager/feature/report/staff-report').then(m => m.StaffManagerFeatureReportStaffReportModule),
        canActivateChild: [RoleGuardService],
        data: {
          code: ReportFunctionCode.EMPLOYEE_REPORT_SUMMARY,
          scope: Scopes.VIEW,
          pageName: 'staffManager.pageName.staffReportSummary',
          breadcrumb: 'staffManager.breadcrumb.staffReportSummary'
        }
      },
      {
        path: ReportFunctionCode.EMPLOYEE_REPORT_EMP_FULL.toLowerCase(),
        loadChildren: () => import('@hcm-mfe/staff-manager/feature/report/staff-report').then(m => m.StaffManagerFeatureReportStaffReportModule),
        canActivateChild: [RoleGuardService],
        data: {
          code: ReportFunctionCode.EMPLOYEE_REPORT_EMP_FULL,
          scope: Scopes.VIEW,
          pageName: 'staffManager.pageName.staffReportFull',
          breadcrumb: 'staffManager.breadcrumb.staffReportFull'
        }
      },
      {
        path: ReportFunctionCode.EMPLOYEE_REPORT_TERMINATION.toLowerCase(),
        loadChildren: () => import('@hcm-mfe/staff-manager/feature/report/staff-report').then(m => m.StaffManagerFeatureReportStaffReportModule),
        canActivateChild: [RoleGuardService],
        data: {
          code: ReportFunctionCode.EMPLOYEE_REPORT_TERMINATION,
          scope: Scopes.VIEW,
          pageName: 'staffManager.pageName.staffReportEmpTermination',
          breadcrumb: 'staffManager.breadcrumb.staffReportEmpTermination'
        }
      },
      {
        path: ReportFunctionCode.EMPLOYEE_REPORT_TRANSFER.toLowerCase(),
        loadChildren: () => import('@hcm-mfe/staff-manager/feature/report/staff-report').then(m => m.StaffManagerFeatureReportStaffReportModule),
        canActivateChild: [RoleGuardService],
        data: {
          code: ReportFunctionCode.EMPLOYEE_REPORT_TRANSFER,
          scope: Scopes.VIEW,
          pageName: 'staffManager.pageName.staffReportEmpTransfer',
          breadcrumb: 'staffManager.breadcrumb.staffReportEmpTransfer'
        }
      },
      {
        path: ReportFunctionCode.EMPLOYEE_REPORT_LEAVE.toLowerCase(),
        loadChildren: () => import('@hcm-mfe/staff-manager/feature/report/staff-report').then(m => m.StaffManagerFeatureReportStaffReportModule),
        canActivateChild: [RoleGuardService],
        data: {
          code: ReportFunctionCode.EMPLOYEE_REPORT_LEAVE,
          scope: Scopes.VIEW,
          pageName: 'staffManager.pageName.staffReportEmpLeave',
          breadcrumb: 'staffManager.breadcrumb.staffReportEmpLeave'
        }
      },
      {
        path: ReportFunctionCode.EMPLOYEE_REPORT_RESUME.toLowerCase(),
        loadChildren: () => import('@hcm-mfe/staff-manager/feature/report/staff-report-resume').then(m => m.StaffManagerFeatureReportStaffReportResumeModule),
        canActivateChild: [RoleGuardService],
        data: {
          code: ReportFunctionCode.EMPLOYEE_REPORT_RESUME,
          scope: Scopes.VIEW,
          pageName: 'staffManager.pageName.staffReportEmpResume',
          breadcrumb: 'staffManager.breadcrumb.staffReportEmpResume'
        }
      },
    ]
  },
  {
    path: 'staff-draft',
    component: LayoutDraftComponent,
    data: {
      pageName: 'staffManager.pageName.staffResearch',
      breadcrumb: 'staffManager.breadcrumb.staffDraft'
    },
    children: [
      {
        path: 'identity',
        canActivateChild: [RoleGuardService],
        children: [{
          path: '',
          loadChildren: () => import('@hcm-mfe/staff-manager/feature/draft/identity').then(m => m.StaffManagerFeatureDraftIdentityModule),
          data: {
            code: FunctionCode.HR_MODIFY_IDENTITY,
            scope: Scopes.VIEW,
            pageName: 'staffManager.pageName.identityResearch',
            breadcrumb: 'staffManager.breadcrumb.identityResearch'
          }
        }]
      },
      {
        path: 'bank-accounts',
        canActivateChild: [RoleGuardService],
        children: [{
          path: '',
          loadChildren: () => import('@hcm-mfe/staff-manager/feature/draft/bank-accounts').then(m => m.StaffManagerFeatureDraftBankAccountsModule),
          data: {
            code: FunctionCode.HR_MODIFY_ACCOUNT,
            scope: Scopes.VIEW,
            pageName: 'staffManager.pageName.bankResearch',
            breadcrumb: 'staffManager.breadcrumb.bankResearch'
          }
        }]
      },
      {
        path: 'dependent-persons',
        canActivateChild: [RoleGuardService],
        children: [{
          path: '',
          loadChildren: () => import('@hcm-mfe/staff-manager/feature/draft/dependent-person').then(m => m.StaffManagerFeatureDraftDependentPersonModule),
          data: {
            code: FunctionCode.HR_MODIFY_DEPENDENT,
            scope: Scopes.VIEW,
            pageName: 'staffManager.pageName.relativeResearch',
            breadcrumb: 'Thông tin giảm trừ gia cảnh'
          }
        }]
      },
      {
        path: 'work-process',
        canActivateChild: [RoleGuardService],
        children: [{
          path: '',
          loadChildren: () => import('@hcm-mfe/staff-manager/feature/draft/work-process').then(m => m.StaffManagerFeatureDraftWorkProcessModule),
          data: {
            code: FunctionCode.HR_MODIFY_WORK_PROCESS,
            scope: Scopes.VIEW,
            pageName: 'staffManager.pageName.workHisResearch',
            breadcrumb: 'staffManager.breadcrumb.workHisResearch'
          }
        }]
      },
      {
        path: 'project-members',
        canActivateChild: [RoleGuardService],
        children: [{
          path: '',
          loadChildren: () => import('@hcm-mfe/staff-manager/feature/draft/project-members').then(m => m.StaffManagerFeatureDraftProjectMembersModule),
          data: {
            code: FunctionCode.HR_MODIFY_PROJECT_MEMBER,
            scope: Scopes.VIEW,
            pageName: 'staffManager.pageName.projectHisResearch',
            breadcrumb: 'staffManager.breadcrumb.projectHisResearch'
          }
        }]
      },
      {
        path: 'contract-process',
        canActivateChild: [RoleGuardService],
        children: [{
          path: '',
          loadChildren: () => import('@hcm-mfe/staff-manager/feature/draft/contract-process').then(m => m.StaffManagerFeatureDraftContractProcessModule),
          data: {
            code: FunctionCode.HR_MODIFY_CONTRACT_PROCESS,
            scope: Scopes.VIEW,
            pageName: 'staffManager.pageName.contractHisResearch',
            breadcrumb: 'staffManager.breadcrumb.contractHisResearch'
          }
        }]
      },
      {
        path: 'education-degree',
        canActivateChild: [RoleGuardService],
        children: [{
          path: '',
          loadChildren: () => import('@hcm-mfe/staff-manager/feature/draft/education-degree').then(m => m.StaffManagerFeatureDraftEducationDegreeModule),
          data: {
            code: FunctionCode.HR_MODIFY_DEGREE,
            scope: Scopes.VIEW,
            pageName: 'staffManager.pageName.educationDegree',
            breadcrumb: 'staffManager.breadcrumb.educationDegree'
          }
        }]
      },
      {
        path: 'education-process',
        canActivateChild: [RoleGuardService],
        children: [{
          path: '',
          loadChildren: () => import('@hcm-mfe/staff-manager/feature/draft/education-process').then(m => m.StaffManagerFeatureDraftEducationProcessModule),
          data: {
            code: FunctionCode.HR_MODIFY_EDU_PROCESS,
            scope: Scopes.VIEW,
            pageName: 'staffManager.pageName.eduHisResearch',
            breadcrumb: 'staffManager.breadcrumb.eduHisResearch'
          }
        }]
      },
      {
        path: 'salary-process',
        canActivateChild: [RoleGuardService],
        children: [{
          path: '',
          loadChildren: () => import('@hcm-mfe/staff-manager/feature/draft/salary-process').then(m => m.StaffManagerFeatureDraftSalaryProcessModule),
          data: {
            code: FunctionCode.HR_MODIFY_SALARY_PROCESS,
            scope: Scopes.VIEW,
            pageName: 'staffManager.pageName.salaryHisResearch',
            breadcrumb: 'staffManager.breadcrumb.salaryHisResearch'
          }
        }]
      },
      {
        path: 'allowance',
        canActivateChild: [RoleGuardService],
        children: [{
          path: '',
          loadChildren: () => import('@hcm-mfe/staff-manager/feature/draft/allowance').then(m => m.StaffManagerFeatureDraftAllowanceModule),
          data: {
            code: FunctionCode.HR_MODIFY_ALLOWANCE_PROCESS,
            scope: Scopes.VIEW,
            pageName: 'staffManager.pageName.allowanceHisResearch',
            breadcrumb: 'staffManager.breadcrumb.allowanceHisResearch'
          }
        }]
      },
      {
        path: 'reward-records',
        canActivateChild: [RoleGuardService],
        children: [{
          path: '',
          loadChildren: () => import('@hcm-mfe/staff-manager/feature/draft/reward-records').then(m => m.StaffManagerFeatureDraftRewardRecordsModule),
          data: {
            code: FunctionCode.HR_MODIFY_REWARD,
            scope: Scopes.VIEW,
            pageName: 'staffManager.pageName.rewardRecordResearch',
            breadcrumb: 'staffManager.breadcrumb.rewardRecordResearch'
          }
        }]
      },
      {
        path: 'decpline-records',
        canActivateChild: [RoleGuardService],
        children: [{
          path: '',
          loadChildren: () => import('@hcm-mfe/staff-manager/feature/draft/decpline-records').then(m => m.StaffManagerFeatureDraftDecplineRecordsModule),
          data: {
            code: FunctionCode.HR_MODIFY_DECIPLINE,
            scope: Scopes.VIEW,
            pageName: 'staffManager.pageName.decplineResearch',
            breadcrumb: 'staffManager.breadcrumb.decplineResearch'
          }
        }]
      },
      {
        path: 'manager-process',
        canActivateChild: [RoleGuardService],
        children: [{
          path: '',
          loadChildren: () => import('@hcm-mfe/staff-manager/feature/draft/manager-process').then(m => m.StaffManagerFeatureDraftManagerProcessModule),
          data: {
            code: FunctionCode.HR_MODIFY_MANAGER,
            scope: Scopes.VIEW,
            pageName: 'staffManager.pageName.managerResearch',
            breadcrumb: 'staffManager.breadcrumb.managerResearch'
          }
        }]
      },
      {
          path: 'concurrent-process',
          canActivateChild: [RoleGuardService],
          children: [{
              path: '',
              loadChildren: () => import('@hcm-mfe/staff-manager/feature/draft/concurrent-process').then(m => m.StaffManagerFeatureDraftConcurrentProcessModule),
              data: {
                  code: FunctionCode.HR_DRAFT_CONCURRENT_PROCESS,
                  scope: Scopes.VIEW,
                  pageName: 'staffManager.pageName.concurrentProcessResearch',
                  breadcrumb: 'staffManager.breadcrumb.concurrentProcessResearch'
              }
          }]
      },
    ]
  },
  {
    path: 'recruit',
    data: {
      pageName: 'staffManager.pageName.recruit',
      breadcrumb: 'staffManager.breadcrumb.recruit'
    },
    children: [
      {
        path: '',
        redirectTo: 'proposal-candidate',
        pathMatch: 'full'
      },
      {
        path: 'proposal-candidate',
        loadChildren: () => import('@hcm-mfe/staff-manager/feature/recruit/approval-recruitment').then(m => m.StaffManagerFeatureRecruitApprovalRecruitmentModule),
        data: {
          pageName: 'staffManager.pageName.approvalRecruitment',
          breadcrumb: 'staffManager.breadcrumb.proposalRecruitment',
          screenMode: Constant.SCREEN_MODE.CREATE
        }
      },
      {
        path: 'approval-candidate',
        loadChildren: () => import('@hcm-mfe/staff-manager/feature/recruit/approval-recruitment').then(m => m.StaffManagerFeatureRecruitApprovalRecruitmentModule),
        data: {
          pageName: 'staffManager.pageName.approvalRecruitment',
          breadcrumb: 'staffManager.breadcrumb.approvalRecruitment',
          screenMode: Constant.SCREEN_MODE.APPROVAL
        }
      },
    ],
  },
  {
    path: 'acceptance-liquidation',
    children: [
      {
        path: 'search',
        loadChildren: () => import('@hcm-mfe/staff-manager/feature/acceptance/acceptance-liquidation').then(m => m.StaffManagerFeatureAcceptanceAcceptanceLiquidationModule),
        data: {
          pageName: 'staffManager.pageName.acceptanceAndLiquidation',
          breadcrumb: 'staffManager.breadcrumb.acceptanceAndLiquidation'
        }
      },
    ]
  },
  {
    path: 'manager-direct',
    data: {
      pageName: 'staffManager.pageName.managerDirect',
      breadcrumb: 'staffManager.breadcrumb.managerDirect'
    },
    children: [
      {
        data: {
          pageName: 'staffManager.pageName.listManagerDirect',
          breadcrumb: 'staffManager.breadcrumb.listManagerDirect'
        },
        path: '',
        loadChildren: () => import('@hcm-mfe/staff-manager/feature/list-manager-direct').then((m) => m.StaffManagerFeatureListManagerDirectModule)
      },
      {
        data: {
          pageName: 'staffManager.pageName.assignManager',
          breadcrumb: 'staffManager.breadcrumb.assignManager'
        },
        path: 'assign-manager',
        loadChildren: () => import('@hcm-mfe/staff-manager/feature/assign-manager-init').then((m) => m.StaffManagerFeatureAssignManagerInitModule)
      },
      {
        data: {
          pageName: 'staffManager.pageName.listManagerDirect',
          breadcrumb: 'staffManager.breadcrumb.listManagerDirect'
        },
        path: 'list-manager-direct',
        loadChildren: () => import('@hcm-mfe/staff-manager/feature/list-manager-direct').then((m) => m.StaffManagerFeatureListManagerDirectModule)
      },
      {
        data: {
          pageName: 'staffManager.pageName.directManagerInfo',
          breadcrumb: 'staffManager.breadcrumb.directManagerInfo'
        },
        path: 'manager-direct-info',
        loadChildren: () => import('@hcm-mfe/staff-manager/feature/direct-manager-info').then((m) => m.StaffManagerFeatureDirectManagerInfoModule)
      },
    ]
  },
  {
    path: 'categories',
    data: {
      pageName: 'staffManager.pageName.categories',
      breadcrumb: 'staffManager.breadcrumb.categories'
    },
    component: LayoutCategoriesComponent,
    children: [
      {
        path: '',
        redirectTo: 'document-type/search',
        pathMatch: 'full'
      },
      {
        path: 'document-type/search',
        loadChildren: () => import('@hcm-mfe/staff-manager/feature/categories/document-types').then(m => m.StaffManagerFeatureCategoriesDocumentTypesModule),
        data: {
          pageName: 'staffManager.pageName.categoryDocumentTypes',
          breadcrumb: 'staffManager.breadcrumb.categoryDocumentTypes',
        }
      },
      {
        path: 'school/search',
        loadChildren: () => import('@hcm-mfe/staff-manager/feature/categories/schools').then(m => m.StaffManagerFeatureCategoriesSchoolsModule),
        data: {
          pageName: 'staffManager.pageName.categorySchools',
          breadcrumb: 'staffManager.breadcrumb.categorySchools',
        }
      },
      {
        path: 'faculty/search',
        loadChildren: () => import('@hcm-mfe/staff-manager/feature/categories/faculties').then(m => m.StaffManagerFeatureCategoriesFacultiesModule),
        data: {
          pageName: 'staffManager.pageName.categoryFaculty',
          breadcrumb: 'staffManager.breadcrumb.categoryFaculty',
        }
      },
      {
        path: 'major-level/search',
        loadChildren: () => import('@hcm-mfe/staff-manager/feature/categories/major-levels').then(m => m.StaffManagerFeatureCategoriesMajorLevelsModule),
        data: {
          pageName: 'staffManager.pageName.categoryMajorLevel',
          breadcrumb: 'staffManager.breadcrumb.categoryMajorLevel',
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StaffManagerFeatureShellRoutingModule {}
