import { NgModule } from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {FormatCurrencyPipe} from "@hcm-mfe/shared/pipes/format-currency";
import {StaffManagerFeatureShellRoutingModule} from "./staff-manager-feature-shell.routing.module";
import {NzModalModule} from "ng-zorro-antd/modal";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { NzMessageModule } from 'ng-zorro-antd/message';

@NgModule({
  imports: [CommonModule,
    StaffManagerFeatureShellRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NzModalModule, NzMessageModule
  ],
  providers: [DatePipe, FormatCurrencyPipe]
})
export class StaffManagerFeatureShellModule {}
