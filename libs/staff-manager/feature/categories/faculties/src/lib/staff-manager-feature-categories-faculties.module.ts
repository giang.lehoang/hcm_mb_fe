import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from "@angular/router";
import {StaffManagerUiCategoriesSearchFormModule} from "@hcm-mfe/staff-manager/ui/categories/search-form";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {CoreModule} from "@hcm-mfe/shared/ui/core";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiPopupModule} from "@hcm-mfe/shared/ui/popup";
import {FacultiesSearchComponent} from "./faculties-search/faculties-search.component";
import {AddFacultyComponent} from "./add-faculty/add-faculty.component";

@NgModule({
  imports: [CommonModule, StaffManagerUiCategoriesSearchFormModule, SharedUiMbButtonModule, TranslateModule, SharedUiMbTableWrapModule, SharedUiMbTableModule, SharedUiMbInputTextModule, SharedUiMbSelectModule, SharedUiPopupModule,
    RouterModule.forChild([
      {
        path: '',
        component: FacultiesSearchComponent
      }]
    ), CoreModule],
  declarations: [
    FacultiesSearchComponent,
    AddFacultyComponent
  ],
})

export class StaffManagerFeatureCategoriesFacultiesModule {}
