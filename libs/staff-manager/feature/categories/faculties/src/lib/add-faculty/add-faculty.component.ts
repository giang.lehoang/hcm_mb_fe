import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NzModalRef, NzModalService} from "ng-zorro-antd/modal";
import {UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {Subscription} from "rxjs";
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";
import {ActivatedRoute, Router} from "@angular/router";
import {CategoriesService} from "@hcm-mfe/staff-manager/data-access/services";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {Faculties} from "@hcm-mfe/staff-manager/data-access/models/categories";

@Component({
  selector: 'hcm-mfe-add-faculty',
  templateUrl: './add-faculty.component.html',
  styleUrls: ['./add-faculty.component.scss']
})
export class AddFacultyComponent implements OnInit {

  form!: FormGroup;
  modal!: NzModalRef;
  facultyId!: number;
  isSubmitted = false;
  subscriptions: Subscription[] = [];
  isDetail?: boolean;

  constructor(
    private fb: FormBuilder,
    private toastService: ToastrService,
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private modalService: NzModalService,
    private modalRef: NzModalRef,
    private categoriesService: CategoriesService
  ) { }

  ngOnInit(): void {
    this.initFormGroup();
    if (this.facultyId) {
      this.pathValueForm();
    }
  }

  initFormGroup() {
    this.form = this.fb.group({
      code: [null, [Validators.required]],
      name: [null, [Validators.required]],
      isTextManual: null,
    });
  }

  pathValueForm() {
    this.subscriptions.push(
      this.categoriesService.getDataById(this.facultyId, UrlConstant.CATEGORIES.GET_FACULTY_BY_ID).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.form.patchValue(res.data);
          this.form.controls['isTextManual'].patchValue(res.data?.isTextManual === 1)
        }

        if (this.isDetail) {
          this.form.disable();
        }
      }, error => this.toastService.error(error.message))
    )
  }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      const request: Faculties = this.form.value;
      request.facultyId = this.facultyId;
      request.isTextManual = this.form.controls['isTextManual'].value ? 1 : 0;
      const formData = new FormData();
      formData.append('data', new Blob([JSON.stringify(request)], {
        type: 'application/json'
      }));

      this.subscriptions.push(
        this.categoriesService.saveData(UrlConstant.CATEGORIES.SAVE_FACULTY ,formData).subscribe(res => {
          if (res && res.code === HTTP_STATUS_CODE.OK) {
            const message = request.facultyId ? this.translate.instant('common.notification.updateSuccess') : this.translate.instant('common.notification.addSuccess')
            this.toastService.success(message);
            this.modalRef.close({refresh:true});
          } else {
            this.toastService.error(this.translate.instant(request.facultyId ? 'common.notification.updateError' : 'common.notification.addError') + ': ' + res?.message);
          }
        }, error => {
          this.toastService.error(error.message);
        })
      )
    }
  }


  closeModalAdd() {
    this.modalService.closeAll();
  }

  ngOnDestroy(): void {
    this.subscriptions?.forEach(sub => sub.unsubscribe());
  }

}
