import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {AppFunction, MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {Faculties} from "@hcm-mfe/staff-manager/data-access/models/categories";
import {Subscription} from "rxjs";
import {NzModalRef, NzModalService} from "ng-zorro-antd/modal";
import {FunctionCode, UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {SearchFormCategoriesComponent} from "@hcm-mfe/staff-manager/ui/categories/search-form";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {CategoriesService, SearchFormService} from "@hcm-mfe/staff-manager/data-access/services";
import {ToastrService} from "ngx-toastr";
import {SessionService} from "@hcm-mfe/shared/common/store";
import {PopupService} from "@hcm-mfe/shared/ui/popup";
import {TranslateService} from "@ngx-translate/core";
import {HttpParams} from "@angular/common/http";
import {getTypeExport} from "@hcm-mfe/shared/common/utils";
import {saveAs} from "file-saver";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {
  AddDocumentTypeComponent
} from "../../../../document-types/src/lib/add-document-type/add-document-type.component";
import {AddFacultyComponent} from "../add-faculty/add-faculty.component";

@Component({
  selector: 'hcm-mfe-faculties-search',
  templateUrl: './faculties-search.component.html',
  styleUrls: ['./faculties-search.component.scss']
})
export class FacultiesSearchComponent implements OnInit {

  tableConfig!: MBTableConfig;
  searchResult: Faculties[] = [];
  pagination = new Pagination();
  subs: Subscription[] = [];
  modal!: NzModalRef;
  functionCode = FunctionCode.HR_FACULTY;
  objFunction!: AppFunction;

  @ViewChild('searchForm') searchForm!: SearchFormCategoriesComponent;
  @ViewChild('actionTmpl', {static: true}) action!: TemplateRef<NzSafeAny>;

  constructor(private searchFormService: SearchFormService,
              private toastService: ToastrService,
              private modalService: NzModalService,
              private sessionService: SessionService,
              private categoriesService: CategoriesService,
              private deletePopup: PopupService,
              private translateService: TranslateService) {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${this.functionCode}`);
  }

  ngOnInit(): void {
    this.initTable();
  }

  ngAfterViewInit() {
    this.doSearch(1);
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'staffManager.categories.facultyCode',
          field: 'code',
          width: 150
        },
        {
          title: 'staffManager.categories.facultyName',
          field: 'name',
        },

        {
          title: 'staffManager.categories.isTextManualFaculty',
          field: 'isTextManual',
          width: 200,
          tdClassList: ['text-center'],
        },
        {
          title: ' ',
          field: 'action',
          tdClassList: ['text-center'],
          width: 65,
          tdTemplate: this.action,
          fixed: window.innerWidth > 1024,
          fixedDir: 'right',
        }
      ],
      total: 0,
      needScroll: false,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  doExportData() {
    const searchParam =  this.searchForm ? this.searchForm.parseOptions() : new HttpParams();
    this.subs.push(
      this.searchFormService.doExportCategories(UrlConstant.EXPORT_REPORT.FACULTIES, searchParam).subscribe(res => {
        if (res?.headers?.get('Excel-File-Empty')) {
          this.toastService.error(this.translateService.instant("common.notification.exportFail"));
          return;
        } else {
          const reportFile = new Blob([res.body], {type: getTypeExport('xlsx')});
          saveAs(reportFile, 'danh_sach_chuyen_nganh_dao_tao.xlsx');
        }
      }, error => {
        this.toastService.error(error.message);
      })
    )
  }

  doSearch(pageIndex: number,) {
    this.pagination.pageNumber = pageIndex;
    const searchParam =  this.searchForm ? this.searchForm.parseOptions() : new HttpParams();
    this.subs.push(
      this.searchFormService.categoriesResearch(UrlConstant.SEARCH_FORM.FACULTIES, searchParam, this.pagination.getCurrentPage()).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.searchResult = res.data.listData.map((item: Faculties) => {
            item.isTextManual = this.translateService.instant(item.isTextManual === 1 ? 'staffManager.categories.yes' : 'staffManager.categories.no');
            return item;
          });
          this.tableConfig.total = res.data.count;
        }
      }, error => {
        this.toastService.error(error?.message);
      })
    );
  }

  openAddModal() {
    this.openModal();
  }

  onOpenEdit(facultyId: number) {
    this.openModal(facultyId);
  }


  onOpenDetail(event:NzSafeAny) {
    this.openModal(event?.facultyId, true);
  }


  openModal(facultyId?: number, isDetail?: boolean) {
    this.modal = this.modalService.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
      nzTitle: this.translateService.instant(facultyId ? (isDetail ? 'staffManager.pageName.detailFaculty' : 'staffManager.pageName.editFaculty') : 'staffManager.pageName.addFaculty'),
      nzContent: AddFacultyComponent,
      nzComponentParams: {
        facultyId: facultyId,
        isDetail: isDetail
      },
      nzFooter: null
    });
    this.subs.push(
      this.modal.afterClose.subscribe(res => {
        if (res && res.refresh) {
          this.doSearch(1);
        }
      })
    );
  }

  deleteById(facultyId: number) {
    this.deletePopup.showModal(() => {
      this.subs.push(
        this.categoriesService.deleteDataById(facultyId, UrlConstant.CATEGORIES.GET_FACULTY_BY_ID).subscribe(res => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.toastService.success(this.translateService.instant('common.notification.deleteSuccess'));
            this.doSearch(this.pagination.pageNumber);
          } else {
            this.toastService.error(res?.message);
          }
        }, error => {
          this.toastService.error(error.message);
        })
      );
    });
  }

  ngOnDestroy(): void {
    this.subs?.forEach(sub => sub?.unsubscribe());
  }

}
