import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutCategoriesComponent } from './layout-categories/layout-categories.component';
import {RouterModule} from "@angular/router";
import {SharedUiLoadingPageModule} from "@hcm-mfe/shared/ui/loading-page";

@NgModule({
  imports: [CommonModule, RouterModule, SharedUiLoadingPageModule],
  declarations: [
    LayoutCategoriesComponent
  ],
})
export class StaffManagerFeatureCategoriesLayoutCategoriesModule {}
