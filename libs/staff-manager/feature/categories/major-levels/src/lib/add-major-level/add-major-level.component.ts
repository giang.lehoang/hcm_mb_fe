import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NzModalRef, NzModalService} from "ng-zorro-antd/modal";
import {Subscription} from "rxjs";
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";
import {ActivatedRoute, Router} from "@angular/router";
import {CategoriesService} from "@hcm-mfe/staff-manager/data-access/services";
import {UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {Faculties} from "@hcm-mfe/staff-manager/data-access/models/categories";

@Component({
  selector: 'hcm-mfe-add-major-level',
  templateUrl: './add-major-level.component.html',
  styleUrls: ['./add-major-level.component.scss']
})
export class AddMajorLevelComponent implements OnInit {

  form!: FormGroup;
  modal!: NzModalRef;
  majorLevelId!: number;
  isSubmitted = false;
  subscriptions: Subscription[] = [];
  isDetail?: boolean;

  constructor(
    private fb: FormBuilder,
    private toastService: ToastrService,
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private modalService: NzModalService,
    private modalRef: NzModalRef,
    private categoriesService: CategoriesService
  ) { }

  ngOnInit(): void {
    this.initFormGroup();
    if (this.majorLevelId) {
      this.pathValueForm();
    }
  }

  initFormGroup() {
    this.form = this.fb.group({
      code: [null, [Validators.required]],
      name: [null, [Validators.required]]
    });
  }

  pathValueForm() {
    this.subscriptions.push(
      this.categoriesService.getDataById(this.majorLevelId, UrlConstant.CATEGORIES.GET_MAJOR_LEVEL_BY_ID).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.form.patchValue(res.data);
        }
        if (this.isDetail) {
          this.form.disable();
        }
      }, error => this.toastService.error(error.message))
    )
  }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      const request: Faculties = this.form.value;
      request.majorLevelId = this.majorLevelId;
      const formData = new FormData();
      formData.append('data', new Blob([JSON.stringify(request)], {
        type: 'application/json'
      }));

      this.subscriptions.push(
        this.categoriesService.saveData(UrlConstant.CATEGORIES.SAVE_MAJOR_LEVEL ,formData).subscribe(res => {
          if (res && res.code === HTTP_STATUS_CODE.OK) {
            const message = request.majorLevelId ? this.translate.instant('common.notification.updateSuccess') : this.translate.instant('common.notification.addSuccess')
            this.toastService.success(message);
            this.modalRef.close({refresh:true});
          } else {
            this.toastService.error(this.translate.instant(request.majorLevelId ? 'common.notification.updateError' : 'common.notification.addError') + ': ' + res?.message);
          }
        }, error => {
          this.toastService.error(error.message);
        })
      )
    }
  }


  closeModalAdd() {
    this.modalService.closeAll();
  }

  ngOnDestroy(): void {
    this.subscriptions?.forEach(sub => sub.unsubscribe());
  }

}
