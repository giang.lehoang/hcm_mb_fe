import {AfterViewInit, Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {AppFunction, MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {SearchFormCategoriesComponent} from "@hcm-mfe/staff-manager/ui/categories/search-form";
import {HttpParams} from "@angular/common/http";
import {FunctionCode, UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {Subscription} from "rxjs";
import {CategoriesService, SearchFormService} from "@hcm-mfe/staff-manager/data-access/services";
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";
import {getTypeExport} from "@hcm-mfe/shared/common/utils";
import {saveAs} from "file-saver";
import {NzModalRef, NzModalService} from "ng-zorro-antd/modal";
import {AddDocumentTypeComponent} from "../add-document-type/add-document-type.component";
import {DocumentType} from "@hcm-mfe/staff-manager/data-access/models/categories";
import {SessionService} from "@hcm-mfe/shared/common/store";
import {PopupService} from "@hcm-mfe/shared/ui/popup";

@Component({
  selector: 'document-types',
  templateUrl: './document-types.component.html',
  styleUrls: ['./document-types.component.scss']
})
export class DocumentTypesComponent implements OnInit, AfterViewInit {
  tableConfig!: MBTableConfig;
  searchResult: DocumentType[] = [];
  pagination = new Pagination();
  subs: Subscription[] = [];
  modal!: NzModalRef;
  functionCode = FunctionCode.HR_DOCUMENT_TYPE;
  objFunction!: AppFunction;

  @ViewChild('searchForm') searchForm!: SearchFormCategoriesComponent;
  @ViewChild('actionTmpl', {static: true}) action!: TemplateRef<NzSafeAny>;

  constructor(private searchFormService: SearchFormService,
              private toastService: ToastrService,
              private modalService: NzModalService,
              private sessionService: SessionService,
              private categoriesService: CategoriesService,
              private deletePopup: PopupService,
              private translateService: TranslateService) {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${this.functionCode}`);
  }

  ngOnInit(): void {
    this.initTable();
  }

  ngAfterViewInit() {
    this.doSearch(1);
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'staffManager.categories.documentTypeCode',
          field: 'code',
          fixed: true,
          fixedDir: 'left',
          width: 100
        },
        {
          title: 'staffManager.categories.documentTypeName',
          field: 'name',
          fixed: true,
          fixedDir: 'left',
          width: 200
        },
        {
          title: 'staffManager.categories.documentType',
          field: 'type',
          width: 100
        },
        {
          title: 'staffManager.categories.showEndDate',
          field: 'isShowEndDate',
          width: 100,
          tdClassList: ['text-center'],
        },
        {
          title: 'staffManager.categories.requiredEndDate',
          field: 'isRequiredEndDate',
          width: 100,
          tdClassList: ['text-center'],
        },
        {
          title: 'staffManager.categories.showTrial',
          field: 'isShowTrial',
          width: 100,
          tdClassList: ['text-center'],
        },
        {
          title: 'staffManager.categories.requiredTrial',
          field: 'isRequiredTrial',
          width: 100,
          tdClassList: ['text-center'],
        },
        {
          title: 'staffManager.categories.showKeepSenior',
          field: 'isShowKeepSenior',
          width: 100,
          tdClassList: ['text-center'],
        },
        {
          title: ' ',
          field: 'action',
          tdClassList: ['text-center'],
          width: 65,
          tdTemplate: this.action,
          fixed: window.innerWidth > 1024,
          fixedDir: 'right',
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  doExportData() {
    const searchParam =  this.searchForm ? this.searchForm.parseOptions() : new HttpParams();
    this.subs.push(
      this.searchFormService.doExportCategories(UrlConstant.EXPORT_REPORT.DOCUMENT_TYPES, searchParam).subscribe(res => {
        if (res?.headers?.get('Excel-File-Empty')) {
          this.toastService.error(this.translateService.instant("common.notification.exportFail"));
          return;
        } else {
          const reportFile = new Blob([res.body], {type: getTypeExport('xlsx')});
          saveAs(reportFile, 'danh_sach_loai_quyet_dinh.xlsx');
        }
      }, error => {
        this.toastService.error(error.message);
      })
    )
  }

  doSearch(pageIndex: number,) {
    this.pagination.pageNumber = pageIndex;
    const searchParam =  this.searchForm ? this.searchForm.parseOptions() : new HttpParams();
    this.subs.push(
      this.searchFormService.categoriesResearch(UrlConstant.SEARCH_FORM.DOCUMENT_TYPES, searchParam, this.pagination.getCurrentPage()).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.searchResult = res.data.listData.map((item: DocumentType) => {
            item.isShowEndDate =  this.translateService.instant(item.isShowEndDate === 1 ? 'staffManager.categories.yes' : 'staffManager.categories.no');
            item.isRequiredEndDate = this.translateService.instant(item.isRequiredEndDate === 1? 'staffManager.categories.yes' : 'staffManager.categories.no');
            item.isShowTrial = this.translateService.instant(item.isShowTrial === 1? 'staffManager.categories.yes' : 'staffManager.categories.no');
            item.isRequiredTrial = this.translateService.instant(item.isRequiredTrial === 1? 'staffManager.categories.yes' : 'staffManager.categories.no');
            item.isShowKeepSenior = this.translateService.instant(item.isShowKeepSenior === 1? 'staffManager.categories.yes' : 'staffManager.categories.no');
            return item;
          });
          this.tableConfig.total = res.data.count;
        }
      }, error => {
        this.toastService.error(error?.message);
      })
    );
  }

  openAddModal() {
    this.openModal();
  }

  onOpenEdit(documentTypeId: number) {
    this.openModal(documentTypeId);
  }

  onOpenDetail(event: NzSafeAny) {
    this.openModal(event?.documentTypeId, true);
  }

  openModal(documentTypeId?: number, isDetail?: boolean) {
    this.modal = this.modalService.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
      nzTitle: this.translateService.instant(documentTypeId ? (isDetail ? 'staffManager.pageName.detailDocumentType' : 'staffManager.pageName.editDocumentType') : 'staffManager.pageName.addDocumentType'),
      nzContent: AddDocumentTypeComponent,
      nzComponentParams: {
        documentTypeId: documentTypeId,
        isDetail: isDetail
      },
      nzFooter: null
    });
    this.subs.push(
      this.modal.afterClose.subscribe(res => {
        if (res && res.refresh) {
          this.doSearch(1);
        }
      })
    );
  }

  deleteById(documentTypeId: number) {
    this.deletePopup.showModal(() => {
      this.subs.push(
        this.categoriesService.deleteDataById(documentTypeId, UrlConstant.CATEGORIES.GET_DOCUMENT_TYPE_BY_ID).subscribe(res => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.toastService.success(this.translateService.instant('common.notification.deleteSuccess'));
            this.doSearch(this.pagination.pageNumber);
          } else {
            this.toastService.error(res?.message);
          }
        }, error => {
          this.toastService.error(error.message);
        })
      );
    });
  }

  ngOnDestroy(): void {
    this.subs?.forEach(sub => sub?.unsubscribe());
  }
}
