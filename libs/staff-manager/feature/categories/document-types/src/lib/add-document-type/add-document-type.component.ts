import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NzModalRef, NzModalService} from "ng-zorro-antd/modal";
import {CatalogModel} from "@hcm-mfe/shared/data-access/models";
import {Subscription} from "rxjs";
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";
import {ActivatedRoute, Router} from "@angular/router";
import {Constant, UrlConstant} from "@hcm-mfe/staff-manager/data-access/common";
import {CategoriesService} from "@hcm-mfe/staff-manager/data-access/services";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {DocumentType} from "@hcm-mfe/staff-manager/data-access/models/categories";

@Component({
  selector: 'hcm-mfe-add-document-type',
  templateUrl: './add-document-type.component.html',
  styleUrls: ['./add-document-type.component.scss']
})
export class AddDocumentTypeComponent implements OnInit {

  form!: FormGroup;
  modal!: NzModalRef;
  documentTypeId!: number;
  listType: CatalogModel[] = [];

  isSubmitted = false;

  constant = Constant;
  subscriptions: Subscription[] = [];
  isDetail?: boolean;

  constructor(
    private fb: FormBuilder,
    private toastService: ToastrService,
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private modalService: NzModalService,
    private modalRef: NzModalRef,
    private categoriesService: CategoriesService
  ) { }

  ngOnInit(): void {
    this.initListDataSelect();
    this.initFormGroup();
    if (this.documentTypeId) {
      this.pathValueForm();
    }
  }

  initFormGroup() {
    this.form = this.fb.group({
      code: [null, [Validators.required]],
      name: [null, [Validators.required]],
      type: [null, [Validators.required]],
      isShowEndDate: null,
      isRequiredEndDate: null,
      isShowTrial: null,
      isRequiredTrial: null,
      isShowKeepSenior: null
    });
  }

  pathValueForm() {
    this.subscriptions.push(
      this.categoriesService.getDataById(this.documentTypeId, UrlConstant.CATEGORIES.GET_DOCUMENT_TYPE_BY_ID).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.form.patchValue(res.data);
          this.form.controls['isRequiredTrial'].patchValue(res.data?.isRequiredTrial === 1)
          this.form.controls['isShowTrial'].patchValue(res.data?.isShowTrial === 1)
          this.form.controls['isShowEndDate'].patchValue(res.data?.isShowEndDate === 1)
          this.form.controls['isRequiredEndDate'].patchValue(res.data?.isRequiredEndDate === 1)
          this.form.controls['isShowKeepSenior'].patchValue(res.data?.isShowKeepSenior === 1)
        }
        if (this.isDetail) {
          this.form.disable();
        }
      }, error => this.toastService.error(error.message))
    )
  }

  initListDataSelect() {
    this.listType = Constant.LIST_DOCUMENT_TYPE;
  }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      const request: DocumentType = this.form.value;
      request.documentTypeId = this.documentTypeId;
      request.isRequiredTrial = this.form.controls['isRequiredTrial'].value ? 1 : 0;
      request.isShowTrial = this.form.controls['isShowTrial'].value ? 1 : 0;
      request.isShowEndDate = this.form.controls['isShowEndDate'].value ? 1 : 0;
      request.isRequiredEndDate = this.form.controls['isRequiredEndDate'].value ? 1 : 0;
      request.isShowKeepSenior = this.form.controls['isShowKeepSenior'].value ? 1 : 0;
      const formData = new FormData();
      formData.append('data', new Blob([JSON.stringify(request)], {
        type: 'application/json'
      }));

      this.subscriptions.push(
        this.categoriesService.saveData(UrlConstant.CATEGORIES.SAVE_DOCUMENT_TYPE ,formData).subscribe(res => {
          if (res && res.code === HTTP_STATUS_CODE.OK) {
            const message = request.documentTypeId ? this.translate.instant('common.notification.updateSuccess') : this.translate.instant('common.notification.addSuccess')
            this.toastService.success(message);
            this.modalRef.close({refresh:true});
          } else {
            this.toastService.error(this.translate.instant(request.documentTypeId ? 'common.notification.updateError' : 'common.notification.addError') + ': ' + res?.message);
          }
        }, error => {
          this.toastService.error(error.message);
        })
      )
    }
  }


  closeModalAdd() {
    this.modalService.closeAll();
  }

  ngOnDestroy(): void {
    this.subscriptions?.forEach(sub => sub.unsubscribe());
  }
}
