import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectPointComponent } from './select-point/select-point.component';

@NgModule({
  imports: [CommonModule],
  declarations: [
    SelectPointComponent
  ],
  exports: [
    SelectPointComponent
  ]
})
export class AngleItUiSelectPointModule {}
