import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';

@Component({
  selector: 'hcm-mfe-select-point',
  templateUrl: './select-point.component.html',
  styleUrls: ['./select-point.component.scss']
})
export class SelectPointComponent implements OnInit {
  @Input() index?: number;
  @Input() mbTitle: string = '';
  @Input() mbImgPath: string = '/assets/img/angle-it/img-default.png'
  @Input() isSelect: boolean = false;
  @Output() isSelectChange = new EventEmitter<boolean>();
  @Output() onChange = new EventEmitter<number>();

  @ViewChild('wrapImg', { static: false }) wrapImg!: ElementRef;

  constructor() {}
  ngOnInit(): void {

  }

  select() {
    this.isSelect = true;
    this.isSelectChange.emit(this.isSelect);
    this.onChange.emit(this.index);
  }

}
