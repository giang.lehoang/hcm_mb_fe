import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImgItemComponent } from './img-item/img-item.component';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { FormsModule } from '@angular/forms';
import { NzImageModule } from 'ng-zorro-antd/image';
import { SharedUiMbButtonIconModule } from '@hcm-mfe/shared/ui/mb-button-icon';

@NgModule({
  imports: [CommonModule, NzUploadModule, NzModalModule, NzIconModule, FormsModule, NzImageModule, SharedUiMbButtonIconModule],
  declarations: [
    ImgItemComponent
  ],
  exports: [
    ImgItemComponent
  ]
})
export class AngleItUiImgItemModule {}
