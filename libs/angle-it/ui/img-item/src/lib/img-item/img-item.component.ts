import { Component, ElementRef, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { ImageService } from '@hcm-mfe/angle-it/data-access/services';
import { Subscription } from 'rxjs';

const getBase64 = (file: File): Promise<string | ArrayBuffer | null> =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });

@Component({
  selector: 'hcm-mfe-img-item',
  templateUrl: './img-item.component.html',
  styleUrls: ['./img-item.component.scss']
})
export class ImgItemComponent implements OnInit {
  strBase64 :any;
  subs: Subscription[] = [];

  @Input() docId?: number;
  @Input() mbRouterLink: string = '';
  @Input() mbTitle: string = '';
  @Input() mbIsDisable: boolean = false;
  @Input() isUpload: boolean = false;
  @Output() onFileSelect = new EventEmitter<{strBase64?: string, file?: NzUploadFile}>();

  @ViewChild('wrapImg', { static: false }) wrapImg!: ElementRef;

  mbSrc: NzSafeAny = '/assets/img/angle-it/image-default.png'
  isLoading = false;

  constructor(
    private readonly router: Router,
    private toastrService: ToastrService,
    private imageService: ImageService,
    private translate: TranslateService
  ) {
  }
  ngOnInit(): void {
    if (this.docId) {
      this.getImage(this.docId);
    }
  }

  fileList: NzUploadFile[] = []
  showUploadList = {
    showRemoveIcon: true,
    showPreviewIcon: false,
    showDownloadIcon: false
  };

  getImage(id: number) {
    this.isLoading = true;
    this.subs.push(
      this.imageService.getImage(id).subscribe(res => {
        this.mbSrc = res?.data ? 'data:image/jpg;base64,' + res?.data : null;
        this.isLoading = false;
      }, () => {
        this.isLoading = false;
      })
    );
  }

  beforeUpload = (file: NzUploadFile): boolean => {
    if (file.size && file.size >= 3000000) {
      this.toastrService.error(this.translate.instant('shared.notification.upload.limitSize'));
      this.fileList = []
      return false;
    }
    if (file.type && file.type !== 'image/png') {
      this.toastrService.error(this.translate.instant('angleIt.notification.typeFileNotSupport'));
      this.fileList = []
      return false;
    }
    getBase64(file as any).then(res => {
      this.strBase64 = res;
      this.onFileSelect.emit({strBase64: this.strBase64, file: file});
    });
    this.fileList = this.fileList.concat(file);
    return false;
  };

  deleteImage() {
    this.fileList = [];
    this.onFileSelect.emit({});
  }

  overBtn() {
    this.wrapImg.nativeElement.setAttribute('class', 'img-upload-hover')
  }

  outBtn() {
    this.wrapImg.nativeElement.setAttribute('class', 'img-upload')
  }
}
