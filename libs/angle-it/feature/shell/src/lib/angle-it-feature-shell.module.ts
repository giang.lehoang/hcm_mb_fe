import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { SharedUiPopupModule } from '@hcm-mfe/shared/ui/popup';
import { OverlayModule } from '@angular/cdk/overlay';
import { ToastrModule } from 'ngx-toastr';
import { FormatCurrencyPipe } from '@hcm-mfe/shared/pipes/format-currency';
import { AngleItFeatureShellRoutingModule } from './angle-it-feature-shell.routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NzMessageModule,
    NzModalModule,
    SharedUiPopupModule,
    OverlayModule,
    AngleItFeatureShellRoutingModule,
    ToastrModule.forRoot({})
  ],
  providers: [DatePipe, FormatCurrencyPipe]
})
export class AngleItFeatureShellModule {}
