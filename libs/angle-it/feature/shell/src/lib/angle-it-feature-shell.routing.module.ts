import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import { FUNCTION_CODE } from '@hcm-mfe/angle-it/data-access/common';
import { RoleGuardService } from '@hcm-mfe/shared/core';

const routes: Routes = [
  {
    path: 'thank-you',
    children: [
      {
        path: 'config',
        canActivateChild: [RoleGuardService],
        data: {
          code: FUNCTION_CODE.THANKYOU_SETTING,
          pageName: 'angleIt.pageName.thankYouConfig',
          breadcrumb: 'angleIt.breadcrumb.thankYouConfig'
        },
        loadChildren: () => import('@hcm-mfe/angle-it/feature/thank-you/config').then((m) => m.AngleItFeatureThankYouConfigModule)
      },
      {
        path: 'send',
        canActivateChild: [RoleGuardService],
        data: {
          code: FUNCTION_CODE.SEND_THANK_MESSAGE,
        },
        loadChildren: () => import('@hcm-mfe/angle-it/feature/thank-you/send').then((m) => m.AngleItFeatureThankYouSendModule)
      },
    ]
  },
  {
    path: 'allocation',
    data: {
      pageName: 'angleIt.pageName.pointConfig',
      breadcrumb: 'angleIt.breadcrumb.pointConfig'
    },
    children: [
      { path: '', redirectTo: 'point-config', pathMatch: 'full' },
      {
        path: 'point-config',
        canActivateChild: [RoleGuardService],
        data: {
          code: FUNCTION_CODE.ALLOTMENT_SETTING,
        },
        loadChildren: () => import('@hcm-mfe/angle-it/feature/allocation/point-config').then((m) => m.AngleItFeatureAllocationPointConfigModule)
      }
    ]
  },
  {
    path: 'thanks-report',
    canActivateChild: [RoleGuardService],
    data: {
      code: FUNCTION_CODE.THANK_REPORT,
      pageName: 'angleIt.pageName.thankReport',
      breadcrumb: 'angleIt.breadcrumb.thankReport'
    },
    loadChildren: () => import('@hcm-mfe/angle-it/feature/thanks-report').then((m) => m.AngleItFeatureThanksReportModule)
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AngleItFeatureShellRoutingModule {}
