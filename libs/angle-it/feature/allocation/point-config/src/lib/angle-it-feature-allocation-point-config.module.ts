import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PointConfigComponent} from './point-config/point-config.component';
import {RouterModule} from '@angular/router';
import {NzFormModule} from 'ng-zorro-antd/form';
import {NzInputModule} from 'ng-zorro-antd/input';
import {TranslateModule} from '@ngx-translate/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SharedUiLoadingModule} from '@hcm-mfe/shared/ui/loading';
import {SharedUiMbButtonModule} from '@hcm-mfe/shared/ui/mb-button';
import {SharedUiMbTableMergeCellWrapModule} from '@hcm-mfe/shared/ui/mb-table-merge-cell-wrap';
import {SharedUiMbTableMergeCellModule} from '@hcm-mfe/shared/ui/mb-table-merge-cell';
import {SharedUiMbButtonIconModule} from '@hcm-mfe/shared/ui/mb-button-icon';
import {SharedUiMbDatePickerModule} from '@hcm-mfe/shared/ui/mb-date-picker';
import {SharedUiMbInputTextModule} from '@hcm-mfe/shared/ui/mb-input-text';
import {SharedDirectivesTrimInputModule} from '@hcm-mfe/shared/directives/trim-input';
import {NzDatePickerModule} from 'ng-zorro-antd/date-picker';
import {SharedDirectivesNumbericModule} from '@hcm-mfe/shared/directives/numberic';
import {PointHisConfigComponent} from "./point-his-config/point-his-config.component";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzIconModule} from "ng-zorro-antd/icon";
import {ViewDetailHisPointComponent} from "./view-detail-his-point/view-detail-his-point.component";

@NgModule({
  imports: [CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: PointConfigComponent
      },
      {
        path: 'history',
        component: PointHisConfigComponent,
        data: {
          pageName: 'angleIt.pageName.pointHisConfig',
          breadcrumb: 'angleIt.breadcrumb.pointHisConfig'
        }
      }
    ]), NzFormModule, NzInputModule, TranslateModule, SharedUiLoadingModule, SharedUiMbButtonModule,
    SharedUiMbTableMergeCellWrapModule, SharedUiMbTableMergeCellModule, SharedUiMbButtonIconModule, SharedUiMbDatePickerModule,
    SharedUiMbInputTextModule, SharedDirectivesTrimInputModule, ReactiveFormsModule, NzDatePickerModule, FormsModule, SharedDirectivesNumbericModule, NzDropDownModule, NzButtonModule, NzIconModule],
  declarations: [
    PointConfigComponent, PointHisConfigComponent, ViewDetailHisPointComponent
  ],
})
export class AngleItFeatureAllocationPointConfigModule {}
