import {Component, Injector, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {TableConfig} from '@hcm-mfe/shared/data-access/models';
import {userConfig} from '@hcm-mfe/shared/common/constants';
import {Subscription} from 'rxjs';
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";

@Component({
  selector: 'app-view-detail-his-point',
  templateUrl: './view-detail-his-point.component.html',
  styleUrls: ['./view-detail-his-point.component.scss']
})
export class ViewDetailHisPointComponent extends BaseComponent implements OnInit {
  subs: Subscription[] = [];
  isLoadingPage = false;
  param?: {
    fromDate: string,
    content: string,
    listGocItAllotmentSettingDetail: any
  };
  tableConfig!: TableConfig;
  size = userConfig.pageSize;
  listData = [];
  paramSearch = {
    size: this.size,
    page: 0,
  };
  sumTotalPoint = 0;

  constructor(injector: Injector,) {
    super(injector);
  }

  @ViewChild('footerTmpl', {static: true}) footer!: TemplateRef<NzSafeAny>;

  ngOnInit(): void {
    this.initTable();
    this.listData = this.param?.listGocItAllotmentSettingDetail;
    this.listData.forEach((el: NzSafeAny) => {
      el.totalPoint = (el.point ?? 0) * (el.numberOfPosGroup ?? 0);
      this.sumTotalPoint += el.totalPoint;
    });
  }

  initTable() {
    this.tableConfig = {
      headers: [
        {
          title: 'STT',
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          fixed: true,
          fixedDir: 'left',
          width: 80
        },
        {
          title: 'angleIt.pointConfig.allocationGroup',
          field: 'posGroupName',
          width: 300,
        },
        {
          title: 'angleIt.pointConfig.point',
          field: 'point',
          width: 200,
        },
        {
          title: 'angleIt.pointConfig.amount',
          field: 'numberOfPosGroup',
          width: 200,
        },
        {
          title: 'angleIt.pointConfig.totalPoint',
          field: 'totalPoint',
          width: 200,
          fixed: true,
          fixedDir: 'right'
        },
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.paramSearch.page,
      pageIndex: 1,
      footer: this.footer
    };
  }

  closeModalAdd() {
    this.modal.closeAll();
  }


  override ngOnDestroy() {
    this.subs?.forEach(sub => sub?.unsubscribe());
  }

}
