import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PointHisConfigComponent } from './point-his-config.component';

describe('PointConfigComponent', () => {
  let component: PointHisConfigComponent;
  let fixture: ComponentFixture<PointHisConfigComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PointHisConfigComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PointHisConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
