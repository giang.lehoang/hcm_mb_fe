import {Component, Injector, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {Subscription} from 'rxjs';
import {BaseComponent} from '@hcm-mfe/shared/common/base-component';
import {userConfig} from '@hcm-mfe/shared/common/constants';
import {Pageable, TableConfig} from '@hcm-mfe/shared/data-access/models';
import {ValidateService} from '@hcm-mfe/shared/core';
import {NzSafeAny} from 'ng-zorro-antd/core/types';
import {NzModalRef} from 'ng-zorro-antd/modal';
import {HistoryPoint, HistoryPointDetail} from '@hcm-mfe/angle-it/data-access/models';
import {PointHisConfigService} from "../../../../../../data-access/services/src/lib/point-his-config.service";
import {ViewDetailHisPointComponent} from '../view-detail-his-point/view-detail-his-point.component';
import {Constant, FUNCTION_CODE} from '@hcm-mfe/angle-it/data-access/common';

@Component({
  selector: 'hcm-mfe-point-his-config',
  templateUrl: './point-his-config.component.html',
  styleUrls: ['./point-his-config.component.scss']
})
export class PointHisConfigComponent extends BaseComponent implements OnInit {
  subs: Subscription[] = [];
  textFilter = '';
  value = '';
  listData?: HistoryPoint[] = [];
  size = userConfig.pageSize;
  paramSearch = {
    size: this.size,
    page: 0,
  };
  tableConfig!: TableConfig;
  pageable: Pageable = new Pageable();
  modalRef: NzModalRef | undefined;
  constant = Constant;


  @ViewChild('actionTmpl', {static: true}) action!: TemplateRef<NzSafeAny>;
  @ViewChild('footerTmpl', {static: true}) footer!: TemplateRef<NzSafeAny>;

  constructor(injector: Injector,
              private readonly pointHisConfigService: PointHisConfigService,
              public validateService: ValidateService) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FUNCTION_CODE.ALLOTMENT_SETTING}`);
  }

  ngOnInit(): void {
    this.initTable();
    this.search(1);
  }

  initTable() {
    this.tableConfig = {
      headers: [
        {
          title: 'STT',
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          fixed: true,
          fixedDir: 'left',
          width: 30
        },
        {
          title: 'angleIt.pointHisConfig.allocationDate',
          field: 'fromDate',
          width: 200,
        }, {
          title: 'angleIt.pointHisConfig.content',
          field: 'content',
          width: 400,
        }, {
          title: 'angleIt.pointHisConfig.allocationPoint',
          field: 'totalPoint',
          width: 150,
        }, {
          title: ' ',
          field: 'action',
          tdClassList: ['text-nowrap', 'text-center'], thClassList: ['text-nowrap', 'text-center'],
          width: 50,
          tdTemplate: this.action,
          fixed: true,
          fixedDir: 'right'
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.paramSearch.size,
      pageIndex: 1
    };
  }

  search(page: number) {
    this.paramSearch.page = page - 1;
    this.isLoading = true;
    const sub = this.pointHisConfigService.search(this.paramSearch).subscribe(result => {
        if (result.data) {
          this.listData = this.processResponse(result.data.content) || [];
          this.tableConfig.total = result.data.totalElements;
          this.tableConfig.pageIndex = this.paramSearch.page + 1;
        }
        this.isLoading = false;
      }, () => {
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }

  processResponse(responseData: HistoryPoint[]) {
    return responseData.map(item => {
      item.id = item.allotmentSettingId;  // set id field for each row in table: required
      return item;
    });
  }

  viewDetail(fromDate: string, content: string, listGocItAllotmentSettingDetail: Array<HistoryPointDetail>) {
    this.openModalView(fromDate, content, listGocItAllotmentSettingDetail);
  }

  copy(fromDate: string, content: string, listGocItAllotmentSettingDetail: Array<HistoryPointDetail>) {
    this.router.navigateByUrl('/goc-it/allocation/point-config');
    localStorage.removeItem('objHisPoint');
    const value = {
      fromDate: fromDate,
      content: content,
      listGocItAllotmentSettingDetail: listGocItAllotmentSettingDetail,
    }
    localStorage.setItem('objHisPoint', JSON.stringify(value));
  }

  openModalView(fromDate: string, content: string, listGocItAllotmentSettingDetail: Array<HistoryPointDetail>) {
    const param = {
      fromDate: fromDate,
      content: content,
      listGocItAllotmentSettingDetail: listGocItAllotmentSettingDetail
    }
    const title = "angleIt.pageName.pointHisConfig";
    this.modalRef = this.modal.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
      nzTitle: this.translate.instant(title),
      nzContent: ViewDetailHisPointComponent,
      nzComponentParams: {
        param: param
      },
      nzFooter: null
    });
  }

  override ngOnDestroy() {
    this.deletePopup?.modalDelete?.destroy();
    this.subs.forEach((sub) => sub.unsubscribe());
  }

}
