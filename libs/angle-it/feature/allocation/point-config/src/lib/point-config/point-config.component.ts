import {Component, Injector, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {Subscription} from 'rxjs';
import {BaseComponent} from '@hcm-mfe/shared/common/base-component';
import {userConfig} from '@hcm-mfe/shared/common/constants';
import {Pageable, TableConfig} from '@hcm-mfe/shared/data-access/models';
import {ValidateService} from '@hcm-mfe/shared/core';
import {NzSafeAny} from 'ng-zorro-antd/core/types';
import {NzModalRef} from 'ng-zorro-antd/modal';
import {AllocationInterface, PointConfigInterface} from '@hcm-mfe/angle-it/data-access/models';
import {PointConfigService} from '@hcm-mfe/angle-it/data-access/services';
import {Utils} from '@hcm-mfe/shared/common/utils';
import {Validators} from '@angular/forms';
import {Constant, FUNCTION_CODE} from '@hcm-mfe/angle-it/data-access/common';

@Component({
  selector: 'hcm-mfe-point-config',
  templateUrl: './point-config.component.html',
  styleUrls: ['./point-config.component.scss']
})
export class PointConfigComponent extends BaseComponent implements OnInit {
  subs: Subscription[] = [];
  isDisable = true;
  isSubmitted = false;
  listData: Array<AllocationInterface> = [];
  sumTotalPoint = 0;
  limit = userConfig.pageSize;
  paramSearch = {
    limit: this.limit,
    page: 0,
    rrgCode: ''
  };
  form = this.fb.group({
    content: [null, Validators.required],
    fromDate: null,
  });
  tableConfig!: TableConfig;
  @ViewChild('pointTmpl', { static: true }) point!: TemplateRef<NzSafeAny>;
  @ViewChild('totalPointTmpl', { static: true }) totalPoint!: TemplateRef<NzSafeAny>;
  @ViewChild('footerTmpl', { static: true }) footer!: TemplateRef<NzSafeAny>;
  pageable: Pageable = new Pageable();
  modalRef: NzModalRef | undefined;
  constant = Constant;
  objHisPoint: any;

  constructor(
    injector: Injector,
    private readonly pointConfigService: PointConfigService,
    public validateService: ValidateService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FUNCTION_CODE.ALLOTMENT_SETTING}`);
  }

  ngOnInit(): void {
    this.initTable();
    this.objHisPoint = JSON.parse(<string>localStorage.getItem("objHisPoint"));
    if (this.objHisPoint !== undefined && this.objHisPoint !== null) {
      this.form.controls['content'].setValue(null);
      this.copy(this.objHisPoint);
    } else {
      this.getAllotment();
    }
  }

  initTable() {
    this.tableConfig = {
      headers: [
        {
          title: 'STT',
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          fixed: true,
          fixedDir: 'left',
          width: 65
        },
        {
          title: 'angleIt.pointConfig.allocationGroup',
          field: 'posGroupName',
        },
        {
          title: 'angleIt.pointConfig.point',
          field: 'point',
          tdTemplate: this.point,
        },
        {
          title: 'angleIt.pointConfig.amount',
          field: 'totalEmployees',
        },
        {
          title: 'angleIt.pointConfig.totalPoint',
          field: 'totalPoint',
          width: 200,
          fixed: true,
          fixedDir: 'right'
        },
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.paramSearch.limit,
      pageIndex: 1,
      showPagination: false,
      footer: this.footer
    };
  }

  getAllotment() {
    this.isLoading = true;
    const sub = this.pointConfigService.getAllotment().subscribe(result => {
        if (result.data) {
          this.form.controls['content'].setValue(result.data.content);
          this.form.controls['fromDate'].setValue(result.data.fromDate);
          this.listData = result.data.allotmentSettingDetailList || [];
          this.listData.forEach((el: NzSafeAny) => {
            el.totalPoint = (el.point ?? 0) * (el.totalEmployees ?? 0);
            this.sumTotalPoint += el.totalPoint;
          });
        }
        this.isLoading = false;
      },(e) => {
        this.toastrCustom.error(
          this.translate.instant('common.notification.error') + `: ${e?.message}`
        );
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }

  add() {
    localStorage.removeItem('objHisPoint');
    this.isDisable = false;
    this.isLoading = true;
    this.isSubmitted = false;
    this.form.reset();
    const sub = this.pointConfigService.getAllotmentGroup().subscribe(result => {
        if (result.data) {
          this.form.controls['fromDate'].setValue(Utils.convertDateToFillForm(new Date));
          this.listData = result.data || [];
        }
        this.isLoading = false;
      }, (e) => {
        this.toastrCustom.error(
          this.translate.instant('common.notification.error') + `: ${e?.message}`
        );
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }

  copy(objHisPoint: any) {
    this.isDisable = false;
    this.isLoading = true;
    this.isSubmitted = false;
    this.form.reset();
    const sub = this.pointConfigService.getAllotmentGroup().subscribe(result => {
        if (result.data) {
          this.form.controls['fromDate'].setValue(Utils.convertDateToFillForm(new Date));
          this.form.controls['content'].setValue(null);
          this.listData = result.data || [];
          this.getRs(this.listData, this.objHisPoint.listGocItAllotmentSettingDetail);
        }
        this.isLoading = false;
      }, (e) => {
        this.toastrCustom.error(
          this.translate.instant('common.notification.error') + `: ${e?.message}`
        );
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }

  getRs(a: Array<AllocationInterface>, b: Array<AllocationInterface>) {
    let result = a.filter(item => b.some(itemToBeRemoved => {
      if(itemToBeRemoved.posGroupId === item.posGroupId){
        item.point = itemToBeRemoved.point;
        itemToBeRemoved.posGroupId === item.posGroupId
      }
    }))
  }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      this.isLoading = true;
      const data: PointConfigInterface = {
        content: this.form.controls['content'].value,
        fromDate: Utils.convertDateToSendServer(this.form.controls['fromDate'].value),
        allotmentSettingDetailList: this.listData
      }
      const subAdd = this.pointConfigService.saveAllotment(data).subscribe(
        () => {
          this.isLoading = false;
          this.isDisable = true;
          this.toastrCustom.success(this.translate.instant('common.notification.addSuccess'));
        },
        (error) => {
          this.toastrCustom.error(this.translate.instant('common.notification.addError') + `: ${error?.message}`);
          this.isLoading = false;
        }
      );
      this.subs.push(subAdd);
    }
  }

  goToHistory() {
    this.router.navigate([this.router.url, 'history']);
  }

  changePoint(row: AllocationInterface) {
    if(!row.point) {
      row.point = 0;
    }
    if (row.point && row.totalEmployees) {
      row.totalPoint = +row.point * row.totalEmployees;
    } else {
      row.totalPoint = 0;
    }
    this.sumTotalPoint = this.listData.map(e => e.totalPoint).reduce((a?: number, b?: number) => (a ?? 0) + (b ?? 0), 0) ?? 0;
  }

  override ngOnDestroy() {
    this.deletePopup?.modalDelete?.destroy();
    this.subs.forEach((sub) => sub.unsubscribe());
    localStorage.removeItem('objHisPoint');
  }

}
