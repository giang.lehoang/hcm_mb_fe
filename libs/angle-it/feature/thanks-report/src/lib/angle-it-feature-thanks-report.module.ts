import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from "@angular/router";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {NzGridModule} from "ng-zorro-antd/grid";
import {NzInputModule} from "ng-zorro-antd/input";
import {FormsModule} from "@angular/forms";
import {ThanksReportComponent} from "./thanks-report/thanks-report.component";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {NzIconModule} from "ng-zorro-antd/icon";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import { SharedUiMbEmployeeDataPickerModule } from '@hcm-mfe/shared/ui/mb-employee-data-picker';

@NgModule({
  imports: [CommonModule, RouterModule.forChild([
    {
      path: '',
      component: ThanksReportComponent
    }
  ]), SharedUiMbButtonModule, TranslateModule, SharedUiMbDatePickerModule, NzGridModule, NzInputModule,
    FormsModule, SharedUiMbInputTextModule, NzIconModule, SharedUiMbTableWrapModule, SharedUiMbTableModule, SharedUiLoadingModule, SharedUiMbEmployeeDataPickerModule
  ],
  exports: [ThanksReportComponent],
  declarations: [ThanksReportComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class AngleItFeatureThanksReportModule {}
