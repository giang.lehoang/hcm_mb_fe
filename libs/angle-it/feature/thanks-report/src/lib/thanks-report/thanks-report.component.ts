import {Component, Injector, OnInit} from "@angular/core";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {FormModalShowTreeUnitComponent} from "@hcm-mfe/shared/ui/form-modal-show-tree-unit";
import {NzModalRef} from "ng-zorro-antd/modal";
import {MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import * as moment from "moment";
import {ThanksHistoryService} from "@hcm-mfe/angle-it/data-access/services";
import {take} from "rxjs";
import * as FileSaver from "file-saver";
import {FUNCTION_CODE} from "@hcm-mfe/angle-it/data-access/common";

@Component({
  selector: 'app-thanks-report',
  templateUrl: './thanks-report.component.html',
  styleUrls: ['./thanks-report.component.scss'],
})
export class ThanksReportComponent extends BaseComponent implements OnInit {
  modalRef: NzModalRef | undefined;
  params = {
    empCode: "",
    orgId: "",
    fromDate: "",
    toDate: "",
  }
  fromDate = "";
  toDate = ""
  orgName = '';
  functionCode = FUNCTION_CODE.THANK_REPORT;
  heightTable = { x: '80vw', y: '50vh'};
  tableConfig: MBTableConfig = new MBTableConfig();
  listData = [];
  ngOnInit(): void {
    this.isLoading = true;
    this.getList();
    this.initTable();
  }
  constructor(injector : Injector,
              private readonly thanksHistoryService: ThanksHistoryService) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FUNCTION_CODE.THANK_REPORT}`);
    console.log(this.objFunction)
  }
  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'angleIt.thankReport.empCode',
          field: 'employeeCode',
          width: 150,
        },
        {
          title: 'angleIt.thankReport.fullName',
          field: 'fullName',
          fixed: window.innerWidth > 1024,
          width: 200
        },
        {
          title: 'angleIt.thankReport.orgName',
          field: 'orgName',
          needEllipsis: true,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'angleIt.thankReport.numOfTimesThanked',
          tdClassList: ['text-right'],
          field: 'numOfTimesThanked',
          fixed: window.innerWidth > 1024,
          width: 160
        },
        {
          title: 'angleIt.thankReport.pointReceipt',
          tdClassList: ['text-right'],
          field: 'pointReceipt',
          fixed: window.innerWidth > 1024,
          width: 150
        },
        {
          title: 'angleIt.thankReport.numOfThank',
          tdClassList: ['text-right'],
          field: 'numOfThank',
          width: 150,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'angleIt.thankReport.pointThank',
          tdClassList: ['text-right'],
          field: 'pointThank',
          width: 150,
          fixed: window.innerWidth > 1024,
        },
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageIndex: 0,
      pageSize: this.listData.length,
      showFrontPagination: false
    };
  }
  getList() {
    this.thanksHistoryService.getListThankReport(this.params).subscribe(res => {
      this.isLoading = false;
      this.listData = res.data;
      this.tableConfig.total = res.data.totalElements;
    }, () => {
      this.isLoading = false;
    });
  }
  clearDataOrgId() {
    this.orgName = '';
    this.params.orgId = '';
  }

  showModalOrg() {
    this.modalRef = this.modal.create({
      nzWidth: '80vw',
      nzTitle: this.translate.instant('Đơn vị'),
      nzContent: FormModalShowTreeUnitComponent,
    });
    this.modalRef.afterClose.subscribe(result => {
      this.orgName = result ? result.orgName : '';
      this.params.orgId = result ? result.orgId : '';
    });
  }

  exportExcel() {
    this.isLoading = true;
    this.thanksHistoryService.exportThankReport(this.params).pipe(take(1)).subscribe((res) => {
      this.isLoading = false;
      const blob = new Blob([res], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      FileSaver.saveAs(blob, 'Danh sach bao cao cam on.xlsx');
    }, () => {
      this.isLoading = false;
    })
  }
  onChange(value: any) {
    this.params.empCode = value?.employeeCode ? value.employeeCode : '';
  }
  search() {
    this.params.fromDate = this.fromDate ? moment(this.fromDate).format('DD/MM/YYYY') : '';
    this.params.toDate = this.toDate ? moment(this.toDate).format('DD/MM/YYYY') : '';
    if (this.fromDate && this.toDate && !moment(this.fromDate).isBefore(moment(this.toDate))) {
      this.toastrCustom.error(this.translate.instant('angleIt.thankReport.validateDate'));
      return;
    }
    this.isLoading = true;
    this.getList();
  }
}
