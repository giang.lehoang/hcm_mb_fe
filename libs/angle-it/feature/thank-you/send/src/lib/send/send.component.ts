import { Component, Injector, OnInit } from '@angular/core';
import { CustomValidators } from '@hcm-mfe/shared/common/validators';
import { FormGroup, Validators } from '@angular/forms';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { SendThanksService, ThanksConfigService, ThanksHistoryService } from '@hcm-mfe/angle-it/data-access/services';
import { SendThanksHistory, SendThanksInterface, ThanksConfigInterface } from '@hcm-mfe/angle-it/data-access/models';
import { maxInt32 } from '@hcm-mfe/shared/common/enums';
import { EmployeeDetail } from '@hcm-mfe/shared/data-access/models';
import { Subscription } from 'rxjs';
import { FUNCTION_CODE } from '@hcm-mfe/angle-it/data-access/common';

@Component({
  selector: 'hcm-mfe-send',
  templateUrl: './send.component.html',
  styleUrls: ['./send.component.scss']
})
export class SendComponent extends BaseComponent implements OnInit {
  subs: Subscription[] = [];
  isSubmitted = false;
  isDisablePoint = true;
  index?: number;
  myPoint = 0;
  form!: FormGroup;
  SEND_THANK_MESSAGE = FUNCTION_CODE.SEND_THANK_MESSAGE

  listImg: ThanksConfigInterface[] = [];

  listPoint = [
    {index: 0, path: '/assets/img/angle-it/point-1.png', value: 5 , isSelect: false, title: '5 Đ'},
    {index: 1, path: '/assets/img/angle-it/point-2.png', value: 10, isSelect: false, title: '10 Đ'},
    {index: 2, path: '/assets/img/angle-it/point-3.png', value: 15, isSelect: false, title: '15 Đ'},
    {index: 3, path: '/assets/img/angle-it/point-4.png', isSelect: false, title: 'Số khác'}
  ]

  indexPoint?: number;

  data?: SendThanksHistory;

  constructor(injector: Injector,
              private thanksConfigService: ThanksConfigService,
              private sendThanksService: SendThanksService) {
    super(injector);
    this.data = this.router.getCurrentNavigation()?.extras.state;
  }

  initForm() {
    this.form = this.fb.group({
      employees: [null, [CustomValidators.required]],
      point: [null, [CustomValidators.required]],
      content: [null, [CustomValidators.required, Validators.maxLength(500)]],
      thankyouSettingId: [null],
      message: [null, Validators.maxLength(500)],
    });
  }

  ngOnInit(): void {
    this.initForm();
    this.getMyPoint();
    this.getListThanksConfig();
    if(this.data) {
      const emp = {
        employeeId: this.data.employeeId,
        employeeCode: this.data.employeeCode,
        fullName: this.data.fullName,
        email: this.data.email
      }
      this.index = this.listImg.findIndex(el => el.thankyouSettingId === this.data?.thankYouSettingId);
      this.indexPoint = this.listPoint.findIndex(el => el.value === this.data?.point) > 0 ? this.listPoint.findIndex(el => el.value === this.data?.point) : 3;
      this.listPoint[this.indexPoint].isSelect = true;
      this.form = this.fb.group({
        employees: [[emp], [CustomValidators.required]],
        point: [this.data?.point, [CustomValidators.required]],
        content: [this.data?.content, [CustomValidators.required, Validators.maxLength(500)]],
        thankyouSettingId: [this.data?.thankYouSettingId],
        message: [this.data?.message, Validators.maxLength(500)],
      });
    }
  }

  getMyPoint() {
    this.subs.push(this.sendThanksService.getMyPoint().subscribe(res => {
        this.myPoint = res?.data?.point ?? 0;
    }, (error) => {
      this.toastrCustom.error(error?.message);
    })
    )
  }

  getListThanksConfig() {
    this.isLoading = true;
    this.subs.push(this.thanksConfigService.getListThanksConfig({ page: 0, size: maxInt32, flagStatus: 1 }).subscribe(res => {
      this.listImg = res.data.listData;
      if(this.data) {
        this.index = this.listImg.findIndex(el => el.thankyouSettingId === this.data?.thankYouSettingId);
      }
      this.isLoading = false;
    }, (error) => {
      this.toastrCustom.error(error?.message);
      this.isLoading = false;
    })
    )
  }

  selectPoint(event: number) {
    if(this.indexPoint || this.indexPoint === 0) {
      this.listPoint[this.indexPoint].isSelect = false;
    }
    this.indexPoint = event;
    const value = this.listPoint[this.indexPoint].value;
    if (value) {
      this.isDisablePoint = true;
    } else {
      this.isDisablePoint = false;
    }
    this.form.controls['point'].setValue(this.listPoint[this.indexPoint].value);
  }

  changeSelect(index: number) {
    if (index || index === 0) {
      this.index = index;
      this.form.controls['content'].setValue(this.listImg[index]?.content);
      this.form.controls['thankyouSettingId'].setValue(this.listImg[index]?.thankyouSettingId);
    }
  }

  send() {
    if(!this.form.controls['thankyouSettingId'].value) {
      this.toastrCustom.error(this.translate.instant('angleIt.send.chooseThanks'));
      return;
    }
    this.isSubmitted = true;
    if(this.form.valid) {
      const dataSend: SendThanksInterface = {};
      dataSend.thankYouSettingId = this.form.controls['thankyouSettingId'].value;
      dataSend.emailReceive = this.form.controls['employees'].value.map((el: EmployeeDetail) => el.email);
      dataSend.point = this.form.controls['point'].value;
      dataSend.message = this.form.controls['message'].value;
      dataSend.content = this.form.controls['content'].value;
      this.isLoading = true;
      this.subs.push(this.sendThanksService.onSend(dataSend).subscribe(() => {
        this.form.controls['thankyouSettingId'].setValue(null);
        this.form.controls['employees'].setValue(null);
        this.form.controls['content'].setValue(null);
        this.form.controls['message'].setValue(null);
        this.form.controls['point'].setValue(null);
        this.listPoint[this.indexPoint ?? 0].isSelect = false;
        this.index = undefined;
        this.toastrCustom.success(this.translate.instant('angleIt.send.sendSuccess'));
        this.isSubmitted = false;
        this.getMyPoint();
        this.isLoading = false;
      }, (error) => {
        this.toastrCustom.error(error?.message);
        this.isLoading = false;
      })
      )
    }
  }

  override ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }

  goToHistory() {
    this.router.navigate([this.router.url, 'history']);
  }
}
