import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SendComponent } from './send/send.component';
import { NzFormModule } from 'ng-zorro-antd/form';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { SharedDirectivesNumbericModule } from '@hcm-mfe/shared/directives/numberic';
import { AngleItUiSelectPointModule } from '@hcm-mfe/angle-it/ui/select-point';
import { AngleItUiImgItemModule } from '@hcm-mfe/angle-it/ui/img-item';
import { SendHistoryComponent } from './send-history/send-history.component';
import { SharedUiMbTableMergeCellWrapModule } from '@hcm-mfe/shared/ui/mb-table-merge-cell-wrap';
import { SharedUiMbTableMergeCellModule } from '@hcm-mfe/shared/ui/mb-table-merge-cell';
import { SharedUiEmployeeDataPickerModule } from '@hcm-mfe/shared/ui/employee-data-picker';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { SharedUiMbDatePickerModule } from '@hcm-mfe/shared/ui/mb-date-picker';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { SharedUiMbButtonIconModule } from '@hcm-mfe/shared/ui/mb-button-icon';
import { SendViewDetailComponent } from './send-view-detail/send-view-detail.component';
import { NzImageModule } from 'ng-zorro-antd/image';

@NgModule({
  imports: [CommonModule,
    RouterModule.forChild([
      {
        path: '',
        data: {
          pageName: 'angleIt.pageName.thankYouSend',
          breadcrumb: 'angleIt.breadcrumb.thankYouSend'
        },
        component: SendComponent
      },
      {
        path: 'history',
        data: {
          pageName: 'angleIt.pageName.thankYouHistory',
          breadcrumb: 'angleIt.breadcrumb.thankYouHistory'
        },
        component: SendHistoryComponent
      }
    ]), NzFormModule, TranslateModule, AngleItUiImgItemModule, SharedUiMbInputTextModule,
    SharedDirectivesNumbericModule, AngleItUiSelectPointModule, SharedUiMbTableMergeCellWrapModule, SharedUiMbTableMergeCellModule, SharedUiEmployeeDataPickerModule, ReactiveFormsModule, SharedUiLoadingModule, SharedUiMbDatePickerModule, SharedUiMbSelectModule, SharedUiMbButtonModule, NzTagModule, SharedUiMbButtonIconModule, NzImageModule],
  declarations: [
    SendComponent,
    SendHistoryComponent,
    SendViewDetailComponent
  ],
})
export class AngleItFeatureThankYouSendModule {}
