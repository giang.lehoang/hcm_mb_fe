import { Component, Injector, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { CatalogModel, Pageable, TableConfig } from '@hcm-mfe/shared/data-access/models';
import { userConfig } from '@hcm-mfe/shared/common/constants';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { ValidateService } from '@hcm-mfe/shared/core';
import { cleanDataForm, Utils } from '@hcm-mfe/shared/common/utils';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { DateValidator } from '@hcm-mfe/shared/common/validators';
import { Constant } from '@hcm-mfe/angle-it/data-access/common';
import { ThanksHistoryService } from '@hcm-mfe/angle-it/data-access/services';
import { SendViewDetailComponent } from '../send-view-detail/send-view-detail.component';
import { SendThanksHistory } from '@hcm-mfe/angle-it/data-access/models';

@Component({
  selector: 'hcm-mfe-send-history',
  templateUrl: './send-history.component.html',
  styleUrls: ['./send-history.component.scss']
})
export class SendHistoryComponent extends BaseComponent implements OnInit, OnDestroy {
  subs: Subscription[] = [];
  isSubmitted = false;
  constant = Constant;
  listData: Array<SendThanksHistory> = [];
  listType: CatalogModel[] = [];
  limit = userConfig.pageSize;
  paramSearch = {
    size: this.limit,
    page: 0
  };
  searchForm = this.fb.group(
    {
      fromDate: null,
      toDate: null,
      type: null,
    },{
      validators: [DateValidator.validateRangeDate('fromDate', 'toDate', 'rangeDateError')],
    }
  );
  tableConfig!: TableConfig;
  @ViewChild('actionTmpl', { static: true }) action!: TemplateRef<NzSafeAny>;
  @ViewChild('senderTmpl', { static: true }) sender!: TemplateRef<NzSafeAny>;
  @ViewChild('receiverTmpl', { static: true }) receiver!: TemplateRef<NzSafeAny>;
  @ViewChild('typeTmpl', { static: true }) type!: TemplateRef<NzSafeAny>;
  pageable: Pageable = new Pageable();
  modalRef: NzModalRef | undefined;

  constructor(
    injector: Injector,
    public validateService: ValidateService,
    public thanksHistoryService: ThanksHistoryService,
  ) {
    super(injector);
    const subRoute = this.route.queryParams.subscribe((params: any) => {
      if (params?.id) {
        this.getDetailById(params.id);
      }
    });
    this.subs.push(subRoute);
  }

  ngOnInit(): void {
    this.listType = Constant.SEND_HISTORY_TYPE.map(item => {
      item.label = this.translate.instant(item.label);
      return item;
    })
    this.initTable();
    this.search(1);
  }

  initTable() {
    this.tableConfig = {
      headers: [
        {
          title: 'STT',
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          fixed: true,
          fixedDir: 'left',
          width: 50
        },
        {
          title: 'angleIt.sendHistory.sentDate',
          field: 'createdDate', width: 250,
        },
        {
          title: 'angleIt.sendHistory.sender',
          tdTemplate: this.sender,
          field: '', width: 250,
        },
        {
          title: 'angleIt.sendHistory.receiver',
          tdTemplate: this.receiver,
          field: '', width: 250,
        },{
          title: 'angleIt.sendHistory.classify',
          field: 'type', width: 250,
          tdTemplate: this.type
        },
        {
          title: 'angleIt.sendHistory.thanksPoints',
          field: 'point', width: 200,
        },
        {
          title: 'angleIt.sendHistory.pointsRemaining',
          field: 'stillPoint', width: 200,
        },
        {
          title: ' ',
          field: '',
          tdTemplate: this.action,
          tdClassList: ['text-center'],
          fixed: true,
          fixedDir: 'right',
          width: 100,
        },
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.paramSearch.size,
      pageIndex: 1,
    };
  }

  override triggerSearchEvent(): void {
    this.search(1);
  }

  search(page: number) {
    this.isSubmitted = true;
    if (this.searchForm.valid) {
      this.paramSearch.page = page - 1;
      const paramSearch = cleanDataForm(this.searchForm);
      if (paramSearch.fromDate) {
        paramSearch.fromDate = Utils.convertDateToSendServer(paramSearch.fromDate);
      }
      if (paramSearch.toDate) {
        paramSearch.toDate = Utils.convertDateToSendServer(paramSearch.toDate);
      }
      const params = { ...paramSearch, page: this.paramSearch.page, size: this.limit };
      this.isLoading = true;
      const sub = this.thanksHistoryService.search(params).subscribe(result => {
          if (result.data) {
            this.listData = result.data.content || [];
            this.tableConfig.total = result.data.totalElements;
            this.tableConfig.pageIndex = this.paramSearch.page + 1;
            this.isSubmitted = false;
          }
          this.isLoading = false;
        },(error) => {
          this.toastrCustom.error(error?.message);
          this.isLoading = false;
        }
      );
      this.subs.push(sub);
    }
  }

  create() {
    this.router.navigate([this.router.url, 'create']);
  }

  getDetailById(id: number) {
    this.thanksHistoryService.getDetailById(id.toString()).subscribe(res => {
      this.showModalDetail(res.data);
    }, error => {
      this.toastrCustom.error(error?.message);
    })
  }

  viewDetail(index: number) {
    this.showModalDetail(this.listData[index]);
  }

  showModalDetail(data: SendThanksHistory) {
    const title = "angleIt.sendDetail.title";
    this.modalRef = this.modal.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
      nzTitle: this.translate.instant(title),
      nzContent: SendViewDetailComponent,
      nzComponentParams: {
        data: data
      },
      nzFooter: null
    });
  }

  copy(index: number) {
    const data = this.listData[index];
    this.router.navigate(['/goc-it/thank-you/send'], {state: data} );
  }

  override ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }

}
