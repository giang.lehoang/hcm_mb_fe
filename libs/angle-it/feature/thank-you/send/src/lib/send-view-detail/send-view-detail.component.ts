import { Component, Injector, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, Validators } from '@angular/forms';
import { SendThanksHistory} from '@hcm-mfe/angle-it/data-access/models';
import { ImageService, SendThanksService, ThanksConfigService } from '@hcm-mfe/angle-it/data-access/services';
import { CustomValidators } from '@hcm-mfe/shared/common/validators';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { NzSafeAny } from 'ng-zorro-antd/core/types';

@Component({
  selector: 'hcm-mfe-send-view-detail',
  templateUrl: './send-view-detail.component.html',
  styleUrls: ['./send-view-detail.component.scss']
})
export class SendViewDetailComponent extends BaseComponent implements OnInit {

  data: SendThanksHistory = {};
  subs: Subscription[] = [];
  isSubmitted = false;
  myPoint = 0;
  form!: FormGroup;
  imgSrc: string | null = '';

  constructor(injector: Injector,
              private thanksConfigService: ThanksConfigService,
              private imageService: ImageService,
              private sendThanksService: SendThanksService) {
    super(injector)
  }

  initForm(data: SendThanksHistory) {
    const emps: NzSafeAny = [];
    if(data) {
      emps.push({
        employeeId: data.employeeId,
        employeeCode: data.employeeCode,
        fullName: data.fullName,
        email: data.email
      })
    }
      this.form = this.fb.group({
        employees: [emps, [CustomValidators.required]],
        point: [data?.point, [CustomValidators.required]],
        content: [data?.content, [CustomValidators.required, Validators.maxLength(500)]],
        thankyouSettingId: [data?.thankYouSettingId],
        message: [data?.message, Validators.maxLength(500)],
      });
      if (data?.imageId) {
        this.getImage(data.imageId);
      }
  }

  ngOnInit(): void {
    this.initForm(this.data);
    this.getMyPoint();
  }

  getMyPoint() {
    this.subs.push(this.sendThanksService.getMyPoint().subscribe(res => {
        this.myPoint = res?.data?.point ?? 0;
      }, (error) => {
        this.toastrCustom.error(error?.message);
      })
    )
  }

  getImage(id: number) {
    this.isLoading = true;
    this.subs.push(
      this.imageService.getImage(id).subscribe(res => {
        this.imgSrc = res?.data ? 'data:image/jpg;base64,' + res?.data : null;
        this.isLoading = false;
      }, () => {
        this.isLoading = false;
      })
    );
  }

  override ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }

}
