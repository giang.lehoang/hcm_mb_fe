import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SendViewDetailComponent } from './send-view-detail.component';

describe('SendViewDetailComponent', () => {
  let component: SendViewDetailComponent;
  let fixture: ComponentFixture<SendViewDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SendViewDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SendViewDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
