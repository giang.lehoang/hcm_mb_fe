import { Component, Injector, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormArray, FormGroup, Validators } from '@angular/forms';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { Constant, FUNCTION_CODE } from '@hcm-mfe/angle-it/data-access/common';
import { CatalogModel } from '@hcm-mfe/shared/data-access/models';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { SelectImageComponent } from '../select-image/select-image.component';
import { ImageService, ThanksConfigService } from '@hcm-mfe/angle-it/data-access/services';
import { ThanksConfigError, ThanksConfigInterface } from '@hcm-mfe/angle-it/data-access/models';
import { Subscription } from 'rxjs';
import { CommonUtils } from '@hcm-mfe/shared/core';
import { maxInt32 } from '@hcm-mfe/shared/common/enums';

@Component({
  selector: 'hcm-mfe-config',
  templateUrl: './config.component.html',
  styleUrls: ['./config.component.scss']
})
export class ConfigComponent extends BaseComponent implements OnInit {

  form = this.fb.group({
    listThanksConfig: this.fb.array([])
  });
  subs: Subscription[] = [];
  isSubmitted = false;
  isError = false;
  listError: ThanksConfigError[] = [];
  listStatus: CatalogModel[] = [];
  constant = Constant;
  modalRef: NzModalRef | undefined;
  listData: ThanksConfigInterface[] = [];
  listImage: ThanksConfigInterface[] = [];

  @ViewChild('modalFooterTmpl', {static: true}) modalFooter!: TemplateRef<NzSafeAny>;

  constructor(
    injector: Injector,
    private thanksConfigService: ThanksConfigService,
    private imageService: ImageService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FUNCTION_CODE.THANKYOU_SETTING}`);
    this.getAllImage();
  }

  ngOnInit(): void {
    this.listStatus = Constant.CONFIG_STATUS.map(item => {
      item.label = this.translate.instant(item.label);
      return item;
    })
    this.getListThanksConfig();
    this.getAllImage();
  }

  getListThanksConfig() {
    this.isLoading = true;
    this.thanksConfigService.getListThanksConfig({page: 0, size: maxInt32}).subscribe(res => {
      this.form.controls['listThanksConfig'] = this.fb.array([]);
      this.listData = res.data.listData;
      if (this.listData.length > 0) {
        this.listData.forEach(item => {
          this.addParam(false, item);
        });
      } else {
        this.addParam(true);
      }
      this.isLoading = false;
    }, (error) => {
      this.toastrCustom.error(error?.message);
      this.isLoading = false;
    })
  }

  get listThanksConfig(): NzSafeAny {
    return this.form.controls['listThanksConfig'] as FormArray;
  }

  addParam(isEdit: boolean, item?: ThanksConfigInterface) {
    const param = this.fb.group({
      thankyouSettingId: [item?.thankyouSettingId ? item.thankyouSettingId : null],
      content: [item?.content ? item.content : null, [Validators.required]],
      imageId: [item?.imageId ? item.imageId : null],
      imageContent: [item?.imageContent ? item.imageContent : null],
      imageData: [null],
      image: [item?.image ? item.image : null],
      docId: [item?.docId ? item.docId : null],
      flagStatus: [item?.flagStatus ? item.flagStatus : 0, Validators.required],
      isEdit: [isEdit],
    });
      if (isEdit) {
        param.get('flagStatus')?.setValue(1);
        this.listThanksConfig.insert(0, param);
      } else {
        this.listThanksConfig.push(param);
        this.getImage(param);
      }
  }

  selectImage(data: FormGroup) {
    this.modalRef = this.modal.create({
      nzWidth: this.getModalWidth(),
      nzTitle: this.translate.instant('angleIt.config.selectImage'),
      nzContent: SelectImageComponent,
      nzComponentParams: {
        listImage: this.listImage,
        imageId: data.controls['imageId'].value
      },
      nzFooter: this.modalFooter
    });

    this.modalRef.afterClose.subscribe((res) => {
      if(res != null) {
        if (res) {
          if(res?.imageId) {
            data.controls['imageId'].setValue(res?.imageId);
            this.getImage(data);
          } else {
            if(res.isUpload) {
              data.controls['imageId'].setValue(null);
              data.controls['imageData'].setValue(res.strBase64);
              data.controls['image'].setValue(res.file as any);
            }
          }
        }
      }
    });
  }

  getModalWidth(): number {
    if(window.innerWidth > 767) {
      return window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5
    }
    return window.innerWidth;
  }

  getAllImage() {
    this.imageService.getAllImage().subscribe(res => {
      this.listImage = res.data;
    })
  }

  edit(data: FormGroup) {
    data.controls['isEdit'].setValue(true);
  }

  reset(data: FormGroup) {
    data.controls['isEdit'].setValue(false);
    const thankyouSettingId = data.controls['thankyouSettingId'].value;
    const item = this.listData.find(el => el.thankyouSettingId === thankyouSettingId);
    if (item) {
      data.controls['imageId'].setValue(item.imageId);
      data.controls['content'].setValue(item.content);
      data.controls['flagStatus'].setValue(item.flagStatus);
      data.controls['image'].setValue(null);
      this.getImage(data);
    }
  }

  delete(data: FormGroup, index: number) {
    const id = data.controls['thankyouSettingId'].value;
    if (id) {
      this.deletePopup.showModal(() => {
        const subDelete = this.thanksConfigService.deleteThanksConfigById(id).subscribe(res => {
          this.listThanksConfig.removeAt(index);
          this.toastrCustom.success(this.translate.instant('common.notification.deleteSuccess'));
          this.isLoading = false;
        }, (e) => {
          this.toastrCustom.error(this.translate.instant('common.notification.deleteError') + `: ${e?.message}`);
          this.isLoading = false;
        });
        this.subs.push(subDelete);
      });
    } else {
      this.listThanksConfig.removeAt(index);
    }
  }

  onAddParam() {
    this.isError = false;
    this.addParam(true);
  }

  onSave() {
    this.isSubmitted = true;
    const listThanksConfig = this.form.controls['listThanksConfig'].value.filter((el: NzSafeAny) => el.isEdit);
    listThanksConfig.forEach((item: NzSafeAny, index: number) => {
      item.index = index;
    })
    if (this.form.controls['listThanksConfig'].valid && listThanksConfig.length > 0 && !listThanksConfig.some((el: NzSafeAny) => !el.image && !el.imageId)) {
      this.isLoading = true;
      this.isSubmitted = false;
      CommonUtils.convertFormFile({dataList: listThanksConfig})
      this.thanksConfigService.onSave(CommonUtils.convertFormFile({dataList: listThanksConfig})).subscribe(() => {
        this.isLoading = false;
        this.getListThanksConfig();
        this.toastrCustom.success(this.translate.instant('common.notification.success'));
      }, (error) => {
        this.toastrCustom.error(this.translate.instant('common.notification.error'));
        this.isError = true;
        this.listError = error.data ?? [];
        this.isLoading = false;
      });
    }
  }

  getImage(data: FormGroup) {
    this.subs.push(
      this.imageService.getImage(data.controls['imageId'].value).subscribe(res => {
        data.controls['imageData'].setValue(res?.data ? 'data:image/jpg;base64,' + res?.data : null);
      })
    );
  }

  checkError(i: number, data: FormGroup, type: 'contentError' | 'imageError' | 'flagStatusError') {
    if(this.listError.length > 0) {
      const thankyouSettingId = data.controls['thankyouSettingId'].value;
      if(thankyouSettingId) {
        const itemError: NzSafeAny = this.listError.find(e => e.thankYouSettingId === thankyouSettingId);
        if(itemError) {
          return itemError[type];
        }
      } else {
        const itemError: NzSafeAny = this.listError.find(e => e.index === i);
        if(itemError) {
          return itemError[type];
        }
      }
    }
    return false;
  }
}
