import { Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { ImageService } from '@hcm-mfe/angle-it/data-access/services';
import { Subscription } from 'rxjs';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { NzUploadFile } from 'ng-zorro-antd/upload';

@Component({
  selector: 'hcm-mfe-select-image',
  templateUrl: './select-image.component.html',
  styleUrls: ['./select-image.component.scss']
})
export class SelectImageComponent extends BaseComponent implements OnInit {

  imageId?: number;
  listImage: NzSafeAny[] = [];
  listImageTem: NzSafeAny[] = [];
  subs: Subscription[] = [];
  index?: number;
  fileSelect?: { strBase64?: string; file?: NzUploadFile } = {}

  constructor(injector: Injector,
              private imageService: ImageService,
              private modalRef: NzModalRef,) {
    super(injector);
  }

  ngOnInit(): void {
    this.listImageTem = [];
    this.listImageTem.push(...JSON.parse(JSON.stringify(this.listImage)), {isUpload: true});
  }

  changeSelect(index: number) {
    if (index || index === 0) {
      this.listImageTem[this.index ?? 0].isSelect = false;
      this.index = index;
    }
  }

  choose() {
    if (!this.index && this.imageId) {
      this.modalRef.close(this.listImageTem.find(el => el.imageId === this.imageId));
    } else {
      this.modalRef.close(this.listImageTem[this.index] = {...this.listImageTem[this.index], ...this.fileSelect});
    }
  }

  close() {
    if (!this.index && this.imageId) {
      this.modalRef.close(this.listImageTem.find(el => el.imageId === this.imageId));
    } else {
      this.modalRef.close(false);
    }
  }

  onFileSelect($event: { strBase64?: string; file?: NzUploadFile }) {
    this.fileSelect = $event;
  }
}
