import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfigComponent } from './config/config.component';
import { SelectImageComponent } from './select-image/select-image.component';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzTableModule } from 'ng-zorro-antd/table';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { SharedUiMbButtonIconModule } from '@hcm-mfe/shared/ui/mb-button-icon';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { AngleItUiImgItemModule } from '@hcm-mfe/angle-it/ui/img-item';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';

@NgModule({
  imports: [CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: ConfigComponent
      }
    ]), ReactiveFormsModule, NzFormModule, NzTableModule, TranslateModule, SharedUiMbInputTextModule, SharedUiMbSelectModule,
    NzCheckboxModule, SharedUiMbButtonIconModule, SharedUiMbButtonModule, NzTagModule, AngleItUiImgItemModule, NzAvatarModule, NzIconModule, SharedUiLoadingModule, NzToolTipModule
  ],
  declarations: [
    ConfigComponent,
    SelectImageComponent
  ],
})
export class AngleItFeatureThankYouConfigModule {}
