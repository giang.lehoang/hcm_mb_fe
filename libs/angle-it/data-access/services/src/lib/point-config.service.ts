import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from '@hcm-mfe/angle-it/data-access/common';

@Injectable({
  providedIn: 'root',
})
export class PointConfigService extends BaseService {
  readonly baseUrl = UrlConstant.ALLOTMENT_CONFIG;

  public getAllotment(): Observable<any> {
    const url = this.baseUrl
    return this.get(url, undefined, UrlConstant.SERVICE_ANGLE_IT);
  }

  public saveAllotment(data: any): Observable<any> {
    const url = this.baseUrl;
    return this.post(url, data, undefined, UrlConstant.SERVICE_ANGLE_IT);
  }

  public getAllotmentGroup(): Observable<any> {
    const url = this.baseUrl + UrlConstant.ALLOTMENT_CONFIGS.GET_ALLOTMENT_GROUP;
    return this.get(url, undefined, UrlConstant.SERVICE_ANGLE_IT);
  }

}
