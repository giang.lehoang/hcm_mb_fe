import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import {UrlConstant} from "@hcm-mfe/angle-it/data-access/common";
import { NzSafeAny } from 'ng-zorro-antd/core/types';

@Injectable({
  providedIn: 'root',
})
export class SendThanksService extends BaseService {
  readonly baseUrl = UrlConstant.SEND_THANKS;

  public getMyPoint(): Observable<any> {
    const url = this.baseUrl + UrlConstant.SEND_THANK_YOU.MY_POINT
    return this.get(url, undefined, UrlConstant.SERVICE_ANGLE_IT);
  }

  public onSend(data: NzSafeAny): Observable<any> {
    const url = this.baseUrl;
    return this.post(url, data, undefined, UrlConstant.SERVICE_ANGLE_IT);
  }



}
