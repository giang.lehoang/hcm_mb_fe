import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import {UrlConstant} from "@hcm-mfe/angle-it/data-access/common";
import { NzSafeAny } from 'ng-zorro-antd/core/types';

@Injectable({
  providedIn: 'root',
})
export class ThanksHistoryService extends BaseService {
  readonly baseUrl = UrlConstant.THANKS_HISTORY;

  public search(params: NzSafeAny): Observable<any> {
    const url = this.baseUrl + UrlConstant.THANKS_HISTORIES.SEARCH;
    return this.get(url, {params}, UrlConstant.SERVICE_ANGLE_IT);
  }

  public getDetailById(id: string): Observable<any> {
    const url = this.baseUrl + UrlConstant.THANKS_HISTORIES.BY_ID.replace('{id}', id);
    return this.get(url, undefined, UrlConstant.SERVICE_ANGLE_IT);
  }

  public onSave(data: FormData): Observable<any> {
    const url = this.baseUrl;
    return this.post(url, data, undefined, UrlConstant.SERVICE_ANGLE_IT);
  }

  public getListThankReport(params: NzSafeAny): Observable<any> {
    const url = this.baseUrl + '/report-user-point';
    return this.post(url, params, undefined, UrlConstant.SERVICE_ANGLE_IT);
  }
  public exportThankReport(params: NzSafeAny): Observable<any> {
    const url = this.baseUrl + '/export-user-point';
    return this.post(url, params, {responseType: 'arraybuffer'}, UrlConstant.SERVICE_ANGLE_IT);
  }
}
