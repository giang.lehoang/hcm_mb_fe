import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import {UrlConstant} from "@hcm-mfe/angle-it/data-access/common";

@Injectable({
  providedIn: 'root',
})
export class ThanksConfigService extends BaseService {
  readonly baseUrl = UrlConstant.THANKS_CONFIG;

  public getListThanksConfig(params: { size: number; page: number; flagStatus?: number }): Observable<any> {
    const url = this.baseUrl
    return this.get(url, { params }, UrlConstant.SERVICE_ANGLE_IT);
  }

  public deleteThanksConfigById(id: string): Observable<any> {
    const url = this.baseUrl + UrlConstant.THANKS_CONFIGS.BY_ID.replace('{id}', id);
    return this.delete(url, undefined, UrlConstant.SERVICE_ANGLE_IT);
  }

  public onSave(data: FormData): Observable<any> {
    const url = this.baseUrl;
    return this.post(url, data, undefined, UrlConstant.SERVICE_ANGLE_IT);
  }



}
