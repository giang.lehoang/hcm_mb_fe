import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from '@hcm-mfe/angle-it/data-access/common';

@Injectable({
  providedIn: 'root',
})
export class PointHisConfigService extends BaseService {
  readonly baseUrl = UrlConstant.POINT_HISTORY;

  public search(params: any): Observable<any> {
    const url = this.baseUrl
    return this.get(url, { params }, UrlConstant.SERVICE_ANGLE_IT);
  }
}
