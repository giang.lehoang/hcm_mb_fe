import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import {UrlConstant} from "@hcm-mfe/angle-it/data-access/common";

@Injectable({
  providedIn: 'root',
})
export class ImageService extends BaseService {
  readonly baseUrl = UrlConstant.IMAGE;


  public getAllImage(): Observable<any> {
    const url = this.baseUrl + UrlConstant.IMAGES.GET_ALL_IMAGE;
    return this.get(url, undefined, UrlConstant.SERVICE_ANGLE_IT);
  }


  public getImage(imageId: number) {
    const url = this.baseUrl + UrlConstant.IMAGES.DOWNLOAD.replace('{imageId}', imageId.toString())
    return this.get(url, undefined, UrlConstant.SERVICE_ANGLE_IT);
  }


  // public deleteReportCategoryById(id: string): Observable<any> {
  //   const url = this.baseUrl + UrlConstant.REPORT_GROUPS.BY_ID.replace('{id}', id);
  //   return this.delete(url, undefined, UrlConstant.SERVICE_ANGLE_IT);
  // }
  //
  // public getParamInBI(params: {url: string}): Observable<any> {
  //   const url = this.baseUrl + UrlConstant.REPORT_GROUPS.GET_PARAM_BY_URL;
  //   return this.get(url, { params }, UrlConstant.SERVICE_ANGLE_IT);
  // }



}
