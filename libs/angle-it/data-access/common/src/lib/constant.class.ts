export class Constant {

  public static readonly CONFIG_STATUS = [
    {label: 'angleIt.label.flagStatusEffect', value: 1, bgColor: '#DAF9EC', color: '#06A561'},
    {label: 'angleIt.label.flagStatusNotEffect', value: 0, bgColor:'#FDE7EA', color: '#FA0B0B'}
  ];

  public static readonly SEND_HISTORY_TYPE = [
    {label: 'angleIt.label.thankYou', value: 1, bgColor: '#DAF9EC', color: '#06A561'},
    {label: 'angleIt.label.acceptThankYou', value: 2, bgColor:'#E9EAFF', color: '#141ED2'}
  ];

}
