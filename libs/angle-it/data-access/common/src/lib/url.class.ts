export class UrlConstant {

  public static readonly SERVICE_ANGLE_IT = 'ANGLE_IT';
  public static readonly THANKS_CONFIG = 'v1.0/thankyou-setting';
  public static readonly IMAGE = 'v1.0/image';
  public static readonly ALLOTMENT_CONFIG = 'v1.0/allotment';
  public static readonly THANKS_HISTORY = 'v1.0/thank-you-history';
  public static readonly POINT_HISTORY = 'v1.0/allotment/history';
  public static readonly SEND_THANKS = 'v1.0/points';

  public static readonly SEND_THANK_YOU = {
    MY_POINT: '/my-point'
  }

  public static readonly THANKS_CONFIGS = {
    BY_ID: '/{id}'
  }

  public static readonly THANKS_HISTORIES = {
    SEARCH: '/search-by-condition',
    BY_ID: '/{id}'
  }

  public static readonly ALLOTMENT_CONFIGS = {
    GET_ALLOTMENT_GROUP: '/allotment-group'
  }

  public static readonly IMAGES = {
    GET_ALL_IMAGE: '/get-all-image',
    DOWNLOAD: '/download/{imageId}',
  }

  public static readonly CATEGORY = {
    LOOKUP_VALUE: '/lookup-values'
  }


}


