
export interface SendThanksInterface {
  thankYouSettingId?: number;
  emailReceive?: Array<string>;
  point?: number;
  message?: number;
  content?: number;
}

export interface SendThanksHistory {
  hisId?: number,
  thankYouSettingId?: number,
  point?: number,
  stillPoint?: number,
  createdBy?: string,
  user1?: string,
  user2?: string,
  content?: string,
  message?: string,
  isRed?: number,
  type?: number,
  employeeId?: number,
  employeeCode?: string,
  fullName?: string,
  email?: string
  imageId?: number

}


