export interface PointConfigInterface {
  content?: string;
  fromDate?: string | null;
  allotmentSettingDetailList?: Array<AllocationInterface>;
}

export interface AllocationInterface {
  id?: number;
  detail?: number;
  posGroupId?: number;
  groupName?: string;
  point?: number | string;
  totalEmployees?: number;
  totalPoint?: number;
}


export class HistoryPoint {
  id?: number;
  allotmentSettingId?: number;
  content?: number | string;
  fromDate?: string;
  totalPoint?: number | string;
  listGocItAllotmentSettingDetail?: HistoryPointDetail;
}

export class HistoryPointDetail {
  id?: number;
  allotmentSettingId?: number;
  detailId?: number | string;
  numberOfPosGroup?: number | string;
  point?: number | string;
  posGroupId?: number;
  posGroupName?: string;
}
