export interface PointConfigInterface {
  content?: string;
  fromDate?: string;
  listAllocation?: Array<AllocationInterface>;
}

export interface AllocationInterface {
  prgId?: number;
  prgName?: string;
  point?: number;
  amount?: number;
  totalPoint?: number;
}
