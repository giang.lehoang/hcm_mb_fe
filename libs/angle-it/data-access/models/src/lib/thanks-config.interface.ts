import { NzSafeAny } from 'ng-zorro-antd/core/types';

export interface ThanksConfigInterface {
  thankyouSettingId?: number;
  content?: string;
  flagStatus?: number;
  imageId?: number;
  docId?: number;
  imageContent?: NzSafeAny;
  image?: NzSafeAny;
}

export interface ThanksConfigError {
  thankYouSettingId?: number;
  contentError?: string;
  imageError?: string;
  flagStatusError?: string;
  idError?: string;
  index?: number;
}

