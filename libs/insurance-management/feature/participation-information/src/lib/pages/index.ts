import { ParticipationInformationComponent } from './participation-information/participation-information.component';
import { ParticipationInformationFormComponent } from './participation-information-form/participation-information-form.component';

export const pages = [
  ParticipationInformationComponent,
  ParticipationInformationFormComponent,
];

export * from './participation-information/participation-information.component';
export * from './participation-information-form/participation-information-form.component';
