import { Component, Injector, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import {
  IMbOption,
  insuranceRateSubjectEmployeeOptions,
  IParticipationInformation,
  participationInformationFileTypeOptions,
  participationInformationStatusOptions,
} from '@hcm-mfe/insurance-management/data-access/models';
import {
  ParticipationInformationService,
  TreatmentPlaceService,
} from '@hcm-mfe/insurance-management/data-access/services';
import {
  BaseFormComponent,
  DateTimeUtil,
} from '@hcm-mfe/insurance-management/helper';
import { Constant, Mode } from '@hcm-mfe/shared/common/constants';
import { StaffInfoService } from '@hcm-mfe/shared/core';
import { finalize, forkJoin, tap } from 'rxjs';

@Component({
  selector: 'app-participation-information-form',
  templateUrl: './participation-information-form.component.html',
  styleUrls: ['./participation-information-form.component.scss'],
})
export class ParticipationInformationFormComponent
  extends BaseFormComponent<IParticipationInformation>
  implements OnInit
{
  id: number | null = null;
  CATALOGS = Constant.CATALOGS;
  catalogs: { [key: string]: IMbOption[] } = {};
  subjectEmployeeOptions = insuranceRateSubjectEmployeeOptions();
  fileTypeOptions = participationInformationFileTypeOptions();
  joinedOptions = participationInformationStatusOptions();
  treatmentPlaceOptions: IMbOption[] = [];
  regionOptions: IMbOption[] = [];

  constructor(
    injector: Injector,
    private readonly participationInformationService: ParticipationInformationService,
    private staffInfoService: StaffInfoService,
    private treatmentPlaceService: TreatmentPlaceService
  ) {
    super(injector);
    this.createApi = (body: IParticipationInformation) =>
      this.participationInformationService.create(this.convertBody(body)).pipe(
        tap((res) =>
          this.router.navigate(['..', 'update'], {
            queryParams: { id: res.data.id },
          })
        )
      );
    this.updateApi = (id: number, body: IParticipationInformation) =>
      this.participationInformationService.edit(id, this.convertBody(body));
  }

  override ngOnInit(): void {
    super.ngOnInit();
    this.route.queryParamMap.subscribe((params) => {
      this.mode = location.pathname.includes('/create') ? Mode.ADD : Mode.EDIT;
      const id = params.get('id');
      this.id = id ? +id : null;
      if (!this.id) return;
      this.isLoading = true;
      this.participationInformationService
        .getOne(this.id)
        .pipe(finalize(() => (this.isLoading = false)))
        .subscribe((res) => {
          this.data = res.data;
          this.patchValueInfo();
        });
    });
    this.getCatalogs();
    this.getTreatmentPlaces();
  }

  override initForm() {
    this.form = this.fb.group({
      staffCode: [null, [Validators.required]],
      fullName: [null, [Validators.required]],
      dob: [null, [Validators.required]],
      taxCode: [null, [Validators.required]],
      gender: [null, [Validators.required]],
      nationality: [null, [Validators.required]],
      phone: [null, []],
      email: [null, []],
      ethnic: [null, [Validators.required]],
      identityNo: [null, [Validators.required]],
      issuedDate: [null, []],
      issuedPlace: [null, [Validators.required]],
      permanentAddress: [null, [Validators.required]],
      permanentProvinceCode: [null, [Validators.required]],
      permanentDistrictCode: [null, [Validators.required]],
      permanentWardCode: [null, [Validators.required]],
      contactAddress: [null, [Validators.required]],
      contactProvinceCode: [null, [Validators.required]],
      contactDistrictCode: [null, [Validators.required]],
      contactWardCode: [null, [Validators.required]],
      joinedDate: [null, [Validators.required]],
      officialJoinedDate: [null, [Validators.required]],
      leavingDate: [null, []],
      unit: [null, [Validators.required]],
      jobTitle: [null, []],
      contractId: [null, [Validators.required]],
      contractType: [null, []],
      signedDate: [null, [Validators.required]],
      insuredSubject: [null, [Validators.required]],
      regionId: [null, [Validators.required]],
      insuranceUnitId: [null, [Validators.required]],
      socialInsuranceNo: [null, [Validators.required]],
      heathInsuranceNo: [null, [Validators.required]],
      joined: [null, [Validators.required]],
      tk1Files: [[], [Validators.required]],
      householdRegistrationBook: [null, [Validators.required]],
      socialJoined: [false, [Validators.required]],
      healthJoined: [false, [Validators.required]],
      unemploymentJoined: [false, [Validators.required]],
      accidentJoined: [false, [Validators.required]],
      laborUnionJoined: [false, [Validators.required]],
      fileType: [null, []],
      fileNo: [null, []],
      householdRelationship: [null, []],
      tplId: [null, [Validators.required]],
      fromDate: [null, [Validators.required]],
      toDate: [null, [Validators.required]],
      description: [null, [Validators.maxLength(250)]],
    });
  }

  override convertBody(body: any) {
    [
      'fromDate',
      'toDate',
      'dob',
      'issuedDate',
      'joinedDate',
      'officialJoinedDate',
      'signedDate',
    ].forEach((key) => {
      body[key] = body[key] && DateTimeUtil.formatDate(body[key]);
    });
    return body;
  }

  getTreatmentPlaces() {
    this.treatmentPlaceService
      .getOptions()
      .subscribe((options) => (this.treatmentPlaceOptions = options));
    this.treatmentPlaceService
      .getListCommon('REGION')
      .subscribe((options) => (this.regionOptions = options));
  }

  getCatalogs() {
    // this.isLoading = true;
    const keys = [
      Constant.CATALOGS.GIOI_TINH,
      Constant.CATALOGS.QUOC_GIA,
      Constant.CATALOGS.DAN_TOC,
      Constant.CATALOGS.TINH,
      Constant.CATALOGS.MOI_QUAN_HE_NT,
    ];
    forkJoin(keys.map((key) => this.staffInfoService.getCatalog(key)))
      .pipe(finalize(() => (this.isLoading = false)))
      .subscribe((catalogs) => {
        keys.forEach(
          (key, index) => (this.catalogs[key] = catalogs[index].data)
        );
      });
  }

  onChangeProvince(key: string, code: number) {
    this.catalogs[this.getCatalogKey(Constant.CATALOGS.HUYEN, key)] = [];
    this.catalogs[this.getCatalogKey(Constant.CATALOGS.XA, key)] = [];
    this.form.get(`${key}DistrictCode`)?.reset();
    this.form.get(`${key}WardCode`)?.reset();
    if (!code) return;
    this.staffInfoService
      .getCatalog(Constant.CATALOGS.HUYEN, code)
      .subscribe(
        (res) =>
          (this.catalogs[this.getCatalogKey(Constant.CATALOGS.HUYEN, key)] =
            res.data)
      );
  }

  onChangeDistrict(key: string, code: number) {
    this.catalogs[this.getCatalogKey(Constant.CATALOGS.XA, key)] = [];
    this.form.get(`${key}WardCode`)?.reset();
    if (!code) return;
    this.staffInfoService
      .getCatalog(Constant.CATALOGS.XA, code)
      .subscribe(
        (res) =>
          (this.catalogs[this.getCatalogKey(Constant.CATALOGS.XA, key)] =
            res.data)
      );
  }

  getCatalogKey(catalog: string, key: string) {
    return `${catalog}_${key}`;
  }
}
