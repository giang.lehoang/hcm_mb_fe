import { Component, Injector, OnInit } from '@angular/core';
import {
  IMbOption,
  insuranceRateSubjectEmployeeOptions,
  IParticipationInformation,
  participationInformationStatusOptions,
} from '@hcm-mfe/insurance-management/data-access/models';
import { ParticipationInformationService } from '@hcm-mfe/insurance-management/data-access/services';
import {
  BaseListComponent,
  DateTimeUtil,
  InsuranceFuncCode,
  ObjectUtil,
} from '@hcm-mfe/insurance-management/helper';
import { Constant, HTTP_STATUS_CODE } from '@hcm-mfe/shared/common/constants';
import { StaffInfoService } from '@hcm-mfe/shared/core';
import { finalize, forkJoin } from 'rxjs';

@Component({
  selector: 'app-participation-information',
  templateUrl: './participation-information.component.html',
  styleUrls: ['./participation-information.component.scss'],
})
export class ParticipationInformationComponent
  extends BaseListComponent<IParticipationInformation>
  implements OnInit
{
  CATALOGS = Constant.CATALOGS;
  catalogs: { [key: string]: IMbOption[] } = {};
  subjectEmployeeOptions = insuranceRateSubjectEmployeeOptions();
  subjectEmployeeDict = ObjectUtil.optionsToDictLabel(
    this.subjectEmployeeOptions
  );
  joinedDict = ObjectUtil.optionsToDictLabel(
    participationInformationStatusOptions()
  );

  constructor(
    injector: Injector,
    private readonly participationInformationService: ParticipationInformationService,
    private staffInfoService: StaffInfoService
  ) {
    super(injector);
    this.form = this.fb.group({
      staff: [null],
      regionId: [null],
      unit: [null],
      insuredSubject: [null],
      flagStatus: [null],
      employeeType: [null],
    });
    this.objFunction = this.sessionService.getSessionData(
      `FUNCTION_${InsuranceFuncCode.PARTICIPATION_INFORMATION}`
    );
    this.deleteApi = (id: number) =>
      this.participationInformationService.deleteItem(id);
  }

  override ngOnInit(): void {
    super.ngOnInit();
    this.getCatalogs();
  }

  override search(page?: number) {
    super.search(page);
    if (this.params.fromDateTo) {
      this.params.fromDateTo = DateTimeUtil.formatDate(this.params.fromDateTo);
    }
    forkJoin([
      this.participationInformationService.getList(this.params),
      this.staffInfoService.getCatalog(Constant.CATALOGS.GIOI_TINH),
    ])
      .pipe(finalize(() => (this.isLoading = false)))
      .subscribe(
        ([res, gender]) => {
          if (res.status === HTTP_STATUS_CODE.OK) {
            const genderDict = ObjectUtil.optionsToDictLabel(gender.data);
            res.data.content.forEach((item: IParticipationInformation) => {
              item.genderText = genderDict[item.gender];
              item.insuredSubjectText =
                this.subjectEmployeeDict[item.insuredSubject];
              item.joinedText = this.joinedDict[item.joined];
            });
            this.handleRes(res);
          }
        },
        () => {
          this.translate.instant('common.notification.error');
        }
      );
  }

  getCatalogs() {
    // this.isLoading = true;
    const keys = [Constant.CATALOGS.TINH];
    forkJoin(keys.map((key) => this.staffInfoService.getCatalog(key)))
      .pipe(finalize(() => (this.isLoading = false)))
      .subscribe((catalogs) => {
        keys.forEach(
          (key, index) => (this.catalogs[key] = catalogs[index].data)
        );
      });
  }

  override setHeaders() {
    this.tableConfig.headers = [
      {
        title: 'insurance.participationInformation.label.staffCode',
        field: 'staffCode',
        width: 70,
      },
      {
        title: 'insurance.participationInformation.label.fullName',
        field: 'fullName',
        width: 70,
      },
      {
        title: 'insurance.participationInformation.label.dob',
        field: 'dob',
        width: 70,
        pipe: 'date',
      },
      {
        title: 'insurance.participationInformation.label.gender',
        field: 'genderText',
        width: 70,
      },
      {
        title: 'insurance.participationInformation.label.jobTitle',
        field: 'jobTitle',
        width: 70,
      },
      {
        title: 'insurance.participationInformation.label.unit',
        field: 'unit',
        width: 70,
      },
      {
        title: 'insurance.participationInformation.label.officialJoinedDate',
        field: 'officialJoinedDate',
        width: 70,
        pipe: 'date',
      },
      {
        title: 'insurance.participationInformation.label.signedDate',
        field: 'signedDate',
        width: 70,
        pipe: 'date',
      },
      {
        title: 'insurance.participationInformation.label.signedDate',
        field: 'signedDate',
        width: 70,
        pipe: 'date',
      },
      {
        title: 'insurance.participationInformation.label.insuredSubject',
        field: 'insuredSubjectText',
        width: 70,
      },
      {
        title: 'insurance.participationInformation.label.identityNo',
        field: 'identityNo',
        width: 70,
      },
      {
        title: 'insurance.participationInformation.label.joined',
        field: 'joinedText',
        width: 70,
      },
      {
        title: 'insurance.participationInformation.label.employeeType',
        field: 'code',
        width: 70,
      },
      {
        title: '',
        field: 'action',
        tdClassList: ['text-nowrap', 'text-center'],
        thClassList: ['text-nowrap', 'text-center'],
        width: 60,
        tdTemplate: this.actionTpl,
        fixed: true,
        fixedDir: 'right',
      },
    ];
  }
}
