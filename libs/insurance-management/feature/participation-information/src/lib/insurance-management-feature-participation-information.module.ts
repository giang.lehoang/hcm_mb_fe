import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { InsuranceManagementPipesTranslateModule } from '@hcm-mfe/insurance-management/pipes/translate';
import { SharedUiExtendFormItemModule } from '@hcm-mfe/shared/ui/extend-form-item';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { SharedUiMbButtonIconModule } from '@hcm-mfe/shared/ui/mb-button-icon';
import { SharedUiMbDatePickerModule } from '@hcm-mfe/shared/ui/mb-date-picker';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { SharedUiMbTableModule } from '@hcm-mfe/shared/ui/mb-table';
import { SharedUiMbTableWrapModule } from '@hcm-mfe/shared/ui/mb-table-wrap';
import { SharedUiMbUploadModule } from '@hcm-mfe/shared/ui/mb-upload';
import { TranslateModule } from '@ngx-translate/core';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { components } from './components';
import {
  pages,
  ParticipationInformationComponent,
  ParticipationInformationFormComponent,
} from './pages';
import { InsuranceManagementUiUploadFilesModule } from '@hcm-mfe/insurance-management/ui/upload-files';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: ParticipationInformationComponent,
      },
      {
        path: 'create',
        component: ParticipationInformationFormComponent,
        data: {
          breadcrumb: 'insurance.participationInformation.breadcrumb.create',
          pageName: 'insurance.participationInformation.breadcrumb.create',
        },
      },
      {
        path: 'update',
        component: ParticipationInformationFormComponent,
        data: {
          breadcrumb: 'insurance.participationInformation.breadcrumb.update',
          pageName: 'insurance.participationInformation.breadcrumb.update',
        },
      },
    ]),
    SharedUiLoadingModule,
    SharedUiMbButtonModule,
    TranslateModule,
    ReactiveFormsModule,
    SharedUiMbSelectModule,
    SharedUiMbInputTextModule,
    SharedUiMbDatePickerModule,
    NzTagModule,
    SharedUiMbButtonIconModule,
    NzGridModule,
    NzCheckboxModule,
    FormsModule,
    SharedUiMbTableModule,
    SharedUiMbTableWrapModule,
    InsuranceManagementPipesTranslateModule,
    SharedUiExtendFormItemModule,
    SharedUiMbUploadModule,
    InsuranceManagementUiUploadFilesModule,
  ],
  declarations: [...pages, ...components],
})
export class InsuranceManagementFeatureParticipationInformationModule {}
