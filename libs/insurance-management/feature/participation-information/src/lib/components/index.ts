import { RelativeInformationComponent } from './relative-information/relative-information.component';
import { RelativeInformationFormComponent } from './relative-information-form/relative-information-form.component';

export const components = [
  RelativeInformationComponent,
  RelativeInformationFormComponent,
];

export * from './relative-information/relative-information.component';
export * from './relative-information-form/relative-information-form.component';
