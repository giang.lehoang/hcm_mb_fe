import { Component, Injector, Input, OnInit } from '@angular/core';
import {
  IMbOption,
  IRelativeInformation,
} from '@hcm-mfe/insurance-management/data-access/models';
import { RelativeInformationService } from '@hcm-mfe/insurance-management/data-access/services';
import {
  BaseListComponent,
  InsuranceFuncCode,
  ObjectUtil,
} from '@hcm-mfe/insurance-management/helper';
import { Constant, HTTP_STATUS_CODE } from '@hcm-mfe/shared/common/constants';
import { StaffInfoService } from '@hcm-mfe/shared/core';
import { finalize, forkJoin, tap } from 'rxjs';
import { RelativeInformationFormComponent } from '../relative-information-form/relative-information-form.component';

@Component({
  selector: 'app-relative-information',
  templateUrl: './relative-information.component.html',
  styleUrls: ['./relative-information.component.scss'],
})
export class RelativeInformationComponent
  extends BaseListComponent<IRelativeInformation>
  implements OnInit
{
  @Input() insId!: number;
  catalogs: { [key: string]: IMbOption[] } = {};
  catalogsDict: any = {};
  constructor(
    injector: Injector,
    private readonly relativeInformationService: RelativeInformationService,
    private staffInfoService: StaffInfoService
  ) {
    super(injector);
    this.form = this.fb.group({
      searchText: [null],
      flagStatus: [null],
    });
    this.objFunction = this.sessionService.getSessionData(
      `FUNCTION_${InsuranceFuncCode.RELATIVE_INFORMATION}`
    );
    this.formConfig = {
      title: 'insurance.filterCriteria.formTitle',
      content: RelativeInformationFormComponent,
    };
    this.deleteApi = (id: number) =>
      this.relativeInformationService.deleteItem(id);
  }

  override async search(page?: number) {
    if (!this.insId) return;
    super.search(page);
    forkJoin([
      this.relativeInformationService.getList({
        ...this.params,
        insId: this.insId,
      }),
      this.getCatalogs(),
    ])
      .pipe(finalize(() => (this.isLoading = false)))
      .subscribe(
        ([res, _]) => {
          if (res.status === HTTP_STATUS_CODE.OK) {
            res.data.content.forEach((item: IRelativeInformation) => {
              item.genderText =
                this.catalogsDict[Constant.CATALOGS.GIOI_TINH][item.gender];
              item.relationshipText =
                this.catalogsDict[Constant.CATALOGS.MOI_QUAN_HE_NT][
                  item.relationship
                ];
              item.householdRelationshipText =
                this.catalogsDict[Constant.CATALOGS.MOI_QUAN_HE_NT][
                  item.householdRelationship
                ];
              item.nationalityText =
                this.catalogsDict[Constant.CATALOGS.QUOC_GIA][item.nationality];
              item.ethnicText =
                this.catalogsDict[Constant.CATALOGS.DAN_TOC][item.ethnic];
            });
            this.handleRes(res);
          }
        },
        () => {
          this.translate.instant('common.notification.error');
        }
      );
  }

  get data(): any {
    return { insId: this.insId };
  }

  getCatalogs() {
    const keys = [
      Constant.CATALOGS.GIOI_TINH,
      Constant.CATALOGS.QUOC_GIA,
      Constant.CATALOGS.DAN_TOC,
      Constant.CATALOGS.TINH,
      Constant.CATALOGS.MOI_QUAN_HE_NT,
    ];
    return forkJoin(
      keys.map((key) => this.staffInfoService.getCatalog(key))
    ).pipe(
      tap((catalogs) => {
        keys.forEach((key, index) => {
          this.catalogs[key] = catalogs[index].data;
          this.catalogsDict[key] = ObjectUtil.optionsToDictLabel(
            this.catalogs[key]
          );
        });
      })
    );
  }

  override setHeaders() {
    this.tableConfig.headers = [
      {
        title: 'insurance.relativeInformation.label.fullName',
        field: 'fullName',
        width: 70,
      },
      {
        title: 'insurance.relativeInformation.label.relationship',
        field: 'relationshipText',
        width: 70,
      },
      {
        title: 'insurance.relativeInformation.label.householdRelationship',
        field: 'householdRelationshipText',
        width: 70,
      },
      {
        title: 'insurance.relativeInformation.label.dob',
        field: 'dob',
        width: 70,
        pipe: 'date',
      },
      {
        title: 'insurance.relativeInformation.label.gender',
        field: 'genderText',
        width: 70,
      },
      {
        title: 'insurance.relativeInformation.label.ethnic',
        field: 'ethnicText',
        width: 70,
      },
      {
        title: 'insurance.relativeInformation.label.identityNo',
        field: 'identityNo',
        width: 70,
      },
      {
        title: 'insurance.relativeInformation.label.taxCode',
        field: 'taxCode',
        width: 70,
      },
      {
        title: 'insurance.relativeInformation.label.nationality',
        field: 'nationalityText',
        width: 70,
      },
      {
        title: 'insurance.relativeInformation.label.permanentAddress',
        field: 'permanentAddress',
        width: 70,
      },
      {
        title: '',
        field: 'action',
        tdClassList: ['text-nowrap', 'text-center'],
        thClassList: ['text-nowrap', 'text-center'],
        width: 60,
        tdTemplate: this.actionTpl,
        fixed: true,
        fixedDir: 'right',
      },
    ];
  }
}
