import { Component, Injector, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import {
  IMbOption,
  IRelativeInformation,
} from '@hcm-mfe/insurance-management/data-access/models';
import { RelativeInformationService } from '@hcm-mfe/insurance-management/data-access/services';
import {
  BaseFormComponent,
  DateTimeUtil,
} from '@hcm-mfe/insurance-management/helper';
import { Constant } from '@hcm-mfe/shared/common/constants';
import { StaffInfoService } from '@hcm-mfe/shared/core';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { finalize, forkJoin } from 'rxjs';

@Component({
  selector: 'app-relative-information-form',
  templateUrl: './relative-information-form.component.html',
  styleUrls: ['./relative-information-form.component.scss'],
})
export class RelativeInformationFormComponent
  extends BaseFormComponent<IRelativeInformation>
  implements OnInit
{
  CATALOGS = Constant.CATALOGS;
  catalogs: { [key: string]: IMbOption[] } = {};
  constructor(
    injector: Injector,
    public override modalRef: NzModalRef,
    private readonly relativeInformationService: RelativeInformationService,
    private staffInfoService: StaffInfoService
  ) {
    super(injector);
    this.createApi = (body: IRelativeInformation) =>
      this.relativeInformationService.create(this.convertBody(body));
    this.updateApi = (id: number, body: IRelativeInformation) =>
      this.relativeInformationService.edit(id, this.convertBody(body));
  }

  override ngOnInit(): void {
    super.ngOnInit();
    setTimeout(() => {
      this.patchValueInfo();
    }, 0);
    this.getCatalogs();
  }

  override initForm() {
    this.form = this.fb.group({
      insId: [null],
      fullName: [null, [Validators.required]],
      relationship: [null],
      householdRelationship: [null, [Validators.required]],
      dob: [null, [Validators.required]],
      gender: [null, [Validators.required]],
      nationality: [null, [Validators.required]],
      ethnic: [null, [Validators.required]],
      identityNo: [null],
      taxCode: [null],
      permanentAddress: [
        null,
        [Validators.required, Validators.maxLength(200)],
      ],
      permanentProvinceCode: [
        null,
        [Validators.required, Validators.maxLength(50)],
      ],
      permanentDistrictCode: [
        null,
        [Validators.required, Validators.maxLength(50)],
      ],
      permanentWardCode: [
        null,
        [Validators.required, Validators.maxLength(50)],
      ],
    });
  }

  getCatalogs() {
    // this.isLoading = true;
    const keys = [
      Constant.CATALOGS.GIOI_TINH,
      Constant.CATALOGS.QUOC_GIA,
      Constant.CATALOGS.DAN_TOC,
      Constant.CATALOGS.TINH,
      Constant.CATALOGS.MOI_QUAN_HE_NT,
    ];
    forkJoin(keys.map((key) => this.staffInfoService.getCatalog(key)))
      .pipe(finalize(() => (this.isLoading = false)))
      .subscribe((catalogs) => {
        keys.forEach(
          (key, index) => (this.catalogs[key] = catalogs[index].data)
        );
      });
  }

  onChangeProvince(code: any) {
    this.catalogs[Constant.CATALOGS.HUYEN] = [];
    this.catalogs[Constant.CATALOGS.XA] = [];
    this.form.get('permanentDistrictCode')?.reset();
    this.form.get('permanentWardCode')?.reset();
    if (!code) return;
    this.staffInfoService
      .getCatalog(Constant.CATALOGS.HUYEN, code)
      .subscribe((res) => (this.catalogs[Constant.CATALOGS.HUYEN] = res.data));
  }

  onChangeDistrict(code: any) {
    this.catalogs[Constant.CATALOGS.XA] = [];
    this.form.get('permanentWardCode')?.reset();
    if (!code) return;
    this.staffInfoService
      .getCatalog(Constant.CATALOGS.XA, code)
      .subscribe((res) => (this.catalogs[Constant.CATALOGS.XA] = res.data));
  }

  override convertBody(body: any) {
    return { ...body, dob: DateTimeUtil.formatDate(body.dob) };
  }
}
