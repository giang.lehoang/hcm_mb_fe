import { Component, Injector, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import {
  IMbOption,
  IVolatilityPlan,
} from '@hcm-mfe/insurance-management/data-access/models';
import { VolatilityPlanService } from '@hcm-mfe/insurance-management/data-access/services';
import { BaseFormComponent } from '@hcm-mfe/insurance-management/helper';
import { NzModalRef } from 'ng-zorro-antd/modal';

@Component({
  selector: 'hcm-mfe-form-volatility-plan',
  templateUrl: './form-volatility-plan.component.html',
})
export class FormVolatilityPlanComponent
  extends BaseFormComponent<IVolatilityPlan>
  implements OnInit
{
  solutionOptions: IMbOption[] = [];
  informOptions: IMbOption[] = [];
  volatilityOptions: IMbOption[] = [];

  constructor(
    private readonly volatilityPlanService: VolatilityPlanService,
    modalRef: NzModalRef,
    injector: Injector
  ) {
    super(injector);
    this.modalRef = modalRef;
    this.createApi = (body: IVolatilityPlan) =>
      this.volatilityPlanService.create(this.convertBody(body));
    this.updateApi = (id: number, body: IVolatilityPlan) =>
      this.volatilityPlanService.edit(id, this.convertBody(body));
  }

  override ngOnInit(): void {
    super.ngOnInit();
    this.getListCommon();
  }

  override initForm() {
    this.form = this.fb.group({
      volatilityId: [null, [Validators.required]],
      informIds: [[], [Validators.required]],
      volatilitySolutionIds: [[], [Validators.required]],
      fromDate: [
        { value: null, disabled: Boolean(this.data) },
        [Validators.required],
      ],
      toDate: [{ value: null, disabled: true }],
      expirationDate: [{ value: null, disabled: true }],
      description: [null, [Validators.maxLength(250)]],
      flagStatus: [1, [Validators.required]],
    });
  }

  getListCommon() {
    this.volatilityPlanService
      .getListCommon('VOLATILITY_SOLUTION')
      .subscribe((options) => {
        this.solutionOptions = options;
      });
    this.volatilityPlanService.getListCommon('INFORM').subscribe((options) => {
      this.informOptions = options;
    });
    this.volatilityPlanService
      .getListCommon('VOLATILITY')
      .subscribe((options) => {
        this.volatilityOptions = options;
      });
  }
}
