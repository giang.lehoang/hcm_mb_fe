import {
  Component,
  Injector,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import {
  IMbOption,
  IVolatilityPlan,
} from '@hcm-mfe/insurance-management/data-access/models';
import { VolatilityPlanService } from '@hcm-mfe/insurance-management/data-access/services';
import {
  BaseListComponent,
  InsuranceFuncCode,
  ObjectUtil,
} from '@hcm-mfe/insurance-management/helper';
import { HTTP_STATUS_CODE } from '@hcm-mfe/shared/common/constants';
import { finalize, forkJoin } from 'rxjs';
import { FormVolatilityPlanComponent } from '../../components';

@Component({
  selector: 'hcm-mfe-volatility-plan',
  templateUrl: './volatility-plan.component.html',
  styleUrls: ['./volatility-plan.component.scss'],
})
export class VolatilityPlanComponent
  extends BaseListComponent<IVolatilityPlan>
  implements OnInit
{
  constructor(
    injector: Injector,
    private readonly volatilityPlanService: VolatilityPlanService
  ) {
    super(injector);
    this.form = this.fb.group({
      volatilityIds: [null],
      name: [null],
      flagStatus: [null],
    });
    this.objFunction = this.sessionService.getSessionData(
      `FUNCTION_${InsuranceFuncCode.VOLATILITY}`
    );
    this.formConfig = {
      title: 'insurance.volatilityPlan.formTitle',
      content: FormVolatilityPlanComponent,
    };
    this.deleteApi = (id: number) => this.volatilityPlanService.deleteItem(id);
  }

  volatilityOptions: IMbOption[] = [];
  @ViewChild('informNamesTpl', { static: true })
  informNamesTpl!: TemplateRef<any>;
  @ViewChild('volatilitySolutionNamesTpl', { static: true })
  volatilitySolutionNamesTpl!: TemplateRef<any>;

  override ngOnInit(): void {
    super.ngOnInit();
    this.getListCommon();
  }

  override search(page?: number) {
    super.search(page);
    forkJoin([
      this.volatilityPlanService.getList(this.params),
      this.volatilityPlanService.getListCommon(
        'VOLATILITY_SOLUTION,INFORM,VOLATILITY'
      ),
    ])
      .pipe(finalize(() => (this.isLoading = false)))
      .subscribe(
        ([res, options]) => {
          if (res.status === HTTP_STATUS_CODE.OK) {
            const dict = ObjectUtil.optionsToDictLabel(options);
            res.data.content.forEach((item: IVolatilityPlan) => {
              item.volatilitySolutionNames = item.volatilitySolutionIds.map(
                (key: number) => dict[key]
              );
              item.informNames = item.informIds.map((key: number) => dict[key]);
              item.volatilityName = dict[item.volatilityId];
            });
            this.handleRes(res);
          }
        },
        () => {
          this.translate.instant('common.notification.error');
        }
      );
  }

  getListCommon() {
    this.volatilityPlanService
      .getListCommon('VOLATILITY')
      .subscribe((options) => {
        this.volatilityOptions = options;
      });
  }

  override setHeaders() {
    this.tableConfig.headers = [
      {
        title: 'insurance.volatilityPlan.label.volatility',
        field: 'volatilityName',
        width: 50,
      },
      {
        title: 'insurance.volatilityPlan.label.inform',
        field: 'informNames',
        width: 50,
        tdTemplate: this.informNamesTpl,
      },
      {
        title: 'insurance.volatilityPlan.label.volatilitySolution',
        field: 'volatilitySolutionNames',
        width: 100,
        tdTemplate: this.volatilitySolutionNamesTpl,
      },
      {
        title: 'insurance.common.label.fromDate',
        field: 'fromDate',
        pipe: 'date',
        width: 50,
      },
      {
        title: 'insurance.common.label.toDate',
        field: 'toDate',
        pipe: 'date',
        width: 50,
      },
      {
        title: 'insurance.common.label.description',
        field: 'description',
        width: 50,
      },
      {
        title: 'insurance.common.label.flagStatus',
        thClassList: ['text-center'],
        tdClassList: ['text-center'],
        width: 50,
        tdTemplate: this.flagStatusTpl,
      },
      {
        title: '',
        field: 'action',
        tdClassList: ['text-nowrap', 'text-center'],
        thClassList: ['text-nowrap', 'text-center'],
        width: 60,
        tdTemplate: this.actionTpl,
        fixed: true,
        fixedDir: 'right',
      },
    ];
  }
}
