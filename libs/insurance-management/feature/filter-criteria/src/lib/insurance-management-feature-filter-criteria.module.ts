import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { InsuranceManagementPipesTranslateModule } from '@hcm-mfe/insurance-management/pipes/translate';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { SharedUiMbButtonIconModule } from '@hcm-mfe/shared/ui/mb-button-icon';
import { SharedUiMbDatePickerModule } from '@hcm-mfe/shared/ui/mb-date-picker';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { SharedUiMbTableModule } from '@hcm-mfe/shared/ui/mb-table';
import { SharedUiMbTableWrapModule } from '@hcm-mfe/shared/ui/mb-table-wrap';
import { TranslateModule } from '@ngx-translate/core';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { FilterCriteriaComponent } from './filter-criteria/filter-criteria.component';
import { FilterCriteriaFormComponent } from './filter-criteria-form/filter-criteria-form.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: FilterCriteriaComponent,
      },
    ]),
    SharedUiLoadingModule,
    SharedUiMbButtonModule,
    TranslateModule,
    ReactiveFormsModule,
    SharedUiMbSelectModule,
    SharedUiMbInputTextModule,
    SharedUiMbDatePickerModule,
    NzTagModule,
    SharedUiMbButtonIconModule,
    NzGridModule,
    FormsModule,
    SharedUiMbTableModule,
    SharedUiMbTableWrapModule,
    InsuranceManagementPipesTranslateModule,
  ],
  declarations: [FilterCriteriaComponent, FilterCriteriaFormComponent],
})
export class InsuranceManagementFeatureFilterCriteriaModule {}
