import { Component, Injector, OnInit } from '@angular/core';
import {
  filterCriteriaDataTypesOptions,
  filterCriteriaSourceDataTypesOptions,
  IFilterCriteria,
} from '@hcm-mfe/insurance-management/data-access/models';
import { FilterCriteriaService } from '@hcm-mfe/insurance-management/data-access/services';
import {
  BaseListComponent,
  DateTimeUtil,
  InsuranceFuncCode,
  ObjectUtil,
} from '@hcm-mfe/insurance-management/helper';
import { HTTP_STATUS_CODE } from '@hcm-mfe/shared/common/constants';
import { finalize } from 'rxjs';
import { FilterCriteriaFormComponent } from '../filter-criteria-form/filter-criteria-form.component';

@Component({
  selector: 'app-filter-criteria',
  templateUrl: './filter-criteria.component.html',
  styleUrls: ['./filter-criteria.component.scss'],
})
export class FilterCriteriaComponent
  extends BaseListComponent<IFilterCriteria>
  implements OnInit
{
  constructor(
    injector: Injector,
    private readonly filterCriteriaService: FilterCriteriaService
  ) {
    super(injector);
    this.form = this.fb.group({
      searchText: [null],
      flagStatus: [null],
    });
    this.objFunction = this.sessionService.getSessionData(
      `FUNCTION_${InsuranceFuncCode.FILTER_CRITERIA}`
    );
    this.formConfig = {
      title: 'insurance.filterCriteria.formTitle',
      content: FilterCriteriaFormComponent,
    };
    this.deleteApi = (id: number) => this.filterCriteriaService.deleteItem(id);
  }

  dataTypesOptions = filterCriteriaDataTypesOptions();
  sourceDataTypesOptions = filterCriteriaSourceDataTypesOptions();
  dataTypesDict = ObjectUtil.optionsToDictLabel(this.dataTypesOptions);
  sourceDataTypesDict = ObjectUtil.optionsToDictLabel(
    this.sourceDataTypesOptions
  );

  override search(page?: number) {
    super.search(page);
    if (this.params.fromDateTo) {
      this.params.fromDateTo = DateTimeUtil.formatDate(this.params.fromDateTo);
    }
    this.filterCriteriaService
      .getList(this.params)
      .pipe(finalize(() => (this.isLoading = false)))
      .subscribe(
        (res) => {
          if (res.status === HTTP_STATUS_CODE.OK) {
            res.data.content.forEach((item: IFilterCriteria) => {
              item.dataTypeName = this.dataTypesDict[item.dataType];
              item.dataSourceTypeName =
                this.sourceDataTypesDict[item.dataSourceType];
            });
            this.handleRes(res);
          }
        },
        () => {
          this.translate.instant('common.notification.error');
        }
      );
  }

  override setHeaders() {
    this.tableConfig.headers = [
      {
        title: 'insurance.filterCriteria.label.code',
        field: 'code',
        width: 70,
      },
      {
        title: 'insurance.filterCriteria.label.name',
        field: 'name',
        width: 70,
        thClassList: ['text-center'],
      },
      {
        title: 'insurance.filterCriteria.label.dataType',
        field: 'dataTypeName',
        width: 50,
      },
      {
        title: 'insurance.filterCriteria.label.dataSourceType',
        field: 'dataSourceTypeName',
        width: 70,
      },
      {
        title: 'insurance.filterCriteria.label.dataSource',
        field: 'dataSource',
        width: 60,
      },
      {
        title: 'insurance.filterCriteria.label.description',
        field: 'description',
        width: 60,
      },
      {
        title: 'insurance.common.label.flagStatus',
        thClassList: ['text-center'],
        tdClassList: ['text-center'],
        width: 50,
        tdTemplate: this.flagStatusTpl,
      },
      {
        title: '',
        field: 'action',
        tdClassList: ['text-nowrap', 'text-center'],
        thClassList: ['text-nowrap', 'text-center'],
        width: 60,
        tdTemplate: this.actionTpl,
        fixed: true,
        fixedDir: 'right',
      },
    ];
  }
}
