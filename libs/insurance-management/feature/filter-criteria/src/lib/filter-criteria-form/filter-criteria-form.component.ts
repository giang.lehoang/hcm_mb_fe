import { Component, Injector, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import {
  FilterCriteriaDataTypes,
  filterCriteriaDataTypesOptions,
  filterCriteriaSourceDataTypesOptions,
  IFilterCriteria,
} from '@hcm-mfe/insurance-management/data-access/models';
import { FilterCriteriaService } from '@hcm-mfe/insurance-management/data-access/services';
import { BaseFormComponent } from '@hcm-mfe/insurance-management/helper';
import { NzModalRef } from 'ng-zorro-antd/modal';

@Component({
  selector: 'app-filter-criteria-form',
  templateUrl: './filter-criteria-form.component.html',
})
export class FilterCriteriaFormComponent
  extends BaseFormComponent<IFilterCriteria>
  implements OnInit
{
  dataTypesOptions = filterCriteriaDataTypesOptions();
  sourceDataTypesOptions = filterCriteriaSourceDataTypesOptions();
  isDataTypeList = false;
  labelRequired = `<span class='label__required'>*</span>`;

  constructor(
    private readonly filterCriteriaService: FilterCriteriaService,
    modalRef: NzModalRef,
    injector: Injector
  ) {
    super(injector);
    this.modalRef = modalRef;
    this.createApi = (body: IFilterCriteria) =>
      this.filterCriteriaService.create(this.convertBody(body));
    this.updateApi = (id: number, body: IFilterCriteria) =>
      this.filterCriteriaService.edit(id, this.convertBody(body));
  }

  override initForm() {
    this.form = this.fb.group({
      code: [null, [Validators.required, Validators.maxLength(50)]],
      name: [null, [Validators.required, Validators.maxLength(250)]],
      dataType: [null, [Validators.required]],
      dataSourceType: [null],
      dataSource: [null],
      dataIdField: [null],
      dataNameField: [null],
      description: [null, [Validators.maxLength(250)]],
      flagStatus: [1, [Validators.required]],
    });
  }

  onDataTypeChange(value: FilterCriteriaDataTypes) {
    const controls = [
      'dataSourceType',
      'dataSource',
      'dataIdField',
      'dataNameField',
    ].map((key) => this.form.get(key)!);
    this.isDataTypeList = value === FilterCriteriaDataTypes.LIST;
    if (this.isDataTypeList) {
      controls.forEach((control) =>
        control.setValidators([Validators.required])
      );
    } else {
      controls.forEach((control) => control.setValidators(null));
    }
    controls.forEach((control) => control.updateValueAndValidity());
  }
}
