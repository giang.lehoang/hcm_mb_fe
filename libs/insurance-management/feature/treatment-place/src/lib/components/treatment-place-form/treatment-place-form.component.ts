import { Component, Injector, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import {
  IMbOption,
  ITreatmentPlace,
  treatmentPlaceNewAcceptanceOptions,
} from '@hcm-mfe/insurance-management/data-access/models';
import { TreatmentPlaceService } from '@hcm-mfe/insurance-management/data-access/services';
import {
  BaseFormComponent,
  DateTimeUtil,
} from '@hcm-mfe/insurance-management/helper';
import { Constant } from '@hcm-mfe/shared/common/constants';
import { StaffInfoService } from '@hcm-mfe/shared/core';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { map } from 'rxjs';

@Component({
  selector: 'app-treatment-place-form',
  templateUrl: './treatment-place-form.component.html',
})
export class TreatmentPlaceFormComponent
  extends BaseFormComponent<ITreatmentPlace>
  implements OnInit
{
  locationOptions: IMbOption[] = [];
  newAcceptanceOptions = treatmentPlaceNewAcceptanceOptions();

  constructor(
    modalRef: NzModalRef,
    injector: Injector,
    private treatmentPlaceService: TreatmentPlaceService,
    private staffInfoService: StaffInfoService
  ) {
    super(injector);
    this.modalRef = modalRef;
    this.createApi = (body: ITreatmentPlace) =>
      this.treatmentPlaceService.create(this.convertBody(body));
    this.updateApi = (id: number, body: ITreatmentPlace) =>
      this.treatmentPlaceService.edit(id, this.convertBody(body));
  }

  override ngOnInit(): void {
    super.ngOnInit();
    if (this.data) {
      this.form.patchValue({
        unit: {
          orgId: this.data.unitId,
          orgName: this.data.unitName,
        },
      });
    }
    this.getListLocation();
  }

  override initForm() {
    this.form = this.fb.group({
      unit: [null, [Validators.required]],
      provinceCode: [null, [Validators.required]],
      code: [null, [Validators.required, Validators.maxLength(50)]],
      name: [null, [Validators.required, Validators.maxLength(100)]],
      newAcceptance: [true, [Validators.required]],
      fromDate: [
        { value: null, disabled: Boolean(this.data) },
        [Validators.required],
      ],
      toDate: [{ value: null, disabled: true }],
      description: [null, [Validators.maxLength(250)]],
      flagStatus: [1, [Validators.required]],
    });
  }

  getListLocation() {
    this.staffInfoService
      .getCatalog(Constant.CATALOGS.TINH)
      .pipe(map((res) => res.data))
      .subscribe((options) => {
        this.locationOptions = options;
      });
  }

  override convertBody(body: any) {
    return {
      ...body,
      fromDate: DateTimeUtil.formatDate(body.fromDate),
      unitId: body.unit?.orgId,
    };
  }
}
