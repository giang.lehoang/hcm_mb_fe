import {
  Component,
  Injector,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import {
  ITreatmentPlace,
  treatmentPlaceNewAcceptanceOptions,
} from '@hcm-mfe/insurance-management/data-access/models';
import { TreatmentPlaceService } from '@hcm-mfe/insurance-management/data-access/services';
import {
  BaseListComponent,
  DateTimeUtil,
  InsuranceFuncCode,
  ObjectUtil,
} from '@hcm-mfe/insurance-management/helper';
import { Constant, HTTP_STATUS_CODE } from '@hcm-mfe/shared/common/constants';
import { StaffInfoService } from '@hcm-mfe/shared/core';
import { finalize, forkJoin, map } from 'rxjs';
import { TreatmentPlaceFormComponent } from '../../components';

@Component({
  selector: 'hcm-mfe-treatment-place',
  templateUrl: './treatment-place.component.html',
  styleUrls: ['./treatment-place.component.scss'],
})
export class TreatmentPlaceComponent
  extends BaseListComponent<ITreatmentPlace>
  implements OnInit
{
  constructor(
    injector: Injector,
    private treatmentPlaceService: TreatmentPlaceService,
    private staffInfoService: StaffInfoService
  ) {
    super(injector);
    this.form = this.fb.group({
      newAcceptance: [null],
      unit: [null],
      fromDateTo: [null],
      flagStatus: [null],
    });
    this.objFunction = this.sessionService.getSessionData(
      `FUNCTION_${InsuranceFuncCode.TREATMENT_PLACE}`
    );
    this.formConfig = {
      title: 'insurance.treatmentPlace.formTitle',
      content: TreatmentPlaceFormComponent,
    };
    this.deleteApi = (id: number) => this.treatmentPlaceService.deleteItem(id);
  }

  newAcceptanceOptions = treatmentPlaceNewAcceptanceOptions();
  @ViewChild('newAcceptanceTpl', { static: true })
  newAcceptanceTpl!: TemplateRef<any>;

  override search(page?: number) {
    super.search(page);
    if (this.params.fromDateTo) {
      this.params.fromDateTo = DateTimeUtil.formatDate(this.params.fromDateTo);
    }
    forkJoin([
      this.treatmentPlaceService.getList({
        ...this.params,
        unitId: this.params.unit?.orgId,
      }),
      this.staffInfoService
        .getCatalog(Constant.CATALOGS.TINH)
        .pipe(map((res) => res.data)),
    ])
      .pipe(finalize(() => (this.isLoading = false)))
      .subscribe(
        ([res, options]) => {
          if (res.status === HTTP_STATUS_CODE.OK) {
            const dict = ObjectUtil.optionsToDictLabel(options);
            res.data.content.forEach((item: ITreatmentPlace) => {
              item.provinceName = dict[item.provinceCode];
            });
            this.handleRes(res);
          }
        },
        () => {
          this.translate.instant('common.notification.error');
        }
      );
  }

  override setHeaders() {
    this.tableConfig.headers = [
      {
        title: 'insurance.treatmentPlace.label.unit',
        field: 'unitName',
        width: 70,
      },
      {
        title: 'insurance.treatmentPlace.label.province',
        field: 'provinceName',
        width: 50,
      },
      {
        title: 'insurance.treatmentPlace.label.codeRegisterPlace',
        field: 'code',
        width: 40,
        thClassList: ['text-center'],
      },
      {
        title: 'insurance.treatmentPlace.label.nameRegisterPlace',
        field: 'name',
        width: 50,
      },
      {
        title: 'insurance.treatmentPlace.label.newAcceptance',
        field: 'newAcceptance',
        width: 60,
        thClassList: ['text-center'],
        tdTemplate: this.newAcceptanceTpl,
      },
      {
        title: 'insurance.common.label.fromDate',
        field: 'fromDate',
        pipe: 'date',
        width: 50,
      },
      {
        title: 'insurance.common.label.toDate',
        field: 'toDate',
        pipe: 'date',
        width: 50,
      },
      {
        title: 'insurance.common.label.description',
        field: 'description',
        width: 60,
      },
      {
        title: 'insurance.common.label.flagStatus',
        thClassList: ['text-center'],
        tdClassList: ['text-center'],
        width: 50,
        tdTemplate: this.flagStatusTpl,
      },
      {
        title: '',
        field: 'action',
        tdClassList: ['text-nowrap', 'text-center'],
        thClassList: ['text-nowrap', 'text-center'],
        width: 60,
        tdTemplate: this.actionTpl,
        fixed: true,
        fixedDir: 'right',
      },
    ];
  }
}
