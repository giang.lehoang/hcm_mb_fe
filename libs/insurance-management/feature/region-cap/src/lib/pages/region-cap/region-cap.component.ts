import { Component, Injector, OnInit } from '@angular/core';
import {
  IMbOption,
  IRegionCap,
} from '@hcm-mfe/insurance-management/data-access/models';
import { RegionCapService } from '@hcm-mfe/insurance-management/data-access/services';
import {
  BaseListComponent,
  DateTimeUtil,
  ObjectUtil,
  InsuranceFuncCode,
} from '@hcm-mfe/insurance-management/helper';
import { HTTP_STATUS_CODE } from '@hcm-mfe/shared/common/constants';
import { finalize, forkJoin } from 'rxjs';
import { RegionCapFormComponent } from '../../components';

@Component({
  selector: 'app-region-cap',
  templateUrl: './region-cap.component.html',
  styleUrls: ['./region-cap.component.scss'],
})
export class RegionCapComponent
  extends BaseListComponent<IRegionCap>
  implements OnInit
{
  constructor(injector: Injector, private regionCapService: RegionCapService) {
    super(injector);
    this.form = this.fb.group({
      regionIds: [null],
      fromDateTo: [null],
      flagStatus: [null],
    });
    this.objFunction = this.sessionService.getSessionData(
      `FUNCTION_${InsuranceFuncCode.REGION_CAP}`
    );
    this.formConfig = {
      title: 'insurance.regionCap.formTitle',
      content: RegionCapFormComponent,
    };
    this.deleteApi = (id: number) => this.regionCapService.deleteItem(id);
  }

  regionOptions: IMbOption[] = [];

  override ngOnInit(): void {
    super.ngOnInit();
    this.getListCommon();
  }

  override search(page?: number) {
    super.search(page);
    if (this.params.fromDateTo) {
      this.params.fromDateTo = DateTimeUtil.formatDate(this.params.fromDateTo);
    }
    forkJoin([
      this.regionCapService.getList(this.params),
      this.regionCapService.getListCommon('REGION,INSURANCE'),
    ])
      .pipe(finalize(() => (this.isLoading = false)))
      .subscribe(
        ([res, options]) => {
          if (res.status === HTTP_STATUS_CODE.OK) {
            const dict = ObjectUtil.optionsToDictLabel(options);
            res.data.content.forEach((item: IRegionCap) => {
              item.regionName = dict[item.regionId];
              item.insuranceName = dict[item.insuranceId];
            });
            this.handleRes(res);
          }
        },
        () => {
          this.translate.instant('common.notification.error');
        }
      );
  }

  getListCommon() {
    this.regionCapService.getListCommon('REGION').subscribe((options) => {
      this.regionOptions = options;
    });
  }

  override setHeaders() {
    this.tableConfig.headers = [
      {
        title: 'insurance.regionCap.label.region',
        field: 'regionName',
        width: 60,
      },
      {
        title: 'insurance.regionCap.label.minimumSalary',
        field: 'minimumSalary',
        pipe: 'currencyNumber',
        width: 60,
      },
      {
        title: 'insurance.regionCap.label.baseSalary',
        field: 'baseSalary',
        pipe: 'currencyNumber',
        width: 70,
      },
      {
        title: 'insurance.regionCap.label.insuranceMonth',
        field: 'insuranceMonth',
        width: 70,
      },
      {
        title: 'insurance.regionCap.label.insurance',
        field: 'insuranceName',
        width: 60,
      },
      {
        title: 'insurance.regionCap.label.maximumInsuranceSalary',
        field: 'maximumInsuranceSalary',
        pipe: 'currencyNumber',
        width: 60,
      },
      {
        title: 'insurance.common.label.fromDate',
        field: 'fromDate',
        pipe: 'date',
        width: 60,
      },
      {
        title: 'insurance.common.label.toDate',
        field: 'toDate',
        pipe: 'date',
        width: 60,
      },
      {
        title: 'insurance.common.label.description',
        field: 'description',
        width: 60,
      },
      {
        title: 'insurance.common.label.flagStatus',
        thClassList: ['text-center'],
        tdClassList: ['text-center'],
        width: 50,
        tdTemplate: this.flagStatusTpl,
      },
      {
        title: '',
        field: 'action',
        tdClassList: ['text-nowrap', 'text-center'],
        thClassList: ['text-nowrap', 'text-center'],
        width: 60,
        tdTemplate: this.actionTpl,
        fixed: true,
        fixedDir: 'right',
      },
    ];
  }
}
