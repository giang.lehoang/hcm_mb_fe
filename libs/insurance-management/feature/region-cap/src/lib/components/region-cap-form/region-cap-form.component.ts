import { Component, Injector, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import {
  IMbOption,
  IRegionCap,
} from '@hcm-mfe/insurance-management/data-access/models';
import { RegionCapService } from '@hcm-mfe/insurance-management/data-access/services';
import { BaseFormComponent } from '@hcm-mfe/insurance-management/helper';
import { NzModalRef } from 'ng-zorro-antd/modal';

@Component({
  selector: 'app-region-cap-form',
  templateUrl: './region-cap-form.component.html',
})
export class RegionCapFormComponent
  extends BaseFormComponent<IRegionCap>
  implements OnInit
{
  insuranceOptions: IMbOption[] = [];
  regionOptions: IMbOption[] = [];

  constructor(
    private readonly regionCapService: RegionCapService,
    modalRef: NzModalRef,
    injector: Injector
  ) {
    super(injector);
    this.modalRef = modalRef;
    this.createApi = (body: IRegionCap) =>
      this.regionCapService.create(this.convertBody(body));
    this.updateApi = (id: number, body: IRegionCap) =>
      this.regionCapService.edit(id, this.convertBody(body));
  }

  override ngOnInit(): void {
    super.ngOnInit();
    this.getListCommon();
  }

  override initForm() {
    this.form = this.fb.group({
      regionId: [null, [Validators.required]],
      insuranceId: [null, [Validators.required]],
      minimumSalary: [null, [Validators.required]],
      baseSalary: [null, [Validators.required]],
      insuranceMonth: [null, [Validators.required]],
      maximumInsuranceSalary: [null, [Validators.required]],
      fromDate: [
        { value: null, disabled: Boolean(this.data) },
        [Validators.required],
      ],
      toDate: [{ value: null, disabled: true }],
      description: [null, [Validators.maxLength(250)]],
      flagStatus: [1, [Validators.required]],
    });
  }

  getListCommon() {
    this.regionCapService.getListCommon('INSURANCE').subscribe((options) => {
      this.insuranceOptions = options;
    });
    this.regionCapService.getListCommon('REGION').subscribe((options) => {
      this.regionOptions = options;
    });
  }
}
