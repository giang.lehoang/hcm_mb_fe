import { Component, Injector, OnInit } from '@angular/core';
import {
  categoryTypes,
  ICategoryShareContent,
} from '@hcm-mfe/insurance-management/data-access/models';
import { CategoryShareService } from '@hcm-mfe/insurance-management/data-access/services';
import {
  BaseListComponent,
  ObjectUtil,
  InsuranceFuncCode,
} from '@hcm-mfe/insurance-management/helper';
import { HTTP_STATUS_CODE } from '@hcm-mfe/shared/common/constants';
import { finalize } from 'rxjs';
import { CategoryShareFormComponent } from '../../components';

@Component({
  selector: 'hcm-mfe-volatility-plan',
  templateUrl: './category-share.component.html',
  styleUrls: ['./category-share.component.scss'],
})
export class CategoryShareComponent
  extends BaseListComponent<ICategoryShareContent>
  implements OnInit
{
  categoryTypes = categoryTypes();
  typeDict = ObjectUtil.optionsToDictLabel(this.categoryTypes);

  constructor(
    injector: Injector,
    private readonly categoryShareService: CategoryShareService
  ) {
    super(injector);
    this.initFormSearch();
    this.deleteApi = (id: number) => this.categoryShareService.deleteItem(id);
    this.objFunction = this.sessionService.getSessionData(
      `FUNCTION_${InsuranceFuncCode.COMMON}`
    );
    this.formConfig = {
      title: 'insurance.categoryShare.formTitle',
      content: CategoryShareFormComponent,
    };
  }

  initFormSearch() {
    this.form = this.fb.group({
      types: [null],
      name: [null],
      flagStatus: [null],
    });
  }

  override search(page?: number) {
    super.search(page);
    this.categoryShareService
      .getList(this.params)
      .pipe(finalize(() => (this.isLoading = false)))
      .subscribe(
        (res) => {
          if (res.status === HTTP_STATUS_CODE.OK) {
            res.data.content.forEach((item: ICategoryShareContent) => {
              item.typeName = this.typeDict[item.type];
              item.nameOne = item.properties?.nameOne;
              item.nameTwo = item.properties?.nameTwo;
            });
            this.handleRes(res);
          }
        },
        () => {
          this.translate.instant('common.notification.error');
        }
      );
  }

  override setHeaders() {
    this.tableConfig.headers = [
      {
        title: 'insurance.categoryShare.label.params',
        field: 'typeName',
        width: 60,
      },
      {
        title: 'insurance.categoryShare.label.name',
        field: 'name',
        width: 60,
      },
      {
        title: 'insurance.categoryShare.label.nameOne',
        field: 'nameOne',
        width: 60,
      },
      {
        title: 'insurance.categoryShare.label.nameTwo',
        field: 'nameTwo',
        width: 60,
      },
      {
        title: 'insurance.common.label.fromDate',
        field: 'fromDate',
        pipe: 'date',
        width: 60,
      },
      {
        title: 'insurance.common.label.flagStatus',
        thClassList: ['text-center'],
        tdClassList: ['text-center'],
        width: 50,
        tdTemplate: this.flagStatusTpl,
      },
      {
        title: '',
        field: 'action',
        tdClassList: ['text-nowrap', 'text-center'],
        thClassList: ['text-nowrap', 'text-center'],
        width: 60,
        tdTemplate: this.actionTpl,
        fixed: true,
        fixedDir: 'right',
      },
    ];
  }
}
