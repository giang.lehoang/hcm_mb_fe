import { Component, Injector, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import {
  categoryTypes,
  ICategoryShareContent,
} from '@hcm-mfe/insurance-management/data-access/models';
import { CategoryShareService } from '@hcm-mfe/insurance-management/data-access/services';
import { BaseFormComponent } from '@hcm-mfe/insurance-management/helper';
import { NzModalRef } from 'ng-zorro-antd/modal';

@Component({
  selector: 'hcm-mfe-category-share-form',
  templateUrl: './category-share-form.component.html',
})
export class CategoryShareFormComponent
  extends BaseFormComponent<ICategoryShareContent>
  implements OnInit
{
  categoryTypes = categoryTypes();

  constructor(
    private readonly categoryShareService: CategoryShareService,
    modalRef: NzModalRef,
    injector: Injector
  ) {
    super(injector);
    this.modalRef = modalRef;
    this.createApi = (body: ICategoryShareContent) =>
      this.categoryShareService.create(this.convertBody(body));
    this.updateApi = (id: number, body: ICategoryShareContent) =>
      this.categoryShareService.edit(id, this.convertBody(body));
  }

  override initForm() {
    this.form = this.fb.group({
      type: [null, [Validators.required]],
      name: [null, [Validators.required, Validators.maxLength(50)]],
      properties: this.fb.group({
        nameOne: [null, [Validators.maxLength(100)]],
        nameTwo: [null, [Validators.maxLength(100)]],
      }),
      flagStatus: [1, [Validators.required]],
      fromDate: [
        { value: null, disabled: Boolean(this.data) },
        [Validators.required],
      ],
    });
  }
}
