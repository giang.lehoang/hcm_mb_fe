import { RouterModule, Routes } from '@angular/router';
import { RoleGuardService } from '@hcm-mfe/shared/core';
import { Scopes } from '@hcm-mfe/shared/common/enums';
import { NgModule } from '@angular/core';
import { InsuranceFuncCode } from '@hcm-mfe/insurance-management/helper';

const router: Routes = [
  {
    path: 'participation',
    data: {
      breadcrumb: 'insurance.participation.breadcrumb',
    },
    children: [
      {
        path: 'volatility-plan',
        canActivateChild: [RoleGuardService],
        data: {
          code: InsuranceFuncCode.VOLATILITY,
          scope: Scopes.VIEW,
          breadcrumb: 'insurance.volatilityPlan.breadcrumb',
          pageName: 'insurance.volatilityPlan.pageName',
        },
        children: [
          {
            path: '',
            loadChildren: () =>
              import(
                '@hcm-mfe/insurance-management/feature/volatility-plan'
              ).then((m) => m.InsuranceManagementFeatureVolatilityPlanModule),
          },
        ],
      },
      {
        path: 'participation-status',
        canActivateChild: [RoleGuardService],
        data: {
          code: InsuranceFuncCode.INSURANCE_STATUS,
          scope: Scopes.VIEW,
          breadcrumb: 'insurance.participationStatus.breadcrumb',
          pageName: 'insurance.participationStatus.pageName',
        },
        children: [
          {
            path: '',
            loadChildren: () =>
              import(
                '@hcm-mfe/insurance-management/feature/participation-status'
              ).then(
                (m) => m.InsuranceManagementFeatureParticipationStatusModule
              ),
          },
        ],
      },
      {
        path: 'insurance-rate',
        canActivateChild: [RoleGuardService],
        data: {
          code: InsuranceFuncCode.INSURANCE_RATE,
          scope: Scopes.VIEW,
          breadcrumb: 'insurance.insuranceRate.breadcrumb',
          pageName: 'insurance.insuranceRate.pageName',
        },
        children: [
          {
            path: '',
            loadChildren: () =>
              import(
                '@hcm-mfe/insurance-management/feature/insurance-rate'
              ).then((m) => m.InsuranceManagementFeatureInsuranceRateModule),
          },
        ],
      },
      {
        path: 'region-cap',
        canActivateChild: [RoleGuardService],
        data: {
          code: InsuranceFuncCode.REGION_CAP,
          scope: Scopes.VIEW,
          breadcrumb: 'insurance.regionCap.breadcrumb',
          pageName: 'insurance.regionCap.pageName',
        },
        children: [
          {
            path: '',
            loadChildren: () =>
              import('@hcm-mfe/insurance-management/feature/region-cap').then(
                (m) => m.InsuranceManagementFeatureRegionCapModule
              ),
          },
        ],
      },
      {
        path: 'treatment-place',
        canActivateChild: [RoleGuardService],
        data: {
          code: InsuranceFuncCode.TREATMENT_PLACE,
          scope: Scopes.VIEW,
          breadcrumb: 'insurance.treatmentPlace.breadcrumb',
          pageName: 'insurance.treatmentPlace.pageName',
        },
        children: [
          {
            path: '',
            loadChildren: () =>
              import(
                '@hcm-mfe/insurance-management/feature/treatment-place'
              ).then((m) => m.InsuranceManagementFeatureTreatmentPlaceModule),
          },
        ],
      },
      {
        path: 'category-share',
        canActivateChild: [RoleGuardService],
        data: {
          code: InsuranceFuncCode.COMMON,
          scope: Scopes.VIEW,
          breadcrumb: 'insurance.categoryShare.breadcrumb',
          pageName: 'insurance.categoryShare.pageName',
        },
        children: [
          {
            path: '',
            loadChildren: () =>
              import(
                '@hcm-mfe/insurance-management/feature/category-share'
              ).then((m) => m.InsuranceManagementFeatureCategoryShareModule),
          },
        ],
      },
      {
        path: 'filter-criteria',
        canActivateChild: [RoleGuardService],
        data: {
          code: InsuranceFuncCode.FILTER_CRITERIA,
          scope: Scopes.VIEW,
          breadcrumb: 'insurance.filterCriteria.breadcrumb',
          pageName: 'insurance.filterCriteria.pageName',
        },
        children: [
          {
            path: '',
            loadChildren: () =>
              import(
                '@hcm-mfe/insurance-management/feature/filter-criteria'
              ).then((m) => m.InsuranceManagementFeatureFilterCriteriaModule),
          },
        ],
      },
      {
        path: 'status-setting',
        canActivateChild: [RoleGuardService],
        data: {
          code: InsuranceFuncCode.STATUS_SETTING,
          scope: Scopes.VIEW,
          breadcrumb: 'insurance.statusSetting.breadcrumb',
          pageName: 'insurance.statusSetting.pageName',
        },
        children: [
          {
            path: '',
            loadChildren: () =>
              import(
                '@hcm-mfe/insurance-management/feature/status-setting'
              ).then((m) => m.InsuranceManagementFeatureStatusSettingModule),
          },
        ],
      },
      {
        path: 'participation-information',
        canActivateChild: [RoleGuardService],
        data: {
          code: InsuranceFuncCode.VOLATILITY,
          scope: Scopes.VIEW,
          breadcrumb: 'insurance.participationInformation.breadcrumb',
          pageName: 'insurance.participationInformation.pageName',
        },
        children: [
          {
            path: '',
            loadChildren: () =>
              import(
                '@hcm-mfe/insurance-management/feature/participation-information'
              ).then(
                (m) =>
                  m.InsuranceManagementFeatureParticipationInformationModule
              ),
          },
        ],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(router)],
  exports: [RouterModule],
})
export class InsuranceManagementFeatureShellRoutingModule {}
