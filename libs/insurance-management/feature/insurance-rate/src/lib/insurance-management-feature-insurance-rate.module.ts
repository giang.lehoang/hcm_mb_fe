import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { InsuranceManagementPipesTranslateModule } from '@hcm-mfe/insurance-management/pipes/translate';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { SharedUiMbButtonIconModule } from '@hcm-mfe/shared/ui/mb-button-icon';
import { SharedUiMbDatePickerModule } from '@hcm-mfe/shared/ui/mb-date-picker';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { SharedUiMbTableModule } from '@hcm-mfe/shared/ui/mb-table';
import { SharedUiMbTableWrapModule } from '@hcm-mfe/shared/ui/mb-table-wrap';
import { SharedDirectivesNumberInputModule } from '@hcm-mfe/shared/directives/number-input';
import { SharedDirectivesNumbericModule } from '@hcm-mfe/shared/directives/numberic';
import { TranslateModule } from '@ngx-translate/core';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { components } from './components';
import { InsuranceRateComponent, pages } from './pages';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: InsuranceRateComponent,
      },
    ]),
    SharedUiLoadingModule,
    SharedUiMbButtonModule,
    TranslateModule,
    ReactiveFormsModule,
    SharedUiMbSelectModule,
    SharedUiMbInputTextModule,
    SharedUiMbDatePickerModule,
    NzTagModule,
    SharedUiMbButtonIconModule,
    NzGridModule,
    FormsModule,
    SharedUiMbTableModule,
    SharedUiMbTableWrapModule,
    SharedDirectivesNumberInputModule,
    SharedDirectivesNumbericModule,
    InsuranceManagementPipesTranslateModule,
  ],
  declarations: [...pages, ...components],
})
export class InsuranceManagementFeatureInsuranceRateModule {}
