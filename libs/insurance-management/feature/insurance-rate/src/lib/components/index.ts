import { InsuranceRateFormComponent } from './insurance-rate-form/insurance-rate-form.component';

export const components = [InsuranceRateFormComponent];

export * from './insurance-rate-form/insurance-rate-form.component';
