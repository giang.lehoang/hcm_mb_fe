import { Component, Injector, OnInit } from '@angular/core';
import { AbstractControl, Validators } from '@angular/forms';
import {
  IMbOption,
  IInsuranceRate,
  InsuranceRateSubject,
  insuranceRateSubjectEmployeeOptions,
  insuranceRateSubjectOptions,
} from '@hcm-mfe/insurance-management/data-access/models';
import { InsuranceRateService } from '@hcm-mfe/insurance-management/data-access/services';
import { BaseFormComponent } from '@hcm-mfe/insurance-management/helper';
import { NzModalRef } from 'ng-zorro-antd/modal';

@Component({
  selector: 'app-insurance-rate-form',
  templateUrl: './insurance-rate-form.component.html',
})
export class InsuranceRateFormComponent
  extends BaseFormComponent<IInsuranceRate>
  implements OnInit
{
  insuranceOptions: IMbOption[] = [];
  subjectOptions = insuranceRateSubjectOptions();
  subjectEmployeeOptions = insuranceRateSubjectEmployeeOptions();

  constructor(
    private readonly insuranceRateService: InsuranceRateService,
    modalRef: NzModalRef,
    injector: Injector
  ) {
    super(injector);
    this.modalRef = modalRef;
    this.createApi = (body: IInsuranceRate) =>
      this.insuranceRateService.create(this.convertBody(body));
    this.updateApi = (id: number, body: IInsuranceRate) =>
      this.insuranceRateService.edit(id, this.convertBody(body));
  }

  override ngOnInit(): void {
    super.ngOnInit();
    this.getListCommon();
  }

  override initForm() {
    this.form = this.fb.group({
      subject: [null, [Validators.required]],
      subjectEmployee: [
        {
          value: null,
          disabled: this.data?.insuredSubject === InsuranceRateSubject.EMPLOYER,
        },
        [Validators.required],
      ],
      insuranceId: [null, [Validators.required]],
      contributionPpm: [
        null,
        [Validators.required, Validators.min(0), Validators.max(100)],
      ],
      insuranceName: [null, [Validators.required, Validators.maxLength(100)]],
      fromDate: [
        { value: null, disabled: Boolean(this.data) },
        [Validators.required],
      ],
      toDate: [{ value: null, disabled: true }],
      description: [null, [Validators.maxLength(250)]],
      flagStatus: [1, [Validators.required]],
    });
  }

  getListCommon() {
    this.insuranceRateService
      .getListCommon('INSURANCE')
      .subscribe((options) => {
        this.insuranceOptions = options;
      });
  }

  override convertBody(body: any) {
    return {
      ...super.convertBody(body),
      insuredSubject:
        body.subject === InsuranceRateSubject.EMPLOYER
          ? InsuranceRateSubject.EMPLOYER
          : body.subjectEmployee,
      contributionPpm: body.contributionPpm
        ? body.contributionPpm * 10000
        : body.contributionPpm,
    };
  }

  onSubjectChange(value: InsuranceRateSubject) {
    const subjectEmployee = this.form.get('subjectEmployee') as AbstractControl;
    if (value === InsuranceRateSubject.EMPLOYER) {
      subjectEmployee.setValue(null);
      subjectEmployee.disable();
    } else {
      subjectEmployee.enable();
    }
  }
}
