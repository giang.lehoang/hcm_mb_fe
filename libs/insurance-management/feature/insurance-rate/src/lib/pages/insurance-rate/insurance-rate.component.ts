import { Component, Injector, OnInit } from '@angular/core';
import {
  IInsuranceRate,
  IMbOption,
  insuranceRateMapToSubject,
  InsuranceRateSubject,
  insuranceRateSubjectEmployeeOptions,
  insuranceRateSubjectOptions,
} from '@hcm-mfe/insurance-management/data-access/models';
import { InsuranceRateService } from '@hcm-mfe/insurance-management/data-access/services';
import {
  BaseListComponent,
  DateTimeUtil,
  ObjectUtil,
  InsuranceFuncCode,
} from '@hcm-mfe/insurance-management/helper';
import { HTTP_STATUS_CODE } from '@hcm-mfe/shared/common/constants';
import { finalize, forkJoin } from 'rxjs';
import { InsuranceRateFormComponent } from '../../components';

@Component({
  selector: 'app-insurance-rate',
  templateUrl: './insurance-rate.component.html',
  styleUrls: ['./insurance-rate.component.scss'],
})
export class InsuranceRateComponent
  extends BaseListComponent<IInsuranceRate>
  implements OnInit
{
  constructor(
    injector: Injector,
    private readonly insuranceRateService: InsuranceRateService
  ) {
    super(injector);
    this.form = this.fb.group({
      subject: [null],
      subjectEmployee: [null],
      insuranceIds: [null],
      fromDateTo: [null],
      flagStatus: [null],
    });
    this.objFunction = this.sessionService.getSessionData(
      `FUNCTION_${InsuranceFuncCode.INSURANCE_RATE}`
    );
    this.formConfig = {
      title: 'insurance.insuranceRate.formTitle',
      content: InsuranceRateFormComponent,
    };
    this.deleteApi = (id: number) => this.insuranceRateService.deleteItem(id);
  }

  insuranceOptions: IMbOption[] = [];
  subjectOptions = insuranceRateSubjectOptions();
  subjectEmployeeOptions = insuranceRateSubjectEmployeeOptions();
  subjectDict = ObjectUtil.optionsToDictLabel(this.subjectOptions);
  subjectEmployeeDict = ObjectUtil.optionsToDictLabel(
    this.subjectEmployeeOptions
  );

  override ngOnInit(): void {
    super.ngOnInit();
    this.getListCommon();
  }

  override search(page?: number) {
    super.search(page);
    if (this.params.fromDateTo) {
      this.params.fromDateTo = DateTimeUtil.formatDate(this.params.fromDateTo);
    }
    let insuredSubjects = null;
    if (this.params.subject) {
      insuredSubjects =
        this.params.subject === InsuranceRateSubject.EMPLOYER
          ? InsuranceRateSubject.EMPLOYER
          : this.subjectEmployeeOptions.map((x) => x.value).join(',');
    }
    if (this.params.subjectEmployee) {
      insuredSubjects = this.params.subjectEmployee;
    }
    forkJoin([
      this.insuranceRateService.getList({
        ...this.params,
        insuredSubjects,
      }),
      this.insuranceRateService.getListCommon('INSURANCE'),
    ])
      .pipe(finalize(() => (this.isLoading = false)))
      .subscribe(
        ([res, options]) => {
          if (res.status === HTTP_STATUS_CODE.OK) {
            const dict = ObjectUtil.optionsToDictLabel(options);
            res.data.content.forEach((item: IInsuranceRate) => {
              insuranceRateMapToSubject(item);
              item.contributionPpm = item.contributionPpm
                ? item.contributionPpm / 10000
                : item.contributionPpm;
              item.insuranceText = dict[item.insuranceId];
              item.subjectName = this.subjectDict[item.subject];
              item.subjectEmployeeName =
                this.subjectEmployeeDict[item.subjectEmployee];
            });
            this.handleRes(res);
          }
        },
        () => {
          this.translate.instant('common.notification.error');
        }
      );
  }

  getListCommon() {
    this.insuranceRateService
      .getListCommon('INSURANCE')
      .subscribe((options) => {
        this.insuranceOptions = options;
      });
  }

  onSubjectChange(value: InsuranceRateSubject) {
    const subjectEmployee = this.form.get('subjectEmployee')!;
    if (value === InsuranceRateSubject.EMPLOYER) {
      subjectEmployee.reset();
      subjectEmployee.disable();
    } else {
      subjectEmployee.enable();
    }
  }

  override setHeaders() {
    this.tableConfig.headers = [
      {
        title: 'insurance.insuranceRate.label.subject',
        field: 'subjectName',
        width: 70,
      },
      {
        title: 'insurance.insuranceRate.label.subjectEmployee',
        field: 'subjectEmployeeName',
        width: 70,
        thClassList: ['text-center'],
      },
      {
        title: 'insurance.insuranceRate.label.insurance',
        field: 'insuranceText',
        width: 50,
      },
      {
        title: 'insurance.insuranceRate.label.insuranceName',
        field: 'insuranceName',
        width: 70,
      },
      {
        title: 'insurance.insuranceRate.label.contributionPpm',
        field: 'contributionPpm',
        width: 60,
      },
      {
        title: 'insurance.common.label.fromDate',
        field: 'fromDate',
        pipe: 'date',
        width: 60,
      },
      {
        title: 'insurance.common.label.toDate',
        field: 'toDate',
        pipe: 'date',
        width: 60,
      },
      {
        title: 'insurance.common.label.description',
        field: 'description',
        width: 60,
      },
      {
        title: 'insurance.common.label.flagStatus',
        thClassList: ['text-center'],
        tdClassList: ['text-center'],
        width: 50,
        tdTemplate: this.flagStatusTpl,
      },
      {
        title: '',
        field: 'action',
        tdClassList: ['text-nowrap', 'text-center'],
        thClassList: ['text-nowrap', 'text-center'],
        width: 60,
        tdTemplate: this.actionTpl,
        fixed: true,
        fixedDir: 'right',
      },
    ];
  }
}
