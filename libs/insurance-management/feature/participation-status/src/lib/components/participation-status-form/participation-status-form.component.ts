import { Component, Injector, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import {
  IMbOption,
  IParticipationStatus,
} from '@hcm-mfe/insurance-management/data-access/models';
import {
  ParticipationStatusService,
  VolatilityPlanService,
} from '@hcm-mfe/insurance-management/data-access/services';
import {
  BaseFormComponent,
  DateTimeUtil,
  DestroyService,
} from '@hcm-mfe/insurance-management/helper';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { filter, finalize, takeUntil } from 'rxjs';

@Component({
  selector: 'hcm-mfe-participation-status-form',
  templateUrl: './participation-status-form.component.html',
  providers: [DestroyService],
})
export class ParticipationStatusFormComponent
  extends BaseFormComponent<IParticipationStatus>
  implements OnInit
{
  volatilityOptions: IMbOption[] = [];
  solutionOptions: IMbOption[] = [];
  informOptions: IMbOption[] = [];
  solutionDisplayOptions: IMbOption[] = [];
  informDisplayOptions: IMbOption[] = [];

  constructor(
    modalRef: NzModalRef,
    injector: Injector,
    private readonly participationStatusService: ParticipationStatusService,
    private readonly volatilityPlanService: VolatilityPlanService,
    private destroy$: DestroyService
  ) {
    super(injector);
    this.modalRef = modalRef;
    this.createApi = (body: IParticipationStatus) =>
      this.participationStatusService.create(this.convertBody(body));
    this.updateApi = (id: number, body: IParticipationStatus) =>
      this.participationStatusService.edit(id, this.convertBody(body));
  }

  override ngOnInit(): void {
    super.ngOnInit();
    this.getListCommon();
    this.form
      .get('fromDate')
      ?.valueChanges.pipe(
        filter(() => !this.isLoading),
        takeUntil(this.destroy$)
      )
      .subscribe((value) => this.onChangeFromDate(value));
    this.form
      .get('volatilityId')
      ?.valueChanges.pipe(
        filter(() => !this.isLoading),
        takeUntil(this.destroy$)
      )
      .subscribe((value) => this.onChangeVolatility(value));
  }

  override initForm() {
    this.form = this.fb.group({
      volatilityId: [null, [Validators.required]],
      code: [null, [Validators.required, Validators.maxLength(25)]],
      name: [null, [Validators.required, Validators.maxLength(50)]],
      informId: [null, [Validators.required]],
      volatilitySolutionId: [null, [Validators.required]],
      fromDate: [
        { value: null, disabled: Boolean(this.data) },
        [Validators.required],
      ],
      toDate: [{ value: null, disabled: true }],
      description: [null, [Validators.maxLength(250)]],
      flagStatus: [1, [Validators.required]],
    });
  }

  onChangeVolatility(value: number, isUpdate = false) {
    if (!isUpdate) {
      this.reset();
    }
    this.volatilityPlanService
      .getList({
        page: 0,
        size: 1,
        volatilityIds: value,
        fromDateTo: DateTimeUtil.formatDate(this.form.get('fromDate')?.value),
      })
      .subscribe((res) => {
        const [data] = res.data.content;
        const informIds = data?.informIds || [];
        const volatilitySolutionIds = data?.volatilitySolutionIds || [];
        this.informDisplayOptions = this.informOptions.filter((x) =>
          informIds.includes(x.value)
        );
        this.solutionDisplayOptions = this.solutionOptions.filter((x) =>
          volatilitySolutionIds.includes(x.value)
        );
        if (isUpdate) {
          if (!informIds.includes(this.data.informId)) {
            this.informDisplayOptions = [
              ...this.informDisplayOptions,
              ...this.informOptions
                .filter((x) => x.value === this.data.informId)
                .map((x) => ({ ...x, disabled: true })),
            ];
          }
          if (!volatilitySolutionIds.includes(this.data.volatilitySolutionId)) {
            this.solutionDisplayOptions = [
              ...this.solutionDisplayOptions,
              ...this.solutionOptions
                .filter((x) => x.value === this.data.volatilitySolutionId)
                .map((x) => ({ ...x, disabled: true })),
            ];
          }
        }
      });
  }

  onChangeFromDate(date: Date) {    
    if (!date) {
      return this.reset();
    }
    const volatilityId = this.form.get('volatilityId')?.value;
    if (!volatilityId) return;
    this.onChangeVolatility(volatilityId);
  }

  reset() {
    ['informId', 'volatilitySolutionId'].forEach((key) => {
      this.form.get(key)?.reset();
    });
    this.informDisplayOptions = [];
    this.solutionDisplayOptions = [];
  }

  getListCommon() {
    this.isLoading = true;
    this.participationStatusService
      .getListCommon('VOLATILITY_SOLUTION,INFORM,VOLATILITY')
      .pipe(finalize(() => (this.isLoading = false)))
      .subscribe((options) => {
        this.solutionOptions = options.filter(
          (x) => x.data.type === 'VOLATILITY_SOLUTION'
        );
        this.informOptions = options.filter((x) => x.data.type === 'INFORM');
        this.volatilityOptions = options.filter(
          (x) => x.data.type === 'VOLATILITY'
        );
        if (this.data) {
          this.onChangeVolatility(this.data.volatilityId, true);
        }
      });
  }
}
