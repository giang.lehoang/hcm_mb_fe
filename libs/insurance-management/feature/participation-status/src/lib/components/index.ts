import { ParticipationStatusFormComponent } from './participation-status-form/participation-status-form.component';

export const components = [ParticipationStatusFormComponent];

export * from './participation-status-form/participation-status-form.component';
