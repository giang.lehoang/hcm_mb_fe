import { Component, Injector, OnInit } from '@angular/core';
import {
  IMbOption,
  IParticipationStatus,
} from '@hcm-mfe/insurance-management/data-access/models';
import { ParticipationStatusService } from '@hcm-mfe/insurance-management/data-access/services';
import {
  BaseListComponent,
  ObjectUtil,
  InsuranceFuncCode,
} from '@hcm-mfe/insurance-management/helper';
import { HTTP_STATUS_CODE } from '@hcm-mfe/shared/common/constants';
import { finalize, forkJoin } from 'rxjs';
import { ParticipationStatusFormComponent } from '../../components';

@Component({
  selector: 'hcm-mfe-participation-status',
  templateUrl: './participation-status.component.html',
  styleUrls: ['./participation-status.component.scss'],
})
export class ParticipationStatusComponent
  extends BaseListComponent<IParticipationStatus>
  implements OnInit
{
  constructor(
    injector: Injector,
    private readonly participationStatusService: ParticipationStatusService
  ) {
    super(injector);
    this.form = this.fb.group({
      volatilityId: [null],
      name: [null],
      flagStatus: [null],
    });
    this.objFunction = this.sessionService.getSessionData(
      `FUNCTION_${InsuranceFuncCode.INSURANCE_STATUS}`
    );
    this.formConfig = {
      title: 'insurance.participationStatus.formTitle',
      content: ParticipationStatusFormComponent,
    };
    this.deleteApi = (id: number) =>
      this.participationStatusService.deleteItem(id);
  }

  volatilityOptions: IMbOption[] = [];

  override ngOnInit(): void {
    super.ngOnInit();
    this.getListCommon();
  }

  override search(page?: number) {
    super.search(page);
    forkJoin([
      this.participationStatusService.getList(this.params),
      this.participationStatusService.getListCommon(
        'VOLATILITY_SOLUTION,INFORM,VOLATILITY'
      ),
    ])
      .pipe(finalize(() => (this.isLoading = false)))
      .subscribe(
        ([res, options]) => {
          if (res.status === HTTP_STATUS_CODE.OK) {
            const dict = ObjectUtil.optionsToDictLabel(options);
            res.data.content.forEach((item: IParticipationStatus) => {
              item.volatilityName = dict[item.volatilityId];
              item.informName = dict[item.informId];
              item.volatilitySolutionName = dict[item.volatilitySolutionId];
            });
            this.handleRes(res);
          }
        },
        () => {
          this.translate.instant('common.notification.error');
        }
      );
  }

  getListCommon() {
    this.participationStatusService
      .getListCommon('VOLATILITY')
      .subscribe((options) => {
        this.volatilityOptions = options;
      });
  }

  override setHeaders() {
    this.tableConfig.headers = [
      {
        title: 'insurance.participationStatus.label.code',
        field: 'code',
        width: 60,
      },
      {
        title: 'insurance.participationStatus.label.name',
        field: 'name',
        width: 60,
      },
      {
        title: 'insurance.participationStatus.label.volatility',
        field: 'volatilityName',
        width: 60,
      },
      {
        title: 'insurance.participationStatus.label.inform',
        field: 'informName',
        width: 60,
      },
      {
        title: 'insurance.participationStatus.label.volatilitySolution',
        field: 'volatilitySolutionName',
        width: 60,
      },
      {
        title: 'insurance.common.label.fromDate',
        field: 'fromDate',
        pipe: 'date',
        width: 60,
      },
      {
        title: 'insurance.common.label.toDate',
        field: 'toDate',
        pipe: 'date',
        width: 60,
      },
      {
        title: 'insurance.common.label.description',
        field: 'description',
        width: 60,
      },
      {
        title: 'insurance.common.label.flagStatus',
        thClassList: ['text-center'],
        tdClassList: ['text-center'],
        width: 50,
        tdTemplate: this.flagStatusTpl,
      },
      {
        title: '',
        field: 'action',
        tdClassList: ['text-nowrap', 'text-center'],
        thClassList: ['text-nowrap', 'text-center'],
        width: 60,
        tdTemplate: this.actionTpl,
        fixed: true,
        fixedDir: 'right',
      },
    ];
  }
}
