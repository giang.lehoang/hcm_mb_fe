import { Component, Injector, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormArray,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import {
  FilterCriteriaDataTypes,
  IFilterCriteria,
  IMbOption,
  IStatusSetting,
} from '@hcm-mfe/insurance-management/data-access/models';
import {
  FilterCriteriaService,
  ParticipationStatusService,
  StatusSettingService,
} from '@hcm-mfe/insurance-management/data-access/services';
import { BaseFormComponent } from '@hcm-mfe/insurance-management/helper';
import { flatten, partition } from 'lodash';
import { NzModalRef } from 'ng-zorro-antd/modal';

@Component({
  selector: 'app-status-setting-form',
  templateUrl: './status-setting-form.component.html',
})
export class StatusSettingFormComponent
  extends BaseFormComponent<IStatusSetting>
  implements OnInit
{
  istOptions: IMbOption[] = [];
  conditionOptions: IMbOption<string>[] = [];
  conditionDict: { [key: string]: IMbOption[] } = {};
  dataTypeDict: { [key: string]: FilterCriteriaDataTypes } = {};
  filterCriteriaDataTypes = FilterCriteriaDataTypes;
  parameterLabel = '';
  alertLabel = '';
  valueLabel = '';
  requiredLabel = `<span class='label__required'>*</span>`;
  formArrayNames = ['leftParameters', 'rightParameters'];

  constructor(
    private readonly statusSettingService: StatusSettingService,
    private readonly filterCriteriaService: FilterCriteriaService,
    private readonly participationStatusService: ParticipationStatusService,
    modalRef: NzModalRef,
    injector: Injector
  ) {
    super(injector);
    this.modalRef = modalRef;
    this.createApi = (body: IStatusSetting) =>
      this.statusSettingService.create(this.convertBody(body));
    this.updateApi = (id: number, body: IStatusSetting) =>
      this.statusSettingService.edit(id, this.convertBody(body));
  }

  override ngOnInit(): void {
    super.ngOnInit();
    this.getListCommon();
    this.translate
      .get('insurance.statusSetting.label.value')
      .subscribe(
        (label) => (this.valueLabel = `${label} ${this.requiredLabel}`)
      );
    this.translate
      .get('insurance.statusSetting.label.code')
      .subscribe(
        (label) => (this.parameterLabel = `${label} ${this.requiredLabel}`)
      );
    this.translate
      .get('insurance.statusSetting.label.alert')
      .subscribe(
        (label) => (this.alertLabel = `${label} ${this.requiredLabel}`)
      );
  }

  override initForm() {
    const obj: any = {};
    const parameters = partition(this.data?.conditions, (x) => !x.alert);
    this.formArrayNames.forEach(
      (key, i) =>
        (obj[key] = this.fb.array(
          parameters[i].length
            ? parameters[i].map((data) => this.buildParameterGroup(data))
            : [this.buildParameterGroup()]
        ))
    );
    this.form = this.fb.group({
      alert: [false, [Validators.required]],
      istId: [null, [Validators.required]],
      fromDate: [
        { value: null, disabled: Boolean(this.data) },
        [Validators.required],
      ],
      description: [null, [Validators.maxLength(250)]],
      flagStatus: [1, [Validators.required]],
      ...obj,
    });
  }

  override convertBody(body: any) {
    const conditions = flatten(
      this.formArrayNames.map((key, i) =>
        body[key].map((x: any) => ({ ...x, alert: !i }))
      )
    );
    this.formArrayNames.forEach((key) => delete body[key]);
    return {
      ...super.convertBody(body),
      conditions,
    };
  }

  getFormArray(key: string) {
    return this.form.get(key) as FormArray;
  }

  buildParameterGroup(data?: any) {
    const form = this.fb.group({
      code: [null, [Validators.required, this.codeValidator]],
      value: [null, [Validators.required]],
    });
    if (data) {
      form.patchValue(data);
    }
    return form;
  }

  addParameter(key: string) {
    this.getFormArray(key).push(this.buildParameterGroup());
  }

  removeParameter(key: string, index: number) {
    this.getFormArray(key).removeAt(index);
  }

  codeValidator = (control: AbstractControl): ValidationErrors | null => {
    if (!this.form) return null;
    const code = control.value;
    const formArray = control.parent?.parent as FormArray;
    if (!formArray) return null;
    const values: any[] = formArray.value;
    const exists = code && values.some((x) => x.code === code);
    return exists ? { conflict: true } : null;
  };

  onCodeChange(key: string, index: number, code: string, isUpdate = false) {
    if (!isUpdate) {
      this.getFormArray(key)
        .at(index)
        .get('value')
        ?.setValue(
          this.dataTypeDict[code] === FilterCriteriaDataTypes.LIST ? [] : null
        );
    }
    if (
      ![
        FilterCriteriaDataTypes.LIST,
        FilterCriteriaDataTypes.NUMBER_LIST,
      ].includes(this.dataTypeDict[code]) ||
      this.conditionDict[code]
    ) {
      return;
    }
    this.filterCriteriaService.getValues(code).subscribe((res) => {
      this.conditionDict[code] = (res.data || []).map((item: any) => ({
        value: item.id,
        label: item.name,
      }));
    });
  }

  getListCommon() {
    this.filterCriteriaService
      .getList({ page: 0, size: 1000 })
      .subscribe((res) => {
        this.conditionOptions = res.data.content.map(
          (item: IFilterCriteria) => {
            this.dataTypeDict[item.code] = item.dataType;
            if (
              item.dataType === FilterCriteriaDataTypes.NUMBER &&
              item.dataSource
            ) {
              this.dataTypeDict[item.code] =
                FilterCriteriaDataTypes.NUMBER_LIST;
            }
            return {
              value: item.code,
              label: item.name,
            };
          }
        );
        if (this.data) {
          const parameters = partition(this.data?.conditions, (x) => !x.alert);
          this.formArrayNames.forEach((key, i) => {
            parameters[i].forEach((item, index) => {
              this.onCodeChange(key, index, item.code, true);
            });
          });
        }
      });
    this.participationStatusService
      .getOptions()
      .subscribe((options) => (this.istOptions = options));
  }
}
