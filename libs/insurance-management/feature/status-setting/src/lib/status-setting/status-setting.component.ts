import { Component, Injector, OnInit } from '@angular/core';
import {
  IMbOption,
  IStatusSetting,
} from '@hcm-mfe/insurance-management/data-access/models';
import {
  ParticipationStatusService,
  StatusSettingService,
} from '@hcm-mfe/insurance-management/data-access/services';
import {
  BaseListComponent,
  DateTimeUtil,
  InsuranceFuncCode,
  ObjectUtil,
} from '@hcm-mfe/insurance-management/helper';
import { HTTP_STATUS_CODE } from '@hcm-mfe/shared/common/constants';
import { finalize, forkJoin, lastValueFrom } from 'rxjs';
import { StatusSettingFormComponent } from '../status-setting-form/status-setting-form.component';

@Component({
  selector: 'app-status-setting',
  templateUrl: './status-setting.component.html',
  styleUrls: ['./status-setting.component.scss'],
})
export class StatusSettingComponent
  extends BaseListComponent<IStatusSetting>
  implements OnInit
{
  istOptions: IMbOption[] = [];

  constructor(
    injector: Injector,
    private readonly statusSettingService: StatusSettingService,
    private readonly participationStatusService: ParticipationStatusService
  ) {
    super(injector);
    this.form = this.fb.group({
      istId: [null],
      fromDateTo: [null],
      flagStatus: [null],
    });
    this.objFunction = this.sessionService.getSessionData(
      `FUNCTION_${InsuranceFuncCode.STATUS_SETTING}`
    );
    this.formConfig = {
      title: 'insurance.statusSetting.formTitle',
      content: StatusSettingFormComponent,
    };
    this.deleteApi = (id: number) => this.statusSettingService.deleteItem(id);
  }

  override ngOnInit(): void {
    super.ngOnInit();
    this.getListCommon();
  }

  override search(page?: number) {
    super.search(page);
    if (this.params.fromDateTo) {
      this.params.fromDateTo = DateTimeUtil.formatDate(this.params.fromDateTo);
    }
    forkJoin([
      this.statusSettingService.getList(this.params),
      this.participationStatusService.getOptions(),
    ])
      .pipe(finalize(() => (this.isLoading = false)))
      .subscribe(
        ([res, options]) => {
          if (res.status === HTTP_STATUS_CODE.OK) {
            const dict = ObjectUtil.optionsToDictLabel(options);
            res.data.content.forEach((item: IStatusSetting) => {
              item.istName = dict[item.istId];
            });
            this.handleRes(res);
          }
        },
        () => {
          this.translate.instant('common.notification.error');
        }
      );
  }

  getListCommon() {
    this.participationStatusService
      .getOptions()
      .subscribe((options) => (this.istOptions = options));
  }

  override setHeaders() {
    this.tableConfig.headers = [
      {
        title: 'insurance.statusSetting.label.istId',
        field: 'istName',
        width: 70,
      },
      {
        title: 'insurance.common.label.fromDate',
        field: 'fromDate',
        pipe: 'date',
        width: 50,
      },
      {
        title: 'insurance.statusSetting.label.description',
        field: 'description',
        width: 60,
      },
      {
        title: 'insurance.common.label.flagStatus',
        thClassList: ['text-center'],
        tdClassList: ['text-center'],
        width: 50,
        tdTemplate: this.flagStatusTpl,
      },
      {
        title: '',
        field: 'action',
        tdClassList: ['text-nowrap', 'text-center'],
        thClassList: ['text-nowrap', 'text-center'],
        width: 60,
        tdTemplate: this.actionTpl,
        fixed: true,
        fixedDir: 'right',
      },
    ];
  }
}
