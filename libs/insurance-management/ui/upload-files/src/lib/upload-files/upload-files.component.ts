import { Component, forwardRef, Injector, Input, OnInit } from '@angular/core';
import {
  ControlValueAccessor,
  NgControl,
  NG_VALUE_ACCESSOR,
} from '@angular/forms';
import { DocumentService } from '@hcm-mfe/insurance-management/data-access/services';
import { TranslateService } from '@ngx-translate/core';
import { pick } from 'lodash';
import { InputBoolean } from 'ng-zorro-antd/core/util';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { ToastrService } from 'ngx-toastr';
import { catchError, map, noop, of, tap } from 'rxjs';

@Component({
  selector: 'mb-upload-files',
  templateUrl: './upload-files.component.html',
  styleUrls: ['./upload-files.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => UploadFilesComponent),
    },
  ],
})
export class UploadFilesComponent implements OnInit, ControlValueAccessor {
  @Input() @InputBoolean() mbDisable = false;
  fileList: NzUploadFile[] = [];
  ngControl!: NgControl;
  onTouched: () => void = noop;
  onChange: (_: any) => void = noop;

  constructor(
    private inj: Injector,
    private documentService: DocumentService,
    private toastrService: ToastrService,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    this.ngControl = this.inj.get(NgControl);
  }

  beforeUpload = (file: NzUploadFile) => {
    const form = new FormData();
    form.append('files', file as any);
    return this.documentService.upload(form).pipe(
      tap((res) => {
        const [data] = res.data;
        file['id'] = data.id;
        file.uid = data.id.toString();
        this.fileList = this.fileList.concat(file);
        this.onChange(this.mapValues(this.fileList));
      }),
      catchError((err) => {
        this.toastrService.error(err.message);
        return of(false);
      }),
      map(() => false)
    );
  };

  remove = (file: NzUploadFile) => {
    this.fileList = this.fileList.filter((x) => x.uid !== file.uid);
    this.onChange(this.mapValues(this.fileList));
    return false;
  };

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.mbDisable = isDisabled;
  }

  writeValue(obj: { id: number; uid: string; name: string }[]): void {
    this.fileList = obj.map((x) => ({
      ...x,
      uid: x.uid || x.id?.toString(),
    })) as any;
  }

  private mapValues(values: any[]) {
    return values.map((x) => pick(x, ['id', 'name']));
  }
}
