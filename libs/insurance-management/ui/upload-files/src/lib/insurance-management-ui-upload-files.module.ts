import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploadFilesComponent } from './upload-files/upload-files.component';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    NzUploadModule,
    SharedUiMbButtonModule,
    TranslateModule,
  ],
  declarations: [UploadFilesComponent],
  exports: [UploadFilesComponent],
})
export class InsuranceManagementUiUploadFilesModule {}
