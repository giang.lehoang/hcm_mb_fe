import { IMbOption } from './IOption';

export interface IInsuranceRate {
  id: number;
  insuranceName: string;
  insuranceId: number;
  insuranceText: string;
  insuredSubject: string;
  subject: string;
  subjectName: string;
  subjectEmployee: string;
  subjectEmployeeName: string;
  iraId: number;
  code: string;
  contributionPpm: number;
  description: string;
  flagStatus: number;
  fromDate: string;
  expirationDate: string;
}

export enum InsuranceRateSubject {
  EMPLOYER = 'EMPLOYER',
  EMPLOYEE = 'EMPLOYEE',
  MILITARY = 'MILITARY',
  VIETNAMESE = 'VIETNAMESE',
  FOREIGNER = 'FOREIGNER',
}

export const insuranceRateSubjectOptions = (): IMbOption<string>[] => [
  { value: InsuranceRateSubject.EMPLOYER, label: 'Người Sử Dụng Lao Động' },
  { value: InsuranceRateSubject.EMPLOYEE, label: 'Người Lao Động' },
];

export const insuranceRateSubjectEmployeeOptions = (): IMbOption<string>[] => [
  { value: InsuranceRateSubject.MILITARY, label: 'Quân Nhân' },
  { value: InsuranceRateSubject.VIETNAMESE, label: 'NLĐ Việt Nam' },
  { value: InsuranceRateSubject.FOREIGNER, label: 'NLĐ Nước Ngoài' },
];

export const insuranceRateMapToSubject = (item: IInsuranceRate) => {
  item.subject =
    item.insuredSubject === InsuranceRateSubject.EMPLOYER
      ? item.insuredSubject
      : InsuranceRateSubject.EMPLOYEE;
  item.subjectEmployee =
    item.insuredSubject !== InsuranceRateSubject.EMPLOYER
      ? item.insuredSubject
      : '';
};
