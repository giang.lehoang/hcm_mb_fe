export interface IRelativeInformation {
  id: number;
  insId: number;
  fullName: string;
  relationship: string;
  relationshipText: string;
  householdRelationship: string;
  householdRelationshipText: string;
  dob: string;
  gender: string;
  genderText: string;
  nationality: string;
  nationalityText: string;
  ethnic: string;
  ethnicText: string;
  identityNo: string;
  taxCode: string;
  permanentAddress: string;
  permanentProvinceCode: string;
  permanentDistrictCode: string;
  permanentWardCode: string;
}
