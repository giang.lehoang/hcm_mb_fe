export interface IRegionCap {
  id: number;
  regionId: number;
  insuranceId: number;
  insuranceName: string;
  insuranceMonth: number;
  regionName: string;
  baseSalary: number;
  maximumInsuranceSalary: number;
  minimumSalary: number;
  description: string;
  flagStatus: number;
  fromDate: string;
  expirationDate: string;
}
