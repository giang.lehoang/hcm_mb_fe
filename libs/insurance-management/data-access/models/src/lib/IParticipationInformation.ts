import { IMbOption } from './IOption';

export interface IParticipationInformation {
  id: number;
  staffCode: string;
  fullName: string;
  dob: string;
  taxCode: string;
  gender: string;
  genderText: string;
  nationality: string;
  phone: string;
  email: string;
  ethnic: string;
  identityNo: string;
  issuedDate: string;
  issuedPlace: string;
  permanentAddress: string;
  permanentProvinceCode: string;
  permanentDistrictCode: string;
  permanentWardCode: string;
  contactAddress: string;
  contactProvinceCode: string;
  contactDistrictCode: string;
  contactWardCode: string;
  joinedDate: string;
  officialJoinedDate: string;
  leavingDate: string;
  unit: string;
  jobTitle: string;
  contractId: string;
  contractType: string;
  signedDate: string;
  insuredSubject: string;
  insuredSubjectText: string;
  regionId: string;
  insuranceUnitId: string;
  socialInsuranceNo: string;
  heathInsuranceNo: string;
  joined: string;
  joinedText: string;
  tk1File: string;
  householdRegistrationBook: string;
  socialJoined: string;
  healthJoined: string;
  unemploymentJoined: string;
  accidentJoined: string;
  laborUnionJoined: string;
  fileType: string;
  fileNo: string;
  householdRelationship: string;
  tplId: string;
  fromDate: string;
  toDate: string;
  flagStatus: number;
}

export enum ParticipationInformationFileType {}

export const participationInformationFileTypeOptions =
  (): IMbOption<string>[] => [
    { value: '1', label: 'Sổ hộ khẩu' },
    { value: '2', label: 'Sổ tạm trú' },
    {
      value: '3',
      label: 'Giấy tạm trú',
    },
  ];

export const participationInformationStatusOptions = (): IMbOption<any>[] => [
  { value: true, label: 'Chưa tham gia' },
  { value: false, label: 'Đang tham gia' },
];
