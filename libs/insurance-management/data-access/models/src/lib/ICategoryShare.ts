export interface ICategoryShareContent {
  id: number;
  createDate: string;
  createdBy: string;
  description: string;
  flagStatus: number;
  fromDate: string;
  lastUpdateDate: string;
  lastUpdatedBy: string;
  name: string;
  nameOne: string;
  nameTwo: string;
  properties: any;
  position: number;
  type: string;
  typeName: string;
  categotyType: string;
}

export const categoryTypes = (): { value: string; label: string }[] => [
  { value: 'REGION', label: 'Vùng bảo hiểm' },
  { value: 'VOLATILITY', label: 'Nhóm biến động' },
  { value: 'VOLATILITY_SOLUTION', label: 'Phương án chi tiết' },
  { value: 'INFORM', label: 'Hình thức báo' },
  { value: 'INSURANCE', label: 'Nhóm bảo hiểm' },
];
