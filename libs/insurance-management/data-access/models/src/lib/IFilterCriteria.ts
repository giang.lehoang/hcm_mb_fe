import { IMbOption } from './IOption';

export interface IFilterCriteria {
  id: number;
  code: string;
  name: string;
  dataType: FilterCriteriaDataTypes;
  dataSourceType: FilterCriteriaSourceDataTypes;
  dataSource: string;
  dataTypeName: string;
  dataSourceTypeName: string;
  dataIdField: string;
  dataNameField: string;
  description: string;
  flagStatus: number;
  fromDate: string;
  expirationDate: string;
}

export enum FilterCriteriaDataTypes {
  LIST = 'LIST',
  TEXT = 'TEXT',
  DATE = 'DATE',
  NUMBER = 'NUMBER',
  NUMBER_LIST = 'NUMBER_LIST',
}

export enum FilterCriteriaSourceDataTypes {
  SQL = 'SQL',
  TABLE = 'TABLE',
  API = 'API',
  FUNCTION = 'FUNCTION',
  PROCEDURE = 'PROCEDURE',
}

export const filterCriteriaDataTypesOptions = (): IMbOption<string>[] => [
  { value: FilterCriteriaDataTypes.LIST, label: 'Danh sách' },
  { value: FilterCriteriaDataTypes.TEXT, label: 'Text' },
  { value: FilterCriteriaDataTypes.DATE, label: 'Date' },
  { value: FilterCriteriaDataTypes.NUMBER, label: 'Number' },
];

export const filterCriteriaSourceDataTypesOptions = (): IMbOption<string>[] => [
  { value: FilterCriteriaSourceDataTypes.SQL, label: 'Câu lệnh SQL' },
  { value: FilterCriteriaSourceDataTypes.TABLE, label: 'Table' },
  { value: FilterCriteriaSourceDataTypes.API, label: 'API' },
  { value: FilterCriteriaSourceDataTypes.FUNCTION, label: 'Function' },
  { value: FilterCriteriaSourceDataTypes.PROCEDURE, label: 'Procedure' },
];
