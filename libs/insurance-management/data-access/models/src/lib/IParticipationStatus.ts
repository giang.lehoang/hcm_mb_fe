export interface IParticipationStatus {
  id: number;
  code: string;
  name: string;
  volatilityId: number;
  volatilityName: string;
  informId: number;
  informName: string;
  volatilitySolutionId: number;
  volatilitySolutionName: string;
  description: string;
  flagStatus: number;
  fromDate: string;
  expirationDate: string;
}
