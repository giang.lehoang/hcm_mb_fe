export interface IStatusSetting {
  id: number;
  alert: boolean;
  istId: number;
  istName: string;
  conditions: Array<{ code: string; value: string; alert: boolean }>;
  description: string;
  flagStatus: number;
  fromDate: string;
  expirationDate: string;
}
