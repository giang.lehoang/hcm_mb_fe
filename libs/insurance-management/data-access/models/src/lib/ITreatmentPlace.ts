import { IMbOption } from './IOption';

export interface ITreatmentPlace {
  id: number;
  code: string;
  name: string;
  newAcceptance: boolean;
  provinceCode: string;
  provinceName: string;
  unitId: number;
  unitName: string;
  description: string;
  flagStatus: number;
  fromDate: string;
  toDate: string;
  expirationDate: string;
}

export const treatmentPlaceNewAcceptanceOptions = (): IMbOption<boolean>[] => [
  { value: true, label: 'Cho phép' },
  { value: false, label: 'Không cho phép' },
];
