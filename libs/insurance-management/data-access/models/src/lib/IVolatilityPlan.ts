export interface IVolatilityPlan {
  id: number;
  volatilityId: number;
  volatilityName: string;
  informIds: number[];
  informNames: string[];
  volatilitySolutionIds: number[];
  volatilitySolutionNames: string[];
  description: string;
  flagStatus: number;
  fromDate: string;
  expirationDate: string;
}
