import { Injectable } from '@angular/core';
import { IParticipationInformation } from '@hcm-mfe/insurance-management/data-access/models';
import { BaseInsuranceManagementCRUDService } from './base-crud.service';
import { UrlConstant } from './constant/url.class';

@Injectable({
  providedIn: 'root',
})
export class RelativeInformationService extends BaseInsuranceManagementCRUDService<IParticipationInformation> {
  protected override baseURL = UrlConstant.END_POINT.relativeInformation;
}
