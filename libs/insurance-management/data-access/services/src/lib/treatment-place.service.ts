import { Injectable } from '@angular/core';
import { ITreatmentPlace } from '@hcm-mfe/insurance-management/data-access/models';
import { map } from 'rxjs';
import { BaseInsuranceManagementCRUDService } from './base-crud.service';
import { UrlConstant } from './constant/url.class';

@Injectable({
  providedIn: 'root',
})
export class TreatmentPlaceService extends BaseInsuranceManagementCRUDService<ITreatmentPlace> {
  protected override baseURL = UrlConstant.END_POINT.treatmentPlace;

  getOptions() {
    return this.getList({ page: 0, size: 1000 }).pipe(
      map((res) =>
        res.data.content.map((item: any) => ({
          value: item.id,
          label: item.name,
        }))
      )
    );
  }
}
