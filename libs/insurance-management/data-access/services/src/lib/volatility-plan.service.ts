import { Injectable } from '@angular/core';
import { IVolatilityPlan } from '@hcm-mfe/insurance-management/data-access/models';
import { BaseInsuranceManagementCRUDService } from './base-crud.service';
import { UrlConstant } from './constant/url.class';

@Injectable({
  providedIn: 'root',
})
export class VolatilityPlanService extends BaseInsuranceManagementCRUDService<IVolatilityPlan> {
  protected override baseURL = UrlConstant.END_POINT.volatility;
}
