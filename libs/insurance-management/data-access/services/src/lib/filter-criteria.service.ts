import { Injectable } from '@angular/core';
import { IFilterCriteria } from '@hcm-mfe/insurance-management/data-access/models';
import { BaseInsuranceManagementCRUDService } from './base-crud.service';
import { UrlConstant } from './constant/url.class';

@Injectable({
  providedIn: 'root',
})
export class FilterCriteriaService extends BaseInsuranceManagementCRUDService<IFilterCriteria> {
  protected override baseURL = UrlConstant.END_POINT.filterCriteria;

  getValues(code: string) {
    return this.get(this.buildURL(`code/${code}/values`));
  }
}
