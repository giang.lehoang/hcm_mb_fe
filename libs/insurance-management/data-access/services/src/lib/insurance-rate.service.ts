import { Injectable } from '@angular/core';
import { IInsuranceRate } from '@hcm-mfe/insurance-management/data-access/models';
import { BaseInsuranceManagementCRUDService } from './base-crud.service';
import { UrlConstant } from './constant/url.class';

@Injectable({
  providedIn: 'root',
})
export class InsuranceRateService extends BaseInsuranceManagementCRUDService<IInsuranceRate> {
  protected override baseURL = UrlConstant.END_POINT.participationRate;
}
