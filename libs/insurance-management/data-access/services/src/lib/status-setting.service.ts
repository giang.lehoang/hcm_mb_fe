import { Injectable } from '@angular/core';
import { IStatusSetting } from '@hcm-mfe/insurance-management/data-access/models';
import { BaseInsuranceManagementCRUDService } from './base-crud.service';
import { UrlConstant } from './constant/url.class';

@Injectable({
  providedIn: 'root',
})
export class StatusSettingService extends BaseInsuranceManagementCRUDService<IStatusSetting> {
  protected override baseURL = UrlConstant.END_POINT.statusSetting;
}
