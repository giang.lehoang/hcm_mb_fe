import { IMbOption } from '@hcm-mfe/insurance-management/data-access/models';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { MICRO_SERVICE } from '@hcm-mfe/shared/common/constants';
import { map, Observable } from 'rxjs';
import { UrlConstant } from './constant/url.class';

export class BaseInsuranceManagementService extends BaseService {
  protected baseURL = '';
  protected serviceName = MICRO_SERVICE.DEFAULT;

  protected buildURL(url: string | number = '') {
    return `/hcm-insurance/${this.baseURL}/${url}`;
  }

  public override get(
    endpointUrl: string,
    options?: any,
    serviceName?: string
  ): Observable<any> {
    return super.get(endpointUrl, options, serviceName || this.serviceName);
  }

  public override post(
    endpointUrl: string,
    data?: any,
    options?: any,
    serviceName?: string
  ): Observable<any> {
    return super.post(
      endpointUrl,
      data,
      options,
      serviceName || this.serviceName
    );
  }

  public override put(
    endpointUrl: string,
    data?: any,
    serviceName?: string
  ): Observable<any> {
    return super.put(endpointUrl, data, serviceName || this.serviceName);
  }

  public override delete(
    endpointUrl: string,
    options?: any,
    serviceName?: string
  ): Observable<any> {
    return super.delete(endpointUrl, options, serviceName || this.serviceName);
  }

  public getListCommon(types: string): Observable<IMbOption[]> {
    return this.get(`/hcm-insurance/${UrlConstant.END_POINT.common}`, {
      params: {
        page: 0,
        size: 1000,
        sort: 'position:ASC',
        types,
      },
    }).pipe(
      map((res) =>
        res.data.content.map((x: any) => ({
          value: x.id,
          label: x.name,
          data: { type: x.type },
        }))
      )
    );
  }
}
