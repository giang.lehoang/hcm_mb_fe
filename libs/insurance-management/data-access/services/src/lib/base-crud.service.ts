import { BaseInsuranceManagementService } from './base.service';

export class BaseInsuranceManagementCRUDService<
  T
> extends BaseInsuranceManagementService {
  public create(body: T) {
    return this.post(this.buildURL(), body, this.serviceName);
  }

  public edit(id: number, body: T) {
    return this.put(this.buildURL(id), body);
  }

  public getOne(id: number) {
    return this.get(this.buildURL(id));
  }

  public getList(params: any) {
    return this.get(this.buildURL(), { params: this.toParams(params) });
  }

  public deleteItem(id: number) {
    return this.delete(this.buildURL(id));
  }
}
