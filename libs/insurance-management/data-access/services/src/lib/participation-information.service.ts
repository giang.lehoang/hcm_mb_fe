import { Injectable } from '@angular/core';
import { IRelativeInformation } from '@hcm-mfe/insurance-management/data-access/models';
import { BaseInsuranceManagementCRUDService } from './base-crud.service';
import { UrlConstant } from './constant/url.class';

@Injectable({
  providedIn: 'root',
})
export class ParticipationInformationService extends BaseInsuranceManagementCRUDService<IRelativeInformation> {
  protected override baseURL = UrlConstant.END_POINT.participationInformation;
}
