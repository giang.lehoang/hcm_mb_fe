import { Injectable } from '@angular/core';
import { ICategoryShareContent } from '@hcm-mfe/insurance-management/data-access/models';
import { BaseInsuranceManagementCRUDService } from './base-crud.service';
import { UrlConstant } from './constant/url.class';

@Injectable({
  providedIn: 'root',
})
export class CategoryShareService extends BaseInsuranceManagementCRUDService<ICategoryShareContent> {
  protected override baseURL = UrlConstant.END_POINT.common;
}
