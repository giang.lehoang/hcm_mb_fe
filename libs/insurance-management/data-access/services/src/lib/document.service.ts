import { Injectable } from '@angular/core';
import { UrlConstant } from './constant/url.class';
import { BaseInsuranceManagementService } from './base.service';

@Injectable({
  providedIn: 'root',
})
export class DocumentService extends BaseInsuranceManagementService {
  protected override baseURL = UrlConstant.END_POINT.document;

  upload(form: FormData) {
    return this.post(this.buildURL(''), form, this.serviceName);
  }

  download(id: number, options?: any) {
    return this.getRequestFileD2T(this.buildURL(id), options, this.serviceName);
  }
}
