export class UrlConstant {
  public static readonly END_POINT = {
    common: 'v1.0/common',
    volatility: 'v1.0/volatility-category',
    participationStatus: 'v1.0/insurance-status',
    participationRate: 'v1.0/insurance-rate',
    regionCap: 'v1.0/region-cap',
    treatmentPlace: 'v1.0/treatment-place',
    filterCriteria: 'v1.0/filter-criteria',
    statusSetting: 'v1.0/status-condition',
    participationInformation: 'v1.0/insurance',
    relativeInformation: 'v1.0/family-member',
    document: 'v1.0/document',
  };
}
