import { Pipe, PipeTransform } from '@angular/core';
import { IMbOption } from '@hcm-mfe/insurance-management/data-access/models';

@Pipe({
  name: 'optionDisplayColor',
})
export class OptionDisplayColorPipe implements PipeTransform {
  transform(value: any, options: IMbOption<any>[]): any {
    return options.find((o) => o.value === value)?.color;
  }
}
