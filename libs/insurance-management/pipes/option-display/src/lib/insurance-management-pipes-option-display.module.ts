import { NgModule } from '@angular/core';
import { OptionDisplayLabelPipe } from './option-display-label.pipe';
import { OptionDisplayColorPipe } from './option-display-color.pipe';

@NgModule({
  declarations: [OptionDisplayLabelPipe, OptionDisplayColorPipe],
  exports: [OptionDisplayLabelPipe, OptionDisplayColorPipe],
})
export class InsuranceManagementPipesOptionDisplayModule {}
