import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateLabelPipe } from './translate-label.pipe';

@NgModule({
  imports: [CommonModule],
  declarations: [TranslateLabelPipe],
  exports: [TranslateLabelPipe],
})
export class InsuranceManagementPipesTranslateModule {}
