import { Pipe, PipeTransform } from '@angular/core';
import { StringUtil } from '@hcm-mfe/insurance-management/helper';
import { TranslatePipe } from '@ngx-translate/core';

@Pipe({
  name: 'translateLabel',
  pure: false,
})
export class TranslateLabelPipe extends TranslatePipe implements PipeTransform {
  override transform(
    query: string,
    label: string,
    isToLower = true,
    params: any = {}
  ) {
    params.label = super.transform(label);
    if (isToLower) {
      params.label = StringUtil.firstToLowerCase(params.label);
    }
    return StringUtil.firstToUpperCase(super.transform(query, params));
  }
}
