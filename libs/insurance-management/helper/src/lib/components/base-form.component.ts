import { Component } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { insuaranceStatusOptions } from '@hcm-mfe/insurance-management/data-access/models';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { HTTP_STATUS_CODE, Mode } from '@hcm-mfe/shared/common/constants';
import { BaseResponse } from '@hcm-mfe/shared/data-access/models';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { finalize, Observable } from 'rxjs';
import { DateTimeUtil } from '../utils/datetime.util';
import { cleanDataForm } from '@hcm-mfe/shared/common/utils';

@Component({
  template: `<ng-content></ng-content>`,
})
export class BaseFormComponent<T> extends BaseComponent {
  data!: T;
  mode = Mode.ADD;
  isSubmitted = false;
  form!: FormGroup;
  key = 'id';
  modalRef!: NzModalRef;
  statusOptions = insuaranceStatusOptions();

  createApi!: (body: T) => Observable<any>;
  updateApi!: (id: number, body: T) => Observable<any>;

  get f() {
    return this.form.controls;
  }

  ngOnInit(): void {
    this.initForm();
    this.patchValueInfo();
  }

  initForm() {
    // Init Form
  }

  patchValueInfo() {
    this.data && this.form.patchValue(this.data);
  }

  save() {
    this.isSubmitted = true;
    if (this.form.invalid) return;
    this.isLoading = true;
    cleanDataForm(this.form);
    const body = this.form.value;
    if (this.mode === Mode.ADD) {
      this.createApi(body)
        .pipe(finalize(() => (this.isLoading = false)))
        .subscribe(
          (res: BaseResponse) => {
            if (res.status === HTTP_STATUS_CODE.OK) {
              this.modalRef?.close({ refresh: true });
              this.toastrCustom.success(
                this.translate.instant('common.notification.saveSuccess')
              );
            } else {
              this.toastrCustom.error(
                this.translate.instant('common.notification.saveFail')
              );
            }
          },
          () => {
            this.toastrCustom.error(
              this.translate.instant('common.notification.saveFail')
            );
          }
        );
    } else if (this.mode === Mode.EDIT) {
      this.updateApi((this.data as any)[this.key], body)
        .pipe(finalize(() => (this.isLoading = false)))
        .subscribe(
          (res: BaseResponse) => {
            if (res.status === HTTP_STATUS_CODE.OK) {
              this.modalRef?.close({ refresh: true });
              this.toastrCustom.success(
                this.translate.instant('common.notification.saveSuccess')
              );
            } else {
              this.toastrCustom.error(
                this.translate.instant('common.notification.saveFail')
              );
            }
          },
          () => {
            this.toastrCustom.error(
              this.translate.instant('common.notification.saveFail')
            );
          }
        );
    }
  }

  override handleDestroy() {
    this.modalRef?.destroy();
  }

  convertBody(body: any) {
    const fromDate = (body as any).fromDate;
    return {
      ...body,
      fromDate: fromDate && DateTimeUtil.formatDate(fromDate),
    };
  }
}
