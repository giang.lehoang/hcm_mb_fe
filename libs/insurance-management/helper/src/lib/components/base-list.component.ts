import { Component, TemplateRef, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { insuaranceStatusOptions } from '@hcm-mfe/insurance-management/data-access/models';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import {
  HTTP_STATUS_CODE,
  Mode,
  userConfig,
} from '@hcm-mfe/shared/common/constants';
import { MBTableConfig } from '@hcm-mfe/shared/data-access/models';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { finalize, Observable } from 'rxjs';
import { ObjectUtil } from '../utils/object.util';

@Component({
  template: `<ng-content></ng-content>`,
})
export class BaseListComponent<T> extends BaseComponent {
  form!: FormGroup;
  modalRef!: NzModalRef;
  @ViewChild('flagStatusTpl', { static: true })
  flagStatusTpl!: TemplateRef<any>;
  @ViewChild('actionTpl', { static: true }) actionTpl!: TemplateRef<any>;
  public readonly nzAlignTh = 'left';
  modeEnum = Mode;
  key = 'id';
  deleteApi!: (id: number) => Observable<any>;
  formConfig!: { title: string; content: any };
  statusOptions = insuaranceStatusOptions();
  statusDict = ObjectUtil.optionsToDict(this.statusOptions);
  heightTable = { x: '80vw', y: '27em' };
  tableData: T[] = [];
  tableConfig: MBTableConfig = {
    headers: [],
    total: 0,
    needScroll: true,
    loading: false,
    size: 'small',
    pageSize: userConfig.pageSize,
    pageIndex: 1,
    showFrontPagination: false,
  };
  params: any = {};

  ngOnInit(): void {
    this.setHeaders();
    this.search();
  }

  enterSearch(event: any) {
    if (event.key === 'Enter' || event.type === 'click') {
      this.search();
    }
  }

  search(page?: number) {
    const params = this.form.value;
    this.params = {
      ...params,
      size: this.tableConfig.pageSize,
      page: page ? page - 1 : 0,
    };
    this.isLoading = true;
  }

  showConfirm(id: number, $event: Event): void {
    $event.stopPropagation();
    this.deletePopup.showModal(() => this.deleteItem(id));
  }

  deleteItem(id: number) {
    this.isLoading = true;
    this.deleteApi(id)
      .pipe(finalize(() => (this.isLoading = false)))
      .subscribe(
        (res) => {
          if (res.status === HTTP_STATUS_CODE.OK) {
            this.toastrCustom.success(
              this.translate.instant('common.notification.deleteSuccess')
            );
            this.search();
          }
        },
        () => {
          this.toastrCustom.error(
            this.translate.instant('common.notification.deleteError')
          );
        }
      );
  }

  showModal(footerTpll: TemplateRef<any>, mode: Mode, data?: T) {
    if (data && typeof data !== 'object') {
      data = this.tableData.find((item) => (item as any)[this.key] === data);
    }
    this.modalRef = this.modal.create({
      nzWidth: this.getNzWidth(),
      nzTitle: this.translate.instant(this.formConfig.title),
      nzContent: this.formConfig.content,
      nzComponentParams: {
        mode,
        data,
      },
      nzFooter: footerTpll,
    });
    this.modalRef.afterClose.subscribe((result) =>
      result?.refresh ? this.search() : ''
    );
  }

  private getNzWidth() {
    if (window.innerWidth > 767) {
      return window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5;
    }
    return window.innerWidth;
  }

  override handleDestroy() {
    super.handleDestroy();
    this.modalRef?.destroy();
  }

  trackFnTable(index: number, item: any) {
    return item[this.key];
  }

  handleRes(res: any) {
    this.tableData = res.data.content;
    this.tableConfig.total = res.data.totalElements;
    this.tableConfig.pageIndex = res.data.number + 1;
  }

  setHeaders() {
    // Set headers
  }
}
