import { IMbOption } from '@hcm-mfe/insurance-management/data-access/models';

export class ObjectUtil {
  static optionsToDict(options: IMbOption<number | string>[]) {
    const dict: { [key: string]: IMbOption<number | string> } = {};
    options.forEach((o) => {
      dict[o.value] = o;
    });
    return dict;
  }

  static optionsToDictLabel(options: IMbOption<number | string>[]) {
    const dict: { [key: string]: string } = {};
    options.forEach((o) => (dict[o.value] = o.label));
    return dict;
  }

  static optionsToDictColor(options: IMbOption<number | string>[]) {
    const dict: { [key: string]: string } = {};
    options.forEach((o) => (dict[o.value] = o.color));
    return dict;
  }
}
