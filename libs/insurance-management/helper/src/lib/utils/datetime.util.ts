import * as moment from 'moment';

export class DateTimeUtil {
  static formatDate(date: any) {
    return moment(date).format('YYYY-MM-DD');
  }
}
