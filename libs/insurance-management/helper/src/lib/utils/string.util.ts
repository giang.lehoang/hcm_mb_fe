export class StringUtil {
  static firstToUpperCase(str: string) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  static firstToLowerCase(str: string) {
    return str.charAt(0).toLowerCase() + str.slice(1);
  }
}
