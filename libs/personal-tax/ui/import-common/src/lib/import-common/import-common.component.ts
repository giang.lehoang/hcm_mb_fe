import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { saveAs } from 'file-saver';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';
import { Constant } from '@hcm-mfe/personal-tax/data-access/common';
import { HTTP_STATUS_CODE } from '@hcm-mfe/shared/common/constants';
import { ErrorImport } from '@hcm-mfe/personal-tax/data-access/models';
import { TranslateService } from '@ngx-translate/core';
import {DownloadFileAttachService} from "@hcm-mfe/personal-tax/data-access/services";
import {beforeUploadFile, getTypeExport} from "@hcm-mfe/shared/common/utils";
import {NzUploadFile} from "ng-zorro-antd/upload";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {Subscription} from "rxjs";

@Component({
    selector: 'app-import-common',
    templateUrl: './import-common.component.html',
    styleUrls: ['./import-common.component.scss']
})
export class ImportCommonComponent  {

    @Input() showContent: boolean = false;
    @Input() requiredAttachFile: boolean = false;
    @Input() closeModalWhenClick: boolean = true;
    @Input() urlApiImport: string = '';
    @Input() urlApiDownloadTemp: string = '';
    @Input() nzTitle = this.translate.instant('personalTax.label.import');
    @Output() onCloseModal: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output() onLoadPage = new EventEmitter<boolean>();

    // cho phep chon nam. phuc vu cho man hinh dk nhan chung tu thue
    @Input() isAllowSelectYear: boolean = false;

    selectedYear: Date | undefined;
    isFulfillImportFile = false;

    isModalError = false;
    errorList: ErrorImport[] = [];
    fileList: NzUploadFile[] = [];
    fileName: string | undefined;
    fileImportName = '';
    fileImportSize: string;

    isExistFileImport = false;
    nzWidth: number;
    nzWidthError: number;
    subs: Subscription[] = [];

    constructor(private downloadFileAttachService: DownloadFileAttachService,
                private toastrService: ToastrService,
                private translate: TranslateService) {
        this.nzWidth = window.innerWidth > 767 ? window.innerWidth / 3 : window.innerWidth / 2.5;
        this.nzWidthError = window.innerWidth > 767 ? window.innerWidth / 2 : window.innerWidth / 1.5;
    }

  ngOnInit(): void {

  }

    doClose(refresh: boolean = false) {
        this.showContent = false;
        this.onCloseModal.emit(refresh);
        this.fileImportName = '';
        this.isExistFileImport = false;

        if (this.isAllowSelectYear) {
            this.selectedYear = null;
            this.isFulfillImportFile = false;
        }

    }

    doDownloadTemplate() {
        this.onLoadPage.emit(true);
        this.subs.push(
          this.downloadFileAttachService.downloadTemplate(this.urlApiDownloadTemp).subscribe(res => {
            this.onLoadPage.emit(false);
            const arr = res.headers.get('Content-Disposition')?.split(';');
            const fileName: string = arr[arr.length - 1].replace('filename=', '').trim();
            const reportFile = new Blob([res.body], { type: getTypeExport('xlsx') });
            saveAs(reportFile, fileName);
          }, error => {
            this.onLoadPage.emit(false);
            this.toastrService.error(error.message);
          })
        );
    }

    doCloseModal() {
        this.isModalError = false;
    }

    doDownloadFile() {
      this.onLoadPage.emit(true);
      this.subs.push(
        this.downloadFileAttachService.doDownloadFileByName(this.fileName).subscribe(res => {
          this.onLoadPage.emit(false);
          const reportFile = new Blob([res.body], { type: getTypeExport('xlsx') });
          saveAs(reportFile, 'file_error_information.xlsx');
        }, error => {
          this.onLoadPage.emit(false);
          this.toastrService.error(error.message);
        })
      );
    }

    beforeUpload = (file: NzUploadFile): boolean => {
        this.fileList = [];
        this.fileList = beforeUploadFile(file, this.fileList, 3000000, this.toastrService, this.translate, 'personalTax.notification.upload.errorFileImport', '.XLS', '.XLSX');
        if (this.fileList.length > 0) {
          this.isExistFileImport = true;
          this.fileImportName = file.name;
          this.fileImportSize = (file.size / 1000000).toFixed(2);
        }
        return false;
    };

    doImportFile() {
        this.isFulfillImportFile = true;

        const formData = new FormData();
        this.fileList.forEach((file: NzUploadFile) => {
            formData.append('file', file as NzSafeAny);
        });

        // neu cho phep chon nam
        if (this.isAllowSelectYear) {
            if (this.selectedYear) {
                formData.append('year', new Blob([JSON.stringify(moment(this.selectedYear).format('YYYY'))], { type: 'application/json' }));
            } else {
                return;
            }
        }
        //end: neu cho phep chon nam

        this.onLoadPage.emit(true);
        this.subs.push(
          this.downloadFileAttachService.doImport(this.urlApiImport, formData).subscribe(res => {
            this.onLoadPage.emit(false);
            if (res != undefined && res.code != HTTP_STATUS_CODE.OK) {
              this.errorList = res.data?.errorList;
              if (this.errorList != undefined && this.errorList.length > 0) {
                this.isModalError = true;
                this.fileName = res.data.errorFile;
                this.toastrService.error(this.translate.instant('personalTax.notification.upload.error'));
              } else {
                this.isModalError = false;
                this.errorList = [];
                this.fileName = undefined;
                this.toastrService.error(res.message);
              }
            } else {
              this.isModalError = false;
              this.errorList = [];
              this.fileName = undefined;
              this.doClose(true);
              this.toastrService.success(this.translate.instant('personalTax.notification.upload.success'));
            }
          }, (error) => {
            this.onLoadPage.emit(false);
            this.toastrService.error(error?.message);
          })
        );
    }

    doRemoveFile = (): boolean => {
        this.fileList = [];
        this.isExistFileImport = false;
        return true;
    };

    ngOnDestroy(): void {
      this.subs?.forEach(sub => sub.unsubscribe());
    }

}
