import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImportCommonComponent } from './import-common/import-common.component';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { TranslateModule } from '@ngx-translate/core';
import { NzFormModule } from 'ng-zorro-antd/form';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { NzTableModule } from 'ng-zorro-antd/table';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NzButtonModule} from "ng-zorro-antd/button";

@NgModule({
  imports: [CommonModule, NzModalModule, TranslateModule, NzFormModule, SharedUiMbButtonModule, NzDatePickerModule,
    NzIconModule, NzUploadModule, NzTableModule, FormsModule, ReactiveFormsModule, NzButtonModule],
  declarations: [ImportCommonComponent],
  exports: [ImportCommonComponent],
})
export class PersonalTaxUiImportCommonModule {}
