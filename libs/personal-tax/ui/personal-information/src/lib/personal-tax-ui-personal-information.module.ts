import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PersonalInformationComponent} from "./personal-information/personal-information.component";
import {NzFormModule} from "ng-zorro-antd/form";
import {ReactiveFormsModule} from "@angular/forms";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbTextLabelModule} from "@hcm-mfe/shared/ui/mb-text-label";
import {SharedUiEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/employee-data-picker";

@NgModule({
    imports: [CommonModule, NzFormModule, ReactiveFormsModule, TranslateModule, SharedUiMbTextLabelModule,
      SharedUiEmployeeDataPickerModule],
  declarations: [PersonalInformationComponent],
  exports: [PersonalInformationComponent],
})
export class PersonalTaxUiPersonalInformationModule {}
