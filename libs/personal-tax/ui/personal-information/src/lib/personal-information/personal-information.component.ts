import {Component, EventEmitter, Input, OnDestroy, Output} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {InputBoolean} from 'ng-zorro-antd/core/util';
import {Subscription} from 'rxjs';
import {PersonalInfo} from "@hcm-mfe/personal-tax/data-access/models";
import {PersonalInfoService} from "@hcm-mfe/personal-tax/data-access/services";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {Scopes} from "@hcm-mfe/shared/common/enums";

@Component({
  selector: 'app-personal-information',
  templateUrl: './personal-information.component.html',
  styleUrls: ['./personal-information.component.scss']
})
export class PersonalInformationComponent implements OnDestroy {


  @Output() getEmployee = new EventEmitter<PersonalInfo | undefined>();
  @Input() isSubmitted = false;
  @Input() isShowTaxNo = true;
  @Input() functionCode?: string;
  @Input() scope = Scopes.VIEW;
  @Input() form: FormGroup | undefined;

  @Input() @InputBoolean() isCheckEmployeeHasTaxNumber = false;
  @Input() @InputBoolean() isCheckEmployeeNotHaveTaxNumber = false; // ktra neu nhan vien chua co mst

  personalInfo: PersonalInfo | undefined;
  subs: Subscription[] = [];

  constructor(
    private personalInfoService: PersonalInfoService,
    private toastrService: ToastrService
  ) {
  }

  selectEmp(event: NzSafeAny) {
    if (event) {
      this.subs.push(
        this.personalInfoService.getPersonalInfo(event.employeeId).subscribe(res => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.personalInfo = res.data;
            this.getEmployee.emit(res.data);
          }
        }, error => {
          this.toastrService.error(error.message);
        })
      );
    } else {
      this.personalInfo = undefined;
    }
  }

  ngOnDestroy(): void {
    this.subs?.forEach(sub => sub.unsubscribe());
  }

  onEmployeeChange(event: NzSafeAny) {
    if (!event) {
      this.getEmployee.emit(undefined);
    }
  }

}
