import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchFormComponent } from './search-form/search-form.component';
import { SharedUiOrgDataPickerModule } from '@hcm-mfe/shared/ui/org-data-picker';
import { SharedUiMbSelectCheckAbleModule } from '@hcm-mfe/shared/ui/mb-select-check-able';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { SharedUiMbDatePickerModule } from '@hcm-mfe/shared/ui/mb-date-picker';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzFormModule } from 'ng-zorro-antd/form';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [CommonModule, SharedUiOrgDataPickerModule, SharedUiMbSelectCheckAbleModule, SharedUiMbInputTextModule,
    SharedUiMbDatePickerModule, FormsModule, ReactiveFormsModule, NzFormModule, TranslateModule],
  declarations: [SearchFormComponent],
  exports: [SearchFormComponent],
})
export class PersonalTaxUiSearchFormModule {}
