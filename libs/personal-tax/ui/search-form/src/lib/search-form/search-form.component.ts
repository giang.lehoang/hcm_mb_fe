import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { HttpParams } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { SearchFormService } from '@hcm-mfe/personal-tax/data-access/services';
import { Constant, UrlConstant } from '@hcm-mfe/personal-tax/data-access/common';
import { HTTP_STATUS_CODE } from '@hcm-mfe/shared/common/constants';
import { StringUtils } from '@hcm-mfe/shared/common/utils';
import {Scopes} from "@hcm-mfe/shared/common/enums";

export class CatalogModel {
  value: number | string | null;
  label: string;

  constructor(label: string, value: any) {
    this.value = value;
    this.label = label;
  }
}

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {
  @Input() isShowRegisterStatus = true;
  @Input() isShowConfirmStatus = false;
  @Input() isShowRegisterType = false;
  @Input() functionCode = '';
  @Input() scope = Scopes.VIEW;
  @Input() isShowYear = false;
  @Input() isAddStatus = false;
  @Input() defaultListEmpStatus = [Constant.STATUSES_EMPLOYEE[0].value];

  listEmpTypeCode: CatalogModel[] = [];
  listPosition: CatalogModel[] = [];
  statusRegisterList: CatalogModel[] = [];
  statusEmployeeList: CatalogModel[] = [];
  registerTypeList: CatalogModel[] = [];
  confirmStatusList: CatalogModel[] = [];
  registerInfoList: CatalogModel[] = [];
  subscriptions: Subscription[] = [];
  form!: FormGroup;

  constructor(private fb: FormBuilder,
              private translate: TranslateService,
              private toastrService: ToastrService,
              private searchFormService: SearchFormService) {}

  ngOnInit(): void {
    this.initForm();
    this.getListEmpTypeCode();
    this.getListPosition();
    this.initDataSelect();
    this.getConfirmStatusList();
  }

  initForm() {
    this.form = this.fb.group({
      empInfo: null,
      orgId: null,
      listEmpStatus: [this.defaultListEmpStatus],
      listEmpTypeCode: null,
      listPositionId: null,
      listStatus: null,
      fromDate: null,
      toDate: null,
      listRegType:null,
      dependentTaxNo: null,
      dependentPersonCode: null,
      year: null,
      listAccepted: null
    });
  }

  getListEmpTypeCode() {
    const params = new HttpParams().set('typeCode', Constant.TYPE_CODE.DOI_TUONG_CV);
    this.subscriptions.push(
      this.searchFormService.getListValue(UrlConstant.CATEGORY.LOOKUP_VALUE, params).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listEmpTypeCode = res.data;
        }
      }, error => {
        this.toastrService.error(error.message);
        this.listEmpTypeCode = [];
      })
    );
  }

  getConfirmStatusList() {
    this.confirmStatusList = Constant.CONFIRM_PROVIDE_STATUS.map(item => {
      item.label = this.translate.instant(item.label);
      return item;
    })

    this.registerInfoList = Constant.CONFIRM_PROVIDE_INFO.map(item => {
      item.label = this.translate.instant(item.label);
      return item;
    })
  }

  getListPosition() {
    const params = new HttpParams().set('jobType', Constant.JOB_TYPE.ORG);
    this.subscriptions.push(
      this.searchFormService.getListValue(UrlConstant.CATEGORY.MP_JOB, params).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listPosition = res.data;
        } else {
          this.toastrService.error(res.message);
        }
      }, error => {
        this.toastrService.error(error.message);
        this.listPosition = [];
      })
    );
  }

  initDataSelect() {
    const listStatus = [];
    Constant.STATUSES_REGISTER.forEach(item => {
      listStatus.push(item);
    });
    if (this.isAddStatus) {
      listStatus.push({label: 'personalTax.taxRegisters.statusSelect.workOutStatus', value: '8', bgColor: '#FDE7EA', color: '#FA0B0B'})
    }
    this.statusRegisterList = listStatus.map(item => {
      item.label = this.translate.instant(item.label);
      return item;
    });

    this.statusEmployeeList = Constant.STATUSES_EMPLOYEE.map(item => {
      item.label = this.translate.instant(item.label);
      return item;
    });

    this.registerTypeList = Constant.DEPENDENT_REGISTER_TYPE.map(item => {
      item.label = this.translate.instant(item.label);
      return item;
    })
  }

  get formControl() {
    return this.form.controls;
  }

  parseOptions() {
    let params = new HttpParams();
    if (!StringUtils.isNullOrEmpty(this.formControl['empInfo'].value))
      params = params.set('empInfo', this.formControl['empInfo'].value);
    if (!StringUtils.isNullOrEmpty(this.formControl['dependentTaxNo'].value))
      params = params.set('dependentTaxNo', this.formControl['dependentTaxNo'].value);
    if (!StringUtils.isNullOrEmpty(this.formControl['dependentPersonCode'].value))
      params = params.set('dependentPersonCode', this.formControl['dependentPersonCode'].value);
    if (this.formControl['orgId'].value !== null)
      params = params.set('orgId', this.formControl['orgId'].value?.orgId);
    if (this.formControl['listPositionId'].value !== null)
      params = params.set('listPositionId', this.formControl['listPositionId'].value.join(','));
    if (this.formControl['listEmpTypeCode'].value !== null)
      params = params.set('listEmpTypeCode', this.formControl['listEmpTypeCode'].value.join(','));
    if (this.formControl['listStatus'].value !== null)
      params = params.set('listStatus', this.formControl['listStatus'].value.join(','));
    if (this.formControl['listRegType'].value !== null)
      params = params.set('listRegType', this.formControl['listRegType'].value.join(','));
    if (this.formControl['listEmpStatus'].value !== null)
      params = params.set('listEmpStatus', this.formControl['listEmpStatus'].value.join(','));
    if (this.formControl['fromDate'].value !== null)
      params = params.set('fromDate', moment(this.formControl['fromDate'].value).format('DD/MM/YYYY'));
    if (this.formControl['toDate'].value !== null)
      params = params.set('toDate', moment(this.formControl['toDate'].value).format('DD/MM/YYYY'));
    if (this.formControl['year'].value !== null)
      params = params.set('year', moment(this.formControl['year'].value).format('YYYY'));
    if (this.formControl['listAccepted'].value !== null)
      params = params.set('listAccepted', this.formControl['listAccepted'].value.join(','));
    return params;
  }

  setFormValue(event: any, formName: string) {
    if(formName !== 'listEmpStatus' || event.listOfSelected.length > 0) {
      this.formControl[formName].setValue(event.listOfSelected);
    }
  }

  ngOnDestroy(): void {
    this.subscriptions?.forEach(sub => sub.unsubscribe());
  }
}
