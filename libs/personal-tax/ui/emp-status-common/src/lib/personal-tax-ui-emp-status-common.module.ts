import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmpStatusCommonComponent } from './emp-status-common/emp-status-common.component';
import { NzTagModule } from 'ng-zorro-antd/tag';

@NgModule({
  imports: [CommonModule, NzTagModule],
  declarations: [EmpStatusCommonComponent],
  exports: [EmpStatusCommonComponent],
})
export class PersonalTaxUiEmpStatusCommonModule {}
