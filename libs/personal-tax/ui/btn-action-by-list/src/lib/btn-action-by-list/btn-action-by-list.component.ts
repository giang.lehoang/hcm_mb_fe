import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import { Subscription } from 'rxjs';
import {ConstantColor} from "@hcm-mfe/personal-tax/data-access/common";
import {TaxCommonService} from "@hcm-mfe/personal-tax/data-access/services";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";

@Component({
  selector: 'app-btn-action-by-list',
  templateUrl: './btn-action-by-list.component.html',
  styleUrls: ['./btn-action-by-list.component.scss']
})
export class BtnActionByListComponent implements OnInit {
  @Input() isDisabled = false;
  @Input() searchFormApprove: FormGroup = this.fb.group([]) ;
  @Input() urlApproveAll = '';
  @Input() urlApproveByList = '';
  @Input() listRegisterId: number[] = [];
  @Output() doSearch = new EventEmitter<boolean>();
  @Output() openReject = new EventEmitter<boolean>();
  @Output() resetListRegisterId = new EventEmitter<boolean>();
  @Output() onLoadPage = new EventEmitter<boolean>();

  mbButtonBgColor:string = ConstantColor.COLOR.BG_PRIMARY_LIGHT;
  subscriptions: Subscription[] = [];

  constructor(private taxCommonService: TaxCommonService,
              private toastrService: ToastrService,
              private translateService: TranslateService,
              private fb: FormBuilder,
              ) { }

  ngOnInit(): void {
  }

  approveAll() {
    this.onLoadPage.emit(true);
    this.subscriptions.push(
      this.taxCommonService.approveAll(this.urlApproveAll, this.searchFormApprove).subscribe(res => {
        if(res.code === HTTP_STATUS_CODE.OK) {
          this.doSearch.emit(true);
          this.resetListRegisterId.emit(false);
          this.toastrService.success(this.translateService.instant('common.notification.isApprove'));
        } else {
          this.toastrService.error(res.message);
        }
        this.onLoadPage.emit(false);
      }, error => {
        this.onLoadPage.emit(false);
        this.toastrService.error(error.message);
      })
    );
  }

  approveByList() {
    this.onLoadPage.emit(true);
    this.subscriptions.push(
      this.taxCommonService.approveByList(this.listRegisterId, this.urlApproveByList).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.doSearch.emit(false);
          this.toastrService.success(this.translateService.instant('common.notification.isApprove'));
          this.resetListRegisterId.emit(false);
        } else {
          this.toastrService.error(res.message);
        }
        this.onLoadPage.emit(false);
      },error => {
        this.toastrService.error(error.message);
        this.onLoadPage.emit(false);
      })
    );
  }

  openRejectByList() {
    this.openReject.emit(true);
  }

  ngOnDestroy(): void {
    this.subscriptions?.forEach(sub => sub.unsubscribe());
  }

}
