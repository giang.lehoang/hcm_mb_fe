import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BtnActionByListComponent} from "./btn-action-by-list/btn-action-by-list.component";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  imports: [CommonModule, SharedUiMbButtonModule, TranslateModule],
  declarations: [BtnActionByListComponent],
  exports: [BtnActionByListComponent],
})
export class PersonalTaxUiBtnActionByListModule {}
