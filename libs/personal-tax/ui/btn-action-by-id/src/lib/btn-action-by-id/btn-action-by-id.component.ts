import {Component, EventEmitter, Input, OnDestroy, Output} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {Subscription} from 'rxjs';
import {Constant, ConstantColor} from "@hcm-mfe/personal-tax/data-access/common";
import {InvoiceRequestService, TaxCommonService} from "@hcm-mfe/personal-tax/data-access/services";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {Reject} from "@hcm-mfe/personal-tax/data-access/models";
import {PopupService} from "@hcm-mfe/shared/ui/popup";

@Component({
  selector: 'app-btn-action-by-id',
  templateUrl: './btn-action-by-id.component.html',
  styleUrls: ['./btn-action-by-id.component.scss']
})
export class BtnActionByIdComponent implements OnDestroy {
  @Input() status:number | undefined;
  @Input() registerId: number | undefined;
  @Input() urlApproveByList = '';
  @Input() urlDeleteById = '';
  @Input() showDelete = true;
  @Input() showEdit = true;
  @Input() showDetail = true;
  @Input() showApprove = true;
  @Output() doSearch = new EventEmitter<boolean>();
  @Output() onLoadPage = new EventEmitter<boolean>();
  @Output() openReject = new EventEmitter<Reject>();
  @Output() openModalEdit = new EventEmitter<number>();
  @Output() openModalDetail = new EventEmitter<number>();
  @Input() isCheckApprove = false;
  @Input() showInvoiceRequest = false;
  @Input() invoiceStatus: number | null = null;

  constant = Constant;
  disabledColor:string = ConstantColor.COLOR.DISABLED;
  subscriptions: Subscription[] = [];

  constructor(private taxCommonService: TaxCommonService,
              private toastService: ToastrService,
              private translateService: TranslateService,
              private deletePopup: PopupService,
              private invoiceRequestService: InvoiceRequestService
              ) { }

  approveById(registerId: number, status: number) {
    if (status === Constant.STATUS_OBJ.WAIT_APPROVE) {
      const listId: number[] = [registerId];
      this.onLoadPage.emit(true);
      this.subscriptions.push(
        this.taxCommonService.approveByList(listId, this.urlApproveByList).subscribe(res => {
          this.onLoadPage.emit(false);
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.doSearch.emit(true);
            this.toastService.success(this.translateService.instant('common.notification.isApprove'));
          } else {
            this.toastService.error(res.message)
          }
        },error => {
          this.onLoadPage.emit(false);
          this.toastService.error(error.message);
        })
      );
    }
  }

  openRejectById(registerId: number, status: number) {
    const data: Reject = {registerId, status};
    this.openReject.emit(data);
  }

  deleteById(registerId: number) {
    this.deletePopup.showModal(() => {
      this.onLoadPage.emit(true);
      this.subscriptions.push(
        this.taxCommonService.deleteById(registerId, this.urlDeleteById).subscribe(res => {
          if (res.code == HTTP_STATUS_CODE.OK) {
            this.doSearch.emit(true);
            this.toastService.success(this.translateService.instant('common.notification.deleteSuccess'));
          } else {
            this.toastService.error(res.message);
          }
          this.onLoadPage.emit(false);
        }, error => {
          this.onLoadPage.emit(false);
          this.toastService.error(error.message);
        })
      );
    });
  }

  requestExportInvoice(registerId : number, invoiceStatus: number) {
    if (invoiceStatus === null) {
      this.onLoadPage.emit(true);
      const listId = [registerId];
      this.subscriptions.push(
        this.invoiceRequestService.requestExportInvoiceByList(listId).subscribe(res => {
          this.onLoadPage.emit(false);
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.toastService.success(this.translateService.instant('personalTax.notification.sendRequestSuccess'));
            this.doSearch.emit(true);
          } else {
            this.toastService.error(res?.message);
          }
        }, error => {
          this.onLoadPage.emit(false);
          this.toastService.error(error?.message);
        })
      )
    }
  }

  openEdit(registerId: number, status: number) {
    if (status === Constant.STATUS_OBJ.FINANCIAL_ACCOUNTANT_IS_PROCESSING ||
        status === Constant.STATUS_OBJ.FINANCIAL_ACCOUNTANT_RECEIVED ||
        status === Constant.STATUS_OBJ.WAIT_APPROVE || !status
    ) {
      this.openModalEdit.emit(registerId);
    }
  }

  openDetail(registerId: number) {
    this.openModalDetail.emit(registerId);
  }


  ngOnDestroy(): void {
    this.subscriptions?.forEach(sub => sub.unsubscribe());
  }


}
