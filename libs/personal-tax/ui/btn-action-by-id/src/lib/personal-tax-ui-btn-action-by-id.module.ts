import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BtnActionByIdComponent} from "./btn-action-by-id/btn-action-by-id.component";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {NzIconModule} from "ng-zorro-antd/icon";
import {TranslateModule} from "@ngx-translate/core";
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import {SharedUiPopupModule} from "@hcm-mfe/shared/ui/popup";

@NgModule({
  imports: [CommonModule, NzButtonModule, NzDropDownModule, NzIconModule, TranslateModule, NzPopconfirmModule, NzToolTipModule, SharedUiPopupModule],
  declarations: [BtnActionByIdComponent],
  exports: [BtnActionByIdComponent],
})
export class PersonalTaxUiBtnActionByIdModule {}
