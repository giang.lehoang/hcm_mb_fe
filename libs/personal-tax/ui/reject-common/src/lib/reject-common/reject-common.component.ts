import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import {TaxCommonService} from "@hcm-mfe/personal-tax/data-access/services";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";

@Component({
  selector: 'app-reject-common',
  templateUrl: './reject-common.component.html',
  styleUrls: ['./reject-common.component.scss']
})
export class RejectCommonComponent implements OnInit {
  @Input() isReject: boolean = false;
  @Input() urlRejectById:string = '';
  @Input() urlRejectByList:string = '';
  @Input() isRejectList: boolean = false;
  @Input() listRegisterId: number[] = [];
  @Input() id:number | undefined;
  @Output() onCloseModal: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() resetListDraftId: EventEmitter<boolean> = new EventEmitter<boolean>();

  form: FormGroup;
  isSubmitted:boolean = false;
  subscriptions: Subscription[] = [];
  isLoadingPage = false;

  constructor(private taxCommonService: TaxCommonService,
              private toastrService: ToastrService,
              private fb: FormBuilder,
              private translateService: TranslateService) {
    this.form = this.fb.group({
      rejectReason: [null, [Validators.required, Validators.maxLength(500)]]
    })
  }

  ngOnInit(): void {
  }

  doClose(refresh: boolean = false) {
    this.isReject = false;
    this.isSubmitted = false;
    this.onCloseModal.emit(refresh);
    this.form.controls['rejectReason'].setValue(null);
  }

  doReject(id:number) {
    this.isSubmitted = true;
    if (this.isRejectList) {
      this.rejectByList();
      this.resetListDraftId.emit(false);
    } else {
      this.rejectById(id);
    }
  }

  rejectByList() {
    if(this.form.valid) {
      const rejectReason = this.form.value['rejectReason'];
      this.isLoadingPage = true;
      this.subscriptions.push(
        this.taxCommonService.rejectByList(this.listRegisterId, this.urlRejectByList, rejectReason).subscribe(res => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.toastrService.success(this.translateService.instant('common.notification.isNotApprove'));
            this.doClose(true);
          } else {
            this.toastrService.error(res.message);
          }
          this.isLoadingPage = false;
        }, error => {
          this.toastrService.error(error.message);
          this.isLoadingPage = false;
        })
      );
    }
  }

  rejectById(id:number) {
    if(this.form.valid) {
      const rejectReason = this.form.value['rejectReason'];
      this.isLoadingPage = true;
      this.subscriptions.push(
        this.taxCommonService.rejectById(id, this.urlRejectByList, rejectReason).subscribe(res => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.toastrService.success(this.translateService.instant('common.notification.isNotApprove'));
            this.doClose(true);
          } else {
            this.toastrService.error(res.message);
          }
          this.isLoadingPage = false;
        }, error => {
          this.toastrService.error(error.message);
          this.isLoadingPage = false;
        })
      );
    }
  }

  ngOnDestroy(): void {
    this.subscriptions?.forEach(sub => sub.unsubscribe());
  }

}
