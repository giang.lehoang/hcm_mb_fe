import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RejectCommonComponent} from "./reject-common/reject-common.component";
import {NzModalModule} from "ng-zorro-antd/modal";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NzFormModule} from "ng-zorro-antd/form";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
    imports: [CommonModule, NzModalModule, ReactiveFormsModule, NzFormModule, SharedUiMbInputTextModule, TranslateModule,
        FormsModule, SharedUiLoadingModule],
  declarations: [RejectCommonComponent],
  exports: [RejectCommonComponent],
})
export class PersonalTaxUiRejectCommonModule {}
