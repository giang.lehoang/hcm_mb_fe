import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {NzModalRef, NzModalService} from 'ng-zorro-antd/modal';
import {NzSafeAny} from 'ng-zorro-antd/core/types';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {HttpParams} from '@angular/common/http';
import {Subscription} from 'rxjs';
import {AppFunction, DraftObject, Pagination, TableConfig} from "@hcm-mfe/shared/data-access/models";
import {Reject} from "@hcm-mfe/personal-tax/data-access/models";
import {Constant, ConstantColor, UrlConstant} from "@hcm-mfe/personal-tax/data-access/common";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {SearchFormComponent} from "@hcm-mfe/personal-tax/ui/search-form";
import {
  DownloadFileAttachService,
  SearchFormService,
  TaxCommonService
} from "@hcm-mfe/personal-tax/data-access/services";
import {SessionService} from "@hcm-mfe/shared/common/store";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {AddTaxRegistersComponent} from "../add-tax-registers/add-tax-registers.component";
import {
  doDownloadFileAttach,
  doExportFileUtils,
  openAddModalUtils
} from "@hcm-mfe/personal-tax/data-access/common/utils";
import { MbTableMergeCellComponent } from 'libs/shared/ui/mb-table-merge-cell/src/lib/mb-table-merge-cell/mb-table-merge-cell.component';


@Component({
  selector: 'app-tax-registers',
  templateUrl: './tax-registers.component.html',
  styleUrls: ['./tax-registers.component.scss']
})
export class TaxRegistersComponent implements OnInit {

  pagination = new Pagination();
  searchFormApprove!: FormGroup;
  tableConfig!: TableConfig;
  searchResult: NzSafeAny[] = [];
  constant = Constant;
  modal!: NzModalRef;
  isImportData = false;
  listRegisterId: number[] = [];
  listRegisterObject: DraftObject[] = [];
  isRejectList = false;
  isReject = false;
  isDisabled = false;
  isLoadingPage = false;
  functionCode: string = FunctionCode.PTX_TAX_REGISTER;
  objFunction: AppFunction;
  subscriptions: Subscription[] = [];

  disabledColor:string = ConstantColor.COLOR.DISABLED;

  rejectId: number | undefined;
  urlReject: string = UrlConstant.REJECT_LIST.TAX_REGISTER;
  urlApproveByList = UrlConstant.APPROVE_LIST.TAX_REGISTER;
  urlApproveAll = UrlConstant.APPROVE_ALL.TAX_REGISTER;
  urlDeleteById = UrlConstant.DELETE_DATA.TAX_REGISTER;

  urlApiImport: string = UrlConstant.IMPORT_FORM.TAX_NEW_REGISTER_FORM;
  urlApiDownloadTemp: string = UrlConstant.IMPORT_FORM.TAX_NEW_REGISTER_TEMPLATE;

  @ViewChild('searchForm') searchForm!: SearchFormComponent;
  @ViewChild('statusTmpl', {static: true}) status!: TemplateRef<NzSafeAny>;
  @ViewChild('empStatusTmpl', {static: true}) empStatus!: TemplateRef<NzSafeAny>;
  @ViewChild('table') table!: MbTableMergeCellComponent;
  @ViewChild('actionTmpl', {static: true}) action!: TemplateRef<NzSafeAny>;
  @ViewChild('attachFileTmpl', {static: true}) attachFile!: TemplateRef<NzSafeAny>;

  constructor(private searchFormService: SearchFormService,
              private translateService: TranslateService,
              private modalService: NzModalService,
              private taxCommonService: TaxCommonService,
              public sessionService: SessionService,
              private downloadFileAttachService: DownloadFileAttachService,
              private toastrService: ToastrService) {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.PTX_TAX_REGISTER}`);
  }

  ngOnInit(): void {
    this.initTable();
  }

  ngAfterViewInit() {
    this.doSearch(1);
  }

  doSearch(pageIndex: number, refresh = false) {
    this.pagination.pageNumber = pageIndex;
    let searchParam = this.searchForm? this.searchForm.parseOptions() : new HttpParams();
    searchParam = searchParam.set('regType', Constant.REG_TYPE_CODE.TAX_CREATE);
    this.isLoadingPage = true;
    if (refresh) {
      this.table.resetAllCheckBoxFn();
    }
    this.subscriptions.push(
      this.searchFormService.getFilterResearch(UrlConstant.SEARCH_FORM.TAX_REGISTER, searchParam, this.pagination.getCurrentPage()).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.searchResult = res.data.listData.map((item: NzSafeAny) => {
            item.id = item.taxNumberRegisterId;
            if (item.status && item.status > Constant.STATUS_OBJ.WAIT_APPROVE) { // set disabled status each checkbox in table: optional
              item.disabled = true;
            }
            return item;
          });
          this.tableConfig.pageIndex = pageIndex;
          this.tableConfig.total = res.data.count;
          this.searchFormApprove = this.searchForm.form;
          this.searchFormApprove.addControl('regType', new FormControl(Constant.REG_TYPE_CODE.TAX_CREATE));
        }
        this.isLoadingPage = false;
      }, error => {
        this.toastrService.error(error.message);
      })
    );
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'STT',
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          fixed: true,
          fixedDir: 'left',
          width: 50
        },
        {
          title: 'personalTax.dependent.table.employeeCode',
          field: 'employeeCode', width: 130,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left',
        },
        {
          title: 'personalTax.dependent.table.employeeName',
          fixed: window.innerWidth > 1024,
          fixedDir: 'left',
          field: 'empName', width: 150,
        },
        {
          title: 'personalTax.dependent.table.statusEmployee',
          field: 'empStatus', width: 150,
          tdTemplate: this.empStatus,
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          show: false
        },
        {
          title: 'personalTax.dependent.table.createDate',
          field: 'createDate',
          width: 120,
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        },
        {
          title: 'personalTax.dependent.table.statusRegister',
          field: 'status',
          tdTemplate: this.status,
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          width: 150,
        },
        {
          title: 'personalTax.taxRegisters.paperTypeLabel',
          field: 'idTypeName',
          width: 150,
        },
        {
          title: 'personalTax.taxRegisters.numberLabel',
          field: 'idNo',
          width: 100,
        },
        {
          title: 'personalTax.taxRegisters.dateIssueLabel',
          field: 'idDate',
          width: 100
        },
        {
          title: 'personalTax.taxRegisters.placeIssueLabel',
          field: 'idPlace',
          width: 150
        },
        {
          title: 'personalTax.dependent.table.orgName',
          field: 'orgName', width: 200
        },
        {
          title: 'personalTax.dependent.table.jobName',
          field: 'jobName', width: 150
        },
        {
          title: 'personalTax.dependent.table.employeeType',
          field: 'empTypeName', width: 150,
          show: false
        },
        {
          title: 'personalTax.taxRegisters.rejectReason',
          field: 'rejectReason',
          width: 150
        },
        {
          title: 'personalTax.dependent.table.attachFile',
          field: 'attachFileList',
          width: 250,
          show: true,
          tdTemplate: this.attachFile
        },
        {
          title: ' ',
          field: 'action',
          tdClassList: ['text-nowrap','text-center'], thClassList: ['text-nowrap','text-center'],
          width: 50,
          tdTemplate: this.action,
          show: this.objFunction?.approve || this.objFunction?.delete || this.objFunction?.edit,
          fixed: window.innerWidth > 1024,
          fixedDir: 'right'
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      showSelect: true,
      pageSize: this.pagination.pageSize,
      pageIndex: this.pagination.pageNumber
    };
  }

  doCloseImport(refresh: boolean) {
    this.isImportData = false;
    if (refresh) {
      this.doSearch(this.pagination.pageNumber)
    }
  }

  doImportData() {
    this.isImportData = true;
  }


  openRejectById(data: Reject) {
    if(data.status === Constant.STATUS_OBJ.WAIT_APPROVE) {
      this.isReject = true;
      this.rejectId = data.registerId;
    }
  }

  openRejectByList() {
    const listRejected:string[] = [];
    let error = false;
    this.listRegisterObject.forEach((data:DraftObject) => {
      if(data.status === Constant.STATUS_OBJ.FINANCIAL_ACCOUNTANT_RETURN) {
        listRejected.push(data.employeeCode);
        error = true;
      }
    })
    if (error) {
      this.toastrService.error(this.translateService.instant('common.notification.employeeCode') + " {" + listRejected.toString() + "} " + this.translateService.instant('common.notification.rejected'));
    } else {
      this.isReject = true;
      this.isRejectList = true;
    }
  }

  doCloseReject(refresh: boolean){
    this.isReject = false;
    if (refresh) {
      this.doSearch(this.pagination.pageNumber);
    }
  }

  resetListRegisterId() {
    this.listRegisterId = [];
    this.doSearch(this.pagination.pageNumber, true)
  }

  openAddModal() {
    this.modal = openAddModalUtils(null, this.modalService, this.translateService,"personalTax.modalName.taxRegisters", AddTaxRegistersComponent, 1.2)
    this.subscriptions.push(
      this.modal.afterClose.subscribe(res => {
        if (res && res.refresh) {
          this.doSearch(1);
        }
      })
    );
  }

  openModalEdit(registerId: number) {
    this.modal = openAddModalUtils(registerId, this.modalService, this.translateService,"personalTax.modalName.editRegisterNew", AddTaxRegistersComponent, 1.2)
    this.subscriptions.push(
      this.modal.afterClose.subscribe(res => {
        if (res && res.refresh) {
          this.doSearch(1);
        }
      })
    );
  }

  openModalDetail(registerId: number) {
    this.modal = openAddModalUtils(registerId, this.modalService, this.translateService,"personalTax.modalName.detailTaxRegisters", AddTaxRegistersComponent, 1.2, true, this.objFunction)
    this.subscriptions.push(
      this.modal.afterClose.subscribe(res => {
        if (res && res.refresh) {
          this.doSearch(1);
        }
      })
    );
  }

  onDblClickNode(event: NzSafeAny) {
    this.openModalDetail(event?.taxNumberRegisterId);
  }

  doDownloadAttach(file: any) {
    doDownloadFileAttach(~~file?.docId, file?.security, file.fileName, this.downloadFileAttachService, this.toastrService, this.translateService);
  }

  async doExportData(exportType: 'DEFAULT' | 'ACCORDING_TAX_AUTHORITY') {
    let urlExport;
    let fileName;
    if (exportType === 'DEFAULT') {
      fileName = "Danh-sach-dk-cap-moi-mst.xlsx";
      urlExport = UrlConstant.EXPORT_REPORT.TAX_REGISTER;
    } else {
      fileName = "Danh-sach-dk-cap-moi-mst-theo-mau-co-quan-thue.xls";
      urlExport = UrlConstant.EXPORT_REPORT.TAX_REGISTER_ACCORDING_TAX_AUTHORITY;
    }
    this.isLoadingPage = true;
    let searchParam = this.searchForm ? this.searchForm.parseOptions() : new HttpParams();
    searchParam = searchParam.set('regType', Constant.REG_TYPE_CODE.TAX_CREATE);
    this.isLoadingPage = await doExportFileUtils(urlExport, searchParam, this.taxCommonService, this.toastrService, this.translateService, fileName);
  }

  onLoadPage(isLoadPage: boolean) {
    this.isLoadingPage = isLoadPage;
  }

  onAmountSelectedRowChange(event: NzSafeAny) {
    this.listRegisterId = Array.from(event);
  }

  ngOnDestroy(): void {
    this.modal?.destroy();
    this.subscriptions?.forEach(sub => sub.unsubscribe());
  }

}
