import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TaxRegistersComponent} from "./tax-registers/tax-registers.component";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {PersonalTaxUiSearchFormModule} from "@hcm-mfe/personal-tax/ui/search-form";
import {NzFormModule} from "ng-zorro-antd/form";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {TranslateModule} from "@ngx-translate/core";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {PersonalTaxUiBtnActionByListModule} from "@hcm-mfe/personal-tax/ui/btn-action-by-list";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {PersonalTaxUiBtnActionByIdModule} from "@hcm-mfe/personal-tax/ui/btn-action-by-id";
import {PersonalTaxUiImportCommonModule} from "@hcm-mfe/personal-tax/ui/import-common";
import {PersonalTaxUiRejectCommonModule} from "@hcm-mfe/personal-tax/ui/reject-common";
import {NzTagModule} from "ng-zorro-antd/tag";
import {PersonalTaxUiEmpStatusCommonModule} from "@hcm-mfe/personal-tax/ui/emp-status-common";
import {RouterModule} from "@angular/router";
import {PersonalTaxUiPersonalInformationModule} from "@hcm-mfe/personal-tax/ui/personal-information";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {NzDividerModule} from "ng-zorro-antd/divider";
import {SharedDirectivesNumberInputModule} from "@hcm-mfe/shared/directives/number-input";
import {NzUploadModule} from "ng-zorro-antd/upload";
import {NzIconModule} from "ng-zorro-antd/icon";
import {AddTaxRegistersComponent} from "./add-tax-registers/add-tax-registers.component";
import {SharedUiMbTableMergeCellWrapModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell-wrap";
import {SharedUiMbTableMergeCellModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell";

@NgModule({
    imports: [CommonModule, SharedUiLoadingModule, PersonalTaxUiSearchFormModule, NzFormModule, SharedUiMbButtonModule,
        TranslateModule, NzButtonModule, NzDropDownModule, SharedUiMbTableWrapModule, PersonalTaxUiBtnActionByListModule,
        SharedUiMbTableModule, PersonalTaxUiBtnActionByIdModule, PersonalTaxUiImportCommonModule, PersonalTaxUiRejectCommonModule,
        NzTagModule, PersonalTaxUiEmpStatusCommonModule, PersonalTaxUiPersonalInformationModule, ReactiveFormsModule, FormsModule,
        SharedUiMbSelectModule, SharedUiMbInputTextModule, SharedUiMbDatePickerModule, NzDividerModule, SharedDirectivesNumberInputModule, NzUploadModule, NzIconModule,
        RouterModule.forChild([
            {
                path: '',
                component: TaxRegistersComponent
            }
        ]), SharedUiMbTableMergeCellWrapModule, SharedUiMbTableMergeCellModule
    ],
  declarations: [TaxRegistersComponent, AddTaxRegistersComponent],
  exports: [TaxRegistersComponent]
})
export class PersonalTaxFeatureTaxRegistersModule {}
