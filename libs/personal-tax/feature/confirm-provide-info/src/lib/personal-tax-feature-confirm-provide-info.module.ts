import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ConfirmProvideInfoComponent} from "./confirm-provide-info/confirm-provide-info.component";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {PersonalTaxUiSearchFormModule} from "@hcm-mfe/personal-tax/ui/search-form";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {PersonalTaxUiBtnActionByListModule} from "@hcm-mfe/personal-tax/ui/btn-action-by-list";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {PersonalTaxUiRejectCommonModule} from "@hcm-mfe/personal-tax/ui/reject-common";
import {NzTagModule} from "ng-zorro-antd/tag";
import {PersonalTaxUiEmpStatusCommonModule} from "@hcm-mfe/personal-tax/ui/emp-status-common";
import {PersonalTaxUiBtnActionByIdModule} from "@hcm-mfe/personal-tax/ui/btn-action-by-id";
import {RouterModule} from "@angular/router";
import {NzGridModule} from "ng-zorro-antd/grid";
import {SharedUiMbTableMergeCellWrapModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell-wrap";
import {SharedUiMbTableMergeCellModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell";

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, PersonalTaxUiSearchFormModule, SharedUiMbButtonModule, TranslateModule,
    SharedUiMbTableWrapModule, PersonalTaxUiBtnActionByListModule, SharedUiMbTableModule,
    PersonalTaxUiRejectCommonModule, NzTagModule, PersonalTaxUiEmpStatusCommonModule, PersonalTaxUiBtnActionByIdModule,
    RouterModule.forChild([
      {
        path: '',
        component: ConfirmProvideInfoComponent
      }
    ]), NzGridModule, SharedUiMbTableMergeCellWrapModule, SharedUiMbTableMergeCellModule
  ],
  declarations: [ConfirmProvideInfoComponent],
  exports: [ConfirmProvideInfoComponent],
})
export class PersonalTaxFeatureConfirmProvideInfoModule {}
