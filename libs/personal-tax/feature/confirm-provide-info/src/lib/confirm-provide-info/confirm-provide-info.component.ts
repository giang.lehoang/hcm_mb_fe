import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {NzModalRef} from 'ng-zorro-antd/modal';
import {NzSafeAny} from 'ng-zorro-antd/core/types';
import {HttpParams} from '@angular/common/http';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {Subscription} from 'rxjs';
import {FormGroup} from '@angular/forms';
import {AppFunction, DraftObject, Pagination, TableConfig} from "@hcm-mfe/shared/data-access/models";
import {Reject} from "@hcm-mfe/personal-tax/data-access/models";
import {Constant, ConstantColor, UrlConstant} from "@hcm-mfe/personal-tax/data-access/common";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {SearchFormComponent} from "@hcm-mfe/personal-tax/ui/search-form";
import {SearchFormService, TaxCommonService} from "@hcm-mfe/personal-tax/data-access/services";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {SessionService} from "@hcm-mfe/shared/common/store";
import {doExportFileUtils} from "@hcm-mfe/personal-tax/data-access/common/utils";
import { MbTableMergeCellComponent } from 'libs/shared/ui/mb-table-merge-cell/src/lib/mb-table-merge-cell/mb-table-merge-cell.component';

@Component({
  selector: 'app-confirm-provide-info',
  templateUrl: './confirm-provide-info.component.html',
  styleUrls: ['./confirm-provide-info.component.scss']
})
export class ConfirmProvideInfoComponent implements OnInit {

  pagination = new Pagination();
  tableConfig!: TableConfig;
  searchResult: NzSafeAny[] = [];
  constant = Constant;
  modal?: NzModalRef;
  isImportData = false;
  isLoadingPage = false;
  listRegisterId:number[] = [];
  mbButtonBgColor:string = ConstantColor.COLOR.BG_PRIMARY_LIGHT;
  isShowRegisterStatus = false;
  functionCode: string = FunctionCode.PTX_CONFIRM_PROVIDE;
  objFunction: AppFunction;
  subscriptions: Subscription[] = [];

  isReject = false;
  isRejectList = false;
  rejectId?: number;
  isDisabled = false;
  searchFormApprove?: FormGroup;
  listRegisterObject: DraftObject[] = [];
  urlApproveByList = UrlConstant.APPROVE_LIST.CONFIRM_REGISTERS;
  urlApproveAll = UrlConstant.APPROVE_ALL.CONFIRM_REGISTERS;
  urlReject: string = UrlConstant.REJECT_LIST.CONFIRM_REGISTERS;
  urlDeleteById = UrlConstant.DELETE_DATA.CONFIRM_REGISTERS;

  @ViewChild('searchForm') searchForm!: SearchFormComponent;
  @ViewChild('statusTmpl', {static: true}) status!: TemplateRef<NzSafeAny>;
  @ViewChild('empStatusTmpl', {static: true}) empStatus!: TemplateRef<NzSafeAny>;
  @ViewChild('actionTmpl', {static: true}) action!: TemplateRef<NzSafeAny>;
  @ViewChild('table') table!: MbTableMergeCellComponent;

  constructor(private searchFormService: SearchFormService,
              private translateService: TranslateService,
              private toastrService: ToastrService,
              public sessionService: SessionService,
              private taxCommonService: TaxCommonService) {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.PTX_DEPENDENT_REGISTERS}`);
  }

  ngOnInit(): void {
    this.initTable();
  }

  ngAfterViewInit() {
    this.doSearch(1);
  }

  doSearch(pageIndex: number, refresh= false) {
    this.pagination.pageNumber = pageIndex;
    const searchParam = this.searchForm? this.searchForm.parseOptions() : new HttpParams();
    if (refresh) {
      this.table.resetAllCheckBoxFn();
    }
    this.isLoadingPage = true;
    this.subscriptions.push(
      this.searchFormService.getFilterResearch(UrlConstant.SEARCH_FORM.CONFIRM_PROVIDE_INFO, searchParam, this.pagination.getCurrentPage()).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.searchResult = res.data.listData.map((item: any) => {
            item.id = item.confirmRegisterId;
            if (item.status && item.status > Constant.STATUS_OBJ.WAIT_APPROVE) { // set disabled status each checkbox in table: optional
              item.disabled = true;
            }
            item.registerInfo = this.translateService.instant(Constant.CONFIRM_PROVIDE_INFO.find(data => ~~data.value === item.isAccepted)?.label ?? ' ')
            return item;
          });
          this.tableConfig.pageIndex = pageIndex;
          this.tableConfig.total = res.data.count;
        }
        this.isLoadingPage = false;
        this.searchFormApprove = this.searchForm.form;
      }, (error) => {
        this.toastrService.error(error.message);
      })
    );
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'STT',
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          fixed: true,
          fixedDir: 'left',
          width: 50
        },
        {
          title: 'personalTax.dependent.table.employeeCode',
          field: 'employeeCode', width: 130,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left'
        },
        {
          title: 'personalTax.dependent.table.employeeName',
          fixed: window.innerWidth > 1024,
          fixedDir: 'left',
          field: 'empName', width: 150,
        },
        {
          title: 'personalTax.confirmProvide.idNo',
          field: 'idNo',
          width: 100,
        },
        {
          title: 'personalTax.confirmProvide.taxNo',
          field: 'taxNo',
          width: 100,
        },
        {
          title: 'personalTax.confirmProvide.email',
          field: 'email', width: 180,
        },
        {
          title: 'personalTax.confirmProvide.accountNo',
          field: 'accountNo', width: 100,
        },
        {
          title: 'personalTax.dependent.table.statusEmployee',
          field: 'empStatus', width: 150,
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          tdTemplate: this.empStatus,
          show: false
        },
        {
          title: 'personalTax.dependent.table.createDate',
          field: 'createDate',
          width: 120,
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        },
        {
          title: 'personalTax.declarationRegisters.registerInfo',
          field: 'registerInfo',
          width: 150,
        },
        {
          title: 'personalTax.dependent.table.statusRegister',
          field: 'status',
          tdTemplate: this.status,
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          width: 200
        },
        {
          title: 'personalTax.declarationRegisters.note',
          field: 'note',
          width: 150,
        },
        {
          title: 'personalTax.dependent.table.orgName',
          field: 'orgName', width: 250
        },
        {
          title: 'personalTax.dependent.table.jobName',
          field: 'jobName', width: 150
        },
        {
          title: 'personalTax.dependent.table.employeeType',
          field: 'empTypeName', width: 150,
          show: false
        },
        {
          title: ' ',
          field: 'action',
          tdClassList: ['text-nowrap','text-center'], thClassList: ['text-nowrap','text-center'],
          width: 50,
          tdTemplate: this.action,
          fixed: window.innerWidth > 1024,
          fixedDir: 'right',
          show: this.objFunction?.approve || this.objFunction?.delete || this.objFunction?.edit
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      showSelect: true,
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  async doExportData() {
    this.isLoadingPage = true;
    const searchParam = this.searchForm ? this.searchForm.parseOptions() : new HttpParams();
    this.isLoadingPage = await doExportFileUtils(UrlConstant.EXPORT_REPORT.CONFIRM_PROVIDE, searchParam, this.taxCommonService, this.toastrService, this.translateService,'Danh-sach-dk-cung-cap-tt.xlsx');
  }

  openRejectById(data: Reject) {
    if(data.status === Constant.STATUS_OBJ.WAIT_APPROVE) {
      this.isReject = true;
      this.rejectId = data.registerId;
    }
  }

  openRejectByList() {
    const listRejected:string[] = [];
    let error = false;
    this.listRegisterObject.forEach((data:DraftObject) => {
      if(data.status === Constant.STATUS_OBJ.FINANCIAL_ACCOUNTANT_RETURN) {
        listRejected.push(data.employeeCode);
        error = true;
      }
    })
    if (error) {
      this.toastrService.error(this.translateService.instant('common.notification.employeeCode') + " {" + listRejected.toString() + "} " + this.translateService.instant('common.notification.rejected'));
    } else {
      this.isReject = true;
      this.isRejectList = true;
    }
  }

  resetListRegisterId() {
    this.listRegisterId = [];
    this.doSearch(this.pagination.pageNumber, true);
  }

  onAmountSelectedRowChange(event: NzSafeAny) {
    this.listRegisterId = Array.from(event);
  }

  doCloseReject(refresh: boolean){
    this.isReject = false;
    if (refresh) {
      this.doSearch(this.pagination.pageNumber);
    }
  }

  onLoadPage(isLoadPage: boolean) {
    this.isLoadingPage = isLoadPage;
  }

  ngOnDestroy(): void {
    this.modal?.destroy();
    this.subscriptions?.forEach(sub => sub.unsubscribe());
  }
}
