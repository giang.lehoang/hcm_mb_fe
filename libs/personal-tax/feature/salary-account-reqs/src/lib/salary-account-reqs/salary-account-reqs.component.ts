import {AfterViewInit, Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {Subscription} from 'rxjs';
import {NzSafeAny} from 'ng-zorro-antd/core/types';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {HttpParams} from '@angular/common/http';
import {NzModalRef, NzModalService} from 'ng-zorro-antd/modal';
import {BaseResponse, Pagination, TableConfig} from "@hcm-mfe/shared/data-access/models";
import {T24AccountReqRequest, T24AccountReqResponse} from "@hcm-mfe/personal-tax/data-access/models";
import {Constant, ConstantColor, UrlConstant} from '@hcm-mfe/personal-tax/data-access/common';
import {SearchFormService, T24AccountReqService} from "@hcm-mfe/personal-tax/data-access/services";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {openAddModalUtils} from "@hcm-mfe/personal-tax/data-access/common/utils";
import {SearchFormComponent} from '../search-form/search-form.component';
import {AddSalaryAccountReqComponent} from '../add-salary-account-req/add-salary-account-req.component';
import {getTypeExport, Utils} from "@hcm-mfe/shared/common/utils";
import {saveAs} from "file-saver";
import {ContractApproveResponse} from "@hcm-mfe/partnership/data-access/pns-contract/models";
import {
    MbTableMergeCellComponent
} from "../../../../../../shared/ui/mb-table-merge-cell/src/lib/mb-table-merge-cell/mb-table-merge-cell.component";
import {PopupService} from "@hcm-mfe/shared/ui/popup";

@Component({
    selector: 'app-salary-account-reqs',
    templateUrl: './salary-account-reqs.component.html',
    styleUrls: ['./salary-account-reqs.component.scss']
})
export class SalaryAccountReqsComponent implements OnInit, AfterViewInit, OnDestroy {

    tableConfig!: TableConfig;
    dataTable: T24AccountReqResponse[] = [];
    constant = Constant;

    t24AccountReqSet = new Set<number>();

    disabledColor = ConstantColor.COLOR.DISABLED;

    modal!: NzModalRef;


    isLoadingPage = false;
    pagination = new Pagination();
    subs: Subscription[] = [];

    @ViewChild('searchForm') searchForm!: SearchFormComponent;
    @ViewChild('actionTmpl', { static: true }) action!: TemplateRef<NzSafeAny>;
    @ViewChild('statusTmpl', { static: true }) statusTmpl!: TemplateRef<NzSafeAny>;
    @ViewChild('table') table!: MbTableMergeCellComponent;

    constructor(
        private searchFormService: SearchFormService,
        private translate: TranslateService,
        private toastService: ToastrService,
        private translateService: TranslateService,
        private modalService: NzModalService,
        private t24AccountReqService: T24AccountReqService,
        private deletePopup: PopupService,
    ) {
    }

    ngOnInit(): void {
        this.initTable();
    }

    ngAfterViewInit() {
        this.doSearch(1);
    }

    doSearch(pageIndex: number, refresh= false) {
        this.pagination.pageNumber = pageIndex;
        if (this.searchForm?.form.valid) {
            this.tableConfig = { ...this.tableConfig, loading: true };
            this.isLoadingPage = true;
            if (refresh) {
                this.table.resetAllCheckBoxFn();
            }
            const params = this.searchForm.parseOptions();
            this.subs.push(
                this.searchFormService.getFilterResearch(UrlConstant.SEARCH_FORM.T24_ACCOUNT_REQUESTS, params, this.pagination.getCurrentPage()).subscribe({
                    next: (res: BaseResponse) => {
                        if (res.code === HTTP_STATUS_CODE.OK) {
                            this.dataTable = this.processResponse(res?.data.listData);
                            this.tableConfig.total = res.data.count;
                        } else {
                            this.toastService.error(res?.message);
                        }
                        this.tableConfig = { ...this.tableConfig, loading: false };
                        this.isLoadingPage = false;
                    },
                    error: (err) => {
                        this.toastService.error(err?.message);
                        this.tableConfig = { ...this.tableConfig, loading: false };
                        this.isLoadingPage = false;
                    }
                })
            );
        }
    }

    processResponse(responseData: T24AccountReqResponse[]): ContractApproveResponse[]{
        return responseData.map(item => {
            item.id = item.t24AccountReqId;  // set id field for each row in table: required
            if (item.status && item.status !== Constant.T24_ACCOUNT_REQS_STATUSES[1].value) { // set disabled status each checkbox in table: optional
                item.disabled = true;
            }
            this.processErrorWhenCreateAccount(item);  // xử lý trường Chi tiết lỗi khi tạo tài khoản
            this.processDetailMap(item);  // xử lý trường Chi tiết lỗi map tttk vs tk lương
            return item;
        });
    }

    processErrorWhenCreateAccount(item: T24AccountReqResponse) {
        item.reasonCreate = item.reasonCreate ? item.reasonCreate : '';
        item.reasonT24 = item.reasonT24 ? item.reasonT24 : '';

        if (item.reasonCreate.length > 75) {
            item.reasonCreate = item.reasonCreate.substring(0, 75) + '...';
        }

        if (item.reasonT24.length > 75) {
            item.reasonT24 = item.reasonT24.substring(0, 75) + '...';
        }
        item.reasonCreate = (item.reasonCreate + ' - ' + item.reasonT24).trim();
    }

    processDetailMap(item: T24AccountReqResponse) {
        item.reasonMap = item.reasonMap ? item.reasonMap : '';
        item.reasonPer = item.reasonPer ? item.reasonPer : '';

        if (item.reasonMap.length > 75) {
            item.reasonMap = item.reasonMap.substring(0, 75) + '...';
        }

        if (item.reasonPer.length > 75) {
            item.reasonPer = item.reasonPer.substring(0, 75) + '...';
        }
        item.reasonMap = (item.reasonMap + ' - ' + item.reasonPer).trim();
    }

    private initTable(): void {
        this.tableConfig = {
            headers: [
                {
                    title: 'STT',
                    thClassList: ['text-center'],
                    tdClassList: ['text-center'],
                    fixed: true,
                    fixedDir: 'left',
                    width: 50
                },
                {
                    title: 'personalTax.salaryAccountReqs.table.orgNameManage',
                    field: 'orgNameManage', width: 180
                },
                {
                    title: 'personalTax.salaryAccountReqs.table.orgNameLevel1',
                    field: 'orgNameLevel1', width: 180, show: false
                },
                {
                    title: 'personalTax.salaryAccountReqs.table.orgNameLevel2',
                    field: 'orgNameLevel2', width: 180, show: false
                },
                {
                    title: 'personalTax.salaryAccountReqs.table.orgNameLevel3',
                    field: 'orgNameLevel3', width: 180, show: false
                },
                {
                    title: 'personalTax.salaryAccountReqs.table.t24Code',
                    field: 't24Code', width: 150
                },
                {
                    title: 'personalTax.salaryAccountReqs.table.employeeCode',
                    field: 'employeeCode', width: 130,
                },
                {
                    title: 'personalTax.salaryAccountReqs.table.employeeName',
                    field: 'empName', width: 150
                },
                {
                    title: 'personalTax.salaryAccountReqs.table.personalIdNo',
                    field: 'personalIdNo', width: 150,
                    show: false
                },
                {
                    title: 'personalTax.salaryAccountReqs.table.customerAccount',
                    field: 'customerAccount', width: 150
                },
                {
                    title: 'personalTax.salaryAccountReqs.table.paymentAccount',
                    field: 'paymentAccount', width: 150
                },
                {
                    title: 'personalTax.salaryAccountReqs.table.registrationStatus',
                    field: 'status', width: 130,
                    thClassList: ['text-center'],
                    tdTemplate: this.statusTmpl
                },
                {
                    title: 'personalTax.salaryAccountReqs.table.salaryAccount',
                    field: 'salaryAccount', width: 150
                },
                {
                    title: 'personalTax.salaryAccountReqs.table.accountOpenedResult',
                    field: 'reasonCreate', width: 200,
                    needBreakword: true
                },
                {
                    title: 'personalTax.salaryAccountReqs.table.accountMapResult',
                    field: 'reasonMap', width: 200,
                    needBreakword: true
                },
                {
                    title: ' ',
                    field: 'action',
                    tdClassList: ['text-nowrap', 'text-center'], thClassList: ['text-nowrap', 'text-center'],
                    width: 50,
                    tdTemplate: this.action,
                    fixed: true,
                    fixedDir: 'right'
                }
            ],
            total: 0,
            needScroll: true,
            loading: false,
            size: 'small',
            showSelect: true,
            pageSize: this.pagination.pageSize,
            pageIndex: 1
        };
    }

    ngOnDestroy(): void {
        this.modal?.destroy();
        this.subs?.forEach(sub => sub?.unsubscribe());
    }

    createRequestAll() {
        const t24AccountReqsDTO: T24AccountReqRequest = {};
        t24AccountReqsDTO.type = this.searchForm.formControl['requestType'].value;
        t24AccountReqsDTO.orgId = this.searchForm.formControl['organizationId'].value?.orgId;
        t24AccountReqsDTO.employeeCode = this.searchForm.formControl['employeeCode'].value;
        t24AccountReqsDTO.empName = this.searchForm.formControl['fullName'].value;
        t24AccountReqsDTO.personalIdNo = this.searchForm.formControl['personalIdNo'].value;
        t24AccountReqsDTO.listStatus = this.searchForm.formControl['listRegistrationStatus'].value;
        t24AccountReqsDTO.fromDate = Utils.convertDateToSendServer(this.searchForm.formControl['fromDate'].value);
        t24AccountReqsDTO.toDate = Utils.convertDateToSendServer(this.searchForm.formControl['toDate'].value);

        this.isLoadingPage = true;
        this.subs.push(
            this.t24AccountReqService.createRequestAll(t24AccountReqsDTO).subscribe({
                next: (res: BaseResponse) => {
                    if (res.code === HTTP_STATUS_CODE.OK) {
                        this.doSearch(1);
                        this.toastService.success(this.translateService.instant('personalTax.notification.createRequestSuccess'));
                    } else {
                        this.toastService.error(res?.message);
                    }
                    this.isLoadingPage = false;
                },
                error: (err) => {
                    this.toastService.error(err?.message);
                    this.isLoadingPage = false;
                }
            })
        );
    }

    createRequest(id: number, status: number) {
        if (status === 1) {
            const listId = [id];
            let params = new HttpParams();
            params = params.set('listId', listId.toString());

            this.isLoadingPage = true;
            this.subs.push(
                this.t24AccountReqService.createRequestByList(params).subscribe({
                    next: (res: BaseResponse) => {
                        if (res.code === HTTP_STATUS_CODE.OK) {
                            this.doSearch(1);
                            this.toastService.success(this.translateService.instant('personalTax.notification.createRequestSuccess'));
                        } else {
                            this.toastService.error(res?.message);
                        }
                        this.isLoadingPage = false;
                    },
                    error: (err) => {
                        this.toastService.error(err?.message);
                        this.isLoadingPage = false;
                    }
                })
            );
        }
    }

    createRequestByList() {
        this.isLoadingPage = true;
        this.subs.push(
            this.t24AccountReqService.createRequestByList(this.getListSelectParam()).subscribe({
                next: (res: BaseResponse) => {
                    if (res.code === HTTP_STATUS_CODE.OK) {
                        this.doSearch(1);
                        this.table.resetAllCheckBoxFn();
                        this.toastService.success(this.translateService.instant('personalTax.notification.createRequestSuccess'));
                    } else {
                        this.toastService.error(res?.message);
                    }
                    this.isLoadingPage = false;
                },
                error: (err) => {
                    this.toastService.error(err?.message);
                    this.isLoadingPage = false;
                }
            })
        );
        this.t24AccountReqSet.clear();
    }

    getListSelectParam(): HttpParams {
        const listId = Array.from(this.t24AccountReqSet);
        let params = new HttpParams();
        params = params.set('listId', listId.toString());
        return params;
    }

    onDeleteClick(id: number, status: number) {
        if(status === 1) {
            this.deletePopup.showModal(() => {
                this.isLoadingPage = true;
                this.subs.push(
                    this.t24AccountReqService.deleteData(id, UrlConstant.DELETE_DATA.T24_ACCOUNT_REQUESTS).subscribe({
                        next: (res: BaseResponse) => {
                            if (res.code === HTTP_STATUS_CODE.OK) {
                                this.doSearch(1);
                                this.toastService.success(this.translateService.instant("personalTax.notification.deleteSuccess"));
                            } else {
                                this.toastService.error(res?.message);
                            }
                        },
                        error: (err) => {
                            this.toastService.error(err?.message);
                        }
                    })
                );
            });
        }
    }

    openAddModal() {
        this.modal = openAddModalUtils(null, this.modalService, this.translateService,"personalTax.modalName.salaryAccountReq", AddSalaryAccountReqComponent, 1.2)
        this.subs.push(
            this.modal.afterClose.subscribe(res => {
                if (res && res.refresh) {
                    this.doSearch(1);
                }
            })
        );
    }

    onDblClickNode(event: NzSafeAny) {
      this.openModalEdit(event?.t24AccountReqId, true);
    }

    openModalEdit(t24AccountReqId: number, isDetail = false, status?: number) {
        if  (isDetail || status === 1)  {
            const title = isDetail ? "personalTax.modalName.salaryAccountReqDetail" : "personalTax.modalName.salaryAccountReqEdit";
            this.modal = this.modalService.create({
                nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
                nzTitle: this.translate.instant(title),
                nzContent: AddSalaryAccountReqComponent,
                nzComponentParams: {
                    t24AccountReqId: t24AccountReqId,
                    isDetail: isDetail
                },
                nzFooter: null
            });

            this.subs.push(
                this.modal.afterClose.subscribe(res => {
                    if (res && res.refresh) {
                        this.doSearch(1);
                    }
                })
            );
        }
    }

    doExportData() {
        this.isLoadingPage = true;
        const searchParam = this.searchForm.parseOptions() ?? new HttpParams();
        this.subs.push(
            this.t24AccountReqService.doExportFile(searchParam).subscribe(res => {
                this.isLoadingPage = false;
                if ((res?.headers?.get('Excel-File-Empty'))) {
                    this.toastService.error(this.translate.instant("common.notification.exportFail"));
                    return;
                } else {
                    const reportFile = new Blob([res.body], {type: getTypeExport('xlsx')});
                    saveAs(reportFile, 'danh_sach_dk_mo_tai_khoan_luong.xlsx');
                }
            })
        );
    }

    onAmountSelectedRowChange(event: NzSafeAny) {
        this.t24AccountReqSet = event as Set<number>;
    }

}
