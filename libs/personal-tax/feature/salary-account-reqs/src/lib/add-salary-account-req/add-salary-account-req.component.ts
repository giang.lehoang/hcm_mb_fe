import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {NzModalRef, NzModalService} from 'ng-zorro-antd/modal';
import {AppFunction, BaseResponse, CatalogModel} from '@hcm-mfe/shared/data-access/models';
import {
  PersonalInfoService,
  SearchFormService,
  T24AccountReqService
} from '@hcm-mfe/personal-tax/data-access/services';
import {Constant} from "@hcm-mfe/personal-tax/data-access/common";
import {StringUtils, Utils} from '@hcm-mfe/shared/common/utils';
import {HTTP_STATUS_CODE} from '@hcm-mfe/shared/common/constants';
import {EmployeeT24Info, T24AccountReqRequest, T24AccountReqResponse} from '@hcm-mfe/personal-tax/data-access/models';
import {catchError, forkJoin, map, of, Subscription} from 'rxjs';
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {HttpParams} from "@angular/common/http";
import {FunctionCode, Scopes} from "@hcm-mfe/shared/common/enums";

@Component({
  selector: 'app-add-salary-account-req',
  templateUrl: './add-salary-account-req.component.html',
  styleUrls: ['./add-salary-account-req.component.scss']
})
export class AddSalaryAccountReqComponent implements OnInit, OnDestroy {

  form!: FormGroup;
  requestTypes: CatalogModel[] = [];
  t24AccountReqId?: number;

  t24AccountReqEdit?: T24AccountReqResponse;
  t24InfoEmpHCM?: EmployeeT24Info;
  t24InfoEmpTax?: T24AccountReqResponse;
  isSubmitted = false;
  subs: Subscription[] = [];
  employeeCode?: string;
  isDetail = false;
  isLoadingPage = false;
  functionCode = FunctionCode.PTX_SALARY_ACCOUNT_REQS;
  scope = Scopes.VIEW;
  objFunction?: AppFunction;

  constructor(
    private fb: FormBuilder,
    private translate: TranslateService,
    private toastService: ToastrService,
    private searchFormService: SearchFormService,
    private modalService: NzModalService,
    private t24AccountRequestService: T24AccountReqService,
    private modalRef: NzModalRef,
    private personalInfoService: PersonalInfoService,
  ) {
  }

  ngOnInit(): void {
    this.initFormGroup();
    this.initDataSelect();
    // Mode = EDIT
    if (this.t24AccountReqId) {
      this.getModelEdit();
    }
  }

  initFormGroup() {
    this.form = this.fb.group({
      employeeId: [null, Validators.required],
      requestType: [0, Validators.required],
      customerAccount: [null, [Validators.required]],
      personalIdNo: [null, [Validators.required, Validators.maxLength(20)]],
      paymentAccount: [null, Validators.required],
      salaryAccount: [null, Validators.required],
      t24Code: [null, Validators.required],
      note: [null]
    });
  }

  getModelEdit() {
    if (this.t24AccountReqId) {
      this.subs.push(
        this.t24AccountRequestService.getById(this.t24AccountReqId).subscribe({
          next: (res: BaseResponse) => {
            if (res.code === HTTP_STATUS_CODE.OK) {
              this.t24AccountReqEdit = res?.data;
              if (this.t24AccountReqEdit?.employeeId) {
                this.getEmployee(this.t24AccountReqEdit.employeeId);
              }
            } else {
              this.toastService.error(res?.message);
            }
          },
          error: (err: NzSafeAny) => {
            this.toastService.error(err?.message);
          }
        })
      );
    }
  }

  getEmployee(employeeId: number) {
    this.subs.push(
      this.personalInfoService.getPersonalInfo(employeeId).subscribe({
        next: (res: BaseResponse) => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.form.controls['employeeId'].setValue(res?.data);
            this.patchValueToForm();
          } else {
            this.toastService.error(res?.data);
          }
        },
        error: (err) => {
          this.toastService.error(err?.message);
        }
      })
    );
  }

  patchValueToForm() {
    this.form.patchValue({
      requestType: this.t24AccountReqEdit?.type,
      customerAccount: this.t24AccountReqEdit?.customerAccount,
      personalIdNo: this.t24AccountReqEdit?.personalIdNo,
      paymentAccount: this.t24AccountReqEdit?.paymentAccount,
      salaryAccount: this.t24AccountReqEdit?.salaryAccount,
      t24Code: this.t24AccountReqEdit?.t24Code ? this.t24AccountReqEdit?.t24Code.replace("VN", "") : null,
      note: this.t24AccountReqEdit?.note,
    });
    if (this.isDetail) {
      this.form.disable();
    }
  }

  initDataSelect() {
    this.requestTypes = Constant.SALARY_ACCOUNT_REQUEST_TYPES.map(item => {
      item.label = this.translate.instant(item.label);
      return item;
    });
  }

  closeModalAdd() {
    this.modalService.closeAll();
  }

  onSave(action?: string) {
    this.isSubmitted = true;
    Utils.processTrimFormControlsBeforeSend(this.form, "customerAccount", "t24Code", "note");
    if (this.form.controls['requestType'].value !== 1) {
      this.form.removeControl("salaryAccount");
    }

    if (this.form.valid) {
      if (!StringUtils.isNullOrEmpty(this.form.controls['t24Code'].value)) {
        this.form.controls['t24Code'].setValue('VN' + this.form.controls['t24Code'].value);
      }
      const request: T24AccountReqRequest = this.form.value;
      request.employeeId = this.form.controls['employeeId'].value.employeeId;
      request.t24AccountReqId = this.t24AccountReqId;
      request.employeeCode = this.employeeCode;
      if (action === 'SEND_BROWSE') {
        request.isSendBrowseAction = 1;
      }

      const formData = new FormData();
      formData.append('data', new Blob([JSON.stringify(request)], {
        type: 'application/json'
      }));
      this.isLoadingPage = true;
      this.subs.push(
        this.t24AccountRequestService.saveRecord(formData).subscribe((res: BaseResponse) => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            if (action === 'SEND_BROWSE') {
              this.toastService.success(this.translate.instant('personalTax.notification.createRequestSuccess'));
            } else {
              this.toastService.success(this.translate.instant('personalTax.notification.saveSuccess'));
            }
            this.modalRef.close({refresh: true});
          } else {
            this.toastService.error(res.message);
          }
          this.isLoadingPage = false;
        }, error => {
          this.isLoadingPage = false;
          this.toastService.error(error.message);
        })
      );
    }
  }

  selectEmp(event: NzSafeAny) {
    if (event) {
      this.employeeCode = event.employeeCode;
      if (!this.t24AccountReqEdit) { // nếu là màn thêm mới
        this.getT24InfoEmployee(event.employeeId);
      }
    }
  }

  getT24InfoEmployee(employeeId: number) {
    const t24InfoHcm = this.t24AccountRequestService.getT24InfoEmployeeHcm(employeeId);
    const t24InfoTax = this.t24AccountRequestService.getT24InfoEmployeeTax(employeeId);
    forkJoin([
      t24InfoHcm.pipe(map((res) => res), catchError(() => of(null))),
      t24InfoTax.pipe(map((res) => res), catchError(() => of(null)))
    ]).subscribe(res => this.handleT24DataSuggest(res))
  }

  handleT24DataSuggest(res: BaseResponse[]) {
    const resHcm = res[0];
    const resTax = res[1];
    if (resHcm && resHcm.code === HTTP_STATUS_CODE.OK) {
      this.t24InfoEmpHCM = resHcm?.data;
    }
    if (resTax && resTax.code === HTTP_STATUS_CODE.OK) {
      this.t24InfoEmpTax = resTax?.data;
    }
    this.fillT24InfoToForm();
  }

  fillT24InfoToForm() {
    const requestType = this.form.controls['requestType'].value;
    if (requestType === Constant.SALARY_ACCOUNT_REQUEST_TYPES[0].value) { // Mở tài khoản lương
      this.form.patchValue({
        personalIdNo: this.t24InfoEmpHCM?.personalId,
        customerAccount: this.t24InfoEmpHCM?.accountNo,
        paymentAccount: this.t24InfoEmpHCM?.paymentAccount,
        salaryAccount: this.t24InfoEmpHCM?.salaryAccount,
        t24Code: this.t24InfoEmpHCM?.t24Code?.replace("VN", "")
      });
    } else {  // Gán tài khoản lương với tài khoản thanh toán
      if (this.t24InfoEmpTax) {
        this.form.patchValue({ // ưu tiên t24 tax
          personalIdNo: this.t24InfoEmpTax?.personalIdNo ? this.t24InfoEmpTax?.personalIdNo : this.t24InfoEmpHCM?.personalId,
          customerAccount: this.t24InfoEmpTax?.customerAccount ? this.t24InfoEmpTax?.customerAccount : this.t24InfoEmpHCM?.accountNo,
          salaryAccount: this.t24InfoEmpTax?.salaryAccount ? this.t24InfoEmpTax?.salaryAccount : this.t24InfoEmpHCM?.salaryAccount,
          t24Code: this.t24InfoEmpTax?.t24Code ? this.t24InfoEmpTax?.t24Code?.replace("VN", "") : this.t24InfoEmpHCM?.t24Code?.replace("VN", ""),
        });
      }
    }
  }

  createRequest(id: number) {
    const listId = [id];
    let params = new HttpParams();
    params = params.set('listId', listId.toString());

    this.isLoadingPage = true;
    this.subs.push(
      this.t24AccountRequestService.createRequestByList(params).subscribe({
        next: (res: BaseResponse) => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.toastService.success(this.translate.instant('personalTax.notification.createRequestSuccess'));
            this.modalRef.close({refresh: true});
          } else {
            this.toastService.error(res?.message);
          }
          this.isLoadingPage = false;
        },
        error: (err) => {
          this.toastService.error(err?.message);
          this.isLoadingPage = false;
        }
      })
    );
  }

  onRequestTypeChange(event: number) {
    if (!this.t24AccountReqEdit) {
      const temp = event;
      this.form.reset();
      this.form.controls['requestType'].setValue(temp);
    }
  }

  ngOnDestroy(): void {
    this.subs?.forEach(sub => sub?.unsubscribe());
  }

}
