import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { HttpParams } from '@angular/common/http';
import { Subscription } from 'rxjs';
import {CatalogModel} from "@hcm-mfe/shared/data-access/models";
import {SearchFormService} from "@hcm-mfe/personal-tax/data-access/services";
import {Constant, UrlConstant} from "@hcm-mfe/personal-tax/data-access/common";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {StringUtils, Utils} from "@hcm-mfe/shared/common/utils";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import * as moment from "moment/moment";
import {DateValidator} from "@hcm-mfe/shared/common/validators";
import {FunctionCode, Scopes} from "@hcm-mfe/shared/common/enums";

@Component({
    selector: 'salary-account-reqs-search-form',
    templateUrl: './search-form.component.html',
    styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit, OnDestroy {

    form: FormGroup = this.fb.group([]);
    requestTypes: CatalogModel[] = [];
    registrationStatusData: CatalogModel[] = [];

    listPosition: CatalogModel[] = [];

    functionCodeOrg = FunctionCode.PTX_SALARY_ACCOUNT_REQS;
    scope = Scopes.VIEW;
    subs: Subscription[] = [];

    constructor(
        private fb: FormBuilder,
        private translate: TranslateService,
        private toastService: ToastrService,
        private searchFormService: SearchFormService
    ) {
    }

    ngOnInit(): void {
        this.initFormGroup();
        this.getListPosition();
        this.initDataSelect();
    }

    initDataSelect() {
        this.requestTypes = Constant.SALARY_ACCOUNT_REQUEST_TYPES.map(item => {
            item.label = this.translate.instant(item.label);
            return item;
        });

        this.registrationStatusData = Constant.SALARY_ACCOUNT_STATUSES.map(item => {
            item.label = this.translate.instant(item.label);
            return item;
        });
    }

    getListPosition() {
        const params = new HttpParams().set('jobType', Constant.JOB_TYPE.ORG);
        this.subs.push(
            this.searchFormService.getListValue(UrlConstant.CATEGORY.MP_JOB, params).subscribe(res => {
                if (res.code === HTTP_STATUS_CODE.OK) {
                    this.listPosition = res.data;
                } else {
                    this.toastService.error(res.message);
                }
            }, error => {
                this.toastService.error(error.message);
                this.listPosition = [];
            })
        );
    }

    initFormGroup() {
        this.form = this.fb.group({
            requestType: [0],
            organizationId: [null],
            employeeCode: [null],
            fullName: [null],
            personalIdNo: [null],
            listRegistrationStatus: [null],
            fromDate: [null],
            toDate: [null],
        },{
            validators: [
                DateValidator.validateRangeDate('fromDate', 'toDate', 'rangeDateError')
            ]
        })
    }

    get formControl() {
        return this.form.controls;
    }

    parseOptions() {
        Utils.processTrimFormControlsBeforeSend(this.form, "employeeCode", "fullName");
        let params = new HttpParams();
        if (this.formControl['requestType'].value)
            params = params.set('type', this.formControl['requestType'].value);
        if (this.formControl['organizationId'].value !== null)
            params = params.set('orgId', this.formControl['organizationId'].value?.orgId);
        if (!StringUtils.isNullOrEmpty(this.formControl['employeeCode'].value))
            params = params.set('employeeCode', this.formControl['employeeCode'].value);
        if (!StringUtils.isNullOrEmpty(this.formControl['fullName'].value))
            params = params.set('empName', this.formControl['fullName'].value);
        if (this.formControl['personalIdNo'].value)
            params = params.set('personalIdNo', this.formControl['personalIdNo'].value);
        if (this.formControl['listRegistrationStatus'].value !== null)
            params = params.set('listStatus', this.formControl['listRegistrationStatus'].value.join(','));
        if (this.formControl['fromDate'].value !== null)
            params = params.set('fromDate', moment(this.formControl['fromDate'].value).format('DD/MM/YYYY'));
        if (this.formControl['toDate'].value !== null)
            params = params.set('toDate', moment(this.formControl['toDate'].value).format('DD/MM/YYYY'));
        return params;
    }

    setFormValue(event: NzSafeAny, formName: string) {
        if(event.listOfSelected.length > 0) {
            this.formControl[formName].setValue(event.listOfSelected);
        }
    }

    ngOnDestroy(): void {
        this.subs?.forEach(sub => sub.unsubscribe());
    }

}
