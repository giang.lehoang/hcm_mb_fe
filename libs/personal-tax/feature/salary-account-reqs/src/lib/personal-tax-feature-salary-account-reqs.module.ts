import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SalaryAccountReqsComponent} from "./salary-account-reqs/salary-account-reqs.component";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {TranslateModule} from "@ngx-translate/core";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NzFormModule} from "ng-zorro-antd/form";
import {RouterModule} from "@angular/router";
import {NzTagModule} from 'ng-zorro-antd/tag';
import {NzPopconfirmModule} from 'ng-zorro-antd/popconfirm';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';
import {SharedUiEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/employee-data-picker";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedDirectivesNumberInputModule} from "@hcm-mfe/shared/directives/number-input";
import {SharedUiOrgDataPickerModule} from "@hcm-mfe/shared/ui/org-data-picker";
import {SharedUiMbSelectCheckAbleModule} from "@hcm-mfe/shared/ui/mb-select-check-able";
import {AddSalaryAccountReqComponent} from "./add-salary-account-req/add-salary-account-req.component";
import {SearchFormComponent} from "./search-form/search-form.component";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {SharedUiMbTableMergeCellWrapModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell-wrap";
import {SharedUiMbTableMergeCellModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell";
import {SharedDirectivesNumbericModule} from "@hcm-mfe/shared/directives/numberic";

@NgModule({
    imports: [CommonModule, SharedUiLoadingModule, SharedUiMbButtonModule,
        TranslateModule, NzButtonModule, NzDropDownModule, NzIconModule, NzCheckboxModule, SharedUiMbTableModule, SharedUiMbTableWrapModule,
        FormsModule, NzFormModule, ReactiveFormsModule, SharedUiEmployeeDataPickerModule, SharedUiMbSelectModule, SharedUiMbInputTextModule, SharedDirectivesNumberInputModule, SharedUiOrgDataPickerModule, SharedUiMbSelectCheckAbleModule,
        RouterModule.forChild([
            {
                path: '',
                component: SalaryAccountReqsComponent
            }
        ]), NzTagModule, NzPopconfirmModule, NzToolTipModule, SharedUiMbDatePickerModule, SharedUiMbTableMergeCellWrapModule, SharedUiMbTableMergeCellModule, SharedDirectivesNumbericModule
    ],
  declarations: [SalaryAccountReqsComponent, AddSalaryAccountReqComponent, SearchFormComponent],
  exports: [SalaryAccountReqsComponent],
})
export class PersonalTaxFeatureSalaryAccountReqsModule {}
