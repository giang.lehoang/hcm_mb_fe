import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpParams } from '@angular/common/http';
import * as moment from 'moment';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { Subscription } from 'rxjs';
import {DependentRegisters} from "@hcm-mfe/personal-tax/data-access/models";
import {Constant, UrlConstant} from "@hcm-mfe/personal-tax/data-access/common";
import {
  DependentService,
  DownloadFileAttachService, PersonalInfoService,
  TaxCommonService
} from "@hcm-mfe/personal-tax/data-access/services";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {beforeUploadFile, Utils} from "@hcm-mfe/shared/common/utils";
import {doDownloadFileAttach} from "@hcm-mfe/personal-tax/data-access/common/utils";
import {AppFunction, SelectModal} from "@hcm-mfe/shared/data-access/models";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";

@Component({
  selector: 'app-add-dependent-deduction',
  templateUrl: './add-dependent-deduction.component.html',
  styleUrls: ['./add-dependent-deduction.component.scss']
})
export class AddDependentDeductionComponent implements OnInit {

  form: FormGroup = this.fb.group({
    familyRelationshipId: [null, Validators.required],
    dateOfBirth: null,
    taxNumber: [null, [Validators.minLength(10), Validators.maxLength(10)]],
    note: null,
    fromDate: null,
    toDate: [null, [Validators.required]],
    employee: [null, [Validators.required]],
    rejectReason: [null]
  });
  listDependents: DependentRegisters[] = [];
  isSubmitted = false;
  fileList: NzUploadFile[] = [];
  employeeId: number | undefined;
  constant = Constant;
  data: DependentRegisters | undefined;
  registerId: number | undefined;
  docIdsDelete: number[] = [];
  subscriptions: Subscription[] = [];
  isDetail = false;
  isLoadingPage = false;
  status!: number;
  isReject = false;
  urlReject: string = UrlConstant.REJECT_LIST.DEPENDENT_REGISTER;
  functionCode: string = FunctionCode.PTX_DEPENDENT_REGISTERS;
  objFunction?: AppFunction;

  constructor(
    private toastrService: ToastrService,
    private translate: TranslateService,
    private fb: FormBuilder,
    private dependentService: DependentService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private taxCommonService: TaxCommonService,
    private downloadFileAttachService: DownloadFileAttachService,
    private modalService: NzModalService,
    private modalRef: NzModalRef,
    private personalInfoService: PersonalInfoService
  ) { }

  ngOnInit(): void {
    if (this.registerId) {
      this.pathValueForm().then();
    }
  }

  async pathValueForm() {
    if (this.registerId) {
      this.isLoadingPage = true;
      const res = await this.taxCommonService.getTaxRegisterById(this.registerId, Constant.REG_TYPE.REGISTER_LOW_DP).toPromise();
      this.isLoadingPage = false;
      if (res.code === HTTP_STATUS_CODE.OK) {
        const data = res.data;
        this.status = data.status;

        const employeeRes = await this.personalInfoService.getPersonalInfo(data.employeeId).toPromise();
        if (employeeRes.code === HTTP_STATUS_CODE.OK) {
          this.form.controls['employee'].setValue(employeeRes.data);
        }

        this.form.patchValue(data);
        this.form.controls['taxNumber'].setValue(data.taxNo);
        this.form.controls['dateOfBirth'].setValue(data.dateOfBirth ? moment(data.dateOfBirth, 'DD/MM/YYYY').toDate() : null);
        this.form.controls['toDate'].setValue(data.toDate ? moment(data.toDate, 'DD/MM/YYYY').toDate() : null);

        if (this.isDetail) {
          this.form.disable();
        }

        if (data?.attachFileList && data?.attachFileList.length > 0) {
          data?.attachFileList.forEach((item: any) => {
            this.fileList.push({
              uid: item.docId,
              name: item.fileName,
              thumbUrl: item.security,
              status: 'done'
            });
          });
          this.fileList = [...this.fileList];
        }
      }
    }
  }

  getListDependents(event: any) {
    this.employeeId = event?.employeeId;
    const param = new HttpParams().set("pageSize", 1000);
    this.subscriptions.push(
      this.dependentService.getListDependents(this.employeeId, param).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          const data: DependentRegisters[] = res.data.listData;
          this.listDependents = data.map(item => {
            item.value = item.familyRelationshipId;
            item.label = item.relationTypeName + ": "+ item.fullName;
            return item;
          })
        }
      }, error => {
        this.toastrService.error(error.message)
        this.listDependents = [];
      })
    );
  }

  changeDependents(event: any) {
    if (event.itemSelected) {
      this.form.controls['dateOfBirth'].setValue(event.itemSelected.dateOfBirth ? moment(event.itemSelected.dateOfBirth,'DD/MM/YYYY').toDate() : null);
      this.form.controls['taxNumber'].setValue(event.itemSelected.taxNumber);
      this.form.controls['toDate'].setValue(event.itemSelected.toDate ? moment(event.itemSelected.toDate,'MM/YYYY').toDate() : null);
    }
  }

  setFromDateToForm(event: SelectModal) {
    this.form.controls['fromDate'].setValue(event?.itemSelected?.fromDate ? moment(event.itemSelected.fromDate,'MM/YYYY').toDate() : null);
  }

  beforeUpload = (file: NzUploadFile): boolean => {
    const maxSize: number = 5*1024*1024;
    if (this.fileList.length < 4) {
      this.fileList = beforeUploadFile(file, this.fileList, maxSize, this.toastrService, this.translate, 'common.notification.fileExtensionInvalidTaxRegister', ".TIF", ".JPG", ".GIF", ".PNG", ".PDF", ".DOC", ".DOCX", ".XLS", ".XLSX")
    } else {
      this.toastrService.error(this.translate.instant('personalTax.notification.maxFile'));
    }
    return false;
  }

  downloadFile = (file: NzUploadFile) => {
    doDownloadFileAttach(~~file?.uid, file?.thumbUrl, file.name, this.downloadFileAttachService, this.toastrService, this.translate);
  }

  removeFile = (file: NzUploadFile): boolean => {
    if (file.uid) {
      this.docIdsDelete.push(Number(file.uid));
    }
    const index = this.fileList.indexOf(file);
    this.fileList.splice(index, 1);
    return false;
  }

  onSave(status: number): any {
    this.isSubmitted = true;
    const toDate = this.form.controls['toDate'].value;
    const fromDate = this.form.controls['fromDate'].value;
    if (toDate && toDate <= fromDate) {
      this.toastrService.error(this.translate.instant('common.notification.errorDate'));
      return false;
    }
    if (this.form.valid) {
      const value: DependentRegisters = this.form.value;
      value.status = status;
      value.regType = Constant.REG_TYPE.REGISTER_LOW_DP;
      value.employeeId = this.employeeId;
      value.taxNo = this.form.value['taxNumber'];
      value.docIdsDelete = this.docIdsDelete;
      value.dependentRegisterId = this.registerId;
      value.fromDate = fromDate ? moment(fromDate, 'dd/MM/yyyy').toDate() : null;
      value.toDate = toDate ? moment(toDate, 'dd/MM/yyyy').toDate() : null;
      const formData = new FormData();

      formData.append('data', new Blob([JSON.stringify(value)], {
        type: 'application/json'
      }));

      this.fileList.forEach((nzFile: any) => {
        formData.append('files', nzFile);
      });

      this.isLoadingPage = true;
      this.subscriptions.push(
        this.dependentService.saveDependentRegister(formData).subscribe(res => {
          this.isLoadingPage = false;
          if (res && res.code === HTTP_STATUS_CODE.OK) {
            const notification = this.registerId ? 'common.notification.editSuccess' : 'common.notification.addSuccess'
            this.toastrService.success(this.translate.instant(notification));
	    this.modalRef.close({refresh:true});
          } else {
            this.toastrService.error(this.translate.instant('common.notification.updateError') + ': ' + res?.message);
          }
        }, (error) => {
          this.isLoadingPage = false;
          this.toastrService.error(error.message);
        })
      );
    }
  }

  approveById(registerId: number) {
    this.isLoadingPage = true;
    this.subscriptions.push(
      this.taxCommonService.approveByList([registerId], UrlConstant.APPROVE_LIST.DEPENDENT_REGISTER).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.toastrService.success(this.translate.instant('common.notification.isApprove'));
          this.modalRef.close({refresh: true});
        } else {
          this.toastrService.error(res.message)
        }
        this.isLoadingPage = false;
      },error => {
        this.isLoadingPage = false;
        this.toastrService.error(error.message);
      })
    );
  }

  doCloseReject(refresh: boolean){
    this.isReject = false;
    if (refresh) {
      this.modalRef.close({refresh: true});
    }
  }

  openRejectForm() {
    this.isReject = true;
  }

  closeModalAdd() {
    this.modalService.closeAll();
  }

  ngOnDestroy(): void {
    this.modalRef?.destroy();
    this.subscriptions?.forEach(sub => sub.unsubscribe());
  }

}
