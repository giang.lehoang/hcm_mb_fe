import {Component, OnDestroy, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { HttpParams } from '@angular/common/http';
import * as moment from 'moment';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { Subscription } from 'rxjs';
import {CatalogModel, DependentRegisters} from "@hcm-mfe/personal-tax/data-access/models";
import {AppFunction, BaseResponse, UserLogin} from "@hcm-mfe/shared/data-access/models";
import {Constant, UrlConstant} from "@hcm-mfe/personal-tax/data-access/common";
import {
  DependentService,
  DownloadFileAttachService, PersonalInfoService,
  TaxCommonService
} from "@hcm-mfe/personal-tax/data-access/services";
import {CustomValidators, DateValidator} from "@hcm-mfe/shared/common/validators";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {beforeUploadFile, StringUtils} from "@hcm-mfe/shared/common/utils";
import {doDownloadFileAttach} from "@hcm-mfe/personal-tax/data-access/common/utils";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";

@Component({
  selector: 'app-add-dependent-register',
  templateUrl: './add-dependent-register.component.html',
  styleUrls: ['./add-dependent-register.component.scss']
})
export class AddDependentRegisterComponent implements OnInit, OnDestroy {
  form: FormGroup = this.fb.group({
    familyRelationshipId: [null, [Validators.required]],
    dateOfBirth: [null],
    isHaveIdNo: [true],
    taxNo: [null, [Validators.minLength(10), Validators.maxLength(10)]],
    idNo: [null],
    note: [null],
    fromDate: [null, [Validators.required]],
    toDate: [null],
    codeNo: null,
    bookNo:null,
    nationCode: null,
    provinceCode: null,
    districtCode: null,
    wardCode: null,
    employee: [null, Validators.required],
    rejectReason: [null],
    dependentAge: [null]
  },{
    validators: [
      DateValidator.validateRangeDate('fromDate', 'toDate', 'rangeDateError'),
      CustomValidators.validateOnly9Or12Digit('idNo')
    ]
  })

  modal: NzModalRef | undefined;
  listRelatives: CatalogModel[] = [];
  employeeId: number | undefined;
  userLogin: UserLogin = new UserLogin();

  provinces: CatalogModel[] = [];
  nations: CatalogModel[] = [];
  districts: CatalogModel[] = [];
  wards: CatalogModel[] = [];
  isSubmitted = false;
  fileList: NzUploadFile[] = [];
  data: DependentRegisters | undefined;
  constant = Constant;
  registerId: number | undefined;
  docIdsDelete: number[] = [];
  subscriptions: Subscription[] = [];
  isDetail = false;
  isLoadingPage = false;
  status?: number;
  isReject = false;
  urlReject: string = UrlConstant.REJECT_LIST.DEPENDENT_REGISTER;
  functionCode: string = FunctionCode.PTX_DEPENDENT_REGISTERS;
  isDisabledCheckIdNo = false;
  objFunction?: AppFunction;

  constructor(
    private dependentRegistersService: DependentService,
    private fb: FormBuilder,
    private toastrService: ToastrService,
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private taxCommonService: TaxCommonService,
    private downloadFileAttachService: DownloadFileAttachService,
    private modalService: NzModalService,
    private modalRef: NzModalRef,
    private personalInfoService: PersonalInfoService
  ) { }

  ngOnInit(): void {
    this.getProvinces();
    this.getNations();
    if (this.registerId) {
      this.pathValueForm().then();
    }
  }

  async pathValueForm() {
      if (this.registerId) {
          this.isLoadingPage = true;
          const res = await this.taxCommonService.getTaxRegisterById(this.registerId, Constant.REG_TYPE.REGISTER_NEW_DP).toPromise();
          this.isLoadingPage = false;
          if (res.code === HTTP_STATUS_CODE.OK) {
              const data: DependentRegisters = res.data;
              this.status = data.status;

              if (data.provinceCode) {
                  this.getDistricts(data.provinceCode);
              }
              if (data.districtCode) {
                  this.getWards(data.districtCode);
              }
              this.form.patchValue(data);

              if (data.employeeId) {
                  const employeeRes = await this.personalInfoService.getPersonalInfo(data.employeeId).toPromise();
                  if (employeeRes.code === HTTP_STATUS_CODE.OK) {
                      this.form.controls['employee'].setValue(employeeRes.data);
                  }
              }

              if (data.codeNo) {
                  this.form.controls['isHaveIdNo'].setValue(false);
              }

              this.form.controls['dateOfBirth'].setValue(data.dateOfBirth ? moment(data.dateOfBirth, 'DD/MM/YYYY').toDate() : null);
              this.form.controls['fromDate'].setValue(data.fromDate ? moment(data.fromDate, 'DD/MM/YYYY').toDate() : null);
              this.form.controls['toDate'].setValue(data.toDate ? moment(data.toDate, 'DD/MM/YYYY').toDate() : null);
              this.form.controls['idNo'].setValue(data?.idNo?.toString().trim());
              this. form.controls['taxNo'].setValue(data?.taxNo?.toString().trim());

              if (this.form.controls['dateOfBirth'].value) {
                this.form.controls['dependentAge'].setValue(this.getDependentAge(this.form.controls['dateOfBirth'].value)?.toString());
              }

              if (this.isDetail) {
                this.form.disable();
              }

              if (data?.attachFileList && data?.attachFileList.length > 0) {
                  data?.attachFileList.forEach((item) => {
                      this.fileList.push({
                          uid: item.docId,
                          name: item.fileName,
                          thumbUrl: item.security,
                          status: 'done'
                      });
                  });
                  this.fileList = [...this.fileList];
              }
          } else {
              this.toastrService.error(res.message);
          }
      }
  }

  getDependentAge(dateOfBirth: Date): number {
    const nowDate = new Date();
    let age = 0;
    try {
      age = nowDate.getFullYear() - dateOfBirth?.getFullYear();
    } catch (error) {}
    return age > 0 ? age : 0;
  }

  getListRelatives(event: NzSafeAny) {
    this.employeeId = event?.employeeId;
    const param = new HttpParams().set("pageSize", 1000);
    this.subscriptions.push(
      this.dependentRegistersService.getListRelatives(this.employeeId, param).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listRelatives = res.data.listData.map((item: NzSafeAny) => {
              item.label = item?.relationTypeName + " " + item?.fullName;
              item.value = item.familyRelationshipId;
              item.taxNo = item.taxNumber;
              item.idNo =  item.personalIdNumber;
              return item;
          })
        }
      }, error => {
        this.toastrService.error(error.message);
        this.listRelatives = []
      })
    );
  }

  changeRelatives(event: NzSafeAny) {
    if (event?.itemSelected) {
      this.form.controls['dateOfBirth']?.setValue(event.itemSelected?.dateOfBirth ? moment(event?.itemSelected?.dateOfBirth,'DD/MM/YYYY').toDate() : null);
      this.form.controls['idNo']?.setValue(event?.itemSelected.personalIdNumber?.toString().trim());
      this.form.controls['taxNo']?.setValue(event?.itemSelected.taxNumber?.toString().trim());
    }
  }

  getNations() {
    this.subscriptions.push(
      this.dependentRegistersService.getCatalog(Constant.CATALOGS.QUOC_GIA).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.nations = res.data;
        }
      }, error => {
        this.toastrService.error(error.message);
      })
    );
  }

  getProvinces() {
    this.subscriptions.push(
      this.dependentRegistersService.getCatalog(Constant.CATALOGS.TINH).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.provinces = res.data;
        }
      }, error => this.toastrService.error(error.message))
    );
  }

  getDistricts(parentCode?: string) {
    if (parentCode) {
      this.subscriptions.push(
        this.dependentRegistersService.getCatalog(Constant.CATALOGS.HUYEN, parentCode).subscribe(res => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.districts = res.data;
          }
        }, error => this.toastrService.error(error.message))
      );
    }
  }

  getWards(parentCode?: string) {
    if (parentCode) {
      this.subscriptions.push(
        this.dependentRegistersService.getCatalog(Constant.CATALOGS.XA, parentCode).subscribe(res => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.wards = res.data;
          }
        }, error => this.toastrService.error(error.message))
      );
    }
  }

  changeProvince(event: NzSafeAny) {
    this.getDistricts(event?.itemSelected?.value);
  }

  changeDistrict(event: NzSafeAny) {
    this.getWards(event?.itemSelected?.value);
  }

  checkIsIdNo(event: NzSafeAny) {
    this.setValidateForm(!event, "codeNo", "bookNo", "nationCode", "provinceCode", "districtCode", "wardCode");
  }

  setValidateForm(isError: boolean, ...properties: NzSafeAny) {
    if(isError) {
      properties.forEach((item: NzSafeAny) => {
        this.form.controls[item].setValidators(Validators.required);
      });
    } else {
      properties.forEach((item: NzSafeAny) => {
        this.form.controls[item].setErrors(null);
      });
    }
  }

  save(status:number) {
    this.validateTaxNo();
    this.isSubmitted = true;
    if (this.form.valid && this.fileList?.length > 0) {
      const value: DependentRegisters = this.form.value;
      value.status = status;
      value.regType = Constant.REG_TYPE.REGISTER_NEW_DP;
      value.employeeId = this.employeeId;
      value.dependentRegisterId = this.registerId ? this.registerId : undefined;
      value.docIdsDelete = this.docIdsDelete;
      if (this.form.controls['isHaveIdNo'].value) {
        value.codeNo = null;
      }
      const formData = new FormData();
      formData.append('data', new Blob([JSON.stringify(value)], {
        type: 'application/json'
      }));

      this.fileList.forEach((nzFile: NzSafeAny) => {
        formData.append('files', nzFile);
      });
      this.isLoadingPage = true;
      this.subscriptions.push(
        this.dependentRegistersService.saveDependentRegister(formData).subscribe(res => {
          this.isLoadingPage = false;
          if (res && res.code === HTTP_STATUS_CODE.OK) {
            const notification = this.registerId ? 'common.notification.editSuccess' : 'common.notification.addSuccess'
            this.toastrService.success(this.translate.instant(notification));
            this.modalRef.close({refresh:true});
          } else {
            this.toastrService.error(this.translate.instant('common.notification.updateError') + ': ' + res?.message);
          }
        }, (error) => {
          this.isLoadingPage = false;
          this.toastrService.error(error.message);
        })
      );
    }
  }

  validateTaxNo() {
    if (this.form.value['isHaveIdNo']) {
      this.form.controls['taxNo'].removeValidators(Validators.required);
      if (StringUtils.isNullOrEmpty(this.form.value['idNo']) && StringUtils.isNullOrEmpty(this.form.value['taxNo'])) {
        this.form.controls['taxNo'].addValidators([Validators.required]);
      }
    } else {
      this.form.controls['idNo'].reset();
      this.form.controls['taxNo'].reset();
    }
    this.form.controls['taxNo'].updateValueAndValidity();
  }

  approveById(registerId: number) {
    this.isLoadingPage = true;
    this.subscriptions.push(
      this.taxCommonService.approveByList([registerId], UrlConstant.APPROVE_LIST.DEPENDENT_REGISTER).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.toastrService.success(this.translate.instant('common.notification.isApprove'));
          this.modalRef.close({refresh: true});
        } else {
          this.toastrService.error(res.message)
        }
        this.isLoadingPage = false;
      },error => {
        this.isLoadingPage = false;
        this.toastrService.error(error.message);
      })
    );
  }

  doCloseReject(refresh: boolean){
    this.isReject = false;
    if (refresh) {
      this.modalRef.close({refresh: true});
    }
  }

  openRejectForm() {
    this.isReject = true;
  }


  beforeUpload = (file: NzUploadFile): boolean => {
    const maxSize: number = 5*1024*1024;
    if (this.fileList.length < 4) {
      this.fileList = beforeUploadFile(file, this.fileList, maxSize, this.toastrService, this.translate, 'common.notification.fileExtensionInvalidTaxRegister', ".TIF", ".JPG", ".GIF", ".PNG", ".PDF", ".DOC", ".DOCX", ".XLS", ".XLSX")
    } else {
      this.toastrService.error(this.translate.instant('personalTax.notification.maxFile'));
    }
    return false;
  }

  changeDateOfBirth(event: NzSafeAny) {
    if (!this.form.controls['toDate'].value) {
      const toDate = new Date(event);
      const dateNow = new Date();
      if (event && dateNow.getFullYear() - toDate.getFullYear() < Constant.MAX_AGE_REDUCE) {
        toDate.setFullYear(toDate.getFullYear() + Constant.MAX_AGE_REDUCE);
        this.form.controls['toDate'].setValue(toDate);
      } else {
        this.form.controls['toDate'].setValue(null);
      }
    }

    //tinh tuoi nguoi phu thuoc
    if (event && this.getDependentAge(event) >= 14) {
      this.form.controls['isHaveIdNo'].setValue(true);
      this.isDisabledCheckIdNo = true;
      this.form.controls['idNo'].addValidators(Validators.required);
    } else {
      this.isDisabledCheckIdNo = false;
      this.form.controls['idNo'].removeValidators(Validators.required);
    }
    this.form.controls['idNo'].updateValueAndValidity();
  }

  downloadFile = (file: NzUploadFile) => {
    doDownloadFileAttach(~~file?.uid, file?.thumbUrl, file.name, this.downloadFileAttachService, this.toastrService, this.translate);
  }

  removeFile = (file: NzUploadFile): boolean => {
    if(file.uid) {
      this.docIdsDelete.push(Number(file.uid));
    }
    const index = this.fileList.indexOf(file);
    this.fileList.splice(index, 1);
    return false;
  }

  closeModalAdd() {
    this.modalService.closeAll();
  }

  ngOnDestroy(): void {
    this.modal?.destroy();
    this.subscriptions?.forEach(sub => sub.unsubscribe());
  }
}
