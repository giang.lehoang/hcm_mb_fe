import {AfterViewInit, Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {HttpParams} from '@angular/common/http';
import {AppFunction, DraftObject, Pagination, TableConfig} from "@hcm-mfe/shared/data-access/models";
import {FormGroup} from "@angular/forms";
import {DependentRegisters, Reject} from "@hcm-mfe/personal-tax/data-access/models";
import {Constant, ConstantColor, UrlConstant} from "@hcm-mfe/personal-tax/data-access/common";
import {NzModalRef, NzModalService} from "ng-zorro-antd/modal";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {Subscription} from "rxjs";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {SearchFormComponent} from "@hcm-mfe/personal-tax/ui/search-form";
import {
  DependentService,
  DownloadFileAttachService,
  SearchFormService,
  TaxCommonService
} from "@hcm-mfe/personal-tax/data-access/services";
import {TranslateService} from "@ngx-translate/core";
import {SessionService} from "@hcm-mfe/shared/common/store";
import {ToastrService} from "ngx-toastr";
import * as moment from "moment";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {AddDependentRegisterComponent} from "../add-dependent-register/add-dependent-register.component";
import {AddDependentDeductionComponent} from "../add-dependent-deduction/add-dependent-deduction.component";
import {
  doDownloadFileAttach,
  doExportFileUtils,
  openAddModalUtils
} from "@hcm-mfe/personal-tax/data-access/common/utils";
import { MbTableMergeCellComponent } from 'libs/shared/ui/mb-table-merge-cell/src/lib/mb-table-merge-cell/mb-table-merge-cell.component';

@Component({
  selector: 'app-dependent-registers',
  templateUrl: './dependent-registers.component.html',
  styleUrls: ['./dependent-registers.component.scss']
})
export class DependentRegistersComponent implements OnInit, AfterViewInit, OnDestroy {
  pagination = new Pagination();
  searchFormApprove!: FormGroup;
  tableConfig!: TableConfig;
  searchResult: DependentRegisters[] = [];
  constant = Constant;
  modal!: NzModalRef;
  isImportData = false;
  listRegisterId:number[] = [];
  listRegisterObject: DraftObject[] = [];
  isRejectList = false;
  isReject = false;
  isDisabled = false;
  isLoadingPage = false;
  functionCode: string = FunctionCode.PTX_DEPENDENT_REGISTERS;
  objFunction: AppFunction;
  subscriptions: Subscription[] = [];

  disabledColor:string = ConstantColor.COLOR.DISABLED;

  urlReject: string = UrlConstant.REJECT_LIST.DEPENDENT_REGISTER;
  rejectId?: number;

  urlApproveByList = UrlConstant.APPROVE_LIST.DEPENDENT_REGISTER;
  urlApproveAll = UrlConstant.APPROVE_ALL.DEPENDENT_REGISTER;
  urlDeleteById = UrlConstant.DELETE_DATA.DEPENDENT_REGISTER;

  urlApiImport: string = UrlConstant.IMPORT_FORM.DEPENDENT_REGISTER_FORM;
  urlApiDownloadTemp: string = UrlConstant.IMPORT_FORM.DEPENDENT_REGISTER_TEMPLATE;
  statusRegisterList: NzSafeAny[] = [];

  @ViewChild('searchForm') searchForm!: SearchFormComponent;
  @ViewChild('statusTmpl', {static: true}) status!: TemplateRef<NzSafeAny>;
  @ViewChild('empStatusTmpl', {static: true}) empStatus!: TemplateRef<NzSafeAny>;
  @ViewChild('actionTmpl', {static: true}) action!: TemplateRef<NzSafeAny>;
  @ViewChild('attachFileTmpl', {static: true}) attachFile!: TemplateRef<NzSafeAny>;
  @ViewChild('table') table!: MbTableMergeCellComponent;

  constructor(private searchFormService: SearchFormService,
              private translateService: TranslateService,
              private modalService: NzModalService,
              private taxCommonService: TaxCommonService,
              public sessionService: SessionService,
              private downloadFileAttachService: DownloadFileAttachService,
              private toastrService: ToastrService,
              private dependentService: DependentService) {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.PTX_DEPENDENT_REGISTERS}`);
  }

  ngOnInit(): void {
    this.initTable();
  }

  ngAfterViewInit() {
    this.statusRegisterList = this.searchForm.statusRegisterList;
    this.doSearch(1);
  }

  doSearch(pageIndex: number, refresh = false) {
    this.pagination.pageNumber = pageIndex;
    const searchParam = this.searchForm? this.searchForm.parseOptions() : new HttpParams();
    this.isLoadingPage = true;
    if (refresh) {
      this.table.resetAllCheckBoxFn();
    }
    this.subscriptions.push(
      this.searchFormService.getFilterResearch(UrlConstant.SEARCH_FORM.DEPENDENT_REGISTER, searchParam, this.pagination.getCurrentPage()).subscribe(res => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.searchResult = res.data.listData.map((item: NzSafeAny) => {
              item.id = item.dependentRegisterId;
              if (item.status && item.status > Constant.STATUS_OBJ.WAIT_APPROVE) { // set disabled status each checkbox in table: optional
                item.disabled = true;
              }
              item.toDate = item.toDate ? moment(moment(item.toDate, 'DD/MM/YYYY').toDate()).format('MM/YYYY') : null;
              item.fromDate = item.fromDate ? moment(moment(item.fromDate, 'DD/MM/YYYY').toDate()).format('MM/YYYY') : null;
              item.dependentName = item.relationTypeName + " " + item.dependentName;
              item.regTypeName = this.translateService.instant(Constant.DEPENDENT_REGISTER_TYPE.find(data => data.value === item.regType)?.label ?? '')
              return item;
            });
            this.tableConfig.pageIndex = pageIndex;
            this.tableConfig.total = res.data.count;
            this.searchFormApprove = this.searchForm.form;
          }
          this.tableConfig.loading = false;
          this.isLoadingPage = false;
        }, error => {
          this.toastrService.error(error.message);
          this.tableConfig.loading = true
        }
      )
    );
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'STT',
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          fixed: true,
          fixedDir: 'left',
          width: 50
        },
        {
          title: 'personalTax.dependent.table.employeeCode',
          field: 'employeeCode', width: 130,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left'
        },
        {
          title: 'personalTax.dependent.table.employeeName',
          fixed: window.innerWidth > 1024,
          fixedDir: 'left',
          field: 'empName', width: 150,
        },
        {
          title: 'personalTax.dependent.table.statusEmployee',
          field: 'empStatus', width: 150,
          tdTemplate: this.empStatus,
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          show: false
        },
        {
          title: 'personalTax.dependent.table.registerType',
          field: 'regTypeName',
          width: 150
        },
        {
          title: 'personalTax.dependent.table.createDate',
          field: 'createDate',
          width: 120,
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        },
        {
          title: 'personalTax.dependent.table.statusRegister',
          field: 'status',
          tdTemplate: this.status,
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          width: 150,
        },
        {
          title: 'personalTax.dependent.table.empTaxNo',
          field: 'empTaxNo',
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          width: 150,
        },
        {
          title: 'personalTax.dependent.table.dependentName',
          field: 'dependentName',
          width: 150,
        },
        {
          title: 'personalTax.dependent.table.dependentPersonCode',
          field: 'dependentPersonCode',
          width: 150,
        },
        {
          title: 'personalTax.dependent.table.dateOfBirth',
          field: 'dateOfBirth',
          width: 120,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          pipe: "date: dd/MM/YYYY"
        },
        {
          title: 'personalTax.dependent.table.taxNo',
          field: 'taxNo',
          width: 120,
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        },
        {
          title: 'personalTax.dependent.table.idNo',
          field: 'idNo',
          width: 120,
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        },
        {
          title: 'personalTax.dependent.table.codeNo',
          field: 'codeNo',
          width: 100,
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        },
        {
          title: 'personalTax.dependent.table.bookNo',
          field: 'bookNo',
          width: 100,
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        },
        {
          title: 'personalTax.dependent.table.fromDate',
          field: 'fromDate',
          width: 100,
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        },
        {
          title: 'personalTax.dependent.table.endDate',
          field: 'toDate',
          width: 100,
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        },
        {
          title: 'personalTax.dependent.table.nationName',
          field: 'nationName',
          width: 130,
          show: false
        },
        {
          title: 'personalTax.dependent.table.provinceName',
          field: 'provinceName',
          width: 130,
          show: false
        },
        {
          title: 'personalTax.dependent.table.districtName',
          field: 'districtName',
          width: 130,
          show: false
        },
        {
          title: 'personalTax.dependent.table.wardName',
          field: 'wardName',
          width: 130,
          show: false
        },
        {
          title: 'personalTax.dependent.table.orgName',
          field: 'orgName', width: 250,
          show: false
        },
        {
          title: 'personalTax.dependent.table.jobName',
          field: 'jobName', width: 150,
          show: false
        },
        {
          title: 'personalTax.dependent.table.employeeType',
          field: 'empTypeName', width: 150,
          show: false
        },
        {
          title: 'personalTax.dependent.table.rejectReason',
          field: 'rejectReason',
          width: 150,
          show: false
        },
        {
          title: 'personalTax.dependent.table.attachFile',
          field: 'attachFileList',
          width: 250,
          show: true,
          tdTemplate: this.attachFile
        },
        {
          title: ' ',
          field: 'action',
          tdClassList: ['text-nowrap','text-center'], thClassList: ['text-nowrap','text-center'],
          width: 50,
          tdTemplate: this.action,
          fixed: window.innerWidth > 1024,
          fixedDir: 'right',
          show: this.objFunction?.approve || this.objFunction?.delete || this.objFunction?.edit,
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      showSelect: true,
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  doCloseImport(refresh: boolean) {
    this.isImportData = false;
    if (refresh) {
      this.doSearch(this.pagination.pageNumber)
    }
  }

  doImportData() {
    this.isImportData = true;
  }

  openRejectById(data: Reject) {
    if(data.status === Constant.STATUS_OBJ.WAIT_APPROVE) {
      this.isReject = true;
      this.rejectId = data.registerId;
    }
  }

  openRejectByList() {
    const listRejected:string[] = [];
    let error = false;
    this.listRegisterObject.forEach((data:DraftObject) => {
      if(data.status === Constant.STATUS_OBJ.FINANCIAL_ACCOUNTANT_RETURN) {
        listRejected.push(data.employeeCode);
        error = true;
      }
    })
    if (error) {
      this.toastrService.error(this.translateService.instant('common.notification.employeeCode') + " {" + listRejected.toString() + "} " + this.translateService.instant('common.notification.rejected'));
    } else {
      this.isReject = true;
      this.isRejectList = true;
    }
  }

  doCloseReject(refresh: boolean){
    this.isReject = false;
    if (refresh) {
      this.doSearch(this.pagination.pageNumber);
    }
  }

  resetListRegisterId() {
    this.listRegisterId = [];
    this.doSearch(this.pagination.pageNumber, true);
  }

  openAddModal(regType: string) {
    if (regType === Constant.REG_TYPE_CODE.REGISTER_NEW_DP) {
      this.modal = openAddModalUtils(null, this.modalService, this.translateService,"personalTax.modalName.dependentRegisters", AddDependentRegisterComponent);
    } else {
      this.modal = openAddModalUtils(null, this.modalService, this.translateService,"personalTax.modalName.dependentDeduction", AddDependentDeductionComponent);
    }
    this.subscriptions.push(
      this.modal.afterClose.subscribe(res => {
        if (res && res.refresh) {
          this.doSearch(1);
        }
      })
    );
  }

  openModalEdit(registerId: number, regType: string) {
    if (regType === Constant.REG_TYPE_CODE.REGISTER_NEW_DP) {
      this.modal = openAddModalUtils(registerId, this.modalService, this.translateService,"personalTax.modalName.dependentRegisterEdit", AddDependentRegisterComponent);
    } else {
      this.modal = openAddModalUtils(registerId, this.modalService, this.translateService,"personalTax.modalName.dependentDeductionEdit", AddDependentDeductionComponent);
    }
    this.subscriptions.push(
      this.modal.afterClose.subscribe(res => {
        if (res && res.refresh) {
          this.doSearch(1);
        }
      })
    );
  }

  openModalDetail(registerId: number, regType: string) {
    if (regType === Constant.REG_TYPE_CODE.REGISTER_NEW_DP) {
      this.modal = openAddModalUtils(registerId, this.modalService, this.translateService,"personalTax.modalName.dependentRegisterDetail", AddDependentRegisterComponent, 1.5, true, this.objFunction);
    } else {
      this.modal = openAddModalUtils(registerId, this.modalService, this.translateService,"personalTax.modalName.dependentDeductionDetail", AddDependentDeductionComponent,1.5, true, this.objFunction);
    }
    this.subscriptions.push(
      this.modal.afterClose.subscribe(res => {
        if (res && res.refresh) {
          this.doSearch(1);
        }
      })
    );
  }

  onDblClickNode(event: NzSafeAny) {
    this.openModalDetail(event?.dependentRegisterId, event?.regType);
  }

  doDownloadAttach(file: NzSafeAny) {
     doDownloadFileAttach(~~file?.docId, file?.security, file.fileName, this.downloadFileAttachService, this.toastrService, this.translateService);
  }

  async doExportData(exportType: 'DEFAULT' | 'ACCORDING_TAX_AUTHORITY') {
    let urlExport;
    let fileName;
    if (exportType === 'DEFAULT') {
      fileName = "Danh-sach-dk-nguoi-phu-thuoc.xlsx";
      urlExport = UrlConstant.EXPORT_REPORT.DEPENDENT_REGISTER;
    } else {
      fileName = "Danh-sach-dk-nguoi-phu-thuoc-theo-mau-co-quan-thue.xls";
      urlExport = UrlConstant.EXPORT_REPORT.DEPENDENT_REGISTER_ACCORDING_TAX_AUTHORITY;
    }
    this.isLoadingPage = true;
    const searchParam = this.searchForm ? this.searchForm.parseOptions() : new HttpParams();
    this.isLoadingPage = await doExportFileUtils(urlExport, searchParam, this.taxCommonService, this.toastrService, this.translateService, fileName);
  }

  onLoadPage(isLoadPage: boolean) {
    this.isLoadingPage = isLoadPage;
  }

  onAmountSelectedRowChange(event: NzSafeAny) {
    this.listRegisterId = Array.from(event);
  }

  sendMailConfirmDependent() {
    this.isLoadingPage = true;
    const searchParam = this.searchForm? this.searchForm.parseOptions() : new HttpParams();
    this.subscriptions.push(
      this.dependentService.sendMailConfirmDependent(searchParam).subscribe(res => {
        this.isLoadingPage = false;
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.toastrService.success(this.translateService.instant('personalTax.notification.sendEmailSuccess'))
        } else {
          this.toastrService.error(this.translateService.instant('personalTax.notification.sendEmailError'))
        }
      }, error => {
        this.toastrService.error(error.message);
        this.isLoadingPage = false;
      })
    );
  }

  ngOnDestroy(): void {
    this.modal?.destroy();
    this.subscriptions?.forEach(sub => sub.unsubscribe());
  }

}
