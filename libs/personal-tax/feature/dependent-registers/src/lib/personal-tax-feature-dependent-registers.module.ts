import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DependentRegistersComponent} from "./dependent-registers/dependent-registers.component";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {PersonalTaxUiSearchFormModule} from "@hcm-mfe/personal-tax/ui/search-form";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {TranslateModule} from "@ngx-translate/core";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {NzIconModule} from "ng-zorro-antd/icon";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {PersonalTaxUiBtnActionByListModule} from "@hcm-mfe/personal-tax/ui/btn-action-by-list";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {PersonalTaxUiBtnActionByIdModule} from "@hcm-mfe/personal-tax/ui/btn-action-by-id";
import {PersonalTaxUiImportCommonModule} from "@hcm-mfe/personal-tax/ui/import-common";
import {PersonalTaxUiRejectCommonModule} from "@hcm-mfe/personal-tax/ui/reject-common";
import {NzTagModule} from "ng-zorro-antd/tag";
import {PersonalTaxUiEmpStatusCommonModule} from "@hcm-mfe/personal-tax/ui/emp-status-common";
import {RouterModule} from "@angular/router";
import {PersonalTaxUiPersonalInformationModule} from "@hcm-mfe/personal-tax/ui/personal-information";
import {NzFormModule} from "ng-zorro-antd/form";
import {ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {NzSwitchModule} from "ng-zorro-antd/switch";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedDirectivesNumberInputModule} from "@hcm-mfe/shared/directives/number-input";
import {NzUploadModule} from "ng-zorro-antd/upload";
import {AddDependentDeductionComponent} from "./add-dependent-deduction/add-dependent-deduction.component";
import {AddDependentRegisterComponent} from "./add-dependent-register/add-dependent-register.component";
import {SharedUiMbTableMergeCellWrapModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell-wrap";
import {SharedUiMbTableMergeCellModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell";
import {NzPopconfirmModule} from "ng-zorro-antd/popconfirm";

@NgModule({
    imports: [CommonModule, SharedUiLoadingModule, PersonalTaxUiSearchFormModule, SharedUiMbButtonModule,
        TranslateModule, NzButtonModule, NzDropDownModule, NzIconModule, SharedUiMbTableWrapModule,
        PersonalTaxUiBtnActionByListModule, SharedUiMbTableModule, PersonalTaxUiBtnActionByIdModule,
        PersonalTaxUiImportCommonModule, PersonalTaxUiRejectCommonModule, NzTagModule, PersonalTaxUiEmpStatusCommonModule,
        PersonalTaxUiPersonalInformationModule, NzFormModule, ReactiveFormsModule, SharedUiMbSelectModule, SharedUiMbDatePickerModule, NzSwitchModule, SharedUiMbInputTextModule, SharedDirectivesNumberInputModule, NzUploadModule,
        RouterModule.forChild([
            {
                path: '',
                component: DependentRegistersComponent
            }
        ]), SharedUiMbTableMergeCellWrapModule, SharedUiMbTableMergeCellModule, NzPopconfirmModule
    ],
  declarations: [DependentRegistersComponent, AddDependentDeductionComponent, AddDependentRegisterComponent],
  exports: [DependentRegistersComponent],
})
export class PersonalTaxFeatureDependentRegistersModule {}
