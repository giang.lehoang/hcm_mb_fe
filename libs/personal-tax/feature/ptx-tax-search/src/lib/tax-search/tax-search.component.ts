import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { saveAs } from 'file-saver';
import { HttpParams } from '@angular/common/http';
import { Subscription } from 'rxjs';
import {FunctionCode, Scopes} from "@hcm-mfe/shared/common/enums";
import {AppFunction, Pagination} from "@hcm-mfe/shared/data-access/models";
import {MBTableConfig} from "@hcm-mfe/shared/data-access/models";
import {TaxSearch} from "@hcm-mfe/personal-tax/data-access/models";
import {TaxSearchService} from "@hcm-mfe/personal-tax/data-access/services";
import {ValidateService} from "@hcm-mfe/shared/core";
import {SessionService} from "@hcm-mfe/shared/common/store";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {getTypeExport, StringUtils} from "@hcm-mfe/shared/common/utils";

export class CatalogModel {
  value: number | string;
  label: string;

  constructor(label: any, value: any) {
    this.value = value;
    this.label = label;
  }
}

@Component({
  selector: 'app-tax-search',
  templateUrl: './tax-search.component.html',
  styleUrls: ['./tax-search.component.scss']
})
export class TaxSearchComponent implements OnInit {
  isSubmitted: boolean = false;
  isLoadingPage: boolean = false;
  formSearch: FormGroup;
  empTypeList: CatalogModel[] = [];
  functionCode: string = FunctionCode.PTX_TAX_SEARCH;
  scope = Scopes.VIEW;
  objFunction: AppFunction;
  tableConfig!: MBTableConfig;
  searchResult: TaxSearch[] = [];
  pagination = new Pagination();
  subs: Subscription[] = [];
  listEmpStatus: CatalogModel[] = [
    {
      value: '1',
      label: this.translate.instant('personalTax.taxSearch.label.dropdown.present')
    },
    {
      value: '2',
      label: this.translate.instant('personalTax.taxSearch.label.dropdown.workOut')
    },
    {
      value: '3',
      label: this.translate.instant('personalTax.taxSearch.label.dropdown.contractPending')
    }
  ];

  listTaxNo: CatalogModel[] = [
    {
      value: '1',
      label: this.translate.instant('personalTax.taxSearch.label.dropdown.exist')
    },
    {
      value: '0',
      label: this.translate.instant('personalTax.taxSearch.label.dropdown.emty')
    }
  ];

  constructor(
    private router: Router,
    private toastrService: ToastrService,
    private translate: TranslateService,
    // private searchFormService: SearchFormService,
    private taxSearchService: TaxSearchService,
    public validateService: ValidateService,
    public sessionService: SessionService,
    private fb: FormBuilder
  ) {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.PTX_TAX_SEARCH}`);
    this.formSearch = fb.group({
      orgId: [null],
      listEmpStatus: [null],
      taxNo: [null],
      idNo: [null],
      empCode: [null],
      empName: [null],
      listEmpTypeCode: [null],
      hasTaxNo: [null]
    });
  }

  ngOnInit(): void {
    this.getEmpTypeList();
    this.initTable();
    this.doSearch(1);
  }

  // empTypeList: this.searchFormService.getEmpTypeList();
  getEmpTypeList(){
    this.subs.push(
      this.taxSearchService.getEmpTypeList().subscribe(data => {
        if (data.code === HTTP_STATUS_CODE.OK) {
          this.empTypeList = data.data;
        }
      }, error => {
        this.toastrService.error(error.message);
      })
    );
  }

  get formControl() {
    return this.formSearch.controls;
  }

  parseOptions() {
    let params = new HttpParams();
    if (this.formControl['orgId'].value !== null)
      params = params.append('orgId', this.formControl['orgId'].value['orgId']);
    if (this.formControl['listEmpStatus'].value !== null && this.formControl['listEmpStatus'].value.length > 0)
      params = params.set('listEmpStatus', this.formControl['listEmpStatus'].value.join(','));
    if (!StringUtils.isNullOrEmpty(this.formControl['taxNo'].value))
      params = params.set('taxNo', this.formControl['taxNo'].value);
    if (!StringUtils.isNullOrEmpty(this.formControl['idNo'].value))
      params = params.set('idNo', this.formControl['idNo'].value);
    if (!StringUtils.isNullOrEmpty(this.formControl['empCode'].value))
      params = params.set('empCode', this.formControl['empCode'].value);
    if (!StringUtils.isNullOrEmpty(this.formControl['empName'].value))
      params = params.set('empName', this.formControl['empName'].value);
    if (this.formControl['listEmpTypeCode'].value !== null && this.formControl['listEmpTypeCode'].value.length > 0)
      params = params.set('listEmpTypeCode', this.formControl['listEmpTypeCode'].value.join(','));
    if (this.formControl['hasTaxNo'].value !== null)
      params = params.set('hasTaxNo', this.formControl['hasTaxNo'].value);
    return params;
  }

  doSearch(page: number) {
    this.pagination.pageNumber = page;
    let searchParam = this.parseOptions() ?? new HttpParams();
    this.tableConfig.loading = true;
    this.isLoadingPage = true;
    if(page) {
      searchParam = searchParam.set('startRecord', this.pagination.getCurrentPage().startRecord);
      searchParam = searchParam.set('pageSize', this.pagination.getCurrentPage().pageSize);
    }
    this.subs.push(
      this.taxSearchService.getTaxNo(searchParam).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.searchResult = res.data.listData;
          this.tableConfig.total = res.data.count;
        }
        this.tableConfig.loading = false;
        this.isLoadingPage = false;
      }, (error) => {
        this.toastrService.error(error.message);
        this.tableConfig.loading = true;
        this.isLoadingPage = false;
      })
    );

  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'personalTax.taxSearch.label.table.empCode',
          field: 'empCode',
          tdClassList: ['text-left'], thClassList: ['text-center'],
          fixed: true,
          fixedDir: 'left',
          width: 120
        },
        {
          title: 'personalTax.taxSearch.label.table.empName',
          field: 'empName',
          tdClassList: ['text-left'], thClassList: ['text-center'],
          fixed: true,
          fixedDir: 'left',
          width: 150
        },
        {
          title: 'personalTax.taxSearch.label.table.idNo',
          field: 'idNo',
          tdClassList: ['text-left'], thClassList: ['text-center'],
          show: true,
          width: 120
        },
        {
          title: 'personalTax.taxSearch.label.table.taxNo',
          field: 'taxNo',
          tdClassList: ['text-left'], thClassList: ['text-center'],
          width: 150
        },
        {
          title: 'personalTax.taxSearch.label.table.taxDate',
          field: 'taxDate',
          tdClassList: ['text-center'], thClassList: ['text-center'],
          width: 120
        },
        {
          title: 'personalTax.taxSearch.label.table.taxPlace',
          field: 'taxPlace',
          tdClassList: ['text-nowrap'], thClassList: ['text-center'],
          show: true,
          width: 150
        },
        {
          title: 'personalTax.taxSearch.label.table.empTypeName',
          field: 'empTypeName',
          tdClassList: ['text-left'], thClassList: ['text-center'],
          width: 150
        },
        {
          title: 'personalTax.taxSearch.label.table.positionName',
          field: 'jobName',
          tdClassList: ['text-left'], thClassList: ['text-center'],
          // tdTemplate: this.isPaymentAccount,
          show: true,
          width: 160
        },
        {
          title: 'personalTax.taxSearch.label.table.orgName',
          field: 'orgName',
          width: 250,
          show: true,
          tdClassList: ['text-left'], thClassList: ['text-center']
          // tdTemplate: this.attachFile
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  setFormValue(event: any, formName: string) {
    if(event.listOfSelected.length > 0) {
      this.formControl[formName].setValue(event.listOfSelected);
    }
  }

  doExportData() {
    this.isLoadingPage = true;
    let searchParam = this.parseOptions() ?? new HttpParams();
    this.subs.push(
      this.taxSearchService.doExportFile(searchParam).subscribe(res => {
        this.isLoadingPage = false;
        if (!!(res?.headers?.get('Excel-File-Empty'))) {
          this.toastrService.error(this.translate.instant("common.notification.exportFail"));
          return;
        } else {
          const reportFile = new Blob([res.body], {type: getTypeExport('xlsx')});
          saveAs(reportFile, 'thong_tin_ma_so_thue.xlsx');
        }
      })
    );
  }

  ngOnDestroy(): void {
    this.subs?.forEach(sub => sub.unsubscribe());
  }
}
