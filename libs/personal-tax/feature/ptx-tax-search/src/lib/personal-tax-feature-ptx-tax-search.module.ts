import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TaxSearchComponent} from "./tax-search/tax-search.component";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {NzFormModule} from "ng-zorro-antd/form";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbSelectCheckAbleModule} from "@hcm-mfe/shared/ui/mb-select-check-able";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {SharedUiOrgDataPickerModule} from "@hcm-mfe/shared/ui/org-data-picker";
import {RouterModule} from "@angular/router";

@NgModule({
    imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedUiLoadingModule, NzFormModule, TranslateModule,
      SharedUiMbSelectCheckAbleModule, SharedUiMbInputTextModule, SharedUiMbSelectModule, SharedUiMbButtonModule,
      SharedUiMbTableWrapModule, SharedUiMbTableModule, SharedUiOrgDataPickerModule,
    RouterModule.forChild([
      {
        path: '',
        component: TaxSearchComponent
      }
    ])],
  declarations: [TaxSearchComponent],
  exports: [TaxSearchComponent]
})
export class PersonalTaxFeaturePtxTaxSearchModule {}
