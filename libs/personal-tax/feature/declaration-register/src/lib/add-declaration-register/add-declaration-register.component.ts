import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import * as moment from 'moment';
import { Subscription } from 'rxjs';
import { Constant } from '@hcm-mfe/personal-tax/data-access/common';
import { DeclarationRegister, DeclarationYear } from '@hcm-mfe/personal-tax/data-access/models';
import { DeclarationService, TaxCommonService } from '@hcm-mfe/personal-tax/data-access/services';
import { HTTP_STATUS_CODE } from '@hcm-mfe/shared/common/constants';
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {AppFunction} from "@hcm-mfe/shared/data-access/models";

@Component({
  selector: 'app-add-declaration-register',
  templateUrl: './add-declaration-register.component.html',
  styleUrls: ['./add-declaration-register.component.scss']
})
export class AddDeclarationRegisterComponent implements OnInit {

  form: FormGroup = this.fb.group({
    year: [null, Validators.required],
    isCommit: true,
    note: null,
    methodCode: Constant.IS_AUTHORIZATION,
    isRegisterReceive: false,
    declarationRegisterId: null,
    employee: [null, [Validators.required]]
  });
  isSubmitted = false;
  constant = Constant;
  employeeId: number | undefined;
  registerId:number | undefined;
  listYear: DeclarationYear[] = [];
  currentYear = new Date().getFullYear() - 1;
  subscriptions: Subscription[] = [];
  isDetail = false;
  isLoadingPage = false;
  functionCode: string = FunctionCode.PTX_DECLARATION_REGISTERS;
  objFunction?: AppFunction;

  options = [
    { label: this.translate.instant('personalTax.declarationRegisters.authorization'), value: Constant.IS_AUTHORIZATION },
    { label: this.translate.instant('personalTax.declarationRegisters.selfSettlement'), value: Constant.IS_NOT_AUTHORIZATION },
  ]

  constructor(
    private fb: FormBuilder,
    private toastrService: ToastrService,
    private translate: TranslateService,
    private declarationService: DeclarationService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private taxCommonService: TaxCommonService,
    private modalService: NzModalService,
    private modalRef: NzModalRef
  ) {}

  ngOnInit(): void {
    this.initFormGroup();
    this.getListYear();
    if (this.registerId) {
      this.pathValueForm().then();
    }
  }

  initFormGroup() {
    this.form = this.fb.group({
      year: [null, Validators.required],
      isCommit: true,
      note: null,
      methodCode: Constant.IS_AUTHORIZATION,
      isRegisterReceive: false,
      declarationRegisterId: null,
      employee: [null, [Validators.required]]
    })
  }

  async pathValueForm() {
    if (this.registerId) {
      this.isLoadingPage = true;
      const response = await this.declarationService.getById(this.registerId).toPromise();
      this.isLoadingPage = false;
      if (response.code === HTTP_STATUS_CODE.OK) {
        const data: DeclarationRegister = response.data;
        this.form.patchValue(data);
        this.form.controls['isRegisterReceive'].setValue(data.revInvoice === Constant.REV_INVOICE_STATUS.IS_REV_INVOICE);
        this.subscriptions.push(
          this.declarationService.getEmployeeInfo(response.data.employeeId).subscribe(res => {
            if (res.code === HTTP_STATUS_CODE.OK) {
              this.form.controls['employee'].setValue(res.data);
            }
          }, error => {
            this.isLoadingPage = false;
            this.toastrService.error(error.message)
          })
        );
      }
      if (this.isDetail) {
        this.form.disable();
      }
    }
  }

  getListYear() {
    this.subscriptions.push(
      this.declarationService.getListYear(Constant.REGISTRATION_TYPE.DECLARATION_REGISTER).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listYear = res.data.map((item: NzSafeAny) => {
            item.value = item.year;
            item.label = item.year.toString();
            return item;
          });
        }
      }, error => {
        this.toastrService.error(error.message);
      })
    );
  }

  changeDeclarationYear(event: any) {
    if (event.itemSelected) {
      const currentDate = moment(moment(new Date()).format('DD/MM/YYYY'), 'DD/MM/YYYY').toDate();
      const fromDate = moment(event.itemSelected.fromDate, 'DD/MM/YYYY').toDate();
      const toDate = moment(event.itemSelected.toDate, 'DD/MM/YYYY').toDate();
      if (currentDate < fromDate || currentDate > toDate) {
        this.form.setErrors({errorDate: true})
      } else {
        this.form.setErrors(null);
      }
    }
  }

  getEmployeeId(event: any) {
    this.employeeId = event?.employeeId;
  }

  changeMethodCode(event: any) {
    if (event === Constant.IS_AUTHORIZATION) {
      this.form.controls['isRegisterReceive'].setValue(false);
    }

    if (event === Constant.IS_NOT_AUTHORIZATION) {
        this.form.controls['isRegisterReceive'].setValue(true);
    }
  }

  onSave(): any {
    this.isSubmitted = true;
    if (this.form.errors && this.form.errors['errorDate']) {
      this.toastrService.error(this.translate.instant('personalTax.notification.errorDeclarationDate'));
      return false;
    }
    if (this.form.valid) {
      const data: DeclarationRegister = this.form.value;
      data.employeeId = this.employeeId;
      data.status = Constant.STATUS_OBJ.IS_DECLARING;
      data.regType = Constant.REG_TYPE.REGISTER_TAX_SETTLEMENT;
      data.revInvoice = this.form.controls['isRegisterReceive'].value ? Constant.REV_INVOICE_STATUS.IS_REV_INVOICE : Constant.REV_INVOICE_STATUS.IS_NOT_REV_INVOICE;
      this.isLoadingPage = true;
      this.subscriptions.push(
        this.declarationService.saveData(data).subscribe(res => {
          this.isLoadingPage = false;
          if (res && res.code === HTTP_STATUS_CODE.OK) {
            const notification = this.registerId ? 'common.notification.editSuccess' : 'common.notification.addSuccess'
            this.toastrService.success(this.translate.instant(notification));
            this.modalRef.close({refresh:true});
          } else {
            this.toastrService.error(this.translate.instant('common.notification.updateError') + ': ' + res.message);
          }
        }, error => {
          this.isLoadingPage = false;
          this.toastrService.error(error.message);
        })
      );
    }
  }

  closeModalAdd() {
    this.modalService.closeAll();
  }

  ngOnDestroy(): void {
    this.modalRef?.destroy();
    this.subscriptions?.forEach(sub => sub.unsubscribe());
  }

}
