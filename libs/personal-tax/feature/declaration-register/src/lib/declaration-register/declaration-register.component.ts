import {AfterViewInit, Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { HttpParams } from '@angular/common/http';
import { Subscription } from 'rxjs';
import {AppFunction, MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {DeclarationRegister} from "@hcm-mfe/personal-tax/data-access/models";
import {
  Constant,
  ConstantColor,
  UrlConstant
} from "@hcm-mfe/personal-tax/data-access/common";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {SearchFormComponent} from "@hcm-mfe/personal-tax/ui/search-form";
import {SearchFormService, TaxCommonService} from "@hcm-mfe/personal-tax/data-access/services";
import { SessionService } from "@hcm-mfe/shared/common/store";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {AddDeclarationRegisterComponent} from "../add-declaration-register/add-declaration-register.component";
import {ConfigDateModalComponent} from "../config-date-modal/config-date-modal.component";
import {
  doExportFileUtils,
  openAddModalUtils
} from "@hcm-mfe/personal-tax/data-access/common/utils";

@Component({
  selector: 'app-declaration-register',
  templateUrl: './declaration-register.component.html',
  styleUrls: ['./declaration-register.component.scss']
})
export class DeclarationRegisterComponent implements OnInit, AfterViewInit, OnDestroy {

  pagination = new Pagination();
  tableConfig!: MBTableConfig;
  isShowRegisterStatus = false;
  searchResult: DeclarationRegister[] = [];
  constant = Constant;
  modal!: NzModalRef;
  isImportData = false;
  isLoadingPage = false;
  listRegisterId:number[] = [];
  functionCode: string = FunctionCode.PTX_DECLARATION_REGISTERS;
  objFunction: AppFunction;
  subscriptions: Subscription[] = [];

  urlApiImport: string = UrlConstant.IMPORT_FORM.DECLARATION_REGISTER;
  urlApiDownloadTemp: string = UrlConstant.IMPORT_FORM.DECLARATION_REGISTER_TEMPLATE;
  urlDeleteById: string = UrlConstant.DELETE_DATA.DECLARATION_REGISTER;

  mbButtonBgColor:string = ConstantColor.COLOR.BG_PRIMARY_LIGHT;

  @ViewChild('searchForm') searchForm!: SearchFormComponent;
  @ViewChild('statusTmpl', {static: true}) status!: TemplateRef<NzSafeAny>;
  @ViewChild('empStatusTmpl', {static: true}) empStatus!: TemplateRef<NzSafeAny>;
  @ViewChild('selectTmpl', {static: true}) select!: TemplateRef<NzSafeAny>;
  @ViewChild('actionTmpl', {static: true}) action!: TemplateRef<NzSafeAny>;

  constructor(private searchFormService: SearchFormService,
              private translateService: TranslateService,
              private modalService: NzModalService,
              private taxCommonService: TaxCommonService,
              public sessionService: SessionService,
              private toastrService: ToastrService) {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.PTX_DECLARATION_REGISTERS}`);
  }

  ngOnInit(): void {
    this.initTable();
  }

  ngAfterViewInit() {
    this.doSearch(1);
  }

  doSearch(pageIndex: number) {
    this.pagination.pageNumber = pageIndex;
    const searchParam = this.searchForm? this.searchForm.parseOptions() : new HttpParams();
    this.tableConfig.loading = true;
    this.isLoadingPage = true;
    this.subscriptions.push(
      this.searchFormService.getFilterResearch(UrlConstant.SEARCH_FORM.DECLARATION_REGISTER, searchParam, this.pagination.getCurrentPage()).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.searchResult = res.data.listData.map((item: NzSafeAny) => {
            item.revInvoiceName = item.revInvoice === Constant.REV_INVOICE_STATUS.IS_REV_INVOICE ? Constant.REV_INVOICE_STATUS_NAME.IS_REV_INVOICE : '';
            return item;
          });
          this.tableConfig.pageIndex = pageIndex;
          this.tableConfig.total = res.data.count;
        }
        this.tableConfig.loading = false;
        this.isLoadingPage = false;
      }, error => {
        this.toastrService.error(error.message)
        this.tableConfig.loading = true
      })
    );
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'personalTax.dependent.table.employeeCode',
          field: 'employeeCode', width: 130,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left'
        },
        {
          title: 'personalTax.dependent.table.employeeName',
          fixed: window.innerWidth > 1024,
          fixedDir: 'left',
          field: 'empName', width: 150,
        },
        {
          title: 'personalTax.taxSearch.label.table.taxNo',
          field: 'taxNo', width: 150,
        },
        {
          title: 'personalTax.dependent.table.joinCompanyDate',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          field: 'joinCompanyDate', width: 100,
        },
        {
          title: 'personalTax.dependent.table.statusEmployee',
          field: 'empStatus', width: 150,
          tdTemplate: this.empStatus,
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          show: false
        },
        {
          title: 'personalTax.dependent.table.createDate',
          field: 'createDate',
          width: 120,
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        },
        {
          title: 'personalTax.declarationRegisters.year',
          field: 'year',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 100,
        },
        {
          title: 'personalTax.declarationRegisters.methodCode',
          field: 'methodCode',
          width: 120,
        },
        {
          title: 'personalTax.declarationRegisters.revInvoice',
          field: 'revInvoiceName',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 100,
        },
        {
          title: 'personalTax.declarationRegisters.note',
          field: 'note',
          width: 150,
        },
        {
          title: 'personalTax.dependent.table.orgName',
          field: 'orgName', width: 250
        },
        {
          title: 'personalTax.dependent.table.jobName',
          field: 'jobName', width: 150
        },
        {
          title: 'personalTax.dependent.table.employeeType',
          field: 'empTypeName', width: 150,
          show: false
        },
        {
          title: ' ',
          field: 'action',
          tdClassList: ['text-nowrap','text-center'], thClassList: ['text-nowrap','text-center'],
          width: 60,
          tdTemplate: this.action,
          fixed: window.innerWidth > 1024,
          fixedDir: 'right'
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }


  deleteById(registerId: number) {
    this.subscriptions.push(
      this.taxCommonService.deleteById(registerId, UrlConstant.DELETE_DATA.DECLARATION_REGISTER).subscribe(res => {
        if(res.code === HTTP_STATUS_CODE.OK) {
          this.doSearch(1);
          this.toastrService.success(this.translateService.instant('common.notification.deleteSuccess'));
        } else {
          this.toastrService.error(res.message);
        }
      }, error => {
        this.toastrService.error(error.message);
      })
    );
  }

  onEdit(registerId: number) {
    this.openAddModal(registerId);
  }

  onDetail(registerId: number) {
    this.modal = openAddModalUtils(registerId, this.modalService, this.translateService,"personalTax.modalName.declarationRegistersDetail", AddDeclarationRegisterComponent, 1.5, true)
    this.subscriptions.push(
      this.modal.afterClose.subscribe(res => {
        if (res && res.refresh) {
          this.doSearch(1);
        }
      })
    );
  }

  onDblClickNode(event: NzSafeAny) {
    this.onDetail(event?.declarationRegisterId);
  }


  openAddModal(registerId?: number) {
    this.modal = openAddModalUtils(registerId, this.modalService, this.translateService,"personalTax.modalName.declarationRegisters", AddDeclarationRegisterComponent)
    this.subscriptions.push(
      this.modal.afterClose.subscribe(res => {
        if (res && res.refresh) {
          this.doSearch(1);
        }
      })
    );
  }

  openConfigDateModal() {
    this.modal = openAddModalUtils(null, this.modalService, this.translateService,"personalTax.label.configDate", ConfigDateModalComponent, 2.5);
  }

  async doExportData() {
    this.isLoadingPage = true;
    const searchParam = this.searchForm ? this.searchForm.parseOptions() : new HttpParams();
    this.isLoadingPage = await doExportFileUtils(UrlConstant.EXPORT_REPORT.DECLARATION_REGISTER, searchParam, this.taxCommonService, this.toastrService, this.translateService,'Danh-sach-dk-quyet-toan-thue.xlsx');
  }

  doImportData() {
    this.isImportData = true;
  }

  doCloseImport(refresh: boolean) {
    this.isImportData = false;
    if (refresh) {
      this.doSearch(this.pagination.pageNumber)
    }
  }

  onLoadPage(isLoadPage: boolean) {
    this.isLoadingPage = isLoadPage;
  }

  ngOnDestroy(): void {
    this.modal?.destroy();
    this.subscriptions?.forEach(sub => sub.unsubscribe());
  }

}
