import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DeclarationRegisterComponent} from "./declaration-register/declaration-register.component";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {PersonalTaxUiSearchFormModule} from "@hcm-mfe/personal-tax/ui/search-form";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {NzTagModule} from "ng-zorro-antd/tag";
import {PersonalTaxUiEmpStatusCommonModule} from "@hcm-mfe/personal-tax/ui/emp-status-common";
import {RouterModule} from "@angular/router";
import {PersonalTaxUiImportCommonModule} from "@hcm-mfe/personal-tax/ui/import-common";
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import {PersonalTaxUiPersonalInformationModule} from "@hcm-mfe/personal-tax/ui/personal-information";
import {NzFormModule} from "ng-zorro-antd/form";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {NzRadioModule} from "ng-zorro-antd/radio";
import {NzSwitchModule} from "ng-zorro-antd/switch";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {AddDeclarationRegisterComponent} from "./add-declaration-register/add-declaration-register.component";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {ConfigDateModalComponent} from "./config-date-modal/config-date-modal.component";
import {PersonalTaxUiBtnActionByIdModule} from "@hcm-mfe/personal-tax/ui/btn-action-by-id";

@NgModule({
    imports: [CommonModule, SharedUiLoadingModule, PersonalTaxUiSearchFormModule, SharedUiMbButtonModule,
        TranslateModule, SharedUiMbTableWrapModule, SharedUiMbTableModule, NzIconModule, NzButtonModule,
        NzDropDownModule, NzTagModule, PersonalTaxUiEmpStatusCommonModule, PersonalTaxUiPersonalInformationModule, NzFormModule, ReactiveFormsModule, FormsModule,
        SharedUiMbSelectModule, NzRadioModule, NzSwitchModule, SharedUiMbInputTextModule, SharedUiMbDatePickerModule,
        RouterModule.forChild([
            {
                path: '',
                component: DeclarationRegisterComponent
            }
        ]), PersonalTaxUiImportCommonModule, NzPopconfirmModule, NzToolTipModule, PersonalTaxUiBtnActionByIdModule
    ],
  declarations: [DeclarationRegisterComponent, AddDeclarationRegisterComponent, ConfigDateModalComponent],
  exports: [DeclarationRegisterComponent],
})
export class PersonalTaxFeatureDeclarationRegisterModule {}
