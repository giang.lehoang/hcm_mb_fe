import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzModalService } from 'ng-zorro-antd/modal';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import {DeclarationService} from "@hcm-mfe/personal-tax/data-access/services";
import {DateValidator} from "@hcm-mfe/shared/common/validators";
import {Constant} from "@hcm-mfe/personal-tax/data-access/common";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {DeclarationYear} from "@hcm-mfe/personal-tax/data-access/models";

@Component({
  selector: 'app-config-date-modal',
  templateUrl: './config-date-modal.component.html',
  styleUrls: ['./config-date-modal.component.scss']
})
export class ConfigDateModalComponent implements OnInit {
  form: FormGroup = this.fb.group([]);
  isSubmitted: boolean = false;
  disabledFromDate = false;
  lockRegistrationId?: number;
  subscriptions: Subscription[] = [];
  isLoadingPage = false;

  constructor(private fb: FormBuilder,
              private modalService: NzModalService,
              private translate: TranslateService,
              private declarationService: DeclarationService,
              private toastrService: ToastrService) {
  }

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.form = this.fb.group({
      year: [null, Validators.required],
      fromDate: [null, Validators.required],
      toDate: [null, Validators.required],
      fromRemindDate: [null, Validators.required],
      toRemindDate: [null, Validators.required]
    }, {
      validators: [
        DateValidator.validateRangeDate('fromDate', 'toDate', 'rangeDateError'),
        DateValidator.validateRangeDate('fromRemindDate', 'toRemindDate', 'rangeDateRemindError'),
        DateValidator.validateRangeDate('toRemindDate', 'toDate', 'toDateRemindError'),
        DateValidator.validateRangeDate('fromDate', 'fromRemindDate', 'fromDateRemindError')
      ]
    });
  }

  selectYear(event: any) {
    if(event) {
      const year = event.getFullYear();
      this.subscriptions.push(
        this.declarationService.getListYear(Constant.REGISTRATION_TYPE.DECLARATION_REGISTER, year).subscribe(res => {
          if (res.code === HTTP_STATUS_CODE.OK && res.data.length > 0) {
            const dataDate: DeclarationYear = res.data[0];
            this.lockRegistrationId = dataDate.lockRegistrationId;
            const fromDate: any = dataDate.fromDate ? moment(dataDate.fromDate, 'DD/MM/YYYY').toDate() : null;
            const toDate = dataDate.toDate ? moment(dataDate.toDate, 'DD/MM/YYYY').toDate() : null;
            this.form.controls['fromDate'].setValue(fromDate);
            this.form.controls['toDate'].setValue(toDate);
            const currentDate = moment(moment(new Date()).format('DD/MM/YYYY'), 'DD/MM/YYYY').toDate();
            this.disabledFromDate = fromDate < currentDate;

            const fromRemindDate = dataDate.fromRemindDate ? moment(dataDate.fromRemindDate, 'DD/MM/YYYY').toDate() : null;
            this.form.controls['fromRemindDate'].setValue(fromRemindDate);

            const toRemindDate = dataDate.toRemindDate ? moment(dataDate.toRemindDate, 'DD/MM/YYYY').toDate() : null;
            this.form.controls['toRemindDate'].setValue(toRemindDate);
          } else {
            this.form.controls['fromDate'].setValue(null);
            this.form.controls['toDate'].setValue(null);
            this.form.controls['fromRemindDate'].setValue(null);
            this.form.controls['toRemindDate'].setValue(null);
            this.disabledFromDate = false;
          }
        }, error => {
          this.toastrService.error(error.message);
        })
      );
    } else {
      this.form.controls['fromDate'].setValue(null);
      this.form.controls['toDate'].setValue(null);
      this.form.controls['fromRemindDate'].setValue(null);
      this.form.controls['toRemindDate'].setValue(null);
      this.disabledFromDate = false;
    }
  }

  closeConfigDateModal() {
    this.modalService.closeAll();
  }

  onSave() {
    this.isSubmitted = true;
    if (this.form.valid) {
      const data: DeclarationYear = new DeclarationYear();
      data.year = this.form.controls['year'].value.getFullYear();
      data.fromDate = moment(this.form.controls['fromDate'].value).format('DD/MM/YYYY');
      data.toDate = moment(this.form.controls['toDate'].value).format('DD/MM/YYYY');
      data.fromRemindDate = moment(this.form.controls['fromRemindDate'].value).format('DD/MM/YYYY');
      data.toRemindDate = moment(this.form.controls['toRemindDate'].value).format('DD/MM/YYYY');
      data.lockRegistrationId = this.lockRegistrationId;
      data.registrationType = Constant.REGISTRATION_TYPE.DECLARATION_REGISTER;

      let formData = new FormData();
      formData.append('data', new Blob([JSON.stringify(data)], {
        type: 'application/json'
      }));
      this.isLoadingPage = true;
      this.subscriptions.push(
        this.declarationService.saveLockRegister(formData).subscribe(res => {
          this.isLoadingPage = false;
          if (res.code === HTTP_STATUS_CODE.OK) {
            const notification = this.lockRegistrationId ? 'common.notification.editSuccess' : 'common.notification.addSuccess'
            this.toastrService.success(this.translate.instant(notification));
            this.modalService.closeAll();
          } else {
            this.toastrService.error(this.translate.instant('common.notification.updateError') + ': ' + res?.message);
          }
        }, error => {
          this.isLoadingPage = false;
          this.toastrService.error(error.message);
        })
      );
    }
  }

  ngOnDestroy(): void {
    this.subscriptions?.forEach(sub => sub.unsubscribe());
  }

}
