import {AfterViewInit, Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { HttpParams } from '@angular/common/http';
import { Subscription } from 'rxjs';
import {AppFunction, Pagination, TableConfig} from "@hcm-mfe/shared/data-access/models";
import {
  Constant,
  ConstantColor,
  UrlConstant
} from "@hcm-mfe/personal-tax/data-access/common";
import {DeclarationRegister} from "@hcm-mfe/personal-tax/data-access/models";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {SearchFormComponent} from "@hcm-mfe/personal-tax/ui/search-form";
import {InvoiceRequestService, SearchFormService, TaxCommonService} from "@hcm-mfe/personal-tax/data-access/services";
import { SessionService } from "@hcm-mfe/shared/common/store";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {AddReceiveInvoiceComponent} from "../add-receive-invoice/add-receive-invoice.component";
import {
  doExportFileUtils,
  openAddModalUtils
} from "@hcm-mfe/personal-tax/data-access/common/utils";
import {
  MbTableMergeCellComponent
} from "../../../../../../shared/ui/mb-table-merge-cell/src/lib/mb-table-merge-cell/mb-table-merge-cell.component";

@Component({
  selector: 'app-receive-invoice',
  templateUrl: './receive-invoice.component.html',
  styleUrls: ['./receive-invoice.component.scss']
})
export class ReceiveInvoiceComponent implements OnInit, AfterViewInit, OnDestroy {
  pagination = new Pagination();
  tableConfig!: TableConfig;
  searchResult: DeclarationRegister[] = [];
  constant = Constant;
  modal!: NzModalRef;
  isImportData = false;
  listRegisterId: number[] = [];
  isRejectList = false;
  isReject = false;
  isDisabled = false;
  isLoadingPage = false;
  mbButtonBgColor: string = ConstantColor.COLOR.BG_PRIMARY_LIGHT;
  isShowRegisterStatus = false;
  functionCode: string = FunctionCode.PTX_RECEIVE_INVOICE;
  objFunction: AppFunction;
  subscriptions: Subscription[] = [];

  disabledColor: string = ConstantColor.COLOR.DISABLED;

  urlApiImport: string = UrlConstant.IMPORT_FORM.INVOICE_REQUEST;
  urlApiDownloadTemp: string = UrlConstant.IMPORT_FORM.INVOICE_REQUEST_TEMPLATE;
  urlDeleteById: string = UrlConstant.DELETE_DATA.INVOICE_REQUEST;
  listInvoiceStatus = Constant.INVOICE_STATUS_REQUEST;


  @ViewChild('searchForm') searchForm!: SearchFormComponent;
  @ViewChild('statusTmpl', { static: true }) status!: TemplateRef<NzSafeAny>;
  @ViewChild('empStatusTmpl', { static: true }) empStatus!: TemplateRef<NzSafeAny>;
  @ViewChild('actionTmpl', { static: true }) action!: TemplateRef<NzSafeAny>;
  @ViewChild('table') table!: MbTableMergeCellComponent;

  constructor(private searchFormService: SearchFormService,
              private translateService: TranslateService,
              private modalService: NzModalService,
              private taxCommonService: TaxCommonService,
              public sessionService: SessionService,
              public invoiceRequestService: InvoiceRequestService,
              private toastrService: ToastrService) {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.PTX_RECEIVE_INVOICE}`);
  }

  ngOnInit(): void {
    this.initTable();
  }

  ngAfterViewInit() {
    this.doSearch(1);
  }

  doSearch(pageIndex: number, refresh = false) {
    this.pagination.pageNumber = pageIndex;
    const searchParam = this.searchForm ? this.searchForm.parseOptions() : new HttpParams();
    this.tableConfig.loading = true;
    this.isLoadingPage = true;
    if (refresh) {
      this.table.resetAllCheckBoxFn();
      this.listRegisterId = [];
    }
    this.subscriptions.push(
      this.searchFormService.getFilterResearch(UrlConstant.SEARCH_FORM.RECEIVE_INVOICE, searchParam, this.pagination.getCurrentPage()).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.searchResult = res.data.listData.map((item: NzSafeAny) => {
            item.id = item.invoiceRequestId;
            item.disabled = item.invoiceStatus != null;
            item.invoiceStatus = item.invoiceStatus ?? null;
            return item;
          });
          this.tableConfig.pageIndex = pageIndex;
          this.tableConfig.total = res.data.count;
          this.tableConfig.loading = false;
        }
        this.isLoadingPage = false;
      }, error => {
        this.toastrService.error(error.message);
      })
    );
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'STT',
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          fixed: true,
          fixedDir: 'left',
          width: 50
        },
        {
          title: 'personalTax.dependent.table.employeeCode',
          field: 'employeeCode', width: 130,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left'
        },
        {
          title: 'personalTax.dependent.table.employeeName',
          fixed: window.innerWidth > 1024,
          fixedDir: 'left',
          field: 'empName', width: 150
        },
        {
          title: 'personalTax.declarationRegisters.statusInvoiceRequest',
          field: 'invoiceStatus',
          tdTemplate: this.status,
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          width: 150,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left',
        },
        {
          title: 'personalTax.taxSearch.label.table.taxNo',
          field: 'taxNo', width: 150
        },
        {
          title: 'personalTax.taxSearch.label.table.idNo',
          field: 'idNo', width: 150
        },
        {
          title: 'personalTax.dependent.table.statusEmployee',
          field: 'empStatus', width: 150,
          tdTemplate: this.empStatus,
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          show: false
        },
        {
          title: 'personalTax.dependent.table.createDate',
          field: 'createDate',
          width: 120,
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        },
        {
          title: 'personalTax.declarationRegisters.yearRev',
          field: 'year',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 100
        },
        {
          title: 'personalTax.declarationRegisters.email',
          field: 'email',
          width: 200
        },
        {
          title: 'personalTax.declarationRegisters.note',
          field: 'note',
          width: 180
        },
        {
          title: 'personalTax.dependent.table.orgManageName',
          field: 'orgManageName', width: 250
        },
        {
          title: 'personalTax.dependent.table.orgCurrentName',
          field: 'orgName', width: 250
        },
        {
          title: 'personalTax.dependent.table.jobName',
          field: 'jobName', width: 150
        },
        {
          title: 'personalTax.dependent.table.employeeType',
          field: 'empTypeName', width: 150,
          show: false
        },
        {
          title: ' ',
          field: 'action',
          tdClassList: ['text-nowrap', 'text-center'], thClassList: ['text-nowrap', 'text-center'],
          width: 60,
          tdTemplate: this.action,
          fixed: window.innerWidth > 1024,
          fixedDir: 'right'
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1,
      showSelect: true
    };
  }

  deleteById(registerId: number) {
    this.subscriptions.push(
      this.taxCommonService.deleteById(registerId, UrlConstant.DELETE_DATA.INVOICE_REQUEST).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.doSearch(1);
          this.toastrService.success(this.translateService.instant('common.notification.deleteSuccess'));
        } else {
          this.toastrService.error(res.message);
        }
      }, error => {
        this.toastrService.error(error.message);
      })
    );
  }

  async doExportData() {
    this.isLoadingPage = true;
    const searchParam = this.searchForm ? this.searchForm.parseOptions() : new HttpParams();
    this.isLoadingPage = await doExportFileUtils(UrlConstant.EXPORT_REPORT.RECEIVE_INVOICE, searchParam, this.taxCommonService, this.toastrService, this.translateService, 'Danh-sach-dk-nhạn-chung-tu-thue.xlsx');
  }

  onEdit(registerId: number) {
    this.openAddModal(registerId);
  }

  onDetail(registerId?: number) {
    this.modal = openAddModalUtils(registerId, this.modalService, this.translateService, 'personalTax.modalName.receiveInvoicesDetail', AddReceiveInvoiceComponent, 1.5, true);
    this.subscriptions.push(
      this.modal.afterClose.subscribe(res => {
        if (res && res.refresh) {
          this.doSearch(1);
        }
      })
    );
  }

  onDblClickNode(event: NzSafeAny) {
    this.onDetail(event?.invoiceRequestId);
  }


  openAddModal(registerId?: number) {
    this.modal = openAddModalUtils(registerId, this.modalService, this.translateService, 'personalTax.modalName.receiveInvoicesRegister', AddReceiveInvoiceComponent);
    this.subscriptions.push(
      this.modal.afterClose.subscribe(res => {
        if (res && res.refresh) {
          this.doSearch(1);
        }
      })
    );
  }

  doImportData() {
    this.isImportData = true;
  }

  doCloseImport(refresh: boolean) {
    this.isImportData = false;
    if (refresh) {
      this.doSearch(this.pagination.pageNumber);
    }
  }

  onLoadPage(isLoadPage: boolean) {
    this.isLoadingPage = isLoadPage;
  }

  onAmountSelectedRowChange(event: NzSafeAny) {
    this.listRegisterId = Array.from(event);
  }

  doRequestAll() {
    this.isLoadingPage = true;
    const searchParam = this.searchForm ? this.searchForm.parseOptions() : new HttpParams();
    this.subscriptions.push(
      this.invoiceRequestService.requestExportInvoiceByForm(searchParam).subscribe(res => {
        this.isLoadingPage = false;
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.toastrService.success(this.translateService.instant('personalTax.notification.sendRequestSuccess'));
        } else {
          this.toastrService.error(res?.message);
        }
      }, error => {
        this.isLoadingPage = false;
        this.toastrService.error(error.message);
      })
    );
  }

  doRequestList() {
    this.isLoadingPage = true;
    this.subscriptions.push(
      this.invoiceRequestService.requestExportInvoiceByList(this.listRegisterId).subscribe(res => {
        this.isLoadingPage = false;
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.toastrService.success(this.translateService.instant('personalTax.notification.sendRequestSuccess'));
          this.doSearch(1, true);
        } else {
          this.toastrService.error(res?.message);
        }
      }, error => {
        this.isLoadingPage = false;
        this.toastrService.error(error.message);
      })
    );
  }


  ngOnDestroy(): void {
    this.modal?.destroy();
    this.subscriptions?.forEach(sub => sub.unsubscribe());
  }

}
