import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ReceiveInvoiceComponent} from "./receive-invoice/receive-invoice.component";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {PersonalTaxUiSearchFormModule} from "@hcm-mfe/personal-tax/ui/search-form";
import {NzFormModule} from "ng-zorro-antd/form";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {NzTagModule} from "ng-zorro-antd/tag";
import {PersonalTaxUiEmpStatusCommonModule} from "@hcm-mfe/personal-tax/ui/emp-status-common";
import {PersonalTaxUiImportCommonModule} from "@hcm-mfe/personal-tax/ui/import-common";
import {RouterModule} from "@angular/router";
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import {PersonalTaxUiPersonalInformationModule} from "@hcm-mfe/personal-tax/ui/personal-information";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiMbTextLabelModule} from "@hcm-mfe/shared/ui/mb-text-label";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {AddReceiveInvoiceComponent} from "./add-receive-invoice/add-receive-invoice.component";
import {PersonalTaxUiBtnActionByIdModule} from "@hcm-mfe/personal-tax/ui/btn-action-by-id";
import {SharedDirectivesNumberInputModule} from "@hcm-mfe/shared/directives/number-input";
import {SharedUiMbTableMergeCellWrapModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell-wrap";
import {SharedUiMbTableMergeCellModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell";

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, PersonalTaxUiSearchFormModule, NzFormModule, SharedUiMbButtonModule,
    TranslateModule, SharedUiMbTableWrapModule, SharedUiMbTableModule, NzIconModule, NzButtonModule, NzDropDownModule,
    NzTagModule, PersonalTaxUiEmpStatusCommonModule, PersonalTaxUiImportCommonModule, PersonalTaxUiPersonalInformationModule, ReactiveFormsModule, FormsModule,
    SharedUiMbSelectModule, SharedUiMbTextLabelModule, SharedUiMbInputTextModule,
    RouterModule.forChild([
      {
        path: '',
        component: ReceiveInvoiceComponent
      }
    ]), NzPopconfirmModule, NzToolTipModule, PersonalTaxUiBtnActionByIdModule, SharedDirectivesNumberInputModule, SharedUiMbTableMergeCellWrapModule, SharedUiMbTableMergeCellModule
  ],
  declarations: [ReceiveInvoiceComponent, AddReceiveInvoiceComponent],
  exports: [ReceiveInvoiceComponent],
})
export class PersonalTaxFeatureReceiveInvoiceModule {}
