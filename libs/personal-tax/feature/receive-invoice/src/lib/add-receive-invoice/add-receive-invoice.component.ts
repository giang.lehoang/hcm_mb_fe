import {Component, OnDestroy, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { Subscription } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import {Constant} from "@hcm-mfe/personal-tax/data-access/common";
import {DeclarationRegister, DeclarationYear} from "@hcm-mfe/personal-tax/data-access/models";
import {DeclarationService, InvoiceRequestService, TaxCommonService} from "@hcm-mfe/personal-tax/data-access/services";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {FunctionCode, Scopes} from "@hcm-mfe/shared/common/enums";
import {AppFunction, CatalogModel} from "@hcm-mfe/shared/data-access/models";

@Component({
  selector: 'app-add-receive-invoice',
  templateUrl: './add-receive-invoice.component.html',
  styleUrls: ['./add-receive-invoice.component.scss']
})
export class AddReceiveInvoiceComponent implements OnInit, OnDestroy {

  form: FormGroup = this.fb.group({
    year: [null, Validators.required],
    note: null,
    email: null,
    employee: [null],
    idNo: [null],
    fullName: [null],
    taxNo: [null],
    orgId: [null, Validators.required],
  });
  constant = Constant;
  employeeId: number | undefined;
  employeeCode: string | undefined;
  isSubmitted = false;
  registerId: number | undefined;
  listYear: DeclarationYear[] = [];
  email = '';
  subscriptions: Subscription[] = [];
  disableBtnSave = false;
  isDetail = false;
  isLoadingPage = false;
  isEmployeeMb = true;
  functionCode: string = FunctionCode.PTX_RECEIVE_INVOICE;
  isEdit = false;
  objFunction?: AppFunction;
  listOrgManage: CatalogModel[] = [];

  constructor(private fb: FormBuilder,
              private declarationService: DeclarationService,
              private invoiceRequestService: InvoiceRequestService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private modalService: NzModalService,
              private taxCommonService: TaxCommonService,
              private toastrService: ToastrService,
              private translate: TranslateService,
              private modalRef: NzModalRef) {
  }

  ngOnInit(): void {
    this.getListYear();
    this.getListOrgManage();
    if (this.registerId) {
      this.pathValueForm().then();
    }

  }

  async pathValueForm() {
      if (this.registerId) {
          this.isLoadingPage = true;
          const response = await this.invoiceRequestService.getById(this.registerId).toPromise();
          if (response.code === HTTP_STATUS_CODE.OK) {
              const data = response.data;
              if (!data.employeeId) {
                this.isEmployeeMb = false;
              }
              this.form.patchValue(data);
              this.subscriptions.push(
                  this.declarationService.getEmployeeInfo(response.data.employeeId).subscribe(res => {
                      if (res.code === HTTP_STATUS_CODE.OK) {
                          this.form.controls['employee'].setValue(res.data);
                      }
                  }, error => this.toastrService.error(error.message))
              );
          }
          this.isLoadingPage = false;
          this.isEdit = true;
          if (this.isDetail) {
            this.form.disable();
          }
      }
  }

  getListOrgManage() {
    this.subscriptions.push(
      this.declarationService.getListOrgManage(Scopes.VIEW, this.functionCode, 2).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listOrgManage = res.data?.map((item: NzSafeAny) => {
            item.value = item.orgId;
            item.label = item.orgName;
            return item;
          })
        }
      }, error => {
        this.toastrService.error(error.message);
      })
    )
  }

  getListYear() {
    this.subscriptions.push(
      this.declarationService.getListYear(Constant.REGISTRATION_TYPE.DECLARATION_REGISTER).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listYear = res.data.map((item: NzSafeAny) => {
            item.value = item.year;
            item.label = item.year.toString();
            return item;
          });
        }
      }, error => {
        this.toastrService.error(error.message);
      })
    );
  }

  getEmployeeId(event: NzSafeAny) {
    this.employeeId = event?.employeeId;
    this.employeeCode = event?.employeeCode;
    if (!this.isEdit) {
      this.form.controls['email'].setValue(event?.email);
    }
    if (this.form.controls['year'].value) {
      this.checkDeclarationRegister(this.form.controls['year'].value);
    }
    this.isEdit = false;
  }

  onSave(status: number) {
    this.validateForm();
    this.isSubmitted = true;
    if (this.form.valid) {
      const data: DeclarationRegister = this.form.value;
      data.employeeId = this.employeeId;
      data.employeeCode = this.employeeCode;
      data.status = status;
      data.invoiceRequestId = this.registerId;
      this.isLoadingPage = true;
      this.subscriptions.push(
        this.invoiceRequestService.saveData(data).subscribe(res => {
          if (res && res.code === HTTP_STATUS_CODE.OK) {
            const notification = this.registerId ? 'common.notification.editSuccess' : 'common.notification.addSuccess'
            this.toastrService.success(this.translate.instant(notification));
            this.modalRef.close({refresh:true});
          } else {
            this.toastrService.error(this.translate.instant('common.notification.updateError') + res?.message);
          }
          this.isLoadingPage = false;
        }, error => {
          this.isLoadingPage = true;
          this.toastrService.error(error.message);
        })
      );
    }
  }

  validateForm() {
    if (this.isEmployeeMb) {
      this.setValidateRequired(this.form, 'employee');
    } else {
      this.setValidateRequired(this.form, 'idNo');
      this.setValidateRequired(this.form, 'fullName');

      if (this.form.controls['idNo'].value && this.form.controls['idNo'].value.length !== 9 && this.form.controls['idNo'].value.length !== 12) {
        this.form.setErrors({onlyAccept9Or12Digit: true})
      } else {
        this.form.controls['taxNo'].setErrors(null);
      }

      if (this.form.controls['taxNo'].value && this.form.controls['taxNo'].value.length !== 10) {
        this.form.controls['taxNo'].setErrors({errLength: true});
      } else {
        this.form.controls['taxNo'].setErrors(null);
      }
    }
  }

  setValidateRequired(form: FormGroup, key: string) {
    if (!form.controls[key].value) {
      form.controls[key].setErrors({required: true});
    } else {
      form.controls[key].setErrors(null);
    }
  }

  closeModalAdd() {
    this.modalService.closeAll();
  }

  selectYear(year: number) {
    if (year && this.employeeId) {
      this.checkDeclarationRegister(year)
    }
  }

  checkDeclarationRegister(year: number) {
    const params = new HttpParams().set('employeeId', this.employeeId ?? '').set('year', year).set('methodCode', Constant.IS_AUTHORIZATION).set('regType', Constant.REG_TYPE_CODE.REGISTER_TAX_SETTLEMENT);
    this.subscriptions.push(
      this.declarationService.getDataByProperties(params).subscribe(res => {
          if (res.code === HTTP_STATUS_CODE.OK ) {
            if (res.data.length > 0) {
              this.toastrService.error(this.translate.instant('personalTax.notification.errorRegister'));
              this.disableBtnSave = true;
            } else {
              this.disableBtnSave = false;
            }
          }
        }, error => this.toastrService.error(error.message))
    );
  }

  ngOnDestroy(): void {
    this.modalRef?.destroy();
    this.subscriptions?.forEach(sub => sub.unsubscribe());
  }

}
