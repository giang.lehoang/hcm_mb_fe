import { NgModule } from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {PersonalTaxFeatureShellRoutingModule} from "./personal-tax-feature-shell.routing.module";
import {FormatCurrencyPipe} from "@hcm-mfe/shared/pipes/format-currency";
import {SharedUiPopupModule} from "@hcm-mfe/shared/ui/popup";
import {NzModalModule} from "ng-zorro-antd/modal";

@NgModule({
  imports: [CommonModule,
    NzModalModule,
    PersonalTaxFeatureShellRoutingModule,
    SharedUiPopupModule
  ],
  providers: [DatePipe, FormatCurrencyPipe]
})
export class PersonalTaxFeatureShellModule {}
