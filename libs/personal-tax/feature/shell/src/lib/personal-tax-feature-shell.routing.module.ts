import { NgModule } from '@angular/core';
import {RouterModule} from "@angular/router";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";

const router = [
  {
    path: 'tax-search',
    data: {
      code: FunctionCode.PTX_TAX_SEARCH,
      pageName: 'personalTax.pageName.taxSearch',
      breadcrumb: 'personalTax.breadcrumb.taxSearch'
    },
    loadChildren: () => import('@hcm-mfe/personal-tax/feature/ptx-tax-search').then((m) => m.PersonalTaxFeaturePtxTaxSearchModule)
  },
  {
    path: 'tax-registers',
      data: {
      code: FunctionCode.PTX_TAX_SEARCH,
      pageName: 'personalTax.pageName.taxRegisters',
      breadcrumb: 'personalTax.breadcrumb.taxRegisters'
    },
    loadChildren: () => import('@hcm-mfe/personal-tax/feature/tax-registers').then((m) => m.PersonalTaxFeatureTaxRegistersModule)
  },
  {
    path: 'register-change',
      data: {
      code: FunctionCode.PTX_TAX_SEARCH,
      pageName: 'personalTax.pageName.registerChange',
      breadcrumb: 'personalTax.breadcrumb.registerChange',
    },
    loadChildren: () => import('@hcm-mfe/personal-tax/feature/change-tax-registered-info').then((m) => m.PersonalTaxFeatureChangeTaxRegisteredInfoModule)
  },
  {
    path: 'dependent-registers',
      data: {
      code: FunctionCode.PTX_TAX_SEARCH,
      pageName: 'personalTax.pageName.dependentRegisters',
      breadcrumb: 'personalTax.breadcrumb.dependentRegisters'
    },
    loadChildren: () => import('@hcm-mfe/personal-tax/feature/dependent-registers').then((m) => m.PersonalTaxFeatureDependentRegistersModule)
  },
  {
    path: 'declaration-registers',
      data: {
      code: FunctionCode.PTX_TAX_SEARCH,
      pageName: 'personalTax.pageName.declarationRegisters',
      breadcrumb: 'personalTax.breadcrumb.declarationRegisters'
    },
    loadChildren: () => import('@hcm-mfe/personal-tax/feature/declaration-register').then((m) => m.PersonalTaxFeatureDeclarationRegisterModule)
  },
  {
    path: 'receive-invoice',
      data: {
      code: FunctionCode.PTX_TAX_SEARCH,
      pageName: 'personalTax.pageName.receiveInvoice',
      breadcrumb: 'personalTax.breadcrumb.receiveInvoice'
    },
    loadChildren: () => import('@hcm-mfe/personal-tax/feature/receive-invoice').then((m) => m.PersonalTaxFeatureReceiveInvoiceModule)
  },
  {
    path: 'confirm-provide-info',
      data: {
      code: FunctionCode.PTX_TAX_SEARCH,
      pageName: 'personalTax.pageName.confirmProvideInfo',
      breadcrumb: 'personalTax.breadcrumb.confirmProvideInfo'
    },
    loadChildren: () => import('@hcm-mfe/personal-tax/feature/confirm-provide-info').then((m) => m.PersonalTaxFeatureConfirmProvideInfoModule)
  },
  {
    path: 'salary-account-reqs',
      data: {
      code: FunctionCode.PTX_SALARY_ACCOUNT_REQS,
      pageName: 'personalTax.pageName.salaryAccountReqs',
      breadcrumb: 'personalTax.breadcrumb.salaryAccountReqs'
    },
    loadChildren: () => import('@hcm-mfe/personal-tax/feature/salary-account-reqs').then((m) => m.PersonalTaxFeatureSalaryAccountReqsModule)
  },
  {
    path: 'report-dependent-person',
    data: {
      pageName: 'personalTax.pageName.reportDependentPerson',
      breadcrumb: 'personalTax.breadcrumb.reportDependentPerson'
    },
    loadChildren: () => import('@hcm-mfe/personal-tax/feature/report-dependent-person').then((m) => m.PersonalTaxFeatureReportDependentPersonModule)
  },
]

@NgModule({
  imports: [RouterModule.forChild(router)],
  exports: [RouterModule]
})
export class PersonalTaxFeatureShellRoutingModule {}
