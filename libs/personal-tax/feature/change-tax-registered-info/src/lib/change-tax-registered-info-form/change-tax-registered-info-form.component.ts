import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NzUploadFile} from 'ng-zorro-antd/upload';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import * as moment from 'moment';
import {NzModalRef, NzModalService} from 'ng-zorro-antd/modal';
import {of, Subscription} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {Constant, ObjectCategory, UrlConstant} from "@hcm-mfe/personal-tax/data-access/common";
import {doDownloadFileAttach} from "@hcm-mfe/personal-tax/data-access/common/utils";
import {PersonalInfo, TaxNumberRegister, TaxNumberRegistersResponse} from "@hcm-mfe/personal-tax/data-access/models";
import {AppFunction, BaseResponse, LookupValues, SelectModal} from "@hcm-mfe/shared/data-access/models";
import {
  DownloadFileAttachService,
  LookupValuesService,
  PersonalInfoService,
  TaxCommonService,
  TaxRegistersService
} from "@hcm-mfe/personal-tax/data-access/services";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {beforeUploadFile, Utils} from "@hcm-mfe/shared/common/utils";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {IdentityInfo} from "@hcm-mfe/staff-manager/data-access/models/info";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";


@Component({
    selector: 'app-change-tax-registered-info-form',
    templateUrl: './change-tax-registered-info-form.component.html',
    styleUrls: ['./change-tax-registered-info-form.component.scss']
})
export class ChangeTaxRegisteredInfoFormComponent implements OnInit, OnDestroy {

    form: FormGroup = this.fb.group([]);
    listPaperType: ObjectCategory[] = [];
    listNational: LookupValues[] = [];

    provinces: LookupValues[] = [];
    districts: LookupValues[] = [];
    wards: LookupValues[] = [];
    listCitizenPlace: ObjectCategory[] = [];
    listOldCitizenPlace: ObjectCategory[] = [];

    districtsCurrent: LookupValues[] = [];
    wardsCurrent: LookupValues[] = [];

    isLoadingProvinces = false;
    isLoadingDistricts = false;
    isLoadingWards = false;
    isLoadingProvincesCurrent = false;
    isLoadingDistrictsCurrent = false;
    isLoadingWardsCurrent = false;

    isSubmitted = false;
    fileList: NzUploadFile[] = [];
    docIdsDelete: number[] = [];

    employeeId?: number;

    registerId?: number;
    citizenPaperTypeValue = Constant.PAPERS_CODE.CITIZEN_ID;
    paperTypeCurrent?: SelectModal;
    changeTaxRegisterInfoEdit?: TaxNumberRegistersResponse;
    employeeIdentities: IdentityInfo[] = [];

    subs: Subscription[] = [];
    isEdit = false;
    isDetail = false;
    isLoadingPage = false;
    constant = Constant;
    status?: number;
    isReject = false;
    urlReject: string = UrlConstant.REJECT_LIST.TAX_REGISTER;
    functionCode: string = FunctionCode.PTX_TAX_CHANGE;
    objFunction?: AppFunction;

    constructor(
        private fb: FormBuilder,
        private lookupValuesService: LookupValuesService,
        private toastService: ToastrService,
        private translate: TranslateService,
        private downloadFileAttachService: DownloadFileAttachService,
        private taxRegisterService: TaxRegistersService,
        private taxCommonService: TaxCommonService,
        private personalInfoService: PersonalInfoService,
        private modalService: NzModalService,
        private modalRef: NzModalRef
    ) {
    }

    ngOnInit(): void {
        this.getPaperTypes();
        this.getCatalogs();
        this.initForm();

        // Mode = EDIT
        if (this.registerId) {
            this.getRegisterEdit();
        }
    }

    getCatalogs() {
        this.getNational();
        this.getProvinces();
    }

    getCitizenPlace(key: string) {
        let typeCode;
        if (key === 'oldIdTypeCode') {
          typeCode = this.form.controls['oldIdTypeCode']?.value === this.citizenPaperTypeValue ? Constant.CATALOGS.NOI_CAP_CCCD : Constant.CATALOGS.NOI_CAP_CMND;
        } else {
          typeCode = this.form.controls['idTypeCode']?.value === this.citizenPaperTypeValue ? Constant.CATALOGS.NOI_CAP_CCCD : Constant.CATALOGS.NOI_CAP_CMND;
        }
        this.subs.push(
            this.lookupValuesService.getCatalog(typeCode).subscribe({
                next: (res: BaseResponse) => {
                    if (res.code === HTTP_STATUS_CODE.OK) {
                        if (key === 'oldIdTypeCode') {
                            this.listOldCitizenPlace = res?.data;
                        } else {
                            this.listCitizenPlace = res?.data;
                        }
                    } else {
                        this.toastService.error(res?.message);
                    }
                },
                error: (err) => {
                    this.toastService.error(err?.message);
                }
            })
        );
    }

    getNational() {
        this.subs.push(
            this.lookupValuesService.getCatalog(Constant.CATALOGS.QUOC_GIA).subscribe({
                next: (res: BaseResponse) => {
                    if (res.code === HTTP_STATUS_CODE.OK) {
                        this.listNational = res.data;
                        this.setDefaultNation(this.listNational);
                    } else {
                        this.toastService.error(res?.data);
                    }
                },
                error: (err) => {
                    this.toastService.error(err?.message);
                }
            })
        );
    }

    setDefaultNation(nations: LookupValues[]) {
        const vnModel = nations?.find(nation => nation.label === Constant.VN_NATION);
        if (vnModel) {
            this.form.controls['permanentNationCode'].setValue(vnModel?.value);
            this.form.controls['currentNationCode'].setValue(vnModel?.value);
        }
    }

    getRegisterEdit() {
        if (this.registerId) {
            const commonTaxApi = this.taxCommonService.getTaxRegisterById(this.registerId, Constant.REG_TYPE.ALTER_TAX_INFO);
            this.isLoadingPage = true;
            this.subs.push(
                commonTaxApi.pipe(
                    map((res: BaseResponse) => {
                        let taxRegisterEdit: TaxNumberRegistersResponse;
                        if (res.code === HTTP_STATUS_CODE.OK) {
                            taxRegisterEdit = res?.data;
                            this.changeTaxRegisterInfoEdit = taxRegisterEdit;
                            this.status = taxRegisterEdit?.status;
                        } else {
                            this.toastService.error(res?.message);
                            return null;
                        }
                        return taxRegisterEdit;
                    }),
                    catchError(() => {
                        this.toastService.error(this.translate.instant("common.notification.internalServerError"));
                        this.isLoadingPage = false;
                        return of(null);
                    })
                ).subscribe({
                    next: (modelEdit: TaxNumberRegistersResponse | null) => {
                        this.isLoadingPage = false;
                        if (modelEdit) {
                            const employeeId = modelEdit.employeeId;
                            if (employeeId) {
                                this.subs.push(
                                    this.personalInfoService.getPersonalInfo(employeeId).subscribe({
                                        next: (res: BaseResponse) => {
                                            if (res.code === HTTP_STATUS_CODE.OK) {
                                                this.form.controls['employee'].setValue(res?.data);
                                                this.patchValueToForm();
                                            } else {
                                                this.toastService.error(res?.data);
                                            }
                                        },
                                        error: (err) => {
                                            this.toastService.error(err?.message);
                                        }
                                    })
                                );
                            }
                        }
                    },
                    error: (err) => {
                        this.isLoadingPage = false;
                        this.toastService.error(err?.message);
                    }
                })
            );
        }
    }

    patchValueToForm() {
        this.form.controls['oldIdTypeCode'].setValue(this?.changeTaxRegisterInfoEdit?.oldIdTypeCode);

        this.form.controls['idTypeCode'].setValue(this?.changeTaxRegisterInfoEdit?.idTypeCode);
        this.form.controls['idNo'].setValue(this?.changeTaxRegisterInfoEdit?.idNo);
        this.form.controls['idDate'].setValue(Utils.convertDateToFillForm(this.changeTaxRegisterInfoEdit?.idDate ?? ''));
        this.form.controls['idPlace'].setValue(this?.changeTaxRegisterInfoEdit?.idPlace);

        this.form.controls['permanentProvinceCode'].setValue(this?.changeTaxRegisterInfoEdit?.permanentProvinceCode);
        this.form.controls['permanentDistrictCode'].setValue(this?.changeTaxRegisterInfoEdit?.permanentDistrictCode);
        this.form.controls['permanentWardCode'].setValue(this?.changeTaxRegisterInfoEdit?.permanentWardCode);
        this.form.controls['permanentDetail'].setValue(this?.changeTaxRegisterInfoEdit?.permanentDetail);
        this.form.controls['currentProvinceCode'].setValue(this?.changeTaxRegisterInfoEdit?.currentProvinceCode);
        this.form.controls['currentDistrictCode'].setValue(this?.changeTaxRegisterInfoEdit?.currentDistrictCode);
        this.form.controls['currentWardCode'].setValue(this?.changeTaxRegisterInfoEdit?.currentWardCode);
        this.form.controls['currentDetail'].setValue(this?.changeTaxRegisterInfoEdit?.currentDetail);

        this.form.controls['note'].setValue(this?.changeTaxRegisterInfoEdit?.note ? this?.changeTaxRegisterInfoEdit?.note : null);
        this.form.controls['mobileNumber'].setValue(this?.changeTaxRegisterInfoEdit?.mobileNumber);
        this.form.controls['email'].setValue(this?.changeTaxRegisterInfoEdit?.email);
        this.form.controls['rejectReason'].setValue(this?.changeTaxRegisterInfoEdit?.rejectReason);
        this.form.controls['taxPlace'].setValue(this?.changeTaxRegisterInfoEdit?.taxPlace);

        // file
        if (this?.changeTaxRegisterInfoEdit?.attachFileList && this.changeTaxRegisterInfoEdit?.attachFileList?.length > 0) {
            this?.changeTaxRegisterInfoEdit?.attachFileList.forEach((item) => {
                this.fileList.push({
                    uid: item.docId,
                    name: item.fileName,
                    thumbUrl: item.security,
                    status: 'done'
                });
            });
            this.fileList = [...this.fileList];
        }

        if (this.isDetail) {
          this.form.disable();
        }
        this.isEdit = true;
    }

    getEmployee(event: PersonalInfo) {
        if (event) {
            this.employeeId = event?.employeeId;
            this.setTaxInfo(event);
            this.setPhoneAndEmailInfo(event);
            this.getIdentities(event);
        } else {
            this.form.reset();
        }
    }

    getIdentities(event: PersonalInfo) {
        if (event.employeeId) {
            this.subs.push(
                this.personalInfoService.getIdentities(event.employeeId).subscribe({
                    next: (res: BaseResponse) => {
                        if (res.code === HTTP_STATUS_CODE.OK) {
                            this.employeeIdentities = res?.data;
                        } else {
                            this.toastService.error(res?.message);
                        }
                    },
                    error: (err) => {
                        this.toastService.error(err?.message);
                    }
                })
            );
        }
    }

    setTaxInfo(employeeInfo: PersonalInfo) {
        this.form.controls['taxNo'].setValue(employeeInfo?.taxNo);
        if (!this.changeTaxRegisterInfoEdit) {  // m�n th�m m?i th� m?i set
          this.form.controls['taxPlace'].setValue(employeeInfo?.taxPlace);
        }
    }

    setPhoneAndEmailInfo(employeeInfo: PersonalInfo) {
        if (!this.isEdit) {
            this.form.controls['mobileNumber'].setValue(employeeInfo?.mobileNumber);
            this.form.controls['email'].setValue(employeeInfo?.email);
        }
        this.isEdit = false;
    }

    getPaperTypes() {
        this.subs.push(
            this.lookupValuesService.getCatalogModuleTax(Constant.CATALOGS.LOAI_GIAY_TO, Constant.CATALOGS.TEN_CQT).subscribe(res => {
                if (res.code === HTTP_STATUS_CODE.OK) {
                    this.listPaperType = res.data;
                }
            }, error => this.toastService.error(error.message))
        );
    }

    onOldPaperTypeChange(event: SelectModal, key: string) {
        this.getCitizenPlace(key);
        const paperType = event?.itemSelected?.value;
        this.addOldIdNoValidator(paperType);
        let identity;
        if (paperType === Constant.PAPERS_CODE.ID_NO) {
            identity = this.employeeIdentities?.find(item => item.idTypeCode ? item.idTypeCode === paperType : item.idNo?.length === 9);
        } else if (paperType === Constant.PAPERS_CODE.CITIZEN_ID) {
            identity = this.employeeIdentities?.find(item => item.idTypeCode ? item.idTypeCode === paperType : item.idNo?.length === 12);
        } else {
            identity = this.employeeIdentities?.find(item => item.idTypeCode ? item.idTypeCode === paperType : (item.idNo?.length !== 12 && item.idNo?.length !== 9));
        }
        if (!this.registerId) { // MODE = ADD
            if (identity) {
                this.form.controls['oldIdNo'].setValue(identity?.idNo);
                this.form.controls['oldIdDate'].setValue(Utils.convertDateToFillForm(identity?.idIssueDate));
                this.form.controls['oldIdPlace'].setValue(identity?.idIssuePlace);
                this.form.controls['oldIdPlaceCode'].setValue(identity?.idIssuePlace);
            } else {
                this.form.controls['oldIdNo'].reset();
                this.form.controls['oldIdDate'].reset();
                this.form.controls['oldIdPlace'].reset();
                this.form.controls['oldIdPlaceCode'].reset();
            }
        } else { // MODE = EDIT
            if (paperType === this.changeTaxRegisterInfoEdit?.oldIdTypeCode) {
                this.form.controls['oldIdNo'].setValue(this?.changeTaxRegisterInfoEdit?.oldIdNo);
                this.form.controls['oldIdDate'].setValue(Utils.convertDateToFillForm(this?.changeTaxRegisterInfoEdit?.oldIdDate));
                this.form.controls['oldIdPlace'].setValue(this?.changeTaxRegisterInfoEdit?.oldIdPlace);
                this.form.controls['oldIdPlaceCode'].setValue(this?.changeTaxRegisterInfoEdit?.oldIdPlaceCode);
            } else if (identity) {
                this.form.controls['oldIdNo'].setValue(identity?.idNo);
                this.form.controls['oldIdDate'].setValue(Utils.convertDateToFillForm(identity?.idIssueDate));
                this.form.controls['oldIdPlace'].setValue(identity?.idIssuePlace);
                this.form.controls['oldIdPlaceCode'].setValue(identity?.idIssuePlace);
            } else {
                this.form.controls['oldIdNo'].reset();
                this.form.controls['oldIdDate'].reset();
                this.form.controls['oldIdPlace'].reset();
                this.form.controls['oldIdPlaceCode'].reset();
            }
        }
    }

    initForm() {
        this.form = this.fb.group({
            taxNo: [{value: '', disabled: true}],
            taxPlace: [null],

            oldIdTypeCode: ['', [Validators.required]],
            oldIdNo: [null, [Validators.required]],
            oldIdDate: [null, [Validators.required]],
            oldIdPlace: [null],
            oldIdPlaceCode: [null],

            idTypeCode: ['', [Validators.required]],
            idNo: [null, [Validators.required]],
            idDate: [null, [Validators.required]],
            idPlace: [null],
            idPlaceCode: [null],

            permanentNationCode: [{value: null}, [Validators.required]],
            permanentProvinceCode: [null, [Validators.required]],
            permanentDistrictCode: [null, [Validators.required]],
            permanentWardCode: [null, [Validators.required]],
            permanentDetail: [null, [Validators.required]],
            currentNationCode: [{value: null}, [Validators.required]],
            currentProvinceCode: [null, [Validators.required]],
            currentDistrictCode: [null, [Validators.required]],
            currentWardCode: [null, [Validators.required]],
            currentDetail: [null, [Validators.required]],

            note: [null],
            mobileNumber: [null, [Validators.required, Validators.pattern('^[0-9]{10}$')]],
            email: [null, [Validators.required, Validators.email]],
            employee: [null, Validators.required],
            rejectReason: [null]
        });
    }

    onPaperTypeChange(event: SelectModal, key: string) {
        this.getCitizenPlace(key);
        this.paperTypeCurrent = event;
        this.addIdNoValidator(event?.itemSelected?.value);
        if (this.registerId) { // MODE = EDIT
            if (event?.itemSelected?.value === this.changeTaxRegisterInfoEdit?.idTypeCode) {
                this.form.controls['idNo'].setValue(this.changeTaxRegisterInfoEdit?.idNo);
                this.form.controls['idDate'].setValue(Utils.convertDateToFillForm(this.changeTaxRegisterInfoEdit?.idDate ?? ''));
                this.form.controls['idPlace'].setValue(this.changeTaxRegisterInfoEdit?.idPlace);
                this.form.controls['idPlaceCode'].setValue(this.changeTaxRegisterInfoEdit?.idPlaceCode);
            } else {
                this.form.controls['idNo'].reset();
                this.form.controls['idDate'].reset();
                this.form.controls['idPlace'].reset();
                this.form.controls['idPlaceCode'].reset();
            }
        }

    }

    beforeUpload = (file: NzUploadFile): boolean => {
        const maxSize: number = 5*1024*1024;
        if (this.fileList.length < 4) {
          this.fileList = beforeUploadFile(file, this.fileList, maxSize, this.toastService, this.translate, 'common.notification.fileExtensionInvalidTaxRegister', '.TIF', '.JPG', '.GIF', '.PNG', '.PDF', '.DOC', '.DOCX', '.XLS', '.XLSX');
        } else {
          this.toastService.error(this.translate.instant('personalTax.notification.maxFile'));
        }
        return false;
    };

    downloadFile = (file: NzUploadFile) => {
        doDownloadFileAttach(~~file?.uid, file?.thumbUrl, file.name, this.downloadFileAttachService, this.toastService, this.translate);
    };

    removeFile = (file: NzUploadFile): boolean => {
        this.docIdsDelete.push(Number(file.uid));
        const index = this.fileList.indexOf(file);
        this.fileList.splice(index, 1);
        return false;
    };

    getProvinces() {
        this.subs.push(
            this.lookupValuesService.getCatalog(Constant.CATALOGS.TINH).subscribe({
                next: (res: BaseResponse) => {
                    if (res.code === HTTP_STATUS_CODE.OK) {
                        this.provinces = res.data;
                        this.isLoadingProvinces = false;
                        this.isLoadingProvincesCurrent = false;
                    } else {
                        this.toastService.error(res?.data);
                    }
                },
                error: (err) => {
                    this.toastService.error(err?.message);
                }
            })
        );
    }

    changeProvince($event: SelectModal, type: 'HK' | 'HT') {
        if ($event?.itemSelected) {
            if (type === 'HK' && $event?.itemSelected?.value !== this?.changeTaxRegisterInfoEdit?.permanentProvinceCode) {
                this.form.controls['permanentDistrictCode'].reset();
                this.form.controls['permanentWardCode'].reset();
            }
            if (type === 'HT' && $event?.itemSelected?.value !== this?.changeTaxRegisterInfoEdit?.currentProvinceCode) {
                this.form.controls['currentDistrictCode'].reset();
                this.form.controls['currentWardCode'].reset();
            }
            this.getDistricts($event?.itemSelected?.value, type);
        }
    }

    changeDistrict($event: SelectModal, type: 'HK' | 'HT') {
        if ($event?.itemSelected) {
            if (type === 'HK' && $event?.itemSelected?.value !== this?.changeTaxRegisterInfoEdit?.permanentDistrictCode) {
                this.form.controls['permanentWardCode'].reset();
            }
            if (type === 'HT' && $event?.itemSelected?.value !== this?.changeTaxRegisterInfoEdit?.currentDistrictCode) {
                this.form.controls['currentWardCode'].reset();
            }
            this.getWards($event?.itemSelected?.value, type);
        }
    }

    getDistricts(parentCode?: number, type: 'HK' | 'HT' = 'HK') {
        if (parentCode) {
            if (type === 'HK') {
                this.isLoadingDistricts = true;
            } else {
                this.isLoadingDistrictsCurrent = true;
            }
            this.subs.push(
                this.lookupValuesService.getCatalog(Constant.CATALOGS.HUYEN, parentCode).subscribe({
                    next: (res: BaseResponse) => {
                        if (res.code === HTTP_STATUS_CODE.OK) {
                            if (type === 'HK') {
                                this.districts = res.data;
                                this.isLoadingDistricts = false;
                            } else {
                                this.districtsCurrent = res.data;
                                this.isLoadingDistrictsCurrent = false;
                            }
                        } else {
                            this.toastService.error(res?.data);
                        }
                    },
                    error: (err) => {
                        this.toastService.error(err?.message);
                    }
                })
            );
        }
    }

    getWards(parentCode?: number, type: 'HK' | 'HT' = 'HK') {
        if (parentCode) {
            if (type === 'HK') {
                this.isLoadingWards = true;
            } else {
                this.isLoadingWardsCurrent = true;
            }
            this.subs.push(
                this.lookupValuesService.getCatalog(Constant.CATALOGS.XA, parentCode).subscribe({
                    next: (res: BaseResponse) => {
                        if (res.code === HTTP_STATUS_CODE.OK) {
                            if (type === 'HK') {
                                this.wards = res.data;
                                this.isLoadingWards = false;
                            } else {
                                this.wardsCurrent = res.data;
                                this.isLoadingWardsCurrent = false;
                            }
                        } else {
                            this.toastService.error(res?.data);
                        }
                    },
                    error: (err) => {
                        this.toastService.error(err?.message);
                    }
                })
            );
        }
    }

    validateIdNo() {
        const oldIdTypeCode = this.form.controls['oldIdTypeCode'].value;
        const oldIdNo = this.form.controls['oldIdNo'].value;

        const idTypeCode = this.form.controls['idTypeCode'].value;
        const idNo = this.form.controls['idNo'].value;

        // so giay to cu
        if (oldIdTypeCode && oldIdTypeCode === Constant.PAPERS_CODE.ID_NO) { // cmt
            if (oldIdNo && oldIdNo.length !== 9) {
                this.form.controls['oldIdNo'].setErrors({idNoOnlyAccept9Number : true});
            } else {
                this.form.controls['oldIdNo'].setErrors(null);
                this.form.controls['oldIdNo'].setValidators(Validators.required);
            }
        } else if (oldIdTypeCode && oldIdTypeCode === Constant.PAPERS_CODE.CITIZEN_ID) { // cccd
            if (oldIdNo && oldIdNo.length !== 12) {
                this.form.controls['oldIdNo'].setErrors({citizenOnlyAccept12Number : true});
            } else {
                this.form.controls['oldIdNo'].setErrors(null);
                this.form.controls['oldIdNo'].setValidators(Validators.required);
            }
        }

        // so giay to moi
        if (idTypeCode && idTypeCode === Constant.PAPERS_CODE.ID_NO) { // cmt
            if (idNo && idNo.length !== 9) {
                this.form.controls['idNo'].setErrors({idNoOnlyAccept9Number : true});
            } else {
                this.form.controls['idNo'].setErrors(null);
                this.form.controls['idNo'].setValidators(Validators.required);
            }
        } else if (idTypeCode && idTypeCode === Constant.PAPERS_CODE.CITIZEN_ID) { // cccd
            if (idNo && idNo.length !== 12) {
                this.form.controls['idNo'].setErrors({citizenOnlyAccept12Number : true});
            } else {
                this.form.controls['idNo'].setErrors(null);
                this.form.controls['idNo'].setValidators(Validators.required);
            }
        }

    }

    onSave(actionType: 'SAVE' | 'SEND_BROWSE') {
        this.isSubmitted = true;
        this.processFormValue();
        this.validateIdNo();
        if (this.form.valid && this.fileList?.length > 0) {
            const request: TaxNumberRegister = this.form.value;

            request.employeeId = this.employeeId;
            request.regType = Constant.REQUEST_TYPES[2].value;

            request.oldIdTypeCode = this.form.controls['oldIdTypeCode'].value;
            request.oldIdNo = this.form.controls['oldIdNo'].value;
            request.oldIdDate = moment(this.form.controls['oldIdDate'].value).format('DD/MM/YYYY');
            request.oldIdPlace = this.form.controls['oldIdPlace'].value;
            request.taxNo = this.form.controls['taxNo'].value;
            request.taxPlace = this.form.controls['taxPlace'].value;

            request.idDate = moment(this.form.controls['idDate'].value).format('DD/MM/YYYY');
            request.docIdsDelete = this.docIdsDelete;

            request.taxNumberRegisterId = this.registerId ? this.registerId : undefined;
            if (actionType === 'SAVE') {
                request.status = Constant.SAVE_DRAFT;
            } else {
                request.status = Constant.SEND_BROWSE;
            }
            const formData = new FormData();
            formData.append('data', new Blob([JSON.stringify(request)], {
                type: 'application/json'
            }));

            this.fileList.forEach((nzFile: NzSafeAny) => {
                formData.append('files', nzFile);
            });

            this.isLoadingPage = true;
            this.subs.push(
                this.taxRegisterService.saveRecord(formData).subscribe((res: BaseResponse) => {
                    this.isLoadingPage = false;
                    if (res.code === HTTP_STATUS_CODE.OK) {
                        if (actionType === 'SEND_BROWSE') {
                            this.toastService.success(this.translate.instant('personalTax.notification.registerSuccess'));
                        } else {
                            const notification = this.registerId ? 'common.notification.editSuccess' : 'common.notification.addSuccess'
                            this.toastService.success(this.translate.instant(notification));
                        }
                        this.modalRef.close({refresh: true});
                    } else {
                        this.toastService.error(res.message);
                    }
                }, error => {
                    this.isLoadingPage = false;
                    this.toastService.error(error.message);
                })
            );
        }
    }

    approveById(registerId: number) {
        this.isLoadingPage = true;
        this.subs.push(
            this.taxCommonService.approveByList([registerId], UrlConstant.APPROVE_LIST.TAX_REGISTER).subscribe(res => {
                if (res.code === HTTP_STATUS_CODE.OK) {
                    this.toastService.success(this.translate.instant('common.notification.isApprove'));
                    this.modalRef.close({refresh: true});
                } else {
                    this.toastService.error(res.message)
                }
                this.isLoadingPage = false;
            },error => {
                this.isLoadingPage = false;
                this.toastService.error(error.message);
            })
        );
    }

    doCloseReject(refresh: boolean){
        this.isReject = false;
        if (refresh) {
            this.modalRef.close({refresh: true});
        }
    }

    openRejectForm() {
        this.isReject = true;
    }

    processFormValue() {
        this.form.controls['oldIdNo'].setValue(this.form.controls['oldIdNo'].value?.trim());
        this.form.controls['oldIdPlace'].setValue(this.form.controls['oldIdPlace'].value?.trim());
        this.form.controls['idNo'].setValue(this.form.controls['idNo'].value?.trim());
        this.form.controls['idPlace'].setValue(this.form.controls['idPlace'].value?.trim());
        this.form.controls['note'].setValue(this.form.controls['note'].value?.trim());
        this.form.controls['email'].setValue(this.form.controls['email'].value?.trim());
    }

    addOldIdNoValidator(paperType: string) {
        switch (paperType) {
            case Constant.PAPERS_CODE.ID_NO:
            case Constant.PAPERS_CODE.CITIZEN_ID:
                this.form.controls['oldIdPlaceCode'].setValidators(Validators.required);
                this.form.controls['oldIdPlace'].setValidators(null);
                break;
            default :
                this.form.controls['oldIdPlaceCode'].setValidators(null);
                this.form.controls['oldIdPlace'].setValidators(Validators.required);
                break;
        }
    }

    addIdNoValidator(paperType: string) {
        switch (paperType) {
            case Constant.PAPERS_CODE.ID_NO:
            case Constant.PAPERS_CODE.CITIZEN_ID:
                this.form.controls['idPlaceCode'].setValidators(Validators.required);
                this.form.controls['idPlace'].setValidators(null);
                break;
            default :
                this.form.controls['idPlaceCode'].setValidators(null);
                this.form.controls['idPlace'].setValidators(Validators.required);
                break;
        }
    }

    selectOldIdPlace(event: SelectModal) {
      this.form.controls['oldIdPlace'].setValue(event?.itemSelected?.label);
    }

    selectIdPlace(event: SelectModal) {
      this.form.controls['idPlace'].setValue(event?.itemSelected?.label);
    }

    onChangeIssueDate(event: Date, key: string) {
        if (event) {
            const nowDate = new Date();
            if (event >= nowDate) {
                this.form.controls[key].setErrors({errorDate: true});
            } else {
                this.form.controls[key].setErrors(null);
            }
        } else {
            this.form.controls[key].setErrors(null);
        }
    }

    closeModalAdd() {
        this.modalService.closeAll();
    }

    ngOnDestroy(): void {
        this.modalRef?.destroy();
        this.subs?.forEach(sub => sub.unsubscribe());
    }

}
