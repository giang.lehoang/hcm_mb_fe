import {AfterViewInit, Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { HttpParams } from '@angular/common/http';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { Subscription } from 'rxjs';
import {SearchFormComponent} from "@hcm-mfe/personal-tax/ui/search-form";
import {
  Constant,
  UrlConstant
} from "@hcm-mfe/personal-tax/data-access/common";
import {AppFunction, DraftObject, Pagination, TableConfig} from "@hcm-mfe/shared/data-access/models";
import {
  DownloadFileAttachService,
  SearchFormService,
  TaxCommonService
} from "@hcm-mfe/personal-tax/data-access/services";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {Reject} from "@hcm-mfe/personal-tax/data-access/models";
import {ChangeTaxRegisteredInfoFormComponent} from "../change-tax-registered-info-form/change-tax-registered-info-form.component";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import { SessionService } from "@hcm-mfe/shared/common/store";
import {
  doDownloadFileAttach,
  doExportFileUtils,
  openAddModalUtils
} from "@hcm-mfe/personal-tax/data-access/common/utils";
import {
  MbTableMergeCellComponent
} from "../../../../../../shared/ui/mb-table-merge-cell/src/lib/mb-table-merge-cell/mb-table-merge-cell.component";

@Component({
    selector: 'app-change-tax-registered-info',
    templateUrl: './change-tax-registered-info.component.html',
    styleUrls: ['./change-tax-registered-info.component.scss']
})
export class ChangeTaxRegisteredInfoComponent implements OnInit, AfterViewInit, OnDestroy {

    @ViewChild('searchForm') searchForm!: SearchFormComponent;
    @ViewChild('table') table!: MbTableMergeCellComponent;
    @ViewChild('actionTmpl', {static: true}) action!: TemplateRef<NzSafeAny>;
    @ViewChild('statusTmpl', {static: true}) status!: TemplateRef<NzSafeAny>;
    @ViewChild('empStatusTmpl', {static: true}) empStatus!: TemplateRef<NzSafeAny>;
    @ViewChild('attachFileTmpl', {static: true}) attachFile!: TemplateRef<NzSafeAny>;


    tableConfig!: TableConfig;
    dataTable: NzSafeAny[] = [];

    searchFormApprove: FormGroup = this.fb.group([]);

    listRegisterId: number[] = [];
    listRegisterObject: DraftObject[] = [];

    isReject = false;
    isRejectList = false;
    rejectId: number | undefined;

    urlApproveByList = UrlConstant.APPROVE_LIST.TAX_REGISTER;
    urlApproveAll = UrlConstant.APPROVE_ALL.TAX_REGISTER;
    urlDeleteById = UrlConstant.DELETE_DATA.TAX_REGISTER;
    urlReject: string = UrlConstant.REJECT_LIST.TAX_REGISTER;

    isDisabled = false;
    isLoadingPage = false;
    modal: NzModalRef | undefined;
    isImportData = false;


    urlApiImport: string = UrlConstant.IMPORT_FORM.TAX_CHANGE_REGISTER_FORM;
    urlApiDownloadTemp: string = UrlConstant.IMPORT_FORM.TAX_CHANGE_REGISTER_TEMPLATE;

    constant = Constant;
    pagination = new Pagination();
    subs: Subscription[] = [];
    functionCode: string = FunctionCode.PTX_TAX_CHANGE;
    objFunction: AppFunction;

    constructor(
        private searchFormService: SearchFormService,
        private toastService: ToastrService,
        private translateService: TranslateService,
        private downloadFileAttachService: DownloadFileAttachService,
        private modalService: NzModalService,
        private taxCommonService: TaxCommonService,
        private fb: FormBuilder,
	    public sessionService: SessionService
    ) {
      this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.PTX_DEPENDENT_REGISTERS}`);
    }

    ngOnInit(): void {
        this.initTable();
    }

    ngAfterViewInit() {
      this.doSearch(1);
    }

    doSearch(pageIndex: number, refresh = false) {
        this.pagination.pageNumber = pageIndex;
        let searchParam = this.searchForm? this.searchForm.parseOptions() : new HttpParams();
        searchParam = searchParam.set('regType', Constant.REG_TYPE_CODE.REGISTER_CHANGE_TAX);
        this.isLoadingPage = true;
        if (refresh) {
          this.table.resetAllCheckBoxFn();
        }
        this.subs.push(
            this.searchFormService.getFilterResearch(UrlConstant.SEARCH_FORM.TAX_NUMBER_REGISTER, searchParam, this.pagination.getCurrentPage()).subscribe(res => {
                if (res.code === HTTP_STATUS_CODE.OK) {
                    this.dataTable = res.data.listData.map((item: NzSafeAny) => {
                        item.id = item.taxNumberRegisterId;
                        if (item.status && item.status > Constant.STATUS_OBJ.WAIT_APPROVE) { // set disabled status each checkbox in table: optional
                            item.disabled = true;
                        }
                        return item;
                    });
                    this.tableConfig.pageIndex = pageIndex;
                    this.tableConfig.total = res.data.count;
                    this.searchFormApprove = this.searchForm.form;
                    this.searchFormApprove.addControl('regType', new FormControl(Constant.REG_TYPE_CODE.REGISTER_CHANGE_TAX));
                }
                this.isLoadingPage = false;
            }, (err) => {
                this.toastService.error(err?.message);
            })
        );
    }

    private initTable(): void {
        this.tableConfig = {
            headers: [
                {
                    title: "STT",
                    thClassList: ['text-center'],
                    tdClassList: ['text-center'],
                    rowspan: 2,
                    fixed: true,
                    fixedDir: "left",
                    width: 50
                },
                {
                    title: "personalTax.taxRegisters.table.employeeCode",
                    field: "employeeCode",
                    rowspan: 2,
                    fixed: true,
                    fixedDir: "left",
                    width: 130
                },
                {
                    title: "personalTax.taxRegisters.table.empName",
                    field: "empName",
                    rowspan: 2,
                    fixed: true,
                    fixedDir: "left",
                    width: 150
                },
                {
                  title: 'personalTax.dependent.table.statusRegister',
                  field: 'status',
                  thClassList: ['text-center'],
                  tdClassList: ['text-center'],
                  tdTemplate: this.status,
                  width: 200,
                  rowspan: 2,
                },
                {
                    title: "personalTax.taxRegisters.table.taxNo",
                    field: "taxNo",
                    rowspan: 2,
                    width: 150
                },
                {
                    title: "personalTax.taxRegisters.table.taxPlace",
                    field: "taxPlace",
                    rowspan: 2,
                    width: 150
                },
                {
                    title: "personalTax.taxRegisters.table.oldInfo",
                    colspan: 4,
                    child: [
                        {
                            title: "personalTax.taxRegisters.table.oldIdTypeCode",
                            width: 150,
                            field: "oldIdTypeName",
                            thClassList: ['text-center'],
                        },
                        {
                            title: "personalTax.taxRegisters.table.oldIdNo",
                            width: 100,
                            field: "oldIdNo",
                            thClassList: ['text-center']
                        },
                        {
                            title: "personalTax.taxRegisters.table.oldIdDate",
                            width: 100,
                            field: "oldIdDate",
                            thClassList: ['td-date'],
                            tdClassList: ['td-date']
                        },
                        {
                            title: "personalTax.taxRegisters.table.oldIdPlace",
                            field: "oldIdPlace",
                            thClassList: ['text-center'],
                            width: 200
                        },
                    ]
                },
                {
                    title: "personalTax.taxRegisters.table.infoSuggestChange",
                    colspan: 4,
                    child: [
                        {
                            title: "personalTax.taxRegisters.table.idTypeCode",
                            width: 150,
                            field: "idTypeName",
                            thClassList: ['text-center'],
                            rowspan: 1,
                        },
                        {
                            title: "personalTax.taxRegisters.table.idNo",
                            width: 100,
                            field: "idNo",
                            thClassList: ['text-center'],
                        },
                        {
                            title: "personalTax.taxRegisters.table.idDate",
                            width: 100,
                            field: "idDate",
                            thClassList: ['td-date'],
                            tdClassList: ['td-date'],
                        },
                        {
                            title: "personalTax.taxRegisters.table.idPlace",
                            field: "idPlace",
                            thClassList: ['text-center'],
                            rowspan: 1,
                            width: 200
                        },
                    ]
                },
                {
                    title: "personalTax.taxRegisters.table.note",
                    field: "note",
                    width: 200,
                    rowspan: 2,
                    show: false
                },
                {
                    title: "personalTax.taxRegisters.table.mobileNumber",
                    field: "mobileNumber",
                    width: 100,
                    rowspan: 2,
                },
                {
                    title: "personalTax.taxRegisters.table.email",
                    field: "email",
                    width: 200,
                    rowspan: 2,
                },
                {
                    title: 'personalTax.dependent.table.createDate',
                    field: 'createDate',
                    width: 200,
                    tdClassList: ['text-center'],
                    thClassList: ['text-center'],
                    rowspan: 2,
                },
                {
                    title: 'personalTax.dependent.table.statusEmployee',
                    field: 'empStatus',
                    width: 150,
                    tdTemplate: this.empStatus,
                    tdClassList: ['text-center'],
                    thClassList: ['text-center'],
                    show: false,
                    rowspan: 2,
                },
                {
                    title: 'personalTax.dependent.table.orgName',
                    field: 'orgName',
                    width: 250,
                    rowspan: 2,
                    show: false
                },
                {
                    title: 'personalTax.dependent.table.employeeType',
                    field: 'empTypeName',
                    width: 150,
                    show: false,
                    rowspan: 2,
                },
                {
                    title: 'personalTax.dependent.table.jobName',
                    field: 'jobName',
                    width: 150,
                    rowspan: 2,
                    show: false
                },
                {
                    title: "personalTax.taxRegisters.table.rejectReason",
                    field: 'rejectReason',
                    width: 150,
                    rowspan: 2,
                    show: false
                },
                {
                    title: "personalTax.taxRegisters.table.attachFileList",
                    field: "attachFileList",
                    tdTemplate: this.attachFile,
                    width: 300,
                    rowspan: 2,
                    show: false
                },
                {
                    title: " ",
                    field: 'action',
                    rowspan: 2,
                    tdClassList: ['text-nowrap','text-center'], thClassList: ['text-nowrap','text-center'],
                    width: 50,
                    tdTemplate: this.action,
                    fixed: window.innerWidth > 1024,
                    fixedDir: 'right',
                    show: this.objFunction?.approve || this.objFunction?.delete || this.objFunction?.edit
                },
            ],
            needScroll: true,
            total: 0,
            loading: false,
            size: 'small',
            showSelect: true,
            pageSize: this.pagination.pageSize,
            pageIndex: 1
        };
    }

    onAmountSelectedRowChange(event: NzSafeAny) {
      this.listRegisterId = Array.from(event);
    }

    openRejectById(data: Reject) {
        if(data.status === Constant.STATUS_OBJ.WAIT_APPROVE) {
            this.isReject = true;
            this.rejectId = data.registerId;
        }
    }

    openRejectByList() {
        const listRejected:string[] = [];
        let error = false;
        this.listRegisterObject.forEach((data:DraftObject) => {
            if(data.status === Constant.STATUS_OBJ.FINANCIAL_ACCOUNTANT_RETURN) {
                listRejected.push(data.employeeCode);
                error = true;
            }
        })
        if (error) {
            this.toastService.error(this.translateService.instant('common.notification.employeeCode') + " {" + listRejected.toString() + "} " + this.translateService.instant('common.notification.rejected'));
        } else {
            this.isReject = true;
            this.isRejectList = true;
        }
    }

    resetListRegisterId() {
        this.listRegisterId = [];
        this.doSearch(this.pagination.pageNumber, true);
    }


    doDownloadAttach(file: NzSafeAny) {
        doDownloadFileAttach(~~file?.docId, file?.security, file.fileName, this.downloadFileAttachService, this.toastService, this.translateService);
    }


    openAddModal() {
        this.modal = openAddModalUtils(null, this.modalService, this.translateService,"personalTax.label.registerChangeAdd", ChangeTaxRegisteredInfoFormComponent, 1.2)
        this.modal?.afterClose.subscribe(res => {
            if (res && res.refresh) {
              this.doSearch(1);
            }
        })
    }

    openModalEdit(registerId: number) {
        this.modal = openAddModalUtils(registerId, this.modalService, this.translateService,"personalTax.modalName.registerChangeEdit", ChangeTaxRegisteredInfoFormComponent, 1.2)
        const sub = this.modal?.afterClose.subscribe(res => {
          if (res && res.refresh) {
            this.doSearch(1);
          }
        });
        if (sub) {
          this.subs.push(sub);
        }
    }

    openModalDetail (registerId: number) {
        this.modal = openAddModalUtils(registerId, this.modalService, this.translateService,"personalTax.modalName.registerChangeDetail", ChangeTaxRegisteredInfoFormComponent, 1.2, true, this.objFunction)
        const sub = this.modal?.afterClose.subscribe(res => {
            if (res && res.refresh) {
              this.doSearch(1);
            }
        });
        if (sub) {
            this.subs.push(sub);
        }
    }

    dblClickRow(event: NzSafeAny) {
      this.openModalDetail(event.taxNumberRegisterId);
    }

    doImportData() {
        this.isImportData = true;
    }

    doCloseImport(refresh: boolean) {
        this.isImportData = false;
        if (refresh) {
            this.doSearch(this.pagination.pageNumber)
        }
    }

    doCloseReject(refresh: boolean){
        this.isReject = false;
        if (refresh) {
            this.doSearch(this.pagination.pageNumber);
        }
    }

    async doExportData(exportType: 'DEFAULT' | 'ACCORDING_TAX_AUTHORITY') {
        let urlExport;
        let fileName;
        if (exportType === 'DEFAULT') {
            fileName = "Danh-sach-dk-thay-doi-mst.xlsx";
            urlExport = UrlConstant.EXPORT_REPORT.TAX_CHANGE;
        } else {
            fileName = "Danh-sach-dk-thay-doi-mst-theo-mau-co-quan-thue.xls";
            urlExport = UrlConstant.EXPORT_REPORT.TAX_REGISTER_ACCORDING_TAX_AUTHORITY;
        }
        this.isLoadingPage = true;
        let searchParam = this.searchForm ? this.searchForm.parseOptions() : new HttpParams();
        searchParam = searchParam.set('regType', Constant.REG_TYPE_CODE.TAX_CHANGE);
        this.isLoadingPage = await doExportFileUtils(urlExport, searchParam, this.taxCommonService, this.toastService, this.translateService, fileName);
    }

    onLoadPage(isLoadPage: boolean) {
        this.isLoadingPage = isLoadPage;
    }

    ngOnDestroy(): void {
        this.modal?.destroy();
        this.subs?.forEach(sub => sub.unsubscribe());
    }

}
