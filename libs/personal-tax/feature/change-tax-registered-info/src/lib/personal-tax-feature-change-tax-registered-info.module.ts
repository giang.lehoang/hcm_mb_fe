import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ChangeTaxRegisteredInfoComponent} from "./change-tax-registered-info/change-tax-registered-info.component";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {PersonalTaxUiSearchFormModule} from "@hcm-mfe/personal-tax/ui/search-form";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {TranslateModule} from "@ngx-translate/core";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {NzIconModule} from "ng-zorro-antd/icon";
import {PersonalTaxUiBtnActionByListModule} from "@hcm-mfe/personal-tax/ui/btn-action-by-list";
import {SharedUiMbTableMergeCellModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell";
import {SharedUiMbTableMergeCellWrapModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell-wrap";
import {PersonalTaxUiBtnActionByIdModule} from "@hcm-mfe/personal-tax/ui/btn-action-by-id";
import {NzTagModule} from "ng-zorro-antd/tag";
import {PersonalTaxUiEmpStatusCommonModule} from "@hcm-mfe/personal-tax/ui/emp-status-common";
import {PersonalTaxUiImportCommonModule} from "@hcm-mfe/personal-tax/ui/import-common";
import {PersonalTaxUiRejectCommonModule} from "@hcm-mfe/personal-tax/ui/reject-common";
import {RouterModule} from "@angular/router";
import {NzFormModule} from "ng-zorro-antd/form";
import {PersonalTaxUiPersonalInformationModule} from "@hcm-mfe/personal-tax/ui/personal-information";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {NzDividerModule} from "ng-zorro-antd/divider";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {SharedDirectivesNumberInputModule} from "@hcm-mfe/shared/directives/number-input";
import {NzUploadModule} from "ng-zorro-antd/upload";
import {ChangeTaxRegisteredInfoFormComponent} from "./change-tax-registered-info-form/change-tax-registered-info-form.component";

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, PersonalTaxUiSearchFormModule, SharedUiMbButtonModule, TranslateModule,
    NzButtonModule, NzDropDownModule, NzIconModule, PersonalTaxUiBtnActionByListModule, SharedUiMbTableMergeCellModule,
    SharedUiMbTableMergeCellWrapModule, PersonalTaxUiBtnActionByIdModule, NzTagModule, PersonalTaxUiEmpStatusCommonModule,
    PersonalTaxUiImportCommonModule, PersonalTaxUiRejectCommonModule, PersonalTaxUiPersonalInformationModule, ReactiveFormsModule, FormsModule,
    SharedUiMbInputTextModule, NzDividerModule, SharedUiMbSelectModule, SharedUiMbDatePickerModule, SharedDirectivesNumberInputModule, NzUploadModule,
    RouterModule.forChild([
      {
        path: '',
        component: ChangeTaxRegisteredInfoComponent
      }
    ]), NzFormModule
  ],
  declarations: [ChangeTaxRegisteredInfoComponent, ChangeTaxRegisteredInfoFormComponent],
  exports: [ChangeTaxRegisteredInfoComponent],
})
export class PersonalTaxFeatureChangeTaxRegisteredInfoModule {}
