import {AfterViewInit, Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {AppFunction, Pagination, TableConfig} from "@hcm-mfe/shared/data-access/models";
import {Constant, UrlConstant} from "@hcm-mfe/personal-tax/data-access/common";
import {NzModalRef, NzModalService} from "ng-zorro-antd/modal";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {Subscription} from "rxjs";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {
  DownloadFileAttachService,
  SearchFormService,
  TaxCommonService
} from "@hcm-mfe/personal-tax/data-access/services";
import {TranslateService} from "@ngx-translate/core";
import {SessionService} from "@hcm-mfe/shared/common/store";
import {ToastrService} from "ngx-toastr";
import {HttpParams} from "@angular/common/http";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {doExportFileUtils} from "@hcm-mfe/personal-tax/data-access/common/utils";
import {SearchFormComponent} from "../search-form/search-form.component";

@Component({
  selector: 'report-dependent-person',
  templateUrl: './report-dependent-person.component.html',
  styleUrls: ['./report-dependent-person.component.scss']
})
export class ReportDependentPersonComponent implements OnInit, AfterViewInit, OnDestroy {

  pagination = new Pagination();
  tableConfig!: TableConfig;
  searchResult: NzSafeAny[] = [];
  constant = Constant;
  modal!: NzModalRef;
  isLoadingPage = false;
  functionCode: string = FunctionCode.PTX_REPORT_DEPENDENT;
  objFunction: AppFunction;
  subscriptions: Subscription[] = [];

  @ViewChild('searchForm') searchForm!: SearchFormComponent;
  @ViewChild('statusTmpl', {static: true}) status!: TemplateRef<NzSafeAny>;
  @ViewChild('empStatusTmpl', {static: true}) empStatus!: TemplateRef<NzSafeAny>;

  constructor(
    private searchFormService: SearchFormService,
    private translateService: TranslateService,
    private modalService: NzModalService,
    private taxCommonService: TaxCommonService,
    public sessionService: SessionService,
    private downloadFileAttachService: DownloadFileAttachService,
    private toastService: ToastrService) {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.PTX_DEPENDENT_REGISTERS}`);
  }

  ngOnInit(): void {
    this.initTable();
  }

  ngAfterViewInit() {
    this.doSearch(1);
  }

  doSearch(pageIndex: number) {
    this.pagination.pageNumber = pageIndex;
    const searchParam = this.searchForm ? this.searchForm.parseOptions() : new HttpParams();
    this.isLoadingPage = true;
    this.subscriptions.push(
      this.searchFormService.getFilterResearch(UrlConstant.SEARCH_FORM.DEPENDENT_REPORT, searchParam, this.pagination.getCurrentPage()).subscribe(res => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.searchResult = res.data?.listData;
            this.tableConfig.pageIndex = pageIndex;
            this.tableConfig.total = res.data.count;
          }
          this.isLoadingPage = false;
        }, error => {
          this.toastService.error(error.message);
          this.isLoadingPage = false;
        }
      )
    );
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'STT',
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          fixed: true,
          fixedDir: 'left',
          width: 50
        },
        {
          title: 'personalTax.dependent.table.employeeCode',
          field: 'employeeCode', width: 130,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left'
        },
        {
          title: 'personalTax.dependent.table.employeeName',
          fixed: window.innerWidth > 1024,
          fixedDir: 'left',
          field: 'empName', width: 150,
        },
        {
          title: 'personalTax.dependent.table.taxNoEmp',
          field: 'empTaxNo',
          width: 150
        },
        {
          title: 'personalTax.dependent.table.statusEmployee',
          field: 'empStatusName', width: 150,
          tdTemplate: this.empStatus,
          thClassList: ['text-center'],
          tdClassList: ['text-center']
        },
        {
          title: 'personalTax.dependent.table.orgName',
          field: 'orgName', width: 250,
        },
        {
          title: 'personalTax.dependent.table.jobName',
          field: 'jobName', width: 150,
        },
        {
          title: 'personalTax.dependent.table.employeeType',
          field: 'empTypeName', width: 150,
        },
        {
          title: 'personalTax.dependent.table.dependentName',
          field: 'dependentName',
          width: 150,
        },
        {
          title: 'personalTax.dependent.table.dateOfBirth',
          field: 'fDateOfBith',
          width: 120,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
        },
        {
          title: 'personalTax.dependent.table.dependentPersonCode',
          field: 'dependentPersonCode',
          width: 150,
        },
        {
          title: 'personalTax.dependent.table.taxNo',
          field: 'ftaxNo',
          width: 120,
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        },
        {
          title: 'personalTax.dependent.table.idNo',
          field: 'fpersonalId',
          width: 120,
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        },
        {
          title: 'personalTax.dependent.table.codeNo',
          field: 'codeNo',
          width: 100,
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        },
        {
          title: 'personalTax.dependent.table.bookNo',
          field: 'bookNo',
          width: 100,
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        },
        {
          title: 'personalTax.dependent.table.fromDate',
          field: 'fromMonth',
          width: 100,
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        },
        {
          title: 'personalTax.dependent.table.endDate',
          field: 'toMonth',
          width: 100,
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        },
        {
          title: 'personalTax.dependent.table.note',
          field: 'note',
          width: 200,
          thClassList: ['text-center']
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  async doExportData(type?: 'GROUP') {
    let fileName = 'Danh_sach_giam_tru.xlsx';
    let UrlExport = UrlConstant.EXPORT_REPORT.DEPENDENT_REPORT;
    if (type) {
      fileName = 'Thong_ke_giam_tru.xlsx';
      UrlExport = UrlConstant.EXPORT_REPORT.DEPENDENT_REPORT_GROUP;
    }
    this.isLoadingPage = true;
    const searchParam = this.searchForm ? this.searchForm.parseOptions() : new HttpParams();
    this.isLoadingPage = await doExportFileUtils(UrlExport, searchParam, this.taxCommonService, this.toastService, this.translateService, fileName);
  }

  onLoadPage(isLoadPage: boolean) {
    this.isLoadingPage = isLoadPage;
  }

  ngOnDestroy(): void {
    this.modal?.destroy();
    this.subscriptions?.forEach(sub => sub.unsubscribe());
  }

}
