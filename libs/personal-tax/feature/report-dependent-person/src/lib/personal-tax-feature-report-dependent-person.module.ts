import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SearchFormComponent} from './search-form/search-form.component';
import {ReportDependentPersonComponent} from './report-dependent-person/report-dependent-person.component';
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {TranslateModule} from "@ngx-translate/core";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {NzIconModule} from "ng-zorro-antd/icon";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {NzFormModule} from "ng-zorro-antd/form";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbTableMergeCellWrapModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell-wrap";
import {SharedUiMbTableMergeCellModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell";
import {NzTagModule} from "ng-zorro-antd/tag";
import {PersonalTaxUiEmpStatusCommonModule} from "@hcm-mfe/personal-tax/ui/emp-status-common";
import {RouterModule} from "@angular/router";
import {SharedUiOrgDataPickerModule} from "@hcm-mfe/shared/ui/org-data-picker";
import {SharedUiMbSelectCheckAbleModule} from "@hcm-mfe/shared/ui/mb-select-check-able";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule,
    SharedUiMbButtonModule,
    TranslateModule,
    NzButtonModule,
    NzDropDownModule,
    NzIconModule,
    SharedUiMbTableWrapModule,
    SharedUiMbTableModule,
    NzFormModule,
    ReactiveFormsModule,
    SharedUiMbTableMergeCellWrapModule,
    SharedUiMbTableMergeCellModule,
    NzTagModule,
    PersonalTaxUiEmpStatusCommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: ReportDependentPersonComponent
      }
    ]),
    CommonModule, SharedUiOrgDataPickerModule, SharedUiMbSelectCheckAbleModule, SharedUiMbInputTextModule,
    SharedUiMbDatePickerModule, FormsModule, ReactiveFormsModule, NzFormModule, TranslateModule
  ],
  declarations: [
    SearchFormComponent,
    ReportDependentPersonComponent
  ],
})
export class PersonalTaxFeatureReportDependentPersonModule {}
