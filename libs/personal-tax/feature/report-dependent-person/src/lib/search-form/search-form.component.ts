import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {FormBuilder, FormGroup} from "@angular/forms";
import {TranslateService} from "@ngx-translate/core";
import {HttpParams} from "@angular/common/http";
import {CatalogModel} from "@hcm-mfe/shared/data-access/models";
import {Constant} from "@hcm-mfe/personal-tax/data-access/common";
import * as moment from "moment";
import { Scopes } from '@hcm-mfe/shared/common/enums';

@Component({
  selector: 'search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit, OnDestroy {
  @Input() functionCode = '';
  @Input() scope = Scopes.VIEW;

  form!: FormGroup;
  subscriptions: Subscription[] = [];
  statusEmployeeList: CatalogModel[] = [];

  constructor(
    private fb: FormBuilder,
    private translate: TranslateService,
  ) {
  }

  ngOnInit(): void {
    this.innitDataSelect();
    this.initForm();
  }

  initForm() {
    this.form = this.fb.group({
      orgId: null,
      empCode: null,
      empName: null,
      dateReport: [new Date()],
      listEmpStatus: [null]
    });
  }

  innitDataSelect() {
    this.statusEmployeeList = Constant.STATUSES_EMPLOYEE.map(item => {
      item.label = this.translate.instant(item.label);
      return item;
    });
  }

  get formControl() {
    return this.form.controls;
  }

  parseOptions() {
    let params = new HttpParams();
    if (this.formControl['orgId'].value !== null)
      params = params.set('orgId', this.formControl['orgId'].value?.orgId);
    if (this.formControl['empCode'].value !== null)
      params = params.set('empCode', this.formControl['empCode'].value);
    if (this.formControl['empName'].value !== null)
      params = params.set('empName', this.formControl['empName'].value);
    if (this.formControl['listEmpStatus'].value !== null)
      params = params.set('listEmpStatus', this.formControl['listEmpStatus'].value.join(','));
    if (this.formControl['dateReport'].value !== null)
      params = params.set('dateReport', moment(this.formControl['dateReport'].value).format('DD/MM/YYYY'));
    return params;
  }

  setFormValue(event: any, formName: string) {
    if(formName !== 'listEmpStatus' || event.listOfSelected.length > 0) {
      this.formControl[formName].setValue(event.listOfSelected);
    }
  }

  ngOnDestroy(): void {
    this.subscriptions?.forEach(sub => sub.unsubscribe());
  }
}
