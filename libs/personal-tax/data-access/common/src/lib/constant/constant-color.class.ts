export class ConstantColor {

    public static readonly COLOR = {
        BG_PRIMARY_DARK: '#24264E',
        BG_PRIMARY_DARKER: '#00009A',
        BG_PRIMARY_DEFAULT: '#141ED2',
        BG_PRIMARY_LIGHT: '#7F85F3',
        BG_PRIMARY_BG: '#E9EAFF',
        DISABLED: '#c0c0c0'
    };

    public static readonly TAG = {
        STATUS_INITIALIZE: '#E8EAF6', // Khởi tạo
        STATUS_APPROVED: '#DAF9EC', // Phê duyệt
        STATUS_NOT_APPROVED: '#FFC9C9',  // Từ chối

        STATUS_IS_WORKING: '#DAF9EC', // Hiện diện
        STATUS_SUSPENSE: '#FFF2DA', // Tạm hoãn
        STATUS_RETIRED: '#FDE7EA', // Đã nghỉ việc
    };

}
