export class UrlConstant {
  public static readonly API_VERSION = '/v1.0';

  public static readonly T24_ACCOUNT_REQUEST = {
    SAVE: '/t24-account-reqs',
    GET_BY_ID: '/t24-account-reqs/{id}',
    EXPORT: '/t24-account-reqs/export-t24-account-reqs',
    GET_T24_INFO_EMPLOYEE_HCM: '/employees/{employeeId}/t24-info',
    GET_T24_INFO_EMPLOYEE_TAX: '/t24-account-reqs/employee/{employeeId}/t24-info',
  }

  public static readonly TAX_SEARCH = {
    PREFIX: '/admin/tax',
    SEARCH: '/search-tax-no',
    EXPORT: '/export-employee',
    LOOKUP_EMP_TYPE: '/lookup-values?typeCode=DOI_TUONG_CV',
  };

  public static readonly EMPLOYEES = {
    PREFIX: '/employees',
    DATA_PICKER: '/data-picker',
    PERSONAL_INFO: '/personal-information',
    IDENTITIES: '/identities',
  }

  public static readonly SEARCH_FORM = {
    TAX_NUMBER_REGISTER: "/admin/tax-number-registers",
    DEPENDENT_REGISTER: "/admin/dependent-registers",
    DECLARATION_REGISTER: "/admin/declaration-registers",
    RECEIVE_INVOICE: "/admin/invoice-requests",
    CONFIRM_PROVIDE_INFO: "/admin/confirm-registers",
    TAX_REGISTER: "/admin/tax-number-registers",
    T24_ACCOUNT_REQUESTS: "/t24-account-reqs",
    DEPENDENT_REPORT: '/admin/dependent-registers/search-by-month'

  };

  public static readonly CONFIRM_REGISTERS = {
    SAVE: '/personal/confirm-registers',
    GET_BY_ID: '/personal/confirm-registers/{id}'
  }

  public static readonly INVOICE_REQUEST = {
    SAVE: '/admin/invoice-requests',
    GET_BY_ID: '/admin/invoice-requests/{id}',
    REQUEST_BY_LIST: '/admin/invoice-requests/export-invoice-by-list',
    REQUEST_BY_FORM: '/admin/invoice-requests/export-invoice-by-form',
  }

  public static readonly IMPORT_FORM = {
    DEPENDENT_REGISTER_FORM: '/admin/dependent-registers/import-process/new-register-result',
    DEPENDENT_REGISTER_TEMPLATE: '/admin/dependent-registers/import-template/new-register-result',
    DEPENDENT_DEDUCTION_FORM: '/admin/dependent-registers/import-process/cancel-register-result',
    DEPENDENT_DEDUCTION_TEMPLATE: '/admin/dependent-registers/import-template/cancel-register-result',
    TAX_NEW_REGISTER_FORM: '/admin/tax-number-registers/import-process/new-register-result',
    TAX_NEW_REGISTER_TEMPLATE: '/admin/tax-number-registers/import-template/new-register-result',
    TAX_CHANGE_REGISTER_FORM: '/admin/tax-number-registers/import-process/change-register-result',
    TAX_CHANGE_REGISTER_TEMPLATE: '/admin/tax-number-registers/import-template/change-register-result',
    INVOICE_REQUEST: '/admin/invoice-requests/import-process',
    INVOICE_REQUEST_TEMPLATE: '/admin/invoice-requests/import-template',
    DECLARATION_REGISTER: '/admin/declaration-registers/import-process',
    DECLARATION_REGISTER_TEMPLATE: '/admin/declaration-registers/import-template',
  };

  public static readonly CATEGORY = {
    LOOKUP_VALUE: '/lookup-values',
    MP_JOB: '/category/mp-jobs',
    ORG_MANAGE: '/org-tree/get-org-by-level'
  }
  public static readonly TAX_REGISTERS = {
    UPDATE_WORK_FLOW: '/admin/tax-number-registers/update-work-flow/{id}/{status}',
    DELETE_DATA: '/admin/tax-number-registers/{id}',
    SAVE: '/admin/tax-number-registers',
    GET_BY_ID: '/admin/tax-number-registers/{id}',
    GET_RECENT_REGISTER: '/admin/tax-number-registers/recent-register/{employeeId}',
  }

  public static readonly DEPENDENT_REGISTERS = {
    SAVE_DEPENDENT: '/admin/dependent-registers',
    GET_BY_ID: '/admin/dependent-registers/{id}',
    UPDATE_WORK_FLOW: '/admin/dependent-registers/update-work-flow/{id}/{status}',
    DELETE_DATA: '/admin/dependent-registers/{id}',
  }

  public static readonly LOCK_DATE_REGISTERS = {
    SAVE: '/lock-registrations'
  }

  public static readonly RELATIVES_INFO = {
    LIST: '/family-relationships/employees/{employeeId}',
  }

  public static readonly DEPENDENT_INFO = {
    LIST: '/dependent-persons/employees/{employeeId}',
    SEND_EMAIL: '/admin/dependent-registers/send-mail-confirm-dependent'
  }

  public static readonly CATALOGS = {
    PREFIX: '/lookup-values',
  }

  public static readonly DECLARATION_REGISTERS = {
    SAVE: '/admin/declaration-registers',
    CHECK: '/admin/declaration-registers/check-rev-invoice',
    GET_BY_ID: '/admin/declaration-registers/{id}',
    GET_LIST_YEAR: '/lock-registrations',
  }
  public static readonly DOWNLOAD_FILE_ATTACH = '/download/file';
  public static readonly DOWNLOAD_FILE_ATTACHMENT = '/download/file?docId=';

  public static readonly APPROVE_LIST = {
    DEPENDENT_REGISTER: '/admin/dependent-registers/approve-by-list',
    DECLARATION_REGISTER: '/admin/dependent-registers/approve-by-list',
    TAX_REGISTER:'/admin/tax-number-registers/approve-by-list',
    CONFIRM_REGISTERS: '/admin/confirm-registers/approve-by-list'
  }

  public static readonly REJECT_LIST = {
    DEPENDENT_REGISTER: '/admin/dependent-registers/reject-by-list',
    TAX_REGISTER:'/admin/tax-number-registers/reject-by-list',
    CONFIRM_REGISTERS: '/admin/confirm-registers/reject-by-list'
  }

  public static readonly APPROVE_ALL = {
    DEPENDENT_REGISTER: '/admin/dependent-registers/approve-all',
    TAX_REGISTER:'/admin/tax-number-registers/approve-all',
    CONFIRM_REGISTERS: '/admin/confirm-registers/approve-all'
  }

  public static readonly DELETE_DATA = {
    DEPENDENT_REGISTER: '/admin/dependent-registers/{id}',
    DECLARATION_REGISTER: '/admin/declaration-registers/{id}',
    TAX_REGISTER: '/admin/tax-number-registers/{id}',
    INVOICE_REQUEST: '/admin/invoice-requests/{id}',
    T24_ACCOUNT_REQUESTS: '/t24-account-reqs/{id}',
    CONFIRM_REGISTERS: '/admin/confirm-registers/{id}'
  }

  public static readonly EXPORT_REPORT = {
    DEPENDENT_REGISTER: '/admin/dependent-registers/export',
    DEPENDENT_REGISTER_ACCORDING_TAX_AUTHORITY: '/admin/dependent-registers/export-according-tax-authority',
    DECLARATION_REGISTER: '/admin/declaration-registers/export',
    TAX_REGISTER: '/admin/tax-number-registers/export-new-register',
    TAX_REGISTER_ACCORDING_TAX_AUTHORITY: '/admin/tax-number-registers/export',
    TAX_CHANGE: '/admin/tax-number-registers/export-change-register',
    CONFIRM_PROVIDE: '/admin/confirm-registers/export',
    RECEIVE_INVOICE: '/admin/invoice-requests/export',
    DEPENDENT_REPORT: '/admin/dependent-registers/export-by-month',
    DEPENDENT_REPORT_GROUP: '/admin/dependent-registers/export-group-by-month'
  }

  public static readonly CREATE_REQUEST = {
    T24_ACCOUNT_REQUESTS: '/t24-account-reqs/create-request',
  }

  public static readonly CREATE_REQUEST_ALL = {
    T24_ACCOUNT_REQUESTS: '/t24-account-reqs/create-request-all',
  }
}
