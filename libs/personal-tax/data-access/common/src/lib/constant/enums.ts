
export enum PERSONAL_TYPE {
  PERSONAL = 1,
  OTHER = 2
}

export enum SCREEN_MODE {
  CREATE = 'CREATE', // Thêm mới
  UPDATE = 'UPDATE', // Cập nhật
  AJUST = 'AJUST', // Điều chỉnh
  VIEW = 'VIEW', // Xem chi tiết
}

export enum REQUEST_STATUS {
  DRAFT = 1, //1 = Dự thảo
  WAIT_APPROVE = 2, //2 = Chờ phê duyệt
  APPROVED = 3, //3 = Đã phê duyệt
  REJECT = 4, //4 = Từ chối phê duyệt
  REQUEST_CANCEL = 7, // Đề nghị hủy
  CANCEL = 5, //5 = Đã Hủy
  AJUST = 6, //6 = Đề nghị điều chỉnh
}

export enum LEAVE_TYPE {
  LEAVE = "LEAVE",
  WORK_EARLY = "WORK_EARLY",
  BASIC_RESEARCH = "BASIC_RESEARCH",
  MISSION = "MISSION",
  OT = "OT",
  SATURDAY = "SATURDAY",
  TRAINING = "TRAINING",
}

export enum REQUEST_APPROVER_STATUS {
  WAIT_APPROVE = 1, // "Chờ duyệt"
  APPROVED = 2, // "Đã duyệt"
  REJECT = 3, // "Từ chối"
}

export enum PAGE_NAME {
    APPROVE = "APPROVE", // "Màn hình phê duyệt đơn nghỉ"
    EMPLOYEE = "EMPLOYEE", // "Màn hình đơn nghỉ của tôi hoặc tôi tạo"
    ADMIN =  "ADMIN", // "Màn hình quản lý nghỉ bên HR"
}