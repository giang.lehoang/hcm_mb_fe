export interface ObjectCategory {
  label: string,
  value: string
}

import {environment} from "@hcm-mfe/shared/environment";

export class Constant {

  // 0: Thành công, 1: Đang nhập, 2: Gửi duyệt, 3: Đang xử lý, 4: Thất bại
  public static readonly SALARY_ACCOUNT_STATUSES = [
    {label: 'personalTax.salaryAccountReqs.statusSelect.isTyping', value: 1},
    {label: 'personalTax.salaryAccountReqs.statusSelect.sendBrowse', value: 2},
    {label: 'personalTax.salaryAccountReqs.statusSelect.isProcessing', value: 3},
    {label: 'personalTax.salaryAccountReqs.statusSelect.success', value: 0},
    {label: 'personalTax.salaryAccountReqs.statusSelect.createSalaryAccountFail', value: 4},
    {label: 'personalTax.salaryAccountReqs.statusSelect.mapSalaryToPaymentFail', value: 5},
  ]

  public static readonly SALARY_ACCOUNT_REQUEST_TYPES = [
    {label: 'personalTax.salaryAccountReqs.requestTypeSelect.openSalaryAccount', value: 0},
    {label: 'personalTax.salaryAccountReqs.requestTypeSelect.updatePaymentAccount', value: 1},
  ]

  // public static readonly HR_LEVELS : ObjectCategory[] = [
  //   { label: 'staffAbs.approvalFlow.hrLevelSelect.LEVEL0', value: '0' }, // Không cần
  //   { label: 'staffAbs.approvalFlow.hrLevelSelect.LEVEL1', value: '1'}, //HR HO
  //   { label: 'staffAbs.approvalFlow.hrLevelSelect.LEVEL2', value: '2' }, //HR HO và TP DVNS
  //   { label: 'staffAbs.approvalFlow.hrLevelSelect.LEVEL3', value: '3' }, //HR HO , TP DVNS và GĐ NS
  // ]

  // public static readonly CatalogType = {
  //   LOAI_HINH_CHAM_CONG: 'LOAI_HINH_CHAM_CONG'
  // }

  public static readonly IS_AUTHORIZATION = 'Ủy quyền';
  public static readonly IS_NOT_AUTHORIZATION = 'Tự quyết toán';
  public static readonly SEND_BROWSE = 2;
  public static readonly SAVE_DRAFT = 1;
  public static readonly VN_NATION = "Việt Nam";

  public static readonly STATUSES_REGISTER = [
    {label: 'personalTax.taxRegisters.statusSelect.waitApprove', value: '2', bgColor: '#FFF2DA', color: '#F99600'},
    {label: 'personalTax.taxRegisters.statusSelect.financialAccountantReceived', value: '3', bgColor: '#FFF2DA', color: '#F99600'},
    {label: 'personalTax.taxRegisters.statusSelect.financialAccountantIsProcessing', value: '4', bgColor:'#FFF2DA', color: '#F99600'},
    {label: 'personalTax.taxRegisters.statusSelect.financialAccountantReturn', value: '5', bgColor: '#FDE7EA', color: '#FA0B0B'},
    {label: 'personalTax.taxRegisters.statusSelect.taxAuthoritiesAgree', value: '6', bgColor: '#DAF9EC', color: '#06A561'},
    {label: 'personalTax.taxRegisters.statusSelect.taxAuthoritiesReturn', value: '7', bgColor: '#FDE7EA', color: '#FA0B0B'},
      // { label: 'selfService.taxRegisters.statusSelect.endDueOff', value: '8', color: '#FA0B0B', bgColor: '#FDE7EA' },  // no delete
      // { label: 'selfService.taxRegisters.statusSelect.proposeCancel', value: '9', color: '', bgColor: '' }, // no delete
      // { label: 'personalTax.taxRegisters.statusSelect.registered', value: '10', color: '#F99600', bgColor: '#FFF2DA' },
      // { label: 'personalTax.taxRegisters.statusSelect.confirmed', value: '11', color: '#06A561', bgColor: '#DAF9EC' }
  ];

  public static readonly T24_ACCOUNT_REQS_STATUSES = [
    {label: 'personalTax.t24AccountReqStatus.success', value: 0, bgColor: '#DAF9EC', color: '#06A561'},
    {label: 'personalTax.t24AccountReqStatus.isTyping', value: 1, bgColor:'#FFF2DA', color: '#F99600'},
    {label: 'personalTax.t24AccountReqStatus.sendBrowse', value: 2, bgColor: '#FFF2DA', color: '#F99600'},
    {label: 'personalTax.t24AccountReqStatus.isProcessing', value: 3, bgColor: '#FFF2DA', color: '#F99600'},
    {label: 'personalTax.t24AccountReqStatus.createSalaryAccountFail', value: 4, bgColor:'#FDE7EA', color: '#FA0B0B'},
    {label: 'personalTax.t24AccountReqStatus.mapSalaryToPaymentFail', value: 5, bgColor:'#FDE7EA', color: '#FA0B0B'},
  ];

  public static readonly CONFIRM_PROVIDE_INFO = [
    {label: 'personalTax.confirmProvide.isAccepted', value: '1'},
    {label: 'personalTax.confirmProvide.isNotAccepted', value: '0'},
  ];

  public static readonly CONFIRM_PROVIDE_STATUS = [
    {label: 'personalTax.confirmProvide.isWaiting', value: '2', bgColor: '#FFF2DA', color: '#F99600'},
    {label: 'personalTax.confirmProvide.confirmed', value: '11', bgColor: '#DAF9EC', color: '#06A561'},
  ];

  public static readonly REQUEST_TYPES = [
    {label: 'personalTax.taxRegisters.requestTypeSelect.select', value: ''},
    {label: 'personalTax.taxRegisters.requestTypeSelect.registerNew', value: '1'},
    {label: 'personalTax.taxRegisters.requestTypeSelect.alterTaxInfo', value: '2'},
    {label: 'personalTax.taxRegisters.requestTypeSelect.registerNewDP', value: '3'},
    {label: 'personalTax.taxRegisters.requestTypeSelect.registerLowDP', value: '4'},
    {label: 'personalTax.taxRegisters.requestTypeSelect.registerTaxSettlement', value: '5'},
    {label: 'personalTax.taxRegisters.requestTypeSelect.registerReceiveReceipt', value: '6'},
    {label: 'personalTax.taxRegisters.requestTypeSelect.confirmAuthorized', value: '7'},
  ]

  public static readonly PAPERS_CODE = environment?.production ?
    {ID_NO: 'CMT', CITIZEN_ID: 'CCCD'} : (environment?.isMbBank ? {ID_NO: '1010', CITIZEN_ID: '2080'} : {ID_NO: '03', CITIZEN_ID: '05'})


  public static readonly REV_INVOICE_STATUS_NAME = {
    IS_REV_INVOICE: 'Có',
  }

  public static readonly REG_TYPE = {
    REGISTER_NEW: "1",
    ALTER_TAX_INFO: "2",
    REGISTER_NEW_DP: "3",
    REGISTER_LOW_DP: "4",
    REGISTER_TAX_SETTLEMENT: "5",
    REGISTER_RECEIVE_RECEIPT: "6",
    CONFIRM_AUTHORIZED: "7"
  };

  public static readonly REG_TYPE_CODE = {
    TAX_CREATE: 'CREATE',
    TAX_CHANGE: "CHANGE",
    REGISTER_CHANGE_TAX: 'CHANGE',
    REGISTER_NEW_DP: 'CREATE',
    REGISTER_LOW_DP: 'CANCEL',
    REGISTER_TAX_SETTLEMENT: 'DECLARE',
    REGISTER_RECEIVE_RECEIPT: 'REV_INVOICE',
  }


  public static readonly STATUS_OBJ = {
    IS_DECLARING: 1,
    WAIT_APPROVE: 2,
    FINANCIAL_ACCOUNTANT_RECEIVED: 3,
    FINANCIAL_ACCOUNTANT_IS_PROCESSING: 4,
    FINANCIAL_ACCOUNTANT_RETURN: 5,
    TAX_AUTHORITIES_AGREE: 6,
    TAX_AUTHORITIES_RETURN: 7
  }

  public static readonly REV_INVOICE_STATUS = {
    IS_REV_INVOICE: 1,
    IS_NOT_REV_INVOICE: 0
  }

  public static readonly INVOICE_STATUS_REQUEST = [
    {label: 'personalTax.declarationRegisters.reject', value: '0', bgColor:'#FDE7EA', color: '#FA0B0B'},
    {label: 'personalTax.declarationRegisters.isProcessing', value: '1', bgColor: '#FFF2DA', color: '#F99600'},
    {label: 'personalTax.declarationRegisters.published', value: '2', bgColor: '#DAF9EC', color: '#06A561'},
  ]

  public static readonly MAX_AGE_REDUCE = 18;

  public static readonly CATALOGS = {
    GIOI_TINH: 'GIOI_TINH',
    DAN_TOC: 'DAN_TOC',
    TON_GIAO: 'TON_GIAO',
    TINH_TRANG_HON_NHAN: 'TINH_TRANG_HON_NHAN',
    TINH: 'TINH',
    HUYEN: 'HUYEN',
    XA: 'XA',
    LOAI_TAI_KHOAN: 'LOAI_TAI_KHOAN',
    LOAI_GIAY_TO: 'LOAI_GIAY_TO',
    MA_VUNG_DIEN_THOAI: 'MA_VUNG_DIEN_THOAI',
    QUOC_GIA: 'QUOC_GIA',
    DOI_TUONG_CV: 'DOI_TUONG_CV',
    CAP_BAC_QUAN_HAM: 'CAP_BAC_QUAN_HAM',
    LEVEL_NV: 'LEVEL_NV',
    LOAI_HOP_DONG: 'LOAI_HOP_DONG',
    LOAI_HINH_DT: 'LOAI_HINH_DT',
    HE_DT: 'HE_DT',
    HINHTHUC_DAOTAO: 'HINHTHUC_DAOTAO',
    XEP_LOAI_DT: 'XEP_LOAI_DT',
    MOI_QUAN_HE_NT: 'MOI_QUAN_HE_NT',
    TINH_TRANG_NT: 'TINH_TRANG_NT',
    DOITUONG_CHINHSACH: 'DOITUONG_CHINHSACH',
    CAP_QD_KHENTHUONG: 'CAP_QD_KHENTHUONG',
    HINHTHUC_KHENTHUONG: 'HINHTHUC_KHENTHUONG',
    LOAI_KHENTHUONG: 'LOAI_KHENTHUONG',
    HINHTHUC_KYLUAT: 'HINHTHUC_KYLUAT',
    LOAI_HO_SO: 'LOAI_HO_SO',
    LOAI_PHU_CAP: 'LOAI_PHU_CAP',
    CAP_QD_KYLUAT: 'CAP_QD_KYLUAT',
    LY_DO_NGHI: 'LY_DO_NGHI',
    NOI_CAP_CCCD: 'NOI_CAP_CCCD',
    NOI_CAP_CMND: "NOI_CAP_CMND",
    TEN_CQT:'TEN_COQUANTHUE'
  }

  public static readonly DEPENDENT_REGISTER_TYPE = [
    {label: 'personalTax.dependentRegisters.registerNewDP', value: 'CREATE'},
    {label: 'personalTax.dependentRegisters.registerLowDP', value: 'CANCEL'}
  ]

  public static readonly STATUSES_EMPLOYEE = [
    {label: 'personalTax.label.workIn', value: '1'},
    {label: 'personalTax.label.contractPending', value: '2'},
    {label: 'personalTax.label.workOut', value: '3'},
  ];
  public static readonly TYPE_CODE = {
    DOI_TUONG_CV: 'DOI_TUONG_CV'
  }

  public static readonly REGISTRATION_TYPE = {
    DECLARATION_REGISTER: 'DECLARATION_REGISTER'
  }

  public static readonly JOB_TYPE = {
    ORG: 'ORG'
  }

  public static readonly EXCEL_TYPE_1 = 'xls';
  public static readonly EXCEL_MIME_1 = 'application/vnd.ms-excel';

  public static readonly EXCEL_TYPE_2 = 'xlsx';
  public static readonly EXCEL_MIME_2 = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';

  public static readonly PDF_TYPE = 'pdf';
  public static readonly PDF_MIME = 'application/pdf';
}
