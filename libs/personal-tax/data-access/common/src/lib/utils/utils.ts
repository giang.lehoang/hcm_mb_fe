import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { TranslateService } from '@ngx-translate/core';
import { HttpParams } from '@angular/common/http';
import { saveAs } from 'file-saver';
import { ToastrService } from 'ngx-toastr';
import { getTypeExport } from '@hcm-mfe/shared/common/utils';
import {DownloadFileAttachService, TaxCommonService} from "@hcm-mfe/personal-tax/data-access/services";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {AppFunction} from "@hcm-mfe/shared/data-access/models";

export function openAddModalUtils(registerId:number | NzSafeAny, modalService: NzModalService, translateService: TranslateService, nzTitle: string, nzContent: NzSafeAny, ratioWidth?: number, isDetail? : boolean, objFunction?: AppFunction) {
  const ratio = ratioWidth ? ratioWidth : 1.5;
  const modal: NzModalRef = modalService.create({
    nzWidth: window.innerWidth > 767 ? window.innerWidth / ratio > 1100 ? 1100 : window.innerWidth / ratio : window.innerWidth,
    nzTitle: translateService.instant(nzTitle),
    nzContent: nzContent,
    nzComponentParams: {
      registerId: registerId,
      isDetail: isDetail,
      objFunction: objFunction
    },
    nzFooter: null
  });
  return modal;
}

export async function doExportFileUtils(url: string, prams: HttpParams, taxCommonService: TaxCommonService, toastService: ToastrService, translateService: TranslateService, fileName: string):Promise<boolean> {
  const res = await taxCommonService.doExportFile(url, prams).toPromise();
  if ((res?.headers?.get('Excel-File-Empty'))) {
    toastService.error(translateService.instant("common.notification.exportFail"));
  } else {
    const reportFile = new Blob([res.body], { type: getTypeExport(fileName.split('.')?.pop() ?? '') });
    saveAs(reportFile, fileName);
  }
  return false;
}

export function doDownloadFileAttach(docId: number, security: string | NzSafeAny, fileName: string, downloadFileAttachService: DownloadFileAttachService, toastService: ToastrService, translateService: TranslateService) {
  downloadFileAttachService.doTaxDownloadAttachFile(docId, security).subscribe(res => {
    if (res) {
      const reportFile = new Blob([res], { type: getTypeExport(fileName.split('.').pop() ?? '') });
      saveAs(reportFile, fileName);
    }
  }, () => {
    toastService.error(translateService.instant('personalTax.notification.downloadFileError'));
  })
}
