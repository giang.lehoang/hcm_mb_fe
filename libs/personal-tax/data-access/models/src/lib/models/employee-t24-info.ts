export class EmployeeT24Info {
  employeeId?: number;
  employeeCode?: string;
  employeeName?: string;
  personalId?: string;
  accountTypeName?: string;
  accountNo?: string;
  t24Code?: string;
  paymentAccount?: string;
  salaryAccount?: string;
}
