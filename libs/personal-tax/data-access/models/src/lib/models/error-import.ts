export class ErrorImport {
  column: number;
  row: number;
  columnLabel: string;
  content: string;
  description: string;
}
