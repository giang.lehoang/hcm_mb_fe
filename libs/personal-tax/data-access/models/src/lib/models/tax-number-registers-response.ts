import { ListFileAttach } from './dependent-registers';

export class TaxNumberRegistersResponse {
    empCode?: string;
    empName?: string;
    orgName?: string;
    empStatus?: number;
    empTypeName?: string;
    jobName?: string;
    taxNo?: string;
    taxPlace?: string;
    idNo?: string;


    taxNumberRegisterId?: number;
    employeeId?: number;
    idTypeCode?: string;
    idDate?: string;
    idPlace?: string;
    idPlaceCode?: string;
    permanentNationCode?: string;
    permanentProvinceCode?: string;
    permanentDistrictCode?: string;
    permanentWardCode?: string;
    permanentDetail?: string;
    currentNationCode?: string;
    currentProvinceCode?: string;
    currentDistrictCode?: string;
    currentWardCode?: string;
    currentDetail?: string;
    mobileNumber?: string;
    email?: string;
    oldIdTypeCode?: string;
    oldIdNo?: string;
    oldIdDate?: string;
    oldIdPlace?: string;
    oldIdPlaceCode?: string;
    createdBy?: string;
    createDate?: string;
    lastUpdatedBy?: string;
    lastUpdateDate?: string;
    flagStatus?: number;
    status?: number;
    regType?: string;
    registerId?: number;
    registerInformation?: string;
    oldRegisterInformation?: string;
    rejectReason?: string;

    attachFileList: Array<ListFileAttach>;
    docIdsDelete: number[];
    note?:string;
}

