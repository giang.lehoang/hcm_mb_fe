export class PersonalIdentity {
    personalIdentityId?: number;
    idTypeCode?: string;
    idTypeName?: string;
    idNo?: string;
    idIssueDate?: string;
    idIssuePlace?: string;
    isMain?: number;
    toDate?: string
}
