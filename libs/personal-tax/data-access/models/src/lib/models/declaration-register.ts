export class DeclarationRegister {
  declarationRegisterId?: number;
  invoiceRequestId?: number;
  year?: number;
  employeeId?: number;
  employeeCode?: string;
  methodCode?: string;
  regType?: string;
  email?: string;
  status?: number;
  note?:string;
  revInvoice?:number;
  revInvoiceName?:string;
  registerInfo?:string;
  idNo?: string;
  taxNo?: string;
  orgId?: number;
}
