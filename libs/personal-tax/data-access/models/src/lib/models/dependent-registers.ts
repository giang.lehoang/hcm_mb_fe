export class DependentRegisters {
  dependentRegisterId?: number;
  employeeCode?: string;
  employeeId?: number;
  empName?: string;
  orgName?: string;
  jobName?: string;
  createDate?: string;
  fromDate?: string | Date | null;
  toDate?: string | Date | null;
  empTypeName?: string;
  status?: number;
  empStatus?: number;
  taxNo?: string;
  dependentName?: string;
  idNo?: string;
  bookNo?: string;
  nationName?: string;
  provinceName?: string;
  provinceCode?: string;
  districtName?: string;
  districtCode?: string;
  wardName?: string;
  wardCode?: string;
  codeNo?: string | null;
  dateOfBirth?: string | Date | null;
  regType?: string;
  regTypeName?: string;
  familyRelationshipId?: number;
  fullName?: string;
  value?: number;
  label?: string;
  relationTypeName?:string;
  rejectReason?:string;
  attachFileList?: Array<ListFileAttach>;
  docIdsDelete?: number[];
  personalIdNumber?: string;
  taxNumber?: string;
}

export class ListFileAttach {
  docId: string;
  fileName: string;
  security?: string;
}
