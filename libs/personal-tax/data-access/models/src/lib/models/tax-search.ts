export class TaxSearch {
  employeeId: number;
  empCode: string;
  empName: string;
  idNo: string;
  taxNo: string;
  taxDate: string;
  taxPlace: string;
  empTypeName: string;
  positionName: string;
  orgName: string;
}

export class Tax {
  pageSize: string;
  empCode: string;
  toDate: string;
  listEmpStatus: string | string[];
  listPositionId: string | string[];
  listEmpTypeCode: string | string[];
  fromDate: string;
  empName: string;
  taxNo: string;
  orgId: string;
  startRecord: string;
  regType: string;
  status: string;
  hasTaxNo: string;
  idNo: string;
  listStatus: string | string[];
}
