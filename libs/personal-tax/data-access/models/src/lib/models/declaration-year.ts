export class DeclarationYear {
  lockRegistrationId?: number;
  registrationType?: string;
  year?: number;
  fromDate?: string;
  toDate?: string;
  value?: number;
  label?: string;
  fromRemindDate?: string;
  toRemindDate?: string;
}
