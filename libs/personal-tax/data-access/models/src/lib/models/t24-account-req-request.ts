export class T24AccountReqRequest {
    t24AccountReqId?: number;
    employeeId?: number;
    employeeCode?: string;
    personalIdNo?: string;
    t24Code?: string;
    customerAccount?: string;
    salaryAccount?: string;
    paymentAccount?: string;
    status?: number;
    statusCreate?: string;
    reasonCreate?: string;
    statusMap?: string;
    reasonMap?: string;
    statusT24?: string;
    reasonT24?: string;
    branchCode?: string;
    type?: number;
    startRecord?: number;
    pageSize?: number;
    orgId?: number;
    listPositionId?: string[];
    empName?: string;
    note?: string;
    listStatus?: string[];
    fromDate?: string | null;
    toDate?: string | null;
    isSendBrowseAction?: number;
}