import { Injectable } from '@angular/core';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import {Constant, UrlConstant} from '@hcm-mfe/personal-tax/data-access/common';
import { MICRO_SERVICE } from '@hcm-mfe/shared/common/constants';

@Injectable({
  providedIn: "root"
})
export class TaxRegistersService extends BaseService {

    public deleteData(id: number, regType: string) {
      let url = '';
      switch (regType) {
        case Constant.REG_TYPE.REGISTER_NEW:
        case Constant.REG_TYPE.ALTER_TAX_INFO:
          url = UrlConstant.API_VERSION + UrlConstant.TAX_REGISTERS.DELETE_DATA;
          break;
        case Constant.REG_TYPE.REGISTER_NEW_DP:
        case Constant.REG_TYPE.REGISTER_LOW_DP:
          url = UrlConstant.API_VERSION + UrlConstant.DEPENDENT_REGISTERS.DELETE_DATA;
          break;
      }
      url = url.replace('{id}', id.toString());
      return this.delete(url, {}, MICRO_SERVICE.TAX_SERVICE);
    }

    public saveRecord(request: FormData) {
        const url = UrlConstant.API_VERSION + UrlConstant.TAX_REGISTERS.SAVE;
        return this.post(url, request, {}, MICRO_SERVICE.TAX_SERVICE);
    }

    public adminGetRecentRegister(employeeId: number) {
        const url = UrlConstant.API_VERSION + UrlConstant.TAX_REGISTERS.GET_RECENT_REGISTER.replace("{employeeId}", employeeId?.toString());
        return this.get(url, {}, MICRO_SERVICE.TAX_SERVICE);
    }

}
