import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { MICRO_SERVICE } from '@hcm-mfe/shared/common/constants';
import { UrlConstant } from '@hcm-mfe/personal-tax/data-access/common';
import {FormGroup} from "@angular/forms";
import {HttpParams} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class DependentService extends BaseService{

  public getListRelatives(employeeId: any, param: any): Observable<any> {
    const url = UrlConstant.API_VERSION + UrlConstant.RELATIVES_INFO.LIST.replace('{employeeId}', employeeId)
    return this.get(url, {params: param});
  }

  public getCatalog(typeCode: string, parentCode?: string){
    const url = UrlConstant.API_VERSION + UrlConstant.CATALOGS.PREFIX;
    return this.get(url, {params: parentCode ? {typeCode, parentCode} : {typeCode}});
  }

  public saveDependentRegister(data: FormData) {
    const url = UrlConstant.API_VERSION + UrlConstant.DEPENDENT_REGISTERS.SAVE_DEPENDENT;
    return this.post(url, data,{}, MICRO_SERVICE.TAX_SERVICE);
  }

  public getListDependents(employeeId: any, param: any): Observable<any> {
    const url = UrlConstant.API_VERSION + UrlConstant.DEPENDENT_INFO.LIST.replace('{employeeId}', employeeId)
    return this.get(url, {params: param});
  }

  sendMailConfirmDependent(searchParam: HttpParams) {
    const url = UrlConstant.API_VERSION + UrlConstant.DEPENDENT_INFO.SEND_EMAIL;
    return this.get(url, {params: searchParam}, MICRO_SERVICE.TAX_SERVICE);
  }

}
