import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from '@hcm-mfe/personal-tax/data-access/common';
import { MICRO_SERVICE } from '@hcm-mfe/shared/common/constants';
import { BaseResponse } from '@hcm-mfe/shared/data-access/models';


@Injectable({
  providedIn: 'root'
})
export class SearchFormService extends BaseService{

  public getFilterResearch(urlEndpoint: string, searchParam: HttpParams, pagination: { startRecord: number, pageSize: number }): Observable<BaseResponse> {
    if (pagination) searchParam = searchParam.appendAll(pagination);
    const url = UrlConstant.API_VERSION + urlEndpoint;
    return this.get(url, {params: searchParam}, MICRO_SERVICE.TAX_SERVICE);
  }

  public getListValue(urlEndpoint: string, params: HttpParams) {
    const url = UrlConstant.API_VERSION + urlEndpoint;
    return this.get(url, {params: params});
  }
}
