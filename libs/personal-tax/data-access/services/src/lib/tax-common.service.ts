import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { HttpParams } from '@angular/common/http';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import {Constant, UrlConstant} from '@hcm-mfe/personal-tax/data-access/common';
import { MICRO_SERVICE } from '@hcm-mfe/shared/common/constants';
import { environment } from '@hcm-mfe/shared/environment';

@Injectable({
  providedIn: 'root'
})
export class TaxCommonService extends BaseService{

  public approveById(id:number, urlEndpoint:string) {
    const data = {id}
    const url = UrlConstant.API_VERSION + urlEndpoint;
    return this.post(url, data, {}, MICRO_SERVICE.TAX_SERVICE);
  }

  public approveByList(listId:number[], urlEndpoint:string) {
    let params = new HttpParams();
    if (listId.length > 0) {
      params = params.set('listId', listId.join(','));
    }
    const url = UrlConstant.API_VERSION + urlEndpoint;
    return this.post(url,null, {params: params}, MICRO_SERVICE.TAX_SERVICE);
  }

  public rejectById(id:number, urlEndpoint:string, rejectReason:string) {
    const listId: number[] = [id];
    const data = {listId, rejectReason}
    const url = UrlConstant.API_VERSION + urlEndpoint;
    return this.post(url,data,{}, MICRO_SERVICE.TAX_SERVICE);
  }
  public rejectByList(listId:number[], urlEndpoint:string, rejectReason?:string) {
    const data = {listId, rejectReason}
    const url = UrlConstant.API_VERSION + urlEndpoint;
    return this.post(url,data, {}, MICRO_SERVICE.TAX_SERVICE);
  }

  public approveAll(urlEndpoint:string, form: FormGroup) {
    const data = form.value;
    const url = UrlConstant.API_VERSION + urlEndpoint;
    return this.post(url, data, {}, MICRO_SERVICE.TAX_SERVICE);
  }

  public deleteById(id:number, urlEndpoint:string){
    const url = UrlConstant.API_VERSION + urlEndpoint.replace('{id}',id.toString());
    return this.delete(url, {}, MICRO_SERVICE.TAX_SERVICE);
  }

  getTaxRegisterById(id: number, regType: string) {
    let url: string = '';
    switch (regType) {
      case  Constant.REG_TYPE.ALTER_TAX_INFO:
      case  Constant.REG_TYPE.REGISTER_NEW:
        url = UrlConstant.API_VERSION + UrlConstant.TAX_REGISTERS.GET_BY_ID.replace('{id}', id.toString());
        break;
      case  Constant.REG_TYPE.REGISTER_NEW_DP:
      case  Constant.REG_TYPE.REGISTER_LOW_DP:
        url = UrlConstant.API_VERSION + UrlConstant.DEPENDENT_REGISTERS.GET_BY_ID.replace('{id}', id.toString());
        break;
      case Constant.REG_TYPE.REGISTER_TAX_SETTLEMENT:
      case Constant.REG_TYPE.REGISTER_RECEIVE_RECEIPT:
        url = UrlConstant.API_VERSION + UrlConstant.DECLARATION_REGISTERS.GET_BY_ID.replace('{id}', id.toString());
        break;
      case Constant.REG_TYPE.CONFIRM_AUTHORIZED:
        url = UrlConstant.API_VERSION + UrlConstant.CONFIRM_REGISTERS.GET_BY_ID.replace('{id}', id.toString());
        break;
    }
    return this.get(url, {}, MICRO_SERVICE.TAX_SERVICE);
  }

  doExportFile(urlEndpoint: string, searchParam: HttpParams) {
    const url = UrlConstant.API_VERSION + urlEndpoint;
    return this.getRequestFile(url, {params: searchParam}, environment.backend.taxServiceBackend + url);
  }

}
