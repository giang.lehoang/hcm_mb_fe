import {Injectable} from '@angular/core';
import {HttpParams} from '@angular/common/http';
import {BaseService} from '@hcm-mfe/shared/common/base-service';
import {UrlConstant} from '@hcm-mfe/personal-tax/data-access/common';
import {MICRO_SERVICE} from '@hcm-mfe/shared/common/constants';
import {T24AccountReqRequest} from '@hcm-mfe/personal-tax/data-access/models';

@Injectable({
    providedIn: 'root'
})
export class T24AccountReqService extends BaseService {

    createRequestByList(data: HttpParams) {
        const url = UrlConstant.API_VERSION + UrlConstant.CREATE_REQUEST.T24_ACCOUNT_REQUESTS;
        return this.post(url, null, {params: data}, MICRO_SERVICE.TAX_SERVICE);
    }

    createRequestAll(data: T24AccountReqRequest) {
        const url = UrlConstant.API_VERSION + UrlConstant.CREATE_REQUEST_ALL.T24_ACCOUNT_REQUESTS;
        return this.post(url, data, {}, MICRO_SERVICE.TAX_SERVICE);
    }

    deleteData(id: number, urlEndpoint: string) {
        const url = UrlConstant.API_VERSION + urlEndpoint.replace('{id}', id.toString());
        return this.delete(url, {}, MICRO_SERVICE.TAX_SERVICE);
    }

    saveRecord(request: FormData) {
        const url = UrlConstant.API_VERSION + UrlConstant.T24_ACCOUNT_REQUEST.SAVE;
        return this.post(url, request, {}, MICRO_SERVICE.TAX_SERVICE);
    }

    getById(id: number) {
        const url = UrlConstant.API_VERSION + UrlConstant.T24_ACCOUNT_REQUEST.GET_BY_ID.replace('{id}', id.toString());
        return this.get(url, {}, MICRO_SERVICE.TAX_SERVICE);
    }

    doExportFile(searchData: HttpParams) {
        const url = UrlConstant.API_VERSION +  UrlConstant.T24_ACCOUNT_REQUEST.EXPORT;
        return this.getRequestFileD2T(url, { params: searchData }, MICRO_SERVICE.TAX_SERVICE)
    }

    getT24InfoEmployeeHcm(employeeId: number) {
      const url = UrlConstant.API_VERSION + UrlConstant.T24_ACCOUNT_REQUEST.GET_T24_INFO_EMPLOYEE_HCM.replace('{employeeId}', employeeId.toString());
      return this.get(url, {});
    }

    getT24InfoEmployeeTax(employeeId: number) {
      const url = UrlConstant.API_VERSION + UrlConstant.T24_ACCOUNT_REQUEST.GET_T24_INFO_EMPLOYEE_TAX.replace('{employeeId}', employeeId.toString());
      return this.get(url, {}, MICRO_SERVICE.TAX_SERVICE);
    }

}
