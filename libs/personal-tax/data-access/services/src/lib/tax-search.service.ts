import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";
import {UrlConstant} from "@hcm-mfe/personal-tax/data-access/common";

@Injectable({
  providedIn: 'root'
})
export class TaxSearchService extends BaseService {

  readonly baseUrl = UrlConstant.API_VERSION + UrlConstant.TAX_SEARCH.PREFIX;
  // lấy danh sách thông tin mã số thuế
  public getTaxNo(searchData: HttpParams) {
    const url = this.baseUrl +  UrlConstant.TAX_SEARCH.SEARCH;
    return this.get(url, { params: searchData }, MICRO_SERVICE.TAX_SERVICE)
  }

  doExportFile(searchData: HttpParams) {
    const url = this.baseUrl +  UrlConstant.TAX_SEARCH.EXPORT;
    return this.getRequestFileD2T(url, { params: searchData }, MICRO_SERVICE.TAX_SERVICE)
  }

  public getEmpTypeList() {
    return this.get(UrlConstant.API_VERSION + UrlConstant.TAX_SEARCH.LOOKUP_EMP_TYPE)
  }
}
