export class UrlConstant {
  public static readonly API_VERSION = '/v1.0/admin';

  public static readonly TAX_SEARCH = {
    PREFIX: '/tax',
    SEARCH: '/search-tax-no',
    EXPORT: '/export-employee',
    LOOKUP_EMP_TYPE: '/lookup-values?typeCode=DOI_TUONG_CV',
  };
}
