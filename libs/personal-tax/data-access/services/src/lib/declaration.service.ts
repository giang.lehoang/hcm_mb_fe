import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { DeclarationRegister } from '@hcm-mfe/personal-tax/data-access/models';
import { MICRO_SERVICE } from '@hcm-mfe/shared/common/constants';
import { UrlConstant } from '@hcm-mfe/personal-tax/data-access/common';

@Injectable({
  providedIn: 'root'
})
export class DeclarationService extends BaseService{
  saveData(data: DeclarationRegister) {
    const url = UrlConstant.API_VERSION + UrlConstant.DECLARATION_REGISTERS.SAVE;
    return this.post(url, data, {}, MICRO_SERVICE.TAX_SERVICE);
  }

  getById(id: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.DECLARATION_REGISTERS.GET_BY_ID.replace('{id}', id.toString());
    return this.get(url,{}, MICRO_SERVICE.TAX_SERVICE);
  }

  getEmployeeInfo(empId: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.EMPLOYEES.PREFIX + '/' + empId + UrlConstant.EMPLOYEES.PERSONAL_INFO;
    return this.get(url);
  }

  public getListYear(registrationType: string, year?: number) {
    let params = new HttpParams().set('registrationType', registrationType);
    if (year) {
      params = params.set('year', year);
    }
    const url = UrlConstant.API_VERSION + UrlConstant.DECLARATION_REGISTERS.GET_LIST_YEAR;
    return this.get(url, {params: params}, MICRO_SERVICE.TAX_SERVICE);
  }

  public saveLockRegister(data: FormData) {
    const url = UrlConstant.API_VERSION + UrlConstant.LOCK_DATE_REGISTERS.SAVE;
    return this.post(url, data,{}, MICRO_SERVICE.TAX_SERVICE);
  }

  public getDataByProperties(params: HttpParams) {
    const url = UrlConstant.API_VERSION + UrlConstant.DECLARATION_REGISTERS.CHECK;
    return this.get(url, {params: params}, MICRO_SERVICE.TAX_SERVICE);
  }

  public getListOrgManage(scope: string, functionCode: string, orgLevel: number) {
    let params = new HttpParams().set('scope', scope).set('functionCode', functionCode).set('orgLevel', orgLevel);
    const url = UrlConstant.API_VERSION + UrlConstant.CATEGORY.ORG_MANAGE;
    return this.get(url, {params: params});
  }
}
