import {Injectable} from '@angular/core';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { DeclarationRegister } from '@hcm-mfe/personal-tax/data-access/models';
import { UrlConstant } from '@hcm-mfe/personal-tax/data-access/common';
import { MICRO_SERVICE } from '@hcm-mfe/shared/common/constants';
import {HttpParams} from "@angular/common/http";

@Injectable({
    providedIn: 'root'
})
export class InvoiceRequestService extends BaseService {

    saveData(data: DeclarationRegister) {
        const url = UrlConstant.API_VERSION + UrlConstant.INVOICE_REQUEST.SAVE;
        return this.post(url, data, {}, MICRO_SERVICE.TAX_SERVICE);
    }


    public getById(id: number) {
        const url = UrlConstant.API_VERSION + UrlConstant.INVOICE_REQUEST.GET_BY_ID.replace('{id}', id.toString());
        return this.get(url,{}, MICRO_SERVICE.TAX_SERVICE);
    }

    public requestExportInvoiceByList(listId: number[]) {
      let params = new HttpParams();
      if (listId.length > 0) {
        params = params.set('listId', listId.join(','));
      }
      const url = UrlConstant.API_VERSION + UrlConstant.INVOICE_REQUEST.REQUEST_BY_LIST;
      return this.post(url,null, {params: params}, MICRO_SERVICE.TAX_SERVICE);
    }

    public requestExportInvoiceByForm(searchParam: HttpParams) {
      const url = UrlConstant.API_VERSION + UrlConstant.INVOICE_REQUEST.REQUEST_BY_FORM;
      return this.post(url,null, {params: searchParam}, MICRO_SERVICE.TAX_SERVICE);
    }


}
