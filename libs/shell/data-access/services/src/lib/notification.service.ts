import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { MICRO_SERVICE, url } from '@hcm-mfe/shared/common/constants';
import { BaseService } from '@hcm-mfe/shared/common/base-service';

@Injectable({
  providedIn: 'root',
})
export class NotificationService extends BaseService {
  countNotification$ = new BehaviorSubject<number>(0);
  countNotify$ = this.countNotification$.asObservable();

  public findAll(paramSearch: any): Observable<any> {
    const urlUtilities = url.url_endpoint_get_notify;
    return this.get(urlUtilities, {params: paramSearch}, MICRO_SERVICE.DEFAULT);
  }

  public getCount(): Observable<any> {
    const urlUtilities = url.url_endpoint_get_count_notify;
    return this.get(urlUtilities, undefined, MICRO_SERVICE.DEFAULT);
  }

  public markAsRead(id: number): Observable<any> {
    const urlUtilities = `${url.url_endpoint_read_notify}/${id}`;
    return this.get(urlUtilities, undefined , MICRO_SERVICE.DEFAULT);
  }

  public updateTodo(id:number, data: any): Observable<any> {
    const urlUtilities = `${url.url_endpoint_update_todo}/${id}`;
    return this.put(urlUtilities, data, MICRO_SERVICE.DEFAULT);
  }
  public getUnreadNoti(): Observable<any> {
    const urlUtilities = `${url.url_endpoint_unread_notify}`;
    return this.get(urlUtilities, undefined , MICRO_SERVICE.DEFAULT);
  }
  public updateUnreadNoti(): Observable<any> {
    const urlUtilities = `${url.url_endpoint_update_unread_notify}`;
    return this.put(urlUtilities, undefined , MICRO_SERVICE.DEFAULT);
  }
  public getImageThank(id: number): Observable<any> {
    const urlUtilities = `${url.url_endpoint_image_unread_notify}/${id}`;
    return this.get(urlUtilities, undefined , MICRO_SERVICE.DEFAULT);
  }
  changecountNotify(count: number) {
    this.countNotification$.next(count);
  }
}
