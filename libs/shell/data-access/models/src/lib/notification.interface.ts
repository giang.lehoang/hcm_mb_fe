export interface Notification {
  notiId: number;
  subject: string;
  shortContent: string;
  notiLink: string;
  createDate: string;
  initializedDate: string;
  isViewed: string;
  todoStatus: string;
}

export interface NotificationSearch {
  createDate?: string;
  isViewed?: string;
  page?: number;
  size?: number;
}