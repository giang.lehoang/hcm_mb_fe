import {Component, EventEmitter, Inject, Input, LOCALE_ID, OnInit, Output} from '@angular/core';
import {HeaderEventEmit} from './header-event-emit';
import {KeycloakService} from 'keycloak-angular';
import {TranslateService} from '@ngx-translate/core';
import {NzI18nService} from 'ng-zorro-antd/i18n';
import {enUS, vi} from 'date-fns/locale';
import {BsLocaleService} from "ngx-bootstrap/datepicker";
import { NzModalService } from 'ng-zorro-antd/modal';
import { NzModalRef } from 'ng-zorro-antd/modal/modal-ref';
import { Subscription } from 'rxjs';
import { PersonalInformation, User } from '@hcm-mfe/shared/data-access/models';
import { NotificationService } from '@hcm-mfe/shell/data-access/services';
import { SessionKey } from '@hcm-mfe/shared/common/enums';
import { en_US_ext, vi_VN_ext } from '@hcm-mfe/shared/common/constants';
import { environment } from '@hcm-mfe/shared/environment';
import { NotificationComponent } from '@hcm-mfe/shell/feature/notification';
import { UserService } from '@hcm-mfe/shared/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() isCollapsed: any;
  @Input() isMobile: any;
  @Input() searchResult: PersonalInformation[];
  @Output() headerEventEmit: EventEmitter<HeaderEventEmit> = new EventEmitter<HeaderEventEmit>();
  searchValue = null;
  showSearch = false;
  currentUser: User;
  langCurrent = 'vn';
  countNotifynotView = '';
  modalRef: NzModalRef;
  subs: Subscription[] = [];
  constructor(@Inject(LOCALE_ID) protected localeId: string,
              private readonly keycloakService: KeycloakService,
              private readonly i18n: NzI18nService,
              private readonly localService: BsLocaleService,
              private readonly modalService: NzModalService,
              private readonly translate: TranslateService,
              private readonly notificationService: NotificationService,
              private readonly userService: UserService,
              private readonly router: Router,
              ) {
  }

  ngOnInit(): void {
    if(localStorage.getItem(SessionKey.CURRENCY_USER)) {
      this.currentUser = JSON.parse(localStorage.getItem(SessionKey.CURRENCY_USER));
    } else {
      this.userService.getCurrentUser().toPromise().then(res => {
        if(res && res.data) {
          localStorage.setItem(SessionKey.CURRENCY_USER, JSON.stringify(res.data));
          this.currentUser = res.data;
        }
      });
    }
    const sub = this.notificationService.countNotify$.subscribe(count => {
      this.countNotifynotView = count > 99 ? '99+' : count.toString();
    });
    this.subs.push(sub);
  }

  clickEvent() {
    this.headerEventEmit.emit(new HeaderEventEmit('TRIGGER', 'CLICK'));
  }

  showInputSearch(event: 'OPEN' | 'CLOSE') {
    if (event === 'OPEN') {
      this.showSearch = true;
    } else {
      this.showSearch = false;
      this.searchValue = null;
    }
  }

  changeLanguage() {
    if (this.langCurrent === 'vn') {
      this.i18n.setLocale(en_US_ext);
      this.i18n.setDateLocale(enUS);
      this.translate.use('en');
      this.langCurrent = 'en';
      this.localService.use('en');
    } else {
      this.i18n.setLocale(vi_VN_ext);
      this.i18n.setDateLocale(vi);
      this.translate.use('vn');
      this.langCurrent = 'vn';
      this.localService.use('vn');
    }
  }

  logout() {
    this.userService.logout();
  }

  showModalUpdate() {
    this.modalRef = this.modalService.create({
      nzCloseIcon: null,
      nzWidth: 440,
      nzClassName:'notification-modal',
      nzWrapClassName: 'notification-container',
      nzMaskStyle: { background: 'transparent'},
      nzContent: NotificationComponent,
      nzFooter: null,
      nzComponentParams: {
      }
    });
  }

  openPersonalInfo() {
    this.router.navigateByUrl(environment.url.urlPersonalInfo);
  }

  openUserProfile() {
    this.router.navigateByUrl(environment.url.urlUserProfile);
  }
  changePassword() {
    const theTop=((screen.height/2)-(300))/2;
    const theLeft=(screen.width/2)-(255);
    const features = 'height=600,width=550,top='+theTop+',left='+theLeft+',toolbar=1,Location=0,Directories=0,Status=0,menubar=1,Scrollbars=1,Resizable=1';
    window.open(environment.url.urlChangePassword,'_blank', features);
  }

  ngOnDestroy() {
    if(this.modalRef) {
      this.modalRef.destroy();
    }
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
