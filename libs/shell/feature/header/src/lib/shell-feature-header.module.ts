import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShellFeatureNotificationModule } from '@hcm-mfe/shell/feature/notification';
import { HeaderComponent } from './header/header.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {NzLayoutModule} from "ng-zorro-antd/layout";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzInputModule} from "ng-zorro-antd/input";
import {FormsModule} from "@angular/forms";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  imports: [CommonModule, ShellFeatureNotificationModule, BrowserAnimationsModule, NzLayoutModule, NzIconModule, NzInputModule, FormsModule, NzButtonModule, NzToolTipModule, NzDropDownModule, TranslateModule],
  declarations: [HeaderComponent],
  exports: [HeaderComponent],
})
export class ShellFeatureHeaderModule {}
