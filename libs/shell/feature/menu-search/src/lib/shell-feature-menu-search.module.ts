import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuSearchComponent } from './menu-search/menu-search.component';
import {NzInputModule} from "ng-zorro-antd/input";
import {SharedDirectivesSpecialInputModule} from "@hcm-mfe/shared/directives/special-input";
import {FormsModule} from "@angular/forms";
import {NzIconModule} from "ng-zorro-antd/icon";

@NgModule({
  imports: [CommonModule, NzInputModule, SharedDirectivesSpecialInputModule, FormsModule, NzIconModule],
  declarations:[MenuSearchComponent],
  exports:[MenuSearchComponent],
})
export class ShellFeatureMenuSearchModule {}
