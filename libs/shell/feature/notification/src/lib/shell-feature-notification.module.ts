import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationComponent } from './notification/notification.component';
import { ListNotifyComponent } from './notification/app-list-notify/app-list-notify.component';
import { NotificationService } from '@hcm-mfe/shell/data-access/services';
import {ScrollingModule} from "@angular/cdk/scrolling";
import {NzListModule} from "ng-zorro-antd/list";
import {NzFormModule} from "ng-zorro-antd/form";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
import {ReactiveFormsModule} from "@angular/forms";
import {TranslateModule} from "@ngx-translate/core";
import {NzDatePickerModule} from "ng-zorro-antd/date-picker";
import {NzTabsModule} from "ng-zorro-antd/tabs";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzButtonModule} from "ng-zorro-antd/button";

@NgModule({
  imports: [CommonModule, ScrollingModule, NzListModule, NzFormModule, NzIconModule, NzToolTipModule, ReactiveFormsModule, TranslateModule, NzDatePickerModule, NzTabsModule, NzTableModule, NzButtonModule],
  declarations: [NotificationComponent, ListNotifyComponent],
  exports: [NotificationComponent],
  providers: [NotificationService]
})
export class ShellFeatureNotificationModule {}
