import { noop, Subscription } from 'rxjs';
import { Component, DoCheck, HostBinding, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NzDatePickerComponent } from 'ng-zorro-antd/date-picker';
import { ControlValueAccessor, FormBuilder } from '@angular/forms';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { Notification, NotificationSearch } from '@hcm-mfe/shell/data-access/models';
import { NotificationService } from '@hcm-mfe/shell/data-access/services';
import { SessionService } from "@hcm-mfe/shared/common/store";
import { CommonUtils, WebSocketService } from '@hcm-mfe/shared/core';
import { getInitializedDate, StringUtils } from '@hcm-mfe/shared/common/utils';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class NotificationComponent implements OnInit, ControlValueAccessor, DoCheck {
  @HostBinding('class') elementClass = 'app-notification';
  onChange: (_: any) => void = noop;
  onTouched: () => void = noop;
  searchDateAll = '';
  searchDateNotView = '';
  subs: Subscription[] = [];
  isLoading = false;
  listDataAll: Array<Notification> = [];
  listDataNotView: Array<Notification> = [];
  listDataAllTemp: Array<Notification> = [];
  listDataNotViewTemp: Array<Notification> = [];
  listDataNotViewCount = '0';
  isAll = true;
  isSearch = false;
  isShowFullAll = false;
  isShowFullNotView = false;
  isShowFooter = false;
  isShowFullBody = false;
  readonly limit = 15;
  readonly dateFomat = 'DD/MM/yyyy';
  paramSearch: NotificationSearch = {
    page: 0,
    size: this.limit,
  }
  paramSearchNotView: NotificationSearch = {
    page: 0,
    size: this.limit,
    isViewed: 'N'
  }

  paramSearchFullAll: NotificationSearch = {
    size: this.limit
  };
  paramSearchNotViewFullAll: NotificationSearch = {
    isViewed: 'N',
    size: this.limit
  };

  @ViewChild('notifyDatePicker') notifyDatePicker: NzDatePickerComponent;
  @ViewChild('notifyDatePickerNotView') notifyDatePickerNotView: NzDatePickerComponent;

  form = this.fb.group({
    searchDateAll: '',
    searchDateNotView: '',
  });

  constructor(
    public fb: FormBuilder,
    private readonly notificationService: NotificationService,
    public readonly translate: TranslateService,
    private readonly webSocketService: WebSocketService,
    private readonly route: Router,
    private readonly session: SessionService,
  ) {
  }

  writeValue(obj: any) {
    this.searchDateAll = obj;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  onInputChange($event: any) {
    this.onChange($event);
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.findAll();
    const subChangeDateAll = this.form.get('searchDateAll').valueChanges.subscribe((value) => {
      if (!this.form.controls.searchDateAll.value) {
        // xử lý nếu date change là null
        this.paramSearchFullAll = {};
        this.paramSearchFullAll = this.paramSearch;
        this.getNotify(this.paramSearch, true);
      } else if (this.isSearch !== true && this.searchDateAll !== this.form.controls.searchDateAll.value) {
        this.form.controls.searchDateAll.setValue(this.searchDateAll);
      }
    });
    this.subs.push(subChangeDateAll);
    const subChangeDateNotView = this.form.get('searchDateNotView').valueChanges.subscribe((value) => {
      if (!this.form.controls.searchDateNotView.value) {
        // xử lý nếu date change là null
        this.paramSearchNotViewFullAll = {};
        this.paramSearchNotViewFullAll = this.paramSearchNotView;
        this.getNotify(this.paramSearchNotView, false);
      } else if (this.isSearch !== true && this.searchDateNotView !== this.form.controls.searchDateNotView.value) {
        this.form.controls.searchDateNotView.setValue(this.searchDateNotView);
      }
    });
    this.subs.push(subChangeDateNotView);
    const subCountNotify = this.notificationService.countNotify$.subscribe((count: number) => {
      this.listDataNotViewCount = count > 99 ? '99 +' : count.toString();
    });
    this.subs.push(subCountNotify);
    const subSocketService = this.webSocketService.message$.subscribe((item: Notification) => {
      item.initializedDate = this.translate.instant('notify.label.now');
      if(CommonUtils.isNullOrEmpty(this.searchDateAll) || this.convertDateToString(this.searchDateAll) === this.convertDateToString(new Date().toString())) {
        this.listDataAll = [];
        if(this.listDataAllTemp.length === this.limit) {
          this.listDataAllTemp.pop();
        }
        this.listDataAllTemp.unshift(item);
        this.listDataAll = this.listDataAllTemp.slice(0,5);
      }
      if(CommonUtils.isNullOrEmpty(this.searchDateNotView) || this.convertDateToString(this.searchDateNotView) === this.convertDateToString(new Date().toString())) {
        this.listDataNotView = [];
        if(this.listDataNotViewTemp.length === this.limit) {
          this.listDataNotViewTemp.pop();
        }
        this.listDataNotViewTemp.unshift(item);
        this.listDataNotView = this.listDataNotViewTemp.slice(0,5);
      }
    });
    this.subs.push(subSocketService);
  }

  ngDoCheck(): void {
    this.isShowFullBody = (this.isAll && this.isShowFullAll) || (!this.isAll && this.isShowFullNotView);
    this.isShowFooter = (this.isAll && (this.isShowFullAll || this.listDataAllTemp.length < 5))
      || (!this.isAll && (this.isShowFullNotView || this.listDataNotViewTemp.length < 5));
  }

  trackByFn(index: number, item: Notification) {
    return item.notiId;
  }

  findAll() {
    this.getNotify(this.paramSearch, true);
    this.getNotify(this.paramSearchNotView, false);
  }

  getNotify(params: NotificationSearch, isAll: boolean) {
    const sub = this.notificationService.findAll(params).subscribe((res) => {
      const now = new Date();
      res.data.content.forEach((item: Notification)  => {
        item.initializedDate = getInitializedDate(item.createDate, now, this.translate);
      });
      if(isAll) {
        this.listDataAllTemp = res.data.content;
        this.listDataAll = res.data.content.slice(0,5);
      } else {
        this.listDataNotViewTemp = res.data.content;
        this.listDataNotView = res.data.content.slice(0,5);
        this.listDataNotViewCount = res.data.totalElements > 99 ? '99 +' : res.data.totalElements;
      }
      this.isLoading = false;
    },
    () => {
      this.isLoading = false;
    });
    this.subs.push(sub);
  }

  changeTab(isAll: boolean) {
    if (isAll) {
      this.isAll = true;
      const paramSearch: NotificationSearch = {...this.paramSearch, createDate: this.convertDateToString(this.form.controls.searchDateAll.value)}
      this.getNotify(paramSearch, true);
    } else {
      this.isAll = false;
      const paramSearchNotView: NotificationSearch = {...this.paramSearchNotView, createDate: this.convertDateToString(this.form.controls.searchDateNotView.value)}
      this.getNotify(paramSearchNotView, false);
    }
  }

  convertDateToString(date: string): string {
    if(date && date !== '') {
      return moment(date).format(this.dateFomat);
    }
    return '';
  }

  close() {
    this.notifyDatePicker.checkAndClose();
    this.notifyDatePickerNotView.checkAndClose();
  }

  // search thông báo theo ngày
  search() {
    this.isSearch = true;
    this.isLoading = true;
    if (this.isAll) {
      this.notifyDatePicker.checkAndClose();
      this.searchDateAll = this.form.controls.searchDateAll.value;
      if(!this.isShowFullAll) {
        this.listDataAllTemp = [];
        this.listDataAll = [];
        const paramSearch: NotificationSearch = {...this.paramSearch, createDate: this.convertDateToString(this.form.controls.searchDateAll.value)};
        this.getNotify(paramSearch, true);
      }
      this.paramSearchFullAll = {};
      this.paramSearchFullAll = {...this.paramSearch, createDate: this.convertDateToString(this.form.controls.searchDateAll.value)};
      this.isSearch = false;
    } else {
      this.notifyDatePickerNotView.checkAndClose();
      this.searchDateNotView = this.form.controls.searchDateNotView.value;
      if(!this.isShowFullNotView) {
        this.listDataNotViewTemp = [];
        this.listDataNotView = [];
        const paramSearchNotView: NotificationSearch = {...this.paramSearchNotView, createDate: this.convertDateToString(this.form.controls.searchDateNotView.value)};
        this.getNotify(paramSearchNotView, false);
      }
      this.paramSearchNotViewFullAll = {};
      this.paramSearchNotViewFullAll = {...this.paramSearchNotView, createDate: this.convertDateToString(this.form.controls.searchDateNotView.value)};
      this.isSearch = false;
    }
    this.isLoading = false;
  }

  // chuyển trạng thái từ chưa đọc sang đã đọc
  changeStatus(data: Notification) {
    if (data.isViewed === 'N') {
      const read = this.notificationService.markAsRead(data.notiId).subscribe((res) => {
        data.isViewed = 'Y';
        const count = this.session.getSessionData('countNotify')
        const changeCount = count - 1;
        this.session.setSessionData('countNotify', changeCount);
        this.notificationService.changecountNotify(changeCount);
      });
      this.subs.push(read);
    }
    if (!StringUtils.isNullOrEmpty(data.notiLink)) {
      this.route.navigateByUrl(data.notiLink);
    }
  }

  // gắn cờ cho thông báo
  setTodoStatus(data: Notification) {
    const dataTodo: Pick<Notification, 'todoStatus' | 'isViewed'> = {
      todoStatus: data.todoStatus === 'N' ? 'Y' : 'N',
      isViewed: data.isViewed,
    };
    const changeTodo = this.notificationService.updateTodo(data.notiId, dataTodo).subscribe((res) => {
      if (data.todoStatus === 'N') {
        data.todoStatus = 'Y';
      } else {
        data.todoStatus = 'N';
      }
    });
    this.subs.push(changeTodo);
  }

  // show tất cả thông báo
  showFullNotify() {
    if (this.isAll) {
      this.isShowFullAll = true;
    } else {
      this.isShowFullNotView = true
    }
  }

  ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
