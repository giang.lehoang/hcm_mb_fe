import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Notification, NotificationSearch } from '@hcm-mfe/shell/data-access/models';
import { NotificationService } from '@hcm-mfe/shell/data-access/services';
import { SessionService } from "@hcm-mfe/shared/common/store";
import { getInitializedDate, StringUtils } from '@hcm-mfe/shared/common/utils';
import { WebSocketService } from '@hcm-mfe/shared/core';


@Component({
  selector: 'app-list-notify',
  templateUrl: './app-list-notify.component.html',
  styleUrls: ['./app-list-notify.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListNotifyComponent implements OnInit, OnDestroy, OnChanges {
  @Input() params: NotificationSearch;
  @Input() data: Array<Notification>;
  @Input() date: string;
  @Input() isAll: boolean;

  @ViewChild(CdkVirtualScrollViewport) virtualScroll: CdkVirtualScrollViewport;

  ds = new Notify(this.notificationService, this.translate);

  private readonly destroy$ = new Subject();
  constructor(
    private readonly notificationService: NotificationService,
    private readonly cd: ChangeDetectorRef,
    private readonly translate: TranslateService,
    private readonly webSocketService: WebSocketService,
    private readonly route: Router,
    private readonly session: SessionService
  ) {}

  ngOnInit(): void {
    const dataNotify = [...this.data];
    this.ds.params = this.params;
    this.ds.cachedData = dataNotify;
    this.ds.fetchedPages.add(0);
    this.ds
      .completed()
      .pipe(takeUntil(this.destroy$))
      .subscribe();
    this.webSocketService.message$.subscribe((item) => {
      this.ds.cachedData.unshift(item);
      this.ds.dataStream.next(this.ds.cachedData);
      this.ds.completed().subscribe();
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(this.virtualScroll) {
      this.virtualScroll.scrollToIndex(1);
      this.virtualScroll.scrollToIndex(0);
    }
    if((changes.params && !changes?.params.firstChange) || (changes.params && this.date == null)) {
      this.ds = new Notify(this.notificationService, this.translate);
      this.params.createDate = moment(this.date).format('DD/MM/yyyy')
      this.ds.params = this.params;
      if(this.date == null) {
        delete this.ds.params.createDate;
      }
      this.ds
        .completed()
        .pipe(takeUntil(this.destroy$))
        .subscribe();
    }
  }

  // chuyển trạng thái từ chưa đọc sang đã đọc
  changeStatus(data: Notification) {
    if (data.isViewed === 'N') {
      this.notificationService.markAsRead(data.notiId).subscribe((res) => {
        data.isViewed = 'Y';
        const count = this.session.getSessionData('countNotify')
        const changeCount = count - 1;
        this.session.setSessionData('countNotify', changeCount);
        this.notificationService.changecountNotify(changeCount);
        this.cd.markForCheck();
      });
    }
    if (!StringUtils.isNullOrEmpty(data.notiLink)) {
      this.route.navigateByUrl(data.notiLink);
    }
  }

  // gắn cờ cho thông báo
  setTodoStatus(data: Notification) {
    if (data.todoStatus === 'N') {
      const dataTodo: Pick<Notification, 'todoStatus' | 'isViewed'> = {
        todoStatus: 'Y',
        isViewed: data.isViewed,
      };
      this.notificationService.updateTodo(data.notiId, dataTodo).subscribe((res) => {
        data.todoStatus = 'Y';
        this.cd.markForCheck();
      });
    } else {
      const dataTodo: Pick<Notification, 'todoStatus' | 'isViewed'> = {
        todoStatus: 'N',
        isViewed: data.isViewed,
      };
      this.notificationService.updateTodo(data.notiId, dataTodo).subscribe((res) => {
        data.todoStatus = 'N';
        this.cd.markForCheck();
      });
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next(null);
    this.destroy$.complete();
  }
}

class Notify extends DataSource<Notification> {

  isLoading: boolean;
  params: NotificationSearch;

  private readonly pageSize = 15;
  page = 0;
  cachedData: Notification[] = [];
  fetchedPages = new Set<number>();
  dataStream = new BehaviorSubject<Notification[]>(this.cachedData);
  private readonly complete$ = new Subject<void>();
  private readonly disconnect$ = new Subject<void>();

  constructor(private readonly notificationService: NotificationService, private readonly translate: TranslateService) {
    super();
  }

  completed(): Observable<void> {
    return this.complete$.asObservable();
  }

  connect(collectionViewer: CollectionViewer): Observable<Notification[]> {
    this.setup(collectionViewer);
    return this.dataStream;
  }

  disconnect(): void {
    this.disconnect$.next();
    this.disconnect$.complete();
  }

  private setup(collectionViewer: CollectionViewer): void {
    this.fetchPage(this.page);
    collectionViewer.viewChange.pipe(takeUntil(this.complete$), takeUntil(this.disconnect$)).subscribe(range => {
      const endPage = this.getPageForIndex(range.end);
      this.fetchPage(endPage + 1);
    });
  }

  private getPageForIndex(index: number): number {
    return Math.floor(index / this.pageSize);
  }

  private fetchPage(page: number): void {
    if (this.fetchedPages.has(page)) {
      if(page === 0) {
        this.dataStream.next(this.cachedData);
      }
      return;
    }
    this.isLoading = true;
    this.fetchedPages.add(page);
    this.notificationService.findAll({...this.params, page: page})
      .subscribe(res => {
        if(page >= res.data.totalPages) {
          this.complete$.next();
          this.complete$.complete();
        }
        const now = new Date();
        res.data.content.forEach((item: Notification)  => {
          item.initializedDate = getInitializedDate(item.createDate, now, this.translate);
        });
        this.cachedData.splice(page * this.pageSize, this.pageSize, ...res.data.content);
        this.dataStream.next(this.cachedData);
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      });
  }
}

