import {
  AfterViewChecked,
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  HostListener,
  OnDestroy,
  OnInit,
  QueryList, TemplateRef,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter, map } from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import * as _ from 'lodash';
import {NzModalRef, NzModalService} from 'ng-zorro-antd/modal';
import { PersonalInformation } from '@hcm-mfe/shared/data-access/models';
import { SessionService } from "@hcm-mfe/shared/common/store";
import { NotificationService } from '@hcm-mfe/shell/data-access/services';
import { StringUtils } from '@hcm-mfe/shared/common/utils';
import { Notification } from '@hcm-mfe/shell/data-access/models';
import { SessionKey } from '@hcm-mfe/shared/common/enums';
import { HeaderEventEmit } from '@hcm-mfe/shell/feature/header';
import { CommonUtils, WebSocketService } from '@hcm-mfe/shared/core';
import {PopUpThanksComponent} from "@hcm-mfe/shell/feature/pop-up-thanks";
import { HttpClient } from '@angular/common/http';
import {Subscription} from "rxjs";

export interface IBreadCrumb {
  label: string;
  url: string;
  disabled?: boolean;
}

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LayoutComponent implements OnInit, AfterViewInit, AfterViewChecked {
  modalRef: NzModalRef | undefined;
  idImage: number;
  static readonly ROUTE_DATA_PAGENAME = 'pageName';
  isCollapsed = false;
  pageName: string;
  countNoti: any;
  menus: any = [];
  menusOrigin: any = [];
  menusFlat: any = [];
  selectedMenu: any;
  isMobile: any;
  openMenuMap: { [id: string]: boolean } = {};
  width: number = window.innerWidth;
  height: number = window.innerHeight;
  mobileWidth = 993;
  currentUrl: string;
  sideBarClass = 'sidebar__primary--no-active';
  sideBarClassMobile = 'sidebar__primary--no-mobile';
  breadcrumbs: IBreadCrumb[];
  selectedMenuId: string;
  _routerState: string;
  showHeader = true;
  nzWidth = '300px';
  searchResult: PersonalInformation[];
  deviceMobileNotByWidth = false;
  firstClick = true;
  isNeedShowBackBtn = false;
  subs: Subscription[] = [];

  @ViewChild('elementWR', { static: false }) elementWR: ElementRef;
  @ViewChildren('elementWM') elementWM: QueryList<ElementRef>;
  @ViewChildren('elementW') elementW: QueryList<ElementRef>;


  constructor(
    private readonly route: Router,
    private readonly activeRoute: ActivatedRoute,
    private readonly translate: TranslateService,
    private readonly session: SessionService,
    private readonly webSocket: WebSocketService,
    private readonly toastr: ToastrService,
    private readonly notificationService: NotificationService,
    private readonly cdRef: ChangeDetectorRef,
    private readonly modal: NzModalService
  ) {
    this.getCurrentUrl(this.route);
    this.checkDevice();
    if (window.innerWidth < this.mobileWidth) {
      this.isMobile = true;
      this.sideBarClassMobile = 'sidebar__primary--mobile';
    }
    this.breadcrumbs = this.buildBreadCrumb(this.activeRoute.root);
    this.checkNeedShowBackBtn();
    // this.communicateService.request$.subscribe((req) => {
    //   if (req?.code === SHOW_HEADER) {
    //     this.showHeader = req?.value;
    //   }
    // });
  }

  ngOnInit(): void {
    this.route.events
      .pipe(
        filter((event) => event instanceof NavigationEnd),
        map(() => this.getPageInfo())
      )
      .subscribe((pageName: string) => {
        this.pageName = pageName;
        this.breadcrumbs = this.buildBreadCrumb(this.activeRoute.root);
      });
    this.pageName = this.getPageInfo();
    this.getMenu();
    this.notificationService.getCount().subscribe((res) => {
      this.notificationService.changecountNotify(res.data);
      this.session.setSessionData('countNotify', res.data);
    });
    this.webSocket.message$.subscribe((data: Notification) => {
      const count = this.session.getSessionData('countNotify');
      this.session.setSessionData('countNotify', count + 1);
      this.notificationService.changecountNotify(count + 1);
      const toast = this.toastr.show(data.shortContent, data.subject, { enableHtml: true });
      toast.onTap.subscribe(() => {
        this.onTapToastr(data);
      });
    });
    this.notificationService.getUnreadNoti().subscribe(res => {
      this.countNoti = res.data ? res.data.count : 0;
      this.idImage = res.data.thankYouHistoryDTO.imageId;
    })
  }

  checkNeedShowBackBtn() {
    this.subs.push(
      this.route.events
        .pipe(
          filter((event) => event instanceof NavigationEnd),
          map(() => {
            let child = this.activeRoute.firstChild;
            while (child) {
              if (child.firstChild) {
                child = child.firstChild;
              } else if (child.snapshot.data && child.snapshot.data['isNeedShowBackBtn']) {
                return child.snapshot.data['isNeedShowBackBtn'];
              } else {
                return null;
              }
            }
            return null;
          })
        )
        .subscribe((isNeedShowBackBtn: any) => {
          this.isNeedShowBackBtn = isNeedShowBackBtn;
        })
    )
  }

  onTapToastr(data: Notification) {
    this.notificationService.markAsRead(data.notiId).subscribe();
    if (!StringUtils.isNullOrEmpty(data.notiLink)) {
      this.route.navigateByUrl(data.notiLink);
    }
  }

  ngAfterViewInit(): void {}
  showPopUp(footerTmpl: TemplateRef<any>) {
    this.modalRef = this.modal.create({
      nzWidth: '30vw',
      nzTitle: '',
      nzContent: PopUpThanksComponent,
      nzComponentParams: {
        idImage: this.idImage
      },
      nzFooter: footerTmpl,
    });
    this.modalRef.afterClose.subscribe(() => {
      this.countNoti = 0;
    });
  }
  convertToFlat(tree) {
    let data: { [key: string]: object } = {};
    data['subs'] = Object.assign([], tree);
    let flatten = (subs, extractChildren) =>
      Array.prototype.concat.apply(
        subs,
        subs?.map((x) => flatten(extractChildren(x) || [], extractChildren))
      );

    let extractChildren = (x) => x.subs;

    let flat = flatten(extractChildren(data), extractChildren).map((x) => delete x.subs && x);
    return flat;
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  getMenu() {
    this.menus = this.session.getSessionData(SessionKey.MENU);
    this.menusOrigin = this.session.getSessionData(SessionKey.MENU);
    this.menusFlat = this.convertToFlat(_.cloneDeep(this.session.getSessionData(SessionKey.MENU)));
    this.setOpenMenu(this.menusOrigin);
  }

  getPageInfo() {
    let child = this.activeRoute;
    while (child.firstChild) {
      child = child.firstChild;
    }
    if (child.snapshot.data[LayoutComponent.ROUTE_DATA_PAGENAME]) {
      return child.snapshot.data[LayoutComponent.ROUTE_DATA_PAGENAME];
    }
    return '';
  }

  headerEventEmit($event: HeaderEventEmit) {
    switch ($event.subject) {
      case 'TRIGGER':
        if (this.isMobile) {
          this.firstClick = false;
          this.isCollapsed = false;
          this.sideBarClass = 'sidebar__primary--active';
          this.setOpenMenu(this.menus);
        }
        break;
      case 'CHAT':
        break;
      default:
        break;
    }
  }

  openHandler(value: number): void {
    for (const key in this.openMenuMap) {
      if (key !== value.toString()) {
        this.openMenuMap[key] = false;
      }
    }
    this.setParentMenuOpen(value, this.menus);
  }

  setParentMenuOpen(value: number, menus: any[]) {
    return menus?.some((menu) => {
      this.openMenuMap[menu.id] = false;
      if (menu.subs && menu.subs.length > 0) {
        this.openMenuMap[menu.id] = this.setParentMenuOpen(value, menu.subs);
        return this.setParentMenuOpen(value, menu.subs);
      }
      if (menu.id === value) {
        this.openMenuMap[menu.id] = true;
        return true;
      }
    });
  }

  clearMenuOpen(childNode: any) {
    let listParent = this.getParentNode(childNode, [childNode.id.toString()]);
    for (const key in this.openMenuMap) {
      if (listParent.indexOf(key) == -1) {
        this.openMenuMap[key] = false;
      }
    }
  }

  getParentNode(childNode: any, listParent: any[]) {
    if (childNode && childNode.parentId != null && childNode.parentId != 0) {
      listParent.push(childNode.parentId.toString());
      let parentNext = this.menusFlat.find((x) => x.id == childNode.parentId);
      this.getParentNode(parentNext, listParent);
    }
    return listParent;
  }

  @HostListener('window:resize', ['$event'])
  onWindowResize(event) {
    this.width = event.target.innerWidth;
    this.height = event.target.innerHeight;
    this.isMobile = this.width < this.mobileWidth;
    this.sideBarClassMobile =
      this.width < this.mobileWidth ? 'sidebar__primary--mobile' : 'sidebar__primary--no-mobile';
    this.checkDevice();
    if (!this.isMobile) {
      this.isCollapsed = true;
      this.sideBarClass = 'sidebar__primary--no-active';
    }
  }

  checkDevice() {
    const userAgent = navigator.userAgent;
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini|Mobile|mobile|CriOS/i.test(userAgent)) {
      this.deviceMobileNotByWidth = true;
      this.firstClick = true;
    } else {
      this.deviceMobileNotByWidth = false;
      this.firstClick = false;
    }
  }

  getCurrentUrl(router: Router) {
    this._routerState = this.activeRoute.snapshot['_routerState']['url'];
    router.events.subscribe((e) => {
      if (e instanceof NavigationEnd) {
        this.currentUrl = e.url;
      }
    });
  }

  closeMenuMobile() {
    this.sideBarClass = 'sidebar__primary--no-active';
    this.firstClick = true;
  }

  setOpenMenuMap(menus: any) {
    if (this.isMobile) {
      return false;
    }
    return menus?.some((menu) => {
      this.openMenuMap[menu.id] = false;
      if (menu.subs && menu.subs.length > 0) {
        this.openMenuMap[menu.id] = this.setOpenMenuMap(menu.subs);
        return this.setOpenMenuMap(menu.subs);
      }
      if (this.currentUrl != '/' && this.selectedMenuId != '/' && menu.uri === this.currentUrl) {
        this.openMenuMap[menu.id] = true;
        return true;
      }
    });
  }

  setOpenMenu(menus: any, id?: any) {
    const getNodes = (result, object) => {
      object.open = false;
      this.openMenuMap[object.id] = false;
      if (id) {
        if (object.id === id) {
          let objectResult = Object.assign({}, object);
          result.push(objectResult);
          objectResult.open = true;
          this.openMenuMap[object.id] = true;
          return result;
        }
      } else {
        if (this.currentUrl != '/' && this.selectedMenuId != '/' && object.uri === this.currentUrl) {
          let objectResult = Object.assign({}, object);
          result.push(objectResult);
          objectResult.open = true;
          this.openMenuMap[object.id] = true;
          return result;
        }
      }
      if (Array.isArray(object.subs)) {
        const subs = object.subs.reduce(getNodes, []);
        if (subs.length) {
          object.open = false;
          this.openMenuMap[object.id] = false;
          for (let i = 0; i < subs.length; i++) {
            if (subs[i].open) {
              object.open = true;
              this.openMenuMap[object.id] = true;
              break;
            }
          }
          result.push({ ...object, subs });
        } else {
          object.open = false;
          this.openMenuMap[object.id] = false;
          result.push({ ...object });
        }
      } else {
        object.open = false;
        this.openMenuMap[object.id] = false;
        result.push({ ...object });
      }
      return result;
    };
    this.menus = menus.reduce(getNodes, []);
  }

  buildBreadCrumb(route: ActivatedRoute, url: string = '', breadcrumbs: IBreadCrumb[] = []): IBreadCrumb[] {
    let label = route.routeConfig && route.routeConfig.data ? route.routeConfig.data.breadcrumb : '';
    let path = route.routeConfig && route.routeConfig.data ? route.routeConfig.path : '';
    const disabled = route.routeConfig && route.routeConfig.data && route.routeConfig.data.disabled ? route.routeConfig.data.disabled : false;
    const lastRoutePart = path.split('/').pop();
    const isDynamicRoute = lastRoutePart.startsWith(':');
    if (isDynamicRoute && !!route.snapshot) {
      const paramName = lastRoutePart.split(':')[1];
      path = path.replace(lastRoutePart, route.snapshot.params[paramName]);
      label = route.snapshot.params[paramName];
    }
    const nextUrl = path ? `${url}/${path}` : url;
    const breadcrumb: IBreadCrumb = {
      label,
      url: nextUrl,
      disabled: disabled
    };
    const newBreadcrumbs = breadcrumb.label ? [...breadcrumbs, breadcrumb] : [...breadcrumbs];
    if (route.firstChild) {
      return this.buildBreadCrumb(route.firstChild, nextUrl, newBreadcrumbs);
    }
    return newBreadcrumbs;
  }

  mouseenter() {
    if (!this.deviceMobileNotByWidth) {
      this.setOpenMenu(this.menus);
      this.isCollapsed = false;
    }
  }

  mouseLeave() {
    if (!this.deviceMobileNotByWidth) {
      if (!this.isCollapsed) {
        this.setOpenMenu(this.menus);
        this.isCollapsed = true;
      }
    }
  }

  searchMenu($event: string, data: any = this.menus) {
    this.filter($event, this.menusOrigin);
  }

  filter(text, array: any) {
    const getNodes = (result, object) => {
      if (!text) {
        this.openMenuMap[object.id] = false;
        result.push(object);
        return result;
      } else {
        if (!object.displayName) object.displayName = '';
        let index = object.displayName.toLowerCase().indexOf(text.toLowerCase());
        if (index != -1) {
          let objectResult = Object.assign({}, object);
          objectResult.displayName =
            objectResult.displayName.substring(0, index) +
            "<span class='text__search--highlight'>" +
            objectResult.displayName.substring(index, index + text.length) +
            '</span>' +
            objectResult.displayName.substring(index + text.length);
          result.push(objectResult);
          this.openMenuMap[object.id] = true;
          //this.setHighlight(object.subs, text);
          return result;
        }
        if (Array.isArray(object.subs)) {
          const subs = object.subs.reduce(getNodes, []);
          if (subs.length) {
            result.push({ ...object, subs });
            this.openMenuMap[object.id] = true;
          } else {
            this.openMenuMap[object.id] = false;
          }
        } else {
          this.openMenuMap[object.id] = false;
        }
        return result;
      }
    };
    this.menus = array.reduce(getNodes, []);
  }

  setClassSelected(id) {
    this.selectedMenuId = id;
  }

  touchEnter() {
    if (!this.isMobile && this.deviceMobileNotByWidth && this.firstClick) {
      this.firstClick = false;
      this.isCollapsed = false;
      this.setOpenMenu(this.menus);
    }
  }

  touchLeave() {
    if (!this.isMobile && this.deviceMobileNotByWidth && !this.firstClick) {
      this.firstClick = true;
      this.isCollapsed = true;
      this.setOpenMenu(this.menus);
    }
  }

  translates(label: string): string {
    return this.translate.instant(CommonUtils.isNullOrEmpty(label) ? " " : label);
  }

  trackByFn(index, item) {
    return item.id;
  }

  ngOnDestroy(): void {
    this.subs?.forEach(sub => sub?.unsubscribe());
  }

  onBackClick() {
    localStorage.setItem("redirectByBackBtn", "true");
    history.back();
  }

  open(uri: string) {
    if(uri.includes('https') || uri.includes('http')) {
      window.location.href = uri
    } else {
      this.route.navigateByUrl(uri);
    }
  }
}

