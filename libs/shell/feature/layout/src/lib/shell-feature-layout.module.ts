import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './layout/layout.component';
import { ShellFeatureMenuSearchModule } from '@hcm-mfe/shell/feature/menu-search';
import { ShellFeatureHeaderModule } from '@hcm-mfe/shell/feature/header';
import { SharedUiTopProgressBarModule } from '@hcm-mfe/shared/ui/top-progress-bar';
import { RouterModule } from '@angular/router';
import {SharedCommonStoreModule} from "@hcm-mfe/shared/common/store";
import {NzLayoutModule} from "ng-zorro-antd/layout";
import {NzPageHeaderModule} from "ng-zorro-antd/page-header";
import {NzBreadCrumbModule} from "ng-zorro-antd/breadcrumb";
import {TranslateModule} from "@ngx-translate/core";
import {NzMenuModule} from "ng-zorro-antd/menu";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzMessageModule} from "ng-zorro-antd/message";
import {PopupService, SharedUiPopupModule} from "@hcm-mfe/shared/ui/popup";

@NgModule({
    imports: [CommonModule,
        SharedCommonStoreModule,
        ShellFeatureMenuSearchModule,
        ShellFeatureHeaderModule,
        SharedUiTopProgressBarModule,
        RouterModule,
        NzLayoutModule,
        NzPageHeaderModule,
        NzBreadCrumbModule,
        TranslateModule,
        NzMenuModule, SharedUiMbButtonModule, NzMessageModule, SharedUiPopupModule
    ],
  declarations: [LayoutComponent],
  exports: [LayoutComponent],
})
export class ShellFeatureLayoutModule {}
