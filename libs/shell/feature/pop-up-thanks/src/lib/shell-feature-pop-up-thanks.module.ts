import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {PopUpThanksComponent} from "./pop-up-thanks/pop-up-thanks.component";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule],
  exports: [PopUpThanksComponent],
  declarations: [PopUpThanksComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class ShellFeaturePopUpThanksModule {}
