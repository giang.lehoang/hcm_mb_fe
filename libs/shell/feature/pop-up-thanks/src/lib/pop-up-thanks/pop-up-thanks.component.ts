import {Component, Injector, Input, OnInit, ViewChild} from "@angular/core";
import {NzModalRef} from "ng-zorro-antd/modal";
import {NotificationService} from "@hcm-mfe/shell/data-access/services";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: 'app-pop-up-thanks',
  templateUrl: './pop-up-thanks.component.html',
  styleUrls: ['./pop-up-thanks.component.scss'],
})
export class PopUpThanksComponent extends BaseComponent implements OnInit {
  @Input() idImage: number | undefined;
  imagePath:any;
  constructor(injector : Injector,
              readonly _sanitizer: DomSanitizer,
              private readonly modalRef: NzModalRef,
              private readonly notificationService: NotificationService){
    super(injector);
  }
  ngOnInit(): void {
    this.getImage();
    this.updateThankMessage();
  }
  getImage() {
    if (!this.idImage) {
      return;
    }
    this.isLoading = true;
    this.notificationService.getImageThank(this.idImage).subscribe(res => {
      this.imagePath = this._sanitizer.bypassSecurityTrustResourceUrl(
        'data:image/jpg;base64,' + res.data
      );
      this.isLoading = false;
    }, () => {
      this.isLoading = false;
    })
  }
  redirectToDetail() {
    this.router.navigateByUrl('/goc-it/thank-you/send/history');
    this.modalRef.destroy();
  }
  updateThankMessage() {
    this.isLoading = true;
    this.notificationService.updateUnreadNoti().subscribe(res => {
      this.isLoading = false;
    }, err => {
      this.isLoading = false;
      if(err.code === 500) {
        this.toastrCustom.error(this.translate.instant("common.notification.error"));
      } else {
        this.toastrCustom.error(err.message);
      }
    });
  }
}
