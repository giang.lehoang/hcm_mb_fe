import {PersonnelExceptionRatingsService} from "../personnelExceptionRatings.service";

export class UrlConstant {
  public static readonly END_POINT_GOAL_MANAGEMENT = {
    urlEndPointPeriodConfig: `goa/v1.0/period-config`,
    urlEndPointPositionSearch:`goa/v1.0/position`,
    urlEndPointSetOfTarget:`goa/v1.0/set-of-target`,
    urlTarget: `goa/v1.0/target`,
    urlLookupValues: `goa/v1.0/lookup-values`,
    urlTargetRacing: `goa/v1.0/target-rating`,
    urlTargetRacingEmployee: `goa/v1.0/target-rating/employee/history`,
    urlTargetResult: `goa/v1.0/target-result`,
    urlEmployee: `goa/v1.0/employee`,
    urlEndPointAssessmentPeriod: 'goa/v1.0/period',
    urlEndPointSystemAllocation: 'goa/v1.0/classification-rate',
    urlRatingCategory: `goa/v1.0/classification`,
    urlClassification: `goa/v1.0/classification-result`,
    urlClassificationLine: `goa/v1.0/classification-result/line`,
    urlEmpInAllocation: `goa/v1.0/classification-changes`,
    urlListPersonnelAllocation: 'goa/v1.0/allocated-of-employees',
    urlPersonnelExceptionRatingsService: 'goa/v1.0/allocated-exception/rate',
    urlPersonnelExceptionService: 'goa/v1.0/allocated-exception',
    urlSetUpSystem: 'goa/v1.0/disciplines',
    urlClassificationShare: 'goa/v1.0/share/classification',
    urlTargetResultSyncTheTic: 'goa/v1.0/target-result/synthetic',
    urlCrmEmp: 'goa/v1.0/employee-crm'
  }
  public static readonly END_POINT_MODEL_PLAN = {
    urlEndPointOrgTree: `hcm-model-plan/organization-tree`,
    urlLookupValues: `hcm-model-plan/v1.0/lookup-values`,
    urlEndPointOrgTreeId: `hcm-model-plan/v1.0/organization-tree`,
    urlEndpointJob:`hcm-model-plan/v1.0/jobs/type`,
    urlEnpointLookupValues:`hcm-model-plan/v1.0/lookup-values`,
    url_enpoint_organization_tree_get_by_id: 'hcm-model-plan/v1.0/organization-tree/findTreeById',
    url_enpoint_organization_tree_get_by_name: 'hcm-model-plan/organization-tree/searchTreeByName',
    SUB_TREE_ACTIVE: '/v1.0/organization-tree/sub-branch/list-active',
    SUB_TREE_INACTIVE: '/v1.0/organizations/sub-branch/list-active-and-inactive',
    urlLookupCodeHinhThucKYLuat: 'hcm-model-plan/v1.0/category/lookup-values'
  }
  public static readonly HCM_ENDPOINT = {
    urlEndPointProjectListJob: `hcm-model-plan/v1.0/jobs/type`
  }

  public static readonly END_POIN_TARGET_PLAN_OF_STAFF = {
    url_endpoint_list_detail_target_plan_of_staff: 'goa/v1.0/set-of-target/sot-employee',
  }
  public static readonly END_POLICY_MANAGEMENT = {
    url_endpoint_list_allowances_extend: 'pol/v1.0/allowance-extend-list/search',
  }

  public static readonly END_POINT_POLICY_MANAGEMENT = {
    urlEndPointAllowanceProcess: `v1.0/allowance-process`,
    urlEndPointAllowanceType: `v1.0/allowance`,
    urlEndPointLookupCode: `v1.0/lookup-values`
  }
}
