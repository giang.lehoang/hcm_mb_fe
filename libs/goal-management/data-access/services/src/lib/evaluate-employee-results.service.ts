import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {UrlConstant} from "@hcm-mfe/goal-management/data-access/services";
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";
import {BaseService} from "@hcm-mfe/shared/common/base-service";

@Injectable({
  providedIn: 'root',
})
export class EvaluateEmployeeResultsService extends BaseService {
  constructor(readonly http: HttpClient) {
    super(http)
  }
  objectDetail = {
    asmIds: null,
    empId: null
  }

  getListTargetPlanStaff(asmId: number) {

    return this.post(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlTargetRacing}/employee`,
      asmId, undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
  upDateEveluateEmployeeResult(request: any) {
    return this.put(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlTargetRacing}/employee`,
      request, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  sendEveluateEmployeeResult(request: any) {
    return this.post(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlTargetRacing}/employee/public`,
      request,undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  public getListRatingResultEmp(params: any) {
    return this.post(
      `${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointSetOfTarget}/rating-result-emp/search`,
      params,
      undefined,
      MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT
    );
  }

  public saveRatingResultEmp(params: any) {
    return this.put(
      `${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointSetOfTarget}/rating-result-emp`,
      params,
      MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT
    );
  }

  public sendRatingResultEmp(params: any) {
    return this.post(
      `${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointSetOfTarget}/assign-plan-emp/public-result`,
      params,
      undefined,
      MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT
    );
  }

  setInformationDataDetail(data: any) {
    this.objectDetail = data;
  }

  getInFormationDataDetail() {
    return this.objectDetail;
  }
}
