import {Injectable} from "@angular/core";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {UrlConstant} from "@hcm-mfe/goal-management/data-access/services";
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";
import {Observable} from "rxjs";
import {
  IEmpOfAllocations,
  IListPersonnelAllocationDetail,
  orgResults
} from "@hcm-mfe/goal-management/data-access/models";

@Injectable({
  providedIn: 'root',
})
export class ListPersonnelAllocationService extends BaseService{

  private listDataInitialIEmpOfAllocations: IEmpOfAllocations[] | undefined;
  public getOrg(): Observable<any> {
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEmpInAllocation}/org`,
      undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  public getAllocatedOfEmployeeDetail(id: number) {
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlListPersonnelAllocation}/detail/${id}`,
      undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  public getDepartmentLineDropList() {
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlListPersonnelAllocation}/org`,
      undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
  public getListAllocation(params: any) {
    return this.post(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlListPersonnelAllocation}`,
      params, undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
  public saveListAllocation(params: any) {
    return this.put(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlListPersonnelAllocation}`,
      params, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
  setListDataInitialEmpOfAllocations(item: IEmpOfAllocations[] | undefined) {
    this.listDataInitialIEmpOfAllocations = item;
  }

  getListDataInitialEmpOfAllocations() {
    return this.listDataInitialIEmpOfAllocations;
  }
  public getListDetail(params: any) {
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlTargetResult}/synthetic/detail`,
      {params}, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
  public getListOrg() {
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlTargetResult}/synthetic/org-line`,
      undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
  public exportExcel(params: any) {
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlTargetResult}/synthetic/detail/export`,
      {params, responseType: 'arraybuffer'}, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
  public getListClassification(params: any) {
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlRatingCategory}/period`,
      {params}, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
}
