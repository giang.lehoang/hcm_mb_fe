import {Injectable} from "@angular/core";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {UrlConstant} from "@hcm-mfe/goal-management/data-access/services";
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";

@Injectable({
  providedIn: 'root',
})
export class RatingCategoryService extends BaseService {
  getListCategoryRating(params: any) {
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlRatingCategory}`,
      { params }, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
  createCategoryRating(params: any) {
    return this.post(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlRatingCategory}`,
       params, undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
  updateCategoryRating(params: any) {
    return this.put(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlRatingCategory}`,
      params, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
}
