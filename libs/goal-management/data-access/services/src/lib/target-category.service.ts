import { Injectable } from '@angular/core';
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {UrlConstant} from "@hcm-mfe/goal-management/data-access/services";
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";

@Injectable({
  providedIn: 'root',
})
export class TargetCategoryService extends BaseService{
  getListTargetCategory(params: any) {
    const url = `${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlTarget}`;
    return this.get(url, {params}, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  getDetailTargetCategory(id: number | undefined) {
    const url = `${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlTarget}/${id}`;
    return this.get(url, undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  getListTargetParent(paramsSubmit: any) {
    const url = `${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlTarget}/parent`;
    return this.get(url, {params: {
      typeRule: paramsSubmit.typeRule, scoreCard: paramsSubmit.scoreCard
    }}, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  createTargetCategory(request: any) {
    const url = `${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlTarget}`;
    return this.post(url, request, undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  deleteItemCategory(id: number) {
    const url = `${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlTarget}/${id}`;
    return this.delete(url,undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
}
