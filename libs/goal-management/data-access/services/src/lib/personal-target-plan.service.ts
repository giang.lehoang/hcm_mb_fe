import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {IEmployData} from "@hcm-mfe/goal-management/data-access/models";
import {UrlConstant} from "@hcm-mfe/goal-management/data-access/services";
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";


@Injectable({
  providedIn: 'root',
})
export class PersonalTargetPlanService extends BaseService {
  private objectDataApprovePlanAssignMent: IEmployData | undefined;
  private objectDataPersonalTargetList: IEmployData | undefined;

  private type = '';

  getDetailPersonalTargetPlan(params: any) {
    return this.get(`${UrlConstant.END_POIN_TARGET_PLAN_OF_STAFF.url_endpoint_list_detail_target_plan_of_staff}/indirect`,
      { params }, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  setObjectDetailApprovePlanAssignment(value: IEmployData) {
    this.objectDataApprovePlanAssignMent = value;
  }

  setObjectDetailPersonalTargetList(value: any) {
    this.objectDataPersonalTargetList = value;
  }

  getObjectDetailPersonalTargetList() {
    return this.objectDataPersonalTargetList;
  }

  getObjectDetailApprovePlanAssignment() {
    return this.objectDataApprovePlanAssignMent;
  }

  setType(type: any) {
    this.type = type;
  }

  getType() {
    return this.type;
  }

  aprrovePersonalTargetPlan(params: any) {
    return this.post(`${UrlConstant.END_POIN_TARGET_PLAN_OF_STAFF.url_endpoint_list_detail_target_plan_of_staff}/approve`,
      params,
      undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
  recivePersonalTargetPlan(request: any) {
    return this.put(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointSetOfTarget}/current-sot-employee`,
      request, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  getDetailPersonalTargetList(params: any) {
    return this.post(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointSetOfTarget}/detail-sot-employee`,
      params,
      undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  updateStatus(data?: any, option?: any): Observable<any> {
    return this.put(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointSetOfTarget}/current-sot-employee`,
      data, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
}
