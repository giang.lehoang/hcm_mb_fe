import {Injectable} from "@angular/core";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {UrlConstant} from "@hcm-mfe/goal-management/data-access/services";
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";

@Injectable({
  providedIn: 'root',
})
export class ListEmpAllocatedService extends BaseService{
  public getListEmpAllocated(params: any) {
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEmpInAllocation}`,
      {params}, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
  public getPeriod() {
    const params = {
      isIgnoreStatusNotCurrent: true
    }
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointAssessmentPeriod}`,
      {params}, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
  public getOrg() {
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEmpInAllocation}/org`,
      undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
  public offerEmpAllocated(requestBody: any) {
    return this.put(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEmpInAllocation}/propose`,
      requestBody, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
  public getConfig(params: any) {
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEmpInAllocation}/propose/config`,
      {params}, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
}
