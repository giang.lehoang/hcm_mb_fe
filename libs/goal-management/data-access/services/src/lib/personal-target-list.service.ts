import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import {UrlConstant} from "@hcm-mfe/goal-management/data-access/services";
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";

@Injectable({
  providedIn: 'root',
})
export class PersonalTargetListService extends BaseService {

  public getList(params?: any): Observable<any> {
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointSetOfTarget}/current-sot-employee`,
      {params: this.toParams(params)}, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  public getEmployee(params?: any): Observable<any> {
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEmployee}`,
      {params: this.toParams(params)}, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  public updateStatus(data?: any, option?: any): Observable<any> {
    return this.put(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointSetOfTarget}/current-sot-employee`,
      data, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

}
