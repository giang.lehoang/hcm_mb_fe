import { Injectable } from '@angular/core';
import {Observable, tap} from 'rxjs';
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {UrlConstant} from "@hcm-mfe/goal-management/data-access/services";
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";
import {
  IPersonnelExceptionRatings, IResponseContentEntity,
  IResponseEntityData,
} from "@hcm-mfe/goal-management/data-access/models";


@Injectable({
  providedIn: 'root',
})
export class PersonnelExceptionRatingsService extends BaseService {
  getPersonnelExceptionRatingsService(params: any): Observable<IResponseEntityData<IResponseContentEntity<IPersonnelExceptionRatings>>> {
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlPersonnelExceptionRatingsService}`,
      { params }, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
}
