import { Injectable } from '@angular/core';
import {IEmployData} from "@hcm-mfe/goal-management/data-access/models";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";
import {UrlConstant} from "@hcm-mfe/goal-management/data-access/services";


@Injectable({
  providedIn: 'root',
})
export class GetCriteriaService extends BaseService{
  private objectData = {};

  public getListTargetPlanStaff(params: any) {
    params = this.toParams(params);
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointSetOfTarget}/sot-employee`,
      {params}, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  public saveGiveCriteria(params: any) {
    return this.post(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointSetOfTarget}/sot-employee`, params,
      undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  setListDetail(value: IEmployData) {
    this.objectData = value;
  }
  getInformationData() {
    return this.objectData;
  }

  public sendEmail(params: any) {
    return this.post(`${UrlConstant.END_POIN_TARGET_PLAN_OF_STAFF.url_endpoint_list_detail_target_plan_of_staff}/publicize`, params,
      undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  public getNameScoreCard(params: any) {
    params = this.toParams(params);
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlLookupValues}/lvaValue`,
      {params}, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

}
