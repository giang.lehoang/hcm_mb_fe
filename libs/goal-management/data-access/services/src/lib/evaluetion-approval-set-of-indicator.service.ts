import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {IEmployData} from "@hcm-mfe/goal-management/data-access/models";
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";
import {environment} from "@hcm-mfe/shared/environment";
import {UrlConstant} from "@hcm-mfe/goal-management/data-access/services";


@Injectable({
  providedIn: 'root',
})
export class EvaluetionApprovalSetOfIndicatorService extends BaseService {
  constructor(readonly http: HttpClient) {
    super(http);
  }

  public getListEmployeeEvaluetionApproval(params: IEmployData): Observable<any> {
    return this.post(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointSetOfTarget}/target-employee/search`, params,
      undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  public getAssessmentPeriod(params?: any): Observable<any> {
    return this.http.get(`${environment.baseUrlTarget}${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlTargetRacing}/employee/period`,
      { params: this.toParams(params) });
  }

  public correctionEmployeeEvaluetionApproval(param: Pick<IEmployData, "asmIds" | "flagStatus" | "periodId">): Observable<any> {
    return this.post(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlTargetRacing}/employee/adjust`, param,
      undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  public ApprovalorRejectEmployeeEvaluetionApproval(param: any): Observable<any> {
    return this.post(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlTargetRacing}/employee/approve`, param,
      undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  public SendEmployeeEvaluetionApproval(param: any): Observable<any> {
    return this.post(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlTargetRacing}/employee/public`, param,
      undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
  public saveScaleNum(param: any): Observable<any> {
    return this.put(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlTargetRacing}/employee/update-scale-num`,
      param, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
}
