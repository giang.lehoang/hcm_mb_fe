import {Injectable} from "@angular/core";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {UrlConstant} from "@hcm-mfe/goal-management/data-access/services";
import {MICRO_SERVICE, SERVICE_NAME} from "@hcm-mfe/shared/common/constants";
import {BehaviorSubject, map, Observable, of} from "rxjs";
import {
  IChangeRatingDTOS,
  IHandleClassification,
  IListClassificationShare, IParamAdjustedHandle, IParamDownloadExelItem,
  IParamSearchHandleClassification,
  IParamStatusHandleClassification,
  IResponseEntityData,
  ITargetResultSyntheticOrgLine,
  status
} from "@hcm-mfe/goal-management/data-access/models";
import {Pagination} from "@hcm-mfe/shared/data-access/models";

@Injectable({
  providedIn: 'root',
})

export class handleClassification extends BaseService {

  pagination = new Pagination();

  requestedParam$: BehaviorSubject<IParamSearchHandleClassification> = new BehaviorSubject<IParamSearchHandleClassification>({
    periodId: '',
    page: 0,
    size: this.pagination.pageSize,
    dataId: '',
    apportion: '',
    flagStatus: 0
  });

  listApportioned$ = of([
    {
      value: '',
      label: 'Tất cả'
    },
    {
    value: 0,
    label: 'Ngoài phân bổ'
    },
    {
      value: 1,
      label: 'Trong phân bổ'
    }
   ]);
  listFlagStatus = of([
    {
      value: '',
      label: 'Tất cả'
    },
    {
      value: status.waitApproval,
      label: 'Chờ xác nhận',
    },
    {
      value: status.approval,
      label: 'Đồng ý'
    },
    {
      value: status.reject,
      label: 'Từ chối'
    },
    {
      value: status.rejectApproval,
      label: 'Từ chối duyệt'
    },
    {
      value: status.Approved,
      label: 'Đã phê duyệt'
    },
    {
      value: status.notSuggest,
      label: 'Không đề xuất'
    }
  ]);

  listDataHandleClassification(params: IParamSearchHandleClassification): Observable<IResponseEntityData<IHandleClassification<IChangeRatingDTOS>>> {
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlTargetResultSyncTheTic}`,
      {
        params
      }, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  };

  listDataSyntheticOrgLine$ = this.get(
    `${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlTargetResultSyncTheTic}/org-line`,
    undefined,
    MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT
  ) as Observable<IResponseEntityData<ITargetResultSyntheticOrgLine[]>>;

  updateStatusHandleClassification(params: Partial<IParamStatusHandleClassification>): Observable<any> {
    return this.put(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlTargetResultSyncTheTic}/status`,
        params
      , MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  };

  updateAdjustedHandleClassification(params: IParamAdjustedHandle): Observable<any> {
    return this.post(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlTargetResultSyncTheTic}/adjusted`,
      params, undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  };

  DownloadFileExelHandleClassification(params: IParamDownloadExelItem): Observable<any> {
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlTargetResultSyncTheTic}/export`,
      {responseType: 'arraybuffer',params:params }
      , MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  };

  DownloadFileExelHandleClassificationTable(params: {
    dataId : number,
    periodId: number,
    apportion: number,
    flagStatus: number
  }): Observable<any> {
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlTargetResultSyncTheTic}/all/export`,
      {responseType: 'arraybuffer',params:params }
      , MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  };



}
