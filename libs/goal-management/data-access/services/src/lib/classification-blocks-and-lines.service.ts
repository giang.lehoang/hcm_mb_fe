import {Injectable} from "@angular/core";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";
import {UrlConstant} from "./constant/url.class";
import {IListRegisterIdCheck, orgResults} from "@hcm-mfe/goal-management/data-access/models";


@Injectable({
  providedIn: 'root'
})
export class ClassificationBlocksAndLinesService extends BaseService {

    public getClassificationBlocksAndLine(periodId: number) {
      const url = `${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlClassification}/${periodId}`;
      return this.get(url, undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
    }

    public getClassificationBlocksAndLinePopupList(param: number | undefined) {
      const url = `${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlClassification}/org-cfg/${param}`;
      return this.get(url, undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
    }

  public postClassificationBlocksAndLinePopupList(params: IListRegisterIdCheck[]) {
    const url = `${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlClassification}/org-cfg`;
    return this.post(url, params, undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  public saveRecordClassificationBlocksLines(params: orgResults[] | undefined) {
    const url = `${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlClassification}`;
    return this.post(url, params, undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

}
