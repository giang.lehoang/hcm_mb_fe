import {Injectable} from "@angular/core";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";
import {UrlConstant} from "@hcm-mfe/goal-management/data-access/services";

@Injectable({
  providedIn: 'root'
})
export class AssignTargetToAllEmployeesService extends BaseService {
  public getListEmployeeTargetAssigned(params: any) {
    return this.post(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointSetOfTarget}/assign-plan-emp/search`,
      params, undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  public saveListEmployeeTargetAssign(params: any){
    return this.put(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointSetOfTarget}/assign-plan-emp`,
      params, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  public sendListEmployeeTargetAssign(params: any){
    return this.post(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointSetOfTarget}/assign-plan-emp/public`,
      params, undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
}
