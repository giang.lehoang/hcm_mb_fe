import {Injectable} from "@angular/core";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";
import {UrlConstant} from "./constant/url.class";


@Injectable({
  providedIn: 'root'
})
export class EmployeeTargetService extends BaseService {
  public getListEmployeeTarget(params: any) {
    return this.post(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointSetOfTarget}/sot-employee/search`,
      params, undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  public getListEmployeeOfManager() {
    const url = `${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointSetOfTarget}/sot-employee/employees-of-manager`;
    return this.get(url, undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  public postCorrectionEmployee(params: any) {
    const url = `${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointSetOfTarget}/sot-employee/adjust`;
    return this.post(url, params, undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  public posSotEmployeePublicize(params: any) {
    const url = `${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointSetOfTarget}/sot-employee/publicize`;
    return this.post(url, params, undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  public updateEmployeeScaleNumber(params: any) {
    const url = `${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointSetOfTarget}/sot-employee`;
    return this.put(url, params, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  public getSotEmployeeAssignment() {
    const url = `${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointSetOfTarget}/sot-employee/assignment`;
    return this.get(url, undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  public getListNotAutoAssingMent() {
    const url = `${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointSetOfTarget}/position/list-not-auto-assign`;
    return this.get(url, undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  public updateAutoAssingMent(request: any) {
    const url = `${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointSetOfTarget}/position/assign`;
    return this.post(url, request, undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

}
