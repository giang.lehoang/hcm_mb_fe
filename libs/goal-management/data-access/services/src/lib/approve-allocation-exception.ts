import {Injectable} from "@angular/core";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {UrlConstant} from "@hcm-mfe/goal-management/data-access/services";
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";
import {IParamApprove} from "../../../models/src/lib/IApproveEmpAllocated";

@Injectable({
  providedIn: 'root',
})
export class ApproveAllocationExceptionService extends BaseService{
  public getPeriod() {
    const params = {
      isIgnoreStatusNotCurrent: true
    }
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointAssessmentPeriod}`,
      {params}, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
  public getListApprove(params: any) {
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEmpInAllocation}/approve`,
      {params}, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
  public approveEmpAllocated(body: any) {
    return this.put(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEmpInAllocation}/approve`,
      body, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
  public getListEmployee(params: any) {
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlTargetResult}/emp`,
      {params}, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
  public saveEmployee(params: any) {
    return this.put(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlTargetResult}/apportion`,
      params, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
  public getConfig(params: any) {
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEmpInAllocation}/propose/config`,
      {params}, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
  public updateConfig(params: any) {
    return this.put(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEmpInAllocation}/propose/config`,
      params, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
  public getAmountApportion(params: IParamApprove) {
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlTargetResult}/apportion/rate`,
      {params}, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
}
