import {Injectable} from "@angular/core";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";
import {UrlConstant} from "./constant/url.class";


@Injectable({
  providedIn: 'root'
})
export class ApprovePlanAssignmentService extends BaseService {
  public getListSotEmployee(params: any){
    const url = `${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointSetOfTarget}/sot-employee/indirect-management/search`;
    return this.post(url, params, undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  public approvePlanAssignment(params: any){
    const url = `${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointSetOfTarget}/sot-employee/approve`;
    return this.post(url, params, undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
}
