import { Injectable } from '@angular/core';
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {THE_DIEM} from "@hcm-mfe/goal-management/data-access/models";
import {UrlConstant} from "@hcm-mfe/goal-management/data-access/services";
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";

@Injectable({
  providedIn: 'root',
})
export class BallancedScoreCardService extends BaseService {

  getListBallancedGoal() {
    const url = `${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlLookupValues}`;
    return this.get(url, {
      params: {
        lcoName: THE_DIEM,

      }
    }, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }


  getListBallancedGoalActive() {
    const url = `${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlLookupValues}`;
    return this.get(url, {params: {
        lcoName: THE_DIEM,
        flagStatus: 1
      }}, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  getListBallancedGoalDetail(id: number | undefined) {
    const url = `${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlLookupValues}/${id}`;
    return this.get(url, undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  createBallancedGoal(objectSubmit: any) {
    const url = `${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlLookupValues}`;
    return this.post(url, objectSubmit, undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  updateBallancedGoal(objectSubmit: any) {
    const url = `${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlLookupValues}`;
    return this.put(url, objectSubmit, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);

  }

  deleteBallancedScorecard(id: number){
    const url = `${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlLookupValues}/${id}`;
    return this.delete(url, undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
}
