import {Injectable} from "@angular/core";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";
import {UrlConstant} from "./constant/url.class";
import {IListRegisterIdCheck, ItAssessmentPeriod, orgResults} from "@hcm-mfe/goal-management/data-access/models";
import {IParamSearchAllocationLineBranch} from "../../../models/src/lib/IAllocationLineBranch";


@Injectable({
  providedIn: 'root'
})
export class AllocationLineBranchService extends BaseService {
  private dataCurrentPeriod: ItAssessmentPeriod | undefined = undefined;

  private listDataInitialOrgResults: orgResults[] | undefined;

  public getListAllocationLineBranchSelect() {
    const url = `${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlClassification}/line/branch`;
    return this.get(url, undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  public getAllocationLineBranchDataTable(params: IParamSearchAllocationLineBranch | undefined) {
    const url = `${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlClassification}/line`;
    return this.get(url, { params: { periodId: params?.periodId, orgId: params?.orgId ? params?.orgId : ''  } }, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  setCurrentPeriod(item: ItAssessmentPeriod | undefined) {
    this.dataCurrentPeriod = item;
  }

  getCurrentPeriod() {
    return this.dataCurrentPeriod;
  }

  setListDataInitialOrgResults(item: orgResults[] | undefined) {
    this.listDataInitialOrgResults = item;
  }

  getListDataInitialOrgResults() {
    return this.listDataInitialOrgResults;
  }

  public saveRecordAllocationLineBranch(params: orgResults[] | undefined) {
    const url = `${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlClassification}/line`;
    return this.post(url, params, undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
}
