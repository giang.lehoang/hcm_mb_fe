import {Injectable} from "@angular/core";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {UrlConstant} from "@hcm-mfe/goal-management/data-access/services";
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";
import {BehaviorSubject, Observable, of, switchMap} from "rxjs";
import {
  IListClassificationShare,
  IParamSetupSystem,
  IResponseContentEntity,
  IResponseEntityData, ISetupSystem
} from "@hcm-mfe/goal-management/data-access/models";
import {Pagination} from "@hcm-mfe/shared/data-access/models";

@Injectable({
  providedIn: 'root',
})

export class SetUpSystemService extends BaseService {

  pagination = new Pagination();

  requestedParam$: BehaviorSubject<IParamSetupSystem> = new BehaviorSubject<IParamSetupSystem>({
    periodId: '',
    page: 0,
    size: this.pagination.pageSize
  });

  listDisciplines$ = this.requestedParam$?.pipe(
    switchMap(params => this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlSetUpSystem}`,
      {
        params
      }, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT) as Observable<IResponseEntityData<IResponseContentEntity<ISetupSystem>>>));

  // urlClassificationShare
  listClassificationShare$ = this.get(
    `${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlClassificationShare}`,
    undefined,
    MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT
  ) as Observable<IResponseEntityData<IListClassificationShare[]>>;

  // urlLookupCodeHinhThucKYLuat
  listLookupCodeHinhThucKYLuat$ = this.get(
    `${UrlConstant.END_POINT_MODEL_PLAN.urlLookupCodeHinhThucKYLuat}?lookupCode=HINHTHUC_KYLUAT`,
    undefined,
    MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT
  ) as Observable<IResponseEntityData<IListClassificationShare[]>>;

  // PostDisciplines
  PostDisciplines(params: Partial<ISetupSystem>): Observable<IResponseEntityData<Partial<ISetupSystem>>> {
    return this.post(
      `${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlSetUpSystem}`,
      params,
      undefined,
      MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT
    )
  };
  // PutDisciplines
  PutDisciplines(params: Partial<ISetupSystem>, id: number | undefined): Observable<IResponseEntityData<Partial<ISetupSystem>>> {
    return this.put(
      `${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlSetUpSystem}/${id}`,
      params,
      MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT
    )
  };

  // DeleteDisciplines
  DeleteDisciplines(id: number): Observable<IResponseEntityData<null>> {
    return this.delete(
      `${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlSetUpSystem}/${id}`,
      undefined,
      MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT
    )
  };

}
