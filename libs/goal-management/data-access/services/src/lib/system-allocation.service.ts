import {Injectable} from '@angular/core';
import {MICRO_SERVICE, SERVICE_NAME} from "@hcm-mfe/shared/common/constants";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {
  IClassificationDetailRateDTORow, IParamExceptional, IParamIndividualRate, IParamSave,
  IResponseEntityData,
  ItAssessmentPeriod
} from "@hcm-mfe/goal-management/data-access/models";
import {UrlConstant} from "./constant/url.class";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class SystemAllocationService extends BaseService {
  url: undefined | string;

  getAssessmentPeriod(isException?: boolean): Observable<IResponseEntityData<ItAssessmentPeriod[]>> {
    this.url = `${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointAssessmentPeriod}?${isException ? 'isException=true': ''}`;
    return this.get(this.url, { params: {isIgnoreStatusNotCurrent: true}}, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  getClassificationRate( periodId: number ) {
    this.url = `${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointSystemAllocation}`;
    return this.get(this.url, { params: {periodId: periodId} }, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  updateClassificationRate(params: {periodId: number, updateClassificationRateDetailDTOs: Pick<IClassificationDetailRateDTORow, 'clrId' | 'rate'>[]}) {
    this.url = `${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointSystemAllocation}`;
    return this.put(this.url, params, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT)
  }
  getListAllocatedException(params: {type: number}): Observable<any> {
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlPersonnelExceptionService}`,
      {params}, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
  deleteExceptionPersonnel(params: number[]) {
    return this.delete(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlPersonnelExceptionService}`,
      {body: params}, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
  createExceptionPersonnel(param: IParamExceptional[]) {
    return this.post(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlPersonnelExceptionService}`,
      param, undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
  getIndividualRate(params: IParamIndividualRate): Observable<any> {
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointSystemAllocation}/out-of-allocation`,
      {params}, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
  saveIndividualRate(params: IParamSave): Observable<any> {
    return this.put(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointSystemAllocation}/out-of-allocation`,
      params, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
}
