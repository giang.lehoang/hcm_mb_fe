import {Injectable} from '@angular/core';
import {MICRO_SERVICE, SERVICE_NAME} from "@hcm-mfe/shared/common/constants";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {ISetupAssessmentData} from "@hcm-mfe/goal-management/data-access/models";
import {UrlConstant} from "./constant/url.class";

@Injectable({
  providedIn: 'root'
})
export class TargetAssessmentService extends BaseService {

  public getListAssessmentCycle() {
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlLookupValues}?lcoName=LOAI_CHU_KY`,
      undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }


  public getListSetupAssessmentData(params?: ISetupAssessmentData | number | any) {
    let urlListAssessment = UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointPeriodConfig;
    if (typeof params === 'number') {
      urlListAssessment = `${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointPeriodConfig}/${params}`;
    }
    return this.get(urlListAssessment, {params}, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  public createSetupAssessmentData(params: any) {
    return this.post(UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointPeriodConfig, params,
      undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  public editSetupAssessmentData(params: any, id: number | undefined) {
    return this.put(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointPeriodConfig}/${id}`,
      params, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  public deleteSetupAssessmentItem(id: number) {
    return this.delete(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointPeriodConfig}/${id}`,
      undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  public getPositionsSetupAssessment(params: any) {
    return this.get(UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointPositionSearch,
      {params}, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  public getListJobTypeORG(param: any) {
    const urlEndPoint = `/${UrlConstant.HCM_ENDPOINT.urlEndPointProjectListJob}?jobType=${param}`;
    return this.get(urlEndPoint, undefined, MICRO_SERVICE.DEFAULT);
  }

  public getLazyLoadDataDropDownList(params: any, url: any) {
    params = this.toParams(params);
    return this.get(url,
      {params}, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  public getSetOfTargetIndicators(params: any) {
    return this.post(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointSetOfTarget}/search`, params,
      undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  public getUrlUploadFile(params: any, url: string | undefined, microService: any, endPoint: string, serviceName?: any, authority?: any) {
      // @ts-ignore
    return this.post(`${serviceName ? SERVICE_NAME[serviceName] + '/' : ''}${UrlConstant[endPoint][url]}/${authority ? authority + '/' : ''}import-excel`,
        params, undefined,  MICRO_SERVICE[microService]);
  }

  public getDowloadFileExel(url: any, endpoint: any, microService: any, serviceName?: any, param?: any, authority?: any) {
    if (param?.periodId) {
      // @ts-ignore
      return this.get(`${serviceName ? SERVICE_NAME[serviceName] + '/' : ''}${UrlConstant[endpoint][url]}/${authority ? authority + '/': ''}template`,
        {params: param, responseType: 'blob'}, MICRO_SERVICE[microService]);
    }
    // @ts-ignore
    return this.get(`${serviceName ? SERVICE_NAME[serviceName] + '/' : ''}${UrlConstant[endpoint][url]}/template${param ? '?' + param : ''}`,
      {responseType: 'blob'}, MICRO_SERVICE[microService]);
  }

  public deleteSetOfIndicator(id: number) {
    return this.delete(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointSetOfTarget}/${id}`,
      undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
}
