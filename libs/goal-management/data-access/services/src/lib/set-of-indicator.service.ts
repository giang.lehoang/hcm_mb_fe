
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";
import {UrlConstant} from "./constant/url.class";

@Injectable({
  providedIn: 'root',
})
export class SetOfIndicatorService extends BaseService {
  constructor(readonly http: HttpClient) {
    super(http);
  }

  getListSetOfIndicatorTree(): Observable<any> {
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlTarget}/parent-and-child`,
      undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  getListPositionUnassigned(): Observable<any> {
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointPositionSearch}/unassigned`,
      undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  createSetOfTarget(request: any): Observable<any> {
    return this.post(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointSetOfTarget}`, request,
      undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  updateSetOfTarget(id: number, request: any): Observable<any> {
    return this.put(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointSetOfTarget}/${id}`,
      request, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  getDetailOfTarget(id: number | null): Observable<any> {
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointSetOfTarget}/sot-detail/${id}`,
      undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  getListConfigAll(): Observable<any> {
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointPeriodConfig}/find-all`,
      undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  getDetailOfTargetHT(): Observable<any> {
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointSetOfTarget}/sot-detail`,
      undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  getTargetParent(params?: any): Observable<any> {
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlTarget}/parents`,
      { params: this.toParams(params) }, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  getTargetChild(params?: any): Observable<any> {
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlTarget}/target-with-child`,
      { params: this.toParams(params) }, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

}
