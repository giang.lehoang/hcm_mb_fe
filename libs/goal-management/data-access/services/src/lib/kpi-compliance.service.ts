import {Injectable} from "@angular/core";
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {UrlConstant} from "@hcm-mfe/goal-management/data-access/services";
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";
import {IParamKpiCompliance, IParamListOfIndicator} from "../../../models/src/lib/IKpiCompliance";

@Injectable({
  providedIn: 'root',
})
export class KpiComplianceService extends BaseService{
  public getListKPI(params: IParamKpiCompliance) {
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlTargetResult}/rate-point`,
      {params}, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
  public getListSetOfIndicator(params: any) {
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlTargetRacing}/employee/status`,
      {params}, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
  public getInfo() {
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEmployee}`,
      undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
  public exportExcel(params: any) {
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlTargetRacing}/employee/status/template`,
      {params: params, responseType: 'arraybuffer'}, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
  public getListCrmEmp(params: IParamListOfIndicator) {
    return this.post(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlCrmEmp}/search`,
      params, undefined, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
}
