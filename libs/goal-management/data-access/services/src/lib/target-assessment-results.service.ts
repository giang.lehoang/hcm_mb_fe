import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { environment } from '@hcm-mfe/shared/environment';
import {UrlConstant} from "@hcm-mfe/goal-management/data-access/services";
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";

@Injectable({
  providedIn: 'root',
})
export class TargetAssessmentResultsService extends BaseService {
  constructor(readonly http: HttpClient) {
    super(http);
  }

  updateStatus(data?: any, option?: any): Observable<any> {
    return this.post(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointSetOfTarget}/employee/approve-reject`, data,
      option, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
  approveEvaluateResult(data?: any, option?: any): Observable<any> {
    return this.post(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlTargetRacing}/employee/approve-reject`, data,
      option, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
  getDetails(params?: any): Observable<any> {
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlTargetRacing}/current-employee`,
      {params: this.toParams(params)}, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

  getPeriod(params?: any): Observable<any> {
    return this.get(`${UrlConstant.END_POINT_GOAL_MANAGEMENT.urlTargetRacing}/employee/period-employee`,
      {params: this.toParams(params)}, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }

}
