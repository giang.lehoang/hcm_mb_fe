import {IResponseContentEntity} from "./IResponse";

export type IParamSearchHandleClassification =  {
  page: number,
  size: number | undefined,
  periodId: number | '',
  dataId: number | '',
  apportion: number | '',
  flagStatus: number | ''
}

export type IClassificationList = {
  "cldId"?: number,
  "claId": number,
  "claType": string,
  "claCode": string,
  "maxPoint": number,
  "minPoint": number,
  "claPriority": string | null,
  "cldName": string,
  "flagStatus": number
}

export type ICurrentRatingDTOS = {
  "apportion": string,
  "currentRatingDetailDTOS": ICurrentRatingDetailDTOS[]
}

export type ICurrentRatingDetailDTOS = {
  "claCode": string,
  "numberEmpReal": number,
  "numberEmpRealApportion": number,
  "perNumberEmpReal": number,
  "perNumberEmpApportion": number,
  "isRed": boolean,
  rdeId: number,
  errorMess?: string | undefined
}

export type IChangeRatingDTOS = {
  "id": number,
  "idOrg": number,
  "requestChangeId": number,
  "orgName": string,
  "ratingOrg": string,
  "ratingLineCn": string,
  "district": string,
  "apportion": string,
  "currentRatingDetailDTOS": ICurrentRatingDetailDTOS[],
  "flagStatus": number,
  "pgrId": number,
  "disabled"?: boolean,
}

export interface IHandleClassification<IChangeRatingDTOS> {
  "totalEmp": number,
  "inApportion": string,
  "rating": string,
  "classificationList": IClassificationList[],
  "currentRatingDTOS": ICurrentRatingDTOS[],
  "changeRatingDTOS": IResponseContentEntity<IChangeRatingDTOS>
  "syntheticRequest": {
    "orgId": number,
    "periodId": number,
    "apportion": string,
    "flagStatus": number
  }
}

export interface ITargetResultSyntheticOrgLine {
  "dataId": number,
  "objId": number,
  "name": string,
  "orgGroup": string
}

export interface IParamStatusHandleClassification {
  "flagStatus": number | undefined,
  "reason": string,
  "rdeId": number[],
  "periodId": number | '',
  "dataId": number | '',
}

export interface IParamDownloadExelItem {
  "orgId": number,
  "pgrId": number | '',
  "periodId": number,
  "apportion": string
}

export type IParamAdjustedHandle = {
  "syntheticAdjustedDetails": syntheticAdjustedDetails[]
}

type syntheticAdjustedDetails = {
  "rdeId": number,
  "totalSuggest": number
}
export enum status {
  waitApproval = 0,
  approval = 1,
  reject = 2,
  rejectApproval= 3,
  Approved = 4,
  notSuggest = 5,
}

