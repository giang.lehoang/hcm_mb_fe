export interface IClassificationBlocksAndLines<T> {
  dataCode: string,
  dataName: string,
  isColumnTable: isColumnTableStatus | string,
  isEdit?: boolean,
  orgResults: T[],
}

export interface orgResults {
  dataId: number | null,
  dataCode: string,
  periodId: number | undefined,
  objId?: number,
  dataType: string,
  dataName: string,
  flagStatus: null | number,
  orgId?: number,
  pgrId?: number,
  customId?: string,
}

export enum isColumnTableStatus {
  Y = "Y",
  N = "N",
}

export interface IClassificationTablePopupBlocksLines {
  dataCode: string,
  dataId?: number,
  dataName: string,
  dataType: string,
  flagStatus: number,
  isRanking: boolean,
}

export type IListRegisterIdCheck = {
  dataId: number,
  dataType: string
}
