export interface IDataResponseCustomExtendSpecial<T> {
  data?: TargetRatingEmployeeDetailDTOS<T>;
  number: number;
  numberOfElements: number;
  size: number;
  totalElements: number;
  totalPages: number;
  pageable?: {
    pageNumber: number;
  };
}
interface TargetRatingEmployeeDetailDTOS<T> {
  targetRatingEmployeeDetailDTOS?: T;
  empCode: string;
  fullName: string;
  posName: string;
  flagStatus: number;
  periodName: string;
  periodYear: string;
}
export interface IEvaluateEmployeeResults {
  asmId: number;
  assignedType: string;
  nameAssign: string;
  resultTargetDTOs: Array<ResultTargetDTOs>;
  scaleNumSot: number;
  gaFromDate: string;
  gaToDate: string;
  gpFromDate: string;
  gpToDate: string;
  plan?: number;
  result?: number;
  point?: number;
  mergeArray?: Array<ResultTargetDTOs>;
}

interface ResultTargetDTOs {
  asdId: number;
  description: string;
  estimateNum: number;
  nameTarget: string;
  point: number;
  resultNum: number;
  scale: number;
  scoreCardName: string;
  scoringRule: string;
  stdId: number;
  stdParent: number;
  targetType: string;
  typeRule: string;
  subs?: Array<ResultTargetDTOs>;
}

export interface IEvaluateEmployeeResult {
  asdId: number;
  asmId: number;
  empId: number;
  empCode: string;
  fullName: string;
  posId: number;
  posName: string;
  scoreCard: string;
  tarId: number;
  tarName: string;
  targetNum: number;
  targetParentId: number;
  description: string;
  estimateNum: number;
  periodId: number;
  periodName: string;
  periodYear: string;
  resultNum: string;
  assignedType: string;
  point: number;
  scale: number;
  isRequired: boolean;
  scaleNumSot: number;
  scaleNumSotHT?: number;
  sotName: string,
  scoreCardName: string,
  targetTypeName?: string,
  nameTarget: string,
  typeRule: number,
  subs: Array<IEvaluateEmployeeResult>;
  sumHT?: number;
  sumQL?: number;
  total?: number;
  isEditAble: boolean;
}

export interface IResponse<T> {
  data: T;
}

export interface IParam {
  empCode: number,
  fullName: string,
  posId: number,
  periodYear: number,
  periodId: number,
  flagStatus: number,
  page: any,
  size: any,
  periodName?: string,
  jobId?: number,
  orgId?: number
}

export const statusEvaluateEmp = (): any[] => [
  {
    label: 'Hoàn tất phê duyệt giao',
    value: 14,
  },
  {
    label: 'Chưa gửi xác nhận KQ',
    value: 15,
  },
]
