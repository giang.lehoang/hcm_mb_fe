export interface Ijob {
  jobId: number;
  jobCode: string;
  jobName: string;
  flagStatus: number;
  createdBy: string;
  createdDate: string;
  lastUpdatedBy: string;
  lastUpdateDate: string;
  toDate: string;
  fromDate: string;
  jobType: string;
  jobDescription: string;
}

export interface Template {
  orgId: number;
  orgName: string;
  branchType: string;
  flagStatus: number;
  createdDate: string;
}
