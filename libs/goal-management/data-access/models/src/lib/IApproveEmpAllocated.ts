
export interface IParamApprove {
  employeeId?: number | string,
  flagStatus?: number | string,
  periodId?: number | string,
  page?: number,
  size?: number
}
export interface IParamEmp {
  empCode: string,
  periodId: string | number,
  apportion: string,
  page: number,
  size: number
}
export interface IApproveList {
  clcId: number,
  employeeCode: string,
  fullName: string,
  orgName: string,
  positionName: string,
  flagStatus: number,
  clcSuggest: string | number,
  clcChange: string | number,
  description: string,
  workingTime: string,
  title: string,
  orgManagement: string,
  apportion: string | number,
  checkChanges?: boolean
}
export interface IBodyRequest {
  periodId: number | undefined | string,
  clcIds: number[],
  flagStatus: number | string,
  reason: string
}
export const flagStatus = (): any[] => [
  {
    name: 'Đề xuất',
    value: 0
  },
  {
    name: 'Phê duyệt',
    value: 1
  },
  {
    name: 'Từ chối',
    value: 2
  }
];

export const typeAllocation = (): any[] => [
  {
    name: 'Ngoài phân bổ',
    value: 0
  },
  {
    name: 'Trong phân bổ',
    value: 1
  },
  {
    name: 'Tất cả',
    value: ""
  }
];
export const typeAllocationTable = (): any[] => [
  {
    name: 'Ngoài phân bổ',
    value: 0
  },
  {
    name: 'Trong phân bổ',
    value: 1
  },
];

