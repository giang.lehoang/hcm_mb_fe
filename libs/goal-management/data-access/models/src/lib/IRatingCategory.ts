export interface IRatingCategory {
  cldId: number,
  claId: number,
  claType: string,
  claCode: string,
  maxPoint: number | null,
  minPoint: number,
  claPriority: number,
  cldName: string,
  flagStatus: number
  duplicateName?: boolean,
  isValidPoint?: boolean,
  invalidMaxValue?: boolean
}
 export interface IBodyRequest {
   claType: string,
   cldName: string,
   minPoint: number | null,
   maxPoint: number | null,
   displaySeq: number,
   flagStatus: number
 }
export const flagStatus = (): any[] => [
  {
    name: 'Đang hoạt động',
    value: 1
  },
  {
    name: 'Không hoạt động',
    value: 0
  }
];
export const ratingType = (): any[] => [
  {
    name: 'Cá nhân',
    value: 'CN'
  },
  {
    name: 'Đơn vị',
    value: 'DV'
  }
];
