import { NzTableSortFn, NzTableSortOrder } from 'ng-zorro-antd/table';

export interface IAssignTargetToAllEmployees {
  dto: Dto[],
  periodsOfYear: IPeriodInfos[],
}

export interface IPeriodInfos
{
  asmId: number | null,
  asdId: number | null,
  periodId: number,
  periodName: string,
  estimateNum: number | null,
  isHavePeriod?: boolean,
  errMessage?: boolean, // add from client check error message when validate user
}

export interface Subs {
  description: string | null,
  periodInfos: IPeriodInfos[],
  scoreCard: string,
  tarId: number,
  tarName: string,
  targetParentId: number,
  targetScale: number | null,
  targetTypeName?: string
}

export interface Dto {
  asdId: number,
  asmId: number,
  empId: number,
  empCode: string,
  fullName: string,
  posId: number,
  posName: string,
  sotName: string,
  scoreCard: string,
  tarId: number,
  tarName: string,
  targetParentId: string,
  description: string | null,
  estimateNum: number,
  subs: Subs[] | null,
  periodInfos: IPeriodInfos[],
  targetScale: number,
  nameTarget: string,
  displaySeq?: number,
  targetTypeName?: string
}

export interface ColumnItem {
  name: string;
  sortOrder?: NzTableSortOrder | null;
  sortFn?: NzTableSortFn<Dto> | null;
  sortDirections?: NzTableSortOrder[] | null;
  nzWidth?: string;
  priority?: number;
}

export interface ISendEmployeeSub {
  empCode: null | string,
  fullName: null | string,
  requests: IRequest[]
}
export interface IRequest {
  asmId?: number | null,
  asdId: number | null,
  estimateNum: number | null,
  periodId?: number,
  periodName?: string,
  description: string | null,
}
