export interface IPersonnelExceptionRatings {
  "tgrId": number,
  "employeeCode": string,
  "fullName": string,
  "orgName": string,
  "jobName": string,
  "classificationName": string,
  "resultFinal": number
}


export type IParamPersonnelExceptionRating = {
  fullName?: string,
  employeeCode?: string,
  periodId?: number | null,
  orgId?: number | '',
  page: number,
  size: number
}

