export interface ResponseEntityDataList<T> {
  content?: T[],
  number: number,
  numberOfElements: number,
  totalElements?: number;
  totalPages?: number;
}
export interface ResponseEntityDataAssessment<T> {
  data: ResponseEntityDataList<T>;
  size?: number;
  pageable?: {
    pageNumber: number;
  };
}
export interface IAsssessmentPeriod {
  flagStatus: number,
  periodId: number,
  periodName: string,
  toDate: string,
  asmCloseDate: string,
  finishDate?: string
}
export interface IListOrg {
  "dataId": number,
  "objId": number,
  "name": string,
  "orgGroup": string
}
export interface ISetupAssessmentData {
  pcfId?: number;
  pcfName: string;
  pcfCode: string;
  cycleType: string;
  initialNum: number;
  fromDate: string;
  toDate: string | undefined;
  editableNum: number;
  finishNum: number;
  isLoopAble: string;
  flagStatus: number;
}

export interface ISetOfIndicatorsData {
  flagStatus?: number,
  jobId?: number | null;
  orgId?: number | null;
  posId?: number;
  posName?: string;
  sotCode?: string;
  sotId?: number;
  sotName?: string;
  pgrName?: string;
  psdName?: string;
  currentScaleNum?: number | string;
}

export interface ICycleAssessment {
  lvaId: number;
  lvaValue: string;
  lvaMean: string;
  description: string;
  flagStatus: number;
}

interface IAsssessmentSelect {
  value: number | string;
  label: string;
}
export interface IAsssessmentCustomSelect {
  value: string;
  label: string;
}
export const mDataSelectsAssessmentStatus = (): IAsssessmentSelect[] => [
  {
    value: 1,
    label: 'Hoạt động',
  },
  {
    value: 0,
    label: 'Không hoạt động',
  },
];

export const mbDataSelectsAssessmentIsLoopAble = (): IAsssessmentCustomSelect[] => [
  {
    value: '0',
    label: 'Không lặp lại',
  },
  {
    value: '1',
    label: 'Lặp lại theo năm',
  },
  {
    value: '2',
    label: 'Lặp lại theo kỳ',
  },
];

export const mbDataSelectsSub = (): IAsssessmentCustomSelect[] => [
  {
    label: 'Không sub',
    value: '0',
  },
  {
    label: 'Sub không tương đồng',
    value: '1',
  },
  {
    label: 'Sub tương đồng',
    value: '2',
  },
];

export const mbDataSelectTargetType = (): IAsssessmentCustomSelect[] => [
  {
    label: 'Xuôi',
    value: 'MY01',
  },
  {
    label: 'Ngược',
    value: 'MY02',
  },
];

export const mbDataSelectTargetShare = (): IAsssessmentCustomSelect[] => [
  {
    label: 'Không phải KPIs share',
    value: '0',
  },
  {
    label: 'KPIs Hệ thống',
    value: '1',
  },
  {
    label: 'KPIs Đơn vị',
    value: '2',
  },
];

export const mbDataSelectScoringRule = (): IAsssessmentCustomSelect[] => [
  {
    label: 'Tỷ lệ',
    value: 'SR01',
  },
  {
    label: 'Đạt-Không đạt',
    value: 'SR02',
  },
  {
    label: 'Kết hợp',
    value: 'SR03',
  },
]

export const THE_DIEM = 'THE_DIEM'
