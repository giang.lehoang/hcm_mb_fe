export interface DataTargetCategory {
  data: ChildTargetCategory;
}

export interface ChildTargetCategory {
  content: Array<TargetCategory>;
  totalElements: number;
}

export interface TargetCategory {
  flagStatus: number;
  idTargetParent: number;
  nameTargetParent: string;
  scoreCard: string;
  scoringRule: string;
  tarCode: string;
  tarId: number;
  tarName: string;
  targetShare: string;
  targetType: string;
  typeRule: string;
  parentId?: number;
  maxNum?: number;
}

export interface TargetParent {
  flagStatus: number;
  parentId: number;
  tarCode: string;
  tarId: string;
  tarName: string;
  targetType: string;
}

export interface DataDetailTargetCategory {
  data: TargetCategory;
}

export interface ObjectSearch {
  page: number;
  size: number;
  code?: string;
  name?: string;
  scoreCard?: string;
}
