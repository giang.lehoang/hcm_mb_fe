import { ISetOfIndicatorsData } from "./ISetupAssessment";

export interface IEmployData extends ISetOfIndicatorsData {
  asmId?: number,
  empId?: number,
  empCode?: string,
  fullName?: string,
  scaleNum?: number,
  periodYear?: number | null,
  asmIds?: Array<number>, // add on client when user Transform data list from BE
  sort?: string,
  managerId?: string,
  action?: string, // add on client when user check action approve and reject,
  content?: string, // add on client when user check action approve and reject for the message,
  index?: number | null, // add on client count number when compose data
  periodId?: string,
  assignedType?: string,
  periodName?: string,
  totalResultNum?: number,
  resultNum?: number,
  type?: string,
  isActive?: number,
  logic?: number | null,
  isRejected?: boolean,
  plusPoint?: number,
  minusPoint?: number,
  resultFinal?: number,
  asmCloseDate?: string,
  finishDate?: string,
  rowSpan?: number,
  periodActive?: boolean,
  invalid?: boolean,
  asmFromDate?: string,
  asmToDate?: string,
  size?: number
}

export interface OneEmpCodeMultipleEmpId {
  empCode?: string;
  arrEmpId: Array<number>;
}

export interface ResponseEntityEmployTarget<T> {
  data: ContentEmployee<T>;
  status: number;
  message: string;
  code: number;
}

export interface ResponseEntityObject<T> {
  data: T;
  status: number;
  message: string;
  code: number;
}

export interface ResponseError {
  error: MessageError
}

export interface MessageError {
  message: string
}
export interface ContentEmployee<T> {
  content: Array<T>;
  size?: number;
  totalElements?: number;
  totalPages?: number;
  numberOfElements?: number;
  number?: number;
  last?: boolean;
  first?: boolean;
  empty?: boolean;
  display?: boolean;
  pageable?: {
    pageNumber: number;
    pageSize: number;
    offset: number;
    paged: boolean;
    unpaged: boolean;
  };
}

export type listFlagStatus = {
  CHUA_GUI_XAC_NHAN_GIAO: number,
  CHO_XAC_NHAN_GIAO: number,
  CHO_PHE_DUYET_GIAO: number,
  HOAN_TAT_PHE_DUYET_GIAO: number,
  CHUA_GUI_XAC_NHAN_KQ: number,
  CHO_XAC_NHAN_KQ: number,
  CHO_PHE_DUYET_KQ: number,
  HOAN_TAT_PHE_DUYET_KQ: number,
  CHUA_GAN_BO_CHI_TIEU: number
}

interface ISOT {
  sotId: number,
  sotName:string
}
export  interface ILISTUNASSIGN {
  empId: number,
  empCode: string,
  fullName: string,
  posName:string,
  setOfTargetList: Array<ISOT>
}
export const processFlagStatus = (): listFlagStatus => {
  return {
    CHUA_GUI_XAC_NHAN_GIAO: 11,
    CHO_XAC_NHAN_GIAO: 12,
    CHO_PHE_DUYET_GIAO: 13,
    HOAN_TAT_PHE_DUYET_GIAO: 14,
    CHUA_GUI_XAC_NHAN_KQ: 15,
    CHO_XAC_NHAN_KQ: 16,
    CHO_PHE_DUYET_KQ: 17,
    HOAN_TAT_PHE_DUYET_KQ: 18,
    CHUA_GAN_BO_CHI_TIEU: 10
  }
}
export interface IObject {
  label: string,
  value: number | string
}
export const typePropose = (): any[] => [
  {
    name: 'Ngoài tỷ lệ phân bổ',
    value: true
  },
  {
    name: 'Trong tỷ lệ phân bổ',
    value: false
  },
  {
    name: 'Tất cả',
    value: ""
  }
];
export const listFlagStatus = (): IObject[] => [
  {
    label: 'Chưa gán bộ chỉ tiêu',
    value: 10
  },
  {
    label: 'Chưa gửi xác nhận giao',
    value: 11
  },
  {
    label: 'Chờ xác nhận giao',
    value: 12
  },
  {
    label: 'Chờ phê duyệt giao',
    value: 13
  },
  {
    label: 'Hoàn tất phê duyệt giao',
    value: 14
  },
  {
    label: 'Chưa gửi xác nhận kết quả',
    value: 15
  },
  {
    label: 'Chờ xác nhận kết quả',
    value: 16
  },
  {
    label: 'Chờ phê duyệt kết quả',
    value: 17
  },
  {
    label: 'Hoàn tất phê duyệt kết quả',
    value: 18
  }
]

export const getActionRejectorAprrove = (): {
  reject: string,
  approve: string
} => {
  return {
    reject: "REJECT",
    approve: "APPROVE",
  }
}

export interface IDataResponseCustom<T> {
  data?: T;
  number: number;
  numberOfElements: number;
  size: number;
  totalElements: number;
  totalPages: number;
  pageable?: {
    pageNumber: number;
  };
}
export function renderFlagStatus(flagStatus: number) {
  switch(flagStatus) {
    case processFlagStatus().CHUA_GAN_BO_CHI_TIEU:
      return 'targerManager.common.notification.haventAssignedBCT'
    case processFlagStatus().CHUA_GUI_XAC_NHAN_GIAO:
      return 'targerManager.common.notification.tranctionHasnotbeenSent'
    case processFlagStatus().CHO_XAC_NHAN_GIAO:
      return 'targerManager.common.notification.waitingForDeliveryConfirmation'
    case processFlagStatus().CHO_PHE_DUYET_GIAO:
      return 'targerManager.common.notification.waitingForDeliveryApproval'
    case processFlagStatus().HOAN_TAT_PHE_DUYET_GIAO:
      return 'targerManager.common.notification.completeTheDeliveryApproval'
    case processFlagStatus().CHUA_GUI_XAC_NHAN_KQ:
      return 'targerManager.common.notification.haventSentConfirmationOfKQ'
    case processFlagStatus().CHO_XAC_NHAN_KQ:
      return 'targerManager.common.notification.waitingForConfirmationOfKQ'
    case processFlagStatus().CHO_PHE_DUYET_KQ:
      return 'targerManager.common.notification.waitingForResultsApproval'
    case processFlagStatus().HOAN_TAT_PHE_DUYET_KQ:
      return 'targerManager.common.notification.finalizationOfResultsApproval'
    default:
      return ''
  }
}
