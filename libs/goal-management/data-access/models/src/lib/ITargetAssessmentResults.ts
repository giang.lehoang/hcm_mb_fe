
export interface DataSource {
    empId?: number;
    asmId: number;
    sotId?: number;
    sotName?: string;
    flagStatus?: number;
    scaleNum?: number;
    scaleNumSot?: number;
    posName?: string | null;
    asmIds: Array<number>;
    reason?: string;
    periodId?: number;
    periodName?: string;
    score: number;
    assignedType?: string;
    scoreByScale?: number;
    ratioDate?: number;
    plusPoint?: number | null;
    minusPoint?: number | null;
    resultFinal?: number;
    ratingBefore?: string | null;
    ratingAfter?: string | null;
    resultNum?: number;
    fromDate?: string;
    toDate?: string;
}

export interface DataSourceDetails {
    empCode?: string;
    flagStatus?: number;
    fullName?: string;
    periodName?: string;
    periodYear?: string;
    posName?: string;
    targetRatingEmployeeDetailDTOS: Array<SetOfIndicators>;
    manageName?: string;
    directEmp?: string;
    directJobName?: string;
    indirectEmp?: string;
    indirectJobName?: string;
    totalPoint?: number;
    xpoint?: number;
}

export interface SetOfIndicators {
    asmId?: number;
    assignedType?: string;
    nameAssign?: string;
    scaleNumSot?: number;
    resultTargetDTOs: Array<Targets>;
    gaFromDate?: Date,
    gaToDate?: Date,
    gpFromDate?: Date,
    gpToDate?: Date,
    point?: number
}

export interface Targets {
    asdId?: number;
    description?: string;
    estimateNum?: number;
    nameTarget?: string;
    resultNum?: number;
    scale?: number;
    point: number | null;
    scoreCardName?: string;
    stdId?: number;
    stdParent?: number;
    typeRule: number | string;
    targetType?: string;
    scoringRule?: string;
    no: number | null;
    nameAssign: string | undefined | null;
    isParent?: boolean;
}

export interface CurrentUser {
    email?: string;
    empCode?: string;
    fullName?: string;
}

export interface ParamSearch {
    size?: number;
    page?: number;
    currentYear?: string | null;
    periodYear?: number | string | null;
    periodId?: number;
    asmIds?: Array<number>;
    isGetResult?: number;
    assignedType?: string | null;
}

export interface Period {
    periodId?: number;
    periodName?: string;
    toDate?: Date;
    flagStatus?: number;
}

export const FlagStatus = {
    CHUA_GUI_XAC_NHAN_GIAO: 11,
    CHO_XAC_NHAN_GIAO: 12,
    CHO_PHE_DUYET_GIAO: 13,
    HOAN_TAT_PHE_DUYET_GIAO: 14,
    CHUA_GUI_XAC_NHAN_KET_QUA: 15,
    CHO_XAC_NHAN_KET_QUA: 16,
    CHO_PHE_DUYET_KET_QUA: 17,
    HOAN_TAT_PHE_DUYET_KET_QUA: 18
};
