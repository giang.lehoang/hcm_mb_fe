export interface ListBallanceScoreCardManagement {
  lvaId: number;
  lvaValue: string;
  lvaMean: string;
  flagStatus: number;
  description: string;
  lcoId?: number;
  displaySeq: number;
}

export type paramSetupPosition = {
  jobId: string,
  orgId: string,
}
