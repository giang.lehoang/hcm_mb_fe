export interface IInfoOrg {
  "orgName": string,
  "superOrgName": string,
  "district": string,
  "branchType": string,
  "orgId": number,
}

export interface IParam {
  "orgId": number | string | undefined,
  "periodId": number | string | undefined,
  apportion: number | string | undefined,
  page: number,
  size: number
}

export interface IListEmpAllocated {
  employeeCode: string,
  fullName: string,
  lineName: string,
  positionName: string,
  apportion: number | string,
  description: string,
  check?: boolean,
  workingTime: string,
  isRequired: boolean,
  flagStatus: number
}

