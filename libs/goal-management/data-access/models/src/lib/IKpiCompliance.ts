export interface IKpiComplianceList {
  "tgrId": number,
  "employeeCode": string,
  "fullName": string,
  "unitName": string,
  "positionName": string,
  "plusPoint": number,
  "minusPoint": number
}
export interface IListOfIndicator {
  "empCode": string,
  "fullName": string,
  "sotName": string,
  "posName": string,
  "manageName": string,
  "orgName": string,
  "flagStatus": number,
  "periodName": string,
  "year": string
}
export interface IParamListOfIndicator {
  empCode?: string,
  orgId: string | number,
  periodId?: string | number,
  flagStatus?: string | number,
  isLockDate?: boolean,
  page: number,
  size: number
}
export interface IParamKpiCompliance {
  employeeCode: string,
  periodId: string,
  orgId: number | string,
  page: number,
  size: number
}
export interface IParamDetailClassification {
  employeeId?: string,
  apportion: string | number,
  dataId: string | number,
  claCode: string,
  isPropose: string,
  periodId: string | number,
  page: number,
  size: number
}

export interface IDetailClassification {
  "indexColumn": number,
  "employeeCode": string,
  "empName": string,
  "orgName": string,
  "posName": string,
  "jobName": string,
  "joinCompanyDate": string,
  "resultNum": number | null,
  "minusPoint": number,
  "resultFinal": string | number | null,
  "ratingInApportion": string,
  "ratingOutApportion": string,
  "clcId": number,
  "lineName": string,
  "isOutRateApportion": boolean
}

