
export interface IListAllocationLineBranchSelect {
  orgId: number,
  orgName: string,
}

export type IParamSearchAllocationLineBranch = {orgId: number | string, periodId: number | undefined }
