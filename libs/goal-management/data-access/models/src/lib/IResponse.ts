export interface ResponseEntity<T> {
  status: number;
  data?: Array<T>;
  content?: Array<T>;
  size?: number;
  totalElements?: number;
  totalPages?: number;
  numberOfElements?: number;
  number?: number;
  pageable?: {
    pageNumber: number;
  };
}

export type IResponseContentEntity<A> = {
  "content": Array<A>,
  "pageable": {
    "sort": {
      "empty": boolean,
      "unsorted": boolean,
      "sorted": boolean
    },
    "offset": number,
    "pageNumber": number,
    "pageSize": number,
    "unpaged": boolean,
    "paged": boolean
  },
  "last": boolean,
  "totalElements": number,
  "totalPages": number,
  "size": number,
  "number": boolean,
  "sort": {
    "empty": boolean,
    "unsorted": boolean,
    "sorted": boolean
  },
  "numberOfElements": number,
  "first": boolean,
  "empty": boolean
}


export interface IResponseEntityData<T> {
  "timestamp": string,
  "clientMessageId": string,
  "transactionId": string,
  "code": number,
  "message": string,
  "path": string
  "status": number,
  "data": T
}


