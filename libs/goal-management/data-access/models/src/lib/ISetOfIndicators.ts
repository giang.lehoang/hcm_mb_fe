import { IAsssessmentCustomSelect } from './ISetupAssessment';

export interface PositionUnassign {
  assignee: string;
  posCode: string;
  posId: number;
  posName: string;
}


export interface ListPeriodConfig {
  id: number;
  name: string;
  pcfId?: number;
}

export const mbDataSelectsAssignType = (): IAsssessmentCustomSelect[] => [
  {
    label: 'Quản lý trực tiếp',
    value: 'QL',
  },
  {
    label: 'Mục tiêu bổ trợ',
    value: 'HT',
  },
];


