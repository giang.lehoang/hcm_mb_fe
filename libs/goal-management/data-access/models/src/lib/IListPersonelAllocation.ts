import {isColumnTableStatus} from "@hcm-mfe/goal-management/data-access/models";

export interface IListPersonnelAllocationDetail {
  "orgId": number | undefined | string,
  "orgName": string,
  "orgManagerName": string,
  "region": string,
  "branchType": string,
  "countOfClassification": string
}

export interface  IListPersonnelAllocationListTable<T> {
  check?: boolean;
  lastElement?: boolean;
  "columnCode": string,
  "columnName": string,
  isColumnTable: isColumnTableStatus | string,
  totalOfColumn: number | null,
  minPoint: number | null,
  maxPoint: number | null,
  empOfAllocations: T[],
  isLock: number,
  active?: boolean,
  isRed?: boolean,
  laneLength?: number
}

export type IEmpOfAllocations = {
  id?: number | null,
  empId?: number | null,
  empCode: string,
  fullName: string,
  ratingBefore: string | null,
  ratingAfter: string | null,
  columnCode: string | null,
  columnName: string | null,
  periodId: number | null,
  color: string | null,
  resultNum: number | null,
  customId?: string,
  maxClassificationForDecpline?: string,
  isException: string,
  active?: boolean,
}
export interface IResultFilter {
  periodId: string,
  empCode: string,
  beforeColumnCode: string,
  afterColumnCode: string,

}
export interface IParamPersonnel {
  orgId?: string | number,
  periodId: string | number,
  objId: string | null,
  inAllocation: boolean,
  orgGroup?: string
}
