export interface ISetupSystem {
  id?: number,
  type: string;
  typeName: string;
  maxRanking: string;
  maxRankingName: string;
  flagStatus: number;
  fromDate: string;
  toDate: string;
  note: string,
  oldType?: string,
  oldFromDate?: string,
  oldFlagStatus?: number
}

export interface IParamSetupSystem {
  page: number,
  size: number,
  periodId: number | ''
}

export interface IListClassificationShare {value: string, meaning: string}

