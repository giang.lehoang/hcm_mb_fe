export interface ItAssessmentPeriod {
  flagStatus: number;
  periodId: number;
  periodName: string;
  toDate: string;
}

export interface ISystemAllocation {
  classificationRateDTOS: classificationRateDTOS[];
  isEdit: boolean;
  periodId: number;
}

export interface classificationRateDTOS {
  cldOrgId: number;
  cldOrgName: string;
  claPriority: number;
  classificationDetailRateDTORow: IClassificationDetailRateDTORow[];
  errMess: string | undefined;
}

export interface IClassificationDetailRateDTORow {
  clrId: number;
  claPerId: number;
  claPerName: string;
  rate: number | null;
  claPriority: number;
  errorMess?: string;
}

export interface IParamExceptional {
  aleType: number,
  flagStatus: number,
  employeeId?: string,
  jobId?: string
}
export interface IParamIndividualRate {
  periodId: number | string;
}
export interface IIndividualRate {
  clrId?: number,
  claPerId?: number,
  claPerName?: string,
  rate?: number | string | null,
  claPriority?: number,
  claCode?: string
}
export interface IParamSave {
  periodId: number | string,
  updateClassificationRateDetailDTOs: IIndividualRate[]
}

