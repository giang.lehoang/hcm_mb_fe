export interface AsmDetailDTOSCUSTOM {
  sotId: number;
  stdId: number;
  tarId: number;
  tarName: string;
  estimateNum: number;
  periodId: number;
  periodName: string;
  scoreCard: string;
  asdId: number;
  asmId: number;
  description: string;
  empId: number;
  empCode: string;
  flagStatus: number;
  fullName: string;
  periodYear: string;
  positionName: string;
  disabled?: boolean;
  asmIds?: Array<number>;
  managerId?: number;
  isDirectManager?: boolean;
  scale: number;
  parentId?: number;
}

export interface ITargetPlanStaff {
  asdId: number;
  fullName?: string;
  positionName?:string;
  asmId: number;
  description: string;
  empId: number;
  estimateNum: number;
  parentId?: number;
  periodId: number;
  periodName: string;
  periodYear: string;
  flagStatus?: number;
  scale: number;
  scoreCard: string;
  sotId: number;
  stdId: number;
  tarId: number;
  tarName: string;
  typeRuleName?: string;
  targetTypeName?: string;
  subs?: Array<ITargetPlanStaff>;
  empCode: string;
  error?: string;
  disabled?: boolean;
  asmIds?: Array<number>;
  arrayTarget?: Array<string>;
  gaFlagStatus?: number;
  scoreCardName?: string
  isDirectManager?:boolean
  asmDetailDTOS? : Array<AsmDetailDTOSCUSTOM>;
  managerId?: number,
  managerName?: string,
  displaySeq?: number,
}
