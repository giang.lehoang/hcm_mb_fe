import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {StatusHandleClassification} from "./validate/show-status-handle-classification.pipe";
import {TransformFormatPercent} from "./validate/Transform-format-percent.pipe";

@NgModule({
  imports: [CommonModule],
  declarations: [
    StatusHandleClassification,
    TransformFormatPercent
  ],
  exports: [
    StatusHandleClassification,
    TransformFormatPercent
  ],
})
export class GoalManagementHelperModule {}
