import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'transformFormatPercent'})
export class TransformFormatPercent implements PipeTransform {
  transform(number: number | null, arg: string, type: string): string {
    let pre = ( number !== null && number % 1 === 0 ) ? number : number === null ? null : Number(number)?.toFixed(1);
    if(pre === 'NaN'){
      pre = 0;
    }
    switch (type) {
      case 'percent':
        if (pre !== undefined && pre !== null && arg === 'prev') {
          return `${pre}%`
        } else if ((pre == undefined || pre !== null) && arg === 'prev') {
          return '0'
        } else if (pre !== undefined && pre !== null && arg === 'curr') {
          return `/${pre}%`
        }
        return ``;
      case 'notPercent':
        if (pre !== undefined && pre !== null && arg === 'prev') {
          return `${pre}`
        } else if ((pre == undefined || pre !== null) && arg === 'prev') {
          return '0'
        } else if (pre !== undefined && pre !== null && arg === 'curr') {
          return `/${pre}`
        }
        return ``;
    }
    return '';

  }
}
