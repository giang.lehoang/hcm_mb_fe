import {AbstractControl, ValidatorFn} from "@angular/forms";
import {IEmployData} from "@hcm-mfe/goal-management/data-access/models";

const transformdataDemcimal = (arr: IEmployData[]): IEmployData[] => arr.map(el => ({
  ...el,
  scaleNum: Number(el.scaleNum) * 100,
  // @ts-ignore
  currentScaleNum: +(el?.currentScaleNum * 100).toFixed()
}));


export const transFormDataGroupByAsmId = (dataListEmploy: IEmployData[]): IEmployData[] => {
  // transfrom data, groupBy data with list keys: empCode and posId and add new propertykey asmIds
  if (dataListEmploy.length === 0) {
    return [];
  }

  const code: string[] = [];
  const totalEmpCode: string[] = [];
  const tempResult: IEmployData[] = [];

  transformdataDemcimal(dataListEmploy).forEach((el) => {
    if (!code.includes(`${el.empCode}-${el.posId}-${el.asmFromDate}-${el.asmToDate}`)) {
      code.push(`${el.empCode}-${el.posId}-${el.asmFromDate}-${el.asmToDate}`);
      el.asmIds = [];
      el.asmId != null && el.asmIds.push(el.asmId);
      tempResult.push(el);
      el.empCode != null && totalEmpCode.push(el.empCode);
    } else {
      el.asmId != null && tempResult?.find((e: IEmployData) => e.posId === el.posId && e.empCode === el.empCode)
        ?.asmIds?.push(
          el.asmId
        );
    }
  });

  for (const item of tempResult) {
    const empCodeNotDuplicate = totalEmpCode.filter(el => el === item.empCode);
    if (empCodeNotDuplicate.length === 1 && empCodeNotDuplicate[0] && item.currentScaleNum === null) {
      item.scaleNum = 100;
      item.currentScaleNum = 100;
    }
    if(item.currentScaleNum === 0 || item.currentScaleNum === null){
      item.currentScaleNum = '0';
    }
  }

  return tempResult;
}

export const numberDecimalValidator = (isNotDecimal: string): ValidatorFn => {
  const numRegex = /^-?\d*[.,]?\d{0,2}$/;
  return (formGroup: AbstractControl): { [key: string]: boolean } | null => {
    const fromDateVal = formGroup.value;
    const regexDecimal = new RegExp(numRegex);
    if (fromDateVal !== '' && fromDateVal !== null && !regexDecimal.test(fromDateVal)) {
      return {[isNotDecimal]: true};
    }
    return null;
  };
}

export const getTotalScaleNumberEmployTarget = (
  inputText: string,
  scaleNumberTotalNotValid: string,
  item: IEmployData,
  listData: IEmployData[],
  index: number
): ValidatorFn => {
  return (formGroup: AbstractControl): { [key: string]: boolean } | null => {
    const fromDateVal = formGroup.value;
    let sumScale = 0;
    for (let i = 0; i < listData.length; i++) {
      if (i === index) {
        listData[i].scaleNum = fromDateVal;
      }
      if (item.empCode === listData[i].empCode) {
        sumScale += Number(listData[i].scaleNum);
      }
    }
    if (fromDateVal !== '' && fromDateVal !== null && !isNaN(fromDateVal) && (sumScale > 100 || sumScale !== 100)) {
      return {[scaleNumberTotalNotValid]: true};
    }
    return null;
  };
}

