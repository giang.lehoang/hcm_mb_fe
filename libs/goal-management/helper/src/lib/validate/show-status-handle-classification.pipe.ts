import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'statusHandleClassification'})
export class StatusHandleClassification implements PipeTransform {
  transform(status: number ): { text: string, class: string} {
    switch (status) {
      case 0:
        return {
          text: 'Chờ xác nhận',
          class: 'tag-custom ant-tag-magenta-custom',
        };
      case 1:
        return {
          text: 'Đồng ý',
          class: 'tag-custom ant-tag-green-custom',
        };
      case 2:
        return {
          text: 'Từ chối',
          class: 'tag-custom ant-tag-orange-custom',
        };
      case 3:
        return {
          text: 'Từ chối duyệt',
          class: 'tag-custom ant-tag-red-custom',
        };
      case 4:
        return {
          text: 'Đã phê duyệt',
          class: 'tag-custom ant-tag-green-custom',
        };
      case 5:
        return {
          text: 'Không đề xuất',
          class: 'tag-custom ant-tag-orange-custom',
        };
      default:
        return {
          text: 'Tạo mới',
          class: 'tag-custom ant-tag-orange-custom',
        };
    }
  }
}
