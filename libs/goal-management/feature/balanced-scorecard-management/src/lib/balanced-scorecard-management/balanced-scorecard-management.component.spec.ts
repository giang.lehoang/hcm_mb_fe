import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BalancedScorecardManagementComponent } from './balanced-scorecard-management.component';

describe('BalancedScorecardManagementComponent', () => {
  let component: BalancedScorecardManagementComponent;
  let fixture: ComponentFixture<BalancedScorecardManagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BalancedScorecardManagementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BalancedScorecardManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
