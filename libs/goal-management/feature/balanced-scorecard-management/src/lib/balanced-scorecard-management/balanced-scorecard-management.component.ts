import { Component, Injector, OnInit, TemplateRef } from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {
  ListBallanceScoreCardManagement,
  ResponseEntityEmployTarget
} from "@hcm-mfe/goal-management/data-access/models";
import {IAsssessmentSelect, mbDataSelectsAssessmentStatus} from "@hcm-mfe/system/data-access/models";
import {BallancedScoreCardService} from "@hcm-mfe/goal-management/data-access/services";
import {CustomValidators} from "@hcm-mfe/shared/common/validators";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {HTTP_STATUS_CODE, Mode} from "@hcm-mfe/shared/common/constants";
import {FormBalancedScorecardManagementComponent} from "@hcm-mfe/goal-management/ui/form-input";

@Component({
  selector: 'app-balanced-scorecard-management',
  templateUrl: './balanced-scorecard-management.component.html',
  styleUrls: ['./balanced-scorecard-management.component.scss'],
})
export class BalancedScorecardManagementComponent extends BaseComponent implements OnInit {
  isLoadingPage = false;
  isEdit = false;
  listBallanceScoreCardManagement: Array<ListBallanceScoreCardManagement> | undefined;
  ballanceScoreCardServiceForm: FormGroup;

  listStatus: IAsssessmentSelect[] = mbDataSelectsAssessmentStatus();
  isSubmitted = false;
  isVisible = false;
  id: number | undefined;
  modalRef: NzModalRef | undefined;
  private readonly subs: Subscription[] = [];

  constructor(
    injector: Injector,
    private readonly ballancedScoreCardService: BallancedScoreCardService,
  ) {
    super(injector);
    this.ballanceScoreCardServiceForm = new FormGroup({
      lvaValue: new FormControl('', [Validators.required, Validators.maxLength(50)]),
      lvaMean: new FormControl('', [Validators.required, Validators.maxLength(200)]),
      description: new FormControl('', [Validators.maxLength(250)]),
      flagStatus: new FormControl(1, [Validators.required]),
      displaySeq: new FormControl('', [Validators.required, Validators.min(0), CustomValidators.onlyNumber]),
    });

    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.BALANCED_SCORECARD_MANAGEMENT}`);
  }

  getDataBallancedGoal(){
    this.isLoadingPage = true;
    const getAll = this.ballancedScoreCardService.getListBallancedGoal().subscribe(
      (data) => {
        this.isLoadingPage = false;
        this.listBallanceScoreCardManagement = data.data;
      },
      (error: any) => {
        this.isLoadingPage = false;
        this.toastrCustom.error(error.error.message)
      }
    );
    this.subs.push(getAll);
  }

  ngOnInit(): void {
    this.getDataBallancedGoal();
  }

  showModalBalanced(footerTmpl: TemplateRef<any>, mode: string, data?: ListBallanceScoreCardManagement) {
    this.modalRef = this.modal.create({
      nzTitle: 'Thẻ điểm cân bằng',
      nzWidth: this.getInnerWidth(1.5),
      nzContent: FormBalancedScorecardManagementComponent,
      nzComponentParams: {
        mode: mode === 'ADD' ? Mode.ADD : Mode.EDIT,
        data: data,
      },
      nzFooter: footerTmpl,
    });
    this.modalRef.afterClose.subscribe((result) => (result?.refresh ? this.getDataBallancedGoal() : ''));
  }

  getInnerWidth(point: number): number {
    if (window.innerWidth > 767) {
      return window.innerWidth / point > 1100 ? 1100 : window.innerWidth / point;
    }
    return window.innerWidth;
  }

  trackFnTable(index: number, item: ListBallanceScoreCardManagement){
    return item.lvaId;
  }

  showConfirm(id: number, event: Event): void {
    event.stopPropagation();
    this.deletePopup.showModal(() => this.deleteBalancedScorecardItem(id));
  }

  deleteBalancedScorecardItem(id: number){
     if(this.objFunction?.delete){
      this.isLoadingPage = true;
      const deleteItemBallanced = this.ballancedScoreCardService.deleteBallancedScorecard(id).subscribe(
        (res:ResponseEntityEmployTarget<null> ) => {
          if(res.status === HTTP_STATUS_CODE.OK){
            this.getDataBallancedGoal();
            this.toastrCustom.success('Xóa bản ghi thành công')
          }
        },
        (error) => {
          this.isLoadingPage = false;
          this.toastrCustom.error( error?.message)
        }
      );
      this.subs.push(deleteItemBallanced);
     }
  }


  override ngOnDestroy() {
    this.modalRef?.destroy();
    this.subs.forEach((s) => s.unsubscribe());
  }
}
