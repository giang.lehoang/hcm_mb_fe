import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  BalancedScorecardManagementComponent
} from "./balanced-scorecard-management/balanced-scorecard-management.component";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbButtonIconModule} from "@hcm-mfe/shared/ui/mb-button-icon";
import {NzTableModule} from "ng-zorro-antd/table";
import {TranslateModule} from "@ngx-translate/core";
import {NzPageHeaderModule} from "ng-zorro-antd/page-header";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzTagModule} from "ng-zorro-antd/tag";
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [
    CommonModule,
    SharedUiLoadingModule,
    SharedUiMbButtonIconModule,
    NzTableModule,
    TranslateModule,
    NzPageHeaderModule,
    SharedUiMbButtonModule,
    NzTagModule,
    RouterModule.forChild([
      {
        path: '',
        component: BalancedScorecardManagementComponent,
      },
    ]),
  ],
  declarations: [
    BalancedScorecardManagementComponent
  ],
  exports: [
    BalancedScorecardManagementComponent
  ]
})
export class GoalManagementFeatureBalancedScorecardManagementModule {}
