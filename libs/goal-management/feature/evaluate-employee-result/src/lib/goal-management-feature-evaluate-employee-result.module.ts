import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {CommonModule, DecimalPipe} from '@angular/common';
import {EvaluateEmployeeResultComponent} from "./evaluate-employee-result/evaluate-employee-result.component";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {GoalManagementUiFormInputModule} from "@hcm-mfe/goal-management/ui/form-input";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {NzModalModule} from "ng-zorro-antd/modal";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {TranslateModule} from "@ngx-translate/core";
import {RouterModule} from "@angular/router";
import {NzIconModule} from "ng-zorro-antd/icon";
import {SharedPipesConvertNumbertoString0Module} from "@hcm-mfe/shared/pipes/convert-numberto-string0";
import {SharedDirectivesNumbericModule} from "@hcm-mfe/shared/directives/numberic";

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule,
    SharedUiMbButtonModule, GoalManagementUiFormInputModule,
    NzTableModule, NzToolTipModule,
    SharedUiMbInputTextModule, NzModalModule,
    FormsModule, ReactiveFormsModule, TranslateModule,
    RouterModule.forChild([
      {
        path: '',
        component: EvaluateEmployeeResultComponent,
      },
    ]), NzIconModule, SharedPipesConvertNumbertoString0Module, SharedDirectivesNumbericModule],
  declarations: [EvaluateEmployeeResultComponent],
  exports: [EvaluateEmployeeResultComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class GoalManagementFeatureEvaluateEmployeeResultModule {}
