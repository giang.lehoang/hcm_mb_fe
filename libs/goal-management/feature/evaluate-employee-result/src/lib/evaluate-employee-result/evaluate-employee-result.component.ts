import { Component, Injector, OnInit } from '@angular/core';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {
  ColumnItem,
  Dto,
  IEvaluateEmployeeResult,
  IParam, IResponse,
  mbDataSelectsAssignType, statusEvaluateEmp
} from "@hcm-mfe/goal-management/data-access/models";
import {
  EvaluateEmployeeResultsService,
  EvaluetionApprovalSetOfIndicatorService
} from "@hcm-mfe/goal-management/data-access/services";
import * as moment from 'moment';
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {scoringRule, targetType, typeRule} from "@hcm-mfe/shared/common/constants";
import {isNull} from "lodash";

@Component({
  selector: 'app-evaluate-employee-result',
  templateUrl: './evaluate-employee-result.component.html',
  styleUrls: ['./evaluate-employee-result.component.scss'],
  // providers: [TargetAssessmentService, EmployeeTargetService],
})
export class EvaluateEmployeeResultComponent extends BaseComponent implements OnInit {
  isVisible: boolean | undefined;
  isRequired: boolean | undefined;
  isInvalid: boolean | undefined;
  periodName: string | undefined;
  listAssingType = mbDataSelectsAssignType();
  listStatus = statusEvaluateEmp();
  InitColumnHeaderTable: ColumnItem[] = [
    {
      name: this.translate.instant('STT'),
      sortDirections: null,
      nzWidth: '4.5em'
    },
    {
      name: this.translate.instant('goaManagement.evaluateEmpResult.table.empCode'),
      sortDirections: null,
    },
    {
      name: this.translate.instant('goaManagement.evaluateEmpResult.table.fullName'),
      sortOrder: null,
      sortFn: (a: Dto, b: Dto) => a.fullName.localeCompare(b.fullName),
      sortDirections: ['ascend', 'descend'],
    },
    {
      name: this.translate.instant('goaManagement.evaluateEmpResult.table.position'),
      sortDirections: null,
    },
    {
      name: this.translate.instant('goaManagement.evaluateEmpResult.table.setOfIndicators'),
      sortDirections: null,
    },
    {
      name: this.translate.instant('goaManagement.evaluateEmpResult.table.assign'),
      sortDirections: null,
    },
    {
      name: this.translate.instant('goaManagement.evaluateEmpResult.table.scoreCard'),
      sortDirections: null,
      nzWidth: '10em',
    },
    {
      name: this.translate.instant('goaManagement.evaluateEmpResult.table.nameIndicator'),
      sortOrder: null,
      sortFn: (a: Dto, b: Dto) => a?.nameTarget.localeCompare(b?.nameTarget),
      sortDirections: ['ascend', 'descend', null],
      nzWidth: '25%'
    },
    {
      name: this.translate.instant('goaManagement.assign-target-to-all-employees.label.table.typeKpi'),
      sortDirections: null,
      nzWidth: '10em',
    },
    {
      name: this.translate.instant('goaManagement.evaluateEmpResult.table.density'),
      sortDirections: null,
      nzWidth: '5%'
    },
    {
      name: '',
      sortDirections: null,
    },
    {
      name: this.translate.instant('goaManagement.evaluateEmpResult.table.result'),
      sortDirections: null,
    },
    {
      name: this.translate.instant('goaManagement.evaluateEmpResult.table.score'),
      sortDirections: null,
      nzWidth: '5%'
    },
    {
      name: this.translate.instant('goaManagement.evaluateEmpResult.table.description'),
      sortDirections: null,
    },
  ];
  param: IParam | undefined;
  listDataModal: IEvaluateEmployeeResult[] = [];
  listData: IEvaluateEmployeeResult[] = [];
  constructor(
    private readonly evaluetionApprovalSetOfIndicatorService: EvaluetionApprovalSetOfIndicatorService,
    readonly evaluateEmployeeResultService: EvaluateEmployeeResultsService,
    injector: Injector
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.EVALUATE_EMPLOYEE_RESULT}`);
    console.log(this.objFunction);
  }
  ngOnInit(): void {
  }

  initData() {
    this.evaluateEmployeeResultService.getListRatingResultEmp(this.param).subscribe(
      (data: IResponse<IEvaluateEmployeeResult[]>) => {
        this.listData = data.data;
        this.isLoading = false;
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }
  minusDay(d1: any, d2: any): number | null {
    if (!d1 || !d2) {
      return null;
    }
    const date1 = moment(d1, 'YYYY-MM-DD').toDate();
    const date2 = moment(d2, 'YYYY-MM-DD').toDate();
    const ms1 = date1.getTime();
    const ms2 = date2.getTime();
    const temp = (ms1 - ms2) / (24 * 60 * 60 * 1000);
    return Math.ceil(temp) + 1;
  }

  buildRequest(listData: any, bodyRequest: any, isSendEmployee: any) {
    listData.forEach((el: any) => {
      const objTemp: any = {};
      objTemp['asdId'] = el.asdId;
      objTemp['asmId'] = el.asmId;
      objTemp['resultNum'] = el.resultNum;
      objTemp['description'] = el.description;
      if (isSendEmployee) {
        objTemp['empCode'] = el.empCode;
        objTemp['fullName'] = el.fullName;
        objTemp['periodId'] = el.periodId;
        objTemp['periodName'] = el.periodName;
        objTemp['scale'] = el.scale;
      }
      bodyRequest.push(objTemp);
      if (el?.subs) {
        this.buildRequest(el?.subs, bodyRequest, isSendEmployee);
      }
    });
  }
  save() {
    const cloneDataEmployeeAssign = [...this.listData];
    const tempArrEmployee: any = [];
    this.buildRequest(cloneDataEmployeeAssign, tempArrEmployee, false);
    this.isLoading = true;
    this.evaluateEmployeeResultService.saveRatingResultEmp(tempArrEmployee).subscribe(
      (res) => {
        this.isLoading = false;
        this.toastrCustom.success(this.translate.instant('common.notification.success'));
        this.router.navigateByUrl('/goal-management/periodic-goal-management/evaluetion-approval-set-of-indicator');
      },
      (err) => {
        this.isLoading = false;
        this.toastrCustom.error(`${this.translate.instant('common.notification.error')}${err.message ? (': ' + err.message) : ''}`);
      }
    );
  }
  sum(subs: any, data?: any): number {
    const total = subs.map((el: any) => el.point).reduce((sum: any, cur: any) => sum + +cur, 0);
    if (data) {
      data.point = total;
      if(!isNull(data.maxNum)){
         data.point = Number(data.point) >= Number(data.maxNum) * Number(data.scale)
          ? Number(data.maxNum) *  Number(data.scale)
          : data.point;
        return data.point;
      }
    }
    return total;
  }

  buildArray() {
    const map: { [key: number]: any[] } = {};
    const cloneData = [...this.listData];
    cloneData.forEach(el => {
      if(map[el.empId]) {
        map[el.empId].push(el)
      } else {
        map[el.empId] = [el];
      }
    })

    for (const property in map) {
      const scaleNumSotHT = map[property].find( ht => ht.assignedType === this.listAssingType[1].value).scaleNumSot;
      map[property].forEach(el => {
        if (el.assignedType === this.listAssingType[0].value) {
          el['scaleNumSotHT'] = scaleNumSotHT;
        }
      })
    }
    this.listDataModal = cloneData.filter((cur, index) => {
      return (
        this.listData.findIndex((item) => item.empId === cur.empId && item.posId === cur.posId && item.assignedType === this.listAssingType[0].value) === index
      );
    });

    this.listDataModal.forEach((element) => {
      if (element.scaleNumSotHT) {
        element['sumHT'] = this.sum(map[element.empId].filter(el => el.assignedType === this.listAssingType[1].value)) * element.scaleNumSotHT;
        element['sumQL'] = this.sum(map[element.empId].filter(el => el.assignedType === this.listAssingType[0].value)) * (1 - element.scaleNumSotHT);
      }
    });
  }
  caculateTotalPointModal(data: any){
    // @ts-ignore
    return data.scaleNumSot * (data.sumHT + data.sumQL);
  }
  showModal(isVisible: boolean): void {
    this.isVisible = isVisible;
    this.buildArray();
    this.listDataModal.forEach(element => {
      element['total'] = this.caculateTotalPointModal(element);
    })
  }
  stopPopupEvent(event: any) {
    event.stopPropagation();
  }
  receiveDataEmployee(event: any) {
    this.isLoading = true;
    this.periodName = event.periodName;
    this.param = { ...event };
    if(this.param) {
      this.param.page = null;
      this.param.size = null;
      delete this.param['periodName'];
      delete this.param['jobId'];
      delete this.param['orgId'];
    }
    this.initData();
  }

  addValidateInput(listData: any) {
    listData.forEach((el: any) => {
      if (el.scale === 0 || el.scale === null || (el.scale !== 0 && el.resultNum)) {
        el.isRequired = false;
      } else {
        el.isRequired = true;
      }
      if (el.resultNum === 0) {
        el.isRequired = false;
      }
      if (el.subs) {
        el.isRequired = false;
        this.addValidateInput(el.subs);
      }
    });
  }
  checkValidate(listData: any) {
    let arr = [...listData];
    listData.forEach((el: any) => {
      if(el.subs?.length) {
        arr = arr.concat(el.subs);
      }
    })
    if(arr.some(item => item.isRequired)){
      this.isInvalid = true;
    } else {
      this.isInvalid = false;
    }
  }
  sendEmployee() {
    if (!this.listData.length) {
      return;
    }
    const cloneDataEmployeeAssign = [...this.listData];
    const tempArrEmployee: any = [];
    this.addValidateInput(this.listData);
    this.checkValidate(this.listData);
    if (this.isInvalid) {
      this.toastrCustom.error(this.translate.instant('common.notification.requiredResult'));
      return;
    }
    this.buildRequest(cloneDataEmployeeAssign, tempArrEmployee, true);
    this.isLoading = true;
    this.evaluateEmployeeResultService.sendRatingResultEmp(tempArrEmployee).subscribe(
      (res) => {
        this.isLoading = false;
        this.toastrCustom.success(
          this.translate.instant(
            'goaManagement.evaluation-approval-set-of-indicator.notification.evaluationOfSetSuccessIndicators'
          )
        );
        this.router.navigateByUrl('/goal-management/periodic-goal-management/evaluetion-approval-set-of-indicator');
      },
      (err) => {
        this.isLoading = false;
        this.toastrCustom.error(`${this.translate.instant('common.notification.error')}${err.message ? (': ' + err.message) : ''}`);
      }
    );
  }

  checkTargetType(data: any) {
    if (data.targetType === targetType.right) {
      if (+data.resultNum === 0) {
        data.point = 0;
        return;
      }
      if (data.estimateNum === 0) {
        data.point = data.scale * 100;
        return;
      }
      const compare = data.resultNum < data.estimateNum;
      this.checkScoringRuleAndCaculate(data, compare);
    } else {
      if (+data.resultNum === 0) {
        data.point = data.scale * 100;
        return;
      }
      const compare = data.resultNum > data.estimateNum;
      this.checkScoringRuleAndCaculate(data, compare);
    }
  }
  checkScoringRuleAndCaculate(data: any, compare: any): void {
    const firstFormula = ((data.scale * +data.resultNum) / data.estimateNum) * 100;
    const secondFormula = data.scale * 100;
    const thirdFormula = ((data.scale * data.estimateNum) / +data.resultNum) * 100;
    if (!data.resultNum || data.scale === 0 || !data.estimateNum) {
      data.point = 0;
      return;
    }
    switch (data.scoringRule) {
      case scoringRule.rate:
        if (data.targetType === targetType.right) {
          data.point = firstFormula;
        } else {
          data.point = thirdFormula;
        }
        break;
      case scoringRule.passOrNot:
        if (compare) {
          data.point = 0;
          break;
        } else {
          data.point = secondFormula;
          break;
        }
      case scoringRule.combine:
        if (compare) {
          data.point = 0;
          break;
        } else {
          data.point = firstFormula;
          break;
        }
    }
    // condition check maxNum
    if(!isNull(data.maxNum)){
      data.point = Number(data.point) >= Number(data.maxNum) * Number(data.scale) ? Number(data.maxNum) * Number(data.scale) : data.point;
    }
  }
  // @ts-ignore
  renderType(type: any) {
    if (type === this.listAssingType[0].value) {
      return this.listAssingType[0].label
    }
    if (type === this.listAssingType[1].value) {
      return this.listAssingType[1].label
    }
  }
  caculateResult(event: any, data: any): void {
    if (event === 0 || event) {
      data.resultNum = +event;
    } else {
      data.resultNum = "";
    }
    if (+data.typeRule === typeRule.noSub || +data.typeRule === typeRule.similarSub) {
      data.isRequired = false;
      this.checkTargetType(data);
    } else {
      data.point = null
      return;
    }
  }

  roundNumber(num: any) {
    if (num === null) {
      return 0;
    }
    return Math.round(num * 100) / 100;
  }
}
