import {Component, Injector, OnDestroy, OnInit, TemplateRef, ViewChild} from "@angular/core";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {SystemAllocationService} from "@hcm-mfe/goal-management/data-access/services";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";

@Component({
  selector: 'app-exceptional-personnel',
  templateUrl: './exceptional-personnel.component.html',
  styleUrls: ['./exceptional-personnel.component.scss']
})
export class ExceptionalPersonnelComponent extends  BaseComponent implements OnInit, OnDestroy {
  @ViewChild('actionTmpl', {static: true}) action!: TemplateRef<NzSafeAny>;
  tableConfig: MBTableConfig = new MBTableConfig();
  pagination = new Pagination();
  infoEmp = {
    fullName: '',
    employeeId: ''
  }
  functionCode = FunctionCode.SYSTEM_ALLOCATION;
  listData = [];
  heightTable = { x: '80vw', y: '50vh'};
  params = {
    type: 2,
    page: 0,
    size: this.pagination.pageSize
  }
  constructor(injector: Injector, private systemAllocationService: SystemAllocationService) {
    super(injector);
  }
  ngOnInit(): void {
    this.isLoading = true;
    this.initTable()
    this.getListExceptionPersonnel();
  }
  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'goaManagement.listEmpAllocated.table.empCode',
          field: 'employeeCode',
          width: 100,
        },
        {
          title: 'goaManagement.listEmpAllocated.table.fullName',
          field: 'employeeName',
          fixed: window.innerWidth > 1024,
          width: 120
        },
        {
          title: 'goaManagement.listEmpAllocated.table.orgName',
          field: 'organizationName',
          needEllipsis: true,
          fixed: window.innerWidth > 1024,
          width: 200
        },
        {
          title: 'goaManagement.listEmpAllocated.table.job',
          field: 'employeeJobName',
          fixed: window.innerWidth > 1024,
          width: 200
        },
        {
          title: '',
          tdTemplate: this.action,
          field: 'aleId',
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          width: 60,
          fixedDir: 'right',
          fixed: window.innerWidth > 1024,
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1,
      showFrontPagination: false
    };
  }
  changePage(pageNumber: any) {
    this.isLoading = true;
    this.pagination.pageNumber = pageNumber - 1 ;
    this.params.page = pageNumber - 1;
    this.getListExceptionPersonnel();
  }
  getListExceptionPersonnel() {
    this.systemAllocationService.getListAllocatedException(this.params).subscribe(res => {
      this.listData = res.data.content;
      this.tableConfig.total = res.data.totalElements;
      this.isLoading = false;
    }, () => {
      this.isLoading = false;
    })
  }
  showDeleteConfirm(id: number, $event: Event): void {
    $event.stopPropagation();
    this.deletePopup.showModal(() => this.deleteAllowanceItem(id));
  }
  onChange(event: any) {
    this.infoEmp = {...event};
  }
  create() {
    if (!this.infoEmp.employeeId) {
      return;
    }
    const params = [{
      aleType: 2,
      employeeId: this.infoEmp.employeeId,
      flagStatus: 1
    }]
    this.isLoading = true;
    this.systemAllocationService.createExceptionPersonnel(params).subscribe(res => {
      this.isLoading = false;
      this.toastrCustom.success(this.translate.instant('common.notification.success'));
      this.getListExceptionPersonnel();
    }, (err) => {
      this.isLoading = false;
      if (err?.status === 400 && err?.message) {
        this.toastrCustom.error(err.message);
      } else {
        this.toastrCustom.error(this.translate.instant('common.notification.error'));
      }
    })
  }
  deleteAllowanceItem(id: number){
    this.isLoading = true;
    const body = [id]
    this.systemAllocationService.deleteExceptionPersonnel(body).subscribe(
      ()=>{
        this.toastrCustom.success(this.translate.instant('common.notification.success'));
        this.getListExceptionPersonnel();
        this.isLoading = false;
      }, (err: any) => {
        this.isLoading = false;
        if (err?.status === 400 && err?.message) {
          this.toastrCustom.error(err.message);
        } else {
          this.toastrCustom.error(this.translate.instant('common.notification.error'));
        }
      }
    )
  }
}
