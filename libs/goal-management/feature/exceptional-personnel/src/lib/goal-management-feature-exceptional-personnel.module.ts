import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {ExceptionalPersonnelComponent} from "./exceptional-personnel/exceptional-personnel.component";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/employee-data-picker";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {NzGridModule} from "ng-zorro-antd/grid";
import {TranslateModule} from "@ngx-translate/core";
import {FormsModule} from "@angular/forms";

@NgModule({
  imports: [CommonModule, SharedUiMbButtonModule, SharedUiMbTableWrapModule, SharedUiMbTableModule, SharedUiLoadingModule,
    SharedUiEmployeeDataPickerModule, SharedUiMbInputTextModule, NzGridModule, TranslateModule, FormsModule],
  exports: [ExceptionalPersonnelComponent],
  declarations: [ExceptionalPersonnelComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class GoalManagementFeatureExceptionalPersonnelModule {}
