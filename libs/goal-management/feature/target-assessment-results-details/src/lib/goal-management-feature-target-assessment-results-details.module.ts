import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TargetAssessmentResultsDetailsComponent} from "./target-assessment-results-details/target-assessment-results-details.component";
import {RouterModule} from "@angular/router";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedDirectivesUppercaseInputModule} from "@hcm-mfe/shared/directives/uppercase-input";
import {SharedDirectivesTrimInputModule} from "@hcm-mfe/shared/directives/trim-input";
import {SharedUiMbButtonIconModule} from "@hcm-mfe/shared/ui/mb-button-icon";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedPipesFormatScoreModule} from "@hcm-mfe/shared/pipes/format-score";
import {NzFormModule} from "ng-zorro-antd/form";
import {NzInputModule} from "ng-zorro-antd/input";
import {NzModalModule} from "ng-zorro-antd/modal";
import {NzTagModule} from "ng-zorro-antd/tag";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import {NzTableModule} from "ng-zorro-antd/table";
import {TranslateModule} from "@ngx-translate/core";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

const COMMON = [
  SharedUiMbButtonModule,
  SharedUiLoadingModule,
  SharedUiMbInputTextModule,
  SharedDirectivesUppercaseInputModule,
  SharedDirectivesTrimInputModule,
  SharedUiMbButtonIconModule,
  SharedUiMbDatePickerModule,
  SharedUiMbSelectModule,
  SharedPipesFormatScoreModule,
];
const ANT_DESIGN = [
  NzFormModule,
  NzInputModule,
  NzModalModule,
  NzTagModule,
  NzPaginationModule,
  NzTableModule,
];
@NgModule({
  imports: [CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: TargetAssessmentResultsDetailsComponent,
      },
    ]),
    ...ANT_DESIGN,
    ...COMMON,
    TranslateModule],
  declarations: [TargetAssessmentResultsDetailsComponent],
  exports: [TargetAssessmentResultsDetailsComponent],
})
export class GoalManagementFeatureTargetAssessmentResultsDetailsModule {
}
