import {
  Component,
  Injector,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { NgForm } from '@angular/forms';
import * as moment from 'moment';
import { ResponseEntityObject } from '@hcm-mfe/goal-management/data-access/models';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';


import { Pageable } from '@hcm-mfe/shared/data-access/models';
import { userConfig } from '@hcm-mfe/shared/common/constants';
import {
  CurrentUser,
  DataSource, DataSourceDetails,
  FlagStatus,
  ParamSearch, SetOfIndicators, Targets
} from "../../../../../data-access/models/src/lib/ITargetAssessmentResults";
import {TargetAssessmentResultsService} from "../../../../../data-access/services/src/lib/target-assessment-results.service";
import {PersonalTargetListService} from "../../../../../data-access/services/src/lib/personal-target-list.service";

@Component({
  selector: 'app-target-assessment-results-details',
  templateUrl: './target-assessment-results-details.component.html',
  styleUrls: ['./target-assessment-results-details.component.scss'],
})
export class TargetAssessmentResultsDetailsComponent
  extends BaseComponent
  implements OnInit, OnDestroy
{
  @ViewChild('formSearch', { static: false }) formSearch: NgForm = new NgForm(
    [],
    []
  );
  @ViewChild('formReject', { static: false }) formReject: NgForm = new NgForm(
    [],
    []
  );

  public subs: Subscription[] = [];
  isSubmitted = false;
  pageableData: Pageable | undefined;
  limit: number = userConfig.pageSize;
  paramSearch: ParamSearch = {
    asmIds: [],
  };
  dataSourceTemp: any[] | undefined;
  currentUser: CurrentUser = {
    empCode: '',
    fullName: '',
  };
  listStatus = FlagStatus;
  dataReject: DataSource = {asmId: 0, asmIds: [], score: 0};
  dataObject: DataSourceDetails | undefined;
  avgTotalPoint = 0;
  flagStatus: number | undefined;
  SetOfIndicatorHT: SetOfIndicators | undefined =  undefined;
  SetOfIndicatorQL: SetOfIndicators | undefined =  undefined;


  constructor(
    injector: Injector,
    private readonly targetAssessmentResultsService: TargetAssessmentResultsService,
    private readonly personalTargetListService: PersonalTargetListService
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.getDataFromNavigate();
    this.getList();
    this.personalTargetListService
      .getEmployee()
      .subscribe((res: ResponseEntityObject<CurrentUser>) => {
        if (res && res.data) {
          this.currentUser.empCode = res.data.empCode;
          this.currentUser.fullName = res.data.fullName;
        }
      });
  }

  getDataFromNavigate() {
    const data = window.history.state.data;
    if (!data) {
      this.goToList();
    } else {
      this.paramSearch.asmIds = data.asmIds;
      this.flagStatus = data.flagStatus;
    }
  }

  goToList() {
    this.back();
  }

  getList(): void {
    this.isLoading = true;
    this.dataSourceTemp = [];
    const ListTargetCategoryData = this.targetAssessmentResultsService
      .getDetails(this.paramSearch)
      .subscribe(
        (res: ResponseEntityObject<DataSourceDetails>) => {
          if (res) {
            this.dataObject = res.data;
            this.dataSourceTemp = this.convertData(
              this.dataObject.targetRatingEmployeeDetailDTOS, this.dataObject.totalPoint
            );
            this.SetOfIndicatorHT = this.dataObject?.targetRatingEmployeeDetailDTOS?.find(el => el.assignedType === 'HT');
            this.SetOfIndicatorQL = this.dataObject?.targetRatingEmployeeDetailDTOS?.find(el => el.assignedType === 'QL');


            this.avgTotalPoint = res.data?.xpoint;
            this.limit = this.dataSourceTemp.length;
            this.pageableData = {
              totalElements: this.dataSourceTemp.length,
              totalPages: 1,
              currentPage: 1,
              numberOfElements: this.dataSourceTemp.length,
              size: this.limit,
            };
          }
          this.isLoading = false;
        },
        (error) => {
          this.toastrCustom.error(error?.error?.message ? error?.error?.message : error?.message);
          this.isLoading = false;
        }
      );
    this.subs.push(ListTargetCategoryData);
  }

  sortDescNameAssign(a: any, b: any) {
    if (a.nameAssign > b.nameAssign) {
      return -1;
    }
    if (a.nameAssign < b.nameAssign) {
      return 1;
    }
    return 0;
  }

  minusDay(d1: any, d2: any): number | null {
    if (!d1 || !d2) {
      return null;
    }
    const date1 = moment(d1, 'YYYY-MM-DD').toDate();
    const date2 = moment(d2, 'YYYY-MM-DD').toDate();
    const ms1 = date1.getTime();
    const ms2 = date2.getTime();
    const temp = (ms1 - ms2) / (24 * 60 * 60 * 1000);
    return Math.ceil(temp);
  }

  calculateRatioDate(sotQL: SetOfIndicators | undefined) {
    let result: number | null = null;
    if (sotQL) {
      const ga = this.minusDay(sotQL.gaToDate, sotQL.gaFromDate);
      const gp = this.minusDay(sotQL.gpToDate, sotQL.gpFromDate);
      if (ga && gp) {
        result = ga / gp;
      }
    }
    return result;
  }

  convertData(list: Array<SetOfIndicators>, total?: number) {
    const sotQL: SetOfIndicators | undefined = list.find(
      (m) => m.assignedType === 'QL'
    );
    const ratioDate: null | number = this.calculateRatioDate(sotQL); // (GOA_ASSIGNMENTS.TO_DATE - GOA_ASSIGNMENTS.FROM_DATE)/(GOA_PERIODS.TO_DATE - GOA_PERIODS.FROM_DATE)

    const sotHT: SetOfIndicators | undefined = list.find(
      (m) => m.assignedType === 'HT'
    );
    const scaleHT: number | undefined = sotHT ? sotHT.scaleNumSot : 0;

    list.sort(this.sortDescNameAssign);

    const arr = [];
    for (let i = 0; i < list.length; i++) {
      let totalPoint = 0;
      const listParent = list[i].resultTargetDTOs.filter((m) => !m.stdParent);
      this.getArrayOrderByDisplaySeqAndScale(listParent);
      for (let j = 0; j < listParent.length; j++) {
        let parent: Targets = listParent[j];

        parent = { ...parent };
        parent.no = j === 0 ? i + 1 : null;
        (parent.nameAssign = j === 0 ? list[i].nameAssign : null),
          (parent.isParent = true);
        arr.push(parent);

        const listChild: Array<Targets> = list[i].resultTargetDTOs.filter(
          (m) => m.stdParent === parent.stdId
        );
        this.getArrayOrderByDisplaySeqAndScale(listChild);
        this.convertData2(arr, parent, listChild);

        totalPoint += parent.point ? parent.point : 0;
      }
      arr.push({ type: 'sum', totalPoint: totalPoint, point: list[i].point, scaleNumSot: list[i].scaleNumSot, assignedType: list[i].assignedType});
    }
    return arr;
  }

  convertData2(arr: any, parent: Targets, listChild: Array<Targets>) {
    if(listChild.length > 0){
      for (const child of listChild) {

        child.point = this.calculateScore(child);

        const dataTempChild = { ...child };
        arr.push(dataTempChild);
      }
    }
    if (listChild.length === 0) {
      parent.point = this.calculateScore(parent);
    }
  }

  calculateScore(data: Targets) {
    let result: number | null = 0;
    result = data?.point === null  ? 0 : data?.point;
    return result;
  }

  search(): void {
    this.getList();
  }

  changePage(event: number): void {
    if (event) {
      this.paramSearch.page = event - 1;
      this.getList();
    }
  }

  goBack() {
    this.location.back();
  }

  override ngOnDestroy(): void {
    this.subs.forEach((s) => s.unsubscribe());
  }

  updateStatus(isReject?: boolean) {
    this.isLoading = true;
    const dataUpdate = {
      asmIds: this.paramSearch.asmIds,
      status: isReject ? this.listStatus.CHUA_GUI_XAC_NHAN_KET_QUA : this.listStatus.CHO_PHE_DUYET_KET_QUA,
      reason: this.dataReject?.reason,
    };
    this.targetAssessmentResultsService.approveEvaluateResult(dataUpdate).subscribe(
      (response: ResponseEntityObject<number>) => {
        if (response) {
          this.toastrCustom.success(response.message);
          this.goToList();
        }
        this.isLoading = false;
      },
      (error) => {
        this.isLoading = false;
        this.toastrCustom.error(
          error.error.message ? error.error.message : error.error.error
        );
      }
    );
  }

  reject(): void {
    this.isSubmitted = true;
    if (this.formReject.valid) {
      this.updateStatus(true);
    }
  }

  private getArrayOrderByDisplaySeqAndScale(arrTemp: any[]) {
    // @ts-ignore
    return arrTemp.sort((a: any, b: any) => {
      if (a.displaySeq && b.displaySeq) {
        if (a.displaySeq > b.displaySeq) {
          return 1;
        } else if (a.displaySeq === b.displaySeq) {
          if (a.scale > b.scale) {
            return -1;
          } else return 1;
        }
        return -1;
      }
    });
  }
}
