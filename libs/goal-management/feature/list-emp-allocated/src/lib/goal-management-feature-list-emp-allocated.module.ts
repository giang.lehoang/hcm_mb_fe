import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ListEmpAllocatedComponent} from "./list-emp-allocated/list-emp-allocated.component";
import {RouterModule} from "@angular/router";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {TranslateModule} from "@ngx-translate/core";
import {NzCardModule} from "ng-zorro-antd/card";
import {NzGridModule} from "ng-zorro-antd/grid";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {FormsModule} from "@angular/forms";
import {NzTableModule} from "ng-zorro-antd/table";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {GoalManagementUiFormInputModule} from "@hcm-mfe/goal-management/ui/form-input";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";

@NgModule({
    imports: [CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: ListEmpAllocatedComponent,
            },
        ]), SharedUiLoadingModule, SharedUiMbButtonModule, TranslateModule, NzCardModule, NzGridModule,
        SharedUiMbSelectModule, FormsModule, NzTableModule, SharedUiMbInputTextModule, NzPaginationModule, NzIconModule, NzToolTipModule],
  exports: [ListEmpAllocatedComponent],
  declarations: [ListEmpAllocatedComponent]
})
export class GoalManagementFeatureListEmpAllocatedModule {}
