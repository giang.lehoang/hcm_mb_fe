import {Component, Injector, OnDestroy, OnInit, TemplateRef} from "@angular/core";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {userConfig} from "@hcm-mfe/shared/common/constants";
import {NzModalRef} from "ng-zorro-antd/modal";
import {ListEmpAllocatedService} from "../../../../../data-access/services/src/lib/list-emp-allocated";
import {IInfoOrg, IListEmpAllocated, IParam} from "../../../../../data-access/models/src/lib/IListEmpAllocated";
import {typeAllocation, typeAllocationTable} from "../../../../../data-access/models/src/lib/IApproveEmpAllocated";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";

@Component({
  selector: 'app-list-emp-allocated',
  templateUrl: './list-emp-allocated.component.html',
  styleUrls: ['./list-emp-allocated.component.scss']
})
export class ListEmpAllocatedComponent extends  BaseComponent implements OnInit, OnDestroy {
  modalRef: NzModalRef | undefined;
  isSubmit = false;
  isLock = false;
  pastPeriod = false;
  amountInAllocation = 0;
  totalEmp = 0;
  infoOrg: IInfoOrg | undefined;
  listData: IListEmpAllocated[] = [];
  params: IParam = {
    orgId: "",
    periodId: "",
    apportion: "",
    page: 0,
    size: userConfig.pageSize
  }
  paginationCustom = {
    size: 0,
    total: 0,
    pageIndex: 0,
    numberOfElements: 0
  };
  listDepartmentLine: any = [];
  listIOAllocation = typeAllocation();
  listIOAllocationTable = typeAllocationTable();
  listPeriod: any = [];
  constructor (injector : Injector, readonly listEmpAllocatedService: ListEmpAllocatedService) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.LIST_EMP_ALLOCATED}`);
    console.log(this.objFunction)
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.listEmpAllocatedService.getPeriod().subscribe(res => {
      this.listPeriod = res.data;
      if (this.listPeriod.length) {
        this.params.periodId = this.listPeriod[0].periodId;
        this.getConfig();
        this.getListEmpAllocated();
      }
    }, () => {
      this.isLoading = false;
    });
    this.listEmpAllocatedService.getOrg().subscribe(res => {
      const all: any = {
        orgId: '',
        name: 'Tất cả',
      }
      this.listDepartmentLine = res.data;
      this.listDepartmentLine.push(all);
    });
  }
  getInnerWidth(point: number): number {
    if (window.innerWidth > 767) {
      return window.innerWidth / point > 1100 ? 1100 : window.innerWidth / point;
    }
    return window.innerWidth;
  }
  getConfig() {
    const params = {
      periodId: this.params.periodId
    }
    this.listEmpAllocatedService.getConfig(params).subscribe(res => {
      this.isLock = res.data;
    });
  }
  changePeriod(value: number){
    const item = this.listPeriod.find((el: any) => el.periodId === value);
    this.pastPeriod = !!item?.flagStatus;
  }
  changeAllocation(data: any) {
    data.check = !data.check;
  }
  offerEmp() {
    if (!this.listData.length) {
      return;
    }
    const tempArr: any = [];
    this.listData.forEach(el => {
      if(el.check) {
        el.isRequired = !el.description;
        const tempObject = {
          employeeCode: el.employeeCode,
          apportion: el.apportion + "",
          description: el.description
        }
        tempArr.push(tempObject);
      }
    });
    const isValid = this.listData.some(el => el.isRequired);
    this.isSubmit = true;
    if (!tempArr.length) {
      return;
    }
    if (isValid) {
      this.toastrCustom.error(this.translate.instant("goaManagement.listEmpAllocated.validate.required"));
      return;
    }
    const requestBody = {
      periodId: this.params.periodId,
      changeDetails: tempArr
    }
    this.isLoading = true;
    this.listEmpAllocatedService.offerEmpAllocated(requestBody).subscribe(res => {
      this.isLoading = false;
      this.toastrCustom.success(this.translate.instant("common.notification.success"));
      this.getListEmpAllocated();
    }, (err) => {
      this.isLoading = false;
      if(err.code === 500) {
        this.toastrCustom.error(this.translate.instant("common.notification.error"));
      } else {
        this.toastrCustom.error(err.message);
      }
    })
  }
  changePage(event: any) {
    this.params.page = event - 1;
    this.getListEmpAllocated();
  }
  getListEmpAllocated() {
    this.listEmpAllocatedService.getListEmpAllocated(this.params).subscribe(res => {
      this.infoOrg = {...res.data};
      this.listData = res.data.classificationChangesDetails.content;
      this.listData.forEach((el: IListEmpAllocated) => {
        el.apportion = +el.apportion;
        el.check = false;
      })
      this.paginationCustom.size = res.data.classificationChangesDetails.size;
      this.paginationCustom.total = res.data.classificationChangesDetails.totalElements;
      this.paginationCustom.pageIndex =res.data.classificationChangesDetails.pageable.pageNumber;
      this.paginationCustom.numberOfElements = res.data.classificationChangesDetails.numberOfElements;
      this.amountInAllocation = res.data.empInApportion;
      this.totalEmp = res.data.totalEmp;
      this.isLoading = false;
    }, () => {
      this.isLoading = false;
    })
  }
  search() {
    this.isLoading = true;
    this.params.page = 0;
    this.getListEmpAllocated();
  }
  override triggerSearchEvent(): void {
    this.getListEmpAllocated();
  }
}
