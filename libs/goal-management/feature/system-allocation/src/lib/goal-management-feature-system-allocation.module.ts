import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SystemAllocationComponent } from './system-allocation/system-allocation.component';
import {RouterModule} from "@angular/router";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NzGridModule} from "ng-zorro-antd/grid";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {TranslateModule} from "@ngx-translate/core";
import {NzTableModule} from "ng-zorro-antd/table";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {NzTabsModule} from "ng-zorro-antd/tabs";
import {SharedUiMbCollapseModule} from "@hcm-mfe/shared/ui/mb-collapse";

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: SystemAllocationComponent,
            },
        ]),
        ReactiveFormsModule,
        NzGridModule,
        SharedUiMbSelectModule,
        SharedUiMbButtonModule,
        TranslateModule,
        NzTableModule,
        SharedUiMbInputTextModule,
        FormsModule,
        SharedUiLoadingModule,
        NzTabsModule,
        SharedUiMbCollapseModule,
    ],
  declarations: [
    SystemAllocationComponent
  ],
  exports: [
    SystemAllocationComponent
  ]
})
export class GoalManagementFeatureSystemAllocationModule {}
