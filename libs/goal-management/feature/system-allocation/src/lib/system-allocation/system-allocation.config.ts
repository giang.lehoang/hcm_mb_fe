import {PanelOption} from "@hcm-mfe/shared/data-access/models";
import {ExceptionalPersonnelComponent} from "@hcm-mfe/goal-management/feature/exceptional-personnel";
import {ExceptionalTitleComponent} from "@hcm-mfe/goal-management/feature/exceptional-title";
import {IndividualRateComponent} from "@hcm-mfe/goal-management/feature/individual-rate";

export const PANELS_INFORMATION: PanelOption[] = [
  {
    id: 'exceptional_title',
    active: true,
    disabled: false,
    icon: '',
    name: 'goaManagement.system-allocation.panel.individualRate',
    panelComponent: IndividualRateComponent,
  },
  {
    id: 'exceptional_title',
    active: true,
    disabled: false,
    icon: '',
    name: 'goaManagement.system-allocation.panel.exceptionalTitle',
    panelComponent: ExceptionalTitleComponent,
  },
  // {
  //   id: 'exceptional_personnel',
  //   active: true,
  //   disabled: false,
  //   icon: '',
  //   name: 'goaManagement.system-allocation.panel.exceptionalPersonnel',
  //   panelComponent: ExceptionalPersonnelComponent,
  // }
]
