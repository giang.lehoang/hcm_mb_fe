import {Component, Injector, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {FormGroup} from "@angular/forms";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {Subject, takeUntil} from "rxjs";
import {SystemAllocationService} from "@hcm-mfe/goal-management/data-access/services";
import {
  IClassificationDetailRateDTORow,
  ISystemAllocation,
  ItAssessmentPeriod
} from "@hcm-mfe/goal-management/data-access/models";
import {getInnerWidth} from "@hcm-mfe/policy-management/helper";
import {MICRO_SERVICE, Mode} from "@hcm-mfe/shared/common/constants";
import {NzModalRef} from "ng-zorro-antd/modal";
import {ActionDocumentAttachedFileComponent} from "@hcm-mfe/goal-management/ui/form-input";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {PANELS_INFORMATION} from "./system-allocation.config";
import {MbCollapseComponent} from "@hcm-mfe/shared/ui/mb-collapse";
import {PanelOption} from "@hcm-mfe/shared/data-access/models";

@Component({
  selector: 'hcm-mfe-system-allocation',
  templateUrl: './system-allocation.component.html',
  styleUrls: ['./system-allocation.component.scss']
})
export class SystemAllocationComponent extends BaseComponent implements OnInit {
  public panels = PANELS_INFORMATION;
  private readonly destroyed$: Subject<void> = new Subject<void>();
  // @ViewChild('collapse') collapse!: MbCollapseComponent;
  override isLoading = false;
  modalRef: NzModalRef | undefined;
  tabSelectIndex = 0;

  constructor(
    injector: Injector,
    private systemAllocationService: SystemAllocationService
  ) {
    super(injector);
    this.form = this.fb.group({
      periodId: [null],
      flagStatus: [null],
    });
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.SYSTEM_ALLOCATION}`);
  }

  form: FormGroup;
  dataAssessmentPeriod: ItAssessmentPeriod[] = [];
  dataSystemAllocation: ISystemAllocation | undefined;
  classificationHeader: string[] | undefined;

  ngOnInit(): void {
    this.isLoading = true;
    this.systemAllocationService.getAssessmentPeriod().pipe(
      // it is now important to unsubscribe from the subject
      takeUntil(this.destroyed$)
    ).subscribe(data => {
      if (data.status === 200) {
        this.dataAssessmentPeriod = data.data;
        this.form.patchValue({
          periodId: this.dataAssessmentPeriod[0].periodId,
          flagStatus: this.dataAssessmentPeriod[0].flagStatus,
        });
        // do search data
        //this.doSearch();
      }
    }, (err) => {
      this.isLoading = false;
      this.toastrCustom.error(this.translate.instant('goaManagement.system-allocation.notification.systemAllocationTotalRate'));
    }, () => this.isLoading = false
  );

    this.onChanges();

  }

  onChanges(): void {
    this.form.valueChanges.pipe(
      takeUntil(this.destroyed$)
    ).subscribe(val => {
      this.doSearch();
    });
  }
  changeTab(event: any) {
    // this.isLoading = true;
    if (event.index) {
      this.tabSelectIndex = event.index;
      return;
    }
    this.tabSelectIndex = event.index;

  }
  doSearch() {
    this.isLoading = true;
    const requestSearch: Pick<ItAssessmentPeriod, 'periodId'> = this.form.value;
    this.systemAllocationService.getClassificationRate(requestSearch.periodId).pipe(
      takeUntil(this.destroyed$)
    ).subscribe(data => {
      if (data.status === 200) {
        this.dataSystemAllocation = data.data;
        if (this.dataSystemAllocation && this.dataSystemAllocation?.classificationRateDTOS?.length > 0) {
          // get classification header table show on table
          this.classificationHeader = this.dataSystemAllocation?.classificationRateDTOS[0]
            .classificationDetailRateDTORow.map((el: IClassificationDetailRateDTORow) => el.claPerName);
        }
      }
    }, (err) => {
      this.isLoading = false;
      this.toastrCustom.error(this.translate.instant('goaManagement.system-allocation.notification.errorResponse'));
    }, () => this.isLoading = false );
  }

  getValueRateDTOItem($event: number, classificationDetailRateDTOItem: IClassificationDetailRateDTORow, index: number) {
    classificationDetailRateDTOItem.rate = Number($event);


    if (Number($event) === $event && $event % 1 !== 0 || Number($event) > 100 || Number($event) < 0) {
      classificationDetailRateDTOItem.errorMess = this.translate.instant('goaManagement.system-allocation.notification.rangeNumberValid');
      return;
    } else classificationDetailRateDTOItem.errorMess = undefined
  }

  convertStringToBoolean(el: string) {
    return Boolean(el);
  }

  saveRecord() {
    let flagShowError = false;
    let updateClassificationRateDetailDTOs: Pick<IClassificationDetailRateDTORow, 'clrId' | 'rate'>[] = [];

    if (this.dataSystemAllocation) {
      for (let index = 0; index < this.dataSystemAllocation?.classificationRateDTOS?.length; index++) {

        const classificationRateDTOS = this.dataSystemAllocation?.classificationRateDTOS[index];

        const getListDetailRateDto = classificationRateDTOS?.classificationDetailRateDTORow?.map(el => {
          return {rate: el.rate, clrId: el.clrId}
        });

        updateClassificationRateDetailDTOs = [...updateClassificationRateDetailDTOs, ...getListDetailRateDto];

        // check totalRate not equal 100
        const totalRate = classificationRateDTOS.classificationDetailRateDTORow?.reduce((previousValue, currentValue) => {
          return Number(previousValue) + Number(currentValue.rate);
        }, 0);

        if (totalRate !== 100) {
          flagShowError = true;
          this.dataSystemAllocation.classificationRateDTOS[index].errMess = this.translate.instant('goaManagement.system-allocation.notification.systemAllocationTotalRate');
        } else {
          this.dataSystemAllocation.classificationRateDTOS[index].errMess = undefined;
        }
      }

      // save system allocation
      if (!flagShowError) {
        this.isLoading = true;
        this.systemAllocationService.updateClassificationRate({
          periodId: this.dataSystemAllocation?.periodId,
          updateClassificationRateDetailDTOs
        }).pipe(takeUntil(this.destroyed$)).subscribe(data => {
          if(data.status === 200){
            this.toastrCustom.success(this.translate.instant('common.notification.success'));
          }
        }, error => {
          this.isLoading = false;
          this.toastrCustom.error(this.translate.instant('common.notification.error'));
        }, () => this.isLoading = false)

      }
    }
  }

  showModalUploadFile(footerTmpl: TemplateRef<any>) {
    this.modalRef = this.modal.create({
      nzWidth: getInnerWidth(3.5),
      nzTitle: this.translate.instant('targerManager.targetCategory.label.atribute.UploadFileExel'),
      nzContent: ActionDocumentAttachedFileComponent,
      nzComponentParams: {
        mode: Mode.ADD,
        url:  'urlEndPointSystemAllocation',
        endpoint: 'END_POINT_GOAL_MANAGEMENT',
        microService: MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT,
        fileName: 'TEMPLATE_SYSTEM-ALLOCATION',
        param: `periodId=${this.form.value?.periodId}`
      },
      nzFooter: footerTmpl,
    });
    this.modalRef.afterClose.subscribe((result) => (result?.refresh ? this.doSearch(): ''));
  }

  override ngOnDestroy() {
    super.ngOnDestroy();
    this.destroyed$.next();
    this.destroyed$.complete();
    this.modalRef?.destroy();
  }

}
