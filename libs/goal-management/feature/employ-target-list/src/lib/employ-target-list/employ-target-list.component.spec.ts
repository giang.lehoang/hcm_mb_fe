import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployTargetListComponent } from './employ-target-list.component';

describe('EmployTargetListComponent', () => {
  let component: EmployTargetListComponent;
  let fixture: ComponentFixture<EmployTargetListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmployTargetListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployTargetListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
