import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { forkJoin, Subscription, throwError } from 'rxjs';
import { catchError, mergeMap, take } from 'rxjs/operators';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {
  ApprovePlanAssignmentService,
  EmployeeTargetService, GetCriteriaService,
  TargetAssessmentService
} from "@hcm-mfe/goal-management/data-access/services";
import {ValidateService} from "@hcm-mfe/shared/core";
import {FunctionCode, SessionKey} from "@hcm-mfe/shared/common/enums";
import {
  getActionRejectorAprrove,
  IEmployData,
  ISetOfIndicatorsData,
  listFlagStatus,
  processFlagStatus, ResponseEntity, ResponseEntityEmployTarget
} from "@hcm-mfe/goal-management/data-access/models";
import { User } from '@hcm-mfe/system/data-access/models';
import {HTTP_STATUS_CODE, userConfig} from "@hcm-mfe/shared/common/constants";
import {Pageable} from "@hcm-mfe/shared/data-access/models";
import {ModalConfirmComponent} from "@hcm-mfe/shared/ui/mb-modal-confirm";
import {numberDecimalValidator, transFormDataGroupByAsmId} from "@hcm-mfe/goal-management/helper";
import * as moment from "moment";


@Component({
  selector: 'app-employ-target-list',
  templateUrl: './employ-target-list.component.html',
  styleUrls: ['./employ-target-list.component.scss'],
})
export class EmployTargetListComponent extends BaseComponent implements OnInit {

  constructor(
    injector: Injector,
    public validateService: ValidateService,
    private readonly employeeTargetService: EmployeeTargetService,
    private readonly getCriteriaService: GetCriteriaService,
    private readonly approvePlanAssignmentService: ApprovePlanAssignmentService
  ) {
    super(injector);
    this.processFlagStatus = processFlagStatus();
    const formControl = new FormGroup({});
    formControl.addControl(this.listEmplployee, this.fb.array([this.createFormGroup(null, null)]));
    this.formEmployee = this.fb.group(formControl.controls);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.EMPLOYEE_TARGET_LIST}`);

  }

  currentUser: User | undefined;
  processFlagStatus: listFlagStatus | null = null;
  listEmplployee = 'data';
  private readonly subs: Subscription[] = [];
  override isLoading = false;
  formEmployee: FormGroup;
  private readonly notificationError = 'common.notification.error';

  limit: number = userConfig.pageSize;
  count = 0;
  value = '';
  pageable: Pageable | undefined;

  currentScaleNum = 'currentScaleNum';
  empCode = 'empCode';
  fullName = 'fullName';
  posName = 'posName';
  sotName = 'sotName';
  flagStatus = 'flagStatus';
  sotCode = 'sotCode'; // Mã bộ chỉ tiêu
  managerId = 'managerId';
  logic = 'logic';
  isActive = 'isActive';
  isRejected = 'isRejected';
  currentPeriodId = 'currentPeriodId';
  asmFromDate = 'asmFromDate';
  asmToDate = 'asmToDate';
  responseSelect = 0;
  tabSelectIndex = 0;

  asmIds = 'asmIds';
  posId = 'posId';
  empId = 'empId';
  sotId = 'sotId';// list asmId
  periodYear = 'periodYear';
  isSubmitted = false;

  isSaved = false;
  isDisplay = false;

  listDataEmployee: IEmployData[] = [];
  listTempDataEmployee: IEmployData[] = [];
  paramEmployeeTarget: IEmployData = {
    empCode: undefined,
    fullName: undefined,
    jobId: null,
    orgId: null,
    posId: undefined,
    periodYear: moment().month() === 0 ? moment().year() - 1 : new Date().getFullYear(),
    sotId: undefined,
    empId: undefined
  };

  paramSearchEmployPagination: IEmployData = {
    empCode: undefined,
    fullName: undefined,
    jobId: undefined,
    orgId: undefined,
    posId: undefined,
    periodYear: moment().month() === 0 ? moment().year() - 1 : new Date().getFullYear()
  };

  paramApprovePlan: IEmployData = {
    asmIds: undefined,
    empCode: undefined,
    managerId: undefined,
    content: undefined,
    action: undefined,
    flagStatus: processFlagStatus().CHO_PHE_DUYET_GIAO
  };

  @ViewChild('modalCompCorrection') modalCompCorrection: ModalConfirmComponent | undefined;
  @ViewChild('modalCompReject') modalCompReject: ModalConfirmComponent | undefined;
  paramCorrectionDialog = {
    asmIds: undefined,
    scale: undefined,
    sotId: undefined,
    flagStatus: undefined,
    content: undefined
  };

  listDataPositionsIndicators: ISetOfIndicatorsData[] = [];

  createFormGroup(item: any, index: number | null): FormGroup {
    const formControl = new FormGroup({});
    formControl.addControl(this.currentScaleNum, new FormControl(item ? item[this.currentScaleNum] : null, [
      Validators.required,
      Validators.max(100),
      Validators.min(0),
      numberDecimalValidator('isNotDecimal')
      // getTotalScaleNumberEmployTarget(this.scaleNum, 'scaleNumberTotalNotValid', item, this.listTempDataEmployee, index),
    ]));
    formControl.addControl(this.empCode, new FormControl(item ? item[this.empCode] : null));
    formControl.addControl(this.fullName, new FormControl(item ? item[this.fullName] : null));
    formControl.addControl(this.posName, new FormControl(item ? item[this.posName] : null));
    formControl.addControl(this.sotName, new FormControl(item ? item[this.sotName] : null));
    formControl.addControl(this.sotCode, new FormControl(item ? item[this.sotCode] : null));
    formControl.addControl(this.flagStatus, new FormControl(item ? item[this.flagStatus] : null));
    formControl.addControl(this.asmIds, new FormControl(item ? item[this.asmIds] : null));
    formControl.addControl(this.sotId, new FormControl(item ? item[this.sotId] : null));
    formControl.addControl(this.posId, new FormControl(item ? item[this.posId] : null));
    formControl.addControl(this.empId, new FormControl(item ? item[this.empId] : null));
    formControl.addControl(this.periodYear, new FormControl(item ? item[this.periodYear] : null));
    formControl.addControl(this.managerId, new FormControl(item ? item[this.managerId] : null));
    formControl.addControl(this.logic, new FormControl(item ? item[this.logic] : null));
    formControl.addControl(this.asmFromDate, new FormControl(item ? item[this.asmFromDate] : null));
    formControl.addControl(this.asmToDate, new FormControl(item ? item[this.asmToDate] : null));
    formControl.addControl(this.isActive, new FormControl(item ? item[this.isActive] : null));
    formControl.addControl(this.isRejected, new FormControl(item ? item[this.isRejected] : null));
    formControl.addControl(this.currentPeriodId, new FormControl(item ? item[this.currentPeriodId] : null));
    formControl.addControl('index', new FormControl(index ? index : null));
    return new FormGroup(formControl.controls);
  }

  confirmApprove(currentScaleNum: number, asmIds: number[], $event: Event) {
    $event.stopPropagation();
    this.isSubmitted = true;
    const value = this.formEmployee.value.data;
    this.listTempDataEmployee = value;
    this.pathValue(this.listTempDataEmployee);
    if (this.formEmployee.valid && this.isSaved || this.formEmployee.valid) {
      this.isLoading = true;
      const paramApprove = {
        asmIds,
        scale: Number(currentScaleNum) / 100
      };
      const postCorrectionEmployee = this.employeeTargetService.posSotEmployeePublicize(paramApprove)
        .subscribe((dataPositon: ResponseEntity<ISetOfIndicatorsData>) => {
          if (dataPositon) {
            this.listDataPositionsIndicators = dataPositon.data || [];
            this.isLoading = false;
            this.toastrCustom.success(this.translate.instant('targerManager.common.notification.deliveringSetOfSuccessTargets'));
            this.search();
          }
        }, (error) => {
          !error.message ? this.toastrCustom.error(this.translate.instant(error?.error))
            : this.toastrCustom.error(this.translate.instant(error?.message));

          this.isLoading = false;
        }, () => {
          this.isLoading = false;
        });
      this.subs.push(postCorrectionEmployee);
    } else {
      this.toastrCustom.error(this.translate.instant('targerManager.common.notification.validSave'));
    }
  }

  // correction employee
  correctionEmployTargetItem($event: any) {
    const postCorrectionEmployee = this.employeeTargetService.postCorrectionEmployee(
      {
        ...this.paramCorrectionDialog,
        content: $event
      }
    ).subscribe((dataPositon: ResponseEntity<Pick<ISetOfIndicatorsData, 'posId' | 'posName' | 'flagStatus'>>) => {
      if (dataPositon) {
        this.listDataPositionsIndicators = dataPositon.data || [];

        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        this.modalCompCorrection.isVisibleCorrectionDialog = false;
        this.modalCompCorrection?.formSubmit.patchValue({
          content: null
        });
        this.search();
      }
    }, (error) => {
      error.status === 400 ? this.toastrCustom.error(this.translate.instant('targerManager.common.notification.timeoutCorrection'))
        : this.toastrCustom.error(this.translate.instant(error.message));
    });
    this.subs.push(postCorrectionEmployee);
  }

  correctionShowModal(valueEmployee: IEmployData, $event: Event) {
    $event.stopPropagation();
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    this.modalCompCorrection.isVisibleCorrectionDialog = true;
    const { asmIds, currentScaleNum, sotId, flagStatus } = valueEmployee;
    this.paramCorrectionDialog = {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      asmIds,
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      scale: Number(currentScaleNum) / 100,
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      sotId,
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      flagStatus,
      content: undefined
    };
  }

  save() {
    this.isSubmitted = true;
    const value = this.formEmployee.value.data;
    this.listTempDataEmployee = value;
    this.pathValue(this.listTempDataEmployee);
    if (this.formEmployee.valid) {
      const paramSaveEmployee = value.map((el: any) => {
        return {
          asmIds: el.asmIds,
          scale: Number(el.currentScaleNum) / 100,
          empCode: el.empCode,
          currentPeriodId: el.currentPeriodId
        };
      });
      this.isLoading = true;
      const updateEmployeeScaleNumber = this.employeeTargetService.updateEmployeeScaleNumber(paramSaveEmployee)
        .subscribe((res: ResponseEntityEmployTarget<null>) => {
          if (res.status === HTTP_STATUS_CODE.OK) {
            this.isLoading = false;
            this.toastrCustom.success('Lưu tỷ trọng thành công');
            this.isSaved = true;
            this.search();
          }
        }, function(err: any) {
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          this.isLoading = false;
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          this.toastrCustom.error(this.translate.instant(err ? err?.message : this.notificationError));
        }.bind(this), () => {
          this.isLoading = false;
        });
      this.subs.push(updateEmployeeScaleNumber);
    } else {
      this.isSaved = false;
    }
  }

  override back() {
    this.location.back();
  }

  approvePlanAssignment(employeeItem: IEmployData, $event: Event): void {
    $event.stopPropagation();
    this.isLoading = true;
    const { asmIds, empCode, managerId, content } = employeeItem;
    this.paramApprovePlan = {
      ...this.paramApprovePlan,
      asmIds,
      empCode,
      managerId,
      content,
      action: getActionRejectorAprrove().approve
    };
    const subApprovePlanAssign = this.approvePlanAssignmentService.approvePlanAssignment(this.paramApprovePlan)
      .subscribe((res: ResponseEntityEmployTarget<null>) => {
      if (res.status === 200) {
        this.isLoading = false;
        this.toastrCustom.success(this.translate.instant('targerManager.personalTargetList.label.table.completedApproveAssign'));
        this.search();
      } else {
        this.isLoading = false;
      }
    }, (err: any) => {
      this.isLoading = false;
      this.toastrCustom.error(`${this.translate.instant(this.notificationError)} : ${err.message}`);
    });
    this.subs.push(subApprovePlanAssign);
  }

  rejectPlanAssignment($event: string): void {
    this.paramApprovePlan = {
      ...this.paramApprovePlan,
      content: $event
    };
    const subApprovePlanAssign = this.approvePlanAssignmentService.approvePlanAssignment(this.paramApprovePlan)
      .subscribe((res: ResponseEntityEmployTarget<null>) => {
      if (res.status === 200) {
        this.toastrCustom.success(this.translate.instant('targerManager.personalTargetList.label.table.rejectAssign'));
        this.modalCompReject?.formSubmit.patchValue({
          content: null
        });
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        this.modalCompReject.isVisibleCorrectionDialog = false;
        this.search();
      }
    }, (err) => {
      this.toastrCustom.error(`${this.translate.instant(this.notificationError)} : ${err.message}`);
    });
    this.subs.push(subApprovePlanAssign);
  }


  showModalRejectPlan(data: IEmployData, $event: Event): void {
    $event.stopPropagation();
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    this.modalCompReject.isVisibleCorrectionDialog = true;
    const { asmIds, empCode, managerId } = data;
    this.paramApprovePlan = {
      asmIds,
      empCode,
      managerId,
      content: undefined,
      action: getActionRejectorAprrove().reject,
      flagStatus: processFlagStatus().CHO_PHE_DUYET_GIAO
    };
  }


  pathValue(listTempDataEmployee?: IEmployData[]) {
    this.listDataEmployee.sort((a: any, b: any) => {
      if (a.logic === null) {
        a.logic = 2;
      }
      if (b.logic === null) {
        b.logic = 2;
      }
      if(a.logic > b.logic) return -1;
      if(a.logic < b.logic) return 1;
      return 0;
    })
    /* eslint-disable */
    const form = this.formEmployee.get(this.listEmplployee) as FormArray;
    if (listTempDataEmployee) {
      form?.clear();
      this.listTempDataEmployee.forEach((item, index) => {
        form.push(this.createFormGroup(item, index));
      });
    } else if (this.listDataEmployee.length > 0) {
      form?.clear();
      this.listDataEmployee.forEach((item, index) => {
        form.push(this.createFormGroup(item, index));
      });
    }
  }

  trackByFnEmployee(index: number, item: IEmployData) {
    return item;
  }

  redirectToDetail(data: any) {
      // check redirectToDetail if user is managerId or manager Indirect
      if(
          data.value?.managerId === this.currentUser?.empCode
          || (data.value?.flagStatus === this.processFlagStatus?.CHO_PHE_DUYET_GIAO
          && this.objFunction?.approve && !data.value?.logic)
      ){
          const cloneObject = { ...data.value };
          this.getCriteriaService.setListDetail(cloneObject);
          this.router.navigateByUrl('goal-management/periodic-goal-management/employ-target-list/give-criteria');
      }
  }

  changeTab(event: any) {
    if (event.index) {
      this.tabSelectIndex = event.index;
      return;
    }
    this.isLoading = true;
    this.tabSelectIndex = event.index;
    this.getListSotEmployeeAssignment();
  }

    goToAssignTargetToAllEmployee(){
        this.router.navigateByUrl('/goal-management/periodic-goal-management/employ-target-list/assign-target-to-all-employees');
    }

  ngOnInit(): void {
    this.isLoading = true;
    if (localStorage.getItem(SessionKey.CURRENCY_USER)) {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      this.currentUser = JSON.parse(localStorage.getItem(SessionKey.CURRENCY_USER));
    }
    this.getListSotEmployeeAssignment();
  }
  getListSotEmployeeAssignment() {
    this.employeeTargetService.getSotEmployeeAssignment().pipe(
      mergeMap( (res) => {
        if (res.status === 200){
          this.isDisplay = res.data.display;
          return this.employeeTargetService.getListEmployeeTarget(this.paramEmployeeTarget);
        }
        return throwError({message: "Error" });
      }),catchError(err => {
        return throwError(err);
      }), take(1))
      .pipe(take(1))
      .subscribe((res) => {
          this.responseSelect = res.data.number;
          this.listDataEmployee = transFormDataGroupByAsmId(res.data.content || []);
          this.pathValue();
      }, error => {
        this.isLoading = false;
        this.toastrCustom.error(this.translate.instant(error ? error.message : this.notificationError));
      }, () => {
        this.isLoading = false;
      })
  }
  receiveDataEmployee($event: IEmployData) {
    this.paramSearchEmployPagination = $event;
    this.search();
  }

  search($event?: any) {
    this.isLoading = true;
    const subSearchEmploy =
      forkJoin([
        this.employeeTargetService.getListEmployeeTarget(this.paramSearchEmployPagination),
        this.employeeTargetService.getSotEmployeeAssignment()
      ]).subscribe(([res, dataSotEmployeeAssignment]: [ResponseEntityEmployTarget<IEmployData>, ResponseEntityEmployTarget<null>]) => {
        if (res && dataSotEmployeeAssignment) {
          this.isLoading = false;
          this.listDataEmployee = transFormDataGroupByAsmId(res.data.content || []);
          this.pathValue();
        }

      }, (err) => {
        this.isLoading = false;
        this.toastrCustom.error(`${this.translate.instant(this.notificationError)} : ${err.message}`);
      });
    this.subs.push(subSearchEmploy);
  }

  override ngOnDestroy() {
    this.subs.forEach((s) => s.unsubscribe());
  }

}
