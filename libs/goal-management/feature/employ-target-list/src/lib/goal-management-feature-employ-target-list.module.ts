import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EmployTargetListComponent} from "./employ-target-list/employ-target-list.component";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {TranslateModule} from "@ngx-translate/core";
import {NzTabsModule} from "ng-zorro-antd/tabs";
import {GoalManagementUiFormInputModule} from "@hcm-mfe/goal-management/ui/form-input";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzTagModule} from "ng-zorro-antd/tag";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiMbModalConfirmModule} from "@hcm-mfe/shared/ui/mb-modal-confirm";
import {RouterModule} from "@angular/router";
import {
  GoalManagementFeatureSetTargetsForEmployeesModule
} from "@hcm-mfe/goal-management/feature/set-targets-for-employees";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {NzFormModule} from "ng-zorro-antd/form";


@NgModule({
    imports: [
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        SharedUiMbButtonModule,
        TranslateModule,
        NzTabsModule,
        GoalManagementUiFormInputModule,
        NzTableModule,
        NzTagModule,
        SharedUiMbInputTextModule,
        SharedUiMbModalConfirmModule,
        SharedUiMbSelectModule,
        GoalManagementFeatureSetTargetsForEmployeesModule,
        RouterModule.forChild([
            {
                path: '',
                component: EmployTargetListComponent,
            },
        ]),
        SharedUiLoadingModule,
        NzFormModule,
    ],
  declarations: [
    EmployTargetListComponent,
  ],
  exports: [
    EmployTargetListComponent
  ]
})
export class GoalManagementFeatureEmployTargetListModule {}
