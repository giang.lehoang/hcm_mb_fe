import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersonnelExceptionRatingsComponent } from './personnel-exception-ratings/personnel-exception-ratings.component';
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {TranslateModule} from "@ngx-translate/core";
import {RouterModule} from "@angular/router";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NzGridModule} from "ng-zorro-antd/grid";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiMbEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/mb-employee-data-picker";
import {SharedUiMbSelectModule} from "../../../../../shared/ui/mb-select/src";
import {NzInputModule} from "ng-zorro-antd/input";
import {SharedUiMbTableWrapModule} from "../../../../../shared/ui/mb-table-wrap/src";
import {SharedUiMbTableModule} from "../../../../../shared/ui/mb-table/src";

@NgModule({
  imports: [
    CommonModule,
    SharedUiLoadingModule,
    SharedUiMbButtonModule,
    TranslateModule,
    RouterModule.forChild([
      {
        path: '',
        component: PersonnelExceptionRatingsComponent,
      },
    ]),
    NzGridModule,
    SharedUiMbInputTextModule,
    SharedUiMbEmployeeDataPickerModule,
    SharedUiMbSelectModule,
    FormsModule,
    ReactiveFormsModule,
    NzInputModule,
    SharedUiMbTableWrapModule,
    SharedUiMbTableModule
  ],
  declarations: [
    PersonnelExceptionRatingsComponent
  ],
})
export class GoalManagementFeaturePersonnelExceptionRatingsModule {}
