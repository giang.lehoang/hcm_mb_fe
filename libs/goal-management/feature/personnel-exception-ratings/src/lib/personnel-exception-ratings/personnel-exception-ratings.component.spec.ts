import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonnelExceptionRatingsComponent } from './personnel-exception-ratings.component';

describe('PersonnelExceptionRatingsComponent', () => {
  let component: PersonnelExceptionRatingsComponent;
  let fixture: ComponentFixture<PersonnelExceptionRatingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonnelExceptionRatingsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonnelExceptionRatingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
