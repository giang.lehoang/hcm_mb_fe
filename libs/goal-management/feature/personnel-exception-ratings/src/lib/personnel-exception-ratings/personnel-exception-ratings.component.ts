import {Component, Injector, OnInit, TemplateRef} from '@angular/core';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {catchError, EMPTY, finalize, map, Observable, Subject, Subscription, takeUntil} from "rxjs";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";

import {
  IParamPersonnelExceptionRating, IPersonnelExceptionRatings, IResponseContentEntity, IResponseEntityData,
  ItAssessmentPeriod,
  ResponseEntity
} from "../../../../../data-access/models/src";
import {PersonnelExceptionRatingsService, SystemAllocationService} from "../../../../../data-access/services/src";
import {IPersonalInfoEmployeePolicy, MBTableConfig, Pagination} from "../../../../../../shared/data-access/models/src";
import {FormModalShowTreeUnitComponent} from "../../../../../../shared/ui/form-modal-show-tree-unit/src";
import {NzModalRef} from "ng-zorro-antd/modal";
import {ActionDocumentAttachedFileComponent} from "@hcm-mfe/goal-management/ui/form-input";
import {Mode} from "@hcm-mfe/shared/common/constants";

@Component({
  selector: 'hcm-mfe-personnel-exception-ratings',
  templateUrl: './personnel-exception-ratings.component.html',
  styleUrls: ['./personnel-exception-ratings.component.scss']
})
export class PersonnelExceptionRatingsComponent extends BaseComponent implements OnInit {

  constructor(
    injector: Injector,
    private systemAllocationService: SystemAllocationService,
    private personnelExceptionRatingsService: PersonnelExceptionRatingsService
    ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.PERSONNEL_EXCEPTION_RATINGS}`);
  }

  resultOrgId: { orgId: number | '', orgName: string | null}  = { orgId: '', orgName: null};
  dataAssessmentPeriod: ItAssessmentPeriod[] = [];
  employeeId = '';
  private readonly subs: Subscription[] = [];
  override isLoading = false;
  functionCode = FunctionCode.PERSONNEL_EXCEPTION_RATINGS;
  pagination = new Pagination();
  paramSearch: IParamPersonnelExceptionRating = {
    fullName: '',
    employeeCode: '',
    periodId: null,
    orgId: '',
    page: 0,
    size: this.pagination.pageSize
  }
  modalRef: NzModalRef | undefined;
  tableConfig: MBTableConfig = new MBTableConfig();
  heightTable = { x: '80vw', y: '27em'};
  dataTable$: Observable<IResponseEntityData<IResponseContentEntity<IPersonnelExceptionRatings>>> | undefined;


  private readonly destroyed$: Subject<void> = new Subject<void>()

  searchInit(){
    localStorage.removeItem("waringUploadFile");
    this.isLoading = true;
    this.dataTable$ = this.personnelExceptionRatingsService.getPersonnelExceptionRatingsService(this.paramSearch).pipe(
      map(result => {
        this.tableConfig.total = result.data.totalElements;
        return result;
      }),
      catchError((err) => {
        console.log('err', err);
        this.toastrCustom.error(this.translate.instant('goaManagement.system-allocation.notification.errorResponse'));
        return EMPTY;
      }),
      finalize(() => {
        this.isLoading = false
      })
    );
  }

  ngOnInit(): void {
    this.tableConfig.showFrontPagination = true;
    this.initTable();
    this.isLoading = true;
    this.systemAllocationService.getAssessmentPeriod(true).pipe(
      takeUntil(this.destroyed$)
    ).subscribe(data => {
        if (data.status === 200) {
          this.dataAssessmentPeriod = data.data;
          this.paramSearch = {
            ...this.paramSearch,
            periodId: this.dataAssessmentPeriod[0].periodId,
          }
          this.searchInit();
        }
      }, (err) => {
        this.isLoading = false;
        this.toastrCustom.error(this.translate.instant('goaManagement.system-allocation.notification.errorResponse'));
      }, () => this.isLoading = false
    );
  }

  clearDataOrgId() {
    this.resultOrgId = {
      orgId: '',
      orgName: null,
    };
  }

  showModalUnit() {
    this.modalRef = this.modal.create({
      nzWidth: '80vw',
      nzTitle: this.translate.instant('targerManager.setOfIndicators.unit'),
      nzContent: FormModalShowTreeUnitComponent,
    });
    this.modalRef.afterClose.subscribe((result) => {
      if (result?.orgName) {
        this.resultOrgId = {
          orgId: this.modalRef?.getContentComponent().objectSearch?.orgId,
          orgName: this.modalRef?.getContentComponent().orgName
        }
      }
    });
  }

  formSearch(){
    this.paramSearch = {
      ...this.paramSearch,
      orgId: this.resultOrgId.orgId === '' ?  this.resultOrgId.orgId : Number(this.resultOrgId?.orgId)
    }
    this.searchInit();
  }

  onChange(personalInfo: IPersonalInfoEmployeePolicy){
    if(personalInfo){
      const { employeeCode, fullName } = personalInfo;
      this.paramSearch = {
        ...this.paramSearch,
        fullName,
        employeeCode,
      }
    }else{
      this.paramSearch = {
        ...this.paramSearch,
        fullName: '',
        employeeCode: '',
      }
    }
  }

  showModalUploadFileExel(footerTmpl: TemplateRef<any>) {
    const param = { periodId: this.paramSearch.periodId };
    this.modalRef = this.modal.create({
      nzWidth: this.getInnerWidth(3.5),
      nzTitle: this.translate.instant('goaManagement.common.UploadFileExel'),
      nzContent: ActionDocumentAttachedFileComponent,
      nzComponentParams: {
        mode: Mode.ADD,
        url: 'urlPersonnelExceptionRatingsService',
        endpoint: 'END_POINT_GOAL_MANAGEMENT',
        microService: 'TARGET_ENDPOIN_ASSESSMENT',
        fileName: 'TEMPLATE_DANH_SACH_XEP_LOAI_NHAN_SU_NGOAI_LE',
        param: param
      },
      nzFooter: footerTmpl,
    });
    // this.modalRef.afterClose.subscribe((result) => (result?.refresh ? this.searchInit() : ''));
    this.modalRef.afterClose.subscribe((result) => {
      // @ts-ignore
      if(JSON.parse(localStorage.getItem('waringUploadFile')) || result?.refresh ) {
        this.searchInit()
      }
    });
  }

  getInnerWidth(point: number): number {
    if (window.innerWidth > 767) {
      return window.innerWidth / point > 1100 ? 1100 : window.innerWidth / point;
    }
    return window.innerWidth;
  }

  override ngOnDestroy() {
    super.ngOnDestroy();
    this.destroyed$.next();
    this.destroyed$.complete();
    this.modalRef?.destroy();
    localStorage.removeItem("waringUploadFile");
  }

  changePage(pageNumber?: number) {

    if (pageNumber) {
      this.pagination.pageNumber = pageNumber - 1 ;
      this.paramSearch.page = pageNumber - 1;
    }
    this.isLoading = true;
    this.searchInit();
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'goaManagement.common.employeeCode',
          field: 'employeeCode',
          thClassList: ['text-center'],
          width: 60,
        },
        {
          title: 'goaManagement.common.fullName',
          field: 'fullName',
          thClassList: ['text-center'],
          width: 70,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'goaManagement.common.orgName',
          field: 'orgName',
          thClassList: ['text-center'],
          width: 110,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'Job',
          field: 'jobName',
          thClassList: ['text-center'],
          tdClassList: ['text-right'],
          width: 60,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'goaManagement.common.scoresTHCV',
          thClassList: ['text-center'],
          field: 'resultFinal',
          width: 50,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'goaManagement.common.classification',
          thClassList: ['text-center'],
          field: 'classificationName',
          width: 110,
          fixed: window.innerWidth > 1024,
        },
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1,
      showFrontPagination: false
    };
  }

}
