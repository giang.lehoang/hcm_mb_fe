import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EvaluetionApprovalSetOfIndicatorComponent} from "./evaluetion-approval-set-of-indicator/evaluetion-approval-set-of-indicator.component";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {TranslateModule} from "@ngx-translate/core";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzTagModule} from "ng-zorro-antd/tag";
import {SharedPipesFormatScoreModule} from "@hcm-mfe/shared/pipes/format-score";
import {SharedUiMbModalConfirmModule} from "@hcm-mfe/shared/ui/mb-modal-confirm";
import {RouterModule} from "@angular/router";
import {GoalManagementUiFormInputModule} from "@hcm-mfe/goal-management/ui/form-input";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {FormsModule} from "@angular/forms";
import {NzGridModule} from "ng-zorro-antd/grid";

@NgModule({
    imports: [CommonModule, SharedUiLoadingModule, SharedUiMbButtonModule,
        TranslateModule, NzTableModule, NzTagModule, FormsModule,
        SharedPipesFormatScoreModule, SharedUiMbModalConfirmModule,
        RouterModule.forChild([
            {
                path: '',
                component: EvaluetionApprovalSetOfIndicatorComponent,
            },
        ]), GoalManagementUiFormInputModule, SharedUiMbInputTextModule, NzGridModule],
  declarations: [EvaluetionApprovalSetOfIndicatorComponent],
  exports: [EvaluetionApprovalSetOfIndicatorComponent]
})
export class GoalManagementFeatureEvaluetionApprovalSetOfIndicatorModule {}
