import { HttpStatusCode } from '@angular/common/http';
import { Component, Injector, TemplateRef, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {FunctionCode, maxInt32, SessionKey} from "@hcm-mfe/shared/common/enums";
import {NzModalRef} from "ng-zorro-antd/modal";
import {
  getActionRejectorAprrove,
  IEmployData,
  listFlagStatus,
  OneEmpCodeMultipleEmpId, processFlagStatus,
  ResponseEntityEmployTarget
} from "@hcm-mfe/goal-management/data-access/models";
import {ActionDocumentAttachedFileComponent} from "@hcm-mfe/goal-management/ui/form-input";
import {Mode} from "@hcm-mfe/shared/common/constants";
import {
  EvaluateEmployeeResultsService,
  EvaluetionApprovalSetOfIndicatorService
} from "@hcm-mfe/goal-management/data-access/services";
import {ModalConfirmComponent} from "@hcm-mfe/shared/ui/mb-modal-confirm";
import {User} from "@hcm-mfe/system/data-access/models";
import * as moment from "moment";

@Component({
  selector: 'app-evaluetion-approval-set-of-indicator',
  templateUrl: './evaluetion-approval-set-of-indicator.component.html',
  styleUrls: ['./evaluetion-approval-set-of-indicator.component.scss'],
  // providers: [TargetAssessmentService, EmployeeTargetService, EvaluetionApprovalSetOfIndicatorService],
})
export class EvaluetionApprovalSetOfIndicatorComponent extends BaseComponent {
  constructor(
    injector : Injector,
    private readonly evaluetionApprovalSetOfIndicatorService: EvaluetionApprovalSetOfIndicatorService,
    private readonly evaluateEmployeeResultsService: EvaluateEmployeeResultsService,
  ) {
    super(injector);
    this.processFlagStatus = processFlagStatus();
    // hard code here update the menu
    this.objFunction = this.sessionService.getSessionData(
      `FUNCTION_${FunctionCode.EVALUETION_APPROVAL_SET_OF_INDICATOR}`
    );
    // @ts-ignore
    this.currentUser = JSON.parse(localStorage.getItem(SessionKey.CURRENCY_USER));
    console.log(this.objFunction)
  }
  @ViewChild('modalCompCorrection') modalCompCorrection: ModalConfirmComponent | undefined;
  @ViewChild('modalCompReject') modalCompReject: ModalConfirmComponent | undefined;

  listWidth = ['5%', '10%', '13%', '15%', '10%', '10%', '10%', '180px', '8%', '8%', '8%', '8%', '8%', '8%', '200px'];
  isSubmitted = false;
  listData: IEmployData[] = [];
  currentUser: User | undefined;
  modalRef: NzModalRef | undefined;
  processFlagStatus: listFlagStatus | null = null;
  paramSearchListIndicator: IEmployData = {
    empCode: undefined,
    fullName: undefined,
    jobId: undefined,
    orgId: undefined,
    posId: undefined,
    periodYear: moment().month() === 0 ? moment().year() - 1 : new Date().getFullYear(),
    periodId: undefined,
    size: maxInt32
  };

  private readonly subs: Subscription[] = [];
  listDataEmployeeIndicator: IEmployData[] = [];
  isVisibleRejectDialog = false;
  paramApprovePlan = {
    asmIds: undefined,
    empId: undefined,
    periodId: undefined,
    flagStatus: undefined,
    content: undefined,
    action: undefined,
    totalResultNum: undefined,
  };


  receiveDataEmployee($event: IEmployData) {
    this.paramSearchListIndicator.empCode = $event?.empCode;
    this.paramSearchListIndicator.fullName = $event?.fullName;
    this.paramSearchListIndicator.jobId = $event?.jobId;
    this.paramSearchListIndicator.orgId = $event?.orgId;
    this.paramSearchListIndicator.posId = $event?.posId;
    this.paramSearchListIndicator.periodYear = $event?.periodYear;
    this.paramSearchListIndicator.periodId = $event?.periodId;
    this.search();
  }

  onLoading(event: any) {
    this.isLoading = event;
  }

  search() {
    this.isLoading = true;
    const listDataIndicator = this.evaluetionApprovalSetOfIndicatorService
      .getListEmployeeEvaluetionApproval(this.paramSearchListIndicator)
      .subscribe(
        (response: ResponseEntityEmployTarget<IEmployData>) => {
          if (response.status === HttpStatusCode.Ok) {
            const tempArr = response.data.content;
            tempArr.sort((a: any, b: any) => {
              if (a.logic === null) {
                a.logic = 2;
              }
              if (b.logic === null) {
                b.logic = 2;
              }
              if(a.logic > b.logic) return -1;
              if(a.logic < b.logic) return 1;
              return 0;
            })
            this.listData = [...tempArr];
            this.listDataEmployeeIndicator = this.convertData(tempArr || []);
            this.checkDuplicate();
            this.isLoading = false;
          } else {
            this.isLoading = false;
            this.toastrCustom.error(response.message);
          }
        },
        (err: any) => {
          this.isLoading = false;
          this.toastrCustom.error(err.message);
        }
      );
    this.subs.push(listDataIndicator);
  }
  save() {
    this.isSubmitted = true;
    this.isLoading = true;
    const invalidScale = this.listDataEmployeeIndicator.some(el => el.invalid);
    // @ts-ignore
    const limitScale = this.listDataEmployeeIndicator.some(el => +el.scaleNum > 100 || +el.scaleNum < 0);
    if (invalidScale || limitScale) {
      return;
    }
    const bodyRequest: any = [];
    this.listData.forEach(el => {
      const item = this.listDataEmployeeIndicator.find(data => data.asmId === el.asmId);
      const objTemp = {
        asmId: el.asmId,
        // @ts-ignore
        scaleNum: item ? (+item.scaleNum / 100) : el.scaleNum,
        empId: el.empId,
        empCode: el.empCode,
        assignedType: el.assignedType,
        periodId: el.periodId,
      }
      bodyRequest.push(objTemp);
    })
    this.evaluetionApprovalSetOfIndicatorService.saveScaleNum(bodyRequest).subscribe(res => {
      this.isSubmitted = false;
      this.isLoading = false;
      this.search();
      this.toastrCustom.success(this.translate.instant("common.notification.success"));
    }, err => {
      this.isLoading = false;
      if(err.code === 500) {
        this.toastrCustom.error(this.translate.instant("common.notification.error"));
      } else {
        this.toastrCustom.error(err.message);
      }
    })
  }
  checkDuplicate() {
    this.listDataEmployeeIndicator.forEach(el => {
      const tempArr = this.listDataEmployeeIndicator.filter(data => el.empCode === data.empCode);
      el.rowSpan = tempArr.length >= 2 ? tempArr.length : 1;
    });
  }
  convertData(list: Array<IEmployData>) {
    const arr: Array<IEmployData> = [];
    const arrEmpCodeEmpId: Array<OneEmpCodeMultipleEmpId> = [];
    this.groupByEmpId(list, arrEmpCodeEmpId);

    for (let i = 0; i < arrEmpCodeEmpId.length; i++) {
      const empCodeEmpId = arrEmpCodeEmpId[i];
      if (empCodeEmpId.arrEmpId) {
        for (let j = 0; j < empCodeEmpId.arrEmpId.length; j++) {
          const empId = empCodeEmpId.arrEmpId[j];
          const arrQL: Array<IEmployData> = list.filter((m) => m.assignedType === 'QL' && m.empId === empId);
          this.extractedAsmidAndCaculatorScaleTotal(arrQL, j, i, list, arr);
        }
      }
    }
    return arr;
  }

  private extractedAsmidAndCaculatorScaleTotal(
    arrQL: Array<IEmployData>,
    j: number,
    i: number,
    list: Array<IEmployData>,
    arr: Array<IEmployData>) {
    for (let k = 0; k < arrQL.length; k++) {
      const item = arrQL[k];
      const temp: IEmployData = {
        index: j === 0 && k === 0 ? i + 1 : null,
        ...item,
        // @ts-ignore
        scaleNum: item.scaleNum && item.scaleNum * 100 % 1 === 0 ? item.scaleNum * 100 : (item.scaleNum * 100).toFixed(2),
        asmIds: item.asmId ? [item.asmId] : []
      };
      const itemHT = list.find((m) => m.assignedType === 'HT' && m.empId === item.empId);
      if (itemHT?.asmId && temp.asmIds) {
        temp.asmIds.push(itemHT.asmId);
      }
      arr.push(temp);
    }
  }

  private groupByEmpId(list: Array<IEmployData>, arrEmpCodeEmpId: Array<OneEmpCodeMultipleEmpId>) {
    for (const item of list) {
      const temp: OneEmpCodeMultipleEmpId | undefined = arrEmpCodeEmpId.find((m) => m.empCode === item.empCode);
      if (!temp && item.empId) {
        arrEmpCodeEmpId.push({ empCode: item.empCode, arrEmpId: [item.empId] });
      } else if (!temp?.arrEmpId.some((m) => m === item.empId) && item.empId) {
        temp?.arrEmpId.push(item.empId);
      }
    }
  }
  onChange(value: any, data: any) {
    const numRegex = /^-?\d*[.,]?\d{0,2}$/;
    const regexDecimal = new RegExp(numRegex);
    data.scaleNum = value;
    if (!regexDecimal.test(value)) {
      data.invalid = true;
    } else {
      data.invalid = false;
    }
  }

  override ngOnDestroy() {
    this.modalRef?.destroy();
    this.subs.forEach((s) => s.unsubscribe());
  }

  confirmApprove(params: IEmployData, $event: Event) {
    $event.stopPropagation();
    this.isLoading = true;
    const { asmIds, empId, flagStatus } = params;

    const paramApprove = {
      asmIds,
      empId,
      content: null,
      flagStatus,
    };
    const subApprovePlanAssign = this.evaluetionApprovalSetOfIndicatorService
      .SendEmployeeEvaluetionApproval(paramApprove)
      .subscribe(
        (res: ResponseEntityEmployTarget<null>) => {
          if (res.status === HttpStatusCode.Ok) {
            this.isLoading = false;
            this.toastrCustom.success(
                this.translate.instant('goaManagement.evaluation-approval-set-of-indicator.notification.sendSucess')
            );
            this.search();
          } else {
            this.isLoading = false;
          }
        },
        (err: any) => {
          this.isLoading = false;
          this.toastrCustom.error(err.message);
        }
      );
    this.subs.push(subApprovePlanAssign);
  }
  navigateRateEmployeeResult() {
    this.router.navigateByUrl('/goal-management/periodic-goal-management/evaluate-employee-result');
  }
  approvePlanAssignment(params: IEmployData, $event: Event): void {
    $event.stopPropagation();
    this.isLoading = true;
    const { asmIds, empId, periodId, totalResultNum } = params;
    const paramApprove = {
      asmIds,
      empId,
      periodId,
      totalResultNum,
      content: null,
      action: getActionRejectorAprrove().approve,
    };
    const subApprovePlanAssign = this.evaluetionApprovalSetOfIndicatorService
      .ApprovalorRejectEmployeeEvaluetionApproval(paramApprove)
      .subscribe(
        (res: ResponseEntityEmployTarget<null>) => {
          if (res.status === HttpStatusCode.Ok) {
            this.isLoading = false;
            this.toastrCustom.success(
              this.translate.instant('targerManager.personalTargetList.label.table.completedApproveAssign')
            );
            this.search();
          } else {
            this.isLoading = false;
          }
        },
        (err: any) => {
          this.isLoading = false;
          this.toastrCustom.error(err.message);
        }
      );
    this.subs.push(subApprovePlanAssign);
  }

  trackByFn(item: any){
    return item.asmId;
  }
  getInnerWidth(point: number): number {
    if (window.innerWidth > 767) {
      return window.innerWidth / point > 1100 ? 1100 : window.innerWidth / point;
    }
    return window.innerWidth;
  }
  showModalUploadFile(footerTmpl: TemplateRef<any>) {
    this.modalRef = this.modal.create({
      nzWidth: this.getInnerWidth(3.5),
      nzTitle: this.translate.instant('targerManager.targetCategory.label.atribute.UploadFileExel'),
      nzContent: ActionDocumentAttachedFileComponent,
      nzComponentParams: {
        mode: Mode.ADD,
        url: 'urlTargetRacing',
        endpoint: 'END_POINT_GOAL_MANAGEMENT',
        microService: 'TARGET_ENDPOIN_ASSESSMENT',
        fileName: 'TEMPLATE_KET_QUA'
      },
      nzFooter: footerTmpl,
    });
    this.modalRef.afterClose.subscribe((result) => (result?.refresh ? this.search() : ''));
  }
  showModalRejectPlan(data: IEmployData, $event: Event): void {
    $event.stopPropagation();
    if (this.modalCompReject) {
      this.modalCompReject.isVisibleCorrectionDialog = true;
    }
    const { asmIds, empId, periodId, totalResultNum } = data;
    this.paramApprovePlan = {
      // @ts-ignore
      asmIds,
      // @ts-ignore
      empId,
      // @ts-ignore
      periodId,
      flagStatus: undefined,
      content: undefined,
      // @ts-ignore
      totalResultNum,
      // @ts-ignore
      action: getActionRejectorAprrove().reject,
    };
  }

  rejectPlanAssignment($event: string): void {
      this.paramApprovePlan = {
        ...this.paramApprovePlan,
        // @ts-ignore
        content: $event,
      };
     // delete this.paramApprovePlan.action;
      const subApprovePlanAssign = this.evaluetionApprovalSetOfIndicatorService
        .ApprovalorRejectEmployeeEvaluetionApproval(this.paramApprovePlan)
        .subscribe(
          (res: ResponseEntityEmployTarget<null>) => {
            if (res.status === HttpStatusCode.Ok) {
              this.toastrCustom.success(
                this.translate.instant('targerManager.personalTargetList.label.table.rejectAssign')
              );
              if (this.modalCompReject) {
                this.modalCompReject.formSubmit.patchValue({
                  content: null,
                });
                this.modalCompReject.isVisibleCorrectionDialog = false;
              }
              this.search();
            }
          },
          (err: any) => {
            this.toastrCustom.error(err.message);
          }
        );
      this.subs.push(subApprovePlanAssign);
  }

  showModalCorrectionPlan(data: IEmployData, $event: Event): void {
    $event.stopPropagation();
    const { asmIds, empId, periodId, totalResultNum } = data;
    if (this.modalCompCorrection) {
      this.modalCompCorrection.isVisibleCorrectionDialog = true;
    }
    this.paramApprovePlan = {
      // @ts-ignore
      asmIds,
      // @ts-ignore
      empId,
      // @ts-ignore
      periodId,
      flagStatus: undefined,
      content: undefined,
      // @ts-ignore
      totalResultNum,
      action: undefined,
    };
  }

  correctionPlanAssignment($event: string) {
      const paramApprovePlan = {
        asmIds: this.paramApprovePlan.asmIds,
        periodId: this.paramApprovePlan.periodId,
        flagStatus: this.processFlagStatus?.HOAN_TAT_PHE_DUYET_KQ,
        content: $event,
      };
      const sub = this.evaluetionApprovalSetOfIndicatorService
        .correctionEmployeeEvaluetionApproval(paramApprovePlan)
        .subscribe(
          (res: ResponseEntityEmployTarget<null>) => {
            if (res.status === HttpStatusCode.Ok) {
              this.toastrCustom.success(
                this.translate.instant('targerManager.EvaluetionApprovalSetOfIndicator.label.successCorrection')
              );
              if (this.modalCompCorrection) {
                this.modalCompCorrection.formSubmit.patchValue({
                  content: null,
                });
                this.modalCompCorrection.isVisibleCorrectionDialog = false;
              }
              this.search();
            }
          },
          (err: any) => {
            this.toastrCustom.error(err.message);
          }
        );
      this.subs.push(sub);
  }

  redrirectToDetail(item: any) {
    if(item.managerId === this.currentUser?.empCode || !item.logic) {
      this.router.navigateByUrl('/goal-management/periodic-goal-management/evaluate-employee-results');
      localStorage.setItem('objectDetail', JSON.stringify(item));
      this.evaluateEmployeeResultsService.setInformationDataDetail(item);
    }
  }
}
