import { Component, Injector, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { forkJoin, Subscription } from 'rxjs';
import { NzModalRef } from 'ng-zorro-antd/modal';
import {
  SetOfIndicatorService,
  TargetAssessmentService,
  UrlConstant
} from "@hcm-mfe/goal-management/data-access/services";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {ValidateService} from "@hcm-mfe/shared/core";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {Pageable} from "@hcm-mfe/shared/data-access/models";
import {
  Ijob,
  ISetOfIndicatorsData,
  PositionUnassign,
  ResponseEntityDataAssessment, ResponseEntityEmployTarget
} from "@hcm-mfe/goal-management/data-access/models";
import {HTTP_STATUS_CODE, Mode, url, userConfig} from "@hcm-mfe/shared/common/constants";
import {mbDataSelectsAssessmentStatus} from "@hcm-mfe/system/data-access/models";
import {MbDropDowListLazySearchComponent} from "../../../../../../shared/ui/mb-drop-dow-list-lazy-search/src/lib/mb-drop-dow-list-lazy-search/mb-drop-dow-list-lazy-search.component";
import {ActionDocumentAttachedFileComponent} from "@hcm-mfe/goal-management/ui/form-input";
import {FormSetOfIndicatorsComponent} from "@hcm-mfe/goal-management/feature/form-set-of-indicators";
import { FormModalShowTreeUnitComponent } from '@hcm-mfe/shared/ui/form-modal-show-tree-unit';


@Component({
  selector: 'app-set-of-indicators',
  templateUrl: './set-of-indicators.component.html',
  styleUrls: ['./set-of-indicators.component.scss'],
})
export class SetOfIndicatorsComponent extends BaseComponent implements OnInit {
  constructor(
    injector: Injector,
    public validateService: ValidateService,
    readonly targetAssessmentService: TargetAssessmentService
  ) {
    super(injector);
    this.form = this.fb.group({
      orgId: [''],
      sotCode: [''],
      posId: [''],
      flagStatus: [''],
      jobId: [''],
    });
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.SET_OF_INDICATORS}`);
    console.log(this.objFunction);
  }

  @ViewChild('formPositionComp') formPositionComp: MbDropDowListLazySearchComponent | undefined;
  @ViewChild(FormSetOfIndicatorsComponent) formSetOfIndicatorComp: any;
  readonly subs: Subscription[] = [];
  form: FormGroup;
  limit: number = userConfig.pageSize;
  count = 0;
  value = '';
  modalRef: NzModalRef | undefined;
  pageable: Pageable | undefined;
  public readonly nzAlignTh = 'left';

  resultOrgId: number | null = null;
  resultJobId: number | null = null;
  mbDataSelectsAssessmentStatus = mbDataSelectsAssessmentStatus();
  listDataTargetIndicators: ISetOfIndicatorsData[] = [];
  urlPositionService = UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointPositionSearch;
  urlGetListJobTypeORG = `${url.url_enpoint_project_list_job}`
  paramSetupPosition: Pick<ISetOfIndicatorsData, 'jobId' | 'orgId'> | { jobId: string; orgId: string } = {
    // @ts-ignore
    jobId: this.resultJobId,
    // @ts-ignore
    orgId: this.resultOrgId,
  };

  paramSetUpJob: Pick<Ijob, 'jobType'> = {
    jobType: 'ORG',
  }

  paramTargetIndicators = {
    page: 0,
    size: userConfig.pageSize,
  };

  showModalUnit($event: any, value: string, footerTmpl?: TemplateRef<any>, data?: any) {
    this.value = '';
    this.modalRef = this.modal.create({
      nzWidth: '80vw',
      nzTitle: this.translate.instant('targerManager.setOfIndicators.unit'),
      nzContent: FormModalShowTreeUnitComponent,
    });
    this.modalRef.afterClose.subscribe((result) => {
      if (result?.orgName) {
        this.resultOrgId = this.modalRef?.getContentComponent().objectSearch?.orgId;
        this.form.patchValue({
          orgId: this.modalRef?.getContentComponent().orgName,
        });
      }
      this.getPositionSettupAssessment();
    });
  }

  trackFntable(index: number, item: ISetOfIndicatorsData) {
    return item.sotCode;
  }

  selectPositionId(item: PositionUnassign) {
    if (item && item.posId) {
      this.form.patchValue({
        posId: item.posId
      });
    } else this.form.patchValue({
      posId: ''
    });

  }

  clearDataOrgId() {
    this.form.patchValue({
      orgId: null,
    });
    this.resultOrgId = null;
  }

  selectDataJobId(item: any) {
    if (item && item.jobId) {
      this.form.patchValue({
        jobId: item.jobId
      });
    } else this.form.patchValue({
      jobId: ''
    });
    this.getPositionSettupAssessment();
  }

  // back() {
  //   this.location.back();
  // }

  getPositionSettupAssessment() {
    const request: Pick<ISetOfIndicatorsData, 'jobId' | 'orgId'> = this.form.value;
    this.paramSetupPosition.jobId = request.jobId ? Number(request.jobId) : '';
    // @ts-ignore
    this.paramSetupPosition.orgId = request.orgId ? this.resultOrgId : '';
    this.formPositionComp?.onRetrivedData(this.paramSetupPosition);
  }

  ngOnInit(): void {
    this.isLoading = true;
    const request: ISetOfIndicatorsData = this.form.value;
    this.paramSetupPosition.jobId = request.jobId ? Number(request.jobId) : '';
    this.paramSetupPosition.orgId = request.orgId ? Number(request.orgId) : '';
    const listParamsTargetIndicators = {
      ...request,
      ...this.paramTargetIndicators,
    };
    const formSetofIndicator = forkJoin([
      this.targetAssessmentService.getSetOfTargetIndicators(listParamsTargetIndicators),
    ]).subscribe(
      ([dataTargetIndicators]: [
        ResponseEntityDataAssessment<ISetOfIndicatorsData>
      ]) => {
        if (dataTargetIndicators) {
          this.listDataTargetIndicators = dataTargetIndicators.data.content || [];
          this.pageable = {
            totalElements: dataTargetIndicators.data.totalElements,
            totalPages: dataTargetIndicators.data.totalPages,
            currentPage: dataTargetIndicators.data.number + 1,
            numberOfElements: dataTargetIndicators.data.numberOfElements,
            size: this.limit,
          };
        }
        this.isLoading = false;
      },
      (error) => {
        this.isLoading = false;
        this.toastrCustom.error(this.translate.instant('common.notification.error'));
      },
      () => {
        this.isLoading = false;
      }
    );
    this.subs.push(formSetofIndicator);
  }

  onSearch() {
    this.paramTargetIndicators.page = 0;
    this.initData();
  }

  changePage(event: any) {
    if (event) {
      this.paramTargetIndicators.page = event - 1;
    }
    this.initData();
  }

  initData() {
    const request: ISetOfIndicatorsData = this.form.value;
    if (this.form.value.orgId && this.form.value.orgId !== '') {
      request.orgId = Number(this.resultOrgId);
    } else {
      request.orgId = null;
    }

    const listParamsTargetIndicators = {
      ...request,
      ...this.paramTargetIndicators,
    };

    this.isLoading = true;
    const getTargetIndicators = this.targetAssessmentService
      .getSetOfTargetIndicators(listParamsTargetIndicators)
      .subscribe(
        (dataTargetIndicators: ResponseEntityDataAssessment<ISetOfIndicatorsData>) => {
          if (dataTargetIndicators) {
            this.listDataTargetIndicators = dataTargetIndicators?.data.content || [];
            this.pageable = {
              totalElements: dataTargetIndicators.data.totalElements,
              totalPages: dataTargetIndicators.data.totalPages,
              currentPage: dataTargetIndicators.data.number + 1,
              numberOfElements: dataTargetIndicators.data.numberOfElements,
              size: this.limit,
            };
          }
          this.isLoading = false;
        },
        (err) => {
          this.isLoading = false;
          this.toastrCustom.error(this.translate.instant('common.notification.error'));
        },
        () => {
          this.isLoading = false;
        }
      );

    this.subs.push(getTargetIndicators);
  }
  getInnerWidth(point: number): number {
    if (window.innerWidth > 767) {
      return window.innerWidth / point > 1100 ? 1100 : window.innerWidth / point;
    }
    return window.innerWidth;
  }

  showModalUploadFile(footerTmpl: TemplateRef<any>) {
    this.modalRef = this.modal.create({
      nzWidth: this.getInnerWidth(3.5),
      nzTitle: this.translate.instant('targerManager.targetCategory.label.atribute.UploadFileExel'),
      nzContent: ActionDocumentAttachedFileComponent,
      nzComponentParams: {
        mode: Mode.ADD,
        url: 'urlEndPointSetOfTarget',
        endpoint: 'END_POINT_GOAL_MANAGEMENT',
        microService: 'TARGET_ENDPOIN_ASSESSMENT',
        fileName: 'TEMPLATE_BO_CHI_TIEU'
      },
      nzFooter: footerTmpl,
    });
    this.modalRef.afterClose.subscribe((result) => (result?.refresh ? this.initData() : ''));
  }
  showModalSetOfCategory(footerTmpl: TemplateRef<any>, data?: any, event?: any, copy?: any) {
    if (event) {
      event.stopPropagation();
    }
    this.modalRef = this.modal.create({
      nzTitle: copy ? 'Sao chép bộ chỉ tiêu' : 'Bộ chỉ tiêu',
      nzWidth: '80vw',
      nzContent: FormSetOfIndicatorsComponent,
      nzFooter: footerTmpl,
      nzComponentParams: {
        data: data,
        copy: copy
      },
    });
    this.modalRef.afterClose.subscribe((result) => (result?.refresh ? this.initData() : ''));
  }

  showConfirm(event: any, data: any): void {
    event.stopPropagation();
    this.deletePopup.showModal(() => this.handleDeleteSetOfIndicator(data.sotId));
  }

  handleDeleteSetOfIndicator(id: number) {
    this.targetAssessmentService.deleteSetOfIndicator(id).subscribe(
      (res: ResponseEntityEmployTarget<null>) => {
        if (res.status === HTTP_STATUS_CODE.OK) {
          this.initData();
          this.toastrCustom.success(this.translate.instant('common.notification.deleteSuccess'));
        }
      },
      (error) => {
        this.isLoading = false;
        this.toastrCustom.error(error.message);
      }
    );
  }

  override ngOnDestroy() {
    this.modalRef?.destroy();
    this.subs.forEach((s) => s.unsubscribe());
  }
}
