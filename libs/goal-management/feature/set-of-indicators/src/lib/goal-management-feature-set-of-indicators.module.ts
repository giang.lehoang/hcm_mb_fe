import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SetOfIndicatorsComponent} from "./set-of-indicators/set-of-indicators.component";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzInputModule} from "ng-zorro-antd/input";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {NzTableModule} from "ng-zorro-antd/table";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbButtonIconModule} from "@hcm-mfe/shared/ui/mb-button-icon";
import {NzTagModule} from "ng-zorro-antd/tag";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbDropDowListLazySearchModule} from "@hcm-mfe/shared/ui/mb-drop-dow-list-lazy-search";
import {NzGridModule} from "ng-zorro-antd/grid";
import {RouterModule} from "@angular/router";

const COMMON = [SharedUiLoadingModule, SharedUiMbButtonModule, SharedUiMbSelectModule, SharedUiMbInputTextModule, SharedUiMbButtonIconModule,];
const ANT_DESIGN = [NzInputModule, NzTableModule, NzTagModule, NzPaginationModule, NzToolTipModule];

@NgModule({
  imports: [CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    ...COMMON,
    ...ANT_DESIGN, SharedUiMbDropDowListLazySearchModule, NzGridModule,
    RouterModule.forChild([{
      path: '',
      component: SetOfIndicatorsComponent
    }])
  ],
  declarations: [SetOfIndicatorsComponent],
  exports: [SetOfIndicatorsComponent],
})
export class GoalManagementFeatureSetOfIndicatorsModule {
}
