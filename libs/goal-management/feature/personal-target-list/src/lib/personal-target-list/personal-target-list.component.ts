import { Component, Injector, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {AppFunction, Pageable} from "@hcm-mfe/shared/data-access/models";
import {userConfig} from "@hcm-mfe/shared/common/constants";

import {
  CurrentUser,
  DataSource,
  FlagStatus,
  ParamSearch,
  ResponseEntityObject
} from "@hcm-mfe/goal-management/data-access/models";
import {CustomValidator} from "@hcm-mfe/shared/common/validators";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {PersonalTargetListService, PersonalTargetPlanService} from "@hcm-mfe/goal-management/data-access/services";


@Component({
  selector: 'app-personal-target-list',
  templateUrl: './personal-target-list.component.html',
  styleUrls: ['./personal-target-list.component.scss']
})
export class PersonalTargetListComponent extends  BaseComponent implements OnInit, OnDestroy {
  subs: Subscription[] = [];
  listWidth = ['5%', '15%', '22%', '10%', '10%', '15%', '8%', '15%'];
  isVisibleRejectDialog = false;
  isConfirmLoadingRejectDialog = false;
  formReject: FormGroup;
  override isLoading = false;
  isSubmitted = false;
  pageable: Pageable | undefined;
  limit: number = userConfig.pageSize;
  currentYear = moment().month() === 0 ?   moment().subtract(1, 'years').format('YYYY-MM-DD')  : moment(new Date()).format("YYYY-MM-DD");
  paramSearch: ParamSearch = {
    size: this.limit,
    page: 0,
    currentYear: null,
    periodYear: null,
    assignedType: null
  };
  dataSource: Array<DataSource> = [];
  currentUser: CurrentUser = {
    empCode: '',
    fullName: ''
  };
  dataReject: DataSource | undefined;
  listStatus = FlagStatus;
  constructor(
    injector : Injector,
    public personalTargetListService: PersonalTargetListService,
    public personalTargetPlanService: PersonalTargetPlanService,
  ) {
    super(injector);
    this.formReject = this.fb.group({
      reason: [null, [Validators.required, Validators.maxLength(250)]],
    },
      {
        validators: [
          CustomValidator.noWhitespaceValidator('reason', 'noAllowSpace'),
        ]
      });
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.PERSONAL_TARGET_LIST}`);
  }

  ngOnInit(): void {
    this.paramSearch.assignedType = "QL";
    this.personalTargetListService.getEmployee().subscribe((res: ResponseEntityObject<CurrentUser>) => {
      if (res && res.data) {
        this.currentUser.empCode = res.data.empCode;
        this.currentUser.fullName = res.data.fullName;
      }
    });
  }

  getList(): void {
    this.isLoading = true;
    this.dataSource = [];
    const ListTargetCategory = this.personalTargetListService.getList(this.paramSearch).subscribe(
      (res: ResponseEntityObject<Array<DataSource>>) => {
        if (res && res.data && res.data.length > 0) {
          this.dataSource = this.dataSource = this.convertData(res.data);
          this.limit = this.dataSource.length;
          this.pageable = {
            totalElements: this.dataSource.length,
            totalPages: 1,
            currentPage: 1,
            numberOfElements: this.dataSource.length,
            size: this.limit,
          };
        }
        this.isLoading = false;
      },
      (error) => {
        this.toastrCustom.error(error?.error ? error?.error?.message : error?.message);
        this.isLoading = false;
      }
    );
    this.subs.push(ListTargetCategory);
  }

  convertData(list: any) {
    const arr: Array<DataSource> = [];
    for (const item of list) {
      const temp = arr.find(m => m.empId === item.empId);
      if (temp) {
        temp.asmIds.push(item.asmId);
      } else {
        arr.push({
          ...item,
          asmIds: [item.asmId]
        });
      }
    }
    return arr;
  }


  changePage(event: number): void {
    if (event) {
      this.paramSearch.page = event - 1;
      this.getList();
    }
  }

  onChangeSelectYeah(event: any): void {
    if (event) {
      this.paramSearch.currentYear = moment(event).format("YYYY-MM-DD");
      this.paramSearch.periodYear = event ? moment(event).format("YYYY") : null;
      this.getList();
    }
  }

  goBack() {
    this.location.back()
  }

  redrirectToDetail(data: any) {
    this.personalTargetPlanService.setObjectDetailPersonalTargetList(data);
    this.personalTargetPlanService.setType('personal-target-list');
    this.router.navigateByUrl('/goal-management/periodic-goal-management/personal-target-list/details');
  }

  override ngOnDestroy(): void {
    this.subs.forEach((s) => s.unsubscribe());
  }

  updateStatus(data: DataSource, event: Event) {
    event.stopPropagation();
    this.isLoading = true;
    const dataUpdate = {
      asmIds: data.asmIds,
      status: this.listStatus.CHO_PHE_DUYET_GIAO
    };
    this.personalTargetListService.updateStatus(dataUpdate).subscribe(
      (res: ResponseEntityObject<number>) => {
        if (res) {
          this.toastrCustom.success(res.message);
          this.getList();
        }
        this.isLoading = false;
      },
      (error: any) => {
        this.isLoading = false;
        this.toastrCustom.error(error?.message ? error.error.message : error.error.error);
      });
  }


  acceptRejectDialog(): void {
    this.isSubmitted = true;
    if (this.formReject.valid) {
      this.isConfirmLoadingRejectDialog = true;
      const dataUpdate = {
        asmIds: this.dataReject?.asmIds,
        status: this.listStatus.CHUA_GUI_XAC_NHAN_GIAO,
        reason: this.formReject.controls['reason'].value
      };
      this.personalTargetListService.updateStatus(dataUpdate).subscribe((res: ResponseEntityObject<number>) => {
        if (res) {
          this.toastrCustom.success(res.message);
          this.isVisibleRejectDialog = false;
          this.isConfirmLoadingRejectDialog = false;
          this.isLoading = false;
          this.getList();
        }
      },
        (error: any) => {
          this.isLoading = false;
          this.toastrCustom.error(error.error.message ? error.error.message : error.error.error);

        });
    }
  }

  showRejectDialog(data: DataSource, event: Event): void {
    event.stopPropagation();
    this.isVisibleRejectDialog = true;
    this.dataReject = data;
  }

  cancelRejectDialog(): void {
    this.isVisibleRejectDialog = false;
  }

}
