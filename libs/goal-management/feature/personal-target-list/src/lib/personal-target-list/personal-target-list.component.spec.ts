import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalTargetListComponent } from './personal-target-list.component';

describe('PersonalTargetListComponent', () => {
  let component: PersonalTargetListComponent;
  let fixture: ComponentFixture<PersonalTargetListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonalTargetListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalTargetListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
