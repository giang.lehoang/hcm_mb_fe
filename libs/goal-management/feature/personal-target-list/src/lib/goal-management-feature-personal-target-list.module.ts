import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PersonalTargetListComponent} from "./personal-target-list/personal-target-list.component";
import {TranslateModule} from "@ngx-translate/core";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NzGridModule} from "ng-zorro-antd/grid";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzTagModule} from "ng-zorro-antd/tag";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzModalModule} from "ng-zorro-antd/modal";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {NzButtonModule} from "ng-zorro-antd/button";
import {RouterModule} from "@angular/router";
import {SharedPipesFormatScoreModule} from "@hcm-mfe/shared/pipes/format-score";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TranslateModule,
    ReactiveFormsModule,
    NzGridModule,
    SharedUiLoadingModule,
    SharedUiMbDatePickerModule,
    NzTableModule,
    NzTagModule,
    SharedUiMbButtonModule,
    NzModalModule,
    SharedUiMbInputTextModule,
    NzButtonModule,
    SharedPipesFormatScoreModule,
    RouterModule.forChild([
      {
        path: '',
        component: PersonalTargetListComponent,
      },
    ]),
  ],
  declarations: [
    PersonalTargetListComponent
  ],
  exports: [
    PersonalTargetListComponent
  ]
})
export class GoalManagementFeaturePersonalTargetListModule {}
