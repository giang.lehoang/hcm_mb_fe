import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RatingCategoryComponent} from "./rating-category/rating-category.component";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {TranslateModule} from "@ngx-translate/core";
import {RouterModule} from "@angular/router";
import {NzCardModule} from "ng-zorro-antd/card";
import {NzGridModule} from "ng-zorro-antd/grid";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {NzTableModule} from "ng-zorro-antd/table";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {NzModalModule} from "ng-zorro-antd/modal";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {MapPipe} from "@hcm-mfe/shared/pipes/map";
import {NzTagModule} from "ng-zorro-antd/tag";

@NgModule({
  imports: [
    CommonModule,
    SharedUiMbButtonModule,
    TranslateModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: RatingCategoryComponent,
      },
    ]),
    NzCardModule,
    NzGridModule,
    SharedUiMbSelectModule,
    NzTableModule,
    SharedUiLoadingModule,
    NzModalModule,
    SharedUiMbInputTextModule,
    NzTagModule,
  ],
  declarations: [RatingCategoryComponent],
  exports: [RatingCategoryComponent],
})
export class GoalManagementFeatureRatingCategoryModule {}
