import {Component, Injector, OnDestroy, OnInit} from "@angular/core";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {RatingCategoryService} from "../../../../../data-access/services/src/lib/rating-category.service";
import {
  flagStatus,
  IBodyRequest,
  IRatingCategory,
  ratingType
} from "../../../../../data-access/models/src/lib/IRatingCategory";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";

@Component({
  selector: 'app-rating-category',
  templateUrl: './rating-category.component.html',
  styleUrls: ['./rating-category.component.scss']
})
export class RatingCategoryComponent extends  BaseComponent implements OnInit, OnDestroy {
  isVisible = false;
  isHide = false;
  isSubmit = false;
  isUpdateModal = false;
  zeroNumber = 0;
  isDVList = false;
  listRating = ratingType();
  listStatus = flagStatus();
  param = {
    claType: this.listRating[0].value
  };
  bodyRequest: IBodyRequest = {
    claType: this.listRating[0].value,
    cldName: "",
    minPoint: null,
    maxPoint: null,
    displaySeq: 1,
    flagStatus: 1
  }
  listData: IRatingCategory[] = [];
  listDataModal: IRatingCategory[] = [];
  constructor (injector : Injector, readonly ratingCategoryService: RatingCategoryService) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RATING_CATEGORY}`);
    console.log(this.objFunction)
  }
  ngOnInit(): void {
    this.isLoading = true;
    this.initData();
  }

  initData() {
    this.ratingCategoryService.getListCategoryRating(this.param).subscribe(res => {
      this.listData = res.data;
      this.isDVList = this.param.claType === this.listRating[1].value;
      this.isLoading = false;
    }, (err) => {
      this.isLoading = false;
      this.listData = [];
    });
  }
  selectItem() {
    this.isHide = this.bodyRequest.claType !== this.listRating[1].value;
  }
  renderFlagStatus(flagStatus: number) {
    switch (flagStatus) {
      case 0:
        return {
          text: this.listStatus[1].name,
          class: 'tag-custom ant-tag-orange-custom',
        };
      case 1:
        return {
          text: this.listStatus[0].name,
          class: 'tag-custom ant-tag-green-custom',
        };
      default:
        return {
          text: '',
          class: '',
        };
    }
  }
  checkDuplicate() {
    const arr = [];
    for (const item of this.listDataModal) {
      if (item.cldName) {
        arr.push(item.cldName.trim());
      }
    }
    for (const item of this.listDataModal) {
      const arrTemp = arr.filter((m) => m.trim() === item.cldName.trim());
      if (arrTemp.length > 1) {
        item['duplicateName'] = true;
      } else {
        item['duplicateName'] = false;
      }
    }
  }
  formatPoint(value: any, point: string, data: any) {
    if (point === 'min') {
      data.minPoint = +value;
      if (!this.listData.length) {
        data.maxPoint = null;
      }
      const numMin = (data.minPoint + '').split('.')[1];
      if (numMin && +numMin[0] !== 5) {
        data.minPoint = +Number.parseFloat(value).toFixed(0);
      }
    } else {
      data.maxPoint = +value;
      const numMax = (data.maxPoint + '').split('.')[1];
      if (numMax && +numMax[0] !== 5) {
        data.maxPoint = +Number.parseFloat(value).toFixed(0);
      }
    }
  }
  showModalUpdate() {
    this.isUpdateModal = true;
    this.isVisible = true;
    this.listDataModal =  JSON.parse(JSON.stringify(this.listData));
  }
  showModalCreate() {
    this.isUpdateModal = false;
    this.isVisible = true;
    this.bodyRequest.claType = this.param.claType;
    if (this.listData.length) {
      if (this.bodyRequest.claType === this.listRating[0].value) {
        this.bodyRequest.maxPoint = this.listData[this.listData.length - 1].minPoint !== 0 ? this.listData[this.listData.length - 1].minPoint : 0;
      }
    } else {
      this.bodyRequest.maxPoint = 0;
    }
  }
  setValuePoint(value: any, data: any, type?: any) {
    if(type) {
      data.minPoint = value;
    } else {
      data.maxPoint = value;
    }
  }
  handleCancel(): void {
    this.isVisible = false;
    this.isSubmit = false;
    this.bodyRequest.cldName = '';
    this.bodyRequest.minPoint = 0;
    this.bodyRequest.maxPoint = 0;
    this.listDataModal =  JSON.parse(JSON.stringify(this.listData));
  }
  save(): void {
    this.isSubmit = true;
    if (!this.bodyRequest.claType || !this.bodyRequest.cldName) {
      return;
    }
    const checkPoint = this.bodyRequest.minPoint !== null && this.bodyRequest.maxPoint !== null ? this.bodyRequest.minPoint >= this.bodyRequest.maxPoint : '';
    if (this.bodyRequest.claType === this.listRating[0].value && this.listData.length && (checkPoint || !this.bodyRequest.minPoint?.toString())) {
      return;
    }
    this.isLoading = true;
    this.isVisible = false;
    this.bodyRequest.cldName = this.bodyRequest.cldName.trim();
    this.bodyRequest.minPoint = this.bodyRequest.minPoint !== null ? +this.bodyRequest.minPoint : null;
    this.bodyRequest.maxPoint = this.bodyRequest.maxPoint !== null ? +this.bodyRequest.maxPoint : null;
    const request = this.bodyRequest.claType === this.listRating[0].value ? {...this.bodyRequest} : {...this.bodyRequest, minPoint: null, maxPoint: null}
    this.ratingCategoryService.createCategoryRating(request).subscribe(res => {
      this.isLoading = false;
      this.toastrCustom.success(this.translate.instant('common.notification.addSuccess'));
      this.param.claType = this.bodyRequest.claType;
      this.bodyRequest.cldName = '';
      this.bodyRequest.minPoint = 0;
      this.bodyRequest.maxPoint = 0;
      this.search();
      this.isSubmit = false;
    }, (err) => {
      this.isSubmit = false;
      this.isLoading = false;
      this.bodyRequest.cldName = '';
      if (err.status === 500) {
        this.toastrCustom.error(this.translate.instant('common.notification.addError'));
        return;
      }
      this.toastrCustom.error(err?.message);
    })
  }
  update() {
    this.isSubmit = true;
    const invalid = this.listDataModal.some(el => el.duplicateName === true);
    const requiredPoint = this.listDataModal.some(el => !el.minPoint && el.minPoint !== 0);
    const isValidPoint = this.listDataModal.some(el => el.isValidPoint === true);
    const invaliDMaxPoint = this.listDataModal.some(el => el.invalidMaxValue === true);
    if (this.param.claType === this.listRating[0].value) {
      if (invalid) {
        this.toastrCustom.error(this.translate.instant('goaManagement.ratingCategory.error.duplicateName'));
        return;
      }
      if (requiredPoint) {
        this.toastrCustom.error(this.translate.instant('goaManagement.ratingCategory.error.requiredMinPoint'));
        return;
      }
      if (isValidPoint) {
        this.toastrCustom.error(this.translate.instant('goaManagement.ratingCategory.error.invalidMinPoint'));
        return;
      }
      if (invaliDMaxPoint) {
        this.toastrCustom.error(this.translate.instant('goaManagement.ratingCategory.error.invalidMaxPoint1'));
        return;
      }
    }
    this.listDataModal.forEach(el => {
      delete el.invalidMaxValue;
      delete el.duplicateName;
      delete el.isValidPoint;
      el.cldName = el.cldName.trim();
      el.minPoint = +el.minPoint;
      el.maxPoint = el.maxPoint === null ? null : +el.maxPoint;
    });
    const tempData = JSON.stringify(this.listData);
    const bodyRequest = {
      isChangeInfo: JSON.stringify(this.listDataModal) === tempData ? "NO" : "YES",
      claType: this.param.claType,
      dtos: [...this.listDataModal]
    };
    this.isLoading = true;
    this.isVisible = false;
    this.ratingCategoryService.updateCategoryRating(bodyRequest).subscribe(res => {
      this.isLoading = false;
      this.toastrCustom.success(this.translate.instant('common.notification.updateSuccess'));
      this.bodyRequest.cldName = '';
      this.bodyRequest.minPoint = 0;
      this.bodyRequest.maxPoint = 0;
      this.search();
      this.isSubmit = false;
    }, (err) => {
      this.isLoading = false;
      if (err.status === 500) {
        this.toastrCustom.error(this.translate.instant('common.notification.updateError'));
        return;
      }
      this.toastrCustom.error(err?.message);
    })
  }
   processPoint(data: any, index: number) {
    if (index !== 0) {
      data.isValidPoint = data.maxPoint !== null && data.minPoint >= data.maxPoint;
    }
    // this.validateMaxPoint(data, index);
      if (this.listDataModal[index + 1]) {
        if (data.flagStatus) {
          this.listDataModal[index + 1].maxPoint = data?.minPoint;
        }
        // @ts-ignore
        if (this.listDataModal[index + 1].maxPoint < this.listDataModal[index + 1].minPoint) {
          for (let i = index + 1; i < this.listDataModal.length ;i++) {
            this.listDataModal[i].minPoint = 0;
            if (i !== index + 1) {
              this.listDataModal[i].maxPoint = 0;
            }
          }
        }
      }
      // else {
      //   if(this.listDataModal[index + 1]) {
      //     this.listDataModal[index + 1].maxPoint = 0;
      //   }
      // }
  }
  handlePointValue(value: any, point: any, data: any, index: any ) {
    this.formatPoint(value, point, data);
    this.processPoint(data, index);
  }
  // validateMaxPoint(data: any, index: number) {
  //   if (index === 0 ) {
  //     return;
  //   }
  //   for (let i = index - 1; i < this.listDataModal.length; i--) {
  //     if (this.listDataModal[i] && this.listDataModal[i].flagStatus) {
  //       data.invalidMaxValue = data.maxPoint !== this.listDataModal[i]?.minPoint - 0.5;
  //       break;
  //     }
  //   }
  // }
  calculatePoint(value: number, index: number) {
    if (value === 0) {
      let point;
      for (let i = index - 1; i >= 0; i--) {
            if (this.listDataModal[i] && this.listDataModal[i].flagStatus) {
              point = this.listDataModal[i].minPoint;
              break;
            }
          }
      for (let j = index + 1; j < this.listDataModal.length; j++) {
        if (this.listDataModal[j] && this.listDataModal[j].flagStatus && point) {
          this.listDataModal[j].maxPoint = point;
          break;
        } else {
          this.listDataModal[j].maxPoint = null;
        }
      }
    }
    if (value === 1 && this.listDataModal[index + 1]) {
      this.listDataModal[index + 1].maxPoint = this.listDataModal[index].minPoint;
    }
  }
  convertZeroNumber(value: number): string {
    return this.param.claType === this.listRating[0].value && this.listData.length ? value + '' : '';
  }
  search() {
    this.isLoading = true;
    this.initData();
  }

  override triggerSearchEvent(): void {
    this.search();
  }
}
