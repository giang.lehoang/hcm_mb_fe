import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {GiveCriteriaComponent} from "./give-criteria/give-criteria.component";
import {NzGridModule} from "ng-zorro-antd/grid";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzCardModule} from "ng-zorro-antd/card";
import {NzTableModule} from "ng-zorro-antd/table";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {FormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {SharedPipesSortPipeModule} from "@hcm-mfe/shared/pipes/sort-pipe";
import {TranslateModule} from "@ngx-translate/core";
import {NzInputModule} from "ng-zorro-antd/input";
import {NzIconModule} from "ng-zorro-antd/icon";
import {SharedUiMbModalConfirmModule} from "@hcm-mfe/shared/ui/mb-modal-confirm";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedDirectivesNumbericModule} from "@hcm-mfe/shared/directives/numberic";
import {SharedPipesConvertNumbertoString0Module} from "@hcm-mfe/shared/pipes/convert-numberto-string0";

@NgModule({
  imports: [
    CommonModule,
    NzGridModule,
    SharedUiMbButtonModule,
    NzCardModule,
    NzTableModule,
    SharedUiMbInputTextModule,
    FormsModule,
    SharedPipesSortPipeModule,
    TranslateModule,
    NzInputModule,
    NzIconModule,
    SharedUiMbModalConfirmModule,
    SharedUiLoadingModule,
    SharedDirectivesNumbericModule,
    SharedPipesConvertNumbertoString0Module,
    RouterModule.forChild([
      {
        path: '',
        component: GiveCriteriaComponent,
      },
    ]),
  ],
  exports: [
    GiveCriteriaComponent
  ],
  declarations: [
    GiveCriteriaComponent
  ]
})
export class GoalManagementFeatureGiveCriteriaModule {}
