import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GiveCriteriaComponent } from './give-criteria.component';

describe('GiveCriteriaComponent', () => {
  let component: GiveCriteriaComponent;
  let fixture: ComponentFixture<GiveCriteriaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GiveCriteriaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GiveCriteriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
