import {Component, Injector, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {
  getActionRejectorAprrove,
  IEmployData,
  ITargetPlanStaff,
  listFlagStatus,
  processFlagStatus, ResponseEntityEmployTarget
} from "@hcm-mfe/goal-management/data-access/models";
import {ApprovePlanAssignmentService, GetCriteriaService} from "@hcm-mfe/goal-management/data-access/services";
import {FunctionCode, SessionKey} from "@hcm-mfe/shared/common/enums";
import {ModalConfirmComponent} from "@hcm-mfe/shared/ui/mb-modal-confirm";
import {User} from "@hcm-mfe/system/data-access/models";

@Component({
  selector: 'app-give-criteria',
  templateUrl: './give-criteria.component.html',
  styleUrls: ['./give-criteria.component.scss']
})
export class GiveCriteriaComponent extends BaseComponent implements OnInit, OnDestroy {
  arrayParent: Array<ITargetPlanStaff> = [];
  isDirectManager: boolean | undefined;
  targetPlanStaffItem: ITargetPlanStaff | undefined;
  arrayChild: Array<ITargetPlanStaff> = [];
  listNotUnique: Array<ITargetPlanStaff> = [];
  listAsmIds: Array<number> |undefined;
  listResult: any;
  listResultChild: any;
  listResutYear: Array<ITargetPlanStaff> | undefined;
  infoEmployee:any;
  disabled = false;
  readonly subs: Subscription[] = [];
  processFlagStatus: listFlagStatus | null = null;
  paramApprovePlan: IEmployData = {
    asmIds: undefined,
    empCode: undefined,
    managerId: undefined,
    content: undefined,
    action: undefined,
    flagStatus: processFlagStatus().CHO_PHE_DUYET_GIAO
  };

  currentUser: User | undefined;
  @ViewChild('modalCompReject') modalCompReject: ModalConfirmComponent | undefined;

  constructor(readonly getCriteriaService: GetCriteriaService, injector: Injector,
              private readonly approvePlanAssignmentService: ApprovePlanAssignmentService) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.GIVE_CRITERIA}`);
    this.processFlagStatus = processFlagStatus();
  }

  getValueEstimateNum(event: any, data: any, key: any, index?: number) {
    if (key === 'parent') {
      this.makeResulData(this.listResult, data, event);
    } else if (key === 'child') {
      this.makeResulData(this.listResultChild, data, event);
    }
  }

  getValueDescription(event: any, data: any) {
    if (event?.target?.value !== '') {
      this.listNotUnique.forEach((item) => {
        if (item.stdId === data.stdId) {
          item['description'] = event.target.value;
        }
      });
    }
  }

  makeResulData(objectData: any, data: any, value: any) {
    Object.keys(objectData).forEach((keyObject) => {
      if (parseInt(keyObject, 10) === data.stdId) {
       // data.estimateNum = value;
        if(String(value) === '' || value === null ){
          data.estimateNum = value;
        }else if(value !== null && value >= 0){
          data.estimateNum = Number(value);
        }
      }
    });
  }
  approvePlanAssignment(): void {
    this.isLoading = true;
    const listDeTail = this.getCriteriaService.getInformationData() as IEmployData;
    const { asmIds, empCode, managerId, content } = listDeTail;
    this.paramApprovePlan = {
      ...this.paramApprovePlan,
      asmIds,
      empCode,
      managerId,
      content,
      action: getActionRejectorAprrove().approve
    };
    this.approvePlanAssignmentService.approvePlanAssignment(this.paramApprovePlan).subscribe((res: ResponseEntityEmployTarget<null>) => {
      if (res.status === 200) {
        this.isLoading = false;
        this.toastrCustom.success(this.translate.instant('targerManager.personalTargetList.label.table.completedApproveAssign'));
        this.router.navigateByUrl('goal-management/periodic-goal-management/employ-target-list');
      } else {
        this.isLoading = false;
      }
    }, (err) => {
      this.isLoading = false;
      this.toastrCustom.error(`${this.translate.instant('common.notification.error')} : ${err.message}`);
    });
  }

  rejectPlanAssignment($event: string): void {
    this.paramApprovePlan = {
      ...this.paramApprovePlan,
      content: $event
    };
    const subApprovePlanAssign = this.approvePlanAssignmentService.approvePlanAssignment(this.paramApprovePlan).subscribe((res: ResponseEntityEmployTarget<null>) => {
      if (res.status === 200) {
        this.toastrCustom.success(this.translate.instant('targerManager.personalTargetList.label.table.rejectAssign'));
        this.modalCompReject?.formSubmit.patchValue({
          content: null
        });
        if (this.modalCompReject) {
          this.modalCompReject.isVisibleCorrectionDialog = false;
        }
        this.router.navigateByUrl('goal-management/periodic-goal-management/employ-target-list');
      }
    }, (err) => {
      this.toastrCustom.error(`${this.translate.instant('common.notification.error')} : ${err.message}`);
    });
    this.subs.push(subApprovePlanAssign);
  }
  renderFlagStatus(flagStatus: number): string {
    switch (flagStatus) {
      case 11:
        return 'Chưa gửi xác nhận giao';
      case 12:
        return 'Chờ xác nhận giao';
      case 13:
        return 'Chờ phê duyệt giao';
      case 14:
        return 'Hoàn tất phê duyệt giao';
      default:
        return '';
    }
  }
  saveGiveCriteria() {
    const PUTGIVECRITERIA = this.getCriteriaService.saveGiveCriteria(this.processRequestBody()).subscribe(
      (data) => {
        this.toastrCustom.success('Cập nhật thành công');
        this.router.navigateByUrl('/goal-management/periodic-goal-management/employ-target-list');
      },
      (err) => {
        this.toastrCustom.error(err.error.message);
      }
    );
    this.subs.push(PUTGIVECRITERIA);
  }

  processRequestBody() {
    const arraySubmit: any[] = [];
    this.arrayParent.forEach((item) => {
      this.listResult[item.stdId].forEach((ele: any) => {
        arraySubmit.push(ele);
      });
    });
    this.arrayChild.forEach((item) => {
      this.listResultChild[item.stdId].forEach((ele: any) => {
        arraySubmit.push(ele);
      });
    });
    return arraySubmit;
  }
  showModalRejectPlan(): void {
    const listDeTail = this.getCriteriaService.getInformationData() as IEmployData;
    if (this.modalCompReject) {
      this.modalCompReject.isVisibleCorrectionDialog = true;
    }
    const { asmIds, empCode, managerId } = listDeTail;
    this.paramApprovePlan = {
      asmIds,
      empCode,
      managerId,
      content: undefined,
      action: getActionRejectorAprrove().reject,
      flagStatus: processFlagStatus().CHO_PHE_DUYET_GIAO
    };
  }

  sendEmployee() {
    const requestBody = {
      asmIds: [],
      asmDetailDTOS: [],
    };
    let isValid = false;
    const arraySubmit: any[] = [];
    this.arrayParent.forEach((item) => {
      this.listResult[item.stdId] &&
        this.listResult[item.stdId].forEach((ele: any) => {
          if (Number(ele.typeRule) === 1 || ele.scale === 0) {
            ele.error = '';
          } else if (
              ele.estimateNum === null && (!item.subs || item.subs.length === 0)
              || String(ele.estimateNum) === '' && (!item.subs || item.subs.length === 0)
          ) {
            ele.error = 'Giao chỉ tiêu bắt buộc nhập';
            isValid = true;
          } else {
            ele.error = '';
          }
          arraySubmit.push(ele);
        });
    });
    this.arrayChild.forEach((item) => {
      this.listResultChild[item.stdId] &&
        this.listResultChild[item.stdId].forEach((eleSubChild: any) => {
          if (Number(eleSubChild.typeRule) === 1 || Number(eleSubChild.scale) === 0) {
            eleSubChild.error = '';
          } else if (eleSubChild.estimateNum === null || String(eleSubChild.estimateNum) === '') {
            eleSubChild.error = 'Giao chỉ tiêu bắt buộc nhập';
            isValid = true;
          } else {
            eleSubChild.error = '';
          }
          arraySubmit.push(eleSubChild);
        });
    });

    if (!isValid) {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      requestBody.asmIds = this.listAsmIds;
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      requestBody.asmDetailDTOS = arraySubmit;
      const SENDMAIL = this.getCriteriaService.sendEmail(requestBody).subscribe(
        (data) => {
          this.toastrCustom.success(
            this.translate.instant('targerManager.common.notification.deliveringSetOfSuccessTargets')
          );
          this.router.navigateByUrl('/goal-management/periodic-goal-management/employ-target-list');
        },
        (err) => {
          this.toastrCustom.error(err?.message ? err?.message : err?.error?.message);
        }
      );
      this.subs.push(SENDMAIL);
    }
  }

  goBackPage() {
    this.location.back();
  }

  ngOnInit(): void {
    const listDeTail = this.getCriteriaService.getInformationData() as IEmployData;
    if (localStorage.getItem(SessionKey.CURRENCY_USER)) {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      this.currentUser = JSON.parse(localStorage.getItem(SessionKey.CURRENCY_USER));
    }

    if (Object.keys(listDeTail).length === 0) {
      this.router.navigateByUrl('/goal-management/periodic-goal-management/employ-target-list');
    }
    this.listAsmIds = listDeTail.asmIds;
    const params = {
      empId: listDeTail.empId ? listDeTail.empId : null,
      sotId: listDeTail.sotId ? listDeTail.sotId : null,
      periodYear: listDeTail.periodYear ? listDeTail.periodYear : null,
    };

    const GETCRITERIA = this.getCriteriaService.getListTargetPlanStaff(params).subscribe(
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      (data: any) => {
        this.isDirectManager = data.data.isDirectManager;
        this.targetPlanStaffItem = data?.data;

        const cloneArray = [...data.data.asmDetailDTOS];
        data.data.asmDetailDTOS.forEach((item: any) => {
          item.scale = Math.round(item.scale * 100 * 100) / 100;
        });
        this.listNotUnique = [...cloneArray];

        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        this.listResutYear = [
          ...new Map(data.data.asmDetailDTOS.map((item: any) => [item['periodId'], item])).values(),
        ].sort((a: any, b: any) => (a?.periodId > b.periodId ? -1 : 1));

        const result = data.data.asmDetailDTOS
          .filter((item: any) => item.parentId === null)

          .reduce((rv: any, x: any) => {

            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            for (let index = 0; index < this.listResutYear.length; index++) {
              // eslint-disable-next-line @typescript-eslint/ban-ts-comment
              // @ts-ignore
              if (this.listResutYear[index]) {
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                // @ts-ignore
                if (x.periodId === this.listResutYear[index].periodId) (rv[x['stdId']] = rv[x['stdId']] || []).push(x);
              }
            }

            return rv;
          }, {});
        const resultChild = data.data.asmDetailDTOS
          .filter((item: any) => item.parentId !== null)
          .reduce((rv: any, x: any) => {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            for (let index = 0; index < this.listResutYear.length; index++) {
              // eslint-disable-next-line @typescript-eslint/ban-ts-comment
              // @ts-ignore
              if (this.listResutYear[index]) {
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                // @ts-ignore
                if (x.periodId === this.listResutYear[index].periodId) (rv[x['stdId']] = rv[x['stdId']] || []).push(x);
              }
            }

            return rv;
          }, {});
        this.listResultChild = resultChild;
        this.listResult = result;
        const arrayUniqueByKey = [...new Map(data.data.asmDetailDTOS.map((item: any) => [item['stdId'], item])).values()];
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        this.arrayParent = arrayUniqueByKey.filter((item: ITargetPlanStaff) => item.parentId === null);
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        this.arrayChild = arrayUniqueByKey.filter((item: ITargetPlanStaff) => item.parentId !== null);
        this.arrayParent.forEach((itemParent) => {
          itemParent.subs = [];
          this.arrayChild.forEach((itemChild) => {
            if (itemChild.parentId === itemParent.stdId) {
              itemParent.subs?.push(itemChild);
            }
          });
        });
        this.arrayParent = this.getArrayOrderByDisplaySeqAndScale([...this.arrayParent]);
        for (let index = 0; index <= this.arrayParent.length; index++) {
          if (this.arrayParent[index]?.subs) {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            this.arrayParent[index].subs = this.getArrayOrderByDisplaySeqAndScale([...this.arrayParent[index].subs]);
          }
        }
        this.infoEmployee = data.data
        console.log('arrayParent', this.arrayParent);

        if (data.data.flagStatus !== 11) {
          this.disabled = true;
        } else {
          this.disabled = false;
        }
      },
      (err) => {
        this.toastrCustom.error(err.error.message);
      }
    );
    this.subs.push(GETCRITERIA);
  }

  private getArrayOrderByDisplaySeqAndScale(arrTemp: ITargetPlanStaff[]) {
    return arrTemp.sort((a: ITargetPlanStaff, b: ITargetPlanStaff) => {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      if (a.displaySeq > b.displaySeq) {
        return 1;
      } else if (a.displaySeq === b.displaySeq) {
        if (a.scale > b.scale) {
          return -1;
        } else return 1;
      }
      return -1;
    });
  }

  override ngOnDestroy() {
    this.subs.forEach((s) => s.unsubscribe());
  }
}
