import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignTargetToAllEmployeesComponent } from './assign-target-to-all-employees.component';

describe('AssignTargetToAllEmployeesComponent', () => {
  let component: AssignTargetToAllEmployeesComponent;
  let fixture: ComponentFixture<AssignTargetToAllEmployeesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AssignTargetToAllEmployeesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignTargetToAllEmployeesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
