import { Component, Injector, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import * as _ from 'lodash';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {
  ColumnItem,
  Dto,
  Subs,
  IAssignTargetToAllEmployees,
  IEmployData,
  IPeriodInfos, ISendEmployeeSub, IRequest
} from "@hcm-mfe/goal-management/data-access/models";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {AssignTargetToAllEmployeesService} from "@hcm-mfe/goal-management/data-access/services";
import * as moment from "moment";

@Component({
    selector: 'app-assign-target-to-all-employees',
    templateUrl: './assign-target-to-all-employees.component.html',
    styleUrls: ['./assign-target-to-all-employees.component.scss'],
})
export class AssignTargetToAllEmployeesComponent extends BaseComponent implements OnInit {

    constructor(
        injector: Injector,
        private readonly assignTargetToAllEmployeesService: AssignTargetToAllEmployeesService,
    ) {
        super(injector);
        this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ASSIGN_TARGET_TO_ALL_EMPLOYEES}`);
    }

    private readonly notificationError = 'common.notification.error';
  override isLoading = false;
    paramSearchEmployPagination: IEmployData = {
        empCode: undefined,
        fullName: undefined,
        posId: undefined,
        periodYear: moment().month() === 0 ? moment().year() - 1 : new Date().getFullYear()
    };

    InitColumnHeaderTable: ColumnItem[] = [
        {
            name: this.translate.instant('STT'),
            nzWidth: '4.5em',
        },
        {
            name: this.translate.instant('goaManagement.assign-target-to-all-employees.label.table.employeeCode'),
        },
        {
            name: this.translate.instant('goaManagement.assign-target-to-all-employees.label.table.fullName'),
            sortOrder: null,
            sortFn: (a: Dto, b: Dto) => a.fullName.localeCompare(b.fullName),
            sortDirections: ['ascend', 'descend']
        },
        {
            name: this.translate.instant('goaManagement.assign-target-to-all-employees.label.table.setOfIndicators'),
            nzWidth: '15%'
        },
        {
            name: this.translate.instant('goaManagement.assign-target-to-all-employees.label.table.scoreCardName'),
        },
        {
            name: this.translate.instant('goaManagement.assign-target-to-all-employees.label.table.targetName'),
            nzWidth: '20%',
            sortFn: (a: Dto, b: Dto) => a.tarName.localeCompare(b.tarName),
            sortDirections: ['ascend', 'descend']
        },
        {
            name: this.translate.instant('goaManagement.assign-target-to-all-employees.label.table.density'),
            nzWidth: '6.5em',
        },
        {
          name: this.translate.instant('goaManagement.assign-target-to-all-employees.label.table.typeKpi'),
          nzWidth: '6.5em',
        }
    ];

    private readonly subs: Subscription[] = [];
    listDataEmployeeAssign: IAssignTargetToAllEmployees | undefined;

    receiveDataEmployee($event: IEmployData) {
        this.paramSearchEmployPagination = $event;
        this.search();
    }

    setValueEstimateNum($event: number, dataParentItem: Dto, el: IPeriodInfos){
        if(String($event) === '' || $event === null ){
            el.estimateNum = $event;

        }else if($event !== null && $event >= 0){
            el.estimateNum = Number($event);

        }
    }

    setValueDescription(value: string, dataParent: any){
        dataParent.description = value;
    }

    search($event?: any) {
        this.isLoading = true;
        const { empCode, fullName, periodYear, posId } = this.paramSearchEmployPagination;
        this.paramSearchEmployPagination = {
            empCode,
            fullName,
            posId,
            periodYear
        };
        const subSearchEmployees = this.assignTargetToAllEmployeesService.getListEmployeeTargetAssigned(this.paramSearchEmployPagination)
            .subscribe((res: any) => {
                    if (res.status === HTTP_STATUS_CODE.OK) {

                        const convertDtoEstimateNumToString = _.cloneDeep(res.data?.dto);
                        this.listDataEmployeeAssign = { ...res.data, dto: convertDtoEstimateNumToString } || [];
                    }
                }, (err: any) => {
                    this.isLoading = false;
                    this.toastrCustom.error(`${this.translate.instant(this.notificationError)} : ${err.message}`);
                },
                () => this.isLoading = false
            );
        this.subs.push(subSearchEmployees);
    }

    ngOnInit(): void {
        this.search();
    }

  override ngOnDestroy() {
        this.subs.forEach((s) => s.unsubscribe());
    }

    sendEmployees(){
        const cloneDataEmployeeAssign = _.cloneDeep(this.listDataEmployeeAssign?.dto);
        let isValidValidate = true;

        // validate employee and alert message
      this.listDataEmployeeAssign
      && this.listDataEmployeeAssign.dto?.length > 0
      && this.listDataEmployeeAssign?.dto.forEach(((elParent: Dto, indexParent: number) => {
            if(!elParent.subs && Number(elParent.targetScale) !== 0){
                elParent?.periodInfos.forEach((itemInfo: IPeriodInfos, indexItemInfo: number) => {
                    if(
                        String(itemInfo?.estimateNum) === ''
                        || itemInfo?.estimateNum === null
                        || itemInfo?.isHavePeriod && itemInfo?.estimateNum < 0
                    ){
                      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                      // @ts-ignore
                      this.listDataEmployeeAssign.dto[indexParent].periodInfos[indexItemInfo].errMessage = true;
                        isValidValidate = false;
                      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                    }else { // @ts-ignore
                      this.listDataEmployeeAssign.dto[indexParent].periodInfos[indexItemInfo].errMessage = false;
                    }
                })
            }

        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        elParent?.subs?.length > 0 && elParent.subs?.forEach((elSub: Subs, indexSub: number) => {
                elSub.periodInfos?.length > 0 && Number(elSub?.targetScale) && elSub.periodInfos.forEach((elSubItem:IPeriodInfos, indexSubItem: number) => {
                    if(
                        String(elSubItem?.estimateNum) === ''
                        || elSubItem?.estimateNum === null
                        || elSubItem.isHavePeriod && elSubItem?.estimateNum < 0
                    ){
                      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                      // @ts-ignore
                        this.listDataEmployeeAssign.dto[indexParent].subs[indexSub].periodInfos[indexSubItem].errMessage = true;
                        isValidValidate = false;
                      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                      // @ts-ignore
                    }else this.listDataEmployeeAssign.dto[indexParent].subs[indexSub].periodInfos[indexSubItem].errMessage = false;
                });
            });
        }));

        const tempArrEmployeeParameter: any[] = [];
        this.listDataEmployeeAssign?.dto.forEach((el: Dto) => {
            if(!el.subs && el.periodInfos?.length > 0){
                const objTempSub: ISendEmployeeSub = { requests: [], empCode: null, fullName: null };
                objTempSub.empCode = el?.empCode;
                objTempSub.fullName = el?.fullName;

                el?.periodInfos.forEach((elChild: IPeriodInfos) => {
                    if (elChild?.isHavePeriod) {
                        objTempSub.requests.push({
                            asmId: elChild?.asmId,
                            asdId: elChild?.asdId,
                            estimateNum: elChild?.estimateNum,
                            periodId: elChild?.periodId,
                            periodName: elChild?.periodName,
                            description: el?.description,
                        });
                    }
                });
                tempArrEmployeeParameter.push(objTempSub);
            }
            if(el.subs && el.subs.length > 0){
                el.subs.forEach((elSub: Subs) => {
                    const objTempSub: ISendEmployeeSub = { requests: [], empCode: null, fullName: null };
                    objTempSub.empCode = el?.empCode;
                    objTempSub.fullName = el?.fullName;

                    elSub?.periodInfos.forEach((elChild: IPeriodInfos) => {
                        if (elChild?.isHavePeriod) {
                            objTempSub.requests.push({
                                asdId: elChild?.asdId,
                                asmId: elChild?.asmId,
                                estimateNum: elChild?.estimateNum,
                                periodId: elChild?.periodId,
                                periodName: elChild?.periodName,
                                description: elSub?.description,
                            });
                        }
                    });
                    tempArrEmployeeParameter.push(objTempSub);
                });
            }
        });

        if(isValidValidate){
            this.isLoading = true;
            const saveEmployee = this.assignTargetToAllEmployeesService.sendListEmployeeTargetAssign(tempArrEmployeeParameter)
                .subscribe((res: any) => {
                    if (res.status === HTTP_STATUS_CODE.OK) {
                        this.toastrCustom.success(this.translate.instant(`goaManagement.assign-target-to-all-employees.notification.SetGoalsForSuccess`));
                        this.router.navigateByUrl('/goal-management/periodic-goal-management/employ-target-list');
                    }
                }, (err: any) => {
                    this.isLoading = false;
                    this.toastrCustom.error(`${this.translate.instant(this.notificationError)} : ${err.message}`);
                },
                () => this.isLoading = false
            );
            this.subs.push(saveEmployee);
        }else {
            this.toastrCustom.error(this.translate.instant(`goaManagement.assign-target-to-all-employees.notification.targetNameIsRequired`));
        }
    }

    saveEmployees(){
        const cloneDataEmployeeAssign = _.cloneDeep(this.listDataEmployeeAssign?.dto);
        const tempArrEmployee: any = [];
        cloneDataEmployeeAssign?.forEach((el: Dto, index: number) => {
            if(!el.subs && el.periodInfos?.length > 0){
                this.extractedParamSaveEmployee(el, tempArrEmployee);
            }
            if(el.subs && el.subs.length > 0){
                el.subs.forEach((elSub: Subs) => {
                    this.extractedParamSaveEmployee(elSub, tempArrEmployee);
                })
            }
        });
        this.isLoading = true;

        const saveEmployee = this.assignTargetToAllEmployeesService.saveListEmployeeTargetAssign(tempArrEmployee).subscribe((res: any) => {
                if (res.status === HTTP_STATUS_CODE.OK) {
                    this.toastrCustom.success(this.translate.instant('common.notification.updateSuccess'));
                    this.router.navigateByUrl('/goal-management/periodic-goal-management/employ-target-list');
                    // this.search();
                }
            }, (err: any) => {
                this.isLoading = false;
                this.toastrCustom.error(`${this.translate.instant(this.notificationError)} : ${err.message}`);
            },
            () => this.isLoading = false
        );
        this.subs.push(saveEmployee);
    }

    private extractedParamSaveEmployee(el: Dto | Subs, tempArrEmployee: any[]) {
        const objTemp: { requests: IRequest[] } = { requests: [] };
        el.periodInfos.forEach((elPeriod: IPeriodInfos) => {
            if (elPeriod.isHavePeriod) {
                objTemp.requests.push({
                    asdId: elPeriod.asdId,
                    estimateNum: elPeriod.estimateNum,
                    description: el.description
                });
            }
        });
        tempArrEmployee.push(objTemp);
    }
}
