import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  AssignTargetToAllEmployeesComponent
} from "./assign-target-to-all-employees/assign-target-to-all-employees.component";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {GoalManagementUiFormInputModule} from "@hcm-mfe/goal-management/ui/form-input";
import {NzTableModule} from "ng-zorro-antd/table";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {FormsModule} from "@angular/forms";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
import {RouterModule} from "@angular/router";
import {SharedDirectivesNumbericModule} from "@hcm-mfe/shared/directives/numberic";
import {SharedPipesConvertNumbertoString0Module} from "@hcm-mfe/shared/pipes/convert-numberto-string0";

@NgModule({
    imports: [
        CommonModule,
        SharedUiLoadingModule,
        GoalManagementUiFormInputModule,
        NzTableModule,
        TranslateModule,
        SharedUiMbButtonModule,
        SharedUiMbInputTextModule,
        SharedPipesConvertNumbertoString0Module,
      SharedDirectivesNumbericModule,
      FormsModule,
        NzToolTipModule,
        RouterModule.forChild([
            {
                path: '',
                component: AssignTargetToAllEmployeesComponent,
            },
        ]),
    ],
  exports: [
    AssignTargetToAllEmployeesComponent
  ],
  declarations: [
    AssignTargetToAllEmployeesComponent
  ]
})
export class GoalManagementFeatureAssignTargetToAllEmployeesModule {}
