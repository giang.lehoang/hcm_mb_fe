import {Component, Injector, OnDestroy, OnInit, TemplateRef, ViewChild} from "@angular/core";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {
  IDetailClassification,
  IParamDetailClassification
} from "../../../../../data-access/models/src/lib/IKpiCompliance";
import {ListPersonnelAllocationService} from "../../../../../data-access/services/src/lib/list-personnel-allocation.service";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import * as FileSaver from "file-saver";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";

@Component({
  selector: 'app-detail-classification',
  templateUrl: './detail-classification.component.html',
  styleUrls: ['./detail-classification.component.scss']
})
export class DetailClassificationComponent extends  BaseComponent implements OnInit, OnDestroy {
  heightTable = { x: '80vw', y: '50vh'};
  tableConfig: MBTableConfig = new MBTableConfig();
  pagination = new Pagination();
  listData: IDetailClassification[] = [];
  functionCode = FunctionCode.DETAIL_CLASSIFICATION;
  params: IParamDetailClassification = {
    employeeId: '',
    apportion: '',
    dataId: '',
    claCode: '',
    isPropose: '',
    periodId: '',
    page: 0,
    size: this.pagination.pageSize
  }
  @ViewChild('IOAllocationTmpl', {static: true}) IOAllocationTmpl!: TemplateRef<NzSafeAny>;

  constructor (injector : Injector, private listPersonnelService: ListPersonnelAllocationService) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.DETAIL_CLASSIFICATION}`);
    console.log(this.objFunction)
  }
  ngOnInit(): void {
    this.initTable();
  }
  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'goaManagement.listEmpAllocated.table.empCode',
          field: 'employeeCode',
          width: 150,
        },
        {
          title: 'goaManagement.listEmpAllocated.table.fullName',
          field: 'empName',
          fixed: window.innerWidth > 1024,
          width: 200
        },
        {
          title: 'goaManagement.listEmpAllocated.table.job',
          field: 'jobName',
          needEllipsis: true,
          width: 200,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'goaManagement.listEmpAllocated.table.orgName',
          field: 'orgName',
          needEllipsis: true,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'goaManagement.listEmpAllocated.table.dateMB',
          field: 'joinCompanyDate',
          width: 200,
          fixed: window.innerWidth > 1024,
          pipe: 'date'
        },
        {
          title: 'goaManagement.listEmpAllocated.table.pointWork',
          field: 'resultNum',
          width: 110,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'goaManagement.listEmpAllocated.table.rateKPI',
          field: 'minusPoint',
          width: 110,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'goaManagement.listEmpAllocated.table.pointWorkPivot',
          field: 'resultFinal',
          width: 110,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'goaManagement.listEmpAllocated.table.classification',
          field: 'ratingInApportion',
          width: 130,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'goaManagement.listEmpAllocated.table.outAllocation',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          field: 'isOutRateApportion',
          width: 110,
          tdTemplate: this.IOAllocationTmpl,
          fixed: window.innerWidth > 1024,
        },
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1,
      showFrontPagination: false
    };
  }
  redirectPage() {
    this.router.navigate(['/goal-management/periodic-goal-management/personnel-exception-ratings']).then();
  }
  exportExcel(event: any) {
    this.isLoading = event;
    const params = {
      employeeId: this.params.employeeId,
      apportion: this.params.apportion,
      dataId: this.params.dataId,
      claCode: this.params.claCode,
      isPropose: this.params.isPropose,
      periodId: this.params.periodId,
    }
    this.listPersonnelService.exportExcel(params).subscribe(res => {
      this.isLoading = false;
      const blob = new Blob([res], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      FileSaver.saveAs(blob, 'Danh sach xep loai chi tiet don vi.xls');
    }, () => {
      this.isLoading = false;
    })
  }
  getListDetail() {
    this.isLoading = true;
    this.listPersonnelService.getListDetail(this.params).subscribe(res => {
      this.isLoading = false;
      this.listData =  res.data.responseData.content;
      this.tableConfig.total = res.data.responseData.totalElements;
      this.listData.forEach(el => {
        if (!el.ratingInApportion) {
          el.ratingInApportion = el.ratingOutApportion;
        }
        el.resultNum = el.resultNum ? +Number(el.resultNum).toFixed(2) : null;
        el.resultFinal = el.resultFinal ? +Number(el.resultFinal).toFixed(2) : null;
      })
    }, (err) => {
      this.isLoading = false;
      if(err.code === 500) {
        this.toastrCustom.error(this.translate.instant("common.notification.error"));
      } else {
        this.toastrCustom.error(err.message);
      }
    })
  }
  receiveDataEmployee(event: any) {
    this.params.claCode = event.claCode ? event.claCode : '';
    this.params.periodId = event.periodId ? event.periodId : '';
    this.params.apportion = event.apportion !== null ? event.apportion : '';
    this.params.isPropose = event.isPropose !== null ? event.isPropose : '';
    this.params.dataId = event.dataId ? event.dataId : '';
    this.params.employeeId = event.employeeId ? event.employeeId : '';
    if (this.params.dataId) {
      this.getListDetail();
    }
  }
  changePage(value: any) {
    this.isLoading = true;
    this.params.page = value - 1;
    this.getListDetail();
  }
}
