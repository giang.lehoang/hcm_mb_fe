import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from "@angular/router";
import {DetailClassificationComponent} from "./detail-classification/detail-classification.component";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {GoalManagementUiFormInputModule} from "@hcm-mfe/goal-management/ui/form-input";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {NzIconModule} from "ng-zorro-antd/icon";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";

@NgModule({
  imports: [CommonModule, RouterModule.forChild([
    {
      path: '',
      component: DetailClassificationComponent,
    },
  ]), SharedUiLoadingModule, GoalManagementUiFormInputModule, SharedUiMbTableWrapModule, SharedUiMbTableModule, NzIconModule, TranslateModule, SharedUiMbButtonModule],
  exports: [DetailClassificationComponent],
  declarations: [DetailClassificationComponent]
})
export class GoalManagementFeatureDetailClassificationModule {}
