import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TargetCategoryComponent} from "./target-category/target-category.component";
import {SharedUiMbButtonIconModule} from "@hcm-mfe/shared/ui/mb-button-icon";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzGridModule} from "ng-zorro-antd/grid";
import {NzCardModule} from "ng-zorro-antd/card";
import {NzTagModule} from "ng-zorro-antd/tag";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import {TranslateModule} from "@ngx-translate/core";
import {RouterModule} from "@angular/router";
import {FormsModule} from "@angular/forms";

@NgModule({
  imports: [CommonModule,
    SharedUiMbButtonIconModule,
    SharedUiMbButtonModule,
    SharedUiLoadingModule,
    SharedUiMbSelectModule,
    SharedUiMbInputTextModule,
    FormsModule,
    NzTableModule,
    NzGridModule,
    NzCardModule,
    NzTagModule,
    NzPaginationModule,
    TranslateModule,
    RouterModule.forChild([
      {
        path: '',
        component: TargetCategoryComponent,
      },
    ]),],
  declarations: [TargetCategoryComponent],
  exports: [TargetCategoryComponent],
})
export class GoalManagementFeatureTargetCategoryModule {
}
