import { Component, Injector, OnInit, TemplateRef } from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { Subscription } from 'rxjs';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {
  DataTargetCategory, ListBallanceScoreCardManagement,
  ObjectSearch,
  ResponseEntityEmployTarget,
  TargetCategory
} from "@hcm-mfe/goal-management/data-access/models";
import {AppFunction} from "@hcm-mfe/shared/data-access/models";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {BallancedScoreCardService, TargetCategoryService} from "@hcm-mfe/goal-management/data-access/services";
import {HTTP_STATUS_CODE, Mode} from "@hcm-mfe/shared/common/constants";
import {ActionDocumentAttachedFileComponent, FormTargetCategoryComponent} from "@hcm-mfe/goal-management/ui/form-input";

@Component({
  selector: 'app-target-category',
  templateUrl: './target-category.component.html',
  styleUrls: ['./target-category.component.scss'],
})
export class TargetCategoryComponent extends  BaseComponent implements OnInit {
  isLoadingPage = false;
  modalRef: NzModalRef | undefined;
  listTargetCategory: Array<TargetCategory> | undefined;
  listBallanceScoreCardManagement: Array<ListBallanceScoreCardManagement> | undefined;
  objectSearch: ObjectSearch = {
    page: 0,
    size: 15,
  };
  listSettingPaginationCustom: {
    total: number;
  } = {
    total: 0,
  };
  override objFunction: AppFunction;
  readonly subs: Subscription[] = [];
  constructor(
    injector : Injector,
    private readonly ballancedScoreCardService: BallancedScoreCardService,
    private readonly targetCategoryService: TargetCategoryService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.TARGET_CATEGORY}`);
  }

  ngOnInit(): void {
    const ListBallanceScoreCard = this.ballancedScoreCardService.getListBallancedGoalActive().subscribe(
      (data) => {
        this.isLoadingPage = false;
        this.listBallanceScoreCardManagement = data.data;
      },
      (error) => {
        this.toastrCustom.error(error.error.message)
        this.isLoadingPage = false;
      }
    );
    this.subs.push(ListBallanceScoreCard);
    this.getListTargetCategory(this.objectSearch);
  }

  trackFntable(index: number, item: TargetCategory){
    return item.tarCode;
  }

  changePage(value: number): void {
    this.objectSearch.page = value - 1;
    this.getListTargetCategory(this.objectSearch);
  }

  changeTargetCode(value: string): void {
    if (value) {
      this.objectSearch.code = value;
    } else {
      delete this.objectSearch.code;
    }
  }

  changeTargetName(value: string): void {
    if (value) {
      this.objectSearch.name = value;
    } else {
      delete this.objectSearch.name;
    }
  }

  selectBalanceGoal(value: any): void {
    if (value.itemSelected && value.itemSelected !== undefined && value.itemSelected.lvaValue) {
      this.objectSearch.scoreCard = value.itemSelected.lvaValue;
    } else {
      delete this.objectSearch.scoreCard;
    }
  }

  searchTargetCategory(event: any): void {
    if (event.key === 'Enter' || event.type === 'click') {
    this.objectSearch.page = 0;
    this.getListTargetCategory(this.objectSearch);
    }
  }

  renderShare(value: any): string {
    if (value === '0') {
      return 'Không phải KPIs share';
    } else if (value === '1') {
      return 'KPIs Hệ thống';
    } else {
      return 'KPIs Đơn vị';
    }
  }

  getListTargetCategory(param: any): void {
    this.isLoadingPage = true;
    const ListTargetCategory = this.targetCategoryService.getListTargetCategory(param).subscribe(
      (data: DataTargetCategory) => {
        this.listTargetCategory = data.data.content;
        this.listSettingPaginationCustom.total = data.data.totalElements;
        this.isLoadingPage = false;
      },
      function (error: any) {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        this.isLoadingPage = false;
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        this.toastr.error(error.error.message)
      }.bind(this)
    );
    this.subs.push(ListTargetCategory);
  }

  getInnerWidth(point: number): number {
    if (window.innerWidth > 767) {
      return window.innerWidth / point > 1100 ? 1100 : window.innerWidth / point;
    }
    return window.innerWidth;
  }

  showModalUploadFile(footerTmpl: TemplateRef<any>) {
    this.modalRef = this.modal.create({
      nzWidth: this.getInnerWidth(3.5),
      nzTitle: this.translate.instant('targerManager.targetCategory.label.atribute.UploadFileExel'),
      nzContent: ActionDocumentAttachedFileComponent,
      nzComponentParams: {
        mode: Mode.ADD,
        url: 'urlTarget',
        endpoint: 'END_POINT_GOAL_MANAGEMENT',
        microService: 'TARGET_ENDPOIN_ASSESSMENT',
        fileName: 'TEMPLATE_CHI_TIEU'
      },
      nzFooter: footerTmpl,
    });
    this.modalRef.afterClose.subscribe((result) => (result?.refresh ? this.getListTargetCategory(this.objectSearch) : ''));
  }

  showModalTargetCategory(footerTmpl: TemplateRef<any>, mode: string, data?: TargetCategory): void {
    this.modalRef = this.modal.create({
      nzTitle: 'Chỉ tiêu',
      nzWidth: this.getInnerWidth(1.5),
      nzContent: FormTargetCategoryComponent,
      nzComponentParams: {
        mode: mode === 'ADD' ? Mode.ADD : Mode.EDIT,
        data: data,
      },
      nzFooter: footerTmpl,
    });
    this.modalRef.afterClose.subscribe((result) => (result?.refresh ? this.getListTargetCategory(this.objectSearch) : ''));
  }

  goBackPage() {
    this.location.back();
  }


  showConfirm(event: any, data: any): void {
    event.stopPropagation();
    this.deletePopup.showModal(() => this.handleDeleteTargetCategory(data.tarId))
  }

  handleDeleteTargetCategory(id: number) {
    this.targetCategoryService.deleteItemCategory(id).subscribe(
      (res: ResponseEntityEmployTarget<null>) => {
        if (res.status === HTTP_STATUS_CODE.OK) {
          this.getListTargetCategory(this.objectSearch);
          this.toastrCustom.success('Xóa bản ghi thành công')
        }
      },
      (error) => {
        this.isLoadingPage = false;
        this.toastrCustom.error(error.error.message)
      }
    );
  }

  override ngOnDestroy() {
    this.modalRef?.destroy();
    this.subs.forEach((s) => s.unsubscribe());
  }
}
