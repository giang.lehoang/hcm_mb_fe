import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TargetCategoryComponent } from './target-category.component';

describe('TargetCategoryComponent', () => {
  let component: TargetCategoryComponent;
  let fixture: ComponentFixture<TargetCategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TargetCategoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TargetCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
