import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from "@angular/router";
import {CrmListEmployeeComponent} from "./crm-list-employee/crm-list-employee.component";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {TranslateModule} from "@ngx-translate/core";
import {GoalManagementUiFormInputModule} from "@hcm-mfe/goal-management/ui/form-input";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";

@NgModule({
  imports: [CommonModule, RouterModule.forChild([
    {
      path: '',
      component: CrmListEmployeeComponent,
    },
  ]), SharedUiLoadingModule, SharedUiMbButtonModule, TranslateModule, GoalManagementUiFormInputModule, SharedUiMbTableWrapModule, SharedUiMbTableModule],
  declarations: [CrmListEmployeeComponent],
  exports: [CrmListEmployeeComponent]
})
export class GoalManagementFeatureCrmListEmployeeModule {}
