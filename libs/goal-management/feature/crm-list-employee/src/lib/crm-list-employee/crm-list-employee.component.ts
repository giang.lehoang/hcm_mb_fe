import {Component, Injector, OnDestroy, OnInit, TemplateRef} from "@angular/core";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {ActionDocumentAttachedFileComponent} from "@hcm-mfe/goal-management/ui/form-input";
import {Mode} from "@hcm-mfe/shared/common/constants";
import {NzModalRef} from "ng-zorro-antd/modal";
import {MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {IParamListOfIndicator} from "../../../../../data-access/models/src/lib/IKpiCompliance";
import {KpiComplianceService} from "../../../../../data-access/services/src/lib/kpi-compliance.service";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";

@Component({
  selector: 'app-crm-list-emp',
  templateUrl: './crm-list-employee.component.html',
  styleUrls: ['./crm-list-employee.component.scss']
})
export class CrmListEmployeeComponent extends  BaseComponent implements OnInit, OnDestroy {
  heightTable = { x: '80vw', y: '50vh'};
  tableConfig: MBTableConfig = new MBTableConfig();
  pagination = new Pagination();
  modalRef: NzModalRef | undefined;
  params: IParamListOfIndicator = {
    empCode: '',
    orgId: '',
    page: 0,
    size: this.pagination.pageSize
  }
  listData = [];
  constructor (injector : Injector,private readonly listOfIndicatorService: KpiComplianceService) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CRM_LIST_EMP}`);
    console.log(this.objFunction)
  }

  ngOnInit(): void {
    this.getListCrmEmp();
    this.initTable();
  }
  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'goaManagement.listEmpAllocated.table.empCode',
          field: 'empCode',
          width: 200
        },
        {
          title: 'goaManagement.listEmpAllocated.table.fullName',
          field: 'fullName',
          fixed: window.innerWidth > 1024,
          width: 250
        },
        {
          title: 'goaManagement.listEmpAllocated.table.job',
          field: 'jobName',
          needEllipsis: true,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'goaManagement.listEmpAllocated.table.orgName',
          field: 'orgName',
          needEllipsis: true,
          fixed: window.innerWidth > 1024,
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1,
      showFrontPagination: false
    };
  }
  getInnerWidth(point: number): number {
    if (window.innerWidth > 767) {
      return window.innerWidth / point > 1100 ? 1100 : window.innerWidth / point;
    }
    return window.innerWidth;
  }
  showModalUploadFile(footerTmpl: TemplateRef<any>) {
    this.modalRef = this.modal.create({
      nzWidth: this.getInnerWidth(3.5),
      nzTitle: this.translate.instant('targerManager.targetCategory.label.atribute.UploadFileExel'),
      nzContent: ActionDocumentAttachedFileComponent,
      nzComponentParams: {
        mode: Mode.ADD,
        url: 'urlCrmEmp',
        endpoint: 'END_POINT_GOAL_MANAGEMENT',
        microService: 'TARGET_ENDPOIN_ASSESSMENT',
        fileName: 'TEMPLATE_DANH_SACH_NHAN_VIEN_CRM'
      },
      nzFooter: footerTmpl,
    });
    this.modalRef.afterClose.subscribe((result) => (result?.refresh ? this.search() : ''));
  }
  getListCrmEmp() {
    this.listOfIndicatorService.getListCrmEmp(this.params).subscribe(res => {
      this.isLoading = false;
      this.listData = res.data.content;
      this.tableConfig.total = res.data.totalElements;
    }, () => {
      this.isLoading = false;
    })
  }
  search() {
    this.params.page = 0;
    this.tableConfig.pageIndex = 1;
    this.getListCrmEmp();
  }
  receiveDataEmployee(value: any) {
    this.isLoading = true;
    this.params.empCode = value.empCode ? value.empCode : '';
    this.params.orgId = value.orgId ? value.orgId : '';
    this.search();
  }
  changePage(value: any) {
    this.isLoading = true;
    this.params.page = value - 1;
    this.getListCrmEmp();
  }
  override ngOnDestroy() {
    this.modalRef?.destroy();
    super.ngOnDestroy();
  }
}
