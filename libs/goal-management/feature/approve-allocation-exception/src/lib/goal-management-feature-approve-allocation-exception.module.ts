import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ApproveAllocationExceptionComponent} from "./approve-allocation-exception/approve-allocation-exception.component";
import {RouterModule} from "@angular/router";
import {GoalManagementUiFormInputModule} from "@hcm-mfe/goal-management/ui/form-input";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiMbEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/mb-employee-data-picker";
import {NzGridModule} from "ng-zorro-antd/grid";
import {FormsModule} from "@angular/forms";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {NzCardModule} from "ng-zorro-antd/card";
import {SharedUiMbModalConfirmModule} from "@hcm-mfe/shared/ui/mb-modal-confirm";
import {NzSwitchModule} from "ng-zorro-antd/switch";
import {NzTabsModule} from "ng-zorro-antd/tabs";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
import {NzTagModule} from "ng-zorro-antd/tag";

@NgModule({
    imports: [CommonModule, RouterModule.forChild([
        {
            path: '',
            component: ApproveAllocationExceptionComponent,
        },
    ]), GoalManagementUiFormInputModule, SharedUiLoadingModule, NzTableModule, NzPaginationModule, TranslateModule,
        SharedUiMbButtonModule, SharedUiMbEmployeeDataPickerModule, NzGridModule, FormsModule, SharedUiMbSelectModule, SharedUiMbInputTextModule, NzCardModule, SharedUiMbModalConfirmModule, NzSwitchModule, NzTabsModule, NzToolTipModule, NzTagModule],
  declarations: [ApproveAllocationExceptionComponent],
  exports: [ApproveAllocationExceptionComponent]
})
export class GoalManagementFeatureApproveAllocationExceptionModule {}
