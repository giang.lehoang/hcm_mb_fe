import {Component, Injector, OnDestroy, OnInit, TemplateRef, ViewChild} from "@angular/core";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {ApproveAllocationExceptionService} from "../../../../../data-access/services/src/lib/approve-allocation-exception";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {Mode, userConfig} from "@hcm-mfe/shared/common/constants";
import {
  flagStatus,
  IApproveList,
  IBodyRequest,
  IParamApprove, IParamEmp, typeAllocation, typeAllocationTable
} from "../../../../../data-access/models/src/lib/IApproveEmpAllocated";
import {ModalConfirmComponent} from "@hcm-mfe/shared/ui/mb-modal-confirm";
import {Subject, takeUntil} from "rxjs";
import {NzModalRef} from "ng-zorro-antd/modal";
import {ActionDocumentAttachedFileComponent} from "@hcm-mfe/goal-management/ui/form-input";
import {MbDataPickerComponent} from "@hcm-mfe/shared/ui/mb-data-picker";
import {EmployeeDataSearchPolicyComponent} from "@hcm-mfe/shared/ui/mb-employee-data-picker";

@Component({
  selector: 'app-approve-allocation-exception',
  templateUrl: './approve-allocation-exception.component.html',
  styleUrls: ['./approve-allocation-exception.component.scss']
})
export class ApproveAllocationExceptionComponent extends  BaseComponent implements OnInit, OnDestroy {
  setOfCheckedId = new Set<number>();
  modalRef: NzModalRef | undefined;
  amountInAllocation = 0;
  totalEmp = 0;
  tabSelectIndex = 0;
  isLock = false;
  pastPeriod = false;
  @ViewChild('modalCompReject') modalCompReject: ModalConfirmComponent | undefined;
  orgName = "";
  fullName = "";
  orgNameOffer = "";
  fullNameOffer = "";
  private ngUnsubscribe = new Subject<void>();
  arrayId: number[] = [];
  listData: IApproveList[] = [];
  listDataOffer: IApproveList[] = [];
  listPeriod: any = [];
  listStatus = flagStatus();
  listAllocation = typeAllocation();
  listIOAllocationTable = typeAllocationTable();
  checked = false;
  functionCode = FunctionCode.APPROVE_EMP_ALLOCATED;
  paginationCustom = {
    size: 0,
    total: 0,
    pageIndex: 0,
    numberOfElements: 0
  };
  params: IParamApprove = {
    employeeId: "",
    periodId: "",
    flagStatus: "",
    page: 0,
    size: userConfig.pageSize
  }
  paramsEmp: IParamEmp = {
    empCode: "",
    periodId: "",
    apportion: "",
    page: 0,
    size: userConfig.pageSize
  }
  requestBody: IBodyRequest = {
    periodId: '',
    clcIds: [],
    flagStatus: '',
    reason: ''
  }
  constructor (injector : Injector, readonly approveAllocationExceptionService: ApproveAllocationExceptionService) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.APPROVE_EMP_ALLOCATED}`);
    console.log(this.objFunction)

  }
  ngOnInit(): void {
    this.isLoading = true;
    this.listStatus.push({
      name: 'Tất cả',
      value: ''
    })
    this.approveAllocationExceptionService.getPeriod().subscribe(res => {
      this.listPeriod = res.data;
      if (this.listPeriod.length) {
        this.params.periodId = this.listPeriod[0].periodId;
        this.paramsEmp.periodId = this.listPeriod[0].periodId;
        this.getConfig();
        this.getListEmployee();
      } else {
        this.isLoading = false;
      }
    }, () => {
      this.isLoading = false;
    });
  }
  getConfig() {
    const params = {
      periodId: this.paramsEmp.periodId
    }
    this.approveAllocationExceptionService.getConfig(params).subscribe(res => {
      this.isLock = res.data;
    });
  }
  getAmountApportion(periodId: any) {
    const params = {
      periodId: periodId
    }
    this.approveAllocationExceptionService.getAmountApportion(params).subscribe(res => {
      this.amountInAllocation = res.data.empInApportion;
      this.totalEmp = res.data.totalEmp;
    })
  }
  onAllChecked(checked: boolean): void {
    if (checked) {
      this.listDataOffer.forEach((item) => {
        if (item.flagStatus) {
          return;
        }
        this.updateCheckedSet(item.clcId, checked)
      });
    } else {
      this.setOfCheckedId.clear();
    }
    this.refreshCheckedStatus();
  }
  refreshCheckedStatus(): void {
    this.checked = this.listDataOffer.every(({ clcId }) => this.setOfCheckedId.has(clcId));
  }
  updateCheckedSet(id: number, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }
  updateConfig(value: any) {
    const formData = new FormData();
    formData.append('isLock', value);
    formData.append('periodId', '' + this.paramsEmp.periodId);
    this.isLoading = true;
    this.approveAllocationExceptionService.updateConfig(formData).subscribe(res => {
      this.isLoading = false;
      this.toastrCustom.success(this.translate.instant("common.notification.success"));
    }, () => {
      this.isLoading = false;
      this.isLock = !value;
      this.toastrCustom.error(this.translate.instant("common.notification.error"));
    })
  }
  changePage(event: any) {
    if (this.tabSelectIndex) {
      this.params.page = event - 1;
      this.getListOffer();
    } else {
      this.paramsEmp.page = event - 1;
      this.getListEmployee()
    }
  }

  onChange(event: any) {
    if (!this.tabSelectIndex) {
      this.paramsEmp.empCode = event ? event.employeeCode : '';
      this.orgName = event ? event.orgName : '';
      this.fullName = event ? event.fullName : '';
    } else {
      this.orgNameOffer = event ? event.orgName : '';
      this.fullNameOffer = event ? event.fullName : '';
      this.params.employeeId = event ? event.employeeId : '';
    }
  }
  onItemChecked(item: any, checked: boolean): void {
    this.updateCheckedSet(item, checked);
    this.refreshCheckedStatus();
  }
  renderOffer(data: any) {
    const value = +data;
    switch (value) {
      case 0:
        return this.listAllocation[0].name;
      case 1:
        return this.listAllocation[1].name;
      default:
        return '';
    }
  }
  renderFlagStatus(data: number) {
    switch (data) {
      case 0:
        return {
          text: this.listStatus[0].name,
          class: 'tag-custom ant-tag-orange-custom'
        };
      case 1:
        return {
          text: this.listStatus[1].name,
          class: 'tag-custom ant-tag-green-custom'
        };
      case 2:
        return {
          text: this.listStatus[2].name,
          class: 'tag-custom ant-tag-red-custom'
        };
      default: return null
    }
  }
  changeAllocation(data: any) {
    data.checkChanges = !data.checkChanges;
  }
  getListOffer() {
    this.isLoading = true;
    this.approveAllocationExceptionService.getListApprove(this.params).subscribe(res => {
      this.listDataOffer = res.data.content;
      this.listDataOffer.forEach(el => {
        el.clcSuggest = +el.clcSuggest;
        el.clcChange = +el.clcChange;
      });
      this.setOfCheckedId.clear();
      this.checked = false;
      this.paginationCustom.size = res.data.size;
      this.paginationCustom.total = res.data.totalElements;
      this.paginationCustom.pageIndex =res.data.pageable.pageNumber;
      this.paginationCustom.numberOfElements = res.data.numberOfElements;
      this.getAmountApportion(this.params.periodId);
      this.isLoading = false;
    }, () => {
      this.isLoading = false;
    })
  }
  getListEmployee() {
    this.isLoading = true;
    this.approveAllocationExceptionService.getListEmployee(this.paramsEmp).subscribe(res => {
      this.listData = res.data.content;
      this.listData.forEach(el => {
        el.clcSuggest = +el.clcSuggest;
        el.clcChange = +el.clcChange;
        el.apportion = +el.apportion;
        el.checkChanges = false;
      });
      this.setOfCheckedId.clear();
      this.checked = false;
      this.paginationCustom.size = res.data.size;
      this.paginationCustom.total = res.data.totalElements;
      this.paginationCustom.pageIndex =res.data.pageable.pageNumber;
      this.paginationCustom.numberOfElements = res.data.numberOfElements;
      this.getAmountApportion(this.paramsEmp.periodId);
      this.isLoading = false;
    }, () => {
      this.isLoading = false;
    })
  }
  save(data: any) {
    this.isLoading = true;
    const params = {
      tgrId: data.tgrId,
      apportion: data.apportion
    }
    this.approveAllocationExceptionService.saveEmployee(params).subscribe(res => {
      this.isLoading = false;
      this.toastrCustom.success(this.translate.instant("common.notification.success"));
      this.getListEmployee();
    }, () => {
      this.isLoading = false;
      this.toastrCustom.error(this.translate.instant("common.notification.error"));
    })
  }
  changePeriod(value: number) {
    const item = this.listPeriod.find((el: any) => el.periodId === value);
    if (this.tabSelectIndex) {
      this.params.periodId = this.params.periodId ? value : '';
      this.pastPeriod = !!item?.flagStatus;
    } else {
      this.paramsEmp.periodId = this.paramsEmp.periodId ? value : '';
      this.pastPeriod = !!item?.flagStatus;
    }
  }
  checkItemSelected() {
    if (this.setOfCheckedId.size === 0) {
      this.toastrCustom.error(this.translate.instant('goaManagement.approveListEmpAllocated.notification.noSelected'));
      return;
    }
  }
  showModalUploadFile(footerTmpl: TemplateRef<any>) {
    const param = { periodId: this.params.periodId };
    this.modalRef = this.modal.create({
      nzWidth: this.getInnerWidth(3.5),
      nzTitle: this.translate.instant('targerManager.targetCategory.label.atribute.UploadFileExel'),
      nzContent: ActionDocumentAttachedFileComponent,
      nzComponentParams: {
        mode: Mode.ADD,
        url: 'urlEmpInAllocation',
        endpoint: 'END_POINT_GOAL_MANAGEMENT',
        microService: 'TARGET_ENDPOIN_ASSESSMENT',
        fileName: 'TEMPLATE_DANH_SACH_NHAN_SU_TRONG_NGOAI_PHAN_BO',
        param: param
      },
      nzFooter: footerTmpl,
    });
    this.modalRef.afterClose.subscribe((result) => (result?.refresh ? this.getListEmployee() : ''));
  }
  getInnerWidth(point: number): number {
    if (window.innerWidth > 767) {
      return window.innerWidth / point > 1100 ? 1100 : window.innerWidth / point;
    }
    return window.innerWidth;
  }
  changeTab(event: any) {
    this.isLoading = true;
    this.amountInAllocation = 0;
    if (event.index) {
      this.tabSelectIndex = event.index;
      this.paramsEmp.periodId = this.listPeriod[0].periodId
      this.getListOffer();
      return;
    }
    this.tabSelectIndex = event.index;
    this.params.periodId = this.listPeriod[0].periodId
    this.getConfig();
    this.getListEmployee();

  }
  approveAllocation(flagStatus: number) {
    this.checkItemSelected();
    const approve:any = [];
    this.setOfCheckedId.forEach((value, key) => {
      approve.push({ key });
    });
    this.requestBody = {
      periodId: this.params.periodId,
      clcIds: approve.map((value: any) => value.key),
      flagStatus: flagStatus,
      reason: ''
    }
    this.buildBodyRequest('', flagStatus);
    if (this.setOfCheckedId.size > 0) {
      this.approveOrReject();
    }
  }
  buildBodyRequest(reason: string, flagStatus: number) {
    const approve:any = [];
    this.setOfCheckedId.forEach((value, key) => {
      approve.push({ key });
    });
    this.requestBody = {
      periodId: this.params.periodId,
      clcIds: approve.map((value: any) => value.key),
      flagStatus: flagStatus,
      reason: reason
    }
  }
  approveOrReject() {
    this.isLoading = true;
    this.approveAllocationExceptionService.approveEmpAllocated(this.requestBody).pipe(takeUntil(this.ngUnsubscribe)).subscribe(res => {
      this.isLoading = false;
      this.toastrCustom.success(this.translate.instant("common.notification.success"));
      if (this.modalCompReject) {
        this.modalCompReject.isVisibleCorrectionDialog = false;
      }
      this.getListOffer();
    },(err) => {
      this.isLoading = false;
      if(err.code === 500) {
        this.toastrCustom.error(this.translate.instant("common.notification.error"));
      } else {
        this.toastrCustom.error(err.message);
      }
    });
  }
  rejectAllocation() {
    if (this.modalCompReject) {
      this.modalCompReject.isVisibleCorrectionDialog =  true;
    }
  }
  reject(event: any, flagStatus: number) {
    this.buildBodyRequest(event, flagStatus);
    this.approveOrReject();
  }

  override ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
    this.modalRef?.destroy();
  }
}
