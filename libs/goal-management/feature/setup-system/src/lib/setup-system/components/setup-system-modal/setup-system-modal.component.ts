import {Component, Injector, OnInit} from '@angular/core';
import {HTTP_STATUS_CODE, Mode} from "@hcm-mfe/shared/common/constants";
import {IResponseEntityData, ISetupSystem} from "@hcm-mfe/goal-management/data-access/models";
import {FormGroup, Validators} from "@angular/forms";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {SetUpSystemService} from "../../../../../../../data-access/services/src/lib/setUpSystemService.service";
import {catchError, of} from "rxjs";
import {mbDataSelectsAssessmentStatus} from "@hcm-mfe/system/data-access/models";
import {DateValidator} from "@hcm-mfe/shared/common/validators";
import * as moment from "moment";
import {NzModalRef} from "ng-zorro-antd/modal";

@Component({
  selector: 'hcm-mfe-setup-system-modal',
  templateUrl: './setup-system-modal.component.html',
  styleUrls: ['./setup-system-modal.component.scss']
})
export class SetupSystemModalComponent extends BaseComponent implements OnInit {

  constructor(
    injector : Injector,
    private setUpSystemService: SetUpSystemService,
    private readonly modalRef: NzModalRef,
  ) {
    super(injector);
    this.form = this.fb.group({
      maxRanking: [null, [Validators.required]],
      type: [null, [Validators.required]],
      note: [null, [Validators.maxLength(50)]],
      flagStatus: [1, [Validators.required]],
      fromDate: [moment().format(this.formatDateType), [Validators.required]],
      toDate: [null],
    }, {
      validators: [
        DateValidator.validateSpecificRangeDate(
          'fromDate',
          'toDate',
          'rangeDateError1',
          false,
          ),
        DateValidator.validateSpecificCurrentDateMin(
          'fromDate',
          'rangeDateError2',
          String(moment().format(this.formatDateType)),
          this.mode === Mode.EDIT ? true : false
        ),
      ]
    });
  }
  mode = Mode.ADD;
  data: ISetupSystem | undefined;
  form: FormGroup;
  isSubmitted = false;
  mbDataSelectsAssessmentStatus = mbDataSelectsAssessmentStatus();
  private readonly formatDateType = "YYYY-MM-DD";
  conditionDateDisable = false;


  listClassificationShare$ = this.setUpSystemService.listClassificationShare$.pipe(
    catchError((err) => {
      // this.toastrCustom.error(this.translate.instant('goaManagement.system-allocation.notification.errorResponse'));
      return of({data: []});
    })
  );

  listLookupCodeHinhThucKYLuat$ = this.setUpSystemService.listLookupCodeHinhThucKYLuat$.pipe(
    catchError((err) => {
      // this.toastrCustom.error(this.translate.instant('goaManagement.system-allocation.notification.errorResponse'));
      return of({data: []});
    })
  );

  save() {
    this.isSubmitted = true;
    const request: Partial<ISetupSystem> = this.form.value;
    request.fromDate = moment(request.fromDate).format(this.formatDateType);
    request.toDate = request.toDate ? moment(request.toDate).format(this.formatDateType) : undefined;
    if (this.form.valid) {
      switch (this.mode){
        case Mode.ADD:
          this.setUpSystemService.PostDisciplines(request).subscribe(
            ((res: IResponseEntityData<Partial<ISetupSystem>>) => {
              if (res.status === HTTP_STATUS_CODE.OK) {
                this.isLoading = false;
                this.modalRef.close({refresh:true});
                this.toastrCustom.success(this.translate.instant('targerManager.common.notification.saveSuccess'));
              }else{
                this.isLoading = false;
                this.toastrCustom.error(`${this.translate.instant('targerManager.common.notification.saveError')} : ${res?.message}`);
              }
            }), (err) => {
              this.isLoading = false;
              this.toastrCustom.error(`${this.translate.instant('targerManager.common.notification.saveError')} : ${err?.message}`);
            }
          );
          break;
        case Mode.EDIT:
          this.setUpSystemService.PutDisciplines({
            ...request,
            oldType: this.data?.type,
            oldFromDate: this.data?.fromDate,
            oldFlagStatus: this.data?.flagStatus
          }, this.data?.id).subscribe(
            ((res: IResponseEntityData<Partial<ISetupSystem>>) => {
              if (res.status === HTTP_STATUS_CODE.OK) {
                this.isLoading = false;
                this.modalRef.close({refresh:true});
                this.toastrCustom.success(this.translate.instant('targerManager.common.notification.saveSuccess'));
              }else{
                this.isLoading = false;
                this.toastrCustom.warning(`${this.translate.instant('targerManager.common.notification.saveError')} : ${res?.message}`);
              }
            }), (err) => {
              this.isLoading = false;
              this.toastrCustom.error(`${this.translate.instant('targerManager.common.notification.saveError')} : ${err?.message}`);
            }
          );
          break;
      }
    }
    return 1;
  }

  disabledStartDate = (effectiveStartDate: Date): boolean => {
    return effectiveStartDate < moment().toDate();
  };

  disabledTypeSelectPresent(){
    if(this.mode === Mode.EDIT){
      return moment(this.data?.fromDate).toDate() < moment().toDate()
    }
    return false;
  }

  ngOnInit(): void {
    if (this.mode === Mode.EDIT) {
      this.data && this.form.patchValue(this.data);
      this.conditionDateDisable = moment(this.data?.fromDate).format("YYYY-MM-DD") <= moment().format("YYYY-MM-DD");
      console.log('this.conditionDateDisable', this.conditionDateDisable);
    }
  }

}
