import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetupSystemModalComponent } from './setup-system-modal.component';

describe('SetupSystemModalComponent', () => {
  let component: SetupSystemModalComponent;
  let fixture: ComponentFixture<SetupSystemModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetupSystemModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetupSystemModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
