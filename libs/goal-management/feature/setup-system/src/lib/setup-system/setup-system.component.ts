import {ChangeDetectionStrategy, Component, Injector, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {
  IParamSetupSystem,
  IResponseContentEntity,
  IResponseEntityData, ISetupAssessmentData,
  ISetupSystem,
  ItAssessmentPeriod
} from "@hcm-mfe/goal-management/data-access/models";
import {MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {
  catchError,
  EMPTY,
  map,
  Observable,
} from "rxjs";
import {SystemAllocationService} from "@hcm-mfe/goal-management/data-access/services";
import {SetUpSystemService} from "../../../../../data-access/services/src/lib/setUpSystemService.service";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {NzModalRef} from "ng-zorro-antd/modal";
import {HTTP_STATUS_CODE, Mode} from "@hcm-mfe/shared/common/constants";
import {SetupSystemModalComponent} from "./components/setup-system-modal/setup-system-modal.component";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";

@Component({
  selector: 'hcm-mfe-setup-system',
  templateUrl: './setup-system.component.html',
  styleUrls: ['./setup-system.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetupSystemComponent extends BaseComponent implements OnInit {

  constructor(
    injector: Injector,
    private systemAllocationService: SystemAllocationService,
    private setUpSystemService: SetUpSystemService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(
      `FUNCTION_${FunctionCode.GOA_SETUP_SYSTEM}`
    );
  }

  pagination = new Pagination();
  dataAssessmentPeriod$: Observable<IResponseEntityData<ItAssessmentPeriod[]>> = EMPTY;
  tableConfig: MBTableConfig = new MBTableConfig();
  categorySelectedAction$ = this.setUpSystemService.requestedParam$;
  @ViewChild('selectTmpl', {static: true}) selectToggle!: TemplateRef<NzSafeAny>;
  @ViewChild('deleteItemRecord', {static: true}) deleteItem!: TemplateRef<NzSafeAny>;
  heightTable = { x: '80vw', y: '27em'};
  modalRef: NzModalRef | undefined;

  dataTable$ = this.setUpSystemService.listDisciplines$.pipe(
    map(
      (res) => {
        this.tableConfig.total = res.data.totalElements;
        this.isLoading = false;
        return res;
      }
    ),
    catchError((err) => {
      this.isLoading = false;
      this.toastrCustom.error(this.translate.instant('goaManagement.system-allocation.notification.errorResponse'));
      return [];
    })
  )

  ngOnInit(): void {
    this.tableConfig.showFrontPagination = true;
    this.initTable();
    this.dataAssessmentPeriod$ = this.systemAllocationService.getAssessmentPeriod().pipe(
      catchError(() => {
        this.toastrCustom.error(this.translate.instant('goaManagement.system-allocation.notification.errorResponse'));
        return EMPTY;
      })
    );
    this.categorySelectedAction$ = this.setUpSystemService.requestedParam$;
  }

  changePage(pageNumber?: number): void {
    if (pageNumber) {
      this.pagination.pageNumber = pageNumber - 1;
      this.categorySelectedAction$.next({
        ...this.categorySelectedAction$.getValue(),
        page: pageNumber - 1,
      })
    }
    this.isLoading = true;
  }

  onSelected(id: number) {
    this.isLoading = true;
    this.categorySelectedAction$.next({
      ...this.categorySelectedAction$.getValue(),
      periodId: id === null ? '' : id,
    });
  }

  showModalAddItem(footerTmpl: TemplateRef<any>, mode: string, data?: ISetupSystem) {
    this.modalRef = this.modal.create({
      nzWidth: this.getNzWidth(),
      nzTitle: this.translate.instant('goaManagement.setupSystem.label.addItemSetupSystem'),
      nzContent: SetupSystemModalComponent,
      nzComponentParams: {
        mode: mode === 'ADD' ? Mode.ADD : Mode.EDIT,
        data: data,
      },
      nzFooter: footerTmpl,
    });
    // end get new page
    this.modalRef.afterClose.subscribe((result) => (
      result?.refresh ? this.categorySelectedAction$.next(this.categorySelectedAction$.getValue()) : '')
    );
  }

  showDeleteItemRecord(id: number): void {
    this.deletePopup.showModal(() => this.deleteItemRecord(id));
  }

  deleteItemRecord(id: number){
    this.isLoading = true;
    const deleteItem = this.setUpSystemService.DeleteDisciplines(id).subscribe(
      (res)=>{
        if(res.status === HTTP_STATUS_CODE.OK){
          this.toastrCustom.success(this.translate.instant('common.notification.success'));
          this.categorySelectedAction$.next(this.categorySelectedAction$.getValue());
        }
        this.isLoading = false;
      }, (err) => {
        this.isLoading = false;
        this.toastrCustom.error(err.message ? err.message : this.translate.instant('common.notification.error'));
      }
    )
    //this.subs.push(deleteItem);
  }


  private getNzWidth() {
    if(window.innerWidth > 767){
      return window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5;
    }
    return window.innerWidth;
  }

  override ngOnDestroy() {
    super.ngOnDestroy();
    this.categorySelectedAction$.complete();
    this.modalRef?.destroy();
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'goaManagement.setupSystem.label.table.typeName',
          field: 'typeName',
          thClassList: ['text-center'],
          width: 60,
        },
        {
          title: 'goaManagement.setupSystem.label.table.maxRankingName',
          field: 'maxRankingName',
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          width: 70,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'goaManagement.common.flagStatus',
          field: 'flagStatus',
          thClassList: ['text-center'],
          width: 30 ,
          tdTemplate: this.selectToggle,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'goaManagement.common.note',
          field: 'note',
          thClassList: ['text-center'],
          tdClassList: ['text-right'],
          width: 60,
          fixed: window.innerWidth > 1024,
        },
        {
          title: ' ',
          thClassList: ['text-center'],
          field: 'id',
          width: 30,
          fixed: window.innerWidth > 1024,
          tdTemplate: this.deleteItem,
        },

      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1,
      showFrontPagination: false
    };
  }
}
