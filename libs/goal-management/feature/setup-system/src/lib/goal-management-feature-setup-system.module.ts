import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SetupSystemComponent } from './setup-system/setup-system.component';
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {NzGridModule} from "ng-zorro-antd/grid";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {NzSwitchModule} from "ng-zorro-antd/switch";
import { SetupSystemModalComponent } from './setup-system/components/setup-system-modal/setup-system-modal.component';
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {SharedUiMbButtonIconModule} from "@hcm-mfe/shared/ui/mb-button-icon";


@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    SharedUiMbButtonModule,
    SharedUiLoadingModule,
    SharedUiMbSelectModule,
    NzGridModule,
    RouterModule.forChild([
      {
        path: '',
        component: SetupSystemComponent,
      },
    ]),
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    SharedUiMbTableWrapModule,
    SharedUiMbTableModule,
    NzSwitchModule,
    SharedUiMbInputTextModule,
    SharedUiMbDatePickerModule,
    SharedUiMbButtonIconModule,
  ],
  declarations: [
    SetupSystemComponent,
    SetupSystemModalComponent
  ],
})
export class GoalManagementFeatureSetupSystemModule {}
