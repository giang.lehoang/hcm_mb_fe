import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormSetOfIndicatorsComponent} from "./form-set-of-indicators/form-set-of-indicators.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {NzFormModule} from "ng-zorro-antd/form";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiMbDropDowListLazySearchModule} from "@hcm-mfe/shared/ui/mb-drop-dow-list-lazy-search";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {NzTagModule} from "ng-zorro-antd/tag";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzTableModule} from "ng-zorro-antd/table";
import {RouterModule} from "@angular/router";
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";

@NgModule({
    imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedUiLoadingModule, NzFormModule, SharedUiMbSelectModule,
        SharedUiMbDropDowListLazySearchModule, SharedUiMbInputTextModule, NzTagModule, NzToolTipModule, NzIconModule, NzTableModule, NzCheckboxModule
    ],
  declarations: [FormSetOfIndicatorsComponent],
  exports: [FormSetOfIndicatorsComponent],
})
export class GoalManagementFeatureFormSetOfIndicatorsModule {}
