import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormSetOfIndicatorsComponent } from './form-set-of-indicators.component';

describe('FormSetOfIndicatorsComponent', () => {
  let component: FormSetOfIndicatorsComponent;
  let fixture: ComponentFixture<FormSetOfIndicatorsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormSetOfIndicatorsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormSetOfIndicatorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
