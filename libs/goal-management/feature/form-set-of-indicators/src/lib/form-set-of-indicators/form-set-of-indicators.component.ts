import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { forkJoin } from 'rxjs';
import { NzSelectComponent } from 'ng-zorro-antd/select';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {MbDropDowListLazySearchComponent} from "../../../../../../shared/ui/mb-drop-dow-list-lazy-search/src/lib/mb-drop-dow-list-lazy-search/mb-drop-dow-list-lazy-search.component";
import {ListPeriodConfig, mbDataSelectsAssignType} from "@hcm-mfe/goal-management/data-access/models";
import {mbDataSelectsAssessmentStatus} from "@hcm-mfe/system/data-access/models";
import {SetOfIndicatorService} from "@hcm-mfe/goal-management/data-access/services";
import {validateRangeNumber} from "@hcm-mfe/shared/common/validators";
import {url} from "@hcm-mfe/shared/common/constants";

@Component({
  selector: 'app-form-set-of-indicators',
  templateUrl: './form-set-of-indicators.component.html',
  styleUrls: ['./form-set-of-indicators.component.scss'],
})
export class FormSetOfIndicatorsComponent extends BaseComponent implements OnInit {
  @ViewChild(NzSelectComponent) nzSelectComponent: NzSelectComponent | undefined;
  @ViewChild('formPositionComp') formPositionComp: MbDropDowListLazySearchComponent | undefined;
  @ViewChild('formPositionCompTemp') formPositionCompTemp: MbDropDowListLazySearchComponent | undefined;
  @ViewChild('formTargetComp') formTargetComp: MbDropDowListLazySearchComponent | undefined;
  checked = false;
  data = { sotId: null };
  copy: boolean | undefined;
  sotId: number | null = null;
  selectedValue: Array<number> | undefined;
  formSelect: FormGroup;
  listConfigAll: Array<ListPeriodConfig> = [];
  count = 1;
  isSubmitted = false;
  setOfIndicatorForm: FormGroup;
  listAssingType = mbDataSelectsAssignType();
  listStatus = mbDataSelectsAssessmentStatus();

  listPosType = [
    { lable: 'Position chi tiết', value: 0 },
    { lable: 'Position', value: 1 },
    { lable: 'Nhóm position', value: 2 },
  ];
  urlPositionApi = url.url_endpoint_list_position_for_sot_detail;
  urlTargetApi = url.url_endpoint_goa_list_target_parent;
  paramSearchPosition = {
    posType: null,
    name: null,
    positionId: null,
  };
  paramSearchPositionTemp = {
    posType: null,
    name: null,
  };
  paramSearchTarget = {
    name: null,
  };
  listTarget = [];
  dataSourceTarget = [];

  constructor(
    injector: Injector,
    private readonly setOfIndicatorService: SetOfIndicatorService,
    private readonly modalRef: NzModalRef
  ) {
    super(injector);
    this.formSelect = this.fb.group({
      target: [null, []],
    });
    this.setOfIndicatorForm = new FormGroup({
      posType: new FormControl(null, [Validators.required]),
      positionId: new FormControl(''),
      posId: new FormControl('', [Validators.required]),
      sotName: new FormControl('', [Validators.required, Validators.maxLength(200)]),
      assignedType: new FormControl('QL', [Validators.required]),
      sotCode: new FormControl('', [Validators.required, Validators.maxLength(50)]),
      flagStatus: new FormControl(1, [Validators.required]),
      scale: new FormControl('', [Validators.required, validateRangeNumber()])
    });
  }

  ngOnInit(): void {
    this.initData();
  }

  initData() {
    this.isLoading = true;
    forkJoin([
      this.setOfIndicatorService.getTargetParent({ page: 0, size: 100 }),
      this.setOfIndicatorService.getListConfigAll(),
    ]).subscribe(
      ([resTarget, resConfigHT]) => {
        this.listTarget = resTarget.data.content;
        this.listConfigAll = resConfigHT.data;
        this.listConfigAll.sort(this.sortAscListConfigAll);

        if (this.data) {
          this.setOfIndicatorService.getDetailOfTarget(this.data.sotId).subscribe((res) => {
            if (res && res.data && res.data.assignedType) {
              this.onChangeAssignedType(res.data.assignedType);
            }
          });
        } else {
          this.onChangeAssignedType('QL');
        }

        this.isLoading = false;
      },
      (err) => {
        this.isLoading = false;
        this.toastrCustom.error(err.message);
      }
    );
  }

  sortAscListConfigAll(a: any, b: any) {
    if (a.id < b.id) {
      return -1;
    }
    if (a.id > b.id) {
      return 1;
    }
    return 0;
  }

  onModelChangeTarget(event: any) {
    if (event && event.tarId) {
      this.isLoading = true;
      this.setOfIndicatorService.getTargetChild({ tarIds: event.tarId }).subscribe(
        (resTargetChild) => {
          this.isLoading = false;
          event.subs = resTargetChild.data;
          if (!this.dataSourceTarget.some((m: any) => m.tarId === event.tarId)) {
            // @ts-ignore
            this.dataSourceTarget.unshift(event);
          }
          this.dataSourceTarget = [...this.dataSourceTarget];

          if (this.setOfIndicatorForm.value['assignedType'] === 'HT') {
            this.initEstimateNumHT(this.dataSourceTarget);
          }
        },
        () => {
          this.isLoading = false;
        }
      );
    }
  }

  onChangeAssignedType(event: any) {
    if (event === 'QL') {
      if (this.data && this.data.sotId) {
        this.isLoading = true;
        this.setOfIndicatorService.getDetailOfTarget(this.data.sotId).subscribe(
          (resQL) => {
            this.sotId = resQL.data.sotId;
            this.setOfIndicatorForm.controls['assignedType'].setValue(resQL.data.assignedType);
            this.setOfIndicatorForm.controls['posType'].setValue(resQL.data.posType);
            this.setOfIndicatorForm.controls['sotName'].setValue(resQL.data.sotName);
            this.setOfIndicatorForm.controls['sotCode'].setValue(resQL.data.sotCode);
            this.setOfIndicatorForm.controls['flagStatus'].setValue(resQL.data.flagStatus);
            this.setOfIndicatorForm.controls['scale'].setValue(resQL.data.scale * 100);
            if (this.setOfIndicatorForm.controls['posType'].value === 1) {
              this.setOfIndicatorForm.controls['posId'].setValue(resQL.data.posId);
              if (this.formPositionComp) {
                this.formPositionComp.userInputSearch = resQL.data.posName;
              }
            }
            if(this.copy === null) {
              this.setOfIndicatorForm.controls['posId'].setValue(resQL.data.posId);
              if (this.formPositionCompTemp) {
                this.formPositionCompTemp.userInputSearch = resQL.data.positionName;
              }
              if (this.formPositionComp) {
                this.formPositionComp.userInputSearch = resQL.data.posName;
              }
            }
            this.dataSourceTarget = resQL.data.sotDetails;
            this.setScale(this.dataSourceTarget);

            this.isLoading = false;
          },
          () => {
            this.isLoading = false;
          }
        );
      } else {
        this.sotId = null;
        this.setOfIndicatorForm.controls['posType'].setValue(null);
        this.setOfIndicatorForm.controls['posId'].setValue(null);
        this.setOfIndicatorForm.controls['sotName'].setValue(null);
        this.setOfIndicatorForm.controls['sotCode'].setValue(null);
        this.setOfIndicatorForm.controls['flagStatus'].setValue(1);
        this.setOfIndicatorForm.controls['scale'].setValue(null);
        if(this.formPositionComp) {
          this.formPositionComp.userInputSearch = '';
        }
        this.dataSourceTarget = [];
      }
    }

    if (event === 'HT') {
      this.isLoading = true;
      this.setOfIndicatorService.getDetailOfTargetHT().subscribe(
        (res) => {
          if (res) {
            this.sotId = res.data.sotId;
            this.setOfIndicatorForm.controls['assignedType'].setValue(res.data.assignedType);
            this.setOfIndicatorForm.controls['posType'].setValue(null);
            this.setOfIndicatorForm.controls['posId'].setValue(null);
            this.setOfIndicatorForm.controls['sotName'].setValue(res.data.sotName);
            this.setOfIndicatorForm.controls['sotCode'].setValue(res.data.sotCode);
            this.setOfIndicatorForm.controls['flagStatus'].setValue(res.data.flagStatus);
            this.setOfIndicatorForm.controls['scale'].setValue(res.data.scale * 100);

            this.dataSourceTarget = res.data.sotDetails;
            this.setScale(this.dataSourceTarget);
            this.initEstimateNumHT(this.dataSourceTarget);
          } else {
            this.resetFormHT();
          }

          this.isLoading = false;
        },
        () => {
          this.resetFormHT();
          this.isLoading = false;
        }
      );
    }
  }

  resetFormHT() {
    this.sotId = null;
    this.setOfIndicatorForm.controls['posType'].setValue(null);
    this.setOfIndicatorForm.controls['posId'].setValue(null);
    this.setOfIndicatorForm.controls['sotName'].setValue(null);
    this.setOfIndicatorForm.controls['sotCode'].setValue(null);
    this.setOfIndicatorForm.controls['flagStatus'].setValue(1);
    this.setOfIndicatorForm.controls['scale'].setValue(null);
    this.dataSourceTarget = [];
  }

  convertScale(value: any) {
    let result = value;
    if (result) {
      result = Math.round(parseFloat(value) * 100 * 100) / 100;
    }
    return result;
  }

  setScale(list: any) {
    if (list) {
      for (const item of list) {
        if (item.scale) {
          item.scale = this.convertScale(item.scale);
        }
        if (item.subs) {
          for (const child of item.subs) {
            if (child.scale) {
              child.scale = this.convertScale(child.scale);
            }
          }
        }
      }
    }
  }

  sortAscEstimatePeriodConfigs(a: any, b: any) {
    if (a.pcfId < b.pcfId) {
      return -1;
    }
    if (a.pcfId > b.pcfId) {
      return 1;
    }
    return 0;
  }

  initEstimateNumHT(list: any) {
    const listConfigAllId = this.listConfigAll.map((m) => m.id);
    for (const item of list) {
      if (item.estimatePeriodConfig) {
        item.estimatePeriodConfigs = JSON.parse(item.estimatePeriodConfig);
        item.estimatePeriodConfigs = item.estimatePeriodConfigs.filter((m: any) => listConfigAllId.includes(m.pcfId));
        item.estimatePeriodConfigs.sort(this.sortAscEstimatePeriodConfigs);
        item.estimatePeriodConfig = JSON.stringify(item.estimatePeriodConfigs);
      } else {
        item.estimatePeriodConfigs = [];
        for (const element of this.listConfigAll) {
          item.estimatePeriodConfigs.push({ pcfId: element.id, estimateNum: null });
        }
        item.estimatePeriodConfig = JSON.stringify(item.estimatePeriodConfigs);
      }

      if (item.subs) {
        this.initEstimateNumHTChild(item.subs, listConfigAllId);
      }
    }
  }

  initEstimateNumHTChild(subs: any, listConfigAllId: any) {
    for (const child of subs) {
      if (child.estimatePeriodConfig) {
        child.estimatePeriodConfigs = JSON.parse(child.estimatePeriodConfig);
        child.estimatePeriodConfigs = child.estimatePeriodConfigs.filter((m: any) => listConfigAllId.includes(m.pcfId));
        child.estimatePeriodConfigs.sort(this.sortAscEstimatePeriodConfigs);
        child.estimatePeriodConfig = JSON.stringify(child.estimatePeriodConfigs);
      } else {
        child.estimatePeriodConfigs = [];
        for (const element of this.listConfigAll) {
          child.estimatePeriodConfigs.push({ pcfId: element.id, estimateNum: null });
        }
        child.estimatePeriodConfig = JSON.stringify(child.estimatePeriodConfigs);
      }
    }
  }

  onChangePosType(event: any) {
    if (event === 0) {
      // @ts-ignore
      this.paramSearchPositionTemp.posType = 1;
      if (this.formPositionCompTemp) {
        this.formPositionCompTemp.userInputSearch = '';
      }
      this.formPositionCompTemp?.onRetrivedData(this.paramSearchPositionTemp);
    } else {
      this.paramSearchPosition.positionId = null;
    }
    this.paramSearchPosition.posType = event;
    this.paramSearchPosition.name = null;
    if (this.formPositionComp) {
      this.formPositionComp.userInputSearch = '';
    }
    this.formPositionComp?.onRetrivedData(this.paramSearchPosition);

    this.setOfIndicatorForm.controls['posId'].setErrors({ required: true });
    this.setOfIndicatorForm.controls['posId'].setValue(null);
  }

  selectPosition(event: any) {
    if (event && event.id) {
      this.setOfIndicatorForm.controls['posId'].setErrors(null);
      this.setOfIndicatorForm.controls['posId'].setValue(event.id);
    } else {
      this.setOfIndicatorForm.controls['posId'].setErrors({ required: true });
      this.setOfIndicatorForm.controls['posId'].setValue(null);
    }
  }

  selectPositionTemp(event: any) {
    if (event && event.id) {
      this.paramSearchPosition.positionId = event.id;
      if(this.formPositionComp) {
        this.formPositionComp.userInputSearch = '';
      }
      this.formPositionComp?.onRetrivedData(this.paramSearchPosition);
      this.setOfIndicatorForm.controls['positionId'].setErrors(null);
      this.setOfIndicatorForm.controls['positionId'].setValue(event.id);
    } else {
      this.setOfIndicatorForm.controls['positionId'].setErrors({ required: true });
      this.setOfIndicatorForm.controls['positionId'].setValue(null);
    }
  }

  onDeleteTargetTag(data: any) {
    this.dataSourceTarget = this.dataSourceTarget.filter((m: any) => m.tarId !== data.tarId);
    if (this.setOfIndicatorForm.value['assignedType'] === 'HT') {
      this.initEstimateNumHT(this.dataSourceTarget);
    }
  }

  getTotalData(scale: any, total: any) {
    if (scale) {
      total.value = total.value + Math.floor(scale);
    }
  }

  checkValidateAndShowError(checkValid: any, arraySubmit: any) {
    const totalScale = { value: 0 };

    if (!this.setOfIndicatorForm.valid) {
      checkValid.value = false;
    }

    if (this.dataSourceTarget.length === 0) {
      this.toastrCustom.error('Chưa chọn danh sách chỉ tiêu');
      checkValid.value = false;
      return;
    }

    this.dataSourceTarget.forEach((item: any) => {
        if (item.typeRule !== '1') {
          if (!this.hasValue(item.scale)) {
            item.error = 'Trọng số bắt buộc nhập';
            checkValid.value = false;
          } else if (0 > Math.floor(item.scale) || Math.floor(item.scale) > 100) {
            item.error = 'Trọng số phải nằm trong khoảng từ 0 đến 100';
            checkValid.value = false;
          } else {
            item.error = '';
          }
          this.checkValidateTargetChildAndShowError(item, checkValid);
          this.getTotalData(item.scale, totalScale);
        } else {
          item.scale = null;
        }
        arraySubmit.push(item);
    })

    if (totalScale.value !== 100) {
      this.toastrCustom.error('Tổng tỷ trọng các chỉ tiêu trong bộ chỉ tiêu phải bằng 100%');
      checkValid.value = false;
    }
  }

  checkValidateTargetChildAndShowError(item: any, checkValid: any) {
    let totalSubs = 0;
    if (item.subs && item.subs.length > 0) {
      for (const sub of item.subs) {
        if (sub.typeRule !== '1') {
          if (!this.hasValue(sub.scale)) {
            sub.error = 'Trọng số bắt buộc nhập';
            checkValid.value = false;
          } else if (0 > Math.floor(sub.scale) || Math.floor(sub.scale) > 100) {
            sub.error = 'Trọng số phải nằm trong khoảng từ 0 đến 100';
            checkValid.value = false;
          } else {
            sub.error = '';
          }
          totalSubs = totalSubs + Math.floor(sub.scale);
        } else {
          sub.scale = null;
        }
      }

      this.checkErrorParentWithChildTarget(item, checkValid, totalSubs);
    }
  }

  checkErrorParentWithChildTarget(item: any, checkValid: any, totalSubs: any) {
    if (item.scale && totalSubs !== Math.floor(item.scale)) {
      this.toastrCustom.error('Tổng tỷ trọng các chỉ tiêu con phải bằng tỷ trọng chỉ tiêu cha');
      checkValid.value = false;
    }
  }

  hasValue(data: any) {
    let result = true;
    if (data === null || data === undefined || data === '') {
      result = false;
    }
    return result;
  }

  transformScaleBeforeSave() {
    this.setOfIndicatorForm.value.scale = this.setOfIndicatorForm.value.scale / 100;
    if (this.setOfIndicatorForm.value.subs) {
      for (const item of this.setOfIndicatorForm.value.subs) {
        this.caculateScale(item)
        if (item.subs) {
          for (const itemDetail of item.subs) {
            this.caculateScale(itemDetail)
          }
        }
      }
    }
  }
  caculateScale(data: any) {
    if (!data.scale) {
      return null;
    }
    return data.scale = data.scale / 100;
  }

  saveTargetCategory() {
    if (this.setOfIndicatorForm.value['assignedType'] === 'QL') {
      this.setOfIndicatorForm.get('scale')?.clearValidators();
      this.setOfIndicatorForm.get('scale')?.updateValueAndValidity();
    } else if (this.setOfIndicatorForm.value['assignedType'] === 'HT') {
      this.setOfIndicatorForm.get('posId')?.clearValidators();
      this.setOfIndicatorForm.get('posId')?.updateValueAndValidity();
      this.setOfIndicatorForm.get('posType')?.clearValidators();
      this.setOfIndicatorForm.get('posType')?.updateValueAndValidity();
    }

    this.isSubmitted = true;
    const arraySubmit: any = [];
    const checkValid = { value: true };

    this.checkValidateAndShowError(checkValid, arraySubmit);
    const resultArray = JSON.parse(JSON.stringify(arraySubmit));
    this.setOfIndicatorForm.value.subs = resultArray;

    if (!checkValid.value) {
      this.setFormControlDirty();
    } else {
      this.transformScaleBeforeSave();

      if (this.sotId && !this.copy) {
        this.isLoading = true;
        this.setOfIndicatorForm.value.isUpdateAssignment = this.checked;
        this.setOfIndicatorService.updateSetOfTarget(this.sotId, this.setOfIndicatorForm.value).subscribe(
          (_) => {
            this.isLoading = false;
            this.toastrCustom.success('Cập nhật thành công');
            this.modalRef.close({ refresh: true });
          },
          (err) => {
            this.isLoading = false;
            this.toastrCustom.error(err.message);
          }
        );
      } else {
        const dataCreate = JSON.parse(JSON.stringify(this.setOfIndicatorForm.value));
        if (this.copy) {
          dataCreate.sotId = null;
        }
        this.isLoading = true;
        this.setOfIndicatorService.createSetOfTarget(dataCreate).subscribe(
          () => {
            this.isLoading = false;
            this.toastrCustom.success('Tạo mới thành công');
            this.modalRef.close({ refresh: true });
          },
          (err) => {
            this.isLoading = false;
            this.toastrCustom.error(err.message);
          }
        );
      }
    }
  }

  setFormControlDirty() {
    Object.values(this.setOfIndicatorForm.controls).forEach((control) => {
      if (control.invalid) {
        control.markAsDirty();
        control.updateValueAndValidity({ onlySelf: true });
      }
    });
  }

  convertNumber(event: any, data: any) {
    if (event) {
      data.error = false;
      data.scale = Math.floor(+event);
    }
  }

  removeSpace(key: any) {
    this.setOfIndicatorForm.controls[`${key}`].setValue(this.setOfIndicatorForm.controls[`${key}`].value.trim());
  }
}
