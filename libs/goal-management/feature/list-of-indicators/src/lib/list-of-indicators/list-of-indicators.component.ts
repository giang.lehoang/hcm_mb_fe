import {Component, Injector, OnDestroy, OnInit, TemplateRef, ViewChild} from "@angular/core";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {KpiComplianceService} from "../../../../../data-access/services/src/lib/kpi-compliance.service";
import {IListOfIndicator, IParamListOfIndicator} from "../../../../../data-access/models/src/lib/IKpiCompliance";
import {
  IEmployData,
  listFlagStatus,
  processFlagStatus,
  renderFlagStatus
} from "@hcm-mfe/goal-management/data-access/models";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import * as FileSaver from "file-saver";
import * as moment from "moment";
import {FormSearchEmployeeComponent} from "../../../../../ui/form-input/src/lib/form-search-employee/form-search-employee.component";
import {NzModalRef} from "ng-zorro-antd/modal";
import {take} from "rxjs";
import {ActionDocumentAttachedFileComponent} from "@hcm-mfe/goal-management/ui/form-input";
import {Mode} from "@hcm-mfe/shared/common/constants";

@Component({
  selector: 'app-list-of-indicator',
  templateUrl: './list-of-indicators.component.html',
  styleUrls: ['./list-of-indicators.component.scss']
})
export class ListOfIndicatorsComponent extends  BaseComponent implements OnInit, OnDestroy {
  isLock = false;
  heightTable = { x: '80vw', y: '50vh'};
  tableConfig: MBTableConfig = new MBTableConfig();
  pagination = new Pagination();
  orgGroup = '';
  periodName = '';
  isCheckCloseDate = false;
  isCheckFinishDate = false;
  isVisible = false;
  @ViewChild('flagStatusTmpl', {static: true}) isFlagStatus!: TemplateRef<NzSafeAny>;
  @ViewChild('formSearch') formSearch!: FormSearchEmployeeComponent;
  modalRef: NzModalRef | undefined;
  isPeriodActive: boolean | undefined;

  listData: IListOfIndicator[] = [];
  listFlagStatus = listFlagStatus();
  renderFlagStatus = renderFlagStatus;
  params: IParamListOfIndicator = {
    empCode: '',
    orgId: '',
    periodId: '',
    flagStatus: '',
    page: 0,
    size: this.pagination.pageSize
  }
  processFlagStatus: listFlagStatus | null = null;
  constructor (injector : Injector, readonly listOfIndicatorService: KpiComplianceService) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.LIST_OF_INDICATOR}`);
  }
  ngOnInit(): void {
    this.processFlagStatus = processFlagStatus();
    this.listOfIndicatorService.getInfo().subscribe(res => {
      this.orgGroup = res.data.orgGroup;
    })
    this.initTable();
  }
  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'goaManagement.listEmpAllocated.table.empCode',
          field: 'empCode',
          width: 150,
        },
        {
          title: 'goaManagement.listEmpAllocated.table.fullName',
          field: 'fullName',
          fixed: window.innerWidth > 1024,
          width: 200
        },
        {
          title: 'goaManagement.listEmpAllocated.table.job',
          field: 'jobName',
          width: 150,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'goaManagement.listEmpAllocated.table.setOfIndicator',
          field: 'sotName',
          fixed: window.innerWidth > 1024,
          width: 200
        },
        {
          title: 'goaManagement.listEmpAllocated.table.position',
          field: 'posName',
          fixed: window.innerWidth > 1024,
          width: 200
        },
        {
          title: 'targerManager.setOfIndicators.label.table.asmFromDate',
          field: 'fromDate',
          pipe: 'date',
          fixed: window.innerWidth > 1024,
          width: 120
        },
        {
          title: 'targerManager.setOfIndicators.label.table.asmToDate',
          field: 'toDate',
          pipe: 'date',
          fixed: window.innerWidth > 1024,
          width: 120
        },
        {
          title: 'goaManagement.listEmpAllocated.table.orgName',
          field: 'orgName',
          width: 200,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'goaManagement.listEmpAllocated.table.flagStatus',
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          field: 'flagStatus',
          width: 200,
          tdTemplate: this.isFlagStatus,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'goaManagement.listEmpAllocated.table.mngNameCode',
          field: 'managerCode',
          width: 200,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'goaManagement.listEmpAllocated.table.mngName',
          field: 'manageName',
          width: 200,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'goaManagement.listEmpAllocated.table.indirectMngNameCode',
          field: 'indirectManagerCode',
          width: 200,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'goaManagement.listEmpAllocated.table.indirectMngName',
          field: 'indirectManageName',
          width: 200,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'goaManagement.listEmpAllocated.table.directDate',
          field: 'directSendDate',
          width: 100,
          fixed: window.innerWidth > 1024,
          pipe: 'date'
        },
        {
          title: 'goaManagement.listEmpAllocated.table.empDate',
          field: 'empSendDate',
          width: 100,
          fixed: window.innerWidth > 1024,
          pipe: 'date'
        },
        {
          title: 'goaManagement.listEmpAllocated.table.indirectDate',
          field: 'indirectSendDate',
          width: 100,
          fixed: window.innerWidth > 1024,
          pipe: 'date'
        },
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1,
      showFrontPagination: false
    };
  }
  search(isConfirm?: boolean) {
    this.isVisible = false;
    this.params.page = 0;
    this.tableConfig.pageIndex = 1;
    this.getListSetOfIndicator(isConfirm);
  }

  getListSetOfIndicator(isConfirm?: boolean) {
    this.listOfIndicatorService.getListSetOfIndicator(this.params).pipe(take(1)).subscribe(res => {
      this.isLoading = false;
      this.listData = res.data.content;
      this.tableConfig.total = res.data.totalElements;
      if(isConfirm) {
        this.formSearch.getListPeriod();
      }
    }, () => {
      this.isLoading = false;
    })
  }
  updateConfig(value: any) {
    this.isVisible = true;
  }
  exportExcel(event: boolean) {
    this.isLoading = event;
    const params = {
      empCode: this.params.empCode,
      orgId: this.params.orgId,
      periodId: this.params.periodId,
      flagStatus: this.params.flagStatus,
    }
    this.listOfIndicatorService.exportExcel(params).pipe(take(1)).subscribe(res => {
      this.isLoading = false;
      const blob = new Blob([res], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      FileSaver.saveAs(blob, 'Danh sach lich su giao va danh gia.xlsx');
    }, () => {
      this.isLoading = false;
    })
  }

  showModalUploadFile(footerTmpl: TemplateRef<any>) {
    this.modalRef = this.modal.create({
      nzWidth: this.getInnerWidth(3.5),
      nzTitle: this.translate.instant('targerManager.targetCategory.label.atribute.UploadFileExel'),
      nzContent: ActionDocumentAttachedFileComponent,
      nzComponentParams: {
        mode: Mode.ADD,
        url: 'urlTargetRacingEmployee',
        endpoint: 'END_POINT_GOAL_MANAGEMENT',
        microService: 'TARGET_ENDPOIN_ASSESSMENT',
        fileName: 'TEMPLATE_LICH_SU_GIAO_VA_DANH_GIA',
        param: `periodId=${this.params?.periodId}`,
        requestBlod: {
          periodId: this.params?.periodId
        }
      },
      nzFooter: footerTmpl,
    });
    this.modalRef.afterClose.subscribe((result) => (result?.refresh ? this.search() : ''));
  }

  getInnerWidth(point: number): number {
    if (window.innerWidth > 767) {
      return window.innerWidth / point > 1100 ? 1100 : window.innerWidth / point;
    }
    return window.innerWidth;
  }

  closeModal(isVisible: boolean): void {
    this.isVisible = isVisible;
    this.params.isLockDate = !this.params.isLockDate;
  }
  receiveDataEmployee(value: any) {
    this.isLoading = true;
    this.params.empCode = value.empCode ? value.empCode : '';
    this.params.orgId = value.orgId ? value.orgId : '';
    this.params.flagStatus = value.multipleStatus ? value.multipleStatus : '';
    this.params.periodId = value.periodId ? value.periodId : '';
    this.periodName = value.periodName;
    this.isPeriodActive = value.periodActive;
    const currentDate =  moment().format("YYYY-MM-DD");
    if (value.asmCloseDate) {
      this.isCheckCloseDate = Date.parse(value.asmCloseDate) <= Date.parse(currentDate);
    }
    if (value.finishDate) {
      this.isCheckFinishDate = Date.parse(value.finishDate) <= Date.parse(currentDate);
    }
    if (this.params.periodId) {
      this.params.isLockDate = !!value.asmCloseDate;
      this.search();
    } else {
      this.isLoading = false;
    }
  }

  changePage(value: any) {
    this.isLoading = true;
    this.params.page = value - 1;
    this.getListSetOfIndicator();
  }

  override ngOnDestroy() {
    this.modalRef?.destroy();
    super.ngOnDestroy();
  }
}
