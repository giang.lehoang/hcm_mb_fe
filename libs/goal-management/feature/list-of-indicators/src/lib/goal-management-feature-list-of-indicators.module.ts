import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {ListOfIndicatorsComponent} from "./list-of-indicators/list-of-indicators.component";
import {RouterModule} from "@angular/router";
import {GoalManagementUiFormInputModule} from "@hcm-mfe/goal-management/ui/form-input";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {NzTagModule} from "ng-zorro-antd/tag";
import {TranslateModule} from "@ngx-translate/core";
import {NzSwitchModule} from "ng-zorro-antd/switch";
import {FormsModule} from "@angular/forms";
import {NzModalModule} from "ng-zorro-antd/modal";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";

@NgModule({
  imports: [CommonModule, RouterModule.forChild([
    {
      path: '',
      component: ListOfIndicatorsComponent,
    },
  ]), GoalManagementUiFormInputModule, SharedUiMbTableWrapModule, SharedUiMbTableModule, SharedUiLoadingModule,
    NzTagModule, TranslateModule, NzSwitchModule, FormsModule, NzModalModule, SharedUiMbButtonModule],
  declarations: [ListOfIndicatorsComponent],
  exports: [ListOfIndicatorsComponent]
})
export class GoalManagementFeatureListOfIndicatorsModule {}
