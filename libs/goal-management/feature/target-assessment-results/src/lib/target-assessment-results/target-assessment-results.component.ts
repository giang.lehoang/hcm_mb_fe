import {
  Component,
  Injector,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { NgForm } from '@angular/forms';
import * as moment from 'moment';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { Pageable } from '@hcm-mfe/shared/data-access/models';
import { userConfig } from '@hcm-mfe/shared/common/constants';
import { FunctionCode } from '@hcm-mfe/shared/common/enums';

import {
  CurrentUser,
  DataSource, FlagStatus,
  ParamSearch,
  Period
} from "../../../../../data-access/models/src/lib/ITargetAssessmentResults";
import {ResponseEntityObject} from "../../../../../data-access/models/src/lib/IEmployTargetList";
import {TargetAssessmentResultsService} from "../../../../../data-access/services/src/lib/target-assessment-results.service";
import {PersonalTargetListService} from "../../../../../data-access/services/src/lib/personal-target-list.service";

@Component({
  selector: 'app-target-assessment-results',
  templateUrl: './target-assessment-results.component.html',
  styleUrls: ['./target-assessment-results.component.scss'],
})
export class TargetAssessmentResultsComponent
  extends BaseComponent
  implements OnInit, OnDestroy
{
  @ViewChild('formSearch', { static: false }) formSearch: NgForm = new NgForm(
    [],
    []
  );
  @ViewChild('formReject', { static: false }) formReject: NgForm = new NgForm(
    [],
    []
  );

  private readonly subs: Subscription[] = [];
  listWidth = ['5%', '15%', '17%', '12%', '12%', '20%', '8%', '8%', '8%', '8%', '8%', '8%', '10%', '10%', '15%'];
  isSubmitted = false;
  pageable: Pageable | undefined;
  limit: number = userConfig.pageSize;
  currentYear = moment().month() === 0 ?   moment().subtract(1, 'years').format('YYYY-MM-DD')  : moment(new Date()).format("YYYY-MM-DD");
  paramSearch: ParamSearch = {
    size: this.limit,
    page: 0,
    currentYear: null,
    periodYear: null,
    isGetResult: 1,
  };
  dataSource: Array<DataSource> = [];
  currentUser: CurrentUser = {
    empCode: '',
    fullName: '',
  };
  dataReject: DataSource | undefined;
  listPeriod: Array<Period> = [];
  listStatus = FlagStatus;
  scoreTotal: number | null | undefined;
  plusPoint: number | null | undefined;
  minusPoint: number | null | undefined;
  resultFinal: number | null | undefined;

  constructor(
    injector: Injector,
    public targetAssessmentResultsService: TargetAssessmentResultsService,
    public personalTargetListService: PersonalTargetListService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(
      `FUNCTION_${FunctionCode.TARGET_ASSESSMENT_RESULT}`
    );
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.paramSearch.periodYear =  moment().month() === 0 ? moment().year() - 1 : new Date().getFullYear();

    this.personalTargetListService.getEmployee().subscribe(
      (res: ResponseEntityObject<CurrentUser>) => {
        if (res && res.data) {
          this.currentUser.empCode = res.data.empCode;
          this.currentUser.fullName = res.data.fullName;
        }
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  getList(): void {
    if (!this.paramSearch.periodId) {
      return;
    }
    this.isLoading = true;
    this.scoreTotal = null;
    this.plusPoint = null;
    this.minusPoint = null;
    this.resultFinal = null;
    this.dataSource = [];
    const sub = this.personalTargetListService
      .getList(this.paramSearch)
      .subscribe(
        (res: ResponseEntityObject<Array<DataSource>>) => {
          if (res && res.data && res.data.length > 0) {
            this.dataSource = this.convertData(res.data);
            this.plusPoint = this.dataSource[0].plusPoint;
            this.minusPoint = this.dataSource[0].minusPoint;
            this.resultFinal = this.dataSource[0].resultFinal;
            this.scoreTotal = this.dataSource[0].resultNum;
            this.limit = this.dataSource.length;
            this.pageable = {
              totalElements: this.dataSource.length,
              totalPages: 1,
              currentPage: 1,
              numberOfElements: this.dataSource.length,
              size: this.limit,
            };
          }
          this.isLoading = false;
        },
        (error: any) => {
          this.isLoading = false;
          this.toastrCustom.error(
            error?.error?.message ? error?.error?.message : error?.error
          );
        }
      );
  }

  convertData(list: Array<DataSource>) {
    const arr: Array<DataSource> = [];
    const arrQL: Array<DataSource> = list.filter(
      (m) => m.assignedType === 'QL'
    );
    for (const item of arrQL) {
      const temp: DataSource = {
        ...item,
        asmIds: [item.asmId],
      };
      const itemHT = list.find(
        (m) => m.assignedType === 'HT' && m.empId === item.empId
      );
      if (itemHT) {
        temp.asmIds.push(itemHT.asmId);
      }
      const scaleHT: number | undefined = itemHT ? itemHT.scaleNumSot : 0;
      if (temp.ratioDate && scaleHT && temp.score) {
        temp.scoreByScale =
          temp.ratioDate *
          ((itemHT ? itemHT.score : 0) * scaleHT + temp.score * (1 - scaleHT));
      }
      arr.push(temp);
    }
    return arr;
  }


  changePage(event: number): void {
    if (event) {
      this.paramSearch.page = event - 1;
      this.getList();
    }
  }

  getPeriod() {
    const periodTemp = this.paramSearch.periodId;
    this.targetAssessmentResultsService
      .getPeriod({ year: this.paramSearch.periodYear })
      .subscribe((res: ResponseEntityObject<Array<Period>>) => {
        this.isLoading = false;
        if (res && res.data && res.data.length > 0) {
          this.listPeriod = res.data;
          this.paramSearch.periodId = this.listPeriod[0].periodId;
        } else {
          this.listPeriod = [];
          this.paramSearch.periodId = -1;
        }
        if (this.paramSearch.periodId === periodTemp) {
          this.getList();
        }
      });
  }

  onChangeSelectYeah(event: any): void {
    if (event) {
      this.paramSearch.currentYear = moment(event).format('YYYY-MM-DD');
      this.paramSearch.periodYear = event ? moment(event).format('YYYY') : null;
      this.getPeriod();
    }
  }

  onChangeSelectPeriod(event: any): void {
    if (event) {
      this.paramSearch.periodId = event;
      this.getList();
    }
  }

  goBack() {
    this.location.back();
  }

  goToDetails(data: any): void {
    this.router.navigate(['details'], {
      relativeTo: this.route.parent,
      state: { data: data },
    });
  }

  updateStatus(data: DataSource, event: Event) {
    event.stopPropagation();
    this.isLoading = true;
    const dataUpdate = {
      asmIds: data.asmIds,
      status: this.listStatus.CHO_PHE_DUYET_KET_QUA,
    };
    this.targetAssessmentResultsService.approveEvaluateResult(dataUpdate).subscribe(
      (res: ResponseEntityObject<number>) => {
        if (res) {
          this.toastrCustom.success(res.message);
          this.getList();
        }
        this.isLoading = false;
      },
      (error: any) => {
        this.isLoading = false;
        this.toastrCustom.error(
          error.error.message ? error.error.message : error.error.error
        );
      }
    );
  }

  isVisibleRejectDialog = false;
  isConfirmLoadingRejectDialog = false;
  acceptRejectDialog(): void {
    this.isSubmitted = true;
    if (this.formReject.valid) {
      this.isConfirmLoadingRejectDialog = true;
      const dataUpdate = {
        asmIds: this.dataReject?.asmIds,
        status: this.listStatus.CHUA_GUI_XAC_NHAN_KET_QUA,
        reason: this.dataReject?.reason,
      };
      this.targetAssessmentResultsService.approveEvaluateResult(dataUpdate).subscribe(
        (res: ResponseEntityObject<number>) => {
          if (res) {
            this.toastrCustom.success(res.message);
            this.isVisibleRejectDialog = false;
            this.isConfirmLoadingRejectDialog = false;
            this.isLoading = false;
            this.getList();
          }
        },
        (error: any) => {
          this.toastrCustom.error(
            error.error.message ? error.error.message : error.error.error
          );
          this.isLoading = false;
        }
      );
    }
  }

  showRejectDialog(data: DataSource, event: Event): void {
    event.stopPropagation();
    this.isSubmitted = false;
    this.isVisibleRejectDialog = true;
    this.dataReject = data;
  }

  cancelRejectDialog(): void {
    this.isVisibleRejectDialog = false;
    if (this.dataReject) {
      this.dataReject.reason = '';
    }
  }
}
