import { NgModule } from '@angular/core';
import { TargetAssessmentResultsComponent } from './target-assessment-results/target-assessment-results.component';
import { CommonModule } from '@angular/common';
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedDirectivesUppercaseInputModule} from "@hcm-mfe/shared/directives/uppercase-input";
import {SharedDirectivesTrimInputModule} from "@hcm-mfe/shared/directives/trim-input";
import {SharedUiMbButtonIconModule} from "@hcm-mfe/shared/ui/mb-button-icon";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedPipesFormatScoreModule} from "@hcm-mfe/shared/pipes/format-score";
import {NzFormModule} from "ng-zorro-antd/form";
import {NzInputModule} from "ng-zorro-antd/input";
import {NzModalModule} from "ng-zorro-antd/modal";
import {NzTagModule} from "ng-zorro-antd/tag";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzButtonModule} from "ng-zorro-antd/button";
import {RouterModule} from "@angular/router";
import {TranslateModule} from "@ngx-translate/core";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

const COMMON = [
  SharedUiMbButtonModule,
  SharedUiLoadingModule,
  SharedUiMbInputTextModule,
  SharedDirectivesUppercaseInputModule,
  SharedDirectivesTrimInputModule,
  SharedUiMbButtonIconModule,
  SharedUiMbDatePickerModule,
  SharedUiMbSelectModule,
  SharedPipesFormatScoreModule,
];
const ANT_DESIGN = [
  NzFormModule,
  NzInputModule,
  NzModalModule,
  NzTagModule,
  NzPaginationModule,
  NzTableModule,
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    ...COMMON,
    ...ANT_DESIGN,
    RouterModule.forChild([
      {
        path: '',
        component: TargetAssessmentResultsComponent,
      },
    ]),
    NzButtonModule,
  ],
  declarations: [TargetAssessmentResultsComponent],
  exports: [TargetAssessmentResultsComponent],
})
export class GoalManagementDataAccessFeatureTargetAssessmentResultsModule {}
