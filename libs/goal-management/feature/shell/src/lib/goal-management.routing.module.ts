import {RouterModule} from '@angular/router';
import {RoleGuardService} from '@hcm-mfe/shared/core';
import {FunctionCode, Scopes} from '@hcm-mfe/shared/common/enums';
import {NgModule} from '@angular/core';

const router = [
  {
    path: 'periodic-goal-management',
    data: {
      breadcrumb: 'targerManager.breadcrumb.periodicGoalManagement'
    },
    children: [
      {
        path: 'target-assessment-results',
        canActivateChild: [RoleGuardService],
        data: {
          code: FunctionCode.TARGET_ASSESSMENT_RESULT,
          pageName:
            'targerManager.targetAssessmentResults.label.atribute.targetAssessmentResults',
          breadcrumb:
            'targerManager.targetAssessmentResults.label.atribute.targetAssessmentResults',
        },
        children: [
          {
            path: 'details',
            scope: Scopes.VIEW,
            data: {
              pageName:
                'targerManager.targetAssessmentResults.label.atribute.targetAssessmentResultsDetails',
              breadcrumb:
                'targerManager.targetAssessmentResults.label.atribute.targetAssessmentResultsDetails',
            },
            loadChildren: () =>
              import('@hcm-mfe/goal-management/feature/target-assessment-results-details').then(
                (m) =>
                  m.GoalManagementFeatureTargetAssessmentResultsDetailsModule
              )
          },
          {
            path: '',
            loadChildren: () =>
              import(
                '@hcm-mfe/goal-management/feature/target-assessment-results'
                ).then(
                (m) =>
                  m.GoalManagementDataAccessFeatureTargetAssessmentResultsModule
              )
          }
        ],

      },
      {
        path: 'target-category',
        canActivateChild: [RoleGuardService],
        data: {
          code: FunctionCode.TARGET_CATEGORY,
          pageName:
            'targerManager.pageName.targetCategory',
          breadcrumb:
            'targerManager.breadcrumb.targetCategory',
        },
        children: [
          {
            path: '',
            loadChildren: () =>
              import(
                '@hcm-mfe/goal-management/feature/target-category'
                ).then(
                (m) =>
                  m.GoalManagementFeatureTargetCategoryModule
              ),
          },
        ],
      },
      {
        path: 'balanced-scorecard-management',
        canActivateChild: [RoleGuardService],
        data: {
          code: FunctionCode.BALANCED_SCORECARD_MANAGEMENT,
          scope: Scopes.VIEW,
          breadcrumb: 'targerManager.breadcrumb.blancedScorecardManagement',
          pageName: 'targerManager.pageName.blancedScorecardManagement',
        },
        children: [
          {
            path: '',
            loadChildren: () =>
              import(
                '@hcm-mfe/goal-management/feature/balanced-scorecard-management'
                ).then(
                (m) =>
                  m.GoalManagementFeatureBalancedScorecardManagementModule
              ),
          },
        ],
      },
      {
        path: 'setup-assessment',
        canActivateChild: [RoleGuardService],
        data: {
          code: FunctionCode.SETUP_ASSESSMENT,
          scope: Scopes.VIEW,
          breadcrumb: 'targerManager.breadcrumb.setupAssessment',
          pageName: 'targerManager.pageName.setupAssessment'
        },
        children: [
          {
            path: '',
            loadChildren: () =>
              import(
                '@hcm-mfe/goal-management/feature/setup-assessment'
                ).then(
                (m) =>
                  m.GoalManagementFeatureSetupAssessmentModule
              ),
          },
        ],
      },
      {
        path: 'set-of-indicators',
        canActivateChild: [RoleGuardService],
        data: {
          code: FunctionCode.SET_OF_INDICATORS,
          scope: Scopes.VIEW,
          pageName:
            'targerManager.pageName.setOfIndicators',
          breadcrumb:
            'targerManager.breadcrumb.setOfIndicators',
        },
        children: [
          {
            path: '',
            loadChildren: () =>
              import(
                '@hcm-mfe/goal-management/feature/set-of-indicators'
                ).then(
                (m) =>
                  m.GoalManagementFeatureSetOfIndicatorsModule
              ),
          },
        ],
      },
      {
        path: 'personal-target-list',
        canActivateChild: [RoleGuardService],
        data: {
          code: FunctionCode.PERSONAL_TARGET_LIST,
          scope: Scopes.VIEW,
          pageName: 'targerManager.personalTargetList.label.atribute.personalTargetList',
          breadcrumb: 'targerManager.personalTargetList.label.atribute.personalTargetList'
        },
        children: [
          {
            path: 'details',
            data: {
              code: FunctionCode.PERSONAL_PLAN_TARGET,
              scope: Scopes.VIEW,
              pageName: 'targerManager.pageName.personalPlanTarget',
              breadcrumb: 'targerManager.breadcrumb.personalPlanTarget'
            },
            loadChildren: () =>
              import('@hcm-mfe/goal-management/feature/personal-plan-target').then(
                (m) =>
                  m.GoalManagementFeaturePersonalPlanTargetModule
              )
          },
          {
            path: '',
            loadChildren: () =>
              import(
                '@hcm-mfe/goal-management/feature/personal-target-list'
                ).then(
                (m) =>
                  m.GoalManagementFeaturePersonalTargetListModule
              )
          }
        ],

      },
      {
        path: 'evaluetion-approval-set-of-indicator',
        canActivateChild: [RoleGuardService],
        data: {
          code: FunctionCode.EVALUETION_APPROVAL_SET_OF_INDICATOR,
          scope: Scopes.VIEW,
          pageName:
            'targerManager.breadcrumb.EvaluetionApprovalSetOfIndicator',
          breadcrumb:
            'targerManager.breadcrumb.EvaluetionApprovalSetOfIndicator',
        },
        children: [
          {
            path: '',
            loadChildren: () =>
              import(
                '@hcm-mfe/goal-management/feature/evaluetion-approval-set-of-indicator'
                ).then(
                (m) =>
                  m.GoalManagementFeatureEvaluetionApprovalSetOfIndicatorModule
              ),
          },
        ],
      },
      {
        path: 'evaluate-employee-result',
        canActivateChild: [RoleGuardService],
        data: {
          code: FunctionCode.EVALUATE_EMPLOYEE_RESULT,
          scope: Scopes.VIEW,
          pageName:
            'targerManager.pageName.evaluateEmployeeResult',
          breadcrumb:
            'targerManager.breadcrumb.evaluateEmployeeResult',
        },
        children: [
          {
            path: '',
            loadChildren: () =>
              import(
                '@hcm-mfe/goal-management/feature/evaluate-employee-result'
                ).then(
                (m) =>
                  m.GoalManagementFeatureEvaluateEmployeeResultModule
              ),
          },
        ],
      },
      {
        path: 'evaluate-employee-results',
        canActivateChild: [RoleGuardService],
        data: {
          code: FunctionCode.EVALUATE_EMPLOYEE_RESULTS,
          scope: Scopes.VIEW,
          pageName:
            'targerManager.pageName.evaluateEmployeeResult',
          breadcrumb:
            'targerManager.breadcrumb.evaluateEmployeeResult',
        },
        children: [
          {
            path: '',
            loadChildren: () =>
              import(
                '@hcm-mfe/goal-management/feature/evaluate-employee-results'
                ).then(
                (m) =>
                  m.GoalManagementFeatureEvaluateEmployeeResultsModule
              ),
          },
        ],
      },
      {
        path: 'employ-target-list',
        canActivateChild: [RoleGuardService],
        data: {
          code: FunctionCode.EMPLOYEE_TARGET_LIST,
          scope: Scopes.VIEW,
          pageName: 'targerManager.pageName.employTargetList',
          breadcrumb: 'targerManager.breadcrumb.employTargetList'
        },
        children: [
          {
            path: 'assign-target-to-all-employees',
            data: {
              scope: Scopes.VIEW,
              pageName: 'goaManagement.pageName.assignTargetToAllEmployees',
              breadcrumb: 'goaManagement.breadcrumb.assignTargetToAllEmployees'
            },
            loadChildren: () =>
              import('@hcm-mfe/goal-management/feature/assign-target-to-all-employees').then(
                (m) =>
                  m.GoalManagementFeatureAssignTargetToAllEmployeesModule
              )
          },
          {
            path: 'give-criteria',
            data: {
              code: FunctionCode.GIVE_CRITERIA,
              scope: Scopes.VIEW,
              pageName: 'targerManager.pageName.giveCriteria',
              breadcrumb: 'targerManager.breadcrumb.giveCriteria'
            },
            loadChildren: () =>
              import('@hcm-mfe/goal-management/feature/give-criteria').then(
                (m) =>
                  m.GoalManagementFeatureGiveCriteriaModule
              )
          },
          {
            path: '',
            loadChildren: () =>
              import(
                '@hcm-mfe/goal-management/feature/employ-target-list'
                ).then(
                (m) =>
                  m.GoalManagementFeatureEmployTargetListModule
              ),
          },
        ],
      },
      {
        path: 'system-allocation',
        canActivateChild: [RoleGuardService],
        data: {
          code: FunctionCode.SYSTEM_ALLOCATION,
          scope: Scopes.VIEW,
          breadcrumb: 'goaManagement.breadcrumb.system-allocation',
          pageName: 'goaManagement.pageName.system-allocation'
        },
        children: [
          {
            path: '',
            loadChildren: () =>
              import(
                '@hcm-mfe/goal-management/feature/system-allocation'
                ).then(
                (m) =>
                  m.GoalManagementFeatureSystemAllocationModule
              ),
          },
        ],
      },
      {
        path: 'rating-category',
        canActivateChild: [RoleGuardService],
        data: {
          code: FunctionCode.RATING_CATEGORY,
          scope: Scopes.VIEW,
          breadcrumb: 'goaManagement.breadcrumb.ratingCategory',
          pageName: 'goaManagement.pageName.ratingCategory'
        },
        children: [
          {
            path: '',
            loadChildren: () =>
              import(
                '@hcm-mfe/goal-management/feature/rating-category'
                ).then(
                (m) =>
                  m.GoalManagementFeatureRatingCategoryModule
              ),
          },
        ],
      },
      {
        path: 'classification-of-blocks-and-lines',
        canActivateChild: [RoleGuardService],
        data: {
          code: FunctionCode.CLASSIFICATION_OF_BLOCKS_AND_LINES,
          scope: Scopes.VIEW,
          breadcrumb: 'goaManagement.breadcrumb.classification-of-blocks-and-lines',
          pageName: 'goaManagement.pageName.classification-of-blocks-and-lines'
        },
        children: [
          {
            path: '',
            loadChildren: () =>
              import(
                '@hcm-mfe/goal-management/feature/block-and-line-classification'
                ).then(
                (m) =>
                  m.GoalManagementFeatureBlockAndLineClassificationModule
              ),
          },
        ],
      },
      {
        path: 'allocation-line-branch',
        canActivateChild: [RoleGuardService],
        data: {
          code: FunctionCode.ALLOCATION_LINE_BRANCH,
          scope: Scopes.VIEW,
          breadcrumb: 'goaManagement.breadcrumb.allocation-line-branch',
          pageName: 'goaManagement.pageName.allocation-line-branch'
        },
        children: [
          {
            path: '',
            loadChildren: () =>
              import(
                '@hcm-mfe/goal-management/feature/allocation-line-branch'
                ).then(
                (m) =>
                  m.GoalManagementFeatureAllocationLineBranchModule
              ),
          },
        ],
      },
      {
        path: 'list-emp-allocated',
        canActivateChild: [RoleGuardService],
        data: {
          code: FunctionCode.LIST_EMP_ALLOCATED,
          scope: Scopes.VIEW,
          breadcrumb: 'goaManagement.breadcrumb.listEmpAllocated',
          pageName: 'goaManagement.pageName.listEmpAllocated'
        },
        children: [
          {
            path: '',
            loadChildren: () =>
              import(
                '@hcm-mfe/goal-management/feature/list-emp-allocated'
                ).then(
                (m) =>
                  m.GoalManagementFeatureListEmpAllocatedModule
              ),
          },
        ],
      },
      {
        path: 'approve-allocation-exception',
        canActivateChild: [RoleGuardService],
        data: {
          code: FunctionCode.APPROVE_EMP_ALLOCATED,
          scope: Scopes.VIEW,
          breadcrumb: 'goaManagement.breadcrumb.approveAllocationException',
          pageName: 'goaManagement.pageName.approveAllocationException'
        },
        children: [
          {
            path: '',
            loadChildren: () =>
              import(
                '@hcm-mfe/goal-management/feature/approve-allocation-exception'
                ).then(
                (m) =>
                  m.GoalManagementFeatureApproveAllocationExceptionModule
              ),
          },
        ],
      },
      {
        path: 'list-personnel-allocation',
        canActivateChild: [RoleGuardService],
        data: {
          code: FunctionCode.LIST_PERSONNEL_ALLOCATION,
          scope: Scopes.VIEW,
          breadcrumb: 'goaManagement.breadcrumb.listPersonnelAllocation',
          pageName: 'goaManagement.pageName.listPersonnelAllocation'
        },
        children: [
          {
            path: '',
            loadChildren: () =>
              import(
                '@hcm-mfe/goal-management/feature/list-personnel-allocation'
                ).then(
                (m) =>
                  m.GoalManagementFeatureListPersonnelAllocationModule
              ),
          },
        ],
      },
      {
        path: 'kpi-compliance',
        canActivateChild: [RoleGuardService],
        data: {
          code: FunctionCode.KPI_COMPLIANCE,
          scope: Scopes.VIEW,
          breadcrumb: 'goaManagement.breadcrumb.KPIcompliance',
          pageName: 'goaManagement.pageName.KPIcompliance'
        },
        children: [
          {
            path: '',
            loadChildren: () =>
              import(
                '@hcm-mfe/goal-management/feature/kpi-compliance'
                ).then(
                (m) =>
                  m.GoalManagementFeatureKpiComplianceModule
              ),
          },
        ],
      },
      {
        path: 'list-of-indicators',
        canActivateChild: [RoleGuardService],
        data: {
          code: FunctionCode.LIST_OF_INDICATOR,
          scope: Scopes.VIEW,
          breadcrumb: 'goaManagement.breadcrumb.listOfIndicator',
          pageName: 'goaManagement.pageName.listOfIndicator'
        },
        children: [
          {
            path: '',
            loadChildren: () =>
              import(
                '@hcm-mfe/goal-management/feature/list-of-indicators'
                ).then(
                (m) =>
                  m.GoalManagementFeatureListOfIndicatorsModule
              ),
          },
        ],
      },
      {
        path: 'personnel-exception-ratings',
        canActivateChild: [RoleGuardService],
        data: {
          code: FunctionCode.PERSONNEL_EXCEPTION_RATINGS,
          scope: Scopes.VIEW,
          breadcrumb: 'goaManagement.breadcrumb.personnelExceptionRatings',
          pageName: 'goaManagement.pageName.personnelExceptionRatings'
        },
        children: [
          {
            path: '',
            loadChildren: () =>
              import(
                '@hcm-mfe/goal-management/feature/personnel-exception-ratings'
                ).then(
                (m) =>
                  m.GoalManagementFeaturePersonnelExceptionRatingsModule
              ),
          },
        ],
      },
      {
        path: 'setup-system',
        canActivateChild: [RoleGuardService],
        data: {
          code: FunctionCode.GOA_SETUP_SYSTEM,
          scope: Scopes.VIEW,
          breadcrumb: 'goaManagement.breadcrumb.setupSystem',
          pageName: 'goaManagement.pageName.setupSystem'
        },
        children: [
          {
            path: '',
            loadChildren: () =>
              import(
                '@hcm-mfe/goal-management/feature/setup-system'
                ).then(
                (m) =>
                  m.GoalManagementFeatureSetupSystemModule
              ),
          },
        ],
      },
      {
        path: 'handle-classification',
        canActivateChild: [RoleGuardService],
        data: {
          code: FunctionCode.GOA_HANDLE_CLASSIFICATION,
          scope: Scopes.VIEW,
          breadcrumb: 'goaManagement.breadcrumb.handleClassification',
          pageName: 'goaManagement.pageName.handleClassification'
        },
        children: [
          {
            path: '',
            loadChildren: () =>
              import(
                '@hcm-mfe/goal-management/feature/handle-classification'
                ).then(
                (m) =>
                  m.GoalManagementFeatureHandleClassificationModule
              ),
          },
        ],
      },
      {
        path: 'detail-classification',
        canActivateChild: [RoleGuardService],
        data: {
          code: FunctionCode.DETAIL_CLASSIFICATION,
          scope: Scopes.VIEW,
          breadcrumb: 'goaManagement.breadcrumb.detailClassification',
          pageName: 'goaManagement.pageName.detailClassification'
        },
        children: [
          {
            path: '',
            loadChildren: () =>
              import(
                '@hcm-mfe/goal-management/feature/detail-classification'
                ).then(
                (m) =>
                  m.GoalManagementFeatureDetailClassificationModule
              ),
          },
        ],
      },
      {
        path: 'crm-list-emp',
        canActivateChild: [RoleGuardService],
        data: {
          code: FunctionCode.CRM_LIST_EMP,
          scope: Scopes.VIEW,
          breadcrumb: 'goaManagement.breadcrumb.crmListEmp',
          pageName: 'goaManagement.pageName.crmListEmp'
        },
        children: [
          {
            path: '',
            loadChildren: () =>
              import(
                '@hcm-mfe/goal-management/feature/crm-list-employee'
                ).then(
                (m) =>
                  m.GoalManagementFeatureCrmListEmployeeModule
              ),
          },
        ],
      }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(router)],
  exports: [RouterModule],
})
export class GoalManagementRoutingModule {
}
