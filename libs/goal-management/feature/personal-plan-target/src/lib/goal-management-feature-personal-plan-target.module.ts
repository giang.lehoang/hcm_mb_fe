import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PersonalPlanTargetComponent} from "./personal-plan-target/personal-plan-target.component";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzCardModule} from "ng-zorro-antd/card";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {ReactiveFormsModule} from "@angular/forms";
import {TranslateModule} from "@ngx-translate/core";
import {NzTableModule} from "ng-zorro-antd/table";
import {RouterModule} from "@angular/router";
import {NzGridModule} from "ng-zorro-antd/grid";

@NgModule({
  imports: [CommonModule, SharedUiMbButtonModule, NzCardModule, SharedUiMbInputTextModule, ReactiveFormsModule, TranslateModule, NzTableModule,
    RouterModule.forChild([
      {
        path: '',
        component: PersonalPlanTargetComponent,
      },
    ]), NzGridModule,
  ],
  declarations: [
    PersonalPlanTargetComponent
  ],
  exports: [
    PersonalPlanTargetComponent
  ]
})
export class GoalManagementFeaturePersonalPlanTargetModule {}
