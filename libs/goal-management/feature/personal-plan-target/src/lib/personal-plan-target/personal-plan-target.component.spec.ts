import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalPlanTargetComponent } from './personal-plan-target.component';

describe('PersonalPlanTargetComponent', () => {
  let component: PersonalPlanTargetComponent;
  let fixture: ComponentFixture<PersonalPlanTargetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonalPlanTargetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalPlanTargetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
