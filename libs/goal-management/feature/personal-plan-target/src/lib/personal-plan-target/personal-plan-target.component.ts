import {Component, Injector, OnDestroy, OnInit} from '@angular/core';
import { Location } from '@angular/common';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  IEmployData,
  ITargetPlanStaff, processFlagStatus,
  ResponseEntityEmployTarget,
  ResponseError
} from "@hcm-mfe/goal-management/data-access/models";
import {PersonalTargetPlanService} from "@hcm-mfe/goal-management/data-access/services";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {CustomValidator} from "@hcm-mfe/shared/common/validators";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";


@Component({
  selector: 'hcm-mfe-app-personal-plan-target',
  templateUrl: './personal-plan-target.component.html',
  styleUrls: ['./personal-plan-target.component.scss'],
})
export class PersonalPlanTargetComponent extends BaseComponent implements OnInit, OnDestroy {
  arrayParent: Array<ITargetPlanStaff> = [];
  arrayChild: Array<ITargetPlanStaff> = [];
  listResutYear: Array<ITargetPlanStaff> = [];
  listNotUnique: Array<ITargetPlanStaff> = [];
  listResult: any;
  listResultChild: any;
  objectSubmit: Pick<IEmployData, 'asmIds' | 'empCode' | 'managerId' | 'content' | 'action' | 'flagStatus'> = {
    empCode: '',
    managerId: undefined,
    content: '',
    action: '',
    asmIds: [],
    flagStatus: undefined,
  };
  public subs: Subscription[] = [];
  infoEmployee: any;
  title = 'Chi tiết bộ chỉ tiêu kế hoạch cá nhân';
  formReject: FormGroup;
  flagStatus: number | undefined;
  isSubmitted = false;
  managerName: string | undefined;
  constructor(
    public personalTargetPlanService: PersonalTargetPlanService,
    injector: Injector,
  ) {
    super(injector);
    this.formReject = this.fb.group(
      {
        reason: [null, [Validators.required, Validators.maxLength(250)]],
      },
      {
        validators: [CustomValidator.noWhitespaceValidator('reason', 'noAllowSpace')],
      }
    );
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.PERSONAL_PLAN_TARGET}`);
  }

  goBackPage() {
    this.location.back();
  }

  getValueDescription(event: any) {
    if (event.currentTarget.value !== '') {
      this.objectSubmit.content = event.currentTarget.value;
    }
  }

  handleRecive() {
    this.isSubmitted = false;
    const data = this.personalTargetPlanService.getObjectDetailPersonalTargetList();
    const dataUpdate = {
      asmIds: data?.asmIds,
      status: 13,
    };
    const APPROVE = this.personalTargetPlanService.updateStatus(dataUpdate).subscribe(
      (_: ResponseEntityEmployTarget<null>) => {
        this.toastrCustom.success('Xác nhận bộ chỉ tiêu kế hoạch thành công');
        this.router.navigateByUrl('goal-management/periodic-goal-management/personal-target-list');
      },
      (err: ResponseError) => {
        this.toastrCustom.error(err.error.message);
      }
    );
    this.subs.push(APPROVE);
  }

  handleApprove() {
    this.objectSubmit.action = 'APPROVE';
    this.objectSubmit.flagStatus = processFlagStatus().CHO_PHE_DUYET_GIAO;
    const APPROVE = this.personalTargetPlanService.recivePersonalTargetPlan(this.objectSubmit).subscribe(
      (_: ResponseEntityEmployTarget<null>) => {
        this.toastrCustom.success('Duyệt thành công');
        this.router.navigateByUrl('goal-management/periodic-goal-management/approve-plan-assignment');
      },
      (err: ResponseError) => {
        this.toastrCustom.error(err.error.message);
      }
    );
    this.subs.push(APPROVE);
  }

  handleReject() {
    this.isSubmitted = true;
    if (this.formReject.valid) {
      const data = this.personalTargetPlanService.getObjectDetailPersonalTargetList();
      const dataUpdate = {
        asmIds: data?.asmIds,
        status: 11,
        reason: this.formReject.controls['reason'].value,
      };

      const REJECT = this.personalTargetPlanService.updateStatus(dataUpdate).subscribe(
        (_: ResponseEntityEmployTarget<null>) => {
          this.toastrCustom.success('Từ chối thành công');
          const type = this.personalTargetPlanService.getType();
          if (type && type === 'approve-plan-assignment') {
            this.router.navigateByUrl('goal-management/periodic-goal-management/approve-plan-assignment');
          } else if (type && type === 'personal-target-list') {
            this.router.navigateByUrl('goal-management/periodic-goal-management/personal-target-list');
          }
        },
        (err: ResponseError) => {
          this.toastrCustom.error(err.error.message);
        }
      );
      this.subs.push(REJECT);
    } else {
      Object.values(this.formReject.controls).forEach((control) => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }
  }

  ngOnInit(): void {
    const type = this.personalTargetPlanService.getType();
    if (type && type === 'approve-plan-assignment') {
      this.title = 'Chi tiết bộ chỉ tiêu kế hoạch giao nhân viên';
      const listDeTail = this.personalTargetPlanService.getObjectDetailApprovePlanAssignment();
      this.objectSubmit.asmIds = listDeTail?.asmIds;
      this.objectSubmit.empCode = listDeTail?.empCode;
      this.objectSubmit.managerId = listDeTail?.managerId;
      const params = {
        empId: listDeTail?.empId ? listDeTail?.empId : null,
        sotId: listDeTail?.sotId ? listDeTail?.sotId : null,
        periodYear: listDeTail?.periodYear ? listDeTail.periodYear : null,
      };

      const LISTPERSONALTARGET = this.personalTargetPlanService.getDetailPersonalTargetPlan(params).subscribe(
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        (data: any) => {

          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          this.listNotUnique = [...new Map(data.data.asmDetailDTOS.map((item: any) => [item['periodName'], item])).values()];
          const result = data.data.asmDetailDTOS
            .filter((item: any) => item.parentId === null)

            .reduce(function (rv: any, x: any) {
              (rv[x['stdId']] = rv[x['stdId']] || []).push(x);
              return rv;
            }, {});
          const resultChild = data.data.asmDetailDTOS
            .filter((item: any) => item.parentId !== null)
            .reduce(function (rv: any, x: any) {
              (rv[x['stdId']] = rv[x['stdId']] || []).push(x);
              return rv;
            }, {});
          this.listResultChild = resultChild;
          this.listResult = result;
          const arrayUniqueByKey = [...new Map(data.data.asmDetailDTOS.map((item: any) => [item['asmId'], item])).values()];


          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          this.listResutYear = [...new Map(data.data.asmDetailDTOS.map((item: any) => [item['periodName'], item])).values()];

          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          this.arrayParent = arrayUniqueByKey.filter((item: ITargetPlanStaff) => item.parentId === null);

          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          this.arrayChild = arrayUniqueByKey.filter((item: ITargetPlanStaff) => item.parentId !== null);
          this.arrayParent.forEach((itemParent) => {
            itemParent.subs = [];
            itemParent.scale = Math.round(itemParent.scale * 100 * 100) / 100;
            this.arrayChild.forEach((itemChild) => {
              itemChild.scale = Math.round(itemChild.scale * 100 * 100) / 100;

              if (itemChild.parentId === itemParent.stdId) {
                itemParent.subs?.push(itemChild);
              }
            });
          });


          this.infoEmployee = data.data;
        },
        (err: ResponseError) => {
          this.toastrCustom.error(err.error.message);
        }
      );
      this.subs.push(LISTPERSONALTARGET);
    } else if (type && type === 'personal-target-list') {
      const listDetail = this.personalTargetPlanService.getObjectDetailPersonalTargetList();
      this.flagStatus = listDetail?.flagStatus;
      this.personalTargetPlanService.getDetailPersonalTargetList(listDetail?.asmIds).subscribe(
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        (data) => {
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          this.listNotUnique = [...new Map(data.data.asmDetailDTOS.map((item: any) => [item['periodName'], item])).values()];
          const result = data.data.asmDetailDTOS
            .filter((item: any) => item.parentId === null)
            .reduce(function (rv: any, x: any) {
              (rv[x['stdId']] = rv[x['stdId']] || []).push(x);
              return rv;
            }, {});
          this.listResult = result;
          const resultChild = data.data.asmDetailDTOS
            .filter((item: any) => item.parentId !== null)
            .reduce(function (rv: any, x: any) {
              (rv[x['stdId']] = rv[x['stdId']] || []).push(x);
              return rv;
            }, {});
          this.listResultChild = resultChild;
          console.log(this.listResult, 'this.listResult')
          const arrayUniqueByKey = [...new Map(data.data.asmDetailDTOS.map((item: any) => [item['stdId'], item])).values()];
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          this.listResutYear = arrayUniqueByKey.filter((item: ITargetPlanStaff) => item.parentId === null);
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          this.arrayParent = arrayUniqueByKey.filter((item: ITargetPlanStaff) => item.parentId === null);
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          this.arrayChild = arrayUniqueByKey.filter((item: ITargetPlanStaff) => item.parentId !== null);
          this.listResutYear.forEach((_, index) => {
            if (this.listResutYear[index] && this.listResutYear[index + 1]) {
              if (this.listResutYear[index]['periodName'] === this.listResutYear[index + 1]['periodName']) {
                this.listResutYear.splice(0, index + 1);
              }
            }
          });
          this.arrayParent.forEach((itemParent) => {
            itemParent.subs = [];
            itemParent.scale = Math.round(itemParent.scale * 100 * 100) / 100;
            this.arrayChild.forEach((itemChild) => {
              if (itemChild.parentId === itemParent.stdId) {
                itemParent.subs?.push(itemChild);
              }
            });
            itemParent.subs.forEach((itemSubs) => {
              itemSubs.scale = Math.round(itemSubs.scale * 100 * 100) / 100;
            });
          });

          this.arrayParent = this.getArrayOrderByDisplaySeqAndScale([...this.arrayParent]);
            for(let index = 0; index <= this.arrayParent.length; index ++){
                if(this.arrayParent[index]?.subs){
                  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                  // @ts-ignore
                    this.arrayParent[index].subs = this.getArrayOrderByDisplaySeqAndScale([...this.arrayParent[index].subs]);
                }
            }


          this.objectSubmit.empCode = data.data.empCode;
          this.objectSubmit.asmIds = data.data.asmIds;
          this.objectSubmit.managerId = data.data.managerId;
          this.infoEmployee = data.data
        },
        (err) => {
          this.toastrCustom.error(err.error.message);
        }
      );
    }
  }
  renderFlagStatus(flagStatus: number): string {
    switch (flagStatus) {
      case 11:
        return 'Chưa gửi xác nhận giao';
      case 12:
        return 'Chờ xác nhận giao';
      case 13:
        return 'Chờ phê duyệt giao';
      case 14:
        return 'Hoàn tất phê duyệt giao';
      default:
        return '';
    }
  }

    private getArrayOrderByDisplaySeqAndScale(arrTemp: ITargetPlanStaff[]) {
        return arrTemp.sort((a: ITargetPlanStaff, b: ITargetPlanStaff) => {
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
          if (a.displaySeq > b.displaySeq) {
                return 1;
            } else if (a.displaySeq === b.displaySeq) {
                if (a.scale > b.scale) {
                    return -1;
                } else return 1;
            }
            return -1;
        });
    }

  override ngOnDestroy() {
    this.subs.forEach((s) => s.unsubscribe());
  }
}
