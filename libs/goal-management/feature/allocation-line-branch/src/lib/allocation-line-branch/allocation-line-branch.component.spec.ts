import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllocationLineBranchComponent } from './allocation-line-branch.component';

describe('AllocationLineBranchComponent', () => {
  let component: AllocationLineBranchComponent;
  let fixture: ComponentFixture<AllocationLineBranchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllocationLineBranchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllocationLineBranchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
