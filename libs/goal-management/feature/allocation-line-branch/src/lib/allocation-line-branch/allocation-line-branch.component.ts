import {Component, Injector, OnInit, SimpleChanges, TemplateRef} from '@angular/core';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {Subject, takeUntil} from "rxjs";
import {NzModalRef} from "ng-zorro-antd/modal";
import {FormGroup} from "@angular/forms";
import {
  IClassificationBlocksAndLines, IResponseEntityData, isColumnTableStatus,
  ItAssessmentPeriod, orgResults,
  ResponseEntity
} from "@hcm-mfe/goal-management/data-access/models";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {AllocationLineBranchService, SystemAllocationService} from "@hcm-mfe/goal-management/data-access/services";
import {
  IParamSearchAllocationLineBranch
} from "../../../../../data-access/models/src/lib/IAllocationLineBranch";
import {cloneDeep} from "lodash";
import {getInnerWidth} from "@hcm-mfe/policy-management/helper";
import {ActionDocumentAttachedFileComponent} from "@hcm-mfe/goal-management/ui/form-input";
import {MICRO_SERVICE, Mode} from "@hcm-mfe/shared/common/constants";

@Component({
  selector: 'hcm-mfe-allocation-line-branch',
  templateUrl: './allocation-line-branch.component.html',
  styleUrls: ['./allocation-line-branch.component.scss']
})
export class AllocationLineBranchComponent extends BaseComponent implements OnInit {
  private readonly destroyed$: Subject<void> = new Subject<void>()
  override isLoading = false;
  modalRef: NzModalRef | undefined;
  dataAssessmentPeriod: ItAssessmentPeriod[] | undefined = [];

  dataSearchFormAllocationLineBranch: IParamSearchAllocationLineBranch | undefined;
  dataAllocationLineBranch: IClassificationBlocksAndLines<orgResults>[] | undefined;


  constructor(
    injector: Injector,
    private allocationLineBranchService: AllocationLineBranchService,
    private systemAllocationService: SystemAllocationService,

  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ALLOCATION_LINE_BRANCH}`);
    this.dataSearchFormAllocationLineBranch = undefined;
  }

  ngOnInit(): void {
    this.systemAllocationService.getAssessmentPeriod().pipe(
      takeUntil(this.destroyed$)).subscribe((res: IResponseEntityData<ItAssessmentPeriod[]>) => {
      if (res.status === 200) {
        this.dataAssessmentPeriod = res?.data;
        this.allocationLineBranchService.setCurrentPeriod(this.dataAssessmentPeriod && this.dataAssessmentPeriod[0]);
        this.dataSearchFormAllocationLineBranch = {
          periodId: this.dataAssessmentPeriod && this.dataAssessmentPeriod[0].periodId,
          orgId: ''
        }
        this.doSearch();
      }
      }, (err) => {
        console.log(err);
        this.toastrCustom.error(this.translate.instant('goaManagement.system-allocation.notification.errorResponse'));
      }
    )
  }

  doSearch() {
    this.isLoading = true;
    this.allocationLineBranchService.getAllocationLineBranchDataTable(
       this.dataSearchFormAllocationLineBranch
    ).pipe(
    takeUntil(this.destroyed$)).subscribe(
      (data: ResponseEntity<IClassificationBlocksAndLines<orgResults>>) => {
        if(data.status === 200){
          this.dataAllocationLineBranch = data?.data;
        }
      }, error => {
        this.toastrCustom.error(this.translate.instant('goaManagement.system-allocation.notification.errorResponse'));
        this.isLoading = false;
      }, () => {
        this.isLoading = false;
      }
    )
  }

  doSearchFormCallBack($event: IParamSearchAllocationLineBranch) {
    this.dataSearchFormAllocationLineBranch = $event;
    this.doSearch();
  }

  updateDropColumn($event: IClassificationBlocksAndLines<orgResults>[]) {
    // assign lanes when user drag or drop column
    this.dataAllocationLineBranch = $event;
  }

  confirmApprovalSave(footerTmplShowModalConfirmSave: TemplateRef<any>){
    const cloneDeepDataRecordInitialItem: IClassificationBlocksAndLines<orgResults> | undefined = cloneDeep(this.dataAllocationLineBranch)?.find(
      el => el.isColumnTable === isColumnTableStatus[isColumnTableStatus.N]
    );

    if(cloneDeepDataRecordInitialItem && cloneDeepDataRecordInitialItem?.orgResults?.length > 0){
      // show modal when list orgResults has length > 0
      this.modalRef = this.modal.create({
        nzWidth: getInnerWidth(3.55),
        nzTitle: this.translate.instant('goaManagement.classification-blocks-and-lines.label.attribute.approvalSaveRecord'),
        nzContent: this.translate.instant('goaManagement.classification-blocks-and-lines.notification.warningWhenSaveRecord'),
        nzFooter: footerTmplShowModalConfirmSave,
      });
      this.modalRef.afterClose.subscribe((result) => (result?.refresh ? this.doSearch(): ''));
    }else {
      this.saveRecord();
    }

  }


  saveRecord() {
    const cloneDeepDataRecord: IClassificationBlocksAndLines<orgResults>[] | undefined = cloneDeep(this.dataAllocationLineBranch);
    // get param
    const responseFilter : orgResults[] | undefined = cloneDeepDataRecord?.reduce((result: orgResults[], currentEl) => {
      if(currentEl.isColumnTable !== isColumnTableStatus[isColumnTableStatus.N]){
        const transformData = currentEl.orgResults?.map((el: orgResults) => {
          return  {
            dataId: el.dataId,
            periodId: this.dataSearchFormAllocationLineBranch?.periodId,
            orgId: el.orgId,
            pgrId: el.pgrId,
            dataType: el.dataType,
            dataCode: currentEl.dataCode,
            dataName: el.dataName,
            flagStatus: el.flagStatus
          }
        })

        result.push(...transformData);
      }
      return result;
    }, []);

    // save
    this.isLoading = true;
    this.allocationLineBranchService.saveRecordAllocationLineBranch(responseFilter).subscribe(data => {
      if(data?.status === 200){
        this.toastrCustom.success(this.translate.instant('goaManagement.system-allocation.notification.saveSuccessResponse'));
        this.modalRef?.close();
        this.modalRef?.destroy();
        this.doSearch();
      }
    }, (err) => {
      this.isLoading = false;
      this.toastrCustom.error(err?.message ? err?.message : this.translate.instant('goaManagement.system-allocation.notification.errorResponse'));
    }, () => {
      this.isLoading = false;
    })

  };

  showModalUploadFile(footerTmpl: TemplateRef<any>){
    this.modalRef = this.modal.create({
      nzWidth: getInnerWidth(3.5),
      nzTitle: this.translate.instant('targerManager.targetCategory.label.atribute.UploadFileExel'),
      nzContent: ActionDocumentAttachedFileComponent,
      nzComponentParams: {
        mode: Mode.ADD,
        url:  'urlClassificationLine',
        endpoint: 'END_POINT_GOAL_MANAGEMENT',
        microService: MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT,
        fileName: 'Thiết lập xếp loại của line chi nhánh',
        param: `periodId=${this.dataAssessmentPeriod && this.dataAssessmentPeriod[0].periodId}`,
        requestBlod: {
          periodId: this.dataAssessmentPeriod && this.dataAssessmentPeriod[0].periodId
        }
      },
      nzFooter: footerTmpl,
    });
    this.modalRef.afterClose.subscribe((result) => (result?.refresh ? this.doSearch(): ''));
  };


  override ngOnDestroy() {
    super.ngOnDestroy();
    this.destroyed$.next();
    this.destroyed$.complete();
  };

}
