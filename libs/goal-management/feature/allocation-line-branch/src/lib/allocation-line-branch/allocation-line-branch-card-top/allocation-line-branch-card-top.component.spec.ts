import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllocationLineBranchCardTopComponent } from './allocation-line-branch-card-top.component';

describe('AllocationLineBranchCardTopComponent', () => {
  let component: AllocationLineBranchCardTopComponent;
  let fixture: ComponentFixture<AllocationLineBranchCardTopComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllocationLineBranchCardTopComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllocationLineBranchCardTopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
