import {Component, EventEmitter, Injector, Input, OnInit, Output, SimpleChanges} from '@angular/core';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {forkJoin, Subject, takeUntil} from "rxjs";
import {AllocationLineBranchService, SystemAllocationService} from "@hcm-mfe/goal-management/data-access/services";
import {
  IEmployData,
  ItAssessmentPeriod,
  ResponseEntity,
  ResponseEntityEmployTarget
} from "@hcm-mfe/goal-management/data-access/models";
import {FormGroup} from "@angular/forms";
import {
  IListAllocationLineBranchSelect,
  IParamSearchAllocationLineBranch
} from "../../../../../../data-access/models/src/lib/IAllocationLineBranch";

@Component({
  selector: 'hcm-mfe-allocation-line-branch-card-top',
  templateUrl: './allocation-line-branch-card-top.component.html',
  styleUrls: ['./allocation-line-branch-card-top.component.scss']
})
export class AllocationLineBranchCardTopComponent extends BaseComponent implements OnInit {
  override isLoading = false;
  @Input() dataAssessmentPeriod: ItAssessmentPeriod[] | undefined = [];
  listAllocationLineBranchSelect: IListAllocationLineBranchSelect[] | undefined = [];
  form: FormGroup;
  private readonly destroyedComponent$: Subject<void> = new Subject<void>()
  @Output() isSubmitForm = new EventEmitter<IParamSearchAllocationLineBranch>();

  constructor(
    injector: Injector,
    private allocationLineBranchService: AllocationLineBranchService,
  ) {
    super(injector);
    this.form = this.fb.group({
      periodId: [null],
      orgId: [null]
    });
  }

  ngOnInit(): void {
    this.isLoading = true;
      this.allocationLineBranchService.getListAllocationLineBranchSelect().pipe(
      takeUntil(this.destroyedComponent$)
    ).subscribe((listAllocationLineBranchSelect: ResponseEntity<IListAllocationLineBranchSelect>) => {

        if(listAllocationLineBranchSelect.status === 200){
          this.listAllocationLineBranchSelect = listAllocationLineBranchSelect?.data
        }
      }, (err) => {
        this.isLoading = false;
        this.toastrCustom.error(this.translate.instant('goaManagement.system-allocation.notification.errorResponse'));
      }, () => this.isLoading = false
    );
  }

  searchForm() {
    const requestSearch: IParamSearchAllocationLineBranch = this.form.value;
    this.isSubmitForm.emit(requestSearch);
  }

  ngOnChanges(changes: SimpleChanges){
    this.form.patchValue({
      periodId: this.dataAssessmentPeriod && this.dataAssessmentPeriod[0]?.periodId
    });
  }

  override ngOnDestroy() {
    super.ngOnDestroy();
    this.destroyedComponent$.next();
    this.destroyedComponent$.complete();
  }

}
