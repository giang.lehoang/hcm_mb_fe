import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AllocationLineBranchComponent } from './allocation-line-branch/allocation-line-branch.component';
import {RouterModule} from "@angular/router";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {ReactiveFormsModule} from "@angular/forms";
import {NzGridModule} from "ng-zorro-antd/grid";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {TranslateModule} from "@ngx-translate/core";
import { AllocationLineBranchCardTopComponent } from './allocation-line-branch/allocation-line-branch-card-top/allocation-line-branch-card-top.component';
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {GoalManagementUiFormInputModule} from "@hcm-mfe/goal-management/ui/form-input";
import {DragDropModule} from "@angular/cdk/drag-drop";

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: AllocationLineBranchComponent,
      },
    ]),
    SharedUiLoadingModule,
    ReactiveFormsModule,
    NzGridModule,
    SharedUiMbSelectModule,
    TranslateModule,
    SharedUiMbButtonModule,
    GoalManagementUiFormInputModule,
    DragDropModule,
  ],
  declarations: [
    AllocationLineBranchComponent,
    AllocationLineBranchCardTopComponent
  ],
})
export class GoalManagementFeatureAllocationLineBranchModule {}
