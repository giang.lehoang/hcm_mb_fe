import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetTargetsForEmployeesComponent } from './set-targets-for-employees.component';

describe('SetTargetsForEmployeesComponent', () => {
  let component: SetTargetsForEmployeesComponent;
  let fixture: ComponentFixture<SetTargetsForEmployeesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetTargetsForEmployeesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetTargetsForEmployeesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
