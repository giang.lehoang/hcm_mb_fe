import { Component, EventEmitter, Injector, OnInit, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {IDataResponseCustom, ILISTUNASSIGN} from "@hcm-mfe/goal-management/data-access/models";
import {EmployeeTargetService} from "@hcm-mfe/goal-management/data-access/services";

@Component({
  selector: 'app-set-targets-for-employees',
  templateUrl: './set-targets-for-employees.component.html',
  styleUrls: ['./set-targets-for-employees.component.scss'],
  //providers: [EmployeeTargetService],
})
export class SetTargetsForEmployeesComponent extends BaseComponent implements OnInit {
  isSubmit: boolean | undefined;
  @Output() changeTab = new EventEmitter<any>();
  private readonly subs: Subscription[] = [];
  private readonly notificationError = 'common.notification.error';
  objectSubmit = {
    empId: null,
    sotId: null,
  };
  listUnassigned: Array<ILISTUNASSIGN> | undefined;
  constructor(injector: Injector, private readonly employeeTargetService: EmployeeTargetService) {
    super(injector);
  }

  trackFntable(index: number, item: ILISTUNASSIGN) {
    return item.empId;
  }

  selectSetOfIndicator(event: any) {
    this.objectSubmit.sotId =  event.itemSelected && event.itemSelected.sotId;
  }
  backToFirstTab() {
    this.changeTab.emit({index: 0});
  }
  submitData(data: any) {
    this.isSubmit = true;
    if (!this.objectSubmit.sotId) {
      return;
    }
    const selectedItem = data;
    selectedItem.setOfTargetList.forEach((el: any) => {
      if(el.sotId === this.objectSubmit.sotId) {
        selectedItem.sotName = el.sotName;
      }
    })
    this.deletePopup.showConfirm(() => {
      this.objectSubmit.empId = data.empId;
      this.isLoading = true;
    const putNotUnAssign = this.employeeTargetService.updateAutoAssingMent(this.objectSubmit).subscribe(data  => {
      this.toastrCustom.success('Xác nhận thành công');
      this.employeeTargetService.getListNotAutoAssingMent().subscribe(
        (data: IDataResponseCustom<ILISTUNASSIGN[]>) => {
          this.listUnassigned = data.data;
          this.isLoading = false;
          if(!this.listUnassigned?.length) {
            this.backToFirstTab();
          }
        },
        (err) => {
          this.isLoading = false;
          this.toastrCustom.error(this.translate.instant(err ? err.message : this.notificationError));
        }
      )
    },err => {
      this.isLoading = false;
      this.toastrCustom.error(err.error.message);
    });
    this.subs.push(putNotUnAssign);
    }, selectedItem);
  }


  ngOnInit(): void {
    const LISTNOTAUTOASSIGN = this.employeeTargetService.getListNotAutoAssingMent().subscribe(
      (data: IDataResponseCustom<ILISTUNASSIGN[]>) => {
        this.listUnassigned = data.data;
      },
      (err) => {
        this.toastrCustom.error(this.translate.instant(err ? err.message : this.notificationError));
      }
    );
    this.subs.push(LISTNOTAUTOASSIGN);
  }

  override ngOnDestroy() {
    this.subs.forEach((s) => s.unsubscribe());
  }
}
