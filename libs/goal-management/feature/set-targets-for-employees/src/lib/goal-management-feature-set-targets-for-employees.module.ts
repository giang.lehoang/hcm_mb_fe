import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SetTargetsForEmployeesComponent} from "./set-targets-for-employees/set-targets-for-employees.component";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {TranslateModule} from "@ngx-translate/core";
import {NzTableModule} from "ng-zorro-antd/table";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
  imports: [
    CommonModule,
    SharedUiMbSelectModule,
    TranslateModule,
    NzTableModule,
    SharedUiLoadingModule,
  ],
  exports: [
    SetTargetsForEmployeesComponent
  ],
  declarations: [
    SetTargetsForEmployeesComponent
  ]
})
export class GoalManagementFeatureSetTargetsForEmployeesModule {}
