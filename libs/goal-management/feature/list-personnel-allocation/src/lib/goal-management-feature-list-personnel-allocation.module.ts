import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ListPersonnelAllocationComponent} from "./list-personnel-allocation/list-personnel-allocation.component";
import {RouterModule} from "@angular/router";
import {NzCardModule} from "ng-zorro-antd/card";
import {NzGridModule} from "ng-zorro-antd/grid";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {FormsModule} from "@angular/forms";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {NzTabsModule} from "ng-zorro-antd/tabs";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {TranslateModule} from "@ngx-translate/core";
import {GoalManagementUiFormInputModule} from "@hcm-mfe/goal-management/ui/form-input";
import {DragDropModule} from "@angular/cdk/drag-drop";
import { HcmMfeBoardPersonnelListComponent } from './list-personnel-allocation/components/hcm-mfe-board-personnel-list/hcm-mfe-board-personnel-list/hcm-mfe-board-personnel-list.component';
import { HcmMfeBoardPersonnelCardComponent } from './list-personnel-allocation/components/hcm-mfe-board-personnel-list/hcm-mfe-board-personnel-card/hcm-mfe-board-personnel-card.component';
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";

@NgModule({
    imports: [CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: ListPersonnelAllocationComponent,
            },
        ]), NzCardModule, NzGridModule, SharedUiMbSelectModule, FormsModule, SharedUiLoadingModule, NzTabsModule, SharedUiMbButtonModule, TranslateModule, GoalManagementUiFormInputModule, DragDropModule, SharedUiMbInputTextModule],
  exports: [ListPersonnelAllocationComponent],
  declarations: [ListPersonnelAllocationComponent, HcmMfeBoardPersonnelListComponent, HcmMfeBoardPersonnelCardComponent]
})
export class GoalManagementFeatureListPersonnelAllocationModule {}
