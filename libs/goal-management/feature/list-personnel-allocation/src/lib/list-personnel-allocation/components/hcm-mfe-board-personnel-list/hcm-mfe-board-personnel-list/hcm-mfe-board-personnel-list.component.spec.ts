import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HcmMfeBoardPersonnelListComponent } from './hcm-mfe-board-personnel-list.component';

describe('HcmMfeBoardPersonnelListComponent', () => {
  let component: HcmMfeBoardPersonnelListComponent;
  let fixture: ComponentFixture<HcmMfeBoardPersonnelListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HcmMfeBoardPersonnelListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HcmMfeBoardPersonnelListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
