import {Component, Input, OnInit} from '@angular/core';
import {IEmpOfAllocations} from "@hcm-mfe/goal-management/data-access/models";

@Component({
  selector: 'hcm-mfe-hcm-mfe-board-personnel-card',
  templateUrl: './hcm-mfe-board-personnel-card.component.html',
  styleUrls: ['./hcm-mfe-board-personnel-card.component.scss']
})
export class HcmMfeBoardPersonnelCardComponent implements OnInit {
  @Input() issue: IEmpOfAllocations | undefined;
  constructor() { }

  ngOnInit(): void {
  }
  roundPoint(point: number): number {
    if (point === null) {
      return 0
    }
    return Number(point.toFixed(2));
  }
}
