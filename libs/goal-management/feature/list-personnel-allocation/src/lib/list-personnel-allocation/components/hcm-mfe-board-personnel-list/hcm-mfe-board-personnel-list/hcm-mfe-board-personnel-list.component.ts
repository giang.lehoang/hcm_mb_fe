import {Component, EventEmitter, Injector, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {
  IEmpOfAllocations,
  IListPersonnelAllocationListTable,
  isColumnTableStatus,
} from "@hcm-mfe/goal-management/data-access/models";
import {cloneDeep} from "lodash";
import {CdkDragDrop, moveItemInArray, transferArrayItem} from "@angular/cdk/drag-drop";
import {ListPersonnelAllocationService} from "../../../../../../../../data-access/services/src/lib/list-personnel-allocation.service";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";

@Component({
  selector: '[hcm-mfe-hcm-mfe-board-personnel-list]',
  templateUrl: './hcm-mfe-board-personnel-list.component.html',
  styleUrls: ['./hcm-mfe-board-personnel-list.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class HcmMfeBoardPersonnelListComponent extends BaseComponent implements OnInit {

  @Input() lane: IListPersonnelAllocationListTable<IEmpOfAllocations> | undefined;
  @Input() lanes: IListPersonnelAllocationListTable<IEmpOfAllocations>[] | undefined;
  @Input() isOutAllocation: boolean | undefined;
  @Output() isChangeDropListLands = new EventEmitter<IListPersonnelAllocationListTable<IEmpOfAllocations>[]>();
  @Input() policy: boolean | undefined;

  inputKeyPressSearch: string | undefined;
  matchedIndexInitial: number | undefined;
  listDataInitialOrgResults: IEmpOfAllocations[] | undefined;
  isColumnTableStatus: string;
  laneLength: number | undefined = 0;
  constructor(
    injector : Injector,
    private allocationService: ListPersonnelAllocationService
  ) {
    super(injector);
    this.isColumnTableStatus = isColumnTableStatus[isColumnTableStatus.N];
  }

  ngOnInit(): void {
    const cloneListDataLands = cloneDeep(this.lanes);
    this.getLengthOfLane();
    this.matchedIndexInitial = this.lanes?.findIndex(el => el.isColumnTable === isColumnTableStatus.N);
    if(this.matchedIndexInitial !== undefined && this.matchedIndexInitial >= 0){
      this.listDataInitialOrgResults = cloneListDataLands && cloneListDataLands[this.matchedIndexInitial]?.empOfAllocations;
    }
    if (this.lane?.isColumnTable !== 'N' && !this.lane?.isLock) {
      this.handleOfferEmp();
    }
  }


  dropEvent(event: CdkDragDrop<IEmpOfAllocations[]>) {
    const id = event.container.id;
    const maxClass = event.item.data.maxClassificationForDecpline;
    const currentContainer = this.lanes?.find(el => el.isColumnTable !== 'N' && el.columnCode === id);
    const maxClassification = this.lanes?.find(el => el.isColumnTable !== 'N' && el.columnCode === maxClass);
    const activeContainer = this.lanes?.find(el => el.isColumnTable !== 'N' && el.active);
    // @ts-ignore
    this.handleItemSelected(activeContainer, currentContainer, event);
    // @ts-ignore
    if (event.item.data.resultNum < currentContainer?.minPoint) {
      this.toastrCustom.error(this.translate.instant("goaManagement.listPersonnelAllocation.notification.classifyInvalidPoint"));
      return;
    }
    // @ts-ignore
    if ((this.lane?.isColumnTable !== 'N' && !(this.lanes?.indexOf(currentContainer) >= this.lanes?.indexOf(maxClassification)))) {
      this.toastrCustom.error(this.translate.instant("goaManagement.listPersonnelAllocation.notification.classifyInvalid"));
      return;
    }
    const isMovingInsideTheSameListItem = event.previousContainer === event.container;
    if (isMovingInsideTheSameListItem) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }

    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    if(event.previousContainer.id === this.lanes![0]!.columnCode){

      const matchedIndex = this.listDataInitialOrgResults?.findIndex(el => el?.customId === event.item.data.customId);
      const tmp = cloneDeep(this.listDataInitialOrgResults);
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      tmp?.splice(matchedIndex!, 1);
      this.allocationService.setListDataInitialEmpOfAllocations(tmp);
    }
    this.getLengthOfLane();
    this.handleOfferEmp();
    this.isChangeDropListLands.emit(this.lanes);

  }
  getLengthOfLane() {
    this.lanes?.forEach((lane: IListPersonnelAllocationListTable<IEmpOfAllocations>) => {
      lane['laneLength'] = this.isOutAllocation ? lane.empOfAllocations.filter(el => el.isException === 'N').length : lane.empOfAllocations.length;
    });
  }
  handleOfferEmp() {
    if (!this.lanes) {
      return;
    }
    for(let i = 1; i < this.lanes.length + 1; i++) {
      if (!this.lanes[i]) {
        return;
      }
      if (!this.isOutAllocation) {
        let quantityDifference = 0;
        if (i === 1) {
          // @ts-ignore
          quantityDifference = this.lanes[i].laneLength - this.lanes[i].totalOfColumn;
          this.handleClassification(i);
        } else {
          quantityDifference = this.handleClassification(i);
        }
        const arrTemp = this.lanes[i].totalOfColumn !== null && quantityDifference > 0 ? this.lanes[i].empOfAllocations.slice(-quantityDifference) : [];
        this.lanes[i].empOfAllocations.forEach(el => {
          el.color = this.lanes && this.lanes[i].check && arrTemp.includes(el) ? 'O' : '';
        });
      } else {
        this.handleClassification(i);
      }
    }
  }
  handleItemSelected(activeContainer: IListPersonnelAllocationListTable<IEmpOfAllocations>,
                     currentContainer: IListPersonnelAllocationListTable<IEmpOfAllocations>,
                     event: CdkDragDrop<IEmpOfAllocations[]>) {
    if (activeContainer) {
      activeContainer.active = false;
    }
    if (currentContainer) {
      currentContainer.active = true;
    }
    this.lanes?.forEach(container => {
      if (!container.active) {
        container.empOfAllocations.forEach(el => {
          el.active = false;
        })
      } else {
        const itemSelected = container.empOfAllocations.find(el => el.active);
        if (itemSelected) {
          itemSelected.active = false;
        }
      }
    })
    event.item.data.active = true;
  }
  handleClassification(index: any): number {
    if (!this.lanes) {
      return 0;
    }
    let total = this.lanes[index]?.totalOfColumn;
    let realityTotal = this.lanes[index].laneLength;
    let quantityDifference = 0;
    if (index === 1) {
      // @ts-ignore
      this.lanes[index].check = this.lanes[index].totalOfColumn ? this.lanes[index].laneLength > this.lanes[index].totalOfColumn : '';
    }

    if(index > 1) {
      for(let i = index - 1 ; i > 0; i--) {
        if (this.lanes[i].check) {
          // @ts-ignore
          realityTotal += this.lanes[i].totalOfColumn;
        } else {
          // @ts-ignore
          realityTotal += this.lanes[i].laneLength;
        }
        // @ts-ignore
        total = total + this.lanes[i].totalOfColumn;
        if (this.lanes[index]) {
          this.lanes[index].check = total && realityTotal ? realityTotal > total : false;
          quantityDifference = total && realityTotal ? realityTotal - total : 0;
        }
      }
      return quantityDifference;
    }
    return 0;
  }
  searchItemInBlockInitialColumn($event: string, listData: IListPersonnelAllocationListTable<IEmpOfAllocations>[]) {

    const storeStateList = this.allocationService.getListDataInitialEmpOfAllocations();
    if (this.matchedIndexInitial !== undefined && this.matchedIndexInitial >= 0) {
      this.inputKeyPressSearch = $event;
      if (typeof($event) === 'string' && $event.trim() === '') {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        listData[this.matchedIndexInitial].empOfAllocations = storeState ? storeState : this.listDataInitialOrgResults;
        return 0;
      }else if(listData[this.matchedIndexInitial].empOfAllocations?.length <= 0) {
        listData[this.matchedIndexInitial].empOfAllocations = [];
      }

      const cloneOrgResultsInitial = storeStateList ? storeStateList.filter(el =>
        el.fullName.toUpperCase().includes($event.trim().toUpperCase())
      ) : this.listDataInitialOrgResults?.filter(el =>
        el.fullName.toUpperCase().includes($event.trim().toUpperCase())
      );

      if(cloneOrgResultsInitial){
        listData[this.matchedIndexInitial].empOfAllocations = cloneOrgResultsInitial;
      }

    }
    return 2;
  }

}
