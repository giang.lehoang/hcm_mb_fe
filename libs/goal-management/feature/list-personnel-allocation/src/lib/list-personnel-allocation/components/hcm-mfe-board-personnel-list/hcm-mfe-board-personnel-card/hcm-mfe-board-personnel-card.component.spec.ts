import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HcmMfeBoardPersonnelCardComponent } from './hcm-mfe-board-personnel-card.component';

describe('HcmMfeBoardPersonnelCardComponent', () => {
  let component: HcmMfeBoardPersonnelCardComponent;
  let fixture: ComponentFixture<HcmMfeBoardPersonnelCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HcmMfeBoardPersonnelCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HcmMfeBoardPersonnelCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
