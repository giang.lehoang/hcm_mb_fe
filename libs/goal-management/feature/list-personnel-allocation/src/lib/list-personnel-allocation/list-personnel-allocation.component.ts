import {Component, Injector, OnDestroy, OnInit, TemplateRef} from "@angular/core";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {ListPersonnelAllocationService} from "../../../../../data-access/services/src/lib/list-personnel-allocation.service";
import {NzModalRef} from "ng-zorro-antd/modal";
import {NzTabChangeEvent} from "ng-zorro-antd/tabs";
import {ListEmpAllocatedService} from "../../../../../data-access/services/src/lib/list-emp-allocated";
import {
  IEmpOfAllocations,
  IListPersonnelAllocationDetail,
  IListPersonnelAllocationListTable,
  IParamPersonnel,
  isColumnTableStatus
} from "@hcm-mfe/goal-management/data-access/models";
import {cloneDeep} from "lodash";
import {getInnerWidth} from "@hcm-mfe/policy-management/helper";
import {ActionDocumentAttachedFileComponent} from "@hcm-mfe/goal-management/ui/form-input";
import {Mode} from "@hcm-mfe/shared/common/constants";
import {KpiComplianceService} from "../../../../../data-access/services/src/lib/kpi-compliance.service";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";


@Component({
  selector: 'app-list-personnel-allocation',
  templateUrl: './list-personnel-allocation.component.html',
  styleUrls: ['./list-personnel-allocation.component.scss']
})
export class ListPersonnelAllocationComponent extends  BaseComponent implements OnInit, OnDestroy {
  modalRef: NzModalRef | undefined;
  orgGroup = '';
  tabSelectIndex = 0;
  isLock = 0;
  flagStatus: number | undefined;
  lanes: IListPersonnelAllocationListTable<IEmpOfAllocations>[] = [];
  params: IParamPersonnel = {
    periodId: '',
    objId: '',
    inAllocation: true
  };
  listPersonnelAllocationInfo: IListPersonnelAllocationDetail | undefined;
  departmentLineDropList: any = [];
  listPeriod: any = [];
  constructor (injector : Injector, readonly listPersonnelAllocation: ListPersonnelAllocationService,
               readonly listEmpAllocatedService: ListEmpAllocatedService,
               readonly listOfIndicatorService: KpiComplianceService) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.LIST_PERSONNEL_ALLOCATION}`);
    console.log(this.objFunction)
  }
  ngOnInit(): void {
    this.isLoading = true;
    this.getPeriod();
  }
  getPeriod() {
    this.listEmpAllocatedService.getPeriod().subscribe(res => {
      this.listPeriod = res.data;
      if (this.listPeriod.length) {
        this.params.periodId = +this.listPeriod[0].periodId;
        this.getDetail();
      }
    }, () => {
      this.isLoading = false;
    });
  }
  getDetail() {
    this.listPersonnelAllocation.getAllocatedOfEmployeeDetail(+this.params.periodId).subscribe(res => {
      this.listPersonnelAllocationInfo = res.data;
      this.orgGroup = res.data.orgGroup;
      this.getDepartment();
    }, () => {
      this.isLoading = false;
    })
  }
  changeObj(event: number) {

  }
  updateDropColumn($event: IListPersonnelAllocationListTable<IEmpOfAllocations>[]) {
    // assign lanes when user drag or drop column
    this.lanes = $event;
  }
  getDepartment() {
    this.listPersonnelAllocation.getDepartmentLineDropList().subscribe(res => {
      const all: any = {
        orgId: '',
        name: 'Tất cả',
      }
      this.departmentLineDropList = res.data;
      if (this.orgGroup !== 'CN') {
        this.departmentLineDropList.push(all);
      } else {
        this.params.objId = this.departmentLineDropList[0].orgId;
      }
      this.search();
    }, () => {
      this.isLoading = false;
    })
  }
  changeTab(value: NzTabChangeEvent) {
    switch (value?.index) {
      case 0:
        this.isLoading = true;
        this.tabSelectIndex = value?.index;
        this.params.inAllocation = true;
        this.lanes = [];
        this.search();
        break;
      case 1:
        this.isLoading = true;
        this.tabSelectIndex = value?.index;
        this.params.inAllocation = false;
        this.lanes = [];
        this.search();
        break;
    }
  }
  showModalUploadFile(footerTmpl: TemplateRef<any>) {
    const param = {
      periodId: this.params.periodId,
      objId: this.params.objId
    };
    this.modalRef = this.modal.create({
      nzWidth: this.getInnerWidth(3.5),
      nzTitle: this.translate.instant('targerManager.targetCategory.label.atribute.UploadFileExel'),
      nzContent: ActionDocumentAttachedFileComponent,
      nzComponentParams: {
        mode: Mode.ADD,
        url: 'urlListPersonnelAllocation',
        endpoint: 'END_POINT_GOAL_MANAGEMENT',
        microService: 'TARGET_ENDPOIN_ASSESSMENT',
        fileName: 'TEMPLATE_DANH_SACH_NHAN_SU_TRONG_NGOAI_PHAN_BO',
        authority: !this.tabSelectIndex ? 'in-apportion' : '',
        param: param
      },
      nzFooter: footerTmpl,
    });

    this.modalRef.afterClose.subscribe((result) => {
      const checkWarning = localStorage.getItem('waringUploadFile');
      if((checkWarning && JSON.parse(checkWarning)) || result?.refresh ) {
        this.isLoading = true;
        this.search();
      }
    });
  }
  getInnerWidth(point: number): number {
    if (window.innerWidth > 767) {
      return window.innerWidth / point > 1100 ? 1100 : window.innerWidth / point;
    }
    return window.innerWidth;
  }
  selectPeriod() {
    this.getDetail()
  }
  search() {
    this.flagStatus = this.listPeriod.find((el: { periodId: string | number; }) => this.params.periodId === el.periodId)?.flagStatus;
    localStorage.removeItem("waringUploadFile");
    this.isLoading = true;
    this.listPersonnelAllocation.getListAllocation(this.params).subscribe(res => {
      this.lanes = res.data;
      this.lanes.forEach(lane => {
        if (lane.isColumnTable !== 'N') {
          // @ts-ignore
          lane.empOfAllocations.sort((item1, item2) => (item2.isException < item1.isException) ? -1 : 1);
        }
      })
      if (this.lanes) {
        this.isLock = this.lanes[1]?.isLock;
      }
      const matchedIsColumnTableStatus = this.lanes?.findIndex(el => el.isColumnTable === isColumnTableStatus[isColumnTableStatus.N]);
      if(matchedIsColumnTableStatus !== undefined && matchedIsColumnTableStatus >= 0){
        const isNullListDataId = this.lanes && this.lanes[matchedIsColumnTableStatus].empOfAllocations.map(el => { return {
          ...el,
          dataId: null
        }});
        if(isNullListDataId && this.lanes){
          this.lanes[matchedIsColumnTableStatus].empOfAllocations = isNullListDataId;
        }
      }
      this.isLoading = false;
    }, () => {
      this.isLoading = false;
    })
  }
  confirmApprovalSave(footerTmplShowModalConfirmSave: TemplateRef<any>){
    const cloneDeepDataRecordInitialItem: IListPersonnelAllocationListTable<IEmpOfAllocations> | undefined = cloneDeep(this.lanes)?.find(
        el => el.isColumnTable === isColumnTableStatus[isColumnTableStatus.N]
    );

    if(cloneDeepDataRecordInitialItem && cloneDeepDataRecordInitialItem?.empOfAllocations?.length > 0){
      // show modal when list empOfAllocations has length > 0
      this.modalRef = this.modal.create({
        nzWidth: getInnerWidth(3.5),
        nzTitle: this.translate.instant('goaManagement.classification-blocks-and-lines.label.attribute.approvalSaveRecord'),
        nzContent: this.translate.instant('goaManagement.classification-blocks-and-lines.notification.warningWhenSaveRecord'),
        nzFooter: footerTmplShowModalConfirmSave,
      });
      this.modalRef.afterClose.subscribe((result) => (result?.refresh ? this.search(): ''));
    }else {
      this.save();
    }
  }
  save() {
    const cloneDeepDataRecord: IListPersonnelAllocationListTable<IEmpOfAllocations>[] | undefined = cloneDeep(this.lanes);
    // get param
    const responseFilter : IEmpOfAllocations[] | undefined = cloneDeepDataRecord?.reduce((result: IEmpOfAllocations[], currentEl) => {
      const columnCode = currentEl.columnCode;
      const transformData: any = currentEl.empOfAllocations?.map((el: IEmpOfAllocations, index: number) => {
        return  {
          periodId: this.params.periodId,
          empCode: el.empCode,
          beforeColumnCode: el.columnCode,
          afterColumnCode: currentEl.isColumnTable !== isColumnTableStatus[isColumnTableStatus.N] ? columnCode : '',
          id: el.id,
          empId: el.empId,
          color: el.color,
          apportion: this.tabSelectIndex ? '0' : '1',
          indexOfColumn: index
        }
      })

      result.push(...transformData);
      return result;
    }, []);
    const bodyResquest = {
      requests: responseFilter,
      objId: this.params.objId
    }
    // save
    this.isLoading = true;
    this.modalRef?.destroy();
    this.listPersonnelAllocation.saveListAllocation(bodyResquest).subscribe(data => {
      if(data?.status === 200){
        this.toastrCustom.success(this.translate.instant('goaManagement.system-allocation.notification.saveSuccessResponse'));
        this.search();
      }
    }, (err) => {
      this.isLoading = false;
      this.modalRef?.destroy();
      this.toastrCustom.error(err?.message ? err?.message : this.translate.instant('goaManagement.system-allocation.notification.errorResponse'));
    })
  }

  override ngOnDestroy() {
    super.ngOnDestroy();
    this.modalRef?.destroy();
    localStorage.removeItem("waringUploadFile");
  }
}
