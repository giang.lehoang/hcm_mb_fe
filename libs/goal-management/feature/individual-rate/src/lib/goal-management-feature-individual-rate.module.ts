import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {IndividualRateComponent} from "./individual-rate/individual-rate.component";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {NzGridModule} from "ng-zorro-antd/grid";
import {TranslateModule} from "@ngx-translate/core";
import {FormsModule} from "@angular/forms";
import {NzTableModule} from "ng-zorro-antd/table";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";

@NgModule({
  imports: [CommonModule, SharedUiMbSelectModule, NzGridModule, TranslateModule,
    FormsModule, NzTableModule, SharedUiMbInputTextModule, SharedUiLoadingModule, SharedUiMbButtonModule],
  exports: [IndividualRateComponent],
  declarations: [IndividualRateComponent]
})
export class GoalManagementFeatureIndividualRateModule {}
