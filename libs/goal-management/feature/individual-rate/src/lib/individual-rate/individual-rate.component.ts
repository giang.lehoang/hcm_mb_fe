import {Component, Injector, OnDestroy, OnInit} from "@angular/core";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {
  IIndividualRate,
  IParamIndividualRate,
  IParamSave,
  ItAssessmentPeriod
} from "@hcm-mfe/goal-management/data-access/models";
import {Subject, takeUntil} from "rxjs";
import {SystemAllocationService} from "@hcm-mfe/goal-management/data-access/services";
import {REGEX_NUMBER} from "@hcm-mfe/model-organization/data-access/models";

@Component({
  selector: 'app-individual-rate',
  templateUrl: './individual-rate.component.html',
  styleUrls: ['./individual-rate.component.scss']
})
export class IndividualRateComponent extends  BaseComponent implements OnInit, OnDestroy {
  params: IParamIndividualRate = {
    periodId: ''
  }
  point = 0;
  flagStatus = 0;
  isEdit = false;
  listData: IIndividualRate[] = [];
  classificationHeader: string[] | undefined;
  dataAssessmentPeriod: ItAssessmentPeriod[] = [];
  private readonly destroyed$: Subject<void> = new Subject<void>();
  constructor(injector: Injector, private systemAllocationService: SystemAllocationService) {
    super(injector);
  }
  ngOnInit(): void {
    this.isLoading = true;
    this.getPeriod();
  }
  selectPeriod() {
    this.dataAssessmentPeriod.forEach(el => {
      if (el.periodId === this.params.periodId) {
        this.flagStatus = el.flagStatus;
      }
    })
    this.getDatandividualRate();
  }
  getDatandividualRate() {
    this.isLoading = true;
    this.systemAllocationService.getIndividualRate(this.params).subscribe(res => {
      this.listData = res.data.classificationRateOutOfAllocation;
      this.isEdit = res.data.isEdit;
      this.isLoading = false;
    }, () => {
      this.isLoading = false;
    })
  }
  convertNumber(rate: any) {
    return rate === 0 ? '' + rate : rate;
  }
  processValue(event: any, value: any) {
    value.rate = event;
  }
  checkMaxPoint(event: any) {
    const pattern = REGEX_NUMBER;
    if (!pattern.test(event.key)) {
      event.preventDefault();
      return;
    }
    const value = parseInt(event.target.value + event.key);
    if (value > 100) {
      event.preventDefault();
      return;
    }
  }
  onPaste(event: any) {
    const clipboardData = event.clipboardData;
    const pastedText = clipboardData.getData('text');
    if((pastedText.includes('-') || pastedText.includes('+'))) {
      event.preventDefault();
      return;
    }
  }

  save() {
    this.isLoading = true;
    const ratingArr: IIndividualRate[] = [];
    this.listData.forEach(el => {
      const obj: IIndividualRate = {
        clrId: el.clrId,
        rate: el.rate === '' ? null : Number(el.rate)
      }
      ratingArr.push(obj);
    })
    const bodyRequest: IParamSave = {
      periodId: this.params.periodId,
      updateClassificationRateDetailDTOs: ratingArr
    }
    this.systemAllocationService.saveIndividualRate(bodyRequest).subscribe(res => {
      this.isLoading = false;
      this.getDatandividualRate();
      this.toastrCustom.success(this.translate.instant('common.notification.success'));
    }, (err) => {
      this.isLoading = false;
      if (err?.status === 400 && err?.message) {
        this.toastrCustom.error(err.message);
      } else {
        this.toastrCustom.error(this.translate.instant('common.notification.error'));
      }
    })
  }
  getPeriod() {
    this.systemAllocationService.getAssessmentPeriod().pipe(
      // it is now important to unsubscribe from the subject
      takeUntil(this.destroyed$)
    ).subscribe(data => {
        if (data.status === 200) {
          this.dataAssessmentPeriod = data.data;
          this.params.periodId = data.data[0].periodId;
          this.dataAssessmentPeriod.forEach(el => {
            if (el.periodId === this.params.periodId) {
              this.flagStatus = el.flagStatus;
            }
          })
          this.getDatandividualRate();
          // do search data
          //this.doSearch();
        }
      }, (err) => {
        this.isLoading = false;
        this.toastrCustom.error(this.translate.instant('goaManagement.system-allocation.notification.systemAllocationTotalRate'));
      }, () => this.isLoading = false
    );
  }
}
