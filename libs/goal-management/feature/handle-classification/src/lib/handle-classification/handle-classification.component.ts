import {ChangeDetectionStrategy, Component, Injector, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {catchError, combineLatest, EMPTY, map, Observable, of, switchMap, take} from "rxjs";
import {
  IChangeRatingDTOS, IClassificationList,
  ICurrentRatingDetailDTOS,
  IHandleClassification,
  IParamSearchHandleClassification,
  IParamStatusHandleClassification,
  IResponseEntityData,
  ITargetResultSyntheticOrgLine,
  ItAssessmentPeriod, status
} from "@hcm-mfe/goal-management/data-access/models";
import {handleClassification, SystemAllocationService} from "@hcm-mfe/goal-management/data-access/services";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {
  Pagination,
} from "@hcm-mfe/model-organization/data-access/models";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {validateRangeNumber} from "@hcm-mfe/shared/common/validators";
import {ModalConfirmComponent} from "@hcm-mfe/shared/ui/mb-modal-confirm";
import {Mode, userConfig} from "@hcm-mfe/shared/common/constants";
import {isNull} from "lodash";
import * as FileSaver from "file-saver";
import {NzModalRef} from "ng-zorro-antd/modal";
import {
  ModalFormHandleClassificationComponent
} from "./components/modal-form-handle-classification/modal-form-handle-classification.component";

@Component({
  selector: 'hcm-mfe-handle-classification',
  templateUrl: './handle-classification.component.html',
  styleUrls: ['./handle-classification.component.scss'],
})
export class HandleClassificationComponent extends BaseComponent implements OnInit {

  constructor(
    injector: Injector,
    private systemAllocationService: SystemAllocationService,
    private handleClassificationService: handleClassification,
  ) {
    super(injector);
    this.formControl = new FormGroup({
      periodId: new FormControl(null),
      dataId: new FormControl(),
      apportion: new FormControl(null),
      flagStatus: new FormControl(null),
    });
    this.objFunction = this.sessionService.getSessionData(
      `FUNCTION_${FunctionCode.GOA_HANDLE_CLASSIFICATION}`
    );
  }
  formControl: FormGroup;
  @ViewChild('modalCompReject') modalCompReject: ModalConfirmComponent | undefined;

  dataAssessmentPeriod$: IResponseEntityData<ItAssessmentPeriod[]> | undefined;
  listDataSyntheticOrgLine$: IResponseEntityData<ITargetResultSyntheticOrgLine[]> | undefined;

  listApportioned$ = this.handleClassificationService.listApportioned$.pipe(map(
    res => {
      this.formControl.controls['apportion'].setValue(res[0].value)
      return res;
    }
    ));
  listFlagStatus$ = this.handleClassificationService.listFlagStatus.pipe(map(
    res => {
      this.formControl.controls['flagStatus'].setValue(res[0].value)
      return res;
    }
  ));;
  pagination: Pagination = new Pagination(userConfig.pageSize);
  checked = false;
  indeterminate = false;
  setOfCheckedId = new Set<number>();
  listOfCurrentPageData: readonly IChangeRatingDTOS[] = [];
  paramSearch: IParamSearchHandleClassification = {
    periodId: '',
    page: 0,
    size: this.pagination.pageSize,
    dataId: '',
    apportion: '',
    flagStatus: 0
  }
  dataTable: IResponseEntityData<IHandleClassification<IChangeRatingDTOS>> | undefined;
  override isLoading = false;
  statusNumber: IParamStatusHandleClassification = {
    flagStatus: undefined,
    reason: '',
    rdeId: [],
    periodId: '',
    dataId: ''
  };
  modalRef: NzModalRef | undefined;
  listDataSyntheticOrgLine$Main: Observable<IResponseEntityData<ITargetResultSyntheticOrgLine[]>> | undefined;

  listOfSelection = [
    {
      text: 'Select All Row',
      onSelect: () => {
        this.onAllChecked(true);
      }
    },
  ];


  getListDataTable(){
    this.isLoading = true;
    this.handleClassificationService.listDataHandleClassification(this.paramSearch)
      .pipe(
        take(1))
    .subscribe(
      (res: IResponseEntityData<IHandleClassification<IChangeRatingDTOS>>) => {
        // this.tableConfig.total = res.data.totalElements;
        res.data.changeRatingDTOS.content.forEach((el: IChangeRatingDTOS, index: number) => {
          if(el.flagStatus === 1 || el.flagStatus === 2){
            el.disabled = false;
          }else {
            el.disabled = true;
          }
          el.id = index + el.idOrg
        });
       // this.refreshCheckedStatus();
        this.listOfCurrentPageData.forEach(item => this.updateCheckedSet(item.id, false));
        this.refreshCheckedStatus();

        this.pagination.totalElements = res.data.changeRatingDTOS?.totalElements;
        this.pagination.totalPages = res.data.changeRatingDTOS?.totalPages;
        this.pagination.numberOfElements = res.data.changeRatingDTOS?.numberOfElements;
        this.dataTable = res;
        this.isLoading = false;
      }, (err) => {
        this.isLoading = false;
        this.toastrCustom.error(this.translate.instant('goaManagement.system-allocation.notification.errorResponse'));
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  onAllChecked(value: boolean): void {
    this.listOfCurrentPageData.filter(({ disabled }) => !disabled).forEach(item => this.updateCheckedSet(item.id, value));
    this.refreshCheckedStatus();
  };

  refreshCheckedStatus(): void {
    this.checked = this.listOfCurrentPageData.every(item => this.setOfCheckedId.has(item.id));
    this.indeterminate = this.listOfCurrentPageData.some(item => this.setOfCheckedId.has(item.id)) && !this.checked;
  }

  onItemChecked(id: number, checked: boolean): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
  }

  onCurrentPageDataChange($event: readonly IChangeRatingDTOS[]): void {
    this.listOfCurrentPageData = $event;
    this.refreshCheckedStatus();
  }

  updateCheckedSet(id: number, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }

  search(){
    this.paramSearch = {
      ...this.paramSearch,
      ...this.formControl.value,
      flagStatus: isNull(this.formControl.value.flagStatus) ? '' :  this.formControl.value.flagStatus,
      apportion: isNull(this.formControl.value.apportion) ? '' :  this.formControl.value.apportion,
    }
    this.getListDataTable();
  }

  ngOnInit(): void {
    combineLatest(this.handleClassificationService.listDataSyntheticOrgLine$, this.systemAllocationService.getAssessmentPeriod()).pipe(
      map((res) => {
        if(res[0]?.data.length > 0){
          this.formControl.controls['dataId'].setValue(res[0].data[0].dataId)
        }
        if(res[1]?.data.length > 0){
          this.formControl.controls['periodId'].setValue(res[1].data[0].periodId)
        }
          this.paramSearch = {
            ...this.paramSearch,
            dataId: res[0]?.data[0]?.dataId,
            periodId: res[1]?.data[0]?.periodId,
          }
        return res;
      })
    ).subscribe(
      ([listDataSyntheticOrgLine$Main, dataAssessmentPeriod$]) => {
        this.listDataSyntheticOrgLine$ = listDataSyntheticOrgLine$Main;
        this.dataAssessmentPeriod$ = dataAssessmentPeriod$;
        this.getListDataTable();
      }, (err) => {
        this.toastrCustom.error(this.translate.instant('goaManagement.system-allocation.notification.errorResponse'));
      }
    )
  }

  onApprovalItem( rdeId: ICurrentRatingDetailDTOS[],  reason?: string){
    const listResultRdeId = rdeId.map((el: ICurrentRatingDetailDTOS) => el.rdeId );
    this.handleSubcribleApproval(status.approval, listResultRdeId, reason);
  }

  onCorrectionItem(
    itemSuggestRating: IChangeRatingDTOS,
    currentRatingDetailDTOS: ICurrentRatingDetailDTOS[],
    classificationList: IClassificationList[],
    footerTmplTable: TemplateRef<any>
  ) {
    const totalRateInit = currentRatingDetailDTOS?.reduce((previousValue, currentValue) => {
      return Number(previousValue) + Number(currentValue.numberEmpReal);
    }, 0);
      this.modalRef = this.modal.create({
        nzWidth: this.getNzWidth(),
        nzTitle: this.translate.instant('goaManagement.handleClassification.label.correctionSuggest'),
        nzContent: ModalFormHandleClassificationComponent,
        nzComponentParams: {
          mode: Mode.EDIT,
          dataItemSuggestRating: currentRatingDetailDTOS,
          dataClassificationList: classificationList,
          totalRateInit,
          detailItemCorrection: itemSuggestRating
        },
        nzFooter: footerTmplTable,
      });
      // end get new page
      this.modalRef.afterClose.subscribe((result) => (result?.refresh ? this.search() : ''));
  }

  private getNzWidth() {
    if(window.innerWidth > 767){
      return window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5;
    }
    return window.innerWidth;
  }

  rejectListSuggest(content: string){
    // closeModal
    if(this.modalCompReject){
      this.statusNumber = {
        ...this.statusNumber,
        reason: content,
      }
      this.handleClassificationService.updateStatusHandleClassification(this.statusNumber).subscribe(res => {
        if(res){
          this.getListDataTable();
          this.modalCompReject!.isVisibleCorrectionDialog = false;
          this.modalCompReject?.formSubmit.patchValue({
            content: null,
          });
          this.toastrCustom.success(this.translate.instant('goaManagement.system-allocation.notification.updateStatus'));
        }
      }, error => {
        this.isLoading = false;
        if(error.status === 400) {
          this.toastrCustom.error(error?.message ? error?.message : error?.error?.message);
        }else {
          this.toastrCustom.error(this.translate.instant('goaManagement.system-allocation.notification.errorResponse'));
        }
      })
    }
  }
  onDisplayModalReject(flagStatus: number, rdeId: ICurrentRatingDetailDTOS[]){
    if(this.modalCompReject){
      this.statusNumber = {
        flagStatus,
        rdeId: flagStatus === status.reject ? rdeId.map(el => el.rdeId) : [],
        reason: '',
        periodId: this.formControl.value.periodId,
        dataId: this.formControl.value.dataId

      };
      this.modalCompReject.isVisibleCorrectionDialog = true;
    }
  }

  onDisplayModalRejectDataSelected(){
    const listSelected = Array.from(this.setOfCheckedId);
    if(listSelected.length <= 0){
      this.toastrCustom.error(this.translate.instant('goaManagement.system-allocation.notification.errorResponse'));
      return;
    }
    if(this.modalCompReject){
      this.statusNumber = {
        flagStatus: status.rejectApproval,
        rdeId: this.extractedListRdeId(listSelected),
        reason: '',
        periodId: this.formControl.value.periodId,
        dataId: this.formControl.value.dataId
      };
      this.modalCompReject.isVisibleCorrectionDialog = true;
    }
  }



  onExportItem(dataItem: IChangeRatingDTOS){
    this.isLoading =  true;
    this.handleClassificationService.DownloadFileExelHandleClassification({
      orgId: dataItem.idOrg,
      pgrId: dataItem.pgrId ? dataItem.pgrId : '',
      apportion: dataItem.apportion,
      periodId: this.formControl.value.periodId
    }).subscribe(
      (data) => {
        const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
        FileSaver.saveAs(blob, 'TemplateItem.xlsx');
        this.isLoading = false;
      },
      (error) => {
        this.isLoading = false;
        if(error.status === 400) {
          this.toastrCustom.error(error?.message ? error?.message : error?.error?.message);
        }else {
          this.toastrCustom.error(this.translate.instant('goaManagement.system-allocation.notification.errorResponse'));
        }
      }
    );
  }

  exportExelTable(){
    this.isLoading =  true;
    this.handleClassificationService.DownloadFileExelHandleClassificationTable({
      dataId: this.formControl.value.dataId,
      periodId: this.formControl.value.periodId,
      apportion: this.formControl.value.apportion,
      flagStatus: this.formControl.value.flagStatus,
    }).subscribe(
      (data) => {
        const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
        FileSaver.saveAs(blob, 'TemplateAll.xlsx');
        this.isLoading = false;
      },
      (error) => {
        this.isLoading = false;
        if(error.status === 400) {
          this.toastrCustom.error(error?.message ? error?.message : error?.error?.message);
        }else {
          this.toastrCustom.error(this.translate.instant('goaManagement.system-allocation.notification.errorResponse'));
        }
      }
    );
  }

  approvalSelect(){
    const listSelected = Array.from(this.setOfCheckedId);
    if(listSelected.length <= 0){
      this.toastrCustom.error(this.translate.instant('goaManagement.system-allocation.notification.errorResponse'));
      return;
    }
    this.handleSubcribleApproval(status.Approved, this.extractedListRdeId(listSelected), undefined);
  }

  private extractedListRdeId(listSelected: number[]) {
    const listRdeId: number[] = [];
    if (listSelected.length > 0) {
      const listFillteredSelect: IChangeRatingDTOS[] = []
      for (const selectedItem of listSelected) {
        const item = this.dataTable?.data?.changeRatingDTOS.content.find((el, index) => el.id === selectedItem);
        if (item) {
          listFillteredSelect.push(item);
        }
      }
      if (listFillteredSelect.length > 0) {
        for (const iChangeRatingDTO of listFillteredSelect) {
          for (const currentRatingDetailDTOS of iChangeRatingDTO.currentRatingDetailDTOS) {
            listRdeId.push(currentRatingDetailDTOS.rdeId);
          }
        }

      }
    }
    return listRdeId;
  }

  changePage(event: number) {
    if (event) {
      this.paramSearch.page = event - 1;
    }
    this.getListDataTable();
  }

  handleSubcribleApproval(status: number, rdeId: number[],  reason?: string){
    this.isLoading = true;
    this.handleClassificationService.updateStatusHandleClassification({
      flagStatus: status,
      reason: reason ? reason : '',
      rdeId: rdeId,
      periodId: this.formControl.value.periodId,
      dataId: this.formControl.value.dataId
    }).pipe(take(1)).subscribe((res) => {
      if(res){
        this.getListDataTable();
        this.toastrCustom.success(this.translate.instant('goaManagement.system-allocation.notification.updateStatus'));
      }
    }, (error) => {
      this.isLoading = false;
      if(error.status === 400) {
        this.toastrCustom.error(error?.message ? error?.message : error?.error?.message);
      }else {
        this.toastrCustom.error(this.translate.instant('goaManagement.system-allocation.notification.errorResponse'));
      }
    })
  }

  override ngOnDestroy() {
    super.ngOnDestroy();
    this.modalRef?.destroy();
  }

}
