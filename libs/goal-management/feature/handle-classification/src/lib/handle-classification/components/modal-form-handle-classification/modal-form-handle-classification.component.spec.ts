import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalFormHandleClassificationComponent } from './modal-form-handle-classification.component';

describe('ModalFormHandleClassificationComponent', () => {
  let component: ModalFormHandleClassificationComponent;
  let fixture: ComponentFixture<ModalFormHandleClassificationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalFormHandleClassificationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalFormHandleClassificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
