import {Component, Injector, OnInit} from '@angular/core';
import {Mode} from "@hcm-mfe/shared/common/constants";
import {
  IChangeRatingDTOS,
  IClassificationList,
  ICurrentRatingDetailDTOS,
  IParamAdjustedHandle
} from "@hcm-mfe/goal-management/data-access/models";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {cloneDeep, isUndefined} from "lodash";
import {handleClassification} from "@hcm-mfe/goal-management/data-access/services";
import {NzModalRef} from "ng-zorro-antd/modal";

@Component({
  selector: 'hcm-mfe-modal-form-handle-classification',
  templateUrl: './modal-form-handle-classification.component.html',
  styleUrls: ['./modal-form-handle-classification.component.scss']
})
export class ModalFormHandleClassificationComponent extends BaseComponent implements OnInit {

  constructor(
    injector: Injector,
    private handleClassificationService: handleClassification,
    private readonly modalRef: NzModalRef,
    ) {
    super(injector);
  }
  mode = Mode.EDIT;
  dataItemSuggestRating: ICurrentRatingDetailDTOS[] | undefined;
  dataClassificationList: IClassificationList[] | undefined;
  errMessage: string | undefined;
  totalRateInit: number | undefined;
  dataItemSuggestRatingClone: ICurrentRatingDetailDTOS[] | undefined;
  detailItemCorrection: IChangeRatingDTOS | undefined;
  ngOnInit(): void {
    this.dataItemSuggestRatingClone = cloneDeep(this.dataItemSuggestRating);

  }
  getValueSuggestRatingItem($event: number, numberEmpReal: ICurrentRatingDetailDTOS, index: number){
    numberEmpReal.numberEmpReal = Number($event);
  }



  convertStringToBoolean(el: string) {
    return Boolean(el);
  }

  save() {
    const totalRate = this.dataItemSuggestRatingClone?.reduce((previousValue, currentValue) => {
      return Number(previousValue) + Number(currentValue.numberEmpReal);
    }, 0);
    if(this.dataItemSuggestRatingClone){
      if(totalRate !== this.totalRateInit){
        this.errMessage = this.translate.instant('goaManagement.validate.notificationCorrectionHandleClassification');
      }else {
        this.errMessage = undefined;
      }
    }
    if(isUndefined(this.errMessage)){
      const syntheticAdjustedDetails: IParamAdjustedHandle = {
        syntheticAdjustedDetails: []
      };
      if(this.dataItemSuggestRatingClone){
        for (const item of this.dataItemSuggestRatingClone) {
          syntheticAdjustedDetails.syntheticAdjustedDetails.push({
            rdeId: item.rdeId,
            totalSuggest: item.numberEmpReal
          })
        }
      }
      this.handleClassificationService.updateAdjustedHandleClassification(syntheticAdjustedDetails).subscribe(
        (res) => {
          if(res){
            this.toastrCustom.success(this.translate.instant('goaManagement.system-allocation.notification.updateStatus'));
            this.modalRef.destroy({ refresh: true });
          }
        },
        (error) => {
          if(error.status === 400) {
            this.toastrCustom.error(error?.message ? error?.message : error?.error?.message);
          }else {
            this.toastrCustom.error(this.translate.instant('goaManagement.system-allocation.notification.errorResponse'));
          }
        }
      )
    }

  }

}
