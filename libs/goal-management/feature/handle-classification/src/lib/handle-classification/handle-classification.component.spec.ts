import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HandleClassificationComponent } from './handle-classification.component';

describe('HandleClassificationComponent', () => {
  let component: HandleClassificationComponent;
  let fixture: ComponentFixture<HandleClassificationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HandleClassificationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HandleClassificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
