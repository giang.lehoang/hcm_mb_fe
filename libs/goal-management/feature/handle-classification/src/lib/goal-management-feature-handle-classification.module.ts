import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HandleClassificationComponent } from './handle-classification/handle-classification.component';
import {RouterModule} from "@angular/router";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {TranslateModule} from "@ngx-translate/core";
import {NzGridModule} from "ng-zorro-antd/grid";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzTableModule} from "ng-zorro-antd/table";
import {SharedUiMbModalConfirmModule} from "@hcm-mfe/shared/ui/mb-modal-confirm";
import {MatIconModule} from "@angular/material/icon";
import {MatMenuModule} from "@angular/material/menu";
import {MatButtonModule} from "@angular/material/button";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import { ModalFormHandleClassificationComponent } from './handle-classification/components/modal-form-handle-classification/modal-form-handle-classification.component';
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {NzTagModule} from "ng-zorro-antd/tag";
import {GoalManagementHelperModule} from "@hcm-mfe/goal-management/helper";

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: HandleClassificationComponent,
      },
    ]),
    SharedUiMbSelectModule,
    TranslateModule,
    NzGridModule,
    ReactiveFormsModule,
    SharedUiMbButtonModule,
    NzTableModule,
    SharedUiMbModalConfirmModule,
    MatIconModule,
    MatMenuModule,
    MatButtonModule,
    NzPaginationModule,
    SharedUiLoadingModule,
    SharedUiMbInputTextModule,
    FormsModule,
    NzTagModule,
    GoalManagementHelperModule,
  ],
  declarations: [
    HandleClassificationComponent,
    ModalFormHandleClassificationComponent
  ],
})
export class GoalManagementFeatureHandleClassificationModule {}
