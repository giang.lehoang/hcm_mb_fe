import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassificationOfBlocksAndLinesComponent } from './classification-of-blocks-and-lines.component';

describe('ClassificationOfBlocksAndLinesComponent', () => {
  let component: ClassificationOfBlocksAndLinesComponent;
  let fixture: ComponentFixture<ClassificationOfBlocksAndLinesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClassificationOfBlocksAndLinesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassificationOfBlocksAndLinesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
