import {Component, Injector, OnInit, TemplateRef} from '@angular/core';
import {FormGroup} from "@angular/forms";
import {
  IClassificationBlocksAndLines,
  isColumnTableStatus,
  ItAssessmentPeriod, orgResults
} from "@hcm-mfe/goal-management/data-access/models";
import {Subject, takeUntil} from "rxjs";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {
  ClassificationBlocksAndLinesService,
  SystemAllocationService
} from "@hcm-mfe/goal-management/data-access/services";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";

import {getInnerWidth} from "@hcm-mfe/policy-management/helper";
import {NzModalRef} from "ng-zorro-antd/modal";
import {
  TableConfigClassificationBlocksAndLinesComponent
} from "@hcm-mfe/goal-management/feature/block-and-line-classification";
import {ActionDocumentAttachedFileComponent} from "@hcm-mfe/goal-management/ui/form-input";
import {MICRO_SERVICE, Mode} from "@hcm-mfe/shared/common/constants";
import {cloneDeep} from "lodash";

@Component({
  selector: 'hcm-mfe-classification-of-blocks-and-lines',
  templateUrl: './classification-of-blocks-and-lines.component.html',
  styleUrls: ['./classification-of-blocks-and-lines.component.scss']
})
export class ClassificationOfBlocksAndLinesComponent extends BaseComponent implements OnInit {

  private readonly destroyed$: Subject<void> = new Subject<void>()
  override isLoading = false;
  lanes: IClassificationBlocksAndLines<orgResults>[] | undefined;
  modalRef: NzModalRef | undefined;

  constructor(
    injector: Injector,
    private systemAllocationService: SystemAllocationService,
    private classificationBlocksAndLinesService: ClassificationBlocksAndLinesService,
  ) {
    super(injector);
    this.form = this.fb.group({
      periodId: [null]
    });
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CLASSIFICATION_OF_BLOCKS_AND_LINES}`);
  }

  form: FormGroup;
  dataAssessmentPeriod: ItAssessmentPeriod[] = [];

  ngOnInit(): void {

    this.isLoading = true;
    this.systemAllocationService.getAssessmentPeriod().pipe(
      takeUntil(this.destroyed$)
    ).subscribe(data => {
        if (data.status === 200) {
          this.dataAssessmentPeriod = data.data;
          this.form.patchValue({
            periodId: this.dataAssessmentPeriod[0].periodId
          });
          this.doSearch()
        }
      }, (err) => {
        this.isLoading = false;
        this.toastrCustom.error(this.translate.instant('goaManagement.system-allocation.notification.errorResponse'));
      }, () => this.isLoading = false
    );
    this.onChanges();
  }

  doSearch(){
    this.isLoading = true;
    const requestSearch: Pick<ItAssessmentPeriod, 'periodId'> = this.form.value;
    this.classificationBlocksAndLinesService.getClassificationBlocksAndLine(Number(requestSearch.periodId)).subscribe(data => {
      if(data?.status === 200){

        // set dataId is NULL when save BE could be update data

        this.lanes = data.data;
        const matchedIsColumnTableStatus = this.lanes?.findIndex(el => el.isColumnTable === isColumnTableStatus[isColumnTableStatus.N]);
        if(matchedIsColumnTableStatus !== undefined && matchedIsColumnTableStatus >= 0){
          const isNullListDataId = this.lanes && this.lanes[matchedIsColumnTableStatus].orgResults.map(el => { return {
              ...el,
              dataId: null
          }});
          if(isNullListDataId && this.lanes){
            this.lanes[matchedIsColumnTableStatus].orgResults = isNullListDataId;

          }
        }
      }
    }, (err) => {
      this.isLoading = false;
      // this.toastrCustom.error(err?.message && err?.message);
      this.toastrCustom.error(this.translate.instant('goaManagement.system-allocation.notification.errorResponse'));
    }, () => {
      this.isLoading = false;
    })
  }

  onChanges(): void {
    this.form.valueChanges.pipe(
      takeUntil(this.destroyed$)
    ).subscribe(val => {
      this.doSearch();
    });
  }

  showModalItem($event: boolean, footerTmpl: TemplateRef<any>){
    const requestSearch: Pick<ItAssessmentPeriod, 'periodId'> = this.form.value;
    if($event){
      this.modalRef = this.modal.create({
        nzWidth: getInnerWidth(1.2),
        nzTitle: this.translate.instant('goaManagement.classification-blocks-and-lines.label.attribute.choose-list-evaluate'),
        nzContent: TableConfigClassificationBlocksAndLinesComponent,
        nzFooter: footerTmpl,
        nzComponentParams: {
          periodId: requestSearch.periodId
        }
      });
     this.modalRef.afterClose.subscribe((result) => {
       return this.modalRef?.getContentComponent().isSaveModal ? this.doSearch()  : null;
     });
    }
  }

  saveRecord() {
    const requestSearch: Pick<ItAssessmentPeriod, 'periodId'> = this.form.value;
    const cloneDeepDataRecord: IClassificationBlocksAndLines<orgResults>[] | undefined = cloneDeep(this.lanes);
    // get param
    const responseFilter : orgResults[] | undefined = cloneDeepDataRecord?.reduce((result: orgResults[], currentEl) => {
      if(currentEl.isColumnTable !== isColumnTableStatus[isColumnTableStatus.N]){

        const transformData = currentEl.orgResults?.map((el: orgResults) => {
         return  {
              dataId: el.dataId,
              periodId: requestSearch?.periodId,
              objId: el.objId,
              dataType: el.dataType,
              dataCode: currentEl.dataCode,
              dataName: el.dataName,
              flagStatus: el.flagStatus
          }
        })

        result.push(...transformData);
      }
      return result;
    }, []);

    // save
    this.isLoading = true;
    this.classificationBlocksAndLinesService.saveRecordClassificationBlocksLines(responseFilter).subscribe(data => {
      if(data?.status === 200){
        this.toastrCustom.success(this.translate.instant('goaManagement.system-allocation.notification.saveSuccessResponse'));
        this.modalRef?.destroy();
        this.doSearch();
      }
    }, (err) => {
      this.isLoading = false;
      this.toastrCustom.error(err?.message ? err?.message : this.translate.instant('goaManagement.system-allocation.notification.errorResponse'));
    }, () => {
      this.isLoading = false;
    })

  }

  confirmApprovalSave(footerTmplShowModalConfirmSave: TemplateRef<any>){
    const cloneDeepDataRecordInitialItem: IClassificationBlocksAndLines<orgResults> | undefined = cloneDeep(this.lanes)?.find(
      el => el.isColumnTable === isColumnTableStatus[isColumnTableStatus.N]
    );

    if(cloneDeepDataRecordInitialItem && cloneDeepDataRecordInitialItem?.orgResults?.length > 0){
      // show modal when list orgResults has length > 0
      this.modalRef = this.modal.create({
        nzWidth: getInnerWidth(3.5),
        nzTitle: this.translate.instant('goaManagement.classification-blocks-and-lines.label.attribute.approvalSaveRecord'),
        nzContent: this.translate.instant('goaManagement.classification-blocks-and-lines.notification.warningWhenSaveRecord'),
        nzFooter: footerTmplShowModalConfirmSave,
      });
      this.modalRef.afterClose.subscribe((result) => (result?.refresh ? this.doSearch(): ''));
    }else {
      this.saveRecord();
    }

  }

  updateDropColumn($event: IClassificationBlocksAndLines<orgResults>[]) {
    // assign lanes when user drag or drop column
    this.lanes = $event;
  }

  showModalUploadFile(footerTmpl: TemplateRef<any>){
    const requestSearch: Pick<ItAssessmentPeriod, 'periodId'> = this.form.value;
    this.modalRef = this.modal.create({
      nzWidth: getInnerWidth(3.5),
      nzTitle: this.translate.instant('targerManager.targetCategory.label.atribute.UploadFileExel'),
      nzContent: ActionDocumentAttachedFileComponent,
      nzComponentParams: {
        mode: Mode.ADD,
        url:  'urlClassification',
        endpoint: 'END_POINT_GOAL_MANAGEMENT',
        microService: MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT,
        fileName: 'TEMPLATE_CLASSIFICATION_BLOCKS_AND_LINES',
        param: `periodId=${this.form.value?.periodId}`,
        requestBlod: {
          periodId: requestSearch.periodId
        }
      },
      nzFooter: footerTmpl,
    });
    this.modalRef.afterClose.subscribe((result) => (result?.refresh ? this.doSearch(): ''));
  }

  override ngOnDestroy() {
    super.ngOnDestroy();
    this.destroyed$.next();
    this.destroyed$.complete();
    this.modalRef?.destroy();
  }

}
