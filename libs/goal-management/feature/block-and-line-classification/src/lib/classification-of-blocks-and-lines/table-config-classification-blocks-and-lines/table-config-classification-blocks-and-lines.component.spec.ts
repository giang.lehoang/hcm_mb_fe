import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableConfigClassificationBlocksAndLinesComponent } from './table-config-classification-blocks-and-lines.component';

describe('TableConfigClassificationBlocksAndLinesComponent', () => {
  let component: TableConfigClassificationBlocksAndLinesComponent;
  let fixture: ComponentFixture<TableConfigClassificationBlocksAndLinesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableConfigClassificationBlocksAndLinesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableConfigClassificationBlocksAndLinesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
