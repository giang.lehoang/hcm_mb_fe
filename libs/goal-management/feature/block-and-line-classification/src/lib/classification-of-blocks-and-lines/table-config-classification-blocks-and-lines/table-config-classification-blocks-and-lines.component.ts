import {Component, Injector, OnInit, TemplateRef, ViewChild} from '@angular/core';

import {Subject, takeUntil} from "rxjs";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {ClassificationBlocksAndLinesService} from "@hcm-mfe/goal-management/data-access/services";
import {IClassificationTablePopupBlocksLines, IListRegisterIdCheck} from "@hcm-mfe/goal-management/data-access/models";
import {MBTableConfig, Pagination, TableConfig} from "@hcm-mfe/shared/data-access/models";
import {NzSafeAny} from "ng-zorro-antd/core/types";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import events from "events";
import {NzModalRef} from "ng-zorro-antd/modal";

@Component({
  selector: 'hcm-mfe-table-config-classification-blocks-and-lines',
  templateUrl: './table-config-classification-blocks-and-lines.component.html',
  styleUrls: ['./table-config-classification-blocks-and-lines.component.scss']
})



export class TableConfigClassificationBlocksAndLinesComponent extends BaseComponent implements OnInit {

  private readonly destroyedUnMound$: Subject<void> = new Subject<void>()
  override isLoading = false;
  searchResult: IClassificationTablePopupBlocksLines[] | undefined;
  tableConfig!: MBTableConfig;
  pagination = new Pagination();
  @ViewChild('selectTmpl', {static: true}) select!: TemplateRef<NzSafeAny>;
  periodId: number | undefined;

  isSaveModal: boolean | undefined;

  listRegisterId: number[] = [];
  listRegisterIdCheck: IListRegisterIdCheck[] = []

  constructor(
    private classificationBlocksAndLinesService: ClassificationBlocksAndLinesService,
    injector: Injector,
  ) {
    super(injector);
    this.tableConfig = new MBTableConfig();
  }

  ngOnInit(): void {
    this.initTable();
    this.isLoading = true;
    this.classificationBlocksAndLinesService.getClassificationBlocksAndLinePopupList(this.periodId).pipe(
      takeUntil(this.destroyedUnMound$)
    ).subscribe(data => {
      this.searchResult = data?.data;
      for (const item of data.data) {
        if(item.flagStatus){
          this.listRegisterId.push(item.dataId);
          this.listRegisterIdCheck.push({
            dataId: item.dataId,
            dataType: item.dataType
          })
        }
      }
    }, (err) => {
      this.toastrCustom.error(this.translate.instant("goaManagement.classification-blocks-and-lines.notification.errorResponse"));
      this.isLoading = false;
    }, () => {
      this.isLoading = false;
    })
  }

  doSearchTable($event: number) {
      console.log($event);
  }

  onCheckChange(event: events, status: number, dataId: number, dataType: string) {
    const id = parseInt(event?.target?.value, 10);
    if (event.target.checked) {
      this.listRegisterId.push(id);
      this.listRegisterIdCheck.push({
        dataId,
        dataType
      });
    } else {
      this.listRegisterId.splice(this.listRegisterId.indexOf(id), 1);
      const findIndex = this.listRegisterIdCheck.findIndex(el => el.dataId === id);
      this.listRegisterIdCheck.splice(findIndex, 1);
    }
  }

  save(modalRef: NzModalRef) {
    this.isSaveModal = false;
    this.isLoading = true;

    this.classificationBlocksAndLinesService.postClassificationBlocksAndLinePopupList(this.listRegisterIdCheck).pipe(
      takeUntil(this.destroyedUnMound$)
    ).subscribe((data) => {
      if(data.status === 200){
        modalRef.close();
      }
    }, (err) => {
      this.toastrCustom.error(this.translate.instant("goaManagement.classification-blocks-and-lines.notification.errorResponse"));
      this.isLoading = false;
      this.isSaveModal = false;
    }, () => {
      this.isLoading = false;
      this.isSaveModal = true;
    })
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'goaManagement.classification-blocks-and-lines.label.table.code-classification-blocks-and-lines',
          field: 'dataCode',
          fixed: true,
          thClassList: ['text-center'],
          width: 110
        },
        {
          title: 'goaManagement.classification-blocks-and-lines.label.table.name-classification-blocks-and-lines',
          field: 'dataName',
          fixed: true,
          thClassList: ['text-center'],
          width: 110,
        },
        {
          title: 'goaManagement.system-allocation.label.table.classification',
          field: 'select',
          width: 40,
          tdTemplate: this.select,
          thClassList: ['text-nowrap', 'text-center'],
          tdClassList: ['text-nowrap', 'text-center'],
        },

      ],
      total: 0,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: this.pagination.pageNumber
    }
  };

  override ngOnDestroy() {
    super.ngOnDestroy();
    this.destroyedUnMound$.next();
    this.destroyedUnMound$.complete();
  }

}
