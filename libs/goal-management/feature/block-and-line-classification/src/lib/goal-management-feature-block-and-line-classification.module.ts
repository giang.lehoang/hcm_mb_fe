import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClassificationOfBlocksAndLinesComponent } from './classification-of-blocks-and-lines/classification-of-blocks-and-lines.component';
import {RouterModule} from "@angular/router";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {ReactiveFormsModule} from "@angular/forms";
import {NzGridModule} from "ng-zorro-antd/grid";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {TranslateModule} from "@ngx-translate/core";
import {DragDropModule} from "@angular/cdk/drag-drop";
import {GoalManagementUiFormInputModule} from "@hcm-mfe/goal-management/ui/form-input";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import { TableConfigClassificationBlocksAndLinesComponent } from './classification-of-blocks-and-lines/table-config-classification-blocks-and-lines/table-config-classification-blocks-and-lines.component';
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: ClassificationOfBlocksAndLinesComponent,
            },
        ]),
        SharedUiMbSelectModule,
        ReactiveFormsModule,
        NzGridModule,
        SharedUiLoadingModule,
        TranslateModule,
        DragDropModule,
        GoalManagementUiFormInputModule,
        SharedUiMbButtonModule,
        SharedUiMbTableModule,
        SharedUiMbTableWrapModule,
    ],
  declarations: [
    ClassificationOfBlocksAndLinesComponent,
    TableConfigClassificationBlocksAndLinesComponent
  ],
  exports: [
    ClassificationOfBlocksAndLinesComponent
  ]
})
export class GoalManagementFeatureBlockAndLineClassificationModule {}
