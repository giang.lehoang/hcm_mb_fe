import { Component, Injector, OnInit, TemplateRef } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { NzModalRef } from 'ng-zorro-antd/modal';
import {TargetAssessmentService} from "@hcm-mfe/goal-management/data-access/services";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {forkJoin, Subscription} from "rxjs";
import {ICycleAssessment, ISetupAssessmentData} from "@hcm-mfe/goal-management/data-access/models";
import {HTTP_STATUS_CODE, Mode, userConfig} from "@hcm-mfe/shared/common/constants";
import { BaseResponse, Pageable } from "@hcm-mfe/shared/data-access/models";
import {ValidateService} from "@hcm-mfe/shared/core";
import {
  mbDataSelectsAssessmentStatus
} from '@hcm-mfe/system/data-access/models';
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {FormSetupAssessmentComponent} from "@hcm-mfe/goal-management/ui/form-input";

@Component({
  selector: 'app-setup-assessment',
  templateUrl: './setup-assessment.component.html',
  styleUrls: ['./setup-assessment.component.scss'],
})
export class SetupAssessmentComponent extends BaseComponent implements OnInit {
  private readonly subs: Subscription[] = [];
  form: FormGroup;
  modalRef: NzModalRef | undefined;
  override isLoading = false;
  isSubmitted = false;
  listDataAssessment: ISetupAssessmentData[] = [];

  limit = userConfig.pageSize;
  count = 0;

  paramSearch = {
    size: this.limit,
    page: 0,
    sort: 'appCode,asc',
  };

  pageable: Pageable | undefined;
  mbDataSelectsAssessmentCycle: ICycleAssessment[] = [];
  mbDataSelectsAssessmentStatus = mbDataSelectsAssessmentStatus();
  public readonly nzAlignTh = 'left';

  constructor(
    injector : Injector,
    public validateService: ValidateService,
    private readonly targetAssessmentService: TargetAssessmentService,
  ) {
    super(injector);
    this.form = this.fb.group({
      code: [''],
      name: [''],
      cycleType: [''],
      flagStatus: [''],
    });
    this.objFunction = this.sessionService.getSessionData(
      `FUNCTION_${FunctionCode.SETUP_ASSESSMENT}`
    );

  }

  override back() {
    this.location.back();
  }

  ConvertString(value: string) {
    return Number(value);
  }

  trackFnTable(index: number, item:ISetupAssessmentData){
    return item.pcfCode;
  }

  ngOnInit(): void {
    const subInit = forkJoin([
      this.targetAssessmentService.getListAssessmentCycle(),
      this.targetAssessmentService.getListSetupAssessmentData(this.paramSearch),
    ]).subscribe(
      ([ListAssessmentCycle, ListSetupAssessmentData]) => {
        if (ListAssessmentCycle.data) {
          this.mbDataSelectsAssessmentCycle = ListAssessmentCycle.data || [];
        }
        if (ListSetupAssessmentData.data) {
          this.listDataAssessment = ListSetupAssessmentData.data.content || [];

          this.pageable = {
            totalElements: ListSetupAssessmentData.data.totalElements,
            totalPages: ListSetupAssessmentData.data.totalPages,
            currentPage: ListSetupAssessmentData.data.number + 1,
            numberOfElements: ListSetupAssessmentData.data.numberOfElements,
            size: this.limit,
          };
        }
        this.isLoading = false;
      },
      (err) => {
        this.isLoading = false;
        this.toastrCustom.error(`${this.translate.instant('common.notification.error')} : ${err.message}`);
      }
    );
    this.subs.push(subInit);
  }
  showConfirm(id: number, $event: Event): void {
    $event.stopPropagation();
    this.deletePopup.showModal(() => this.deleteSetupAssessmentItem(id));
  }

  deleteSetupAssessmentItem(id: number){
    this.isLoading = true;
    const deleteItem = this.targetAssessmentService.deleteSetupAssessmentItem(id).subscribe(
      (res)=>{
        if(res.status === HTTP_STATUS_CODE.OK){
          this.toastrCustom.success(this.translate.instant('common.notification.success'));
          this.search();
        }
        this.isLoading = false;
      }, (err) => {
          this.isLoading = false;
          this.toastrCustom.error(err.message ? err.message : this.translate.instant('common.notification.error'));
      }
    )
    this.subs.push(deleteItem);
  }
  enterSearch(event: any) {
    if (event.key === 'Enter' || event.type === 'click') {
      this.search();
    }
  }
  search($event?: any) {
    this.isSubmitted = true;
    const request: ISetupAssessmentData = this.form.value;
    this.paramSearch = {
      ...this.paramSearch,
      ...request,
    };

    if ($event) {
      this.paramSearch.page = $event - 1;
    }
    if (this.form.valid) {
      this.isLoading = true;
      const ListSetupAssessmentDataSub = this.targetAssessmentService
        .getListSetupAssessmentData(this.paramSearch)
        .subscribe((res: BaseResponse) => {
          if (res.status === HTTP_STATUS_CODE.OK) {
            this.listDataAssessment = res.data.content || [];
            this.isLoading = false;
            this.pageable = {
              totalElements: res.data.totalElements,
              totalPages: res.data.totalPages,
              currentPage: res.data.number + 1,
              numberOfElements: res.data.numberOfElements,
              size: this.limit,
            };
          }
        }, (err) => {
          this.isLoading = false;
          this.toastrCustom.error(err.message);
        });
      this.subs.push(ListSetupAssessmentDataSub);
    }
  }

  showModalAssessment(footerTmpl: TemplateRef<any>, mode: string, data?: ISetupAssessmentData) {
    this.modalRef = this.modal.create({
      nzWidth: this.getNzWidth(),
      nzTitle: this.translate.instant('targerManager.setupAssessment.label.atribute.SetupAssessment'),
      nzContent: FormSetupAssessmentComponent,
      nzComponentParams: {
        mode: mode === 'ADD' ? Mode.ADD : Mode.EDIT,
        data: data,
      },
      nzFooter: footerTmpl,
    });
    // end get new page
    this.modalRef.afterClose.subscribe((result) => (result?.refresh ? this.search() : ''));
  }

  private getNzWidth() {
    if(window.innerWidth > 767){
      return window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5;
    }
    return window.innerWidth;
  }

  override ngOnDestroy() {
    this.subs.forEach((s) => s.unsubscribe());
    this.modalRef?.destroy();
  }
}
