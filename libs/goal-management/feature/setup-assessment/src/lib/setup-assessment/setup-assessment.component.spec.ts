import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetupAssessmentComponent } from './setup-assessment.component';

describe('SetupAssessmentComponent', () => {
  let component: SetupAssessmentComponent;
  let fixture: ComponentFixture<SetupAssessmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetupAssessmentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetupAssessmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
