import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SetupAssessmentComponent} from "./setup-assessment/setup-assessment.component";
import {RouterModule} from "@angular/router";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {TranslateModule} from "@ngx-translate/core";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzTagModule} from "ng-zorro-antd/tag";
import {SharedUiMbButtonIconModule} from "@hcm-mfe/shared/ui/mb-button-icon";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import {NzGridModule} from "ng-zorro-antd/grid";
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: SetupAssessmentComponent,
      },
    ]),
    SharedUiLoadingModule,
    SharedUiMbButtonModule,
    TranslateModule,
    ReactiveFormsModule,
    SharedUiMbSelectModule,
    SharedUiMbInputTextModule,
    NzTableModule,
    NzTagModule,
    SharedUiMbButtonIconModule,
    NzPaginationModule,
    NzGridModule,
    FormsModule,
    NzCheckboxModule,
  ],
  declarations: [
    SetupAssessmentComponent
  ],
  exports: [
    SetupAssessmentComponent
  ]
})
export class GoalManagementFeatureSetupAssessmentModule {}
