import {Component, Injector, OnDestroy, OnInit, TemplateRef} from "@angular/core";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {ApproveAllocationExceptionService} from "../../../../../data-access/services/src/lib/approve-allocation-exception";
import {KpiComplianceService} from "../../../../../data-access/services/src/lib/kpi-compliance.service";
import {Mode, userConfig} from "@hcm-mfe/shared/common/constants";
import {IKpiComplianceList, IParamKpiCompliance} from "../../../../../data-access/models/src/lib/IKpiCompliance";
import {NzModalRef} from "ng-zorro-antd/modal";
import {ActionDocumentAttachedFileComponent} from "@hcm-mfe/goal-management/ui/form-input";
import {MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import { FormModalShowTreeUnitComponent } from "@hcm-mfe/shared/ui/form-modal-show-tree-unit";

@Component({
  selector: 'app-kpi-compliance',
  templateUrl: './kpi-compliance.component.html',
  styleUrls: ['./kpi-compliance.component.scss']
})
export class KpiComplianceComponent extends  BaseComponent implements OnInit, OnDestroy {
  functionCode = FunctionCode.APPROVE_EMP_ALLOCATED;
  listPeriod: any = [];
  modalRef: NzModalRef | undefined;
  listData: IKpiComplianceList[] = [];
  tableConfig: MBTableConfig = new MBTableConfig();
  heightTable = { x: '80vw', y: '50vh'};
  pagination = new Pagination();
  param: IParamKpiCompliance = {
    employeeCode: '',
    periodId: '',
    orgId: '',
    page: 0,
    size: 10
  };
  info = {
    fullName: '',
    orgName: ''
  }
  orgName = '';
  constructor (injector : Injector, readonly approveAllocationExceptionService: ApproveAllocationExceptionService,
               readonly kpiComplianceService: KpiComplianceService) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.KPI_COMPLIANCE}`);
  }
  ngOnInit(): void {
    this.isLoading = true;
    this.initTable();
    this.approveAllocationExceptionService.getPeriod().subscribe(res => {
        this.listPeriod = res.data;
        this.param.periodId = this.listPeriod[0].periodId;
        this.isLoading = false;
        this.getListKPI();
    }, () => {
      this.isLoading = false;
    });
  }
  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'goaManagement.listEmpAllocated.table.empCode',
          field: 'employeeCode',
          width: 150,
        },
        {
          title: 'goaManagement.listEmpAllocated.table.fullName',
          field: 'fullName',
          fixed: window.innerWidth > 1024,
          width: 200
        },
        {
          title: 'goaManagement.listEmpAllocated.table.orgName',
          field: 'unitName',
          needEllipsis: true,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'goaManagement.listEmpAllocated.table.position',
          field: 'positionName',
          needEllipsis: true,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'goaManagement.listEmpAllocated.table.plusPoint',
          tdClassList: ['text-right'],
          field: 'plusPoint',
          width: 110,
          fixed: window.innerWidth > 1024,
        },
        {
          title: 'goaManagement.listEmpAllocated.table.minusPoint',
          tdClassList: ['text-right'],
          field: 'minusPoint',
          width: 110,
          fixed: window.innerWidth > 1024,
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1,
      showFrontPagination: false
    };
  }
  getListKPI() {
    this.kpiComplianceService.getListKPI(this.param).subscribe(res => {
      this.listData = res.data.findRatePointDetails.content;
      this.tableConfig.total = res.data.findRatePointDetails.totalElements;
      this.isLoading = false;
    },() => {
      this.isLoading = false;
      this.toastrCustom.error(this.translate.instant("goaManagement.approveListEmpAllocated.notification.errorAPI"));
    })
  }
  search(){
    this.isLoading = true;
    this.param.page = 0;
    this.getListKPI();
  }
  showModalUploadFile(footerTmpl: TemplateRef<any>) {
    const param = { periodId: this.param.periodId };
    this.modalRef = this.modal.create({
      nzWidth: this.getInnerWidth(3.5),
      nzTitle: this.translate.instant('targerManager.targetCategory.label.atribute.UploadFileExel'),
      nzContent: ActionDocumentAttachedFileComponent,
      nzComponentParams: {
        mode: Mode.ADD,
        url: 'urlTargetResult',
        endpoint: 'END_POINT_GOAL_MANAGEMENT',
        microService: 'TARGET_ENDPOIN_ASSESSMENT',
        fileName: 'TEMPLATE_DANH_SACH_KPI_TUAN_THU',
        authority: 'rate-point',
        param: param
      },
      nzFooter: footerTmpl,
    });
    this.modalRef.afterClose.subscribe((result) => (result?.refresh ? this.getListKPI() : ''));
  }
  getInnerWidth(point: number): number {
    if (window.innerWidth > 767) {
      return window.innerWidth / point > 1100 ? 1100 : window.innerWidth / point;
    }
    return window.innerWidth;
  }
  onChange(event: any){
    this.info = {...event};
    this.param.employeeCode = event ? event.empCode : '';
  }

  changePage(value: any){
    this.param.page = value - 1;
    this.getListKPI();
  }
  showModalOrg(event: any): void {
    this.modalRef = this.modal.create({
      nzWidth: this.getInnerWidth(1),
      nzTitle: '',
      nzContent: FormModalShowTreeUnitComponent,
      nzFooter: null,
    });
    this.modalRef.afterClose.subscribe((result) => {
      if (result?.orgName) {
        this.orgName = result?.orgName;
        this.param.orgId = result?.orgId;
      }
    });
  }
  clearOrgInput() {
    this.orgName = '';
    this.param.orgId = '';
  }
  override ngOnDestroy() {
    super.ngOnDestroy();
    this.modalRef?.destroy();
  }
}
