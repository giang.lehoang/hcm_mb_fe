import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {KpiComplianceComponent} from "./kpi-compliance/kpi-compliance.component";
import {RouterModule} from "@angular/router";
import {NzCardModule} from "ng-zorro-antd/card";
import {NzGridModule} from "ng-zorro-antd/grid";
import {SharedUiMbEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/mb-employee-data-picker";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {FormsModule} from "@angular/forms";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {NzIconModule} from "ng-zorro-antd/icon";

@NgModule({
    imports: [CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: KpiComplianceComponent,
            },
        ]), NzCardModule, NzGridModule, SharedUiMbEmployeeDataPickerModule, TranslateModule,
        SharedUiMbInputTextModule, SharedUiMbSelectModule, FormsModule, SharedUiMbButtonModule, NzTableModule, NzToolTipModule, NzPaginationModule, SharedUiLoadingModule, SharedUiMbTableWrapModule, SharedUiMbTableModule, NzIconModule],
  exports: [KpiComplianceComponent],
  declarations: [KpiComplianceComponent]
})
export class GoalManagementFeatureKpiComplianceModule {}
