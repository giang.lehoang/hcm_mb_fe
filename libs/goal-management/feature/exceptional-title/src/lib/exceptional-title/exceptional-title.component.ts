import {Component, Injector, OnDestroy, OnInit} from "@angular/core";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {SystemAllocationService} from "@hcm-mfe/goal-management/data-access/services";
import {DataService} from "@hcm-mfe/model-organization/data-access/services";
import {forkJoin} from "rxjs";

@Component({
  selector: 'app-exceptional-title',
  templateUrl: './exceptional-title.component.html',
  styleUrls: ['./exceptional-title.component.scss']
})
export class ExceptionalTitleComponent extends  BaseComponent implements OnInit, OnDestroy {
  jobSelect = [];
  allFruits = [];
  deletedJob = [];
  selectedJobs = [];
  constructor(injector: Injector,
              private readonly dataService: DataService,
              private readonly systemAllocationService: SystemAllocationService) {
    super(injector);
  }
  ngOnInit(): void {
    this.callGetJob();
    this.getListJob();
  }
  getListJob() {
    const params = {
      type: 1
    }
    this.selectedJobs = [];
    this.systemAllocationService.getListAllocatedException(params).subscribe(res => {
      this.jobSelect = res.data.content;
      this.isLoading = false;
    }, () => {
      this.isLoading = false;
    })
  }
  updateJob(event: any) {
  }
  callGetJob() {
    this.dataService.getJobsByType().subscribe((data) => {
      this.allFruits = data.data.content;
    });
  }
  receiveDeletedJob(data: any) {
    if(data.length === undefined) {
      this.deletedJob.push(data.aleId);
    } else {
      const arrTemp = [...data];
      arrTemp.forEach(el => {
        this.deletedJob.push(el.aleId);
      });
    }
  }
  receiveSelectedJob(data: any) {
    this.selectedJobs = data;
  }
  create() {
    if (!this.jobSelect.length) {
      return;
    }
    const bodyRequest = [];
    this.selectedJobs.forEach(el => {
      const obj = {
        aleType: 1,
        jobId: el.jobId,
        flagStatus: 1
      }
      bodyRequest.push(obj);
    })
    this.isLoading = true;
    this.systemAllocationService.createExceptionPersonnel(bodyRequest).subscribe(res => {
      this.isLoading = false;
      if (this.deletedJob.length > 0) {
        this.systemAllocationService.deleteExceptionPersonnel(this.deletedJob).subscribe(() => {
          this.getListJob();
        });
      } else {
        this.getListJob();
      }
      this.toastrCustom.success(this.translate.instant('common.notification.success'));
    }, (err) => {
      this.isLoading = false;
      if (this.deletedJob.length > 0) {
        this.systemAllocationService.deleteExceptionPersonnel(this.deletedJob).subscribe(() => {
          this.getListJob();
          this.deletedJob = [];
        });
        return;
      }
      if (err?.status === 400 && err?.message) {
        this.toastrCustom.error(err.message);
      } else {
        this.toastrCustom.error(this.translate.instant('common.notification.error'));
      }
    })
  }
}
