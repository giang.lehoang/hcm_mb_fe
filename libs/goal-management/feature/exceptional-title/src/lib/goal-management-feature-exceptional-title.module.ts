import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import {ExceptionalTitleComponent} from "./exceptional-title/exceptional-title.component";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {FormsModule} from "@angular/forms";
import {SharedUiMbJobSelectModule} from "@hcm-mfe/shared/ui/mb-job-select";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
  imports: [CommonModule, SharedUiMbInputTextModule, FormsModule, SharedUiMbJobSelectModule, SharedUiMbButtonModule, TranslateModule, SharedUiLoadingModule],
  exports: [ExceptionalTitleComponent],
  declarations: [ExceptionalTitleComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
})
export class GoalManagementFeatureExceptionalTitleModule {}
