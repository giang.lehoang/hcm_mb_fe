import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EvaluateEmployeeResultsComponent} from "./  evaluate-employee-results/evaluate-employee-results.component";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzCardModule} from "ng-zorro-antd/card";
import {NzGridModule} from "ng-zorro-antd/grid";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbModalConfirmModule} from "@hcm-mfe/shared/ui/mb-modal-confirm";

@NgModule({
    imports: [CommonModule, SharedUiLoadingModule, SharedUiMbButtonModule,
        NzCardModule, NzGridModule, SharedUiMbInputTextModule,
        FormsModule, ReactiveFormsModule,
        RouterModule.forChild([
            {
                path: '',
                component: EvaluateEmployeeResultsComponent,
            },
        ]), TranslateModule, SharedUiMbModalConfirmModule],
  declarations: [EvaluateEmployeeResultsComponent],
  exports: [EvaluateEmployeeResultsComponent]
})
export class GoalManagementFeatureEvaluateEmployeeResultsModule {}
