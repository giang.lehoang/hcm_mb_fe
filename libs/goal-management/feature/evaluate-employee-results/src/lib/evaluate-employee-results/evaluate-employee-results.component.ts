import {Component, Injector, OnDestroy, OnInit, ViewChild} from '@angular/core';
import * as moment from 'moment';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {
  EvaluateEmployeeResultsService,
  EvaluetionApprovalSetOfIndicatorService
} from "@hcm-mfe/goal-management/data-access/services";
import {FunctionCode} from "@hcm-mfe/shared/common/enums";
import {
  getActionRejectorAprrove,
  IEmployData,
  listFlagStatus,
  mbDataSelectsAssignType,
  processFlagStatus, renderFlagStatus, ResponseEntityEmployTarget
} from "@hcm-mfe/goal-management/data-access/models";
import {HttpStatusCode} from "@angular/common/http";
import {ModalConfirmComponent} from "@hcm-mfe/shared/ui/mb-modal-confirm";
import {Subscription} from "rxjs";
import {isNull} from "lodash";

@Component({
  selector: 'app-evaluate-employee-results',
  templateUrl: './evaluate-employee-results.component.html',
  styleUrls: ['./evaluate-employee-results.component.scss'],
})
export class EvaluateEmployeeResultsComponent extends BaseComponent implements OnInit, OnDestroy {
  constructor(
      injector: Injector,
      readonly evaluateEmployeeResultsService: EvaluateEmployeeResultsService,
      readonly evaluetionApprovalSetOfIndicatorService: EvaluetionApprovalSetOfIndicatorService
  ) {
      super(injector);
    this.processFlagStatus = processFlagStatus();
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.EVALUATE_EMPLOYEE_RESULTS}`);
  }
  private readonly subs: Subscription[] = [];
  logic: number | undefined;
  listData: any = [];
  listDataHT: any = [];
  listDataQLTT: any = [];
  listParentHT: any = [];
  listParentQLTT: any = [];
  totalPointMain?: number;
  totalPointHT?: number;
  totalPointQLTT?: number;
  inFormationDeTail: any;
  checkValid = false;
  isEditAble = false;
  id: Array<number> | undefined;
  empId: number | undefined;
  processFlagStatus: listFlagStatus | null = null;
  listAssingType = mbDataSelectsAssignType();
  renderFlagStatus = renderFlagStatus;
  @ViewChild('modalCompReject') modalCompReject: ModalConfirmComponent | undefined;
  filResult(event: any, data: any, key: any) {

    data.isRequired = false;
    data.estimateNum =  data.estimateNum !== null ? data.estimateNum : 0;
    if (event !== null) {
      if (key === this.listAssingType[1].value) {
        this.inFormationDeTail.targetRatingEmployeeDetailDTOS.forEach((item: any) => {
          if (item.assignedType === this.listAssingType[1].value) {
            const index = item.resultTargetDTOs.findIndex((item: any) => item.asdId === data.asdId);
            if (index > -1) {
              item.resultTargetDTOs[index].resultNum = event;
            }
          }
        });
      }

      if (key === this.listAssingType[0].value) {
        this.inFormationDeTail.targetRatingEmployeeDetailDTOS.forEach((item: any) => {
          if (item.assignedType === this.listAssingType[0].value) {
            const index = item.resultTargetDTOs.findIndex((item: any) => item.asdId === data.asdId);
            if (index > -1) {
              item.resultTargetDTOs[index].resultNum = event;
            }
          }
        });
      }
      if (data.stdParent === null && data.targetType !== '2' && data.subs.length > 0) {
        if (data.typeRule === '0' && data.targetType === 'MY01' && data.scoringRule === 'SR01') {
          data.point = ((((data.scale)/100 * event) / data.estimateNum) * 100);
        } else if (data.typeRule === '0' && data.targetType === 'MY01' && data.scoringRule === 'SR03') {
          if (parseFloat(event) < parseFloat(data.estimateNum)) {
            data.point = 0;
          } else if (
            parseFloat(event) > parseFloat(data.estimateNum) ||
            parseFloat(event) === parseFloat(data.estimateNum)
          ) {
            data.point = (((data.scale * event) / data.estimateNum));
          }
        } else if (data.typeRule === '0' && data.targetType === 'MY02' && data.scoringRule === 'SR03') {
          if (parseFloat(event) > parseFloat(data.estimateNum)) {
            data.point = 0;
          } else if (
            parseFloat(event) < parseFloat(data.estimateNum) ||
            parseFloat(event) === parseFloat(data.estimateNum)
          ) {
            data.point = (((data.scale * event) / data.estimateNum));
          }
        } else if (data.typeRule === '0' && data.targetType === 'MY02' && data.scoringRule === 'SR02') {
          if (parseFloat(event) > parseFloat(data.estimateNum)) {
            data.point = 0;
          } else if (
            parseFloat(event) < parseFloat(data.estimateNum) ||
            parseFloat(event) === parseFloat(data.estimateNum)
          ) {
            data.point = ((data.scale)/100 * 100);
          }
        } else if (data.typeRule === '0' && data.targetType === 'MY02' && data.scoringRule === 'SR01') {
          data.point = ((((data.scale)/100 * data.estimateNum) / event) * 100);
        } else if (data.typeRule === '0' && data.targetType === 'MY01' && data.scoringRule === 'SR02') {
          if (parseFloat(event) < parseFloat(data.estimateNum)) {
            data.point = 0;
          } else if (
            parseFloat(event) > parseFloat(data.estimateNum) ||
            parseFloat(event) === parseFloat(data.estimateNum)
          ) {
            data.point = ((data.scale)/100 * 100);
          }
        }
      }else {
        if (data.typeRule === '0' && data.targetType === 'MY01' && data.scoringRule === 'SR01') {
          data.point = ((((data.scale)/100 * event) / data.estimateNum) * 100);
        } else if (data.typeRule === '0' && data.targetType === 'MY01' && data.scoringRule === 'SR03') {
          if (parseFloat(event) < parseFloat(data.estimateNum)) {
            data.point = 0;
          } else if (
            parseFloat(event) > parseFloat(data.estimateNum) ||
            parseFloat(event) === parseFloat(data.estimateNum)
          ) {
            data.point = (((data.scale * event) / data.estimateNum));
          }
        } else if (data.typeRule === '0' && data.targetType === 'MY02' && data.scoringRule === 'SR03') {
          if (parseFloat(event) > parseFloat(data.estimateNum)) {
            data.point = 0;
          } else if (
            parseFloat(event) < parseFloat(data.estimateNum) ||
            parseFloat(event) === parseFloat(data.estimateNum)
          ) {
            data.point = (((data.scale * event) / data.estimateNum));
          }
        } else if (data.typeRule === '2' && data.targetType === 'MY02' && data.scoringRule === 'SR01') {
          data.point = ((((data.scale)/100 * data.estimateNum) / event) * 100);
        } else if (data.typeRule === '2' && data.targetType === 'MY01' && data.scoringRule === 'SR01') {
          data.point = ((((data.scale)/100 * event) / data.estimateNum) * 100);
        } else if (data.typeRule === '2' && data.targetType === 'MY02' && data.scoringRule === 'SR02') {
          if (parseFloat(event) > parseFloat(data.estimateNum)) {
            data.point = 0;
          } else if (
            parseFloat(event) < parseFloat(data.estimateNum) ||
            parseFloat(event) === parseFloat(data.estimateNum)
          ) {
            data.point = ((data.scale)/100 * 100);
          }
         } else if (data.typeRule === '2' && data.targetType === 'MY01' && data.scoringRule === 'SR03') {
          if (parseFloat(event) < parseFloat(data.estimateNum)) {
            data.point = 0;
          } else if (
            parseFloat(event) > parseFloat(data.estimateNum) ||
            parseFloat(event) === parseFloat(data.estimateNum)
          ) {
            data.point = (((data.scale * event) / data.estimateNum));
          }
        }  else if (data.typeRule === '2' && data.targetType === 'MY01' && data.scoringRule === 'SR02') {
          if (parseFloat(event) <  parseFloat(data.estimateNum)) {
            data.point = 0;
          } else if (
            parseFloat(event) > parseFloat(data.estimateNum) ||
            parseFloat(event) === parseFloat(data.estimateNum)
          ) {
            data.point = (((data.scale) * 100)/100);
          }
        } else if (data.typeRule === '2' && data.targetType === 'MY02' && data.scoringRule === 'SR03') {
          if (parseFloat(event) > parseFloat(data.estimateNum)) {
            data.point = 0;
          } else if (
            parseFloat(event) < parseFloat(data.estimateNum) ||
            parseFloat(event) === parseFloat(data.estimateNum)
          ) {
            data.point = (((data.scale * event) / data.estimateNum));
          }
        } else if (data.typeRule === '0' && data.targetType === 'MY02' && data.scoringRule === 'SR02') {
          if (parseFloat(event) > parseFloat(data.estimateNum)) {
            data.point = 0;
          } else if (
            parseFloat(event) < parseFloat(data.estimateNum) ||
            parseFloat(event) === parseFloat(data.estimateNum)
          ) {
            data.point = ((data.scale)/100 * 100);
          }
        } else if (data.typeRule === '0' && data.targetType === 'MY02' && data.scoringRule === 'SR01') {
          data.point = ((((data.scale)/100 * data.estimateNum) / event) * 100);
        } else if (data.typeRule === '0' && data.targetType === 'MY01' && data.scoringRule === 'SR02') {
          if (parseFloat(event) < parseFloat(data.estimateNum)) {
            data.point = 0;
          } else if (
            parseFloat(event) > parseFloat(data.estimateNum) ||
            parseFloat(event) === parseFloat(data.estimateNum)
          ) {
            data.point = ((data.scale)/100 * 100);
          }
        }
      }
      if (data.typeRule !== '1') {
        if (data.targetType === 'MY01' && data.resultNum === 0 ) {
          data.point = 0;
        }
        if ((data.targetType === 'MY01' && data.estimateNum === 0) || (data.targetType === 'MY02' && data.resultNum === 0)) {
          data.point = data.scale;
        }
      }
      if(data.point === Infinity || data.point === 0 || !data.point) {
        data.point = 0;
      }
    } else {
      data.point = null;
      if (key === 'HT') {
        this.inFormationDeTail.targetRatingEmployeeDetailDTOS.forEach((item: any) => {
          if (item.assignedType === 'HT') {
            const index = item.resultTargetDTOs.findIndex((item: any) => item.asdId === data.asdId);
            if (index > -1) {
              item.resultTargetDTOs[index].resultNum = null;
              item.resultTargetDTOs[index].point = null;

            }
          }
        });
      }
      if (key === 'QL') {
        this.inFormationDeTail.targetRatingEmployeeDetailDTOS.forEach((item: any) => {
          if (item.assignedType === 'QL') {
            const index = item.resultTargetDTOs.findIndex((item: any) => item.asdId === data.asdId);
            if (index > -1) {
              item.resultTargetDTOs[index].resultNum = null;
              item.resultTargetDTOs[index].point = null;
            }
          }
        });
      }
    }
    const arraySelect: any = this.listDataHT.length > 0 ? this.listDataHT : this.listDataQLTT.length > 0 ?  this.listDataQLTT : [];
    const diffDayAssignment = arraySelect.length > 0 ? arraySelect[0].gaToDate && arraySelect[0].gaFromDate && moment(arraySelect[0].gaToDate).add(1, 'days').diff(moment(arraySelect[0].gaFromDate), 'days') : 0;

    const diffDayPeriod =  arraySelect ? moment(arraySelect[0].gpToDate, 'YYYY-MM-DD')
      .startOf('day').add(1, 'days')
      .diff(moment(arraySelect[0].gpFromDate, 'YYYY-MM-DD').startOf('day'), 'days'): 0 ;

    const resultFinal = (diffDayAssignment / diffDayPeriod);

    let countTotalPointHT = 0;
    let countTotalPointQLTT = 0;
    this.listParentHT.forEach((item: any) => {
      if (item.subs.length > 0) {
        let count = 0;
        item.subs.forEach((itemDetail: any) => {
          itemDetail.point = itemDetail.point !== null ? itemDetail.point : 0;
          if(!isNull(itemDetail.maxNum)){
            itemDetail.point = Number(itemDetail.point) >= Number(itemDetail.maxNum) * (Number(itemDetail?.scale) / 100)
              ? Number(itemDetail.maxNum) * (Number(itemDetail?.scale) / 100)
              : itemDetail.point;
          }
          count = parseFloat(itemDetail.point) + count;
        });
        item.point = !isNaN(count) ? count : null;
      }
        item.point = item.point !== null ? item.point : 0;
        if(!isNull(item.maxNum)){
          item.point = Number(item.point) >= Number(item.maxNum) * ( Number(item.scale) / 100 )
            ? Number(item.maxNum) * ( Number(item.scale) / 100 )
            : item.point;
        }
        (countTotalPointHT = parseFloat(item.point) + countTotalPointHT);
    });
    this.totalPointHT = countTotalPointHT;

    this.listParentQLTT.forEach((item: any) => {
      if (item.subs.length > 0) {
        let count = 0;
        item.subs.forEach((itemDetail: any) => {
          itemDetail.point = itemDetail.point !== null ? itemDetail.point : 0;
          if(!isNull(itemDetail.maxNum)){
            itemDetail.point = Number(itemDetail.point) >= Number(itemDetail.maxNum) *  ( Number(itemDetail.scale) / 100 )
              ?  Number(itemDetail.maxNum) * ( Number(itemDetail.scale) / 100 )
              : itemDetail.point;
          }
          count = parseFloat(itemDetail.point) + count;
        });
        item.point = !isNaN(count) ? count : null;
      }
        item.point = item.point !== null ? item.point : 0;
        if(!isNull(item.maxNum)){
          item.point = Number(item.point) >= Number(item.maxNum) * ( Number(item.scale) / 100 )
            ? Number(item.maxNum) * ( Number(item.scale) / 100 )
            : item.point;
        }
        countTotalPointQLTT = parseFloat(item.point) + countTotalPointQLTT;
    });

    // caculator point
    this.totalPointQLTT = countTotalPointQLTT  * (1 - this.listDataHT[0]?.scaleNumSot);
    this.totalPointHT = countTotalPointHT * this.listDataHT[0]?.scaleNumSot;
     this.totalPointMain = this.inFormationDeTail?.scaleNumAsm * (this.totalPointHT + this.totalPointQLTT);
  }

  goBackPage() {
    this.location.back();
  }

  fillDesc(event: any, data: any, key: any) {
    if (event.target.value) {
      data.description = event.target.value;
      switch (key) {
          case this.listAssingType[1].value:
              this.inFormationDeTail.targetRatingEmployeeDetailDTOS.forEach((item: any) => {
                  if (item.assignedType === this.listAssingType[1].value) {
                      item.resultTargetDTOs.forEach((itemDetail: any) => {
                          if (data.asdId === itemDetail.asdId) {
                              itemDetail.description = event.target.value;
                          }
                      });
                  }
              });
          break;
          case this.listAssingType[0].value:
              this.inFormationDeTail.targetRatingEmployeeDetailDTOS.forEach((item: any) => {
                  if (item?.assignedType === this.listAssingType[0].value) {
                      item.resultTargetDTOs.forEach((itemDetail: any) => {
                          if (data.asdId === itemDetail.asdId) {
                              itemDetail.description = event.target.value;
                          }
                      });
                  }
              });
          break;
      }

    }
  }

  saveEvaluateEmployeeResult() {
    this.evaluateEmployeeResultsService.upDateEveluateEmployeeResult(this.inFormationDeTail).subscribe(
      (data) => {
        this.toastrCustom.success('Cập nhật thành công')
        this.router.navigateByUrl('/goal-management/periodic-goal-management/evaluetion-approval-set-of-indicator');
      },
      (err) => {
        this.toastrCustom.error(`${this.translate.instant('common.notification.error')} : ${err?.message}`);
      }
    );
  }

  checkValidate(listData: any) {
    listData.forEach((item: any) => {
      if(item.scale !== 0 && (item.resultNum === null || item.resultNum === '')) {
        item.isRequired = true;
        if (item.subs?.length) {
          item.isRequired = false;
        }
      } else {
        item.isRequired = false;
      }
      if(item.subs?.length) {
        this.checkValidate(item.subs);
      }
    })

  }
  validateInput(listDataHT: any, listDataQLTT: any) {
    if(listDataHT.some((item: any) => item.isRequired) || listDataQLTT.some((item: any) => item.isRequired)){
      this.checkValid = true;
    } else {
      this.checkValid = false;
    }
  }
  sendEmployee() {
    this.checkValidate(this.listParentHT);
    this.checkValidate(this.listParentQLTT);
    this.validateInput(this.listParentHT, this.listParentQLTT);
    if (!this.checkValid) {
      this.isLoading = true;
      const cloneObject = JSON.parse(JSON.stringify(this.inFormationDeTail));
      cloneObject.empId = this.empId;
      cloneObject.asmIds =   this.id;
      cloneObject.targetRatingDTO =   this.inFormationDeTail;
      delete cloneObject.fullName;
      delete cloneObject.periodName;
      delete cloneObject.periodYear;
      delete cloneObject.totalPoint;
      delete cloneObject.posName;
      delete cloneObject.empCode;
      delete cloneObject.asmId;
      delete cloneObject.targetRatingEmployeeDetailDTOS;
      const sub = this.evaluateEmployeeResultsService.sendEveluateEmployeeResult(cloneObject).subscribe(
        (data) => {
            this.toastrCustom.success(
                this.translate.instant('goaManagement.evaluation-approval-set-of-indicator.notification.sendSucess')
            );
          this.router.navigateByUrl('/goal-management/periodic-goal-management/evaluetion-approval-set-of-indicator');
          this.isLoading = false;
        },
        (err) => {
          this.toastrCustom.error(err.message);
          this.isLoading = false;
        }
      );
      this.subs.push(sub);
    } else {
      this.toastrCustom.error('common.notification.requiredResult');
    }
  }
  private getArrayOrderByDisplaySeqAndScale(arrTemp: any[]) {
    return arrTemp.sort((a: any, b: any) => {
      if (a.displaySeq > b.displaySeq) {
        return 1;
      } else if (a.displaySeq === b.displaySeq) {
        if (a.scale > b.scale) {
          return -1;
        } else return 1;
      }
      return -1;
    });
  }
  convertResultNum(data: any) {
    data.map((el: any) => {
      if (el.resultNum !== null) {
        el.resultNum = '' + el.resultNum;
      }
      if (el.subs) {
        this.convertResultNum(el.subs);
      }
    })
  }
  ngOnInit(): void {
    this.isLoading = true;
    const objectData = JSON.parse(<string>localStorage.getItem('objectDetail'));
    this.logic = objectData ? objectData.logic : '';
    this.id = objectData ? objectData.asmIds : '';
    this.empId = objectData ? objectData.empId : '';
    if (objectData?.asmIds && objectData.asmIds?.length > 0) {
      this.evaluateEmployeeResultsService.getListTargetPlanStaff(objectData.asmIds).subscribe(
        (data) => {
          this.inFormationDeTail = data.data;
          this.isEditAble = this.inFormationDeTail.targetRatingEmployeeDetailDTOS[0]?.isEditAble;
          const cloneResultArray = JSON.parse(JSON.stringify(this.inFormationDeTail.targetRatingEmployeeDetailDTOS));
          cloneResultArray.forEach((item: any) => {
            item.resultTargetDTOs.forEach((itemDetail: any) => {
              itemDetail.scale = Math.round( itemDetail.scale * 100 * 100) / 100;
            })
          })
          const listDataHT = cloneResultArray.filter((item: any) => item.assignedType === this.listAssingType[1].value);
          this.listDataHT = listDataHT;

          const listDataQLTT = cloneResultArray.filter(
            (item: any) => item.assignedType === this.listAssingType[0].value
          );
          this.listDataQLTT = listDataQLTT;

          let listParentHT: any = [];
          let listChildHT: any = [];
          let listParentQLTT: any = [];
          let listChildQLTT: any = [];
          listDataHT.forEach((ele: any) => {
            listParentHT = ele.resultTargetDTOs.filter((item: any) => item.stdParent === null);
            listChildHT = ele.resultTargetDTOs.filter((item: any) => item.stdParent !== null);
          });

          listDataQLTT.forEach((ele: any) => {
            listParentQLTT = ele.resultTargetDTOs.filter((item: any) => item.stdParent === null);
            listChildQLTT = ele.resultTargetDTOs.filter((item: any) => item.stdParent !== null);
          });
          const arraySelect = listDataHT.length > 0 ? listDataHT : listDataQLTT.length > 0 ?  listDataQLTT : [];
          const diffDayAssignment = arraySelect.length > 0 ? arraySelect[0].gaToDate && arraySelect[0].gaFromDate && moment(arraySelect[0].gaToDate).diff(moment(arraySelect[0].gaFromDate), 'days') : 0;

          const diffDayPeriod =  arraySelect ? moment(arraySelect[0].gpToDate, 'YYYY-MM-DD')
            .startOf('day')
            .diff(moment(arraySelect[0].gpFromDate, 'YYYY-MM-DD').startOf('day'), 'days'): 0 ;

          // calculator total result final of detail
          const resultFinal = ((diffDayAssignment + 1) / (diffDayPeriod + 1));
          listParentQLTT.forEach((itemParent: any) => {
            itemParent.subs = [];
            listChildQLTT.forEach((itemChild: any) => {
              if (itemParent.stdId === itemChild.stdParent) {
                itemParent.subs.push(itemChild);
              }
            });
          });


          let countTotalPointQLTT = 0;
          let countTotalPointHT = 0;
          listParentQLTT.forEach((item: any) => {
            let countParent = 0;
            item.subs.forEach((itemDetail: any) => {
              itemDetail.point =
                itemDetail.point === null || itemDetail.point === 'Infinity' || itemDetail.point === 'NaN'
                  ? 0
                  : itemDetail.point;
              if(!isNull(itemDetail.maxNum)){
                itemDetail.point = Number(itemDetail.point) >= Number(itemDetail.maxNum) * (Number(itemDetail?.scale) / 100)
                  ? Number(itemDetail.maxNum) * (Number(itemDetail?.scale) / 100)
                  : itemDetail.point;
              }
              countParent = parseFloat(itemDetail.point) + countParent;
            });
            item.point = item.point === null || item.point === 'Infinity' || item.point === 'NaN' ? 0 : item.point;
            item.point = item.subs.length > 0 ? countParent : item.point;
            if(!isNull(item.maxNum)){
              item.point = Number(item.point) >= Number(item.maxNum) * ( Number(item.scale) / 100 )
                ? Number(item.maxNum) * ( Number(item.scale) / 100 )
                : item.point;
            }
          });
          listParentQLTT.forEach((item: any) => {
            countTotalPointQLTT = parseFloat(item.point) + countTotalPointQLTT;
          });
          this.totalPointQLTT = countTotalPointQLTT *  (1 - listDataHT[0]?.scaleNumSot);
          listParentHT.forEach((itemParent: any) => {
            itemParent.subs = [];
            listChildHT.forEach((itemChild: any) => {
              if (itemParent.stdId === itemChild.stdParent) {
                itemParent.subs.push(itemChild);
              }
            });
          });
          this.listParentHT = listParentHT;
          this.listParentQLTT = listParentQLTT;
          this.convertResultNum(this.listParentHT);
          this.convertResultNum(this.listParentQLTT);
          this.listParentHT = this.getArrayOrderByDisplaySeqAndScale([...this.listParentHT]);
            for (let index = 0; index <= this.listParentHT.length; index++) {
              if (this.listParentHT[index]?.subs) {
                this.listParentHT[index].subs = this.getArrayOrderByDisplaySeqAndScale([...this.listParentHT[index]?.subs]);
              }
            }
          this.listParentQLTT = this.getArrayOrderByDisplaySeqAndScale([...this.listParentQLTT]);
          for (let index = 0; index <= this.listParentQLTT.length; index++) {
            if (this.listParentQLTT[index]?.subs) {
              this.listParentQLTT[index].subs = this.getArrayOrderByDisplaySeqAndScale([...this.listParentQLTT[index]?.subs]);
            }
          }

          const cloneArray = JSON.parse(JSON.stringify(this.inFormationDeTail.targetRatingEmployeeDetailDTOS));;
          this.listData = [...cloneArray];
          this.listData.forEach((item: any) => {
            if (item?.assignedType === this.listAssingType[1].value) {
              item.resultTargetDTOs.forEach((itemDetail: any) => {
                listParentHT.forEach((itemHT: any) => {
                  if (itemDetail.stdParent === null && itemHT.asdId === itemDetail.asdId) {
                    itemDetail.subs = itemHT.subs;
                  }
                });
              });
            } else if (item?.assignedType === this.listAssingType[0].value) {
              item.resultTargetDTOs.forEach((itemDetail: any) => {
                listParentQLTT.forEach((itemQLTT: any) => {
                  if (itemDetail.stdParent === null && itemQLTT.asdId === itemDetail.asdId) {
                    itemDetail.subs = itemQLTT.subs;
                  }
                });
              });
            }
          });

          listParentHT.forEach((item: any) => {
            let countParent = 0;

            item.subs.forEach((itemDetail: any) => {
              itemDetail.point =
                itemDetail.point === null || itemDetail.point === 'Infinity' || itemDetail.point === 'NaN'
                  ? 0
                  : itemDetail.point;
              if(!isNull(itemDetail.maxNum)){
                itemDetail.point = Number(itemDetail.point) >= Number(itemDetail.maxNum) *  ( Number(itemDetail.scale) / 100 )
                  ?  Number(itemDetail.maxNum) * ( Number(itemDetail.scale) / 100 )
                  : itemDetail.point;
              }
              countParent = parseFloat(itemDetail.point) + countParent;
            });
            item.point = item.point === null || item.point === 'Infinity' || item.point === 'NaN' ? 0 : item.point;
            item.point = item.subs.length > 0 ? countParent : item.point;
            if(!isNull(item.maxNum)){
              item.point = Number(item.point) >= Number(item.maxNum) * ( Number(item.scale) / 100 )
                ? Number(item.maxNum) * ( Number(item.scale) / 100 )
                : item.point;
            }
          });
          listParentHT.forEach((item: any) => {
            countTotalPointHT = parseFloat(item.point) + countTotalPointHT;
          });
          console.log(this.listParentHT)
          console.log(this.listParentQLTT)
          this.totalPointHT = countTotalPointHT * listDataHT[0]?.scaleNumSot;
          this.totalPointMain = this.inFormationDeTail?.scaleNumAsm * (this.totalPointHT + this.totalPointQLTT);
          this.isLoading = false;

        },
        (err) => {
          this.toastrCustom.error(err?.error ? err?.error?.message : err?.message);
          this.isLoading = false;
        }, () => {
          this.isLoading = false;
        }
      );
    }
  }
  approveEmployeeResult() {
    this.isLoading = true;
    const data = JSON.parse(localStorage.getItem('objectDetail'));
    const { asmIds, empId, periodId, totalResultNum } = data;
    const paramApprove = {
      asmIds,
      empId,
      periodId,
      totalResultNum,
      content: null,
      action: getActionRejectorAprrove().approve,
    };
    const sub = this.evaluetionApprovalSetOfIndicatorService
      .ApprovalorRejectEmployeeEvaluetionApproval(paramApprove)
      .subscribe(
        (res: ResponseEntityEmployTarget<null>) => {
          if (res.status === HttpStatusCode.Ok) {
            this.isLoading = false;
            this.toastrCustom.success(
              this.translate.instant('targerManager.personalTargetList.label.table.completedApproveAssign')
            );
            this.router.navigateByUrl('goal-management/periodic-goal-management/evaluetion-approval-set-of-indicator');
          } else {
            this.isLoading = false;
          }
        },
        (err: any) => {
          this.isLoading = false;
          this.toastrCustom.error(err.message);
        }
      );
    this.subs.push(sub);
  }
  showModalReject() {
    if (this.modalCompReject) {
      this.modalCompReject.isVisibleCorrectionDialog = true
    }
  }
  rejectPlanAssignment($event: string): void {
    const data = JSON.parse(localStorage.getItem('objectDetail'));
    const { asmIds, empId, periodId, totalResultNum } = data;
    const paramApprovePlan = {
      asmIds,
      empId,
      periodId,
      flagStatus: undefined,
      content: $event,
      totalResultNum,
      action: getActionRejectorAprrove().reject,
    };
    // delete this.paramApprovePlan.action;
    const sub = this.evaluetionApprovalSetOfIndicatorService
      .ApprovalorRejectEmployeeEvaluetionApproval(paramApprovePlan)
      .subscribe(
        (res: ResponseEntityEmployTarget<null>) => {
          if (res.status === HttpStatusCode.Ok) {
            this.toastrCustom.success(
              this.translate.instant('targerManager.personalTargetList.label.table.rejectAssign')
            );
            if (this.modalCompReject) {
              this.modalCompReject.formSubmit.patchValue({
                content: null,
              });
              this.modalCompReject.isVisibleCorrectionDialog = false;
            }
            this.router.navigateByUrl('goal-management/periodic-goal-management/evaluetion-approval-set-of-indicator');
          }
        },
        (err: any) => {
          this.toastrCustom.error(err.message);
        }
      );
    this.subs.push(sub);
  }
  override ngOnDestroy() {
    localStorage.removeItem("objectDetail");
    this.subs.forEach((s) => s.unsubscribe());
  }
}
