import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormTargetCategoryComponent} from "./form-target-category/form-target-category.component";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {NzGridModule} from "ng-zorro-antd/grid";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ActionDocumentAttachedFileComponent} from "./action-document-attached/action-document-attached-file.component";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {TranslateModule} from "@ngx-translate/core";
import {NzUploadModule} from "ng-zorro-antd/upload";
import {NzListModule} from "ng-zorro-antd/list";
import {NzAlertModule} from "ng-zorro-antd/alert";
import {
  FormBalancedScorecardManagementComponent
} from "./form-balanced-scorecard-management/form-balanced-scorecard-management.component";
import {FormSetupAssessmentComponent} from "./form-setup-assessment/form-setup-asssessment.component";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {FormSearchEmployeeComponent} from "./form-search-employee/form-search-employee.component";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiMbDropDowListLazySearchModule} from "@hcm-mfe/shared/ui/mb-drop-dow-list-lazy-search";
import {NzInputModule} from "ng-zorro-antd/input";
import { BoardDndListComponent } from './board-dnd-list/board-dnd-list.component';
import { CardBlockAndLineComponent } from './card-block-and-line/card-block-and-line.component';
import {DragDropModule} from "@angular/cdk/drag-drop";
import {SharedUiMbEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/mb-employee-data-picker";
import {SharedUiMbSelectCheckAbleModule} from "@hcm-mfe/shared/ui/mb-select-check-able";
import {NzFormModule} from "ng-zorro-antd/form";


@NgModule({
    imports: [CommonModule,
        FormsModule,
        SharedUiMbSelectModule,
        NzGridModule,
        ReactiveFormsModule,
        SharedUiLoadingModule,
        TranslateModule,
        NzUploadModule,
        NzListModule,
        NzAlertModule,
        SharedUiMbDatePickerModule,
        SharedUiMbButtonModule,
        SharedUiMbDropDowListLazySearchModule,
        NzInputModule, SharedUiMbInputTextModule, DragDropModule, SharedUiMbEmployeeDataPickerModule, SharedUiMbSelectCheckAbleModule, NzFormModule],
  exports: [
    FormTargetCategoryComponent,
    ActionDocumentAttachedFileComponent,
    FormBalancedScorecardManagementComponent,
    FormSetupAssessmentComponent,
    FormSearchEmployeeComponent,
    BoardDndListComponent,
    CardBlockAndLineComponent
  ],
  declarations: [
    FormTargetCategoryComponent,
    ActionDocumentAttachedFileComponent,
    FormBalancedScorecardManagementComponent,
    FormSetupAssessmentComponent,
    FormSearchEmployeeComponent,
    BoardDndListComponent,
    CardBlockAndLineComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GoalManagementUiFormInputModule {
}
