import {Component, Input, OnInit} from '@angular/core';
import {
  orgResults
} from "../../../../../data-access/models/src/lib/IClassificationBlocksAndLines";

@Component({
  selector: 'hcm-mfe-card-block-and-line',
  templateUrl: './card-block-and-line.component.html',
  styleUrls: ['./card-block-and-line.component.scss']
})
export class CardBlockAndLineComponent implements OnInit {
  @Input() issue: orgResults | undefined;

  constructor() { }

  ngOnInit() {
  }

}
