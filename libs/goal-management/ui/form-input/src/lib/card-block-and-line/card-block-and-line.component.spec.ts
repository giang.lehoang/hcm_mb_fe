import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardBlockAndLineComponent } from './card-block-and-line.component';

describe('CardBlockAndLineComponent', () => {
  let component: CardBlockAndLineComponent;
  let fixture: ComponentFixture<CardBlockAndLineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardBlockAndLineComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardBlockAndLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
