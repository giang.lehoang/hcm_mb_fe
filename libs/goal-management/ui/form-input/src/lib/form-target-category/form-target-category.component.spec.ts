import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormTargetCategoryComponent } from './form-target-category.component';

describe('FormTargetCategoryComponent', () => {
  let component: FormTargetCategoryComponent;
  let fixture: ComponentFixture<FormTargetCategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormTargetCategoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormTargetCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
