import { Component, Injector, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NzModalRef } from 'ng-zorro-antd/modal';
import {HTTP_STATUS_CODE, Mode} from "@hcm-mfe/shared/common/constants";
import {
  DataDetailTargetCategory,
  IAsssessmentCustomSelect, ListBallanceScoreCardManagement,
  mbDataSelectScoringRule,
  mbDataSelectsSub, mbDataSelectTargetShare,
  mbDataSelectTargetType, ResponseEntity,
  TargetCategory, TargetParent
} from "@hcm-mfe/goal-management/data-access/models";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {mbDataSelectsAssessmentStatus} from "@hcm-mfe/system/data-access/models";
import {Subscription} from "rxjs";
import {BallancedScoreCardService, TargetCategoryService} from "@hcm-mfe/goal-management/data-access/services";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";

@Component({
  selector: 'app-form-target-category',
  templateUrl: './form-target-category.component.html',
  styleUrls: ['./form-target-category.component.scss'],
})
export class FormTargetCategoryComponent extends BaseComponent implements OnInit {
  mode = Mode.ADD;
  data: TargetCategory | undefined;
  targetCategoryForm: FormGroup;
  isLoadingPage = false;
  isSubmitted = false;
  listSub = mbDataSelectsSub();

  listScoringRule = mbDataSelectScoringRule();

  listTargetType = mbDataSelectTargetType();

  listStatus = mbDataSelectsAssessmentStatus();

  listShare = mbDataSelectTargetShare();

  listBallanceScoreCardManagement: Array<ListBallanceScoreCardManagement> | undefined;
  valueSelectedTypeRule = 0;
  valueSelectedScoreCard = 'TC';
  listParent: Array<TargetParent> | undefined;
  readonly subs: Subscription[] = [];

  constructor(
    injector : Injector,
    readonly ballancedScoreCardService: BallancedScoreCardService,
    readonly targetCategoryService: TargetCategoryService,
    readonly modalRef: NzModalRef,
  ) {
    super(injector);
    this.targetCategoryForm = new FormGroup({
      tarCode: new FormControl('', [Validators.required, Validators.maxLength(50)]),
      tarName: new FormControl('', [Validators.required, Validators.maxLength(200)]),
      scoreCard: new FormControl('TC', [Validators.required]),
      typeRule: new FormControl('0', [Validators.required]),
      scoringRule: new FormControl('SR01', [Validators.required]),
      targetType: new FormControl('MY01', [Validators.required]),
      flagStatus: new FormControl(1, [Validators.required]),
      targetShare: new FormControl('0', [Validators.required]),
      parentId: new FormControl(null, []),
      maxNum: new FormControl(null, []),
    });
  }

  ngOnInit(): void {
    const ListBallanceScoreCard = this.ballancedScoreCardService.getListBallancedGoalActive().subscribe(
      (data: ResponseEntity<ListBallanceScoreCardManagement>) => {
        this.listBallanceScoreCardManagement = data.data || [];
      },
      (error) => {
        this.toastrCustom.error(error.error.message);
      }
    );
    this.subs.push(ListBallanceScoreCard);
    if (this.data && this.data.typeRule && this.data.typeRule !== 'Không sub') {
      const array: Array<IAsssessmentCustomSelect> = [];
      this.listSub.forEach((ele) => {
        if (ele.label === this.data?.typeRule) {
          array.push(ele);
        }
      });
      this.convertScoreCard();
      const paramsSubmit = {
        typeRule: array[0].value,
        scoreCard: this.valueSelectedScoreCard,
      };
      const ListParentInint = this.targetCategoryService.getListTargetParent(paramsSubmit).subscribe(
        (data: ResponseEntity<TargetParent>) => {
          this.listParent = data.data;
        },
        (err) => {
          this.toastrCustom.error(err.error.message);
        }
      );
      this.subs.push(ListParentInint);
    }

    if (this.mode === Mode.EDIT) {
      const TargetCategoryDetail = this.targetCategoryService.getDetailTargetCategory(this.data?.tarId).subscribe(
        (data: DataDetailTargetCategory) => {
          this.targetCategoryForm.patchValue(data.data);
        },
        (err) => {
          this.toastrCustom.error(err.error.message);
        }
      );
      this.subs.push(TargetCategoryDetail);
    }
  }

  // move code from fe to microservice
  // code bellow author: tung.os
   convertScoreCard() {
    switch (this.data?.scoreCard) {
      case 'Khách hàng thân thiết':
        this.valueSelectedScoreCard = 'CNTT';
        break;
      case 'Tài chính':
        this.valueSelectedScoreCard = 'TC';
        break;
      case 'Khách hàng':
        this.valueSelectedScoreCard = 'KH';
        break;
      case 'Con người  tổ chức':
        this.valueSelectedScoreCard = 'CNTC';
        break;
    }
  }
  saveTargetCategory() {
    this.isSubmitted = true;
    let checkValid = false;
    this.targetCategoryForm.value.flagStatus = parseInt(this.targetCategoryForm.value.flagStatus, 10);
    this.targetCategoryForm.value.targetShare = parseInt(this.targetCategoryForm.value.targetShare, 10);
    type ObjectSubmit = Pick<ListBallanceScoreCardManagement, 'lcoId' | 'lvaValue'>;
    const objectSubmit: ObjectSubmit = {
      lcoId: 0,
      lvaValue: '',
    };
    this.listBallanceScoreCardManagement?.filter((item) => item.lvaValue === this.targetCategoryForm.controls['scoreCard'].value)
      .forEach((element) => {
        objectSubmit.lcoId = element.lcoId;
        objectSubmit.lvaValue = element.lvaValue;
      });
    this.targetCategoryForm.value.scoreCard = objectSubmit;
    if (!this.targetCategoryForm.valid) {
      checkValid = true;
    }
    if (checkValid) {
      this.isSubmitted = true;
      Object.values(this.targetCategoryForm.controls).forEach((control) => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    } else {
      if (this.mode === Mode.ADD) {
        this.targetCategoryForm.value.tarId = null;
        const CreateTargetCategory = this.targetCategoryService
          .createTargetCategory(this.targetCategoryForm.value)
          .subscribe(
            (res: BaseResponse) => {
              if (res.status === HTTP_STATUS_CODE.OK) {
                this.toastrCustom.success('Tạo mới thành công');
                this.modalRef.close({ refresh: true });
              }
            },
            (err) => {
              this.toastrCustom.error(err.error.message);
            }
          );
        this.subs.push(CreateTargetCategory);
      } else if (this.mode === Mode.EDIT) {
        this.targetCategoryForm.value.tarId = this.data?.tarId;
        const UpdateTargetCategory = this.targetCategoryService
          .createTargetCategory(this.targetCategoryForm.value)
          .subscribe(
            (res: BaseResponse) => {
              if (res.status === HTTP_STATUS_CODE.OK) {
                this.toastrCustom.success('Cập nhật thành công');
                this.modalRef.close({ refresh: true });
              }
            },
            (err) => {
              this.toastrCustom.error(err.error.message);
            }
          );
        this.subs.push(UpdateTargetCategory);
      }
    }
  }

  selectSub(value: any) {
    if (value !== 0) {
      this.valueSelectedTypeRule = value;
      const paramsSubmit = {
        typeRule: this.valueSelectedTypeRule,
        scoreCard: this.valueSelectedScoreCard,
      };
      const ListParent = this.targetCategoryService.getListTargetParent(paramsSubmit).subscribe(
        (data: ResponseEntity<TargetParent>) => {
          this.listParent = data.data;
        },
        (err) => {
          this.toastrCustom.error(err.error.message);
        }
      );
      this.subs.push(ListParent);
    }
  }

  selectScoreCard(value: any) {
    this.valueSelectedScoreCard = value;
    if (this.valueSelectedTypeRule !== 0 || this.targetCategoryForm.get('typeRule')?.value !== 0) {
      const paramsSubmit = {
        typeRule: this.targetCategoryForm.get('typeRule')?.value,
        scoreCard: this.valueSelectedScoreCard,
      };
      const ListParent = this.targetCategoryService.getListTargetParent(paramsSubmit).subscribe(
        (data: ResponseEntity<TargetParent>) => {
          this.listParent = data.data;
        },
        (err) => {
          this.toastrCustom.error(err.error.message);
        }
      );
      this.subs.push(ListParent);
    }
  }
  removeSpace(key: any) {
    this.targetCategoryForm.controls[`${key}`].setValue(this.targetCategoryForm.controls[`${key}`].value?.trim());
  }

  override ngOnDestroy() {
    this.subs.forEach((s) => s.unsubscribe());
  }
}
