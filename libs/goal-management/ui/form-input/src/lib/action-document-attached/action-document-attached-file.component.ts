import { Component, Input } from '@angular/core';
import { FormBuilder } from "@angular/forms";
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { ToastrService } from 'ngx-toastr';
import {Observable, Subscription} from 'rxjs';
import {Mode} from "@hcm-mfe/shared/common/constants";
import {TargetAssessmentService} from "@hcm-mfe/goal-management/data-access/services";
// @ts-ignore
import * as FileSaver from 'file-saver';
import {NzUploadFile} from "ng-zorro-antd/upload";
import { ImportFormService } from '@hcm-mfe/shared/data-access/services';
import { SUCCESS } from '@hcm-mfe/model-organization/data-access/models';

@Component({
  selector: 'app-action-document-attached-fle',
  templateUrl: './action-document-attached-file.component.html',
  styleUrls: ['./action-document-attached-file.component.scss'],
  //providers: [TargetAssessmentService]
})
export class ActionDocumentAttachedFileComponent {
  mode = Mode.ADD;
  @Input() url: string | undefined;
  @Input() endpoint: string = '';
  @Input() microService: string | undefined;
  @Input() fileName: string | undefined;
  @Input() serviceName: string | undefined;
  @Input() param: any = null;
  @Input() module: string = '';
  @Input() urlDownload: string | undefined;
  @Input() requestBlod: any;
  @Input() authority: any;
  @Input() isHideDownload = false;
  error: string | undefined;
  isSubmitted = false;
  fileList: NzUploadFile[] = [];
  urlErr: string = '';
  private readonly subs: Subscription[] = [];
  employeeId = 1;
  isLoadingFile = false;
  modules = ['SALARY_EVOLUTION','MODEL_PLAN'];

  constructor(
    private readonly fb: FormBuilder,
    private readonly toastrService: ToastrService,
    private readonly translate: TranslateService,
    private readonly modalRef: NzModalRef,
    private readonly message: NzMessageService,
    private readonly targetAssessmentService: TargetAssessmentService,
    private readonly importFormService: ImportFormService
  ) {

  }

  dataResponseUploadFile: any = [];
  fileError: any;
  save() {
    if (!this.fileList.length) {
      this.toastrService.error(this.translate.instant('common.notification.emptyFile'));
      return;
    }
    this.isLoadingFile = true;
    this.isSubmitted = true;
    const formData = new FormData();

    this.fileList.forEach((nzFile: NzUploadFile) => {
      formData.append('file', nzFile as any);
      if(this.requestBlod) {
        formData.append("request", new Blob([JSON.stringify(this.requestBlod)], {
          type: 'application/json',
        }));
      }
    });
    let uplioadFileExel;
    if(this.modules.indexOf(this.module) >= 0){
      uplioadFileExel = this.importTemp(formData);
    } else{
      uplioadFileExel = this.targetAssessmentService.getUrlUploadFile(formData, this.url, this.microService, this.endpoint, this.serviceName, this.authority).subscribe(res => {
        if (res && res.status === 200) {
          if(res.data?.errorMessages && res.data.warningMessages){

            this.dataResponseUploadFile = res.data;
            this.isLoadingFile = false;
            if(this.dataResponseUploadFile.errorMessages.length > 0 ){
              this.toastrService.error(this.translate.instant('common.notification.uploadFileError'))
            }else if(this.dataResponseUploadFile.warningMessages.length > 0){
              this.toastrService.success(this.translate.instant('common.notification.updateSuccess'));
              localStorage.setItem("waringUploadFile", "true");
            }
            else {
              this.toastrService.success(this.translate.instant('common.notification.updateSuccess'));
              this.modalRef.destroy({ refresh: true });
            }

          }else {
            if (this.microService === "POL") {
              this.dataResponseUploadFile = res.data.errors;
              this.fileError = res.data.fileExcelBase64;
            } else {
              this.dataResponseUploadFile = res.data;
            }
            if(!this.dataResponseUploadFile[0]){
              this.toastrService.success(this.translate.instant('common.notification.updateSuccess'));
              this.modalRef.destroy({ refresh: true });
              this.isLoadingFile = false;
            } else {
              this.isLoadingFile = false;
              this.toastrService.error(this.translate.instant('common.notification.uploadFileError'))
            }
          }
        }
      }, error => {
        this.isLoadingFile = false;
        this.toastrService.error(this.translate.instant('common.notification.updateError'));
      });
    }

    this.subs.push(uplioadFileExel);
  }

  dowloadFileSample(){
    this.isLoadingFile = true;
    let dowLoadFileSample;
    if(this.modules.indexOf(this.module) >= 0){
      dowLoadFileSample  = this.downloadTemp();
    } else {
       dowLoadFileSample = this.targetAssessmentService.getDowloadFileExel(this.url, this.endpoint, this.microService, this.serviceName, this.param, this.authority).subscribe(data => {
        this.toastrService.success(this.translate.instant('common.notification.dowloadFile'));
        const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
        FileSaver.saveAs(blob, `${this.fileName}.xlsx`);
        this.isLoadingFile = false;
      }, error => {
        this.isLoadingFile = false;
        this.toastrService.error(this.translate.instant('common.notification.dowloadFileError'));
      });
    }

    this.subs.push(dowLoadFileSample);
  }

  beforeUpload = (file: NzUploadFile): boolean | Observable<boolean> => {
    if (file.type !== 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' && file.type !== 'application/vnd.ms-excel') {
      this.toastrService.error(this.translate.instant('common.notification.invalidFileType'));
      return false;
    }

    if (file.size && file.size >= 3000000) {
      this.toastrService.error(this.translate.instant('common.notification.MaximumfileUpload'));
      return false;
    }

    this.fileList = this.fileList.concat(file);
    this.fileList = [this.fileList[this.fileList.length -1]];
    this.dataResponseUploadFile = [];
    return false;
  };

  importTemp(formData: FormData){
    let dowLoadFileSample  = this.importFormService.importFile(this.url,formData, this.microService).subscribe(res => {
      if (res && res.status === 200) {
        this.dataResponseUploadFile = res.data.errors;
        if(this.dataResponseUploadFile.length === 0){
          this.toastrService.success(this.translate.instant('common.notification.updateSuccess'));
          this.modalRef.destroy({ refresh: true });
          this.isLoadingFile = false;
        } else {
          this.fileError = res.data.fileExcelBase64;
          this.isLoadingFile = false;
          this.toastrService.error(this.translate.instant('common.notification.uploadFileError'))
        }
      }
    }, error => {
      this.isLoadingFile = false;
      this.toastrService.error(this.translate.instant('common.notification.updateError'));
    });
    return dowLoadFileSample;
  }
  downloadTemp(){
   let dowLoadFileSample = this.importFormService.exportFile(this.urlDownload, this.param, this.microService).subscribe(data => {
      this.toastrService.success(this.translate.instant('common.notification.dowloadFile'));
      const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      FileSaver.saveAs(blob, `${this.fileName}.xlsx`);
      this.isLoadingFile = false;
    }, error => {
      this.isLoadingFile = false;
      this.toastrService.error(this.translate.instant('common.notification.dowloadFileError'));
    });
   return dowLoadFileSample;
  }
  downloadErrorFile() {
    var url = `data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,${this.fileError}`;
    fetch(url)
      .then(res => res.blob())
      .then((data)=> {
        FileSaver.saveAs(data, `${this.fileName}.xlsx`);
      })
  }
  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subs.forEach((s) => s.unsubscribe());
  }

}
