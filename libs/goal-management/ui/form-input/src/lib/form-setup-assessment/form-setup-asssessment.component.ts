import {Component, Injector, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NzModalRef } from 'ng-zorro-antd/modal';
import { Subscription } from 'rxjs';
import * as moment from 'moment';
import {TargetAssessmentService} from "@hcm-mfe/goal-management/data-access/services";
import {
  ICycleAssessment,
  ISetupAssessmentData,
  mbDataSelectsAssessmentIsLoopAble
} from "@hcm-mfe/goal-management/data-access/models";
import { mbDataSelectsAssessmentStatus
} from '@hcm-mfe/system/data-access/models';
import {HTTP_STATUS_CODE, Mode} from "@hcm-mfe/shared/common/constants";
import {CustomValidator, DateValidator} from "@hcm-mfe/shared/common/validators";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";


@Component({
  selector: 'form-setup-asssessment',
  templateUrl: './form-setup-asssessment.component.html',
})

export class FormSetupAssessmentComponent extends BaseComponent implements OnInit {
  data: ISetupAssessmentData | undefined;
  mode = Mode.ADD;
  isSubmitted = false;
  formAssessment: FormGroup;
  override isLoading = false;
  private readonly  subs: Subscription[] = [];
  private readonly formatDateType = "YYYY-MM-DD";
  private readonly errMessage = 'targerManager.common.notification.saveError';

  mbDataSelectsAssessmentStatus = mbDataSelectsAssessmentStatus();
  mbDataSelectsAssessmentIsLoopAble = mbDataSelectsAssessmentIsLoopAble();

  mbDataSelectsAssessmentCycle: ICycleAssessment[] = [];

  constructor(
    private readonly modalRef: NzModalRef,
    private readonly targetAssessmentService: TargetAssessmentService,
    injector : Injector,
  ) {
    super(injector);
    this.formAssessment = this.fb.group({
      pcfCode: [null, [Validators.required, Validators.maxLength(50)]],
      pcfName: [null, [Validators.required, Validators.maxLength(200)]],
      cycleType: [null, [Validators.required]],
      initialNum: [null, [Validators.required, Validators.min(1)]],
      fromDate: [null, [Validators.required]],
      toDate: [null],
      editableNum: [null, [Validators.required, Validators.min(1)]],
      finishNum: [null, [Validators.required, Validators.min(1)]],
      isLoopAble: ["0", [Validators.required]],
      flagStatus: [1, [Validators.required]]
    }, {
      validators: [
        DateValidator.validateSpecificRangeDate('fromDate', 'toDate', 'rangeDateError1'),
        DateValidator.validateSpecificValidDatetoCreateDate('initialNum', 'fromDate', 'validRangeToCurrentDate1'),
        CustomValidator.noWhitespaceValidator('pcfCode', 'noAllowSpace'),
        CustomValidator.noWhitespaceValidator('pcfName', 'noAllowSpacePcName'),
      ]
    })
  }

  get f() { return this.formAssessment.controls; }

  ngOnInit(): void {
    this.isLoading = true;
    const subInit = this.targetAssessmentService.getListAssessmentCycle().subscribe(
      (result) => {
        if (result.data) {
          this.mbDataSelectsAssessmentCycle = result.data || [];
        }

        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
    if (this.mode === Mode.EDIT) {
      this.getDetailTargetAssesment();
    }
    this.subs.push(subInit);

  }

  getDetailTargetAssesment(){
      this.isLoading = true;
      const detailTargetAssesmentSub =  this.targetAssessmentService.getListSetupAssessmentData(this.data?.pcfId).subscribe(
        ((res: BaseResponse) => {
          if (res.status === HTTP_STATUS_CODE.OK) {
            this.data = res.data || [];
            this.formAssessment.patchValue(res.data);
            this.isLoading = false;
          }
        }), (err) => {
          this.toastrCustom.error(`${this.translate.instant('common.notification.error')} : ${err.message}`);
          this.isLoading = false;
          this.modalRef.close();
        }
      )
      this.subs.push(detailTargetAssesmentSub);
  }

  patchValueInfo() {
    this.data && this.formAssessment.patchValue(this.data);
  }

  save() {
    this.isSubmitted = true;
    if (this.formAssessment.valid) {
      if (this.mode === Mode.ADD) {
        const request: ISetupAssessmentData = this.formAssessment.value;
        request.fromDate = moment(request.fromDate).format(this.formatDateType);
        request.toDate = request.toDate ? moment(request.toDate).format(this.formatDateType) : undefined;
        const createSetupAssessment = this.targetAssessmentService.createSetupAssessmentData(request).subscribe(
          ((res: BaseResponse) => {
            if (res.status === HTTP_STATUS_CODE.OK) {
              this.isLoading = false;
              this.modalRef.close({refresh:true});
              this.toastrCustom.success(this.translate.instant('targerManager.common.notification.saveSuccess'));
            }
          }), (err) => {
            this.isLoading = false;
            this.toastrCustom.error(`${this.translate.instant(this.errMessage) } : ${err.message}`);
          }
        )

        this.subs.push(createSetupAssessment);
      } else if (this.mode === Mode.EDIT) {
        const request: ISetupAssessmentData = {
          ...this.formAssessment.value,
          fromDate: moment(this.formAssessment.value.fromDate).format(this.formatDateType),
          toDate: this.formAssessment.value.toDate ? moment(this.formAssessment.value.toDate).format(this.formatDateType) : undefined,
        };

        const setupAssessmentData = this.targetAssessmentService.editSetupAssessmentData(request, this.data?.pcfId).subscribe(
          ((res: BaseResponse) => {
            if (res.status === HTTP_STATUS_CODE.OK) {
              this.isLoading = false;
              this.modalRef.close({refresh:true});
              this.toastrCustom.success(this.translate.instant('targerManager.common.notification.saveSuccess'));
            }else{
              this.isLoading = false;
              this.toastrCustom.warning(`${this.translate.instant(this.errMessage)} : ${res?.message}`);
            }
          }), (err) => {
            this.isLoading = false;
            this.toastrCustom.error(`${this.translate.instant(this.errMessage)} : ${err.message}`);
          })
        this.subs.push(setupAssessmentData);
      }
    }
  }

  override ngOnDestroy() {
    this.subs.forEach((s) => s.unsubscribe());
  }
}
