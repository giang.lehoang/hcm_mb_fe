import {Component, EventEmitter, Injector, Input, OnInit, Output, TemplateRef, ViewEncapsulation} from '@angular/core';
import {
  IClassificationBlocksAndLines, isColumnTableStatus,
  orgResults
} from "../../../../../data-access/models/src/lib/IClassificationBlocksAndLines";
import {CdkDragDrop, moveItemInArray, transferArrayItem} from "@angular/cdk/drag-drop";
import {cloneDeep} from "lodash";
import {AllocationLineBranchService} from "@hcm-mfe/goal-management/data-access/services";

@Component({
  selector: '[hcm-mfe-board-dnd-list]',
  templateUrl: './board-dnd-list.component.html',
  styleUrls: ['./board-dnd-list.component.scss'],
  encapsulation: ViewEncapsulation.None,

})
export class BoardDndListComponent implements OnInit {
  @Input() lane: IClassificationBlocksAndLines<orgResults> | undefined;
  @Input() lanes: IClassificationBlocksAndLines<orgResults>[] | undefined;
  @Output() isShowModalEvent = new EventEmitter<boolean>();
  @Output() isChangeDropListLands = new EventEmitter<IClassificationBlocksAndLines<orgResults>[]>();
  @Input() policy: boolean | undefined;
  @Input() flagSwitchType: boolean = false;

  inputSearch: string | undefined;
  matchedIndexInitial: number | undefined;
  listDataInitialOrgResults: orgResults[] | undefined;
  isColumnTableStatus: string;

  constructor(
    private allocationLineBranchService: AllocationLineBranchService
  ) {
    this.isColumnTableStatus =  isColumnTableStatus[isColumnTableStatus.N];
  }

  ngOnInit(): void {
    const cloneListDataLands = cloneDeep(this.lanes);
    this.matchedIndexInitial = this.lanes?.findIndex(el => el.isColumnTable === isColumnTableStatus.N);
    if(this.matchedIndexInitial !== undefined && this.matchedIndexInitial >= 0){
      this.listDataInitialOrgResults = cloneListDataLands && cloneListDataLands[this.matchedIndexInitial]?.orgResults;
    }
  }

  drop(event: CdkDragDrop<orgResults[]>) {
    const isMovingInsideTheSameList = event.previousContainer === event.container;
    if (isMovingInsideTheSameList) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }

    if(event.previousContainer.id === this.lanes![0]!.dataCode){
      //this.listDataInitialOrgResults?.splice(event.previousIndex, 1);
      // @ts-ignore
      const matchedIndex = this.listDataInitialOrgResults?.findIndex(el => el?.customId === event.item.data.customId);
      const tmp = cloneDeep(this.listDataInitialOrgResults);
      tmp?.splice(matchedIndex!, 1);
      this.allocationLineBranchService.setListDataInitialOrgResults(tmp);
    }
    this.isChangeDropListLands.emit(this.lanes);
  }

  searchItemInBlock($event: string, listData: IClassificationBlocksAndLines<orgResults>[], land: orgResults[]) {

    const storeState = this.allocationLineBranchService.getListDataInitialOrgResults();
    if (this.matchedIndexInitial !== undefined && this.matchedIndexInitial >= 0) {
      this.inputSearch = $event;
      if (typeof($event) === 'string' && $event.trim() === '') {
        // @ts-ignore
        listData[this.matchedIndexInitial].orgResults = storeState ? storeState : this.listDataInitialOrgResults;
        return 0;
      }else if(listData[this.matchedIndexInitial].orgResults?.length <= 0) {
        listData[this.matchedIndexInitial].orgResults = [];
      }

      const cloneOrgResultsInitial = storeState ? storeState.filter(el =>
        el.dataName.toUpperCase().includes($event.trim().toUpperCase())
      ) : this.listDataInitialOrgResults?.filter(el =>
        el.dataName.toUpperCase().includes($event.trim().toUpperCase())
      );

      if(cloneOrgResultsInitial){
        listData[this.matchedIndexInitial].orgResults = cloneOrgResultsInitial;
      }

    }
    return 2;
  }

  showModalUpdateBlockAndLine($event?: MouseEvent ){
    this.isShowModalEvent.emit(true);
  }

}
