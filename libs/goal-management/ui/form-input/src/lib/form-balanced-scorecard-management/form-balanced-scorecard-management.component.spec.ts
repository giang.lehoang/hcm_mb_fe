import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBalancedScorecardManagementComponent } from './form-balanced-scorecard-management.component';

describe('FormBalancedScorecardManagementComponent', () => {
  let component: FormBalancedScorecardManagementComponent;
  let fixture: ComponentFixture<FormBalancedScorecardManagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormBalancedScorecardManagementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBalancedScorecardManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
