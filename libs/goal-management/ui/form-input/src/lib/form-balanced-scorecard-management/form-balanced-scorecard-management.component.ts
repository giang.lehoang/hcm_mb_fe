import { Component, Injector, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { NzModalRef } from 'ng-zorro-antd/modal';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {Mode} from "@hcm-mfe/shared/common/constants";
import {mbDataSelectsAssessmentStatus} from "@hcm-mfe/system/data-access/models";
import {ListBallanceScoreCardManagement} from "@hcm-mfe/goal-management/data-access/models";
import {BallancedScoreCardService} from "@hcm-mfe/goal-management/data-access/services";
import {CustomValidators} from "@hcm-mfe/shared/common/validators";

@Component({
  selector: 'app-form-balanced-scorecard-management',
  templateUrl: './form-balanced-scorecard-management.component.html',
  styleUrls: ['./form-balanced-scorecard-management.component.scss'],
})
export class FormBalancedScorecardManagementComponent extends BaseComponent implements OnInit {
  mode = Mode.ADD;
  isSubmitted = false;
  listStatus = mbDataSelectsAssessmentStatus();
  data: ListBallanceScoreCardManagement | undefined;
  ballanceScoreCardServiceForm: FormGroup;
  private readonly subs: Subscription[] = [];
  constructor(
    private readonly ballancedScoreCardService: BallancedScoreCardService,
    injector: Injector,
    readonly modalRef: NzModalRef
  ) {
    super(injector);
    this.ballanceScoreCardServiceForm = new FormGroup({
      lvaValue: new FormControl('', [Validators.required, Validators.maxLength(50)]),
      lvaMean: new FormControl('', [Validators.required, Validators.maxLength(200)]),
      description: new FormControl('', [Validators.maxLength(250)]),
      flagStatus: new FormControl(1, [Validators.required]),
      displaySeq: new FormControl('', [Validators.required, Validators.min(0), CustomValidators.onlyNumber]),
    });
  }

  removeSpace(event: any) {
    event.target.value = event.target.value.replace(/\s+/g, '');
  }

  ngOnInit(): void {
    if (this.mode === Mode.EDIT) {
      const getDetailItem = this.ballancedScoreCardService
        .getListBallancedGoalDetail(this.data?.lvaId)
        .subscribe((data) => {
          this.ballanceScoreCardServiceForm.controls['lvaValue'].setValue(data.data.lvaValue);
          this.ballanceScoreCardServiceForm.controls['lvaMean'].setValue(data.data.lvaMean);
          this.ballanceScoreCardServiceForm.controls['description'].setValue(data.data.description);
          this.ballanceScoreCardServiceForm.controls['flagStatus'].setValue(data.data.flagStatus);
          this.ballanceScoreCardServiceForm.controls['displaySeq'].setValue(data.data?.displaySeq.toString());
        });
      this.subs.push(getDetailItem);
    }
  }

  saveBalanceScoreCard() {
    let checkValid = false;
    if (!this.ballanceScoreCardServiceForm.valid) {
      checkValid = true;
    }
    if (checkValid) {
      this.isSubmitted = true;
      Object.values(this.ballanceScoreCardServiceForm.controls).forEach((control) => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    } else {
      if (this.mode === Mode.ADD) {
        this.ballancedScoreCardService.createBallancedGoal(this.ballanceScoreCardServiceForm.value).subscribe(
          (data) => {
            this.toastrCustom.success('Tạo mới thành công');
            this.modalRef.close({ refresh: true });
          },
          (error) => {
            this.toastrCustom.error(error.error.message);
          }
        );
      } else {
        this.ballanceScoreCardServiceForm.value.lvaId = this.data?.lvaId;
        const cloneValueFormSubmit = {
          ...this.ballanceScoreCardServiceForm.value,
          flagStatus: Number(this.ballanceScoreCardServiceForm.value.flagStatus),
          displaySeq: Number(this.ballanceScoreCardServiceForm.value.displaySeq),
        };
        this.ballancedScoreCardService.updateBallancedGoal(cloneValueFormSubmit).subscribe(
          (data) => {
            this.toastrCustom.success('Cập nhật thành công');
            this.modalRef.close({ refresh: true });
          },
          (error) => {
            this.toastrCustom.error(error.error.message);
          }
        );
      }
    }
  }
}
