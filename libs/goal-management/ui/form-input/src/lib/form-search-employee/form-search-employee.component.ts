import {
  Component,
  EventEmitter,
  Injector,
  Input,
  OnInit,
  Output,
  SimpleChanges,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { forkJoin, Subscription } from 'rxjs';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {
  EmployeeTargetService,
  EvaluetionApprovalSetOfIndicatorService,
  TargetAssessmentService, UrlConstant
} from "@hcm-mfe/goal-management/data-access/services";
import {
  IAsssessmentPeriod,
  IEmployData,
  Ijob, IListOrg,
  ISetOfIndicatorsData,
  PositionUnassign,
  ResponseEntity, typePropose
} from "@hcm-mfe/goal-management/data-access/models";
import * as moment from 'moment';
import {FormModalShowTreeUnitComponent} from "@hcm-mfe/shared/ui/form-modal-show-tree-unit";
import {MbDropDowListLazySearchComponent} from "@hcm-mfe/shared/ui/mb-drop-dow-list-lazy-search";
import {typeAllocation} from "../../../../../data-access/models/src/lib/IApproveEmpAllocated";
import {ListPersonnelAllocationService} from "../../../../../data-access/services/src/lib/list-personnel-allocation.service";
import {ListEmpAllocatedService} from "../../../../../data-access/services/src/lib/list-emp-allocated";


@Component({
  selector: 'form-search-employee-component',
  templateUrl: './form-search-employee.component.html',
  styleUrls: ['./form-search-employee.component.scss']
})
export class FormSearchEmployeeComponent extends BaseComponent implements OnInit {

  constructor(
    injector : Injector,
    private readonly targetAssessmentService: TargetAssessmentService,
    private readonly employeeTargetService: EmployeeTargetService,
    private readonly evaluetionApprovalSetOfIndicatorService: EvaluetionApprovalSetOfIndicatorService,
    private listPersonnelService: ListPersonnelAllocationService,
    private readonly listEmpAllocatedService: ListEmpAllocatedService
  ) {
    super(injector);

  }

  ngOnChanges(changes: SimpleChanges){
    this.formSearchEmployee = this.fb.group({
      orgId: [''],
      jobId: [''],
      posId: [''],
      fullName: [''],
      // if periodYear has month start is 1-periodYear (example: 1/2022)
      periodYear: [String(moment().month() === 0 && this.isJanuary ? moment().year() - 1 : new Date().getFullYear())],
      empCode: [''],
      periodId: [''],
      flagStatus: [''],
      multipleStatus: [],
      apportion: [''],
      claCode: [],
      isPropose: [''],
      employeeId: [''],
      dataId: ['']
    });

    // if periodYear has month start is 1-periodYear (example: 1/2022)
    this.paramSearchEmployPagination.periodYear = moment().month() === 0 && this.isJanuary ? moment().year() - 1 : new Date().getFullYear();
  }

  @Output() getListSearchDataEmployee = new EventEmitter<IEmployData>();
  @Output() onLoading = new EventEmitter<boolean>();
  @Output() isExportExcel = new EventEmitter<boolean>();
  @Input() IsAssessmentTime = false;
  @Input() isJob = true;
  @Input() isOrgId = true;
  @Input() isStatus = false;
  @Input() isYear = true;
  @Input() isExport = false;
  @Input() listStatus: any;
  @Input() mbDisableListStatus = false;
  @Input() functionCode = '';
  @Input() isEmpCodeAndEmpName = false;
  @Input() empCode = true;
  @Input() fullName = true;
  @Input() isPosition = true;
  @Input() isMultipleStatus = false;
  @Input() isIOAllocation = false;
  @Input() isPropose = false;
  @Input() isClassification = false;
  @Input() isDataId = false;
  @Input() isStatusEmployee = false;
  @Input() isAllPeriod = false;
  @Input() isStatusEmployeeText= '';
  @Input() isJanuary = false;

  formSearchEmployee: FormGroup;
  resultOrgId: number | null = null;
  resultJobId: number | null = null;
  value = '';
  listOrg: IListOrg[] = [];
  modalRef: NzModalRef | undefined;
  listDataAssessmentPeriod: IAsssessmentPeriod[] = [];
  private readonly subs: Subscription[] = [];
  mbDataSelectsCodeEmploy: IEmployData[] = [];
  urlPositionService = UrlConstant.END_POINT_GOAL_MANAGEMENT.urlEndPointPositionSearch;
  urlGetListJobTypeORG = `${UrlConstant.HCM_ENDPOINT.urlEndPointProjectListJob}`;
  listAllocation = typeAllocation();
  listPropose = typePropose();
  listClassification = [];
  @ViewChild('formPositionComp') formPositionComp: MbDropDowListLazySearchComponent | undefined;

  paramSearchEmployPagination: IEmployData = {
    orgId: undefined,
    jobId: undefined,
    posId: undefined,
    fullName: undefined,
    // jan=0, dec=11
    periodYear: moment().month() === 0 && this.isJanuary ? moment().year() - 1 : new Date().getFullYear(),
    empCode: undefined,
    periodId: undefined, // add assessment type
    flagStatus: undefined,
    periodActive: undefined
  };

  paramSetupPosition: Pick<ISetOfIndicatorsData, "jobId" | "orgId"> | { jobId: string, orgId: string } = {
    jobId: this.resultJobId,
    orgId: this.resultOrgId,
  }

  paramSetUpJob: Pick<Ijob, 'jobType'> = {
    jobType: 'ORG',
  }

  clearDataOrgId() {
    this.formSearchEmployee.patchValue({
      orgId: null
    });
    this.resultOrgId = null;
  }

  selectDataJobId(item: Ijob) {
    if(item && item.jobId){
      this.formSearchEmployee.patchValue({
        jobId: item.jobId
      });
    }else this.formSearchEmployee.patchValue({
      jobId: ''
    });
  }

  selectPositionId(item: PositionUnassign){
    if(item && item.posId){
      this.formSearchEmployee.patchValue({
        posId: item.posId
      });
    }else this.formSearchEmployee.patchValue({
      posId: ''
    });
  }

  getPositionSettupAssessment() {
    const request: Pick<ISetOfIndicatorsData, "jobId" | "orgId"> = this.formSearchEmployee.value;
    this.paramSetupPosition.jobId = request.jobId ? Number(request.jobId) : '';
    this.paramSetupPosition.orgId = request.orgId ? this.resultOrgId : '';
    this.formPositionComp?.onRetrivedData(this.paramSetupPosition);
  }

  private getNzWidth() {
    if(window.innerWidth > 767){
      return window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5;
    }
    return window.innerWidth;
  }
  exportExcel() {
    this.isExportExcel.emit(true);
  }
  showModalUnit($event: any, value: string, footerTmpl?: TemplateRef<any>, data?: any) {
    this.value = ''
    this.modalRef = this.modal.create({
      nzWidth: '80vw',
      nzTitle: this.translate.instant('targerManager.setOfIndicators.unit'),
      nzContent: FormModalShowTreeUnitComponent,
    });
    this.modalRef.afterClose.subscribe(result => {
      if (result?.orgName) {
        this.resultOrgId = this.modalRef?.getContentComponent().objectSearch?.orgId;
        this.formSearchEmployee.patchValue({
          orgId: this.modalRef?.getContentComponent().orgName
        });
      }
      this.getPositionSettupAssessment()
    });
  }
  onChange(event: any) {
    this.formSearchEmployee.get('empCode')?.setValue(event?.employeeCode);
    this.formSearchEmployee.get('employeeId')?.setValue(event?.employeeId);
  }
  enterSearch($event: KeyboardEvent) {
    if ($event.key === 'Enter' || $event.type === 'click') {
      this.search();
    }
  }
  search($event?: Event) {
    // emitdata
    const request = this.formSearchEmployee.value;
    if(request.periodYear !== null){
      request.periodYear = new Date(String(request.periodYear)).getFullYear();
    }else {
      request.periodYear = null;
    }
    if (this.formSearchEmployee.value.orgId && this.formSearchEmployee.value.orgId !== '') {
      request.orgId = Number(this.resultOrgId);
    } else {
      request.orgId = null;
    }

    this.paramSearchEmployPagination = {
      ...this.paramSearchEmployPagination,
      ...request,
    };
    if (!this.IsAssessmentTime) {
      const cloneParamSearch = { ...this.paramSearchEmployPagination };
      delete cloneParamSearch.periodId;
    } else {
      this.listDataAssessmentPeriod.forEach(el => {
        if (el.periodId === this.formSearchEmployee.controls['periodId'].value) {
          this.paramSearchEmployPagination.periodName = el.periodName;
          this.paramSearchEmployPagination.asmCloseDate = el.asmCloseDate;
          this.paramSearchEmployPagination.finishDate = el.finishDate;
        }
      })
    }
    if (!this.isStatus) {
      const cloneParamSearch = { ...this.paramSearchEmployPagination };
      delete cloneParamSearch.flagStatus;
      this.getListSearchDataEmployee.emit(cloneParamSearch);
    } else {
      this.getListSearchDataEmployee.emit(this.paramSearchEmployPagination);
    }
  }

  ngOnInit(): void {
    const request: ISetOfIndicatorsData = this.formSearchEmployee.value;
    this.paramSetupPosition.jobId = request.jobId ? Number(request.jobId) : '';
    this.paramSetupPosition.orgId = request.orgId ? Number(request.orgId) : '';
    this.isLoading = true;
    this.onLoading.emit(true);
    this.formSearchEmployee.controls['multipleStatus'].setValue([10,11,12,13,15,16,17]);
    if (this.isDataId) {
      this.getListOrg();
    }
    console.log('this.paramSearchEmployPagination.periodYear', this.paramSearchEmployPagination.periodYear);
    const formSetofIndicator = forkJoin(
      [
        this.employeeTargetService.getListEmployeeOfManager(),
        !this.isPropose ? this.evaluetionApprovalSetOfIndicatorService.getAssessmentPeriod(
          { year: !this.isAllPeriod ? this.paramSearchEmployPagination.periodYear : '' }
        ) : this.listEmpAllocatedService.getPeriod()
      ]).subscribe(
        ([
          dataEmployeesOfManager,
          dataAssessmentPeriod
        ]: [
            ResponseEntity<IEmployData>,
          ResponseEntity<IAsssessmentPeriod>
          ],

        ) => {
          this.isLoading = false;
          this.mbDataSelectsCodeEmploy = dataEmployeesOfManager.data || [];
          this.listDataAssessmentPeriod = dataAssessmentPeriod.data || [];
            if (this.IsAssessmentTime && this.listDataAssessmentPeriod.length > 0) {
                this.formSearchEmployee.controls['periodId'].setValue(this.listDataAssessmentPeriod[0].periodId);
                const periodItem = this.listDataAssessmentPeriod.find(el => el.periodId === Number(this.listDataAssessmentPeriod[0].periodId));
                this.paramSearchEmployPagination.periodActive = Boolean(periodItem?.flagStatus);
                this.onLoading.emit(true);
                if (this.isClassification) {
                  this.getListClassification();

                }
            }

          this.onLoading.emit(true);
          if (this.IsAssessmentTime) {
            this.search()
          }
        },
        (error) => {
          this.isLoading = false;
          this.onLoading.emit(false);
          this.toastrCustom.error(this.translate.instant(error ? error.message : this.translate.instant('common.notification.error')));
        },
        () => {
          this.isLoading = false;
          this.onLoading.emit(false);
        }
      );
    this.subs.push(formSetofIndicator);

  }

  getListPeriod() {
    this.evaluetionApprovalSetOfIndicatorService.getAssessmentPeriod().subscribe(el => {
      this.isLoading = false;
      this.listDataAssessmentPeriod = el.data;
      this.listDataAssessmentPeriod.forEach(el => {
        if (el.periodId === this.formSearchEmployee.controls['periodId'].value) {
          this.paramSearchEmployPagination.periodName = el.periodName;
          this.paramSearchEmployPagination.asmCloseDate = el.asmCloseDate;
          this.paramSearchEmployPagination.finishDate = el.finishDate;
          this.getListSearchDataEmployee.emit(this.paramSearchEmployPagination);
        }
      })
    }, err => {
      this.isLoading = false;
      this.toastrCustom.error(this.translate.instant(err ? err.message : this.translate.instant('common.notification.error')));
    })
  }
  getListOrg() {
    this.listPersonnelService.getListOrg().subscribe(res => {
      this.listOrg = res.data;
      this.formSearchEmployee.controls['dataId'].setValue(this.listOrg[0].dataId);
      this.isLoading = false;
    }, (err) => {
      this.isLoading = false;
      if(err.code === 500) {
        this.toastrCustom.error(this.translate.instant("common.notification.error"));
      } else {
        this.toastrCustom.error(err.message);
      }
    })
  }
  getListClassification() {
    const params = {
      claType: 'CN',
      periodId: this.formSearchEmployee.value.periodId
    };
    this.listPersonnelService.getListClassification(params).subscribe(res => {
      this.listClassification = res.data;
      this.isLoading = false;
    }, (err) => {
      this.isLoading = false;
      if(err.code === 500) {
        this.toastrCustom.error(this.translate.instant("common.notification.error"));
      } else {
        this.toastrCustom.error(err.message);
      }
    })
  }
  onChangeSelectPeriod(event: any): void {
    if (event) {
      this.formSearchEmployee.controls['periodId'].setValue(event);
      const periodItem = this.listDataAssessmentPeriod.find(el => el.periodId === Number(event));
      this.paramSearchEmployPagination.periodActive = Boolean(periodItem?.flagStatus);
      this.search();

    }
  }

  onChangeSelectYeah(event: any): void {
    if (event && this.IsAssessmentTime) {
      this.paramSearchEmployPagination.periodYear = event ? +moment(event).format("YYYY") : null;
      this.onLoading.emit(true);
      const params = { year: this.paramSearchEmployPagination.periodYear };
      this.evaluetionApprovalSetOfIndicatorService.getAssessmentPeriod(params).subscribe((res: ResponseEntity<IAsssessmentPeriod>) => {
        this.onLoading.emit(false);
        if (res && res.data && res.data.length > 0) {
          this.listDataAssessmentPeriod = res.data;
          this.formSearchEmployee.controls['periodId'].setValue(this.listDataAssessmentPeriod[0].periodId);
        } else {
          this.listDataAssessmentPeriod = [];
          this.formSearchEmployee.controls['periodId'].setValue(null);
        }
        this.search();
      });
    }
  }

  override ngOnDestroy() {
    this.subs.forEach((s) => s.unsubscribe());
  }

}
