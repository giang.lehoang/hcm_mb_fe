export interface TreeNode {
  code?: string;
  displayName?: string;
  id?: string;
  indexNumber?: number;
  isMenu?: string;
  name?: string;
  type?: string;
  uri?: string;
  children?: Array<TreeNode>;
  level?: number;
  parent?: TreeNode;
  description?: string;
  dataId: number;
  dataType?: string;
  key: string;
  permissionType?: string;
  parentId?: string;
  checked?: boolean;
  parentChecked?: boolean;
  originId?: string;
  expand?: boolean,
  expandCustom?: boolean,
  isChecked?:boolean
  dataName?: string,
  dataCode?: string,
}

export interface App {
  appCode?: string;
  appId?: string;
  appName?: string;
  description?: string;
  originId?: string;
}

export interface Config {
  cfgId?: string;
  cfgKey?: string;
  cfgValue?: string;
  description?: string;
}

export interface Permission {
  decisionStrategy?: string;
  description?: string;
  id?: string;
  logic?: string;
  name?: string;
  policies?: Array<string>;
  resources?: Array<string>;
  roleName?: string;
  scopes?: string;
  type?: string;
  children?: Array<Permission>;
  level?: number;
  indexNumber?: number;
  code?: string;
  appId?: string;
  parentId?: string;
  path?: string;
  index?: number;
  permissionType?: string;
  resourceId?: string;
  roles?: Array<string>;
}

export interface Policy {
  config?: Array<PolicyChild>;
  decisionStrategy?: string;
  id?: string;
  logic?: string;
  name?: string;
  type?: string;
}

export interface PolicyChild {
  roles?: string;
  code?: string;
}

export interface Resource {
  code?: string;
  displayName?: string;
  id?: string;
  indexNumber?: number;
  isMenu?: boolean | string;
  isInternet?: boolean | string;
  name?: string;
  type?: string;
  uri?: string;
  parentId?: string;
  uriOfResource?: string;
  children?: Array<Resource>;
  uriOfResources?: Array<string>;
  parent?: Resource;
  level?: number;
  scopes?: Array<string>;
}

export interface ResourceType {
  code: string;
  name: string;
}

export interface Role {
  description?: string;
  id?: string;
  links?: Array<string>;
  name?: string;
  attributes?: {
    description?: string
  }
  clientRole?: string;
  composite?: string;
  containerId?: string;
}

export interface Position {
  rpmId: number;
  posId?: number;
  posCode?: string;
  posName?: string;
  orgName?: string;
  jobName?: string;
  isCheck?: number | boolean;
}

export interface RolePosition {
  roleId: string;
  listPos: Array<Pick<Position, 'posId' | 'isCheck'>>
}



export interface Job {
  jobId: number,
  jobName: string,
}

export interface Scope {
  code?: string;
  description?: string;
  id?: string;
  links?: Array<string>;
  name?: string;
}

export interface User {
  userId: string;
  username: string;
  fullName: string;
  empId: number;
  empCode: string;
  dateOfBirth: string;
  genderName: string;
  phoneNumber: string;
  email: string;
  address: string;
  orgName: string;
  jobName: string;
  posName: string;
  isLocked: string;
  reasonLock: string;
  flagStatus: number | boolean;
  isTracing: string | boolean;
  isUserInRole: string;
}

export interface SessionHistory {
  utcId: string,
  username: string,
  loginTime: string,
  fromDate: string,
  deviceName: string,
  appName: string,
  featureName: string,
}
