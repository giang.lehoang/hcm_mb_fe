export class SelectCheckAbleModal {
  action?: 'SUBMIT' | 'CANCEL' | 'NG_MODEL_CHANGE';
  isCheckAll?= false;
  listOfSelected?: any[] = [];
  itemChecked?: any;
  listItemSelected?: any[] = [];

  constructor(action?: 'SUBMIT' | 'CANCEL' | 'NG_MODEL_CHANGE', isCheckAll: boolean = false, listOfSelected: any[] = [], itemChecked?: any) {
    this.action = action;
    this.isCheckAll = isCheckAll;
    this.listOfSelected = listOfSelected;
    this.itemChecked = itemChecked;
  }
}
