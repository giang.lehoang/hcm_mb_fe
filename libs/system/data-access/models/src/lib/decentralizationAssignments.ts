export interface IDecentralizationAssignments {
    assId: number,
    userName: string,
    polId: number,
    roleId: string,
    objId: number,
    dataType: string,
    name: string,
    code: string,
    createdBy: string,
    createDate: string,
}

export interface IPostDecentralization {
    dataIds: Array<any>,
    dataCode: string | null,
    dataName: string | null,
    parentId: string | null,
    dataType: string | null,
    page?: number,
    size?: number,
}

export interface IDecentralizations {
    dataId: number,
    dataName: string,
    parentId: number,
    parentName: string,
    dataType: string,
    children?: any,
    parentChecked?: any,
    level: any,
    checked?: boolean,
    dataCode?: string,
}
