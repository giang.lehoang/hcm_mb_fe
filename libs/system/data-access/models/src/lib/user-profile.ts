export interface IBaseResponse<T> {
  code: number;
  message: string;
  data: T;
}
export interface IRoleFunctionPermissions<T> {
  roleFunctionPermissions: T;
}
export interface IContent<T> {
  content: T;
  numberOfElements: number;
  size: number;
  totalElements: number;
  totalPages: number;
  pageable: IPageable;
}
export interface TreeNodeInterface {
  key: string;
  name: string;
  level?: number;
  expand?: boolean;
  permissionDTOS?: TreeNodeInterface[];
  parent?: TreeNodeInterface;
  scopes?: string[]
}
export interface IUser {
  createdTimestamp: number;
  emailVerified: boolean;
  enabled: boolean;
  federationLink: string;
  firstName: string;
  id: string;
  notBefore: number;
  totp: boolean;
  username: string;
}

export interface IPageable {
  offset: number;
  pageNumber: number;
  pageSize: number;
  paged: boolean;
}

export interface IUserInfo {
  userId: string;
  username: string;
  fullName: string;
  empCode: string;
  dateOfBirth?: string;
  genderName?: string;
  phoneNumber?: string;
  email?: string;
  address?: string;
  orgName?: string;
  jobName?: string;
  posName?: string;
  flagStatus: string;
  isTracing: string;
  reasonLock: string;
}
export interface IUserPram {
  username: string;
}

export interface IFunctionPermission {
  key: string;
  dataDecentralizationList: IDecentralizationList[];
  functionPermissionObjects: IFunctionPermissionObject[];
  roleCode: string;
  active?: boolean | string;
}

export interface IDecentralizationList {
  dataId: number;
  dataCode: string;
  dataName: string;
  dataType: string;
}
export interface IFunctionPermissionObject {
  key: string;
  name: string;
  permissionDTOS: IPermissionDTOS[];
}
export interface IPermissionDTOS {
  key: string;
  name: string;
  scopes: string[];
}
