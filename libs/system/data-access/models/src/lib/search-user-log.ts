export interface IScope {
  code?: string;
  description?: string;
  id?: string;
  links?: Array<string>;
  name?: string;
}

export interface IUserLog {
  userId: string;
  appName: string;
  dateTime: string;
  resourceName: string;
  scopeCode: string;
  scopeName: string;
  userName: string;
  msgContents?: string
}
