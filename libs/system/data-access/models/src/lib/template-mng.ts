export interface IListTemplate {
  ngrId: string;
  ntpId: string;
  ntpCode: string;
  ntpName: string;
  ngrName: string;
  subject: string;
  shortContent: string;
  flagStatus: string;
  fromPassword: string;
  mailCC: string;
  fromAddress: string;
  mailBCC: string;
  notiLink: string;
}

export interface INotiGroup {
  createBy: string;
  flagStatus: number;
  isEmail: string;
  isNotify: string;
  ngrCode: string;
  ngrId: number;
  ngrName: string;
}

export interface IAsssessmentSelect {
  value: number | string;
  label: string;
}

export const mbDataSelectsAssessmentStatus = (): IAsssessmentSelect[] => [
  {
    value: 1,
    label: 'Hoạt động',
  },
  {
    value: 0,
    label: 'Không hoạt động',
  },
];
