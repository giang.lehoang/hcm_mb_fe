export interface ManagerModel {
  employeeCode: string;
  employeeId: number;
  fullName?: string;
  jobName: string;
  orgName: string;
  posName: string;
  managerProcessId:number;
}

export interface EmployeeDetail {
  empCode: string;
  empId: number;
  employeeCode: string;
  employeeId: number;
  fullName: string;
  jobName: string;
  levelName: string;
  orgName: string;
  positionName: string;
  positionId: number | string;
}

export interface AssignRequest {
  managerProcessId: string | number | null,
  employeeId: string | number,
  employeeFullName: string | number,
  managerId: string | number,
  indirectManagerId: string | number,
  positionId: string | number,
  fromDate: string,
  toDate: string,
  hasIndirectManager: string | boolean
  jobName?:string;
  orgName?:string;
  posName?:string;
}
export class ParamsModel {
  employeeId = '';
  orgId = '';
  jobId = '';
  isAuto = '';
  managerId = '';
  indirectManagerId = '';
  fromDate= '';
  isLasted = '1';
  isAssign = '0';
  orgName = '';
  page = 0;
  size = 15;
}

export interface ProcessManagerDetail {
  employeeCode: string;
  employeeId: number;
  employeeLevel: string;
  fromDate: string;
  fullName: string;
  indirectManagerId: number;
  isAuto: string;
  jobId: number;
  jobName: string;
  managerId: number;
  managerProcessId: number;
  orgId: number;
  orgName: string;
  posId: number;
  posName: string
  toDate: string
}
