export interface NotiGroup {
  ngrId?: number,
  ngrCode?: string,
  ngrName?: string,
  flagStatus?: number | boolean,
  isEmail?: string | boolean,
  isNotify?: string | boolean,
  isSendSingle?: string | boolean,
  isSaveLogContent?: string | boolean,
  isFcm?: string | boolean,
}
