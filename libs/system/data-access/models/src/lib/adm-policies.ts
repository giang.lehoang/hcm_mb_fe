export interface IAdmPolicies {
    polId: number,
    polCode: string,
    polName: string,
    dataType: string,
    flagStatus: number
}
