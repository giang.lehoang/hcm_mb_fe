export interface IUserRoleData {
  content: Array<IUserRole>;
}
export interface IUserPermission {
  userId: string;
  username: string;
  fullName: string;
  empCode: string;
  dateOfBirth: string;
  genderName: string;
  phoneNumber: string;
  email: string;
  address: string;
  orgName: string;
  jobName: string;
  posName: string;
  isLocked: string;
  reasonLock: string;
  flagStatus: number | boolean;
  isTracing: string | boolean;
}
export interface IUserRole {
    userId: string,
    id: string,
    createdTimestamp: number | string,
    username: string,
    enabled: boolean,
    totp: boolean,
    emailVerified: boolean,
    firstName: string,
    federationLink: string,
    attributes: {
        LDAP_ENTRY_DN: Array<string>,
        modifyTimestamp: Array<string>,
        LDAP_ID: Array<string>,
        createTimestamp: Array<string | number>,
    },
    disableableCredentialTypes: Array<any>
    requiredActions: Array<any>,
    notBefore: number,
    access: {
        manageGroupMembership: boolean,
        view: boolean,
        mapRoles: boolean,
        impersonate: boolean,
        manage: boolean
    }
}

export interface IRoleData {
  content: Array<IRole>;
}

export interface IRole {
  id: string;
  name: string;
  description?: string;
  links?: string[];
  isChecked?: boolean;
}

export interface Pagination {
  currentPage: number,
  totalElement: number,
  size?: number,
  totalPage: number,
  numberOfElements: number;
  numCaculate?: number;
}
