import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from '@hcm-mfe/system/data-access/services';
import { IBaseResponse, IContent, IListTemplate, INotiGroup } from '@hcm-mfe/system/data-access/models';

@Injectable({
  providedIn: 'root',
})
export class TemplateManagementService extends BaseService {

  readonly baseUrl = UrlConstant.TEMPLATE_MNG;
  public searchTemplate(params: any): Observable<IBaseResponse<IContent<IListTemplate[]>>> {
    const url = this.baseUrl.SEARCH_TEMPLATE;
    return this.get(url, { params }, UrlConstant.URL_ENDPOIN_UTILITIES);
  }
  public getListGroupNoti(): Observable<IBaseResponse<INotiGroup[]>> {
    const url = this.baseUrl.NOTI_GROUP;
    return this.get(url, undefined, UrlConstant.URL_ENDPOIN_UTILITIES);
  }
  public deleteTemplate(id: any): Observable<IBaseResponse<IContent<IListTemplate[]>>> {
    const url = this.baseUrl.SEARCH_TEMPLATE + `/${id}`;
    return this.delete(url, undefined, UrlConstant.URL_ENDPOIN_UTILITIES);
  }
  public createTemplate(data: any): Observable<IBaseResponse<IContent<IListTemplate[]>>> {
    const url = this.baseUrl.SEARCH_TEMPLATE;
    return this.post(url, data, undefined, UrlConstant.URL_ENDPOIN_UTILITIES);
  }
  public updateTemplate(data: any, id: any): Observable<IBaseResponse<IContent<IListTemplate[]>>> {
    const url = this.baseUrl.SEARCH_TEMPLATE + `/${id}`;
    return this.put(url, data, UrlConstant.URL_ENDPOIN_UTILITIES);
  }
  public getDetailTemplate(id: any): Observable<IBaseResponse<IListTemplate>> {
    const url = this.baseUrl.SEARCH_TEMPLATE + `/${id}`;
    return this.get(url, undefined, UrlConstant.URL_ENDPOIN_UTILITIES);
  }
  public sendEmail(data: any): Observable<any> {
    const url = this.baseUrl.SEND_EMAIL;
    return this.post(url, data, undefined, UrlConstant.URL_ENDPOIN_UTILITIES);
  }
}
