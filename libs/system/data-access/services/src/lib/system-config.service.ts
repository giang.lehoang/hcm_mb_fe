import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from '@hcm-mfe/system/data-access/services';

@Injectable({
  providedIn: 'root',
})
export class SystemConfigService extends BaseService {
  readonly baseUrl = UrlConstant.CONFIGS;

  public searchAdmConfig(params: any): Observable<any> {
    const url = this.baseUrl;
    return this.get(url, { params }, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public getAdmConfigById(id: string): Observable<any> {
    const url = this.baseUrl + UrlConstant.CONFIG.BY_ID.replace('{id}', id);
    return this.get(url, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public createAdmConfig(data: any): Observable<any> {
    const url = this.baseUrl;
    return this.post(url, data, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public updateAdmConfig(data: any): Observable<any> {
    const url = this.baseUrl;
    return this.put(url, data, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public deleteAdmConfig(id: string): Observable<any> {
    const url = this.baseUrl + UrlConstant.CONFIG.BY_ID.replace('{id}', id);
    return this.delete(url, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }
}
