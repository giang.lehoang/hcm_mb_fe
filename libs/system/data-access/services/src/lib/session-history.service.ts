import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from '@hcm-mfe/system/data-access/services';

@Injectable({
  providedIn: 'root'
})
export class SessionHistoryService extends BaseService {
  readonly baseUrl = UrlConstant.SESSION_HISTORY;
  readonly urlDetail = UrlConstant.SESSION_DETAIL;

  // Lấy danh sách
  public getList(param: any): Observable<any> {
    const url = this.baseUrl;
    return this.get(url, {params: param}, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public findById(utcId: string): Observable<any> {
    const url = this.baseUrl + UrlConstant.SESSION_HISTORYS.DETAIL.replace('{utcId}', utcId);
    return this.get(url, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public getListDetail(utcId: string, param: any): Observable<any> {
    const url = this.urlDetail + UrlConstant.SESSION_DETAILS.LIST_DETAIL.replace('{utcId}', utcId);
    return this.get(url, {params: param}, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

}
