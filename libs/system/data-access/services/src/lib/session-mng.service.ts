import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from '@hcm-mfe/system/data-access/services';
import { IBaseResponse, IContent, ISession } from '@hcm-mfe/system/data-access/models';

@Injectable({
  providedIn: 'root',
})
export class SessionManageService extends BaseService {
  constructor(private readonly http: HttpClient) {
    super(http);
  }
  readonly baseUrl = UrlConstant.SESSION_MNG;
  public getListSession(params: any): Observable<IBaseResponse<IContent<ISession[]>>> {
    const url = this.baseUrl;
    return this.get(url, { params }, UrlConstant.URL_ENDPOIN_CATEGORY);
  }
  public cancleSession(id: any): Observable<IBaseResponse<IContent<ISession[]>>> {
    const params = { session: id };
    const url = this.baseUrl;
    return this.delete(url, { params }, UrlConstant.URL_ENDPOIN_CATEGORY);
  }
}
