import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from '@hcm-mfe/system/data-access/services';
import { IBaseResponse, IContent, IScope, IUserLog } from '@hcm-mfe/system/data-access/models';

@Injectable({
  providedIn: 'root',
})
export class UserLogService extends BaseService {
  constructor(private readonly http: HttpClient) {
    super(http);
  }
  readonly baseUrl = UrlConstant.USER_LOG;
  public searchUserLog(params: any): Observable<IBaseResponse<IContent<IUserLog[]>>> {
    const url = this.baseUrl.LIST_USER_LOG;
    return this.get(url, { params }, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public getScope(): Observable<IBaseResponse<IContent<IScope[]>>> {
    const url = this.baseUrl.SCOPES;
    return this.get(url, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }
}
