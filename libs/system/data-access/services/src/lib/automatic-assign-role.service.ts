import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {BaseService} from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from './constant/url.class';
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";
@Injectable({
  providedIn: 'root',
})
export class AutomaticAssignRoleService extends BaseService {
  readonly baseUrlRole = UrlConstant.ROLES;

  public getListPositionByRoleId(id: string, params: any): Observable<any> {
    const url = this.baseUrlRole + UrlConstant.ROLE.LIST_POSITION_BY_ROLE.replace('{roleId}', id);
    return this.get(url, {params}, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public deletePositionRoleMapping(rpmId: number): Observable<any> {
    const url = UrlConstant.ROLE.DELETE_ROLE_POSITION_MAPPING.replace('{rpmId}', rpmId.toString());
    return this.delete(url, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public getPositionRoleMapping(params: any): Observable<any> {
    const url = UrlConstant.AUTOMATIC_ASSIGN_ROLE.GET;
    return this.post(url, params,undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public savePositionRoleMapping(params: any): Observable<any> {
    const url = UrlConstant.AUTOMATIC_ASSIGN_ROLE.SAVE;
    return this.post(url, params,undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  syncAutomatic(): Observable<any> {
    const url = UrlConstant.AUTOMATIC_ASSIGN_ROLE.SYNC_AUTOMATIC;
    return this.post(url, undefined, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  getSyncStatus(): Observable<any> {
    const url = UrlConstant.AUTOMATIC_ASSIGN_ROLE.CHECK_STATUS;
    return this.get(url, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }
}
