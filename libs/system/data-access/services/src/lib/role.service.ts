import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from './constant/url.class';
@Injectable({
  providedIn: 'root',
})
export class RoleService extends BaseService {
  readonly baseUrl = UrlConstant.ROLES;

  public searchRole(params: any): Observable<any> {
    const url = this.baseUrl;
    return this.get(url, { params }, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public getRoleById(id: string): Observable<any> {
    const url = this.baseUrl + UrlConstant.ROLE.BY_ID.replace('{id}', id);
    return this.get(url, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public createRole(data: any): Observable<any> {
    const url = this.baseUrl;
    return this.post(url, data, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public updateRole(data: any): Observable<any> {
    const url = this.baseUrl;
    return this.put(url, data, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public deleteRoleById(id: string): Observable<any> {
    const url = this.baseUrl + UrlConstant.APP.BY_ID.replace('{id}', id);
    return this.delete(url, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public getUserByRoleId(id: string): Observable<any> {
    const url = this.baseUrl + UrlConstant.ROLE.USER_BY_ROLE.replace('{id}', id);
    return this.get(url, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public getListUserByRoleId(id: string, params: any): Observable<any> {
    const url = this.baseUrl + UrlConstant.ROLE.LIST_USER_BY_ROLE.replace('{id}', id);
    return this.get(url, {params}, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  syncRole(): Observable<any> {
    const url = this.baseUrl + UrlConstant.ROLE.SYNC_ROLE;
    return this.get(url, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }
}
