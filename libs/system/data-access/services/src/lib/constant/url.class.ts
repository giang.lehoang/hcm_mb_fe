export class UrlConstant {
  public static readonly URL_ENDPOIN_CATEGORY = 'URL_ENDPOIN_CATEGORY';

  public static readonly URL_ENDPOIN_UTILITIES = 'URL_ENDPOIN_UTILITIES';

  public static readonly SESSION_HISTORY = 'sessions-history';
  public static readonly URL_ENDPOINT_EMPLOYEE = '/employees';

  public static readonly SESSION_HISTORYS = {
    DETAIL: '/{utcId}',
  }

  public static readonly AUTOMATIC_ASSIGN_ROLE = {
    GET: 'model-plan/position/list/roles',
    SAVE: 'role-position-mappings/save',
    SYNC_AUTOMATIC: 'role-position-mappings/assign-list-role-for-list-user',
    CHECK_STATUS: 'role-position-mappings/check-status-of-assign-list-role-for-list-user'
  }

  public static readonly SESSION_DETAIL = 'sessions-detail';

  public static readonly SESSION_DETAILS = {
    LIST_DETAIL: '/{utcId}',
  }
  public static readonly SESSION_MNG = 'sessions-management';
  public static readonly USER_LOG = {
    LIST_USER_LOG: 'user-logs/search',
    SCOPES: 'scopes'
  };
  public static readonly USER_PERMISSION = {
    ADM_POLICIES: 'adm-policies',
    DECENTRALIZATION_USER: 'decentralization/assignments',
    DECENTRALIZATION: 'model-plan/get-data-decentralization'
  };
  public static readonly USERS = 'users';

  public static readonly USER = {
    INFO_USER: '/{username}',
    BY_ID: '/find-by-id/{id}',
    SEARCH: '/search',
    GET_ROLE_BY_USER: '/{id}/role-assign-user',
    GET_ROLE_BY_USER_INCLUDE_ROLE_ALL: '/{id}/role-assign-user-include-role-all',
    UPDATE_ROLE_BY_USER: '/{id}/assign-roles-to-user',
    CHANGE_STATUS: '/update-state/{id}',
    GET_INFO_PERMISSIONS_ADMIN: '/role-function-permission',
    GET_INFO_PERMISSIONS: '/role-function-permission-user',
    SYNC_USERS_FROM_KEYCLOAK: '/sync-users-from-keycloak',
    GET_SYNC_STATUS: '/sync-status'
  }

  public static readonly APPS = 'apps';
  public static readonly MEILI_SEARCH_INDEXES = 'v1.0/full-text-search/indexes';

  public static readonly APP = {
    SEARCH: '/search',
    BY_ID: '/{id}',
  }

  public static readonly INDEX = {
    BY_ID: '/{indexUid}',
    SETTING: '/{indexUid}/settings',
    IMPORT_DOCUMENT: '/{index}/import-documents',
  }

  public static readonly CONFIGS = 'adm-config';

  public static readonly CONFIG = {
    BY_ID: '/{id}'
  }

  public static readonly SCOPES = 'scopes';

  public static readonly SCOPE = {
    BY_ID: '/{id}',
  }

  public static readonly ROLES = 'roles';

  public static readonly ROLE = {
    BY_ID: '/{id}',
    USER_BY_ROLE: '/{id}/users',
    LIST_USER_BY_ROLE: '/{id}/get-user-by-role',
    LIST_POSITION_BY_ROLE: '/{roleId}/positions',
    DELETE_ROLE_POSITION_MAPPING: '/role-position-mappings/{rpmId}',
    SYNC_USER_ROLE: '/sync-role-assign-user-from-keycloak',
    CHECK_STATUS: '/sync-role-assign-user-from-keycloak/sync-status',
    SYNC_ROLE: '/sync-role-from-keycloak',
  }

  public static readonly FUNCTIONS = 'function';

  public static readonly FUNCTION = {
    BY_ID: '/{id}',
  }

  public static readonly PERMISSIONS = 'permissions';

  public static readonly PERMISSION = {
    BY_ID: '/{id}?type={type}',
    DELETE_BY_ID: '/{id}',
    GET_POLICY: '/get-policy',
    GET_BY_ROLE: '/permissions-by-role?roleCode={roleCode}',
    ASSIGN: '/assign-permissions-role',
    SYNC_PERMISSION: '/sync-role-permission-from-keycloak',
    CHECK_STATUS: '/sync-status',
    EXPORT: '/export-role-permission'
  }
  public static readonly TEMPLATE_MNG = {
    SEARCH_TEMPLATE: 'v1.0/emails-notifications',
    NOTI_GROUP: 'v1.0/notification-groups',
    SEND_EMAIL: 'v1.0/mail/send-test-email'
  };

  public static readonly NOTI_GROUP = 'v1.0/notification-groups';

  public static readonly NOTI_GROUPS = {
    SEARCH: '/search',
    BY_ID: '/{id}'
  };

  public static readonly IMPORT_FORM = {
    USER_ROLE: 'roles/get-template',
    USER_ROLE_IMPORT: 'roles/import-template',
    USER_PERMISSION: 'decentralization/assignments/get-template',
    USER_PERMISSION_IMPORT: 'decentralization/assignments/import-data-assignment',
    RESOURCE: 'function/get-template',
    RESOURCE_IMPORT: 'function/import-template',
    ROLE: 'roles/get-template-import-roles',
    ROLE_IMPORT: 'roles/import-excel-template/roles',
    PERMISSIONS: 'permissions/get-template',
    PERMISSIONS_IMPORT: 'permissions/import-permission-assign-role'
  }
  public static readonly MANAGER_DIRECT = {
    MANAGER_LIST: '/v1.0/employees/manager/list',
    MANAGER_PROCESS_ASSIGN: '/v1.0/manager-processes/assigned',
    LIST_DATA: '/v1.0/manager-processes/assigned/list',
    DELETE_MANAGER: '/v1.0/manager-processes/assigned',
    EXPORT_EXCEL: '/v1.0/manager-processes/assigned/list/export',
    DIRECT_INFO: '/v1.0/manager-processes/overview',
    SUB_EMP: '/v1.0/manager-processes/lower'
  }
}
