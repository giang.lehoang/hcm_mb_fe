import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from './constant/url.class';

@Injectable({
  providedIn: 'root',
})
export class AppService extends BaseService {
  readonly baseUrl = UrlConstant.APPS;

  public searchAppCategory(params: any): Observable<any> {
    const url = this.baseUrl + UrlConstant.APP.SEARCH;
    return this.get(url, { params }, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public getAppCategoryById(id: string): Observable<any> {
    const url = this.baseUrl + UrlConstant.APP.BY_ID.replace('{id}', id);
    return this.get(url, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public createAppCategory(data: any): Observable<any> {
    const url = this.baseUrl;
    return this.post(url, data, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public updateAppCategory(data: any): Observable<any> {
    const url = this.baseUrl;
    return this.put(url, data, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public deleteAppCategory(id: string): Observable<any> {
    const url = this.baseUrl + UrlConstant.APP.BY_ID.replace('{id}', id);
    return this.delete(url, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }
}
