import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from './constant/url.class';
import { NzSafeAny } from 'ng-zorro-antd/core/types';

@Injectable({
  providedIn: 'root',
})
export class IndexService extends BaseService {
  readonly baseUrl = UrlConstant.MEILI_SEARCH_INDEXES;

  public searchIndexes(params: any): Observable<any> {
    const url = this.baseUrl;
    return this.get(url, { params }, UrlConstant.URL_ENDPOIN_UTILITIES);
  }

  public createIndex(data: any): Observable<any> {
    const url = this.baseUrl;
    return this.post(url, data, undefined, UrlConstant.URL_ENDPOIN_UTILITIES);
  }

  public deleteIndex(id: string): Observable<any> {
    const url = this.baseUrl + UrlConstant.INDEX.BY_ID.replace('{indexUid}', id.toString());
    return this.delete(url, undefined, UrlConstant.URL_ENDPOIN_UTILITIES);
  }

  public getIndexSettingById(id: string): Observable<any> {
    const url = this.baseUrl + UrlConstant.INDEX.SETTING.replace('{indexUid}', id.toString());
    return this.get(url, undefined, UrlConstant.URL_ENDPOIN_UTILITIES);
  }

  public updateIndexSettingById(data: any, id: string): Observable<any> {
    const url = this.baseUrl + UrlConstant.INDEX.SETTING.replace('{indexUid}', id.toString());
    return this.put(url, data, UrlConstant.URL_ENDPOIN_UTILITIES);
  }

  public importDocument(data: FormData, index: string): Observable<any> {
    const url = this.baseUrl + UrlConstant.INDEX.IMPORT_DOCUMENT.replace("{index}", index);
    return this.post(url, data, undefined, UrlConstant.URL_ENDPOIN_UTILITIES);
  }

}
