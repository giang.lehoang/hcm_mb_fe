import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from './constant/url.class';

@Injectable({
  providedIn: 'root',
})
export class FunctionService extends BaseService {
  readonly baseUrl = UrlConstant.FUNCTIONS;

  public searchResourceCategory(params: any): Observable<any> {
    const url = this.baseUrl;
    return this.get(url, { params }, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public getResourceCategoryById(id: string): Observable<any> {
    const url = this.baseUrl + UrlConstant.FUNCTION.BY_ID.replace('{id}', id);
    return this.get(url, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public createResourceCategory(data: any): Observable<any> {
    const url = this.baseUrl;
    return this.post(url, data, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public updateResourceCategory(data: any): Observable<any> {
    const url = this.baseUrl;
    return this.put(url, data, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public deleteResourceCategory(id: string): Observable<any> {
    const url = this.baseUrl + UrlConstant.FUNCTION.BY_ID.replace('{id}', id);
    return this.delete(url, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }
}
