import { Observable } from 'rxjs'
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from '@hcm-mfe/system/data-access/services';
import {
  IBaseResponse, IContent,
  IFunctionPermission,
  IRoleFunctionPermissions, IUser,
  IUserInfo,
  Role
} from '@hcm-mfe/system/data-access/models';

@Injectable({
  providedIn: 'root',
})
export class UserService extends BaseService {
  constructor(private readonly http: HttpClient) {
    super(http)
  }

  readonly baseUrl = UrlConstant.USERS;

  public searchUser(params: any): Observable<any> {
    const url = this.baseUrl;
    return this.get(url, {params}, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public getUserInfo(username: string): Observable<IBaseResponse<IUserInfo>> {
    const url = this.baseUrl + UrlConstant.USER.INFO_USER.replace('{username}', username);
    return this.get(url, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public findById(id: string): Observable<any> {
    const url = this.baseUrl + UrlConstant.USER.BY_ID.replace('{id}', id);
    return this.get(url, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public searchUserRole(params: any): Observable<any> {
    const url = this.baseUrl + UrlConstant.USER.SEARCH;
    return this.get(url, {params}, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public getRolesByUser(id: string): Observable<any> {
    const url = this.baseUrl + UrlConstant.USER.GET_ROLE_BY_USER.replace('{id}', id);
    return this.get(url, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public getRolesByUserIncludeRoleAll(id: string): Observable<any> {
    const url = this.baseUrl + UrlConstant.USER.GET_ROLE_BY_USER_INCLUDE_ROLE_ALL.replace('{id}', id);
    return this.get(url, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public updateRolesByUser(id: string, data: Array<Role>): Observable<any> {
    const url = this.baseUrl + UrlConstant.USER.UPDATE_ROLE_BY_USER.replace('{id}', id);
    return this.post(url, data, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public search(params: any): Observable<any> {
    const url = this.baseUrl;
    return this.get(url, {params}, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  changeStatus(id: string, params: any): Observable<any> {
    const url = this.baseUrl + UrlConstant.USER.CHANGE_STATUS.replace('{id}', id);
    return this.get(url, {params}, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  getFunctionPermissionByAdmin(params: any): Observable<IBaseResponse<IRoleFunctionPermissions<IFunctionPermission[]>>> {
    const url = this.baseUrl + UrlConstant.USER.GET_INFO_PERMISSIONS_ADMIN;
    return this.get(url, {params}, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  getFunctionPermission(): Observable<IBaseResponse<IRoleFunctionPermissions<IFunctionPermission[]>>> {
    const url = this.baseUrl + UrlConstant.USER.GET_INFO_PERMISSIONS;
    return this.get(url, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  getListUser(params: any): Observable<IBaseResponse<IContent<IUserInfo[]>>> {
    const url = this.baseUrl;
    return this.get(url, {params}, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public syncUsersFromKeycloak(): Observable<any> {
    const url = this.baseUrl + UrlConstant.USER.SYNC_USERS_FROM_KEYCLOAK;
    return this.post(url, undefined, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public getSyncStatus(params: any): Observable<any> {
    const url = this.baseUrl + UrlConstant.USER.GET_SYNC_STATUS;
    return this.get(url, {params}, UrlConstant.URL_ENDPOIN_CATEGORY);
  }
}
