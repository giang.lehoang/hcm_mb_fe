import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from './constant/url.class';

@Injectable({
  providedIn: 'root',
})
export class NotiGroupService extends BaseService {
  readonly baseUrl = UrlConstant.NOTI_GROUP;

  public searchNotiGroup(params: any): Observable<any> {
    const url = this.baseUrl + UrlConstant.NOTI_GROUPS.SEARCH;
    return this.get(url, { params }, UrlConstant.URL_ENDPOIN_UTILITIES);
  }

  public getNotiGroupById(id: string): Observable<any> {
    const url = this.baseUrl + UrlConstant.NOTI_GROUPS.BY_ID.replace('{id}', id);
    return this.get(url, undefined, UrlConstant.URL_ENDPOIN_UTILITIES);
  }

  public createNotiGroup(data: any): Observable<any> {
    const url = this.baseUrl;
    return this.post(url, data, undefined, UrlConstant.URL_ENDPOIN_UTILITIES);
  }

  public updateNotiGroup(data: any): Observable<any> {
    const url = this.baseUrl;
    return this.put(url, data, UrlConstant.URL_ENDPOIN_UTILITIES);
  }

  public deleteNotiGroup(id: string): Observable<any> {
    const url = this.baseUrl + UrlConstant.NOTI_GROUPS.BY_ID.replace('{id}', id);
    return this.delete(url, undefined, UrlConstant.URL_ENDPOIN_UTILITIES);
  }
}
