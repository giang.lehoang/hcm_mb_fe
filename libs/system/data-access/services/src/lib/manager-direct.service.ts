import { Injectable } from '@angular/core';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { MICRO_SERVICE, SERVICE_NAME } from '@hcm-mfe/shared/common/constants';
import { UrlConstant } from '@hcm-mfe/system/data-access/services';
import { ParamsJDSearch } from '@hcm-mfe/model-organization/data-access/models';
import { JD_URL } from '../../../../../model-organization/data-access/services/src/lib/constant/jd.url';
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root',
})
export class ManagerDirectService extends BaseService {

  getListManager(params:any) {
    return this.get(UrlConstant.MANAGER_DIRECT.MANAGER_LIST, { params: params }, MICRO_SERVICE.EMPLOYEE_MANAGEMENT);
  }
  getListData(params:any) {
    return this.get(UrlConstant.MANAGER_DIRECT.LIST_DATA, { params: params }, MICRO_SERVICE.EMPLOYEE_MANAGEMENT);
  }

  assignManager(body:any){
    return this.post(UrlConstant.MANAGER_DIRECT.MANAGER_PROCESS_ASSIGN, body, {}, MICRO_SERVICE.EMPLOYEE_MANAGEMENT);
  }
  deleteManager(id:any){
    const url = '/'+ id;
    return this.delete(UrlConstant.MANAGER_DIRECT.DELETE_MANAGER+url, {}, MICRO_SERVICE.EMPLOYEE_MANAGEMENT);
  }
  exportExcel(params:any) {
    return this.get(UrlConstant.MANAGER_DIRECT.EXPORT_EXCEL, { responseType: 'arraybuffer',params: params}, MICRO_SERVICE.EMPLOYEE_MANAGEMENT);
  }

  assignManagerDetail(id:number | string){
    return this.get(`${UrlConstant.MANAGER_DIRECT.MANAGER_PROCESS_ASSIGN}/${id}` , {}, MICRO_SERVICE.EMPLOYEE_MANAGEMENT);
  }
}
