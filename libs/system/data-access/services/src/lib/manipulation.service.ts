import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from './constant/url.class';

@Injectable({
  providedIn: 'root',
})
export class ManipulationService extends BaseService {
  readonly baseUrl = UrlConstant.SCOPES;

  public searchScope(params: any): Observable<any> {
    const url = this.baseUrl;
    return this.get(url, { params }, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public getScopeById(id: string): Observable<any> {
    const url = this.baseUrl + UrlConstant.SCOPE.BY_ID.replace('{id}', id);
    return this.get(url, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public createScope(data: any): Observable<any> {
    const url = this.baseUrl;
    return this.post(url, data, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public updateScope(data: any): Observable<any> {
    const url = this.baseUrl;
    return this.put(url, data, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public deleteScopeById(id: string): Observable<any> {
    const url = this.baseUrl + UrlConstant.SCOPE.BY_ID.replace('{id}', id);
    return this.delete(url, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }
}
