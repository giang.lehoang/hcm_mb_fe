import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from '@hcm-mfe/system/data-access/services';
import { IPostDecentralization } from '@hcm-mfe/system/data-access/models';

@Injectable({
  providedIn: 'root',
})
export class UserPermissionService extends BaseService {
  constructor(private readonly http: HttpClient) {
    super(http);
  }
  readonly baseUrl = UrlConstant.USER_PERMISSION;

  public getAdmPolicies(params: any): Observable<any> {
    const url = this.baseUrl.ADM_POLICIES;
    return this.get(url, { params }, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public getDecentralizationUser(params: any): Observable<any> {
    const url = this.baseUrl.DECENTRALIZATION_USER;
    return this.get(url, { params }, UrlConstant.URL_ENDPOIN_CATEGORY);
  }
  public getDataDecentralizations(data: IPostDecentralization): Observable<any> {
    const url = this.baseUrl.DECENTRALIZATION;
    return this.post(url, data , undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }
  public postDecentralizationUser(params: any): Observable<any> {
    const url = this.baseUrl.DECENTRALIZATION_USER;
    return this.post(url, params , undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }
}
