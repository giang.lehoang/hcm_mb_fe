import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from './constant/url.class';

@Injectable({
  providedIn: 'root',
})
export class PermissionService extends BaseService {
  readonly baseUrl = UrlConstant.PERMISSIONS;

  public getPermission(): Observable<any> {
    const url = this.baseUrl;
    return this.get(url, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public getPermissionById(id: string, type: string): Observable<any> {
    const url = this.baseUrl + UrlConstant.PERMISSION.BY_ID.replace('{id}', id).replace('{type}', type);
    return this.get(url, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public getAllPolicy(params: any): Observable<any> {
    const url = this.baseUrl + UrlConstant.PERMISSION.GET_POLICY;
    return this.get(url, { params }, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public createPermission(data: any): Observable<any> {
    const url = this.baseUrl;
    return this.post(url, data, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public updatePermission(data: any): Observable<any> {
    const url = this.baseUrl;
    return this.put(url, data, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public deletePermission(id: string): Observable<any> {
    const url = this.baseUrl + UrlConstant.PERMISSION.DELETE_BY_ID.replace('{id}', id);
    return this.delete(url, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public getPermissionByRole(roleCode: string): Observable<any> {
    const url = this.baseUrl + UrlConstant.PERMISSION.GET_BY_ROLE.replace('{roleCode}', roleCode);
    return this.get(url, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  public addPermissionForRole(data: any): Observable<any> {
    const url = this.baseUrl + UrlConstant.PERMISSION.ASSIGN;
    return this.post(url, data, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  syncPermission(): Observable<any> {
    const url = this.baseUrl + UrlConstant.PERMISSION.SYNC_PERMISSION;
    return this.get(url, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  getSyncStatus(): Observable<any> {
    const url = this.baseUrl + UrlConstant.PERMISSION.CHECK_STATUS;
    return this.get(url, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  export(): Observable<any> {
    const url = this.baseUrl + UrlConstant.PERMISSION.EXPORT;
    return this.getRequestFileD2T(url, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }
}
