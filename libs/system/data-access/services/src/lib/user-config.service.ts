import {Injectable} from "@angular/core";
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { MICRO_SERVICE } from '@hcm-mfe/shared/common/constants';
import { Observable } from 'rxjs';
import { UrlConstant } from './constant/url.class';

@Injectable({
  providedIn: 'root'
})
export class UserConfigService extends BaseService {
  public getListUserRole(params: any){
    const url = `users`;
    return this.get(url, {params}, MICRO_SERVICE.URL_ENDPOIN_CATEGORY);
  }
  public searchRole(params: any){
    const url = `roles`;
    return this.get(url, {params}, MICRO_SERVICE.URL_ENDPOIN_CATEGORY);
  }

  syncUserRole(): Observable<any> {
    const url = UrlConstant.ROLES + UrlConstant.ROLE.SYNC_USER_ROLE;
    return this.get(url, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }

  getSyncStatus(): Observable<any> {
    const url = UrlConstant.ROLES + UrlConstant.ROLE.CHECK_STATUS;
    return this.get(url, undefined, UrlConstant.URL_ENDPOIN_CATEGORY);
  }
}
