import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalListComponent } from './modal-list/modal-list.component';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { TranslateModule } from '@ngx-translate/core';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, NzGridModule, SharedUiMbInputTextModule, TranslateModule,
    NzButtonModule, SharedUiLoadingModule, NzTableModule, NzPaginationModule, SharedUiMbButtonModule],
  declarations: [ModalListComponent],
  exports: [ModalListComponent],

})
export class SystemUiModalListModule {}
