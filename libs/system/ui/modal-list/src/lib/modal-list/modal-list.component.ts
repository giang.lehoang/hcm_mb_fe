import { Component, OnInit } from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { userConfig } from '@hcm-mfe/shared/common/constants';
import { IBaseResponse, IContent, IUserInfo } from '@hcm-mfe/system/data-access/models';
import { UserService } from '@hcm-mfe/system/data-access/services';

@Component({
  selector: 'app-modal-list',
  templateUrl: './modal-list.component.html',
  styleUrls: ['./modal-list.component.scss'],
})
export class ModalListComponent implements OnInit {
  isLoading: boolean = false;
  params = {
    username: '',
    email: '',
    empCode: '',
    fullName: '',
    jobName: '',
    orgName: '',
    posName: '',
    phoneNumber: '',
    page: 0,
    size: userConfig.pageSize,
  };
  paginationModal = {
    size: 0,
    total: 0,
    pageIndex: 0,
    numberOfElements: 0,
  };
  listUser: IUserInfo[] = [];
  constructor(readonly userService: UserService, readonly modal: NzModalRef) {}

  ngOnInit(): void {
    this.isLoading = true;
    this.getListUser();
  }
  changePage(value: any) {
    this.isLoading = true;
    this.params.page = value - 1;
    this.getListUser();
  }
  getListUser(): void {
    this.userService.getListUser(this.params).subscribe(
      (data: IBaseResponse<IContent<IUserInfo[]>>) => {
        this.listUser = data.data.content;
        this.paginationModal.size = data.data.size;
        this.paginationModal.total = data.data.totalElements;
        this.paginationModal.pageIndex = data.data.pageable.pageNumber;
        this.paginationModal.numberOfElements = data.data.numberOfElements;
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }
  searchUserName(event: any): void {
    if (event.key === 'Enter' || event.type === 'click') {
      this.isLoading = true;
      this.params.page = 0;
      this.params.size = 15;
      this.getListUser();
    }
  }
  chooseUser(index: any): void {
    const data = this.listUser[index].username;
    this.modal.destroy({ data: data });
  }
}
