import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  OnChanges,
  SimpleChanges,
  TemplateRef,
  OnDestroy
} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { FunctionModalComponent } from '../function-modal/function-modal.component';
import { Resource } from '@hcm-mfe/system/data-access/models';

@Component({
  selector: 'app-choose-function-input',
  templateUrl: './choose-function-input.component.html',
  styleUrls: ['./choose-function-input.component.scss'],
})
export class ChooseFunctionInputComponent implements OnInit, OnChanges, OnDestroy {
  value: string | undefined = '';
  functionId: string | undefined;
  modal: NzModalRef | undefined;
  @Input() listFunction: Array<Resource> = [];
  @Input() model: string | undefined;
  @Output() modelChange = new EventEmitter<any>();
  constructor(
    private readonly modalService: NzModalService,
    private readonly translate: TranslateService
  ) {}
  ngOnInit(): void {
    if (this.listFunction && this.model) {
      const item = this.listFunction.find((obj) => {
        return obj.id === this.model;
      });
      if (item) {
        this.value = item.name;
        this.functionId = item.id;
      }
    }
  }

  ngOnChanges(simpleChange: SimpleChanges) {
    this.ngOnInit();
  }

  showModalUpdate(icon: TemplateRef<any>, footerTmpl: TemplateRef<any>) {
    this.modal = this.modalService.create({
      nzCloseIcon: icon,
      nzWidth: this.getModalWidth(),
      nzTitle: this.translate.instant('system.title.resource'),
      nzContent: FunctionModalComponent,
      nzComponentParams: {
        functionId: this.functionId,
        listFunction: this.listFunction,
      },
      nzFooter: footerTmpl
    });
    this.modal.afterClose.subscribe((res) => {
      if(res != null) {
        if (res) {
          this.functionId = res?.id;
          this.value = res?.name;
        } else {
          this.functionId = '';
          this.value = '';
        }
      }
      this.modelChange.emit(this.functionId);
    });
  }

  getModalWidth(): number {
    if(window.innerWidth > 767) {
      return window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5
    }
    return window.innerWidth;
  }

  clearData($event: any) {
    $event.stopPropagation();
    this.value = '';
    this.functionId = '';
    this.modelChange.emit(this.functionId);
  }

  ngOnDestroy() {
    this.modal?.destroy();
  }
}
