import { Subscription } from 'rxjs';
import { Component, HostBinding, Injector, OnInit, ViewEncapsulation } from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { App, Resource, TreeNode } from '@hcm-mfe/system/data-access/models';
import { AppService, FunctionService } from '@hcm-mfe/system/data-access/services';
import { maxInt32 } from '@hcm-mfe/shared/common/enums';
import { CommonUtils } from '@hcm-mfe/shared/core';

@Component({
  selector: 'app-function-modal',
  templateUrl: './function-modal.component.html',
  styleUrls: ['./function-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class FunctionModalComponent extends BaseComponent implements OnInit {
  @HostBinding('class') elementClass = 'app-function-modal';
  subs: Subscription[] = [];
  functionId: string | undefined;
  listFunction: Array<Resource> = [];
  selected: any;
  listApp: Array<App> = [];

  listData: Array<Resource> = [];
  temp: Array<Resource> = [];
  textFilter = '';
  listOfMap: TreeNode[] = []
  mapOfExpanded: { [key: string]: TreeNode[] } = {};


  constructor(
    injector: Injector,
    private readonly functionService: FunctionService,
    private readonly appService: AppService,
    private readonly modalRef: NzModalRef,
  ) {
    super(injector)
  }

  selectedItem(item: any) {
    this.functionId = item.id;
    this.selected = item;
  }

  ngOnInit(): void {
    this.isLoading = true;
    const param = { page: 0, size: maxInt32 };
    const sub = this.appService.searchAppCategory(param).subscribe(
      (listApp) => {
        this.listApp = listApp.data.content;
      }, () => {
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
    if (!this.listFunction) {
      const subResource = this.functionService.searchResourceCategory(param).subscribe(listData => {
          this.getData(listData.data.content);
        }, () => {
          this.isLoading = false;
        }
      );
      this.subs.push(subResource);
    } else {
      this.getData(this.listFunction);
    }
  }

  getData(list: any) {
    this.temp = list || [];
    this.listData = list || [];
    this.updateFilter();
  }

  selectedAndChoose(item: any) {
    this.functionId = item.id;
    this.selected = item;
    this.choose();
  }

  choose() {
    if (!this.selected && this.functionId) {
      this.modalRef.close(this.temp.find(el => el.id === this.functionId));
    } else {
      this.modalRef.close(this.selected);
    }
  }

  generateAppName(type: any) {
    if (this.listApp && this.listApp.length > 0) {
      for (const app of this.listApp) {
        if (type === app.appId) {
          return app.appName;
        }
      }
    }
    return '';
  }

  close() {
    if (!this.selected && this.functionId) {
      this.modalRef.close(this.temp.find(el => el.id === this.functionId));
    } else {
      this.modalRef.close(false);
    }
  }

  search() {
    this.isLoading = true;
    this.updateFilter();
  }

  updateFilter() {
    this.textFilter = this.textFilter.trim();
    const val = this.textFilter.toLowerCase();
    this.listData = [];

    // filter our data
    const temp = this.temp.filter((d: any) => {
      return d.code.toLowerCase().indexOf(val) !== -1 || d.name.toLowerCase().indexOf(val) !== -1 || !val;
    });
    const listResult = [...temp];
    CommonUtils.sortResource(listResult, 'indexNumber', 'code')
    // update the rows
    this.listData = listResult;
    this.listOfMap = [];
    this.mapOfExpanded = {};
    this.listOfMap = CommonUtils.convertToTree(this.listData, 'sortResource', ['indexNumber', 'code'], false);
    CommonUtils.mapOfExpand(this.listOfMap, this.mapOfExpanded, true);
    this.isLoading = false;
  }

  collapses(array: TreeNode[], data: TreeNode, $event: boolean | any): void {
    CommonUtils.collapses(array, data, $event, true);
  }

  override ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
