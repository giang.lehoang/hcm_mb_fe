import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChooseFunctionInputComponent } from './choose-function-input/choose-function-input.component';
import { FunctionModalComponent } from './function-modal/function-modal.component';
import { NzInputModule } from 'ng-zorro-antd/input';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { FormsModule } from '@angular/forms';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzFormModule } from 'ng-zorro-antd/form';
import { SharedDirectivesTrimInputModule } from '@hcm-mfe/shared/directives/trim-input';

@NgModule({
  imports: [CommonModule, NzInputModule, TranslateModule, SharedUiMbButtonModule, NzIconModule, FormsModule, NzToolTipModule, NzTableModule, NzFormModule,
    SharedDirectivesTrimInputModule],
  declarations: [ChooseFunctionInputComponent, FunctionModalComponent],
  exports: [ChooseFunctionInputComponent]
})
export class SystemUiChooseFunctionModule {}
