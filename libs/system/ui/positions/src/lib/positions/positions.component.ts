import {
  Component,
  Injector,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild
} from '@angular/core';
import {Job, Position, RolePosition} from "@hcm-mfe/system/data-access/models";
import {BaseResponse, TableConfig} from "@hcm-mfe/shared/data-access/models";
import {userConfig} from "@hcm-mfe/shared/common/constants";
import {AutomaticAssignRoleService} from "@hcm-mfe/system/data-access/services";
import {ShareService, ValidateService} from "@hcm-mfe/shared/core";
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {Subscription} from "rxjs";
import {NzModalRef} from "ng-zorro-antd/modal";
import {NzSafeAny} from "ng-zorro-antd/core/types";

@Component({
  selector: 'hcm-mfe-component',
  templateUrl: './positions.component.html',
  styleUrls: ['./positions.component.scss']
})
export class PositionsComponent extends BaseComponent implements OnInit, OnDestroy {
  subs: Subscription[] = [];
  pageSize = userConfig.pageSize;
  listPosition: Array<Position> = [];
  listJob: Array<Job> = [];
  roleId = '';
  paramsPosition = {
    size: this.pageSize,
    page: 0,
    jobId: '',
    orgId: '',
    type: ''
  };

  objRolePosition: RolePosition = {
    roleId: '',
    listPos: []
  }

  tableConfig!: TableConfig;
  checkAll: boolean[] = [];
  @ViewChild('selectTmpl', { static: true }) select!: TemplateRef<NzSafeAny>;
  @ViewChild('selectHeaderTmpl', { static: true }) selectHeader!: TemplateRef<NzSafeAny>;
  constructor(
    injector: Injector,
    private readonly assignRoleService: AutomaticAssignRoleService,
    public validateService: ValidateService,
    readonly shareService: ShareService,
    private readonly modalRef: NzModalRef,
  ) {
    super(injector)
  }

  ngOnInit(): void {
    const sub = this.shareService.getJobsByType().subscribe((res: BaseResponse) => {
      this.listJob = res.data.content
    });
    this.subs.push(sub);
    this.paramsPosition.type = this.roleId;
    this.initTable();
  }

  initTable() {
    this.tableConfig = {
      headers: [
        {
          title: ' ',
          field: 'isCheck',
          tdTemplate: this.select,
          thTemplate: this.selectHeader,
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          fixed: true,
          fixedDir: 'left',
          width: 40,
        },
        {
          title: 'STT',
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          fixed: true,
          fixedDir: 'left',
          width: 70
        },
        {
          title: 'system.automaticAssignRole.label.table.posCode',
          field: 'posCode', width: 120,
        },
        {
          title: 'system.automaticAssignRole.label.table.posName',
          field: 'posName', width: 250,
        },
        {
          title: 'system.automaticAssignRole.label.table.orgName',
          field: 'orgName', width: 250,
        },
        {
          title: 'system.automaticAssignRole.label.table.jobName',
          field: 'jobName', width: 150,
        },
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.paramsPosition.size,
      pageIndex: 1,
    };
  }

  changePageSize(size: number) {
    this.checkAll = [];
    this.paramsPosition.size = size;
    this.tableConfig.pageIndex = 1;
    this.searchPosition();
  }

  searchPosition($event?: any) {
    this.paramsPosition.page = 0;
    if ($event) {
      this.paramsPosition.page = $event - 1;
    } else {
      this.checkAll = [];
    }
    this.isLoading = true;
    const sub = this.assignRoleService.getPositionRoleMapping(this.paramsPosition).subscribe((result) => {
        if (result.data && result.data.content) {
          result.data.content.forEach((el: Position) => {
            if (el.isCheck) {
              el.isCheck = +el.isCheck === 1;
            }
          })
          this.listPosition = result.data.content || [];
          if (this.checkAll[this.paramsPosition.page]) {
            this.listPosition.forEach(e => e.isCheck = true);
          } else {
            this.listPosition.forEach(e => {
              const item = this.objRolePosition.listPos.find(el => e.posId === el.posId);
              if (item) {
                e.isCheck = item.isCheck;
              }
            });
            this.checkAll[this.paramsPosition.page] = this.listPosition.length > 0 && !this.listPosition.some(e => e.isCheck === false);
          }
          this.tableConfig.total = result.data.totalElements;
          this.tableConfig.pageIndex = this.paramsPosition.page + 1;
        }
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }

  onCheckChange(event: boolean, id: number){
    const pos = this.objRolePosition.listPos.find(e => e.posId === id);
    if (!pos) {
      this.objRolePosition.listPos.push({posId: id, isCheck: event})
    } else {
      pos.isCheck = event;
    }
    const item = this.listPosition.find(e => e.posId === id);
    if (item) {
      item.isCheck = event;
    }
    this.checkAll[this.paramsPosition.page] = !this.listPosition.some(e => e.isCheck === false);
  }

  onCheckAll(event: boolean) {
    this.listPosition.forEach(el => {
      el.isCheck = event;
      const pos = this.objRolePosition.listPos.find(e => e.posId === el.posId);
      if (!pos) {
        this.objRolePosition.listPos.push({posId: el.posId, isCheck: event})
      } else {
        pos.isCheck = event;
      }
    })
    this.checkAll[this.paramsPosition.page] = event;
  }

  close() {
    this.modalRef.close();
  }

  savePosition() {
    this.isLoading = true;
    this.objRolePosition.roleId = this.roleId;
    const sub = this.assignRoleService.savePositionRoleMapping(this.objRolePosition).subscribe(() => {
        this.isLoading = false;
        this.toastrCustom.success(this.translate.instant('common.notification.success'));
        this.modalRef.close(true);
      },
      (e) => {
        this.toastrCustom.error(
          this.translate.instant('common.notification.error') + `: ${e?.message}`
        );
        this.isLoading = false;
      });
    this.subs.push(sub);
  }

  override ngOnDestroy() {
    this.subs.forEach(e => e.unsubscribe());
  }
}
