import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PositionsComponent } from './positions/positions.component';
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {NzFormModule} from "ng-zorro-antd/form";
import {TranslateModule} from "@ngx-translate/core";
import {FormsModule} from "@angular/forms";
import {SharedDirectivesTrimInputModule} from "@hcm-mfe/shared/directives/trim-input";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiMbTreeSelectModule} from "@hcm-mfe/shared/ui/mb-tree-select";
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";
import {NzModalModule} from "ng-zorro-antd/modal";
import {SharedUiMbTableMergeCellWrapModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell-wrap";
import {SharedUiMbTableMergeCellModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell";

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, NzFormModule, TranslateModule, FormsModule, SharedDirectivesTrimInputModule,
    SharedUiMbButtonModule, NzTableModule, NzPaginationModule, SharedUiMbSelectModule, SharedUiMbTreeSelectModule,
    NzCheckboxModule, NzModalModule, SharedUiMbTableMergeCellWrapModule, SharedUiMbTableMergeCellModule],
  declarations: [
    PositionsComponent
  ],
  exports: [PositionsComponent]
})
export class SystemUiPositionsModule {}
