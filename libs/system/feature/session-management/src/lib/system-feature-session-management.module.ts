import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SessionManagementComponent } from './session-management/session-management.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { NzInputModule } from 'ng-zorro-antd/input';
import { TranslateModule } from '@ngx-translate/core';
import { SharedDirectivesTrimInputModule } from '@hcm-mfe/shared/directives/trim-input';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { RouterModule } from '@angular/router';
import { NzFormModule } from 'ng-zorro-antd/form';
import {NzButtonModule} from "ng-zorro-antd/button";

@NgModule({
    imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedUiLoadingModule, NzInputModule, TranslateModule, NzFormModule,
        SharedDirectivesTrimInputModule, SharedUiMbButtonModule, NzTableModule, NzToolTipModule, NzIconModule, NzPaginationModule,
        RouterModule.forChild([
            {
                path: '',
                component: SessionManagementComponent
            }
        ]), NzButtonModule
    ],
  declarations: [SessionManagementComponent],
  exports: [SessionManagementComponent],
})
export class SystemFeatureSessionManagementModule {}
