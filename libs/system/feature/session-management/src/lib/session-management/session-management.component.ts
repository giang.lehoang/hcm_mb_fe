import { Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { IBaseResponse, IContent, ISession } from '@hcm-mfe/system/data-access/models';
import { userConfig } from '@hcm-mfe/shared/common/constants';
import { FunctionCode, maxInt32 } from '@hcm-mfe/shared/common/enums';
import { SessionManageService } from '@hcm-mfe/system/data-access/services';

@Component({
  selector: 'app-session-management',
  templateUrl: './session-management.component.html',
  styleUrls: ['./session-management.component.scss'],
})
export class SessionManagementComponent extends BaseComponent implements OnInit {
  isSearch = false;
  username = '';
  tempList: ISession[] = [];
  params = {
    page: 0,
    size: userConfig.pageSize,
  };
  paramSession = {
    first: 0,
    max: userConfig.pageSize,
  };
  paramsSearch = {
    page: 0,
    size: maxInt32,
  };
  paginationCustom = {
    size: 0,
    totalElements: 0,
    pageIndex: 0,
    numberOfElements: 0,
  };
  pageableSession = {
    size: 0,
    totalElements: 0,
    currentPage: 0,
    totalPages: 0,
  };
  listSession: ISession[] = [];
  constructor(
    injector: Injector,
    readonly sessionMngService: SessionManageService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ADMIN_SESSION_MANAGEMENT}`);
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.getListSession(this.params);
  }
  trackByFn(index: number, item: ISession) {
    return item.id;
  }
  getListSession(params: any): void {
    if (params.size === userConfig.pageSize) {
      this.sessionMngService.getListSession(params).subscribe(
        (data: IBaseResponse<IContent<ISession[]>>) => {
          this.listSession = data?.data?.content;
          this.paginationCustom.size = data?.data?.size;
          this.paginationCustom.totalElements = data?.data?.totalElements;
          this.paginationCustom.pageIndex = data?.data?.pageable.pageNumber;
          this.paginationCustom.numberOfElements = data?.data?.numberOfElements;
          this.isLoading = false;
        },
        (error) => {
          this.isLoading = false;
        }
      );
    } else {
      this.sessionMngService.getListSession(params).subscribe(
        (data: IBaseResponse<IContent<ISession[]>>) => {
          this.listSession = data.data.content;
          this.tempList = data.data.content;
          this.paramSession.first = 0;
          this.paramSession.max = userConfig.pageSize;
          const dataResult = this.tempList?.filter((item) => {
            return item?.username?.toLowerCase().includes(this.username.toLowerCase().trim()) || !this.username;
          });
          const totalSize = Math.floor(dataResult?.length);
          this.pageableSession = {
            totalElements: totalSize,
            totalPages: Math.floor(totalSize / userConfig.pageSize),
            currentPage: Math.floor(this.paramSession.first / userConfig.pageSize + 1),
            size: userConfig.pageSize,
          };
          this.listSession = dataResult || [];
          this.listSession = this.listSession.slice(this.paramSession.first, this.paramSession.max);
          this.isLoading = false;
        },
        (error) => {
          this.listSession = [];
          this.isLoading = false;
        }
      );
    }
  }
  searchSession(event: any): void {
    if (event.key === 'Enter' || event.type === 'click') {
      this.isLoading = true;
      if (!this.username) {
        this.isSearch = false;
        this.params.page = 0;
        this.getListSession(this.params);
      } else {
        this.isSearch = true;
        this.getListSession(this.paramsSearch);
      }
    }
  }
  changePage(value: any): void {
    this.params.page = value - 1;
    if (!this.username) {
      this.getListSession(this.params);
    } else {
      this.paramSession.first = (this.pageableSession?.currentPage - 1) * this.pageableSession.size;
      const dataResult = this.tempList?.filter((item) => {
        return item?.username?.toLowerCase().includes(this.username.toLowerCase().trim()) || !this.username;
      });
      const totalCount = dataResult?.length || 0;
      this.pageableSession = {
        totalElements: totalCount,
        totalPages: Math.floor(totalCount / userConfig.pageSize),
        currentPage: Math.floor(this.paramSession.first / userConfig.pageSize + 1),
        size: userConfig.pageSize,
      };
      this.listSession = dataResult.slice(this.paramSession.first, this.paramSession.max + this.paramSession.first);
    }
  }
  cancleSession(id: any): void {
    this.deletePopup.showModal(() => {
      this.sessionMngService.cancleSession(id).subscribe(
        (data) => {
          this.toastrCustom.success('Xóa thành công');
          this.username = '';
          this.params.page = 0;
          this.getListSession(this.params);
        },
        (error) => {
          this.toastrCustom.error('Xóa thất bại');
        }
      );
    });
  }
}
