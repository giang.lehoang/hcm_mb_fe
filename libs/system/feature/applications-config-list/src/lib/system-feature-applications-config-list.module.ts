import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { NzFormModule } from 'ng-zorro-antd/form';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { SharedDirectivesUppercaseInputModule } from '@hcm-mfe/shared/directives/uppercase-input';
import { AcListComponent } from './ac-list/ac-list.component';
import { NzInputModule } from 'ng-zorro-antd/input';
import { SharedDirectivesTrimInputModule } from '@hcm-mfe/shared/directives/trim-input';
import { NzTableModule } from 'ng-zorro-antd/table';
import { SharedUiMbButtonIconModule } from '@hcm-mfe/shared/ui/mb-button-icon';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { RouterModule } from '@angular/router';
@NgModule({
  imports: [CommonModule, SharedUiMbButtonModule, TranslateModule, SharedUiLoadingModule, NzFormModule, FormsModule,
    ReactiveFormsModule, SharedUiMbInputTextModule, SharedDirectivesUppercaseInputModule, NzInputModule,
    SharedDirectivesTrimInputModule, NzTableModule, SharedUiMbButtonIconModule, NzPaginationModule,
    RouterModule.forChild([
      {
        path: '',
        component: AcListComponent
      }
    ])
  ],
  declarations: [AcListComponent],
  exports: [AcListComponent],
})
export class SystemFeatureApplicationsConfigListModule {}
