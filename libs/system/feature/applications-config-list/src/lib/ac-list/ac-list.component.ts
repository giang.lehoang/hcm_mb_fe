import { Component, OnInit, Injector } from '@angular/core';
import { Subscription } from 'rxjs';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { userConfig } from '@hcm-mfe/shared/common/constants';
import { Pageable } from '@hcm-mfe/shared/data-access/models';
import { AppService } from '@hcm-mfe/system/data-access/services';
import { App } from '@hcm-mfe/system/data-access/models';
import { FunctionCode } from '@hcm-mfe/shared/common/enums';
import { ValidateService } from '@hcm-mfe/shared/core';

@Component({
  selector: 'app-ac-list',
  templateUrl: './ac-list.component.html',
  styleUrls: ['./ac-list.component.scss'],
})
export class AcListComponent extends BaseComponent implements OnInit {
  subs: Subscription[] = [];
  textFilter = '';
  listData: Array<App> = [];
  limit = userConfig.pageSize;
  paramSearch = {
    size: this.limit,
    page: 0,
    search: ''
  };
  pageable: Pageable = new Pageable();
  constructor(
    injector: Injector,
    private readonly appService: AppService,
    public validateService: ValidateService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ADMIN_APP}`);
  }

  ngOnInit(): void {
    this.search();
  }
  trackByFn(idx: number, item: App) {
    return item.appId;
  }
  search($event?: any) {
    this.paramSearch.page = 0;
    this.paramSearch.search =  this.textFilter ? this.textFilter.trim() : '';
    if ($event) {
      this.paramSearch.page = $event - 1;
    }
    this.isLoading = true;
    const sub = this.appService.searchAppCategory(this.paramSearch).subscribe(result => {
        if (result.data) {
          this.listData = result.data.content || [];
          this.pageable = {
            totalElements: result.data.totalElements,
            currentPage: result.data.number + 1,
            totalPages: result.data.totalPages,
            size: this.limit,
            numberOfElements: result.data.numberOfElements,
          };
        }
        this.isLoading = false;
      },() => {
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }

  create() {
    this.router.navigate([this.router.url, 'create']);
  }

  update(item: App | any) {
    this.isLoading = true;
    const sub = this.appService.getAppCategoryById(item.appId).subscribe(() => {
      this.isLoading = false;
      this.router.navigate([this.router.url, 'update'], { queryParams: { appId: item.appId } });
    }, (e) => {
      this.toastrCustom.error(
        this.translate.instant('common.notification.error') + `: ${e?.message}`
      );
      this.isLoading = false;
    });
    this.subs.push(sub);
  }

  delete(item: App | any) {
    this.deletePopup.showModal(() => {
      const subDelete = this.appService.deleteAppCategory(item.appId).subscribe(() => {
          this.toastrCustom.success(this.translate.instant('common.notification.deleteSuccess'));
          this.search();
          this.isLoading = false;
        }, (e) => {
          this.toastrCustom.error(
            this.translate.instant('common.notification.deleteError') + `: ${e?.message}`
          );
          this.isLoading = false;
        }
      );
      this.subs.push(subDelete);
    });
  }

  override ngOnDestroy() {
    this.deletePopup?.modalDelete?.destroy();
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
