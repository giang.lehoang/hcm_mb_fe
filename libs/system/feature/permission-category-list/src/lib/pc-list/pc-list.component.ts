import { Component, Injector, OnInit } from '@angular/core';
import { SelectionType } from '@swimlane/ngx-datatable';
import { forkJoin, Subscription } from 'rxjs';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { App, Permission, Resource, TreeNode } from '@hcm-mfe/system/data-access/models';
import {FunctionCode, maxInt32, SessionKey} from '@hcm-mfe/shared/common/enums';
import { AppService, FunctionService, PermissionService } from '@hcm-mfe/system/data-access/services';
import { CommonUtils } from '@hcm-mfe/shared/core';
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";

@Component({
  selector: 'app-permission-category-list',
  templateUrl: './pc-list.component.html',
  styleUrls: ['./pc-list.component.scss'],
})
export class PcListComponent extends BaseComponent implements OnInit {
  listApp: Array<App> = [];
  listData: Array<Permission> = [];
  listDataTemp: Array<Permission> = [];
  listResource: Array<Resource> = [];
  subs: Subscription[] = [];
  textSearch = '';
  SelectionType = SelectionType;
  idApp = '';
  listOfMap: TreeNode[] = [];
  mapOfExpanded: { [key: string]: TreeNode[] } = {};
  resourceId = '';
  params = {
    page: 0,
    size: maxInt32,
  };

  constructor(
    injector: Injector,
    private readonly permissionService: PermissionService,
    private readonly functionService: FunctionService,
    private readonly appService: AppService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ADMIN_PERMISSION}`);
  }

  collapses(array: TreeNode[], data: TreeNode, $event: boolean): void {
    CommonUtils.collapses(array, data, $event, true);
  }

  ngOnInit(): void {
    this.isLoading = true;
    const sub = forkJoin([
      this.functionService.searchResourceCategory(this.params),
      this.appService.searchAppCategory(this.params),
      this.permissionService.getPermission(),
    ]).subscribe(
      ([listResource, listApp, listPermission]) => {
        this.listApp =  CommonUtils.sort(listApp?.data?.content || [], 'name');
        listPermission = listPermission?.data || [];
        listPermission?.forEach((permission: any) => {
          permission.parentId = permission?.resources[0];
          if (permission.parentId) {
            const itemResource = listResource?.data?.content?.find((resource: any) => resource.id === permission.parentId);
            if (itemResource) {
              permission.appId = itemResource?.type;
              itemResource.appId = itemResource?.type;
              if (itemResource?.parentId) {
                delete itemResource.parentId;
              }
              this.listDataTemp.push(itemResource);
            }
          }
        });
        this.listDataTemp = CommonUtils.sort([...this.listDataTemp, ...listPermission], 'name');
        this.listDataTemp = [...new Set(this.listDataTemp)];
        this.listData = [...this.listDataTemp];
        this.listOfMap = CommonUtils.convertToTree(this.listData, '' ,['name']);
        CommonUtils.mapOfExpand(this.listOfMap, this.mapOfExpanded, true);
        this.isLoading = false;
      }, () => {
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }
  trackByFn(index: number, item: TreeNode) {
    return item.id;
  }
  filterPermission() {
    this.isLoading = true;
    const val = this.textSearch?.trim().toLowerCase();
    const appId = this.idApp?.trim().toLowerCase();
    // filter our data
    const result: Permission[] = [];
    const parents: Permission[] = [];
    if (appId && appId !== '') {
      result.push(...this.getListByApp(appId, val));
      result.forEach((item) => {
        if (item.id) {
          const listData = this.listDataTemp?.filter((i) => i.parentId === item.id);
          parents.push(...listData);
        }
        if (item.parentId) {
          const parent = this.getParent(item.parentId);
          if (parent) {
            parents.push(parent);
          }
        }
      });
    } else {
      result.push(...this.getList(val));
      result.forEach((item) => {
        if (item.parentId) {
          const parent = this.getParent(item.parentId);
          if (parent) {
            parents.push(parent);
          }
        }
      });
    }
    const resultAll = CommonUtils.sort([...result, ...parents], 'name');

    // update the rows
    this.listData = [...new Set<Permission>(resultAll.filter((el: any) => el != null))];
    this.listOfMap = [];
    this.mapOfExpanded = {};
    this.listOfMap = CommonUtils.convertToTree(this.listData, '', ['name']);
    CommonUtils.mapOfExpand(this.listOfMap, this.mapOfExpanded, true);
    this.isLoading = false;
  }

  getParent(parentId: string) {
    return this.listDataTemp?.find((i) => i.id === parentId);
  }

  getListByApp(appId: string, val: string) {
    return this.listDataTemp?.filter((item: any) => {
      return item.appId === appId && (item.name.toLowerCase().indexOf(val) !== -1 || !val);
    })
  }

  getList(val: string) {
    return this.listDataTemp?.filter((item: any) => {
      return item.name.toLowerCase().indexOf(val) !== -1 || !val;
    });
  }

  changeApp($event: any) {
    if(!Array.isArray($event.listOfSelected)) {
      this.filterPermission();
    }
  }

  create() {
    this.router.navigate([this.router.url, 'create'], {
      queryParams: { resourceId: this.resourceId, appId: this.idApp },
    });
  }

  edit(item: Permission | any) {
    this.isLoading = true;
    const sub = this.permissionService.getPermissionById(item.id, item.type).subscribe(() => {
      this.isLoading = false;
      this.router.navigate([this.router.url, 'update'], { queryParams: { id: item.id, type: item.type } });
    }, (e) => {
      this.toastrCustom.error(
        this.translate.instant('common.notification.error') + `: ${e?.message}`
      );
      this.isLoading = false;
    });
    this.subs.push(sub);
  }

  delete(item: any) {
    this.deletePopup.showModal(() => {
      this.isLoading = true;
      const subDelete = this.permissionService.deletePermission(item.id).subscribe(() => {
          this.toastrCustom.success(this.translate.instant('common.notification.deleteSuccess'));
          this.listData = this.listData.filter((itemOld) => {
            return item.id !== itemOld.id;
          });
          const listByParentId = this.listData.filter((itemOld) => itemOld.parentId === item.parentId);
          if (listByParentId.length === 0) {
            this.listData = this.listData.filter((itemOld) => {
              return item.parentId !== itemOld.id;
            });
          }
          this.listDataTemp = this.listDataTemp.filter((itemOld) => {
            return item.id !== itemOld.id;
          });
          const listTempByParentId = this.listDataTemp.filter((itemOld) => itemOld.parentId === item.parentId);
          if (listTempByParentId.length === 0) {
            this.listDataTemp = this.listDataTemp.filter((itemOld) => {
              return item.parentId !== itemOld.id;
            });
          }
          this.listData = [...new Set(this.listData)];
          this.listOfMap = [];
          this.mapOfExpanded = {};
          this.listOfMap = CommonUtils.convertToTree(this.listData, '', ['name']);
          CommonUtils.mapOfExpand(this.listOfMap, this.mapOfExpanded, true);
          this.isLoading = false;
        }, (e) => {
          this.toastrCustom.error(
            this.translate.instant('common.notification.deleteError') + `: ${e?.message}`
          );
          this.isLoading = false;
        }
      );
      this.subs.push(subDelete);
    });
  }

  generateAppName(type: string) {
    if (this.listApp && this.listApp.length > 0) {
      for (const app of this.listApp) {
        if (type === app.appId) {
          return app.appName;
        }
      }
    }
    return '';
  }

  selected(item: TreeNode | any) {
    if(item.type === 'resource' || item.type === 'scope') {
      this.resourceId = item.parentId;
    } else {
      this.resourceId = item.id;
    }
  }

  override ngOnDestroy() {
    this.deletePopup?.modalDelete?.destroy();
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
