import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PcListComponent } from './pc-list/pc-list.component';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { TranslateModule } from '@ngx-translate/core';
import { NzInputModule } from 'ng-zorro-antd/input';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { NzTableModule } from 'ng-zorro-antd/table';
import { SharedUiMbButtonIconModule } from '@hcm-mfe/shared/ui/mb-button-icon';
import { RouterModule } from '@angular/router';
import { NzFormModule } from 'ng-zorro-antd/form';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, SharedUiMbButtonModule, TranslateModule, NzInputModule,
    SharedUiMbSelectModule, NzTableModule, SharedUiMbButtonIconModule, NzFormModule, FormsModule, ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: PcListComponent
      }
    ])],
  declarations: [PcListComponent],
  exports: [PcListComponent],
})
export class SystemFeaturePermissionCategoryListModule {}
