import { Component, OnInit, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { CustomValidators } from '@hcm-mfe/shared/common/validators';
import { validateAllFormFields } from '@hcm-mfe/shared/common/utils';
import { NotiGroupService } from '@hcm-mfe/system/data-access/services';
import { NotiGroup } from '@hcm-mfe/system/data-access/models';

@Component({
  selector: 'hcm-mfe-noti-group-form',
  templateUrl: './noti-group-form.component.html',
  styleUrls: ['./noti-group-form.component.scss']
})
export class NotiGroupFormComponent extends BaseComponent implements OnInit {
  isSubmitted = false;
  data: NotiGroup = {};
  subs: Subscription[] = [];
  form = this.fb.group({
    ngrId: [''],
    ngrCode: ['', [CustomValidators.required, CustomValidators.code, Validators.maxLength(50)]],
    ngrName: ['', [CustomValidators.required, Validators.maxLength(100)]],
    flagStatus: [true],
    isEmail: [false],
    isNotify: [false],
    isSendSingle: [false],
    isSaveLogContent: [false],
    isFcm: [false],
  });

  id: string | undefined;

  constructor(
    injector: Injector,
    private readonly notiGroupService: NotiGroupService,
    private readonly activatedRoute: ActivatedRoute,
  ) {
    super(injector);
  }

  ngOnInit(): void {
    const subRoute = this.activatedRoute.queryParams.subscribe((params: any) => {
      this.id = params.id;
    });
    this.subs.push(subRoute);
    if (this.id) {
      this.isLoading = true;
      const sub = this.notiGroupService.getNotiGroupById(this.id.toString()).subscribe(
        (res) => {
          if (res.data) {
            this.data = res.data;
            const obj: NotiGroup = JSON.parse(JSON.stringify(this.data)) ?? {};
            if (res.data.flagStatus === 1) {
              obj.flagStatus = true;
            } else {
              obj.flagStatus = false;
            }
            if (res.data.isEmail === 'Y') {
              obj.isEmail = true;
            } else {
              obj.isEmail = false;
            }
            if (res.data.isNotify === 'Y') {
              obj.isNotify = true;
            } else {
              obj.isNotify = false;
            }
            if (res.data.isSendSingle === 'Y') {
              obj.isSendSingle = true;
            } else {
              obj.isSendSingle = false;
            }
            if (res.data.isSaveLogContent === 'Y') {
              obj.isSaveLogContent = true;
            } else {
              obj.isSaveLogContent = false;
            }
            if (res.data.isFcm === 'Y') {
              obj.isFcm = true;
            } else {
              obj.isFcm = false;
            }
            this.form.patchValue(obj);
          }
          this.isLoading = false;
        },
        (error) => {
          this.toastrCustom.error(error?.message);
          this.isLoading = false;
        }
      );
      this.subs.push(sub);
    }
  }

  codeBlur() {
    this.form.controls['ngrCode'].setValue(this.form.controls['ngrCode'].value.trim());
  }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      this.isLoading = true;
      const request: NotiGroup = JSON.parse(JSON.stringify(this.form.value)) ?? {};
      if (request.flagStatus) {
        request.flagStatus = 1;
      } else {
        request.flagStatus = 0;
      }
      if (request.isEmail) {
        request.isEmail = 'Y';
      } else {
        request.isEmail = 'N';
      }
      if (request.isNotify) {
        request.isNotify = 'Y';
      } else {
        request.isNotify = 'N';
      }
      if (request.isSendSingle) {
        request.isSendSingle = 'Y';
      } else {
        request.isSendSingle = 'N';
      }
      if (request.isSaveLogContent) {
        request.isSaveLogContent = 'Y';
      } else {
        request.isSaveLogContent = 'N';
      }
      if (request.isFcm) {
        request.isFcm = 'Y';
      } else {
        request.isFcm = 'N';
      }
      if (!this.id) {
        delete request.ngrId
        const subAdd = this.notiGroupService.createNotiGroup(request).subscribe(
          () => {
            this.isLoading = false;
            this.back();
            this.toastrCustom.success(this.translate.instant('common.notification.addSuccess'));
          },
          (error) => {
            this.toastrCustom.error(this.translate.instant('common.notification.addError') + `: ${error?.message}`);
            this.isLoading = false;
          }
        );
        this.subs.push(subAdd);
      } else {
        const subUpdate = this.notiGroupService.updateNotiGroup(request).subscribe(
          () => {
            this.isLoading = false;
            this.back();
            this.toastrCustom.success(this.translate.instant('common.notification.updateSuccess'));
          }, (error) => {
            this.toastrCustom.error(this.translate.instant('common.notification.updateError') + `: ${error?.message}`);
            this.isLoading = false;
          }
        );
        this.subs.push(subUpdate);
      }
    } else {
      validateAllFormFields(this.form);
    }
  }

  override ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
