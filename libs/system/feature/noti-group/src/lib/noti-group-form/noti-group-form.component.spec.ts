import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotiGroupFormComponent } from './noti-group-form.component';

describe('ReportGroupFormComponent', () => {
  let component: NotiGroupFormComponent;
  let fixture: ComponentFixture<NotiGroupFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NotiGroupFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotiGroupFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
