import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { NzFormModule } from 'ng-zorro-antd/form';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedDirectivesTrimInputModule } from '@hcm-mfe/shared/directives/trim-input';
import { NzInputModule } from 'ng-zorro-antd/input';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { SharedUiMbTableMergeCellWrapModule } from '@hcm-mfe/shared/ui/mb-table-merge-cell-wrap';
import { SharedUiMbTableMergeCellModule } from '@hcm-mfe/shared/ui/mb-table-merge-cell';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { RouterModule } from '@angular/router';
import { SharedDirectivesUppercaseInputModule } from '@hcm-mfe/shared/directives/uppercase-input';
import { SharedUiMbButtonIconModule } from '@hcm-mfe/shared/ui/mb-button-icon';
import { Scopes } from '@hcm-mfe/shared/common/enums';
import { SharedDirectivesNumbericModule } from '@hcm-mfe/shared/directives/numberic';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NotiGroupListComponent } from './noti-group-list/noti-group-list.component';
import { NotiGroupFormComponent } from './noti-group-form/noti-group-form.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        children: [
          {
            path: '',
            component: NotiGroupListComponent
          },
          {
            path: 'create',
            data: {
              scope: Scopes.CREATE,
              pageName: 'system.pageName.notifyGroupCreate',
              breadcrumb: 'system.breadcrumb.notifyGroupCreate'
            },
            component: NotiGroupFormComponent
          },
          {
            path: 'update',
            data: {
              scope: Scopes.EDIT,
              pageName: 'system.pageName.notifyGroupUpdate',
              breadcrumb: 'system.breadcrumb.notifyGroupUpdate'
            },
            component: NotiGroupFormComponent
          }
        ]
      }
    ]),
    CommonModule, SharedUiLoadingModule, NzFormModule, TranslateModule, FormsModule, SharedDirectivesTrimInputModule, NzInputModule,
    SharedUiMbButtonModule, SharedUiMbTableMergeCellWrapModule, SharedUiMbTableMergeCellModule, NzIconModule, NzButtonModule, NzDropDownModule,
    ReactiveFormsModule, SharedUiMbInputTextModule, SharedDirectivesUppercaseInputModule, SharedUiMbButtonIconModule, SharedDirectivesNumbericModule, NzSwitchModule, NzTagModule],
  declarations: [NotiGroupListComponent, NotiGroupFormComponent]
})
export class SystemFeatureNotiGroupModule {
}
