import { Component, OnInit, Injector, ViewChild, TemplateRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { userConfig } from '@hcm-mfe/shared/common/constants';
import { Pageable, TableConfig } from '@hcm-mfe/shared/data-access/models';
import { ValidateService } from '@hcm-mfe/shared/core';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { NotiGroupService } from '@hcm-mfe/system/data-access/services';
import { NotiGroup } from '@hcm-mfe/system/data-access/models';
import { FunctionCode } from '@hcm-mfe/shared/common/enums';

export const NOTY_GROUP_STATUS = [
  {label: 'system.notifyGroup.flagStatusUse', value: 1, bgColor: '#DAF9EC', color: '#06A561'},
  {label: 'system.notifyGroup.flagStatusNotUse', value: 0, bgColor:'#FDE7EA', color: '#FA0B0B'}
];

@Component({
  selector: 'hcm-mfe-noti-group-list',
  templateUrl: './noti-group-list.component.html',
  styleUrls: ['./noti-group-list.component.scss']
})
export class NotiGroupListComponent extends BaseComponent implements OnInit {
  subs: Subscription[] = [];
  textFilter = '';
  listData: Array<NotiGroup> = [];
  limit = userConfig.pageSize;
  constStatus = NOTY_GROUP_STATUS;
  paramSearch = {
    size: this.limit,
    page: 0,
    search: ''
  };
  tableConfig!: TableConfig;
  @ViewChild('actionTmpl', { static: true }) action!: TemplateRef<NzSafeAny>;
  @ViewChild('statusTmpl', { static: true }) status!: TemplateRef<NzSafeAny>;
  pageable: Pageable = new Pageable();
  modalRef: NzModalRef | undefined;

  constructor(
    injector: Injector,
    private readonly notiGroupService: NotiGroupService,
    public validateService: ValidateService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_NOTIFY_GROUP`);
  }

  ngOnInit(): void {
    this.initTable();
    this.search(1);
  }

  initTable() {
    this.tableConfig = {
      headers: [
        {
          title: 'STT',
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          fixed: true,
          fixedDir: 'left',
          width: 30
        },
        {
          title: 'system.notifyGroup.ngrCode',
          field: 'ngrCode', width: 200,
        },
        {
          title: 'system.notifyGroup.ngrName',
          needEllipsis: true,
          field: 'ngrName', width: 350,
        },
        {
          title: 'system.notifyGroup.flagStatus',
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          field: 'flagStatus', width: 100,
          tdTemplate: this.status
        },
        {
          title: ' ',
          field: '',
          tdTemplate: this.action,
          tdClassList: ['text-center'],
          fixed: true,
          fixedDir: 'right',
          width: 70,
        },
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.paramSearch.size,
      pageIndex: 1,

    };
  }

  search(page: number) {
    this.paramSearch.search =  this.textFilter ? this.textFilter.trim() : '';
    this.paramSearch.page = page - 1;
    this.isLoading = true;
    const sub = this.notiGroupService.searchNotiGroup(this.paramSearch).subscribe(result => {
        if (result.data) {
          this.listData = result.data.content || [];
          this.tableConfig.total = result.data.totalElements;
          this.tableConfig.pageIndex = this.paramSearch.page + 1;
        }
        this.isLoading = false;
      },(err) => {
        this.isLoading = false;
        this.toastrCustom.error(err?.message);
      }
    );
    this.subs.push(sub);
  }

  update(id: number) {
    this.isLoading = true;
    const sub = this.notiGroupService.getNotiGroupById(id.toString()).subscribe(() => {
      this.isLoading = false;
      this.router.navigate([this.router.url, 'update'], { queryParams: { id: id } });
    }, (e) => {
      this.toastrCustom.error(
        this.translate.instant('common.notification.error') + `: ${e?.message}`
      );
      this.isLoading = false;
    });
    this.subs.push(sub);
  }

  delete(id: number) {
    this.deletePopup.showModal(() => {
      const subDelete = this.notiGroupService.deleteNotiGroup(id.toString()).subscribe(() => {
          this.toastrCustom.success(this.translate.instant('common.notification.deleteSuccess'));
          this.search(this.tableConfig.pageIndex ?? 1);
          this.isLoading = false;
        }, (e) => {
          this.toastrCustom.error(
            this.translate.instant('common.notification.deleteError') + `: ${e?.message}`
          );
          this.isLoading = false;
        }
      );
      this.subs.push(subDelete);
    });
  }

  create() {
    this.router.navigate([this.router.url, 'create']);
  }

  override ngOnDestroy() {
    this.deletePopup?.modalDelete?.destroy();
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
