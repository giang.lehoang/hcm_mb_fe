import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotiGroupListComponent } from './noti-group-list.component';

describe('ReportGroupListComponent', () => {
  let component: NotiGroupListComponent;
  let fixture: ComponentFixture<NotiGroupListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NotiGroupListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotiGroupListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
