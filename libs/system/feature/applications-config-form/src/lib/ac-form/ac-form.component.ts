import { Component, OnInit, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { App } from '@hcm-mfe/system/data-access/models';
import { CustomValidators } from '@hcm-mfe/shared/common/validators';
import { AppService } from '@hcm-mfe/system/data-access/services';
import { validateAllFormFields } from '@hcm-mfe/shared/common/utils';
import { NzUploadFile } from 'ng-zorro-antd/upload';

@Component({
  selector: 'app-config-form',
  templateUrl: './ac-form.component.html',
  styleUrls: ['./ac-form.component.scss'],
})
export class AcFormComponent extends BaseComponent implements OnInit {
  isSubmitted = false;
  data: App | undefined;
  subs: Subscription[] = [];
  form = this.fb.group({
    appId: [''],
    appCode: ['', [CustomValidators.required, CustomValidators.code, Validators.maxLength(50)]],
    appName: ['', [CustomValidators.required, Validators.maxLength(150)]],
    description: ['', Validators.maxLength(255)],
  });
  appId: string | undefined;

  constructor(
    injector: Injector,
    private readonly appService: AppService,
    private readonly activatedRoute: ActivatedRoute,
  ) {
    super(injector);
  }

  ngOnInit(): void {
    const subRoute = this.activatedRoute.queryParams.subscribe((params: any) => {
      this.appId = params.appId;
    });
    this.subs.push(subRoute);
    if (this.appId) {
      this.isLoading = true;
      const sub = this.appService.getAppCategoryById(this.appId).subscribe(
        (res) => {
          this.data = res.data;
          this.form.patchValue(this.data ?? {});
          this.isLoading = false;
        },
        () => {
          this.isLoading = false;
        }
      );
      this.subs.push(sub);
    }
  }

  codeBlur() {
    this.form.controls['appCode'].setValue(this.form.controls['appCode'].value.trim());
  }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      this.isLoading = true;
      const request: App = this.form.value;
      if (!this.appId) {
        const subAdd = this.appService.createAppCategory(request).subscribe(
          () => {
            this.isLoading = false;
            this.back();
            this.toastrCustom.success(this.translate.instant('common.notification.addSuccess'));
          },
          (error) => {
            this.toastrCustom.error(this.translate.instant('common.notification.addError') + `: ${error?.message}`);
            this.isLoading = false;
          }
        );
        this.subs.push(subAdd);
      } else {
        const subUpdate = this.appService.updateAppCategory(request).subscribe(
          () => {
            this.isLoading = false;
            this.back();
            this.toastrCustom.success(this.translate.instant('common.notification.updateSuccess'));
          }, (error) => {
            this.toastrCustom.error(this.translate.instant('common.notification.updateError') + `: ${error?.message}`);
            this.isLoading = false;
          }
        );
        this.subs.push(subUpdate);
      }
    } else {
      validateAllFormFields(this.form);
    }
  }

  override ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
