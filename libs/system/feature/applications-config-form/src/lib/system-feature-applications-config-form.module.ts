import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { NzFormModule } from 'ng-zorro-antd/form';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { SharedDirectivesUppercaseInputModule } from '@hcm-mfe/shared/directives/uppercase-input';
import { NzInputModule } from 'ng-zorro-antd/input';
import { SharedDirectivesTrimInputModule } from '@hcm-mfe/shared/directives/trim-input';
import { RouterModule } from '@angular/router';
import { AcFormComponent } from './ac-form/ac-form.component';

@NgModule({
  imports: [CommonModule, SharedUiMbButtonModule, TranslateModule, SharedUiLoadingModule, NzFormModule, FormsModule,
    ReactiveFormsModule, SharedUiMbInputTextModule, SharedDirectivesUppercaseInputModule, NzInputModule, SharedDirectivesTrimInputModule,
    RouterModule.forChild([
      {
        path: '',
        component: AcFormComponent
      }
    ])
  ],
  declarations: [AcFormComponent],
  exports: [AcFormComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SystemFeatureApplicationsConfigFormModule {}
