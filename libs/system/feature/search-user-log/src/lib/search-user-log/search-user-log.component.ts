import { Component, Injector, OnInit } from '@angular/core';
import * as moment from 'moment';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { FormGroup, Validators } from '@angular/forms';
import { userConfig } from '@hcm-mfe/shared/common/constants';
import { IBaseResponse, IContent, IScope, IUserLog } from '@hcm-mfe/system/data-access/models';
import { UserLogService } from '@hcm-mfe/system/data-access/services';
import { FunctionCode } from '@hcm-mfe/shared/common/enums';
import { DateValidator } from '@hcm-mfe/shared/common/validators';
import { ModalListComponent } from '@hcm-mfe/system/ui/modal-list';
@Component({
  selector: 'app-search-user-log',
  templateUrl: './search-user-log.component.html',
  styleUrls: ['./search-user-log.component.scss'],
})
export class SearchUserLogComponent extends BaseComponent implements OnInit {
  isSubmitted = false;
  searchForm: FormGroup = this.fb.group(
    {
      fromDate: [null, [Validators.required]],
      toDate: [null, [Validators.required]],
      userName: [null, [Validators.required, Validators.maxLength(255)]],
      scopeCodeList: '',
      appName: '',
      resourceName: '',
      message: '',
    },
    {
      validators: [DateValidator.validateRangeDate('fromDate', 'toDate', 'rangeDateError')],
    }
  );
  params = {
    fromDate: '',
    toDate: '',
    userName: 'hcmadmin',
    scopeCodeList: '',
    appName: '',
    resourceName: '',
    message: '',
    sort: 'dateTime,desc',
    page: 0,
    size: userConfig.pageSize,
  };
  paginationCustom = {
    size: 0,
    total: 0,
    pageIndex: 0,
    numberOfElements: 0,
  };
  listScopes: IScope[] = [];
  listUserLog: IUserLog[] = [];
  constructor(injector: Injector, readonly userLogService: UserLogService) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ADMIN_SEARCH_USER_LOG}`);
  }

  ngOnInit(): void {
    this.userLogService.getScope().subscribe((data: IBaseResponse<IContent<IScope[]>>) => {
      this.listScopes = data.data.content;
    });
  }
  trackByFn(index: number, item: IUserLog) {
    return item.userId;
  }

  getListUserLog(): void {
    this.userLogService.searchUserLog(this.params).subscribe(
      (data: IBaseResponse<IContent<IUserLog[]>>) => {
        this.listUserLog = data.data.content;
        this.paginationCustom.size = data.data.size;
        this.paginationCustom.total = data.data.totalElements;
        this.paginationCustom.pageIndex = data.data.pageable.pageNumber;
        this.paginationCustom.numberOfElements = data.data.numberOfElements;
        this.isLoading = false;
      },
      (error) => {
        this.isLoading = false;
      }
    );
  }

  override triggerSearchEvent(): void {
    this.searchUserLog();
  }

  searchUserLog($event?: any): void {
    this.isSubmitted = true;
    if (this.searchForm.valid) {
      this.isLoading = true;
      this.params.page = 0;
      if ($event) {
        this.params.page = $event - 1;
      }
      if (this.searchForm.value.scopeCodeList === null) {
        this.searchForm.value.scopeCodeList = '';
      }
      this.params = {
        ...this.searchForm.value,
        fromDate: moment(this.searchForm.value.fromDate).format('DD/MM/yyyy HH:mm:ss'),
        toDate: moment(this.searchForm.value.toDate).format('DD/MM/yyyy HH:mm:ss'),
        sort: 'dateTime,desc',
        page: 0,
        size: userConfig.pageSize,
      };
      this.getListUserLog();
    }
  }
  showListUserModal(): void {
    const modal = this.modal.create({
      nzWidth: this.getModalWidth(),
      nzTitle: 'Danh sách người dùng',
      nzContent: ModalListComponent,
      nzFooter: null,
    });
    modal.afterClose.subscribe((result) => {
      if (result?.data) {
        this.searchForm.get('userName')?.setValue(result?.data);
      }
    });
  }
  changePage(value: any): void {
    this.params.page = value - 1;
    this.getListUserLog();
  }

  getModalWidth(): number {
    if(window.innerWidth > 767) {
      return window.innerWidth / 1.5 > 1368 ? 1368 : window.innerWidth / 1.3
    }
    return window.innerWidth;
  }
}
