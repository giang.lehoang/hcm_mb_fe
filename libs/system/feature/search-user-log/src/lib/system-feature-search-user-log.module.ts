import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchUserLogComponent } from './search-user-log/search-user-log.component';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { NzFormModule } from 'ng-zorro-antd/form';
import { SharedUiMbDatePickerModule } from '@hcm-mfe/shared/ui/mb-date-picker';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedUiLoadingModule, NzFormModule, SharedUiMbDatePickerModule, TranslateModule,
    SharedUiMbInputTextModule, SharedUiMbSelectModule, SharedUiMbButtonModule, NzTableModule, NzPaginationModule,
    RouterModule.forChild([
      {
        path: '',
        component: SearchUserLogComponent
      }
    ])
  ],
  declarations: [SearchUserLogComponent],
  exports: [SearchUserLogComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SystemFeatureSearchUserLogModule {}
