import { Component, OnInit, Injector } from '@angular/core';
import { Subscription } from 'rxjs';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { Scope } from '@hcm-mfe/system/data-access/models';
import { userConfig } from '@hcm-mfe/shared/common/constants';
import { Pageable } from '@hcm-mfe/shared/data-access/models';
import { ManipulationService } from '@hcm-mfe/system/data-access/services';
import { ValidateService } from '@hcm-mfe/shared/core';
import { FunctionCode } from '@hcm-mfe/shared/common/enums';
import { KeycloakService } from 'keycloak-angular';

@Component({
  selector: 'manipulation-category-list',
  templateUrl: './mc-list.component.html',
  styleUrls: ['./mc-list.component.scss'],
})
export class McListComponent extends BaseComponent implements OnInit {
  subs: Subscription[] = [];
  listData: Array<Scope> = [];
  limit = userConfig.pageSize;
  paramSearch = {
    size: this.limit,
    page: 0,
    search: '',
  };
  pageable: Pageable = new Pageable();

  constructor(
    injector: Injector,
    private readonly manipulationService: ManipulationService,
    public validateService: ValidateService,
    public keycloakService: KeycloakService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ADMIN_SCOPES}`);
  }

  ngOnInit(): void {
    this.search(true);
  }
  trackByFn(index: number, item: Scope) {
    return item.id;
  }
  search($event?: any) {
    this.paramSearch.page = 0;
    this.paramSearch.search = this.paramSearch?.search?.trim();
    if ($event) {
      this.paramSearch.page = $event - 1;
    }
    this.isLoading = true;
    const sub = this.manipulationService.searchScope(this.paramSearch).subscribe(
      (result) => {
        this.listData = result?.data?.content || [];
        this.pageable = {
          totalElements: result?.data?.page?.totalElements,
          totalPages: result?.data?.page?.totalPages,
          currentPage: result?.data?.page?.number + 1,
          size: this.limit,
          numberOfElements: result.data.content.length,
        };
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }

  create() {
    this.router.navigate([this.router.url, 'create']);
  }

  update(item: Scope | any) {
    this.isLoading = true;
    const sub = this.manipulationService.getScopeById(item.id).subscribe(() => {
      this.isLoading = false;
      this.router.navigate([this.router.url, 'update'], { queryParams: { id: item.id } });
    }, (e) => {
      this.toastrCustom.error(
        this.translate.instant('common.notification.error') + `: ${e?.message}`
      );
      this.isLoading = false;
    });
    this.subs.push(sub);
  }

  delete(item: Scope | any) {
    this.deletePopup.showModal(() => {
      const subDelete = this.manipulationService.deleteScopeById(item.id).subscribe(() => {
          this.toastrCustom.success(this.translate.instant('common.notification.deleteSuccess'));
          this.search();
          this.isLoading = false;
        }, (e) => {
          this.toastrCustom.error(this.translate.instant('common.notification.deleteError') + `: ${e?.message}`);
          this.isLoading = false;
        }
      );
      this.subs.push(subDelete);
    });
  }

  override ngOnDestroy() {
    this.deletePopup?.modalDelete?.destroy();
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
