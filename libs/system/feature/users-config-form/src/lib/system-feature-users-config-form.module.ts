import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UcFormComponent } from './uc-form/uc-form.component';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NzInputModule } from 'ng-zorro-antd/input';
import { SharedDirectivesTrimInputModule } from '@hcm-mfe/shared/directives/trim-input';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, NzFormModule, NzButtonModule, TranslateModule, SharedUiMbInputTextModule,
    NzSwitchModule, NzInputModule, SharedDirectivesTrimInputModule, SharedUiMbButtonModule, NzToolTipModule, NzTableModule,
    NzCheckboxModule, NzPaginationModule, FormsModule, ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: UcFormComponent
      }
    ])
  ],
  declarations: [UcFormComponent],
  exports: [UcFormComponent],
})
export class SystemFeatureUsersConfigFormModule {}
