import { Component, OnInit, Injector } from '@angular/core';
import { forkJoin, Subscription } from 'rxjs';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { Role, User } from '@hcm-mfe/system/data-access/models';
import { Pageable } from '@hcm-mfe/shared/data-access/models';
import { userConfig } from '@hcm-mfe/shared/common/constants';
import { RoleService, UserService } from '@hcm-mfe/system/data-access/services';
import { ValidateService } from '@hcm-mfe/shared/core';
import { FunctionCode, maxInt32, rolesDefault } from '@hcm-mfe/shared/common/enums';

@Component({
  selector: 'user-config-form',
  templateUrl: './uc-form.component.html',
  styleUrls: ['./uc-form.component.scss'],
})
export class UcFormComponent extends BaseComponent implements OnInit {
  subs: Subscription[] = [];
  isUpdate = false;
  isSubmitted = false;
  data: User | any;
  form = this.fb.group({
    id: [''],
    username: [''],
    fullName: [''],
    branch: [''],
    email: [''],
    phoneNumber: [''],
    flagStatus: [null],
    isLocked: [null],
    hrsCode: [''],
    reasonLock: [''],
    orgName: [''],
  });
  pageable: Pageable = new Pageable();
  dataRole: Array<Role> = [];
  listRole: Array<Role> = [];
  role: Role | any;
  params = {
    first: 0,
    max: 10,
    search: '',
  };
  pageSize = userConfig.pageSize;
  isList = false;
  roleSelected: Array<Role> = [];
  id: string | any;

  constructor(
    injector: Injector,
    private readonly roleService: RoleService,
    private readonly userService: UserService,
    public validateService: ValidateService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ADMIN_USER}`);
  }

  ngOnInit(): void {
    this.isLoading = true;
    const subRoute = this.route.queryParams.subscribe((params: any) => {
      this.id = params.id;
    });
    this.subs.push(subRoute);
    this.form.disable();
    if (this.id) {
      const sub = forkJoin([
        this.userService.findById(this.id),
        this.userService.getRolesByUser(this.id)
      ]).subscribe(
        ([itemUser, roles]) => {
          this.roleSelected = roles.data || [];
          if(itemUser.data) {
            if(itemUser.data.isTracing === 'Y') {
              itemUser.data.isTracing = true;
            } else {
              itemUser.data.isTracing = false;
            }
            if(itemUser.data.isLocked === 'Y') {
              itemUser.data.isLocked = false;
            } else {
              itemUser.data.isLocked = true;
            }
          }
          this.data = itemUser.data;
          this.form.patchValue(this.data ?? {});
          this.getListRole();
          this.isLoading = false;
        },
        () => {
          this.isLoading = false;
        }
      );
      this.subs.push(sub);
      if (!this.isUpdate) {
        this.form.disable();
      }
    }
  }

  getListRole() {
    const param = {
      page: 0,
      size: maxInt32,
    };
    const subRole = this.roleService.searchRole(param).subscribe(listRoles => {
      this.dataRole =
      listRoles?.data?.content?.filter((role: any) => {
        return rolesDefault.indexOf(role.name) === -1;
      }) || [];
      this.searchRole(undefined);
    });
    this.subs.push(subRole);
  }

  searchRole($event: any) {
    this.isLoading = true;
    this.params.search = this.params.search.trim();
    if (!$event) {
      this.params.first = 0;
      this.params.max = this.pageSize;
      this.params.search = this.params.search.trim();
    } else {
      this.params.first = ($event - 1) * this.pageable.size;
    }
    const listAll = this.dataRole.filter(
      (item) =>
        item?.name?.toLowerCase().includes(this.params.search?.toLowerCase()) ||
        item?.description?.toLowerCase().includes(this.params.search?.toLowerCase()) ||
        !this.params.search
    );
    listAll.sort(this.sortBy());
    const totalCount = listAll.length || 0;
    this.listRole = listAll.slice(this.params.first, this.params.first + this.params.max) || [];
    this.pageable = {
      totalElements: totalCount,
      totalPages: Math.floor(totalCount / this.pageSize),
      currentPage: Math.floor(this.params.first / this.pageSize + 1),
      numberOfElements: Math.floor(this.listRole.length),
      size: this.pageSize,
    };
    this.isLoading = false;
  }

  sortBy() {
    return (a: any, b: any) => {
      if (this.isChecked(a)) {
        if (this.isChecked(a) && this.isChecked(b)) {
          if (a.name?.toLowerCase() > b.name?.toLowerCase()) {
            return 1;
          }
        }
        return -1;
      } else if (a.name?.toLowerCase() > b.name?.toLowerCase()) {
        return 1;
      }
      return 1;
    };
  }

  onCheckboxRoleFn(row: any, isChecked: boolean) {
    if (isChecked) {
      const data: Role = {
        // clientRole: row.clientRole,
        // composite: row.composite,
        // containerId: row.containerId,
        // description: row.description,
        id: row.id,
        name: row.name,
      };
      this.roleSelected.push(data);
    } else {
      this.roleSelected.splice(this.roleSelected.findIndex(item => item.id === row.id), 1);
    }
    this.role = row;
  }

  doSave() {
    this.isLoading = true;
    const subUpdateRole = this.userService.updateRolesByUser(this.data.userId, this.roleSelected).subscribe(
      () => {
        this.isLoading = false;
        this.toastrCustom.success(this.translate.instant('common.notification.success'));
      },
      (e) => {
        this.toastrCustom.error(
          this.translate.instant('common.notification.error') + `: ${e?.message}`
        );
        this.roleSelected.splice(this.roleSelected.findIndex(item => item.id === this.role.id), 1);
        this.getListRole();
        this.isLoading = false;
      }
    );
    this.subs.push(subUpdateRole);
  }

  isChecked(item: any) {
    return (
      this.roleSelected?.findIndex((role) => {
        return role.id === item.id;
      }) > -1
    );
  }

  chooseTab() {
    this.isList = !this.isList;
  }

  trackByFn(index:number, item: Role) {
    return item.id;
  }

  override ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
