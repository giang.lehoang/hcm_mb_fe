import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RcFormComponent } from './rc-form/rc-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { TranslateModule } from '@ngx-translate/core';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzFormModule } from 'ng-zorro-antd/form';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { RouterModule } from '@angular/router';
import { SharedDirectivesUppercaseInputModule } from '@hcm-mfe/shared/directives/uppercase-input';
import {NzInputModule} from "ng-zorro-antd/input";
import {SharedDirectivesTrimInputModule} from "@hcm-mfe/shared/directives/trim-input";

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedUiLoadingModule, TranslateModule,
    NzButtonModule, NzFormModule, SharedUiMbInputTextModule, SharedUiMbButtonModule, NzToolTipModule,
    NzTableModule, NzPaginationModule, NzCheckboxModule,
    RouterModule.forChild([
      {
        path: '',
        component: RcFormComponent
      }
    ]), SharedDirectivesUppercaseInputModule, NzInputModule, SharedDirectivesTrimInputModule
  ],
  declarations: [RcFormComponent],
  exports: [RcFormComponent],
})
export class SystemFeatureRoleCategoryFormModule {}
