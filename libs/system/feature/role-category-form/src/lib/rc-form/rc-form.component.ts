import { Component, OnInit, Injector } from '@angular/core';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { forkJoin, Subscription } from 'rxjs';
import { Role, User } from '@hcm-mfe/system/data-access/models';
import { CustomValidators } from '@hcm-mfe/shared/common/validators';
import { Validators } from '@angular/forms';
import { Pageable } from '@hcm-mfe/shared/data-access/models';
import { userConfig } from '@hcm-mfe/shared/common/constants';
import { RoleService, UserService } from '@hcm-mfe/system/data-access/services';
import { ValidateService } from '@hcm-mfe/shared/core';
import { FunctionCode } from '@hcm-mfe/shared/common/enums';
import { validateAllFormFields } from '@hcm-mfe/shared/common/utils';

@Component({
  selector: 'role-category-form',
  templateUrl: './rc-form.component.html',
  styleUrls: ['./rc-form.component.scss'],
})
export class RcFormComponent extends BaseComponent implements OnInit {
  subs: Subscription[] = [];
  isUpdate = false;
  isSubmitted = false;
  data: Role | undefined;
  listUser: Array<User> = [];
  usersOfRole: Array<string> = [];
  form = this.fb.group({
    id: [''],
    code: ['', [CustomValidators.required, CustomValidators.code, Validators.maxLength(50)]],
    name: ['', [CustomValidators.required, Validators.maxLength(150)]],
    description: ['', Validators.maxLength(150)],
  });
  pageable: Pageable = new Pageable();
  limit = userConfig.pageSize;
  paramUserSearch = {
    size: this.limit,
    page: 0,
    search: ''
  };
  textFilter = '';
  isList = false;
  listRoleOfUser: Array<Role> = [];
  id: string | undefined;
  constructor(
    injector: Injector,
    private readonly roleService: RoleService,
    private readonly userService: UserService,
    public validateService: ValidateService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ADMIN_ROLE}`);
  }

  ngOnInit(): void {
    const subRoute = this.route.queryParams.subscribe((params: any) => {
      this.id = params.id;
    });
    this.subs.push(subRoute);
    if (this.id) {
      this.isLoading = true;
      this.isUpdate = true;
      const sub = forkJoin([
        this.roleService.getRoleById(this.id),
        this.roleService.getListUserByRoleId(this.id, this.paramUserSearch),
      ]).subscribe(
        ([itemRole, listUser]) => {
          this.data = itemRole.data;
          if (this.data) {
            this.form.controls['id'].setValue(this.data.id);
            this.form.controls['code'].setValue(this.data.name);
            this.form.controls['name'].setValue(this.data.description);
            if (this.data.attributes?.description) {
              this.form.controls['description'].setValue(this.data.attributes?.description[0]);
            }
          }
          this.listUser = listUser.data.content;
          this.pageable.totalElements = listUser.data.totalElements;
          this.pageable.totalPages = listUser.data.totalPages;
          this.pageable.currentPage = listUser.data.number + 1;
          this.pageable.size = listUser.data.size;

          this.isLoading = false;
        },
        () => {
          this.isLoading = false;
        }
      );
      this.subs.push(sub);
    }
  }
  trackByFn(index: number, item: User) {
    return item.userId;
  }
  search($event?: any) {
    if (this.id) {
      this.paramUserSearch.page = 0;
      if ($event) {
        this.paramUserSearch.page = $event - 1;
      }
      this.isLoading = true;
      this.paramUserSearch.search = this.textFilter.trim();
      const sub = this.roleService.getListUserByRoleId(this.id, this.paramUserSearch).subscribe((result) => {
          if (result.data) {
            this.listUser = result.data.content || [];
            this.pageable.totalElements = result.data.totalElements;
            this.pageable.totalPages = result.data.totalPages;
            this.pageable.currentPage = result.data.number + 1;
            this.pageable.size = result.data.size;
          }
          this.isLoading = false;
        },
        () => {
          this.isLoading = false;
        }
      );
      this.subs.push(sub);
    }
  }

  onCheckboxFn(item: any, isChecked: boolean) {
    this.isLoading = true;
    const sub = this.userService.getRolesByUser(item.userId).subscribe((roles) => {
        this.listRoleOfUser = roles.data || [];
        if (isChecked) {
          const data: Role = {
            clientRole: this.data?.clientRole,
            composite: this.data?.composite,
            containerId: this.data?.containerId,
            description: this.data?.description,
            id: this.data?.id,
            name: this.data?.name,
          };
          this.listRoleOfUser.push(data);
        } else {
          this.listRoleOfUser.splice(this.listRoleOfUser.findIndex(el => el.id === this.data?.id), 1);
        }
        this.userService.updateRolesByUser(item.userId, this.listRoleOfUser).subscribe(() => {
            this.isLoading = false;
            this.toastrCustom.success(this.translate.instant('common.notification.success'));
            if (isChecked) {
              this.usersOfRole.push(item.userId);
            } else {
              this.usersOfRole.splice(this.usersOfRole.indexOf(item.userId), 1);
            }
          }, () => {
            this.isLoading = false;
            this.toastrCustom.error(this.translate.instant('common.notification.error'));
          }
        );
      }, () => {
        this.toastrCustom.error(this.translate.instant('common.notification.error'));
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }

  isChecked(isUserInRole: string): boolean {
    return isUserInRole === 'Y';
  }

  chooseTab() {
    this.isList = !this.isList;
  }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      this.isLoading = true;
      const request: Role = this.form.value;
      if (!this.id) {
        const subAdd = this.roleService.createRole(request).subscribe(() => {
            this.isLoading = false;
            this.back();
            this.toastrCustom.success(this.translate.instant('common.notification.addSuccess'));
          }, (e) => {
            this.toastrCustom.error(this.translate.instant('common.notification.addError') + `: ${e?.message}`);
            this.isLoading = false;
          }
        );
        this.subs.push(subAdd);
      } else {
        const subUpdate = this.roleService.updateRole(request).subscribe(() => {
            this.isLoading = false;
            this.back();
            this.toastrCustom.success(this.translate.instant('common.notification.updateSuccess'));
          }, (e) => {
            this.toastrCustom.error(
              this.translate.instant('common.notification.updateError') + `: ${e?.message}`
            );
            this.isLoading = false;
          }
        );
        this.subs.push(subUpdate);
      }
    } else {
      validateAllFormFields(this.form);
    }
  }

  codeBlur() {
    this.form.controls['code'].setValue(this.form.controls['code'].value.trim())
  }

  override ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
