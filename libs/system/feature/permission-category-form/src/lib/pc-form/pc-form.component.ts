import { Component, OnInit, Injector } from '@angular/core';
import { forkJoin, Subscription } from 'rxjs';
import { Validators } from '@angular/forms';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import {
  Permission,
  Policy,
  Resource,
  ResourceType,
  Role,
  Scope,
  SelectCheckAbleModal
} from '@hcm-mfe/system/data-access/models';
import { CustomValidators } from '@hcm-mfe/shared/common/validators';
import { maxInt32 } from '@hcm-mfe/shared/common/enums';
import {
  FunctionService,
  ManipulationService,
  PermissionService,
  RoleService
} from '@hcm-mfe/system/data-access/services';
import { CommonUtils } from '@hcm-mfe/shared/core';
import { cleanDataForm, Utils, validateAllFormFields } from '@hcm-mfe/shared/common/utils';
type popType = 'id' | 'type'| 'resourceId' | 'appId';
@Component({
  selector: 'permission-category-form',
  templateUrl: './pc-form.component.html',
  styleUrls: ['./pc-form.component.scss'],
})
export class PcFormComponent extends BaseComponent implements OnInit {
  isUpdate = false;
  isSubmitted = false;
  subs: Subscription[] = [];
  resourceId = '';
  appId: string | undefined;
  listResourceType: Array<ResourceType> = [];
  listResource: Array<Resource> = [];
  listScope: Array<Scope> = [];
  listRoleForApply: Array<Role> = [];
  listRoleSelected: Array<Role> = [];
  listPolicies: Array<Policy> = [];
  listScopesOfResource: Array<Scope> = [];
  pop: Pick<Permission, popType> | undefined;
  form = this.fb.group({
    id: [''],
    name: ['', [CustomValidators.required, Validators.maxLength(150)]],
    resources: ['', CustomValidators.required],
    type: ['', CustomValidators.required],
    description: ['', [Validators.maxLength(150)]],
    scopes: [null],
    roleName: [''],
    policies: [''],
  });
  isShowScopes = false;
  data: Permission | undefined;
  params = {
    page: 0,
    size: maxInt32,
  };
  constructor(
    injector: Injector,
    private readonly permissionService: PermissionService,
    private readonly functionService: FunctionService,
    private readonly manipulationService: ManipulationService,
    private readonly roleService: RoleService
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.isLoading = true;
    const subRoute = this.route.queryParams.subscribe((params) => {
      this.pop = params;
    });
    this.subs.push(subRoute);
    this.listResourceType.push({ code: 'resource', name: 'Quyền cho chức năng' });
    this.listResourceType.push({ code: 'scope', name: 'Quyền cho thao tác' });
    this.resourceId = this.pop?.resourceId || '';
    this.appId = this.pop?.appId || '';
    const permissionId = this.pop?.id || '';
    const type = this.pop?.type || '';
    if (!permissionId) {
      this.getDataFormAdd();
    } else {
      this.getDataFormEdit(permissionId, type);
    }
    this.checkResourceChange();
    this.checkTypeChange();
    this.checkRoleChange();
  }

  getDataFormAdd() {
    const param = {...this.params, type: this.appId};
    const sub = forkJoin([
      this.functionService.searchResourceCategory(param),
      this.manipulationService.searchScope(this.params),
      this.permissionService.getAllPolicy(this.params),
      this.roleService.searchRole(this.params),
    ]).subscribe(
      ([listResource, listScope, listPolicies, listRole]) => {
        this.listResource = listResource.data.content;
        if (!Utils.isEmpty(this.resourceId)) {
          this.form.controls['resources'].setValue(this.resourceId);
        }
        this.form.controls['type'].setValue('resource');
        this.listScope = listScope?.data.content;
        this.listRoleForApply = listRole?.data.content || [];
        this.listRoleForApply?.forEach((item) => {
          item.description = item.description ? item.name + `-${item.description}` : item.name;
        });
        CommonUtils.sort(this.listRoleForApply, 'description');
        this.listPolicies = listPolicies.data;
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }

  getDataFormEdit(permissionId: string, type: string) {
    const sub = forkJoin([
      this.permissionService.getPermissionById(permissionId, type),
      this.functionService.searchResourceCategory(this.params),
      this.roleService.searchRole(this.params),
      this.manipulationService.searchScope(this.params),
      this.permissionService.getAllPolicy(this.params)
    ]).subscribe(
      ([itemPermission, listResource, listRole, listScope, listPolicies]) => {
        this.isUpdate = true;
        this.listScope = listScope?.data?.content || [];
        this.listPolicies = listPolicies.data || [];
        this.listResource = listResource?.data?.content || [];
        this.resourceId = itemPermission?.data?.resources[0];
        this.data = itemPermission?.data;
        this.form.patchValue(itemPermission?.data);
        const listRoleAll = listRole?.data?.content || [];
        this.listRoleSelected = listRoleAll.filter((i: Role) => itemPermission?.data?.roles?.includes(i.id));
        this.listRoleForApply = listRoleAll.filter((i: Role) => !itemPermission?.data?.roles?.includes(i.id));
        this.listRoleForApply?.forEach((item) => {
          item.description = item.description ? item.name + `-${item.description}` : item.name;
        });
        CommonUtils.sort(this.listRoleForApply, 'description');
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    )
    this.subs.push(sub);
  }

  checkResourceChange() {
    const subResource = this.form.controls['resources'].valueChanges.subscribe((value) => {
      if (value != null && value !== '') {
        const subGet = this.functionService.getResourceCategoryById(value).subscribe((itemResource) => {
          this.listScopesOfResource = [];
          itemResource?.data?.scopes?.forEach((scope: any) => {
            const itemScope = this.listScope.find((itemScope: any) => { return scope === itemScope.code; });
            if (itemScope) {
              this.listScopesOfResource.push(itemScope);
            }
          });
        })
        this.subs.push(subGet);
      } else {
        this.listScopesOfResource = [];
      }
    });
    this.subs.push(subResource)
  }

  checkTypeChange() {
    const subType = this.form.controls['type'].valueChanges.subscribe((value) => {
      if (value === this.listResourceType[1].code) {
        this.form.controls['scopes'].setValidators(CustomValidators.required);
        this.form.controls['scopes'].updateValueAndValidity();
        this.isShowScopes = true;
      } else {
        this.form.controls['scopes'].clearValidators();
        this.form.controls['scopes'].updateValueAndValidity();
        this.isShowScopes = false;
      }
    });
    this.subs.push(subType);
  }

  checkRoleChange() {
    const subRole = this.form.controls['roleName'].valueChanges.subscribe((value) => {
      if (value || value !== '') {
        const role = this.listRoleForApply.find((item) => { return item?.name === value; });
        if (role) {
          this.listRoleSelected.push(role);
        }
        this.listRoleSelected = [...this.listRoleSelected];
        this.listRoleForApply = this.listRoleForApply.filter((item) => {
          return item?.name !== value;
        });
        this.form.controls['roleName'].setValue('');
      }
    });
    this.subs.push(subRole);
  }

  changeresourceId(resourceId: string) {
    if(this.form.controls['resources'].value !== resourceId) {
      this.form.patchValue({scopes: null});
    }
    this.form.controls['resources'].setValue(resourceId);
  }

  deleteRole(itemRole: Role) {
    this.listRoleForApply.push(itemRole);
    CommonUtils.sort(this.listRoleForApply, 'description');
    const listRoleSelected = this.listRoleSelected.filter((item) => item?.name !== itemRole?.name);
    this.listRoleSelected = [...listRoleSelected];
  }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      const data = cleanDataForm(this.form);
      data.decisionStrategy = 'AFFIRMATIVE';
      data.logic = 'POSITIVE';
      const policyIds: any[] = [];
      let itemPolicy: any;
      this.listRoleSelected.forEach((role) => {
        itemPolicy = this.listPolicies.find((policy) => {
          return policy.name === role.name;
        });
        if (itemPolicy) {
          policyIds.push(itemPolicy.id);
        }
      });
      data.policies = policyIds;
      delete data.roleName;
      this.isLoading = true;
      if (!data.id) {
        if (data.type === 'resource') {
          delete data.scopes;
        }
        data.resources = [data.resources];
        delete data.id;
        const subAdd = this.permissionService.createPermission(data).subscribe(() => {
            this.isLoading = false;
            this.back();
            this.toastrCustom.success(this.translate.instant('common.notification.addSuccess'));
          }, (e) => {
            this.toastrCustom.error(this.translate.instant('common.notification.addError') + `: ${e?.message}`);
            this.isLoading = false;
          }
        )
        this.subs.push(subAdd);
      } else {
        data.resources = [this.resourceId];
        const subUpdate = this.permissionService.updatePermission(data).subscribe(() => {
            this.isLoading = false;
            this.back();
            this.toastrCustom.success(this.translate.instant('common.notification.updateSuccess'));
          }, (e) => {
            this.toastrCustom.error(
              this.translate.instant('common.notification.updateError') + `: ${e?.message}`);
            this.isLoading = false;
          }
        )
        this.subs.push(subUpdate);
      }
    } else {
      validateAllFormFields(this.form);
    }
  }

  itemchoose(value: any) {
    if (value) {
      const role = this.listRoleForApply.find((item) => { return item?.name === value; });
      if (role) {
        this.listRoleSelected.push(role);
      }
      this.listRoleSelected = [...this.listRoleSelected];
      this.listRoleForApply = this.listRoleForApply.filter((item) => {
        return item?.name !== value;
      });
      this.form.patchValue({ roleName: '' });
    }
  }

  override ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }

  emitValue(item: SelectCheckAbleModal) {
    if((item.listOfSelected && item.listOfSelected.length > 0) || item.itemChecked === "ALL") {
      this.form.patchValue({scopes: item.listOfSelected});
    }
  }
}
