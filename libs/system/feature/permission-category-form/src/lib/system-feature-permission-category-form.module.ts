import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PcFormComponent } from './pc-form/pc-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { NzFormModule } from 'ng-zorro-antd/form';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { TranslateModule } from '@ngx-translate/core';
import { SystemUiChooseFunctionModule } from '@hcm-mfe/system/ui/choose-function';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { SharedUiMbSelectCheckAbleModule } from '@hcm-mfe/shared/ui/mb-select-check-able';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzTableModule } from 'ng-zorro-antd/table';
import { SharedUiMbButtonIconModule } from '@hcm-mfe/shared/ui/mb-button-icon';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedUiLoadingModule, NzFormModule, SharedUiMbInputTextModule,
    TranslateModule, SystemUiChooseFunctionModule, NzIconModule, SharedUiMbSelectModule, SharedUiMbSelectCheckAbleModule,
    NzSelectModule, NzTableModule, SharedUiMbButtonIconModule, SharedUiMbButtonModule,
    RouterModule.forChild([
      {
        path: '',
        component: PcFormComponent
      }
    ])
  ],
  declarations: [PcFormComponent],
  exports: [PcFormComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SystemFeaturePermissionCategoryFormModule {}
