import { forkJoin, Subscription } from 'rxjs';
import { Component, HostBinding, OnInit, ViewEncapsulation } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { FormBuilder } from '@angular/forms';
import * as moment from 'moment';
import { SessionHistory } from '@hcm-mfe/system/data-access/models';
import { userConfig } from '@hcm-mfe/shared/common/constants';
import { Pageable } from '@hcm-mfe/shared/data-access/models';
import { ValidateService } from '@hcm-mfe/shared/core';
import { SessionHistoryService } from '@hcm-mfe/system/data-access/services';

@Component({
  selector: 'app-session-history-detail',
  templateUrl: './sh-detail.component.html',
  styleUrls: ['./sh-detail.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ShDetailComponent implements OnInit {
  @HostBinding('class') elementClass = 'app-session-history-detail';
  subs: Subscription[] = [];
  isLoading = false;
  listData: Array<Pick<SessionHistory, 'fromDate' | 'appName' | 'featureName'>> = [];
  limit = userConfig.pageSize;
  utcId: string | undefined;
  paramSearch = {
    size: this.limit,
    page: 0,
  };
  pageable: Pageable = new Pageable();
  form = this.fb.group({
      username: '',
      deviceName: '',
      loginTime: '',
    });
  constructor(
    public validateService: ValidateService,
    private readonly fb: FormBuilder,
    private readonly modalService: NzModalService,
    private readonly sessionHistoryService: SessionHistoryService
  ) {}

  ngOnInit(): void {
    this.form.disable();
    this.isLoading = true;
    this.search()
  }

  search($event?: any) {
    this.paramSearch.page = 0;
    if ($event) {
      this.paramSearch.page = $event - 1;
    }
    this.isLoading = true;
    if (this.utcId) {
      const sub = forkJoin([
        this.sessionHistoryService.getListDetail(this.utcId, this.paramSearch),
        this.sessionHistoryService.findById(this.utcId)
      ]).subscribe(
        ([listDetail, session]) => {
          if(session.data) {
            session.data.loginTime = moment(session.data.loginTime).format('DD/MM/yyyy HH:mm:ss');
            this.form.patchValue(session.data);
          }
          if (listDetail.data) {
            this.listData = listDetail.data.content || [];
            this.pageable.totalElements = listDetail?.data?.totalElements;
            this.pageable.totalPages = listDetail?.data?.totalPages;
            this.pageable.currentPage = listDetail?.data?.number + 1;
            this.pageable.size = this.limit;
          }
          this.isLoading = false;
        },
        () => {
          this.isLoading = false;
        }
      );
      this.subs.push(sub);
    }
  }

  ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }

  close() {
    this.modalService.closeAll();
  }
}
