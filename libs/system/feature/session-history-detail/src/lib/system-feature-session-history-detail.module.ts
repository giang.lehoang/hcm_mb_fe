import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShDetailComponent } from './sh-detail/sh-detail.component';
import { NzFormModule } from 'ng-zorro-antd/form';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { TranslateModule } from '@ngx-translate/core';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, NzFormModule, SharedUiMbInputTextModule, TranslateModule, NzTableModule, NzPaginationModule],
  declarations: [ShDetailComponent],
  exports: [ShDetailComponent],
})
export class SystemFeatureSessionHistoryDetailModule {}
