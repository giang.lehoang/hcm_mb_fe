import { Component, OnInit, Injector } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { Scope } from '@hcm-mfe/system/data-access/models';
import { CustomValidators } from '@hcm-mfe/shared/common/validators';
import { Validators } from '@angular/forms';
import { ManipulationService } from '@hcm-mfe/system/data-access/services';
import { validateAllFormFields } from '@hcm-mfe/shared/common/utils';

@Component({
  selector: 'manipulation-category-form',
  templateUrl: './mc-form.component.html',
  styleUrls: ['./mc-form.component.scss'],
})
export class McFormComponent extends BaseComponent implements OnInit {
  subs: Subscription[] = [];
  isSubmitted = false;
  data: Scope | undefined;
  form = this.fb.group({
    id: [''],
    code: ['', [CustomValidators.required, CustomValidators.code, Validators.maxLength(50)]],
    name: ['', [CustomValidators.required, Validators.maxLength(150)]],
    description: ['', Validators.maxLength(150)],
  });
  id: string | undefined;
  constructor(
    injector: Injector,
    private readonly manipulationService: ManipulationService,
    private readonly activatedRoute: ActivatedRoute
  ) {
    super(injector);
  }

  ngOnInit(): void {
    const subRoute = this.activatedRoute.queryParams.subscribe((params: any) => {
      this.id = params.id;
    });
    this.subs.push(subRoute);
    if (this.id) {
      this.isLoading = true;
      const sub = this.manipulationService.getScopeById(this.id).subscribe(res => {
          this.data = res.data;
          this.form.patchValue(this.data ?? {});
          this.isLoading = false;
        }, () => {
          this.isLoading = false;
        }
      );
      this.subs.push(sub);
    }
  }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      this.isLoading = true;
      const request: Scope = this.form.value;
      if (!this.id) {
        const subAdd = this.manipulationService.createScope(request).subscribe(() => {
            this.isLoading = false;
            this.back();
            this.toastrCustom.success(this.translate.instant('common.notification.addSuccess'));
          }, (e) => {
            this.toastrCustom.error(this.translate.instant('common.notification.addError') + `: ${e?.message}`);
            this.isLoading = false;
          }
        );
        this.subs.push(subAdd);
      } else {
        const subUpdate = this.manipulationService.updateScope(request).subscribe( () => {
            this.isLoading = false;
            this.back();
            this.toastrCustom.success(this.translate.instant('common.notification.updateSuccess'));
          }, (e) => {
            this.toastrCustom.error(
              this.translate.instant('common.notification.updateError') + `: ${e?.message}`
            );
            this.isLoading = false;
          }
        );
        this.subs.push(subUpdate);
      }
    } else {
      validateAllFormFields(this.form);
    }
  }

  override ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }

  codeBlur() {
    this.form.controls['code'].setValue(this.form.controls['code'].value.trim())
  }
}
