import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { McFormComponent } from './mc-form/mc-form.component';
import { RouterModule } from '@angular/router';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { NzFormModule } from 'ng-zorro-antd/form';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { SharedDirectivesUppercaseInputModule } from '@hcm-mfe/shared/directives/uppercase-input';
import { NzInputModule } from 'ng-zorro-antd/input';
import { SharedDirectivesTrimInputModule } from '@hcm-mfe/shared/directives/trim-input';
import { NzTableModule } from 'ng-zorro-antd/table';
import { SharedUiMbButtonIconModule } from '@hcm-mfe/shared/ui/mb-button-icon';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';

@NgModule({
  imports: [CommonModule, SharedUiMbButtonModule, TranslateModule, SharedUiLoadingModule, NzFormModule, FormsModule,
    ReactiveFormsModule, SharedUiMbInputTextModule, SharedDirectivesUppercaseInputModule, NzInputModule,
    SharedDirectivesTrimInputModule, NzTableModule, SharedUiMbButtonIconModule, NzPaginationModule,
    RouterModule.forChild([
      {
        path: '',
        component: McFormComponent
      }
    ])
  ],
  declarations: [McFormComponent],
  exports: [McFormComponent],
})
export class SystemFeatureManipulationFormModule {}
