import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzFormModule } from 'ng-zorro-antd/form';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { SharedUiMbTextLabelModule } from '@hcm-mfe/shared/ui/mb-text-label';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { NzTableModule } from 'ng-zorro-antd/table';
import { SharedPipesConvertArrayStringModule } from '@hcm-mfe/shared/pipes/convert-array-string';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, NzCardModule, NzFormModule, SharedUiMbInputTextModule, TranslateModule,
    SharedUiMbButtonModule, SharedUiMbTextLabelModule, NzCollapseModule, NzTypographyModule, NzTableModule,
    SharedPipesConvertArrayStringModule, NzIconModule, FormsModule, ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: UserProfileComponent
      }
    ])
  ],
  declarations: [UserProfileComponent],
  exports: [UserProfileComponent],
})
export class SystemFeatureUserProfileModule {}
