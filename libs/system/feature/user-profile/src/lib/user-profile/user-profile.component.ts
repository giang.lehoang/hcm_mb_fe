import { Component, Injector, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { AppFunction, TreeNode } from '@hcm-mfe/shared/data-access/models';
import {
  IBaseResponse,
  IFunctionPermission, IRoleFunctionPermissions,
  IUserInfo,
  IUserPram,
  TreeNodeInterface
} from '@hcm-mfe/system/data-access/models';
import { UserService } from '@hcm-mfe/system/data-access/services';
import { FunctionCode } from '@hcm-mfe/shared/common/enums';
import { ModalListComponent } from '@hcm-mfe/system/ui/modal-list';
import { CommonUtils } from '@hcm-mfe/shared/core';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss'],
})
export class UserProfileComponent extends BaseComponent implements OnInit {
  subs: Subscription[] = [];
  objFunctionSearch: AppFunction;
  labelRole: string;
  styleCustom = { 'padding-left': '3rem' };
  dataPermission = [
    {
      active: true,
      name: 'ORG',
      dataPermission: [],
    },
    {
      active: false,
      name: 'LINE',
      dataPermission: [],
    },
    {
      active: false,
      name: 'TV',
      dataPermission: [],
    },
    {
      active: false,
      name: 'KV',
      dataPermission: [],
    },
    {
      active: false,
      name: 'PRJ',
      dataPermission: [],
    },
    {
      active: false,
      name: 'DTCV',
      dataPermission: [],
    },
  ];
  userParam: IUserPram = {
    username: '',
  };
  infoUser: IUserInfo = {
    userId: '',
    username: '',
    fullName: '',
    empCode: '',
    dateOfBirth: '',
    genderName: '',
    phoneNumber: '',
    email: '',
    address: '',
    orgName: '',
    jobName: '',
    posName: '',
    flagStatus: '',
    isTracing: '',
    reasonLock: '',
  };
  listFunctionPermission: IFunctionPermission[] = [];
  mapOfExpandedData: { [key: string]: TreeNodeInterface[] } = {};
  listDataPermissions: {[key: string] : any} = {};
  dataPermissions: { [key: string]: any } = {};
  constructor(injector: Injector, readonly userService: UserService) {
    super(injector);
    this.labelRole = this.translate.instant('system.userProfile.label.roleCode');
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ADMIN_USER_PROFILE}`);
    this.objFunctionSearch = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ADMIN_SEARCH_PERMISSION}`);
    this.userParam.username = JSON.parse(localStorage.getItem('CURRENCY_USER') ?? '')?.username;
  }

  ngOnInit(): void {
    this.searchInfo();
  }

  trackByNode(index: number, item: any) {
    return item.id;
  }
  getFunctionPermission(): void {
    if (this.objFunctionSearch?.view) {
      const sub = this.userService.getFunctionPermissionByAdmin(this.userParam).subscribe(data => {
          this.getFunction(data);
        },
        () => {
          this.isLoading = false;
        }
      );
      this.subs.push(sub);
    } else {
      const sub = this.userService.getFunctionPermission().subscribe(data => {
        this.getFunction(data);
        },
        () => {
          this.isLoading = false;
        }
      );
      this.subs.push(sub);
    }
  }
  getFunction(data: IBaseResponse<IRoleFunctionPermissions<IFunctionPermission[]>>) {
    this.listFunctionPermission = data.data.roleFunctionPermissions;
    this.changeData();
    this.isLoading = false;
  }

  getUserInfo(): void {
    const sub = this.userService.getUserInfo(this.userParam.username).subscribe((data) => {
      this.infoUser = data.data;
    });
    this.subs.push(sub);
  }
  changeData(): void {
    if (!this.listFunctionPermission) {
      return;
    }
    this.listFunctionPermission.forEach((item) => {
      item['active'] = false;
      this.dataPermissions[item.key] = this.convertDataPermission(item.dataDecentralizationList, item.key);
      item.functionPermissionObjects.forEach((element) => {
        this.mapOfExpandedData[element.key] = this.convertTreeToList(element);
      });
    });
    this.listFunctionPermission[0].active = true;
  }
  collapse(array: TreeNodeInterface[], data: TreeNodeInterface, $event: boolean): void {
    if (!$event) {
      if (data.permissionDTOS) {
        data.permissionDTOS.forEach((d) => {
          const target: any = array.find((a) => a.key === d.key);
          target.expand = true;
          this.collapse(array, target, true);
        });
      } else {
        return;
      }
    }
  }

  collapsePermissions(array: TreeNode[], data: TreeNode, $event: boolean): void {
    if (!$event) {
      if (data.children) {
        data.children.forEach((d) => {
          const target: any = array.find((a) => a.key === d.key);
          target.expand = true;
          this.collapsePermissions(array, target, true);
        });
      } else {
        return;
      }
    }
  }
  convertDataPermission(root: any, key: string): any {
    const treeNode: TreeNode[] = [];
    treeNode.push(...[
      {id: 'ORG', name: 'ORG'},
      {id: 'LINE', name: 'LINE'},
      {id: 'TV', name: 'TV'},
      {id: 'KV', name: 'KV'},
      {id: 'PRJ', name: 'PRJ'},
      {id: 'DTCV', name: 'DTCV'}
    ])
    root?.forEach((item: any) => {
      treeNode.push({
        id: item.dataId,
        name: item.dataName,
        parentId: item.dataType
      })
    });
    const mapOfExpanded = {};
    const tree = CommonUtils.convertToTree(treeNode, '', ['name']);
    this.listDataPermissions[key] = tree;
    CommonUtils.mapOfExpand(tree, mapOfExpanded, true);
    return mapOfExpanded;
  }
  convertTreeToList(root: TreeNodeInterface): TreeNodeInterface[] {
    const stack: TreeNodeInterface[] = [];
    const array: TreeNodeInterface[] = [];
    const hashMap = {};
    stack.push({ ...root, level: 0, expand: false });

    while (stack.length !== 0) {
      const node: any = stack.pop();
      this.visitNode(node, hashMap, array);
      if (node.permissionDTOS) {
        for (let i = node.permissionDTOS.length - 1; i >= 0; i--) {
          stack.push({ ...node.permissionDTOS[i], level: node.level + 1, expand: true, parent: node });
        }
      }
    }

    return array;
  }
  visitNode(node: TreeNodeInterface, hashMap: { [key: string]: boolean }, array: TreeNodeInterface[]): void {
    if (!hashMap[node.key]) {
      hashMap[node.key] = true;
      array.push(node);
    }
  }
  showListUserModal(): void {
    const modal = this.modal.create({
      nzWidth: this.getModalWidth(),
      nzTitle: 'Danh sách người dùng',
      nzContent: ModalListComponent,
      nzFooter: null,
    });
    modal.afterClose.subscribe((result) => {
      if (result?.data) {
        this.userParam.username = result?.data;
      }
    });
  }

  getModalWidth(): number {
    if(window.innerWidth > 767) {
      return window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.2
    }
    return window.innerWidth;
  }
  searchInfo(): void {
    this.isLoading = true;
    this.getUserInfo();
    this.getFunctionPermission();
  }

  activeChange(panel: IFunctionPermission) {
    panel.active = true;
  }

  override ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
