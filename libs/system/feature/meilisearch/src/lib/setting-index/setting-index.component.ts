import { Component, ElementRef, Injector, OnInit, ViewChild } from '@angular/core';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { IndexService } from '@hcm-mfe/system/data-access/services';
import { Subscription } from 'rxjs';

@Component({
  selector: 'hcm-mfe-setting-index',
  templateUrl: './setting-index.component.html',
  styleUrls: ['./setting-index.component.scss']
})
export class SettingIndexComponent extends BaseComponent implements OnInit {

  indexUid?: string;
  isSubmitted = false;
  configs: string[] = [];
  inputVisible = false;
  inputValue = '';
  subs: Subscription[] = [];
  @ViewChild('inputElement', { static: false }) inputElement?: ElementRef;

  constructor(injector: Injector,
              private readonly modalRef: NzModalRef,
              private readonly indexService: IndexService
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.getSetting();
  }

  handleClose(removedTag: {}): void {
    this.configs = this.configs.filter(config => config !== removedTag);
  }

  sliceTagName(config: string): string {
    const isLongTag = config.length > 20;
    return isLongTag ? `${config.slice(0, 20)}...` : config;
  }

  showInput(): void {
    this.inputVisible = true;
    setTimeout(() => {
      this.inputElement?.nativeElement.focus();
    }, 10);
  }

  handleInputConfirm(): void {
    if (this.inputValue && this.configs.indexOf(this.inputValue) === -1) {
      this.configs = [...this.configs, this.inputValue];
    }
    this.inputValue = '';
    this.inputVisible = false;
    this.showInput();
  }

  cancel() {
    this.modalRef.close();
  }

  save() {
    this.isSubmitted = true;
    if (this.configs.length > 0 && this.indexUid) {
      const request: NzSafeAny = {filterableAttributes: this.configs};
      const sub = this.indexService.updateIndexSettingById(request, this.indexUid).subscribe(
        () => {
          this.toastrCustom.success(this.translate.instant('common.notification.success'));
          this.modalRef.close(true);
        },
        (error) => {
          this.toastrCustom.error(this.translate.instant('common.notification.error') + `: ${error?.message}`);
        }
      );
      this.subs.push(sub);
    }
  }

  private getSetting() {
    this.isLoading = true;
    if(this.indexUid) {
      const sub = this.indexService.getIndexSettingById(this.indexUid).subscribe(
        (res) => {
          this.configs = res.data.filterableAttributes;
          this.isLoading = false;
        },
        (error) => {
          this.toastrCustom.error(error?.message);
          this.isLoading = false;
        }
      );
      this.subs.push(sub);
    }
  }
}
