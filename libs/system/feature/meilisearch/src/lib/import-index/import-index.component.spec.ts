import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportIndexComponent } from './import-index.component';

describe('ImportIndexComponent', () => {
  let component: ImportIndexComponent;
  let fixture: ComponentFixture<ImportIndexComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImportIndexComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
