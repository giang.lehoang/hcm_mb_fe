import { Component, Injector, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { ToastrService } from 'ngx-toastr';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { CommonUtils } from '@hcm-mfe/shared/core';
import { IndexService } from '@hcm-mfe/system/data-access/services';

@Component({
  selector: 'hcm-mfe-import-index',
  templateUrl: './import-index.component.html',
  styleUrls: ['./import-index.component.scss']
})
export class ImportIndexComponent extends BaseComponent implements OnInit {

  indexUid?: string;
  fileList: NzUploadFile[] = [];
  isModalError: boolean = false;
  fileName: string = '';
  fileImportName: string = '';
  fileImportSize: string = '';
  documentJson: NzSafeAny;

  subs: Subscription[] = [];
  constructor(
    injector: Injector,
    private readonly modalRef: NzModalRef,
    private readonly toastrService: ToastrService,
    private readonly indexService: IndexService,
  ) {
    super(injector);
  }

  ngOnInit(): void {
  }


  save() {
    let isParseJson = true;
    try {
      JSON.parse(this.documentJson)
    } catch (e) {
      isParseJson = false;
    }
    if ((!CommonUtils.isNullOrEmpty(this.documentJson) && isParseJson) || this.fileList.length > 0 && this.indexUid) {
      const formData = new FormData();
      if(this.fileList.length > 0) {
        this.fileList.forEach((file: NzUploadFile) => {
          formData.append('file', file as any);
        });
      } else {
        formData.append('documents', JSON.stringify(JSON.parse(this.documentJson)));
      }
      const sub = this.indexService.importDocument(formData, this.indexUid).subscribe(
        () => {
          this.toastrCustom.success(this.translate.instant('common.notification.success'));
          this.modalRef.close(true);
        },
        (error) => {
          this.toastrCustom.error(this.translate.instant('common.notification.error') + `: ${error?.message}`);
        }
      );
      this.subs.push(sub);
    } else {
      if (!CommonUtils.isNullOrEmpty(this.documentJson) && !isParseJson) {
        this.toastrCustom.error(this.translate.instant('system.meilisearch.error.format'));
      } else {
        this.toastrCustom.error(this.translate.instant('system.meilisearch.error.empty'));
      }
    }

  }

  format() {
    const value = this.documentJson;
    let isJson = false;
    let jsonFormat;
    try {
      jsonFormat = JSON.parse(value);
      isJson = true;
    } catch (e) {
      isJson = false;
    }
    if(isJson && jsonFormat) {
      this.documentJson = JSON.stringify(jsonFormat, null, 4);
    }
  }

  cancel() {
    this.modalRef.close();
  }

  beforeUpload = (file: NzUploadFile): boolean => {
    if (file.size && file.size >= 3000000 || file.type !== 'application/json') {
      if(file.type !== 'application/json') {
        this.toastrService.error(this.translate.instant('common.upload.fileError'));
      }
      if(file.size && file.size >= 3000000) {
        this.toastrService.error(this.translate.instant('common.notification.limitSize'));
      }
    } else {
      this.fileList = [];
      this.fileList = this.fileList.concat(file);
      this.fileImportName = file.name;
      if (file.size && file.size > 10000) {
        this.fileImportSize = ((file.size ?? 1)/1000000).toFixed(2) + ' MB';
      } else {
        this.fileImportSize = file.size?.toFixed(2) + ' KB';
      }
    }
    return false;
  };

  doRemoveFile = (): boolean => {
    this.fileList = [];
    return true;
  };

  override ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }

}
