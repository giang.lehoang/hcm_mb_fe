import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { NzFormModule } from 'ng-zorro-antd/form';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { SharedDirectivesUppercaseInputModule } from '@hcm-mfe/shared/directives/uppercase-input';
import { IndexListComponent } from './index-list/index-list.component';
import { NzInputModule } from 'ng-zorro-antd/input';
import { SharedDirectivesTrimInputModule } from '@hcm-mfe/shared/directives/trim-input';
import { NzTableModule } from 'ng-zorro-antd/table';
import { SharedUiMbButtonIconModule } from '@hcm-mfe/shared/ui/mb-button-icon';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { RouterModule } from '@angular/router';
import { SharedUiMbTableMergeCellWrapModule } from '@hcm-mfe/shared/ui/mb-table-merge-cell-wrap';
import { SharedUiMbTableMergeCellModule } from '@hcm-mfe/shared/ui/mb-table-merge-cell';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzNoAnimationModule } from 'ng-zorro-antd/core/no-animation';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { AddIndexComponent } from './add-index/add-index.component';
import { SettingIndexComponent } from './setting-index/setting-index.component';
import { ImportIndexComponent } from './import-index/import-index.component';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzUploadModule } from 'ng-zorro-antd/upload';
@NgModule({
  imports: [CommonModule, SharedUiMbButtonModule, TranslateModule, SharedUiLoadingModule, NzFormModule, FormsModule,
    ReactiveFormsModule, SharedUiMbInputTextModule, SharedDirectivesUppercaseInputModule, NzInputModule,
    SharedDirectivesTrimInputModule, NzTableModule, SharedUiMbButtonIconModule, NzPaginationModule,
    RouterModule.forChild([
      {
        path: '',
        component: IndexListComponent
      }
    ]), SharedUiMbTableMergeCellWrapModule, SharedUiMbTableMergeCellModule, NzModalModule, NzIconModule, NzTagModule,
    NzNoAnimationModule, NzToolTipModule, NzDropDownModule, NzButtonModule, NzUploadModule
  ],
  declarations: [IndexListComponent, AddIndexComponent, SettingIndexComponent, ImportIndexComponent],
  exports: [IndexListComponent],
})
export class SystemFeatureMeilisearchModule {}
