import { Component, Injector, OnDestroy, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { IndexService } from '@hcm-mfe/system/data-access/services';
import { Subscription } from 'rxjs';
import { NzSafeAny } from 'ng-zorro-antd/core/types';

@Component({
  selector: 'hcm-mfe-add-index',
  templateUrl: './add-index.component.html',
  styleUrls: ['./add-index.component.scss']
})
export class AddIndexComponent extends BaseComponent implements OnInit {

  form = this.fb.group({
    uid: [null, Validators.required]
  });

  subs: Subscription[] = [];

  isSubmitted = false;

  constructor(
    injector: Injector,
    private readonly modalRef: NzModalRef,
    private readonly indexService: IndexService
  ) {
    super(injector);
  }

  ngOnInit(): void {
  }

  blur() {
    this.form.controls['uid'].setValue(this.form.controls['uid'].value.trim());
  }

  cancel() {
    this.modalRef.close();
  }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      const request: NzSafeAny = this.form.value;
      const subAdd = this.indexService.createIndex(request).subscribe(
        () => {
          this.toastrCustom.success(this.translate.instant('common.notification.addSuccess'));
          this.modalRef.close(true);
        },
        (error) => {
          this.toastrCustom.error(this.translate.instant('common.notification.addError') + `: ${error?.message}`);
        }
      );
      this.subs.push(subAdd);
    }
  }

}
