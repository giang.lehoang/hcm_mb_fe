import { Component, OnInit, Injector, ViewChild, TemplateRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { userConfig } from '@hcm-mfe/shared/common/constants';
import { Pageable, TableConfig } from '@hcm-mfe/shared/data-access/models';
import { AppService, IndexService } from '@hcm-mfe/system/data-access/services';
import { App } from '@hcm-mfe/system/data-access/models';
import { FunctionCode } from '@hcm-mfe/shared/common/enums';
import { ValidateService } from '@hcm-mfe/shared/core';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { AddIndexComponent } from '../add-index/add-index.component';
import { SettingIndexComponent } from '../setting-index/setting-index.component';
import { ImportIndexComponent } from '../import-index/import-index.component';

@Component({
  selector: 'app-index-list',
  templateUrl: './index-list.component.html',
  styleUrls: ['./index-list.component.scss'],
})
export class IndexListComponent extends BaseComponent implements OnInit {
  subs: Subscription[] = [];
  listData: Array<App> = [];
  limit = userConfig.pageSize;
  paramSearch = {
    limit: this.limit,
    page: 0,
  };
  tableConfig!: TableConfig;
  @ViewChild('actionTmpl', { static: true }) action!: TemplateRef<NzSafeAny>;
  pageable: Pageable = new Pageable();
  modalRef: NzModalRef | undefined;

  constructor(
    injector: Injector,
    private readonly appService: AppService,
    private readonly indexService: IndexService,
    public validateService: ValidateService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ADMIN_APP}`);
  }

  ngOnInit(): void {
    this.initTable();
    this.search(1);
  }

  initTable() {
    this.tableConfig = {
      headers: [
        {
          title: 'STT',
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          fixed: true,
          fixedDir: 'left',
          width: 5
        },
        {
          title: 'system.meilisearch.table.index',
          field: 'uid', width: 120,
        },
        {
          title: ' ',
          field: '',
          tdTemplate: this.action,
          tdClassList: ['text-center'],
          fixed: true,
          fixedDir: 'right',
          show: this.objFunction?.edit || this.objFunction?.delete,
          width: 5,
        },
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.paramSearch.limit,
      pageIndex: 1,
    };
  }

  search(page: any) {
    this.paramSearch.page = page - 1;
    this.isLoading = true;
    const sub = this.indexService.searchIndexes(this.paramSearch).subscribe(result => {
        if (result.data) {
          this.listData = result.data.results || [];
          this.tableConfig.total = result.data.total;
          this.tableConfig.pageIndex = this.paramSearch.page + 1;
        }
        this.isLoading = false;
      },() => {
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }

  delete(indexUid: string) {
    this.deletePopup.showModal(() => {
      const subDelete = this.indexService.deleteIndex(indexUid).subscribe(() => {
          this.toastrCustom.success(this.translate.instant('common.notification.deleteSuccess'));
          this.search(this.tableConfig.pageIndex);
          this.isLoading = false;
        }, (e) => {
          this.toastrCustom.error(
            this.translate.instant('common.notification.deleteError') + `: ${e?.message}`
          );
          this.isLoading = false;
        }
      );
      this.subs.push(subDelete);
    });
  }

  addIndex() {
    this.modalRef = this.modal.create({
      nzTitle: this.translate.instant('system.meilisearch.label.addIndex'),
      nzContent: AddIndexComponent,
    });
    this.modalRef.afterClose.subscribe(res => {
      if(res) {
        this.search(1);
      }
    });
  }

  settingIndex(indexUid: string) {
    this.modalRef = this.modal.create({
      nzTitle: this.translate.instant('system.meilisearch.label.settingIndex'),
      nzContent: SettingIndexComponent,
      nzComponentParams: {
        indexUid: indexUid
      }
    });
  }

  importDocument(indexUid: string) {
    this.modalRef = this.modal.create({
      nzTitle: this.translate.instant('Import document'),
      nzContent: ImportIndexComponent,
      nzWidth: this.getModalWidth(),
      nzComponentParams: {
        indexUid: indexUid
      }
    });
  }

  getModalWidth(): number {
    if(window.innerWidth > 767) {
      return window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5
    }
    return window.innerWidth;
  }

  override ngOnDestroy() {
    this.deletePopup?.modalDelete?.destroy();
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
