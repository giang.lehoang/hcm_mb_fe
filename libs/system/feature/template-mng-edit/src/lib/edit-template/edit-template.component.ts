import { ChangeDetectorRef, Component, ElementRef, Injector, OnInit, ViewChild } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import {
  IBaseResponse,
  IListTemplate,
  INotiGroup,
  mbDataSelectsAssessmentStatus
} from '@hcm-mfe/system/data-access/models';
import { TemplateManagementService } from '@hcm-mfe/system/data-access/services';
import { CustomValidators } from '@hcm-mfe/shared/common/validators';

@Component({
  selector: 'app-edit-template',
  templateUrl: './edit-template.component.html',
  styleUrls: ['./edit-template.component.scss'],
})
export class EditTemplateComponent extends BaseComponent implements OnInit {
  @ViewChild('passwordInput') input: ElementRef | undefined;
  subs: Subscription[] = [];
  passwordVisible = false;
  id: string | undefined;
  toAddress: string | undefined;
  isVisible = false;
  isSubmitted = false;
  isEmail = true;
  isNoti = false;
  selectedItem: INotiGroup | undefined;
  listGroup: INotiGroup[] = [];
  listStatus = mbDataSelectsAssessmentStatus();
  formUpdate: FormGroup = this.fb.group({
    ngrId: [{ value: '', disabled: true }, [Validators.required]],
    ntpCode: [{ value: '', disabled: true }, [Validators.required, Validators.maxLength(50), CustomValidators.code]],
    ntpName: ['', [Validators.required, Validators.maxLength(250)]],
    subject: ['', [Validators.required, Validators.maxLength(100)]],
    fromAddress: ['', [Validators.required, Validators.maxLength(100)]],
    mailCC: ['', [Validators.maxLength(500), CustomValidators.mailValidator]],
    mailBCC: ['', [Validators.maxLength(200), CustomValidators.mailValidator]],
    toAddress: ['', [Validators.maxLength(200), CustomValidators.mailValidator]],
    fromPassword: [''],
    hidePassword: [''],
    flagStatus: ['', [Validators.required]],
    content: [''],
    notiLink: ['', [Validators.maxLength(100), CustomValidators.url]],
    pushNotiLink: ['', [Validators.maxLength(100), CustomValidators.url]],
    shortContent: [''],
  });
  modules = {
    toolbar: [
      ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
      ['blockquote', 'code-block'],

      [{ 'header': 1 }, { 'header': 2 }],               // custom button values
      [{ 'list': 'ordered'}, { 'list': 'bullet' }],
      [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
      [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
      [{ 'direction': 'rtl' }],                         // text direction

      [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
      [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

      [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
      [{ 'font': [] }],
      [{ 'align': [] }],

      ['clean'],                                         // remove formatting button

      ['link', 'image']                         // link and image
    ]
  };
  constructor(
    injector: Injector,
    readonly cdref: ChangeDetectorRef,
    readonly templateMngService: TemplateManagementService,
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.isLoading = true;
    const subRoute = this.route.queryParams.subscribe((params: any) => {
      this.id = params.id;
    });
    this.subs.push(subRoute);
    this.toAddress = JSON.parse(localStorage.getItem('CURRENCY_USER') ?? '')?.email;
    const sub = this.templateMngService.getListGroupNoti().subscribe((data: IBaseResponse<INotiGroup[]>) => {
      this.listGroup = data.data;
      this.getDetailTemplate();
    }, (e) => {
      this.toastrCustom.error(this.translate.instant(e?.message));
    });
    this.subs.push(sub);
  }
  ngAfterContentChecked(): void {
    this.cdref.detectChanges();
  }

  getDetailTemplate(): void {
    const sub = this.templateMngService.getDetailTemplate(this.id).subscribe(
      (data: IBaseResponse<IListTemplate>) => {
        this.formUpdate.patchValue(data.data);
        this.formUpdate.controls['flagStatus'].setValue(+data.data.flagStatus);
        this.formUpdate.controls['hidePassword'].setValue(data.data.fromPassword);
        this.formUpdate.controls['mailCC'].setValue(data.data.mailCC?.replace(/,/g, ", "));
        this.formUpdate.controls['mailBCC'].setValue(data.data.mailBCC?.replace(/,/g, ", "));
        this.selectNotiGroup(data.data.ngrId);
        this.isLoading = false;
      },
      (e) => {
        this.toastrCustom.error(this.translate.instant(e?.message));
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }
  showModal(): void {
    this.isVisible = true;
  }
  sendEmail(data: any): void {
    const sub = this.templateMngService.sendEmail(data).subscribe(
      () => {
        this.toastrCustom.success(this.translate.instant('common.notification.sendEmailSuccess'));
        this.isLoading = false;
      },
      (e) => {
        if (e?.message === "Gửi email thất bại") {
          this.toastrCustom.error(this.translate.instant(e?.message));
        } else {
          this.toastrCustom.error(this.translate.instant('common.notification.sendEmailError') + e?.message);
        }
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }
  submitForm(): void {
    this.isSubmitted = true;
    const data = this.formUpdate.getRawValue();
    if (this.formUpdate.valid) {
      this.isLoading = true;
      this.updateTemplate(data, this.id);
    }
  }
  updateTemplate(data: any, id: any): void {
    const sub = this.templateMngService.updateTemplate(data, id).subscribe(
      () => {
        this.toastrCustom.success(this.translate.instant('common.notification.updateSuccess'));
        this.isLoading = false;
        this.router.navigateByUrl('/system/template-mng');
      },
      (e) => {
        this.toastrCustom.error(
          this.translate.instant('common.notification.updateError') + e?.message
        );
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }
  handleCancel() {
    this.isVisible = false;
  }
  handleOk() {
    this.isLoading = true;
    this.isVisible = false;
    const data = {
      fromAddress: this.formUpdate.value.fromAddress,
      password: this.formUpdate.value.fromPassword,
      hidePassword: this.formUpdate.value.hidePassword,
      content: this.formUpdate.value.content,
      title: this.formUpdate.value.subject,
      toAddress: this.toAddress,
      encrypt : "true"
    }
    if (data.password !== data.hidePassword) {
      data.encrypt = "false";
    }
    if (CustomValidators.mailValidator(this.formUpdate.controls['fromAddress'])) {
      delete data['password'];
      delete data['encrypt'];
      delete data['hidePassword'];
    }
    this.sendEmail(data);
  }
  changeTypeInput() {
    this.passwordVisible = !this.passwordVisible;
    if (this.input) {
      if (this.passwordVisible) {
        this.input.nativeElement.type = 'text';
      } else {
        this.input.nativeElement.type = 'password';
      }
    }
  }
  selectNotiGroup(value: any): void {
    this.selectedItem = this.listGroup.find((el) => el.ngrId === value);
    if (!this.selectedItem) {
      return;
    }
    if (this.selectedItem.isEmail === 'Y' && this.selectedItem.isNotify === 'Y') {
      this.isEmail = true;
      this.isNoti = true;
      return;
    }
    if (this.selectedItem.isEmail === 'Y') {
      this.isEmail = true;
      this.isNoti = false;
      this.formUpdate.controls['shortContent'].setValue('');
      this.formUpdate.controls['notiLink'].setValue('');
    }
    if (this.selectedItem.isNotify === 'Y') {
      this.isNoti = true;
      this.isEmail = false;
      this.formUpdate.controls['fromAddress'].setValue('');
      this.formUpdate.controls['fromPassword'].setValue('');
      this.formUpdate.controls['fromAddress'].setErrors(null);
      this.formUpdate.controls['fromPassword'].setErrors(null);
      this.formUpdate.controls['mailCC'].setValue('');
      this.formUpdate.controls['mailBCC'].setValue('');
    }
  }

  override ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
