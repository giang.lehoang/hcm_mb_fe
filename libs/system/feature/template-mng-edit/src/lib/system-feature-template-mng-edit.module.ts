import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditTemplateComponent } from './edit-template/edit-template.component';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { TranslateModule } from '@ngx-translate/core';
import { NzFormModule } from 'ng-zorro-antd/form';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { QuillModule } from 'ngx-quill';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedDirectivesUppercaseInputModule } from '@hcm-mfe/shared/directives/uppercase-input';

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, SharedUiMbButtonModule, TranslateModule, NzFormModule, SharedUiMbSelectModule,
    SharedUiMbInputTextModule, NzInputModule, NzIconModule, NzModalModule, QuillModule, FormsModule, ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: EditTemplateComponent
      }
    ]), SharedDirectivesUppercaseInputModule
  ],
  declarations: [EditTemplateComponent],
  exports: [EditTemplateComponent]
})
export class SystemFeatureTemplateMngEditModule {}
