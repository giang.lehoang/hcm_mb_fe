import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResourcesCategoryComponent } from './resources-category/resources-category.component';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { TranslateModule } from '@ngx-translate/core';
import { NzTableModule } from 'ng-zorro-antd/table';
import { SharedUiMbButtonIconModule } from '@hcm-mfe/shared/ui/mb-button-icon';
import { SharedUiMbImportModule } from '@hcm-mfe/shared/ui/mb-import';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedDirectivesTrimInputModule } from '@hcm-mfe/shared/directives/trim-input';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';

@NgModule({
    imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedUiLoadingModule, SharedUiMbButtonModule,
        TranslateModule, NzTableModule, SharedUiMbButtonIconModule, SharedUiMbImportModule, NzFormModule,
        SharedDirectivesTrimInputModule, NzInputModule,
        RouterModule.forChild([
            {
                path: '',
                component: ResourcesCategoryComponent
            }
        ])
    ],
  declarations: [ResourcesCategoryComponent],
  exports: [ResourcesCategoryComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SystemFeatureResourceCategoryListModule {}
