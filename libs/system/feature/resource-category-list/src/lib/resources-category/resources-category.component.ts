import { Component, OnInit, Injector } from '@angular/core';
import { forkJoin, Subscription } from 'rxjs';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { App, Resource, TreeNode } from '@hcm-mfe/system/data-access/models';
import { FunctionCode, maxInt32 } from '@hcm-mfe/shared/common/enums';
import { Pageable } from '@hcm-mfe/shared/data-access/models';
import { AppService, FunctionService, UrlConstant } from '@hcm-mfe/system/data-access/services';
import { CommonUtils } from '@hcm-mfe/shared/core';

@Component({
  selector: 'app-resources-category',
  templateUrl: './resources-category.component.html',
  styleUrls: ['./resources-category.component.scss'],
})
export class ResourcesCategoryComponent extends BaseComponent implements OnInit {
  subs: Subscription[] = [];
  listData: Array<Resource> = [];
  listApp: Array<App> = [];
  temp: Array<Resource> = [];
  textFilter = '';
  searchForm = this.fb.group({
    code: [''],
    name: [''],
    type: [''],
  });
  paramSearch = {
    size: maxInt32,
    page: 0,
    name: '',
    code: '',
    type: '',
  };
  pageable: Pageable | undefined;
  listOfMap: TreeNode[] = []
  mapOfExpanded: { [key: string]: TreeNode[] } = {};

  isImportData = false;
  urlApiDownloadTemp: string = UrlConstant.IMPORT_FORM.RESOURCE;
  urlApiImport: string = UrlConstant.IMPORT_FORM.RESOURCE_IMPORT;

  constructor(
    injector: Injector,
    private readonly appService: AppService,
    private readonly functionService: FunctionService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ADMIN_FUNCTION}`);
    this.isLoading = true;
  }


  ngOnInit(): void {
    this.initData();
  }

  initData(): void {
    const param = { page: 0, size: maxInt32 };
    const sub = forkJoin([
      this.functionService.searchResourceCategory(param),
      this.appService.searchAppCategory(param),
    ]).subscribe(
      ([listData, listApp]) => {
        this.listApp = listApp.data.content || [];
        this.temp = listData.data.content || [];
        this.listData = listData.data.content || [];
        this.updateFilter();
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }
  trackByFn(index: number, item: TreeNode) {
    return item.id;
  }
  search() {
    this.updateFilter();
  }

  updateFilter() {
    const val = this.textFilter?.trim().toLowerCase();
    this.listData = [];

    // filter our data
    const temp = this.temp.filter((d) => {
      return d.code?.toLowerCase().indexOf(val) !== -1
        || d.name?.toLowerCase().indexOf(val) !== -1
        || (d.uriOfResource?.toLowerCase().indexOf(val) !== -1 && d.uriOfResource)
        || !val;
    });

    const listResult = [...temp];
    CommonUtils.sortResource(listResult, 'indexNumber', 'code');
    // update the rows
    this.listData = listResult;
    this.listOfMap = [];
    this.mapOfExpanded = {};
    this.listOfMap = CommonUtils.convertToTree(this.listData, 'sortResource', ['indexNumber', 'code'], false);
    CommonUtils.mapOfExpand(this.listOfMap, this.mapOfExpanded, true);
  }

  create() {
    if (this.objFunction?.create === true) {
      this.router.navigate([this.router.url, 'create']);
    }
  }

  update(item: Resource | any) {
    this.isLoading = true;
    const sub = this.functionService.getResourceCategoryById(item.id).subscribe(() => {
      this.isLoading = false;
      this.router.navigate([this.router.url, 'update'], { queryParams: { id: item.id } });
    }, (e) => {
      this.toastrCustom.error(
        this.translate.instant('common.notification.error') + `: ${e?.message}`
      );
      this.isLoading = false;
    });
    this.subs.push(sub);
  }

  deleteItem(item: Resource | any) {
    this.deletePopup.showModal(() => {
      this.isLoading = true;
      const subDelete = this.functionService.deleteResourceCategory(item.id).subscribe((res) => {
          const ids: string[] = res.data.ids;
          const temp = this.temp.filter((itemOld: any) => {
            return !ids.includes(itemOld.id);
          });
          this.temp = temp;
          this.updateFilter();
          this.toastrCustom.success(this.translate.instant('common.notification.deleteSuccess'));
          this.isLoading = false;
        }, (e) => {
          this.toastrCustom.error(
            this.translate.instant('common.notification.deleteError') + `: ${e?.message}`
          );
          this.isLoading = false;
        }
      );
      this.subs.push(subDelete);
    });
  }

  generateAppName(type: any) {
    if (this.listApp && this.listApp.length > 0) {
      for (const app of this.listApp) {
        if (type === app.appId) {
          return app.appName;
        }
      }
    }
    return '';
  }

  collapses(array: TreeNode[], data: TreeNode, $event: boolean): void {
    CommonUtils.collapses(array, data, $event, true);
  }

  doImportData() {
    this.isImportData = true;
  }

  doCloseImport(isSearch: boolean) {
    this.isImportData = false;
    isSearch && this.initData();
  }

  override ngOnDestroy() {
    this.deletePopup?.modalDelete?.destroy();
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
