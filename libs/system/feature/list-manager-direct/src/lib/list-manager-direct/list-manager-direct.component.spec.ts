import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListManagerDirectComponent } from './list-manager-direct.component';

describe('ListManagerDirectComponent', () => {
  let component: ListManagerDirectComponent;
  let fixture: ComponentFixture<ListManagerDirectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListManagerDirectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListManagerDirectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
