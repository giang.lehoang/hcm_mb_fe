import { Component, OnInit, Injector, AfterViewInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { User } from '@hcm-mfe/system/data-access/models';
import { userConfig } from '@hcm-mfe/shared/common/constants';
import { Pageable } from '@hcm-mfe/shared/data-access/models';
import { UserService } from '@hcm-mfe/system/data-access/services';
import { ValidateService } from '@hcm-mfe/shared/core';
import { FunctionCode, SessionKey } from '@hcm-mfe/shared/common/enums';
import { cleanDataForm } from '@hcm-mfe/shared/common/utils';

interface Dropdown {
  code: string | number,
  name: string
}
@Component({
  selector: 'app-users-config-list',
  templateUrl: './uc-list.component.html',
  styleUrls: ['./uc-list.component.scss'],
})
export class UcListComponent extends BaseComponent implements OnInit, AfterViewInit {
  subs: Subscription[] = [];
  listData: Array<User> = [];
  textFilter = '';
  limit = userConfig.pageSize;
  searchForm = this.fb.group({
    username: '',
    fullName: '',
    email: '',
    flagStatus: null,
    isTracing: null,
  });
  paramSearch = {
    size: this.limit,
    page: 0,
    searchAdvanced: '',
  };
  listStatus: Array<Dropdown> = [];
  listTracing:Array<Dropdown> = [];
  pageable: Pageable = new Pageable();
  isSyncUser = false;
  jobId: string | undefined;
  interval: any;
  readonly messageError = 'common.notification.error';
  readonly syncSuccess = 'system.usersConfig.notification.syncSuccess';
  readonly SUCCEEDED = 'SUCCEEDED';

  constructor(
    injector: Injector,
    private readonly userService: UserService,
    public validateService: ValidateService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ADMIN_USER}`);
    this.jobId = localStorage.getItem(SessionKey.SYNC_USER) ?? undefined;
    this.checkSyncStatus(this.jobId);
  }

  ngOnInit(): void {
    this.search();
    this.listStatus.push({ code: 1, name: this.translate.instant('system.usersConfig.label.attributes.flagStatus') });
    this.listStatus.push({ code: 0, name: this.translate.instant('system.usersConfig.label.attributes.unFlagStatus') });
    this.listTracing.push({ code: 'Y', name: this.translate.instant('system.usersConfig.label.attributes.tracing') });
    this.listTracing.push({ code: 'N', name: this.translate.instant('system.usersConfig.label.attributes.unTracing') });
  }

  ngAfterViewInit() {
    this.ref.detectChanges();
  }

  override triggerSearchEvent(): void {
    this.search();
  }

  search($event?:any) {
    this.isLoading = true;
    this.paramSearch.page = 0;
    if ($event) {
      this.paramSearch.page = $event - 1;
    }
    const params = cleanDataForm(this.searchForm);
    if(!params.username) {
      delete params.username;
    }
    if(!params.fullName) {
      delete params.fullName;
    }
    if(!params.email) {
      delete params.email;
    }
    const paramsNew = { ...params, page: this.paramSearch.page, size: this.limit };
    if(paramsNew.flagStatus == null) {
      delete paramsNew.flagStatus;
    }
    if(!paramsNew.isTracing) {
      delete paramsNew.isTracing;
    }
    const sub = this.userService.searchUser(paramsNew).subscribe((result) => {
        result?.data?.content?.forEach((el: any) => {
          if(el.flagStatus === 1) {
            el.flagStatus = true;
          } else {
            el.flagStatus = false;
          }
          if(el.isTracing === 'Y') {
            el.isTracing = true;
          } else {
            el.isTracing = false;
          }
        });
        this.listData = result?.data?.content || [];

        this.pageable.totalElements = result?.data?.totalElements;
        this.pageable.totalPages = result?.data?.totalPages;
        this.pageable.currentPage = result?.data?.number + 1;
        this.pageable.size = this.limit;

        this.isLoading = false;
      }, () => {
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }

  update(item: User) {
    this.isLoading = true;
    const sub =  this.userService.findById(item.userId).subscribe(() => {
      this.isLoading = false;
      this.router.navigate([this.router.url, 'update'], { queryParams: { id: item.userId } });
    }, (e) => {
      this.toastrCustom.error(
        this.translate.instant(this.messageError) + `: ${e?.message}`
      );
      this.isLoading = false;
    });
    this.subs.push(sub);
  }

  isActive(item: User, type: any) {
    if (this.objFunction?.adjusted) {
      this.isLoading = true;
      const data = {
        type: type,
        value: !item.flagStatus ? 'Y' : 'N'
      }
      const subChange = this.userService.changeStatus(item.userId, data).subscribe(() => {
          this.toastrCustom.success(this.translate.instant('common.notification.success'));
          this.isLoading = false;
        }, () => {
          item.flagStatus = !item.flagStatus;
          this.toastrCustom.error(this.translate.instant(this.messageError));
          this.isLoading = false;
        }
      );
      this.subs.push(subChange);
    }
  }

  isTracing(item: User, type: any) {
    if (this.objFunction?.adjusted) {
      this.isLoading = true;
      const data = {
        type: type,
        value: !item.isTracing ? 'Y' : 'N'
      }
      const subChange = this.userService.changeStatus(item.userId, data).subscribe(() => {
          this.toastrCustom.success(this.translate.instant('common.notification.success'));
          this.isLoading = false;
        }, () => {
          item.isTracing = !item.isTracing;
          this.toastrCustom.error(this.translate.instant(this.messageError));
          this.isLoading = false;
        }
      );
      this.subs.push(subChange);
    }
  }

  syncUsersFromKeycloak() {
    this.confirmService.success().then(response => {
      if(response) {
        const syncUsersFromKeycloak = this.userService.syncUsersFromKeycloak().subscribe(res => {
          if(res.data.message) {
            this.toastrCustom.error(res.data.message);
          }
          this.isSyncUser = true;
          const jobId = res.data.id;
          localStorage.setItem(SessionKey.SYNC_USER, jobId);
          this.checkSyncStatus(jobId);
        });
        this.subs.push(syncUsersFromKeycloak);
      }
    })
  }

  checkSyncStatus(jobId: string | undefined) {
    if(jobId) {
      this.interval = setInterval(() => {
        const params = {jobId: jobId};
        const syncStatus = this.userService.getSyncStatus(params).subscribe((res) => {
          const status = res.data;
          if(status === this.SUCCEEDED) {
            this.toastrCustom.success(this.translate.instant(this.syncSuccess));
            localStorage.removeItem(SessionKey.SYNC_USER);
            this.jobId = undefined;
            this.isSyncUser = false;
            clearInterval(this.interval);
          }
        }, (e)=> {
          this.toastrCustom.error(e.message);
          localStorage.removeItem(SessionKey.SYNC_USER);
          this.jobId = undefined;
          this.isSyncUser = false;
          clearInterval(this.interval);
        });
        this.subs.push(syncStatus);
      }, 10000);
    }
  }

  trackByFn(index:number, item: User) {
    return item.userId;
  }

  override ngOnDestroy() {
    clearInterval(this.interval);
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
