import { ChangeDetectorRef, Component, ElementRef, Injector, OnInit, ViewChild } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';;
import { Subscription } from 'rxjs';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { IBaseResponse, INotiGroup, mbDataSelectsAssessmentStatus } from '@hcm-mfe/system/data-access/models';
import { TemplateManagementService } from '@hcm-mfe/system/data-access/services';
import { CustomValidators } from '@hcm-mfe/shared/common/validators';

@Component({
  selector: 'app-create-template',
  templateUrl: './create-template.component.html',
  styleUrls: ['./create-template.component.scss'],
})
export class CreateTemplateComponent extends BaseComponent implements OnInit {
  @ViewChild('passwordInput') input: ElementRef | undefined;
  subs: Subscription[] = [];
  passwordVisible = false;
  errorName: string | undefined;
  toAddress: string | undefined;
  isSubmitted = false;
  isVisible = false;
  isEmail = true;
  isNoti = false;
  selectedItem: INotiGroup | undefined;
  listStatus = mbDataSelectsAssessmentStatus();
  formCreate: FormGroup = this.fb.group({
    ngrId: ['', [Validators.required]],
    ntpCode: ['', [Validators.required, Validators.maxLength(50), CustomValidators.code]],
    ntpName: ['', [Validators.required, Validators.maxLength(250)]],
    subject: ['', [Validators.required, Validators.maxLength(100)]],
    fromAddress: ['', [Validators.required, Validators.maxLength(100)]],
    mailCC: ['', [Validators.maxLength(500), CustomValidators.mailValidator]],
    mailBCC: ['', [Validators.maxLength(200), CustomValidators.mailValidator]],
    toAddress: ['', [Validators.maxLength(200), CustomValidators.mailValidator]],
    fromPassword: [''],
    flagStatus: [this.listStatus[0].value, [Validators.required]],
    content: [''],
    notiLink: ['', [Validators.maxLength(100), CustomValidators.url]],
    pushNotiLink: ['', [Validators.maxLength(100), CustomValidators.url]],
    shortContent: [''],
  });
  modules = {
    toolbar: [
      ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
      ['blockquote', 'code-block'],

      [{ 'header': 1 }, { 'header': 2 }],               // custom button values
      [{ 'list': 'ordered'}, { 'list': 'bullet' }],
      [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
      [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
      [{ 'direction': 'rtl' }],                         // text direction

      [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
      [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

      [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
      [{ 'font': [] }],
      [{ 'align': [] }],

      ['clean'],                                         // remove formatting button

      ['link', 'image']                         // link and image
    ]
  };
  listGroup: INotiGroup[] = [];
  constructor(
    injector: Injector,
    readonly cdref: ChangeDetectorRef,
    readonly templateMngService: TemplateManagementService,
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.toAddress = JSON.parse(localStorage.getItem('CURRENCY_USER') ?? '')?.email;
    const sub = this.templateMngService.getListGroupNoti().subscribe((data: IBaseResponse<INotiGroup[]>) => {
      this.listGroup = data.data;
    });
    this.subs.push(sub);
  }

  ngAfterContentChecked(): void {
    this.cdref.detectChanges();
  }
  sendEmail(data: any): void {
    const sub = this.templateMngService.sendEmail(data).subscribe(
      (res) => {
        this.toastrCustom.success(this.translate.instant('common.notification.sendEmailSuccess'));
        this.isLoading = false;
      },
      (e) => {
        if (e?.message === 'Gửi email thất bại') {
          this.toastrCustom.error(this.translate.instant(e?.message));
        } else {
          this.toastrCustom.error(this.translate.instant('common.notification.sendEmailError') + e?.message);
        }
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }
  selectNotiGroup(value: any): void {
    this.selectedItem = this.listGroup.find((el) => el.ngrId === value);
    if (this.selectedItem && this.selectedItem.isEmail === 'Y' && this.selectedItem.isNotify === 'Y') {
      this.isEmail = true;
      this.isNoti = true;
      return;
    }
    if (this.selectedItem && this.selectedItem.isEmail === 'Y') {
      this.isEmail = true;
      this.isNoti = false;
      this.formCreate.controls['shortContent'].setValue('');
      this.formCreate.controls['notiLink'].setValue('');
    }
    if (this.selectedItem && this.selectedItem.isNotify === 'Y') {
      this.isNoti = true;
      this.isEmail = false;
      this.formCreate.controls['fromAddress'].setValue('');
      this.formCreate.controls['fromPassword'].setValue('');
      this.formCreate.controls['fromAddress'].setErrors(null);
      this.formCreate.controls['fromPassword'].setErrors(null);
      this.formCreate.controls['mailCC'].setValue('');
      this.formCreate.controls['mailBCC'].setValue('');
    }
  }
  createTemplate(data: any): void {
    const sub = this.templateMngService.createTemplate(data).subscribe(
      () => {
        this.toastrCustom.success(this.translate.instant('common.notification.addSuccess'));
        this.isLoading = false;
        this.router.navigateByUrl('/system/template-mng');
      },
      (e) => {
        this.toastrCustom.error(this.translate.instant('common.notification.addError') + e?.message);
        if (e?.message === 'Template code already exist' || e?.message === 'Mã biểu mẫu đã tồn tại') {
          this.errorName = this.translate.instant('system.warning.codeTemplate');
        }
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }
  changeTypeInput() {
    this.passwordVisible = !this.passwordVisible;
    if (this.input) {
      if (this.passwordVisible) {
        this.input.nativeElement.type = 'text';
      } else {
        this.input.nativeElement.type = 'password';
      }
    }
  }
  showModal(): void {
    this.isVisible = true;
  }
  handleCancel() {
    this.isVisible = false;
  }
  handleOk() {
    this.isLoading = true;
    this.isVisible = false;
    const data = {
      fromAddress: this.formCreate.value.fromAddress,
      password: this.formCreate.value.fromPassword,
      toAddress: this.toAddress,
      content: this.formCreate.value.content,
      title: this.formCreate.value.subject,
      encrypt : "false"
    };
    if (CustomValidators.mailValidator(this.formCreate.controls['fromAddress'])) {
      delete data['password'];
      delete data['encrypt'];
    }
    this.sendEmail(data);
  }
  submitForm(): void {
    this.isSubmitted = true;
    const data = this.formCreate.getRawValue();
    if (this.formCreate.valid) {
      this.isLoading = true;
      this.createTemplate(data);
    }
  }
  changeText(): void {
    this.errorName = '';
  }

  override ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
