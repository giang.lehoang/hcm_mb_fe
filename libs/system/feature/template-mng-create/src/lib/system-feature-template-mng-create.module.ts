import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateTemplateComponent } from './create-template/create-template.component';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { NzFormModule } from 'ng-zorro-antd/form';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzInputModule } from 'ng-zorro-antd/input';
import { QuillModule } from 'ngx-quill';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedDirectivesUppercaseInputModule } from '@hcm-mfe/shared/directives/uppercase-input';

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, TranslateModule, SharedUiMbButtonModule, NzFormModule,
    SharedUiMbSelectModule, SharedUiMbInputTextModule, NzIconModule, NzInputModule, QuillModule, NzModalModule,
    FormsModule, ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: CreateTemplateComponent
      }
    ]), SharedDirectivesUppercaseInputModule
  ],
  declarations: [CreateTemplateComponent],
  exports: [CreateTemplateComponent],
})
export class SystemFeatureTemplateMngCreateModule {}
