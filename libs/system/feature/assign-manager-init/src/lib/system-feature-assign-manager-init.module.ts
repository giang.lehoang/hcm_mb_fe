import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AssignManagerInitComponent} from "./assign-manager-init/assign-manager-init.component";
import {NzGridModule} from "ng-zorro-antd/grid";
import {SharedUiMbEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/mb-employee-data-picker";
import {TranslateModule} from "@ngx-translate/core";
import {FormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {NzDividerModule} from "ng-zorro-antd/divider";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
  imports: [CommonModule, NzGridModule, NzCheckboxModule,
    RouterModule.forChild([
      {
        path: '',
        component: AssignManagerInitComponent
      }]), SharedUiMbEmployeeDataPickerModule, TranslateModule, FormsModule, NzDividerModule, SharedUiMbDatePickerModule, SharedUiMbSelectModule, SharedUiMbButtonModule, SharedUiLoadingModule],
  exports: [AssignManagerInitComponent],
  declarations: [AssignManagerInitComponent]
})
export class SystemFeatureAssignManagerInitModule {}
