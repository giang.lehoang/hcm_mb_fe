import { Component, Injector, OnDestroy, OnInit } from '@angular/core';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {AssignRequest, EmployeeDetail, ManagerModel, ProcessManagerDetail} from "@hcm-mfe/system/data-access/models";
import {functionUri, maxInt32} from "@hcm-mfe/shared/common/enums";
import {ManagerDirectService} from "@hcm-mfe/system/data-access/services";
import * as moment from "moment/moment";
import {forkJoin} from "rxjs";

@Component({
  selector: 'app-assign-manager-init',
  templateUrl: './assign-manager-init.component.html',
  styleUrls: ['./assign-manager-init.component.scss']
})
export class AssignManagerInitComponent extends BaseComponent implements OnInit, OnDestroy {

  employee:EmployeeDetail;
  listManager: ManagerModel[]= [];
  managerD: ManagerModel | undefined;
  managerI: ManagerModel | undefined;
  checked:boolean;
  request: AssignRequest;
  submit:boolean;
  fromDate:string | undefined;
  toDate:string | undefined;
  error: string | undefined;

  constructor(injector:Injector,  private readonly managerDirectService: ManagerDirectService) {
    super(injector);
    this.employee = {
      empCode: '',
      empId: 0,
      employeeCode: '',
      employeeId: 0,
      jobName: '',
      fullName: '',
      levelName: '',
      orgName: '',
      positionName: '',
      positionId: ''
    };

    this.request =
    {
      managerProcessId: "",
      employeeId:"",
      employeeFullName:"",
      managerId:"",
      indirectManagerId:"",
      positionId:"",
      fromDate:"",
      toDate:"",
      hasIndirectManager: false
    }
    this.checked = false;
    this.submit = false;
    this.request.managerProcessId = localStorage.getItem('managerDirect');
  }

  ngOnInit(): void {
    this.getListManager();
  }

  onChange(event: EmployeeDetail){
    this.employee = event;
    if(event){
      this.request.employeeId = event.employeeId;
      this.request.employeeFullName = event.fullName;
      this.request.positionId = event.positionId;
    } else {
      this.request.employeeId = '';
      this.request.employeeFullName = '';
      this.request.positionId = '';
    }
  }

  getListManager(){
    const params = {
      keyword:'',
      size: maxInt32
    }
    this.isLoading = true;
    const request = [this.managerDirectService.getListManager(params)];
    if(this.request.managerProcessId){
      request.push( this.managerDirectService.assignManagerDetail(this.request.managerProcessId));
    }

    forkJoin(request).subscribe(data => {
      this.listManager =  data[0].data.listData;
      if(this.request.managerProcessId){
          const detail:ProcessManagerDetail = data[1].data;
          this.employee = {
            empCode: detail.employeeCode,
            empId: detail.employeeId,
            employeeCode: detail.employeeCode,
            employeeId: detail.employeeId,
            jobName: detail.jobName,
            fullName: detail.fullName,
            levelName: detail.employeeLevel,
            orgName: detail.orgName,
            positionName: detail.posName,
            positionId: detail.posId
          };
          this.request.employeeId = this.employee.employeeId;
          this.request.employeeFullName = this.employee.fullName;
          this.request.positionId = this.employee.positionId;
          this.request.managerId = detail.managerId;
          this.request.indirectManagerId = detail.indirectManagerId;
          this.fromDate = new Date(detail.fromDate).toString();
          if(this.toDate){
            this.toDate = new Date(detail.toDate).toString();
          }

          this.listManager.forEach(item => {
            if(detail.managerId === item.employeeId){
              this.managerD = item;
            }

            if(detail.indirectManagerId === item.employeeId){
              this.managerI = item;
            }
          })
      }
      this.isLoading = false;
    }, error => {
      this.isLoading = false;
      this.toastrCustom.error(error?.message);
    });
  }

  findManager(id:string|number):ManagerModel | undefined {
    const data = this.listManager.find(item => item.employeeId === id);
    return data;
  }

  changeEmp($event: string, isDe: boolean) {
    const item = this.findManager($event);
    if(isDe){
      this.managerD = item;
    } else {
      this.managerI = item;
    }

    if(this.request.managerId === this.request.indirectManagerId){
      this.toastrCustom.error(this.translate.instant('system.managerDirect.message.duplicateManager'));
      if(isDe){
        setTimeout(() => {
          this.request.managerId = '';
          this.managerD = undefined;
        }, 500);
      } else {
        setTimeout(() => {
          this.request.indirectManagerId = '';
          this.managerI = undefined;
        }, 500);
      }
    }
  }

  assignManager(){
    this.submit = true;
    if(!this.request.employeeId){
      this.toastrCustom.error(this.translate.instant('system.managerDirect.message.employee'));
      return;
    }

    if(!this.fromDate || !this.request.managerId){
      return;
    }

    if(this.request.hasIndirectManager){
      if(!this.request.indirectManagerId){
        return;
      }
    } else {
      this.request.indirectManagerId = '';
    }


    this.request.fromDate = moment(this.fromDate).format('DD/MM/YYYY');
    if(this.toDate){
      this.request.toDate = moment(this.toDate).format('DD/MM/YYYY');
    }
    this.isLoading = true;
    this.managerDirectService.assignManager(this.request).subscribe(data => {
      this.toastrCustom.success();
      this.isLoading = false;
      this.router.navigate([functionUri.system_manager_direct])
    }, error => {
      this.isLoading = false;
      if(error?.code === 400){
        this.error = error?.message;
      }
      this.toastrCustom.error(error?.message);
    });
  }

  changeDate($event: any, fDate:boolean) {
    if(fDate){
      this.error = '';
    }

    if(this.fromDate && this.toDate){
      const fromDateNum = new Date(this.fromDate).setHours(0, 0, 0, 0);
      const toDateNum = new Date(this.toDate).setHours(0, 0, 0, 0);

      if(fromDateNum >= toDateNum){
        this.toastrCustom.error(this.translate.instant('modelOrganization.message.toDate'));
        setTimeout(() => {
          if (fDate){
            this.fromDate = ''
          } else {
            this.toDate = ''
          }
        })
      }
    }
  }

  override ngOnDestroy() {
    localStorage.removeItem('managerDirect');
  }
}
