import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserPermissionConfigComponent } from './user-permission/user-permission-config.component';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { NzFormModule } from 'ng-zorro-antd/form';
import { TranslateModule } from '@ngx-translate/core';
import { NzInputModule } from 'ng-zorro-antd/input';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { NzCardModule } from 'ng-zorro-antd/card';
import { SharedUiMbImportModule } from '@hcm-mfe/shared/ui/mb-import';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, NzFormModule, TranslateModule, NzInputModule, SharedUiMbButtonModule,
    NzToolTipModule, NzTableModule, NzPaginationModule, NzCardModule, SharedUiMbImportModule, NzDividerModule,
    NzSelectModule, NzModalModule, FormsModule, ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: UserPermissionConfigComponent
      }
    ])
  ],
  declarations: [UserPermissionConfigComponent],
  exports: [UserPermissionConfigComponent],
})
export class SystemFeatureUserPermissionConfigModule {}
