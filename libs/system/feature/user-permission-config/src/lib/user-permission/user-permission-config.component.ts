import { Component, DoCheck, Injector, OnInit } from '@angular/core';
import { forkJoin, Subscription } from 'rxjs';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { userConfig } from '@hcm-mfe/shared/common/constants';
import { Pageable} from '@hcm-mfe/shared/data-access/models';
import {
  IAdmPolicies,
  IDecentralizationAssignments, IDecentralizations, IPostDecentralization,
  IRole,
  IUserPermission, TreeNode
} from '@hcm-mfe/system/data-access/models';
import {UrlConstant, UserPermissionService, UserService} from '@hcm-mfe/system/data-access/services';
import {FunctionCode, maxInt32, SessionKey} from '@hcm-mfe/shared/common/enums';
import { CommonUtils } from '@hcm-mfe/shared/core';

@Component({
  selector: 'app-user-permission-config',
  templateUrl: './user-permission-config.component.html',
  styleUrls: ['./user-permission-config.component.scss'],
})
export class UserPermissionConfigComponent extends BaseComponent implements OnInit, DoCheck {
  subs: Subscription[] = [];
  checked = false;
  isVisible = false;
  titleModal = '';
  limit = userConfig.pageSize;
  isLoadingModal = false;
  pageableUser: Pageable = new Pageable();
  pageablePermission: Pageable = new Pageable();
  paginationUserList: Pageable = new Pageable();
  paginationUserRoleAssignList: Pageable = new Pageable();
  paginationDecentralizationAssignments: Pageable = new Pageable();
  numberOfElementsListUser: number | undefined;
  numberOfElementsRoleAssign: number | undefined;
  numberOfElementsDecentralizationAssignment: number | undefined;
  readonly HCM_MBER = 'HCM-MBER';

  HighlightRowUser: string | undefined;
  HighlightRowPermisson: string | undefined;
  userSelected: IUserPermission | undefined;
  permissionSelected: IRole | undefined;
  isPermissionAll = false;

  isImportData = false;
  urlApiDownloadTemp: string = UrlConstant.IMPORT_FORM.USER_PERMISSION;
  urlApiImport: string = UrlConstant.IMPORT_FORM.USER_PERMISSION_IMPORT;

  paramsUser = {
    page: 0,
    size: this.limit,
    searchAdvanced: '',
  };

  paramSearchUser = {
    size: this.limit,
    page: 0,
    username: '',
    sort: 'username,asc',
  };

  paramRoleAssignUser = {
    first: 0,
    max: this.limit,
    search: '',
  };

  paramDecentralizationAssignmentPagination = {
    first: 0,
    max: this.limit,
    search: '',
  };
  paramDecentralizationAssignment = {
    username: '',
    roleId: '',
    polId: 0,
  };

  searchFormUserName = this.fb.group({
    username: [''],
  });

  searchFormRoleAssignUser = this.fb.group({
    search: [''],
  });

  paramsPermission = {
    first: 0,
    max: this.limit,
    search: '',
  };

  listUser: Array<IUserPermission> = [];
  dataRole: Array<IRole> = [];
  dataRoleList: Array<IRole> = [];
  admPolicies: Array<IAdmPolicies> = [];
  dataDecentralizationAssignments: Array<IDecentralizationAssignments> = [];
  dataDecentralization: Array<IDecentralizationAssignments> = [];

  // filter list item
  rows: any[] = [];

  DataDecentralizationsListPopup: Array<IDecentralizations> = [];
  mapOfExpanded: { [key: string]: TreeNode[] } = {};
  paramDataDecentralizationsListPopup: IPostDecentralization = {
    dataIds: [],
    dataCode: null,
    dataName: null,
    parentId: null,
    dataType: null,
    page: 0,
    size: maxInt32,
  };

  isSearch = {
    roleAssignUser: false,
    branch: false,
  };

  selectedUser: Array<IUserPermission> = [];
  selectedUserRoleAssign: Array<IRole> = [];
  selectedAdmPolicies: IAdmPolicies | undefined;
  selectedAdmPoliciesId: string | undefined;
  selectedDecentralization: IDecentralizationAssignments[] | undefined;
  roleSearch: string | undefined;
  decentralizationSearch: string | undefined;

  listOfMap: TreeNode[] = [];

  permissionSearch: string | undefined;
  treeSearchItem: string | undefined;

  constructor(
    injector: Injector,
    readonly userPermission: UserPermissionService,
    readonly userService: UserService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ADMIN_USER_PERMISSION}`);
  }

  changePageUser() {
    this.paramSearchUser.page = this.paginationUserList?.currentPage - 1;
    this.searchUser(false);
  }

  ngOnInit(): void {
    this.paginationUserList = {
      currentPage: 1,
      totalElements: 50,
      size: 15,
      totalPages: 0,
      numberOfElements: 0,
    };
    this.searchUser(false);
  }
  ngDoCheck(): void {
    this.checked = this.DataDecentralizationsListPopup.every((item) => item.checked === true);
  }
  compareFn = (o1: any, o2: any): boolean => (o1 && o2 ? o1.polId === o2.polId : o1 === o2);

  searchUser(isSearch: boolean) {
    this.isLoading = true;
    const searchValueUser = this.searchFormUserName.getRawValue();
    this.selectedAdmPoliciesId = '';
    this.paramSearchUser.username = searchValueUser.username.length > 0 ? searchValueUser.username.trim() : '';
    this.permissionSearch = '';
    if (isSearch) {
      this.paramSearchUser.page = 0;
    }
    this.paramsPermission.first = 0;
    this.paramsPermission.max = this.limit;
    this.paramsPermission.search = this.permissionSearch;
    this.pageablePermission = new Pageable();
    const sub = forkJoin([
      this.userService.search(this.paramSearchUser),
      this.userPermission.getAdmPolicies({ page: 0, size: maxInt32 }),
    ]).subscribe(
      ([users, admPolicies]) => {
        this.listUser = users?.data.content || [];
        this.admPolicies = admPolicies?.data || [];
        this.paginationUserList.totalElements = users.data.totalElements;
        this.paginationUserList.totalPages = users.data.totalPages;
        this.paginationUserList.size = this.limit;
        this.paginationUserList.numberOfElements = users.data.numberOfElements;
        this.numberOfElementsListUser = users.data.numberOfElements;
        this.selectedUserRoleAssign = [];
        this.dataDecentralizationAssignments = [];
        this.dataDecentralization = [];

        if (this.listUser?.length === 0) {
          this.selectedUser = [];
          this.dataRole = [];
          this.paginationUserRoleAssignList.totalElements = 0;
          this.isLoading = false;
        } else {
          this.selectedUser = [this.listUser[0]];
          this.userSelected = this.selectedUser[0];
          this.HighlightRowUser = this.userSelected.userId;
          this.HighlightRowPermisson = undefined;
          this.roleAssignUser();
        }
      },
      () => {
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }

  searchRole(paginationName: any) {
    if (this.dataRoleList.length > 0 && paginationName === 'dataRole') {
      this.paramRoleAssignUser.first =
        (this.paginationUserRoleAssignList?.currentPage - 1) * this.paginationUserRoleAssignList.size;
      const totalCount = this.dataRoleList.length || 0;

      this.paginationUserRoleAssignList.totalElements = totalCount;
      this.paginationUserRoleAssignList.totalPages = Math.floor(totalCount / this.limit);
      this.paginationUserRoleAssignList.currentPage = Math.floor(this.paramRoleAssignUser.first / this.limit + 1);
      this.paginationUserRoleAssignList.size = this.limit;

      this.dataRole = this.dataRoleList.slice(
        this.paramRoleAssignUser.first,
        this.paramRoleAssignUser.max + this.paramRoleAssignUser.first
      );
    } else if (this.dataDecentralizationAssignments.length > 0 && paginationName === 'decentralization') {
      this.paramDecentralizationAssignmentPagination.first =
        (this.paginationDecentralizationAssignments?.currentPage - 1) * this.paginationDecentralizationAssignments.size;
      const totalCount = this.dataDecentralizationAssignments.length || 0;

      this.paginationDecentralizationAssignments.totalElements = totalCount;
      this.paginationDecentralizationAssignments.totalPages = Math.floor(totalCount / this.limit);
      this.paginationDecentralizationAssignments.currentPage = Math.floor(this.paramDecentralizationAssignmentPagination.first / this.limit + 1);
      this.paginationDecentralizationAssignments.size = this.limit;

      this.dataDecentralization = this.dataDecentralizationAssignments.slice(
        this.paramDecentralizationAssignmentPagination.first,
        this.paramDecentralizationAssignmentPagination.max + this.paramDecentralizationAssignmentPagination.first
      );
    }
  }

  selectAdmPolicie(value: any) {
    if (!value) {
      this.selectedAdmPolicies = undefined;
      return;
    }
    if (this.admPolicies.length > 0) {
      this.dataDecentralizationAssignments = [];
      this.dataDecentralization = [];
      this.admPolicies.forEach((el) => {
        if (el.polId === Number(value)) {
          this.titleModal = el.polName;
          this.selectedAdmPolicies = el;
        }
      });
      this.decentralizationAssignt();
    }
  }

  ClickedRowUser(event: any, dataSelect: any, table: any) {
    if (table === 'user') {
      if (this.paginationDecentralizationAssignments) {
        this.paginationDecentralizationAssignments.totalElements = 0;
      }
      this.HighlightRowUser = dataSelect.userId;
      this.userSelected = dataSelect;
      this.selectedAdmPoliciesId = '';
      this.HighlightRowPermisson = undefined;
      this.selectedUserRoleAssign = [];
      this.dataDecentralization = [];
      this.selectedAdmPolicies = undefined;
      this.roleAssignUser();
    } else if (table === 'permission') {
      if (this.paginationDecentralizationAssignments) {
        this.paginationDecentralizationAssignments.totalElements = 0;
      }
      this.HighlightRowPermisson = dataSelect.id;
      this.permissionSelected = dataSelect;
      this.selectedUserRoleAssign = [dataSelect];
      this.selectedAdmPolicies = undefined;
      this.listOfMap = [];
      this.dataDecentralization = [];
      this.decentralizationAssignt();
      if (!CommonUtils.isNullOrEmpty(this.selectedAdmPoliciesId)) {
        this.selectAdmPolicie(this.selectedAdmPoliciesId);
      }
    }
  }

  decentralizationAssignt() {
    if (this.permissionSelected?.id && this.userSelected?.username && this.selectedAdmPolicies?.polId) {
      this.isLoading = true;
      this.paramDecentralizationAssignmentPagination.first = 0;
      this.paramDecentralizationAssignmentPagination.max = this.limit;
      this.paramDecentralizationAssignment.username = this.userSelected.username;
      this.paramDecentralizationAssignment.roleId = this.permissionSelected.id;
      this.paramDecentralizationAssignment.polId = this.selectedAdmPolicies.polId;
      const sub = this.userPermission.getDecentralizationUser(this.paramDecentralizationAssignment).subscribe(
        (response) => {
          const data = response.data || [];
          this.dataDecentralizationAssignments = data;
          const totalCount = response.data.length || 0;
          this.numberOfElementsDecentralizationAssignment = Math.floor(this.dataDecentralizationAssignments.length);

          this.paginationDecentralizationAssignments.totalElements = totalCount;
          this.paginationDecentralizationAssignments.totalPages = Math.floor(totalCount / this.limit);
          this.paginationDecentralizationAssignments.currentPage = Math.floor(this.paramRoleAssignUser.first / this.limit + 1);
          this.paginationDecentralizationAssignments.size = this.limit;

          const deepClone = JSON.parse(JSON.stringify(this.dataDecentralizationAssignments));
          this.dataDecentralization = deepClone.slice(
            this.paramDecentralizationAssignmentPagination.first,
            this.paramDecentralizationAssignmentPagination.max
          );
          if (this.dataDecentralization?.length === 0) {
            this.selectedDecentralization = [];
          } else {
            this.selectedDecentralization = [this.dataDecentralization[0]];
          }
          this.isLoading = false;
        },
        () => {
          this.isLoading = false;
        }
      );
      this.subs.push(sub);
    }
  }

  searchDataRole(isSearch: boolean) {
    this.isLoading = true;
    if (isSearch) {
      this.paramRoleAssignUser.first = 0;
      this.paramRoleAssignUser.max = this.limit;
      if (this.roleSearch?.trim()) {
        this.paramRoleAssignUser.search = this.roleSearch?.trim();
      }
    }
    const dataResult = this.dataRoleList?.filter((item) => {
      return (
        item?.name?.toLowerCase().includes(this.paramRoleAssignUser.search?.toLowerCase()) ||
        item?.description?.toLowerCase().includes(this.paramRoleAssignUser.search?.toLowerCase()) ||
        !this.paramRoleAssignUser.search
      );
    });

    const totalSize = dataResult?.length || 0;
    this.paginationUserRoleAssignList = {
      totalElements: totalSize,
      totalPages: Math.floor(totalSize / this.limit),
      currentPage: Math.floor(this.paramRoleAssignUser.first / this.limit + 1),
      numberOfElements: Math.floor(dataResult.length),
      size: this.limit,
    };
    // clean old table
    this.HighlightRowPermisson = undefined;
    this.selectedAdmPoliciesId = '';
    this.selectedUserRoleAssign = [];
    this.selectedAdmPolicies = undefined;
    this.dataDecentralization = [];

    this.dataRole = dataResult || [];
    this.isLoading = false;
  }

  searchDataDecentralization(isSearch: boolean) {
    this.isLoading = true;
    if (isSearch) {
      this.paramDecentralizationAssignmentPagination.first = 0;
      this.paramDecentralizationAssignmentPagination.max = this.limit;
      if (this.decentralizationSearch?.trim()) {
        this.paramDecentralizationAssignmentPagination.search = this.decentralizationSearch?.trim();
      }
    }
    const dataResult = this.dataDecentralizationAssignments?.filter((item) => {
      return (
        item?.name?.toLowerCase().includes(this.paramDecentralizationAssignmentPagination.search.toLowerCase()) ||
        item?.userName?.toLowerCase().includes(this.paramDecentralizationAssignmentPagination.search.toLowerCase()) ||
        !this.paramDecentralizationAssignmentPagination.search
      );
    });
    const totalSize = dataResult?.length || 0;
    this.paginationDecentralizationAssignments = {
      totalElements: totalSize,
      totalPages: Math.floor(totalSize / this.limit),
      currentPage: Math.floor(this.paramDecentralizationAssignmentPagination.first / this.limit + 1),
      numberOfElements: Math.floor(dataResult.length),
      size: this.limit,
    };
    this.dataDecentralization = dataResult || [];
    this.isLoading = false;
  }

  roleAssignUser() {
    if (this.userSelected?.username) {
      const searchValue = this.searchFormRoleAssignUser.getRawValue();
      this.isLoading = true;
      if (this.isSearch.roleAssignUser) {
        this.paramRoleAssignUser.first = 0;
        this.paramRoleAssignUser.max = this.limit;
        this.paramRoleAssignUser.search = searchValue.search.length > 0 ? searchValue.search.trim() : '';
      }

      const sub = this.userService.getRolesByUserIncludeRoleAll(this.userSelected?.userId).subscribe(
        (response) => {
          const data = response.data || [];
          this.dataRoleList = [...data.filter((el: any) => el.name !== this.HCM_MBER)];
          const totalCount = this.dataRoleList?.length || 0;
          this.numberOfElementsRoleAssign = Math.floor(this.dataRoleList.length);

          this.paginationUserRoleAssignList.totalElements = totalCount;
          this.paginationUserRoleAssignList.totalPages = Math.floor(totalCount / this.limit);
          this.paginationUserRoleAssignList.currentPage = Math.floor(this.paramRoleAssignUser.first / this.limit + 1);
          this.paginationUserRoleAssignList.size = this.limit;

          const deepClone = JSON.parse(JSON.stringify(this.dataRoleList));
          this.dataRole = deepClone.slice(this.paramRoleAssignUser.first, this.paramRoleAssignUser.max);
          this.isLoading = false;
        },
        () => {
          this.isLoading = false;
        }
      );
      this.subs.push(sub);
    }
  }

  showModal(): void {
    this.isVisible = true;
    if (this.selectedAdmPolicies) {
      this.paramDataDecentralizationsListPopup.dataType = this.selectedAdmPolicies.polCode;
      this.isLoadingModal = true;
      const sub = this.userPermission.getDataDecentralizations(this.paramDataDecentralizationsListPopup).subscribe(
        (listData) => {
          this.DataDecentralizationsListPopup = listData.data.content || [];
          this.updateFilterTreeNode();
          this.isLoadingModal = false;
        },
        () => {
          this.isLoadingModal = false;
        }
      );
      this.subs.push(sub);
    } else {
      this.DataDecentralizationsListPopup = [];
      this.listOfMap = [];
      this.isLoadingModal = false;
    }
  }

  updateFilterTreeNode() {
    this.convertToTree(this.DataDecentralizationsListPopup);
    this.mapOfExpand(false);
    this.checkedParent();
  }

  checkedParent() {
    this.dataDecentralizationAssignments.forEach(el => {
      this.listOfMap.forEach(node => {
        const treeNode = this.findNodeById(node, el);
        let checked = true;
        if(treeNode?.children.length > 0) {
          treeNode.children.forEach((item: any) => {
            if(!this.dataDecentralizationAssignments.map(e => `${e.objId}-${e.dataType}`).includes(`${item.dataId}-${item.dataType}`)) {
              checked = false;
            }
          });
          if(checked) {
            const targetChild: any = this.mapOfExpanded[node.key].find((a) => a.key === treeNode.key);
            treeNode.parentChecked = checked;
            targetChild.parentChecked = checked;
          }
        }
      });
    });
  }

  findNodeById(tree: TreeNode, el: IDecentralizationAssignments): any {
    if(el?.objId === +tree?.dataId && el.dataType === tree.dataType) {
      return tree;
    } else {
      return tree.children?.reduce((result, n) => result || this.findNodeById(n, el), undefined);
    }
  }

  convertToTree(paramDataDecentralizations: Array<IDecentralizations>) {
    let listConvert = [...this.DataDecentralizationsListPopup];
    if (paramDataDecentralizations) {
      listConvert = [...paramDataDecentralizations];
    }
    let currentLevel = 1;
    const map: any = {};
    let item: any;
    const listOfMap = [];
    let i;
    for (i = 0; i < listConvert.length; i += 1) {
      map[listConvert[i].dataId] = i;
      listConvert[i].children = [];
    }
    for (i = 0; i < listConvert.length; i += 1) {
      item = listConvert[i];
      item.checked = this.dataDecentralizationAssignments?.findIndex((el) => {
        return el?.objId === +item?.dataId && el.dataType === item.dataType;
      }) > -1;
      if (item.parentId) {
        if (listConvert[map[item.parentId]]) {
          listConvert[map[item.parentId]].parentChecked = false;
          item.level = listConvert[map[item.parentId]].level + 1;
          listConvert[map[item.parentId]].children.push(item);
          listConvert[map[item.parentId]].children = CommonUtils.sort(
            listConvert[map[item.parentId]].children,
            'dataCode'
          );
        } else {
          item.level = 0;
          listOfMap.push(item);
        }
      } else {
        item.level = 0;
        listOfMap.push(item);
      }

      if (item.level === 0) {
        item.key = String(currentLevel);
        currentLevel++;
      }
    }
    this.listOfMap = listOfMap;
    for (const element of this.listOfMap) {
      this.buildTreeDataTable(element.children, element.key);
    }
  }

  buildTreeDataTable(listData: any, parentKey: any) {
    if (!listData) {
      return;
    }
    for (let i = 0; i < listData.length; i++) {
      listData[i].key = parentKey + `-${i + 1}`;
      if (listData[i].children) {
        this.buildTreeDataTable(listData[i].children, listData[i].key);
      }
    }
  }

  mapOfExpand(isExpand: boolean) {
    this.listOfMap.forEach((item) => {
      this.mapOfExpanded[item.key] = this.convertTreeToLists(item, isExpand);
    });
  }

  convertTreeToLists(root: TreeNode, isExpand: boolean): TreeNode[] {
    const stack: TreeNode[] = [];
    const array: TreeNode[] = [];
    const hashMap = {};
    stack.push({ ...root, level: 0, expand: isExpand, expandCustom: false });

    while (stack.length !== 0) {
      const node: any = stack.pop();
      this.visitNodes(node, hashMap, array);
      if (node.children) {
        for (let i = node.children.length - 1; i >= 0; i--) {
          stack.push({ ...node.children[i], level: node.level + 1, expand: isExpand, parent: node, expandCustom: false });
        }
      }
    }

    return array;
  }

  collapses(array: TreeNode[], data: TreeNode, $event: boolean, isExpanded: boolean): void {
    if(isExpanded) {
      const node: any = array.find((a: any) => a.key === data.key);
      node.expandCustom = $event;
    }
    if (!$event) {
      if (data.children) {
        data.children.forEach(d => {
          const item: any = array.find(a => a.key === d.key);
          item.expand = false;
          this.collapses(array, item, false, false);
        });
      } else {
        return;
      }
    } else {
      if (data.children) {
        data.children.forEach((d) => {
          const item: any = array.find(a => a.key === d.key);
          if(item.expandCustom) {
            item.expand = item.expandCustom;
          } else {
            item.expand = false;
          }
          this.collapses(array, item, item.expand, false);
        });
      }
    }
  }

  onCheckboxDecentralizationFn(array: TreeNode[], data: TreeNode, $event: boolean): void {
    this.checked = false;
    this.setChecked($event, data);
    if (data.children && data.children.length > 0) {
      this.onChangeCheckedParent(array, data, this.getChecked(array, data.children) && $event);
    } else {
      if (data.parent) {
        const parentTree: any = array.find((a) => a.key === data.parent?.key);
        this.onChangeCheckedParent(array, parentTree, this.getChecked(array, parentTree.children) && $event && parentTree.checked);
      }
    }
  }

  getChecked(array: any, children: any): boolean {
    let checked = true;
    for (const item of children) {
      const targetChild = array.find((a: any) => a.key === item.key);
      if (!targetChild.checked) {
        checked = false;
        break;
      }
    }
    return checked;
  }

  onChangeCheckedParent(array: TreeNode[], data: TreeNode, $event: boolean) {
    const target: any = array.find((a) => a.key === data.key);
    target.parentChecked = $event;
    if (target.parent) {
      const parentTree: any = array.find((a) => a.key === target.parent.key);
      this.onChangeCheckedParent(array, parentTree, $event && this.getChecked(array, parentTree.children) && parentTree.checked);
    }
  }

  onCheckboxDecentralizationFnParent(array: TreeNode[], data: TreeNode, $event: boolean): void {
    this.onChangeCheckedParent(array, data, $event);
    this.checked = false;
    data.checked = $event;
    data.parentChecked = $event;
    this.setChecked($event, data);
    if (data.children) {
      data.children.forEach((d) => {
        const target: any = array.find((a) => a.key === d.key);
        this.setChecked($event, d);
        d.checked = $event;
        target.checked = $event;
        this.onCheckboxDecentralizationFnParent(array, target, $event);
      });
    } else {
      if (!$event) {
        return;
      }
    }
  }

  setChecked(event: any, data: any) {
    this.DataDecentralizationsListPopup.forEach((el) => {
      if (el.dataId === data.dataId) {
        el.checked = event;
      }
    });
  }

  generateAppName(type: any) {
    if (this.DataDecentralizationsListPopup && this.DataDecentralizationsListPopup.length > 0) {
      for (const app of this.DataDecentralizationsListPopup) {
        if (type === app.dataType) {
          return app.dataName;
        }
      }
    }
    return '';
  }

  visitNodes(node: TreeNode, hashMap: { [key: string]: boolean }, array: TreeNode[]): void {
    if (!hashMap[node.key]) {
      hashMap[node.key] = true;
      array.push(node);
    }
  }

  handleOk(): void {
    if (this.userSelected && this.selectedAdmPolicies && this.selectedUserRoleAssign.length > 0) {
      this.isLoadingModal = true;
      const body = {
        userName: this.userSelected.username,
        polId: this.selectedAdmPolicies.polId,
        roleId: this.selectedUserRoleAssign[0].id,
        dataList: this.DataDecentralizationsListPopup.map((el) => {
          if (el.checked) {
            return {
              objId: el.dataId,
              objValue: el.dataCode,
            };
          } else {
            return null;
          }
        }).filter((el) => el !== null),
        page: 0,
        size: maxInt32,
      };
      const sub = this.userPermission.postDecentralizationUser({ ...body }).subscribe(
        (listData) => {
          this.decentralizationAssignt();
          this.isLoadingModal = false;
          this.isVisible = false;
          this.treeSearchItem = '';
          this.decentralizationSearch = '';
          this.listOfMap = [];
          this.toastrCustom.success(this.translate.instant('common.notification.updateSuccess'));
        },
        (e) => {
          if (e?.error?.description) {
            this.toastrCustom.error(this.translate.instant('common.notification.addError'));
          } else {
            this.toastrCustom.error(this.translate.instant('common.notification.updateError'));
          }
          this.isLoadingModal = false;
        }
      );
      this.subs.push(sub);
    }
  }

  handleCancel(): void {
    this.treeSearchItem = '';
    this.listOfMap = [];
    this.isVisible = false;
  }

  onAllChecked(value: any): void {
    this.listOfMap.forEach(el => {
      const target: any = this.mapOfExpanded[el.key].find((a) => a.key === el.key);
      target.checked = value;
      this.onCheckboxDecentralizationFnParent(this.mapOfExpanded[el.key], el, value);
    })
  }
  updateFilter() {
    const val: any = this.treeSearchItem?.trim().toLowerCase();
    let deepClone = JSON.parse(JSON.stringify(this.DataDecentralizationsListPopup));
    if (val === '' || val === null) {
      this.convertToTree([...this.DataDecentralizationsListPopup]);
      this.mapOfExpand(false);
      this.checkedParent();
      return;
    }
    this.rows = [];
    const temp = this.DataDecentralizationsListPopup.filter(function (d) {
      return (d.dataName && d.dataName?.toLowerCase().indexOf(val) !== -1) || (d.dataCode && d.dataCode?.toLowerCase().indexOf(val) !== -1) || !val;
    });
    let listResult: IDecentralizations[] = [];
    listResult = [...temp];
    temp.forEach((item) => {
      listResult.push(item, ...this.findParentID(item, this.DataDecentralizationsListPopup, []));
    });
    listResult = [...new Set(listResult)];
    CommonUtils.sort(listResult, 'dataCode');
    this.rows = listResult;
    this.listOfMap = this.rows;
    deepClone = listResult;
    this.convertToTree(deepClone);
    this.mapOfExpand(true);
    this.checkedParent();
  }

  findParentID(item: IDecentralizations, listAll: IDecentralizations[], listResult: IDecentralizations[]) {
    if (item.parentId) {
      const itemOld = listAll.find((obj) => {
        return obj.dataId === item.parentId;
      });
      if (itemOld) {
        itemOld.children = [];
        listResult.push(itemOld);
        this.findParentID(itemOld, listAll, listResult);
      }
    }
    return listResult;
  }

  doImportData() {
    this.isImportData = true;
  }

  doCloseImport(isReSearch: boolean) {
    this.isImportData = false;
    if(isReSearch) {
      this.paginationUserList = {
        currentPage: 1,
        totalElements: 50,
        size: 15,
        totalPages: 0,
        numberOfElements: 0,
      };
      this.searchUser(false);
    }
  }

  trackByUser(index: number, item: IUserPermission) {
    return item.userId;
  }

  trackByRole(index: number, item: IRole) {
    return item.id;
  }

  trackByNode(index: number, item: TreeNode) {
    return item.id;
  }

  trackByDecentralization(index: number, item: IDecentralizationAssignments) {
    return item.assId;
  }

  override ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
