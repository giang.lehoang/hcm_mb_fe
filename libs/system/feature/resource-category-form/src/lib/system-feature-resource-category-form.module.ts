import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResourceFormComponent } from './resource-form/resource-form.component';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { NzFormModule } from 'ng-zorro-antd/form';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { TranslateModule } from '@ngx-translate/core';
import { SystemUiChooseFunctionModule } from '@hcm-mfe/system/ui/choose-function';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { SharedUiMbSelectCheckAbleModule } from '@hcm-mfe/shared/ui/mb-select-check-able';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedDirectivesUppercaseInputModule } from '@hcm-mfe/shared/directives/uppercase-input';
import {NzInputModule} from "ng-zorro-antd/input";
import {SharedDirectivesSpecialApiInputModule} from "@hcm-mfe/shared/directives/special-api-input";

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedUiLoadingModule, NzFormModule, SharedUiMbSelectModule,
    TranslateModule, SystemUiChooseFunctionModule, SharedUiMbInputTextModule, SharedUiMbSelectCheckAbleModule,
    NzSwitchModule, NzIconModule, SharedUiMbButtonModule,
    RouterModule.forChild([
      {
        path: '',
        component: ResourceFormComponent
      }
    ]), SharedDirectivesUppercaseInputModule, NzInputModule, SharedDirectivesSpecialApiInputModule
  ],
  declarations: [ResourceFormComponent],
  exports: [ResourceFormComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SystemFeatureResourceCategoryFormModule {}
