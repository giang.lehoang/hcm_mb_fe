import { Component, OnInit, Injector } from '@angular/core';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { forkJoin, Subscription } from 'rxjs';
import { App, Resource, Scope, SelectCheckAbleModal } from '@hcm-mfe/system/data-access/models';
import { CustomValidators } from '@hcm-mfe/shared/common/validators';
import { FormArray, FormGroup, Validators } from '@angular/forms';
import { AppService, FunctionService, ManipulationService } from '@hcm-mfe/system/data-access/services';
import { maxInt32 } from '@hcm-mfe/shared/common/enums';
import { cleanDataForm, validateAllFormFields } from '@hcm-mfe/shared/common/utils';
@Component({
  selector: 'resource-category-form',
  templateUrl: './resource-form.component.html',
  styleUrls: ['./resource-form.component.scss'],
})
export class ResourceFormComponent extends BaseComponent implements OnInit {
  subs: Subscription[] = [];
  isUpdate = false;
  isSubmitted = false;
  data: Resource = {};
  listApp: Array<App> = [];
  listResourceParent: Array<Resource> = [];
  listManipulation: Array<Scope> = [];
  listUris: Array<string> = [];
  form = this.fb.group({
    id: [''],
    name: ['', [CustomValidators.required, Validators.maxLength(150)]],
    code: ['', [CustomValidators.required, CustomValidators.code, Validators.maxLength(50)]],
    displayName: ['', Validators.maxLength(150)],
    type: ['', CustomValidators.required],
    iconUri: ['', Validators.maxLength(255)],
    uri: ['', [CustomValidators.required, Validators.maxLength(255), CustomValidators.url]],
    parentId: [''],
    isMenu: [''],
    isInternet: [''],
    scopes: [[]],
    uriOfResources: [''],
    indexNumber: [
      '',
      [CustomValidators.required, CustomValidators.onlyNumber, Validators.min(0), Validators.maxLength(20)],
    ],
    api: [''],
    apis: this.fb.array([]),
  });
  parentId = '';
  id: string | undefined;
  constructor(
    injector: Injector,
    private readonly functionService: FunctionService,
    private readonly manipulationService: ManipulationService,
    private readonly appService: AppService
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.isLoading = true;
    const subRoute = this.route.queryParams.subscribe((params: any) => {
      this.id = params.id;
    });
    this.subs.push(subRoute);
    const functionId = this.id;
    this.getData();
    if (functionId) {
      this.isUpdate = true;
      this.getDetailById(functionId);
    }
  }

  getData() {
    const param = {
      page: 0,
      size: maxInt32,
    };
    const sub = forkJoin([
      this.functionService.searchResourceCategory(param),
      this.manipulationService.searchScope(param),
      this.appService.searchAppCategory(param),
    ]).subscribe(
      ([listResourceParent, listManipulation, listApp]) => {
        this.data.isMenu = false;
        this.data.isInternet = false;
        listApp?.data?.content?.sort((a: any, b: any) => {
          if (a.appName?.toLowerCase() < b.appName?.toLowerCase()) {
            return -1;
          }
          if (a.appName?.toLowerCase() > b.appName?.toLowerCase()) {
            return 1;
          }
          return 0;
        });
        this.listApp = listApp.data.content;
        this.listResourceParent = listResourceParent.data.content;
        listManipulation?.data?.content?.sort((a: any, b: any) => {
          if (a.name?.toLowerCase() < b.name?.toLowerCase()) {
            return -1;
          }
          if (a.name?.toLowerCase() > b.name?.toLowerCase()) {
            return 1;
          }
          return 0;
        });
        this.listManipulation = listManipulation.data.content;
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }

  getDetailById(functionId: string) {
    this.functionService.getResourceCategoryById(functionId).subscribe(resource => {
      this.data = resource.data;
      if (this.data.isMenu === 'Y') {
        this.data.isMenu = true;
      } else {
        this.data.isMenu = false;
      }
      if (this.data.isInternet === 'Y') {
        this.data.isInternet = true;
      } else {
        this.data.isInternet = false;
      }
      this.form.patchValue(this.data);
      if (this.data && this.data.parentId) {
        this.parentId = this.data.parentId;
      }
      if (this.data?.uriOfResources && this.data.uriOfResources.length > 0) {
        this.data.uriOfResources.forEach((item, index) => {
          this.apis.push(this.createApi(index, item));
        });
      }
      this.listUris = this.data.uriOfResources || [];
      this.isLoading = false;
    },
    () => {
      this.isLoading = false;
    });
  }

  addUri() {
    const value = this.form.controls['api'].value.trim();
    if (value.length > 0) {
      this.apis.push(this.createApi(this.apis.length, value));
      this.form.controls['api'].setValue('');
    }
  }

  get apis() {
    return this.form.get('apis') as FormArray;
  }

  createApi(index: number, value?: any): FormGroup {
    return this.fb.group({
      id: 'api' + index,
      name:[value ? value : '', CustomValidators.api],
    });
  }

  deleteUri(i: number) {
    this.apis.removeAt(i);
  }

  emitValue(item: SelectCheckAbleModal) {
    this.form.controls['scopes'].setValue(item.listOfSelected)
  }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      this.isLoading = true;
      const formData = cleanDataForm(this.form);
      const data: Resource | any = {};
      Object.keys(formData).forEach((key) => {
        if (key !== 'api' && key !== 'apis') {
          data[key] = formData[key];
        }
      });
      data.uriOfResources = [];
      formData.apis.forEach((api: any) => {
        if (api.name.trim() !== '') {
          data.uriOfResources?.push(api.name);
        }
      });
      if (data.scopes === null) {
        data.scopes = [];
      }
      if (data.isMenu) {
        data.isMenu = 'Y';
      } else {
        data.isMenu = 'N';
      }
      if (data.isInternet) {
        data.isInternet = 'Y';
      } else {
        data.isInternet = 'N';
      }
      data.parentId = this.parentId;
      if (!this.id) {
        const subAdd = this.functionService.createResourceCategory(data).subscribe(() => {
            this.isLoading = false;
            this.back();
            this.toastrCustom.success(this.translate.instant('common.notification.addSuccess'));
          }, (e) => {
            this.toastrCustom.error(this.translate.instant('common.notification.addError') + `: ${e?.message}`);
            this.isLoading = false;
          }
        );
        this.subs.push(subAdd);
      } else {
        const subUpdate = this.functionService.updateResourceCategory(data).subscribe(() => {
            this.isLoading = false;
            this.back();
            this.toastrCustom.success(this.translate.instant('common.notification.updateSuccess'));
          }, (e) => {
            this.toastrCustom.error(
              this.translate.instant('common.notification.updateError') + `: ${e?.message}`
            );
            this.isLoading = false;
          }
        );
        this.subs.push(subUpdate);
      }
    } else {
      validateAllFormFields(this.form);
    }
  }

  codeBlur() {
    this.form.controls['code'].setValue(this.form.controls['code'].value.trim())
  }
  uriBlur() {
    this.form.controls['uri'].setValue(this.form.controls['uri'].value.trim())
  }
  indexNumberBlur() {
    this.form.controls['indexNumber'].setValue(this.form.controls['indexNumber'].value.trim())
  }

  override ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
