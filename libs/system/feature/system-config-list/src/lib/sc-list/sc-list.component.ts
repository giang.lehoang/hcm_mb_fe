import { Component, OnInit, Injector, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { Config } from '@hcm-mfe/system/data-access/models';
import { userConfig } from '@hcm-mfe/shared/common/constants';
import { Pageable } from '@hcm-mfe/shared/data-access/models';
import { SystemConfigService } from '@hcm-mfe/system/data-access/services';
import { ValidateService } from '@hcm-mfe/shared/core';
import { FunctionCode } from '@hcm-mfe/shared/common/enums';

@Component({
  selector: 'app-sc-list',
  templateUrl: './sc-list.component.html',
  styleUrls: ['./sc-list.component.scss'],
})
export class ScListComponent extends BaseComponent implements OnInit, OnDestroy {
  subs: Subscription[] = [];
  textFilter = '';
  listData: Array<Config> = [];
  limit = userConfig.pageSize;
  paramSearch = {
    size: this.limit,
    page: 0,
    search: '',
  };
  pageable: Pageable = new Pageable();
  currentVisible = 5;
  constructor(
    injector: Injector,
    private readonly systemConfigService: SystemConfigService,
    public validateService: ValidateService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ADMIN_SYSTEM}`);
  }

  ngOnInit(): void {
    this.search();
  }
  trackByFn(index: number, item: Config) {
    return item.cfgId;
  }
  search($event?: any) {
    this.paramSearch.page = 0;
    this.paramSearch.search =  this.textFilter ? this.textFilter.trim() : '';
    if ($event) {
      this.paramSearch.page = $event - 1;
    }
    this.isLoading = true;
    const sub = this.systemConfigService.searchAdmConfig(this.paramSearch).subscribe(
      (result) => {
        if (result.data) {
          this.listData = result.data.content || [];
          this.pageable = {
            totalElements: result.data.totalElements,
            totalPages: result.data.totalPages,
            currentPage: result.data.number + 1,
            numberOfElements: result.data.numberOfElements,
            size: this.limit
          };
        }
        this.isLoading = false;
      },() => {
        this.isLoading = false;
      });
    this.subs.push(sub);
  }

  create() {
    this.router.navigate([this.router.url, 'create']);
  }

  update(item: Config | any) {
    this.isLoading = true;
    const sub = this.systemConfigService.getAdmConfigById(item.cfgId).subscribe(() => {
      this.isLoading = false;
      this.router.navigate([this.router.url, 'update'], { queryParams: { cfgId: item.cfgId } });
    }, (e) => {
      this.toastrCustom.error(
        this.translate.instant('common.notification.error') + `: ${e?.message}`
      );
      this.isLoading = false;
    });
    this.subs.push(sub);
  }

  delete(item: Config | any) {
    const subDelete = this.systemConfigService.delete(item.cfgId).subscribe(() => {
        this.toastrCustom.success(this.translate.instant('common.notification.deleteSuccess'));
        this.search();
        this.isLoading = false;
      }, (e) => {
        this.toastrCustom.error(
          this.translate.instant('common.notification.deleteError') + `: ${e?.message}`
        );
        this.isLoading = false;
      }
    );
    this.subs.push(subDelete);
  }

  override ngOnDestroy() {
    this.deletePopup?.modalDelete?.destroy();
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
