import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScListComponent } from './sc-list/sc-list.component';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { TranslateModule } from '@ngx-translate/core';
import { NzInputModule } from 'ng-zorro-antd/input';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { NzTableModule } from 'ng-zorro-antd/table';
import { SharedUiMbButtonIconModule } from '@hcm-mfe/shared/ui/mb-button-icon';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { SharedDirectivesTrimInputModule } from '@hcm-mfe/shared/directives/trim-input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzFormModule } from 'ng-zorro-antd/form';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, TranslateModule, NzInputModule, SharedUiMbButtonModule, NzTableModule, SharedUiMbButtonIconModule,
    NzPaginationModule, SharedDirectivesTrimInputModule, FormsModule, NzFormModule, ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: ScListComponent
      }
    ])
  ],
  declarations: [ScListComponent],
  exports: [ScListComponent],
})
export class SystemFeatureSystemConfigListModule {}
