import { Component, Injector, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { userConfig } from '@hcm-mfe/shared/common/constants';
import { IBaseResponse, IContent, IListTemplate, INotiGroup } from '@hcm-mfe/system/data-access/models';
import { FunctionCode } from '@hcm-mfe/shared/common/enums';
import { TemplateManagementService } from '@hcm-mfe/system/data-access/services';

@Component({
  selector: 'app-template-mng',
  templateUrl: './template-mng.component.html',
  styleUrls: ['./template-mng.component.scss'],
})
export class TemplateManagementComponent extends BaseComponent implements OnInit, OnDestroy {
  subs: Subscription[] = [];
  objSearch = {
    templateCode: '',
    templateName: '',
    title: '',
    notiGroup: '',
    page: 0,
    size: userConfig.pageSize,
  };
  paginationCustom = {
    size: 0,
    totalElements: 0,
    pageIndex: 0,
    numberOfElements: 0,
  };
  listGroup: INotiGroup[] = [];
  listTemplate: IListTemplate[] = [];
  constructor(
    injector: Injector,
    readonly templateMngService: TemplateManagementService) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ADMIN_TEMPLATE_MANAGEMENT}`);
  }

  ngOnInit(): void {
    this.isLoading = true;
    const sub = this.templateMngService.getListGroupNoti().subscribe((data: IBaseResponse<INotiGroup[]>) => {
      this.listGroup = data.data;
    });
    this.subs.push(sub);
    this.getListTemplate();
  }
  getListTemplate(): void {
    const sub = this.templateMngService.searchTemplate(this.objSearch).subscribe(
      (data: IBaseResponse<IContent<IListTemplate[]>>) => {
        this.listTemplate = data.data.content;
        this.paginationCustom.size = data.data.size;
        this.paginationCustom.totalElements = data.data.totalElements;
        this.paginationCustom.pageIndex = data.data.pageable.pageNumber;
        this.paginationCustom.numberOfElements = data.data.numberOfElements;
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }

  override triggerSearchEvent(): void {
    this.searchTemplate();
  }
  searchTemplate(): void {
    this.objSearch.page = 0;
    if(this.objSearch.notiGroup === null) {
      this.objSearch.notiGroup = '';
    }
    this.isLoading = true;
    this.getListTemplate();

  }
  changePage(value: any): void {
    this.objSearch.page = value - 1;
    this.getListTemplate();
  }
  trackByFn(index: number, item: IListTemplate) {
    return item.ntpCode;
  }
  createTemplate(): void {
    this.router.navigateByUrl('/system/template-mng/create');
  }
  updateTemplate(id: any): void {
    this.router.navigate([this.router.url, 'edit'], { queryParams: { id: id } });
  }

  deleteTemplate(id: any): void {
    this.deletePopup.showModal(() => {
      this.isLoading = true;
      const sub = this.templateMngService.deleteTemplate(id).subscribe(
        () => {
          this.toastrCustom.success(this.translate.instant('common.notification.deleteSuccess'));
          this.getListTemplate();
        },
        (e) => {
          this.toastrCustom.error(
            this.translate.instant('common.notification.deleteError') + e?.message
          );
          this.isLoading = false;
        }
      );
      this.subs.push(sub);
    });
  }

  override ngOnDestroy() {
    this.deletePopup?.modalDelete?.destroy();
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
