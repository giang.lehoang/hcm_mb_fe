import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TemplateManagementComponent } from './template-mng/template-mng.component';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { NzFormModule } from 'ng-zorro-antd/form';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { SharedUiMbButtonIconModule } from '@hcm-mfe/shared/ui/mb-button-icon';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, SharedUiMbButtonModule, TranslateModule, SharedUiMbInputTextModule,
    NzFormModule, SharedUiMbSelectModule, NzTableModule, NzToolTipModule, SharedUiMbButtonIconModule, NzPaginationModule,
    FormsModule, ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: TemplateManagementComponent
      }
    ])
  ],
  declarations: [TemplateManagementComponent],
  exports: [TemplateManagementComponent],
})
export class SystemFeatureTemplateMngListModule {}
