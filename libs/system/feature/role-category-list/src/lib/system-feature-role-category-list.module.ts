import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RcListComponent } from './rc-list/rc-list.component';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { NzInputModule } from 'ng-zorro-antd/input';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { NzTableModule } from 'ng-zorro-antd/table';
import { SharedUiMbButtonIconModule } from '@hcm-mfe/shared/ui/mb-button-icon';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedDirectivesTrimInputModule } from '@hcm-mfe/shared/directives/trim-input';
import { NzFormModule } from 'ng-zorro-antd/form';
import {SharedUiMbImportModule} from "@hcm-mfe/shared/ui/mb-import";

@NgModule({
    imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedUiLoadingModule, NzInputModule, TranslateModule,
        SharedUiMbButtonModule, NzTableModule, SharedUiMbButtonIconModule, NzPaginationModule, NzFormModule,
        RouterModule.forChild([
            {
                path: '',
                component: RcListComponent
            }
        ]), SharedDirectivesTrimInputModule, SharedUiMbImportModule
    ],
  declarations: [RcListComponent],
  exports: [RcListComponent],
})
export class SystemFeatureRoleCategoryListModule {}
