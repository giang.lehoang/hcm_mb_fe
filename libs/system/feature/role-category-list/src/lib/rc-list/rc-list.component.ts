import { Component, OnInit, Injector, OnDestroy } from '@angular/core';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { Subscription } from 'rxjs';
import { Role } from '@hcm-mfe/system/data-access/models';
import { userConfig } from '@hcm-mfe/shared/common/constants';
import { Pageable } from '@hcm-mfe/shared/data-access/models';
import {RoleService, UrlConstant} from '@hcm-mfe/system/data-access/services';
import { ValidateService } from '@hcm-mfe/shared/core';
import { FunctionCode } from '@hcm-mfe/shared/common/enums';

@Component({
  selector: 'role-category-list',
  templateUrl: './rc-list.component.html',
  styleUrls: ['./rc-list.component.scss'],
})
export class RcListComponent extends BaseComponent implements OnInit, OnDestroy {
  subs: Subscription[] = [];
  listData: Array<Role> = [];
  limit = userConfig.pageSize;
  paramSearch = {
    size: this.limit,
    page: 0,
    search: '',
  };
  pageable: Pageable = new Pageable();

  isImportData = false;
  urlApiDownloadTemp: string = UrlConstant.IMPORT_FORM.ROLE;
  urlApiImport: string = UrlConstant.IMPORT_FORM.ROLE_IMPORT;

  isSyncRole = false;
  constructor(
    injector: Injector,
    private readonly roleService: RoleService,
    public validateService: ValidateService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ADMIN_ROLE}`);
  }

  ngOnInit(): void {
    this.search();
  }
  trackByFn(index: number, item: Role) {
    return item.id;
  }
  search($event?: any) {
    this.paramSearch.page = 0;
    this.paramSearch.search = this.paramSearch?.search?.trim();
    if ($event) {
      this.paramSearch.page = $event - 1;
    }
    this.isLoading = true;
    const sub = this.roleService.searchRole(this.paramSearch).subscribe(
      (result) => {
        this.listData = result?.data?.content || [];
        this.pageable = {
          totalElements: result?.data?.page?.totalElements,
          totalPages: result?.data?.page?.totalPages,
          numberOfElements: result?.data?.content?.length,
          currentPage: result?.data?.page?.number + 1,
          size: this.limit,
        };
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }

  create() {
    this.router.navigate([this.router.url, 'create']);
  }

  update(item: Role | any) {
    this.isLoading = true;
    const sub = this.roleService.getRoleById(item.id).subscribe(
      () => {
      this.isLoading = false;
      this.router.navigate([this.router.url, 'update'], { queryParams: { id: item.id } });
    },
    (e) => {
      this.toastrCustom.error(
        this.translate.instant('common.notification.error') + `: ${e?.message}`
      );
      this.isLoading = false;
    });
    this.subs.push(sub);
  }

  delete(item: any) {
    this.deletePopup.showModal(() => {
      this.isLoading = true;
      const sub = this.roleService.deleteRoleById(item.id).subscribe(
        () => {
          this.toastrCustom.success(this.translate.instant('common.notification.deleteSuccess'));
          this.search();
        },
        (e) => {
          this.toastrCustom.error(
            this.translate.instant('common.notification.deleteError') + `: ${e?.message}`
          );
          this.isLoading = false;
        }
      );
      this.subs.push(sub);
    });
  }

  doImportData() {
    this.isImportData = true;
  }

  doCloseImport(isSearch: boolean) {
    this.isImportData = false;
    isSearch && this.search();
  }

  syncRole() {
    this.confirmService.success().then(response => {
      if(response) {
        this.isSyncRole = true;
        const syncRole = this.roleService.syncRole().subscribe(res => {
          this.toastrCustom.success(this.translate.instant('system.role.label.notification.syncSuccess'));
          this.isSyncRole = false;
        }, (e) => {
          this.toastrCustom.error(e?.message);
        });
        this.subs.push(syncRole);
      }
    });
  }

  override ngOnDestroy() {
    this.deletePopup?.modalDelete?.destroy();
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
