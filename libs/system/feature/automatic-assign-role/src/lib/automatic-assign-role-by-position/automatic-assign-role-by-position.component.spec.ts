import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutomaticAssignRoleByPositionComponent } from './automatic-assign-role-by-position.component';

describe('AutomaticAssignRoleByPositionComponent', () => {
  let component: AutomaticAssignRoleByPositionComponent;
  let fixture: ComponentFixture<AutomaticAssignRoleByPositionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutomaticAssignRoleByPositionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutomaticAssignRoleByPositionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
