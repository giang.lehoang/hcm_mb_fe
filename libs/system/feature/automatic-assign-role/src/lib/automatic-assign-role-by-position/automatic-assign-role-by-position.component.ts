import {Component, Injector, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {BaseComponent} from "@hcm-mfe/shared/common/base-component";
import {of, Subscription} from "rxjs";
import {userConfig} from "@hcm-mfe/shared/common/constants";
import {Position, Role} from "@hcm-mfe/system/data-access/models";
import {BaseResponse, Pageable, TableConfig} from "@hcm-mfe/shared/data-access/models";
import {AutomaticAssignRoleService, RoleService} from "@hcm-mfe/system/data-access/services";
import {ValidateService} from "@hcm-mfe/shared/core";
import {catchError} from "rxjs/operators";
import {FunctionCode, maxInt32, SessionKey} from "@hcm-mfe/shared/common/enums";
import {NzModalRef} from "ng-zorro-antd/modal";
import {PositionsComponent} from "@hcm-mfe/system/ui/positions";
import {NzSafeAny} from "ng-zorro-antd/core/types";

@Component({
  selector: 'hcm-mfe-automatic-assign-role-by-position',
  templateUrl: './automatic-assign-role-by-position.component.html',
  styleUrls: ['./automatic-assign-role-by-position.component.scss']
})
export class AutomaticAssignRoleByPositionComponent extends BaseComponent implements OnInit, OnDestroy {
  subs: Subscription[] = [];
  pageSize = userConfig.pageSize;
  modalRef!: NzModalRef;
  listTermRole: Array<Role> = [];
  listRole: Array<Role> = [];
  pageableRole: Pageable = new Pageable();
  roleIdSelected: string | undefined;
  paramsRole = {
    first: 0,
    max: this.pageSize,
    search: '',
  };
  isSyncAutomaticAssign = false;
  readonly syncSuccess = 'system.automaticAssignRole.notification.syncSuccess';
  readonly SUCCEEDED = 'SUCCEEDED';
  interval: any;

  listPosition: Array<Position> = [];
  paramsPosition = {
    size: this.pageSize,
    page: 0,
    searchCondition: ''
  };

  tableConfigPosition!: TableConfig;
  @ViewChild('actionTmpl', { static: true }) action!: TemplateRef<NzSafeAny>;
  tableConfigRole!: TableConfig;

  constructor(
    injector: Injector,
    private readonly roleService: RoleService,
    private readonly assignRoleService: AutomaticAssignRoleService,
    public validateService: ValidateService
    ) {
    super(injector)
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.GAN_TU_DONG_VAI_TRO}`);
    localStorage.removeItem(SessionKey.SYNC_AUTOMATIC);
    this.checkSyncAutomaticAssignRole();
  }

  ngOnInit(): void {
    const params = {
      page: 0,
      size: maxInt32,
    };
    const sub = this.roleService.searchRole(params).pipe(catchError(() => of(undefined))).subscribe((listRole: BaseResponse)=> {
      this.listTermRole = listRole?.data?.content || [];
      this.searchRole();
    });
    this.subs.push(sub);
    this.initTableRole();
    this.initTablePosition();
    this.checkSyncStatus();
  }

  initTablePosition() {
    this.tableConfigPosition = {
      headers: [
        {
          title: 'STT',
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          fixed: true,
          fixedDir: 'left',
          width: 70
        },
        {
          title: 'system.automaticAssignRole.label.table.posCode',
          field: 'posCode', width: 120,
        },
        {
          title: 'system.automaticAssignRole.label.table.posName',
          field: 'posName', width: 250,
        },
        {
          title: 'system.automaticAssignRole.label.table.orgName',
          field: 'orgName', width: 250,
        },
        {
          title: 'system.automaticAssignRole.label.table.jobName',
          field: 'jobName', width: 150,
        },
        {
          title: ' ',
          width: 50,
          tdTemplate: this.action,
          show: this.objFunction?.delete
        },
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.paramsPosition.size,
      pageIndex: 1,
    };
  }

  initTableRole() {
    this.tableConfigRole = {
      headers: [
        {
          title: 'STT',
          thClassList: ['text-nowrap','text-center'],
          tdClassList: ['text-nowrap','text-center'],
          fixed: true,
          fixedDir: 'left',
          width: 50
        },
        {
          title: 'system.role.label.table.code',
          field: 'name', width: 120,
        },
        {
          title: 'system.role.label.table.name',
          field: 'description', width: 250,
        },
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pageSize,
      pageIndex: 1,
    };
  }

  searchRole($event?: any) {
    this.isLoading = true;
    this.paramsRole.search = this.paramsRole.search.trim();
    if (!$event) {
      this.paramsRole.first = 0;
      this.paramsRole.max = this.pageSize;
      this.paramsRole.search = this.paramsRole.search.trim();
    } else {
      this.paramsRole.first = ($event - 1) * (this.pageSize ?? 0);
    }
    const data = this.getListRole();
    if (data) {
      this.listRole = data.content || [];
      this.tableConfigRole.total = data.totalElements;
      this.tableConfigRole.pageIndex = data.number;
    }
    if (this.listRole.length > 0) {
      this.roleIdSelected = this.listRole[0].id;
      this.searchPosition(1);
      this.isLoading = false;
    } else {
      this.roleIdSelected = '';
      this.isLoading = false;
    }
  }

  getListRole() {
    const listAll = this.listTermRole.filter(
      (item) =>
        item?.name?.toLowerCase().includes(this.paramsRole.search?.toLowerCase()) ||
        item?.description?.toLowerCase().includes(this.paramsRole.search?.toLowerCase()) ||
        !this.paramsRole.search
    );
    const totalCount = listAll.length || 0;
    this.listRole = listAll.slice(this.paramsRole.first, this.paramsRole.first + this.paramsRole.max) || [];
    return {
      content: this.listRole,
      totalElements: totalCount,
      number: Math.floor(this.paramsRole.first / this.pageSize + 1)
    }
  }

  searchPosition(page: any) {
    if (this.roleIdSelected) {
      this.paramsPosition.page = page - 1;
      this.isLoading = true;
      this.paramsPosition.searchCondition = this.paramsPosition.searchCondition.trim();
      const sub = this.assignRoleService.getListPositionByRoleId(this.roleIdSelected, this.paramsPosition).subscribe((result) => {
          if (result.data) {
            this.listPosition = result.data.content || [];
            this.tableConfigPosition.total = result.data.totalElements;
            this.tableConfigPosition.pageIndex = result.data.number + 1;
          }
          this.isLoading = false;
        },
        () => {
          this.isLoading = false;
        }
      );
      this.subs.push(sub);
    }
  }

  onSelect(selected: any) {
    if (selected.id !== this.roleIdSelected) {
      this.roleIdSelected = selected.id;
      this.paramsPosition.searchCondition = '';
      this.searchPosition(1);
    }
  }


  showModalSelectPosition() {
    this.modalRef = this.modal.create({
      nzWidth: this.getModalWidth(),
      nzTitle: this.translate.instant('system.automaticAssignRole.label.table.selectPos'),
      nzContent: PositionsComponent,
      nzComponentParams: {
        roleId: this.roleIdSelected
      },
      nzClassName: 'modal-padding-top-0'
    });
    const sub = this.modalRef.afterClose.subscribe((res) => {
      if(res != null) {
        this.searchPosition(1);
      }
    });
    this.subs.push(sub);
  }

  getModalWidth(): number {
    if(window.innerWidth > 767) {
      return window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5
    }
    return window.innerWidth;
  }

  deletePosition(rpmId: number) {
    this.deletePopup.showModal(() => {
      const subDelete = this.assignRoleService.deletePositionRoleMapping(rpmId).subscribe(() => {
          this.toastrCustom.success(this.translate.instant('common.notification.deleteSuccess'));
          this.searchPosition(this.tableConfigPosition.pageIndex);
          this.isLoading = false;
        }, (e) => {
          this.toastrCustom.error(
            this.translate.instant('common.notification.deleteError') + `: ${e?.message}`
          );
          this.isLoading = false;
        }
      );
      this.subs.push(subDelete);
    });
  }

  syncAutomaticAssignRole() {
    this.confirmService.success().then(response => {
      if(response) {
        const syncPermission = this.assignRoleService.syncAutomatic().subscribe(res => {
          localStorage.setItem(SessionKey.SYNC_AUTOMATIC, "SYNC_AUTOMATIC");
          this.isSyncAutomaticAssign = true;
          this.checkSyncStatus();
        });
        this.subs.push(syncPermission);
      }
    });
  }

  checkSyncStatus() {
    this.interval = setInterval(() => {
      this.checkSyncAutomaticAssignRole();
    }, 10000);
  }

  checkSyncAutomaticAssignRole() {
    const syncStatus = this.assignRoleService.getSyncStatus().subscribe((res: BaseResponse) => {
      const status = res.data;
      if(status === this.SUCCEEDED && localStorage.getItem(SessionKey.SYNC_AUTOMATIC)) {
        this.toastrCustom.success(this.translate.instant(this.syncSuccess));
        localStorage.removeItem(SessionKey.SYNC_AUTOMATIC);
        this.isSyncAutomaticAssign = false;
        clearInterval(this.interval);
      } else {
        if (status !== this.SUCCEEDED && !localStorage.getItem(SessionKey.SYNC_AUTOMATIC)) {
          localStorage.setItem(SessionKey.SYNC_AUTOMATIC, "SYNC_AUTOMATIC");
          // if (!this.isSyncAutomaticAssign) {
          //   this.checkSyncStatus();
          // }
          this.isSyncAutomaticAssign = true;
        } else if (status === this.SUCCEEDED) {
          clearInterval(this.interval);
          this.isSyncAutomaticAssign = false;
        }
      }
    }, (e: any)=> {
      this.toastrCustom.error(e?.message);
      this.isSyncAutomaticAssign = false;
      clearInterval(this.interval);
    });
    this.subs.push(syncStatus);
  }

  override ngOnDestroy() {
    this.modalRef?.destroy();
    clearInterval(this.interval);
    this.subs.forEach(e => e.unsubscribe());
  }

}
