import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AutomaticAssignRoleByPositionComponent } from './automatic-assign-role-by-position/automatic-assign-role-by-position.component';
import {RouterModule} from "@angular/router";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {NzFormModule} from "ng-zorro-antd/form";
import {NzInputModule} from "ng-zorro-antd/input";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import {FormsModule} from "@angular/forms";
import {SharedDirectivesTrimInputModule} from "@hcm-mfe/shared/directives/trim-input";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbButtonIconModule} from "@hcm-mfe/shared/ui/mb-button-icon";
import {SharedUiMbTableMergeCellWrapModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell-wrap";
import {SharedUiMbTableMergeCellModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell";

@NgModule({
  imports: [CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: AutomaticAssignRoleByPositionComponent
      }
    ]), SharedUiLoadingModule, NzFormModule, NzInputModule, SharedUiMbButtonModule, NzTableModule, NzPaginationModule,
    FormsModule, SharedDirectivesTrimInputModule, TranslateModule, SharedUiMbButtonIconModule, SharedUiMbTableMergeCellWrapModule, SharedUiMbTableMergeCellModule
  ],
  declarations: [
    AutomaticAssignRoleByPositionComponent
  ],
  exports: [AutomaticAssignRoleByPositionComponent]
})
export class SystemFeatureAutomaticAssignRoleModule {}
