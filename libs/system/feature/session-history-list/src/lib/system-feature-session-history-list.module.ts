import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShListComponent } from './sh-list/sh-list.component';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { NzFormModule } from 'ng-zorro-antd/form';
import { SharedUiMbDatePickerModule } from '@hcm-mfe/shared/ui/mb-date-picker';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedUiLoadingModule, NzFormModule, SharedUiMbDatePickerModule,
    TranslateModule, SharedUiMbInputTextModule, SharedUiMbSelectModule, SharedUiMbButtonModule, NzTableModule, NzToolTipModule, NzPaginationModule,
    RouterModule.forChild([
      {
        path: '',
        component: ShListComponent
      }
    ])
  ],
  declarations: [ShListComponent],
  exports: [ShListComponent],
})
export class SystemFeatureSessionHistoryListModule {}
