import { Component, OnInit, Injector, AfterViewInit, TemplateRef } from '@angular/core';
import { Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import * as moment from 'moment';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { App, SessionHistory } from '@hcm-mfe/system/data-access/models';
import { userConfig } from '@hcm-mfe/shared/common/constants';
import { DateValidator } from '@hcm-mfe/shared/common/validators';
import { maxInt32 } from '@hcm-mfe/shared/common/enums';
import { Pageable } from '@hcm-mfe/shared/data-access/models';
import { ValidateService } from '@hcm-mfe/shared/core';
import { AppService, SessionHistoryService } from '@hcm-mfe/system/data-access/services';
import { cleanDataForm } from '@hcm-mfe/shared/common/utils';
import { ShDetailComponent } from '@hcm-mfe/system/feature/session-history-detail';

@Component({
  selector: 'app-session-history-list',
  templateUrl: './sh-list.component.html',
  styleUrls: ['./sh-list.component.scss'],
})
export class ShListComponent extends BaseComponent implements OnInit, AfterViewInit {
  subs: Subscription[] = [];
  isSubmitted = false;
  listData: Array<SessionHistory> = [];
  limit = userConfig.pageSize;
  modalRef: NzModalRef | undefined;
  searchForm = this.fb.group(
    {
      fromDate: [null, [Validators.required]],
      toDate: [null, [Validators.required]],
      username: ['', [Validators.required, Validators.maxLength(255)]],
      device: '',
      appId: null,
      function: '',
    },
    {
      validators: [DateValidator.validateRangeDate('fromDate', 'toDate', 'rangeDateError')],
    }
  );
  paramSearch = {
    size: this.limit,
    page: 0,
  };
  param = { page: 0, size: maxInt32 };
  pageable: Pageable = new Pageable;
  listApp: Array<App> = [];
  constructor(
    injector: Injector,
    public validateService: ValidateService,
    private readonly appService: AppService,
    private readonly sessionHistoryService: SessionHistoryService,
  ) {
    super(injector);
  }
  trackByFn(index: number, item: SessionHistory) {
    return item.utcId;
  }
  ngOnInit(): void {
    this.subs.push(
      this.appService.searchAppCategory(this.param).subscribe((listApp) => {
        this.listApp = listApp.data.content || [];
        this.listApp?.sort((a: any, b: any) => {
          if (a.appName.toLowerCase() < b.appName.toLowerCase()) {
            return -1;
          }
          if (a.appName.toLowerCase() > b.appName.toLowerCase()) {
            return 1;
          }
          return 0;
        });
      })
    );
  }

  override ngOnDestroy() {
    this.modalRef?.destroy();
    this.subs.forEach((sub) => sub.unsubscribe());
  }

  ngAfterViewInit() {
    this.ref.detectChanges();
  }

  override triggerSearchEvent(): void {
    this.search();
  }

  search($event?: any) {
    this.isSubmitted = true;
    if (this.searchForm.valid) {
      this.isLoading = true;
      this.paramSearch.page = 0;
      if ($event) {
        this.paramSearch.page = $event - 1;
      }
      const params = cleanDataForm(this.searchForm);
      if(!params.appId) {
        delete params.appId;
      }
      params.fromDate = moment(params.fromDate).format('DD/MM/yyyy HH:mm:ss');
      params.toDate = moment(params.toDate).format('DD/MM/yyyy HH:mm:ss');
      const paramsNew = { ...params, page: this.paramSearch.page, size: this.limit };
      this.subs.push(
        this.sessionHistoryService.getList(paramsNew).subscribe(
          (result) => {
            this.listData = result?.data?.content || [];

            this.pageable.totalElements = result?.data?.totalElements;
            this.pageable.totalPages = result?.data?.totalPages;
            this.pageable.currentPage = result?.data?.number + 1;
            this.pageable.size = this.limit;

            this.isLoading = false;
          },
          () => {
            this.isLoading = false;
          }
        )
      );
    }
  }


  showDetail(item: any, footerTmpl: TemplateRef<any>) {
    this.modalRef = this.modal.create({
      nzWidth: this.getModalWidth(),
      nzTitle: this.translate.instant('system.sessionHistory.detail'),
      nzContent: ShDetailComponent,
      nzComponentParams: {
        utcId: item.utcId
      },
      nzFooter: footerTmpl
    });
  }

  getModalWidth(): number {
    if(window.innerWidth > 767) {
      return window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5
    }
    return window.innerWidth;
  }
}
