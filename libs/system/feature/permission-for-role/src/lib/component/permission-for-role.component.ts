import { catchError } from 'rxjs/operators';
import { Component, Injector, OnInit, ViewEncapsulation } from '@angular/core';
import { forkJoin, of, Subscription } from 'rxjs';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { userConfig } from '@hcm-mfe/shared/common/constants';
import { App, Permission, Resource, Role, TreeNode } from '@hcm-mfe/system/data-access/models';
import {BaseResponse, Pageable} from '@hcm-mfe/shared/data-access/models';
import {
  AppService,
  FunctionService,
  PermissionService,
  RoleService,
  UrlConstant
} from '@hcm-mfe/system/data-access/services';
import { CommonUtils, ValidateService } from '@hcm-mfe/shared/core';
import {FunctionCode, maxInt32, SessionKey} from '@hcm-mfe/shared/common/enums';
import { getTypeExport } from '@hcm-mfe/shared/common/utils';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-permission-for-role',
  templateUrl: './permission-for-role.component.html',
  styleUrls: ['./permission-for-role.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PermissionForRoleComponent extends BaseComponent implements OnInit {
  subs: Subscription[] = [];
  pageSize = userConfig.pageSize;
  params = {
    first: 0,
    max: this.pageSize,
    search: '',
  };
  listApp: Array<App> = [];
  listPermission: Array<Permission> = [];
  listTerm: Array<Role> = [];
  listRole: Array<Role> = [];
  pageable: Pageable = new Pageable();
  idApp = undefined;
  roleCodeSelected: string | undefined;
  selected: Array<TreeNode> = [];
  permissionOfRoleOld: Array<string> = [];
  currPermissionOfRole: Array<string> = [];
  countPermissionOfApp = 0;
  isCheckedAll = false;

  listOfMapAll: TreeNode[] = [];
  listOfMap: TreeNode[] = [];
  mapOfExpanded: { [key: string]: TreeNode[] } = {};
  listData: Array<Resource> = [];

  isImportData = false;
  urlApiDownloadTemp: string = UrlConstant.IMPORT_FORM.PERMISSIONS;
  urlApiImport: string = UrlConstant.IMPORT_FORM.PERMISSIONS_IMPORT;

  isSyncPermission = false;
  readonly syncSuccess = 'system.permissionConfig.notification.syncSuccess';
  readonly SUCCEEDED = 'SUCCEEDED';
  interval: any;

  constructor(
    injector: Injector,
    private readonly permissionService: PermissionService,
    private readonly functionService: FunctionService,
    private readonly roleService: RoleService,
    private readonly appService: AppService,
    public validateService: ValidateService
  ) {
    super(injector);
    this.isLoading = true;
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ADMIN_PERMISSION_ROLE}`);
    localStorage.removeItem(SessionKey.SYNC_PERMISSION);
    this.checkSyncPermisstion();
  }

  collapses(array: TreeNode[], data: TreeNode, $event: boolean): void {
    CommonUtils.collapses(array, data, $event, true);
  }

  ngOnInit(): void {
    const params = {
      page: 0,
      size: maxInt32,
    };
    const sub = forkJoin([
      this.roleService.searchRole(params).pipe(catchError(() => of(undefined))),
      this.appService.searchAppCategory(params).pipe(catchError(() => of(undefined))),
      this.functionService.searchResourceCategory(params).pipe(catchError(() => of(undefined))),
      this.permissionService.getPermission().pipe(catchError(() => of(undefined))),
    ]).subscribe(
      ([listRole, listApp, listResource, listPermission]) => {
        this.listTerm = listRole?.data?.content || [];
        const listData: any[] = [];
        listApp?.data?.content?.forEach((item: App, i: number) => {
          const itemNew = {
            id: `${i + 1}`,
            name: item.appName,
            originId: item.appId,
          };
          listData.push(itemNew);
          this.listApp.push(itemNew);
        });
        listPermission?.data?.forEach((item: any) => {
          if (item.resources[0]) {
            const itemNew = {
              id: item.id,
              name: item.name,
              permissionType: item.type,
              parentId: item.resources[0] + '_F',
              type: 'P',
              checked: false,
            };
            listData.push(itemNew);
            this.listPermission.push(itemNew);
          }
        });
        listResource?.data?.content?.forEach((item: any) => {
          const itemNew = {
            id: item.id + '_F',
            name: item.name,
            checked: false,
            type: 'F',
            parentId: item.parentId
              ? item.parentId + '_F'
              : listData?.find((i) => (i.originId === item.type ? item.type : ''))?.id,
          };
          listData.push(itemNew);
        });
        const listDataMapPermission: any[] = [];
        this.listPermission?.forEach((item) => {
          listDataMapPermission.push(item);
          this.mapPermission(item, listData, listDataMapPermission);
        });
        CommonUtils.sortCustome(listDataMapPermission, 'type', 'name');
        this.listData = [...new Set(listDataMapPermission)];
        this.listOfMapAll = CommonUtils.convertToTree(this.listData, 'sortCustome', ['type','name'], true);
        this.listOfMap = this.listOfMapAll.find((el) => el.id === this.idApp)?.children || [];
        this.searchRole();
      },
      () => {
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }
  trackByRole(item: Role | any) {
    return item.id;
  }
  trackByNode(item: TreeNode | any) {
    return item.id;
  }
  mapPermission(item: any, listData: any, listDataMapPermission: any) {
    const parent = listData?.find((i: any) => i.id === item.parentId);
    if (parent) {
      listDataMapPermission.push(parent);
      if (parent.parentId) {
        this.mapPermission(parent, listData, listDataMapPermission);
      }
    }
  }

  searchRole($event?: any) {
    this.isLoading = true;
    this.params.search = this.params.search.trim();
    if (!$event) {
      this.params.first = 0;
      this.params.max = this.pageSize;
      this.params.search = this.params.search.trim();
    } else {
      this.params.first = ($event - 1) * (this.pageable?.size ?? 0);
    }
    const listAll = this.listTerm.filter(
      (item) =>
        item?.name?.toLowerCase().includes(this.params.search?.toLowerCase()) ||
        item?.description?.toLowerCase().includes(this.params.search?.toLowerCase()) ||
        !this.params.search
    );
    const totalCount = listAll.length || 0;
    this.listRole = listAll.slice(this.params.first, this.params.first + this.params.max) || [];
    this.pageable = {
      totalElements: totalCount,
      totalPages: Math.floor(totalCount / this.pageSize),
      currentPage: Math.floor(this.params.first / this.pageSize + 1),
      numberOfElements: Math.floor(this.listRole.length),
      size: this.pageSize,
    };
    this.checkAll(false);
    if (this.listRole.length > 0) {
      this.roleCodeSelected = this.listRole[0].name;
      this.getPermissionByRole();
      this.isLoading = false;
    } else {
      this.roleCodeSelected = '';
      this.isLoading = false;
      this.selected = [];
    }
  }

  getPermissionByRole() {
    this.isLoading = true;
    if (this.roleCodeSelected) {
      const sub = this.permissionService
        .getPermissionByRole(this.roleCodeSelected)
        .subscribe((result) => {
          this.permissionOfRoleOld = result?.data;
          this.mapSelectedTreeNode();
          this.isLoading = false;
        }, (e) => {
          this.toastrCustom.error(this.translate.instant('common.notification.error') + `: ${e?.message}`);
          this.isLoading = false;
        });
      this.subs.push(sub);
    }
  }

  findNodeById(tree: TreeNode, id: string): any {
    if(tree.id === id) {
      return tree;
    } else {
      return tree.children?.reduce((result, n) => result || this.findNodeById(n, id), undefined);
    }
  }

  mapSelectedTreeNode() {
    const listPermission: any[] = [];
    this.permissionOfRoleOld.forEach((permissionId) => {
      this.listOfMap.forEach((map: any) => {
        const permission = this.findNodeById(map, permissionId);
        if (permission) {
          listPermission.push(permissionId);
          permission.checked = true;
          if (this.mapOfExpanded[map.key]) {
            this.onChangeCheckbox(this.mapOfExpanded[map.key], permission, true);
          }
        }
      });
    });
    this.currPermissionOfRole = this.permissionOfRoleOld.filter(e => !listPermission.includes(e));
  }

  onSelect(selected: any) {
    if (selected.id !== this.roleCodeSelected) {
      this.checkAll(false);
      this.roleCodeSelected = selected.name;
      this.getPermissionByRole();
    }
  }

  save() {
    this.isLoading = true;
    let data: any[] = [];
    this.selected?.forEach((item) => {
      if (item.permissionType && !data.some(e => e.id === item.id)) {
        data.push({ id: item.id });
      }
    });
    this.currPermissionOfRole.forEach((value) => {
      const item = this.listPermission.find((i) => i.id === value);
      if (item && !data.some(e => e.id === item.id)) {
        data.push({ id: item.id });
      }
    });
    data = [...new Set(data)];
    const subAdd = this.permissionService.addPermissionForRole({ roleCode: this.roleCodeSelected, permissions: data }).subscribe(() => {
        this.isLoading = false;
        this.toastrCustom.success(this.translate.instant('system.permissionForRole.message.success'));
        this.permissionOfRoleOld = data.map((item) => item.id);
      }, (e) => {
        this.toastrCustom.error(this.translate.instant('common.notification.error') + `: ${e?.message}`);
        this.isLoading = false;
      }
    );
    this.subs.push(subAdd);
  }

  changeApp($event: any) {
    this.listOfMap = this.listOfMapAll.find((el) => el.id === this.idApp)?.children || [];
    this.mapOfExpand(this.listOfMap, this.mapOfExpanded, false);
    this.isCheckedAll = $event;
    this.checkAll($event);
    this.mapSelectedTreeNode();
  }

  private mapOfExpand(listOfMap: any, mapOfExpanded: any, expanded: boolean) {
    let countPermissionOfApp = 0;
    listOfMap.forEach((item: any) => {
      mapOfExpanded[item.key] = CommonUtils.convertTreeToLists(item, expanded);
      countPermissionOfApp += mapOfExpanded[item.key].filter((item: any) => item.type === 'P').length;
    });
    this.countPermissionOfApp = countPermissionOfApp;
  }

  checkAll($event: any) {
    this.selected = [];
    this.listOfMap.forEach((data: any) => {
      const target = this.mapOfExpanded[data.key].find((a) => a.key === data.key);
      if (target) {
        target.checked = $event;
      }
      this.onChangeCheckedChildren(this.mapOfExpanded[data.key], data, $event);
    });
  }

  onChangeCheckbox(array: TreeNode[], data: TreeNode, $event: boolean) {
    this.onChangeCheckedChildren(array, data, $event);
    this.onChangeCheckedParent(array, data, $event);
  }

  onChangeCheckedParent(array: TreeNode[], data: TreeNode, $event: boolean) {
    if (data.permissionType) {
      if ($event) {
        this.selected = [...this.selected, data];
      } else {
        this.selected = this.selected.filter((el) => el.id !== data.id);
      }
    }

    const target: any = array.find((a) => a.key === data.key);
    target.checked = $event;
    if (target.parent) {
      const parentTree: any = array.find((a) => a.key === target.parent.key);
      let checked = true;
      for (const item of parentTree.children) {
        const targetChild: any = array.find((a) => a.key === item.key);
        if (targetChild.checked && item.key !== data.key) {
          checked = false;
          break;
        }
      }
      if (checked) {
        parentTree.checked = $event;
        this.onChangeCheckedParent(array, parentTree, $event);
      }
    }
  }

  onChangeCheckedChildren(array: TreeNode[], data: TreeNode, $event: boolean): void {
    if (data.children && data.children.length > 0) {
      data.children.forEach((d) => {
        const target: any = array.find((a) => a.key === d.key);
        target.checked = $event;
        this.onChangeCheckedChildren(array, target, $event);
      });
    } else {
      if ($event && data.permissionType) {
        this.selected = [...this.selected, data];
        this.selected = [...new Set(this.selected)];
      } else {
        this.selected = [...new Set(this.selected.filter((el) => el.id !== data.id))];
      }
      this.isCheckedAll = this.countPermissionOfApp > 0 && this.countPermissionOfApp === this.selected.length;
    }
  }

  doImportData() {
    this.isImportData = true;
  }

  doCloseImport(isSearch: boolean) {
    this.isImportData = false;
    isSearch && this.getPermissionByRole();
  }

  syncPermission() {
    this.confirmService.success().then(response => {
      if(response) {
        const syncPermission = this.permissionService.syncPermission().subscribe(res => {
          localStorage.setItem(SessionKey.SYNC_PERMISSION, "SYNC_PERMISSION");
          this.isSyncPermission = true;
          this.checkSyncStatus();
        });
        this.subs.push(syncPermission);
      }
    });
  }

  checkSyncStatus() {
    this.interval = setInterval(() => {
      this.checkSyncPermisstion();
    }, 10000);
  }

  checkSyncPermisstion() {
    const syncStatus = this.permissionService.getSyncStatus().subscribe((res: BaseResponse) => {
      const status = res.data;
      if(status === this.SUCCEEDED && localStorage.getItem(SessionKey.SYNC_PERMISSION)) {
        this.toastrCustom.success(this.translate.instant(this.syncSuccess));
        localStorage.removeItem(SessionKey.SYNC_PERMISSION);
        this.isSyncPermission = false;
        clearInterval(this.interval);
        this.getPermissionByRole();
      } else {
        if (status !== this.SUCCEEDED && !localStorage.getItem(SessionKey.SYNC_PERMISSION)) {
          localStorage.setItem(SessionKey.SYNC_PERMISSION, "SYNC_PERMISSION");
          // if (!this.isSyncPermission) {
          //   this.checkSyncStatus();
          // }
          this.isSyncPermission = true;
        } else if (status === this.SUCCEEDED) {
          clearInterval(this.interval);
          this.isSyncPermission = false;
        }
      }
    }, (e: any)=> {
      this.toastrCustom.error(e?.message);
      this.isSyncPermission = false;
      clearInterval(this.interval);
    });
    this.subs.push(syncStatus);
  }

  doExportData() {
    this.permissionService.export().subscribe((res: any) => {
      const arr = res.headers.get("Content-Disposition")?.split(';');
      const fileName:string = arr[arr.length - 1].replace('filename=','').trim();
      const reportFile = new Blob([res.body], { type: getTypeExport('xlsx') });
      saveAs(reportFile, fileName);
    })
  }

  override ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
    clearInterval(this.interval);
  }
}
