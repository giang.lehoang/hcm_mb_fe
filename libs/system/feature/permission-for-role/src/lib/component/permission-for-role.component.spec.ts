import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PermissionForRoleComponent } from './permission-for-role.component';

describe('PermissionForRoleComponent', () => {
  let component: PermissionForRoleComponent;
  let fixture: ComponentFixture<PermissionForRoleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PermissionForRoleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PermissionForRoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
