import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PermissionForRoleComponent } from './component/permission-for-role.component';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzFormModule } from 'ng-zorro-antd/form';
import { TranslateModule } from '@ngx-translate/core';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { SharedUiMbSelectCheckAbleModule } from '@hcm-mfe/shared/ui/mb-select-check-able';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzTableModule } from 'ng-zorro-antd/table';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { RouterModule } from '@angular/router';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import {SharedDirectivesTrimInputModule} from "@hcm-mfe/shared/directives/trim-input";
import {SharedUiMbImportModule} from "@hcm-mfe/shared/ui/mb-import";
import {NzInputModule} from "ng-zorro-antd/input";

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedUiLoadingModule, NzFormModule,
    TranslateModule, NzIconModule, SharedUiMbSelectModule, SharedUiMbSelectCheckAbleModule,
    NzSelectModule, NzTableModule, SharedUiMbButtonModule,
    RouterModule.forChild([
      {
        path: '',
        component: PermissionForRoleComponent
      }
    ]), NzPaginationModule, NzCheckboxModule, SharedDirectivesTrimInputModule, SharedUiMbImportModule, NzInputModule],
  declarations: [PermissionForRoleComponent],
  exports: [PermissionForRoleComponent],
})
export class SystemFeaturePermissionForRoleModule {}
