import { Component, Injector, OnInit } from '@angular/core';
import { forkJoin, Subscription } from 'rxjs';
import { KeycloakService } from 'keycloak-angular';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { IRole, IUserPermission, Pagination } from '@hcm-mfe/system/data-access/models';
import { BaseResponse, Pageable } from '@hcm-mfe/shared/data-access/models';
import { userConfig } from '@hcm-mfe/shared/common/constants';
import { UrlConstant, UserConfigService, UserService } from '@hcm-mfe/system/data-access/services';
import { FunctionCode, maxInt32, rolesDefault, SessionKey } from '@hcm-mfe/shared/common/enums';
@Component({
  selector: 'user-role',
  templateUrl: './user-role.component.html',
  styleUrls: ['./user-role.component.scss'],
})
export class UserRoleComponent extends BaseComponent implements OnInit {
  subs: Subscription[] = [];
  pagination: Pageable = new Pageable;
  pageableRole: Pageable = new Pageable;
  listUser: Array<IUserPermission> = [];
  listRole: Array<IRole> = [];

  dataCloneListRole: IRole[] = [];

  listOfCurrentPageData: Array<IRole> = [];
  roleSelected: Array<IRole> = [];
  userSelected: IUserPermission | undefined;
  selected: Array<IUserPermission> = [];
  dataRole: Array<IRole> = [];
  userSearch = '';
  roleSearch = '';
  numberOfElementsListUser: number  = 0;

  limit: number = userConfig.pageSize;
  searchForm = this.fb.group({
    username: [''],
  });

  paramSearch = {
    size: this.limit,
    page: 0,
    username: '',
    sort: 'appCode,asc',
  };
  paramsRole = {
    first: 0,
    max: this.limit,
    search: '',
  };

  HighlightRow: string | undefined;
  isImportData = false;
  urlApiDownloadTemp: string = UrlConstant.IMPORT_FORM.USER_ROLE;
  urlApiImport: string = UrlConstant.IMPORT_FORM.USER_ROLE_IMPORT;

  isSyncUserRole = false;
  readonly syncSuccess = 'system.permissionConfig.notification.syncSuccess';
  readonly SUCCEEDED = 'SUCCEEDED';
  interval: any;

  constructor(
    injector: Injector,
    readonly userService: UserService,
    readonly userConfigService: UserConfigService,
    readonly keycloakService: KeycloakService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ADMIN_USER_ROLE}`);
    localStorage.removeItem(SessionKey.SYNC_USER_ROLE);
    this.checkSyncUserRole();
  }

  changePage() {
    this.paramSearch.page = this.pagination.currentPage - 1;
    this.searchUser(false);
  }
  changePage2() {
    this.paramsRole.first = (this.pageableRole?.currentPage - 1) * this.pageableRole.size;
    const dataResult = this.filterDataRole();
    const totalCount = dataResult?.length || 0;

    this.pageableRole.totalElements = totalCount;
    this.pageableRole.totalPages = Math.floor(totalCount / this.limit);
    this.pageableRole.currentPage = Math.floor(this.paramsRole.first / this.limit + 1);
    this.pageableRole.size = this.limit;

    this.listRole = dataResult.slice(this.paramsRole.first, this.paramsRole.max + this.paramsRole.first);
  }

  ngOnInit(): void {
    this.initData();
  }

  initData() {
    this.pagination = {
      currentPage: 1,
      totalElements: 50,
      size: this.limit,
      totalPages: 0,
      numberOfElements: 0,
    };
    this.isLoading = true;
    const sub = forkJoin([
      this.userConfigService.getListUserRole(this.paramSearch),
      this.userConfigService.searchRole({ page: 0, size: maxInt32 }),
    ]).subscribe(
      ([dataUsers, listRoles]) => {
        if (dataUsers.data) {
          if (dataUsers.data.content.length === 0 && dataUsers.data.number > 0) {
            this.paramSearch.page = 1;
            this.searchUser(true);
            return;
          }

          this.listUser = dataUsers.data.content || [];
          this.dataRole = listRoles?.data.content.filter((role: any) => {
            return rolesDefault.indexOf(role.name) === -1;
          });

          this.selected = [this.listUser[0]];
          this.userSelected = this.selected[0];
          this.HighlightRow = this.userSelected.userId;

          this.pagination.totalElements = dataUsers?.data?.totalElements;
          this.pagination.totalPages = dataUsers.data.totalPages;
          this.pagination.size = this.limit;
          this.pagination.numberOfElements = dataUsers.data.numberOfElements;

          this.numberOfElementsListUser = dataUsers.data.numberOfElements;

          const totalSize = Math.floor(this.dataRole?.length);

          this.pageableRole.totalElements = totalSize;
          this.pageableRole.totalPages = Math.floor(totalSize / this.limit);
          this.pageableRole.currentPage = Math.floor(this.paramsRole.first / this.limit + 1);
          this.pageableRole.size = this.limit;

          this.listRole =
            this.dataRole?.filter((item) => {
              return (
                item.name.toLowerCase().includes(this.paramsRole?.search?.toLowerCase()) ||
                item.description?.toLowerCase().includes(this.paramsRole?.search?.toLowerCase())
              );
            }) || [];
          this.listRole = this.listRole.slice(this.paramsRole.first, this.paramsRole.max);
          this.searchUser(true);
        }
      },
      (e) => {
        this?.keycloakService?.logout();
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }

  searchUser(isSearch: boolean) {
    const searchValue = this.searchForm.getRawValue();
    this.paramSearch.username = searchValue.username.length > 0 ? searchValue.username.trim() : '';
    if(isSearch) {
      this.paramSearch.page = 0;
    }
    this.isLoading = true;
    this.selected = [];
    this.userSelected = undefined;
    const sub = this.userConfigService.getListUserRole(this.paramSearch).subscribe(
      (result) => {
        if (result) {
          this.listUser = result?.data?.content || [];
          this.selected = this.listUser?.slice(0, 1) || [];
          this.userSelected = this.selected[0];
          this.HighlightRow = this.userSelected?.userId;

          this.pagination.totalElements = result?.data?.totalElements;
          this.pagination.totalPages = result?.data?.totalPages;
          this.pagination.size = this.limit;
          this.pagination.numberOfElements = result?.data?.numberOfElements;

          this.numberOfElementsListUser = result?.data?.numberOfElements;
          this.getRolesOfUser();
        }
      },
      () => {
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }

  searchRole(isSearch: boolean) {
    this.isLoading = true;

    if (isSearch) {
      this.paramsRole.first = 0;
      this.paramsRole.max = this.limit;
      this.paramsRole.search = this.roleSearch.trim();
    }
    const dataResult = this.filterDataRole();

    const totalSize = Math.floor(dataResult?.length);

    this.pageableRole.totalElements = totalSize;
    this.pageableRole.totalPages = Math.floor(totalSize / this.limit);
    this.pageableRole.currentPage = Math.floor(this.paramsRole.first / this.limit + 1);
    this.pageableRole.size = this.limit;

    this.listRole = dataResult || [];
    this.listRole = this.listRole.slice(this.paramsRole.first, this.paramsRole.max);
    this.isLoading = false;
  }

  filterDataRole() {
    return this.dataRole?.filter((item) => {
      return (
        item?.name?.toLowerCase().includes(this.paramsRole.search.toLowerCase()) ||
        item?.description?.toLowerCase().includes(this.paramsRole.search.toLowerCase()) ||
        !this.paramsRole.search
      );
    });
  }
  getRolesOfUser() {
    if (this.userSelected?.userId) {
      this.isLoading = true;
      const sub = this.userService.getRolesByUser(this.userSelected.userId).subscribe(
        (roles) => {
          this.roleSelected = roles.data || [];
          for (const itemRole of this.dataRole) {
            itemRole.isChecked =
              this.roleSelected?.findIndex((role) => {
                return role?.id === itemRole?.id;
              }) > -1;
          }
          this.sortDataRole(this.dataRole);
          this.searchRole(true);
          this.isLoading = false;
        },
        () => {
          this.isLoading = false;
        }
      );
      this.subs.push(sub);
    } else {
      for (const itemRole of this.dataRole) {
        itemRole.isChecked = false;
      }
      this.sortDataRole(this.dataRole);
      this.searchRole(true);
    }
  }

  sortDataRole(dataRole: Array<IRole>) {
    return dataRole?.sort((a, b) => {
      if (a?.isChecked === b?.isChecked) {
        return 0;
      } else {
        return a?.isChecked ? -1 : 1;
      }
    });
  }

  onCheckboxRoleFn(row: any, isChecked: boolean) {
    if (isChecked) {
      this.roleSelected.push(row);
    } else {
      this.roleSelected = this.roleSelected.filter((role) => {
        return role.id !== row.id;
      });
    }
    const item: any = this.dataRole.find((item) => row.id === item.id);
    item.isChecked = isChecked;
  }

  ClickedRow(event: any, dataUserSelect: any) {
    this.HighlightRow = dataUserSelect.userId;
    this.userSelected = dataUserSelect;
    this.roleSelected = [];
    this.paramsRole.search = this.roleSearch.trim();
    const params = JSON.parse(JSON.stringify(this.paramsRole));
    params.first = 0;
    params.max = maxInt32;
    this.getRolesOfUser();
  }

  onCurrentPageDataChange($event: any) {
    this.listOfCurrentPageData = $event;
  }
  updateRolesByUser(listRoleName: any): void {
    if (this.userSelected) {
      const sub = this.userService.updateRolesByUser(this.userSelected.userId, listRoleName).subscribe(
          () => {
            for (const itemRole of this.dataRole) {
              itemRole.isChecked =
                this.roleSelected.findIndex((role) => {
                  return role.id === itemRole.id;
                }) > -1;
            }
            this.sortDataRole(this.dataRole);
            this.searchRole(true);
            this.toastrCustom.success(this.translate.instant('common.notification.updateSuccess'));

            this.isLoading = false;
          },
          (e) => {
            if (e?.error?.description) {
              this.toastrCustom.error(this.translate.instant('common.notification.addError'));
            } else {
              this.toastrCustom.error(this.translate.instant('common.notification.updateError'));
            }
            this.isLoading = false;
          }
        );
      this.subs.push(sub);
    }
  }
  save() {
    if (this.roleSelected.length > 0 && this.userSelected) {
      this.isLoading = true;
      const listRoleName =
        this.roleSelected?.map((role) => {
          return { id: role.id, name: role.name };
        }) || [];
        this.updateRolesByUser(listRoleName);
    } else {
      this.isLoading = true;
      const listRoleName: any[] = [];
      this.updateRolesByUser(listRoleName);
    }
  }

  trackByUser(index: number, item: IUserPermission) {
    return item.userId;
  }

  trackByRole(index: number, item: IRole) {
    return item.id;
  }

  doImportData() {
    this.isImportData = true;
  }

  doCloseImport(isSearch: boolean) {
    this.isImportData = false;
    isSearch && this.initData();
  }

  syncUserRole() {
    this.confirmService.success().then(response => {
      if(response) {
        const syncUserRole = this.userConfigService.syncUserRole().subscribe(res => {
          localStorage.setItem(SessionKey.SYNC_USER_ROLE, "SYNC_USER_ROLE");
          this.isSyncUserRole = true;
          this.checkSyncStatus();
        });
        this.subs.push(syncUserRole);
      }
    });
  }

  checkSyncStatus() {
    this.interval = setInterval(() => {
      this.checkSyncUserRole();
    }, 10000);
  }

  checkSyncUserRole() {
    const syncStatus = this.userConfigService.getSyncStatus().subscribe((res: BaseResponse) => {
      const status = res.data;
      if(status === this.SUCCEEDED && localStorage.getItem(SessionKey.SYNC_USER_ROLE)) {
        this.toastrCustom.success(this.translate.instant(this.syncSuccess));
        localStorage.removeItem(SessionKey.SYNC_USER_ROLE);
        this.isSyncUserRole = false;
        clearInterval(this.interval);
        this.getRolesOfUser();
      } else {
        if (status !== this.SUCCEEDED && !localStorage.getItem(SessionKey.SYNC_USER_ROLE)) {
          localStorage.setItem(SessionKey.SYNC_USER_ROLE, "SYNC_USER_ROLE");
          // if (!this.isSyncUserRole) {
          //   this.checkSyncStatus();
          // }
          this.isSyncUserRole = true;
        } else if (status === this.SUCCEEDED) {
          clearInterval(this.interval);
          this.isSyncUserRole = false;
        }
      }
    }, (e: any)=> {
      this.toastrCustom.error(e?.message);
      this.isSyncUserRole = false;
      clearInterval(this.interval);
    });
    this.subs.push(syncStatus);
  }

  override ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
    clearInterval(this.interval);
  }
}
