import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserRoleComponent } from './user-role/user-role.component';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { SharedUiMbImportModule } from '@hcm-mfe/shared/ui/mb-import';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, NzFormModule, NzInputModule, TranslateModule, SharedUiMbButtonModule,
    NzTableModule, NzPaginationModule, SharedUiMbImportModule, NzCheckboxModule, FormsModule, ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: UserRoleComponent
      }
    ])
  ],
  declarations: [UserRoleComponent],
  exports: [UserRoleComponent],
})
export class SystemFeatureUserRoleConfigModule {}
