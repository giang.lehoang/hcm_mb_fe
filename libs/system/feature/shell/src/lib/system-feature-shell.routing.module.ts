import { RouterModule, Routes } from '@angular/router';
import { RoleGuardService } from '@hcm-mfe/shared/core';
import { FunctionCode, Scopes } from '@hcm-mfe/shared/common/enums';
import { NgModule } from '@angular/core';
import {SystemFeatureAutomaticAssignRoleModule} from "@hcm-mfe/system/feature/automatic-assign-role";

const router: Routes = [
  {
    path: 'applications-config',
    canActivateChild: [RoleGuardService],
    data: {
      code: FunctionCode.ADMIN_APP,
      pageName: 'system.pageName.applicaitonsConfig',
      breadcrumb: 'system.breadcrumb.applicaitonsConfig'
    },
    children: [
      {
        path: '',
        loadChildren: () => import('@hcm-mfe/system/feature/applications-config-list').then((m) => m.SystemFeatureApplicationsConfigListModule)
      },
      {
        path: 'update',
        loadChildren: () => import('@hcm-mfe/system/feature/applications-config-form').then((m) => m.SystemFeatureApplicationsConfigFormModule),
        data: {
          scope: Scopes.EDIT,
          pageName: 'system.pageName.acUpdate',
          breadcrumb: 'system.breadcrumb.acUpdate'
        },
      },
      {
        path: 'create',
        loadChildren: () => import('@hcm-mfe/system/feature/applications-config-form').then((m) => m.SystemFeatureApplicationsConfigFormModule),
        data: {
          scope: Scopes.CREATE,
          pageName: 'system.pageName.acCreate',
          breadcrumb: 'system.breadcrumb.acCreate'
        },
      },
    ]
  },
  {
    path: 'full-text-search-management',
    canActivateChild: [RoleGuardService],
    data: {
      code: FunctionCode.ADMIN_FULL_TEXT_SEARCH,
      pageName: 'system.pageName.indexConfig',
      breadcrumb: 'system.breadcrumb.indexConfig'
    },
    children: [
      {
        path: '',
        loadChildren: () => import('@hcm-mfe/system/feature/meilisearch').then((m) => m.SystemFeatureMeilisearchModule)
      },
    ]
  },
  {
    path: 'resources-category',
    canActivateChild: [RoleGuardService],
    data: {
      code: FunctionCode.ADMIN_FUNCTION,
      pageName: 'system.pageName.resourceCategory',
      breadcrumb: 'system.breadcrumb.resourceCategory'
    },
    children: [
      {
        path: '',
        loadChildren: () => import('@hcm-mfe/system/feature/resource-category-list').then((m) => m.SystemFeatureResourceCategoryListModule)
      },
      {
        path: 'update',
        loadChildren: () => import('@hcm-mfe/system/feature/resource-category-form').then((m) => m.SystemFeatureResourceCategoryFormModule),
        data: {
          scope: Scopes.EDIT,
          pageName: 'system.pageName.resourceUpdate',
          breadcrumb: 'system.breadcrumb.resourceUpdate'
        },
      },
      {
        path: 'create',
        loadChildren: () => import('@hcm-mfe/system/feature/resource-category-form').then((m) => m.SystemFeatureResourceCategoryFormModule),
        data: {
          scope: Scopes.CREATE,
          pageName: 'system.pageName.resourceCreate',
          breadcrumb: 'system.breadcrumb.resourceCreate'
        },
      },
    ]
  },
  {
    path: 'role-category',
    canActivateChild: [RoleGuardService],
    data: {
      code: FunctionCode.ADMIN_ROLE,
      pageName: 'system.pageName.roleCategory',
      breadcrumb: 'system.breadcrumb.roleCategory'
    },
    children: [
      {
        path: '',
        loadChildren: () => import('@hcm-mfe/system/feature/role-category-list').then((m) => m.SystemFeatureRoleCategoryListModule)
      },
      {
        path: 'update',
        loadChildren: () => import('@hcm-mfe/system/feature/role-category-form').then((m) => m.SystemFeatureRoleCategoryFormModule),
        data: {
          scope: Scopes.EDIT,
          pageName: 'system.pageName.rcUpdate',
          breadcrumb: 'system.breadcrumb.rcUpdate'
        },
      },
      {
        path: 'create',
        loadChildren: () => import('@hcm-mfe/system/feature/role-category-form').then((m) => m.SystemFeatureRoleCategoryFormModule),
        data: {
          scope: Scopes.CREATE,
          pageName: 'system.pageName.rcCreate',
          breadcrumb: 'system.breadcrumb.rcCreate'
        },
      },
    ]
  },
  {
    path: 'permission-config',
    canActivateChild: [RoleGuardService],
    data: {
      code: FunctionCode.ADMIN_PERMISSION,
      pageName: 'system.pageName.permissionConfig',
      breadcrumb: 'system.breadcrumb.permissionConfig'
    },
    children: [
      {
        path: '',
        loadChildren: () => import('@hcm-mfe/system/feature/permission-category-list').then((m) => m.SystemFeaturePermissionCategoryListModule)
      },
      {
        path: 'update',
        loadChildren: () => import('@hcm-mfe/system/feature/permission-category-form').then((m) => m.SystemFeaturePermissionCategoryFormModule),
        data: {
          scope: Scopes.EDIT,
          pageName: 'system.pageName.pcUpdate',
          breadcrumb: 'system.breadcrumb.pcUpdate'
        },
      },
      {
        path: 'create',
        loadChildren: () => import('@hcm-mfe/system/feature/permission-category-form').then((m) => m.SystemFeaturePermissionCategoryFormModule),
        data: {
          scope: Scopes.CREATE,
          pageName: 'system.pageName.pcCreate',
          breadcrumb: 'system.breadcrumb.pcCreate'
        },
      },
    ]
  },
  {
    path: 'manipulation-category',
    canActivateChild: [RoleGuardService],
    data: {
      code: FunctionCode.ADMIN_SCOPES,
      pageName: 'system.pageName.manipulationCategory',
      breadcrumb: 'system.breadcrumb.manipulationCategory'
    },
    children: [
      {
        path: '',
        loadChildren: () => import('@hcm-mfe/system/feature/manipulation-list').then((m) => m.SystemFeatureManipulationListModule)
      },
      {
        path: 'update',
        loadChildren: () => import('@hcm-mfe/system/feature/manipulation-form').then((m) => m.SystemFeatureManipulationFormModule),
        data: {
          scope: Scopes.EDIT,
          pageName: 'system.pageName.mcUpdate',
          breadcrumb: 'system.breadcrumb.mcUpdate'
        },
      },
      {
        path: 'create',
        loadChildren: () => import('@hcm-mfe/system/feature/manipulation-form').then((m) => m.SystemFeatureManipulationFormModule),
        data: {
          scope: Scopes.CREATE,
          pageName: 'system.pageName.mcCreate',
          breadcrumb: 'system.breadcrumb.mcCreate'
        },
      },
    ]
  },
  {
    path: 'system-config',
    canActivateChild: [RoleGuardService],
    data: {
      code: FunctionCode.ADMIN_SYSTEM,
      pageName: 'system.pageName.systemConfig',
      breadcrumb: 'system.breadcrumb.systemConfig'
    },
    children: [
      {
        path: '',
        loadChildren: () => import('@hcm-mfe/system/feature/system-config-list').then((m) => m.SystemFeatureSystemConfigListModule)
      },
      {
        path: 'update',
        loadChildren: () => import('@hcm-mfe/system/feature/system-config-form').then((m) => m.SystemFeatureSystemConfigFormModule),
        data: {
          scope: Scopes.EDIT,
          pageName: 'system.pageName.scUpdate',
          breadcrumb: 'system.breadcrumb.scUpdate'
        },
      },
      {
        path: 'create',
        loadChildren: () => import('@hcm-mfe/system/feature/system-config-form').then((m) => m.SystemFeatureSystemConfigFormModule),
        data: {
          scope: Scopes.CREATE,
          pageName: 'system.pageName.scCreate',
          breadcrumb: 'system.breadcrumb.scCreate'
        },
      },
    ]
  },
  {
    path: 'users-config',
    canActivateChild: [RoleGuardService],
    data: {
      code: FunctionCode.ADMIN_USER,
      pageName: 'system.pageName.usersConfig',
      breadcrumb: 'system.breadcrumb.usersConfig'
    },
    children: [
      {
        path: '',
        loadChildren: () => import('@hcm-mfe/system/feature/users-config-list').then((m) => m.SystemFeatureUsersConfigListModule)
      },
      {
        path: 'update',
        loadChildren: () => import('@hcm-mfe/system/feature/users-config-form').then((m) => m.SystemFeatureUsersConfigFormModule),
        data: {
          scope: Scopes.EDIT,
          pageName: 'system.pageName.ucUpdate',
          breadcrumb: 'system.breadcrumb.ucUpdate'
        },
      },
    ]
  },
  {
    path: 'template-mng',
    canActivateChild: [RoleGuardService],
    data: {
      code: FunctionCode.ADMIN_TEMPLATE_MANAGEMENT,
      pageName: 'system.pageName.templateMng',
      breadcrumb: 'system.breadcrumb.templateMng'
    },
    children: [
      {
        path: '',
        loadChildren: () => import('@hcm-mfe/system/feature/template-mng-list').then((m) => m.SystemFeatureTemplateMngListModule)
      },
      {
        path: 'create',
        loadChildren: () => import('@hcm-mfe/system/feature/template-mng-create').then((m) => m.SystemFeatureTemplateMngCreateModule),
        data: {
          scope: Scopes.CREATE,
          pageName: 'system.pageName.tmplCreate',
          breadcrumb: 'system.breadcrumb.tmplCreate'
        },
      },
      {
        path: 'edit',
        loadChildren: () => import('@hcm-mfe/system/feature/template-mng-edit').then((m) => m.SystemFeatureTemplateMngEditModule),
        data: {
          scope: Scopes.EDIT,
          pageName: 'system.pageName.tmplEdit',
          breadcrumb: 'system.breadcrumb.tmplEdit'
        },
      },
    ]
  },
  {
    path: 'permission-role',
    canActivateChild: [RoleGuardService],
    data: {
      code: FunctionCode.ADMIN_PERMISSION_ROLE,
      pageName: 'system.pageName.permissionForRole',
      breadcrumb: 'system.breadcrumb.permissionForRole'
    },
    children: [
      {
        path: '',
        loadChildren: () => import('@hcm-mfe/system/feature/permission-for-role').then((m) => m.SystemFeaturePermissionForRoleModule)
      },
    ]
  },
  {
    path: 'user-permission-config',
    canActivateChild: [RoleGuardService],
    data: {
      code: FunctionCode.ADMIN_USER_PERMISSION,
      pageName: 'system.pageName.userPermission',
      breadcrumb: 'system.breadcrumb.userPermission'
    },
    children: [
      {
        path: '',
        loadChildren: () => import('@hcm-mfe/system/feature/user-permission-config').then((m) => m.SystemFeatureUserPermissionConfigModule)
      },
    ]
  },
  {
    path: 'search-user-log',
    canActivateChild: [RoleGuardService],
    data: {
      code: FunctionCode.ADMIN_SEARCH_USER_LOG,
      pageName: 'system.pageName.searchUserLog',
      breadcrumb: 'system.breadcrumb.searchUserLog'
    },
    children: [
      {
        path: '',
        loadChildren: () => import('@hcm-mfe/system/feature/search-user-log').then((m) => m.SystemFeatureSearchUserLogModule)
      },
    ]
  },
  {
    path: 'session-history',
    canActivateChild: [RoleGuardService],
    data: {
      code: FunctionCode.ADMIN_SESSIONS_HISTORY,
      pageName: 'system.pageName.sessionHistory',
      breadcrumb: 'system.breadcrumb.sessionHistory'
    },
    children: [
      {
        path: '',
        loadChildren: () => import('@hcm-mfe/system/feature/session-history-list').then((m) => m.SystemFeatureSessionHistoryListModule)
      },
    ]
  },
  {
    path: 'user-profile',
    canActivateChild: [RoleGuardService],
    data: {
      code: FunctionCode.ADMIN_USER_PROFILE,
      pageName: 'system.pageName.userProfile',
      breadcrumb: 'system.breadcrumb.userProfile'
    },
    children: [
      {
        path: '',
        loadChildren: () => import('@hcm-mfe/system/feature/user-profile').then((m) => m.SystemFeatureUserProfileModule)
      },
    ]
  },
  {
    path: 'user-role-config',
    canActivateChild: [RoleGuardService],
    data: {
      code: FunctionCode.ADMIN_USER_ROLE,
      pageName: 'system.pageName.userRole',
      breadcrumb: 'system.breadcrumb.userRole'
    },
    children: [
      {
        path: '',
        loadChildren: () => import('@hcm-mfe/system/feature/user-role-config').then((m) => m.SystemFeatureUserRoleConfigModule)
      },
    ]
  },
  {
    path: 'session-mng',
    canActivateChild: [RoleGuardService],
    data: {
      code: FunctionCode.ADMIN_SESSION_MANAGEMENT,
      pageName: 'system.pageName.sessionMng',
      breadcrumb: 'system.breadcrumb.sessionMng'
    },
    children: [
      {
        path: '',
        loadChildren: () => import('@hcm-mfe/system/feature/session-management').then((m) => m.SystemFeatureSessionManagementModule)
      },
    ]
  },
  {
    path: 'automatic-assign-role',
    canActivateChild: [RoleGuardService],
    data: {
      code: FunctionCode.GAN_TU_DONG_VAI_TRO,
      pageName: 'system.pageName.automaticAssignRoleConfig',
      breadcrumb: 'system.breadcrumb.automaticAssignRoleConfig'
    },
    children: [
      {
        path: '',
        loadChildren: () => import('@hcm-mfe/system/feature/automatic-assign-role').then((m) => m.SystemFeatureAutomaticAssignRoleModule)
      },
    ]
  },
  {
    path: 'manager-direct',
    data: {
      pageName: 'system.pageName.managerDirect',
      breadcrumb: 'system.pageName.managerDirect'
    },
    children: [
      {
        data: {
          pageName: 'system.pageName.listManagerDirect',
          breadcrumb: 'system.pageName.listManagerDirect'
        },
        path: '',
        loadChildren: () => import('@hcm-mfe/system/feature/list-manager-direct').then((m) => m.SystemFeatureListManagerDirectModule)
      },
      {
        data: {
          pageName: 'system.pageName.assignManager',
          breadcrumb: 'system.pageName.assignManager'
        },
        path: 'assign-manager',
        loadChildren: () => import('@hcm-mfe/system/feature/assign-manager-init').then((m) => m.SystemFeatureAssignManagerInitModule)
      },
      {
        data: {
          pageName: 'system.pageName.listManagerDirect',
          breadcrumb: 'system.pageName.listManagerDirect'
        },
        path: 'list-manager-direct',
        loadChildren: () => import('@hcm-mfe/system/feature/list-manager-direct').then((m) => m.SystemFeatureListManagerDirectModule)
      },
    ]
  },
  {
    path: 'noti-group',
    canActivateChild: [RoleGuardService],
    data: {
      code: 'NOTIFY_GROUP',
      pageName: 'system.pageName.notifyGroup',
      breadcrumb: 'system.breadcrumb.notifyGroup'
    },
    children: [
      {
        path: '',
        loadChildren: () => import('@hcm-mfe/system/feature/noti-group').then((m) => m.SystemFeatureNotiGroupModule)
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(router)],
  exports: [RouterModule],
})

export class SystemFeatureShellRoutingModule {}
