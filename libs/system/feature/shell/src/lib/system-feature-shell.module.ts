import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { OverlayModule } from '@angular/cdk/overlay';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { SharedUiPopupModule } from '@hcm-mfe/shared/ui/popup';
import { SystemFeatureShellRoutingModule } from './system-feature-shell.routing.module';
import { ToastrModule } from 'ngx-toastr';
import { FormatCurrencyPipe } from '@hcm-mfe/shared/pipes/format-currency';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NzMessageModule,
    NzModalModule,
    SharedUiPopupModule,
    OverlayModule,
    SystemFeatureShellRoutingModule,
    ToastrModule.forRoot({})
  ],
  providers: [DatePipe, FormatCurrencyPipe]
})
export class SystemFeatureShellModule {}
