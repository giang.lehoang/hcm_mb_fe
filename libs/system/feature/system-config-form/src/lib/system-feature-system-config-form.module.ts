import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScFormComponent } from './sc-form/sc-form.component';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { NzFormModule } from 'ng-zorro-antd/form';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedDirectivesUppercaseInputModule } from '@hcm-mfe/shared/directives/uppercase-input';

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, NzFormModule, SharedUiMbInputTextModule, TranslateModule, SharedUiMbButtonModule, NzToolTipModule,
    FormsModule, ReactiveFormsModule, RouterModule.forChild([
      {
        path: '',
        component: ScFormComponent
      }
    ]), SharedDirectivesUppercaseInputModule
  ],
  declarations: [ScFormComponent],
  exports: [ScFormComponent],
})
export class SystemFeatureSystemConfigFormModule {}
