import { Component, OnInit, Injector } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { Config } from '@hcm-mfe/system/data-access/models';
import { CustomValidators } from '@hcm-mfe/shared/common/validators';
import { Validators } from '@angular/forms';
import { validateAllFormFields } from '@hcm-mfe/shared/common/utils';
import { SystemConfigService } from '@hcm-mfe/system/data-access/services';

@Component({
  selector: 'app-sc-form',
  templateUrl: './sc-form.component.html',
  styleUrls: ['./sc-form.component.scss'],
})
export class ScFormComponent extends BaseComponent implements OnInit {
  isUpdate = false;
  isSubmitted = false;
  data: Config | undefined;
  subs: Subscription[] = [];
  form = this.fb.group({
    cfgId: [''],
    cfgKey: ['', [CustomValidators.required, CustomValidators.code, Validators.maxLength(100)]],
    cfgValue: ['', [CustomValidators.required, Validators.maxLength(500)]],
    description: ['', Validators.maxLength(500)],
  });
  cfgId: string | undefined;
  constructor(
    injector: Injector,
    private readonly systemConfigService: SystemConfigService,
    private readonly activatedRoute: ActivatedRoute
  ) {
    super(injector);
  }

  ngOnInit(): void {
    const subRoute = this.activatedRoute.queryParams.subscribe((params: any) => {
      this.cfgId = params.cfgId;
    });
    this.subs.push(subRoute);
    if (this.cfgId) {
      this.isLoading = true;
      this.isUpdate = true;
      const sub = this.systemConfigService.getAdmConfigById(this.cfgId).subscribe(
        (res) => {
          this.data = res.data;
          this.form.patchValue(this.data ?? {});
          this.isLoading = false;
        },
        () => {
          this.isLoading = false;
        }
      );
      this.subs.push(sub);
    }
  }

  codeBlur() {
    this.form.controls['cfgKey'].setValue(this.form.controls['cfgKey'].value.trim());
  }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      this.isLoading = true;
      const request: Config | any = this.form.value;
      try {
        request.cfgValue = JSON.stringify(JSON.parse(request.cfgValue));
      } catch (e) {
        console.log('Error convert string to json');
      }
      if (!this.cfgId) {
        const subAdd = this.systemConfigService.createAdmConfig(request).subscribe(
          () => {
            this.isLoading = false;
            this.back();
            this.toastrCustom.success(this.translate.instant('common.notification.addSuccess'));
          },
          (e) => {
            this.toastrCustom.error(this.translate.instant('common.notification.addError') + `: ${e?.message}`);
            this.isLoading = false;
          }
        );
        this.subs.push(subAdd);
      } else {
        const subUpdate = this.systemConfigService.updateAdmConfig(request).subscribe(
          () => {
            this.isLoading = false;
            this.back();
            this.toastrCustom.success(this.translate.instant('common.notification.updateSuccess'));
          },
          (e) => {
            this.toastrCustom.error(this.translate.instant('common.notification.updateError') + `: ${e?.message}`);
            this.isLoading = false;
          }
        );
        this.subs.push(subUpdate);
      }
    } else {
      validateAllFormFields(this.form);
    }
  }

  format() {
    const value = this.form.controls['cfgValue'].value;
    let isJson = false;
    let jsonFormat;
    try {
      jsonFormat = JSON.parse(value);
      isJson = true;
    } catch (e) {
      isJson = false;
    }
    if(isJson && jsonFormat) {
      this.form.controls['cfgValue'].setValue(JSON.stringify(jsonFormat, null, 4))
    }
  }

  override ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
