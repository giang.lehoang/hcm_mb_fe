import { Component, OnInit } from '@angular/core';
import { NzSafeAny } from 'ng-zorro-antd/core/types';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
  pageName = '';
  rptCode?: string;
  isLoading = false;

  ngOnInit() {
    this.isLoading = true;
  }

  changePageName(event: NzSafeAny) {
    this.isLoading = false;
    this.pageName = event.pageName;
    this.rptCode = event.rrcCode;
  }
}
