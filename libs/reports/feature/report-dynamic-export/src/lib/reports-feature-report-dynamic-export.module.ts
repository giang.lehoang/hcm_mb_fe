import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from "@angular/router";
import { ReportDynamicExportComponent } from './report-dynamic-export/report-dynamic-export.component';
import {InputTextComponent} from "./report-dynamic-export/entry-components/input-text.component";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {TranslateModule} from "@ngx-translate/core";
import {ReportInputGenerateDirective} from "./report-input-generate.directive";
import {NzFormModule} from "ng-zorro-antd/form";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {InputSelectComponent} from "./report-dynamic-export/entry-components/input-select.component";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {InputDatePickerComponent} from "./report-dynamic-export/entry-components/input-date-picker.component";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {
  InputSelectCheckAbleComponent
} from "./report-dynamic-export/entry-components/input-select-check-able.component";
import {SharedUiMbSelectCheckAbleModule} from "@hcm-mfe/shared/ui/mb-select-check-able";
import {InputDataPickerComponent} from "./report-dynamic-export/entry-components/input-data-picker.component";
import {SharedUiEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/employee-data-picker";
import {InputOrgDataPickerComponent} from "./report-dynamic-export/entry-components/input-org-data-picker.component";
import {SharedUiOrgDataPickerModule} from "@hcm-mfe/shared/ui/org-data-picker";
import { NzModalModule } from 'ng-zorro-antd/modal';
import {SharedUiMbTableMergeCellWrapModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell-wrap";
import {SharedUiMbTableMergeCellModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell";
import {NzTagModule} from "ng-zorro-antd/tag";
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { LayoutComponent } from './layout/layout.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { SharedUiPopupModule } from '@hcm-mfe/shared/ui/popup';
import { OverlayModule } from '@angular/cdk/overlay';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  imports: [CommonModule,
    TranslateModule,
    NzMessageModule,
    SharedUiPopupModule,
    OverlayModule,
    ToastrModule.forRoot({}),
    RouterModule.forChild([
      {
        path: '',
        component: LayoutComponent,
        children: [
          {
            path: '',
            component: ReportDynamicExportComponent
          }
        ]
      }
    ]), SharedUiMbInputTextModule, ReactiveFormsModule, NzFormModule, SharedUiMbButtonModule, FormsModule, SharedUiMbSelectModule, SharedUiMbDatePickerModule,
    SharedUiMbSelectCheckAbleModule, SharedUiEmployeeDataPickerModule, SharedUiOrgDataPickerModule, NzModalModule, SharedUiMbTableMergeCellWrapModule, SharedUiMbTableMergeCellModule, NzTagModule, SharedUiLoadingModule, NzIconModule, NzMenuModule
  ],
  declarations: [
    LayoutComponent,
    SideBarComponent,
    ReportDynamicExportComponent,
    ReportInputGenerateDirective,
    InputTextComponent,
    InputSelectComponent,
    InputSelectCheckAbleComponent,
    InputDatePickerComponent,
    InputDataPickerComponent,
    InputOrgDataPickerComponent

  ],
  exports: [
    LayoutComponent,
    SideBarComponent,
    ReportDynamicExportComponent,
    InputTextComponent,
    InputSelectComponent,
    InputSelectCheckAbleComponent,
    InputDatePickerComponent,
    InputDataPickerComponent,
    InputOrgDataPickerComponent
  ]
})
export class ReportsFeatureReportDynamicExportModule {}
