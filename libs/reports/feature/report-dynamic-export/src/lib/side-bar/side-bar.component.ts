import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { SessionService } from '@hcm-mfe/shared/common/store';
import { environment } from '@hcm-mfe/shared/environment';
import { Subscription } from 'rxjs';
import { AppFunction } from '@hcm-mfe/shared/data-access/models';
import { ReportCategoryService } from '@hcm-mfe/reports/data-access/services';
import { CommonUtils, CustomToastrService } from '@hcm-mfe/shared/core';

export interface Menus {
  rrcCode: string;
  rrcName: string;
  isActive: boolean;
}

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss']
})

export class SideBarComponent implements OnInit {
  @Output() changePageName = new EventEmitter<{rrcCode: string | null, pageName: string}>();
  subs: Subscription[] = [];
  rrcCode?: string;
  pageName = '';
  menus: Menus[] | any = [];
  objFunction?: AppFunction;
  code = '';

  constructor(
    private route: Router,
    private http: HttpClient,
    public sessionService: SessionService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly reportCategoryService: ReportCategoryService,
    private readonly toastrCustom: CustomToastrService
  ) {
    const subRoute = this.activatedRoute.queryParams.subscribe((params: any) => {
      if (params?.code) {
        this.code = params.code.toUpperCase();
        this.objFunction = this.sessionService.getSessionData(`FUNCTION_${this.code}`);
        if (!this.objFunction?.view) {
          this.route.navigate(['/']);
        }
      }
    });
    this.subs.push(subRoute);
  }

  ngOnInit(): void {
    this.getMenu();
  }

  getMenu() {
    this.reportCategoryService.getReportCategoryByRrgCode(this.code).subscribe(res => {
      res.data.listConfig.forEach((item: Menus) => {
        this.checkScopeView(item);
      });
      this.menus = res.data.listConfig;
      this.pageName = res.data.rrgName;
      this.menus = CommonUtils.sortResource(this.menus, 'isActive', 'rrcCode');
      const item = this.menus.find((el: Menus) => el.isActive);
      if (item) {
        this.openHandler(item);
      } else {
        this.changePageName.emit({rrcCode: null, pageName: this.pageName});
      }
    }, e => {
      this.toastrCustom.error(e?.message);
    });
  }

  checkScopeView(item: Menus) {
    if (environment.isMbBank) {
      item.isActive = this.sessionService.getSessionData(`FUNCTION_${item.rrcCode}`)?.view;
    }
  }

  openHandler(menu: Menus): void {
    if (!menu.isActive) return;
    this.changePageName.emit({rrcCode: menu.rrcCode, pageName: this.pageName});
    this.rrcCode = menu.rrcCode;
  }

}
