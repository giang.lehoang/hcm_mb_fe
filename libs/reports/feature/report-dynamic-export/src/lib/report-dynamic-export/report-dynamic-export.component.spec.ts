import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportDynamicExportComponent } from './report-dynamic-export.component';

describe('ReportDynamicExportComponent', () => {
  let component: ReportDynamicExportComponent;
  let fixture: ComponentFixture<ReportDynamicExportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReportDynamicExportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportDynamicExportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
