import {AfterViewInit, Component, Input} from '@angular/core';
import {ReportInputGenerateComponent} from "../../report-input-generate.component";
import {AbstractControl} from "@angular/forms";
import {DATA_TYPE} from "@hcm-mfe/reports/data-access/common";

@Component({
  template: `
    <mb-input-text
      [mbInputType]="isNumber ? 'number' : 'text'"
      [(ngModel)]="value" (ngModelChange)="changeValue($event)" (mbBlur)="codeBlur()"
      [mbType]="isSubmitted && control.errors ? 'error' : 'default'"
      [mbShowError]="isSubmitted && control.errors !== null"
      [mbErrors]="control.errors"
      [mbErrorDefs]="[{ errorName: 'required', errorDescription: 'reports.validate.required' | translate: {'param': mbLabelText }}]"
      mbPlaceholderText="{{mbLabelText}}"
      mbLabelText="{{mbLabelText}} {{checkRequired()}}">
    </mb-input-text>
  `,
})
export class InputTextComponent implements ReportInputGenerateComponent, AfterViewInit {
  value: any;
  @Input() isSubmitted?: boolean | undefined;
  @Input() mbLabelText?: string | undefined;
  @Input() control: AbstractControl | any;
  @Input() isRequire?: boolean;
  @Input() parameterType?: string;

  isNumber = false;
  constructor() {
  }

  codeBlur() {
    if (this.control && typeof this.value === "string") {
      this.control.setValue(this.value?.trim());
    }
  }

  checkRequired(): string {
    if (this.isRequire) {
      return "<span class='label__required'>*</span>";
    }
    return '';
  }

  changeValue(event: any) {
    this.control.setValue(event)
  }

  ngAfterViewInit(): void {
    this.isNumber = this.parameterType === DATA_TYPE.NUMBER;
  }
}
