import {Component, Input} from '@angular/core';
import {ReportInputGenerateComponent} from "../../report-input-generate.component";
import {AbstractControl} from "@angular/forms";
import {DATA_TYPE} from "@hcm-mfe/reports/data-access/common";

@Component({
  template: `
    <mb-date-picker [mbShowTime]="checkDateTime()" [mbMode]="checkDateTime() ? 'time' : 'date'" [mbFormat]="checkDateTime() ? 'dd/MM/yyyy HH:mm:ss' : 'dd/MM/yyyy'"
      [(ngModel)]="value" (ngModelChange)="changeValue($event)"
      [mbType]="isSubmitted && (control.errors || controlRangeDateError) ? 'error' : 'default'"
      [mbShowError]="isSubmitted && (control.errors || controlRangeDateError) != null"
      [mbErrors]="controlRangeDateError || control.errors"
      [mbErrorDefs]="[{ errorName: 'required', errorDescription: 'reports.validate.required' | translate: {'param': mbLabelText }}]"
      mbPlaceholderText="{{mbLabelText}}"
      mbLabelText="{{mbLabelText}} {{checkRequired()}}">
    </mb-date-picker>
  `,
})
export class InputDatePickerComponent implements ReportInputGenerateComponent {
  value: any;
  @Input() isSubmitted?: boolean | undefined;
  @Input() mbLabelText?: string | undefined;
  @Input() control: AbstractControl | any;
  @Input() controlRangeDateError: AbstractControl | any;
  @Input() isRequire?: boolean;
  @Input() selectData: any[] = [];
  @Input() keyLabel = '';
  @Input() keyValue = '';
  @Input() parameterType?: string;

  constructor() {
  }

  checkDateTime(): boolean {
    return this.parameterType === DATA_TYPE.DATE_TIME;
  }

  codeBlur() {
    if (this.control) {
      this.control.setValue(this.value?.trim());
    }
  }

  checkRequired(): string {
    if (this.isRequire) {
      return "<span class='label__required'>*</span>";
    }
    return '';
  }

  changeValue(event: any) {
    this.control.setValue(event)
  }
}
