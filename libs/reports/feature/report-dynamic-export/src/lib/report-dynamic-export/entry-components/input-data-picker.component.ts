import {Component, Input} from '@angular/core';
import {ReportInputGenerateComponent} from "../../report-input-generate.component";
import {AbstractControl} from "@angular/forms";

@Component({
  template: `
    <employee-data-picker
      [(ngModel)]="value" (ngModelChange)="changeValue($event)"
      [mbCanText]="true"
      [mbType]="isSubmitted && control.errors ? 'error' : 'default'"
      [mbShowError]="isSubmitted && control.errors != null"
      [mbErrors]="control.errors"
      [mbErrorDefs]="[{ errorName: 'required', errorDescription: 'reports.validate.required' | translate: {'param': mbLabelText }}]"
      mbLabelText="{{mbLabelText}} {{checkRequired()}}">
    </employee-data-picker>
  `,
})
export class InputDataPickerComponent implements ReportInputGenerateComponent {
  value: any;
  @Input() isSubmitted?: boolean | undefined;
  @Input() mbLabelText?: string | undefined;
  @Input() control: AbstractControl | any;
  @Input() isRequire?: boolean;

  constructor() {
  }

  codeBlur() {
    if (this.control) {
      this.control.setValue(this.value?.employeeId);
    }
  }

  checkRequired(): string {
    if (this.isRequire) {
      return "<span class='label__required'>*</span>";
    }
    return '';
  }

  changeValue(event: any) {
    this.control.setValue(event?.employeeId)
  }
}
