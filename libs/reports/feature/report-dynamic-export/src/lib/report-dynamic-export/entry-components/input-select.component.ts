import { Component, Input } from '@angular/core';
import {ReportInputGenerateComponent} from "../../report-input-generate.component";
import {AbstractControl} from "@angular/forms";
import { BaseResponse } from '@hcm-mfe/shared/data-access/models';
import { CustomToastrService } from '@hcm-mfe/shared/core';
import { ReportQueueService } from '@hcm-mfe/reports/data-access/services';

@Component({
  template: `
    <mb-select
      mbKeyLabel="LABEL"
      mbKeyValue="VALUE"
      [(ngModel)]="value" (ngModelChange)="changeValue($event)" (click)="onClick()" (mbOnSearch)='onSearch()' [mbIsLoading]='isLoading'
      [mbDataSelects]="selectData"
      [mbType]="isSubmitted && control.errors ? 'error' : 'default'"
      [mbShowError]="isSubmitted && control.errors !== null"
      [mbErrors]="control.errors"
      [mbErrorDefs]="[{ errorName: 'required', errorDescription: 'reports.validate.required' | translate: {'param': mbLabelText }}]"
      mbPlaceholder="{{mbLabelText}}"
      mbLabelText="{{mbLabelText}} {{checkRequired()}}">
    </mb-select>
  `,
})
export class InputSelectComponent implements ReportInputGenerateComponent {
  value: any;
  @Input() rptCode?: string | undefined;
  @Input() rrcpId?: number | undefined;
  @Input() isSubmitted?: boolean | undefined;
  @Input() mbLabelText?: string | undefined;
  @Input() control: AbstractControl | any;
  @Input() isRequire?: boolean;

  selectData: any[] = [];
  isGetData = false;
  isLoading = false;
  constructor(
    private toastrCustom: CustomToastrService,
    private reportQueueService: ReportQueueService) {
  }

  onClick() {
    if(!this.isGetData) {
      this.getDataSelect();
    }
  }

  onSearch() {
    if(!this.isGetData) {
      this.getDataSelect();
    }
  }

  codeBlur() {
    if (this.control) {
      this.control.setValue(this.value?.trim());
    }
  }

  checkRequired(): string {
    if (this.isRequire) {
      return "<span class='label__required'>*</span>";
    }
    return '';
  }

  changeValue(event: any) {
    this.control.setValue(event)
  }

  private getDataSelect() {
    this.isLoading = true;
    this.isGetData = true;
    this.reportQueueService.getSelectDataByRptCodeAndParamCode({id: this.rrcpId}).subscribe((res:BaseResponse)=> {
      this.selectData = res.data;
      this.isLoading = false;
    }, (err) => {
      this.isLoading = false;
      this.toastrCustom.error(err?.message);
    })
  }
}
