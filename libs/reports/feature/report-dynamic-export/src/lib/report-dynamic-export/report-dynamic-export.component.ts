import {
  Component, ComponentFactory,
  ComponentFactoryResolver,
  Injector, Input, OnChanges,
  OnInit, SimpleChanges,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { AbstractControl, FormControl, FormGroup } from '@angular/forms';
import { CustomValidators } from '@hcm-mfe/shared/common/validators';
import { InputTextComponent } from './entry-components/input-text.component';
import { ReportInputGenerateDirective } from '../report-input-generate.directive';
import { ReportInputGenerateComponent } from '../report-input-generate.component';
import { Constant, DATA_TYPE } from '@hcm-mfe/reports/data-access/common';
import { InputDataPickerComponent } from './entry-components/input-data-picker.component';
import { InputOrgDataPickerComponent } from './entry-components/input-org-data-picker.component';
import { InputDatePickerComponent } from './entry-components/input-date-picker.component';
import { InputSelectComponent } from './entry-components/input-select.component';
import { InputSelectCheckAbleComponent } from './entry-components/input-select-check-able.component';
import { BaseResponse, Pageable, TableConfig } from '@hcm-mfe/shared/data-access/models';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { FileConstant, HTTP_STATUS_CODE, userConfig } from '@hcm-mfe/shared/common/constants';
import {
  DynamicReport, DynamicReportParam,
  DynamicReportParamInput,
  ParamBI,
  ReportCategory,
  ReportHistory
} from '@hcm-mfe/reports/data-access/models';
import { PermissionService, ReportCategoryService, ReportQueueService } from '@hcm-mfe/reports/data-access/services';
import { ActivatedRoute } from '@angular/router';
import { forkJoin, Subscription, switchMap } from 'rxjs';
import { Utils } from '@hcm-mfe/shared/common/utils';
import { CommonUtils } from '@hcm-mfe/shared/core';
import { saveAs } from 'file-saver';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { Scopes } from '@hcm-mfe/shared/common/enums';

@Component({
  selector: 'hcm-mfe-report-dynamic-export',
  templateUrl: './report-dynamic-export.component.html',
  styleUrls: ['./report-dynamic-export.component.scss']
})
export class ReportDynamicExportComponent extends BaseComponent implements OnInit, OnChanges {
  @Input() rptCode?: string;
  subs: Subscription[] = [];
  reportId?: number;
  reportStatus?: number;
  fileName?: string;
  isRunReport = false;
  interval: any;
  formSearch: FormGroup = this.fb.group([]);
  arrComponentInput: NzSafeAny[] = [];
  pageable: Pageable = new Pageable();
  listData: Array<ReportHistory> = [];
  dataDetail: ReportCategory = {};
  paramSearch = {
    rptCode: '',
    size: userConfig.pageSize,
    page: 0
  };
  isLoadingModal = false;
  fileType = '';
  listReportType: NzSafeAny[] = [];
  constant = Constant;
  tableConfig!: TableConfig;

  modalRef: NzModalRef | undefined;

  @ViewChild('selectModalTmpl', { static: true }) selectModal!: TemplateRef<NzSafeAny>;
  @ViewChild('selectModalFooterTmpl', { static: true }) selectModalFooter!: TemplateRef<NzSafeAny>;
  @ViewChild('actionTmpl', { static: true }) action!: TemplateRef<NzSafeAny>;
  @ViewChild('statusTmpl', { static: true }) status!: TemplateRef<NzSafeAny>;
  @ViewChild(ReportInputGenerateDirective) inputGenerate!: ReportInputGenerateDirective;

  constructor(
    injector: Injector,
    private componentFactoryResolver: ComponentFactoryResolver,
    private readonly reportCategoryService: ReportCategoryService,
    private readonly reportQueueService: ReportQueueService,
    private readonly permissionService: PermissionService,
    private readonly activatedRoute: ActivatedRoute
  ) {
    super(injector);
    if (!this.rptCode) {
      const subRoute = this.activatedRoute.queryParams.subscribe((params: any) => {
        if (params?.rptcode) {
          this.rptCode = params.rptcode.toUpperCase();
          this.objFunction = this.sessionService.getSessionData(`FUNCTION_${this.rptCode}`);
          if (!this.objFunction?.view) {
            this.router.navigate(['/']);
          }
          this.initReport();
        }
      });
      this.subs.push(subRoute);
    }
  }

  ngOnInit(): void {
    // this.initReport();
  }

  initReport() {
    this.formSearch = this.fb.group([]);
    this.inputGenerate?.viewContainerRef.clear();
    this.initForm();
    this.initTable();
    this.searchReportHistory(1);
  }

  ngOnChanges(changes: SimpleChanges) {
    this.rptCode = changes['rptCode'].currentValue;
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${this.rptCode}`);
    if (!this.objFunction?.view) {
      this.router.navigate(['/']);
    }
    this.initReport();
  }

  getPermission() {
    const data = {
      resourceCode: this.rptCode,
      scope: Scopes.VIEW
    };
    this.permissionService.getPermissionByResourceAndScope(data).subscribe(res => {
      res.data.forEach((item: NzSafeAny) => {
        if (this.formSearch.controls[item.dataType]) {
          this.formSearch.controls[item.dataType].setValue(item.objIds?.join(','));
        }
      });
    });
  }

  initForm() {
    if (this.rptCode) {
      this.isLoading = true;
      this.subs.push(
        this.reportCategoryService.getReportCategoryByCode(this.rptCode).subscribe({
          next: (res: BaseResponse) => {
            if (res.status === HTTP_STATUS_CODE.OK) {
              this.dataDetail = res.data;
              this.rebuildForm(res.data.rptRrcParamDTOList);
            } else {
              this.toastrCustom.error(res?.message);
            }
            this.isLoading = false;
          },
          error: (err) => {
            this.isLoading = false;
            this.toastrCustom.error(err?.message);
          }
        })
      );
    }
  }

  initTable() {
    this.tableConfig = {
      headers: [
        {
          title: 'STT',
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          fixed: true,
          fixedDir: 'left',
          width: 5
        },
        {
          title: 'reports.reportDynamic.dateTime',
          field: 'createDate', width: 50
        },
        {
          title: 'reports.reportDynamic.status',
          field: 'flagStatus', width: 50,
          tdTemplate: this.status
        },
        {
          title: ' ',
          field: '',
          tdTemplate: this.action,
          tdClassList: ['text-center'],
          fixed: true,
          fixedDir: 'right',
          width: 20
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.paramSearch.size,
      pageIndex: 1
    };
  }


  showModal() {
    for (const input of this.arrComponentInput) {
      input.instance.isSubmitted = true;
    }
    if (this.formSearch.valid) {
      this.isLoadingModal = true;
      this.reportQueueService.getListReportType(this.rptCode, { reportUrl: this.dataDetail.reportPath }).subscribe({
        next: (res: BaseResponse) => {
          if (res.status === HTTP_STATUS_CODE.OK) {
            this.listReportType = res.data.listOfTemplateFormatsLabelValues[0].listOfTemplateFormatLabelValue;
            if (this.listReportType.length > 0) {
              if (this.listReportType.some(el => el.templateFormatValue === FileConstant.PDF_TYPE)) {
                this.fileType = FileConstant.PDF_TYPE;
              } else {
                this.fileType = this.listReportType[0].templateFormatValue;
              }
            }
          } else {
            this.toastrCustom.error(res?.message);
          }
          this.isLoadingModal = false;
        },
        error: (err) => {
          this.isLoadingModal = false;
          this.toastrCustom.error(err?.message);
        }
      });
      this.modalRef = this.modal?.create({
        nzTitle: this.translate.instant('reports.label.fileType'),
        nzContent: this.selectModal,
        nzFooter: this.selectModalFooter
      });
    }
  }


  handleCancel() {
    this.modalRef?.close();
  }

  runReport() {
    this.handleCancel();
    const dataPermission = {
      resourceCode: this.rptCode,
      scope: Scopes.VIEW
    };
    this.permissionService.getPermissionByResourceAndScope(dataPermission)
      .pipe(
        switchMap((res) => {
          res.data.forEach((item: NzSafeAny) => {
            if (this.formSearch.controls[item.dataType]) {
              this.formSearch.controls[item.dataType].setValue(item.objIds?.join(','));
            }
          });
          const request: DynamicReportParamInput = {};
          this.setDataSendToServer(request);
          const data: DynamicReport = {
            rrcId: this.dataDetail.id,
            reportUrl: this.dataDetail.reportPath,
            inputParams: JSON.stringify(request)
          };

          return forkJoin(this.reportQueueService.getReportData(this.rptCode, data));
        })
      ).subscribe(([result]) => {
        this.reportId = result.data.reportId;
        this.isRunReport = true;
        this.checkSyncStatus();
      }, (err) => {
        this.toastrCustom.error(err?.message);
      }
    );
  }

  checkSyncStatus() {
    this.interval = setInterval(() => {
      this.checkSyncAutomaticAssignRole();
    }, 10000);
  }

  checkSyncAutomaticAssignRole() {
    const syncStatus = this.reportQueueService.checkStatus(this.rptCode, [this.reportId]).subscribe((res: BaseResponse) => {
      this.reportStatus = res.data[0].flagStatus;
      if (this.reportStatus === Constant.REPORT_HISTORY_STATUS_VALUE.CREATED) {
        this.toastrCustom.success(this.translate.instant('common.notification.fileSuccess'));
        this.fileName = res.data[0].fileName;
        this.isRunReport = false;
        this.downLoadReport(res.data[0].reportId, res.data[0].fileName);
        clearInterval(this.interval);
        this.searchReportHistory(1);
      }
      if (this.reportStatus === Constant.REPORT_HISTORY_STATUS_VALUE.BUG) {
        this.toastrCustom.error(res.data[0].description);
        this.isRunReport = false;
        clearInterval(this.interval);
        this.searchReportHistory(1);
      }
    }, (e: any) => {
      this.toastrCustom.error(e?.message);
      this.isRunReport = false;
      clearInterval(this.interval);
    });
    this.subs.push(syncStatus);
  }

  downLoadReport(reportId: number, fileName: string) {
    this.isLoading = true;
    const sub = this.reportQueueService.downloadReport(this.rptCode, reportId.toString()).subscribe(result => {
        const fileType: string = fileName?.split('.')[fileName?.split('.').length - 1];
        const reportFile = new Blob([result.body], { type: Constant.MIME_FILE_EXTENSIONS[fileType] });
        saveAs(reportFile, fileName);
        this.isLoading = false;
      }, (err) => {
        this.isLoading = false;
        this.toastrCustom.error(err?.message);
      }
    );
    this.subs.push(sub);
  }

  cancel(reportId: number) {
    this.isLoading = true;
    const sub = this.reportQueueService.cancel(this.rptCode, [reportId]).subscribe(result => {
        this.isLoading = false;
        this.reportId = undefined;
        this.fileName = undefined;
        this.isRunReport = false;
        clearInterval(this.interval);
        this.searchReportHistory(this.tableConfig.pageIndex ?? 1);
      }, (err) => {
        this.isLoading = false;
        this.toastrCustom.error(err?.message);
      }
    );
    this.subs.push(sub);
  }

  setDataSendToServer(request: DynamicReportParamInput) {
    request.attributeFormat = this.fileType;
    request.attributeTemplate = this.dataDetail.reportPath;
    for (const param of this.dataDetail.rptRrcParamDTOList ?? []) {
      if (!CommonUtils.isNullOrEmpty(this.formSearch.controls[param.rrcpCode].value)) {
        const item: DynamicReportParam = {
          name: param.rrcpCode,
          values: this.formSearch.controls[param.rrcpCode].value
        };
        if (!request.parameterNameValues) {
          request.parameterNameValues = { listOfParamNameValues: [] };
        }
        request.parameterNameValues?.listOfParamNameValues?.push(item);
      }
    }
  }

  searchReportHistory(page: number) {
    this.paramSearch.rptCode = this.rptCode ?? '';
    this.paramSearch.page = page - 1;
    this.isLoading = true;
    const sub = this.reportQueueService.searchReportHistory(this.paramSearch).subscribe(result => {
        if (result.data) {
          this.listData = result.data.content || [];
          this.listData.forEach(el => {
            el.createDate = Utils.convertDateTimeToSendServer(el.createDate) ?? '';
          });
          this.tableConfig.total = result.data.totalElements;
          this.tableConfig.pageIndex = this.paramSearch.page + 1;
        }
        this.isLoading = false;
      }, (err) => {
        this.isLoading = false;
        this.toastrCustom.error(err?.message);
      }
    );
    this.subs.push(sub);
  }

  public rebuildForm(listParam: ParamBI[]): void {
    for (const param of listParam) {
      if(param.isShow == 0) {
        this.formSearch.addControl(param.rrcpCode, new FormControl());
        continue;
      }
      // set Validate for new control
      const validateFn = [];
      if (param.isRequired == 1) {
        validateFn.push(CustomValidators.required);
      }
      // add controls
      this.formSearch.addControl(param.rrcpCode, new FormControl(null, validateFn));
      if (param.rrcpType === DATA_TYPE.SELECT || param.rrcpType === DATA_TYPE.MULTI_SELECT) {
        this.loadComponent(this.formSearch.controls[param.rrcpCode], param);
        continue;
      }

      if (param.rrcpType === DATA_TYPE.DATE_TIME) {
        this.loadComponent(this.formSearch.controls[param.rrcpCode], param);
        continue;
      }
      if (param.rrcpType === DATA_TYPE.NUMBER) {
        this.loadComponent(this.formSearch.controls[param.rrcpCode], param);
        continue;
      }
      this.loadComponent(this.formSearch.controls[param.rrcpCode], param);
    }
  }

  public loadComponent(control: AbstractControl, param: ParamBI): void {
    let componentFactory: ComponentFactory<InputDataPickerComponent>;
    switch (param.rrcpType) {
      case DATA_TYPE.POPUP_EMP:
        componentFactory = this.componentFactoryResolver.resolveComponentFactory(InputDataPickerComponent);
        break;
      case DATA_TYPE.POPUP_ORG:
        componentFactory = this.componentFactoryResolver.resolveComponentFactory(InputOrgDataPickerComponent);
        break;
      case DATA_TYPE.DATE:
        componentFactory = this.componentFactoryResolver.resolveComponentFactory(InputDatePickerComponent);
        break;
      case DATA_TYPE.DATE_TIME:
        componentFactory = this.componentFactoryResolver.resolveComponentFactory(InputDatePickerComponent);
        break;
      case DATA_TYPE.SELECT:
        componentFactory = this.componentFactoryResolver.resolveComponentFactory(InputSelectComponent);
        break;
      case DATA_TYPE.MULTI_SELECT:
        componentFactory = this.componentFactoryResolver.resolveComponentFactory(InputSelectCheckAbleComponent);
        break;
      case DATA_TYPE.NUMBER:
        componentFactory = this.componentFactoryResolver.resolveComponentFactory(InputTextComponent);
        break;
      default:
        componentFactory = this.componentFactoryResolver.resolveComponentFactory(InputTextComponent);
        break;
    }
    const viewContainerRef = this.inputGenerate.viewContainerRef;
    const componentRef = viewContainerRef.createComponent(componentFactory);
    (<ReportInputGenerateComponent>componentRef.instance).rptCode = this.rptCode;
    (<ReportInputGenerateComponent>componentRef.instance).rrcpId = param.id;
    (<ReportInputGenerateComponent>componentRef.instance).control = control;
    (<ReportInputGenerateComponent>componentRef.instance).isRequire = param.isRequired == 1;
    (<ReportInputGenerateComponent>componentRef.instance).mbLabelText = param.rrcpName;
    (<ReportInputGenerateComponent>componentRef.instance).parameterType = param.rrcpType;
    this.arrComponentInput.push(<ReportInputGenerateComponent>componentRef);
  }

  override ngOnDestroy(): void {
    this.subs?.forEach(sub => sub?.unsubscribe());
  }

}
