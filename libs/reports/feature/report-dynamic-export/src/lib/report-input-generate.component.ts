import {AbstractControl} from "@angular/forms";

export interface ReportInputGenerateComponent {
  rptCode?: string;
  rrcpId?: number;
  parameterName?: string;
  parameterType?: string;
  mbLabelText?: string;
  mbShowError?: boolean;
  isSubmitted?: boolean;
  isRequire?: boolean;
  control?: AbstractControl;
}
