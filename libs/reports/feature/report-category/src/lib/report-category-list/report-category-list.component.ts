import { Component, Injector, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { userConfig } from '@hcm-mfe/shared/common/constants';
import { CatalogModel, Pageable, TableConfig } from '@hcm-mfe/shared/data-access/models';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { ValidateService } from '@hcm-mfe/shared/core';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { ReportCategory, ReportGroup } from '@hcm-mfe/reports/data-access/models';
import { ReportCategoryService, ReportGroupService } from '@hcm-mfe/reports/data-access/services';
import { cleanDataForm } from '@hcm-mfe/shared/common/utils';
import { Constant, FUNCTION_CODE } from '@hcm-mfe/reports/data-access/common';

@Component({
  selector: 'hcm-mfe-report-category-list',
  templateUrl: './report-category-list.component.html',
  styleUrls: ['./report-category-list.component.scss']
})
export class ReportCategoryListComponent extends BaseComponent implements OnInit {
  subs: Subscription[] = [];
  constant = Constant;
  listData: Array<ReportCategory> = [];
  listReportGroup: Array<ReportGroup> = [];
  listStatus: CatalogModel[] = [];
  limit = userConfig.pageSize;
  paramSearch = {
    size: this.limit,
    page: 0
  };
  searchForm = this.fb.group(
    {
      rrcCode: null,
      rrcName: null,
      reportPath: null,
      rrg: null,
      flagStatus: 1,
    }
  );
  tableConfig!: TableConfig;
  @ViewChild('actionTmpl', { static: true }) action!: TemplateRef<NzSafeAny>;
  @ViewChild('statusTmpl', { static: true }) status!: TemplateRef<NzSafeAny>;
  pageable: Pageable = new Pageable();
  modalRef: NzModalRef | undefined;

  constructor(
    injector: Injector,
    private readonly reportCategoryService: ReportCategoryService,
    private readonly reportGroupService: ReportGroupService,
    public validateService: ValidateService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FUNCTION_CODE.REPORT_CATEGORY}`);
  }

  ngOnInit(): void {
    this.listStatus = Constant.REPORT_STATUS.map(item => {
      item.label = this.translate.instant(item.label);
      return item;
    })
    this.initTable();
    this.getListReportGroup();
    this.search(1);
  }

  initTable() {
    this.tableConfig = {
      headers: [
        {
          title: 'STT',
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          fixed: true,
          fixedDir: 'left',
          width: 50
        },
        {
          title: 'reports.reportCategory.rrcCode',
          field: 'rrcCode', width: 250,
        },
        {
          title: 'reports.reportCategory.rrcName',
          needEllipsis: true,
          field: 'rrcName', width: 300,
        },
        {
          title: 'reports.reportCategory.rrgId',
          needEllipsis: true,
          field: 'rrgName', width: 250,
        },
        {
          title: 'reports.reportCategory.reportPath',
          field: 'reportPath', width: 400,
        },
        {
          title: 'reports.reportCategory.flagStatus',
          tdClassList: ['text-nowrap', 'text-center'], thClassList: ['text-nowrap', 'text-center'],
          field: 'flagStatus', width: 250,
          tdTemplate: this.status,
        },
        {
          title: ' ',
          field: '',
          tdTemplate: this.action,
          tdClassList: ['text-center'],
          fixed: true,
          fixedDir: 'right',
          width: 100,
        },
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.paramSearch.size,
      pageIndex: 1,
    };
  }

  override triggerSearchEvent(): void {
    this.search(1);
  }

  search(page: number) {
    this.paramSearch.page = page - 1;
    const paramSearch = cleanDataForm(this.searchForm);
    const params = { ...paramSearch, page: this.paramSearch.page, size: this.limit };
    this.isLoading = true;
    const sub = this.reportCategoryService.searchReportCategory(params).subscribe(result => {
        if (result.data) {
          this.listData = result.data.content || [];
          this.tableConfig.total = result.data.totalElements;
          this.tableConfig.pageIndex = this.paramSearch.page + 1;
        }
        this.isLoading = false;
      },() => {
        this.isLoading = false;
      }
    );
    this.subs.push(sub);
  }

  getListReportGroup() {
    const sub = this.reportGroupService.getList().subscribe((res) => {
        this.listReportGroup = res.data
      }, (e) => {
        this.toastrCustom.error(e?.message);
      }
    );
    this.subs.push(sub);
  }

  delete(id: number) {
    this.deletePopup.showModal(() => {
      const subDelete = this.reportCategoryService.deleteReportCategoryById(id.toString()).subscribe(() => {
          this.toastrCustom.success(this.translate.instant('common.notification.deleteSuccess'));
          this.search(this.tableConfig.pageIndex ?? 1);
          this.isLoading = false;
        }, (e) => {
          this.toastrCustom.error(
            this.translate.instant('common.notification.deleteError') + `: ${e?.message}`
          );
          this.isLoading = false;
        }
      );
      this.subs.push(subDelete);
    });
  }

  create() {
    this.router.navigate([this.router.url, 'create']);
  }

  update(id: number) {
    this.isLoading = true;
    const sub = this.reportCategoryService.getReportCategoryById(id.toString()).subscribe(() => {
      this.isLoading = false;
      this.router.navigate([this.router.url, 'update'], { queryParams: { id: id } });
    }, (e) => {
      this.toastrCustom.error(
        this.translate.instant('common.notification.error') + `: ${e?.message}`
      );
      this.isLoading = false;
    });
    this.subs.push(sub);
  }

  override ngOnDestroy() {
    this.deletePopup?.modalDelete?.destroy();
    this.subs.forEach((sub) => sub.unsubscribe());
  }

}
