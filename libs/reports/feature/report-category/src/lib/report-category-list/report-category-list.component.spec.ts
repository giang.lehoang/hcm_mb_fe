import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportCategoryListComponent } from './report-category-list.component';

describe('ReportCategoryListComponent', () => {
  let component: ReportCategoryListComponent;
  let fixture: ComponentFixture<ReportCategoryListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReportCategoryListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportCategoryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
