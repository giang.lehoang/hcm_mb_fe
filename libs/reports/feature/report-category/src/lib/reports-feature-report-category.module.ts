import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportCategoryListComponent } from './report-category-list/report-category-list.component';
import { ReportCategoryFormComponent } from './report-category-form/report-category-form.component';
import { RouterModule } from '@angular/router';
import { Scopes } from '@hcm-mfe/shared/common/enums';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { NzFormModule } from 'ng-zorro-antd/form';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedDirectivesTrimInputModule } from '@hcm-mfe/shared/directives/trim-input';
import { NzInputModule } from 'ng-zorro-antd/input';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { SharedUiMbTableMergeCellWrapModule } from '@hcm-mfe/shared/ui/mb-table-merge-cell-wrap';
import { SharedUiMbTableMergeCellModule } from '@hcm-mfe/shared/ui/mb-table-merge-cell';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { SharedDirectivesUppercaseInputModule } from '@hcm-mfe/shared/directives/uppercase-input';
import { SharedUiMbButtonIconModule } from '@hcm-mfe/shared/ui/mb-button-icon';
import { SharedDirectivesNumbericModule } from '@hcm-mfe/shared/directives/numberic';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzTagModule } from 'ng-zorro-antd/tag';

@NgModule({
  imports: [RouterModule.forChild([
    {
      path: '',
      children: [
        {
          path: '',
          component: ReportCategoryListComponent
        },
        {
          path: 'create',
          data: {
            scope: Scopes.CREATE,
            pageName: 'reports.pageName.reportCategoryCreate',
            breadcrumb: 'reports.breadcrumb.reportCategoryCreate'
          },
          component: ReportCategoryFormComponent
        },
        {
          path: 'update',
          data: {
            scope: Scopes.EDIT,
            pageName: 'reports.pageName.reportCategoryUpdate',
            breadcrumb: 'reports.breadcrumb.reportCategoryUpdate'
          },
          component: ReportCategoryFormComponent
        }
      ]
    }
  ]),
    CommonModule, SharedUiLoadingModule, NzFormModule, TranslateModule, FormsModule, SharedDirectivesTrimInputModule, NzInputModule,
    SharedUiMbButtonModule, SharedUiMbTableMergeCellWrapModule, SharedUiMbTableMergeCellModule, NzIconModule, NzButtonModule, NzDropDownModule,
    ReactiveFormsModule, SharedUiMbInputTextModule, SharedDirectivesUppercaseInputModule, SharedUiMbButtonIconModule, SharedDirectivesNumbericModule,
    SharedUiMbSelectModule, NzDividerModule, NzTableModule, NzCheckboxModule, NzTagModule],
  declarations: [
    ReportCategoryListComponent,
    ReportCategoryFormComponent
  ],
})
export class ReportsFeatureReportCategoryModule {}
