import { Component, Injector, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormArray, FormGroup, Validators } from '@angular/forms';
import { HTTP_STATUS_CODE, MICRO_SERVICE, Mode } from '@hcm-mfe/shared/common/constants';
import { Subscription } from 'rxjs';
import { BaseResponse, CatalogModel } from '@hcm-mfe/shared/data-access/models';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { TranslateService } from '@ngx-translate/core';
import { HttpParams } from '@angular/common/http';
import { Constant, LOOKUP_CODE, UrlConstant } from '@hcm-mfe/reports/data-access/common';
import { ParamBI, ReportCategory, ReportGroup } from '@hcm-mfe/reports/data-access/models';
import { LookupService, ReportCategoryService, ReportGroupService } from '@hcm-mfe/reports/data-access/services';
import { ActivatedRoute } from '@angular/router';
import { CommonUtils } from '@hcm-mfe/shared/core';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { CustomValidators } from '@hcm-mfe/shared/common/validators';

@Component({
  selector: 'hcm-mfe-report-category-form',
  templateUrl: './report-category-form.component.html',
  styleUrls: ['./report-category-form.component.scss']
})
export class ReportCategoryFormComponent extends BaseComponent implements OnInit {

  mode?: Mode;
  Mode = Mode;
  form!: FormGroup;
  constant = Constant;
  readonly serviceName: string = MICRO_SERVICE.CONTRACT;

  id?: number;
  reportDataEdit?: ReportCategory;
  listReportGroup: Array<ReportGroup> = []

  listStatus: CatalogModel[] = [];
  listParamType: CatalogModel[] = [];
  listDataTypeInBI: CatalogModel[] = [];

  isGetParamBI = false;
  isSubmitted = false;
  subs: Subscription[] = [];

  @ViewChild('statusTmpl', { static: true }) status!: TemplateRef<NzSafeAny>;

  constructor(
    injector: Injector,
    private translateService: TranslateService,
    private lookupService: LookupService,
    private readonly reportCategoryService: ReportCategoryService,
    private readonly reportGroupService: ReportGroupService,
    private readonly activatedRoute: ActivatedRoute,
  ) {
    super(injector);
    const subRoute = this.activatedRoute.queryParams.subscribe((params: any) => {
      this.id = params.id;
    });
    this.subs.push(subRoute);
    this.initForm();
  }

  ngOnInit(): void {
    this.listStatus = Constant.REPORT_STATUS.map(item => {
      item.label = this.translate.instant(item.label);
      return item;
    })
    this.getDataTypeInBI();
    this.getParamType();
    this.getListReportGroup();
    // this.addParam();
    if (this.id) {
      this.getModelEdit();
    }
  }

  getModelEdit() {
    if (this.id) {
      this.subs.push(
        this.reportCategoryService.getReportCategoryById(this.id.toString()).subscribe({
          next: (res: BaseResponse) => {
            if (res.status === HTTP_STATUS_CODE.OK) {
              this.reportDataEdit = res?.data;
              this.patchValueToForm();
            } else {
              this.toastrCustom.error(res?.message);
            }
          },
          error: (err) => {
            this.toastrCustom.error(err?.message);
          }
        })
      );
    }
  }


  patchValueToForm() {
    this.form.patchValue({
      rrcCode: this.reportDataEdit?.rrcCode,
      rrcName: this.reportDataEdit?.rrcName,
      rrg: this.reportDataEdit?.rrg,
      reportPath: this.reportDataEdit?.reportPath,
      flagStatus: this.reportDataEdit?.flagStatus
    });
    this.reportDataEdit?.rptRrcParamDTOList?.forEach(el => {
      if (el.isRequired == 1) {
        el.isRequired = true;
      } else {
        el.isRequired = false;
      }

      if (el.isShow == 1) {
        el.isShow = true;
      } else {
        el.isShow = false;
      }
      this.addParam(el);
    });
  }

  initForm() {
    this.form = this.fb.group({
      rrcCode: [null, [Validators.required, CustomValidators.code] ],
      rrcName: [null, Validators.required],
      rrg: [null, Validators.required],
      reportPath: [null, Validators.required],
      flagStatus: [1, Validators.required],
      rptRrcParamDTOList: this.fb.array([])
    });
  }

  codeBlur() {
    this.form.controls['rrcCode'].setValue(this.form.controls['rrcCode'].value.trim());
  }

  getListReportGroup() {
    const sub = this.reportGroupService.getList({flagStatus: 1}).subscribe((res) => {
        this.listReportGroup = res.data
      }, (e) => {
        this.toastrCustom.error(e?.message);
      }
    );
    this.subs.push(sub);
  }

  getListParamBI() {
    const reportPath = this.form.controls['reportPath'].value;
    if (CommonUtils.isNullOrEmpty(reportPath)) {
      this.toastrCustom.error(this.translateService.instant('reports.reportCategory.validateUrl'));
      return;
    }
    this.isLoading = true;
    this.subs.push(this.reportCategoryService.getParamInBI({reportPath: reportPath}).subscribe({
      next: (res: BaseResponse) => {
        if (res.status === HTTP_STATUS_CODE.OK) {
          this.isGetParamBI = true;
          const listParamChange = this.getListParamBIChange(res?.data?.listOfParamNameValues);
          listParamChange.forEach(item => {
            this.rptRrcParamDTOList.removeAt(this.rptRrcParamDTOList.value.findIndex((el: ParamBI) => el.rrcpCode === item.rrcpCode));
          })
          res?.data?.listOfParamNameValues.forEach((el: NzSafeAny) => {
            if (!this.form.controls['rptRrcParamDTOList'].value.some((item: NzSafeAny) => item.rrcpCode === el.name)) {
              const obj: ParamBI = {
                rrcpCode: el.name,
                rrcpName: el.label,
                isRequired: false,
                isShow: false,
                biDataType: el.dataType.replace('xsd:', '')
              }
              this.addParam(obj);
            }
          });
          this.isLoading = false;
        } else {
          this.isLoading = false;
          this.toastrCustom.error(res?.message);
        }
      },
      error: (err) => {
        this.isLoading = false;
        this.toastrCustom.error(err?.message);
      }
    }));
  }

  getListParamBIChange(listParamBI: NzSafeAny[]) {
    const arrCode = listParamBI.map((item: NzSafeAny) => item.name);
    const listParam: ParamBI[] = [];
    this.form.controls['rptRrcParamDTOList'].value.forEach((e: ParamBI) => {
      const param = listParamBI.find(el => el.name === e.rrcpCode);
      if ((param && e.biDataType !== param.dataType.replace('xsd:', '')) || !arrCode.includes(e.rrcpCode)) {
        listParam.push(e);
      }
    });
    return listParam;
  }


  changeReportPath() {
    this.isGetParamBI = false;
  }

  onSave() {
    this.isSubmitted = true;
    if (!this.isGetParamBI && this.reportDataEdit?.reportPath !== this.form.controls['reportPath'].value) {
      this.toastrCustom.error(this.translateService.instant('reports.reportCategory.validateGetParamBI'));
      return;
    }
    if (this.form.valid) {
      const request: ReportCategory = {};
      this.setDataSendToServer(request);
      if(!this.id) {
        const subAdd = this.reportCategoryService.createReportCategory(request).subscribe(
          () => {
            this.isLoading = false;
            this.back();
            this.toastrCustom.success(this.translate.instant('common.notification.addSuccess'));
          },
          (error) => {
            this.toastrCustom.error(this.translate.instant('common.notification.addError') + `: ${error?.message}`);
            this.isLoading = false;
          }
        );
        this.subs.push(subAdd);
      } else {
        const subUpdate = this.reportCategoryService.updateReportCategory(request).subscribe(
          () => {
            this.isLoading = false;
            this.back();
            this.toastrCustom.success(this.translate.instant('common.notification.updateSuccess'));
          }, (error) => {
            this.toastrCustom.error(this.translate.instant('common.notification.updateError') + `: ${error?.message}`);
            this.isLoading = false;
          }
        );
        this.subs.push(subUpdate);
      }
    }
  }

  setDataSendToServer(request: ReportCategory) {
    if(this.id) {
      request.id = this.id;
    }
    request.rrcCode = this.form.controls['rrcCode'].value;
    request.rrcName = this.form.controls['rrcName'].value;
    request.rrg = this.form.controls['rrg'].value;
    request.reportPath = this.form.controls['reportPath'].value;
    request.flagStatus = this.form.controls['flagStatus'].value;
    this.form.controls['rptRrcParamDTOList'].value.forEach((item: ParamBI) => {
      if (item.isRequired) {item.isRequired = 1} else {item.isRequired = 0}
      if (item.isShow) {item.isShow = 1} else {item.isShow = 0}
    });
    request.rptRrcParamDTOList = this.form.controls['rptRrcParamDTOList'].value;
  }

  getParamType() {
    const params = new HttpParams().set('typeCode', LOOKUP_CODE.LOAI_THAM_SO);
    this.subs.push(
      this.lookupService.getListValue(UrlConstant.CATEGORY.LOOKUP_VALUE, params).subscribe(res => {
        if (res.status === HTTP_STATUS_CODE.OK) {
          this.listParamType = res.data;
        }
      }, error => {
        this.toastrCustom.error(error.message);
      })
    );
  }

  getDataTypeInBI() {
    const params = new HttpParams().set('typeCode', LOOKUP_CODE.KIEU_DU_LIEU_BI);
    this.subs.push(
      this.lookupService.getListValue(UrlConstant.CATEGORY.LOOKUP_VALUE, params).subscribe(res => {
        if (res.status === HTTP_STATUS_CODE.OK) {
          this.listDataTypeInBI = res.data;
          this.listDataTypeInBI.forEach((el: NzSafeAny) => el.value = el.value.toLowerCase());
        }
      }, error => {
        this.toastrCustom.error(error.message);
      })
    );
  }

  get rptRrcParamDTOList(): NzSafeAny {
    return this.form.controls['rptRrcParamDTOList'] as FormArray;
  }

  addParam(item?: ParamBI) {
      const param = this.fb.group({
        id: [item?.id ? item.id : null],
        rrcpCode: [item?.rrcpCode ? item.rrcpCode : null],
        rrcpName: [item?.rrcpName ? item.rrcpName : null, Validators.required],
        rrcpType: [item?.rrcpType ? item.rrcpType : null, Validators.required],
        isRequired: [item?.isRequired ? item.isRequired : false, Validators.required],
        isShow: [item?.isShow ? item.isShow : false, Validators.required],
        biDataType: [item?.biDataType ? item.biDataType : null, Validators.required],
        queryData: [item?.queryData ? item.queryData : null]
      });
      this.rptRrcParamDTOList.push(param);
  }

  changeDataType(data: FormGroup) {
    const type = data.controls['rrcpType'].value;
    if (type === 'SELECT') {
      data.controls['queryData'].addValidators([Validators.required]);
      if(CommonUtils.isNullOrEmpty(data.controls['queryData'].value)) {
        data.controls['queryData'].setErrors({required: true});
      }
    } else {
      data.controls['queryData'].clearValidators();
      data.controls['queryData'].setErrors(null);
    }
  }

  override ngOnDestroy(): void {
    this.subs?.forEach(sub => sub?.unsubscribe());
  }
}
