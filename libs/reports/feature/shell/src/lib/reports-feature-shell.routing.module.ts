import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import { RoleGuardService } from '@hcm-mfe/shared/core';
import { FUNCTION_CODE } from '@hcm-mfe/reports/data-access/common';

const routes: Routes = [
  {
    path: '',
    data: {
      pageName: 'reports.pageName.reportsConfig',
      breadcrumb: 'reports.breadcrumb.reportsConfig'
    },
    children: [
      {
        path: 'report-group',
        canActivateChild: [RoleGuardService],
        data: {
          code: FUNCTION_CODE.REPORT_GROUP,
          pageName: 'reports.pageName.reportGroup',
          breadcrumb: 'reports.breadcrumb.reportGroup'
        },
        loadChildren: () => import('@hcm-mfe/reports/feature/report-group').then((m) => m.ReportsFeatureReportGroupModule)
      },
      {
        path: 'report-category',
        canActivateChild: [RoleGuardService],
        data: {
          code: FUNCTION_CODE.REPORT_CATEGORY,
          pageName: 'reports.pageName.reportCategory',
          breadcrumb: 'reports.breadcrumb.reportCategory'
        },
        loadChildren: () => import('@hcm-mfe/reports/feature/report-category').then((m) => m.ReportsFeatureReportCategoryModule)
      }
    ]
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsFeatureShellRoutingModule {}
