import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportGroupFormComponent } from './report-group-form.component';

describe('ReportGroupFormComponent', () => {
  let component: ReportGroupFormComponent;
  let fixture: ComponentFixture<ReportGroupFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReportGroupFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportGroupFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
