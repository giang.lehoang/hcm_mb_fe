import { Component, OnInit, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { CustomValidators } from '@hcm-mfe/shared/common/validators';
import { validateAllFormFields } from '@hcm-mfe/shared/common/utils';
import { ReportGroup } from '@hcm-mfe/reports/data-access/models';
import { CommonUtils } from '@hcm-mfe/shared/core';
import { ReportGroupService } from '@hcm-mfe/reports/data-access/services';

@Component({
  selector: 'hcm-mfe-report-group-form',
  templateUrl: './report-group-form.component.html',
  styleUrls: ['./report-group-form.component.scss']
})
export class ReportGroupFormComponent extends BaseComponent implements OnInit {
  isSubmitted = false;
  data: ReportGroup = {};
  subs: Subscription[] = [];
  form = this.fb.group({
    id: [''],
    rrgCode: ['', [CustomValidators.required, CustomValidators.code, Validators.maxLength(50)]],
    rrgName: ['', [CustomValidators.required, Validators.maxLength(250)]],
    isActive: [true, [CustomValidators.required]],
    storageDay: ['0', [CustomValidators.required, CustomValidators.onlyNumber, Validators.min(0), Validators.maxLength(10)]],
    cleanDay: ['0', [CustomValidators.required, CustomValidators.onlyNumber, Validators.min(0), Validators.maxLength(10)]],
  });

  id: string | undefined;

  constructor(
    injector: Injector,
    private readonly reportGroupService: ReportGroupService,
    private readonly activatedRoute: ActivatedRoute,
  ) {
    super(injector);
  }

  onClickCleanDay() {
    if(this.form.controls['cleanDay'].value === '0') {
      this.form.controls['cleanDay'].setValue(0)
    }
  }

  onClickStorageDay() {
    if(this.form.controls['storageDay'].value === '0') {
      this.form.controls['storageDay'].setValue(0)
    }
  }

  onBlur() {
    if(this.form.controls['cleanDay'].value === 0 || CommonUtils.isNullOrEmpty(this.form.controls['cleanDay'].value)) {
      this.form.controls['cleanDay'].setValue('0')
    }
    if(this.form.controls['storageDay'].value === 0 || CommonUtils.isNullOrEmpty(this.form.controls['storageDay'].value)) {
      this.form.controls['storageDay'].setValue('0')
    }
  }

  ngOnInit(): void {
    const subRoute = this.activatedRoute.queryParams.subscribe((params: any) => {
      this.id = params.id;
    });
    this.subs.push(subRoute);
    if (this.id) {
      this.isLoading = true;
      const sub = this.reportGroupService.getReportGroupById(this.id.toString()).subscribe(
        (res) => {
          if (res.data) {
            this.data = res.data;
            if (res.data.flagStatus === 1) {
              this.data.isActive = true;
            } else {
              this.data.isActive = false;
            }
            if(this.data.storageDay === 0) {
              this.data.storageDay = '0';
            }
            if(this.data.cleanDay === 0) {
              this.data.cleanDay = '0';
            }
            this.form.patchValue(this.data ?? {});
          }
          this.isLoading = false;
        },
        () => {
          this.isLoading = false;
        }
      );
      this.subs.push(sub);
    }
  }

  codeBlur() {
    this.form.controls['rrgCode'].setValue(this.form.controls['rrgCode'].value.trim());
  }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      this.isLoading = true;
      const request: ReportGroup = this.form.value;
      if (request.isActive) {
        request.flagStatus = 1;
      } else {
        request.flagStatus = 0;
      }
      delete request.isActive;
      if (!this.id) {
        delete request.id
        const subAdd = this.reportGroupService.createReportGroup(request).subscribe(
          () => {
            this.isLoading = false;
            this.back();
            this.toastrCustom.success(this.translate.instant('common.notification.addSuccess'));
          },
          (error) => {
            this.toastrCustom.error(this.translate.instant('common.notification.addError') + `: ${error?.message}`);
            this.isLoading = false;
          }
        );
        this.subs.push(subAdd);
      } else {
        const subUpdate = this.reportGroupService.updateReportGroup(request).subscribe(
          () => {
            this.isLoading = false;
            this.back();
            this.toastrCustom.success(this.translate.instant('common.notification.updateSuccess'));
          }, (error) => {
            this.toastrCustom.error(this.translate.instant('common.notification.updateError') + `: ${error?.message}`);
            this.isLoading = false;
          }
        );
        this.subs.push(subUpdate);
      }
    } else {
      validateAllFormFields(this.form);
    }
  }

  override ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
