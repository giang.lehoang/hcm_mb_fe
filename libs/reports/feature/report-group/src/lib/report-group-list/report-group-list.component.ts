import { Component, OnInit, Injector, ViewChild, TemplateRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { BaseComponent } from '@hcm-mfe/shared/common/base-component';
import { userConfig } from '@hcm-mfe/shared/common/constants';
import { Pageable, TableConfig } from '@hcm-mfe/shared/data-access/models';
import { ValidateService } from '@hcm-mfe/shared/core';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { Constant, FUNCTION_CODE } from '@hcm-mfe/reports/data-access/common';
import { ReportGroupService } from '@hcm-mfe/reports/data-access/services';
import { ReportGroup } from '@hcm-mfe/reports/data-access/models';


@Component({
  selector: 'hcm-mfe-report-group-list',
  templateUrl: './report-group-list.component.html',
  styleUrls: ['./report-group-list.component.scss']
})
export class ReportGroupListComponent extends BaseComponent implements OnInit {
  subs: Subscription[] = [];
  textFilter = '';
  listData: Array<ReportGroup> = [];
  limit = userConfig.pageSize;
  paramSearch = {
    size: this.limit,
    page: 0,
    rrgCode: ''
  };
  tableConfig!: TableConfig;
  @ViewChild('actionTmpl', { static: true }) action!: TemplateRef<NzSafeAny>;
  @ViewChild('statusTmpl', { static: true }) status!: TemplateRef<NzSafeAny>;
  pageable: Pageable = new Pageable();
  modalRef: NzModalRef | undefined;
  constant = Constant;

  constructor(
    injector: Injector,
    private readonly reportGroupService: ReportGroupService,
    public validateService: ValidateService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FUNCTION_CODE.REPORT_GROUP}`);
  }

  ngOnInit(): void {
    this.initTable();
    this.search(1);
  }

  initTable() {
    this.tableConfig = {
      headers: [
        {
          title: 'STT',
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          fixed: true,
          fixedDir: 'left',
          width: 30
        },
        {
          title: 'reports.reportGroup.code',
          field: 'rrgCode', width: 200,
        },
        {
          title: 'reports.reportGroup.name',
          needEllipsis: true,
          field: 'rrgName', width: 350,
        },
        {
          title: 'reports.reportGroup.status',
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          field: 'flagStatus', width: 100,
          tdTemplate: this.status
        },
        {
          title: ' ',
          field: '',
          tdTemplate: this.action,
          tdClassList: ['text-center'],
          fixed: true,
          fixedDir: 'right',
          width: 70,
        },
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.paramSearch.size,
      pageIndex: 1,

    };
  }

  search(page: number) {
    this.paramSearch.rrgCode =  this.textFilter ? this.textFilter.trim() : '';
    this.paramSearch.page = page - 1;
    this.isLoading = true;
    const sub = this.reportGroupService.searchReportGroup(this.paramSearch).subscribe(result => {
        if (result.data) {
          this.listData = result.data.content || [];
          this.tableConfig.total = result.data.totalElements;
          this.tableConfig.pageIndex = this.paramSearch.page + 1;
        }
        this.isLoading = false;
      },(err) => {
        this.isLoading = false;
        this.toastrCustom.error(err?.message);
      }
    );
    this.subs.push(sub);
  }

  update(id: number) {
    this.isLoading = true;
    const sub = this.reportGroupService.getReportGroupById(id.toString()).subscribe(() => {
      this.isLoading = false;
      this.router.navigate([this.router.url, 'update'], { queryParams: { id: id } });
    }, (e) => {
      this.toastrCustom.error(
        this.translate.instant('common.notification.error') + `: ${e?.message}`
      );
      this.isLoading = false;
    });
    this.subs.push(sub);
  }

  delete(id: number) {
    this.deletePopup.showModal(() => {
      const subDelete = this.reportGroupService.deleteReportGroupById(id.toString()).subscribe(() => {
          this.toastrCustom.success(this.translate.instant('common.notification.deleteSuccess'));
          this.search(this.tableConfig.pageIndex ?? 1);
          this.isLoading = false;
        }, (e) => {
          this.toastrCustom.error(
            this.translate.instant('common.notification.deleteError') + `: ${e?.message}`
          );
          this.isLoading = false;
        }
      );
      this.subs.push(subDelete);
    });
  }

  create() {
    this.router.navigate([this.router.url, 'create']);
  }

  override ngOnDestroy() {
    this.deletePopup?.modalDelete?.destroy();
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
