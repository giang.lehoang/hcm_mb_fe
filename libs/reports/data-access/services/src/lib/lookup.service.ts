import { Injectable } from '@angular/core';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from '@hcm-mfe/reports/data-access/common';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class LookupService extends BaseService {
  readonly baseUrl = UrlConstant.REPORT_CONFIG;

  public getListValue(urlEndpoint: string, params: HttpParams) {
    return this.get(urlEndpoint, {params: params}, UrlConstant.SERVICE_REPORT);
  }

}
