import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from '@hcm-mfe/reports/data-access/common';

@Injectable({
  providedIn: 'root',
})
export class ReportGroupService extends BaseService {
  readonly baseUrl = UrlConstant.REPORT_GROUP;

  public searchReportGroup(params: any): Observable<any> {
    const url = this.baseUrl
    return this.get(url, { params }, UrlConstant.SERVICE_REPORT);
  }

  public getReportGroupById(id: string): Observable<any> {
    const url = this.baseUrl + UrlConstant.REPORT_GROUPS.BY_ID.replace('{id}', id);
    return this.get(url, undefined, UrlConstant.SERVICE_REPORT);
  }

  public deleteReportGroupById(id: string): Observable<any> {
    const url = this.baseUrl + UrlConstant.REPORT_GROUPS.BY_ID.replace('{id}', id);
    return this.delete(url, undefined, UrlConstant.SERVICE_REPORT);
  }

  public getList(params?: { flagStatus: number }): Observable<any> {
    const url = this.baseUrl + UrlConstant.REPORT_GROUPS.GET_LIST;
    return this.get(url, params ? { params } : undefined, UrlConstant.SERVICE_REPORT);
  }

  public createReportGroup(data: any): Observable<any> {
    const url = this.baseUrl;
    return this.post(url, data, undefined, UrlConstant.SERVICE_REPORT);
  }

  public updateReportGroup(data: any): Observable<any> {
    const url = this.baseUrl;
    return this.put(url, data, UrlConstant.SERVICE_REPORT);
  }

}
