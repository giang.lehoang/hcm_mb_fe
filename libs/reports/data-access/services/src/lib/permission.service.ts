import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from '@hcm-mfe/reports/data-access/common';

@Injectable({
  providedIn: 'root',
})
export class PermissionService extends BaseService {

  public getPermissionByResourceAndScope(params: any): Observable<any> {
    const url = UrlConstant.ADMIN_PERMISSION;
    return this.get(url, { params }, UrlConstant.SERVICE_ADMIN);
  }


}
