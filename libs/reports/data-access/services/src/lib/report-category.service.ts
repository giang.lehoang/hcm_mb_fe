import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from '@hcm-mfe/reports/data-access/common';

@Injectable({
  providedIn: 'root',
})
export class ReportCategoryService extends BaseService {
  readonly baseUrl = UrlConstant.REPORT_CONFIG;

  public searchReportCategory(params: any): Observable<any> {
    const url = this.baseUrl
    return this.get(url, { params }, UrlConstant.SERVICE_REPORT);
  }

  public getReportCategoryById(id: string): Observable<any> {
    const url = this.baseUrl + UrlConstant.REPORT_CONFIGS.BY_ID.replace('{id}', id);
    return this.get(url, undefined, UrlConstant.SERVICE_REPORT);
  }

  public getReportCategoryByCode(rptCode: string): Observable<any> {
    const url = this.baseUrl + UrlConstant.REPORT_CONFIGS.BY_RPT_CODE.replace('{rptCode}', rptCode);
    return this.get(url, undefined, UrlConstant.SERVICE_REPORT);
  }

  public deleteReportCategoryById(id: string): Observable<any> {
    const url = this.baseUrl + UrlConstant.REPORT_CONFIGS.BY_ID.replace('{id}', id);
    return this.delete(url, undefined, UrlConstant.SERVICE_REPORT);
  }

  public getParamInBI(params: {reportPath: string}): Observable<any> {
    const url = this.baseUrl + UrlConstant.REPORT_CONFIGS.GET_PARAM_BY_URL;
    return this.get(url, { params }, UrlConstant.SERVICE_REPORT);
  }

  public createReportCategory(data: any): Observable<any> {
    const url = this.baseUrl;
    return this.post(url, data, undefined, UrlConstant.SERVICE_REPORT);
  }

  public updateReportCategory(data: any): Observable<any> {
    const url = this.baseUrl;
    return this.put(url, data, UrlConstant.SERVICE_REPORT);
  }

  public getReportCategoryByRrgCode(code: string): Observable<any> {
    const url = this.baseUrl + UrlConstant.REPORT_CONFIGS.GET_LIST_BY_RRG_CODE.replace('{code}', code);
    return this.get(url, undefined, UrlConstant.SERVICE_REPORT);
  }

}
