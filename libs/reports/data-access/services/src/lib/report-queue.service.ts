import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from '@hcm-mfe/reports/data-access/common';

@Injectable({
  providedIn: 'root',
})
export class ReportQueueService extends BaseService {
  readonly baseUrl = UrlConstant.REPORT_QUEUE;

  public searchReportHistory(params: any): Observable<any> {
    const url = this.baseUrl + UrlConstant.REPORT_QUEUES.SEARCH_HISTORY
    return this.get(url, { params }, UrlConstant.SERVICE_REPORT);
  }

  public getReportData(rptCode: string, data: any): Observable<any> {
    const url = this.baseUrl + UrlConstant.REPORT_QUEUES.GET_DATA.replace('{rptCode}', rptCode);
    return this.post(url, data, undefined, UrlConstant.SERVICE_REPORT);
  }

  public cancel(rptCode: string, data: number[]): Observable<any> {
    const url = this.baseUrl + UrlConstant.REPORT_QUEUES.CANCEL.replace('{rptCode}', rptCode);
    return this.post(url, data, undefined, UrlConstant.SERVICE_REPORT);
  }

  public checkStatus(rptCode: string, data: number[]): Observable<any> {
    const url = this.baseUrl + UrlConstant.REPORT_QUEUES.CHECK_STATUS.replace('{rptCode}', rptCode);
    return this.post(url, data, undefined, UrlConstant.SERVICE_REPORT);
  }

  public downloadReport(rptCode: string, reportId: string): Observable<any> {
    const url = this.baseUrl + UrlConstant.REPORT_QUEUES.DOWNLOAD.replace('{rptCode}', rptCode).replace('{reportId}', reportId);
    return this.getRequestFileD2T(url, undefined, UrlConstant.SERVICE_REPORT);
  }

  public getSelectDataByRptCodeAndParamCode(params: {id?: number}): Observable<any> {
    const url = this.baseUrl + UrlConstant.REPORT_QUEUES.GET_SELECT_DATA;
    return this.post(url, params, undefined, UrlConstant.SERVICE_REPORT);
  }

  public getListReportType(rptCode: string, params: {reportUrl?: string}): Observable<any> {
    const url = this.baseUrl + UrlConstant.REPORT_QUEUES.GET_LIST_REPORT_TYPE.replace('{rptCode}', rptCode);
    return this.post(url, params, undefined, UrlConstant.SERVICE_REPORT);
  }

  public getMenuByResourceCode(resource: string): Observable<any> {
    const url = this.baseUrl + UrlConstant.REPORT_QUEUES.GET_LIST_REPORT_TYPE;
    return this.get(url, undefined, UrlConstant.SERVICE_ADMIN);
  }

}
