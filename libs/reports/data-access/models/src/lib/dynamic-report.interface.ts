import { NzSafeAny } from 'ng-zorro-antd/core/types';

export interface DynamicReport {
  rrcId?: number;
  reportUrl?: string;
  inputParams?: string;
}

export interface DynamicReportParamInput {
  attributeFormat?: string,
  attributeTemplate?: string,
  parameterNameValues?: {
    listOfParamNameValues?: DynamicReportParam[]
  }
}

export interface DynamicReportParam {
  name?: string;
  values?: NzSafeAny;
}

export interface ReportHistory {
  createDate?: string
  flagStatus?: number
  reportId?: number
  inputParams?: string
  rrcId?: number
  description?: string
  expirationDate?: string
  fileExpirationDate?: string
  fileName?: string
  reportUrl?: string
}

