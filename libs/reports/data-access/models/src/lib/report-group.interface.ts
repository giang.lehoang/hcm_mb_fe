export interface ReportGroup {
  id?: number
  rrgCode?: string
  rrgName?: string
  isActive?: boolean
  flagStatus?: number
  storageDay?: number | string
  cleanDay?: number | string
}
