import { NzSafeAny } from 'ng-zorro-antd/core/types';

export interface ReportCategory {
  id?: number;
  rrcCode?: string;
  rrcName?: string;
  rrg?: number;
  rrgName?: string;
  reportPath?: string;
  flagStatus?: number;
  rptRrcParamDTOList?: Array<ParamBI>;
}

export interface ParamBI {
  id?: number;
  rrc?: number;
  rrcpCode: string;
  rrcpName?: string;
  rrcpType?: string;
  isRequired?: boolean | number | string;
  isShow?: boolean | number | string;
  biDataType?: string;
  queryData?: string;
}
