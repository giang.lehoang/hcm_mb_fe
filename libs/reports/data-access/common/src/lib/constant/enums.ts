export enum FUNCTION_CODE {
  REPORT_CATEGORY = 'REPORT_CATEGORY',
  REPORT_GROUP = 'REPORT_GROUP',
}

export enum LOOKUP_CODE {
  LOAI_THAM_SO = 'LOAI_THAM_SO',
  KIEU_DU_LIEU_BI = 'KIEU_DU_LIEU_BI'
}

export enum DATA_TYPE {
  DATE = 'DATE',
  DATE_TIME = 'DATE_TIME',
  NUMBER = 'NUMBER',
  POPUP_EMP = 'POPUP_EMP',
  POPUP_ORG = 'POPUP_ORG',
  SELECT = 'SELECT',
  MULTI_SELECT = 'MULTI_SELECT',
  TEXT = 'TEXT'
}
