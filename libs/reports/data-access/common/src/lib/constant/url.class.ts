export class UrlConstant {

  public static readonly API_VERSION = '/v1.0';
  public static readonly SERVICE_REPORT = 'REPORT';
  public static readonly SERVICE_ADMIN = 'URL_ENDPOIN_CATEGORY';
  public static readonly REPORT_GROUP = 'v1.0/report-group';
  public static readonly REPORT_CONFIG = 'v1.0/report-config';
  public static readonly REPORT_QUEUE = 'v1.0/report-queue';
  public static readonly ADMIN_PERMISSION = 'global/get-permission';

  public static readonly REPORT_CONFIGS = {
    BY_ID: '/{id}',
    BY_RPT_CODE: '/get_by_code/{rptCode}',
    GET_PARAM_BY_URL: '/get-parameters',
    GET_LIST_BY_RRG_CODE: '/get-list-by-rrg-code/{code}',
  }

  public static readonly REPORT_GROUPS = {
    BY_ID: '/{id}',
    GET_LIST: '/list-group'
  }

  public static readonly REPORT_QUEUES = {
    SEARCH_HISTORY: '/search',
    GET_DATA: '/get-data/{rptCode}',
    CANCEL: '/cancel-get-data/{rptCode}',
    DOWNLOAD: '/download-report/{rptCode}/{reportId}',
    CHECK_STATUS: '/check-status-get-data/{rptCode}',
    GET_SELECT_DATA: '/get-select-data',
    GET_LIST_REPORT_TYPE: '/get-list-report-type/{rptCode}',
  }

  public static readonly CATEGORY = {
    LOOKUP_VALUE: 'v1.0/lookup-values'
  }
}
