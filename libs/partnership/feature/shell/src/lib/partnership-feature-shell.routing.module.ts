import {NgModule} from '@angular/core';
import {RouterModule} from "@angular/router";
import {PartnershipFeaturePnsContractContractFeesModule} from "@hcm-mfe/partnership/feature/pns-contract/contract-fees";

const router = [
  {
    path: 'pns-contract',
    data: {
      pageName: 'partnership.pns-contract.pageName.contractManagement',
      breadcrumb: 'partnership.pns-contract.breadcrumb.contractManagement'
    },
    children: [
      {
        path: 'contract-proposals',
        data: {
          pageName: 'partnership.pns-contract.pageName.listSigner',
          breadcrumb: 'partnership.pns-contract.breadcrumb.listSigner'
        },
        children: [
          {
            path: '',
            data: {
              pageName: 'partnership.pns-contract.pageName.listSigner'
            },
            loadChildren: () => import('@hcm-mfe/partnership/feature/pns-contract/contract-proposals').then((m) => m.PartnershipFeatureContractProposalsModule),
          },
        ]
      },
      {
        path: 'contract-approval',
        data: {
          pageName: 'partnership.pns-contract.pageName.contractApproval',
          breadcrumb: 'partnership.pns-contract.breadcrumb.contractApproval'
        },
        children: [
          {
            path: '',
            data: {
              pageName: 'partnership.pns-contract.pageName.contractApproval',
            },
            loadChildren: () => import('@hcm-mfe/partnership/feature/pns-contract/contract-approval').then((m) => m.PartnershipFeatureContractApprovalModule),
          }
        ]
      },
      {
        path: 'contract-liquidation-list',
        data: {
          pageName: 'partnership.pns-contract.pageName.contractLiquidationList',
          breadcrumb: 'partnership.pns-contract.breadcrumb.contractLiquidationList'
        },
        loadChildren: () => import('@hcm-mfe/partnership/feature/pns-contract/contract-liquidation/contract-liquidation-list').then((m) => m.ContractLiquidationListModule),
      },
      {
        path: 'contract-fees',
        data: {
          pageName: 'partnership.pns-contract.pageName.contractFeesList',
          breadcrumb: 'partnership.pns-contract.breadcrumb.contractFeesList'
        },
        loadChildren: () => import('@hcm-mfe/partnership/feature/pns-contract/contract-fees').then((m) => m.PartnershipFeaturePnsContractContractFeesModule),
      },
    ]
  },
  {
    path: 'pns-liquidation',
    data: {
      pageName: 'partnership.pns-contract.pageName.contractManagement',
      breadcrumb: 'partnership.pns-contract.breadcrumb.contractManagement'
    },
    children: [
      {
        path: 'consultation/pns-dao-tao',
        data: {
          code: 'PNS-DAO-TAO',
          pageName: 'partnership.pns-contract.pageName.trainingReimbursement',
          breadcrumb: 'partnership.pns-contract.breadcrumb.trainingReimbursement'
        },
        loadChildren: () => import('@hcm-mfe/partnership/feature/pns-contract/contract-liquidation/dynamic-search').then((m) => m.PartnershipFeaturePnsContractContractLiquidationDynamicSearchModule),
      },
      {
        path: 'consultation/pns-dong-phuc',
        data: {
          code: 'PNS-DONG-PHUC',
          pageName: 'partnership.pns-contract.pageName.uniformReimbursement',
          breadcrumb: 'partnership.pns-contract.breadcrumb.uniformReimbursement'
        },
        loadChildren: () => import('@hcm-mfe/partnership/feature/pns-contract/contract-liquidation/dynamic-search').then((m) => m.PartnershipFeaturePnsContractContractLiquidationDynamicSearchModule),
      },
      {
        path: 'consultation/pns-tam-ung',
        data: {
          code: 'PNS-TAM-UNG',
          pageName: 'partnership.pns-contract.pageName.advanceInfo',
          breadcrumb: 'partnership.pns-contract.breadcrumb.advanceInfo'
        },
        loadChildren: () => import('@hcm-mfe/partnership/feature/pns-contract/contract-liquidation/dynamic-search').then((m) => m.PartnershipFeaturePnsContractContractLiquidationDynamicSearchModule),
      },
      {
        path: 'consultation/pns-dvns',
        data: {
          code: 'PNS-DVNS',
          pageName: 'partnership.pns-contract.pageName.hrConfirm',
          breadcrumb: 'partnership.pns-contract.breadcrumb.hrConfirm'
        },
        loadChildren: () => import('@hcm-mfe/partnership/feature/pns-contract/contract-liquidation/dynamic-search').then((m) => m.PartnershipFeaturePnsContractContractLiquidationDynamicSearchModule),
      },
      {
        path: 'consultation/pns-co-phieu',
        data: {
          code: 'PNS-CO-PHIEU',
          pageName: 'partnership.pns-contract.pageName.stockInfo',
          breadcrumb: 'partnership.pns-contract.breadcrumb.stockInfo'
        },
        loadChildren: () => import('@hcm-mfe/partnership/feature/pns-contract/contract-liquidation/dynamic-search').then((m) => m.PartnershipFeaturePnsContractContractLiquidationDynamicSearchModule),
      },
      {
        path: 'consultation/pns-du-no',
        data: {
          code: 'PNS-DU-NO',
          pageName: 'partnership.pns-contract.pageName.creditBalanceInfo',
          breadcrumb: 'partnership.pns-contract.breadcrumb.creditBalanceInfo'
        },
        loadChildren: () => import('@hcm-mfe/partnership/feature/pns-contract/contract-liquidation/dynamic-search').then((m) => m.PartnershipFeaturePnsContractContractLiquidationDynamicSearchModule),
      },
    ]
  },
  {
    path: 'admin-config',
    data: {
      pageName: 'partnership.pns-contract.breadcrumb.adminConfig',
      breadcrumb: 'partnership.pns-contract.breadcrumb.adminConfig'
    },
    children: [
      {
        path: 'config-approvals',
        data: {
          pageName: 'partnership.pns-contract.pageName.configApprovals',
          breadcrumb: 'partnership.pns-contract.breadcrumb.configApprovals'
        },
        loadChildren: () => import('@hcm-mfe/partnership/feature/pns-contract/config-approvals').then((m) => m.PartnershipFeatureConfigApprovalsModule),
      },
      {
        path: 'config-seq',
        loadChildren: () => import('@hcm-mfe/partnership/feature/pns-contract/config-seq').then((m) => m.PartnershipFeatureConfigSeqModule),
        data: {
          pageName: 'partnership.pns-contract.pageName.configSeq',
          breadcrumb: 'partnership.pns-contract.breadcrumb.configSeq'
        }
      },

      {
        path: 'form-configuration',
        data: {
          pageName: 'partnership.pns-contract.pageName.formConfiguration',
          breadcrumb: 'partnership.pns-contract.breadcrumb.formConfiguration'
        },
        children: [
          {
            path: '',
            data: {
              pageName: 'partnership.pns-contract.pageName.updateFormConfiguration',
            },
            loadChildren: () => import('@hcm-mfe/partnership/feature/pns-contract/contract-template').then((m) => m.PartnershipFeatureContractTemplateModule),
          }
        ]
      },
      {
        path: 'contract-types/search',
        loadChildren: () => import('@hcm-mfe/partnership/feature/pns-contract/contract-types').then((m) => m.PartnershipFeatureContractTypesModule),
        data: {
          pageName: 'partnership.pns-contract.pageName.contractType',
          breadcrumb: 'partnership.pns-contract.breadcrumb.contractType'
        }
      },
    ],
  },
]

@NgModule({
  imports: [RouterModule.forChild(router)],
  exports: [RouterModule]
})
export class PartnershipFeatureShellRoutingModule {}
