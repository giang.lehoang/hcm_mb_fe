import { NgModule } from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {PartnershipFeatureShellRoutingModule} from "./partnership-feature-shell.routing.module";
import {SharedUiCoreModule} from "@hcm-mfe/shared/ui/core";
import {FormatCurrencyPipe} from "@hcm-mfe/shared/pipes/format-currency";

@NgModule({
  imports: [CommonModule, PartnershipFeatureShellRoutingModule, SharedUiCoreModule],
  providers: [DatePipe, FormatCurrencyPipe]
})
export class PartnershipFeatureShellModule {}
