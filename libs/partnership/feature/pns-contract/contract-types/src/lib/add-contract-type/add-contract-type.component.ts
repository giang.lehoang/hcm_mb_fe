import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpParams } from '@angular/common/http';
import { CatalogModel } from '@hcm-mfe/shared/data-access/models';
import {Constant, UrlConstant} from '@hcm-mfe/partnership/data-access/pns-contract/common';
import {ContractTypesService, SearchFormService} from "@hcm-mfe/partnership/data-access/pns-contract/services";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {ContractTypes} from "@hcm-mfe/partnership/data-access/pns-contract/models";

@Component({
  selector: 'app-add-contract-type',
  templateUrl: './add-contract-type.component.html',
  styleUrls: ['./add-contract-type.component.scss']
})
export class AddContractTypeComponent implements OnInit {

  form!: FormGroup;
  modal!: NzModalRef;
  contractTypeId!: number;

  listEmpTypeCode: CatalogModel[] = [];
  listClassify: CatalogModel[] = [];
  listType: CatalogModel[] = [];

  isSubmitted = false;

  constant = Constant;
  subscriptions: Subscription[] = [];
  isLoadingPage = false;

  constructor(
    private fb: FormBuilder,
    private toastrService: ToastrService,
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private modalService: NzModalService,
    private modalRef: NzModalRef,
    private searchFormService: SearchFormService,
    private contractTypeService: ContractTypesService
  ) { }

  ngOnInit(): void {
    this.getListEmpTypeCode()
    this.initListDataSelect();
    this.getListClassify();
    this.initFormGroup();
    if (this.contractTypeId) {
      this.pathValueForm();
    }
  }

  initFormGroup() {
    this.form = this.fb.group({
      code: [null, [Validators.required]],
      name: [null, [Validators.required]],
      empTypeCode: [null, [Validators.required]],
      classifyCode: [null, [Validators.required]],
      duration: [null],
      type: [null, [Validators.required]],
      note: [null]
    });
  }

  pathValueForm() {
    this.subscriptions.push(
      this.contractTypeService.getContractTypeById(this.contractTypeId).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.form.patchValue(res.data);
        }
      }, error => this.toastrService.error(error.message))
    )
  }

  getListEmpTypeCode() {
    const params = new HttpParams().set('typeCode', Constant.TYPE_CODE.DOI_TUONG_CV);
    this.subscriptions.push(
      this.searchFormService.getListValue(UrlConstant.CATEGORY.LOOKUP_VALUE, params).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listEmpTypeCode = res.data;
        }
      }, error => {
        this.toastrService.error(error.message);
      })
    );
  }

  getListClassify() {
    const params = new HttpParams().set('typeCode', Constant.TYPE_CODE.PHAN_LOAI_HD);
    this.subscriptions.push(
      this.searchFormService.getListValue(UrlConstant.CATEGORY.LOOKUP_VALUE, params).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listClassify = res.data;
        }
      }, error => {
        this.toastrService.error(error.message);
      })
    );
  }

  initListDataSelect() {
    this.listType = Constant.LIST_CONTRACT_TYPE.map(item => {
      item.label = this.translate.instant(item.label);
      return item;
    })
  }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      this.isLoadingPage = true;
      const value: ContractTypes = this.form.value;
      value.contractTypeId = this.contractTypeId;
      const formData = new FormData();
      formData.append('data', new Blob([JSON.stringify(value)], {
        type: 'application/json'
      }));

      this.subscriptions.push(
        this.contractTypeService.saveContractType(formData).subscribe(res => {
          if (res && res.code === HTTP_STATUS_CODE.OK) {
            this.toastrService.success(this.translate.instant('common.notification.updateSuccess'));
            this.modalRef.close({refresh:true});
          } else {
            this.toastrService.error(this.translate.instant('common.notification.updateError') + ': ' + res?.message);
          }
          this.isLoadingPage = false;
        }, error => {
            this.toastrService.error(error.message);
            this.isLoadingPage = false;
        })
      )
    }
  }


  closeModalAdd() {
    this.modalService.closeAll();
  }

  ngOnDestroy(): void {
    this.subscriptions?.forEach(sub => sub.unsubscribe());
  }

}
