import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {TranslateModule} from "@ngx-translate/core";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {NzIconModule} from "ng-zorro-antd/icon";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {NzTagModule} from "ng-zorro-antd/tag";
import {PartnershipUiSearchFormModule} from "@hcm-mfe/partnership/ui/pns-contract/search-form";
import {RouterModule} from "@angular/router";
import {SharedUiMbTableMergeCellWrapModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell-wrap";
import {SharedUiMbTableMergeCellModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell";
import {PartnershipUiImportCommonModule} from "@hcm-mfe/partnership/ui/pns-contract/import-common";
import {ContractTypesComponent} from "./contract-types/contract-types.component";
import {PartnershipUiSearchFormContractTypeModule} from "@hcm-mfe/partnership/ui/pns-contract/search-form-contract-type";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {SharedUiMbEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/mb-employee-data-picker";
import {PartnershipUiPersonalInfoModule} from "@hcm-mfe/partnership/ui/pns-contract/personal-info";
import {AddContractTypeComponent} from "./add-contract-type/add-contract-type.component";
import {NzFormModule} from "ng-zorro-antd/form";
import {NzPopconfirmModule} from "ng-zorro-antd/popconfirm";
import {ReactiveFormsModule} from "@angular/forms";
import {SharedDirectivesNumberInputModule} from "@hcm-mfe/shared/directives/number-input";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, SharedUiMbButtonModule,
    TranslateModule, NzButtonModule, NzDropDownModule, NzIconModule, SharedUiMbTableWrapModule, SharedUiMbTableModule, NzTagModule, PartnershipUiSearchFormModule,
    RouterModule.forChild([
      {
        path: '',
        component: ContractTypesComponent
      }
    ]), SharedUiMbTableMergeCellWrapModule, SharedUiMbTableMergeCellModule, PartnershipUiImportCommonModule, PartnershipUiSearchFormContractTypeModule,
    SharedUiMbInputTextModule, SharedUiMbSelectModule, SharedUiMbDatePickerModule, SharedUiMbEmployeeDataPickerModule, PartnershipUiPersonalInfoModule,
    NzFormModule, NzPopconfirmModule, ReactiveFormsModule, SharedDirectivesNumberInputModule, NzToolTipModule],
  declarations: [ContractTypesComponent, AddContractTypeComponent],
  exports: [ContractTypesComponent]
})
export class PartnershipFeatureContractTypesModule {}
