import { Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { HttpParams } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { saveAs } from 'file-saver';
import {AppFunction, MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {ContractTypes} from "@hcm-mfe/partnership/data-access/pns-contract/models";
import {SearchFormContractTypeComponent} from "@hcm-mfe/partnership/ui/pns-contract/search-form-contract-type";
import {ContractTypesService} from "@hcm-mfe/partnership/data-access/pns-contract/services";
import {Constant, FunctionCode, UrlConstant} from "@hcm-mfe/partnership/data-access/pns-contract/common";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {getTypeExport} from "@hcm-mfe/shared/common/utils";
import { AddContractTypeComponent } from '../add-contract-type/add-contract-type.component';
import {SessionService} from "@hcm-mfe/shared/common/store";

@Component({
  selector: 'app-contract-types',
  templateUrl: './contract-types.component.html',
  styleUrls: ['./contract-types.component.scss']
})
export class ContractTypesComponent implements OnInit, OnDestroy {

  pagination = new Pagination();
  tableConfig!: MBTableConfig;
  searchResult!: ContractTypes[];
  isLoadingPage = false;
  subscriptions: Subscription[] = [];
  modal!: NzModalRef;
  functionCode = FunctionCode.PNS_CONTRACT_TYPE;
  objFunction!: AppFunction;


  @ViewChild('searchForm') searchForm!: SearchFormContractTypeComponent;
  @ViewChild('actionTmpl', {static: true}) action!: TemplateRef<NzSafeAny>;

  constructor(
    private contractTypeService: ContractTypesService,
    private translateService: TranslateService,
    private toastrService: ToastrService,
    private modalService: NzModalService,
    private sessionService: SessionService
  ) {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${this.functionCode}`);
  }

  ngOnInit(): void {
    this.initTable();
    this.doSearch(1);
  }

  doSearch(pageIndex: number) {
    this.pagination.pageNumber = pageIndex;
    const searchParam = this.searchForm? this.searchForm.parseOptions() : new HttpParams();
    this.tableConfig.loading = true;
    this.isLoadingPage = true;
    this.subscriptions.push(
      this.contractTypeService.getFilterResearch(UrlConstant.SEARCH_FORM.CONTRACT_TYPES, searchParam, this.pagination.getCurrentPage()).subscribe(res => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.searchResult = res.data.listData.map((item: ContractTypes) => {
              const typeName = Constant.LIST_CONTRACT_TYPE.find(data => data.value === item.type)?.label;
              if (typeName) {
                item.typeName = this.translateService.instant(typeName);
              }
              return item;
            });
            this.tableConfig.pageIndex = pageIndex;
            this.tableConfig.total = res.data.count;
          }
          this.tableConfig.loading = false;
          this.isLoadingPage = false;
        }, error => {
          this.toastrService.error(error.message);
          this.tableConfig.loading = true
        }
      )
    );
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'partnership.pns-contract.contractTypes.empTypeName',
          field: 'empTypeName',
          width: 150
        },
        {
          title: 'partnership.pns-contract.contractTypes.classifyName',
          field: 'classifyName',
          width: 180
        },
        {
          title: 'partnership.pns-contract.contractTypes.type',
          field: 'typeName',
          width: 180
        },
        {
          title: 'partnership.pns-contract.contractTypes.code',
          field: 'code',
          width: 150
        },
        {
          title: 'partnership.pns-contract.contractTypes.name',
          field: 'name',
          width: 200,
          thClassList: ['min__name-header']
        },
        {
          title: 'partnership.pns-contract.contractTypes.duration',
          field: 'duration',
          width: 120,
          tdClassList: ['text-center'],
          thClassList: ['text-center']
        },
        {
          title: 'partnership.pns-contract.contractTypes.note',
          field: 'note',
          width: 200
        },
        {
          title: ' ',
          field: 'action',
          tdClassList: ['text-center'],
          width: 60,
          tdTemplate: this.action,
          fixed: window.innerWidth > 1024,
          fixedDir: 'right',
        }
      ],
      total: 0,
      needScroll: false,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  openAddModal() {
    this.openModal();
  }

  onOpenEdit(contractTypeId: number) {
    this.openModal(contractTypeId);
  }

  openModal(contractTypeId?: number) {
    this.modal = this.modalService.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
      nzTitle: this.translateService.instant(contractTypeId ? 'partnership.pns-contract.contractTypes.editContractType' : 'partnership.pns-contract.contractTypes.addContractType'),
      nzContent: AddContractTypeComponent,
      nzComponentParams: {
        contractTypeId: contractTypeId
      },
      nzFooter: null
    });
    this.subscriptions.push(
      this.modal.afterClose.subscribe(res => {
        if (res && res.refresh) {
          this.doSearch(1);
        }
      })
    );
  }

  deleteById(contractTypeId:number) {
    this.subscriptions.push(
      this.contractTypeService.deleteContractTypeById(contractTypeId).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.doSearch(1);
          this.toastrService.success(this.translateService.instant('common.notification.deleteSuccess'));
        } else {
          this.toastrService.error(res.message);
        }
      }, error => this.toastrService.error(error.message))
    )
  }

  ngOnDestroy(): void {
    this.subscriptions?.forEach(sub => sub.unsubscribe());
  }

  doExportData() {
    this.isLoadingPage = true;
    const searchParam = this.searchForm ? this.searchForm.parseOptions() : new HttpParams();
    this.subscriptions.push(
        this.contractTypeService.doExportData(UrlConstant.EXPORT_REPORT.CONTRACT_TYPES, searchParam).subscribe(res => {
          if ((res?.headers?.get('Excel-File-Empty'))) {
            this.toastrService.error(this.translateService.instant('common.notification.exportFail'));
          } else {
            const fileName = 'Danh-sach-danh-muc-loai-hop-dong.xlsx';
            const reportFile = new Blob([res.body], { type: getTypeExport(fileName.split('.')?.pop()) });
            saveAs(reportFile, fileName);
          }
          this.isLoadingPage = false;
        }, error => {
          this.toastrService.error(error.message);
          this.isLoadingPage = false;
        })
    );
  }
}
