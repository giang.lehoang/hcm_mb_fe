import { Component, Injector, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { NzModalService } from 'ng-zorro-antd/modal';
import {Subscription} from "rxjs";
import { saveAs } from 'file-saver';
import {FunctionCode, SearchBaseComponent} from "@hcm-mfe/partnership/data-access/pns-contract/common";
import {Scopes} from "@hcm-mfe/shared/common/enums";
import {getTypeExport, Utils} from "@hcm-mfe/shared/common/utils";
import {AppFunction, CatalogModel} from "@hcm-mfe/shared/data-access/models";
import {ConfigApprovalService, ConfigSeqService, SearchFormService} from "@hcm-mfe/partnership/data-access/pns-contract/services";
import {HTTP_STATUS_CODE, STATUS} from "@hcm-mfe/shared/common/constants";
import {ToastrService} from "ngx-toastr";
import {Constant} from "@hcm-mfe/partnership/data-access/pns-contract/common";
import {TranslateService} from "@ngx-translate/core";
import { ConfigSeqFormComponent } from '../config-seq-form/config-seq-form.component';
import {SessionService} from "@hcm-mfe/shared/common/store";


@Component({
    selector: 'config-seq',
    templateUrl: './config-seq.component.html',
    styleUrls: ['./config-seq.component.scss']
})
export class ConfigSeqComponent extends SearchBaseComponent implements OnInit {
    @Input() scope: string = Scopes.VIEW;
    functionCode = FunctionCode.PNS_CONFIG_SEQ;
    subscriptions: Subscription[] = [];
    objFunction!: AppFunction;

    formSearchConfig = {
        org: [null],
        orgGroup: [null],
        empTypeCode: [null],
        positionGroupId: [null],
        fromDate: [Utils.getFirstDayOrLastDayOfCurrentMonth(true)],
        toDate: [Utils.getFirstDayOrLastDayOfCurrentMonth(false)],
    };

    orgGroupSelects: CatalogModel[] = [];

    positionGroupList: CatalogModel[] = [];
    empTypeSelect: CatalogModel[] = [];
    isSubmitted = false;

    @ViewChild('actionTmpl', { static: true }) action!: TemplateRef<NzSafeAny>;
    @ViewChild('contractTypeName', { static: true }) contractTypeName!: TemplateRef<NzSafeAny>;
    @ViewChild('scopeTmpl', { static: true }) scopeTmpl!: TemplateRef<NzSafeAny>;

    constructor(
        injector: Injector,
        private configSeqService: ConfigSeqService,
        private modalServices: NzModalService,
        private searchFormService: SearchFormService,
        private configApprovalService: ConfigApprovalService,
        public override toastrService: ToastrService,
        public override translate: TranslateService,
        private sessionService: SessionService
    ) {
        super(injector);
        this.buildFormSearch(this.formSearchConfig);
        this.setMainService(this.configSeqService);
        this.objFunction = this.sessionService.getSessionData(`FUNCTION_${this.functionCode}`);
    }

    ngOnInit(): void {
        this.initDataSelect();
        this.subscriptions.push(
          this.searchFormService.getEmpTypeList().subscribe(res => {
            if (res.code === HTTP_STATUS_CODE.OK) {
              this.empTypeSelect = res.data;
            }
          }, err => this.toastrService.error(err.message))
        );
        this.initTable();
        this.doSearch();
        this.getPositionGroupList();
    }

    initDataSelect() {
      this.orgGroupSelects = Constant.ORG_GROUP_SELECTS.map(item => {
        item.label = this.translate.instant(item.label);
        return item;
      })
    }

    showModal(footerTmpl: TemplateRef<NzSafeAny>, configSeqContractId?: number) {
        this.modal = this.modalServices.create({
            nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
            nzTitle: this.translate.instant('partnership.pns-contract.pageName.configSeq'),
            nzContent: ConfigSeqFormComponent,
            nzComponentParams: {
                configSeqContractId: configSeqContractId
            },
            nzFooter: footerTmpl
        });
        this.subs.push(
            this.modal.afterClose.subscribe(res => {
                if (res && res.refresh) {
                    this.doSearch();
                }
            })
        );
    }

    initTable(): void {
        this.tableConfig = {
            headers: [
                {
                    title: 'partnership.pns-contract.label.scope',
                    field: 'scope',
                    width: 80,
                    thClassList: ['text-center'],
                    tdTemplate: this.scopeTmpl,
                    fixedDir: 'left'
                },
                {
                    title: 'partnership.pns-contract.label.org',
                    field: 'orgName',
                    width: 100,
                    thClassList: ['text-center'],
                    fixedDir: 'left'
                },
                {
                    title: 'partnership.pns-contract.label.orgGroup',
                    field: 'orgGroup',
                    tdClassList: ['text-left'],
                    thClassList: ['text-center'],
                    width: 150
                },
                {
                    title: 'partnership.pns-contract.personalInfo.empTypeName',
                    field: 'empTypeName',
                    width: 100,
                    thClassList: ['text-center'],
                    fixedDir: 'left'
                },
                {
                    title: 'partnership.pns-contract.label.groupTitle',
                    field: 'positionGroupName',
                    width: 150,
                    thClassList: ['text-center'],
                    fixedDir: 'left'
                },
                {
                    title: 'partnership.pns-contract.label.status',
                    field: 'state',
                    tdClassList: ['text-left'],
                    thClassList: ['text-center'],
                    width: 80
                },
                {
                    title: 'partnership.label.fromDate',
                    field: 'fromDate',
                    tdClassList: ['text-center'],
                    thClassList: ['text-center'],
                    width: 90
                },
                {
                    title: 'partnership.label.toDate',
                    field: 'toDate',
                    tdClassList: ['text-center'],
                    thClassList: ['text-center'],
                    width: 90
                },
                {
                    title: 'partnership.pns-contract.label.configSeq',
                    field: 'contractTypeName',
                    tdTemplate: this.contractTypeName,
                    tdClassList: ['text-left'],
                    thClassList: ['text-left'],
                    width: 170
                },
                {
                    title: ' ',
                    field: 'action',
                    tdClassList: ['text-nowrap', 'text-center'],
                    thClassList: ['text-nowrap', 'text-center'],
                    width: 60,
                    tdTemplate: this.action,
                    fixed: window.innerWidth > 1024,
                    fixedDir: 'right'
                }
            ],
            total: 0,
            needScroll: false,
            loading: false,
            size: 'small',
            pageSize: this.pagination.pageSize,
            pageIndex: 1
        };
    }

    getPositionGroupList() {
      this.subscriptions.push(
        this.configApprovalService.getPositionGroupList().subscribe(data => {
          if (data.code === HTTP_STATUS_CODE.OK) {
            this.positionGroupList = data.data.reduce((accumulator: CatalogModel[], item: NzSafeAny) => {
              if (item.flagStatus === STATUS.ACTIVE) {
                accumulator.push({ value: '' + item.pgrId, label: item.pgrName });
              }
              return accumulator;
            }, []);
          }
        }, (err) => {
          this.toastrService.error(err.message)
        })
      );
    }

    override ngOnDestroy(): void {
      this.subscriptions?.forEach(sub => sub.unsubscribe());
      this.modal?.destroy();
    }

    doExportData() {
        this.isLoadingPage = true;
        const params = this.formSearch ? { ...this.formSearch.value } : {};
        this.subs.push(
            this.configSeqService.doExportData(params).subscribe(res => {
                if ((res?.headers?.get('Excel-File-Empty'))) {
                    this.toastrService.error(this.translate.instant('common.notification.exportFail'));
                } else {
                    const fileName = 'Danh-sach-cau-hinh-trinh-tu-ky-hop-dong.xlsx';
                    const reportFile = new Blob([res.body], { type: getTypeExport(fileName.split('.')?.pop()) });
                    saveAs(reportFile, fileName);
                }
                this.isLoadingPage = false;
            }, error => {
                this.toastrService.error(error.message);
                this.isLoadingPage = false;
            })
        );
    }

}
