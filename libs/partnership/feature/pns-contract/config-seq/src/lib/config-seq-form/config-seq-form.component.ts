import {Component, Input, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import * as moment from 'moment';
import {ToastrService} from 'ngx-toastr';
import {Subscription} from "rxjs";
import {NzModalRef} from 'ng-zorro-antd/modal';
import {Scopes} from "@hcm-mfe/shared/common/enums";
import {CatalogModel} from '@hcm-mfe/shared/data-access/models';
import {
  ConfigApprovalService,
  ConfigSeqService,
  ContractService,
  SearchFormService
} from "@hcm-mfe/partnership/data-access/pns-contract/services";
import {CustomValidators} from "@hcm-mfe/shared/common/validators";
import {HTTP_STATUS_CODE, STATUS} from "@hcm-mfe/shared/common/constants";
import {CommonUtils} from "@hcm-mfe/shared/core";
import {Constant, FunctionCode} from "@hcm-mfe/partnership/data-access/pns-contract/common";
import {NzSafeAny} from "ng-zorro-antd/core/types";


@Component({
  selector: 'config-seq-form',
  templateUrl: './config-seq-form.component.html',
  styleUrls: ['./config-seq-form.component.scss']
})
export class ConfigSeqFormComponent implements OnInit {

  @Input() scope: string = Scopes.VIEW;
  @Input() functionCode = FunctionCode.PNS_CONFIG_SEQ;
  subscriptions: Subscription[] = [];
  data = [1];
  positionGroupList:CatalogModel[] = [];
  contractTypeSelects: CatalogModel[] = [];
  form: FormGroup;
  isSubmitted = false;
  empTypeSelect: CatalogModel[] = [];
  orgGroupSelects: CatalogModel[] = [];
  scopeSelects: CatalogModel[] = [];
  detail: FormArray;
  configSeqContractId!: number;
  isLoadingPage = false;


  constructor(
    private fb: FormBuilder,
    public toastr: ToastrService,
    public translate: TranslateService,
    private configApprovalService: ConfigApprovalService,
    private searchFormService: SearchFormService,
    private contractService: ContractService,
    private configSeqService: ConfigSeqService,
    private modalRef: NzModalRef

  ) {
    this.form = this.fb.group({
      configSeqContractId: [null],
      org: [null, [Validators.required]],
      empTypeCode: [null, [Validators.required]],
      positionGroupId: [null],
      fromDate: [null, [Validators.required]],
      toDate: [null],
      documentNo: [null, [Validators.required]],
      note: [null],
      scope: [1],
      contractTypeIds: this.fb.array([])
    },
      {
        validator: [
          CustomValidators.toDateAfterFromDate('fromDate', 'toDate'),
        ]
      });
    this.detail = this.form.controls['contractTypeIds'] as FormArray;
    this.addRow();
  }


  ngOnInit(): void {
    this.initDataSelect();
    this.subscriptions.push(
      this.searchFormService.getEmpTypeList().subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.empTypeSelect = res.data;
        }
      }, err => this.toastr.error(err.message))
    );
    this.getContractType();
    this.getPositionGroupList();
    if (this.configSeqContractId) {
      this.subscriptions.push(
        this.configSeqService.getById(this.configSeqContractId).subscribe(res => {
          res.data.fromDate = moment(res.data.fromDate, 'DD/MM/YYYY').toDate();
          if (res.data.toDate) {
            res.data.toDate = moment(res.data.toDate, 'DD/MM/YYYY').toDate();
          }
          if (res.data.orgGroup) {
            this.form.controls['scope'].setValue(2)
            res.data.org = res.data.orgGroup
          }
          if (res.data.positionGroupId) {
            res.data.positionGroupId = '' + res.data.positionGroupId;
          }
          const contractTypeIds = res.data.contractTypeIds;
          delete res.data.contractTypeIds;
          CommonUtils.setFormValue(this.form, res.data);
          this.detail.clear();
          contractTypeIds.forEach((contractTypeId: number) => {
            this.detail.push(new FormControl(contractTypeId, Validators.required));
          })
        })
      );
    }
  }

  getContractType() {
    this.subscriptions.push(
      this.contractService.getContractType().subscribe(
        res => {
          this.contractTypeSelects = res.data;
        }
      )
    );
  }

  initDataSelect() {
    this.orgGroupSelects = Constant.ORG_GROUP_SELECTS.map(item => {
      item.label = this.translate.instant(item.label);
      return item;
    })

    this.scopeSelects = Constant.SCOPE_SELECTS.map(item => {
      item.label = this.translate.instant(item.label);
      return item;
    })
  }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      this.isLoadingPage = true;
      this.subscriptions.push(
        this.configSeqService.save(this.buildFormData())
          .subscribe(res => {
            if (res.code === HTTP_STATUS_CODE.OK) {
              this.toastr.success(this.translate.instant('common.notification.updateSuccess'));
                this.modalRef.close({refresh:true});
            } else {
              this.toastr.error(this.translate.instant('common.notification.error') + ": " + res?.message);
            }
            this.isLoadingPage = false;
          }, error => {
            this.toastr.error(this.translate.instant('common.notification.error') + ": " + error?.message);
            this.isLoadingPage = false;
          })
      );
    }

  }

  getPositionGroupList() {
    this.subscriptions.push(
      this.configApprovalService.getPositionGroupList().subscribe(data => {
        if (data.code === HTTP_STATUS_CODE.OK) {
          this.positionGroupList = data.data.reduce((accumulator: CatalogModel[], item: NzSafeAny) => {
            if (item.flagStatus === STATUS.ACTIVE) {
              accumulator.push({ value: '' + item.pgrId, label: item.pgrName });
            }
            return accumulator;
          }, []);
        }
      }, (err) => {
        this.toastr.error(err.message)
      })
    )
  }

  buildFormData() {
    const data = { ...this.form.value };
    if (data.scope === 1) {
      data.organizationId = data.org.orgId;
    } else {
      data.orgGroup = data.org;
    }
    data.fromDate = moment(data.fromDate).format("DD/MM/YYYY")
    if (data.toDate) {
      data.toDate = moment(data.toDate).format("DD/MM/YYYY")
    }
    return data;
  }

  onChangeScope() {
    this.form.controls['org'].setValue(null);
  }

  doDelete(rowIndex: number) {
    this.detail.removeAt(rowIndex);
    if (this.detail.length === 0) {
      this.addRow();
    }
  }

  addRow() {
    this.detail.push(new FormControl(null, Validators.required));
  }

  ngOnDestroy(): void {
    this.subscriptions?.forEach(sub => sub.unsubscribe());
  }
}
