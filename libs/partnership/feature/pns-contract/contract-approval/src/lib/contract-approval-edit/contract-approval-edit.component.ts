import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { saveAs } from 'file-saver';
import {Constant, UrlConstant} from "@hcm-mfe/partnership/data-access/pns-contract/common";
import {MbDatePickerComponent} from "@hcm-mfe/shared/ui/mb-date-picker";
import {ContractApproverService, SearchFormService} from "@hcm-mfe/partnership/data-access/pns-contract/services";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {
  ContractApproveRequest,
  ContractApproveResponse,
  ContractEvaluationRequest,
  ContractType
} from "@hcm-mfe/partnership/data-access/pns-contract/models";
import {BaseResponse, SelectModal} from "@hcm-mfe/shared/data-access/models";
import {getTypeExport, Utils} from "@hcm-mfe/shared/common/utils";

@Component({
    selector: 'app-contract-approval-edit',
    templateUrl: './contract-approval-edit.component.html',
    styleUrls: ['./contract-approval-edit.component.scss']
})
export class ContractApprovalEditComponent implements OnInit, OnDestroy {
    form!: FormGroup;

    disciplineData = Constant.DISCIPLINE_DATA;
    contractTypeData = [];

    contractApproverId: number;
    employeeId?: number;

    userLoginLevel?: number;
    suggestContractFromDate?: string;
    isSubmitted = false;

    subs: Subscription[] = [];
    isDetail = false;
    fileList: NzUploadFile[] = [];

    @ViewChild('suggestContractToDate') suggestContractToDate!: MbDatePickerComponent;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private contractApproverService: ContractApproverService,
        private toastrService: ToastrService,
        private searchFormService: SearchFormService,
        private translateService: TranslateService,
    ) {
        this.contractApproverId = this?.activatedRoute?.snapshot?.queryParams['id'];
        if (this.router.url.includes(Constant.PAGE.CONTRACT_APPROVAL_DETAIL)) {
            this.isDetail = true;
        }
    }

    ngOnInit(): void {
        this.getListContractType();
        this.initForm();
        if (this.contractApproverId) {
            this.getModelEdit();
        }
    }


    initForm() {
        this.form = this.fb.group({
            curContract: [null],
            curContractFromDate: [null],
            curContractToDate: [null],
            contractByLaw: [null],
            contractByLawFromDate: [null],
            contractByLawToDate: [null],
            suggestContract: [null],
            suggestContractFromDate: [null],
            suggestContractToDate: [null],
            isDisciplined: ['0'],
            disciplineNote: [null],
            kpiPoint: [null],
            rankName: [null],
            note: [null]
        });
    }

    getListContractType() {
        const params = new HttpParams().set('classifyCode', Constant.CLASSIFY_CODE.HOP_DONG);
        this.subs.push(
            this.searchFormService.getContractType(UrlConstant.CATALOGS.CONTRACT_TYPE, params).subscribe(res => {
                if (res.code === HTTP_STATUS_CODE.OK) {
                    this.contractTypeData = res.data.map((item: ContractType) => {
                        item.label = item.name;
                        item.value = item.contractTypeId;
                        return item;
                    });
                }
            }, error => {
                this.toastrService.error(error.message);
            })
        );
    }

    getModelEdit() {
        this.subs.push(
            this.contractApproverService.getById(this.contractApproverId).subscribe({
                next: (res: BaseResponse) => {
                    if (res.code === HTTP_STATUS_CODE.OK) {
                        const data: ContractApproveResponse = res?.data;
                        this.setGlobalVariables(data);
                        this.processDisabledForm();
                        this.patchValueToForm(data);
                    } else {
                        this.toastrService.error(res?.message);
                    }
                },
                error: (err) => {
                    this.toastrService.error(err?.message);
                }
            })
        );
    }

    setGlobalVariables(data: ContractApproveResponse) {
        this.employeeId = data.contractProposalsResponse?.employeeId;
        this.userLoginLevel = data?.approverLevel;
        this.suggestContractFromDate = data?.fromDate;
    }

    processDisabledForm() {
        const arr = [Constant.USER_LOGIN_LEVEL.APPROVAL_LEVEL, Constant.USER_LOGIN_LEVEL.CHANGE_APPROVAL_LEVEL];
        if (this.userLoginLevel && arr.includes(this.userLoginLevel)) {
            this.form.controls['isDisciplined'].disable();
            this.form.controls['disciplineNote'].disable();
            this.form.controls['kpiPoint'].disable();
            this.form.controls['rankName'].disable();
        }
    }

    patchValueToForm(data: ContractApproveResponse) {
        this.form.patchValue({
            curContract: data?.contractProposalsResponse?.curContractTypeId,
            curContractFromDate: Utils.convertDateToFillForm(data?.contractProposalsResponse?.curFromDate),
            curContractToDate: Utils.convertDateToFillForm(data?.contractProposalsResponse?.curToDate),
            contractByLaw: data?.contractProposalsResponse?.contractByLawId,
            contractByLawFromDate: Utils.convertDateToFillForm(data?.contractProposalsResponse?.fromDate),
            contractByLawToDate: Utils.convertDateToFillForm(data?.contractProposalsResponse?.toDateByLaw),
            suggestContract: data?.contractTypeId,
            suggestContractFromDate: Utils.convertDateToFillForm(data?.fromDate),
            suggestContractToDate: Utils.convertDateToFillForm(data?.toDate),
            isDisciplined: data?.contractEvaluationsResponse?.isDisciplined?.toString(),
            disciplineNote: data?.contractEvaluationsResponse?.disciplinedNote,
            kpiPoint: data?.contractEvaluationsResponse?.kpiPoint,
            rankName: data?.contractEvaluationsResponse?.rankName,
            note: data?.contractEvaluationsResponse?.note
        });
        if (data.contractEvaluationsResponse?.attachFileList && data.contractEvaluationsResponse?.attachFileList.length > 0) {
            data.contractEvaluationsResponse?.attachFileList.forEach((item) => {
                this.fileList.push({
                    uid: item.docId?.toString()?? '',
                    name: item.fileName ?? '',
                    url: item.security,
                    status: 'done'
                });
            });
            this.fileList = [...this.fileList];
        }
    }

    onSave() {
        this.isSubmitted = true;

        Utils.processTrimFormControlsBeforeSend(this.form, 'disciplineNote', 'rankName');
        if (this.form.valid) {
            const request: ContractApproveRequest = {};
            this.setDataSendToServer(request);
            const formData = new FormData();
            formData.append('data', new Blob([JSON.stringify(request)], {
                type: 'application/json'
            }));

            this.subs.push(
              this.contractApproverService.saveRecord(formData).subscribe((res: BaseResponse) => {
                if (res.code === HTTP_STATUS_CODE.OK) {
                  this.toastrService.success(this.translateService.instant('partnership.pns-contract.notification.saveSuccess'));
                  this.router.navigateByUrl(Constant.PAGE.CONTRACT_APPROVAL_SEARCH).then();
                } else {
                  this.toastrService.error(res?.message);
                }
              }, error => {
                this.toastrService.error(error?.message);
              })
            );
        }
    }

    setDataSendToServer(request: ContractApproveRequest) {
        request.contractApproverId = this.contractApproverId;
        request.contractTypeId = this.form.controls['suggestContract'].value;

        const contractEvaluationDTO: ContractEvaluationRequest = {};
        contractEvaluationDTO.isDisciplined = this.form.controls['isDisciplined'].value;
        contractEvaluationDTO.disciplinedNote = this.form.controls['disciplineNote'].value;
        contractEvaluationDTO.kpiPoint = this.form.controls['kpiPoint'].value;
        contractEvaluationDTO.rankName = this.form.controls['rankName'].value;

        request.contractEvaluationsDTO = contractEvaluationDTO;
    }

    onBack() {
        this.router.navigateByUrl(Constant.PAGE.CONTRACT_APPROVAL_SEARCH);
    }

    onSuggestContractChange(event: SelectModal) {
        const suggestContract: ContractType = event?.itemSelected;
        const duration = suggestContract.duration;

        let newSuggestContractToDate;
        if (!!duration && !!this.suggestContractFromDate) {
            newSuggestContractToDate = moment(this?.suggestContractFromDate, 'DD/MM/YYYY').add(duration, 'M').toDate();
            this.form.controls['suggestContractToDate'].setValue(newSuggestContractToDate);
        } else {
            this.form.controls['suggestContractToDate'].setValue(null);
            this.suggestContractToDate.mbPlaceholderText = this.translateService.instant(Constant.LIMITLESS);
        }
    }

    downloadFile = (file: NzUploadFile) => {
        this.subs.push(
            this.contractApproverService.doDownloadAttachFile(Number(file.uid), file.url ? file.url : "").subscribe({
                next: (res) => {
                    const reportFile = new Blob([res.body], {type: getTypeExport(file.name.split(".").pop())});
                    saveAs(reportFile, file.name);
                }, error : () => {
                    this.toastrService.error(this.translateService.instant("common.notification.downloadFilePermissionDeny"));
                }
            })
        );
    }

    ngOnDestroy(): void {
        this.subs?.forEach(sub => sub?.unsubscribe());
    }
}
