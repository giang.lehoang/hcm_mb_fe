import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { HttpParams } from '@angular/common/http';
import {AppFunction, BaseResponse, Pagination, TableConfig} from "@hcm-mfe/shared/data-access/models";
import {
  ContractApproveRequest,
  ContractApproveResponse,
  RejectRequest} from '@hcm-mfe/partnership/data-access/pns-contract/models';
import {Constant, ConstantColor, FunctionCode, UrlConstant} from '@hcm-mfe/partnership/data-access/pns-contract/common';
import {
  MbTableMergeCellComponent
} from "../../../../../../../shared/ui/mb-table-merge-cell/src/lib/mb-table-merge-cell/mb-table-merge-cell.component";
import {SearchFormApprovalComponent} from "@hcm-mfe/partnership/ui/pns-contract/search-form-approval";
import {ContractApproverService, SearchFormService} from "@hcm-mfe/partnership/data-access/pns-contract/services";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {Utils} from "@hcm-mfe/shared/common/utils";
import {SessionService} from "@hcm-mfe/shared/common/store";

@Component({
  selector: 'app-contract-approval',
  templateUrl: './contract-approval.component.html',
  styleUrls: ['./contract-approval.component.scss']
})
export class ContractApprovalComponent implements OnInit {

  tableConfig!: TableConfig;
  dataTable!: ContractApproveResponse[];

  contractApproverSet = new Set<number>();

  reasonForm!: FormGroup;
  isSubmittedFormReason = false;
  isShowLiquidateModal = false;
  curContractApproverLiquidateId = 0;

  curAmountValidApprovalRecord = 0;

  isLoadingPage = false;

  disabledColor:string = ConstantColor.COLOR.DISABLED;

  pagination = new Pagination();
  subs: Subscription[] = [];
  constant = Constant;
  functionCode = FunctionCode.PNS_CONTRACT_APPROVE;
  objFunction: AppFunction;

  @ViewChild('searchForm') searchForm!: SearchFormApprovalComponent;
  @ViewChild('table') table!: MbTableMergeCellComponent;
  @ViewChild('actionTmpl', { static: true }) action!: TemplateRef<NzSafeAny>;
  @ViewChild('selectTmpl', {static: true}) select!: TemplateRef<NzSafeAny>;
  @ViewChild('selectThTmpl', {static: true}) selectTh!: TemplateRef<NzSafeAny>;
  @ViewChild('statusTmpl', { static: true }) status!: TemplateRef<NzSafeAny>;
  @ViewChild('employeeCodeTmpl', { static: true }) employeeCode!: TemplateRef<NzSafeAny>;
  @ViewChild('employeeNameTmpl', { static: true }) employeeName!: TemplateRef<NzSafeAny>;

  constructor(
    private fb: FormBuilder,
    private searchFormService: SearchFormService,
    private toastService: ToastrService,
    private translateService: TranslateService,
    private router: Router,
    private contractApproverService: ContractApproverService,
    private sessionService: SessionService,
  ) {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${this.functionCode}`);
  }

  ngOnInit(): void {
    this.initTable();
  }

  ngAfterViewInit() {
    this.doSearch(1);
  }

  doSearch(pageIndex: number) {
    this.pagination.pageNumber = pageIndex;
    if (this.searchForm?.form.valid) {
      this.tableConfig = { ...this.tableConfig, loading: true, };
      this.isLoadingPage = true;
      const params = this.searchForm.parseOptions();
      this.subs.push(
        this.searchFormService.getFilterResearch(UrlConstant.SEARCH_FORM.CONTRACT_APPROVERS, params, this.pagination.getCurrentPage()).subscribe({
          next: (res: BaseResponse) => {
            if (res.code === HTTP_STATUS_CODE.OK) {
              this.dataTable = this.processResponse(res?.data.listData);
              this.tableConfig.total = res.data.count;
              this.getAmountValidApprovalRecord(params);
            } else {
              this.toastService.error(res?.message);
            }
            this.tableConfig = { ...this.tableConfig, loading: false, };
            this.isLoadingPage = false;
          },
          error: (err) => {
            this.toastService.error(err?.message);
            this.tableConfig = { ...this.tableConfig, loading: false, };
            this.isLoadingPage = false;
          }
        })
      );
    }
  }

  getAmountValidApprovalRecord(params: HttpParams) {
    this.subs.push(
      this.contractApproverService.countValidRecord(UrlConstant.COUNT_RECORD.CONTRACT_APPROVERS, params).subscribe({
        next: (res: BaseResponse) => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.curAmountValidApprovalRecord = res?.data;
          } else {
            this.toastService.error(res?.message);
          }
        },
        error: (err) => {
          this.toastService.error(err?.message);
        }
      })
    );
  }

  processResponse(responseData: ContractApproveResponse[]): ContractApproveResponse[]{
    return responseData.map(item => {
      const toDate = item.toDate ? item.toDate : this.translateService.instant('partnership.pns-contract.label.unknown');
      item.contractTerm = item.fromDate + " - " + toDate;

      item.id = item.contractApproverId;  // set id field for each row in table: required
      return item;
    });
  }

  initTable() {
    this.tableConfig = {
      headers: [
        {
          title: 'STT',
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          rowspan: 2,
          fixed: true,
          fixedDir: 'left',
          width: 50
        },
        {
          title: 'partnership.pns-contract.label.employeeCode',
          field: 'employeeCode', width: 120,
          rowspan: 2,
          fixed: true,
          fixedDir: 'left'
        },
        {
          title: 'partnership.pns-contract.label.employeeName',
          field: 'empName', width: 150,
          rowspan: 2,
          fixed: true,
          fixedDir: 'left'
        },
        {
          title: 'partnership.pns-contract.label.status',
          field: 'status',
          tdTemplate: this.status,
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          width: 200,
          rowspan: 2
        },
        {
          title: 'partnership.pns-contract.label.curContractTypeName',
          rowspan: 2,
          field: 'curContractTypeName',
          thClassList: ['text-center'],
          width: 150
        },
        {
          title: 'partnership.pns-contract.label.curContractNumber',
          rowspan: 2,
          field: 'curContractNumber',
          thClassList: ['text-center'],
          width: 150
        },
        {
          title: 'partnership.pns-contract.label.contractByLawName',
          rowspan: 2,
          field: 'contractByLawName',
          thClassList: ['text-center'],
          width: 150
        },
        {
          title: 'partnership.pns-contract.label.contractTerm',
          rowspan: 2,
          field: 'contractTerm',
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          width: 200
        },
        {
          title: 'partnership.pns-contract.label.criteria',
          colspan: 3,
          thClassList: ['text-center'],
          child: [
            {
              title: 'partnership.pns-contract.label.disciplinedName',
              width: 180,
              field: 'disciplinedName',
              thClassList: ['text-center']
            },
            {
              title: 'partnership.pns-contract.label.kpiPoint',
              width: 100,
              field: 'kpiPoint',
              thClassList: ['text-center']
            },
            {
              title: 'partnership.pns-contract.label.rankName',
              width: 100,
              field: 'rankName',
              thClassList: ['td-date'],
              tdClassList: ['td-date']
            }
          ]
        },
        {
          title: 'partnership.pns-contract.label.signerInfo',
          colspan: 3,
          thClassList: ['text-center'],
          child: [
            {
              title: 'partnership.pns-contract.label.signerName',
              width: 200,
              field: 'signerName',
              thClassList: ['text-center']
            },
            {
              title: 'partnership.pns-contract.label.signerPosition',
              width: 150,
              field: 'signerPosition',
              thClassList: ['text-center']
            },
            {
              title: 'partnership.pns-contract.label.delegacyNo',
              width: 150,
              thClassList: ['text-center'],
              field: 'delegacyNo'
            }
          ]
        },
        {
          title: "partnership.pns-contract.label.manager",
          colspan: 3,
          thClassList: ['text-center'],
          child: [
            {
              title: "partnership.pns-contract.label.managerName",
              width: 150,
              field: "directManagerName",
              thClassList: ['text-center'],
            },
            {
              title: "partnership.pns-contract.label.offer",
              width: 150,
              field: "dmLiquidationName",
              thClassList: ['text-center'],
            },
            {
              title: "partnership.pns-contract.label.contractTypeName",
              width: 100,
              field: "dmContractTypeName",
              thClassList: ['text-center']
            }
          ]
        },
        {
          title: "partnership.pns-contract.label.approveLevel",
          colspan: 3,
          thClassList: ['text-center'],
          child: [
            {
              title: "partnership.pns-contract.label.approveName",
              width: 150,
              field: "approvalLevelPersonName",
              thClassList: ['text-center'],
            },
            {
              title: "partnership.pns-contract.label.offer",
              width: 150,
              field: "alLiquidationName",
              thClassList: ['text-center'],
            },
            {
              title: "partnership.pns-contract.label.contractTypeName",
              width: 100,
              field: "alContractTypeName",
              thClassList: ['text-center']
            }
          ]
        },
        {
          title: "partnership.pns-contract.label.approveChangeLevel",
          colspan: 3,
          thClassList: ['text-center'],
          child: [
            {
              title: "partnership.pns-contract.label.approveName",
              width: 150,
              field: "changeApprovalLevelName",
              thClassList: ['text-center'],
            },
            {
              title: "partnership.pns-contract.label.offer",
              width: 150,
              field: "calLiquidationName",
              thClassList: ['text-center'],
            },
            {
              title: "partnership.pns-contract.label.contractTypeName",
              width: 100,
              field: "calContractTypeName",
              thClassList: ['text-center']
            }
          ]
        },
        {
          title: 'partnership.pns-contract.label.orgName',
          field: 'orgName',
          rowspan: 2,
          thClassList: ['text-center'],
          width: 300,
          show:false
        },
        {
          title: 'partnership.pns-contract.label.positionName',
          rowspan: 2,
          field: 'positionName',
          thClassList: ['text-center'],
          width: 150,
          show:false
        },
        {
          title: ' ',
          field: 'action',
          rowspan: 2,
          tdClassList: ['text-nowrap', 'text-center'], thClassList: ['text-nowrap', 'text-center'],
          width: 50,
          tdTemplate: this.action,
          fixed: window.innerWidth > 1024,
          fixedDir: 'right'
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      showSelect: true,
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  onEditClick(contractApproverId: number) {
    this.router.navigateByUrl(Constant.PAGE.CONTRACT_APPROVAL_EDIT + contractApproverId).then();
  }

  onDetailClick(contractApproverId: number) {
    this.router.navigateByUrl(Constant.PAGE.CONTRACT_APPROVAL_DETAIL + contractApproverId).then();
  }

  onEvaluateClick(contractApproverId: number) {
    this.router.navigateByUrl(Constant.PAGE.CONTRACT_APPROVAL_EVALUATE + contractApproverId).then();
  }

  onApproveClick(contractApproverId: number) {
    const listId = [contractApproverId];
    let params = new HttpParams();
    params = params.set('listId', listId.toString());

    this.isLoadingPage = true;
    this.subs.push(
      this.contractApproverService.keepSigningByList(params).subscribe({
        next: (res: BaseResponse) => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.doSearch(1);
            this.toastService.success(this.translateService.instant("partnership.pns-contract.notification.approveSuccess"));
          } else {
            this.toastService.error(res?.message);
          }
          this.isLoadingPage = false;
        },
        error: (err) => {
          this.toastService.error(err?.message);
          this.isLoadingPage = false;
        }
      })
    );
  }

  onApproveAllClick() {
    if (this.curAmountValidApprovalRecord === 0) {
      this.toastService.success(this.translateService.instant("partnership.pns-contract.notification.notHaveRecordValid"));
      return;
    }
    let contractApproverDTO: ContractApproveRequest = this.searchForm.form.value;
    contractApproverDTO.fromDate = Utils.convertDateToSendServer(contractApproverDTO?.fromDate) ?? undefined;
    contractApproverDTO.toDate = Utils.convertDateToSendServer(contractApproverDTO?.toDate) ?? undefined;

    this.isLoadingPage = true;
    this.subs.push(
      this.contractApproverService.keepSigningAll(contractApproverDTO).subscribe({
        next: (res: BaseResponse) => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.doSearch(1);
            this.toastService.success(this.translateService.instant("partnership.pns-contract.notification.approveSuccess"));
          } else {
            this.toastService.error(res?.message);
          }
          this.isLoadingPage = false;
        },
        error: (err) => {
          this.toastService.error(err?.message);
          this.isLoadingPage = false;
        }
      })
    );
  }

  initReasonForm() {
    this.reasonForm = this.fb.group({
      rejectReason: [null, Validators.required]
    });
  }

  onLiquidateClick(contractApproverId: number) {
    this.curContractApproverLiquidateId = contractApproverId;
    this.isShowLiquidateModal = true;
    this.initReasonForm();
  }

  onLiquidateListClick() {
    this.isShowLiquidateModal = true;
    this.initReasonForm();
  }

  onApproveListClick() {
    this.isLoadingPage = true;
    this.subs.push(
      this.contractApproverService.keepSigningByList(this.getListSelectParam()).subscribe({
        next: (res: BaseResponse) => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.doSearch(1);
            this.table.resetAllCheckBoxFn();
            this.toastService.success(this.translateService.instant("partnership.pns-contract.notification.approveSuccess"));
          } else {
            this.toastService.error(res?.message);
          }
          this.isLoadingPage = false;
        },
        error: (err) => {
          this.toastService.error(err?.message);
          this.isLoadingPage = false;
        }
      })
    );
  }

  getListSelectParam(): HttpParams {
    const listId = Array.from(this.contractApproverSet);
    let params = new HttpParams();
    params = params.set('listId', listId.toString());
    return params;
  }

  onLiquidateModalClose() {
    this.isShowLiquidateModal = false;
    this.isSubmittedFormReason = false;
    this.reasonForm.reset();
    this.curContractApproverLiquidateId = 0;
  }

  doLiquidate() {
    this.isSubmittedFormReason = true;

    if (this.reasonForm.valid) {
      let listId;
      if (this.curContractApproverLiquidateId) {
        listId = [this.curContractApproverLiquidateId];
      } else {
        listId = Array.from(this.contractApproverSet);
      }
      let rejectRequest: RejectRequest = {};
      rejectRequest.listId = listId;
      rejectRequest.rejectReason = this.reasonForm.controls['rejectReason'].value;

      this.subs.push(
        this.contractApproverService.liquidationByList(rejectRequest).subscribe({
          next: (res: BaseResponse) => {
            if (res.code === HTTP_STATUS_CODE.OK) {
              this.doSearch(1);
              this.toastService.success(this.translateService.instant("partnership.pns-contract.notification.liquidateSuccess"));
              this.onLiquidateModalClose();
              this.table.resetAllCheckBoxFn();
            } else {
              this.toastService.error(res?.message);
            }
          },
          error: (err) => {
            this.toastService.error(err?.message);
          }
        })
      );
    }
  }

  onAmountSelectedRowChange(event: NzSafeAny) {
    this.contractApproverSet = event as Set<number>;
  }

  onDetail(data: ContractApproveResponse) {
    if (data?.contractApproverId) {
      this.onDetailClick(data.contractApproverId);
    }
  }

  ngOnDestroy(): void {
    this.subs?.forEach(sub => sub?.unsubscribe());
  }

}
