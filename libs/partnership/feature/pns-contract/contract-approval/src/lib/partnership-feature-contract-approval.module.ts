import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {TranslateModule} from "@ngx-translate/core";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {NzIconModule} from "ng-zorro-antd/icon";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {NzTagModule} from "ng-zorro-antd/tag";
import {PartnershipUiSearchFormModule} from "@hcm-mfe/partnership/ui/pns-contract/search-form";
import {RouterModule} from "@angular/router";
import {SharedUiMbTableMergeCellWrapModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell-wrap";
import {SharedUiMbTableMergeCellModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell";
import {PartnershipUiImportCommonModule} from "@hcm-mfe/partnership/ui/pns-contract/import-common";
import {ContractApprovalComponent} from "./contract-approval/contract-approval.component";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {ReactiveFormsModule} from "@angular/forms";
import {PartnershipUiSearchFormApprovalModule} from "@hcm-mfe/partnership/ui/pns-contract/search-form-approval";
import {ContractApprovalEditComponent} from "./contract-approval-edit/contract-approval-edit.component";
import {ContractEvaluateComponent} from "./contract-evaluate/contract-evaluate.component";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import { SharedUiMbDatePickerModule } from '@hcm-mfe/shared/ui/mb-date-picker';
import { SharedUiMbEmployeeDataPickerModule } from '@hcm-mfe/shared/ui/mb-employee-data-picker';
import {PartnershipUiPersonalInfoModule} from "@hcm-mfe/partnership/ui/pns-contract/personal-info";
import { SharedUiMbTextLabelModule } from '@hcm-mfe/shared/ui/mb-text-label';
import {SharedUiMbUploadModule} from "@hcm-mfe/shared/ui/mb-upload";
import {NzFormModule} from "ng-zorro-antd/form";
import {NzDividerModule} from "ng-zorro-antd/divider";
import {SharedDirectivesNumberInputModule} from "@hcm-mfe/shared/directives/number-input";
import {NzPopconfirmModule} from "ng-zorro-antd/popconfirm";
import {NzModalModule} from "ng-zorro-antd/modal";
import {PartnershipDataAccessPnsContractPipesModule} from "@hcm-mfe/partnership/data-access/pns-contract/pipes";
import {
  PartnershipDataAccessPnsContractDirectivesModule
} from "@hcm-mfe/partnership/data-access/pns-contract/directives";

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, SharedUiMbButtonModule,
    TranslateModule, NzButtonModule, NzDropDownModule, NzIconModule, SharedUiMbTableWrapModule, SharedUiMbTableModule, NzTagModule, PartnershipUiSearchFormModule,
    RouterModule.forChild([
      {
        path: '', redirectTo: 'search'
      },
      {
        path: 'search',
        component: ContractApprovalComponent
      },
      {
        path: 'edit',
        data: {
          pageName: 'partnership.pns-contract.pageName.contractApprovalEdit',
          breadcrumb: 'partnership.pns-contract.breadcrumb.contractApprovalEdit'
        },
        component: ContractApprovalEditComponent
      },
      {
        path: 'evaluate',
        data: {
          pageName: 'partnership.pns-contract.pageName.contractEvaluate',
          breadcrumb: 'partnership.pns-contract.breadcrumb.contractEvaluate'
        },
        component: ContractEvaluateComponent
      },
      {
        path: 'detail',
        data: {
          pageName: 'partnership.pns-contract.pageName.contractApprovalDetail',
          breadcrumb: 'partnership.pns-contract.breadcrumb.contractApprovalDetail'
        },
        component: ContractApprovalEditComponent
      },
    ]), SharedUiMbTableMergeCellWrapModule, SharedUiMbTableMergeCellModule,
    PartnershipUiImportCommonModule, ReactiveFormsModule, PartnershipUiSearchFormApprovalModule,
    NzButtonModule, SharedUiMbInputTextModule, SharedUiMbSelectModule, SharedUiMbDatePickerModule,
    SharedUiMbEmployeeDataPickerModule, PartnershipUiPersonalInfoModule,
    SharedUiMbTextLabelModule, SharedUiMbUploadModule, NzFormModule, NzDividerModule,
    SharedDirectivesNumberInputModule, NzPopconfirmModule, NzModalModule,
    PartnershipDataAccessPnsContractPipesModule, PartnershipDataAccessPnsContractDirectivesModule],
  declarations: [ContractApprovalComponent, ContractApprovalEditComponent, ContractEvaluateComponent],
  exports: [ContractApprovalComponent]
})
export class PartnershipFeatureContractApprovalModule {}
