import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import {NzUploadFile} from 'ng-zorro-antd/upload';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {Constant} from "@hcm-mfe/partnership/data-access/pns-contract/common";
import {HTTP_STATUS_CODE, MICRO_SERVICE} from '@hcm-mfe/shared/common/constants';
import {MbDatePickerComponent} from "@hcm-mfe/shared/ui/mb-date-picker";
import {ContractApproverService} from "@hcm-mfe/partnership/data-access/pns-contract/services";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";
import {ContractApproveResponse, ContractEvaluationRequest} from "@hcm-mfe/partnership/data-access/pns-contract/models";
import {Utils} from "@hcm-mfe/shared/common/utils";
import {ModelUpload} from "@hcm-mfe/shared/ui/mb-upload";
import {NzSafeAny} from "ng-zorro-antd/core/types";

@Component({
  selector: 'app-contract-evaluate',
  templateUrl: './contract-evaluate.component.html',
  styleUrls: ['./contract-evaluate.component.scss']
})
export class ContractEvaluateComponent implements OnInit {

    form!: FormGroup;
    disciplineData = Constant.DISCIPLINE_DATA;
    contractApproverId: number;
    employeeId?: number;
    isSubmitted = false;
    subs: Subscription[] = [];
    fileList: NzUploadFile[] = [];
    docIdsDelete: number[] = [];
    contractEvaluationId?: number;
    contractProposalId?: number;
    serviceName = MICRO_SERVICE.CONTRACT;

    @ViewChild('suggestContractToDate') suggestContractToDate?: MbDatePickerComponent;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private contractApproverService: ContractApproverService,
        private toastrService: ToastrService,
        private translateService: TranslateService,
    ) {
        this.contractApproverId = this?.activatedRoute?.snapshot?.queryParams['id'];
    }

    ngOnInit(): void {
        this.initForm();
        if (this.contractApproverId) {
            this.getModelEdit();
        }
    }

    initForm() {
        this.form = this.fb.group({
            isDisciplined: ['0'],
            disciplineNote: [null],
            kpiPoint: [null],
            rankName: [null],
            note: [null]
        });
    }

    getModelEdit() {
        this.subs.push(
            this.contractApproverService.getById(this.contractApproverId).subscribe({
                next: (res: BaseResponse) => {
                    if (res.code === HTTP_STATUS_CODE.OK) {
                        const data: ContractApproveResponse = res?.data;
                        this.setGlobalVariables(data);
                        this.patchValueToForm(data);
                    } else {
                        this.toastrService.error(res?.message);
                    }
                },
                error: (err) => {
                    this.toastrService.error(err?.message);
                }
            })
        );
    }

    setGlobalVariables(data: ContractApproveResponse) {
        this.employeeId = data.contractProposalsResponse?.employeeId;
    }

    patchValueToForm(data: ContractApproveResponse) {
        this.form.patchValue({
            isDisciplined: data?.contractEvaluationsResponse?.isDisciplined?.toString(),
            disciplineNote: data?.contractEvaluationsResponse?.disciplinedNote,
            kpiPoint: data?.contractEvaluationsResponse?.kpiPoint,
            rankName: data?.contractEvaluationsResponse?.rankName,
            note: data?.contractEvaluationsResponse?.note ? data?.contractEvaluationsResponse?.note : null
        });
        if (data.contractEvaluationsResponse?.attachFileList && data.contractEvaluationsResponse?.attachFileList.length > 0) {
            data.contractEvaluationsResponse?.attachFileList.forEach((item) => {
                this.fileList.push({
                    uid: item.docId?.toString() ?? "",
                    name: item.fileName ?? "",
                    url: item.security,
                    status: 'done'
                });
            });
            this.fileList = [...this.fileList];
        }
        this.contractEvaluationId = data.contractEvaluationsResponse?.contractEvaluationId;
        this.contractProposalId = data.contractProposalId;
    }

    onSave() {
        this.isSubmitted = true;

        Utils.processTrimFormControlsBeforeSend(this.form, 'disciplineNote', 'rankName');
        if (this.form.valid) {
            const request: ContractEvaluationRequest = this.setDataSendToServer();

            const formData = new FormData();
            formData.append('data', new Blob([JSON.stringify(request)], {
                type: 'application/json'
            }));

            this.fileList.forEach((nzFile: NzUploadFile) => {
                formData.append('files', nzFile as NzSafeAny);
            });

            this.subs.push(
              this.contractApproverService.saveEvaluation(formData).subscribe((res: BaseResponse) => {
                if (res.code === HTTP_STATUS_CODE.OK) {
                  this.toastrService.success(this.translateService.instant('partnership.pns-contract.notification.saveSuccess'));
                  this.router.navigateByUrl(Constant.PAGE.CONTRACT_APPROVAL_SEARCH).then();
                } else {
                  this.toastrService.error(res?.message);
                }
              }, error => {
                this.toastrService.error(error?.message);
              })
            );
        }
    }

    setDataSendToServer(): ContractEvaluationRequest {
        const contractEvaluationDTO: ContractEvaluationRequest = {};
        contractEvaluationDTO.isDisciplined = this.form.controls['isDisciplined'].value;
        contractEvaluationDTO.disciplinedNote = this.form.controls['disciplineNote'].value;
        contractEvaluationDTO.kpiPoint = this.form.controls['kpiPoint'].value;
        contractEvaluationDTO.rankName = this.form.controls['rankName'].value;
        contractEvaluationDTO.note = this.form.controls['note'].value;
        contractEvaluationDTO.docIdsDelete = this.docIdsDelete;
        contractEvaluationDTO.contractEvaluationId = this.contractEvaluationId;
        contractEvaluationDTO.contractProposalId = this.contractProposalId;
        return contractEvaluationDTO;
    }

    onBack() {
        this.router.navigateByUrl(Constant.PAGE.CONTRACT_APPROVAL_SEARCH).then();
    }


    removeFile(event: number[]) {
        if (event && event.length > 0) {
            this.docIdsDelete = event.filter((docId: number) => !isNaN(docId));
        }
    }

    onFileListChange(event: NzUploadFile[]) {
        this.fileList = event;
    }

    selectDisciplineData(event: string) {
        if (event && event === Constant.DISCIPLINE_DATA[0].value) {
          this.form.controls['disciplineNote'].setValidators(Validators.required);
        } else {
          this.form.controls['disciplineNote'].setValidators(null);
        }
    }

    ngOnDestroy(): void {
        this.subs?.forEach(sub => sub?.unsubscribe());
    }

}
