import { Component, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import {ContractParamService, ContractService} from '@hcm-mfe/partnership/data-access/pns-contract/services';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import {Scopes} from "@hcm-mfe/shared/common/enums";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";


@Component({
  selector: 'pns-template-params-form',
  templateUrl: './template-params.component.html',
})
export class TemplateParamsComponent implements OnInit {

  @Input() scope: string = Scopes.VIEW;
  @Input() functionCode = '';
  @ViewChild('nzUpload', { static: true }) nzUpload!: TemplateRef<NzSafeAny>;
  formArray!: FormArray;
  data = [1];
  constructor(
    private fb: FormBuilder,
    public toastr: ToastrService,
    public translate: TranslateService,
    private contractService: ContractService,
    private contractParamService: ContractParamService,
  ) {
  }
  ngOnInit(): void {
    this.formArray = new FormArray([])
    this.contractParamService.getPnsTemplateParams()
      .subscribe(res => {
          if (res.data.length > 0){
            res.data.forEach((e: NzSafeAny) => {
              const value = {
                templateParamId: e.templateParamId,
                name: e.name,
                code: e.code,
                defaultValue: e.defaultValue,
                state: 'view',
              };
              const form = this.buildForm();
              form.setValue(value)
              this.formArray.push(form)
            })
          }else {
            this.addRow();
          }
      })
  }

  addRow() {
    this.formArray.push(this.buildForm())
  }

  buildForm() {
    return this.fb.group({
      templateParamId: [null],
      name: [null, [Validators.required, Validators.maxLength(255)]],
      code: [null, [Validators.required]],
      defaultValue: [null],
      state: ['edit']
    })
  }

  save(form: FormGroup | NzSafeAny) {
    if (form.valid){
      this.contractParamService.createOrUpdatePnsTemplateParams(form.value)
      .subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          form.controls['templateParamId'].setValue(res.data);
          this.toastr.success(this.translate.instant('common.notification.success'));
        }else{
          if (form.value.templateParamId){
            this.contractParamService.getPnsTemplateParamsById(form.value.templateParamId)
              .subscribe( e => {
                const value = {
                  templateParamId: e.data.templateParamId,
                  name: e.data.name,
                  code: e.data.code,
                  defaultValue: e.data.defaultValue,
                  state: 'edit',
                };
                form.setValue(value);
              })

          }
          this.toastr.error(this.translate.instant( 'common.notification.error') + ": " + res?.message);
        }
      })
    }else {
      this.toastr.error(this.translate.instant( 'common.notification.error') + ": " + this.translate.instant( 'partnership.pns-contract.notification.requiredCode'));
    }

  }

  setState(form: FormGroup, state:string){
    if (state === 'view'){
      if (form.value.templateParamId){
        this.contractParamService.getPnsTemplateParamsById(form.value.templateParamId)
          .subscribe( e => {
            const value = {
              templateParamId: e.data.templateParamId,
              name: e.data.name,
              code: e.data.code,
              defaultValue: e.data.defaultValue,
              state: 'edit',
            };
            form.setValue(value);
          })

      }
    }
    form.controls['state'].setValue(state)
  }
  undo(form: FormGroup | NzSafeAny){
    if (form.value.templateParamId){
      this.contractParamService.getPnsTemplateParamsById(form.value.templateParamId)
        .subscribe( e => {
          const value = {
            templateParamId: e.data.templateParamId,
            name: e.data.name,
            code: e.data.code,
            defaultValue: e.data.defaultValue,
            state: 'view',
          };
          form.setValue(value);
        })

    }
    form.controls['state'].setValue('view')
  }

  edit(form: FormGroup | NzSafeAny){
    form.controls['state'].setValue('edit')
  }

  doDelete(form: FormGroup | NzSafeAny, rowIndex:number){
    if (form.value.templateParamId){
      this.contractParamService.deletePnsTemplateParams(form.value.templateParamId)
        .subscribe(res => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.toastr.success(this.translate.instant('common.notification.success'));
          }else{
            this.toastr.error(this.translate.instant( 'common.notification.error') + ": " + res?.message);
          }

        })
    }
    this.formArray.removeAt(rowIndex);
    if (this.formArray.length === 0){
      this.addRow();
    }
  }

}
