import {Component, Injector, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {NzSafeAny} from 'ng-zorro-antd/core/types';
import {NzModalService} from 'ng-zorro-antd/modal';
import {TranslateService} from '@ngx-translate/core';
import {Router} from '@angular/router';
import {saveAs} from 'file-saver';
import {ContractType} from '@hcm-mfe/partnership/data-access/pns-contract/models';
import {getTypeExport, Utils} from "@hcm-mfe/shared/common/utils";
import {AppFunction, CatalogModel} from '@hcm-mfe/shared/data-access/models';
import {
  CatalogService,
  ConfigApprovalService,
  ContractService,
  SearchFormService
} from "@hcm-mfe/partnership/data-access/pns-contract/services";
import {HTTP_STATUS_CODE, STATUS} from "@hcm-mfe/shared/common/constants";
import {ToastrService} from "ngx-toastr";
import {
  Constant,
  FunctionCode,
  SearchBaseComponent,
  UrlConstant
} from "@hcm-mfe/partnership/data-access/pns-contract/common";
import {HttpParams} from "@angular/common/http";
import { SaveContractTemplateComponent } from '../save-contract-template/save-contract-template.component';
import {Scopes} from "@hcm-mfe/shared/common/enums";
import {SessionService} from "@hcm-mfe/shared/common/store";

@Component({
    selector: 'contract-form-configuration',
    templateUrl: './contract-template.component.html',
    styleUrls: ['./contract-template.component.scss']
})
export class ContractTemplateComponent extends SearchBaseComponent implements OnInit {

    contractTypeSelects: ContractType[] = [];
    formSearchConfig = {
        org: [null],
        orgGroup: [null],
        name: [null],
        contractTypeId: [null],
        fromDate: [Utils.getFirstDayOrLastDayOfCurrentMonth(true)],
        toDate: [Utils.getFirstDayOrLastDayOfCurrentMonth(false)],
        positionGroupId: [null],
        listEmpTypeCode: [null],
    };

    orgGroupSelects: CatalogModel[] = [];

    positionGroupList: CatalogModel[] = [];
    empTypeCodeList: CatalogModel[] = [];
    heightTable = {x : '1700px'};
    functionCode = FunctionCode.PNS_CONFIG_TEMPLATE;
    scope = Scopes.VIEW;
    objFunction!: AppFunction;


    @ViewChild('actionTmpl', { static: true }) action!: TemplateRef<NzSafeAny>;
    @ViewChild('scopeTmpl', { static: true }) scopeTmpl!: TemplateRef<NzSafeAny>;
    @ViewChild('fileName', { static: true }) fileName!: TemplateRef<NzSafeAny>;
    @ViewChild('positionGroup', { static: true }) positionGroup!: TemplateRef<NzSafeAny>;

    constructor(
        injector: Injector,
        private modalServices: NzModalService,
        private translateService: TranslateService,
        private catalogService: CatalogService,
        private contractService: ContractService,
        private router: Router,
        private configApprovalService: ConfigApprovalService,
        private searchFormService: SearchFormService,
        public override toastrService: ToastrService,
        private sessionService: SessionService
    ) {
        super(injector);
        this.buildFormSearch(this.formSearchConfig);
        this.setMainService(contractService);
        this.objFunction = this.sessionService.getSessionData(`FUNCTION_${this.functionCode}`);
    }

    ngOnInit(): void {
        this.initDataSelect();
        this.initTable();
        this.getContractType();
        this.getPositionGroupList();
        this.getListEmpTypeCode();
        this.doSearch();
    }

    getContractType() {
        this.subs.push(
            this.contractService.getContractType().subscribe(res => {
                if (res.code === HTTP_STATUS_CODE.OK) {
                    this.contractTypeSelects = res.data;
                }
            }, error => this.toastrService.error(error.message))
        );
    }

    getListEmpTypeCode() {
        const params = new HttpParams().set('typeCode', Constant.TYPE_CODE.DOI_TUONG_CV);
        this.subs.push(
            this.searchFormService.getListValue(UrlConstant.CATEGORY.LOOKUP_VALUE, params).subscribe(res => {
                if (res.code === HTTP_STATUS_CODE.OK) {
                    this.empTypeCodeList = res.data;
                }
            }, error => {
                this.toastrService.error(error.message);
            })
        );
    }

    initDataSelect() {
      this.orgGroupSelects = Constant.ORG_GROUP_SELECTS.map(item => {
        item.label = this.translateService.instant(item.label);
        return item;
      })
    }

    initTable(): void {
        this.tableConfig = {
            headers: [
                {
                    title: 'partnership.pns-contract.label.scope',
                    field: 'scope',
                    width: 100,
                    thClassList: ['text-center'],
                    tdTemplate: this.scopeTmpl,
                    fixedDir: 'left'
                },
                {
                    title: 'partnership.pns-contract.label.formName',
                    field: 'name',
                    width: 150,
                    thClassList: ['text-center'],
                    fixedDir: 'left'
                },
                {
                    title: 'partnership.pns-contract.label.typeOfcontract',
                    field: 'contractTypeName',
                    width: 150,
                    thClassList: ['text-center'],
                    fixedDir: 'left'
                },
                {
                    title: 'partnership.pns-contract.label.org',
                    field: 'orgName',
                    tdClassList: ['text-left'],
                    thClassList: ['text-center'],
                    width: 150
                },
                {
                    title: 'partnership.pns-contract.label.orgGroup',
                    field: 'orgGroup',
                    tdClassList: ['text-left'],
                    thClassList: ['text-center'],
                    width: 150
                },
                {
                    title: 'partnership.pns-contract.label.empTypeName',
                    field: 'empTypeName',
                    tdClassList: ['text-left'],
                    thClassList: ['text-center'],
                    width: 150
                },
                {
                    title: 'partnership.pns-contract.label.groupTitle',
                    field: 'positionGroup',
                    tdTemplate: this.positionGroup,
                    tdClassList: ['text-left'],
                    thClassList: ['text-center'],
                    width: 250
                },
                {
                    title: 'partnership.pns-contract.label.status',
                    field: 'state',
                    tdClassList: ['text-left'],
                    thClassList: ['text-center'],
                    width: 100
                },
                {
                    title: 'partnership.label.fromDate',
                    field: 'fromDate',
                    tdClassList: ['td-date'],
                    thClassList: ['td-date']
                },
                {
                    title: 'partnership.label.toDate',
                    field: 'toDate',
                    tdClassList: ['td-date'],
                    thClassList: ['td-date']
                },
                {
                    title: 'partnership.pns-contract.label.templateFile',
                    field: 'fileName',
                    tdTemplate: this.fileName,
                    tdClassList: ['text-left'],
                    thClassList: ['text-center'],
                    width: 150
                },
                {
                    title: '  ',
                    field: 'action',
                    tdClassList: ['text-nowrap', 'text-center'],
                    thClassList: ['text-nowrap', 'text-center'],
                    width: 60,
                    tdTemplate: this.action,
                    fixed: window.innerWidth > 1024,
                    fixedDir: 'right'
                }
            ],
            total: 0,
            needScroll: true,
            loading: false,
            size: 'small',
            pageSize: this.pagination.pageSize,
            pageIndex: 1
        };
    }

    showModalUpdate(contractTemplateId: number, footerTmpl: TemplateRef<NzSafeAny>) {
        this.modal = this.modalServices.create({
            nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
            nzTitle: contractTemplateId ? this.translateService.instant('partnership.leaveReason.update') : this.translateService.instant('partnership.leaveReason.add'),
            nzContent: SaveContractTemplateComponent,
            nzComponentParams: {
                contractTypeSelects: this.contractTypeSelects
            },
            nzFooter: footerTmpl
        });
        // this.modal.afterClose.subscribe(result => result?.refresh ? this.search(null): '')
    }


    create() {
        this.router.navigateByUrl(UrlConstant.NAVIGATE_URL.ADD_FORM_CONFIGURATION).then();
    }

    update(contractTemplateId: number) {
        this.router.navigateByUrl(UrlConstant.NAVIGATE_URL.EDIT_FORM_CONFIGURATION + contractTemplateId).then();
    }

    getPositionGroupList() {
        this.subs.push(
            this.configApprovalService.getPositionGroupList().subscribe(data => {
                if (data.code === HTTP_STATUS_CODE.OK) {
                    this.positionGroupList = data.data
                        .filter((item: NzSafeAny) => item.flagStatus === STATUS.ACTIVE)
                        .map((item: NzSafeAny) => {
                            return { value: '' + item.pgrId, label: item.pgrName };
                        });
                }
            }, (error) => {
                this.toastrService.error(error.message)
            })
        );
    }

    downloadFile(contractTemplateId: number, fileName: string) {
        this.subs.push(
            this.contractService.downloadFile(contractTemplateId)
                .subscribe(reportFile => {
                    saveAs(reportFile, fileName);
            }, error => this.toastrService.error(error.message))
        );
    }

    setFormValue(event: NzSafeAny, formName: string) {
        if (event.listOfSelected?.length > 0) {
            this.formSearch?.controls[formName].setValue(event.listOfSelected);
        } else {
            this.formSearch?.controls[formName].setValue(null);
        }
    }

    doExportData() {
        this.isLoadingPage = true;
        const params = this.formSearch ? {...this.formSearch.value} : {};
        this.subs.push(
            this.contractService.doExportData(params).subscribe(res => {
                if ((res?.headers?.get('Excel-File-Empty'))) {
                    this.toastrService.error(this.translateService.instant("common.notification.exportFail"));
                } else {
                    const fileName = 'Danh-sach-bieu-mau-hop-dong.xlsx';
                    const reportFile = new Blob([res.body], { type: getTypeExport(fileName.split('.')?.pop()) });
                    saveAs(reportFile, fileName);
                }
                this.isLoadingPage = false;
            }, error => {
                this.toastrService.error(error.message)
                this.isLoadingPage = false;
            })
        );
    }

    override ngOnDestroy(): void {
        this.subs?.forEach(sub => sub.unsubscribe());
    }

}
