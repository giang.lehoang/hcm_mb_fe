import { Component, Input, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import {Scopes} from "@hcm-mfe/shared/common/enums";
import {Constant, FunctionCode, UrlConstant} from '@hcm-mfe/partnership/data-access/pns-contract/common';
import {HTTP_STATUS_CODE, MICRO_SERVICE, STATUS} from '@hcm-mfe/shared/common/constants';
import { CatalogModel } from '@hcm-mfe/shared/data-access/models';
import { ContractService, ConfigApprovalService } from '@hcm-mfe/partnership/data-access/pns-contract/services';
import {MbPositionGroupService} from "@hcm-mfe/staff-abs/data-access/services";
import {DateValidator} from "@hcm-mfe/shared/common/validators";
import {CommonUtils} from "@hcm-mfe/shared/core";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {ContractType} from "@hcm-mfe/partnership/data-access/pns-contract/models";
import { TemplateParamsComponent } from '../template-params/template-params.component';

@Component({
    selector: 'app-leave-reason-form',
    templateUrl: './save-contract-template.component.html',
    styleUrls: ['./save-contract-template.component.scss']
})
export class SaveContractTemplateComponent implements OnInit {

    @Input() scope: string = Scopes.VIEW;
    @Input() functionCode = FunctionCode.PNS_CONFIG_TEMPLATE;
    form: FormGroup;
    subs: Subscription[] = [];

    positionGroupList: CatalogModel[] = [];
    contractTypeSelects: ContractType[] = [];
    isSubmitted = false;
    fileList: NzUploadFile[] = [];
    modal!: NzModalRef;

    UrlConstant = UrlConstant;
    MICRO_SERVICE = MICRO_SERVICE;

    orgGroupSelects: CatalogModel[] = [];
    scopeSelects: CatalogModel[] = [];
    isLoadingPage = false;

    constructor(
        private fb: FormBuilder,
        private route: ActivatedRoute,
        public toastr: ToastrService,
        public translate: TranslateService,
        private contractService: ContractService,
        private mbPositionGroupService: MbPositionGroupService,
        private modalServices: NzModalService,
        private configApprovalService: ConfigApprovalService
    ) {
        this.form = this.fb.group({
            contractTemplateId: [''],
            name: [null, [Validators.required, Validators.maxLength(255)]],
            contractTypeId: [null, [Validators.required]],
            org: [null, [Validators.required]],
            fromDate: [null, [Validators.required]],
            toDate: [null],
            position: [null],
            fileTemplate: [null, [Validators.required]],
            scope: [1]
        }, {
            validators: [
                DateValidator.validateRangeDate('fromDate', 'toDate', 'rangeDateError')
            ]
        });

        this.route.queryParams.subscribe(params => {
            if (params['contractTemplateId']) {
              this.form.controls['contractTemplateId'].setValue(params['contractTemplateId']);
                this.subs.push(
                    this.contractService.getPnsContractTemplates(params['contractTemplateId']).subscribe(res => {
                        if (res.code === HTTP_STATUS_CODE.OK) {
                            const value = { ...res.data };
                            value.fromDate = moment(value.fromDate, 'DD/MM/YYYY').toDate();
                            if (value.toDate) {
                                value.toDate = moment(value.toDate, 'DD/MM/YYYY').toDate();
                            }
                            if (value.orgGroup) {
                                value.org = value.orgGroup;
                              this.form.controls['scope'].setValue(2);
                            }

                            const file: NzUploadFile = {
                                uid: res.data.contractTemplateId,
                                name: res.data.fileName,
                                status: 'done'
                            };
                            value.fileTemplate = file;
                            this.fileList.push(file);
                            this.fileList = [...this.fileList];
                            CommonUtils.setFormValue(this.form, value);
                        }
                    }, error => this.toastr.error(error.message))
                );
            }
        });
    }

    ngOnInit(): void {
        this.initDataSelect();
        this.getContractType();
        this.getPositionGroupList();
    }

    initDataSelect() {
      this.orgGroupSelects = Constant.ORG_GROUP_SELECTS.map(item => {
        item.label = this.translate.instant(item.label);
        return item;
      })

      this.scopeSelects = Constant.SCOPE_SELECTS.map(item => {
        item.label = this.translate.instant(item.label);
        return item;
      })
    }

    getContractType() {
        this.subs.push(
            this.contractService.getContractType().subscribe(res => {
                if (res.code === HTTP_STATUS_CODE.OK) {
                    this.contractTypeSelects = res.data;
                }
            },error => this.toastr.error(error.message))
        );
    }

    setFormValue(event: NzSafeAny, formName: string) {
        if (event.listOfSelected.length > 0) {
            this.form.controls[formName].setValue(event.listOfSelected);
        }
    }

    onFileListChange(listFile: NzUploadFile[]) {
        if (listFile.length > 0) {
          this.form.controls['fileTemplate'].setValue(listFile[0]);
        } else {
          this.form.controls['fileTemplate'].setValue(null);
        }
    }

    saveOrUpdate() {
        this.isSubmitted = true;

        if (this.form.valid) {
            this.isLoadingPage = true;
            this.subs.push(
                this.contractService.savePnsContractTemplates(this.buildFormData()).subscribe(res => {
                    if (res.code === HTTP_STATUS_CODE.OK) {
                        this.toastr.success(this.translate.instant('common.notification.updateSuccess'));
                        this.back();
                    } else {
                        this.toastr.error(this.translate.instant('common.notification.error') + ': ' + res?.message);
                    }
                    this.isLoadingPage = false;
                }, error => {
                    this.toastr.error(this.translate.instant('common.notification.error') + ': ' + error?.message);
                    this.isLoadingPage = false;
                })
            );
        }
    }

    getPositionGroupList() {
        this.subs.push(
            this.configApprovalService.getPositionGroupList().subscribe(data => {
                if (data.code === HTTP_STATUS_CODE.OK) {
                    this.positionGroupList = data.data
                        .filter((item: NzSafeAny) => item.flagStatus === STATUS.ACTIVE)
                        .map((item: NzSafeAny) => {
                            return { value: '' + item.pgrId, label: item.pgrName };
                        });
                }
            }, error => this.toastr.error(error.message))
        );
    }

    buildFormData() {
        const formData = new FormData();
        const data = { ...this.form.value };
        if (data.scope === 1) {
            data.organizationId = data?.org?.orgId;
        } else {
            data.orgGroup = data.org;
        }
        data.fromDate = moment(data.fromDate).format('DD/MM/YYYY');
        if (data.toDate) {
            data.toDate = moment(data.toDate).format('DD/MM/YYYY');
        }
        delete data.org;
        delete data.fileTemplate;

      formData.append('data', new Blob([JSON.stringify(data)], {
            type: 'application/json'
        }));

        formData.append('file', this.form.value.fileTemplate);
        return formData;
    }

    back() {
        history.back();
    }

    showModalUpdate(footerTmpl: TemplateRef<NzSafeAny>) {
        this.modal = this.modalServices.create({
            nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
            nzTitle: this.translate.instant('partnership.pns-contract.label.templateParams'),
            nzContent: TemplateParamsComponent,
            nzComponentParams: {},
            nzFooter: footerTmpl
        });
        // this.modal.afterClose.subscribe(result => result?.refresh ? this.search(null): '')
    }

    onChangeScope() {
      this.form.controls['org'].setValue(null);
    }

    ngOnDestroy(): void {
        this.subs?.forEach(sub => sub.unsubscribe());
    }

}
