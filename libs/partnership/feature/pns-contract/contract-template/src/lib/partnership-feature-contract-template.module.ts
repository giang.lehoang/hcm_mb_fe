import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {TranslateModule} from "@ngx-translate/core";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {NzIconModule} from "ng-zorro-antd/icon";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {NzTagModule} from "ng-zorro-antd/tag";
import {PartnershipUiSearchFormModule} from "@hcm-mfe/partnership/ui/pns-contract/search-form";
import {RouterModule} from "@angular/router";
import {SharedUiMbTableMergeCellWrapModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell-wrap";
import {SharedUiMbTableMergeCellModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell";
import {PartnershipUiImportCommonModule} from "@hcm-mfe/partnership/ui/pns-contract/import-common";
import {ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiOrgDataPickerModule} from "@hcm-mfe/shared/ui/org-data-picker";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {ContractTemplateComponent} from "./contract-template/contract-template.component";
import {SharedUiMbSelectCheckAbleModule} from "@hcm-mfe/shared/ui/mb-select-check-able";
import {SharedUiMbEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/mb-employee-data-picker";
import {PartnershipUiPersonalInfoModule} from "@hcm-mfe/partnership/ui/pns-contract/personal-info";
import {SharedUiMbTextLabelModule} from "@hcm-mfe/shared/ui/mb-text-label";
import {SharedUiMbUploadModule} from "@hcm-mfe/shared/ui/mb-upload";
import {SaveContractTemplateComponent} from "./save-contract-template/save-contract-template.component";
import {TemplateParamsComponent} from "./template-params/template-params.component";
import {NzFormModule} from "ng-zorro-antd/form";
import {NzPopconfirmModule} from "ng-zorro-antd/popconfirm";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, SharedUiMbButtonModule,
    TranslateModule, NzButtonModule, NzDropDownModule, NzIconModule, SharedUiMbTableWrapModule, SharedUiMbTableModule, NzTagModule, PartnershipUiSearchFormModule,
    RouterModule.forChild([
      {
        path: '', redirectTo: 'search'
      },
      {
        path: 'search',
        component: ContractTemplateComponent
      },
      {
        path: 'update',
        data: {
          pageName: 'partnership.pns-contract.pageName.updateFormConfiguration',
          breadcrumb: 'partnership.pns-contract.breadcrumb.edit',
        },
        component: SaveContractTemplateComponent
      },
      {
        path: 'create',
        data: {
          pageName: 'partnership.pns-contract.pageName.createFormConfiguration',
          breadcrumb: 'partnership.pns-contract.breadcrumb.create'
        },
        component: SaveContractTemplateComponent
      }
    ]), SharedUiMbTableMergeCellWrapModule, SharedUiMbTableMergeCellModule, PartnershipUiImportCommonModule, ReactiveFormsModule, SharedUiMbSelectModule,
    SharedUiOrgDataPickerModule, SharedUiMbInputTextModule, SharedUiMbDatePickerModule, SharedUiMbSelectCheckAbleModule, SharedUiMbEmployeeDataPickerModule,
    PartnershipUiPersonalInfoModule, SharedUiMbTextLabelModule, SharedUiMbUploadModule, NzFormModule, NzPopconfirmModule, NzTableModule, NzToolTipModule],
  declarations: [ContractTemplateComponent, SaveContractTemplateComponent, TemplateParamsComponent],
  exports: [ContractTemplateComponent]
})
export class PartnershipFeatureContractTemplateModule {}
