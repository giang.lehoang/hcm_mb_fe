import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ContractFeesListComponent} from "./contract-fees-list/contract-fees-list.component";
import {RouterModule} from "@angular/router";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NzButtonModule} from "ng-zorro-antd/button";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {NzGridModule} from "ng-zorro-antd/grid";
import {SharedUiOrgDataPickerModule} from "@hcm-mfe/shared/ui/org-data-picker";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiMbSelectCheckAbleModule} from "@hcm-mfe/shared/ui/mb-select-check-able";
import {NzDatePickerModule} from "ng-zorro-antd/date-picker";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzFormModule} from "ng-zorro-antd/form";
import {NzTagModule} from "ng-zorro-antd/tag";
import {NzPopconfirmModule} from "ng-zorro-antd/popconfirm";
import {PartnershipUiImportFileModule} from "@hcm-mfe/partnership/ui/import-file";
import {SharedUiMbTableMergeCellWrapModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell-wrap";
import {SharedUiMbTableMergeCellModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell";
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";
import {NzModalModule} from "ng-zorro-antd/modal";
import {PopupService} from "@hcm-mfe/shared/ui/popup";
import {SharedPipesFormatCurrencyModule} from "@hcm-mfe/shared/pipes/format-currency";
import {ContractFeesFormComponent} from "./contract-fees-form/contract-fees-form.component";
import {SharedUiEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/employee-data-picker";
import {SharedDirectivesNumbericModule} from "@hcm-mfe/shared/directives/numberic";

@NgModule({
  imports: [CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: ContractFeesListComponent
      }
    ]), FormsModule, NzButtonModule, SharedUiLoadingModule, ReactiveFormsModule, NzGridModule, SharedUiOrgDataPickerModule,
    SharedUiMbInputTextModule, SharedUiMbSelectCheckAbleModule, NzDatePickerModule, SharedUiMbButtonModule,
    NzDropDownModule, TranslateModule, SharedUiMbDatePickerModule, NzIconModule, NzFormModule, NzTagModule, NzPopconfirmModule, PartnershipUiImportFileModule,
    SharedUiMbTableMergeCellWrapModule, SharedUiMbTableMergeCellModule, NzCheckboxModule, NzModalModule, SharedPipesFormatCurrencyModule, SharedUiEmployeeDataPickerModule, SharedDirectivesNumbericModule],
  declarations: [ContractFeesListComponent, ContractFeesFormComponent],
  exports: [ContractFeesListComponent, ContractFeesFormComponent],
  providers: [PopupService]
})
export class PartnershipFeaturePnsContractContractFeesModule {}
