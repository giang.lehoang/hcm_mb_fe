import {Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {Subscription} from "rxjs";
import {HTTP_STATUS_CODE, MICRO_SERVICE, Mode, Scopes} from '@hcm-mfe/shared/common/constants';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NzModalRef, NzModalService} from "ng-zorro-antd/modal";
import {
  ContractFeeDTO
} from '@hcm-mfe/partnership/data-access/pns-contract/models';
import {
  ContractFeesService,
  PersonalInfoService,
  SearchFormService
} from '@hcm-mfe/partnership/data-access/pns-contract/services';
import {AppFunction, BaseResponse} from "@hcm-mfe/shared/data-access/models";
import {ToastrService} from "ngx-toastr";
import {Utils} from "@hcm-mfe/shared/common/utils";
import {TranslateService} from "@ngx-translate/core";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {Constant, EMP_TYPE_CODES, FunctionCode} from '@hcm-mfe/partnership/data-access/pns-contract/common';
import {SessionService} from "@hcm-mfe/shared/common/store";
import { DateValidator } from '@hcm-mfe/shared/common/validators';

@Component({
  selector: 'contract-fees-form',
  templateUrl: './contract-fees-form.component.html',
  styleUrls: ['./contract-fees-form.component.scss']
})
export class ContractFeesFormComponent implements OnInit, OnDestroy {
  mode?: Mode;
  Mode = Mode;
  form!: FormGroup;
  constant = Constant;
  readonly serviceName: string = MICRO_SERVICE.CONTRACT;
  contractFeeId?: number;
  contractFeeEdit?: ContractFeeDTO;
  functionCode = FunctionCode.PNS_FEES;
  scope = Scopes.CREATE;
  empTypes: string[] = [EMP_TYPE_CODES.THUE_NGOAI,"03"];
  isSubmitted = false;
  isLoadingPage = false;
  subs: Subscription[] = [];

  @ViewChild('statusTmpl', {static: true}) status!: TemplateRef<NzSafeAny>;

  constructor(
    private fb: FormBuilder,
    private modalService: NzModalService,
    private toastService: ToastrService,
    private translateService: TranslateService,
    private contractFeesService: ContractFeesService,
    private personalInfoService: PersonalInfoService,
    private modalRef: NzModalRef,
    private searchFormService: SearchFormService,
    private sessionService: SessionService
  ) {
    this.initForm();
  }

  ngOnInit(): void {
    if (this.contractFeeId) {
      this.getModelEdit();
    }
  }

  getModelEdit() {
    if (this.contractFeeId) {
      this.subs.push(
        this.contractFeesService.getById(this.contractFeeId).subscribe({
          next: (res: BaseResponse) => {
            if (res.code === HTTP_STATUS_CODE.OK) {
              this.contractFeeEdit = res?.data;
              this.getEmployee();
              this.patchValueToForm();
            } else {
              this.toastService.error(res?.message);
            }
          },
          error: (err) => {
            this.toastService.error(err?.message);
          }
        })
      );
    }
  }

  getEmployee() {
    if (this.contractFeeEdit?.employeeId) {
      this.subs.push(
        this.personalInfoService.getPersonalInfo(this.contractFeeEdit.employeeId).subscribe({
          next: (res: BaseResponse) => {
            if (res.code === HTTP_STATUS_CODE.OK) {
              this.form.controls['employee'].setValue(res?.data);
            } else {
              this.toastService.error(res?.message);
            }
          },
          error: (err) => {
            this.toastService.error(err?.message);
          }
        })
      );
    }
  }

  patchValueToForm() {
    this.form.patchValue({
      fromDate: Utils.convertDateToFillForm(this.contractFeeEdit?.fromDate),
      amountFee: this.contractFeeEdit?.amountFee,
      rejectReason: this.contractFeeEdit?.rejectReason,
    });
  }

  initForm() {
    this.form = this.fb.group({
      employee: [null, Validators.required],
      fromDate: [null, Validators.required],
      toDate: [null],
      amountFee: [null, Validators.required],
      rejectReason: [null],
    },{
      validators: [
          DateValidator.validateRangeDate('fromDate', 'toDate', 'rangeDateError')
      ]
    });
  }

  onSave() {
    this.isSubmitted = true;
    Utils.processTrimFormControlsBeforeSend(this.form, 'rejectReason');
    if (this.form.valid) {
      this.isLoadingPage = true;
      const request: ContractFeeDTO = {};
      this.setDataSendToServer(request);
      this.subs.push(
        this.contractFeesService.saveRecord(request).subscribe({
          next: (res: BaseResponse) => {
            if (res.code === HTTP_STATUS_CODE.OK) {
              this.toastService.success(this.translateService.instant("partnership.pns-contract.notification.saveSuccess"));
            } else {
              this.toastService.error(res?.message);
            }
            this.modalRef.close({refresh: true});
            this.isLoadingPage = false;
          },
          error: (err) => {
            this.toastService.error(err?.message);
            this.isLoadingPage = false;
          }
        })
      )
    }
  }

  setDataSendToServer(request: ContractFeeDTO) {
    request.contractFeeId = this.contractFeeId;
    request.employeeId = this.form.controls['employee'].value?.employeeId;
    request.fromDate = Utils.convertDateToSendServer(this.form.controls['fromDate'].value);
    request.toDate = Utils.convertDateToSendServer(this.form.controls['toDate'].value);
    request.amountFee = this.form.controls['amountFee'].value;
    request.rejectReason = this.form.controls['rejectReason'].value;
    if (this.contractFeeEdit?.status) {
      request.status = this.contractFeeEdit?.status;
    }
  }

  closeModalAdd() {
    this.modalService.closeAll();
  }

  ngOnDestroy(): void {
    this.subs?.forEach(sub => sub?.unsubscribe());
  }
}
