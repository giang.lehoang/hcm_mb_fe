import {Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AppFunction, BaseResponse, Pagination, TableConfig} from '@hcm-mfe/shared/data-access/models';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {Subscription} from 'rxjs';
import {HttpParams} from '@angular/common/http';
import {HTTP_STATUS_CODE, leadType, Mode} from '@hcm-mfe/shared/common/constants';
import {getTypeExport, StringUtils} from '@hcm-mfe/shared/common/utils';
import {Constant, FunctionCode, UrlConstant} from '@hcm-mfe/partnership/data-access/pns-contract/common';
import {
  ContractFeesService,
  SearchFormService
} from '@hcm-mfe/partnership/data-access/pns-contract/services';
import * as moment from 'moment';
import {NzSafeAny} from 'ng-zorro-antd/core/types';
import {CatalogModel} from '@hcm-mfe/partnership/ui/pns-contract/search-form';
import {NzModalRef, NzModalService} from 'ng-zorro-antd/modal';
import {saveAs} from 'file-saver';
import {SessionService} from "@hcm-mfe/shared/common/store";
import {ContractFeeResponse} from "@hcm-mfe/partnership/data-access/pns-contract/models";
import {CustomValidators} from "@hcm-mfe/shared/common/validators";
import {PopupService} from "@hcm-mfe/shared/ui/popup";
import {ContractFeesFormComponent} from "../contract-fees-form/contract-fees-form.component";
import {
  MbTableMergeCellComponent
} from "../../../../../../../shared/ui/mb-table-merge-cell/src/lib/mb-table-merge-cell/mb-table-merge-cell.component";
import { Scopes } from '@hcm-mfe/shared/common/enums';

@Component({
  selector: 'hcm-mfe-contract-liquidation-list',
  templateUrl: './contract-fees-list.component.html',
  styleUrls: ['./contract-fees-list.component.scss']
})
export class ContractFeesListComponent implements OnInit, OnDestroy {

  form!: FormGroup;
  tableConfig!: TableConfig;

  contractFeeStatus: CatalogModel[] = [];
  dataTable: ContractFeeResponse[] = [];
  checkAll: boolean[] = [];

  modal: NzModalRef | undefined;
  constant = Constant;
  pagination = new Pagination();
  isLoadingPage = false;

  isImportData = false;
  urlApiImport = UrlConstant.CONTRACT_FEES.IMPORT;
  urlApiDownloadTemp = UrlConstant.CONTRACT_FEES.DOWNLOAD_TEMPLATE;

  subs: Subscription[] = [];
  objFunction: AppFunction;
  scope = Scopes.VIEW;
  functionCode = FunctionCode.PNS_FEES;
  listContractFeeIds: number[] = [];

  isShowModalReject = false;
  isReject = false;
  listRejectId: number[] = [];
  formReject = this.fb.group({
    rejectReason: ['', [CustomValidators.required, Validators.maxLength(500)]]
  });

  @ViewChild('actionTmpl', {static: true}) action!: TemplateRef<NzSafeAny>;
  @ViewChild('statusTmpl', {static: true}) status!: TemplateRef<NzSafeAny>;
  @ViewChild('selectTmpl', {static: true}) select!: TemplateRef<NzSafeAny>;
  @ViewChild('selectHeaderTmpl', {static: true}) selectHeader!: TemplateRef<NzSafeAny>;
  @ViewChild('amountFeeTmpl', {static: true}) amountFee!: TemplateRef<NzSafeAny>;
  @ViewChild('table', {static: true}) table!: MbTableMergeCellComponent;

  constructor(
    private fb: FormBuilder,
    private translate: TranslateService,
    private toastService: ToastrService,
    private modalService: NzModalService,
    private searchFormService: SearchFormService,
    private contractFeesService: ContractFeesService,
    private sessionService: SessionService,
    private deletePopup: PopupService
  ) {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.PNS_FEES}`);
  }

  ngOnInit(): void {
    this.initDataSelect();
    this.initFormGroup();
    this.initTable();
    this.doSearch(1);
  }

  initDataSelect() {
    this.contractFeeStatus = Constant.CONTRACT_FEES_STATUS.map((item => {
      item.label = this.translate.instant(item.label);
      return item;
    }));
  }

  doSearch(pageIndex: number, refresh?: boolean, orderBy?: string) {
    this.pagination.pageNumber = pageIndex;
    this.tableConfig = {...this.tableConfig};
    this.isLoadingPage = true;
    let params = this.parseOptions();
    if(orderBy){
      params = params.set('orderBy',orderBy);
    }
    if (refresh) {
      this.table.resetAllCheckBoxFn();
    }
    this.subs.push(
      this.searchFormService.getFilterResearch(UrlConstant.SEARCH_FORM.CONTRACT_FEES, params, this.pagination.getCurrentPage()).subscribe({
        next: (res: BaseResponse) => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            // this.dataTable = res?.data.listData;
            this.dataTable = res.data.listData.map((item: NzSafeAny) => {
              item.id = item.contractFeeId;
              if (item.status && item.status !== Constant.DRAFT) { // set disabled status each checkbox in table: optional
                item.disabled = true;
              }
              return item;
            });
            this.tableConfig.total = res.data.count;
          } else {
            this.toastService.error(res?.message);
          }
          this.tableConfig = {...this.tableConfig};
          this.isLoadingPage = false;
        },
        error: (err) => {
          this.toastService.error(err?.message);
          this.tableConfig = {...this.tableConfig};
          this.isLoadingPage = false;
        }
      })
    );
  }

  initFormGroup() {
    this.form = this.fb.group({
      employeeCode: [null],
      empName: [null],
      orgId: [null],
      status: [null],
      year: [null],
      toDate:[null],
      fromDate:[null],
      orderBy:[null]
    });
  }

  parseOptions() {
    let params = new HttpParams();
    if (!StringUtils.isNullOrEmpty(this.form.controls['employeeCode'].value))
      params = params.set('employeeCode', this.form.controls['employeeCode'].value);

    if (!StringUtils.isNullOrEmpty(this.form.controls['empName'].value))
      params = params.set('fullName', this.form.controls['empName'].value);

    if (this.form.controls['orgId'].value !== null)
      params = params.set('orgId', this.form.controls['orgId'].value?.orgId);

    if (this.form.controls['status'].value !== null)
      params = params.set('listStatus', this.form.controls['status'].value.join(','));

    if (this.form.controls['fromDate'].value !== null)
      params = params.set('fromDate', moment(this.form.controls['fromDate'].value).format('DD/MM/YYYY'));

    if (this.form.controls['toDate'].value !== null)
      params = params.set('toDate', moment(this.form.controls['toDate'].value).format('DD/MM/YYYY'));

    return params;
  }

  initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: 'STT',
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          fixed: true,
          fixedDir: 'left',
          width: 70
        },
        {
          title: 'partnership.pns-contract.contractLiquidation.table.employeeCode',
          field: 'employeeCode',
          fixed: true,
          fixedDir: 'left'
        },
        {
          title: 'partnership.pns-contract.contractLiquidation.table.employeeName',
          field: 'fullName',
          fixed: true,
          fixedDir: 'left'
        },
         {
          title: 'partnership.pns-contract.label.jobName',
          field: 'jobName'
        },
        {
          title: 'partnership.pns-contract.label.org',
          field: 'orgName'
        },
        {
          title: 'partnership.pns-contract.label.fromDate',
          field: 'fromDate'
        },
        {
          title: 'partnership.pns-contract.label.toDate',
          field: 'toDate'
        },
        {
          title: 'partnership.pns-contract.label.contractFee',
          field: 'amountFee',
          tdTemplate: this.amountFee,
          fixedDir: 'right'
        },
        {
          title: 'partnership.pns-contract.contractLiquidation.table.status',
          field: 'status',
          tdTemplate: this.status
        },
        {
          title: 'partnership.pns-contract.label.rejectReason',
          field: 'rejectReason',
          show: false
        },
        {
          title: 'partnership.pns-contract.label.createdBy',
          field: 'createdBy',
          show: false
        },
        {
          title: 'partnership.pns-contract.label.createDate',
          field: 'createDate',
          show: false
        },
        {
          title: 'partnership.pns-contract.label.lastUpdatedBy',
          field: 'lastUpdatedBy',
          show: false
        },
        {
          title: 'partnership.pns-contract.label.lastUpdateDate',
          field: 'lastUpdateDate',
          show: false
        },
        {
          title: ' ',
          tdClassList: ['text-nowrap', 'text-center'], thClassList: ['text-nowrap', 'text-center'],
          width: 50,
          tdTemplate: this.action
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1,
      showSelect: true
    };
  }

  setFormValue(event: NzSafeAny, formName: string) {
    if (event.listOfSelected?.length > 0) {
      this.form.controls[formName].setValue(event.listOfSelected);
    }
  }

  openAddModal() {
    this.modal = this.modalService.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
      nzTitle: this.translate.instant('partnership.pns-contract.modalName.createContractFees'),
      nzContent: ContractFeesFormComponent,
      nzComponentParams: {
        mode: Mode.ADD
      },
      nzFooter: null
    });
    this.subs.push(
      this.modal.afterClose.subscribe(res => {
        if (res && res.refresh) {
          this.doSearch(1, false, "contract_fee_id desc");
        }
      })
    );
  }

  openModalEdit(contractFeeId: number) {
    this.modal = this.modalService.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
      nzTitle: this.translate.instant('partnership.pns-contract.modalName.updateContractFees'),
      nzContent: ContractFeesFormComponent,
      nzComponentParams: {
        mode: Mode.EDIT,
        contractFeeId: contractFeeId
      },
      nzFooter: null
    });
    const sub = this.modal?.afterClose.subscribe(res => {
      if (res && res.refresh) {
        this.doSearch(1, false, "contract_fee_id desc");
      }
    });
    if (sub) {
      this.subs.push(sub);
    }
  }

  onDeleteClick(contractFeeId: number, status: number) {
    if (status !== 0) return;
    this.deletePopup.showModal(() => {
      this.subs.push(
        this.contractFeesService.deleteById(contractFeeId).subscribe({
          next: (res: BaseResponse) => {
            if (res.code === HTTP_STATUS_CODE.OK) {
              this.doSearch(1, true);
              this.toastService.success(this.translate.instant('partnership.pns-contract.notification.delSuccess'));
            } else {
              this.toastService.error(res?.message);
            }
          },
          error: (err) => {
            this.toastService.error(err?.message);
          }
        })
      );
    })
  }

  doExportData() {
    if (this.form.valid) {
      this.isLoadingPage = true;
      const params = this.parseOptions();
      this.subs.push(
        this.contractFeesService.exportData(params).subscribe(res => {
          if ((res?.headers?.get('Excel-File-Empty'))) {
            this.toastService.error(this.translate.instant('common.notification.exportFail'));
          } else {
            const arr = res.headers.get("Content-Disposition")?.split(';');
            const fileName:string = arr[arr.length - 1].replace('filename=','').trim();
            const reportFile = new Blob([res.body], {type: getTypeExport('xlsx')});
            saveAs(reportFile, fileName);
          }
          this.isLoadingPage = false;
        }, error => {
          this.toastService.error(error.message);
          this.isLoadingPage = false;
        })
      );
    }
  }

  onApprove(id: number, status: number) {
    if (status !== 0) return;
    this.approve({listId: [id]});
  }

  onAmountSelectedRowChange(event: NzSafeAny) {
    this.listContractFeeIds = Array.from(event);
  }

  onApproveAll() {
    if (this.listContractFeeIds.length > 0) {
      this.approve({listId: this.listContractFeeIds});
    }
  }

  approve(params: { listId: number[] }) {
    this.contractFeesService.approve(params).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.doSearch(1, true);
        this.toastService.success(this.translate.instant('partnership.pns-contract.notification.approveSuccess'));
      } else {
        this.toastService.error(res?.message);
      }
    }, (err) => {
      this.toastService.error(err?.message);
    });
  }

  reject(params: { listId: number[], rejectReason: string}) {
    this.contractFeesService.reject(params).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.doSearch(1, true);
        this.toastService.success(this.translate.instant('partnership.pns-contract.notification.rejectSuccess'));
      } else {
        this.toastService.error(res?.message);
      }
      this.isShowModalReject = false;
    }, (err) => {
      this.isShowModalReject = false;
      this.toastService.error(err?.message);
    });
  }

  doImportData() {
    this.isImportData = true;
  }

  doCloseImport(isSearch: boolean) {
    this.isImportData = false;
    isSearch && this.doSearch(1, false, "contract_fee_id desc");
  }

  onReject(isRejectAll: boolean, id: number, status: number): void {
    if (status !== 0) return;
    if (isRejectAll) {
      if (this.listContractFeeIds.length > 0) {
        this.listRejectId = this.listContractFeeIds;
      }
    } else {
      this.listRejectId = [id];
    }
    this.isShowModalReject = true;
  }

  handleOk(): void {
    this.isReject = true;
    if (this.formReject.valid) {
      this.reject({listId: this.listRejectId, rejectReason: this.formReject.controls['rejectReason'].value});
    }
  }

  handleCancel(): void {
    this.isShowModalReject = false;
  }

  codeBlur() {
    this.formReject.controls['rejectReason'].setValue(this.formReject.controls['rejectReason'].value.trim());
  }

  ngOnDestroy(): void {
    this.modal?.destroy();
    this.subs?.forEach(sub => sub?.unsubscribe());
  }
}
