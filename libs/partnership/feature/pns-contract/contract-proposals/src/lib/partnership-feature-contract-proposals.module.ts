import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {RouterModule} from "@angular/router";
import {ContractProposalsComponent} from "./contract-proposals/contract-proposals.component";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {NzTagModule} from "ng-zorro-antd/tag";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {PartnershipUiSearchFormModule} from "@hcm-mfe/partnership/ui/pns-contract/search-form";
import {SharedUiMbTableMergeCellWrapModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell-wrap";
import {SharedUiMbTableMergeCellModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell";
import {PartnershipUiImportCommonModule} from "@hcm-mfe/partnership/ui/pns-contract/import-common";
import {EditContinueSignComponent} from "./edit-continue-sign/edit-continue-sign.component";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {SharedUiMbEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/mb-employee-data-picker";
import {PartnershipUiPersonalInfoModule} from "@hcm-mfe/partnership/ui/pns-contract/personal-info";
import {SharedUiMbTextLabelModule} from "@hcm-mfe/shared/ui/mb-text-label";
import {EditNewSignComponent} from "./edit-new-sign/edit-new-sign.component";
import {EditAddendumSignComponent} from "./edit-addendum-sign/edit-addendum-sign.component";
import {NzPopconfirmModule} from "ng-zorro-antd/popconfirm";
import {NzFormModule} from "ng-zorro-antd/form";
import {NzDividerModule} from "ng-zorro-antd/divider";
import {ReactiveFormsModule} from "@angular/forms";
import {SharedDirectivesNumberInputModule} from "@hcm-mfe/shared/directives/number-input";
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";
import { EditAmountFeeComponent } from './edit-amount-fee/edit-amount-fee.component';
import {PartnershipUiImportFileModule} from "@hcm-mfe/partnership/ui/import-file";

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, SharedUiMbButtonModule,
    TranslateModule, NzButtonModule, NzDropDownModule, NzIconModule, SharedUiMbTableWrapModule, SharedUiMbTableModule, NzTagModule, PartnershipUiSearchFormModule,
    RouterModule.forChild([
      {path: '', redirectTo: 'search'},
      {
        path: 'search',
        component: ContractProposalsComponent
      },
      {
        path: 'edit-continue-sign',
        data: {
          pageName: 'partnership.pns-contract.pageName.editContinueSign',
          breadcrumb: 'partnership.pns-contract.breadcrumb.editContinueSign'
        },
        component: EditContinueSignComponent
      },
      {
        path: 'detail-continue-sign',
        data: {
          pageName: 'partnership.pns-contract.pageName.detailContinueSign',
          breadcrumb: 'partnership.pns-contract.breadcrumb.detailContinueSign'
        },
        component: EditContinueSignComponent
      },
      {
        path: 'edit-new-sign',
        data: {
          pageName: 'partnership.pns-contract.pageName.editNewSign',
          breadcrumb: 'partnership.pns-contract.breadcrumb.editNewSign'
        },
        component: EditNewSignComponent
      },
      {
        path: 'detail-new-sign',
        data: {
          pageName: 'partnership.pns-contract.pageName.detailNewSign',
          breadcrumb: 'partnership.pns-contract.breadcrumb.detailNewSign'
        },
        component: EditNewSignComponent
      },
      {
        path: 'edit-addendum-sign',
        data: {
          pageName: 'partnership.pns-contract.pageName.editAddendum',
          breadcrumb: 'partnership.pns-contract.breadcrumb.editAddendum'
        },
        component: EditAddendumSignComponent
      },
      {
        path: 'detail-addendum-sign',
        data: {
          pageName: 'partnership.pns-contract.pageName.detailAddendum',
          breadcrumb: 'partnership.pns-contract.breadcrumb.detailAddendum'
        },
        component: EditAddendumSignComponent
      },
      {
        path: 'edit-amount-fee',
        data: {
          pageName: 'partnership.pns-contract.pageName.editAmountFee',
          breadcrumb: 'partnership.pns-contract.breadcrumb.editAmountFee'
        },
        component: EditAmountFeeComponent
      },
      {
        path: 'detail-amount-fee',
        data: {
          pageName: 'partnership.pns-contract.pageName.detailAmountFee',
          breadcrumb: 'partnership.pns-contract.breadcrumb.detailAmountFee'
        },
        component: EditAmountFeeComponent
      }
    ]), SharedUiMbTableMergeCellWrapModule, SharedUiMbTableMergeCellModule, PartnershipUiImportCommonModule, SharedUiMbSelectModule,
    SharedUiMbDatePickerModule, SharedUiMbEmployeeDataPickerModule, PartnershipUiPersonalInfoModule, SharedUiMbInputTextModule, SharedUiMbTextLabelModule,
    NzPopconfirmModule, NzFormModule, NzDividerModule, ReactiveFormsModule, SharedDirectivesNumberInputModule, NzCheckboxModule, PartnershipUiImportFileModule],
  declarations: [ContractProposalsComponent, EditContinueSignComponent, EditNewSignComponent, EditAddendumSignComponent, EditAmountFeeComponent],
  exports: [ContractProposalsComponent]
})
export class PartnershipFeatureContractProposalsModule {}
