import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { HttpParams } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import {ContractApprove, ContractProposal, ContractType } from '@hcm-mfe/partnership/data-access/pns-contract/models';
import {CatalogModel} from "@hcm-mfe/partnership/ui/pns-contract/search-form";
import {Constant, UrlConstant} from "@hcm-mfe/partnership/data-access/pns-contract/common";
import {
  ContractProposalService,
  PersonalInfoService,
  SearchFormService
} from "@hcm-mfe/partnership/data-access/pns-contract/services";
import { HTTP_STATUS_CODE } from '@hcm-mfe/shared/common/constants';
import { SelectModal } from '@hcm-mfe/shared/data-access/models';

@Component({
  selector: 'app-edit-continue-sign',
  templateUrl: './edit-continue-sign.component.html',
  styleUrls: ['./edit-continue-sign.component.scss']
})
export class EditContinueSignComponent implements OnInit {
  employeeId!: number;
  contractProposalId!: number;
  form!: FormGroup;
  listContractType: ContractType[] = [];
  listContractOffer: CatalogModel[] = [];
  listPosition: CatalogModel[] = [];
  listAssessments: CatalogModel[] = [];
  subscriptions: Subscription[] = [];
  isDisabledManager = false;
  isDisabledApprove = false;
  isDisabledBoss = false;
  listApproveStatus = Constant.APPROVE_STATUS;
  managerStatus = Constant.APPROVE_STATUS[1].value;
  approveStatus = Constant.APPROVE_STATUS[0].value;
  bossStatus = Constant.APPROVE_STATUS[0].value;

  managerResponse!: ContractApprove;
  approverResponse!: ContractApprove;
  bossResponse!: ContractApprove;
  curContractTypeName: string | undefined;
  isDetail = false;
  isSubmitted = false;
  isUpdateDate = true;
  isLoadingPage = false;

  constructor(
    private contractProposalService: ContractProposalService,
    private toastrService: ToastrService,
    private fb: FormBuilder,
    private searchFormService: SearchFormService,
    private personalInfoService: PersonalInfoService,
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.initFormGroup();
  }

  ngOnInit(): void {
    this.getListContractType();
    this.getListPosition();
    this.initDataSelect();
    this.getUrlInfo();
  }

  initFormGroup() {
    this.form = this.fb.group({
      contractNumber: null,
      curContractNumber: null,
      amountSalary: null,
      curContractTypeId: null,
      fromDate: null,
      curFromDate: null,
      toDate: null,
      toDateByLaw: null,
      curToDate: null,
      signer: [null, Validators.required],
      signerPosition: null,
      isDisciplined: null,
      delegacyNo: null,
      offer: null,
      kpiPoint: null,
      rankName: null,
      contractByLawId: [null, Validators.required],
      manager: null,
      managerOffer: null,
      managerFromDate: null,
      managerToDate: null,
      managerContractTypeId: null,
      approver: null,
      approveOffer: null,
      approveContractTypeId: null,
      approveFromDate: null,
      approveToDate: null,
      approverOther: null,
      bossOffer: null,
      bossFromDate: null,
      bossToDate: null,
      bossContractTypeId: null,
      managerNote: null
    });
  }

  get f() {
    return this.form.controls;
  }

  getUrlInfo() {
    this.contractProposalId = this.activatedRoute.snapshot.queryParams['id'];
    this.employeeId = this.activatedRoute.snapshot.queryParams['empId'];
    if (this.router.url.includes(UrlConstant.NAVIGATE_URL.DETAIL_CONTINUE_SIGN)) {
      this.isDetail = true;
    }
    if (this.contractProposalId) {
      this.pathFormGroup().then();
    }
  }

  async pathFormGroup() {
    const res = await this.contractProposalService.getById(this.contractProposalId).toPromise();
    if (res.code === HTTP_STATUS_CODE.OK) {
      const data = res.data;
      this.form.patchValue(data);
      if (data.managerResponse) {
        this.setManagerInfo(data.managerResponse);
      }
      if (data.approverResponse) {
        this.setApproverInfo(data.approverResponse);
      }
      if (data.bossResponse) {
        this.setBossInfo(data.bossResponse);
      }

      this.setValueDateForm(data, 'fromDate', 'toDate');
      this.f['kpiPoint'].setValue(data.evaluationsResponse?.kpiPoint);
      this.f['rankName'].setValue(data.evaluationsResponse?.rankName);
      this.f['isDisciplined'].setValue(data.evaluationsResponse?.isDisciplined);
      this.getPersonalInfo(data.signerId, Constant.APPROVER_LEVEL[3].value);
      this.curContractTypeName = this.listContractType.find(item => item.contractTypeId === data.curContractTypeId)?.name;
      this.f['toDateByLaw'].setValue(data.toDateByLaw ? moment(data.toDateByLaw, 'DD/MM/YYYY').toDate() : null);
      this.isUpdateDate = false;
    }
  }

  setManagerInfo(data: ContractApprove) {
    this.managerResponse = data;
    this.getPersonalInfo(data.approverId, Constant.APPROVER_LEVEL[0].value);
    this.setValueDateForm(data, 'managerFromDate', 'managerToDate');
    this.f['managerOffer'].setValue(data.isLiquidation);
    this.f['managerContractTypeId'].setValue(data.contractTypeId);
    this.f['managerNote'].setValue(data.note ? data.note : null);

    if (data.isLiquidation != null && data.isLiquidation >= 0) {
      this.isDisabledManager = true;
      this.managerStatus = Constant.APPROVE_STATUS[2].value;
    }
  }

  setApproverInfo(data: ContractApprove) {
    this.approverResponse = data;
    this.getPersonalInfo(data.approverId, Constant.APPROVER_LEVEL[1].value);
    this.setValueDateForm(data, 'approveFromDate', 'approveToDate');
    this.f['approveOffer'].setValue(data.isLiquidation);
    this.f['approveContractTypeId'].setValue(data.contractTypeId);

    if (this.isDisabledManager) {
      this.approveStatus = Constant.APPROVE_STATUS[1].value;
    }

    if (data.isLiquidation != null && data.isLiquidation >= 0) {
      this.isDisabledApprove = true;
      this.approveStatus = Constant.APPROVE_STATUS[2].value;
    }
  }

  setBossInfo(data: ContractApprove) {
    this.bossResponse = data;
    this.getPersonalInfo(this.bossResponse.approverId, Constant.APPROVER_LEVEL[2].value);
    this.setValueDateForm(data, 'bossFromDate', 'bossToDate');
    this.f['bossOffer'].setValue(data.isLiquidation);
    this.f['bossContractTypeId'].setValue(data.contractTypeId);
    if (this.isDisabledApprove) {
      this.bossStatus = Constant.APPROVE_STATUS[1].value;
    }
    if (data.isLiquidation != null && data.isLiquidation >= 0) {
      this.isDisabledBoss = true;
      this.bossStatus = Constant.APPROVE_STATUS[2].value;
    }
  }

  setValueDateForm(data: ContractProposal | ContractApprove, ...keys: string[]) {
    keys.forEach((key: string) => {
      if (key.toLowerCase().indexOf('todate') > -1) {
        this.f[key].setValue(data.toDate ? moment(data.toDate, 'DD/MM/YYYY').toDate() : null);
      } else if (key.toLowerCase().indexOf('fromdate') > -1) {
        this.f[key].setValue(data.fromDate ? moment(data.fromDate, 'DD/MM/YYYY').toDate() : null);
      }
    });
  }

  getPersonalInfo(signerId: number | undefined, type: number) {
    if (signerId) {
      this.subscriptions.push(
        this.personalInfoService.getPersonalInfo(signerId).subscribe(res => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            const key = Constant.APPROVER_LEVEL.find(item => item.value === type)?.label;
            if (key) {
              this.f[key].setValue(res.data);
            }
          }
        }, error => this.toastrService.error(error.message))
      );
    }
  }

  getListContractType() {
    const params = new HttpParams().set('classifyCode', Constant.CLASSIFY_CODE.HOP_DONG);
    this.subscriptions.push(
      this.searchFormService.getContractType(UrlConstant.CATALOGS.CONTRACT_TYPE, params).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listContractType = res.data.map((item: ContractType) => {
            item.label = item.name;
            item.value = item.contractTypeId;
            return item;
          });

          if (this.f['curContractTypeId'].value && !this.curContractTypeName) {
            this.curContractTypeName = this.listContractType.find(item => item.contractTypeId === this.f['curContractTypeId'].value)?.name;
          }
        }
      }, error => {
        this.toastrService.error(error.message);
        this.listContractType = [];
      })
    );
  }

  initDataSelect() {
    this.listContractOffer = Constant.CONTRACT_OFFER.map(item => {
      item.label = this.translate.instant(item.label);
      return item;
    });

    this.listAssessments = Constant.ASSESSMENTS.map(item => {
      item.label = this.translate.instant(item.label);
      return item;
    });
  }

  getListPosition() {
    const params = new HttpParams().set('jobType', Constant.JOB_TYPE.ORG);
    this.subscriptions.push(
      this.searchFormService.getListValue(UrlConstant.CATEGORY.MP_JOB, params).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listPosition = res.data;
        } else {
          this.toastrService.error(res.message);
        }
      }, error => {
        this.toastrService.error(error.message);
        this.listPosition = [];
      })
    );
  }

  selectContractType(event: SelectModal, isUpdateDate: boolean) {
    if (isUpdateDate) {
      const data = event.itemSelected;
      if (data && data.duration) {
        const toDate = new Date(moment(this.f['fromDate'].value).format('MM/DD/YYYY'));
        toDate.setMonth(toDate.getMonth() + data.duration);
        toDate.setDate(toDate.getDate() - 1);
        this.f['toDateByLaw'].setValue(toDate);
      } else {
        this.f['toDateByLaw'].setValue(null);
      }
    }
    this.isUpdateDate = true;
  }

  onSave() {
    this.isSubmitted = true;
    this.validateEmpDataPicker();
    if (this.form.valid) {
      this.isLoadingPage = true;
      const request = this.getRequestSave();
      this.subscriptions.push(
        this.contractProposalService.saveContractProposal(request).subscribe(res => {
          if (res && res.code === HTTP_STATUS_CODE.OK) {
            this.toastrService.success(this.translate.instant('common.notification.updateSuccess'));
            this.isLoadingPage = false;
            this.onCancel();
          } else {
            this.toastrService.error(this.translate.instant('common.notification.updateError') + ': ' + res?.message);
          }
          this.isLoadingPage = false;
        }, error => {
          this.toastrService.error(error.message);
          this.isLoadingPage = false;
        })
      );
    }
  }

  getRequestSave(): ContractProposal {
    const request: ContractProposal = this.form.value;
    request.contractProposalId = this.contractProposalId;
    request.signerId = this.f['signer'].value?.employeeId;

    request.contractApproveDTOs = [];
    const approveRequest: ContractApprove[] = [this.managerResponse, this.approverResponse, this.bossResponse];
    approveRequest.forEach((item, index) => {
      const key: string = Constant.APPROVER_LEVEL[index].label;
      const data: ContractApprove = {
        approverId: this.f[key].value?.employeeId,
        approverLevel: Constant.APPROVER_LEVEL[index].value,
        contractProposalId: this.contractProposalId,
        contractApproverId: item?.contractApproverId
      };
      request.contractApproveDTOs?.push(data);
    });

    return request;
  }

  validateEmpDataPicker() {
    Constant.DATA_PICKER_ERROR.some((item, index) => {
      const isDupicate = this.f[item.value].value?.employeeId === ~~this.employeeId;
      if (isDupicate) {
        this.form.setErrors({errDataPicker: true});
        this.toastrService.error(this.translate.instant(Constant.DATA_PICKER_ERROR[index].label));
      } else {
        this.form.setErrors(null);
      }
      return isDupicate;
    });
  }

  onCancel() {
    localStorage.setItem("isBackUrl", "true");
    this.router.navigateByUrl(UrlConstant.NAVIGATE_URL.SEARCH).then();
  }

  ngOnDestroy(): void {
    this.subscriptions?.forEach(sub => sub.unsubscribe());
  }

}
