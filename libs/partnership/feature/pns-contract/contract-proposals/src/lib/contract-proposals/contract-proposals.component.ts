import {AfterViewInit, Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { HttpParams } from '@angular/common/http';
import { saveAs } from 'file-saver';
import {AppFunction, BaseResponse, Pagination, TableConfig} from "@hcm-mfe/shared/data-access/models";
import {ConstantColor, Constant, UrlConstant, FunctionCode} from "@hcm-mfe/partnership/data-access/pns-contract/common";
import {HTTP_STATUS_CODE} from '@hcm-mfe/shared/common/constants';
import {ContractProposalService, SearchFormService} from "@hcm-mfe/partnership/data-access/pns-contract/services";
import { SessionService } from '@hcm-mfe/shared/common/store';
import {getTypeExport} from "@hcm-mfe/shared/common/utils";
import {SearchFormComponent} from "@hcm-mfe/partnership/ui/pns-contract/search-form";
import {NzTooltipTrigger} from "ng-zorro-antd/tooltip/base";
import {SendMailContractResponse} from "@hcm-mfe/partnership/data-access/pns-contract/models";
import { Scopes } from '@hcm-mfe/shared/common/enums';

@Component({
  selector: 'app-contract-proposals',
  templateUrl: './contract-proposals.component.html',
  styleUrls: ['./contract-proposals.component.scss']
})
export class ContractProposalsComponent implements OnInit, AfterViewInit, OnDestroy {

  pagination = new Pagination();
  tableConfig!: TableConfig;
  dataTable: NzSafeAny[] = [];
  constant = Constant;
  isImportResult = false;
  listContractProposalId: number[] = [];
  isRejectList = false;
  isReject = false;
  isDisabled = false;
  isLoadingPage = false;
  mbButtonBgColor = ConstantColor.COLOR.BG_PRIMARY_LIGHT;
  functionCode = FunctionCode.PNS_CONTRACT_PROPOSALS;
  scope = Scopes.VIEW;
  objFunction: AppFunction;
  subscriptions: Subscription[] = [];

  disabledColor = ConstantColor.COLOR.DISABLED;
  newSign = Constant.CONTRACT_TYPE[0].value;
  continueSigne = Constant.CONTRACT_TYPE[1].value;
  addendumSigne = Constant.CONTRACT_TYPE[2].value;
  changeAmountFee = Constant.CONTRACT_TYPE[3].value;
  contractType = this.newSign;
  checkAll: boolean[] = [];
  isSearchAllType = false;
  isImportAmountFee = false;
  urlApiDownloadTemp = UrlConstant.CONTRACT_PROPOSAL.CONTRACT_FEE_TEMPLATE;
  urlApiImport = UrlConstant.CONTRACT_PROPOSAL.CONTRACT_FEE_IMPORT;

  @ViewChild('searchForm') searchForm!: SearchFormComponent;
  @ViewChild('statusTmpl', { static: true }) status!: TemplateRef<NzSafeAny>;
  @ViewChild('empStatusTmpl', { static: true }) empStatus!: TemplateRef<NzSafeAny>;
  @ViewChild('actionTmpl', { static: true }) action!: TemplateRef<NzSafeAny>;
  @ViewChild('selectTmpl', { static: true }) select!: TemplateRef<NzSafeAny>;
  @ViewChild('selectHeaderTmpl', { static: true }) selectHeader!: TemplateRef<NzSafeAny>;
  @ViewChild('employeeCodeTmpl', { static: true }) employeeCode!: TemplateRef<NzSafeAny>;
  @ViewChild('employeeNameTmpl', { static: true }) employeeName!: TemplateRef<NzSafeAny>;

  constructor(private searchFormService: SearchFormService,
              private translateService: TranslateService,
              public sessionService: SessionService,
              private toastrService: ToastrService,
              private router: Router,
              private contractProposalService: ContractProposalService) {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.PNS_CONTRACT_PROPOSALS}`);
  }

  ngOnInit(): void {
    this.initTable();
  }

  ngAfterViewInit() {
    this.getOldSearchForm();
    this.doSearch(1, true);
  }

  getOldSearchForm() {
      if (localStorage.getItem("oldSearchForm") && localStorage.getItem("isBackUrl")) {
          const oldSearchForm = localStorage.getItem("oldSearchForm");
          if (oldSearchForm) {
              this.searchForm.form.setValue(JSON.parse(oldSearchForm));
          }
          this.doSearch(1, true);
          localStorage.removeItem("oldSearchForm");
          localStorage.removeItem("isBackUrl");
      }
  }

  doSearch(pageIndex: number, changeHeader = false) {
    this.pagination.pageNumber = pageIndex;
    let searchParam = new HttpParams();
    if (this.searchForm) {
      searchParam = this.searchForm.parseOptions();
      if (changeHeader) {
        this.changeContractType(this.searchForm.form.controls['type'].value);
        this.checkAll[pageIndex] = false;
        this.listContractProposalId = [];
      }
    }

    this.isLoadingPage = true;
    this.subscriptions.push(
      this.searchFormService.getFilterResearch(UrlConstant.SEARCH_FORM.CONTRACT_PROPOSALS, searchParam, this.pagination.getCurrentPage()).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.dataTable = res.data.listData.map((item: NzSafeAny) => {
            let toDate: string;
            if (item.type === ~~this.continueSigne) {
              toDate = item.toDateByLaw ? item.toDateByLaw : this.translateService.instant('partnership.pns-contract.label.unknown');
            } else {
              toDate = item.toDate ? item.toDate : this.translateService.instant('partnership.pns-contract.label.unknown');
            }
            const curToDate = item.curToDate ? item.curToDate : this.translateService.instant('partnership.pns-contract.label.unknown');
            item.curContractTerm = item.curFromDate + ' - ' + curToDate;
            item.contractTerm = item.fromDate + ' - ' + toDate;
            item.curToDate = curToDate;
            item.toDate = toDate;
            item.amountFee = item.amountFee ? new Intl.NumberFormat('de-DE').format(item.amountFee) : item.amountFee;
            item.oldAmountFee = item.oldAmountFee ? new Intl.NumberFormat('de-DE').format(item.oldAmountFee) : item.oldAmountFee;
            const managerOfferLabel = Constant.CONTRACT_OFFER.find(data => data.value === item.managerOffer)?.label;
            const approveOfferLabel = Constant.CONTRACT_OFFER.find(data => data.value === item.approveOffer)?.label;
            const bossOfferLabel = Constant.CONTRACT_OFFER.find(data => data.value === item.bossOffer)?.label;
            item.managerOffer = managerOfferLabel ? this.translateService.instant(managerOfferLabel) : null;
            item.approveOffer = approveOfferLabel ? this.translateService.instant(approveOfferLabel) : null;
            item.bossOffer = bossOfferLabel ? this.translateService.instant(bossOfferLabel) : null;
            if (this.isSearchAllType && item.type === ~~this.newSign) {
              item.curContractTerm = item.type === ~~this.newSign ? null : item.curContractTerm;
              item.contractByLawName =  item.contractTypeName;
            }
            const typeName = Constant.CONTRACT_TYPE.find(data => ~~data.value === item.type)?.label;
            item.typeName = typeName ? this.translateService.instant(typeName) : "";
            return item;
          });
          this.tableConfig.pageIndex = pageIndex;
          this.tableConfig.total = res.data.count;
        }
        this.isLoadingPage = false;
      }, error => {
        this.isLoadingPage = false;
        this.toastrService.error(error?.message);
      })
    );
  }

  changeContractType(contractType: string) {
    if (contractType) {
      this.contractType = contractType;
      this.isSearchAllType = false;
    } else {
      this.contractType = this.continueSigne;
      this.isSearchAllType = true;
    }
    this.initTable();
  }

  private initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: ' ',
          field: 'select',
          width: 60,
          tdTemplate: this.select,
          thTemplate: this.selectHeader,
          thClassList: ['text-nowrap','text-center'],
          tdClassList: ['text-nowrap','text-center'],
          fixed: window.innerWidth > 1024,
          fixedDir: 'left',
          rowspan: 2,
        },
        {
          title: 'STT',
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          rowspan: 2,
          fixed: true,
          fixedDir: 'left',
          width: 50
        },
        {
          title: 'partnership.pns-contract.label.employeeCode',
          field: 'employeeCode', width: 120,
          rowspan: 2,
          fixed: true,
          thClassList: ['text-center'],
          fixedDir: 'left'
        },
        {
          title: 'partnership.pns-contract.label.employeeName',
          field: 'fullName', width: 150,
          rowspan: 2,
          fixed: true,
          thClassList: ['text-center'],
          fixedDir: 'left'
        },
        {
          title: 'partnership.pns-contract.searchForm.type',
          field: 'typeName',
          thClassList: ['text-center'],
          width: 120,
          rowspan: 2
        },
        {
          title: 'partnership.pns-contract.label.status',
          field: 'status',
          tdTemplate: this.status,
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          width: 180,
          rowspan: 2,
        },
        {
          title: 'partnership.pns-contract.label.curContractTypeName',
          colspan: 3,
          remove: this.contractType === this.newSign,
          child: [
            {
              title: 'partnership.pns-contract.label.contractTypeName',
              width: 180,
              thClassList: ['text-center'],
              field: 'curContractTypeName'
            },
            {
              title: 'partnership.pns-contract.label.contractNumber',
              width: 180,
              thClassList: ['text-center'],
              field: 'curContractNumber'
            },
            {
              title: 'partnership.pns-contract.label.contractTerm',
              width: 120,
              field: 'curContractTerm',
              thClassList: ['text-center']
            }
          ]
        },
        {
          title: 'partnership.pns-contract.label.oldAmountFee',
          width: 120,
          rowspan: 2,
          field: 'oldAmountFee',
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          remove: this.contractType !== this.changeAmountFee && !this.isSearchAllType
        },
        {
          title: this.isSearchAllType ? 'partnership.pns-contract.label.contractInfo' :
                 (this.contractType === this.continueSigne ? 'partnership.pns-contract.label.contractByLawInfo' : 'partnership.pns-contract.label.addendumInfo'),
          colspan: (this.contractType !== this.changeAmountFee && !this.isSearchAllType) ? 3 : 4,
          remove: this.contractType === this.newSign,
          child: [
            {
              title: this.isSearchAllType ? 'partnership.pns-contract.label.contractTypeOrAddendum' :
                     (this.contractType === this.continueSigne ? 'partnership.pns-contract.label.contractTypeName' : 'partnership.pns-contract.label.curAddendumTypeName'),
              width: 180,
              thClassList: ['text-center'],
              field: 'contractByLawName'
            },
            {
              title: this.isSearchAllType ? 'partnership.pns-contract.label.contractOrAddendumNumber' :
                    (this.contractType === this.continueSigne ? 'partnership.pns-contract.label.contractNumber' : 'partnership.pns-contract.label.addendumNumber'),
              width: 180,
              thClassList: ['text-center'],
              field: 'contractNumber'
            },
            {
              title: this.isSearchAllType ? 'partnership.pns-contract.label.contractOrAddendumTerm' :
                     (this.contractType === this.continueSigne ? 'partnership.pns-contract.label.contractTerm' : 'partnership.pns-contract.label.addendumTerm'),
              width: 120,
              field: 'contractTerm',
              thClassList: ['text-center']
            },
            {
              title: 'partnership.pns-contract.label.amountFee',
              width: 120,
              thClassList: ['text-center'],
              tdClassList: ['text-center'],
              field: 'amountFee',
              remove: this.contractType !== this.changeAmountFee && !this.isSearchAllType,
            },
          ]
        },
        {
          title: 'partnership.pns-contract.label.contractTypeName',
          rowspan: 2,
          field: 'contractTypeName',
          thClassList: ['text-center'],
          width: 200,
          remove: this.contractType !== this.newSign
        },
        {
          title: 'partnership.pns-contract.label.contractNumber',
          rowspan: 2,
          field: 'contractNumber',
          thClassList: ['text-center'],
          width: 200,
          remove: this.contractType !== this.newSign
        },
        {
          title: 'partnership.pns-contract.label.contractTerm',
          rowspan: 2,
          field: 'contractTerm',
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          width: 200,
          remove: this.contractType !== this.newSign
        },
        {
          title: 'partnership.pns-contract.label.criteria',
          colspan: 3,
          remove: this.contractType !== this.continueSigne,
          child: [
            {
              title: 'partnership.pns-contract.label.disciplinedName',
              width: 180,
              tdClassList: ['text-center'],
              thClassList: ['text-center'],
              field: 'disciplinedName'
            },
            {
              title: 'partnership.pns-contract.label.kpiPoint',
              width: 100,
              tdClassList: ['text-center'],
              thClassList: ['text-center'],
              field: 'kpiPoint'
            },
            {
              title: 'partnership.pns-contract.label.rankName',
              width: 100,
              field: 'rankName',
              thClassList: ['text-center']
            }
          ]
        },
        {
          title: 'partnership.pns-contract.label.signerInfo',
          colspan: 3,
          child: [
            {
              title: 'partnership.pns-contract.label.signerName',
              width: 200,
              field: 'signerName',
              thClassList: ['text-center']
            },
            {
              title: 'partnership.pns-contract.label.signerPosition',
              width: 150,
              field: 'signerPosition',
              thClassList: ['text-center']
            },
            {
              title: 'partnership.pns-contract.label.delegacyNo',
              width: 150,
              field: 'delegacyNo',
              thClassList: ['text-center'],
              tdClassList: ['text-left']
            }
          ]
        },
        {
          title: 'partnership.pns-contract.label.manager',
          colspan: 4,
          remove: this.contractType !== this.continueSigne,
          child: [
            {
              title: 'partnership.pns-contract.label.managerName',
              width: 150,
              field: 'managerName',
              thClassList: ['text-center']
            },
            {
              title: 'partnership.pns-contract.label.offer',
              width: 150,
              field: 'managerOffer',
              thClassList: ['text-center']
            },
            {
              title: 'partnership.pns-contract.label.contractTypeName',
              width: 100,
              field: 'managerContractTypeName',
              thClassList: ['text-center']
            },
            {
              title: 'partnership.pns-contract.label.contractTerm',
              width: 100,
              field: 'managerContractTerm',
              thClassList: ['text-center']
            }
          ]
        },
        {
          title: 'partnership.pns-contract.label.approveLevel',
          colspan: 4,
          remove: this.contractType !== this.continueSigne,
          child: [
            {
              title: 'partnership.pns-contract.label.approveName',
              width: 150,
              field: 'approveName',
              thClassList: ['text-center']
            },
            {
              title: 'partnership.pns-contract.label.offer',
              width: 150,
              field: 'approveOffer',
              thClassList: ['text-center']
            },
            {
              title: 'partnership.pns-contract.label.contractTypeName',
              width: 100,
              field: 'approveContractTypeName',
              thClassList: ['text-center']
            },
            {
              title: 'partnership.pns-contract.label.contractTerm',
              width: 100,
              field: 'approverContractTerm',
              thClassList: ['text-center']
            }
          ]
        },
        {
          title: 'partnership.pns-contract.label.approveChangeLevel',
          colspan: 4,
          remove: this.contractType !== this.continueSigne,
          child: [
            {
              title: 'partnership.pns-contract.label.approveName',
              width: 150,
              field: 'bossName',
              thClassList: ['text-center']
            },
            {
              title: 'partnership.pns-contract.label.offer',
              width: 150,
              field: 'bossOffer',
              thClassList: ['text-center']
            },
            {
              title: 'partnership.pns-contract.label.contractTypeName',
              width: 100,
              field: 'bossContractTypeName',
              thClassList: ['text-center']
            },
            {
              title: 'partnership.pns-contract.label.contractTerm',
              width: 100,
              field: 'bossContractTerm',
              thClassList: ['text-center']
            }
          ]
        },
        {
          title: 'partnership.pns-contract.label.orgName',
          field: 'orgName',
          rowspan: 2,
          thClassList: ['text-center'],
          width: 200,
          show: false
        },
        {
          title: 'partnership.pns-contract.label.positionName',
          rowspan: 2,
          field: 'positionName',
          thClassList: ['text-center'],
          width: 150,
          show: false
        },
        {
          title: ' ',
          field: 'action',
          rowspan: 2,
          tdClassList: ['text-center'],
          thClassList: ['text-center'],
          width: 50,
          tdTemplate: this.action,
          fixed: window.innerWidth > 1024,
          fixedDir: 'right'
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: this.pagination.pageNumber
    };
  }

  exportContractById(contractProposalId: number, empCode: string, empName: string) {
    this.isLoadingPage = true;
    const fileName = `${empCode}_${empName}.pdf`;
    this.subscriptions.push(
      this.contractProposalService.exportContractById(contractProposalId).subscribe(res => {
        this.downLoadFileContract(res.body, fileName)
      }, () => {
        this.toastrService.error(this.translateService.instant('partnership.pns-contract.notification.downloadFileError'));
        this.isLoadingPage = false;
      })
    );
  }

  doExportListSign() {
    let searchParam = new HttpParams();
    let fileName = '';
    if (this.searchForm) {
      searchParam = this.searchForm.parseOptions();
      const type = this.searchForm.form.controls['type'].value;
      if (type === null) {
        fileName = 'BM_Xuat_DS_Ky_HĐ.xlsx';
      } else if (type === this.newSign) {
        fileName = 'BM_Xuat_DS_KyMoiHĐ.xlsx';
      } else if (type === this.continueSigne) {
        fileName = 'BM_Xuat_DS_KyTiepHĐ.xlsx';
      } else {
        fileName = 'BM_Xuat_DS_ThayDoiPhuLucHĐ.xlsx';
      }
    }
    this.isLoadingPage = true;
    this.contractProposalService.exportContractByForm(UrlConstant.CONTRACT_PROPOSAL.EXPORT_LIST_SIGN, searchParam).subscribe(res => {
      if (res?.headers?.get('Excel-File-Empty')) {
        this.toastrService.error(this.translateService.instant("common.notification.exportFail"));
      } else {
        const reportFile = new Blob([res.body], { type: getTypeExport(fileName.split('.')?.pop()) });
        saveAs(reportFile, fileName);
      }
      this.isLoadingPage = false;
    }, () => {
      this.toastrService.error(this.translateService.instant('partnership.pns-contract.notification.downloadFileError'));
      this.isLoadingPage = false;
    })
  }

  doExportData() {
    let searchParam = new HttpParams();
    let fileName = '';
    if (this.searchForm) {
      searchParam = this.searchForm.parseOptions();
      const type = this.searchForm.form.controls['type'].value;
      if (type === null) {
        fileName = "danh_sach_ky_moi_ky_tiep_hop_dong.zip";
      } else if (type === this.newSign){
        fileName = "danh_sach_ky_moi_hop_dong.zip";
      } else if (type === this.continueSigne){
        fileName = "danh_sach_ky_tiep_hop_dong_lao_dong.zip";
      } else {
        fileName = "danh_sach_ky_phu_luc_thay_doi_luong.zip";
      }
    }
    this.isLoadingPage = true;
    this.contractProposalService.exportContractByForm(UrlConstant.CONTRACT_PROPOSAL.EXPORT_BY_FORM, searchParam).subscribe(res => {
      this.downLoadFileContract(res.body, fileName);
    }, () => {
      this.toastrService.error(this.translateService.instant('partnership.pns-contract.notification.downloadFileError'));
      this.isLoadingPage = false;
    })
  }

  downLoadFileContract(data: Blob, fileName: string) {
    const reportFile = new Blob([data], { type: getTypeExport('docx') });
    const fr = new FileReader();
    fr.onload = (e) => {
      if (typeof e?.target?.result === 'string') {
        try {
          const body = JSON.parse(e.target.result);
          if (body.code === HTTP_STATUS_CODE.CREATED) {
            this.toastrService.error(body.message);
          }
        } catch (e) {
          saveAs(reportFile, fileName);
        }
      }
    };
    fr.readAsText(reportFile);
    this.isLoadingPage = false;
  }

  openEdit(type: number, contractProposalId: number, empId: number, status: number, isDetail?: boolean) {
    if ((status < ~~Constant.SIGN_STATUS[3].value && type === ~~this.continueSigne) ||
      (status < ~~Constant.SIGN_STATUS[4].value && type != ~~this.continueSigne) || isDetail) {
      localStorage.setItem("oldSearchForm", JSON.stringify(this.searchForm.form.value));
      const endUrl = `?id=${contractProposalId}&empId=${empId}`;
      let url: string;
      if (type.toString() === this.newSign) {
        url = isDetail ? (UrlConstant.NAVIGATE_URL.DETAIL_NEW_SIGN + endUrl) : (UrlConstant.NAVIGATE_URL.EDIT_NEW_SIGN + endUrl);
        this.router.navigateByUrl(url).then();
      } else if (type.toString() === this.continueSigne) {
        url = isDetail ? (UrlConstant.NAVIGATE_URL.DETAIL_CONTINUE_SIGN + endUrl) : (UrlConstant.NAVIGATE_URL.EDIT_CONTINUE_SIGN + endUrl);
        this.router.navigateByUrl(url).then();
      } else if (type.toString() === this.addendumSigne) {
        url = isDetail ? (UrlConstant.NAVIGATE_URL.DETAIL_ADDENDUM_SIGN + endUrl) : (UrlConstant.NAVIGATE_URL.EDIT_ADDENDUM_SIGN + endUrl);
        this.router.navigateByUrl(url).then();
      } else {
        url = isDetail ? (UrlConstant.NAVIGATE_URL.DETAIL_AMOUNT_FEE + endUrl) : (UrlConstant.NAVIGATE_URL.EDIT_AMOUNT_FEE + endUrl);
        this.router.navigateByUrl(url).then();
      }
    }
  }

  onCheckChange(event: boolean, id: number){
    if (event) {
      this.listContractProposalId.push(id);
    } else {
      this.listContractProposalId.splice(this.listContractProposalId.indexOf(id),1);
    }
    const notCheckAll = this.dataTable.some(item => {
      return this.listContractProposalId.indexOf(item.contractProposalId) < 0;
    });
    this.checkAll[this.pagination.pageNumber] = !notCheckAll;
  }

  doExportByListId() {
    const type = this.searchForm.form.controls['type'].value;
    let fileName = '';
    if (type === null) {
      fileName = "danh_sach_ky_moi_ky_tiep_hop_dong.pdf";
    } else if (type === this.newSign){
      fileName = "danh_sach_ky_moi_hop_dong.pdf";
    } else if (type === this.continueSigne){
      fileName = "danh_sach_ky_tiep_hop_dong_lao_dong.pdf";
    } else {
      fileName = "danh_sach_ky_phu_luc_thay_doi_luong.pdf";
    }
    this.isLoadingPage = true;
    this.contractProposalService.exportContractByListId(this.listContractProposalId).subscribe(res => {
      this.downLoadFileContract(res.body, fileName);
    }, () => {
      this.toastrService.error(this.translateService.instant('partnership.pns-contract.notification.downloadFileError'));
      this.isLoadingPage = false;
    })
  }

  showImportResult() {
    this.isImportResult = true;
  }

  showImportAmountFee() {
    this.isImportAmountFee = true;
  }

  onLoadPage(event: boolean) {
    this.isLoadingPage = event;
  }

  doCloseImport(refresh: boolean) {
    this.isImportResult = false;
    this.isImportAmountFee = false;
    if (refresh) {
      this.doSearch(this.pagination.pageNumber)
    }
  }

  onCheckAll(event: boolean) {
    if (event) {
      this.dataTable.forEach(item => {
        const id = item.contractProposalId;
        if (this.listContractProposalId.indexOf(id) < 0) {
          this.listContractProposalId.push(id);
        }
      })
    } else {
      this.dataTable.forEach(item => {
        const id = item.contractProposalId;
        this.listContractProposalId = this.listContractProposalId.filter(contractProposalId => contractProposalId != id);
      });
    }
  }

  onDetail(data: NzSafeAny) {
    if (data) {
      this.openEdit(data.type, data.contractProposalId, data.employeeId, data.status, true);
    }
  }

  deleteById(id: number, status: number, type: number) {
    if (status === ~~Constant.SIGN_STATUS[0].value || (status === ~~Constant.SIGN_STATUS[3].value && type !== ~~this.continueSigne)) {
      this.isLoadingPage = true;
      this.subscriptions.push(
        this.contractProposalService.deleteContractProposalById(id).subscribe(res => {
          if(res.code === HTTP_STATUS_CODE.OK) {
            this.doSearch(this.pagination.pageNumber);
            this.toastrService.success(this.translateService.instant('common.notification.deleteSuccess'));
          } else {
            this.toastrService.error(res.message);
          }
          this.isLoadingPage = false;
        }, err => {
          this.toastrService.error(err.message);
          this.isLoadingPage = false;
        })
      );
    }
  }

  onDeleteAll() {
    if (this.searchForm.form.controls['type'].value === null) {
      this.toastrService.error(this.translateService.instant('partnership.pns-contract.validate.typeRequired'));
    } else {
      const searchParams = this.searchForm.parseOptions() ? this.searchForm.parseOptions() : new HttpParams();
      this.isLoadingPage = true;
      this.subscriptions.push(
        this.contractProposalService.deleteContractProposalByForm(searchParams).subscribe(res => {
          if(res.code === HTTP_STATUS_CODE.OK) {
            this.doSearch(this.pagination.pageNumber);
            this.toastrService.success(this.translateService.instant('common.notification.deleteSuccess'));
          } else {
            this.toastrService.error(res.message);
          }
          this.isLoadingPage = false;
        }, err => {
          this.toastrService.error(err.message);
          this.isLoadingPage = false;
        })
      );
    }
  }

  getPopconfirmTrigger(status: number, type: number): NzTooltipTrigger {
    if ((status > ~~Constant.SIGN_STATUS[0].value && type === ~~this.continueSigne) ||
      (status > ~~Constant.SIGN_STATUS[3].value && type !== ~~this.continueSigne)) {
      return null;
    }
    return 'click';
  }

  sendMailContract(contractProposalId?: number, isSentMail?: number) {
    if (contractProposalId) { // gửi mail từng bản ghi
      if (!isSentMail || isSentMail === 0) {
        this.isLoadingPage = true;
        this.subscriptions.push(
          this.contractProposalService.sendMailContract([contractProposalId]).subscribe({
            next: (res: BaseResponse) => {
              if (res.code === HTTP_STATUS_CODE.OK) {
                this.processSendMailResponse(res?.data);
                this.doSearch(1);
              } else {
                this.toastrService.error(res?.message);
              }
              this.isLoadingPage = false;
            },
            error: (err) => {
              this.toastrService.error(err?.message);
              this.isLoadingPage = false;
            }
          })
        );
      }
    } else { // gửi mail theo list checkbox
      this.isLoadingPage = true;
      this.subscriptions.push(
        this.contractProposalService.sendMailContract(this.listContractProposalId).subscribe({
          next: (res: BaseResponse) => {
            if (res.code === HTTP_STATUS_CODE.OK) {
              this.processSendMailResponse(res?.data);
              this.doSearch(1);
            } else {
              this.toastrService.error(res?.message);
            }
            this.isLoadingPage = false;
          },
          error: (err) => {
            this.toastrService.error(err?.message);
            this.isLoadingPage = false;
          }
        })
      )
    }
  }

  processSendMailResponse(data: SendMailContractResponse) {
    const arrEmpSuccess = data?.listEmpSuccess ? data?.listEmpSuccess.map(emp => emp.employeeCode + "-" + emp.fullName).join(", ") : '';
    const arrEmpFail = data?.listEmpFail ? data?.listEmpFail.map(emp => emp.employeeCode + "-" + emp.fullName).join(", ") : '';
    const arrEmpSentBefore =  data?.listEmpSentBefore ? data?.listEmpSentBefore.map(emp => emp.employeeCode + "-" + emp.fullName).join(", ") : '';

    if (data?.listEmpSuccess && data?.listEmpFail && data?.listEmpSentBefore) {
      this.toastrService.success(this.translateService.instant("partnership.pns-contract.notification.sendMailSuccessAndFailAndBefore", {arrEmpSuccess: arrEmpSuccess, arrEmpFail: arrEmpFail, arrEmpSentBefore: arrEmpSentBefore}), '', {enableHtml: true});
    } else if (data?.listEmpFail && data?.listEmpSentBefore) {
      this.toastrService.success(this.translateService.instant("partnership.pns-contract.notification.sendMailFailAndBefore", {arrEmpFail: arrEmpFail, arrEmpSentBefore: arrEmpSentBefore}), '', {enableHtml: true});
    } else if (data?.listEmpSuccess && data?.listEmpSentBefore) {
      this.toastrService.success(this.translateService.instant("partnership.pns-contract.notification.sendMailSuccessAndBefore", {arrEmpSuccess: arrEmpSuccess, arrEmpSentBefore: arrEmpSentBefore}), '', {enableHtml: true});
    } else if (data?.listEmpSuccess && data?.listEmpFail) {
      this.toastrService.success(this.translateService.instant("partnership.pns-contract.notification.sendMailSuccessAndFail", {arrEmpSuccess: arrEmpSuccess, arrEmpFail: arrEmpFail}), '', {enableHtml: true});
    } else if (data?.listEmpSentBefore) {
      this.toastrService.error(this.translateService.instant("partnership.pns-contract.notification.sendMailBefore", {arrEmpSentBefore: arrEmpSentBefore}));
    } else if (data?.listEmpFail) {
      this.toastrService.error(this.translateService.instant("partnership.pns-contract.notification.sendMailFail", {arrEmpFail: arrEmpFail}));
    } else {
      this.toastrService.success(this.translateService.instant("partnership.pns-contract.notification.sendMailSuccess", {arrEmpSuccess: arrEmpSuccess}));
    }
  }

  ngOnDestroy(): void {
    this.subscriptions?.forEach(sub => sub.unsubscribe());
  }

}
