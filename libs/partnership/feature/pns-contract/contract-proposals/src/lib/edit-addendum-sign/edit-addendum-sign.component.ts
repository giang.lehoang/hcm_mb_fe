import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Subscription } from 'rxjs';
import * as moment from 'moment';
import { HttpParams } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { CatalogModel } from '@hcm-mfe/shared/data-access/models';
import {
  ContractProposalService,
  PersonalInfoService,
  SearchFormService
} from "@hcm-mfe/partnership/data-access/pns-contract/services";
import {Constant, UrlConstant} from "@hcm-mfe/partnership/data-access/pns-contract/common";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {ContractProposal, ContractType} from "@hcm-mfe/partnership/data-access/pns-contract/models";
import {NzSafeAny} from "ng-zorro-antd/core/types";

@Component({
  selector: 'app-edit-addendum-sign',
  templateUrl: './edit-addendum-sign.component.html',
  styleUrls: ['./edit-addendum-sign.component.scss']
})
export class EditAddendumSignComponent implements OnInit {

  employeeId!: number;
  contractProposalId!: number;
  form!: FormGroup;
  listContractType: CatalogModel[] = [];
  listContractOffer: CatalogModel[] = [];
  listPosition: CatalogModel[] = [];
  listAssessments: CatalogModel[] = [];
  listAddendumContract: CatalogModel[] = [];
  subscriptions: Subscription[] = [];
  isDetail = false;
  isSubmitted = false;
  isLoadingPage = false;

  constructor(
    private contractProposalService: ContractProposalService,
    private toastrService: ToastrService,
    private fb: FormBuilder,
    private searchFormService: SearchFormService,
    private personalInfoService: PersonalInfoService,
    private translate: TranslateService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    this.getListContractType();
    this.getListPosition();
    this.getListAddendumContract();
    this.initDataSelect();
    this.initFormGroup();
    this.getUrlInfo();
  }

  initFormGroup() {
    this.form = this.fb.group({
      contractNumber: null,
      curContractNumber: null,
      amountSalary: null,
      curContractTypeId: null,
      contractTypeId: null,
      fromDate: null,
      curFromDate: null,
      toDate: null,
      curToDate: null,
      signer: [null, Validators.required],
      signerPosition: null,
      delegacyNo: null
    });
  }

  get f() {
    return this.form.controls;
  }

  getUrlInfo() {
    this.contractProposalId = this.activatedRoute.snapshot.queryParams['id'];
    this.employeeId = this.activatedRoute.snapshot.queryParams['empId'];
    if (this.router.url.includes(UrlConstant.NAVIGATE_URL.DETAIL_ADDENDUM_SIGN)) {
      this.isDetail = true;
    }
    if (this.contractProposalId) {
      this.pathFormGroup().then();
    }
  }

  async pathFormGroup() {
    const res = await this.contractProposalService.getById(this.contractProposalId).toPromise();
    if (res.code === HTTP_STATUS_CODE.OK) {
      const data = res.data;
      this.form.patchValue(data);
      this.f['fromDate'].setValue(data.fromDate ? moment(data.fromDate, 'DD/MM/YYYY').toDate() : null);
      this.f['toDate'].setValue(data.toDate ? moment(data.toDate, 'DD/MM/YYYY').toDate() : null);
      this.f['curFromDate'].setValue(data.curFromDate ? moment(data.curFromDate, 'DD/MM/YYYY').toDate() : null);
      this.f['curToDate'].setValue(data.curToDate ? moment(data.curToDate, 'DD/MM/YYYY').toDate() : null);
      if (data.amountSalary) {
        this.f['amountSalary'].setValue(new Intl.NumberFormat('de-DE').format(data.amountSalary));
      }
      this.getSignerInfo(data.signerId);
    }
  }

  getSignerInfo(signerId: number) {
    if (signerId) {
      this.subscriptions.push(
        this.personalInfoService.getPersonalInfo(signerId).subscribe(res => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.f['signer'].setValue(res.data);
          }
        }, error => this.toastrService.error(error.message))
      );
    }
  }

  getListContractType() {
    const params = new HttpParams().set('classifyCode', Constant.CLASSIFY_CODE.HOP_DONG);
    this.subscriptions.push(
      this.searchFormService.getContractType(UrlConstant.CATALOGS.CONTRACT_TYPE, params).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listContractType = res.data.map((item: ContractType) => {
            item.label = item.name;
            item.value = item.contractTypeId;
            return item;
          });
        }
      }, error => {
        this.toastrService.error(error.message);
        this.listContractType = [];
      })
    );
  }

  getListAddendumContract() {
    const params = new HttpParams().set('classifyCode', Constant.CLASSIFY_CODE.PHU_HOP_DONG);
    this.subscriptions.push(
      this.searchFormService.getContractType(UrlConstant.CATALOGS.CONTRACT_TYPE, params).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listAddendumContract = res.data.map((item: NzSafeAny) => {
            item.label = item.name;
            item.value = item.contractTypeId;
            return item;
          });
        }
      }, error => {
        this.toastrService.error(error.message);
        this.listContractType = [];
      })
    );
  }

  initDataSelect() {
    this.listContractOffer = Constant.CONTRACT_OFFER.map(item => {
      item.label = this.translate.instant(item.label);
      return item;
    });

    this.listAssessments = Constant.ASSESSMENTS.map(item => {
      item.label = this.translate.instant(item.label);
      return item;
    });
  }

  getListPosition() {
    const params = new HttpParams().set('jobType', Constant.JOB_TYPE.ORG);
    this.subscriptions.push(
      this.searchFormService.getListValue(UrlConstant.CATEGORY.MP_JOB, params).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listPosition = res.data;
        } else {
          this.toastrService.error(res.message);
        }
      }, error => {
        this.toastrService.error(error.message);
        this.listPosition = [];
      })
    );
  }

  onSave() {
    this.isSubmitted = true;
    this.validateSigner();
    if (this.form.valid) {
      this.isLoadingPage = true;
      const request = this.getRequestSave();
      this.subscriptions.push(
        this.contractProposalService.saveContractProposal(request).subscribe(res => {
          if (res && res.code === HTTP_STATUS_CODE.OK) {
            this.toastrService.success(this.translate.instant('common.notification.updateSuccess'));
            this.onCancel();
          } else {
            this.toastrService.error(this.translate.instant('common.notification.updateError') + ': ' + res?.message);
          }
          this.isLoadingPage = false;
        }, error => {
            this.toastrService.error(error.message);
            this.isLoadingPage = false;
        })
      );
    }
  }

  validateSigner() {
    if (this.f['signer'].value?.employeeId === ~~this.employeeId) {
      this.form.setErrors({errDataPicker: true});
      this.toastrService.error(this.translate.instant(Constant.DATA_PICKER_ERROR[0].label));
    } else {
      this.form.setErrors(null);
    }
  }

  getRequestSave(): ContractProposal {
    const request: ContractProposal = this.form.value;
    request.contractProposalId = this.contractProposalId;
    request.signerId = this.f['signer'].value?.employeeId;

    return request;
  }

  onCancel() {
    localStorage.setItem("isBackUrl", "true");
    this.router.navigateByUrl(UrlConstant.NAVIGATE_URL.SEARCH).then();
  }

  ngOnDestroy(): void {
    this.subscriptions?.forEach(sub => sub.unsubscribe());
  }

}
