import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpParams } from '@angular/common/http';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import {ContractProposal, ContractType} from "@hcm-mfe/partnership/data-access/pns-contract/models";
import { CatalogModel } from "@hcm-mfe/partnership/ui/pns-contract/search-form";
import { ContractProposalService, PersonalInfoService, SearchFormService } from '@hcm-mfe/partnership/data-access/pns-contract/services';
import {Constant, UrlConstant } from '@hcm-mfe/partnership/data-access/pns-contract/common';
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import { SelectModal } from '@hcm-mfe/shared/data-access/models';

@Component({
  selector: 'app-edit-new-sign',
  templateUrl: './edit-new-sign.component.html',
  styleUrls: ['./edit-new-sign.component.scss']
})
export class EditNewSignComponent implements OnInit {
  employeeId!: number;
  contractProposalId!: number;
  form!: FormGroup;
  listContractType: ContractType[] = [];
  listPosition: CatalogModel[] = [];
  subscriptions: Subscription[] = [];
  isDetail = false;
  isSubmitted = false;
  isLoadingPage = false;

  constructor(
    private contractProposalService: ContractProposalService,
    private toastrService: ToastrService,
    private fb: FormBuilder,
    private searchFormService: SearchFormService,
    private personalInfoService: PersonalInfoService,
    private translate: TranslateService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    this.getListContractType();
    this.getListPosition();
    this.initFormGroup();
    this.getUrlInfo();
  }

  getUrlInfo() {
    this.contractProposalId = this.activatedRoute.snapshot.queryParams['id'];
    this.employeeId = this.activatedRoute.snapshot.queryParams['empId'];
    if (this.router.url.includes(UrlConstant.NAVIGATE_URL.DETAIL_NEW_SIGN)) {
      this.isDetail = true;
    }
    if (this.contractProposalId) {
      this.pathFormGroup().then();
    }
  }

  initFormGroup() {
    this.form = this.fb.group({
      contractNumber: null,
      amountSalary: null,
      salaryPercent: null,
      contractTypeId: [null, [Validators.required]],
      fromDate: null,
      toDate: null,
      signer: [null, [Validators.required]],
      signerPosition: null,
      delegacyNo: null
    });
  }

  get f() {
    return this.form.controls;
  }

  async pathFormGroup() {
    const res = await this.contractProposalService.getById(this.contractProposalId).toPromise();
    if (res.code === HTTP_STATUS_CODE.OK) {
      const data = res.data;
      this.form.patchValue(data);
      this.f['fromDate'].setValue(data.fromDate ? moment(data.fromDate, 'DD/MM/YYYY').toDate() : null);
      this.f['toDate'].setValue(data.toDate ? moment(data.toDate, 'DD/MM/YYYY').toDate() : null);
      if (data.amountSalary) {
        this.f['amountSalary'].setValue(new Intl.NumberFormat('de-DE').format(data.amountSalary));
      }
      this.getSignerInfo(data.signerId);
    }
  }

  getSignerInfo(signerId: number) {
    if (signerId) {
      this.subscriptions.push(
        this.personalInfoService.getPersonalInfo(signerId).subscribe(res => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.f['signer'].setValue(res.data);
          }
        }, error => this.toastrService.error(error.message))
      );
    }
  }

  getListContractType() {
    const params = new HttpParams().set('classifyCode', Constant.CLASSIFY_CODE.HOP_DONG);
    this.subscriptions.push(
      this.searchFormService.getContractType(UrlConstant.CATALOGS.CONTRACT_TYPE, params).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listContractType = res.data.map((item:ContractType) => {
            item.label = item.name;
            item.value = item.contractTypeId;
            return item;
          });
        }
      }, error => {
        this.toastrService.error(error.message);
        this.listContractType = [];
      })
    );
  }

  getListPosition() {
    const params = new HttpParams().set('jobType', Constant.JOB_TYPE.ORG);
    this.subscriptions.push(
      this.searchFormService.getListValue(UrlConstant.CATEGORY.MP_JOB, params).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listPosition = res.data;
        } else {
          this.toastrService.error(res.message);
        }
      }, error => {
        this.toastrService.error(error.message);
        this.listPosition = [];
      })
    );
  }

  selectFromDate(event: Date) {
    const durationContract = this.listContractType.find(item => item.contractTypeId === this.f['contractTypeId'].value)?.duration;
    if (event && durationContract) {
      const toDate = new Date(moment(event).format('MM/DD/YYYY'));
      toDate.setMonth(toDate.getMonth() + durationContract);
      this.f['toDate'].setValue(durationContract ? toDate : null);
    } else {
      this.f['toDate'].setValue(null);
    }
  }

  selectContractType(event: SelectModal) {
    const data = event.itemSelected;
    if (data && data.duration) {
      const toDate = new Date(moment(this.f['fromDate'].value).format('MM/DD/YYYY'));
      toDate.setMonth(toDate.getMonth() + data.duration);
      toDate.setDate(toDate.getDate() - 1);
      this.f['toDate'].setValue(toDate);
    } else {
      this.f['toDate'].setValue(null);
    }
  }

  onSave() {
    this.isSubmitted = true;
    this.validateSigner();
    if (this.form.valid) {
      this.isLoadingPage = true;
      const request: ContractProposal = this.getRequestSave();
      this.subscriptions.push(
        this.contractProposalService.saveContractProposal(request).subscribe(res => {
          if (res && res.code === HTTP_STATUS_CODE.OK) {
            this.toastrService.success(this.translate.instant('common.notification.updateSuccess'));
            this.isLoadingPage = false;
            this.onCancel();
          } else {
            this.toastrService.error(this.translate.instant('common.notification.updateError') + ': ' + res?.message);
          }
          this.isLoadingPage = false;
        }, error => {
          this.toastrService.error(error.message);
          this.isLoadingPage = false;
        })
      );
    }
  }

  validateSigner() {
    if (this.f['signer'].value?.employeeId === ~~this.employeeId) {
      this.form.setErrors({errDataPicker: true});
      this.toastrService.error(this.translate.instant(Constant.DATA_PICKER_ERROR[0].label));
    } else {
      this.form.setErrors(null);
    }
  }

  getRequestSave(): ContractProposal {
    const request: ContractProposal = this.form.value;
    request.contractProposalId = this.contractProposalId;
    request.signerId = this.f['signer'].value?.employeeId;

    return request;
  }

  onCancel() {
    localStorage.setItem("isBackUrl", "true");
    this.router.navigateByUrl(UrlConstant.NAVIGATE_URL.SEARCH).then();
  }

  ngOnDestroy(): void {
    this.subscriptions?.forEach(sub => sub.unsubscribe());
  }

}
