import { Component, Injector, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { NzModalService } from 'ng-zorro-antd/modal';
import { saveAs } from 'file-saver';
import { CatalogModel } from '@hcm-mfe/partnership/ui/pns-contract/search-form';
import {getTypeExport, Utils} from '@hcm-mfe/shared/common/utils';
import {ConfigApprovalService} from "@hcm-mfe/partnership/data-access/pns-contract/services";
import {FunctionCode, SearchBaseComponent} from "@hcm-mfe/partnership/data-access/pns-contract/common";
import {Scopes} from "@hcm-mfe/shared/common/enums";
import { Constant, UrlConstant } from '@hcm-mfe/partnership/data-access/pns-contract/common';
import { TranslateService } from '@ngx-translate/core';
import {ToastrService} from "ngx-toastr";
import { ConfigApprovalsFormComponent } from '../config-approvals-form/config-approvals-form.component';
import {AppFunction} from "@hcm-mfe/shared/data-access/models";
import {SessionService} from "@hcm-mfe/shared/common/store";


@Component({
    selector: 'config-approvals',
    templateUrl: './config-approvals.component.html',
    styleUrls: ['./config-approvals.component.scss']
})
export class ConfigApprovalsComponent extends SearchBaseComponent implements OnInit {
    @Input() scope: string = Scopes.VIEW;

    typeSelects: CatalogModel[] = [];

    orgGroupSelects: CatalogModel[] = [];
    functionCode = FunctionCode.PNS_CONFIG_APPROVAL;
    objFunction!: AppFunction;

    formSearchConfig = {
        type: [null],
        documentNo: [null],
        org: [null],
        orgGroup: [null],
        fromDate: [Utils.getFirstDayOrLastDayOfCurrentMonth(true)],
        toDate: [Utils.getFirstDayOrLastDayOfCurrentMonth(false)],
    }

    isSubmitted = false;
    @ViewChild('actionTmpl', { static: true }) action!: TemplateRef<NzSafeAny>;
    @ViewChild('scopeTmpl', { static: true }) scopeTmpl!: TemplateRef<NzSafeAny>;


  constructor(
        injector: Injector,
        private configApprovalService: ConfigApprovalService,
        private modalServices: NzModalService,
        public override translate: TranslateService,
        public override toastrService: ToastrService,
        private sessionService: SessionService
    ) {
        super(injector);
      this.toastrService = toastrService;
      this.buildFormSearch(this.formSearchConfig);
      this.setMainService(this.configApprovalService);
      this.objFunction = this.sessionService.getSessionData(`FUNCTION_${this.functionCode}`);
  }

    ngOnInit(): void {
        this.initDataSelect();
        this.initTable();
        this.doSearch();
    }

    initDataSelect() {
      this.orgGroupSelects = Constant.ORG_GROUP_SELECTS.map(item => {
        item.label = this.translate.instant(item.label);
        return item;
      })

      this.typeSelects = Constant.TYPE_SELECTS.map(item => {
        item.label = this.translate.instant(item.label);
        return item;
      })
    }

    showModal(footerTmpl: TemplateRef<NzSafeAny>, configApprovalId?: number ) {
        this.modal = this.modalServices.create({
            nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
            nzTitle: this.translate.instant('partnership.pns-contract.label.templateParams'),
            nzContent: ConfigApprovalsFormComponent,
            nzComponentParams: {
                configApprovalId: configApprovalId,
            },
            nzFooter: footerTmpl
        });
        this.subs.push(
            this.modal.afterClose.subscribe(res => {
                if (res && res.refresh) {
                    this.doSearch();
                }
            })
        );
    }

    initTable(): void {
        this.tableConfig = {
            headers: [
                {
                    title: 'partnership.pns-contract.label.configType',
                    field: 'typeStr',
                    width: 120,
                    thClassList: ['text-center'],
                    fixedDir: 'left'
                },
                {
                    title: 'partnership.pns-contract.label.orgOrScope',
                    field: 'orgName',
                    width: 300,
                    thClassList: ['text-center'],
                    fixedDir: 'left'
                },
                {
                    title: 'partnership.pns-contract.label.groupTitle',
                    field: 'empPgrName',
                    tdClassList: ['text-left'],
                    thClassList: ['text-center'],
                    width: 200
                },
                {
                    title: 'partnership.pns-contract.label.signer',
                    field: 'approverName',
                    tdClassList: ['text-left'],
                    thClassList: ['text-center'],
                    width: 200
                },
                {
                    title: 'partnership.pns-contract.label.jobSign',
                    field: 'jobApproverName',
                    tdClassList: ['text-left'],
                    thClassList: ['text-center'],
                    width: 200
                },
                {
                    title: 'partnership.pns-contract.label.documentNo',
                    field: 'documentNo',
                    tdClassList: ['text-left'],
                    thClassList: ['text-center'],
                    width: 200
                },
                {
                    title: 'partnership.label.fromDate',
                    field: 'fromDate',
                    tdClassList: ['text-center'],
                    thClassList: ['text-center'],
                    width: 100,
                },
                {
                  title: 'partnership.label.toDate',
                  field: 'toDate',
                  tdClassList: ['text-center'],
                  thClassList: ['text-center'],
                  width: 100,
                },
                {
                    title: ' ',
                    field: 'action',
                    tdClassList: ['text-nowrap', 'text-center'],
                    thClassList: ['text-nowrap', 'text-center'],
                    width: 60,
                    tdTemplate: this.action,
                    fixed: window.innerWidth > 1024,
                    fixedDir: 'right'
                }
            ],
            total: 0,
            needScroll: true,
            loading: false,
            size: 'small',
            pageSize: this.pagination.pageSize,
            pageIndex: 1
        };
    }

    doExportData() {
        this.isLoadingPage = true;
        const params = this.formSearch ? {...this.formSearch.value} : {};
        this.subs.push(
            this.configApprovalService.doExportFile(UrlConstant.EXPORT_REPORT.CONFIG_APPROVAL, params).subscribe(res => {
                if (res?.headers?.get('Excel-File-Empty')) {
                    this.toastrService.error(this.translate.instant("common.notification.exportFail"));
                    return;
                } else {
                    const reportFile = new Blob([res.body], {type: getTypeExport('xlsx')});
                    saveAs(reportFile, 'DS_CauHinhThongTinNguoiKyNguoiPheDuyet.xlsx');
                }
                this.isLoadingPage = false;
            }, error => {
                this.toastrService.error(error.message);
                this.isLoadingPage = false;
            })
        );
    }

    override ngOnDestroy(): void {
        this.subs?.forEach(sub => sub.unsubscribe());
        this.modal?.destroy();
    }

}

