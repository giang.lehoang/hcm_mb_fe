import { HttpParams } from '@angular/common/http';
import { Component, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { Scopes } from '@hcm-mfe/shared/common/enums';
import {CatalogModel} from "@hcm-mfe/shared/data-access/models";
import {ConfigApprovalService, SearchFormService} from '@hcm-mfe/partnership/data-access/pns-contract/services';
import {HTTP_STATUS_CODE, STATUS} from '@hcm-mfe/shared/common/constants';
import { CustomValidators } from '@hcm-mfe/shared/common/validators';
import {CommonUtils} from "@hcm-mfe/shared/core";
import {Constant, FunctionCode, UrlConstant} from "@hcm-mfe/partnership/data-access/pns-contract/common";

@Component({
    selector: 'config-approvals-form',
    templateUrl: './config-approvals-form.component.html',
    styleUrls: ['./config-approvals-form.component.scss']
})
export class ConfigApprovalsFormComponent implements OnInit {

    @Input() scope: string = Scopes.VIEW;
    @Input() functionCode = FunctionCode.PNS_CONFIG_APPROVAL;
    @ViewChild('nzUpload', { static: true }) nzUpload!: TemplateRef<NzSafeAny>;
    @ViewChild('actionTmpl', { static: true }) action!: TemplateRef<NzSafeAny>;
    form!: FormGroup;
    subs: Subscription[] = [];
    isSubmitted = false;
    typeSelects: CatalogModel[] = [];
    scopeSelects: CatalogModel[] = [];
    branchTypeSelects: CatalogModel[] = [];
    orgGroupSelects: CatalogModel[] = [];
    listPosition = [];
    fileList: NzUploadFile[] = [];
    data = [1];
    positionGroupList: CatalogModel[] = [];
    configApprovalId!: number;
    isLoadingPage = false;


    constructor(
        private fb: FormBuilder,
        public toastr: ToastrService,
        public translate: TranslateService,
        private searchFormService: SearchFormService,
        private configApprovalService: ConfigApprovalService,
        private modalRef: NzModalRef
    ) {
    }
    ngOnInit(): void {
        this.initDataSelect();
        this.form = this.fb.group({
            configApprovalId: [null],
            type: [null, [Validators.required]],
            documentNo: [null, [Validators.required, Validators.maxLength(255)]],
            jobApproverId: [null, [Validators.required]],
            approverId: [null],
            fromDate: [null, [Validators.required]],
            toDate: [null],
            org: [null, [Validators.required]],
            note: [null],
            fileTemplate: [null],
            scope: [1],
            branchType:[null],
            empPgrId: null
        }, {
            validator: [
                CustomValidators.toDateAfterFromDate('fromDate', 'toDate'),
            ]
        });
        this.getPositionGroupList();
        this.getListPosition()
      this.form.controls['type'].setValue(1);
        if (this.configApprovalId) {
            this.subs.push(
                this.configApprovalService.getById(this.configApprovalId).subscribe(res => {
                    if (res.code === HTTP_STATUS_CODE.OK) {
                        this.onChangeType(res.data.type);
                        res.data.fromDate = moment(res.data.fromDate, 'DD/MM/YYYY').toDate();
                        if (res.data.jobApproverId){
                            res.data.jobApproverId = '' + res.data.jobApproverId;
                        }
                        if (res.data.approverId){
                            res.data.approverId = res.data.approver;
                        }
                        if (res.data.toDate){
                            res.data.toDate =  moment(res.data.toDate, 'DD/MM/YYYY').toDate();
                        }
                        if( res.data.empPgrId){
                            res.data.empPgrId = '' + res.data.empPgrId;
                        }
                        if (res.data.file){
                            const file = new File([], res.data.file.fileName);
                            this.fileList = this.fileList.concat(file as NzSafeAny);
                            res.data.file = file;
                        }
                        if(res.data.orgGroup){
                          this.form.controls['scope'].setValue(2)
                            res.data.org = res.data.orgGroup
                        }
                        CommonUtils.setFormValue(this.form, res.data);
                    }
                }, error => this.toastr.error(error.message))
            );

        }
    }

    initDataSelect() {
      this.orgGroupSelects = Constant.ORG_GROUP_SELECTS.map(item => {
        item.label = this.translate.instant(item.label);
        return item;
      })

      this.scopeSelects = Constant.SCOPE_SELECTS.map(item => {
        item.label = this.translate.instant(item.label);
        return item;
      })

      this.typeSelects = Constant.TYPE_SELECTS.map(item => {
        item.label = this.translate.instant(item.label);
        return item;
      })

      this.getListBranchTypes();
    }

    beforeUpload = (file: NzUploadFile): boolean => {
      this.fileList = [];
      this.form.controls['fileTemplate'].setValue(file);
      this.fileList.push(file as NzSafeAny);
      return false;
    };

    removeFile = (file: NzUploadFile): boolean => {
      this.form.controls['fileTemplate'].setValue(null);
        return true;
    }

    setFormValue(event: NzSafeAny, formName: string) {
        if (event.listOfSelected.length > 0) {
            this.form.controls[formName].setValue(event.listOfSelected);
        }
    }

    getPositionGroupList() {
        this.subs.push(
            this.configApprovalService.getPositionGroupList().subscribe(data => {
                if (data.code === HTTP_STATUS_CODE.OK) {
                    this.positionGroupList = data.data
                        .filter((item: NzSafeAny) => item.flagStatus === STATUS.ACTIVE)
                        .map((item: NzSafeAny) => { return { value: '' + item.pgrId, label: item.pgrName } });
                }
            }, (error) => {
                this.toastr.error(error.message)
            })
        );
    }

    getListPosition() {
        const params = new HttpParams().set('jobType', Constant.JOB_TYPE.ORG);
        this.subs.push(
            this.searchFormService.getListValue(UrlConstant.CATEGORY.MP_JOB, params).subscribe(res => {
                if (res.code === HTTP_STATUS_CODE.OK) {
                    this.listPosition = res.data;
                }
            }, error => {
                this.toastr.error(error.message);
            })
        );
    }

    onChangeType(typeValue: number) {
        if (typeValue === 1) {
            this.form.removeControl("empPgrId");
            this.form.controls['jobApproverId'].setValidators([Validators.required]);
        } else {
            this.form.controls['jobApproverId'].setValidators(null);
            if (!this.form.controls['empPgrId']) {
                this.form.setControl("empPgrId", new FormControl(null))
            }
        }
        this.form.controls['jobApproverId'].updateValueAndValidity();
    }

    save() {
        this.isSubmitted = true;
        if (this.form.valid) {
            if (this.form.controls['type'].value !== 1 && !this.form.controls['jobApproverId'].value && !this.form.controls['approverId'].value) {
                const messageError = this.form.controls['type'].value === 2 ? 'partnership.notification.configApprovalError' : 'partnership.notification.configApprovalEditError'
                this.toastr.error(this.translate.instant(messageError));
                return;
            }
            this.isLoadingPage = true;
            this.subs.push(
                this.configApprovalService.saveData(this.buildFormData()).subscribe(res => {
                    if (res.code === HTTP_STATUS_CODE.OK) {
                        this.toastr.success(this.translate.instant('common.notification.updateSuccess'));
                        this.modalRef.close({refresh:true});
                    } else if (res.code === HTTP_STATUS_CODE.CREATED){
                        this.toastr.error(res.message);
                    }
                    this.isLoadingPage = false;
                }, error => {
                    this.toastr.error(error.message);
                    this.isLoadingPage = false;
                })
            );
        }

    }

    buildFormData(): FormData {
        const param = { ...this.form.value };
        const fromData = new FormData();
        fromData.append("file", param.fileTemplate);
        if (param.scope==1){
            param.organizationId = param.org.orgId;
        }else{
            param.orgGroup = param.org;
        }
        delete param.org;
        param.fromDate = moment(param.fromDate).format("DD/MM/YYYY")
        if (param.toDate) {
            param.toDate = moment(param.toDate).format("DD/MM/YYYY")
        }
        if (param.approverId){
            param.approverId = param.approverId.employeeId;
        }
        fromData.append("data", new Blob([JSON.stringify(param)], { type: 'application/json' }));
        return fromData;
    }

    onChangeScope(){
      this.form.controls['org'].setValue(null);
    }

    ngOnDestroy(): void {
        this.subs?.forEach(sub => sub.unsubscribe());
    }

    getListBranchTypes() {
        const params = new HttpParams().set('typeCode', Constant.TYPE_CODE.LOAI_HINH_CHI_NHANH);
        this.searchFormService.getListValue(UrlConstant.CATEGORY.LOOKUP_VALUE, params).subscribe(res => {
            if (res.code === HTTP_STATUS_CODE.OK) {
                this.branchTypeSelects = res.data;
            }
        });
      }

}
