import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {TranslateModule} from "@ngx-translate/core";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {NzIconModule} from "ng-zorro-antd/icon";
import {SharedUiMbTableWrapModule} from "@hcm-mfe/shared/ui/mb-table-wrap";
import {SharedUiMbTableModule} from "@hcm-mfe/shared/ui/mb-table";
import {NzTagModule} from "ng-zorro-antd/tag";
import {PartnershipUiSearchFormModule} from "@hcm-mfe/partnership/ui/pns-contract/search-form";
import {RouterModule} from "@angular/router";
import {SharedUiMbTableMergeCellWrapModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell-wrap";
import {SharedUiMbTableMergeCellModule} from "@hcm-mfe/shared/ui/mb-table-merge-cell";
import {PartnershipUiImportCommonModule} from "@hcm-mfe/partnership/ui/pns-contract/import-common";
import {ConfigApprovalsComponent} from "./config-approvals/config-approvals.component";
import {ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiOrgDataPickerModule} from "@hcm-mfe/shared/ui/org-data-picker";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {SharedUiMbEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/mb-employee-data-picker";
import {PartnershipUiPersonalInfoModule} from "@hcm-mfe/partnership/ui/pns-contract/personal-info";
import {SharedUiMbTextLabelModule} from "@hcm-mfe/shared/ui/mb-text-label";
import {ConfigApprovalsFormComponent} from "./config-approvals-form/config-approvals-form.component";
import {NzFormModule} from "ng-zorro-antd/form";
import {NzPopconfirmModule} from "ng-zorro-antd/popconfirm";
import {NzUploadModule} from "ng-zorro-antd/upload";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, SharedUiMbButtonModule,
    TranslateModule, NzButtonModule, NzDropDownModule, NzIconModule, SharedUiMbTableWrapModule, SharedUiMbTableModule, NzTagModule, PartnershipUiSearchFormModule,
    RouterModule.forChild([
      {
        path: '',
        component: ConfigApprovalsComponent
      }
    ]), SharedUiMbTableMergeCellWrapModule, SharedUiMbTableMergeCellModule, PartnershipUiImportCommonModule, ReactiveFormsModule, SharedUiMbSelectModule,
    SharedUiOrgDataPickerModule, SharedUiMbInputTextModule, SharedUiMbDatePickerModule, SharedUiMbEmployeeDataPickerModule, PartnershipUiPersonalInfoModule,
    SharedUiMbTextLabelModule, NzFormModule, NzPopconfirmModule, NzUploadModule, NzToolTipModule],
  declarations: [ConfigApprovalsComponent, ConfigApprovalsFormComponent],
  exports: [ConfigApprovalsComponent]
})
export class PartnershipFeatureConfigApprovalsModule {}
