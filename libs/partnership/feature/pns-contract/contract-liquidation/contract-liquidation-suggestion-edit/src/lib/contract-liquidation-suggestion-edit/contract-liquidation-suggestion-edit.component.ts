import {Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {Subscription} from "rxjs";
import { HTTP_STATUS_CODE, MICRO_SERVICE, Mode, Scopes } from '@hcm-mfe/shared/common/constants';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import {NzModalRef, NzModalService} from "ng-zorro-antd/modal";
import {
  ConsultationInfo,
  FileDocument,
  LiquidationDTO,
  LiquidationResponse
} from '@hcm-mfe/partnership/data-access/pns-contract/models';
import {
  ContractLiquidationService,
  PersonalInfoService,
  SearchFormService
} from '@hcm-mfe/partnership/data-access/pns-contract/services';
import {AppFunction, BaseResponse} from "@hcm-mfe/shared/data-access/models";
import {ToastrService} from "ngx-toastr";
import {Utils} from "@hcm-mfe/shared/common/utils";
import {TranslateService} from "@ngx-translate/core";
import {NzUploadFile} from "ng-zorro-antd/upload";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import { CommonUtils } from '@hcm-mfe/shared/core';
import { HttpParams } from '@angular/common/http';
import {Constant, FunctionCode, UrlConstant} from '@hcm-mfe/partnership/data-access/pns-contract/common';
import { CatalogModel } from '@hcm-mfe/partnership/ui/pns-contract/search-form';
import {SessionService} from "@hcm-mfe/shared/common/store";
import {DateValidator} from "@hcm-mfe/shared/common/validators";
import * as moment from "moment";

export type Catalog = {
  value?: number | string;
  label: string;
  parentValue: string;
}

@Component({
    selector: 'contract-liquidation-suggestion-edit',
    templateUrl: './contract-liquidation-suggestion-edit.component.html',
    styleUrls: ['./contract-liquidation-suggestion-edit.component.scss']
})
export class ContractLiquidationSuggestionEditComponent implements OnInit, OnDestroy {
    mode?: Mode;
  isCompleteProfile = false;
    Mode = Mode;
    form!: FormGroup;
    constant = Constant;
    readonly serviceName: string = MICRO_SERVICE.CONTRACT;
    scope=Scopes.CREATE;
    functionCode=FunctionCode.PNS_LIQUIDATIONS;
    isConsultationsRequest = false;
    liquidationId?: number;
    contractLiquidationEdit?: LiquidationResponse;

    listDocumentType: Catalog[] = [];
    listConsultationInfoType: Catalog[] = [];
    listRetireReason: CatalogModel[] = []
    listConsultationsRequest: boolean[] = [];

    isSubmitted = false;
    subs: Subscription[] = [];
    objFunction: AppFunction;
    isLoadingPage = false;

    @ViewChild('statusTmpl', { static: true }) status!: TemplateRef<NzSafeAny>;

    constructor(
        private fb: FormBuilder,
        private modalService: NzModalService,
        private toastService: ToastrService,
        private translateService: TranslateService,
        private contractLiquidationService: ContractLiquidationService,
        private personalInfoService: PersonalInfoService,
        private modalRef: NzModalRef,
        private searchFormService: SearchFormService,
        private sessionService: SessionService
    ) {
      this.initForm();
      this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.PNS_LIQUIDATIONS}`);
    }

    ngOnInit(): void {
        this.addConsultationInfo();
        this.addFileDocument();
        this.getListDocument();
        this.getConsultationInfo();
        this.getListRetireReason();
        if (this.liquidationId) {
            this.getModelEdit();
        }
    }

    getModelEdit() {
        if (this.liquidationId) {
            this.subs.push(
                this.contractLiquidationService.getById(this.liquidationId).subscribe({
                    next: (res: BaseResponse) => {
                        if (res.code === HTTP_STATUS_CODE.OK) {
                            this.contractLiquidationEdit = res?.data;
                            this.getEmployee();
                            this.patchValueToForm();
                        } else {
                            this.toastService.error(res?.message);
                        }
                    },
                    error: (err) => {
                        this.toastService.error(err?.message);
                    }
                })
            );
        }
    }

    getEmployee() {
        if (this.contractLiquidationEdit?.employeeId) {
            this.subs.push(
                this.personalInfoService.getPersonalInfo(this.contractLiquidationEdit.employeeId).subscribe({
                    next: (res: BaseResponse) => {
                        if (res.code === HTTP_STATUS_CODE.OK) {
                            this.form.controls['employee'].setValue(res?.data);
                        } else {
                            this.toastService.error(res?.message);
                        }
                    },
                    error: (err) => {
                        this.toastService.error(err?.message);
                    }
                })
            );
        }
    }

    patchValueToForm() {
        this.form.patchValue({
            notifyDay: this.contractLiquidationEdit?.notifyDays,
            lastWorkingDate: Utils.convertDateToFillForm(this.contractLiquidationEdit?.lastWorkingDate),
            retireReason: this.contractLiquidationEdit?.reason,
            reasonCode: this.contractLiquidationEdit?.reasonCode
        });
        this.contractLiquidationEdit?.listFileDocument?.forEach(el => {
          this.addFileDocument(el);
        });
      if(this.contractLiquidationEdit?.listFileDocument) {
        this.removeFileDocument(0);
      }
        this.contractLiquidationEdit?.listConsultationInfo?.forEach(el => {
          this.addConsultationInfo(el);
        });
      if (this.contractLiquidationEdit?.listConsultationInfo) {
        this.removeConsultationInfo(0);
      }
    }

    initForm() {
        this.form = this.fb.group({
            employee: [null, Validators.required],
            personalEmail: [null, Validators.email],
            notifyDay: [null],
            lastWorkingDate: [null],
      leaveDate: [null],
            retireReason: [null],
      reasonCode: [null, Validators.required],
            rejectReason: [null],
            listFileDocument: this.fb.array([]),
            listConsultationInfo: this.fb.array([])
    },{
      validators: [
        DateValidator.validateRangeDate( 'leaveDate', 'lastWorkingDate','rangeDateError')
      ]
        });
    }

    onSave() {
        this.isSubmitted = true;
        Utils.processTrimFormControlsBeforeSend(this.form, 'retireReason');
        if (this.form.valid) {
            this.isLoadingPage = true;
            const request: LiquidationDTO = {};
            this.setDataSendToServer(request);
            this.subs.push(
                this.contractLiquidationService.saveRecord(CommonUtils.convertFormFile(request)).subscribe({
                    next: (res: BaseResponse) => {
                        if (res.code === HTTP_STATUS_CODE.OK) {
                            this.toastService.success(this.translateService.instant("partnership.pns-contract.notification.saveSuccess"));
                        } else {
                            this.toastService.error(res?.message);
                        }
                        this.modalRef.close({refresh: true});
                        this.isLoadingPage = false;
                    },
                    error: (err) => {
                        this.toastService.error(err?.message);
                        this.isLoadingPage = false;
                    }
                })
            )
        }
  }

  onCompleteProfile() {
    const request: LiquidationDTO = {};
    this.setDataSendToServer(request);
    this.isLoadingPage = true;
    this.subs.push(
      this.contractLiquidationService.completeProfile(this.liquidationId, CommonUtils.convertFormFile(request)).subscribe(res => {
        this.toastService.success(this.translateService.instant("common.notification.success"));
        this.isLoadingPage = false;
        this.modalRef.close({refresh: true});
      }, e => {
        this.toastService.error(e?.message);
        this.isLoadingPage = false;
      })
    );
  }

    setDataSendToServer(request: LiquidationDTO) {
        request.liquidationId = this.liquidationId;
        request.employeeId = this.form.controls['employee'].value?.employeeId;
        request.personalEmail = this.form.controls['personalEmail'].value;
        request.notifyDays = this.form.controls['notifyDay'].value;
        request.lastWorkingDate = Utils.convertDateToSendServer(this.form.controls['lastWorkingDate'].value);
        request.reason = this.form.controls['retireReason'].value;
        request.reasonCode = this.form.controls['reasonCode'].value;
        request.rejectReason = this.form.controls['rejectReason'].value;
        request.listFileDocument = this.form.controls['listFileDocument'].value;
        request.listConsultationInfo = this.form.controls['listConsultationInfo'].value;
    }

    closeModalAdd() {
        this.modalService.closeAll();
    }

    ngOnDestroy(): void {
        this.subs?.forEach(sub => sub?.unsubscribe());
    }


  getListRetireReason() {
    const params = new HttpParams().set('typeCode', Constant.TYPE_CODE.LY_DO_NGHI);
    this.subs.push(
      this.searchFormService.getListValue(UrlConstant.CATEGORY.LOOKUP_VALUE, params).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listRetireReason = res.data;
        }
      }, error => {
        this.toastService.error(error.message);
      })
    );
  }

    getListDocument() {
      const params = new HttpParams().set('typeCode', Constant.TYPE_CODE.HO_SO_THANH_LY_HD);
      this.subs.push(
        this.searchFormService.getListValue(UrlConstant.CATEGORY.LOOKUP_VALUE, params).subscribe(res => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.listDocumentType = res.data;
            if(this.mode === Mode.ADD) {
              res.data.forEach((item: Catalog) => {
                if (item.parentValue) {
                  this.addFileDocument({type: item.value});
                }
              });
              if(res.data.some((item: any) => item.parentValue != null)) {
                this.removeFileDocument(0);
              }
            }
          }
        }, error => {
          this.toastService.error(error.message);
        })
      );
    }

    getConsultationInfo() {
      const params = new HttpParams().set('typeCode', Constant.TYPE_CODE.TT_THAM_VAN_THANH_LY_HD);
      this.subs.push(
        this.searchFormService.getListValue(UrlConstant.CATEGORY.LOOKUP_VALUE, params).subscribe(res => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.listConsultationInfoType = res.data;
            if(this.mode === Mode.ADD) {
              res.data.forEach((item: Catalog) => {
                if (item.parentValue) {
                  this.addConsultationInfo({type: item.value});
                }
              });
              if(res.data.some((item: any) => item.parentValue != null)) {
                this.removeConsultationInfo(0);
              }
            }
          }
        }, error => {
          this.toastService.error(error.message);
        })
      );
    }

    get listFileDocument(): NzSafeAny {
      return this.form.controls['listFileDocument'] as FormArray;
    }

  addFileDocument(item?: FileDocument) {
    item?.listDocuments?.forEach(el => {
      el.uid = el.docId;
      el.name = el.fileName;
    })
    if (this.listFileDocument.valid) {
      const fileDocument = this.fb.group({
        type: [item?.type ? item.type : null],
        fileAttachments: [null],
        listDocuments: [item?.listDocuments ? item?.listDocuments : null ],
        canNotDelete: [this.listDocumentType.some(el => el.value === item?.type && el.parentValue)],
        docIdsDelete: [],
      });
      this.listFileDocument.push(fileDocument);
    } else this.toastService.error(this.translateService.instant("partnership.pns-contract.label.emptyDocument"));
  }

  removeFileDocument(index: number) {

    if (this.listFileDocument.length > 1)
      this.listFileDocument.removeAt(index);
    else {
      this.listFileDocument.removeAt(index);
      this.addFileDocument();
    }
  }

  get listConsultationInfo(): NzSafeAny {
    return this.form.controls['listConsultationInfo'] as FormArray;
  }

  addConsultationInfo(item?: ConsultationInfo) {
    if (this.listConsultationInfo.valid) {
      const fileDocument = this.fb.group({
        type: [item?.type ? item.type : null],
        isCheck: [item?.isCheck || item?.isCheck === 0 ? item.isCheck : null],
        note: [item?.note ? item.note : null],
        canNotDelete: [this.listConsultationInfoType.some(el => el.value === item?.type && el.parentValue)],
      });
      this.listConsultationInfo.push(fileDocument);
    } else this.toastService.error(this.translateService.instant("partnership.pns-contract.label.emptyConsultationInfo"));
  }

  removeConsultationInfo(index: number) {
    if (this.listConsultationInfo.length > 1)
      this.listConsultationInfo.removeAt(index);
    else {
      this.listConsultationInfo.removeAt(index);
      this.addConsultationInfo();
    }
  }

  onFileListChange(listFile: NzUploadFile[], item: NzSafeAny) {
    const listFileFilter: NzUploadFile[] = [];
    if (Array.isArray(listFile)) {
      for (let i = 0; i < listFile.length; i++) {
        if (listFile[i] instanceof File) {
          if (listFile[i].size) {
            listFileFilter.push(listFile[i])
          }
        }
      }
    }
    item.get('fileAttachments').setValue(listFileFilter);
  }

  removeFile(event: number[], item: NzSafeAny) {
    if (event && event.length > 0) {
      item.get('docIdsDelete').setValue(event.filter((docId: number) => !isNaN(docId)));
    }
  }

  onApproval() {
    if (this.contractLiquidationEdit?.liquidationId) {
      const params = {liquidationIds: [this.contractLiquidationEdit?.liquidationId]};
      this.isLoadingPage = true;
      this.contractLiquidationService.approval(params).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.toastService.success(this.translateService.instant('partnership.pns-contract.notification.approvalSuccess'));
        } else {
          this.toastService.error(res?.message);
        }
        this.isLoadingPage = false;
        this.modalRef.close({refresh: true});
      }, (err) => {
        this.isLoadingPage = false;
        this.toastService.error(err?.message);
      });
    }
  }

  approveReject() {
    this.isSubmitted = true;
    Utils.processTrimFormControlsBeforeSend(this.form, 'retireReason');
    if (this.liquidationId && this.form.valid) {
      const request: LiquidationDTO = {};
      this.setDataSendToServer(request);
      this.isLoadingPage = true;
      this.contractLiquidationService.approveReject(this.liquidationId, request).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.toastService.success(this.translateService.instant('partnership.pns-contract.notification.consultationsRequest'));
        } else {
          this.toastService.error(res?.message);
        }
        this.isLoadingPage = false;
        this.modalRef.close({refresh: true});
      }, (err) => {
        this.isLoadingPage = false;
        this.toastService.error(err?.message);
      });
    }
  }

  checkConsultationsRequest(event: boolean, index: number) {
    this.listConsultationsRequest[index] = event;
    this.isConsultationsRequest = this.listConsultationsRequest.some(item => item);
  }

  changeEmp(event: any) {
    if (event) {
      this.form.controls['personalEmail'].setValue(event.personalEmail);
    } else {
      this.form.controls['personalEmail'].setValue(null);
    }
  }

  changeLeaveDate( event: NzSafeAny) {
    if(CommonUtils.isNullOrEmpty(event)) return;
    if(!CommonUtils.isNullOrEmpty(this.form.controls['notifyDay'].value)) {
      if(Utils.convertDateToFillForm(Utils.addDays(this.form.controls['leaveDate'].value, (this.form.controls['notifyDay'].value))) !== this.form.controls['lastWorkingDate'].value){
        this.form.controls['lastWorkingDate'].setValue(Utils.convertDateToFillForm(Utils.addDays(this.form.controls['leaveDate'].value, (this.form.controls['notifyDay'].value))));
      }
    } else if (!CommonUtils.isNullOrEmpty(this.form.controls['lastWorkingDate'].value)) {
      if(moment(this.form.controls['lastWorkingDate'].value).diff(moment(this.form.controls['leaveDate'].value), 'days') !== this.form.controls['notifyDay'].value){
        this.form.controls['notifyDay'].setValue(moment(this.form.controls['lastWorkingDate'].value).diff(moment(this.form.controls['leaveDate'].value), 'days'));
      }
    }
  }

  changeNotifyDays( event: NzSafeAny) {
    if(CommonUtils.isNullOrEmpty(event)) return;
    if(!CommonUtils.isNullOrEmpty(this.form.controls['leaveDate'].value)) {
      if(this.form.controls['lastWorkingDate'].value !== Utils.convertDateToFillForm(Utils.addDays(this.form.controls['leaveDate'].value, (this.form.controls['notifyDay'].value)))){
        this.form.controls['lastWorkingDate'].setValue(Utils.convertDateToFillForm(Utils.addDays(this.form.controls['leaveDate'].value, (this.form.controls['notifyDay'].value))));
      }
    } else if (!CommonUtils.isNullOrEmpty(this.form.controls['lastWorkingDate'].value)) {
      if(this.form.controls['leaveDate'].value !== Utils.convertDateToFillForm(Utils.addDays(this.form.controls['lastWorkingDate'].value,0- (this.form.controls['notifyDay'].value)))){
        this.form.controls['leaveDate'].setValue(Utils.convertDateToFillForm(Utils.addDays(this.form.controls['lastWorkingDate'].value,0- (this.form.controls['notifyDay'].value))));
      }
    }
  }


  changeLastWorkingDate( event: NzSafeAny) {
    if(CommonUtils.isNullOrEmpty(event)) return;
    if(!CommonUtils.isNullOrEmpty(this.form.controls['leaveDate'].value)) {
      if(this.form.controls['notifyDay'].value !== moment(this.form.controls['lastWorkingDate'].value).diff(moment(this.form.controls['leaveDate'].value), 'days')){
        this.form.controls['notifyDay'].setValue(moment(this.form.controls['lastWorkingDate'].value).diff(moment(this.form.controls['leaveDate'].value), 'days'));
      }
    } else if (this.form.controls['leaveDate'].value != Utils.convertDateToFillForm(Utils.subtractDays(this.form.controls['lastWorkingDate'].value, (this.form.controls['notifyDay'].value)))) {
      this.form.controls['leaveDate'].setValue(Utils.convertDateToFillForm(Utils.subtractDays(this.form.controls['lastWorkingDate'].value, (this.form.controls['notifyDay'].value))));
    }
  }
}
