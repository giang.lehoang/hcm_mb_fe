import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
    ContractLiquidationSuggestionEditComponent
} from './contract-liquidation-suggestion-edit/contract-liquidation-suggestion-edit.component';
import {NzFormModule} from "ng-zorro-antd/form";
import {ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/mb-employee-data-picker";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {SharedDirectivesNumbericModule} from "@hcm-mfe/shared/directives/numberic";
import {SharedUiMbTextLabelModule} from "@hcm-mfe/shared/ui/mb-text-label";
import {NzDividerModule} from "ng-zorro-antd/divider";
import {NzUploadModule} from "ng-zorro-antd/upload";
import {NzIconModule} from "ng-zorro-antd/icon";
import {PartnershipUiPnsContractBtnUploadFileModule} from "@hcm-mfe/partnership/ui/pns-contract/btn-upload-file";
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";
import {NzButtonModule} from "ng-zorro-antd/button";
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';
import { NzTableModule } from 'ng-zorro-antd/table';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import {SharedUiExtendFormItemModule} from "@hcm-mfe/shared/ui/extend-form-item";
import {SharedUiMbUploadModule} from "@hcm-mfe/shared/ui/mb-upload";
import {NzTagModule} from "ng-zorro-antd/tag";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
    imports: [CommonModule, NzFormModule, ReactiveFormsModule, SharedUiMbSelectModule, SharedUiExtendFormItemModule, SharedUiMbUploadModule,
        SharedUiMbEmployeeDataPickerModule, TranslateModule, SharedUiMbInputTextModule, SharedUiMbDatePickerModule, SharedDirectivesNumbericModule,
        SharedUiMbTextLabelModule, NzDividerModule, NzUploadModule, NzIconModule, PartnershipUiPnsContractBtnUploadFileModule, NzCheckboxModule, SharedUiMbButtonModule, NzButtonModule, NzPopconfirmModule, NzTableModule, NzTagModule, SharedUiLoadingModule],
    declarations: [
      ContractLiquidationSuggestionEditComponent,
    ],
})
export class PartnershipFeaturePnsContractContractLiquidationContractLiquidationSuggestionEditModule {}
