import {Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AppFunction, BaseResponse, MBTableConfig, Pagination} from '@hcm-mfe/shared/data-access/models';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {Subscription} from 'rxjs';
import {LiquidationResponse} from '@hcm-mfe/partnership/data-access/pns-contract/models';
import {HttpParams} from '@angular/common/http';
import {HTTP_STATUS_CODE, Mode, Scopes} from '@hcm-mfe/shared/common/constants';
import {getTypeExport, StringUtils, Utils} from '@hcm-mfe/shared/common/utils';
import {Constant, FunctionCode, UrlConstant} from '@hcm-mfe/partnership/data-access/pns-contract/common';
import {ContractLiquidationService, SearchFormService} from '@hcm-mfe/partnership/data-access/pns-contract/services';
import * as moment from 'moment';
import {NzSafeAny} from 'ng-zorro-antd/core/types';
import {CatalogModel} from '@hcm-mfe/partnership/ui/pns-contract/search-form';
import {NzModalRef, NzModalService} from 'ng-zorro-antd/modal';
import {
  ContractLiquidationSuggestionEditComponent
} from '@hcm-mfe/partnership/feature/pns-contract/contract-liquidation/contract-liquidation-suggestion-edit';
import {saveAs} from 'file-saver';
import {SessionService} from "@hcm-mfe/shared/common/store";
import {CustomValidators} from "@hcm-mfe/shared/common/validators";
import {NzUploadFile} from "ng-zorro-antd/upload";
import {CommonUtils} from "@hcm-mfe/shared/core";

export interface Pos {
  pgrId: number;
  pgrName: string;
}

@Component({
  selector: 'hcm-mfe-contract-liquidation-list',
  templateUrl: './contract-liquidation-list.component.html',
  styleUrls: ['./contract-liquidation-list.component.scss']
})
export class ContractLiquidationListComponent implements OnInit, OnDestroy {

  form!: FormGroup;
  tableConfig!: MBTableConfig;

  liquidationStatus: CatalogModel[] = [];
  dataTable: LiquidationResponse[] = [];
  checkAll: boolean[] = [];

  modal: NzModalRef | undefined;
  constant = Constant;
  pagination = new Pagination();
  isLoadingPage = false;
  scope=Scopes.VIEW;
  functionCode=FunctionCode.PNS_LIQUIDATIONS;

  isImportData = false;
  urlApiImport = UrlConstant.CONTRACT_LIQUIDATION.IMPORT_CREATE;
  urlApiDownloadTemp = UrlConstant.CONTRACT_LIQUIDATION.DOWNLOAD_TEMPLATE_CREATE;

  isImportDataApprove = false;
  urlApiImportApprove = UrlConstant.CONTRACT_LIQUIDATION.IMPORT_APPROVE;
  urlApiDownloadTempApprove = UrlConstant.CONTRACT_LIQUIDATION.DOWNLOAD_TEMPLATE_APPROVE;

  subs: Subscription[] = [];
  objFunction: AppFunction;
  listLiquidationId: number[] = [];

  @ViewChild('actionTmpl', {static: true}) action!: TemplateRef<NzSafeAny>;
  @ViewChild('statusTmpl', {static: true}) status!: TemplateRef<NzSafeAny>;
  @ViewChild('selectTmpl', {static: true}) select!: TemplateRef<NzSafeAny>;
  @ViewChild('selectHeaderTmpl', {static: true}) selectHeader!: TemplateRef<NzSafeAny>;
  listOrgRegions: CatalogModel[] = [];
  listPositionGroup: CatalogModel[] = [];
  listEmpTypeCode: CatalogModel[] = [];
  listLine: Pos[] = [];
  listPosition: Pos[] = [];
  isShowModalReject = false;
  formReject = this.fb.group({
    rejectReason: ['', [CustomValidators.required, Validators.maxLength(500)]]
  });
  isReject = false;
  isShowModalComplete: any;
  formComplete= this.fb.group({
    finishDate: ['', [CustomValidators.required]],
    note: ['', [Validators.maxLength(500)]],
    files: [null]
  });
  isComplete = false;
  private rejectId?: number;
  private completeId?: number;
  fileAttachList: NzUploadFile[] = [];

  constructor(
    private fb: FormBuilder,
    private translate: TranslateService,
    private toastService: ToastrService,
    private modalService: NzModalService,
    private searchFormService: SearchFormService,
    private contractLiquidationService: ContractLiquidationService,
    private sessionService: SessionService
  ) {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.PNS_LIQUIDATIONS}`);
  }

  ngOnInit(): void {
    this.initDataSelect();
    this.initFormGroup();
    this.initTable();
    this.doSearch(1);
  }

  initDataSelect() {
    this.liquidationStatus = Constant.CONTRACT_LIQUIDATION_STATUS.map((item => {
      item.label = this.translate.instant(item.label);
      return item;
    }));

    this.subs.push(
      this.contractLiquidationService.getPgrType('LINE').subscribe(res => {
        this.listLine = res.data;
      }, (e) => {
        this.toastService.error(e?.message);
      })
    );
    this.subs.push(
      this.contractLiquidationService.getPgrType('POSITION').subscribe(res => {
        this.listPosition = res.data;
      }, (e) => {
        this.toastService.error(e?.message);
      })
    );

    const paramsDT = new HttpParams().set('typeCode', Constant.TYPE_CODE.DOI_TUONG_CV);
    this.subs.push(
      this.searchFormService.getListValue(UrlConstant.CATEGORY.LOOKUP_VALUE, paramsDT).subscribe(res => {
        this.listEmpTypeCode = res.data;
      }, error => {
        this.toastService.error(error.message);
      })
    )

    const paramsKV = new HttpParams().set('typeCode', Constant.TYPE_CODE.KHU_VUC);
    this.subs.push(
      this.searchFormService.getListValue(UrlConstant.CATEGORY.LOOKUP_VALUE, paramsKV).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listOrgRegions = res.data;
        }
      }, error => {
        this.toastService.error(error.message);
      })
    );
  }

  doSearch(pageIndex: number) {
    this.pagination.pageNumber = pageIndex;
    this.tableConfig = {...this.tableConfig};
    this.isLoadingPage = true;
    const params = this.parseOptions();
    this.subs.push(
      this.searchFormService.getFilterResearch(UrlConstant.SEARCH_FORM.CONTRACT_LIQUIDATION, params, this.pagination.getCurrentPage()).subscribe({
        next: (res: BaseResponse) => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.dataTable = res?.data.listData;
            this.tableConfig.total = res.data.count;
          } else {
            this.toastService.error(res?.message);
          }
          this.tableConfig = {...this.tableConfig};
          this.isLoadingPage = false;
        },
        error: (err) => {
          this.toastService.error(err?.message);
          this.tableConfig = {...this.tableConfig};
          this.isLoadingPage = false;
        }
      })
    );
  }

  initFormGroup() {
    this.form = this.fb.group({
      employeeCode: [null],
      empName: [null],
      orgId: [null],
      status: [null],
      fromDate: [null],
      toDate: [null],
      year: [null],
      empTypeCodes: [null],
      regionCodes: [null],
      lineIds: [null],
      positionGroupIds: [null]
    });
  }

  parseOptions() {
    let params = new HttpParams();
    if (!StringUtils.isNullOrEmpty(this.form.controls['employeeCode'].value))
      params = params.set('employeeCode', this.form.controls['employeeCode'].value);

    if (!StringUtils.isNullOrEmpty(this.form.controls['empName'].value))
      params = params.set('fullName', this.form.controls['empName'].value);

    if (this.form.controls['orgId'].value !== null)
      params = params.set('orgId', this.form.controls['orgId'].value?.orgId);

    if (this.form.controls['status'].value !== null)
      params = params.set('listStatus', this.form.controls['status'].value.join(','));
    if (this.form.controls['empTypeCodes'].value !== null)
          params = params.set('empTypeCodes', this.form.controls['empTypeCodes'].value.join(','));
    if (this.form.controls['regionCodes'].value !== null)
          params = params.set('regionCodes', this.form.controls['regionCodes'].value.join(','));
    if (this.form.controls['lineIds'].value !== null)
          params = params.set('lineIds', this.form.controls['lineIds'].value.join(','));
    if (this.form.controls['positionGroupIds'].value !== null)
          params = params.set('positionGroupIds', this.form.controls['positionGroupIds'].value.join(','));

    if (!StringUtils.isNullOrEmpty(this.form.controls['year'].value)) {
      params = params.set('year', moment(this.form.controls['year'].value).format('YYYY'));
    }

    return params;
  }

  initTable(): void {
    this.tableConfig = {
      headers: [
        {
          title: ' ',
          field: 'isCheck',
          width: 40,
          tdTemplate: this.select,
          thTemplate: this.selectHeader,
          thClassList: ['text-nowrap', 'text-center'],
          tdClassList: ['text-nowrap', 'text-center'],
          fixed: window.innerWidth > 1024,
          show: this.objFunction?.approve,
          fixedDir: 'left'
        },
        {
          title: 'STT',
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          fixed: true,
          fixedDir: 'left',
          width: 70
        },
        {
          title: 'partnership.pns-contract.contractLiquidation.table.employeeCode',
          field: 'employeeCode',
          fixed: true,
          fixedDir: 'left'
        },
        {
          title: 'partnership.pns-contract.contractLiquidation.table.employeeName',
          field: 'fullName',
          fixed: true,
          fixedDir: 'left'
        },
        {
          title: 'partnership.pns-contract.table.customerId',
          width: 110,
          show: false,
          field: 'customerId'
        },
        {
          title: 'partnership.pns-contract.table.personalId',
          width: 110,
          show: false,
          field: 'personalId'
        },
        {
          title: 'partnership.pns-contract.contractLiquidation.table.posName',
          field: 'positionName',
          fixed: true,
          fixedDir: 'left'
        },
        {
          title: 'partnership.pns-contract.contractLiquidation.table.contractTypeName',
          field: 'contractTypeName'
        },
        {
          title: 'partnership.pns-contract.contractLiquidation.table.contractExpriedDate',
          field: 'contractExpiredDate',
          thClassList: ['text-center'],
          tdClassList: ['text-center']
        },
        {
          title: 'partnership.pns-contract.contractLiquidation.table.orgName',
          field: 'orgName'
        },
        {
          title: 'partnership.pns-contract.contractLiquidation.table.lastWorkingDate',
          field: 'lastWorkingDate',
          thClassList: ['text-center'],
          tdClassList: ['text-center']
        },
        {
          title: 'partnership.pns-contract.contractLiquidation.table.status',
          field: 'status',
          tdTemplate: this.status
        },
        {
          title: ' ',
          tdClassList: ['text-nowrap', 'text-center'], thClassList: ['text-nowrap', 'text-center'],
          width: 50,
          tdTemplate: this.action
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  setFormValue(event: NzSafeAny, formName: string) {
    if (event.listOfSelected?.length > 0) {
      this.form.controls[formName].setValue(event.listOfSelected);
    }
  }

  openAddModal() {
    this.modal = this.modalService.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
      nzTitle: this.translate.instant('partnership.pns-contract.modalName.addContractLiquidationSuggestion'),
      nzContent: ContractLiquidationSuggestionEditComponent,
      nzComponentParams: {
        mode: Mode.ADD
      },
      nzFooter: null
    });
    this.subs.push(
      this.modal.afterClose.subscribe(res => {
        if (res && res.refresh) {
          this.doSearch(1);
        }
      })
    );
  }

  openModalEdit(liquidationId: number, isCompleteProfile?: boolean) {
    this.modal = this.modalService.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
      nzTitle: this.translate.instant('partnership.pns-contract.modalName.addContractLiquidationSuggestion'),
      nzContent: ContractLiquidationSuggestionEditComponent,
      nzComponentParams: {
        mode: Mode.EDIT,
        isCompleteProfile: isCompleteProfile,
        liquidationId: liquidationId
      },
      nzFooter: null
    });
    const sub = this.modal?.afterClose.subscribe(res => {
      if (res && res.refresh) {
        this.doSearch(1);
      }
    });
    if (sub) {
      this.subs.push(sub);
    }
  }

  onDeleteClick(liquidationId: number) {
    this.subs.push(
      this.contractLiquidationService.deleteById(liquidationId).subscribe({
        next: (res: BaseResponse) => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.doSearch(1);
            this.toastService.success(this.translate.instant('partnership.pns-contract.notification.delSuccess'));
          } else {
            this.toastService.error(res?.message);
          }
        },
        error: (err) => {
          this.toastService.error(err?.message);
        }
      })
    );
  }

  doExportData() {
    if (this.form.valid) {
      this.isLoadingPage = true;
      const params = this.form ? {...this.form.value} : {};
      this.subs.push(
        this.contractLiquidationService.exportData(params).subscribe(res => {
          if ((res?.headers?.get('Excel-File-Empty'))) {
            this.toastService.error(this.translate.instant('common.notification.exportFail'));
          } else {
            const arr = res.headers.get("Content-Disposition")?.split(';');
            const fileName:string = arr[arr.length - 1].replace('filename=','').trim();
            const reportFile = new Blob([res.body], {type: getTypeExport('xlsx')});
            saveAs(reportFile, fileName);
          }
          this.isLoadingPage = false;
        }, error => {
          this.toastService.error(error.message);
          this.isLoadingPage = false;
        })
      );
    }
  }

  onCheckChange(event: boolean, id: number) {
    if (event) {
      this.listLiquidationId.push(id);
    } else {
      this.listLiquidationId.splice(this.listLiquidationId.indexOf(id), 1);
    }
    const notCheckAll = this.dataTable.filter(item => item.status === 1).some(item => {
      return this.listLiquidationId.indexOf(item.liquidationId) < 0;
    });
    this.checkAll[this.pagination.pageNumber] = !notCheckAll;
  }

  onCheckAll(event: boolean) {
    if (event) {
      this.dataTable.forEach(item => {
        const id = item.liquidationId;
        if (this.listLiquidationId.indexOf(id) < 0 && item.status === 1) {
          this.listLiquidationId.push(id);
        }
      });
    } else {
      this.dataTable.forEach(item => {
        const id = item.liquidationId;
        this.listLiquidationId = this.listLiquidationId.filter(liquidationId => liquidationId != id);
      });
    }
  }

  checkDisabledAll() {
    return !this.dataTable.some(item => {
      return item.status === 1;
    });
  }

  onApproval(id: number) {
    this.approval({liquidationIds: [id]});
  }

  onApprovalAll() {
    if (this.listLiquidationId.length > 0) {
      this.approval({liquidationIds: this.listLiquidationId});
    }
  }

  approval(params: { liquidationIds: number[] }) {
    this.contractLiquidationService.approval(params).subscribe((res: BaseResponse) => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.doSearch(1);
        this.toastService.success(this.translate.instant('partnership.pns-contract.notification.approvalSuccess'));
      } else {
        this.toastService.error(res?.message);
      }
    }, (err) => {
      this.toastService.error(err?.message);
    });
  }

  doImportApproveData() {
    this.isImportDataApprove = true;
  }

  doImportCreateData() {
    this.isImportData = true;
  }

  doCloseImport(isSearch: boolean) {
    this.isImportData = false;
    isSearch && this.doSearch(1);
  }

  doCloseImportApprove(isSearch: boolean) {
    this.isImportDataApprove = false;
    isSearch && this.doSearch(1);
  }

  ngOnDestroy(): void {
    this.modal?.destroy();
    this.subs?.forEach(sub => sub?.unsubscribe());
  }

  onExportFile(liquidationId: number, reportType: string) {
    this.isLoadingPage = true;
    this.subs.push(
      this.contractLiquidationService.downloadById({liquidationId: liquidationId, reportType: reportType}).subscribe(res => {
        if ((res?.headers?.get('Excel-File-Empty'))) {
          this.toastService.error(this.translate.instant('common.notification.exportFail'));
        } else {
          const arr = res.headers.get("Content-Disposition")?.split(';');
          const fileName:string = arr[arr.length - 1].replace('filename=','').trim();
          const reportFile = new Blob([res.body], {type: getTypeExport('docx')});
          saveAs(reportFile, fileName);
        }
        this.isLoadingPage = false;
      }, error => {
        this.toastService.error(error.message);
        this.isLoadingPage = false;
      })
    );
  }

  confirmProfile(liquidationId: number) {
    this.isLoadingPage = true;
    this.subs.push(
      this.contractLiquidationService.confirmProfile(liquidationId).subscribe(res => {
        this.toastService.success(this.translate.instant("common.notification.success"));
        this.isLoadingPage = false;
        this.doSearch(1);
      }, e => {
        this.isLoadingPage = false;
        this.toastService.error(e?.message);
      })
    );
  }

  onBlur() {
    this.formReject.controls['rejectReason'].setValue(this.formReject.controls['rejectReason'].value?.trim())
  }

  onReject(id: number, status: number): void {
    if (status !== 4) return;
    this.rejectId = id;
    this.isReject = false;
    this.isShowModalReject = true;
  }

  handleCancelReject() {
    this.isShowModalReject = false;
  }

  handleOkReject() {
    this.isReject = true;
    if (this.formReject.valid) {
      this.returnProfile({liquidationId: this.rejectId, rejectReason: this.formReject.controls['rejectReason'].value});
    }
  }

  returnProfile(data: { rejectReason: string; liquidationId: number | undefined }) {
    this.isLoadingPage = true;
    this.subs.push(
      this.contractLiquidationService.returnProfile(data).subscribe(res => {
        this.toastService.success(this.translate.instant("common.notification.success"));
        this.isShowModalReject = false;
        this.isLoadingPage = false;
        this.doSearch(1);
      }, e => {
        this.isLoadingPage = false;
        this.toastService.error(e?.message);
      })
    );
  }

  onComplete(id: number, status: number): void {
    if (status !== 5) return;
    this.completeId = id
    this.isShowModalComplete = true;
  }

  handleCancelComplete() {
    this.isShowModalComplete = false;
  }

  handleOkComplete() {
    this.isComplete = true;
    if (this.formComplete.valid) {
      this.completeLiquidation({
        liquidationId: this.completeId,
        finishDate: Utils.convertDateToSendServer(this.formComplete.controls['finishDate'].value) ?? '',
        note: this.formComplete.controls['note'].value,
        files: this.formComplete.controls['files'].value,
      });
    }
  }

  completeLiquidation(data: { note: string; files: any; finishDate: string; liquidationId: number | undefined }) {
    this.isLoadingPage = true;
    this.subs.push(
      this.contractLiquidationService.completeLiquidation(CommonUtils.convertFormFile(data)).subscribe(res => {
        this.toastService.success(this.translate.instant("common.notification.success"));
        this.isShowModalReject = false;
        this.isLoadingPage = false;
        this.doSearch(1);
      }, e => {
        this.isLoadingPage = false;
        this.toastService.error(e?.message);
      })
    );
  }

  onFileListChange(listFile: NzUploadFile[]) {
    const listFileFilter: NzUploadFile[] = [];
    if (Array.isArray(listFile)) {
      for (let i = 0; i < listFile.length; i++) {
        if (listFile[i] instanceof File) {
          if (listFile[i].size) {
            listFileFilter.push(listFile[i])
          }
        }
      }
    }
    this.formComplete.controls['files'].setValue(listFileFilter);
  }
}
