import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { SharedUiOrgDataPickerModule } from '@hcm-mfe/shared/ui/org-data-picker';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { SharedUiMbSelectCheckAbleModule } from '@hcm-mfe/shared/ui/mb-select-check-able';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbDatePickerModule } from '@hcm-mfe/shared/ui/mb-date-picker';
import { SharedUiMbTableWrapModule } from '@hcm-mfe/shared/ui/mb-table-wrap';
import { SharedUiMbTableModule } from '@hcm-mfe/shared/ui/mb-table';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';
import { DynamicSearchComponent } from './dynamic-search/dynamic-search.component';
import { PartnershipUiImportCommonModule } from '@hcm-mfe/partnership/ui/pns-contract/import-common';
import { PartnershipUiImportFileModule } from '@hcm-mfe/partnership/ui/import-file';
import { SharedUiMbButtonIconModule } from '@hcm-mfe/shared/ui/mb-button-icon';
import { DynamicFormComponent } from './dynamic-form/dynamic-form.component';
import { SharedUiMbTextLabelModule } from '@hcm-mfe/shared/ui/mb-text-label';
import { PopupService } from '@hcm-mfe/shared/ui/popup';
import {SharedDirectivesNumbericModule} from "@hcm-mfe/shared/directives/numberic";
import {SharedUiMbUploadModule} from "@hcm-mfe/shared/ui/mb-upload";
import { TabsDetailComponent } from './tabs-detail/tabs-detail.component';
import {NzTabsModule} from "ng-zorro-antd/tabs";

@NgModule({
  imports: [CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: DynamicSearchComponent
      }
    ]), FormsModule, NzButtonModule, SharedUiLoadingModule, ReactiveFormsModule, NzGridModule, SharedUiOrgDataPickerModule,
    SharedUiMbInputTextModule, SharedUiMbSelectCheckAbleModule, NzDatePickerModule, SharedUiMbButtonModule,
    NzDropDownModule, TranslateModule, SharedUiMbDatePickerModule, SharedUiMbTableWrapModule, PartnershipUiImportFileModule,
    SharedUiMbTableModule, NzIconModule, NzFormModule, NzTagModule, NzPopconfirmModule, PartnershipUiImportCommonModule,
    SharedUiMbButtonIconModule, SharedUiMbTextLabelModule, SharedDirectivesNumbericModule, SharedUiMbUploadModule, NzTabsModule],
  declarations: [
    DynamicSearchComponent,
    DynamicFormComponent,
    TabsDetailComponent
  ],
  providers: [PopupService]
})
export class PartnershipFeaturePnsContractContractLiquidationDynamicSearchModule {}
