import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import {
  EmployeeInfo,
  LiquidationDTO,
  ListConfig, ListValues
} from '@hcm-mfe/partnership/data-access/pns-contract/models';
import { Subscription } from 'rxjs';
import { HTTP_STATUS_CODE, Mode } from '@hcm-mfe/shared/common/constants';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import {
  ConfigLiquidationService,
  ContractLiquidationValueService
} from '@hcm-mfe/partnership/data-access/pns-contract/services';
import { CustomValidators } from '@hcm-mfe/shared/common/validators';
import { ValidationService } from '@hcm-mfe/shared/core';
import { BaseResponse } from '@hcm-mfe/shared/data-access/models';
import {NzUploadFile} from "ng-zorro-antd/upload";
import {NzSafeAny} from "ng-zorro-antd/core/types";

@Component({
  selector: 'hcm-mfe-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.scss']
})
export class DynamicFormComponent implements OnInit {
  mode?: Mode;
  Mode = Mode;
  form!: FormGroup;
  data?: LiquidationDTO;
  configs: ListConfig[] = [];
  isInit = false;
  isLoading = false;

  employeeInfo?: EmployeeInfo;
  liquidationId?: number;
  configType?: string;
  listFile: NzSafeAny[] = []

  isSubmitted = false;
  subs: Subscription[] = [];

  constructor(
    readonly fb: FormBuilder,
    private translate: TranslateService,
    private toastService: ToastrService,
    private modalService: NzModalService,
    private configLiquidationService: ConfigLiquidationService,
    private contractLiquidationValueService: ContractLiquidationValueService,
    public modalRef: NzModalRef
  ) {
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.form = this.fb.group({
      docIdsDelete: [[]],
      listFileDocument: [[]],
    });
    if (this.configType && this.liquidationId) {
      this.contractLiquidationValueService.getById(this.configType, this.liquidationId).subscribe((res: BaseResponse) => {
          this.data = res.data;
          this.data?.attachFiles?.forEach(el => {
            el.uid = el.docId;
            el.name = el.fileName;
          })
          this.form.get('listFileDocument')?.setValue(this.data?.attachFiles);
          this.initForm(res.data.values);
        },
        error => {
          this.toastService.error(error.message);
          this.isLoading = false;
        });
    }
  }

  initForm(values: ListValues[]) {
    this.configs?.forEach(el => {
      const item = values.find(element => element.configCode === el.code);
      const validate = [];
      if (el.maxLength > 0) {
        validate.push(Validators.maxLength(el.maxLength));
      }
      if (el.dataType === 'int') {
        validate.push(CustomValidators.onlyNumber);
      }
      if (el.dataType === 'double') {
        validate.push(ValidationService.number);
      }
      this.form.addControl(el.code, new FormControl(item?.value, [...validate]));
    });
    this.isInit = true;
    this.isLoading = false;
  }

  parseOptions() {
    const values = [];
    for (const [key, value] of Object.entries(this.form.controls)) {
      if (this.form.controls[key].value != null)
        values.push({
          code: key,
          value: this.form.controls[key].value
        })
    }
    return values;
  }

  saveData() {
    this.isSubmitted = true;
    if (this.form.valid) {
      this.isLoading = true;
      let params: any = {};
      const values = this.parseOptions();
      if (this.liquidationId) {
        params['liquidationId'] = this.liquidationId;
        params['values'] = values;
      }
      const formData = new FormData();
      this.listFile.forEach((file: NzUploadFile) => {
        formData.append('files', file as NzSafeAny);
      });
      formData.append('data', new Blob([JSON.stringify(params)], {type: 'application/json'}));
      if (this.configType && this.liquidationId) {
        this.contractLiquidationValueService.saveData(formData, this.configType).subscribe(res => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.toastService.success(this.translate.instant('partnership.pns-contract.notification.saveSuccess'));
          } else {
            this.toastService.error(res?.message);
          }
          this.isLoading = false;
          this.modalRef.close({ refresh: true });
        }, error => {
          this.toastService.error(error?.message);
          this.isLoading = false;
        });
      }
    }
  }

  onFileListChange(listFile: NzUploadFile[]) {
    const listFileFilter: NzUploadFile[] = [];
    if (Array.isArray(listFile)) {
      for (let i = 0; i < listFile.length; i++) {
        if (listFile[i] instanceof File) {
          if (listFile[i].size) {
            listFileFilter.push(listFile[i])
          }
        }
      }
    }
    this.listFile = listFileFilter;
  }

  removeFile(event: number[]) {
    if (event && event.length > 0) {
      this.form.get('docIdsDelete')?.setValue(event.filter((docId: number) => !isNaN(docId)));
    }
  }

  closeModalAdd() {
    this.modalService.closeAll();
  }

}
