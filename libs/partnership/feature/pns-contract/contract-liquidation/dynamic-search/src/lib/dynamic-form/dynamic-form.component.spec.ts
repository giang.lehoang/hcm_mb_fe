import { ComponentFixture, getTestBed, TestBed } from '@angular/core/testing';

import { DynamicFormComponent } from './dynamic-form.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { ToastrModule } from 'ngx-toastr';
import { NzModalModule, NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClient } from '@angular/common/http';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import {
  ConfigLiquidationService,
  ContractLiquidationValueService
} from '@hcm-mfe/partnership/data-access/pns-contract/services';
import { of, throwError } from 'rxjs';

describe('DynamicFormComponent', () => {
  let component: DynamicFormComponent;
  let fixture: ComponentFixture<DynamicFormComponent>;

  let loading: HTMLElement;
  let baseService: BaseService;
  let contractLiquidationValueService: ContractLiquidationValueService;
  let httpClient: HttpClient;
  let injector: TestBed;
  let spyValues: any;
  let spySave: any;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DynamicFormComponent ],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule.forRoot(),
        ToastrModule.forRoot(),
        NzModalModule,
        BrowserAnimationsModule,
        NoopAnimationsModule

      ],
      providers: [FormBuilder,
        {
          provide: NzModalRef,
          useFactory: (modalSvc: NzModalService) => modalSvc.create({
            nzClosable: false,
            nzContent: DynamicFormComponent
          }),
          deps: [NzModalService]
        }]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicFormComponent);
    component = fixture.componentInstance;

    loading = fixture.nativeElement.querySelector('mb-button');

    injector = getTestBed();
    httpClient = injector.get(HttpClient);
    baseService = new BaseService(httpClient);
    contractLiquidationValueService = injector.get(ContractLiquidationValueService);
    spyValues = jest.spyOn(contractLiquidationValueService, 'getById').mockImplementation(() => of(dataById));
    spySave = jest.spyOn(contractLiquidationValueService, 'saveData').mockImplementation(() => of({code: 200}));

    component.configType = 'psn_dao_tao';
    component.liquidationId = 6;
    component.configs = dataConfig.configs;
    component.form = component.fb.group({
        tong_hoi_hoan: ['111'],
        boi_hoan_sau_giam_tru: ['111'],
        ghi_chu: [1],
      }
    );
    // fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('test init', () => {
    component.ngOnInit();
    expect(true).toBeTruthy();
  })

  it('test init error', () => {
    spyValues.mockImplementation(() => throwError(() => new Error('error')))
    component.ngOnInit();
    expect(true).toBeTruthy();
  })

  it('test saveData', () => {
    component.saveData()
    expect(true).toBeTruthy();
  })

  it('test saveData code # 200', () => {
    spySave.mockImplementation(() => of({code: 400}))
    component.saveData()
    expect(true).toBeTruthy();
  })

  it('test saveData error', () => {
    spySave.mockImplementation(() => throwError(() => new Error('error')))
    component.saveData()
    expect(true).toBeTruthy();
  })

  it('test saveData error', () => {
    component.closeModalAdd()
    expect(true).toBeTruthy();
  })
});

export const dataConfig = {
  "configs": [
    {
      "code": "tong_hoi_hoan",
      "name": "Số cần bồi hoàn",
      "dataType": "int",
      "displaySeq": 1,
      "maxLength": 500
    },
    {
      "code": "boi_hoan_sau_giam_tru",
      "name": "Số tiền bồi hoàn sau giảm trừ",
      "dataType": "double",
      "displaySeq": 2,
      "maxLength": 500
    },
    {
      "code": "ghi_chu",
      "name": "Ghi chú",
      "dataType": "string",
      "displaySeq": 3,
      "maxLength": 500
    }
  ],
  "configType": "pns_dao_tao",
  "configTypeName": "Thông tin bồi hoàn đào tạo"
}

export const dataById = {
  "code": 200,
  "message": "Thành công",
  "data": {
    "liquidationId": 6,
    "employeeId": 3,
    "notifyDays": 12,
    "lastWorkingDate": "06/10/2022",
    "reason": "aaaa",
    "createDate": "07/10/2022",
    "createdBy": "mb0001",
    "lastUpdateDate": "07/10/2022",
    "lastUpdatedBy": "mb0001",
    "positionId": 44474,
    "contractTypeId": 2,
    "flagStatus": 1,
    "employeeCode": "MB0010",
    "fullName": "Nguyễn Thành Tuân",
    "positionName": "Giám đốc khối CN. 3 tháng 2",
    "jobName": "Giám đốc khối",
    "orgName": "CN. 3 tháng 2 - Ban giám đốc - Ban giám đốc",
    "contractTypeName": "Hợp đồng lao động 12 tháng",
    "personalId": "22222222",
    "personalIdPlace": "SS",
    "email": "mb0010@mbbank.com.vn",
    "mobileNumber": "12345678",
    "contractNumber": "HĐ2022",
    "currentAddress": "Chung cư Moncity, Phường Mỹ Đình 2, Quận Nam Từ Liêm, Thành phố Hà Nội",
    "personalIdDate": "04/10/2022",
    "values": [
      {
        "displaySeq": 1,
        "configCode": "tong_hoi_hoan",
        "configName": "Số cần bồi hoàn",
        "value": 1000000,
        "dataType": "int"
      },
      {
        "displaySeq": 2,
        "configCode": "boi_hoan_sau_giam_tru",
        "configName": "Số tiền bồi hoàn sau giảm trừ",
        "value": 800000,
        "dataType": "int"
      },
      {
        "displaySeq": 3,
        "configCode": "ghi_chu",
        "configName": "Ghi chú",
        "value": "Test chức năng",
        "dataType": "string"
      }
    ]
  },
  "clientMessageId": "8639c75c-f64e-4350-9624-6d72aac6f32c",
  "transactionId": "b226fe08-1415-4dd9-8067-159359d74d85"
}
