import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TabsDetailComponent } from './tabs-detail.component';

describe('TabsDetailComponent', () => {
  let component: TabsDetailComponent;
  let fixture: ComponentFixture<TabsDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TabsDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TabsDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
