import { Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import {
  AppFunction,
  BaseResponse,
  MBTableConfig,
  MBTableHeader,
  Pagination
} from '@hcm-mfe/shared/data-access/models';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import {
  LiquidationResponse,
  ListConfig
} from '@hcm-mfe/partnership/data-access/pns-contract/models';
import { HttpParams } from '@angular/common/http';
import { HTTP_STATUS_CODE, Mode, Scopes } from '@hcm-mfe/shared/common/constants';
import { getTypeExport, StringUtils } from '@hcm-mfe/shared/common/utils';
import { Constant, UrlConstant } from '@hcm-mfe/partnership/data-access/pns-contract/common';
import {
  ConfigLiquidationService,
  ContractLiquidationValueService
} from '@hcm-mfe/partnership/data-access/pns-contract/services';
import * as moment from 'moment';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { CatalogModel } from '@hcm-mfe/partnership/ui/pns-contract/search-form';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { ActivatedRoute } from '@angular/router';
import { saveAs } from 'file-saver';
import { CustomValidators } from '@hcm-mfe/shared/common/validators';
import { DynamicFormComponent } from '../dynamic-form/dynamic-form.component';
import { CommonUtils } from '@hcm-mfe/shared/core';
import { PopupService } from '@hcm-mfe/shared/ui/popup';
import { SessionService } from '@hcm-mfe/shared/common/store';
import {TabsDetailComponent} from "../tabs-detail/tabs-detail.component";

@Component({
  selector: 'hcm-mfe-dynamic-search',
  templateUrl: './dynamic-search.component.html',
  styleUrls: ['./dynamic-search.component.scss'],
})
export class DynamicSearchComponent implements OnInit, OnDestroy {
  isInitTable = false;
  form!: FormGroup;
  tableConfig!: MBTableConfig;

  liquidationStatus: CatalogModel[] = [];
  dataTable: LiquidationResponse[] = [];
  configs: ListConfig[] = [];

  modal: NzModalRef | undefined;
  constant = Constant;
  pagination = new Pagination();
  isLoadingPage = false;
  scope=Scopes.VIEW;
  configType = '';

  isImportData = false;
  urlApiImport = '';
  urlApiDownloadTemp = '';

  subs: Subscription[] = [];
  objFunction: AppFunction;

  @ViewChild('actionTmpl', { static: true }) action!: TemplateRef<NzSafeAny>;

  constructor(
    public fb: FormBuilder,
    private translate: TranslateService,
    private toastService: ToastrService,
    private modalService: NzModalService,
    private configLiquidationService: ConfigLiquidationService,
    private contractLiquidationValueService: ContractLiquidationValueService,
    private deletePopup: PopupService,
    route: ActivatedRoute,
    private sessionService: SessionService
  ) {
    const code = route.snapshot.data['code'];
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${code}`);
    this.configType = code;
    this.urlApiImport = UrlConstant.API_VERSION + UrlConstant.CONTRACT_LIQUIDATION_VALUES.IMPORT.replace('{configType}', this.configType);
    this.urlApiDownloadTemp = UrlConstant.API_VERSION + UrlConstant.CONTRACT_LIQUIDATION_VALUES.DOWNLOAD_TEMPLATE.replace('{configType}', this.configType);
  }

  @ViewChild('statusTmpl', { static: true }) status!: TemplateRef<NzSafeAny>;
  @ViewChild('filesTmpl', { static: true }) files!: TemplateRef<NzSafeAny>;

  ngOnInit(): void {
    this.initDataSelect();
    this.initFormGroup();
    this.getConfigLiquidation(this.configType);
    this.doSearch(1);
  }

  private getConfigLiquidation(configType: string) {
    const sub = this.configLiquidationService.getConfigLiquidation(configType).subscribe((res: BaseResponse) => {
      CommonUtils.sort(res.data?.configs ?? [], 'displaySeq');
      this.configs = res.data?.configs;
      const listConfig: MBTableHeader[] = [];
      this.configs?.forEach((el: any) => {
        listConfig.push({
          title: el.name,
          field: el.code,
          width: 120,
        });
      });
      this.initTable(listConfig);
    }, (err) => {
      this.toastService.error(err?.message);
    });
    this.subs.push(sub)
  }

  initDataSelect() {
    this.liquidationStatus = Constant.LIQUIDATION_CONSULT_STATUS.map((item => {
      item.label = this.translate.instant(item.label);
      return item;
    }));
  }

  doSearch(pageIndex: number) {
    if (this.form.valid) {
      this.pagination.pageNumber = pageIndex;
      this.tableConfig = { ...this.tableConfig };
      this.isLoadingPage = true;
      const params = this.parseOptions();
      this.subs.push(
        this.contractLiquidationValueService.getListByConfigType(this.configType, params, this.pagination.getCurrentPage()).subscribe({
          next: (res: BaseResponse) => {
            if (res.code === HTTP_STATUS_CODE.OK) {
              this.dataTable = res?.data.listData;
              this.tableConfig.total = res.data.count;
            } else {
              this.toastService.error(res?.message);
            }
            this.tableConfig = { ...this.tableConfig };
            this.isLoadingPage = false;
          },
          error: (err) => {
            this.toastService.error(err?.message);
            this.tableConfig = { ...this.tableConfig };
            this.isLoadingPage = false;
          }
        })
      );
    }
  }

  initFormGroup() {
    this.form = this.fb.group({
        employeeCode: [null],
        fullName: [null],
        org: [null],
        status: [null],
        fromDate: [null],
        toDate: [null]
      },
      {
        validator: [
          CustomValidators.toDateAfterFromDate('fromDate', 'toDate')
        ]
      }
    );
  }

  parseOptions() {
    let params = new HttpParams();
    if (this.form.controls['org'].value !== null)
      params = params.set('organizationId', this.form.controls['org'].value?.orgId);

    if (this.form.controls['status'].value !== null)
      params = params.set('listStatus', this.form.controls['status'].value.join(','));

    if (!StringUtils.isNullOrEmpty(this.form.controls['fromDate'].value)) {
      params = params.set('fromDate', moment(this.form.controls['fromDate'].value).format('DD/MM/YYYY'));
    }

    if (!StringUtils.isNullOrEmpty(this.form.controls['toDate'].value)) {
      params = params.set('toDate', moment(this.form.controls['toDate'].value).format('DD/MM/YYYY'));
    }

    if (!StringUtils.isNullOrEmpty(this.form.controls['employeeCode'].value))
      params = params.set('employeeCode', this.form.controls['employeeCode'].value);

    if (!StringUtils.isNullOrEmpty(this.form.controls['fullName'].value))
      params = params.set('fullName', this.form.controls['fullName'].value);

    return params;
  }

  initTable(listConfig: Array<MBTableHeader>): void {
    this.tableConfig = {
      headers: [
        {
          title: 'partnership.pns-contract.table.employeeCode',
          field: 'employeeCode',
          width: 75,
          fixed: true,
          fixedDir: 'left'
        },
        {
          title: 'partnership.pns-contract.table.employeeName',
          field: 'fullName',
          width: 120,
          fixed: true,
          fixedDir: 'left'
        },
        {
          title: 'partnership.pns-contract.label.lastWorkingDate',
          field: 'lastWorkingDate',
          width: 100,
          tdClassList: ['text-center']
        },
        /// dynamic

        {
          title: 'partnership.pns-contract.table.customerId',
          width: 110,
          field: 'customerId'
        },
        {
          title: 'partnership.pns-contract.table.personalId',
          width: 110,
          field: 'personalId'
        },
        {
          title: 'partnership.pns-contract.table.posName',
          width: 115,
          field: 'positionName'
        },
        {
          title: 'partnership.pns-contract.table.orgName',
          field: 'orgName',
          width: 175,
          thClassList: ['text-center'],
          tdClassList: ['text-center']
        },
        {
          title: 'partnership.pns-contract.label.attachFile',
          field: 'attachFiles',
          tdTemplate: this.files,
          width: 275,
          tdClassList: ['text-left'],
          thClassList: ['text-nowrap', 'text-left']
        },
        {
          title: 'partnership.pns-contract.table.createdDate',
          field: 'createDate',
          width: 100,
          show: false,
          tdClassList: ['text-center']
        },
        {
          title: 'partnership.pns-contract.table.createdBy',
          field: 'createdBy',
          width: 100,
          show: false,
          tdClassList: ['text-center']
        },
        {
          title: 'partnership.pns-contract.table.status',
          field: 'status',
          width: 150,
          thClassList: ['text-center'],
          tdClassList: ['text-center'],
          fixed: true,
          fixedDir: 'right',
          tdTemplate: this.status
        },
        {
          title: ' ',
          tdClassList: ['text-nowrap', 'text-center'], thClassList: ['text-nowrap', 'text-center'],
          width: 80,
          fixed: true,
          fixedDir: 'right',
          tdTemplate: this.action,
          show: this.objFunction?.edit || this.objFunction?.delete
        }
      ],
      total: 0,
      needScroll: true,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1,
    };
    this.tableConfig.headers.splice(3, 0, ...listConfig);
    this.isInitTable = true;
  }

  setFormValue(event: NzSafeAny, formName: string) {
    if (event.listOfSelected?.length > 0) {
      this.form.controls[formName].setValue(event.listOfSelected);
    }
  }

  downloadFile(file: NzSafeAny) {
    this.contractLiquidationValueService.doDownloadAttachFile(Number(file.docId)).subscribe(res => {
      const reportFile = new Blob([res], {type: getTypeExport(file.fileName?.split(".").pop() ?? '')});
      saveAs(reportFile, file.fileName);
    });
  }

  openModalEdit(liquidationId: number) {
    this.modal = this.modalService.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
      nzTitle: '',
      nzContent: DynamicFormComponent,
      nzComponentParams: {
        mode: Mode.EDIT,
        liquidationId: liquidationId,
        configType: this.configType,
        configs: this.configs
      },
      nzFooter: null
    });
    const sub = this.modal?.afterClose.subscribe(res => {
      if (res && res.refresh) {
        this.doSearch(1);
      }
    });
    if (sub) {
      this.subs.push(sub);
    }
  }

  onDeleteClick(id: number) {
    this.deletePopup.showModal(() => {
      this.subs.push(
        this.contractLiquidationValueService.deleteById(this.configType, id).subscribe({
          next: (res: BaseResponse) => {
            if (res.code === HTTP_STATUS_CODE.OK) {
              this.doSearch(1);
              this.toastService.success(this.translate.instant("partnership.pns-contract.notification.delSuccess"));
            } else {
              this.toastService.error(res?.message);
            }
          },
          error: (err) => {
            this.toastService.error(err?.message);
          }
        })
      );
    });
  }

  onSyncClick(id: number) {
    this.isLoadingPage = true;
    this.contractLiquidationValueService.syncById(this.configType, id).subscribe({
      next: (res: BaseResponse) => {
        this.isLoadingPage = false;
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.doSearch(1);
          this.toastService.success(this.translate.instant("partnership.pns-contract.notification.syncSuccess"));
        } else {
          this.toastService.error(res?.message);
        }
      },
      error: (err) => {
        this.isLoadingPage = false;
        this.toastService.error(err?.message);
      }
    })
  }

  ngOnDestroy(): void {
    this.modal?.destroy();
    this.subs?.forEach(sub => sub?.unsubscribe());
  }

  doExportData() {
    if (this.form.valid) {
      this.isLoadingPage = true;
      const params = this.form ? {...this.form.value} : {};
      this.subs.push(
        this.contractLiquidationValueService.exportData(this.configType, params).subscribe(res => {
          if ((res?.headers?.get('Excel-File-Empty'))) {
            this.toastService.error(this.translate.instant('common.notification.exportFail'));
          } else {
            const arr = res.headers.get('Content-Disposition')?.split(';');
            const fileName: string = arr[arr.length - 1].replace('filename=', '').trim();
            const reportFile = new Blob([res.body], { type: getTypeExport('xlsx') });
            saveAs(reportFile, fileName);
          }
          this.isLoadingPage = false;
        }, error => {
          this.toastService.error(error.message);
          this.isLoadingPage = false;
        })
      );
    }
  }

  doImportData() {
    this.isImportData = true;
  }

  doShowModalDetail(row: NzSafeAny) {
    if(this.configType !== 'PNS-DU-NO') {
      return;
    }
    this.modal = this.modalService.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
      nzTitle: this.translate.instant('common.label.detailInfo'),
      nzContent: TabsDetailComponent,
      nzComponentParams: {
        liquidationId: row.liquidationId
      },
      nzFooter: null
    });
  }

  doCloseImport(isSearch: boolean) {
    this.isImportData = false;
    isSearch && this.doSearch(1);
  }
}
