import {Component, OnInit} from '@angular/core';
import {BaseResponse, MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {ContractLiquidationValueService} from "@hcm-mfe/partnership/data-access/pns-contract/services";
import {ToastrService} from "ngx-toastr";
import {
  LiquidationDetailCard,
  LiquidationDetailLoad
} from "@hcm-mfe/partnership/data-access/pns-contract/models";
import {forkJoin} from "rxjs";

@Component({
  selector: 'hcm-mfe-tabs-detail',
  templateUrl: './tabs-detail.component.html',
  styleUrls: ['./tabs-detail.component.scss']
})
export class TabsDetailComponent implements OnInit {

  paginationLoan = new Pagination();
  tableConfigLoan!: MBTableConfig;
  dataTableLoan: LiquidationDetailLoad[] = [];
  dataLoan: LiquidationDetailLoad[] = [];
  paginationCard = new Pagination();
  tableConfigCard!: MBTableConfig;
  dataTableCard: LiquidationDetailCard[] = [];
  dataCard: LiquidationDetailCard[] = [];
  liquidationId?: number;
  isLoadingPage = false;

  constructor(
    private contractLiquidationValueService: ContractLiquidationValueService,
    private toastService: ToastrService,
  ) {

  }

  ngOnInit(): void {
    this.initTableLoan();
    this.initTableCard();
    this.getData();
  }

  getData() {
    if (this.liquidationId) {
      this.isLoadingPage = true;
      forkJoin(
        this.contractLiquidationValueService.getListDetailLoanById(this.liquidationId),
        this.contractLiquidationValueService.getListDetailCardById(this.liquidationId)
      ).subscribe(([resLoan, resCard]: [BaseResponse, BaseResponse]) => {
          if (resLoan.code === HTTP_STATUS_CODE.OK) {
            this.dataLoan = resLoan?.data;
            this.tableConfigLoan.total = resLoan?.data?.length;
            this.doSearchLoan(1);
          } else {
            this.toastService.error(resLoan?.message);
          }
          if (resCard.code === HTTP_STATUS_CODE.OK) {
            this.dataCard = resCard?.data;
            this.tableConfigCard.total = resCard?.data?.length;
            this.doSearchCard(1);
          } else {
            this.toastService.error(resCard?.message);
          }
          this.isLoadingPage = false;
        },
        (err) => {
          this.toastService.error(err?.message);
          this.isLoadingPage = false;
        })
    }
  }

  initTableLoan(): void {
    this.tableConfigLoan = {
      headers: [
        {
          title: 'partnership.pns-contract.contractLiquidation.table.ldNumber',
          field: 'ldNumber',
          width: 85,
          fixedDir: 'left'
        },
        {
          title: 'partnership.pns-contract.contractLiquidation.table.debtAmount',
          field: 'debtAmount',
          width: 85,
          fixedDir: 'left'
        },
        {
          title: 'partnership.pns-contract.contractLiquidation.table.openDate',
          field: 'openDate',
          width: 70,
          thClassList: ['text-center'],
          tdClassList: ['text-center']
        },
        {
          title: 'partnership.pns-contract.contractLiquidation.table.productCode',
          field: 'productCode',
          width: 70
        },
        {
          title: 'partnership.pns-contract.contractLiquidation.table.productName',
          field: 'productName',
          width: 140
        },
      ],
      total: 0,
      needScroll: false,
      loading: false,
      size: 'small',
      pageSize: this.paginationLoan.pageSize,
      pageIndex: 1,
    };
  }

  initTableCard(): void {
    this.tableConfigCard = {
      headers: [
        {
          title: 'partnership.pns-contract.contractLiquidation.table.cardNumber',
          field: 'cardNumber',
          width: 120,
          fixedDir: 'left'
        },
        {
          title: 'partnership.pns-contract.contractLiquidation.table.debtAmount',
          field: 'debtAmount',
          width: 90,
          fixedDir: 'left'
        },
        {
          title: 'partnership.pns-contract.contractLiquidation.table.interestRate',
          field: 'interestRate',
          width: 80,
          tdClassList: ['text-center']
        },
        {
          title: 'partnership.pns-contract.contractLiquidation.table.interestGroup',
          field: 'interestGroup',
          width: 80
        },
        {
          title: 'partnership.pns-contract.contractLiquidation.table.productNameCard',
          field: 'productName',
          width: 150
        },
        {
          title: 'partnership.pns-contract.contractLiquidation.table.cardClass',
          field: 'cardClass',
          width: 65
        },
        {
          title: 'partnership.pns-contract.contractLiquidation.table.accountStatus',
          field: 'accountStatus',
          width: 90,
          thClassList: ['text-center'],
          tdClassList: ['text-center']
        },
      ],
      total: 0,
      needScroll: false,
      loading: false,
      size: 'small',
      pageSize: this.paginationCard.pageSize,
      pageIndex: 1,
    };
  }

  doSearchLoan(pageIndex: number) {
    this.paginationLoan.pageNumber = pageIndex;
    this.dataTableLoan = this.dataLoan.slice(this.paginationLoan.getCurrentPage().startRecord,
      this.paginationLoan.getCurrentPage().startRecord + this.paginationLoan.getCurrentPage().pageSize) || [];
  }

  doSearchCard(pageIndex: number) {
    this.paginationCard.pageNumber = pageIndex;
    this.dataTableCard = this.dataCard.slice(this.paginationCard.getCurrentPage().startRecord,
      this.paginationCard.getCurrentPage().startRecord + this.paginationCard.getCurrentPage().pageSize) || [];
  }

}
