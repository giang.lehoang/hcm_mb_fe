import { ComponentFixture, getTestBed, TestBed } from '@angular/core/testing';

import { DynamicSearchComponent } from './dynamic-search.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { ToastrModule } from 'ngx-toastr';
import { NzModalModule, NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { HttpClient } from '@angular/common/http';
import {
  ConfigLiquidationService,
  ContractLiquidationValueService
} from '@hcm-mfe/partnership/data-access/pns-contract/services';
import { of, throwError } from 'rxjs';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { DynamicFormComponent } from '../dynamic-form/dynamic-form.component';

describe('DynamicSearchComponent', () => {
  let component: DynamicSearchComponent;
  let fixture: ComponentFixture<DynamicSearchComponent>;
  let componentForm: DynamicFormComponent;
  let fixtureForm: ComponentFixture<DynamicFormComponent>;

  let loading: HTMLElement;
  let baseService: BaseService;
  let configLiquidationService: ConfigLiquidationService;
  let contractLiquidationValueService: ContractLiquidationValueService;
  let httpClient: HttpClient;
  let injector: TestBed;
  let spyConfig: any;
  let spyValues: any;
  let spyValuesDalete: any;
  let spyValuesExport: any;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DynamicSearchComponent, DynamicFormComponent ],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule.forRoot(),
        ToastrModule.forRoot(),
        NzModalModule,
        BrowserAnimationsModule,
        NoopAnimationsModule

      ],
      providers: [FormBuilder,
        {
          provide: NzModalRef,
          useFactory: (modalSvc: NzModalService) => modalSvc.create({
            nzClosable: false,
            nzContent: DynamicFormComponent
          }),
          deps: [NzModalService]
        }]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicSearchComponent);
    component = fixture.componentInstance;
    fixtureForm = TestBed.createComponent(DynamicFormComponent);
    componentForm = fixtureForm.componentInstance;
    loading = fixture.nativeElement.querySelector('mb-button');

    injector = getTestBed();
    httpClient = injector.get(HttpClient);
    baseService = new BaseService(httpClient);
    configLiquidationService = injector.get(ConfigLiquidationService);
    contractLiquidationValueService = injector.get(ContractLiquidationValueService);

    const expectedConfig = { code: 200, data: dataConfig };
    const expectedValues = { code: 200, data: dataValues };
    component.configType = 'psn_dao_tao';
    spyConfig = jest.spyOn(configLiquidationService, 'getConfigLiquidation').mockImplementation(() => of(expectedConfig));
    spyValues = jest.spyOn(contractLiquidationValueService, 'getListByConfigType').mockImplementation(() => of(expectedValues));
    spyValuesDalete = jest.spyOn(contractLiquidationValueService, 'deleteById').mockImplementation(() => of({code: 200}));
    spyValuesExport = jest.spyOn(contractLiquidationValueService, 'exportData').mockImplementation(() => of({}));
    component.form = component.fb.group({
        employeeCode: ['111'],
        fullName: ['111'],
        org: [1],
        status: [[1]],
        fromDate: ['06/10/2022'],
        toDate: ['21/10/2022']
      }
    );
    // fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(componentForm).toBeTruthy();
  });

  it('test init', () => {
    component.ngOnInit();
    expect(true).toBeTruthy();
  });

  it('test init # 200', () => {
    spyValues.mockImplementation(() => of({code: 400}));
    component.ngOnInit();
    expect(true).toBeTruthy();
  });

  it('test init error', () => {
    spyConfig.mockImplementation(() => throwError(() => new Error(`Invalid time`)));
    component.ngOnInit();
    expect(true).toBeTruthy();
  });

  it('test init doImportData', () => {
    component.doImportData();
    expect(true).toBeTruthy();
  });

  it('test init doImportData', () => {
    component.doCloseImport(true);
    expect(true).toBeTruthy();
  });

  it('test search error', () => {
    spyValues.mockImplementation(() => throwError(() => new Error(`Invalid time`)));
    component.ngOnInit();
    expect(true).toBeTruthy();
  });

  it('test openModalEdit', () => {
    component.openModalEdit(1);
    component.modal.afterClose.next({refresh: true});
    expect(true).toBeTruthy();
  });

  it('test setFormValue', () => {
    component.setFormValue({listOfSelected: [1]}, 'status');
    expect(true).toBeTruthy();
  });

  it('test onDeleteClick', () => {
    component.onDeleteClick(1);
    expect(true).toBeTruthy();
  });

  it('test onDeleteClick code # 200', () => {
    spyValuesDalete.mockImplementation(() => of({code: 400}))
    component.onDeleteClick(1);
    expect(true).toBeTruthy();
  });

  it('test onDeleteClick error', () => {
    spyValuesDalete.mockImplementation(() => throwError(() => new Error(`Invalid time`)))
    component.onDeleteClick(1);
    expect(true).toBeTruthy();
  });

  it('should ', () => {
    component.doExportData();
    expect(true).toBeTruthy();
  });

  it('should ', () => {
    spyValuesExport.mockImplementation(() => throwError(() => new Error(`Invalid time`)))
    component.doExportData();
    expect(true).toBeTruthy();
  });
});


 export const dataConfig = {
   "configs": [
     {
       "code": "tong_hoi_hoan",
       "name": "Số cần bồi hoàn",
       "dataType": "int",
       "displaySeq": 1,
       "maxLength": 500
     },
     {
       "code": "boi_hoan_sau_giam_tru",
       "name": "Số tiền bồi hoàn sau giảm trừ",
       "dataType": "int",
       "displaySeq": 2,
       "maxLength": 500
     },
     {
       "code": "ghi_chu",
       "name": "Ghi chú",
       "dataType": "string",
       "displaySeq": 3,
       "maxLength": 500
     }
   ],
   "configType": "pns_dao_tao",
   "configTypeName": "Thông tin bồi hoàn đào tạo"
 }


 export const dataValues = {
  "listData": [
    {
      "reason": "aaaa",
      "personalId": null,
      "lastWorkingDate": "06/10/2022",
      "handOverFiles": null,
      "lastUpdateDate": "07/10/2022",
      "mobileNumber": null,
      "values": [],
      "notifyDays": 12,
      "terminationFiles": null,
      "employeeCode": "MB0010",
      "positionName": "Giám đốc khối CN. 3 tháng 2",
      "allowanceFiles": null,
      "ghi_chu": "Test chức năng",
      "contractExpriedDate": null,
      "email": null,
      "createDate": "07/10/2022",
      "jobName": "Giám đốc khối",
      "lastUpdatedBy": "mb0001",
      "orgName": "CN. 3 tháng 2 - Ban giám đốc - Ban giám đốc",
      "personalIdPlace": null,
      "fullName": "Nguyễn Thành Tuân",
      "contractNumber": null,
      "employeeId": 3,
      "liquidationId": 6,
      "contractTypeId": 2,
      "currentAddress": null,
      "boi_hoan_sau_giam_tru": 800000,
      "positionId": 44474,
      "createdBy": "mb0001",
      "flagStatus": 1,
      "contractTypeName": "Hợp đồng lao động 12 tháng",
      "interviewFiles": null,
      "personalIdDate": null,
      "tong_hoi_hoan": 1000000
    },
    {
      "reason": "aaaaa",
      "personalId": null,
      "lastWorkingDate": "10/09/2022",
      "handOverFiles": null,
      "lastUpdateDate": null,
      "mobileNumber": null,
      "values": [],
      "notifyDays": 10,
      "terminationFiles": null,
      "employeeCode": "MB0020",
      "positionName": "Chuyên viên dịch vụ Khách hàng 248 ccccccccccccccc",
      "allowanceFiles": null,
      "ghi_chu": "Test chức năng",
      "contractExpriedDate": null,
      "email": null,
      "createDate": null,
      "jobName": "Chuyên viên dịch vụ Khách hàng 248",
      "lastUpdatedBy": "hatq",
      "orgName": "CN. Quảng Ninh - P. Khách hàng Cá nhân - P. Khách hàng Cá nhân",
      "personalIdPlace": null,
      "fullName": "Cao Anh Tuấn",
      "contractNumber": null,
      "employeeId": 13,
      "liquidationId": 2,
      "contractTypeId": 4,
      "currentAddress": null,
      "boi_hoan_sau_giam_tru": 800000,
      "positionId": 15866,
      "createdBy": null,
      "flagStatus": 1,
      "contractTypeName": "Hợp đồng lao động 36 tháng",
      "interviewFiles": null,
      "personalIdDate": null,
      "tong_hoi_hoan": 1000000
    }
  ],
    "count": 2
}
