import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Subscription} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import * as moment from 'moment';
import {CatalogModel} from "@hcm-mfe/partnership/ui/pns-contract/search-form";
import {SearchFormService} from '@hcm-mfe/partnership/data-access/pns-contract/services';
import {DateValidator} from "@hcm-mfe/shared/common/validators";
import {HttpParams} from '@angular/common/http';
import {StringUtils} from '@hcm-mfe/shared/common/utils';
import {Constant, FunctionCode, UrlConstant} from '@hcm-mfe/partnership/data-access/pns-contract/common';
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {ContractType} from "@hcm-mfe/partnership/data-access/pns-contract/models";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import { Scopes } from '@hcm-mfe/shared/common/enums';

@Component({
    selector: 'app-search-form-approval',
    templateUrl: './search-form-approval.component.html',
    styleUrls: ['./search-form-approval.component.scss']
})
export class SearchFormApprovalComponent implements OnInit, OnDestroy {

    form!: FormGroup;
    contractTypes: CatalogModel[] = [];      // list loai hop dong
    statuses: CatalogModel[] = [];           // list trang thai
    positions: CatalogModel[] = [];          // list chuc danh
    functionCode = FunctionCode.PNS_CONTRACT_APPROVE;
    scope = Scopes.VIEW;
    isSubmitted = false;

    subs: Subscription[] = [];

    constructor(
        private fb: FormBuilder,
        private translate: TranslateService,
        private toastrService: ToastrService,
        private searchFormService: SearchFormService
    ) {
    }

    ngOnInit(): void {
        this.getListPosition();
        this.getListContractType();
        this.initDataSelect();
        this.initFormGroup();
    }

    get formControl() {
        return this.form.controls;
    }

    initFormGroup() {
        this.form = this.fb.group({
            contractType: null,             // loai hop dong
            status: null,                   // trang thai
            employeeCode: null,
            fullName: null,
            position: null,                 // chuc danh
            fromDate: null,                 // thoi gian hieu luc tu
            toDate: null,                    // thoi gian hieu luc den
            orgId: null                    // Ðon v?
        }, {
            validators: [
                DateValidator.validateRangeDate('fromDate', 'toDate', 'rangeDateError')
            ]
        });
    }

    parseOptions() {
        let params = new HttpParams();

        if (this.formControl['orgId'].value !== null)
            params = params.set('orgId', this.formControl['orgId'].value?.orgId);

        if (this.form.controls['contractType'].value !== null)
            params = params.set('listContractTypeId', this.form.controls['contractType'].value);

        if (this.form.controls['status'].value !== null)
            params = params.set('listStatus', this.form.controls['status'].value.join(','));

        if (!StringUtils.isNullOrEmpty(this.form.controls['employeeCode'].value))
            params = params.set('employeeCode', this.form.controls['employeeCode'].value);

        if (!StringUtils.isNullOrEmpty(this.form.controls['fullName'].value))
            params = params.set('fullName', this.form.controls['fullName'].value);

        if (this.form.controls['position'].value !== null)
            params = params.set('listPositionId', this.form.controls['position'].value.join(','));

        if (this.form.controls['fromDate'].value !== null)
            params = params.set('fromDate', moment(this.form.controls['fromDate'].value).format('DD/MM/YYYY'));

        if (this.form.controls['toDate'].value !== null)
            params = params.set('toDate', moment(this.form.controls['toDate'].value).format('DD/MM/YYYY'));

        return params;
    }

    getListContractType() {
        const params = new HttpParams();
        this.subs.push(
            this.searchFormService.getContractType(UrlConstant.CATALOGS.CONTRACT_TYPE, params).subscribe(res => {
                if (res.code === HTTP_STATUS_CODE.OK) {
                    this.contractTypes = res.data.map((item: ContractType) => {
                        item.label = item.name;
                        item.value = item.contractTypeId?.toString();
                        return item;
                    });
                }
            }, error => {
                this.toastrService.error(error.message);
                this.contractTypes = [];
            })
        );
    }

    getListPosition() {
        const params = new HttpParams().set('jobType', Constant.JOB_TYPE.ORG);
        this.subs.push(
            this.searchFormService.getListValue(UrlConstant.CATEGORY.MP_JOB, params).subscribe(res => {
                if (res.code === HTTP_STATUS_CODE.OK) {
                    this.positions = res.data;
                } else {
                    this.toastrService.error(res.message);
                }
            }, error => {
                this.toastrService.error(error.message);
                this.positions = [];
            })
        );
    }

    initDataSelect() {
        this.statuses = Constant.SIGN_STATUS.map((item => {
            item.label = this.translate.instant(item.label);
            return item;
        }));
    }

    setFormValue(event: NzSafeAny, formName: string) {
        if (event.listOfSelected?.length > 0) {
            this.formControl[formName].setValue(event.listOfSelected);
        }
    }

    ngOnDestroy(): void {
        this.subs?.forEach(sub => sub.unsubscribe());
    }

}
