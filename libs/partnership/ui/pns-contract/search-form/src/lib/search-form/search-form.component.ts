import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { HttpParams } from '@angular/common/http';
import * as moment from 'moment';
import {SearchFormService} from "@hcm-mfe/partnership/data-access/pns-contract/services";
import {StringUtils, Utils} from "@hcm-mfe/shared/common/utils";
import {Constant, FunctionCode, UrlConstant} from "@hcm-mfe/partnership/data-access/pns-contract/common";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import { Scopes } from '@hcm-mfe/shared/common/enums';


export class CatalogModel {
  value: number | string | null;
  label: string;

  constructor(label: string, value: number | string | null) {
    this.value = value;
    this.label = label;
  }
}

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {
  @Input() functionCode?: string;
  @Input() scope = Scopes.VIEW;

  form!: FormGroup;
  listEmpTypeCode: CatalogModel[] = [];
  listPosition: CatalogModel[] = [];
  statusEmployeeList: CatalogModel[] = [];
  subscriptions: Subscription[] = [];
  listType: CatalogModel[] = [];
  listContractType: CatalogModel[] = [];
  listStatusSign: CatalogModel[] = [];

  constructor(private fb: FormBuilder,
              private translate: TranslateService,
              private toastrService: ToastrService,
              private searchFormService: SearchFormService) {
  }

  ngOnInit(): void {
    this.getListEmpTypeCode();
    this.getListPosition();
    this.getListContractType();
    this.initDataSelect();
    this.initFormGroup();
  }

  initFormGroup() {
    this.form = this.fb.group({
      fullName: null,
      employeeCode: null,
      orgId: null,
      listStatus: null,
      type: ['1'],
      listContractTypeId: null,
      listPositionId: null,
      fromDate: [Utils.getFirstDayOrLastDayOfCurrentMonth(true)],
      toDate: [Utils.getFirstDayOrLastDayOfCurrentMonth(false)],
      signer: null,
      signerPosition: null
    });
  }

  getListContractType() {
    const params = new HttpParams();
    this.subscriptions.push(
      this.searchFormService.getContractType(UrlConstant.CATALOGS.CONTRACT_TYPE, params).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listContractType = res.data.map((item: NzSafeAny) => {
            item.label = item.name;
            item.value = item.contractTypeId.toString();
            return item;
          });
        }
      }, error => {
        this.toastrService.error(error.message);
        this.listContractType = [];
      })
    );
  }

  getListEmpTypeCode() {
    const params = new HttpParams().set('typeCode', Constant.TYPE_CODE.DOI_TUONG_CV);
    this.subscriptions.push(
      this.searchFormService.getListValue(UrlConstant.CATEGORY.LOOKUP_VALUE, params).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listEmpTypeCode = res.data;
        }
      }, error => {
        this.toastrService.error(error.message);
        this.listEmpTypeCode = [];
      })
    );
  }

  getListPosition() {
    const params = new HttpParams().set('jobType', Constant.JOB_TYPE.ORG);
    this.subscriptions.push(
      this.searchFormService.getListValue(UrlConstant.CATEGORY.MP_JOB, params).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listPosition = res.data;
        } else {
          this.toastrService.error(res.message);
        }
      }, error => {
        this.toastrService.error(error.message);
        this.listPosition = [];
      })
    );
  }

  initDataSelect() {
    this.statusEmployeeList = Constant.STATUSES_EMPLOYEE.map(item => {
      item.label = this.translate.instant(item.label);
      return item;
    });

    this.listType = Constant.CONTRACT_TYPE.map((item => {
      item.label = this.translate.instant(item.label);
      return item;
    }));

    this.listStatusSign = Constant.SIGN_STATUS.map((item => {
      item.label = this.translate.instant(item.label);
      return item;
    }));
  }

  get formControl() {
    return this.form.controls;
  }

  parseOptions() {
    let params = new HttpParams();

    if (!StringUtils.isNullOrEmpty(this.formControl['fullName'].value))
      params = params.set('fullName', this.formControl['fullName'].value);

    if (!StringUtils.isNullOrEmpty(this.formControl['employeeCode'].value))
      params = params.set('employeeCode', this.formControl['employeeCode'].value);

    if (this.formControl['orgId'].value !== null)
      params = params.set('orgId', this.formControl['orgId'].value?.orgId);

    if (this.formControl['signer'].value !== null)
      params = params.set('signerId', this.formControl['signer'].value?.employeeId);

    if (!StringUtils.isNullOrEmpty(this.formControl['signerPosition'].value))
      params = params.set('signerPosition', this.formControl['signerPosition'].value);

    if (this.formControl['listContractTypeId'].value !== null)
      params = params.set('listContractTypeId', this.formControl['listContractTypeId'].value);

    if (this.formControl['listPositionId'].value !== null)
      params = params.set('listPositionId', this.formControl['listPositionId'].value.join(','));

    if (this.formControl['listStatus'].value !== null)
      params = params.set('listStatus', this.formControl['listStatus'].value.join(','));

    if (this.formControl['type'].value !== null)
      params = params.set('type', this.formControl['type'].value);

    if (this.formControl['fromDate'].value !== null)
      params = params.set('fromDate', moment(this.formControl['fromDate'].value).format('DD/MM/YYYY'));

    if (this.formControl['toDate'].value !== null)
      params = params.set('toDate', moment(this.formControl['toDate'].value).format('DD/MM/YYYY'));

    return params;
  }


  setFormValue(event: NzSafeAny, formName: string) {
    if (event.listOfSelected.length > 0) {
      this.formControl[formName].setValue(event.listOfSelected);
    }
  }

  ngOnDestroy(): void {
    this.subscriptions?.forEach(sub => sub.unsubscribe());
  }

}
