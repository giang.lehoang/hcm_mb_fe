import {Component, EventEmitter, Input, Output} from '@angular/core';
import {NzUploadFile} from "ng-zorro-antd/upload";
import {beforeUploadFile, getTypeExport} from "@hcm-mfe/shared/common/utils";
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";
import {DownloadFileAttachService} from "@hcm-mfe/partnership/data-access/pns-contract/models";
import {saveAs} from "file-saver";

@Component({
    selector: 'btn-upload-file',
    templateUrl: './btn-upload-file.component.html',
    styleUrls: ['./btn-upload-file.component.scss']
})
export class BtnUploadFileComponent {

    @Input() isSubmitted = false;
    @Input() labelText = '';
    @Input() hintText = '';
    @Input() isRequired = false;
    @Input() fileExtensionAccept = '';

    @Input() fileList: NzUploadFile[] = [];
    @Output() fileListChange = new EventEmitter<NzUploadFile[]>();

    @Input() docIdsDelete: number[] = [];
    @Output() docIdsDeleteChange = new EventEmitter<number[]>();

    constructor(
        private toastService: ToastrService,
        private translate: TranslateService,
        private downloadFileAttachService: DownloadFileAttachService
    ) {
    }

    beforeUpload = (file: NzUploadFile): boolean => {
        this.fileList = beforeUploadFile(file, this.fileList, 3000000, this.toastService, this.translate, 'common.notification.fileExtensionInvalidTaxRegister', '.TIF', '.JPG', '.GIF', '.PNG', '.PDF', '.DOC', '.DOCX', '.XLS', '.XLSX');
        this.fileListChange.emit(this.fileList);
        return false;
    };

    downloadFile = (file: NzUploadFile) => {
        if (file.uid && file.url) {
            const fileName = file.name;
            this.downloadFileAttachService.doTaxDownloadAttachFile(~~file?.uid, file?.url).subscribe(res => {
                if (res) {
                    const reportFile = new Blob([res.body], { type: getTypeExport(fileName.split('.').pop() ?? '') });
                    saveAs(reportFile, fileName);
                }
            }, () => {
                this.toastService.error(this.translate.instant('partnership.notification.downloadFileError'));
            })
        }
    };

    removeFile = (file: NzUploadFile): boolean => {
        this.docIdsDelete.push(Number(file.uid));
        this.docIdsDeleteChange.emit(this.docIdsDelete);
        const index = this.fileList.indexOf(file);
        this.fileList.splice(index, 1);
        this.fileListChange.emit(this.fileList);
        return false;
    };

}
