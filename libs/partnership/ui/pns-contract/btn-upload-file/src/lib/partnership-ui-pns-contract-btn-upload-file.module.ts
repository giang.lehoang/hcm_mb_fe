import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BtnUploadFileComponent } from './btn-upload-file/btn-upload-file.component';
import {NzUploadModule} from "ng-zorro-antd/upload";
import {TranslateModule} from "@ngx-translate/core";
import {NzIconModule} from "ng-zorro-antd/icon";

@NgModule({
    imports: [CommonModule, NzUploadModule, TranslateModule, NzIconModule],
    declarations: [
        BtnUploadFileComponent
    ],
    exports: [
        BtnUploadFileComponent
    ]
})
export class PartnershipUiPnsContractBtnUploadFileModule {}
