import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import {CatalogModel} from "@hcm-mfe/partnership/ui/pns-contract/search-form";
import {ContractProposalService} from "@hcm-mfe/partnership/data-access/pns-contract/services";
import {Constant} from "@hcm-mfe/partnership/data-access/pns-contract/common";
import {beforeUploadFile} from "@hcm-mfe/shared/common/utils";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {Subscription} from "rxjs";
import {NzUploadFile} from "ng-zorro-antd/upload";
import {NzSafeAny} from "ng-zorro-antd/core/types";

@Component({
  selector: 'app-import-common',
  templateUrl: './import-common.component.html',
  styleUrls: ['./import-common.component.scss']
})
export class ImportCommonComponent implements OnInit {

  @Input() showContent = false;
  @Output() onCloseModal = new EventEmitter<boolean>();


  fileList: NzUploadFile[] = [];
  fileName: string | undefined;

  nzWidth: number;
  listType: CatalogModel[] = [];
  typeContract = '1';
  isExistFileImport = false;
  subs: Subscription[] = [];

  constructor(private toastrService: ToastrService,
              private translate: TranslateService,
              private contractProposalService: ContractProposalService) {
    this.nzWidth = window.innerWidth > 767 ? window.innerWidth / 3 : window.innerWidth / 2.5;
  }

  ngOnInit(): void {
    this.getListType();
  }

  getListType() {
    this.listType = Constant.CONTRACT_TYPE.map((item => {
      item.label = this.translate.instant(item.label);
      return item;
    }));
  }

  doClose(refresh: boolean = false) {
    this.onCloseModal.emit(refresh);
    this.fileList = [];
  }

  beforeUpload = (file: NzUploadFile): boolean => {
    this.fileList = beforeUploadFile(file, this.fileList, 1500000000, this.toastrService, this.translate, 'common.notification.errorAttachFile', '.ZIP', '.PDF');
    if (this.fileList.length > 0) {
      this.isExistFileImport = true;
    }
    return false;
  };

  doImportFile() {
    const formData = new FormData();
    this.fileList.forEach((file: NzUploadFile) => {
      formData.append('files', file as NzSafeAny);
    });
    formData.append('type', new Blob([JSON.stringify(this.typeContract)], { type: 'application/json' }));
    this.subs.push(
      this.contractProposalService.uploadFileSigned(formData).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.doClose(true);
          this.toastrService.success(this.translate.instant('partnership.pns-contract.notification.importSuccess'));
        } else if (res.code === HTTP_STATUS_CODE.CREATED) {
          this.toastrService.error(res.message);
        }
      }, error => this.toastrService.error(error.message))
    );
  }

  selectTypeSign(type: string) {
    this.isExistFileImport = (type != null && this.fileList.length > 0);
  }

  ngOnDestroy() {
    this.subs?.forEach((s) => s.unsubscribe());
  }


}

