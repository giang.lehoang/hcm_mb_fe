import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedUiOrgDataPickerModule} from "@hcm-mfe/shared/ui/org-data-picker";
import {SharedUiMbSelectCheckAbleModule} from "@hcm-mfe/shared/ui/mb-select-check-able";
import {SharedUiMbInputTextModule} from "@hcm-mfe/shared/ui/mb-input-text";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NzFormModule} from "ng-zorro-antd/form";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {SharedUiMbEmployeeDataPickerModule} from "@hcm-mfe/shared/ui/mb-employee-data-picker";
import { SearchFormContractTypeComponent } from './search-form-contract-type/search-form-contract-type.component';

@NgModule({
  imports: [CommonModule, SharedUiOrgDataPickerModule, SharedUiMbSelectCheckAbleModule, SharedUiMbInputTextModule,
    SharedUiMbDatePickerModule, FormsModule, ReactiveFormsModule, NzFormModule, TranslateModule, SharedUiMbSelectModule, SharedUiMbEmployeeDataPickerModule],
  declarations: [SearchFormContractTypeComponent],
  exports: [SearchFormContractTypeComponent],
})
export class PartnershipUiSearchFormContractTypeModule {}
