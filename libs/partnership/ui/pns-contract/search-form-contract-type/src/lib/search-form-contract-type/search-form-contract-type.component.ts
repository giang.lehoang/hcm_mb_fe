import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { HttpParams } from '@angular/common/http';
import {CatalogModel} from "@hcm-mfe/partnership/ui/pns-contract/search-form";
import { SearchFormService } from '@hcm-mfe/partnership/data-access/pns-contract/services';
import { Constant, UrlConstant } from '@hcm-mfe/partnership/data-access/pns-contract/common';
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import { StringUtils } from '@hcm-mfe/shared/common/utils';
import {SelectModal} from "@hcm-mfe/shared/data-access/models";
import {NzSafeAny} from "ng-zorro-antd/core/types";

@Component({
  selector: 'app-search-form-contract-type',
  templateUrl: './search-form-contract-type.component.html',
  styleUrls: ['./search-form-contract-type.component.scss']
})
export class SearchFormContractTypeComponent implements OnInit {

  form!: FormGroup;
  listEmpType: CatalogModel[] = [];

  subscriptions: Subscription[] = [];
  listClassify: CatalogModel[] = [];


  constructor(private fb: FormBuilder,
              private translate: TranslateService,
              private toastrService: ToastrService,
              private searchFormService: SearchFormService) {
  }

  ngOnInit(): void {
    this.getListEmpTypeCode();
    this.getListClassify();
    this.initFormGroup();
  }

  initFormGroup() {
    this.form = this.fb.group({
      name: null,
      listClassifyCode: null,
      listEmpTypeCode: null
    });
  }

  getListEmpTypeCode() {
    const params = new HttpParams().set('typeCode', Constant.TYPE_CODE.DOI_TUONG_CV);
    this.subscriptions.push(
      this.searchFormService.getListValue(UrlConstant.CATEGORY.LOOKUP_VALUE, params).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listEmpType = res.data;
        }
      }, error => {
        this.toastrService.error(error.message);
        this.listEmpType = [];
      })
    );
  }

  getListClassify() {
    const params = new HttpParams().set('typeCode', Constant.TYPE_CODE.PHAN_LOAI_HD);
    this.subscriptions.push(
      this.searchFormService.getListValue(UrlConstant.CATEGORY.LOOKUP_VALUE, params).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listClassify = res.data;
        }
      }, error => {
        this.toastrService.error(error.message);
        this.listClassify = [];
      })
    );
  }

  get formControl() {
    return this.form.controls;
  }

  parseOptions() {
    let params = new HttpParams();

    if (!StringUtils.isNullOrEmpty(this.formControl['name'].value))
      params = params.set('name', this.formControl['name'].value);

    if (this.formControl['listClassifyCode'].value !== null)
      params = params.set('listClassifyCode', this.formControl['listClassifyCode'].value.join(','));

    if (this.formControl['listEmpTypeCode'].value !== null)
      params = params.set('listEmpTypeCode', this.formControl['listEmpTypeCode'].value.join(','));

    return params;
  }


  setFormValue(event: NzSafeAny, formName: string) {
    if (event.listOfSelected?.length > 0) {
      this.formControl[formName].setValue(event.listOfSelected);
    }
  }

  ngOnDestroy(): void {
    this.subscriptions?.forEach(sub => sub.unsubscribe());
  }

}
