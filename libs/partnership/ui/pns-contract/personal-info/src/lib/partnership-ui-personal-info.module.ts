import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PersonalInfoComponent} from "./personal-info/personal-info.component";
import {SharedUiMbTextLabelModule} from "@hcm-mfe/shared/ui/mb-text-label";
import {NzFormModule} from "ng-zorro-antd/form";
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  imports: [CommonModule, SharedUiMbTextLabelModule, NzFormModule, TranslateModule],
  declarations: [PersonalInfoComponent],
  exports: [PersonalInfoComponent]
})
export class PartnershipUiPersonalInfoModule {}
