import {Component, Input, OnChanges} from '@angular/core';
import {Subscription} from 'rxjs';
import {ToastrService} from 'ngx-toastr';
import {PersonalInfo} from "@hcm-mfe/partnership/data-access/pns-contract/models";
import {PersonalInfoService} from "@hcm-mfe/partnership/data-access/pns-contract/services";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";

@Component({
  selector: 'app-personal-info',
  templateUrl: './personal-info.component.html',
  styleUrls: ['./personal-info.component.scss']
})
export class PersonalInfoComponent implements OnChanges {

  @Input() employeeId!: number;
  @Input() isShowEmpType = true;

  personalInfo!: PersonalInfo;
  subs: Subscription[] = [];

  constructor(
    private personalInfoService: PersonalInfoService,
    private toastrService: ToastrService
  ) {
  }

  ngOnChanges() {
    if (this.employeeId) {
      this.getPersonalInfo();
    }
  }

  getPersonalInfo() {
    this.personalInfoService.getPersonalInfo(this.employeeId).subscribe(res => {
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.personalInfo = res.data;
      }
    }, error => {
      this.toastrService.error(error.message);
    });
  }

  ngOnDestroy(): void {
    this.subs?.forEach(sub => sub.unsubscribe());
  }

}
