import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { saveAs } from 'file-saver';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import {beforeUploadFile, getTypeExport} from "@hcm-mfe/shared/common/utils";
import {HTTP_STATUS_CODE, MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";
import {NzUploadFile} from "ng-zorro-antd/upload";
import {ImportService, SearchFormService} from "@hcm-mfe/partnership/data-access/pns-contract/services";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {FormArray, FormBuilder, FormGroup} from "@angular/forms";
import {FileDocument} from "@hcm-mfe/partnership/data-access/pns-contract/models";
import {
  Catalog
} from "@hcm-mfe/partnership/feature/pns-contract/contract-liquidation/contract-liquidation-suggestion-edit";
import {HttpParams} from "@angular/common/http";
import {Constant, UrlConstant} from "@hcm-mfe/partnership/data-access/pns-contract/common";
import {Subscription} from "rxjs";
import {CommonUtils} from "@hcm-mfe/shared/core";

export class ErrorImportLiquidation {
  column?: number;
  row?: number;
  columnLabel?: string;
  content?: string;
  description?: string;
}


@Component({
  selector: 'import-file-liquidation',
  templateUrl: './import-file-liquidation.component.html',
  styleUrls: ['./import-file-liquidation.component.scss']
})
export class ImportFileLiquidationComponent implements OnInit {

  @Input() showContent = false;
  @Input() isRequiredFileAttach = false;
  @Input() closeModalWhenClick = true;
  @Input() urlApiImport: string = '';
  @Input() urlApiDownloadTemp: string = '';
  @Output() onCloseModal: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() onLoadPage: EventEmitter<boolean> = new EventEmitter<boolean>();

  isModalError = false;
  errorList: ErrorImportLiquidation[] = [];
  fileList: NzUploadFile[] = [];
  fileName?: string;
  fileImportName = '';
  fileImportSize?: string;
  isSubmitted=false;

  isExistFileImport = false;
  isRequired = false;
  nzWidth: number;
  nzWidthError: number;
  isHiddenDownloadErrorFile = true;

  listDocumentType: Catalog[] = [];
  form: FormGroup = this.fb.group({
    listFileDocument: this.fb.array([]),
  })
  readonly serviceName: string = MICRO_SERVICE.CONTRACT;
  subs: Subscription[] = [];

  constructor(
    private readonly importService: ImportService,
    private toastrService: ToastrService,
    private translate: TranslateService,
    private fb: FormBuilder,
    private searchFormService: SearchFormService,
  ) {
    this.nzWidth = window.innerWidth > 767 ? 1100 : window.innerWidth / 1.5;
    this.nzWidthError = window.innerWidth > 767 ? window.innerWidth / 2 : window.innerWidth / 1.5;
  }

  ngOnInit(): void {
    this.isRequired = this.isRequiredFileAttach;
    this.addFileDocument();
    this.getListDocument();
  }

  doClose(isSearch?: boolean) {
    this.listFileDocument.controls.forEach((item: FormGroup) => {
      item.get('fileAttachments')?.setValue(null);
      item.get('listDocuments')?.setValue(null);
    });
    this.fileList = [];
    this.showContent = false;
    this.onCloseModal.emit(isSearch);
    this.fileImportName = '';
    this.isExistFileImport = false;
  }

  doDownloadTemplate() {
    this.onLoadPage.emit(true);
    this.importService.downloadTemplate(this.urlApiDownloadTemp).subscribe(res => {
      const arr = res.headers.get("Content-Disposition")?.split(';');
      const fileName: string = arr[arr.length - 1].replace('filename=', '').trim();
      const reportFile = new Blob([res.body], { type: getTypeExport('xlsx') });
      saveAs(reportFile, fileName);
      this.onLoadPage.emit(false);
    });
  }

  doCloseModal() {
    this.isModalError = false;
  }

  doDownloadFile() {
    this.onLoadPage.emit(true);
    this.importService.doDownloadFileByName(this.fileName ?? '').subscribe(res => {
      this.onLoadPage.emit(false);
      const reportFile = new Blob([res.body], { type: getTypeExport('xlsx') });
      saveAs(reportFile, 'file_error_information.xlsx');
      this.isHiddenDownloadErrorFile = false;
    }, error => {
      this.onLoadPage.emit(false);
      this.toastrService.error(error.message);
    })
  }

  beforeUpload = (file: NzUploadFile) => {
    this.fileList = [];
    this.fileList =  beforeUploadFile(file, this.fileList, 3000000, this.toastrService, this.translate, 'partnership.pns-contract.label.limitSize')
    this.isExistFileImport = true;
    this.fileImportName = file.name;
    if (file.size) {
      this.fileImportSize = (file.size/1000000).toFixed(2);
    }
    return false;
  };


  doImportFile() {
    // Tạo form data
    if (this.form.valid && this.fileList.length > 0) {
      this.isHiddenDownloadErrorFile = true;
      const data = {
        fileImport: this.fileList[0],
        listFileDocument: this.form.controls['listFileDocument'].value
      }
      this.isSubmitted=true;
      this.importService.doImport(this.urlApiImport, CommonUtils.convertFormFile(data)).subscribe(res => {
        this.isSubmitted=false;
        if (res != undefined && res.code != HTTP_STATUS_CODE.OK) {
          this.errorList = res.data.errorList;
          if (this.errorList != undefined && this.errorList.length > 0) {
            this.isModalError = true;
            this.fileName = res.data.errorFile;
            this.toastrService.error(this.translate.instant('partnership.pns-contract.label.error'));
          } else {
            this.isModalError = false;
            this.errorList = [];
            this.fileName = undefined;
            this.toastrService.error(res.message);
          }
        } else {
          this.isModalError = false;
          this.errorList = [];
          this.fileName = undefined;
          this.doClose(true);
          this.toastrService.success(this.translate.instant('partnership.pns-contract.label.success'));
        }
      }, error => {
        this.isSubmitted=false;
        this.toastrService.error(error.message);
      });
    } else {
      this.toastrService.error(this.translate.instant("partnership.pns-contract.label.emptyImport"));
    }
  }

  doRemoveFile = (): boolean => {
    this.fileList = [];
    this.isExistFileImport = false;
    return true;
  };

  getFileName(fileImportName: string): string {
    return fileImportName.length > 40 ? fileImportName.substring(0, 40) + '...' : fileImportName;
  }

  getListDocument() {
    const params = new HttpParams().set('typeCode', Constant.TYPE_CODE.HO_SO_THANH_LY_HD);
    this.subs.push(
      this.searchFormService.getListValue(UrlConstant.CATEGORY.LOOKUP_VALUE, params).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listDocumentType = res.data;
          res.data.forEach((item: Catalog) => {
            if (item.parentValue) {
              this.addFileDocument({type: item.value});
            }
          });
          if (res.data.some((item: any) => item.parentValue != null)) {
            this.removeFileDocument(0);
          }
        }
      }, error => {
        this.toastrService.error(error.message);
      })
    );
  }

  get listFileDocument(): NzSafeAny {
    return this.form.controls['listFileDocument'] as FormArray;
  }

  addFileDocument(item?: FileDocument) {
    item?.listDocuments?.forEach(el => {
      el.uid = el.docId;
      el.name = el.fileName;
    })
    if (this.listFileDocument.valid) {
      const fileDocument = this.fb.group({
        type: [item?.type ? item.type : null],
        fileAttachments: [null],
        listDocuments: [item?.listDocuments ? item?.listDocuments : null],
        canNotDelete: [this.listDocumentType.some(el => el.value === item?.type && el.parentValue)],
        docIdsDelete: [],
      });
      this.listFileDocument.push(fileDocument);
    } else this.toastrService.error(this.translate.instant("partnership.pns-contract.label.emptyDocument"));
  }

  removeFileDocument(index: number) {

    if (this.listFileDocument.length > 1)
      this.listFileDocument.removeAt(index);
    else {
      this.listFileDocument.removeAt(index);
      this.addFileDocument();
    }
  }

  onFileListChange(listFile: NzUploadFile[], item: NzSafeAny) {
    const listFileFilter: NzUploadFile[] = [];
    if (Array.isArray(listFile)) {
      for (let i = 0; i < listFile.length; i++) {
        if (listFile[i] instanceof File) {
          if (listFile[i].size) {
            listFileFilter.push(listFile[i])
          }
        }
      }
    }
    item.get('fileAttachments').setValue(listFileFilter);
  }

  removeFile(event: number[], item: NzSafeAny) {
    if (event && event.length > 0) {
      item.get('docIdsDelete').setValue(event.filter((docId: number) => !isNaN(docId)));
    }
  }
}
