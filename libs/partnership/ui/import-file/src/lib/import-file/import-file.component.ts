import { Component, EventEmitter, Input, Output } from '@angular/core';
import { saveAs } from 'file-saver';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import {beforeUploadFile, getTypeExport} from "@hcm-mfe/shared/common/utils";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {NzUploadFile} from "ng-zorro-antd/upload";
import {ImportService} from "@hcm-mfe/partnership/data-access/pns-contract/services";

export class ErrorImport {
  column?: number;
  row?: number;
  columnLabel?: string;
  content?: string;
  description?: string;
}


@Component({
  selector: 'import-file',
  templateUrl: './import-file.component.html',
  styleUrls: ['./import-file.component.scss']
})
export class ImportFileComponent {

  @Input() showContent = false;
  @Input() isRequiredFileAttach = false;
  @Input() isMultiple = false;
  @Input() isShowAttachFile = true;
  @Input() closeModalWhenClick = true;
  @Input() urlApiImport: string = '';
  @Input() urlApiDownloadTemp: string = '';
  @Output() onCloseModal: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() onLoadPage: EventEmitter<boolean> = new EventEmitter<boolean>();

  isModalError = false;
  errorList: ErrorImport[] = [];
  fileList: NzUploadFile[] = [];
  fileAttachList: NzUploadFile[] = [];
  fileName?: string;
  fileImportName = '';
  fileImportSize?: string;
  isSubmitted=false;

  isExistFileImport = false;
  isRequired = false;
  nzWidth: number;
  nzWidthError: number;
  isHiddenDownloadErrorFile = true;

  constructor(
    private readonly importService: ImportService,
    private toastrService: ToastrService,
    private translate: TranslateService
  ) {
    this.nzWidth = window.innerWidth > 767 ? window.innerWidth / 3 : window.innerWidth / 2.5;
    this.nzWidthError = window.innerWidth > 767 ? window.innerWidth / 2 : window.innerWidth / 1.5;
  }

  ngOnInit(): void {
    this.isRequired = this.isRequiredFileAttach;
  }

  doClose(isSearch?: boolean) {
    this.fileList = [];
    this.showContent = false;
    this.onCloseModal.emit(isSearch);
    this.fileImportName = '';
    this.isExistFileImport = false;
  }

  doDownloadTemplate() {
    this.onLoadPage.emit(true);
    this.importService.downloadTemplate(this.urlApiDownloadTemp).subscribe(res => {
      const arr = res.headers.get("Content-Disposition")?.split(';');
      const fileName: string = arr[arr.length - 1].replace('filename=', '').trim();
      const reportFile = new Blob([res.body], { type: getTypeExport('xlsx') });
      saveAs(reportFile, fileName);
      this.onLoadPage.emit(false);
    });
  }

  doCloseModal() {
    this.isModalError = false;
  }

  doDownloadFile() {
    this.onLoadPage.emit(true);
    this.importService.doDownloadFileByName(this.fileName ?? '').subscribe(res => {
      this.onLoadPage.emit(false);
      const reportFile = new Blob([res.body], { type: getTypeExport('xlsx') });
      saveAs(reportFile, 'file_error_information.xlsx');
      this.isHiddenDownloadErrorFile = false;
    }, error => {
      this.onLoadPage.emit(false);
      this.toastrService.error(error.message);
    })
  }

  beforeUpload = (file: NzUploadFile) => {
    this.fileList = [];
    this.fileList =  beforeUploadFile(file, this.fileList, 3000000, this.toastrService, this.translate, 'partnership.pns-contract.label.limitSize')
    this.isExistFileImport = true;
    this.fileImportName = file.name;
    if (file.size) {
      this.fileImportSize = (file.size/1000000).toFixed(2);
    }
    return false;
  };

  beforeUploadAttach = (file: NzUploadFile) => {
    // this.fileAttachList = [];
    this.fileAttachList =  beforeUploadFile(file, this.fileAttachList, 3000000, this.toastrService, this.translate, 'partnership.pns-contract.label.limitSize')
    if (this.isRequiredFileAttach) {
      this.isRequired = false;
    }
    return false;
  };

  doImportFile() {
    // Tạo form data
    this.isHiddenDownloadErrorFile = true;
    const formData = new FormData();
    this.fileList.forEach((file: NzUploadFile) => {
      formData.append('fileImport', file as any);
    });
    this.fileAttachList.forEach((file: NzUploadFile) => {
      formData.append('attachFiles', file as any);
    });

    // Tiến hành import
    this.isSubmitted=true;
    this.importService.doImport(this.urlApiImport, formData).subscribe(res => {
      this.isSubmitted=false;
      if (res != undefined && res.code != HTTP_STATUS_CODE.OK) {
        this.errorList = res.data.errorList;
        if (this.errorList != undefined && this.errorList.length > 0) {
          this.isModalError = true;
          this.fileName = res.data.errorFile;
          this.toastrService.error(this.translate.instant('partnership.pns-contract.label.error'));
        } else {
          this.isModalError = false;
          this.errorList = [];
          this.fileName = undefined;
          this.toastrService.error(res.message);
        }
      } else {
        this.isModalError = false;
        this.errorList = [];
        this.fileName = undefined;
        this.doClose(true);
        this.toastrService.success(this.translate.instant('partnership.pns-contract.label.success'));
      }
    }, error => {
      this.isSubmitted=false;
      this.toastrService.error(error.message);
    });
  }

  doRemoveFile = (): boolean => {
    this.fileList = [];
    this.isExistFileImport = false;
    return true;
  };

  doRemoveFileAttach = (): boolean => {
    this.fileAttachList = [];
    if (this.isRequiredFileAttach) {
      this.isRequired = true;
    }
    return true;
  };

  getFileName(fileImportName: string): string {
    return fileImportName.length > 40 ? fileImportName.substring(0, 40) + '...' : fileImportName;
  }
}
