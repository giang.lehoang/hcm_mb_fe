import { ComponentFixture, getTestBed, TestBed } from '@angular/core/testing';


import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { ToastrModule } from 'ngx-toastr';
import { NzModalModule, NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClient } from '@angular/common/http';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { ImportService } from '@hcm-mfe/partnership/data-access/pns-contract/services';
import { of, throwError } from 'rxjs';
import { ImportFileComponent } from './import-file.component';
import { NzUploadFile } from 'ng-zorro-antd/upload';

describe('ImportFileComponent', () => {
  let component: ImportFileComponent;
  let fixture: ComponentFixture<ImportFileComponent>;

  let loading: HTMLElement;
  let baseService: BaseService;
  let service: ImportService;
  let httpClient: HttpClient;
  let injector: TestBed;
  let spyDownloadTemplate: any;
  let spyDownloadFileByName: any;
  let spyImport: any;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ImportFileComponent],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule.forRoot(),
        ToastrModule.forRoot(),
        NzModalModule,
        BrowserAnimationsModule,
        NoopAnimationsModule

      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportFileComponent);
    component = fixture.componentInstance;

    loading = fixture.nativeElement.querySelector('mb-button');

    injector = getTestBed();
    httpClient = injector.get(HttpClient);
    baseService = new BaseService(httpClient);
    service = injector.get(ImportService);
    spyDownloadTemplate = jest.spyOn(service, 'downloadTemplate').mockImplementation(() => of({ code: 200 }));
    spyImport = jest.spyOn(service, 'doImport').mockImplementation(() => of({ code: 200 }));
    spyDownloadFileByName = jest.spyOn(service, 'doDownloadFileByName').mockImplementation(() => of({ code: 200 }));

    component.configType = 'psn_dao_tao';
    component.urlApiImport = 'uri';
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('test doDownloadTemplate', () => {
    component.urlApiDownloadTemp = 'test';
    component.doDownloadTemplate();
    expect(true).toBeTruthy();
  });

  it('test doDownloadTemplate error', () => {
    spyDownloadTemplate.mockImplementation(() => throwError(() => new Error('error')))
    component.urlApiDownloadTemp = 'test';
    component.doDownloadTemplate();
    expect(true).toBeTruthy();
  });

  it('test doDownloadFile', () => {
    component.doDownloadFile()
    expect(true).toBeTruthy();
  });

  it('test doDownloadFile error', () => {
    spyDownloadFileByName.mockImplementation(() => throwError(() => new Error('error')))
    component.doDownloadFile()
    expect(true).toBeTruthy();
  });

  it('test beforeUpload ', () => {
    const file: NzUploadFile = {uid: '', name: '', size: 300000000, type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'}
    component.beforeUpload(file);
    expect(true).toBeTruthy();
  });

  it('test beforeUpload ', () => {
    const file: NzUploadFile = {uid: '', name: '', size: 100, type: ''}
    component.beforeUpload(file);
    expect(true).toBeTruthy();
  });

  it('test beforeUpload ', () => {
    const file: NzUploadFile = {uid: '', name: '', size: 100, type: 'application/vnd.ms-excel'}
    component.beforeUpload(file);
    expect(true).toBeTruthy();
  });

  it('test doImportFile ', () => {
    component.doImportFile();
    expect(true).toBeTruthy();
  });

  it('test doImportFile error', () => {
    spyImport.mockImplementation(() => throwError(() => new Error('error')));
    component.doImportFile();
    expect(true).toBeTruthy();
  });

  it('test doImportFile error', () => {
    spyImport.mockImplementation(() => of({data: {errorList: []}}));
    component.doImportFile();
    expect(true).toBeTruthy();
  });

  it('test doImportFile error', () => {
    spyImport.mockImplementation(() => of({data: {errorList: [1]}}));
    component.doImportFile();
    expect(true).toBeTruthy();
  });

  it('test beforeUpload', () => {
    component.doRemoveFile();
    expect(true).toBeTruthy();
  });

  it('test saveData error', () => {
    component.doCloseModal()
    expect(true).toBeTruthy();
  });
});
