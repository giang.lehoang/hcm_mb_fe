import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImportFileComponent } from './import-file/import-file.component';
import { TranslateModule } from '@ngx-translate/core';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzTableModule } from 'ng-zorro-antd/table';
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbSelectModule} from "@hcm-mfe/shared/ui/mb-select";
import {ReactiveFormsModule} from "@angular/forms";
import {SharedUiMbUploadModule} from "@hcm-mfe/shared/ui/mb-upload";
import {NzDividerModule} from "ng-zorro-antd/divider";
import {ImportFileLiquidationComponent} from "./import-file-liquidation/import-file-liquidation.component";

@NgModule({
  imports: [CommonModule, TranslateModule, NzModalModule, SharedUiMbButtonModule, NzFormModule, NzUploadModule, NzIconModule, NzTableModule, SharedUiLoadingModule, SharedUiMbSelectModule, ReactiveFormsModule, SharedUiMbUploadModule, NzDividerModule],
  declarations: [ImportFileComponent, ImportFileLiquidationComponent],
  exports: [ImportFileComponent, ImportFileLiquidationComponent]
})
export class PartnershipUiImportFileModule {}
