export interface SalaryInfo {
  fromDate?: string;
  employeeId?: number;
  salaryAmount?: number;
  salaryPercent?: number;
}
