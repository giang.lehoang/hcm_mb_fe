import { ContractProposalResponse } from './contract-proposal-response';
import { ContractEvaluationResponse } from './contract-evaluation-response';

export class ContractApproveResponse {
    id?: number;
    type?: number;
    contractApproverId?: number;
    contractProposalId?: number;
    approverId?: number;
    approverLevel?: number;
    contractTypeId?: number;
    note?: string;
    isLiquidation?: number;
    liquidationStatusOfNext?: number;  // trạng thái đánh giá của ông cấp trên
    status?: number;
    flagStatus?: number;
    createdBy?: string;
    employeeId?: number;
    employeeCode?: string;
    empName?: string;
    positionId?: number;
    positionName?: string;
    orgId?: number;
    orgName?: string;
    curContractTypeId?: number;
    curContractTypeName?: string;
    contractByLawId?: number;
    contractByLawName?: string;
    isDisciplined?: number;
    disciplinedName?: string;
    kpiPoint?: number;
    rankCode?: string;
    rankName?: string;
    signerId?: number;
    signerName?: string;
    signerPosition?: string;
    delegacyNo?: string;
    createDate?: string;
    lastUpdatedBy?: string;
    lastUpdateDate?: string;
    cursorCurrent?: number;
    fromDate?: string;
    toDate?: string;

    contractTerm?: string; // Thoi han hop dong

    // nguoi quan li truc tiep
    directManagerId?: number;
    directManagerName?: string;
    dmIsLiquidation?: number;
    dmLiquidationName?: string;
    dmContractTypeId?: number;
    dmContractTypeName?: string;
    // End: nguoi quan li truc tiep

    // cap phe duyet
    approvalLevelPersonId?: number;
    approvalLevelPersonName?: string;
    alIsLiquidation?: number;
    alLiquidationName?: string;
    alContractTypeId?: number;
    alContractTypeName?: string;
    // End: cap phe duyet

    // cap phe duyet thay doi
    changeApprovalLevelId?: number;
    changeApprovalLevelName?: string;
    calIsLiquidation?: number;
    calLiquidationName?: string;
    calContractTypeId?: number;
    calContractTypeName?: string;
    // End: cap phe duyet thay doi

    contractProposalsResponse?: ContractProposalResponse;
    contractEvaluationsResponse?: ContractEvaluationResponse;

    disabled?: boolean = false;
}
