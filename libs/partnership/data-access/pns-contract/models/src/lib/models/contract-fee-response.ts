
export interface ContractFeeResponse {
  contractFeeId: number;
  employeeId?: number;
  employeeCode?: string;
  fullName?: number;
  fromDate?: string | null;
  toDate?: string;
  flagStatus?: number;
  amountFee?: number;
  rejectReason?: string;
  status?: number;
}

export interface ContractFeeDTO {
  contractFeeId?: number;
  employeeId?: number;
  employeeCode?: string;
  fullName?: number;
  fromDate?: string | null;
  toDate?: string | null;
  flagStatus?: number;
  amountFee?: number;
  rejectReason?: string;
  status?: number;
}

