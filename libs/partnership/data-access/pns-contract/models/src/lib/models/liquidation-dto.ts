import { NzSafeAny } from 'ng-zorro-antd/core/types';

export interface LiquidationDTO {
    liquidationId?: number;
    employeeId?: number;
    personalEmail?: string;
    notifyDays?: number;
    lastWorkingDate?: string | null;
    reason?: string;
    reasonCode?: string;
    rejectReason?: string;
    positionId?: number;
    contractTypeId?: number;
    contractExpiredDate?: string;
    startRecord?: number;
    pageSize?: number;
    organizationId?: number;
    docIdsDelete?: number[];
    employeeCode?: string;
    fullName?: string;
    positionName?: string;
    orgName?: string;
    values?: Array<ListValues>;
    listFileDocument?: Array<any>;
    attachFiles?: Array<any>;
    listConsultationInfo?: Array<any>;
}

export interface ListValues {
  configCode: string;
  configName: string;
  dataType: string;
  displaySeq: number;
  value: NzSafeAny;
}
