
export interface LiquidationDetailLoad {
  ldNumber: number //Số LD :
  debtAmount: number //Dư nợ :
  openDate: string //Ngày mở:
  productCode: string //Mã sản phẩm:
  productName: string //Tên sản phẩm:
}

export interface LiquidationDetailCard {
  cardNumber: string //Số thẻ:
  debtAmount: number //Dư nợ:
  interestRate: string //Lãi suất thẻ:
  interestGroup: string //Nhóm lãi suất:
  productName: string //Loại sản phẩm:
  cardClass: string //Loại thẻ :
  accountStatus: number | string //Trạng thái :
}

