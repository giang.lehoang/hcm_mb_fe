import { ContractApprove } from './contract-approvers';

export interface ContractProposal {
  contractProposalId?: number;
  contractNumber?: string;
  amountSalary?: number;
  percentSalary?: number;
  contractTypeId?: number;
  fromDate?: string;
  toDate?: string;
  signerId?: number;
  signerPosition?: string;
  delegacyNo?: string;
  curFromDate?: string;
  curToDate?: string;
  curContractNumber?: string;
  contractApproveDTOs?: ContractApprove[];
  managerResponse?: ContractApprove;
  approverResponse?: ContractApprove;
  bossResponse?: ContractApprove;
  amountFeeFromDate?: string | null;
  amountFeeToDate?: string | null;
  amountFee?: number;
}
