import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { environment } from '@hcm-mfe/shared/environment';
import { BaseResponse } from '@hcm-mfe/shared/data-access/models';
import { MICRO_SERVICE } from '@hcm-mfe/shared/common/constants';
import {UrlConstant} from "@hcm-mfe/partnership/data-access/pns-contract/common";

@Injectable({
  providedIn: "root"
})
export class DownloadFileAttachService extends BaseService{
  readonly baseUrl = UrlConstant.API_VERSION + UrlConstant.DOWNLOAD_FILE_ATTACH;
  readonly baseUrlATM = UrlConstant.API_VERSION + UrlConstant.DOWNLOAD_FILE_ATTACHMENT;

  doTaxDownloadAttachFile(docId: number, security: string) {
    const url = this.baseUrl + `?docId=${docId}&security=${security}`;
    return this.getRequestAbsFile(url, {}, environment.backend.contractBackend + url);
  }

  public doDownloadFileByName(fileName: string) {
    const url = UrlConstant.API_VERSION + '/download/temp-file?fileName=' + fileName;
    return this.getRequestFile(url, {}, environment.backend.contractBackend + url);
  }

  public downloadTemplate(urlEndpoint: string) {
    const url = UrlConstant.API_VERSION + urlEndpoint;
    return this.getRequestFile(url, {}, environment.backend.contractBackend + url);
  }

  public doImport(endpoint: string, data: FormData): Observable<BaseResponse> {
    const url = UrlConstant.API_VERSION + endpoint;
    return this.post(url, data, {}, MICRO_SERVICE.TAX_SERVICE);
  }

  // dowload file attach by docId
  doDownloadAttachFile(docId: number) {
    const url = this.baseUrlATM + `${docId}`;
    return this.getRequestFile(url);
  }

  doDownloadAttachFileWithSecurity(docId: number, security: string) {
    const url = this.baseUrlATM + `${docId}&security=${security}`;
    return this.getRequestFile(url);
  }
}
