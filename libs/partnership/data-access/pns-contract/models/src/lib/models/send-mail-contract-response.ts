export interface SendMailContractResponse {
  listEmpSuccess?: EmployeeInfoSendMailDTO[];
  listEmpFail?: EmployeeInfoSendMailDTO[];
  listEmpSentBefore?: EmployeeInfoSendMailDTO[]; // list da duoc gui mail truoc do
}

export interface EmployeeInfoSendMailDTO {
  employeeId?: number;
  employeeCode?: string;
  fullName?: string;
  email?: string;
}
