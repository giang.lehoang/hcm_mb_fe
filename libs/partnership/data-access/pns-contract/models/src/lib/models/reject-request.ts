export interface RejectRequest {
    id?: number;
    listId?: number[];
    rejectReason?: string;
}
