export interface ContractApprove {
  contractApproverId?: number;
  approverId?: number;
  approverLevel?: number;
  contractProposalId?: number;
  contractTypeId?: number;
  note?: string;
  isLiquidation?: number;
  cursorCurrent?: number;
  fromDate?: string;
  toDate?: string;
}
