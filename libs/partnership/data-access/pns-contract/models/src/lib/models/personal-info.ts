export interface PersonalInfo {
    employeeId?: number,
    employeeCode?: string,
    fullName?: string, // Tên nhân viên
    genderCode?: string, // Mã giới tính
    genderName?: string, // Text giới tính
    dateOfBirth?: string, // Ngày sinh
    nationName?: string , // Quốc tịch
    empTypeName?: string, // Đối tượng
    jobName?: string, // Chức danh
    orgName?: string // Đơn vị
    empTypeCode?: string, // Đối tượng code
    positionCode?: string, // Chức danh code
    orgCode?: string, // Đơn vị code
    joinCompanyDate: string, // Ngày vào MB
    email: string, // Email
    mobileNumber: string,
    personalIdNumber: string,
    personalIdDate: string,
    personalIdPlace: string,
}
