export interface ContractTypes {
  name?: string;
  code?: string;
  classifyName?: string;
  empTypeName?: string;
  duration?: number;
  contractTypeId?: number;
  typeName?: string;
  type?: string;
  note?: string;
  classifyCode?: string;
  empTypeCode?: string;
}
