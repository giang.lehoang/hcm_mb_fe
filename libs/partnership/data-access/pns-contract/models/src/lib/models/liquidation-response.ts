import { NzSafeAny } from 'ng-zorro-antd/core/types';

export interface LiquidationResponse {
    liquidationId: number;
    employeeId?: number;
    notifyDays?: number;
    lastWorkingDate?: string;
    reason?: string;
    reasonCode?: string;
    createDate?: string;
    createdBy?: string;
    lastUpdateDate?: string;
    lastUpdatedBy?: string;
    positionId?: number;
    contractTypeId?: number;
    contractExpriedDate?: string;
    flagStatus?: number;
    status?: number;

    listFileDocument?: Array<FileDocument>;
    listConsultationInfo?: Array<ConsultationInfo>;
}

export interface FileDocument {
    type?: number | string;
    fileAttach?: Array<NzSafeAny>;
    listDocuments?: Array<ListFileAttach>;
    docIdsDelete?: number[];
    canNotDelete?: boolean;
}

export interface ConsultationInfo {
    type?: number | string;
    isCheck?: number | boolean;
    note?: string;
}

export interface ListFileAttach {
    docId: string;
    uid: string;
    name: string;
    fileName: string;
    security?: string;
}
