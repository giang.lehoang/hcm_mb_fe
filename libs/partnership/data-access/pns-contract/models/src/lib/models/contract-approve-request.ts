import { ContractEvaluationRequest } from './contract-evaluation-request';

export interface ContractApproveRequest {
    employeeId?: number;
    type?: number;
    contractApproverId?: number;
    contractProposalId?: number;
    approverId?: number;
    approverLevel?: number;
    contractTypeId?: number;
    note?: string;
    isLiquidation?: number;
    cursorCurrent?: number;
    fromDate?: string;
    toDate?: string;
    startRecord?: number;
    pageSize?: number;
    positionName?: string;
    documentNo?: string;
    listStatus?: string[];
    listContractTypeId?: string[];
    listPositionId?: string[];
    employeeCode?: string;
    fullName?: string;
    contractEvaluationsDTO?: ContractEvaluationRequest;
}
