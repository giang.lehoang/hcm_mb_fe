export interface ContractType {
    contractTypeId?: number;
    code?: string;
    name?: string;
    classifyCode?: string;
    duration?: number;
    value?: number | string | null;
    label?: string;
}
