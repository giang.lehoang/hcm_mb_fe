export interface EmployeeInfo {
    employeeId?: number
    employeeCode?: string
    fullName?: string
    positionName?: string
    orgName?: string

    jobName?: string
    personalId?: string
    personalIdPlace?: string
    email?: string
    mobileNumber?: string
    currentAddress?: string
    personalIdDate?: string
    contractTypeName?: string
}
