export interface LiquidationConfig {
  configType: string
  configTypeName: string
  configs: Array<ListConfig>
}

export interface ListConfig {
  code: string
  dataType: string
  displaySeq: number
  maxLength: number
  name: string
}
