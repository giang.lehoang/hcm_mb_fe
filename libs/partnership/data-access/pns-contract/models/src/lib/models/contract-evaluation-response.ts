export interface ContractEvaluationResponse {
    contractEvaluationId?: number;
    contractProposalId?: number;
    employeeId?: number;
    isDisciplined?: number;
    disciplinedNote?: string;
    kpiPoint?: string;
    rankCode?: string;
    rankName?: string;
    flagStatus?: number;
    createdBy?: string;
    createDate?: string;
    lastUpdatedBy?: string;
    lastUpdateDate?: string;
    note?: string;
    attachFileList?: AttachFileList[];
}

export class AttachFileList {
    docId?: number;
    security?: string;
    fileName?: string;
    mimeType?: string;
}
