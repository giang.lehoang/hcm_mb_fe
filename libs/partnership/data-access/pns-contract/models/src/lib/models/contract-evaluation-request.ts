export interface ContractEvaluationRequest {
    contractEvaluationId?: number;
    contractProposalId?: number;
    employeeId?: number;
    isDisciplined?: number;
    disciplinedNote?: string;
    kpiPoint?: string;
    rankCode?: string;
    rankName?: string;
    startRecord?: number;
    pageSize?: number;
    note?: string;
    docIdsDelete?: number[];
}
