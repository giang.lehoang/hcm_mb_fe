import {Injectable, Injector, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder, FormGroup, ValidatorFn} from '@angular/forms';
import {Subscription} from 'rxjs';
import {ModalOptions, NzModalRef, NzModalService} from 'ng-zorro-antd/modal';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {saveAs} from 'file-saver';
import * as moment from 'moment';
import {MBTableConfig, Pagination} from "@hcm-mfe/shared/data-access/models";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {CommonUtils} from "@hcm-mfe/shared/core";
import {Mode} from "@hcm-mfe/shared/common/constants";
import {getTypeExport} from "@hcm-mfe/shared/common/utils";

@Injectable()
export class SearchBaseComponent {
  public FILE_TYPE_PERSONAL_UPLOAD = 'xls,xlsx,doc,docx,pdf,png,jpg,rar,zip'; // bien xu ly validate file upload trong cac qa trinh cua nhan vien
  tableConfig!: MBTableConfig;
  resultList: NzSafeAny = {};
  formSearch?: FormGroup;
  @ViewChild('mbTable') mbTable: NzSafeAny;
  private mainService: NzSafeAny;
  searchResult: NzSafeAny[] = [];
  subs: Subscription[] = [];
  pagination = new Pagination();
  modal?: NzModalRef;
  modalComponent: NzSafeAny;
  protected fb?: FormBuilder;
  isLoadingPage = false;

  // innit service
  public actr?: ActivatedRoute;
  public modalService?: NzModalService;
  public translate?: TranslateService;
  public toastrService?: ToastrService;

  constructor(
    private injector: Injector
  ) {
    this.innitService();
  }

  innitService() {
    this.modalService = this.injector.get(NzModalService);
    this.fb = this.injector.get(FormBuilder);
    this.actr = this.injector.get(ActivatedRoute);
    this.toastrService = this.injector.get(ToastrService);
    this.translate = this.injector.get(TranslateService);
  }

  ngOnDestroy() {
    this.subs?.forEach(sub => sub.unsubscribe());
  }

  buildFormSearch(formSearchConfig: NzSafeAny, _validators?: ValidatorFn[] ) {
    this.formSearch = this.fb?.group( formSearchConfig );
    if (_validators) {
      this.formSearch?.setValidators(_validators);
    }
  }

  public setMainService(serviceSearch: NzSafeAny) {
    this.mainService = serviceSearch;
  }

  public setDataTable(param = {
    resultList: undefined,
    formSearch: undefined
  }) {
    this.resultList = param.resultList;
    this.formSearch = param.formSearch;
  }

  public doSearch(pageNumber?: number): void {
    this.pagination.pageNumber = pageNumber ?? 1;
    this.tableConfig.pageIndex = pageNumber ? this.tableConfig.pageIndex: 1;
    this.tableConfig.loading = true;
    this.isLoadingPage = true;
    const params = this.formSearch ? {...this.formSearch.value} : {};
    this.subs.push(
      this.mainService.search({ ...params, ...this.pagination.getCurrentPage() }).subscribe((res: NzSafeAny) => {
          if (CommonUtils.isSuccessRequest(res)) {
            this.searchResult = res.data.listData;
            this.tableConfig.total = res.data.count;
          }
          this.resultList = res;
          this.tableConfig.loading = false;
          this.isLoadingPage = false;
        },
        () => {
          this.tableConfig.loading = false;
          this.isLoadingPage = false;
        }
      )
    );
  }

  /**
   * Xu ly xoa
   */
  public doDelete(id: number): void {
    this.subs.push(
      this.mainService.deleteById(id).subscribe((res: NzSafeAny) => {
        if (CommonUtils.isSuccessRequest(res)) {
          this.doSearch();
          this.toastrService?.success(this.translate?.instant('common.notification.deleteSuccess'));
        }
      }, (err: NzSafeAny) => {
        return this.toastrService?.error(err.message);
      })
    );
  }

  objectCreateModal(id?: number): ModalOptions {
    return {
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
      nzTitle: this.translate?.instant(id ? this.modalComponent.updateTitleKey : this.modalComponent.addTitleKey),
      nzContent: this.modalComponent,
      nzComponentParams: {
        mode: id ? Mode.EDIT : Mode.ADD,
        id: id
      }
    }
  };

  doPrepareInserOrUpdate(id?: number) {

    this.modal = this.modalService?.create(this.objectCreateModal(id));

    this.modal?.afterClose.subscribe(isSearch => {
      if (isSearch) {
        this.doSearch()
      }
    });
  }

  doExport(fileName: string) {
    let searchParam = this.formSearch ? {...this.formSearch.value} : {};
    if(searchParam.fromTime && typeof searchParam.fromTime != 'string' ){
      searchParam.fromTime = moment(new Date(searchParam.fromTime)).format('DD/MM/yyyy HH:mm:ss');
    }
    if(searchParam.toTime && typeof searchParam.toTime != 'string' ){
      searchParam.toTime = moment(new Date(searchParam.toTime)).format('DD/MM/yyyy HH:mm:ss');
    }
    if(searchParam.timekeepingDate && typeof searchParam.timekeepingDate != 'string' ){
      searchParam.timekeepingDate = moment(new Date(searchParam.timekeepingDate)).format('DD/MM/yyyy HH:mm:ss');
    }
    this.subs.push(
      this.mainService.export(searchParam).subscribe((res: NzSafeAny) => {
          const reportFile = new Blob([res.body], { type: getTypeExport('xlsx') });
          saveAs(reportFile, fileName);
        },() =>{
          this.toastrService?.error(this.translate?.instant('partnership.validate.exportNotData'));
        }
      )
    );

  }

}
