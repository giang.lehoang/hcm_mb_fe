export class Constant {

  public static readonly DETAIL = 'detail';
  public static readonly DRAFT = 0;

  public static readonly CLASSIFY_CODE = {
    PHU_HOP_DONG: 'PHD',
    HOP_DONG: 'HD'
  }

  public static readonly APPROVER_LEVEL = [
    {label: 'manager', value: 1},
    {label: 'approver', value: 2},
    {label: 'approverOther', value: 3},
    {label: 'signer', value: 4},
  ]

  public static readonly CONTRACT_TYPE = [
    {label: 'partnership.pns-contract.searchForm.newSign', value: '1'},
    {label: 'partnership.pns-contract.searchForm.continueSign', value: '2'},
    {label: 'partnership.pns-contract.searchForm.changeSign', value: '3'},
    {label: 'partnership.pns-contract.searchForm.amountFeeSign', value: '4'},
  ];

  public static readonly CONTRACT_OFFER = [
    {label: 'partnership.pns-contract.label.liquidation', value: 0},
    {label: 'partnership.pns-contract.label.continueSign', value: 1}
  ];

  public static readonly ASSESSMENTS = [
    {label: 'partnership.pns-contract.label.isNotDisciplined', value: 0},
    {label: 'partnership.pns-contract.label.isDisciplined', value: 1}
  ];

  public static readonly SIGN_STATUS = [
    {label: 'partnership.pns-contract.signStatus.init', value: '1', bgColor: '#E8EAF6', color: '#6E7191'},
    {label: 'partnership.pns-contract.signStatus.managerApproved', value: '2', bgColor:'#E9EAFF', color: '#141ED2'},
    {label: 'partnership.pns-contract.signStatus.levelApproved', value: '3', bgColor: '#DAF9EC', color: '#06A561'},
    {label: 'partnership.pns-contract.signStatus.waitSign', value: '4', bgColor: '#FFF2DA', color: '#F99600'},
    {label: 'partnership.pns-contract.signStatus.signed', value: '5', bgColor:'#E9EAFF', color: '#141ED2'},
    {label: 'partnership.pns-contract.signStatus.approveSigned', value: '6', bgColor: '#DAF9EC', color: '#06A561'},
    {label: 'partnership.pns-contract.signStatus.rejectSignFile', value: '7', bgColor: '#FFC9C9', color: '#FA0B0B'},
    {label: 'partnership.pns-contract.signStatus.liquidated', value: '8', bgColor: '#DAF9EC', color: '#06A561'},
  ];

  public static readonly APPROVE_STATUS = [
    {label: 'partnership.pns-contract.label.waitProcess', value: 1, bgColor: '#FFF2DA', color: '#F99600'},
    {label: 'partnership.pns-contract.label.processing', value: 2, bgColor:'#E9EAFF', color: '#141ED2'},
    {label: 'partnership.pns-contract.label.approved', value: 3, bgColor: '#DAF9EC', color: '#06A561'}
  ];

  // public static readonly CONTRACT_LIQUIDATION_STATUS = [
  //     {label: 'partnership.pns-contract.label.suggest', value: 1, bgColor: '#FFF2DA', color: '#F99600'},
  //     {label: 'partnership.pns-contract.label.consultedInformation', value: 2, bgColor:'#E9EAFF', color: '#141ED2'},
  //     {label: 'partnership.pns-contract.label.approved', value: 3, bgColor: '#DAF9EC', color: '#06A561'},
  //     {label: 'partnership.pns-contract.label.submited', value: 4, bgColor: '#DAF9EC', color: '#06A561'},
  //     {label: 'partnership.pns-contract.label.signed', value: 5, bgColor: '#DAF9EC', color: '#06A561'}
  // ];

  public static readonly CONTRACT_LIQUIDATION_STATUS = [
      {label: 'partnership.pns-contract.label.inConsultation', value: '0', bgColor: '#FFF2DA', color: '#F99600'},
      {label: 'partnership.pns-contract.label.consulted', value: '1', bgColor:'#E9EAFF', color: '#141ED2'},
      {label: 'partnership.pns-contract.label.reviewed', value: '2', bgColor: '#DAF9EC', color: '#06A561'},
      {label: 'partnership.pns-contract.label.consultationRequest', value: '3', bgColor: '#FFF2DA', color: '#F99600'},
      {label: 'partnership.pns-contract.label.approveWaiting', value: '4', bgColor: '#FFF2DA', color: '#F99600'},
      {label: 'partnership.pns-contract.label.paymentWaiting', value: '5', bgColor: '#FFF2DA', color: '#F99600'},
      {label: 'partnership.pns-contract.label.return', value: '6', bgColor: '#FFC9C9', color: '#FA0B0B'},
      {label: 'partnership.pns-contract.label.done', value: '7', bgColor: '#DAF9EC', color: '#06A561'}
  ];

  public static readonly CONTRACT_FEES_STATUS = [
      {label: 'partnership.pns-contract.label.draft', value: '0', bgColor: '#FFF2DA', color: '#F99600'},
      {label: 'partnership.pns-contract.label.approvedS', value: '1', bgColor:'#E9EAFF', color: '#141ED2'},
      {label: 'partnership.pns-contract.label.rejected', value: '2', bgColor: '#FFC9C9', color: '#FA0B0B'},
  ];

  public static readonly CONTRACT_LIQUIDATION_APPROVAL_STATUS = [
    {label: 'partnership.pns-contract.label.notConsult', value: 0, bgColor: '#FFF2DA', color: '#F99600'},
    {label: 'partnership.pns-contract.label.consulted', value: 1, bgColor:'#E9EAFF', color: '#141ED2'}
];

  public static readonly LIQUIDATION_CONSULT_STATUS = [
    {label: 'partnership.pns-liquidation-consult.label.nonCheck', value: 0, bgColor: '#FFF2DA', color: '#F99600'},
    {label: 'partnership.pns-liquidation-consult.label.checked', value: 1, bgColor:'#E9EAFF', color: '#141ED2'}
];

  public static readonly STATUSES_EMPLOYEE = [
    {label: 'partnership.label.workIn', value: '1'},
    {label: 'partnership.label.contractPending', value: '2'},
    {label: 'partnership.label.workOut', value: '3'},
  ];

  public static readonly DATA_PICKER_ERROR = [
    {label: "partnership.pns-contract.validate.signerError", value: "signer"},
    {label: "partnership.pns-contract.validate.managerError", value: "manager"},
    {label: "partnership.pns-contract.validate.approverError", value: "approver"},
    {label: "partnership.pns-contract.validate.approverOtherError", value: "approverOther"}
  ]

  public static readonly LIST_CONTRACT_TYPE = [
    {label: "partnership.pns-contract.contractTypes.laborContract", value: "HDLD"},
    {label: "partnership.pns-contract.contractTypes.probationaryContract", value: "HDTV"},
    {label: "partnership.pns-contract.contractTypes.serviceContract", value: "HDDV"},
  ]

  public static readonly TYPE_CODE = {
    DOI_TUONG_CV: 'DOI_TUONG_CV',
    KHU_VUC: 'KHU_VUC',
    LOAI_HOP_DONG: 'LOAI_HOP_DONG',
    PHAN_LOAI_HD: 'PHAN_LOAI_HOP_DONG',
    LOAI_HINH_CHI_NHANH: 'LOAI_HINH',
    HO_SO_THANH_LY_HD: 'HO_SO_THANH_LY_HD',
    TT_THAM_VAN_THANH_LY_HD: 'TT_THAM_VAN_THANH_LY_HD',
    LY_DO_NGHI: 'LY_DO_NGHI',
  }

  public static readonly JOB_TYPE = {
    ORG: 'ORG'
  }

  public static readonly PAGE = {
    CONTRACT_APPROVAL_SEARCH: '/partnership/pns-contract/contract-approval/search',
    CONTRACT_APPROVAL_EDIT: '/partnership/pns-contract/contract-approval/edit?id=',
    CONTRACT_APPROVAL_DETAIL: '/partnership/pns-contract/contract-approval/detail?id=',
    CONTRACT_APPROVAL_EVALUATE: '/partnership/pns-contract/contract-approval/evaluate?id='
  };

  public static readonly USER_LOGIN_LEVEL = {
    DIRECT_MANAGER: 1,
    APPROVAL_LEVEL: 2,
    CHANGE_APPROVAL_LEVEL: 3
  }

  public static readonly DISCIPLINE_DATA = [
    { label: 'Có', value: '1' },
    { label: 'Không', value: '0' },
  ];

  public static readonly LIMITLESS = "partnership.pns-contract.contractApproval.label.limitless";

  public static readonly ORG_GROUP_SELECTS = [
    { value: "CN", label: "partnership.pns-contract.configApprovals.branch" },
    { value: "HO", label: "partnership.pns-contract.configApprovals.headquarters" },
    { value: "PGD", label: "partnership.pns-contract.configApprovals.transaction" },
  ];

  public static readonly SCOPE_SELECTS = [
    { value: 1, label: "partnership.pns-contract.configApprovals.org" },
    { value: 2, label: "partnership.pns-contract.configApprovals.groupOrg" }
  ]

  public static readonly TYPE_SELECTS = [
    { value: 1, label: "partnership.pns-contract.configApprovals.signer" },
    { value: 2, label: "partnership.pns-contract.configApprovals.approver" },
    { value: 3, label: "partnership.pns-contract.configApprovals.approverOther" }
  ]

  public static readonly EXCEL_TYPE_1 = 'xls';
  public static readonly EXCEL_MIME_1 = 'application/vnd.ms-excel';

  public static readonly EXCEL_TYPE_2 = 'xlsx';
  public static readonly EXCEL_MIME_2 = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';

  public static readonly PDF_TYPE = 'pdf';
  public static readonly PDF_MIME = 'application/pdf';

}
