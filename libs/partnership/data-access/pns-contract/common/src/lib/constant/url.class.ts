export class UrlConstant {
  public static readonly API_VERSION = '/v1.0';

    public static readonly DOWNLOAD_FILE_ATTACH = '/download/file';
    public static readonly DOWNLOAD_FILE_ATTACHMENT = '/download/file?docId=';

  public static readonly SEARCH_FORM = {
    CONTRACT_PROPOSALS: "/contract-proposals",
    CONTRACT_FEES: "/contract-fees",
    CONTRACT_APPROVERS: '/contract-approvers',
    CONTRACT_TYPES: '/contract-types/search',
    CONTRACT_LIQUIDATION: '/liquidations',
    LOOKUP_POSITION_GROUP: '/mp-position-groups/BASE',
    ALL_POSITION_GROUP: '/mp-position-groups/get-all',
    LOOKUP_EMP_TYPE: '/lookup-values?typeCode=DOI_TUONG_CV',
  };

  public static readonly CATEGORY = {
    LOOKUP_VALUE: '/lookup-values',
    MP_JOB: '/category/mp-jobs'
  }

  public static readonly CATALOGS = {
    PREFIX: '/lookup-values',
    CONTRACT_TYPE: '/contract-types'
  }

  public static readonly CONTRACT_PROPOSAL = {
    EXPORT_BY_ID: '/contract-proposals/{id}/export-contract',
    GET_BY_ID: '/contract-proposals/{id}',
    SAVE: '/contract-proposals',
    EXPORT_BY_FORM: '/contract-proposals/export-contract',
    EXPORT_LIST_SIGN: '/contract-proposals/export',
    SALARY_INFO: '/salary-processes/find-by-properties',
    EXPORT_BY_LIST_ID: '/contract-proposals/export-contract-by-ids',
    UPLOAD_FILE_SIGNED: '/contract-proposals/upload-file-signed',
    DELETE_BY_FORM: '/contract-proposals/delete-by-form',
    CONTRACT_FEE_TEMPLATE: '/v1.0/contract-proposals/contract-fee/import-template',
    CONTRACT_FEE_IMPORT: '/v1.0/contract-proposals/contract-fee/import-process',
    SEND_MAIL_BY_LIST: '/contract-proposals/send-mail-by-list'
  }

  public static readonly EMPLOYEES = {
    PERSONAL_INFO: '/employees/{employeeId}/personal-information'
  }

  public static readonly NAVIGATE_URL = {
    EDIT_CONTINUE_SIGN: '/partnership/pns-contract/contract-proposals/edit-continue-sign',
    EDIT_NEW_SIGN: '/partnership/pns-contract/contract-proposals/edit-new-sign',
    EDIT_ADDENDUM_SIGN: '/partnership/pns-contract/contract-proposals/edit-addendum-sign',
    SEARCH: '/partnership/pns-contract/contract-proposals/search',
    DETAIL_CONTINUE_SIGN: '/partnership/pns-contract/contract-proposals/detail-continue-sign',
    DETAIL_NEW_SIGN: '/partnership/pns-contract/contract-proposals/detail-new-sign',
    DETAIL_ADDENDUM_SIGN: '/partnership/pns-contract/contract-proposals/detail-addendum-sign',
    EDIT_AMOUNT_FEE: '/partnership/pns-contract/contract-proposals/edit-amount-fee',
    DETAIL_AMOUNT_FEE: '/partnership/pns-contract/contract-proposals/detail-amount-fee',
    ADD_FORM_CONFIGURATION: '/partnership/admin-config/form-configuration/create',
    EDIT_FORM_CONFIGURATION: '/partnership/admin-config/form-configuration/update?contractTemplateId=',
  }

  public static readonly GET_BY_ID = {
    CONTRACT_APPROVERS: '/contract-approvers/{id}'
  };

  public static readonly SAVE = {
    CONTRACT_APPROVERS: '/contract-approvers',
    CONTRACT_EVALUATE: '/contract-approvers/save-evaluation'
  };

  public static readonly EDIT = {
    CONTRACT_APPROVERS: '/contract-approvers'
  };

  public static readonly APPROVE_BY_LIST = {
    CONTRACT_APPROVERS: '/contract-approvers/keep-signing'
  };

  public static readonly APPROVE_ALL = {
    CONTRACT_APPROVERS: '/contract-approvers/keep-signing-all'
  };

  public static readonly LIQUIDATE_BY_LIST = {
    CONTRACT_APPROVERS: '/contract-approvers/liquidation-by-list'
  };

  public static readonly COUNT_RECORD = {
    CONTRACT_APPROVERS: '/contract-approvers/count-approval-record'
  };

  public static readonly DOWNLOAD_FILE = '/download/file'

  public static readonly CONTRACT_TYPES = {
    SAVE: '/contract-types',
    GET_BY_ID: '/contract-types/{id}'
  }

  public static readonly EXPORT_REPORT = {
    CONFIG_APPROVAL: '/config-approvals/export',
    CONTRACT_TYPES: '/contract-types/export'
  };

    public static readonly CONTRACT_TEMPLATE = {
        DOWNLOAD_FILE: "/contract-templates/file/{id}"
    }

    public static readonly CONTRACT_LIQUIDATION = {
        EMPLOYEE_INFO: "/liquidations/emp-info/{employeeId}",
        SAVE: "/liquidations",
        GET_BY_ID: "/liquidations/{id}",
        DELETE_BY_ID: "/liquidations/{id}",
        EXPORT: "/liquidations/export/excel",
        DOWNLOAD_BY_ID: "/liquidations/download/liquidation-agreement",
        APPROVAL: "/liquidations/approve",
        APPROVAL_REJECT: "/liquidations/approve-reject/{liquidationId}",
        IMPORT_APPROVE: "/v1.0/liquidations/approve-reject/import",
        DOWNLOAD_TEMPLATE_APPROVE: "/v1.0/liquidations/approve-reject/template-import",
        IMPORT_CREATE: "/v1.0/liquidations/import",
        DOWNLOAD_TEMPLATE_CREATE: "/v1.0/liquidations/template-import",
        COMPLETE_PROFILE: "/liquidations/file/submit/{id}",
        CONFIRM_PROFILE: "/liquidations/file/approve/{id}",
        RETURN_PROFILE: "/liquidations/file/reject",
        COMPLETE_LIQUIDATIONS: "/liquidations/complete",
        LOOKUP_PGR_TYPE: '/mp-position-groups/{pgrType}'
    }
    public static readonly CONTRACT_FEES = {
      SEARCH: "/contract-fees",
      SAVE: "/contract-fees",
      DELETE_BY_ID: "/contract-fees/{id}",
      GET_BY_ID: "/contract-fees/{id}",
      EXPORT: "/contract-fees/export",
      APPROVE: "/contract-fees/approve-by-id",
      APPROVE_LIST: "/contract-fees/approve-by-list",
      APPROVE_ALL: "/contract-fees/approve-all",
      REJECT: "/contract-fees/reject-by-id",
      REJECT_LIST: "/contract-fees/reject-by-list",
      IMPORT: "/v1.0/contract-fees/import",
      DOWNLOAD_TEMPLATE: "/v1.0/contract-fees/template-import",
    }

    public static readonly CONTRACT_LIQUIDATION_VALUES = {
        GET_DETAIL_LOAN_BY_ID: "/liquidation-values/detail/loan/{liquidationId}",
        GET_DETAIL_CARD_BY_ID: "/liquidation-values/detail/card/{liquidationId}",
        SEARCH: "/liquidation-values/{configType}",
        DELETE: "/liquidation-values/{configType}/{id}",
        SYNC_DATA: "/liquidation-values/sync/{configType}/{id}",
        SAVE: "/liquidation-values/{configType}",
        GET_BY_ID: "/liquidation-values/info/{configType}/{id}",
        IMPORT: "/liquidation-values/import/{configType}",
        DOWNLOAD_TEMPLATE: "/liquidation-values/download-template/{configType}",
        EXPORT: "/liquidation-values/export/{configType}"
    }

    public static readonly CONFIG_LIQUIDATION = {
        GET_BY_TYPE: "/config-liquidations/config/{configType}"
    }
}
