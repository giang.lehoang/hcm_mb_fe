import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NzFullDisabledDirectiveDirective } from './directives/nz-full-disabled-directive.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [
    NzFullDisabledDirectiveDirective
  ],
  exports: [NzFullDisabledDirectiveDirective]
})
export class PartnershipDataAccessPnsContractDirectivesModule {}
