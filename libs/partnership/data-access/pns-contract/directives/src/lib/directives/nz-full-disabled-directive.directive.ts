import {Directive, ElementRef, Input} from '@angular/core';
import {NzMenuItemDirective} from "ng-zorro-antd/menu";

@Directive({
  selector: '[nzFullDisabled]'
})
export class NzFullDisabledDirectiveDirective {

  constructor(
    private nzMenuItemDirective: NzMenuItemDirective,
    private el: ElementRef
  ) { }

  private _nzFullDisabled = false;

  @Input() set nzFullDisabled(value: boolean) {
    this._nzFullDisabled = value;
    this.nzMenuItemDirective.nzDisabled = value;
    if (value) {
      this.el.nativeElement.style.pointerEvents = 'none';
    } else {
      this.el.nativeElement.style.removeProperty('pointer-events');
    }
  }

}
