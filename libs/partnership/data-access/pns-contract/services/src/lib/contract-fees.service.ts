import {Injectable} from "@angular/core";
import {BaseService} from '@hcm-mfe/shared/common/base-service';
import {MICRO_SERVICE} from '@hcm-mfe/shared/common/constants';
import {UrlConstant} from "@hcm-mfe/partnership/data-access/pns-contract/common";
import {Observable} from "rxjs";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";
import { CommonUtils } from '@hcm-mfe/shared/core';
import { NzSafeAny } from 'ng-zorro-antd/core/types';

@Injectable({
    providedIn: 'root'
})
export class ContractFeesService extends BaseService {

    saveRecord(request: NzSafeAny) {
        const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_FEES.SAVE;
        return this.post(url, request, {}, MICRO_SERVICE.CONTRACT);
    }

    getById(id: number) {
        const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_FEES.GET_BY_ID.replace('{id}', id.toString());
        return this.get(url, {}, MICRO_SERVICE.CONTRACT);
    }

    deleteById(id: number): Observable<BaseResponse> {
        const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_FEES.DELETE_BY_ID.replace('{id}', id.toString());
        return this.delete(url, {}, MICRO_SERVICE.CONTRACT);
    }

  exportData(searchParam: NzSafeAny) {
    const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_FEES.EXPORT;
    return this.getRequestFileD2T(url, { params: searchParam }, MICRO_SERVICE.CONTRACT);
  }

  approve(params: {listId: number[]}) {
    const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_FEES.APPROVE_LIST;
    return this.post(url, params, undefined, MICRO_SERVICE.CONTRACT);
  }

  reject(params: {listId: number[]}) {
    const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_FEES.REJECT_LIST;
    return this.post(url, params, undefined, MICRO_SERVICE.CONTRACT);
  }

  approveReject(liquidationId: number, params: NzSafeAny) {
    const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_FEES.REJECT_LIST;
    return this.post(url, params, {}, MICRO_SERVICE.CONTRACT);
  }
}
