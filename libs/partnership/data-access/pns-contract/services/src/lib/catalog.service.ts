import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { MICRO_SERVICE } from '@hcm-mfe/shared/common/constants';
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";

const API_VERSION = "/v1.0"
const PREFIX = "/contract-templates"
const BASE_URL = API_VERSION + PREFIX;

@Injectable({
  providedIn: 'root'
})
export class CatalogService extends BaseService {

  public getContractType(): Observable<BaseResponse>{
    const url = BASE_URL + "/type";
    return this.get(url, {}, MICRO_SERVICE.CONTRACT);
  }


}
