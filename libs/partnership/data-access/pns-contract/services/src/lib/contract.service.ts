import {Injectable} from '@angular/core';
import * as moment from 'moment';
import {Observable} from 'rxjs';
import {BaseService} from '@hcm-mfe/shared/common/base-service';
import {MICRO_SERVICE} from '@hcm-mfe/shared/common/constants';
import {CommonUtils} from '@hcm-mfe/shared/core';
import {environment} from '@hcm-mfe/shared/environment';
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";
import {NzSafeAny} from "ng-zorro-antd/core/types";

const API_VERSION = "/v1.0"
const PREFIX = "/contract-templates"
const BASE_URL = API_VERSION + PREFIX;

@Injectable({
  providedIn: 'root'
})
export class ContractService extends BaseService {

  public getContractType(): Observable<BaseResponse>{
    const url = BASE_URL + "/type";
    return this.get(url, {}, MICRO_SERVICE.CONTRACT);
  }

  public savePnsContractTemplates(formData: FormData): Observable<BaseResponse>{
    return this.post(BASE_URL, formData, {}, MICRO_SERVICE.CONTRACT);
  }

  public search(searchData: NzSafeAny) : Observable<BaseResponse>{
    searchData.organizationId = searchData.org ? searchData.org.orgId : null;
    delete searchData.org;
    if (searchData.positionGroupId) {
      searchData.positionGroupId = searchData.positionGroupId.join(',');
    }
    if (searchData.listEmpTypeCode) {
          searchData.listEmpTypeCode = searchData.listEmpTypeCode.join(',');
    }
    if (searchData.fromDate){
      searchData.fromDate = moment(searchData.fromDate).format("DD/MM/YYYY")
    }
    if (searchData.toDate){
      searchData.toDate = moment(searchData.toDate).format("DD/MM/YYYY")
    }
    const params = CommonUtils.buildParams(searchData);
    return this.get(BASE_URL, { params: params }, MICRO_SERVICE.CONTRACT);
  }

  public deleteById(contractTemplateId: number) : Observable<BaseResponse>{
    const url = BASE_URL + "/" + contractTemplateId;
    return this.delete(url, {}, MICRO_SERVICE.CONTRACT);
  }

  public downloadFile(contractTemplateId: number){
    const url = BASE_URL + "/file/" + contractTemplateId;
    return this.get(url, {responseType: 'blob' }, MICRO_SERVICE.CONTRACT);
  }

  public getPnsContractTemplates(contractTemplateId: number): Observable<BaseResponse>{
    const url = BASE_URL + "/" + contractTemplateId ;
    return this.get(url, {}, MICRO_SERVICE.CONTRACT);
  }

    public doExportData(searchData: NzSafeAny){
        searchData.organizationId = searchData.org ? searchData.org.orgId : null;
        delete searchData.org;
        if (searchData.positionGroupId) {
            searchData.positionGroupId = searchData.positionGroupId.join(',');
        }
        if (searchData.listEmpTypeCode) {
            searchData.listEmpTypeCode = searchData.listEmpTypeCode.join(',');
        }
        if (searchData.fromDate){
            searchData.fromDate = moment(searchData.fromDate).format("DD/MM/YYYY")
        }
        if (searchData.toDate){
            searchData.toDate = moment(searchData.toDate).format("DD/MM/YYYY")
        }
        const params = CommonUtils.buildParams(searchData);
        const url = BASE_URL + '/export';
        return this.getRequestFile(url, { params: params }, environment.backend.contractBackend + url);
    }

}
