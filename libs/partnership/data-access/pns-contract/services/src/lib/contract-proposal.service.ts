import {Injectable} from '@angular/core';
import {HttpParams} from '@angular/common/http';
import {BaseService} from '@hcm-mfe/shared/common/base-service';
import {MICRO_SERVICE} from '@hcm-mfe/shared/common/constants';
import {environment} from '@hcm-mfe/shared/environment';
import {UrlConstant} from "@hcm-mfe/partnership/data-access/pns-contract/common";
import {ContractProposal} from "@hcm-mfe/partnership/data-access/pns-contract/models";

@Injectable({
  providedIn: 'root'
})
export class ContractProposalService extends BaseService{
  public exportContractById(id: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_PROPOSAL.EXPORT_BY_ID.replace('{id}', id.toString());
    return this.getRequestFile(url, {}, environment.backend.contractBackend + url);
  }

  public getById(id: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_PROPOSAL.GET_BY_ID.replace('{id}', id.toString());
    return this.get(url, {}, MICRO_SERVICE.CONTRACT);
  }

  public saveContractProposal(data: ContractProposal) {
    const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_PROPOSAL.SAVE;
    return this.post(url, data,{}, MICRO_SERVICE.CONTRACT);
  }

  public exportContractByForm(urlEndpoint: string ,searchParam: HttpParams) {
    const url = UrlConstant.API_VERSION + urlEndpoint;
    return this.getRequestFile(url, {params: searchParam}, environment.backend.contractBackend + url);
  }

  public exportContractByListId(listId: number[]) {
    let params = new HttpParams();
    if (listId.length > 0) {
      params = params.set('listId', listId.join(','));
    }
    const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_PROPOSAL.EXPORT_BY_LIST_ID;
    return this.getRequestFile(url, {params: params}, environment.backend.contractBackend + url);
  }

  uploadFileSigned(formData: FormData) {
    const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_PROPOSAL.UPLOAD_FILE_SIGNED;
    return this.post(url, formData, {}, MICRO_SERVICE.CONTRACT);
  }

  deleteContractProposalById(id: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_PROPOSAL.GET_BY_ID.replace('{id}', id.toString());
    return this.delete(url, {}, MICRO_SERVICE.CONTRACT);
  }

  deleteContractProposalByForm(params: HttpParams) {
    const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_PROPOSAL.DELETE_BY_FORM;
    return this.delete(url, {params: params}, MICRO_SERVICE.CONTRACT);
  }

  sendMailContract(listId: number[]) {
    const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_PROPOSAL.SEND_MAIL_BY_LIST;
    return this.post(url, listId, {}, MICRO_SERVICE.CONTRACT);
  }

}
