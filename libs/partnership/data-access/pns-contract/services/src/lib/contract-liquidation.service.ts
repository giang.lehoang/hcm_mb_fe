import { Injectable } from '@angular/core';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { MICRO_SERVICE } from '@hcm-mfe/shared/common/constants';
import { UrlConstant } from '@hcm-mfe/partnership/data-access/pns-contract/common';
import { Observable } from 'rxjs';
import { BaseResponse } from '@hcm-mfe/shared/data-access/models';
import { CommonUtils } from '@hcm-mfe/shared/core';
import { NzSafeAny } from 'ng-zorro-antd/core/types';

@Injectable({
  providedIn: 'root'
})
export class ContractLiquidationService extends BaseService {

  getEmployeeInfo(employeeId: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_LIQUIDATION.EMPLOYEE_INFO.replace('{employeeId}', employeeId.toString());
    return this.get(url, {}, MICRO_SERVICE.CONTRACT);
  }

  saveRecord(request: FormData) {
    const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_LIQUIDATION.SAVE;
    return this.post(url, request, {}, MICRO_SERVICE.CONTRACT);
  }

  completeProfile(id: number, request: FormData) {
    const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_LIQUIDATION.COMPLETE_PROFILE.replace('{id}', id.toString());
    return this.post(url, request, {}, MICRO_SERVICE.CONTRACT);
  }

  confirmProfile(id: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_LIQUIDATION.CONFIRM_PROFILE.replace('{id}', id.toString());
    return this.post(url, undefined, {}, MICRO_SERVICE.CONTRACT);
  }

  returnProfile(data: { rejectReason: string; liquidationId: number | undefined }) {
    const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_LIQUIDATION.RETURN_PROFILE;
    return this.post(url, data, {}, MICRO_SERVICE.CONTRACT);
  }

  getById(id: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_LIQUIDATION.GET_BY_ID.replace('{id}', id.toString());
    return this.get(url, {}, MICRO_SERVICE.CONTRACT);
  }

  getPgrType(pgrType: string) {
    const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_LIQUIDATION.LOOKUP_PGR_TYPE.replace('{pgrType}', pgrType);
    return this.get(url, {}, MICRO_SERVICE.ABS_MANAGEMENT);
  }

  deleteById(id: number): Observable<BaseResponse> {
    const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_LIQUIDATION.DELETE_BY_ID.replace('{id}', id.toString());
    return this.delete(url, {}, MICRO_SERVICE.CONTRACT);
  }

  exportData(searchParam: NzSafeAny) {
    const params = CommonUtils.buildParams(searchParam);
    const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_LIQUIDATION.EXPORT;
    return this.getRequestFileD2T(url, { params: params }, MICRO_SERVICE.CONTRACT);
  }

  approval(params: { liquidationIds: number[] }) {
    const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_LIQUIDATION.APPROVAL;
    return this.post(url, undefined, { params }, MICRO_SERVICE.CONTRACT);
  }

  approveReject(liquidationId: number, params: NzSafeAny) {
    const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_LIQUIDATION.APPROVAL_REJECT.replace('{liquidationId}', liquidationId.toString());
    return this.post(url, params, {}, MICRO_SERVICE.CONTRACT);
  }

  downloadById(searchParam: NzSafeAny) {
    const params = CommonUtils.buildParams(searchParam);
    const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_LIQUIDATION.DOWNLOAD_BY_ID;
    return this.getRequestFileD2T(url, { params: params }, MICRO_SERVICE.CONTRACT);
  }

  completeLiquidation(data: FormData) {
    const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_LIQUIDATION.COMPLETE_LIQUIDATIONS;
    return this.post(url, data, {}, MICRO_SERVICE.CONTRACT);
  }
}
