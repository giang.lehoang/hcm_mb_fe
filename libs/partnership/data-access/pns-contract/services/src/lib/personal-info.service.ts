import {Injectable} from '@angular/core';
import {BaseService} from '@hcm-mfe/shared/common/base-service';
import {UrlConstant} from "@hcm-mfe/partnership/data-access/pns-contract/common";

@Injectable({
  providedIn: 'root'
})
export class PersonalInfoService extends BaseService{

  public getPersonalInfo(employeeId: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.EMPLOYEES.PERSONAL_INFO.replace('{employeeId}', employeeId.toString());
    return this.get(url);
  }

}
