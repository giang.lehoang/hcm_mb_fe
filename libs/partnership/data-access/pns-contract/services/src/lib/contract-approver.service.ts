import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { MICRO_SERVICE } from '@hcm-mfe/shared/common/constants';
import { environment } from '@hcm-mfe/shared/environment';
import {UrlConstant} from "@hcm-mfe/partnership/data-access/pns-contract/common";
import {ContractApproveRequest, RejectRequest} from "@hcm-mfe/partnership/data-access/pns-contract/models";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";


@Injectable({
    providedIn: 'root'
})
export class ContractApproverService extends BaseService {

    getById(id: number) {
        const url = UrlConstant.API_VERSION + UrlConstant.GET_BY_ID.CONTRACT_APPROVERS.replace('{id}', id.toString());
        return this.get(url, {}, MICRO_SERVICE.CONTRACT);
    }

    saveRecord(request: FormData) {
        const url = UrlConstant.API_VERSION + UrlConstant.SAVE.CONTRACT_APPROVERS;
        return this.post(url, request, {}, MICRO_SERVICE.CONTRACT);
    }

    saveEvaluation(request: FormData) {
        const url = UrlConstant.API_VERSION + UrlConstant.SAVE.CONTRACT_EVALUATE;
        return this.post(url, request, {}, MICRO_SERVICE.CONTRACT);
    }

    keepSigningByList(data: HttpParams) {
        const url = UrlConstant.API_VERSION + UrlConstant.APPROVE_BY_LIST.CONTRACT_APPROVERS;
        return this.post(url, null, { params: data }, MICRO_SERVICE.CONTRACT);
    }

    keepSigningAll(data: ContractApproveRequest) {
        const url = UrlConstant.API_VERSION + UrlConstant.APPROVE_ALL.CONTRACT_APPROVERS;
        return this.post(url, data, {}, MICRO_SERVICE.CONTRACT);
    }

    liquidationByList(data: RejectRequest) {
        const url = UrlConstant.API_VERSION + UrlConstant.LIQUIDATE_BY_LIST.CONTRACT_APPROVERS;
        return this.post(url, data, {}, MICRO_SERVICE.CONTRACT);
    }

    countValidRecord(urlEndpoint: string, searchParam: HttpParams): Observable<BaseResponse> {
        const url = UrlConstant.API_VERSION + urlEndpoint;
        return this.get(url, { params: searchParam }, MICRO_SERVICE.CONTRACT);
    }

    doDownloadAttachFile(docId: number, security: string) {
        const url = UrlConstant.API_VERSION + UrlConstant.DOWNLOAD_FILE + `?docId=${docId}&security=${security}`;
        return this.getRequestFile(url, {}, environment.backend.contractBackend + url);
    }

}
