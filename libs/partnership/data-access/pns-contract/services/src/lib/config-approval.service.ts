import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { MICRO_SERVICE } from '@hcm-mfe/shared/common/constants';
import { CommonUtils } from '@hcm-mfe/shared/core';
import { BaseResponse } from '@hcm-mfe/shared/data-access/models';
import {UrlConstant} from "@hcm-mfe/partnership/data-access/pns-contract/common";
import {NzSafeAny} from "ng-zorro-antd/core/types";

const API_VERSION = '/v1.0';
const PREFIX = '/config-approvals';
const BASE_URL = API_VERSION + PREFIX;

@Injectable({
  providedIn: 'root'
})
export class ConfigApprovalService extends BaseService {

    public search(searchData: NzSafeAny): Observable<BaseResponse> {
        if (searchData.org) {
            searchData.organizationId = searchData.org.orgId;
            delete searchData.org;
        }
        if (searchData.fromDate) {
            searchData.fromDate = moment(searchData.fromDate).format('DD/MM/YYYY');
        }
        if (searchData.toDate) {
            searchData.toDate = moment(searchData.toDate).format('DD/MM/YYYY');
        }
        const params = CommonUtils.buildParams(searchData);
        return this.get(BASE_URL, { params: params }, MICRO_SERVICE.CONTRACT);
    }


    public saveData(formData: FormData): Observable<BaseResponse> {
        return this.post(BASE_URL, formData, {}, MICRO_SERVICE.CONTRACT);
    }

    public getById(configApprovalId: number): Observable<BaseResponse> {
        const url = BASE_URL + '/' + configApprovalId;
        return this.get(url, {}, MICRO_SERVICE.CONTRACT);
    }

    public deleteById(configApprovalId: number): Observable<BaseResponse> {
        const url = BASE_URL + '/' + configApprovalId;
        return this.delete(url, {}, MICRO_SERVICE.CONTRACT);
    }

    doExportFile(urlEndpoint: string, searchData: NzSafeAny) {
        if (searchData.org) {
            searchData.organizationId = searchData.org.orgId;
            delete searchData.org;
        }
        if (searchData.fromDate) {
            searchData.fromDate = moment(searchData.fromDate).format('DD/MM/YYYY');
        }
        if (searchData.toDate) {
            searchData.toDate = moment(searchData.toDate).format('DD/MM/YYYY');
        }
        const params = CommonUtils.buildParams(searchData);
        const url = UrlConstant.API_VERSION + urlEndpoint;
        return this.getRequestFileD2T(url, {params: params}, MICRO_SERVICE.CONTRACT);
    }

    getPositionGroupList(): Observable<BaseResponse> {
        const url = UrlConstant.API_VERSION + UrlConstant.SEARCH_FORM.ALL_POSITION_GROUP;
        return this.get(url, {}, MICRO_SERVICE.ABS_MANAGEMENT);
    }

}
