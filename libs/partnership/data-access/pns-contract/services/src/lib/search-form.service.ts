import {Injectable} from '@angular/core';
import {HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {BaseService} from '@hcm-mfe/shared/common/base-service';
import {MICRO_SERVICE} from '@hcm-mfe/shared/common/constants';
import {UrlConstant} from "@hcm-mfe/partnership/data-access/pns-contract/common";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";

@Injectable({
  providedIn: 'root'
})
export class SearchFormService extends BaseService{

  public getFilterResearch(urlEndpoint: string, searchParam: HttpParams, pagination: { startRecord: number, pageSize: number }): Observable<BaseResponse> {
    if (pagination) searchParam = searchParam.appendAll(pagination);
    const url = UrlConstant.API_VERSION + urlEndpoint;
    return this.get(url, {params: searchParam}, MICRO_SERVICE.CONTRACT);
  }

  public getListValue(urlEndpoint: string, params: HttpParams) {
    const url = UrlConstant.API_VERSION + urlEndpoint;
    return this.get(url, {params: params});
  }

  public getContractType(urlEndpoint: string, params: HttpParams) {
    const url = UrlConstant.API_VERSION + urlEndpoint;
    return this.get(url, {params: params});
  }

  public getEmpTypeList() {
    return this.get(UrlConstant.API_VERSION + UrlConstant.SEARCH_FORM.LOOKUP_EMP_TYPE)
  }
}
