import { Injectable } from '@angular/core';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { MICRO_SERVICE } from '@hcm-mfe/shared/common/constants';
import { UrlConstant } from '@hcm-mfe/partnership/data-access/pns-contract/common';
import { HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseResponse } from '@hcm-mfe/shared/data-access/models';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { CommonUtils } from '@hcm-mfe/shared/core';

@Injectable({
  providedIn: 'root'
})
export class ContractLiquidationValueService extends BaseService {

  readonly baseUrlATM = UrlConstant.API_VERSION + UrlConstant.DOWNLOAD_FILE_ATTACHMENT;

  getListByConfigType(configType: string, searchParam: HttpParams, pagination: { startRecord: number, pageSize: number }) {
    if (pagination) searchParam = searchParam.appendAll(pagination);
    const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_LIQUIDATION_VALUES.SEARCH.replace('{configType}', configType);
    return this.get(url, { params: searchParam }, MICRO_SERVICE.CONTRACT);
  }

  exportData(configType: string, searchParam: NzSafeAny) {
    searchParam.organizationId = searchParam.org ? searchParam.org.orgId : null;
    delete searchParam.org;
    const params = CommonUtils.buildParams(searchParam)
    const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_LIQUIDATION_VALUES.EXPORT.replace('{configType}', configType);
    return this.getRequestFileD2T(url, { params: params }, MICRO_SERVICE.CONTRACT);
  }

  public getById(configType: string, id: number): Observable<BaseResponse>{
    const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_LIQUIDATION_VALUES.GET_BY_ID.replace('{configType}', configType).replace('{id}', id.toString());
    return this.get(url, { }, MICRO_SERVICE.CONTRACT);
  }

  public deleteById(configType: string, id: number): Observable<BaseResponse>{
    const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_LIQUIDATION_VALUES.DELETE.replace('{configType}', configType).replace('{id}', id.toString());
    return this.delete(url, { }, MICRO_SERVICE.CONTRACT);
  }

  public syncById(configType: string, id: number): Observable<BaseResponse>{
    const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_LIQUIDATION_VALUES.SYNC_DATA.replace('{configType}', configType).replace('{id}', id.toString());
    return this.post(url, null, { }, MICRO_SERVICE.CONTRACT);
  }

  public saveData(params: FormData, configType: string) {
    const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_LIQUIDATION_VALUES.SAVE.replace('{configType}', configType);
    return this.post(url, params, undefined, MICRO_SERVICE.CONTRACT);
  }

  // dowload file attach by docId
  doDownloadAttachFile(docId: number) {
    const url = this.baseUrlATM + `${docId}`;
    return this.getRequestFileD2T(url, undefined, MICRO_SERVICE.CONTRACT);
  }

  getListDetailLoanById(liquidationId: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_LIQUIDATION_VALUES.GET_DETAIL_LOAN_BY_ID.replace('{liquidationId}', liquidationId.toString());
    return this.get(url, undefined, MICRO_SERVICE.CONTRACT);
  }

  getListDetailCardById(liquidationId: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_LIQUIDATION_VALUES.GET_DETAIL_CARD_BY_ID.replace('{liquidationId}', liquidationId.toString());
    return this.get(url, undefined, MICRO_SERVICE.CONTRACT);
  }
}
