import { Injectable } from '@angular/core';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { MICRO_SERVICE } from '@hcm-mfe/shared/common/constants';
import { UrlConstant } from '@hcm-mfe/partnership/data-access/pns-contract/common';
import { Observable } from 'rxjs';
import { BaseResponse } from '@hcm-mfe/shared/data-access/models';

@Injectable({
  providedIn: 'root'
})
export class ConfigLiquidationService extends BaseService {

  getConfigLiquidation(configType: string) {
    const url = UrlConstant.API_VERSION + UrlConstant.CONFIG_LIQUIDATION.GET_BY_TYPE.replace('{configType}', configType);
    return this.get(url, {}, MICRO_SERVICE.CONTRACT);
  }

}
