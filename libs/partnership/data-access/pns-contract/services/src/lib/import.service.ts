import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseResponse } from '@hcm-mfe/shared/data-access/models';
import { MICRO_SERVICE } from '@hcm-mfe/shared/common/constants';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import {UrlConstant} from "@hcm-mfe/personal-tax/data-access/common";
import {environment} from "@hcm-mfe/shared/environment";

@Injectable({
  providedIn: 'root'
})
export class ImportService extends BaseService {

  public downloadTemplate(urlEndpoint: string) {
    return this.getRequestFileD2T(urlEndpoint, undefined, MICRO_SERVICE.CONTRACT);
  }

  public doImport(endpoint: string, data: FormData): Observable<BaseResponse> {
    return this.post(endpoint, data, undefined, MICRO_SERVICE.CONTRACT);
  }

  public doDownloadFileByName(fileName: string) {
    const url = UrlConstant.API_VERSION + '/download/temp-file?fileName=' + fileName;
    return this.getRequestFile(url, {}, environment.backend.contractBackend + url);
  }
}
