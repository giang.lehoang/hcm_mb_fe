import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {BaseService} from '@hcm-mfe/shared/common/base-service';
import {MICRO_SERVICE} from '@hcm-mfe/shared/common/constants';
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";

const API_VERSION = "/v1.0"
const PREFIX = "/template-params"
const BASE_URL = API_VERSION + PREFIX;

@Injectable({
  providedIn: 'root'
})
export class ContractParamService extends BaseService {


  public createOrUpdatePnsTemplateParams(pnsTemplateParamsForm: NzSafeAny): Observable<BaseResponse>{
    return this.post(BASE_URL, pnsTemplateParamsForm, {}, MICRO_SERVICE.CONTRACT);
  }

  public getPnsTemplateParams(): Observable<BaseResponse>{
    return this.get(BASE_URL, { }, MICRO_SERVICE.CONTRACT);
  }

  public getPnsTemplateParamsById(templateParamId: number): Observable<BaseResponse>{
    const url = BASE_URL + "/" +  templateParamId;
    return this.get(url, { }, MICRO_SERVICE.CONTRACT);
  }

  public deletePnsTemplateParams(templateParamId: number): Observable<BaseResponse>{
    const url = BASE_URL + "/" +  templateParamId;
    return this.delete(url, { }, MICRO_SERVICE.CONTRACT);
  }


}
