import {Injectable} from '@angular/core';
import {HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {BaseService} from '@hcm-mfe/shared/common/base-service';
import {environment} from '@hcm-mfe/shared/environment';
import { UrlConstant } from '@hcm-mfe/partnership/data-access/pns-contract/common';
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";

@Injectable({
  providedIn: 'root'
})
export class ContractTypesService extends BaseService{
  public getFilterResearch(urlEndpoint: string, searchParam: HttpParams, pagination: { startRecord: number, pageSize: number }): Observable<BaseResponse> {
    if (pagination) searchParam = searchParam.appendAll(pagination);
    const url = UrlConstant.API_VERSION + urlEndpoint;
    return this.get(url, {params: searchParam});
  }

  public saveContractType(formData: FormData) {
    const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_TYPES.SAVE;
    return this.post(url, formData, {});
  }

  public getContractTypeById(contractTypeId: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_TYPES.GET_BY_ID.replace("{id}", contractTypeId.toString());
    return this.get(url);
  }

  public deleteContractTypeById(contractTypeId: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.CONTRACT_TYPES.GET_BY_ID.replace("{id}", contractTypeId.toString());
    return this.delete(url);
  }

  public doExportData(urlEndpoint: string, searchParam: HttpParams) {
    const url = UrlConstant.API_VERSION + urlEndpoint;
    return this.getRequestFile(url, { params: searchParam }, environment.backend.employeeServiceBackend + url);
  }

}
