import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IsDisabledActionCaPipe} from "./pipes/is-disabled-action-ca.pipe";

@NgModule({
  imports: [CommonModule],
  declarations: [IsDisabledActionCaPipe],
  exports: [IsDisabledActionCaPipe]
})
export class PartnershipDataAccessPnsContractPipesModule {}
