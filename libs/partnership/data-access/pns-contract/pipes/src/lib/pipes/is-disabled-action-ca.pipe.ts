import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'isDisabledActionCa'
})
export class IsDisabledActionCaPipe implements PipeTransform {

  transform(action: string, status: number, liquidationStatusOfNext: number,
            approverLevel: number, dmIsLiquidation: number,
            alIsLiquidation: number, calCursorCurrent: number,
            contractByLawId: number, dmContractTypeId: number, alContractTypeId: number,
  ): boolean {

    switch (action) {
      case 'APPROVE':
        if (status >= 4 || liquidationStatusOfNext) {
          return true;
        }

        if (approverLevel === 1 && status === 2 && dmIsLiquidation === 1) {  // QLTT đã đánh giá
          return true;
        }

        if (approverLevel === 2 && status === 3 && alIsLiquidation === 1) {  // Cấp phê duyệt đã đánh giá
          return true;
        }

        if (approverLevel === 3 && status === 4) {  // Cấp phê duyệt điều chỉnh đã đánh giá
          return true;
        }

        if ((approverLevel === 1 || approverLevel === 2) && dmIsLiquidation === 1 && alIsLiquidation === 1) {  // Cap 1 va cap 2 la cung 1 nguoi va thuc hien phe duyet
          return true;
        }

        // Truong hop den luot cap 3 xu li, neu hop dong theo tieu chuan === hop dong de xuat -> disabled
        if (calCursorCurrent === 1) {
          if ((contractByLawId === dmContractTypeId) && (dmContractTypeId === alContractTypeId)) {
            return true;
          }
        }
        break;
      case 'LIQUIDATE':
        if (status >= 4 || liquidationStatusOfNext) {
          return true;
        }

        if (approverLevel === 1 && status === 2 && dmIsLiquidation === 0) {  // QLTT đã đánh giá
          return true;
        }

        if (approverLevel === 2 && status === 3 && alIsLiquidation === 0) {  // Cấp phê duyệt đã đánh giá
          return true;
        }

        if (approverLevel === 3 && status === 4) {  // Cấp phê duyệt điều chỉnh đã đánh giá
          return true;
        }

        if ((approverLevel === 1 || approverLevel === 2) && dmIsLiquidation === 0 && alIsLiquidation === 0) {  // Cap 1 va cap 2 la cung 1 nguoi va thuc hien thanh ly
          return true;
        }

        // Truong hop den luot cap 3 xu li, neu hop dong theo tieu chuan === hop dong de xuat -> disabled
        if (calCursorCurrent === 1) {
          if ((contractByLawId === dmContractTypeId) && (dmContractTypeId === alContractTypeId)) {
            return true;
          }
        }
        break;
      case 'EDIT':
        if (status >= 4 || liquidationStatusOfNext) {
          return true;
        }

        if (approverLevel === 1 && status === 2) {  // QLTT đã đánh giá
          return true;
        }

        if (approverLevel === 2 && status === 3) {  // Cấp phê duyệt đã đánh giá
          return true;
        }

        if (approverLevel === 3 && status === 4) {  // Cấp phê duyệt điều chỉnh đã đánh giá
          return true;
        }

        if ((approverLevel === 1 || approverLevel === 2) && dmIsLiquidation === 1 && alIsLiquidation === 1) {  // Cap 1 va cap 2 la cung 1 nguoi va thuc hien phe duyet
          return true;
        }

        if ((approverLevel === 1 || approverLevel === 2) && dmIsLiquidation === 0 && alIsLiquidation === 0) {  // Cap 1 va cap 2 la cung 1 nguoi va thuc hien thanh ly
          return true;
        }

        // Truong hop den luot cap 3 xu li, neu hop dong theo tieu chuan === hop dong de xuat -> disabled
        if (calCursorCurrent === 1) {
          if ((contractByLawId === dmContractTypeId) && (dmContractTypeId === alContractTypeId)) {
            return true;
          }
        }
        break;
      case 'EVALUATE':
        if (status > 2) {
          return true;
        }
        break;
    }

    return false;
  }

}
