import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PopupConfirmComponent} from './popup-confirm/popup-confirm.component';
import {NzGridModule} from "ng-zorro-antd/grid";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";

@NgModule({
    imports: [CommonModule, NzGridModule, TranslateModule, SharedUiMbButtonModule],
    declarations: [
        PopupConfirmComponent
    ],
    exports: [PopupConfirmComponent]
})
export class SelfServiceUiPopupConfirmModule {
}
