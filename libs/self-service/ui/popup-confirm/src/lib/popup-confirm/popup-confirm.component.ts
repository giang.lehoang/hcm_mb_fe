import {Component} from '@angular/core';
import {NzModalService} from "ng-zorro-antd/modal";
import {Router} from "@angular/router";
import {UrlConstant} from "@hcm-mfe/self-service/data-access/common";

@Component({
  selector: 'hcm-mfe-popup-confirm',
  templateUrl: './popup-confirm.component.html',
  styleUrls: ['./popup-confirm.component.scss']
})
export class PopupConfirmComponent {

    constructor(
        private modalService: NzModalService,
        private router: Router,
    ) {
    }

    onCloseClick() {
        this.modalService.closeAll();
    }

    onRegisterClick() {
        this.router.navigateByUrl(UrlConstant.URL.REGISTER_NEW_CREATE).then();
        this.modalService.closeAll();
    }

}
