import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {Subscription} from 'rxjs';
import {PersonalInfo} from '@hcm-mfe/self-service/data-access/models';
import {User, UserLogin} from '@hcm-mfe/shared/data-access/models';
import {PersonalInfoService} from '@hcm-mfe/self-service/data-access/services';
import {StorageService} from '@hcm-mfe/shared/common/store';
import {HTTP_STATUS_CODE, STORAGE_NAME} from '@hcm-mfe/shared/common/constants';
import {SessionKey} from '@hcm-mfe/shared/common/enums';
import {TranslateService} from "@ngx-translate/core";

@Component({
    selector: 'app-personal-information',
    templateUrl: './personal-information.component.html',
    styleUrls: ['./personal-information.component.scss']
})
export class PersonalInformationComponent implements OnInit, OnDestroy {
    @Input() isShowTaxInfo = true;
    @Input() isShowIdNo = false;
    @Input() isShowEmail = false;
    @Output() emailEmployee = new EventEmitter<PersonalInfo>();
    @Output() employeeInfoEvent = new EventEmitter<PersonalInfo>();
    employeeId!: number;
    userLogin: UserLogin = new UserLogin();
    currentUser?: User;
    personalInfo?: PersonalInfo;

    subs: Subscription[] = [];

    constructor(
        private personalInfoService: PersonalInfoService,
        private toastService: ToastrService,
        private translateService: TranslateService
    ) {
        this.userLogin = StorageService.get(STORAGE_NAME.USER_LOGIN);
        if (localStorage.getItem(SessionKey.CURRENCY_USER)) {
            this.currentUser = JSON.parse(localStorage.getItem(SessionKey.CURRENCY_USER) ?? '');
        }
        if (!this.userLogin.employeeId && !this.currentUser?.empId) {
          this.toastService.error(this.translateService.instant("selfService.label.empCodeNotFound"))
        } else {
          this.employeeId = this.userLogin.employeeId ?? this.currentUser?.empId ?? 1;
        }
    }

    ngOnInit(): void {
      if (this.employeeId) {
        this.getPersonalInfo();
      }
    }

    getPersonalInfo() {
        this.subs.push(
            this.personalInfoService.getPersonalInfo(this.employeeId).subscribe(res => {
                if (res.code === HTTP_STATUS_CODE.OK) {
                    this.personalInfo = res?.data;
                    this.emailEmployee.emit(res?.data);
                    this.employeeInfoEvent.emit(res?.data);
                } else {
                    this.toastService.error(res.message);
                }
            }, error => {
                this.toastService.error(error.message);
            })
        );
    }

    ngOnDestroy(): void {
        this.subs?.forEach(sub => sub.unsubscribe());
    }

}
