import {Pipe, PipeTransform} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Constant} from "@hcm-mfe/self-service/data-access/common";
import {ObjectCategory} from "@hcm-mfe/self-service/data-access/models";

@Pipe({
    name: 'isDisableAction'
})
export class IsDisableActionPipe implements PipeTransform {

    constructor(private translate: TranslateService) {
    }

    transform(regType: string, action: string, status: number): boolean {
        const regType1 = ['1', '2', '3', '4'];  // Cấp mới mã số thuế, điều chỉnh thông tin mã số thuế, đăng ký người phụ thuộc, đăng ký giảm người phụ thuộc
        const actionArr = [Constant.ACTION_PER_RECORD.VIEW_HISTORY, Constant.ACTION_PER_RECORD.CANCEL_REQUEST];
        const statusArr = [
            Constant.STATUS_OBJ.FINANCIAL_ACCOUNTANT_RECEIVED,  // TCKT đã tiếp nhận
            Constant.STATUS_OBJ.FINANCIAL_ACCOUNTANT_IS_PROCESSING,  // TCKT đang xử lý
            Constant.STATUS_OBJ.TAX_AUTHORITIES_AGREE,  // CQT đồng ý
        ]

        const statusArr1 = [
            Constant.STATUS_OBJ.FINANCIAL_ACCOUNTANT_RETURN,
            Constant.STATUS_OBJ.TAX_AUTHORITIES_RETURN
        ];

        const actionArr1 = [Constant.ACTION_PER_RECORD.EDIT, Constant.ACTION_PER_RECORD.DELETE, Constant.ACTION_PER_RECORD.VIEW_HISTORY];

        const regTypeObj: ObjectCategory | undefined = Constant.REQUEST_TYPES.find((item) => {
            const label = this.translate.instant(item.label);
            return regType === label;
        });


        if (!!regTypeObj && regType1.includes(regTypeObj.value)) {

            // Neu trang thai === Dang khai bao && Action = Huy yeu cau --> disabled
            if ((Constant.STATUS_OBJ.IS_DECLARING === status) && (Constant.ACTION_PER_RECORD.CANCEL_REQUEST === action)) {
                return true;
            }

            // Neu trang thai === Cho phe duyet && Action !== Xem lich su && Huy yeu cau --> disabled
            if ((Constant.STATUS_OBJ.WAIT_APPROVE === status) && (!actionArr.includes(action))) {
                return true;
            }

            if (statusArr.includes(status) && (action !== Constant.ACTION_PER_RECORD.VIEW_HISTORY)) {
                return true;
            }

            // neu trang thai la tu choi va hanh dong !== sua && xoa && xem lich su --> disabled
            if (statusArr1.includes(status) && !actionArr1.includes(action)) {
                return true;
            }

        }

        return false;
    }

}
