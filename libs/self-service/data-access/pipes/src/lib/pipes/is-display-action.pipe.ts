import {Pipe, PipeTransform} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Constant} from "@hcm-mfe/self-service/data-access/common";
import {ObjectCategory} from "@hcm-mfe/self-service/data-access/models";

@Pipe({
    name: "isDisplayAction"
})
export class IsDisplayActionPipe implements PipeTransform {

    constructor(private translate: TranslateService) {
    }

    transform(regType: string, action: string): boolean {
        const regType1 = ['1', '2', '3', '4']; // Cấp mới mã số thuế, điều chỉnh thông tin mã số thuế, đăng ký người phụ thuộc, đăng ký giảm người phụ thuộc
        const regType2 = ['5', '6', '7'];  // quyết toán thuế, đăng ký nhận chứng từ, xác nhận ủy quyền cung cấp thông tin
        const actionArr = [Constant.ACTION_PER_RECORD.EDIT, Constant.ACTION_PER_RECORD.VIEW_HISTORY];

        // lấy ra loại yêu cầu đăng ký là gì.
        const regTypeObj: ObjectCategory | undefined = Constant.REQUEST_TYPES.find((item) => {
            const label = this.translate.instant(item.label);
            return regType === label;
        });

        if (regTypeObj) {
            if (regTypeObj.value === Constant.REG_TYPE.REGISTER_RECEIVE_RECEIPT && (action === Constant.ACTION_PER_RECORD.EDIT || action === Constant.ACTION_PER_RECORD.DELETE)) { // neu la nhan chung tu thue thi an hanh dong sua va xoa di
                return false;
            }

            if (regType1.includes(regTypeObj.value)) {
                return true;
            }

            if (regType2.includes(regTypeObj.value) && actionArr.includes(action)) {
                return true;
            }
        }

        return false;
    }

}
