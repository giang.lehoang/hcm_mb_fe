import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {IsDisableActionPipe} from "./pipes/is-disable-action.pipe";
import {IsDisplayActionPipe} from "./pipes/is-display-action.pipe";

@NgModule({
  imports: [CommonModule],
  declarations: [IsDisableActionPipe, IsDisplayActionPipe],
  exports: [IsDisableActionPipe, IsDisplayActionPipe]
})
export class SelfServiceDataAccessPipesModule {}
