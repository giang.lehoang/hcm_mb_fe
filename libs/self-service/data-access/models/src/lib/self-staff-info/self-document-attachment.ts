export class SelfDocumentAttachment {
  employeeId: number;
  employeeProfileId: number;
  profileTypeCode: string;
  profileTypeName: string;
  note: string;
  isHardDocument: number;
  createdBy: string;
  createDate: string;
}
