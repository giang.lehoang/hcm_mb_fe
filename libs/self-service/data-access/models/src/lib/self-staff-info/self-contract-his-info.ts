export class SelfContractHistory {
  contractProcessId: number; // ID Bản ghi
  employeeId: number; // ID Nhân viên
  fromDate: string; // Từ ngày
  toDate: string; // Đến ngày
  signedDate: string; // Ngày ký.
  contractTypeName: string; // Tên hợp đồng
  contractTypeId: number; //Loại HĐ
  contractNumber: string; //Số HĐ.
  note: string;
  classifyCode:string;
  signerName:string;
  signerPosition:string;
  classifyName:string; // Tên phân loại hợp đồng
  listFileAttach: Array<SelfListFileAttach>
}

export class SelfListFileAttach {
  docId: number;
  fileName: string;
}

export class SelfContractTypes {
  contractTypeId: number;
  code: string;
  name: string;
}
