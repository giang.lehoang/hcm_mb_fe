export class SelfDisciplinaryHistory {
  disciplineRecordId: number; // ID Bản ghi
  employeeId: number; // ID Nhân viên
  signedDate: string; // Ngày ký.
  disciplineMethodName: string; // Hình thức kỷ luật
  disciplineMethodCode: number; // Mã hình thức kỷ luật
  reason: string; // Lý do
  disciplineLevelName: string; // Cấp QĐ
  disciplineLevelCode: string; // Mã cấp QĐ
  amount: number;
  note: string;
  fromDate: string;
  toDate: string;
  caseName: string;
  signerPosition: string | number | boolean;
  signer: string | number | boolean;
  amountPaid: number;
}
