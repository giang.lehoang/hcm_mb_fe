import {ListFileAttach} from "./self-employee-profile.interface";


export interface SelfIdentityInfo {
  personalIdentityId?: number,
  idTypeCode?: string, //Mã loại giấy tờ
  idTypeName?: string, //Loại giấy tờ
  idNo?: string, //Số giấy tờ
  idIssueDate?: string, //Ngày cấp
  idIssuePlace?: string, //Nơi cấp
  isMain?: number, //Là giấy tờ chính
  fromDate?: string, //Hiệu lực từ ngày
  toDate?: string, //Hiệu lực đến ngày
  employeeId?: number | null;
  draftPersonalIdentityId?: number;
  docIdsDelete: number[];
  inputType: number[] | string[];
  attachFileList?: ListFileAttach[];
  isCloneFile?: number;
}
