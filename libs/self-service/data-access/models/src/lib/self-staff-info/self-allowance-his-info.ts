export class SelfAllowanceHistory {
  allowanceProcessId: number; // ID Bản ghi
  employeeId: number; // ID Nhân viên
  fromDate: string; // Từ ngày
  toDate: string; // Đến ngày
  allowanceTypeCode: string; // Loại phụ cấp .
  allowanceTypeName: string; // Tên Loại phụ cấp .
  amountMoney: number; // Mức hưởng.
  note: string;
}
