export interface SelfFacultyInfo {
    facultyId?: number;
    code?: string;
    name?: string;
    isTextManual?: number;
}
