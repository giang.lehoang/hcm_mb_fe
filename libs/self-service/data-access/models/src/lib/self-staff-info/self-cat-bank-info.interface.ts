export interface SelfCatBankInfo {
    bankId?: number;
    code?: string;
    name?: string;
}
