import { ListFileAttach } from './self-employee-profile.interface';

export class SelfProjectHistory {
  projectMemberId: number; // ID Bản ghi
  employeeId: number; // ID Nhân viên
  fromDate: string; // Từ ngày
  toDate: string; // Đến ngày
  projectName: string; // Tên dự án tham gia - API danh mục tả ra thiếu
  jobName: string; //SelfJob - API danh mục trả ra thiếu
  projectId: number;
  jobId: number;
  note: string;
  managerId: number;
  managerName: string;
  listFileAttach: Array<ListFileAttach>;
}
