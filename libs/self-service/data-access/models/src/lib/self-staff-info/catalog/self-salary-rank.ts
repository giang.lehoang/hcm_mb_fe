// Danh mục bậc lương
export interface SelfSalaryRank {
  salaryRankId: number,
  salaryRankCode: string,
  salaryRankName: string,
  displaySeq: number
}
