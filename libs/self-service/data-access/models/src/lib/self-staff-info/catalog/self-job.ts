// Vai trò tham gia dự án
export interface SelfJob {
  jobId: number,
  jobCode: string,
  jobName: string,
  jobDescription: string,
  jobType: string
}
