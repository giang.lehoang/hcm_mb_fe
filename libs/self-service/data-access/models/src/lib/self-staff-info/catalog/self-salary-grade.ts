// Danh mục dải lương
export interface SelfSalaryGrade {
  salaryGradeId: number,
  salaryGradeCode: string,
  salaryGradeName: string,
  displaySeq: number
}
