import {NzSafeAny} from "ng-zorro-antd/core/types";

export class BaseResponse {
  code: number;
  message: string;
  timestamp: string;
  clientMessageId: string;
  transactionId: string;
  path: string;
  status: number;
  data: NzSafeAny;
}
