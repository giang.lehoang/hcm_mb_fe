export interface SelfEmployeeProfile {
  employeeProfileId: number;
  employeeId: number;
  profileTypeCode: string;
  profileTypeName: string;
  note: string;
  isHardDocument: number;
  createdBy: string;
  createDate: string;
  startRecord: number;
  pageSize: number;
  listFileAttach: Array<ListFileAttach>;
}

export class ListFileAttach {
  docId: string;
  fileName: string;
  security?: string;
}
