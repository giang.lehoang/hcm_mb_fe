export class SelfFamilyDeductionInfo {
  dependentPersonId: number; //ID bản ghi.
  employeeId: number; // ID Nhân viên
  familyRelationshipId: number; // ID quan hệ gia đình.
  relationTypeName: string; //Tên mối quan hệ
  fullName: string; //Họ tên thân nhân
  taxNumber: string; // Mã số thuế NPT
  personalId: string; //Số CCCD NPT
  personalIdNumber: string; //Số CCCD NPT
  codeNo: string; //Số GKS
  bookNo: string; // Quyển số
  provinceCode: string;
  provinceName: string;
  districtCode: string;
  districtName: string;
  wardCode: string;
  wardName: string;
  fromDate: string;
  toDate: string;
  nationCode: string;
}
