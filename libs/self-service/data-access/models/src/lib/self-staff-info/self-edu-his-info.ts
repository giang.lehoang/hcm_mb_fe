export interface SelfEduHisInfo {
  educationProcessId?: number, // ID Bản ghi
  employeeId?: number, // ID Nhân viên
  courseName?: string, //Tên khóa học
  eduMethodTypeCode?: string, //Mã hình thức đào tạo
  eduMethodTypeName?: string, //Tên hình thức đào tạo
  courseContent?: string, //Nội dung đào tạo
  fromDate?: string, //Thời gian đào tạo từ ngày
  toDate?: string, //Thời gian đào tạo đến ngày
  note?: string
  commitmentDate?: string
  result?: string | number | boolean
}
