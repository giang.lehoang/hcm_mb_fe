import {DegreeInfo} from "@hcm-mfe/staff-manager/data-access/models/info";


export class SelfDegreeInfoGroup {
    employeeId: number;
    data: DegreeInfo;
}
