import {PartyInfo} from "@hcm-mfe/staff-manager/data-access/models/info";


export class SelfPartyInfoGroup {
    employeeId: number;
    data: PartyInfo;
}
