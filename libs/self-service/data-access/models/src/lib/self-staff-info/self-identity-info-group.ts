import {IdentityInfo} from "@hcm-mfe/staff-manager/data-access/models/info";


export class SelfIdentityInfoGroup {
    employeeId: number;
    data: IdentityInfo;
}
