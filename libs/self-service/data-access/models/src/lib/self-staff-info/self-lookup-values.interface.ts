export interface SelfLookupValues {
    value?: string;
    label?: string;
    id?: number;
    provinceCode?: string;
    provinceLabel?: string;
    districtCode?: string;
    districtLabel?: string;
    wardCode?: string;
    wardLabel?: string;
}
