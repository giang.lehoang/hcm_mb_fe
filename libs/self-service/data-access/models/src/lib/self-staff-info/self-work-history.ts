import { ListFileAttach } from './self-employee-profile.interface';

export class SelfWorkHistory {
  workProcessId: number; //ID bản ghi.
  employeeId: number; //ID nhân viên.
  fromDate: string; //Từ ngày
  // toDate: string; // Đến ngày

  empTypeName: string; // Đối tượng
  documentTypeName: string; // Loại QĐ
  positionName: string; // Chức danh
  organizationName: string; // Đơn vị
  positionLevel: string; // Bậc chức danh
  positionLevelName: string; // Bậc chức danh


  empTypeCode: number; // Mã Đối tượng
  documentTypeId: number; // ID Loại QĐ
  positionId: number; // ID Chức danh
  organizationId: number; //ID Đơn vị
  listManager: SelfManager[] = [];
  otherOrgId: number; //ID đơn vị kiêm nhiệm
  orgOtherName: string; //ID đơn vị kiêm nhiệm
  otherPositionId: number; //ID chức danh kiêm nhiệm
  otherPositionName: string; //ID chức danh kiêm nhiệm
  documentNo: string; // Số QĐ
  signedDate: string; // Ngày ký quyết định
  expirationDate: string; //Ngày hết hạn điều động
  note: string; // Ghi chú
  isTrial: number; //Thử thách
  toDate: string; // Ngày kết thúc
  toDate2: string; // Ngày kết thúc 2
  isKeepSenior:number; // Bảo lưu thâm niên
  planToDate1: string; //Ngày kết thúc 1
  planToDate2: string; // Ngày kết thúc 2
  jobName: string;
  listFileAttach: Array<ListFileAttach>;
  pgrName?: string;
}

export class SelfManager {
  managerProcessId?: number;
  fromDate: string;
  managerId: number;
  managerName: string
}

export interface SelfDocument {
  documentTypeId: number;
  name: string;
  code: string;
}

export interface SelfPosition {
  posId: number,
  posCode: string;
  posName: string;
}
