import {ContactInfo} from "@hcm-mfe/staff-manager/data-access/models/info";


export class SelfContactInfoGroup {
    employeeId: number;
    data: ContactInfo;
}
