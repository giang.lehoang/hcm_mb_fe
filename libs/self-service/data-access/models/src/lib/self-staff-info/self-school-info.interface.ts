export interface SelfSchoolInfo {
    schoolId?: number;
    code?: string;
    name?: string;
    isTextManual?: number;
}
