import { SelfBankInfo } from './self-bank-info';

export class SelfBankInfoGroup {
    employeeId: number;
    data: SelfBankInfo;
}
