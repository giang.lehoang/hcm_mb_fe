export class SelfRelativeInfo {
  familyRelationshipId: number;
  employeeId: number; // ID Nhân viên
  isInCompany: number; // Là nhân sự MB - API đang trả ra thiếu
  referenceEmployeeId: string | number; // id nhân viên của thân nhân - API đang trả ra thiếu (string or number ???)
  relationTypeCode: string; //Tình trạng thân nhân
  relationTypeName: string;
  fullName: string;
  relationStatusCode: string;
  relationStatusName: string;
  policyTypeCode: string;
  policyTypeName: string;
  personalIdNumber: string;
  dateOfBirth: string | null;
  workOrganization: string;
  job: string;
  currentAddress: string;
  note: string; // Ghi chú -- API đang trả ra thiếu
  isHouseholdOwner: number; // Là chủ hộ --- API đang trả ra thiếu
}
