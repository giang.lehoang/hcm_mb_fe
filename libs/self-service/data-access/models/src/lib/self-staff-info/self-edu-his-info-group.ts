import { SelfEduHisInfo } from './self-edu-his-info';

export class SelfEduHisInfoGroup {
    employeeId: number;
    data: SelfEduHisInfo;
}
