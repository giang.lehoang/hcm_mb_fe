export class ChangeHistory {
    logActionId?: number;
    objectId?: number;
    objectType?: string;
    content?: string;
    createdBy?: string;
    createDate: string;
}
