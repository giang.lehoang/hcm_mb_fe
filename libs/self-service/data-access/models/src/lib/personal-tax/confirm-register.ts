export class ConfirmRegister {
    confirmRegisterId?: number;
    employeeId?: number;
    note?: string;
    isAccepted?: number;
    startRecord?: number;
    pageSize?: number;
    orgId?: number;
    empStatus?: number;
    fromDate?: string;
    toDate?: string;
    empCode?: string;
    empName?: string;
    status?: number;
    accountNo?: string;
}
