export class TaxNumberRegister {
    taxNumberRegisterId?: number | null;
    employeeId?: number;
    idTypeCode?: string;
    idNo?: string;
    idDate?: string;
    idPlace?: string;
    idPlaceCode?: string;
    permanentNationCode?: string;
    permanentProvinceCode?: string;
    permanentDistrictCode?: string;
    permanentWardCode?: string;
    permanentDetail?: string;
    currentNationCode?: string;
    currentProvinceCode?: string;
    currentDistrictCode?: string;
    currentWardCode?: string;
    currentDetail?: string;
    mobileNumber?: string;
    email?: string;
    oldIdTypeCode?: string;
    oldIdNo?: string;
    oldIdDate?: string;
    oldIdPlace?: string;
    oldIdPlaceCode?: string;
    status?: number;
    regType?: string;
    note?: string;
    startRecord?: number;
    pageSize?: number;
    fromDate?: string;
    toDate?: string;
    orgId?: number;
    empStatus?: string;
    empCode?: string;
    empName?: string;
    taxPlace?: string;

    docIdsDelete: number[];
}
