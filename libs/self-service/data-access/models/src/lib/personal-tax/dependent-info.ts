export class DependentInfo {
  familyRelationshipId: number;
  dependentPersonId?: number;
  fromDate?: string;
  taxNumber?: string;
  dateOfBirth?: string;
  value?: number;
  label?: string;
  fullName?: string;
  relationTypeName?: string;
}
