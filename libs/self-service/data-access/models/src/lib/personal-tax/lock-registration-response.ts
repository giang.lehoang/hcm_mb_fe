export interface LockRegistrationResponse {
    lockRegistrationId?: number;
    registrationType?: string;
    year?: number;
    value?: number;
    label?: string;
    fromDate?: string;
    toDate?: string;
    fromRemindDate?: string;
    toRemindDate?: string;
    flagStatus?: number;
    createdBy?: string;
    createDate?: string;
    lastUpdatedBy?: string;
    lastUpdateDate?: string;
}
