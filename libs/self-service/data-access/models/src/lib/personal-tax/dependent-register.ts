import { ListFileAttach } from '../self-staff-info';

export class DependentRegister {
  dependentRegisterId?:number | null;
  employeeId?:number;
  familyRelationshipId?:number;
  dateOfBirth?:string;
  taxNo?:string;
  idNo?:string;
  codeNo?:string | null;
  bookNo?:string;
  nationCode?:string;
  provinceCode?:string;
  districtCode?:string;
  wardCode?:string;
  fromDate?:string | Date | null;
  toDate?:string | Date | null;
  note?:string;
  status?:number;
  regType?:string;
  attachFileList?: Array<ListFileAttach>;
  docIdsDelete?: number[];
  personalIdNumber?: string;
  taxNumber?:string;
}
