export class TaxRegisterInfo {
    registerId?: number;
    regType?: string;
    regTypeCode?:string;
    registerInformation?: string;
    createDate?: string;
    toDate?: string;
    status?: number;
}
