export class CatalogModel {
  value: number;
  label: string;
  dateOfBirth?: string;
  taxNumber?: string;
  personalIdNumber?: string;

  constructor(label, value, dateOfBirth) {
    this.value = value;
    this.label = label;
    this.dateOfBirth = dateOfBirth;
  }
}
