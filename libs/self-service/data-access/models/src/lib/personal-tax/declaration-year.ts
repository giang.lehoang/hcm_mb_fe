export class DeclarationYear {
  year?: number;
  fromDate?: string;
  toDate?: string;
  value?: number;
  label?: string;
}
