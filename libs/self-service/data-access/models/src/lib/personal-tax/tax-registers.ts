export interface ObjectCategory {
    label: string,
    value: string
}
