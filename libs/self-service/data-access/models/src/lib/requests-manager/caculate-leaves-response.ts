import {AbsRequestApprovers} from "./request-approvers";
import {Approver} from "./request-approver-item";

export interface CaculateLeavesResponse {
    employeeId?: number;
    fromTime?: string;
    toTime?: string;
    reasonLeaveId?: number;
    totalDays?: number;
    allDays?: number;
    approvers?: {
        level?: number;
        approvers?: AbsRequestApprovers[];
    }[],
    listApprovers?: CalLeavesApprover[];
}

export interface CalLeavesApprover {
    approvalOrder: number;
    approvalLevel: number;
    isHr: number;

    level?: number;
    approvers?: Approver[];
}
