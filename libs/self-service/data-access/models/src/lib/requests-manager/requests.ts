import { AbsRequestApprovers } from './request-approvers';
import { AbsRequestLeaves } from './request-leaves';
import { AbsRequestSupporters } from './request-supporters';
import { SelfEmployeeDetail } from '../self-staff-info';
export interface AbsRequests {
  requestId?: number,
  requestIds?: number[],
  employeeId?: number | SelfEmployeeDetail,
  reasonCode?: string,
  reasonDetail?: string,
  status?: number,
  reason?: string,
  allowApprove?: boolean,
  oldRequestId?: number,
  createDate?: string,
  createdBy?: string,
  lastUpdateDate?: string,
  lastUpdatedBy?: string,
  listAbsRequestLeaves?: AbsRequestLeaves[],
  listAbsRequestApprovers?: AbsRequestApprovers[],
  listAbsRequestSupporters?: AbsRequestSupporters[],
  docIdsDelete?: number[],
  fileList?: [],
  listAbsRequestLeavesText?: string[],
  allDays?: string,
  totalDays?: string,
  listReviewer?: string[] | AbsRequestApprovers[],
  listApprover?: string[] | AbsRequestApprovers[]
}
