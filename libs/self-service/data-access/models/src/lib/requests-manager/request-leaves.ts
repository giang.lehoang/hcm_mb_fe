import {AbsRequestApprovers} from "./request-approvers";

export interface AbsRequestLeaves {
    requestLeaveId?: number;
    requestId?: number;
    employeeId?: number;
    fromTime?: string;
    toTime?: string;
    timekeepingDate?: string;
    reasonLeaveId?: number;
    partOfTime?: string;
    workPlace?: string;
    leaveType?: string;
    workdayTypeId?: number;
    timeOffType?: number;
    workdayTypeCode?: string;
    content?: string;
    note?: string;
    totalDays?: number;
    allDays?: number;
    defaultTimeOff?: number;
    listApprovers?: AbsRequestApprovers[];
    name?: string;

    approvalOrder: number;
    approvalLevel: number;
    isHr?: number;
}
