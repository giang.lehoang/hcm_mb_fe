export interface AbsReasonLeaves {
    startRecord?: number;
    pageSize?: number;
    reasonLeaveId?: number;
    code?: string;
    name?: string;
    workdayTypeCode?: string;
    workdayTypeName?: string;
    groupCode?: string;
    groupName?: string;
    maxTimeOff?: number;
    yearMaxTimeOff?: number;
    timeOffType?: number;
    createDate?: string;
    createdBy?: string;
    lastUpdateDate?: string;
    lastUpdatedBy?: string;
    fromTime?: string;
    toTime?: string;
    isLeave?: number;
    defaultTimeOff?: number;
}
