import {AbsRequestApproversDTO} from "./abs-request-approvers-dto";

export interface AbsRequestLeavesDTO {
    requestLeaveId?: number;
    requestId?: number;
    employeeId?: number;
    fromTime?: string;
    toTime?: string;
    timekeepingDate?: string;
    reasonLeaveId?: number;
    partOfTime?: string;
    workPlace?: string;
    leaveType?: string;
    workdayTypeId?: number;
    timeOffType?: number;
    workdayTypeCode?: string;
    content?: string;
    note?: string;
    totalDays?: number;
    allDays?: number;
    defaultTimeOff?: number;
    listApprovers?: AbsRequestApproversDTO;
}
