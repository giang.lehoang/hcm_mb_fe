import {CalLeavesApprover} from "@hcm-mfe/self-service/data-access/models";

export interface ComputeListConfigApprover {
    allDays?: number;
    totalDays?: number;
    listConfigApprovers?: CalLeavesApprover[];
}
