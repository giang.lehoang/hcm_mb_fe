import { SelfEmployeeDetail } from '../self-staff-info';

export interface AbsRequestApprovers {
  requestApproverId?: number,
  requestId?: number,
  employeeId?: number | SelfEmployeeDetail,
  approvalOrder: number,
  approvalLevel: number,
  hrLevel?: number,
  status?: number,
  isAllowView?: number,
  employeeCode?: string,
  fullName?: string,
  email?: string,
  posititonName?: string,
  organizationName?: string,
  isSuggest?: number,
  disableSuggest?: number,
  isApprover?: boolean,
  title?: string
}
