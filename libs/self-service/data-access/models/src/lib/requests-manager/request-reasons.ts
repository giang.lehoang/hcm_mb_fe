export interface AbsRequestReasons {
  requestReasonId?: number,
  requestId?: number,
  reasonLeaveId?: number,
  fromTime?: string,
  toTime?: string,
}