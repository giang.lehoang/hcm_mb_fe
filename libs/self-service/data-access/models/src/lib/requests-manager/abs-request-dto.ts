import {AbsRequestLeavesDTO} from "./abs-request-leaves-dto";
import {AbsRequestSupportersDTO} from "./abs-request-supporters-dto";
import {AbsRequestApproversDTO} from "./abs-request-approvers-dto";
import {NzUploadFile} from "ng-zorro-antd/upload";

export interface AbsRequestDTO {
    startRecord?: number;
    pageSize?: number;
    requestId?: number;
    employeeId?: number;
    fullName?: string;
    employeeCode?: string;
    fromTime?: string;
    toTime?: string;
    timekeepingDate?: string;
    leaveType?: string;
    empTypeCode?: string;
    reasonCode?: string;
    reasonDetail?: string;
    status?: number;
    oldRequestId?: number;
    createDate?: string;
    createdBy?: string;
    lastUpdateDate?: string;
    lastUpdatedBy?: string;
    listAbsRequestLeaves?: AbsRequestLeavesDTO[];
    listAbsRequestSupporters?: AbsRequestSupportersDTO[];
    listAbsRequestApprovers?: AbsRequestApproversDTO[];
    listAbsRequestApproversConfig?: AbsRequestLeavesDTO;
    allDays?: number;
    totalDays?: number;
    organizationId?: number;
    organizationName?: string;
    files?: DocumentsDTO[];
    viewEmployeeId?: number;
    rejectReason?: string;
    pageName?: string;
    requestIds?: number[];
    reasonName?: string;
    docIdsDelete?: number[];

    listAbsRequestLeavesText?: string[];
    listReviewer?: string[] | AbsRequestApproversDTO[];
    listApprover?: string[] | AbsRequestApproversDTO[];
    allowApprove?: boolean;
    viewButtonAction?: boolean;

    fileList?: NzUploadFile[];
}

export interface DocumentsDTO {
    docId?: number;
    security?: string;
    fileName?: string;
    mimeType?: string;
}
