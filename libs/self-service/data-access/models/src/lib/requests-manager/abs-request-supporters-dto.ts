export interface AbsRequestSupportersDTO {
    requestSupporterId?: number;
    requestId?: number;
    employeeId?: number;
    taskContent?: string;
    employeeCode?: string;
    fullName?: string;
}
