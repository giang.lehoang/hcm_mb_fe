export interface AbsRequestApproversDTO {
    requestApproverId?: number;
    requestId?: number;
    employeeId?: number;
    approvalOrder?: number;
    status?: number;
    isAllowView?: number;
    approvalLevel?: number;
    isHr?: number;
    employeeCode?: string;
    fullName?: string;
    hrLevel?: number;
    isSuggest?: number;
}
