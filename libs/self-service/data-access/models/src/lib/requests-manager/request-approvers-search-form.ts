export interface RequestApproversSearchForm {
    employeeId?: number,
    approverLevelId?: number,
    hrLevel?: number | null,
}
