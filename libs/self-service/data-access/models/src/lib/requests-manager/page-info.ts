export interface PageInfo {
    pageName?: string;
    breadcrumb?: string;
    screenMode?: string;
}
