export interface RequestApproverItem {
    level?: number;
    approvers?: Approver[];
    title?: string;

    approvalOrder?: number | number[];
    approvalLevel?: number;
    isHr?: number;

    isApprover?: boolean | number;

    isSuggest?: number;

    hrLevel?: number;
}

export interface Approver {
    employeeId?: number;
    employeeCode?: string;
    fullName?: string;
    email?: string;
    positionName?: string;
    organizationName?: string;
    groupLevelId?: number;
    groupLevelOrder?: number;
    nameCode?: string;
}
