export const approvers = {
    "data": [
        {
            "level" : 3,
            "approvers" : [
                {
                    "employeeId": 2,
                    "employeeCode": "MB0002",
                    "fullName": "Đặng Đình Tứ",
                    "email": "",
                    "positionName": "Nhân viên kinh doanh CN. An Phú",
                    "organizationName": "Bộ phận Hành chính - Tổng hợp"
                },
                {
                    "employeeId": 82,
                    "employeeCode": "MB000022",
                    "fullName": "Lê Thị Thu Trang",
                    "email": "",
                    "positionName": "Chuyên gia cao cấp CN. Hưng Yên",
                    "organizationName": "Ban Giám đốc"
                },
                {
                    "employeeId": 3,
                    "employeeCode": "MB0010",
                    "fullName": "Nguyễn Thành Tuân",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "Phòng Quan hệ khách hàng %3cb%3e1%3c%2fb%3e"
                },
                {
                    "employeeId": 4,
                    "employeeCode": "MB0011",
                    "fullName": "Hồ Hoàng Hà",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 5,
                    "employeeCode": "MB0012",
                    "fullName": "Vũ Thị Ánh Liên",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 6,
                    "employeeCode": "MB0013",
                    "fullName": "Phạm Văn Hải",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 7,
                    "employeeCode": "MB0014",
                    "fullName": "Nguyễn Quang Thời",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 8,
                    "employeeCode": "MB0015",
                    "fullName": "Đặng Anh Tuấn",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 9,
                    "employeeCode": "MB0016",
                    "fullName": "Trần Anh Đức",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 10,
                    "employeeCode": "MB0017",
                    "fullName": "Phạm Văn Tùng",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 11,
                    "employeeCode": "MB0018",
                    "fullName": "Phùng Quang Huy",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 12,
                    "employeeCode": "MB0019",
                    "fullName": "Nguyễn Văn Thiện",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 13,
                    "employeeCode": "MB0020",
                    "fullName": "Cao Anh Tuấn",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 14,
                    "employeeCode": "MB0021",
                    "fullName": "Đinh Duy Anh",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 15,
                    "employeeCode": "MB0022",
                    "fullName": "Nguyễn Hữu Dũng",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 16,
                    "employeeCode": "MB0023",
                    "fullName": "Hồ Anh Tuấn",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 17,
                    "employeeCode": "MB0024",
                    "fullName": "Trác Văn Kỳ",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 18,
                    "employeeCode": "MB0025",
                    "fullName": "Nguyễn Hoài Sơn",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 19,
                    "employeeCode": "MB0026",
                    "fullName": "Trần Doãn Xuân",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 20,
                    "employeeCode": "MB0027",
                    "fullName": "Hoàng Văn Sơn",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 21,
                    "employeeCode": "MB0028",
                    "fullName": "Trịnh Thanh Nam",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 23,
                    "employeeCode": "000005",
                    "fullName": "Trần Quang Hà",
                    "email": "",
                    "positionName": "Chuyên gia cao cấp CN. Hưng Yên",
                    "organizationName": "Ban Giám đốc"
                },
                {
                    "employeeId": 24,
                    "employeeCode": "MB0031",
                    "fullName": "Bùi Đức Mạnh",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 25,
                    "employeeCode": "MB0032",
                    "fullName": "Đinh Quang Trung",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 26,
                    "employeeCode": "MB0033",
                    "fullName": "Hoàng Văn Hiệp",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 27,
                    "employeeCode": "MB0034",
                    "fullName": "Nguyễn Trắc Toàn",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 28,
                    "employeeCode": "MB0035",
                    "fullName": "Bùi Văn Hùng",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 29,
                    "employeeCode": "MB0036",
                    "fullName": "Lê Sỹ Đức",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 30,
                    "employeeCode": "MB0037",
                    "fullName": "Bùi Huy Tùng",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 31,
                    "employeeCode": "MB0038",
                    "fullName": "Hoàng Thịnh Vượng",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 32,
                    "employeeCode": "MB0039",
                    "fullName": "Hà Duy Khánh",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 33,
                    "employeeCode": "MB0040",
                    "fullName": "Nguyễn Anh Tú",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 34,
                    "employeeCode": "MB0041",
                    "fullName": "Phạm Hồng Phú",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 35,
                    "employeeCode": "MB0042",
                    "fullName": "Phạm Kiên Cường",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 36,
                    "employeeCode": "MB0043",
                    "fullName": "Lê Mỹ Phương",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 37,
                    "employeeCode": "MB0044",
                    "fullName": "Trương Ngọc Nam",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 38,
                    "employeeCode": "MB0045",
                    "fullName": "Nguyễn Thiên Bách",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 39,
                    "employeeCode": "MB0046",
                    "fullName": "Nguyễn Hoàng Hải",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 40,
                    "employeeCode": "MB0047",
                    "fullName": "Trần Minh Vũ",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 41,
                    "employeeCode": "MB0048",
                    "fullName": "Cao Xuân Quân",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 42,
                    "employeeCode": "MB0049",
                    "fullName": "Chu Minh Thành",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 43,
                    "employeeCode": "MB0050",
                    "fullName": "Nguyễn Xuân Đồng",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 44,
                    "employeeCode": "MB0051",
                    "fullName": "Nguyễn Phú Mạnh",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 45,
                    "employeeCode": "MB0052",
                    "fullName": "Hoàng Anh Sơn",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 46,
                    "employeeCode": "MB0053",
                    "fullName": "Dương Hồng Hà",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 85,
                    "employeeCode": "MB000025",
                    "fullName": "Đặng Đình Tứ",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "Phòng Quan hệ khách hàng %3cb%3e1%3c%2fb%3e"
                },
                {
                    "employeeId": 1,
                    "employeeCode": "MB0001",
                    "fullName": "Lương Thị Phượng",
                    "email": "",
                    "positionName": "Nhân viên kinh doanh CN. 3 tháng 2",
                    "organizationName": "P. Dịch vụ Khách hàng &lt;b&gt;1&lt;/b&gt;"
                },
                {
                    "employeeId": 83,
                    "employeeCode": "MB000023",
                    "fullName": "Đức",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "Phòng Kế toán và Dịch vụ khách hàng <b>1</b>"
                }
            ]
        },
        {
            "level" : 5,
            "approvers" : [
                {
                    "employeeId": 2,
                    "employeeCode": "MB0002",
                    "fullName": "Đặng Đình Tứ",
                    "email": "",
                    "positionName": "Nhân viên kinh doanh CN. An Phú",
                    "organizationName": "Bộ phận Hành chính - Tổng hợp"
                },
                {
                    "employeeId": 82,
                    "employeeCode": "MB000022",
                    "fullName": "Lê Thị Thu Trang",
                    "email": "",
                    "positionName": "Chuyên gia cao cấp CN. Hưng Yên",
                    "organizationName": "Ban Giám đốc"
                },
                {
                    "employeeId": 3,
                    "employeeCode": "MB0010",
                    "fullName": "Nguyễn Thành Tuân",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "Phòng Quan hệ khách hàng %3cb%3e1%3c%2fb%3e"
                },
                {
                    "employeeId": 4,
                    "employeeCode": "MB0011",
                    "fullName": "Hồ Hoàng Hà",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 5,
                    "employeeCode": "MB0012",
                    "fullName": "Vũ Thị Ánh Liên",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 6,
                    "employeeCode": "MB0013",
                    "fullName": "Phạm Văn Hải",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 7,
                    "employeeCode": "MB0014",
                    "fullName": "Nguyễn Quang Thời",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 8,
                    "employeeCode": "MB0015",
                    "fullName": "Đặng Anh Tuấn",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 9,
                    "employeeCode": "MB0016",
                    "fullName": "Trần Anh Đức",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 10,
                    "employeeCode": "MB0017",
                    "fullName": "Phạm Văn Tùng",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 11,
                    "employeeCode": "MB0018",
                    "fullName": "Phùng Quang Huy",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 12,
                    "employeeCode": "MB0019",
                    "fullName": "Nguyễn Văn Thiện",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 13,
                    "employeeCode": "MB0020",
                    "fullName": "Cao Anh Tuấn",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 14,
                    "employeeCode": "MB0021",
                    "fullName": "Đinh Duy Anh",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 15,
                    "employeeCode": "MB0022",
                    "fullName": "Nguyễn Hữu Dũng",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 16,
                    "employeeCode": "MB0023",
                    "fullName": "Hồ Anh Tuấn",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 17,
                    "employeeCode": "MB0024",
                    "fullName": "Trác Văn Kỳ",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 18,
                    "employeeCode": "MB0025",
                    "fullName": "Nguyễn Hoài Sơn",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 19,
                    "employeeCode": "MB0026",
                    "fullName": "Trần Doãn Xuân",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 20,
                    "employeeCode": "MB0027",
                    "fullName": "Hoàng Văn Sơn",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 21,
                    "employeeCode": "MB0028",
                    "fullName": "Trịnh Thanh Nam",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 23,
                    "employeeCode": "000005",
                    "fullName": "Trần Quang Hà",
                    "email": "",
                    "positionName": "Chuyên gia cao cấp CN. Hưng Yên",
                    "organizationName": "Ban Giám đốc"
                },
                {
                    "employeeId": 24,
                    "employeeCode": "MB0031",
                    "fullName": "Bùi Đức Mạnh",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 25,
                    "employeeCode": "MB0032",
                    "fullName": "Đinh Quang Trung",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 26,
                    "employeeCode": "MB0033",
                    "fullName": "Hoàng Văn Hiệp",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 27,
                    "employeeCode": "MB0034",
                    "fullName": "Nguyễn Trắc Toàn",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 28,
                    "employeeCode": "MB0035",
                    "fullName": "Bùi Văn Hùng",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 29,
                    "employeeCode": "MB0036",
                    "fullName": "Lê Sỹ Đức",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 30,
                    "employeeCode": "MB0037",
                    "fullName": "Bùi Huy Tùng",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 31,
                    "employeeCode": "MB0038",
                    "fullName": "Hoàng Thịnh Vượng",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 32,
                    "employeeCode": "MB0039",
                    "fullName": "Hà Duy Khánh",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 33,
                    "employeeCode": "MB0040",
                    "fullName": "Nguyễn Anh Tú",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 34,
                    "employeeCode": "MB0041",
                    "fullName": "Phạm Hồng Phú",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 35,
                    "employeeCode": "MB0042",
                    "fullName": "Phạm Kiên Cường",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 36,
                    "employeeCode": "MB0043",
                    "fullName": "Lê Mỹ Phương",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 37,
                    "employeeCode": "MB0044",
                    "fullName": "Trương Ngọc Nam",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 38,
                    "employeeCode": "MB0045",
                    "fullName": "Nguyễn Thiên Bách",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 39,
                    "employeeCode": "MB0046",
                    "fullName": "Nguyễn Hoàng Hải",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 40,
                    "employeeCode": "MB0047",
                    "fullName": "Trần Minh Vũ",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 41,
                    "employeeCode": "MB0048",
                    "fullName": "Cao Xuân Quân",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 42,
                    "employeeCode": "MB0049",
                    "fullName": "Chu Minh Thành",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 43,
                    "employeeCode": "MB0050",
                    "fullName": "Nguyễn Xuân Đồng",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 44,
                    "employeeCode": "MB0051",
                    "fullName": "Nguyễn Phú Mạnh",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 45,
                    "employeeCode": "MB0052",
                    "fullName": "Hoàng Anh Sơn",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 46,
                    "employeeCode": "MB0053",
                    "fullName": "Dương Hồng Hà",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 85,
                    "employeeCode": "MB000025",
                    "fullName": "Đặng Đình Tứ",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "Phòng Quan hệ khách hàng %3cb%3e1%3c%2fb%3e"
                },
                {
                    "employeeId": 1,
                    "employeeCode": "MB0001",
                    "fullName": "Lương Thị Phượng",
                    "email": "",
                    "positionName": "Nhân viên kinh doanh CN. 3 tháng 2",
                    "organizationName": "P. Dịch vụ Khách hàng &lt;b&gt;1&lt;/b&gt;"
                },
                {
                    "employeeId": 83,
                    "employeeCode": "MB000023",
                    "fullName": "Đức",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "Phòng Kế toán và Dịch vụ khách hàng <b>1</b>"
                }
            ]
        },
        {
            "level" : 6,
            "approvers" : [
                {
                    "employeeId": 2,
                    "employeeCode": "MB0002",
                    "fullName": "Đặng Đình Tứ",
                    "email": "",
                    "positionName": "Nhân viên kinh doanh CN. An Phú",
                    "organizationName": "Bộ phận Hành chính - Tổng hợp"
                },
                {
                    "employeeId": 82,
                    "employeeCode": "MB000022",
                    "fullName": "Lê Thị Thu Trang",
                    "email": "",
                    "positionName": "Chuyên gia cao cấp CN. Hưng Yên",
                    "organizationName": "Ban Giám đốc"
                },
                {
                    "employeeId": 3,
                    "employeeCode": "MB0010",
                    "fullName": "Nguyễn Thành Tuân",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "Phòng Quan hệ khách hàng %3cb%3e1%3c%2fb%3e"
                },
                {
                    "employeeId": 4,
                    "employeeCode": "MB0011",
                    "fullName": "Hồ Hoàng Hà",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 5,
                    "employeeCode": "MB0012",
                    "fullName": "Vũ Thị Ánh Liên",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 6,
                    "employeeCode": "MB0013",
                    "fullName": "Phạm Văn Hải",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 7,
                    "employeeCode": "MB0014",
                    "fullName": "Nguyễn Quang Thời",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 8,
                    "employeeCode": "MB0015",
                    "fullName": "Đặng Anh Tuấn",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 9,
                    "employeeCode": "MB0016",
                    "fullName": "Trần Anh Đức",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 10,
                    "employeeCode": "MB0017",
                    "fullName": "Phạm Văn Tùng",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 11,
                    "employeeCode": "MB0018",
                    "fullName": "Phùng Quang Huy",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 12,
                    "employeeCode": "MB0019",
                    "fullName": "Nguyễn Văn Thiện",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 13,
                    "employeeCode": "MB0020",
                    "fullName": "Cao Anh Tuấn",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 14,
                    "employeeCode": "MB0021",
                    "fullName": "Đinh Duy Anh",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 15,
                    "employeeCode": "MB0022",
                    "fullName": "Nguyễn Hữu Dũng",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 16,
                    "employeeCode": "MB0023",
                    "fullName": "Hồ Anh Tuấn",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 17,
                    "employeeCode": "MB0024",
                    "fullName": "Trác Văn Kỳ",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 18,
                    "employeeCode": "MB0025",
                    "fullName": "Nguyễn Hoài Sơn",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 19,
                    "employeeCode": "MB0026",
                    "fullName": "Trần Doãn Xuân",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 20,
                    "employeeCode": "MB0027",
                    "fullName": "Hoàng Văn Sơn",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 21,
                    "employeeCode": "MB0028",
                    "fullName": "Trịnh Thanh Nam",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 23,
                    "employeeCode": "000005",
                    "fullName": "Trần Quang Hà",
                    "email": "",
                    "positionName": "Chuyên gia cao cấp CN. Hưng Yên",
                    "organizationName": "Ban Giám đốc"
                },
                {
                    "employeeId": 24,
                    "employeeCode": "MB0031",
                    "fullName": "Bùi Đức Mạnh",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 25,
                    "employeeCode": "MB0032",
                    "fullName": "Đinh Quang Trung",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 26,
                    "employeeCode": "MB0033",
                    "fullName": "Hoàng Văn Hiệp",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 27,
                    "employeeCode": "MB0034",
                    "fullName": "Nguyễn Trắc Toàn",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 28,
                    "employeeCode": "MB0035",
                    "fullName": "Bùi Văn Hùng",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 29,
                    "employeeCode": "MB0036",
                    "fullName": "Lê Sỹ Đức",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 30,
                    "employeeCode": "MB0037",
                    "fullName": "Bùi Huy Tùng",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 31,
                    "employeeCode": "MB0038",
                    "fullName": "Hoàng Thịnh Vượng",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 32,
                    "employeeCode": "MB0039",
                    "fullName": "Hà Duy Khánh",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 33,
                    "employeeCode": "MB0040",
                    "fullName": "Nguyễn Anh Tú",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 34,
                    "employeeCode": "MB0041",
                    "fullName": "Phạm Hồng Phú",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 35,
                    "employeeCode": "MB0042",
                    "fullName": "Phạm Kiên Cường",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 36,
                    "employeeCode": "MB0043",
                    "fullName": "Lê Mỹ Phương",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 37,
                    "employeeCode": "MB0044",
                    "fullName": "Trương Ngọc Nam",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 38,
                    "employeeCode": "MB0045",
                    "fullName": "Nguyễn Thiên Bách",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 39,
                    "employeeCode": "MB0046",
                    "fullName": "Nguyễn Hoàng Hải",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 40,
                    "employeeCode": "MB0047",
                    "fullName": "Trần Minh Vũ",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 41,
                    "employeeCode": "MB0048",
                    "fullName": "Cao Xuân Quân",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 42,
                    "employeeCode": "MB0049",
                    "fullName": "Chu Minh Thành",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 43,
                    "employeeCode": "MB0050",
                    "fullName": "Nguyễn Xuân Đồng",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 44,
                    "employeeCode": "MB0051",
                    "fullName": "Nguyễn Phú Mạnh",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 45,
                    "employeeCode": "MB0052",
                    "fullName": "Hoàng Anh Sơn",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 46,
                    "employeeCode": "MB0053",
                    "fullName": "Dương Hồng Hà",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 85,
                    "employeeCode": "MB000025",
                    "fullName": "Đặng Đình Tứ",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "Phòng Quan hệ khách hàng %3cb%3e1%3c%2fb%3e"
                },
                {
                    "employeeId": 1,
                    "employeeCode": "MB0001",
                    "fullName": "Lương Thị Phượng",
                    "email": "",
                    "positionName": "Nhân viên kinh doanh CN. 3 tháng 2",
                    "organizationName": "P. Dịch vụ Khách hàng &lt;b&gt;1&lt;/b&gt;"
                },
                {
                    "employeeId": 83,
                    "employeeCode": "MB000023",
                    "fullName": "Đức",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "Phòng Kế toán và Dịch vụ khách hàng <b>1</b>"
                }
            ]
        },
        {
            "level" : 7,
            "approvers" : [
                {
                    "employeeId": 2,
                    "employeeCode": "MB0002",
                    "fullName": "Đặng Đình Tứ",
                    "email": "",
                    "positionName": "Nhân viên kinh doanh CN. An Phú",
                    "organizationName": "Bộ phận Hành chính - Tổng hợp"
                },
                {
                    "employeeId": 82,
                    "employeeCode": "MB000022",
                    "fullName": "Lê Thị Thu Trang",
                    "email": "",
                    "positionName": "Chuyên gia cao cấp CN. Hưng Yên",
                    "organizationName": "Ban Giám đốc"
                },
                {
                    "employeeId": 3,
                    "employeeCode": "MB0010",
                    "fullName": "Nguyễn Thành Tuân",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "Phòng Quan hệ khách hàng %3cb%3e1%3c%2fb%3e"
                },
                {
                    "employeeId": 4,
                    "employeeCode": "MB0011",
                    "fullName": "Hồ Hoàng Hà",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 5,
                    "employeeCode": "MB0012",
                    "fullName": "Vũ Thị Ánh Liên",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 6,
                    "employeeCode": "MB0013",
                    "fullName": "Phạm Văn Hải",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 7,
                    "employeeCode": "MB0014",
                    "fullName": "Nguyễn Quang Thời",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 8,
                    "employeeCode": "MB0015",
                    "fullName": "Đặng Anh Tuấn",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 9,
                    "employeeCode": "MB0016",
                    "fullName": "Trần Anh Đức",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 10,
                    "employeeCode": "MB0017",
                    "fullName": "Phạm Văn Tùng",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 11,
                    "employeeCode": "MB0018",
                    "fullName": "Phùng Quang Huy",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 12,
                    "employeeCode": "MB0019",
                    "fullName": "Nguyễn Văn Thiện",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 13,
                    "employeeCode": "MB0020",
                    "fullName": "Cao Anh Tuấn",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 14,
                    "employeeCode": "MB0021",
                    "fullName": "Đinh Duy Anh",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 15,
                    "employeeCode": "MB0022",
                    "fullName": "Nguyễn Hữu Dũng",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 16,
                    "employeeCode": "MB0023",
                    "fullName": "Hồ Anh Tuấn",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 17,
                    "employeeCode": "MB0024",
                    "fullName": "Trác Văn Kỳ",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 18,
                    "employeeCode": "MB0025",
                    "fullName": "Nguyễn Hoài Sơn",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 19,
                    "employeeCode": "MB0026",
                    "fullName": "Trần Doãn Xuân",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 20,
                    "employeeCode": "MB0027",
                    "fullName": "Hoàng Văn Sơn",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 21,
                    "employeeCode": "MB0028",
                    "fullName": "Trịnh Thanh Nam",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 23,
                    "employeeCode": "000005",
                    "fullName": "Trần Quang Hà",
                    "email": "",
                    "positionName": "Chuyên gia cao cấp CN. Hưng Yên",
                    "organizationName": "Ban Giám đốc"
                },
                {
                    "employeeId": 24,
                    "employeeCode": "MB0031",
                    "fullName": "Bùi Đức Mạnh",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 25,
                    "employeeCode": "MB0032",
                    "fullName": "Đinh Quang Trung",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 26,
                    "employeeCode": "MB0033",
                    "fullName": "Hoàng Văn Hiệp",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 27,
                    "employeeCode": "MB0034",
                    "fullName": "Nguyễn Trắc Toàn",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 28,
                    "employeeCode": "MB0035",
                    "fullName": "Bùi Văn Hùng",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 29,
                    "employeeCode": "MB0036",
                    "fullName": "Lê Sỹ Đức",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 30,
                    "employeeCode": "MB0037",
                    "fullName": "Bùi Huy Tùng",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 31,
                    "employeeCode": "MB0038",
                    "fullName": "Hoàng Thịnh Vượng",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 32,
                    "employeeCode": "MB0039",
                    "fullName": "Hà Duy Khánh",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 33,
                    "employeeCode": "MB0040",
                    "fullName": "Nguyễn Anh Tú",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 34,
                    "employeeCode": "MB0041",
                    "fullName": "Phạm Hồng Phú",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 35,
                    "employeeCode": "MB0042",
                    "fullName": "Phạm Kiên Cường",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 36,
                    "employeeCode": "MB0043",
                    "fullName": "Lê Mỹ Phương",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 37,
                    "employeeCode": "MB0044",
                    "fullName": "Trương Ngọc Nam",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 38,
                    "employeeCode": "MB0045",
                    "fullName": "Nguyễn Thiên Bách",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 39,
                    "employeeCode": "MB0046",
                    "fullName": "Nguyễn Hoàng Hải",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 40,
                    "employeeCode": "MB0047",
                    "fullName": "Trần Minh Vũ",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 41,
                    "employeeCode": "MB0048",
                    "fullName": "Cao Xuân Quân",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 42,
                    "employeeCode": "MB0049",
                    "fullName": "Chu Minh Thành",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 43,
                    "employeeCode": "MB0050",
                    "fullName": "Nguyễn Xuân Đồng",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 44,
                    "employeeCode": "MB0051",
                    "fullName": "Nguyễn Phú Mạnh",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 45,
                    "employeeCode": "MB0052",
                    "fullName": "Hoàng Anh Sơn",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 46,
                    "employeeCode": "MB0053",
                    "fullName": "Dương Hồng Hà",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 85,
                    "employeeCode": "MB000025",
                    "fullName": "Đặng Đình Tứ",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "Phòng Quan hệ khách hàng %3cb%3e1%3c%2fb%3e"
                },
                {
                    "employeeId": 1,
                    "employeeCode": "MB0001",
                    "fullName": "Lương Thị Phượng",
                    "email": "",
                    "positionName": "Nhân viên kinh doanh CN. 3 tháng 2",
                    "organizationName": "P. Dịch vụ Khách hàng &lt;b&gt;1&lt;/b&gt;"
                },
                {
                    "employeeId": 83,
                    "employeeCode": "MB000023",
                    "fullName": "Đức",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "Phòng Kế toán và Dịch vụ khách hàng <b>1</b>"
                }
            ]
        },
        {
            "level" : 8,
            "approvers" : [
                {
                    "employeeId": 2,
                    "employeeCode": "MB0002",
                    "fullName": "Đặng Đình Tứ",
                    "email": "",
                    "positionName": "Nhân viên kinh doanh CN. An Phú",
                    "organizationName": "Bộ phận Hành chính - Tổng hợp"
                },
                {
                    "employeeId": 82,
                    "employeeCode": "MB000022",
                    "fullName": "Lê Thị Thu Trang",
                    "email": "",
                    "positionName": "Chuyên gia cao cấp CN. Hưng Yên",
                    "organizationName": "Ban Giám đốc"
                },
                {
                    "employeeId": 3,
                    "employeeCode": "MB0010",
                    "fullName": "Nguyễn Thành Tuân",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "Phòng Quan hệ khách hàng %3cb%3e1%3c%2fb%3e"
                },
                {
                    "employeeId": 4,
                    "employeeCode": "MB0011",
                    "fullName": "Hồ Hoàng Hà",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 5,
                    "employeeCode": "MB0012",
                    "fullName": "Vũ Thị Ánh Liên",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 6,
                    "employeeCode": "MB0013",
                    "fullName": "Phạm Văn Hải",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 7,
                    "employeeCode": "MB0014",
                    "fullName": "Nguyễn Quang Thời",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 8,
                    "employeeCode": "MB0015",
                    "fullName": "Đặng Anh Tuấn",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 9,
                    "employeeCode": "MB0016",
                    "fullName": "Trần Anh Đức",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 10,
                    "employeeCode": "MB0017",
                    "fullName": "Phạm Văn Tùng",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 11,
                    "employeeCode": "MB0018",
                    "fullName": "Phùng Quang Huy",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 12,
                    "employeeCode": "MB0019",
                    "fullName": "Nguyễn Văn Thiện",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 13,
                    "employeeCode": "MB0020",
                    "fullName": "Cao Anh Tuấn",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 14,
                    "employeeCode": "MB0021",
                    "fullName": "Đinh Duy Anh",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 15,
                    "employeeCode": "MB0022",
                    "fullName": "Nguyễn Hữu Dũng",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 16,
                    "employeeCode": "MB0023",
                    "fullName": "Hồ Anh Tuấn",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 17,
                    "employeeCode": "MB0024",
                    "fullName": "Trác Văn Kỳ",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 18,
                    "employeeCode": "MB0025",
                    "fullName": "Nguyễn Hoài Sơn",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 19,
                    "employeeCode": "MB0026",
                    "fullName": "Trần Doãn Xuân",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 20,
                    "employeeCode": "MB0027",
                    "fullName": "Hoàng Văn Sơn",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 21,
                    "employeeCode": "MB0028",
                    "fullName": "Trịnh Thanh Nam",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 23,
                    "employeeCode": "000005",
                    "fullName": "Trần Quang Hà",
                    "email": "",
                    "positionName": "Chuyên gia cao cấp CN. Hưng Yên",
                    "organizationName": "Ban Giám đốc"
                },
                {
                    "employeeId": 24,
                    "employeeCode": "MB0031",
                    "fullName": "Bùi Đức Mạnh",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 25,
                    "employeeCode": "MB0032",
                    "fullName": "Đinh Quang Trung",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 26,
                    "employeeCode": "MB0033",
                    "fullName": "Hoàng Văn Hiệp",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 27,
                    "employeeCode": "MB0034",
                    "fullName": "Nguyễn Trắc Toàn",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 28,
                    "employeeCode": "MB0035",
                    "fullName": "Bùi Văn Hùng",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 29,
                    "employeeCode": "MB0036",
                    "fullName": "Lê Sỹ Đức",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 30,
                    "employeeCode": "MB0037",
                    "fullName": "Bùi Huy Tùng",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 31,
                    "employeeCode": "MB0038",
                    "fullName": "Hoàng Thịnh Vượng",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 32,
                    "employeeCode": "MB0039",
                    "fullName": "Hà Duy Khánh",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 33,
                    "employeeCode": "MB0040",
                    "fullName": "Nguyễn Anh Tú",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 34,
                    "employeeCode": "MB0041",
                    "fullName": "Phạm Hồng Phú",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 35,
                    "employeeCode": "MB0042",
                    "fullName": "Phạm Kiên Cường",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 36,
                    "employeeCode": "MB0043",
                    "fullName": "Lê Mỹ Phương",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 37,
                    "employeeCode": "MB0044",
                    "fullName": "Trương Ngọc Nam",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 38,
                    "employeeCode": "MB0045",
                    "fullName": "Nguyễn Thiên Bách",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 39,
                    "employeeCode": "MB0046",
                    "fullName": "Nguyễn Hoàng Hải",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 40,
                    "employeeCode": "MB0047",
                    "fullName": "Trần Minh Vũ",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 41,
                    "employeeCode": "MB0048",
                    "fullName": "Cao Xuân Quân",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 42,
                    "employeeCode": "MB0049",
                    "fullName": "Chu Minh Thành",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 43,
                    "employeeCode": "MB0050",
                    "fullName": "Nguyễn Xuân Đồng",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 44,
                    "employeeCode": "MB0051",
                    "fullName": "Nguyễn Phú Mạnh",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 45,
                    "employeeCode": "MB0052",
                    "fullName": "Hoàng Anh Sơn",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 46,
                    "employeeCode": "MB0053",
                    "fullName": "Dương Hồng Hà",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 85,
                    "employeeCode": "MB000025",
                    "fullName": "Đặng Đình Tứ",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "Phòng Quan hệ khách hàng %3cb%3e1%3c%2fb%3e"
                },
                {
                    "employeeId": 1,
                    "employeeCode": "MB0001",
                    "fullName": "Lương Thị Phượng",
                    "email": "",
                    "positionName": "Nhân viên kinh doanh CN. 3 tháng 2",
                    "organizationName": "P. Dịch vụ Khách hàng &lt;b&gt;1&lt;/b&gt;"
                },
                {
                    "employeeId": 83,
                    "employeeCode": "MB000023",
                    "fullName": "Đức",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "Phòng Kế toán và Dịch vụ khách hàng <b>1</b>"
                }
            ]
        },
        {
            "level" : 9,
            "approvers" : [
                {
                    "employeeId": 2,
                    "employeeCode": "MB0002",
                    "fullName": "Đặng Đình Tứ",
                    "email": "",
                    "positionName": "Nhân viên kinh doanh CN. An Phú",
                    "organizationName": "Bộ phận Hành chính - Tổng hợp"
                },
                {
                    "employeeId": 82,
                    "employeeCode": "MB000022",
                    "fullName": "Lê Thị Thu Trang",
                    "email": "",
                    "positionName": "Chuyên gia cao cấp CN. Hưng Yên",
                    "organizationName": "Ban Giám đốc"
                },
                {
                    "employeeId": 3,
                    "employeeCode": "MB0010",
                    "fullName": "Nguyễn Thành Tuân",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "Phòng Quan hệ khách hàng %3cb%3e1%3c%2fb%3e"
                },
                {
                    "employeeId": 4,
                    "employeeCode": "MB0011",
                    "fullName": "Hồ Hoàng Hà",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 5,
                    "employeeCode": "MB0012",
                    "fullName": "Vũ Thị Ánh Liên",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 6,
                    "employeeCode": "MB0013",
                    "fullName": "Phạm Văn Hải",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 7,
                    "employeeCode": "MB0014",
                    "fullName": "Nguyễn Quang Thời",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 8,
                    "employeeCode": "MB0015",
                    "fullName": "Đặng Anh Tuấn",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 9,
                    "employeeCode": "MB0016",
                    "fullName": "Trần Anh Đức",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 10,
                    "employeeCode": "MB0017",
                    "fullName": "Phạm Văn Tùng",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 11,
                    "employeeCode": "MB0018",
                    "fullName": "Phùng Quang Huy",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 12,
                    "employeeCode": "MB0019",
                    "fullName": "Nguyễn Văn Thiện",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 13,
                    "employeeCode": "MB0020",
                    "fullName": "Cao Anh Tuấn",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 14,
                    "employeeCode": "MB0021",
                    "fullName": "Đinh Duy Anh",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 15,
                    "employeeCode": "MB0022",
                    "fullName": "Nguyễn Hữu Dũng",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 16,
                    "employeeCode": "MB0023",
                    "fullName": "Hồ Anh Tuấn",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 17,
                    "employeeCode": "MB0024",
                    "fullName": "Trác Văn Kỳ",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 18,
                    "employeeCode": "MB0025",
                    "fullName": "Nguyễn Hoài Sơn",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 19,
                    "employeeCode": "MB0026",
                    "fullName": "Trần Doãn Xuân",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 20,
                    "employeeCode": "MB0027",
                    "fullName": "Hoàng Văn Sơn",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 21,
                    "employeeCode": "MB0028",
                    "fullName": "Trịnh Thanh Nam",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 23,
                    "employeeCode": "000005",
                    "fullName": "Trần Quang Hà",
                    "email": "",
                    "positionName": "Chuyên gia cao cấp CN. Hưng Yên",
                    "organizationName": "Ban Giám đốc"
                },
                {
                    "employeeId": 24,
                    "employeeCode": "MB0031",
                    "fullName": "Bùi Đức Mạnh",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 25,
                    "employeeCode": "MB0032",
                    "fullName": "Đinh Quang Trung",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 26,
                    "employeeCode": "MB0033",
                    "fullName": "Hoàng Văn Hiệp",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 27,
                    "employeeCode": "MB0034",
                    "fullName": "Nguyễn Trắc Toàn",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 28,
                    "employeeCode": "MB0035",
                    "fullName": "Bùi Văn Hùng",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 29,
                    "employeeCode": "MB0036",
                    "fullName": "Lê Sỹ Đức",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 30,
                    "employeeCode": "MB0037",
                    "fullName": "Bùi Huy Tùng",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 31,
                    "employeeCode": "MB0038",
                    "fullName": "Hoàng Thịnh Vượng",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 32,
                    "employeeCode": "MB0039",
                    "fullName": "Hà Duy Khánh",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 33,
                    "employeeCode": "MB0040",
                    "fullName": "Nguyễn Anh Tú",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 34,
                    "employeeCode": "MB0041",
                    "fullName": "Phạm Hồng Phú",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 35,
                    "employeeCode": "MB0042",
                    "fullName": "Phạm Kiên Cường",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 36,
                    "employeeCode": "MB0043",
                    "fullName": "Lê Mỹ Phương",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 37,
                    "employeeCode": "MB0044",
                    "fullName": "Trương Ngọc Nam",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 38,
                    "employeeCode": "MB0045",
                    "fullName": "Nguyễn Thiên Bách",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 39,
                    "employeeCode": "MB0046",
                    "fullName": "Nguyễn Hoàng Hải",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 40,
                    "employeeCode": "MB0047",
                    "fullName": "Trần Minh Vũ",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 41,
                    "employeeCode": "MB0048",
                    "fullName": "Cao Xuân Quân",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "Ban Giám Đốc"
                },
                {
                    "employeeId": 42,
                    "employeeCode": "MB0049",
                    "fullName": "Chu Minh Thành",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 43,
                    "employeeCode": "MB0050",
                    "fullName": "Nguyễn Xuân Đồng",
                    "email": "",
                    "positionName": "Trưởng phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 44,
                    "employeeCode": "MB0051",
                    "fullName": "Nguyễn Phú Mạnh",
                    "email": "",
                    "positionName": "Trưởng nhóm",
                    "organizationName": "BP Quan hệ khách hàng"
                },
                {
                    "employeeId": 45,
                    "employeeCode": "MB0052",
                    "fullName": "Hoàng Anh Sơn",
                    "email": "",
                    "positionName": "Trưởng phòng quan hệ khách hàng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 46,
                    "employeeCode": "MB0053",
                    "fullName": "Dương Hồng Hà",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "BP Kế toán và Dịch vụ khách hàng"
                },
                {
                    "employeeId": 85,
                    "employeeCode": "MB000025",
                    "fullName": "Đặng Đình Tứ",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "Phòng Quan hệ khách hàng %3cb%3e1%3c%2fb%3e"
                },
                {
                    "employeeId": 1,
                    "employeeCode": "MB0001",
                    "fullName": "Lương Thị Phượng",
                    "email": "",
                    "positionName": "Nhân viên kinh doanh CN. 3 tháng 2",
                    "organizationName": "P. Dịch vụ Khách hàng &lt;b&gt;1&lt;/b&gt;"
                },
                {
                    "employeeId": 83,
                    "employeeCode": "MB000023",
                    "fullName": "Đức",
                    "email": "",
                    "positionName": "Phó phòng",
                    "organizationName": "Phòng Kế toán và Dịch vụ khách hàng <b>1</b>"
                }
            ]
        }
    ]
}