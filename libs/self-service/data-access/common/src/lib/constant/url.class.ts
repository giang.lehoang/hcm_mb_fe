export class UrlConstant {
  public static readonly API_VERSION = '/v1.0';
  public static readonly DOWNLOAD_FILE_ATTACH = '/download/file';
  public static readonly EVALUATE_INFO = '/result/employee'

  public static readonly SEARCH_FORM = {
    TAX_REGISTERS: '/personal/tax-registers',
    LOG_ACTIONS: '/personal/log-actions'
  };

  public static readonly TAX_REGISTERS = {
    UPDATE_WORK_FLOW: '/personal/tax-number-registers/update-work-flow/{id}/{status}',
    DELETE_DATA: '/personal/tax-number-registers/{id}',
    SAVE: '/personal/tax-number-registers',
    GET_BY_ID: '/personal/tax-number-registers/{id}',
    GET_RECENT_REGISTER: '/personal/tax-number-registers/recent-register'
  };

  public static readonly DEPENDENT_REGISTERS = {
    SAVE_DEPENDENT: '/personal/dependent-registers',
    GET_BY_ID: '/personal/dependent-registers/{id}',
    UPDATE_WORK_FLOW: '/personal/dependent-registers/update-work-flow/{id}/{status}',
    DELETE_DATA: '/personal/dependent-registers/{id}'
  };

  public static readonly RELATIVES_INFO = {
    LIST_BY_EMP_ID: '/personal/family-relationships/list/{employeeId}',
    PREFIX: '/personal/family-relationships',
    LIST: '/list/{employeeId}',
    SAVE: '',
    DELETE: '/{familyRelationshipId}',
    DETAIL: '/{familyRelationshipId}'
  };

  public static readonly DEPENDENT_INFO = {
    LIST: '/personal/dependent-persons/{employeeId}'
  };

  public static readonly CATALOGS = {
    PREFIX: '/lookup-values',
    CONTRACT_TYPES: '/contract-types',
    JOBS: '/mp-jobs',
    PROJECTS: '/mp-projects',
    SALARY_GRADES: '/salary-grades',
    SALARY_RANKS: '/salary-ranks',
    DOCUMENT_TYPES: '/document-types',
    MB_POSITIONS: '/mp-positions/org/{orgId}'
  };

  public static readonly DECLARATION_REGISTERS = {
    SAVE: '/personal/declaration-registers',
    CHECK: '/personal/declaration-registers/check-rev-invoice',
    GET_BY_ID: '/personal/declaration-registers/{id}',
    GET_LIST_YEAR: '/lock-registrations'
  };

  public static readonly INVOICE_REQUEST = {
    SAVE: '/personal/invoice-requests',
    GET_BY_ID: '/personal/invoice-requests/{id}'
  };

  public static readonly CONFIRM_REGISTERS = {
    SAVE: '/personal/confirm-registers',
    GET_BY_ID: '/personal/confirm-registers/{id}'
  };

  public static readonly URL = {
    TAX_REGISTER_PAGE: '/ss/personal-tax/tax-registers',

    REGISTER_NEW_CREATE: '/ss/personal-tax/tax-registers/create',
    REGISTER_NEW_EDIT: '/ss/personal-tax/tax-registers/edit?registerId=',
    REGISTER_NEW_DETAIL: '/ss/personal-tax/tax-registers/detail?registerId=',

    ALTER_TAX_INFO_CREATE: '/ss/personal-tax/tax-registers/register-change',
    ALTER_TAX_INFO_EDIT: '/ss/personal-tax/tax-registers/register-change-edit?registerId=',
    ALTER_TAX_INFO_DETAIL: '/ss/personal-tax/tax-registers/register-change-detail?registerId=',

    REGISTER_NEW_DP_CREATE: '/ss/personal-tax/tax-registers/dependent-registers',
    REGISTER_NEW_DP_EDIT: '/ss/personal-tax/tax-registers/dependent-edit?registerId=',
    REGISTER_NEW_DP_DETAIL: '/ss/personal-tax/tax-registers/dependent-detail?registerId=',

    REGISTER_LOW_DP_CREATE: '/ss/personal-tax/tax-registers/dependent-deduction',
    REGISTER_LOW_DP_EDIT: '/ss/personal-tax/tax-registers/dependent-deduction-edit?registerId=',
    REGISTER_LOW_DP_DETAIL: '/ss/personal-tax/tax-registers/dependent-deduction-detail?registerId=',

    REGISTER_TAX_SETTLEMENT_CREATE: '/ss/personal-tax/tax-registers/declaration-register',
    REGISTER_TAX_SETTLEMENT_EDIT: '/ss/personal-tax/tax-registers/declaration-edit?registerId=',
    REGISTER_TAX_SETTLEMENT_DETAIL: '/ss/personal-tax/tax-registers/declaration-detail?registerId=',

    REGISTER_RECEIVE_RECEIPT_CREATE: '/ss/personal-tax/tax-registers/receive-invoices-register',
    REGISTER_RECEIVE_RECEIPT_EDIT: '/ss/personal-tax/tax-registers/receive-invoices-edit?registerId=',
    REGISTER_RECEIVE_RECEIPT_DETAIL: '/ss/personal-tax/tax-registers/receive-invoices-detail?registerId=',

    CONFIRM_AUTHORIZED_CREATE: '/ss/personal-tax/tax-registers/provide-info',
    CONFIRM_AUTHORIZED_EDIT: '/ss/personal-tax/tax-registers/provide-info-edit?registerId=',
    CONFIRM_AUTHORIZED_DETAIL: '/ss/personal-tax/tax-registers/provide-info-detail?registerId='
  };


  public static readonly CATALOG = '/lookup-values';

  public static readonly BANKS = '/cat-banks';

  public static readonly SCHOOLS = '/schools';

  public static readonly FACULTIES = '/faculties';

  public static readonly MAJOR_LEVELS = '/major_levels';

  public static readonly DEGREE_INFOS = {
    PREFIX: '/personal/education-degree-draft',
    LIST: '/list',
    DRAFT_ACTION: '/personal/education-degree-draft',
    DRAFT_DELETE: '/personal/education-degree-draft/delete'
  };

  public static readonly EDU_HIS_INFO = {
    PREFIX: '/personal/education-process',
    LIST: '/list'
  };

  public static readonly EMPLOYEES = {
    PREFIX: '/personal',
    DATA_PICKER: '/employees/data-picker',
    PERSONAL_INFO: '/personal-information',
    IDENTITIES: '/identities',
    IDENTITY: '/personal-identities',
    DELETE: '/delete',
    CONTACT: '/contact-info',
    BANK: '/bank-accounts',
    PARTY: '/party-army',
    AVATAR: '/avatar'
  };

  // Danh sách giảm trừ gia cảnh
  public static readonly FAMILY_DEDUCTION = {
    PREFIX: '/personal/dependent-persons',
    LIST: '/{employeeId}',
    RELATIVE_INFO: '/employees/{employeeId}/family-relationships',
    SAVE: '',
    DELETE: '/{dependentPersonId}',
    DETAIL: '/{dependentPersonId}'
  };

  // Danh sách làm việc trước MB
  public static readonly WORK_BEFORE_HISTORY = {
    PREFIX: '/personal/worked-outsides',
    LIST: '/list/{employeeId}',
    SAVE: '',
    DELETE: '/{workOutsideId}',
    DETAIL: '/{workOutsideId}'
  };

  // Danh sách làm việc tại MB
  public static readonly WORK_HISTORY = {
    PREFIX: '/personal/work-processes',
    LIST: '/list/{employeeId}',
    SAVE: '',
    DELETE: '/{workProcessId}',
    DETAIL: '/{workProcessId}'
  };

  // Quá trình lương
  public static readonly SALARY_PROGRESS = {
    PREFIX: '/personal/salary-processes',
    LIST: '/list/{employeeId}',
    SAVE: '',
    DELETE: '/{salaryProcessId}',
    DETAIL: '/{salaryProcessId}'
  };

  // Quá trình tham gia dự án
  public static readonly PROJECT_HISTORY_INFO = {
    PREFIX: '/personal/project-members',
    LIST: '/list/{employeeId}',
    SAVE: '',
    DELETE: '/{projectMemberId}',
    DETAIL: '/{projectMemberId}'
  };

  // Quá trình hợp đồng
  public static readonly CONTRACT_HISTORY_INFO = {
    PREFIX: '/personal/contract-processes',
    LIST: '/list/{employeeId}',
    SAVE: '',
    DELETE: '/{contractProcessId}',
    DETAIL: '/{contractProcessId}'
  };

  // Quá trình phụ cấp
  public static readonly ALLOWANCE_HISTORY_INFO = {
    PREFIX: '/personal/allowance-processes',
    LIST: '/list/{employeeId}',
    SAVE: '',
    DELETE: '/{allowanceProcessId}',
    DETAIL: '/{allowanceProcessId}'
  };

  // Quá trình khen thưởng
  public static readonly REWARD_HISTORY_INFO = {
    PREFIX: '/personal/reward-records',
    LIST: '/list/{employeeId}',
    SAVE: '',
    DELETE: '/{rewardRecordId}',
    DETAIL: '/{rewardRecordId}'
  };

  // Quá trình kỷ luật
  public static readonly DISCIPINARY_HISTORY_INFO = {
    PREFIX: '/personal/decpline-records',
    LIST: '/list/{employeeId}',
    SAVE: '',
    DELETE: '/{decplineRecordId}',
    DETAIL: '/{decplineRecordId}'
  };

  // Hồ sơ đính kèm
  public static readonly DOCUMENT_ATTACHMENT = {
    PREFIX: '/personal/employee-profiles',
    LIST: '/list/',
    SAVE: '',
    DELETE: '/{employee-profile-id}',
    DETAIL: '/{employee-profile-id}'
  };

  public static DASHBOARD = {
    BANNER: '/banner/active',
    ARTICLE: '/news/hight-light'
  };

  public static readonly ORG_INFO = {
    PREFIX: '/org-tree',
    SEARCH_NODE: '/search-node',
    GET_BY_PARENT: '/get-by-parent/{parentId}',
  }

  public static readonly REQUESTS = {
    PREFIX: '/requests',
    SEARCH: '/requests/search',
    SAVE: '/requests/leave-save',
    DELETE: '/requests/{requestId}',
    APPROVE: '/requests/action/approve',
    REJECT: '/requests/action/reject',
    REJECT_BACKDATE: '/requests/action/reject-backdate',
    CANCEL: '/requests/action/cancel',
    APPROVERS: '/request/approvers',
    USER_LOGIN: '/requests/emp-info',
    SUBMIT: '/requests/action/submit/{requestId}',
  }

  public static readonly REASON_LEAVES = {
    PREFIX: '/reason-leaves',
    LEAVE_INFO: '/annual-leaves/leave-info',
  }

  public static readonly REQUEST_LEAVES = {
    PREFIX: '/reason-leaves',
    CACULATE_LEAVES: '/request-leaves/caculate-leaves',
  }

}
