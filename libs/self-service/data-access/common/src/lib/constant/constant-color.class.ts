export class ConstantColor {

    public static readonly TAG = {
        STATUS_IS_DECLARING: '#FFF2DA',
        STATUS_WAIT_APPROVE: '#FFF2DA',
        STATUS_FINANCIAL_ACCOUNTANT_RECEIVED: '#FFF2DA',
        STATUS_FINANCIAL_ACCOUNTANT_IS_PROCESSING: '#FFF2DA',
        STATUS_FINANCIAL_ACCOUNTANT_RETURN: '#FDE7EA',
        STATUS_TAX_AUTHORITIES_RETURN: '#FDE7EA',
        STATUS_TAX_AUTHORITIES_AGREE: '#DAF9EC',
    };

}
