import { ObjectCategory } from '@hcm-mfe/self-service/data-access/models';
import {environment} from "@hcm-mfe/shared/environment";

export class Constant {

  public static readonly SEND_BROWSE = 2;
  public static readonly SAVE_DRAFT = 1;
  public static readonly CANCEL_REQUEST = 1;
  public static readonly MAX_AGE_REDUCE = 18;
  public static readonly IS_AUTHORIZATION = 'Ủy quyền';
  public static readonly IS_NOT_AUTHORIZATION = 'Tự quyết toán';
  public static readonly TAX_REGISTER_SEARCH_URL = '/ss/personal-tax/tax-registers';
  public static readonly ACCEPT_PROVIDE_INFO = 1;
  public static readonly NOT_ACCEPT_PROVIDE_INFO = 0;
  public static readonly VN_NATION = 'Việt Nam';

  public static readonly PAPERS_CODE = environment?.production ?
    {ID_NO: 'CMT', CITIZEN_ID: 'CCCD'} : (environment?.isMbBank ? {ID_NO: '1010', CITIZEN_ID: '2080'} : {ID_NO: '03', CITIZEN_ID: '05'})


  public static readonly REQUEST_TYPES: ObjectCategory[] = [
    { label: 'selfService.taxRegisters.requestTypeSelect.registerNew', value: '1' },
    { label: 'selfService.taxRegisters.requestTypeSelect.alterTaxInfo', value: '2' },
    { label: 'selfService.taxRegisters.requestTypeSelect.registerNewDP', value: '3' },
    { label: 'selfService.taxRegisters.requestTypeSelect.registerLowDP', value: '4' },
    { label: 'selfService.taxRegisters.requestTypeSelect.registerTaxSettlement', value: '5' },
    { label: 'selfService.taxRegisters.requestTypeSelect.registerReceiveReceipt', value: '6' },
    { label: 'selfService.taxRegisters.requestTypeSelect.confirmAuthorized', value: '7' }
  ];


  public static readonly STATUSES = [
    { label: 'selfService.taxRegisters.statusSelect.isDeclaring', value: '1', color: '#F99600', bgColor: '#FFF2DA' },
    { label: 'selfService.taxRegisters.statusSelect.waitApprove', value: '2', color: '#F99600', bgColor: '#FFF2DA' },
    { label: 'selfService.taxRegisters.statusSelect.financialAccountantReceived', value: '3', color: '#F99600', bgColor: '#FFF2DA' },
    { label: 'selfService.taxRegisters.statusSelect.financialAccountantIsProcessing', value: '4', color: '#F99600', bgColor: '#FFF2DA' },
    { label: 'selfService.taxRegisters.statusSelect.selfFinancialAccountantReturn', value: '5', color: '#FA0B0B', bgColor: '#FDE7EA' },
    { label: 'selfService.taxRegisters.statusSelect.selfTaxAuthoritiesAgree', value: '6', color: '#06A561', bgColor: '#DAF9EC' },
    { label: 'selfService.taxRegisters.statusSelect.selfTaxAuthoritiesReturn', value: '7', color: '#FA0B0B', bgColor: '#FDE7EA' },
    // { label: 'selfService.taxRegisters.statusSelect.endDueOff', value: '8', color: '#FA0B0B', bgColor: '#FDE7EA' },  // no delete
    // { label: 'selfService.taxRegisters.statusSelect.proposeCancel', value: '9', color: '', bgColor: '' }, // no delete
    { label: 'selfService.taxRegisters.statusSelect.registered', value: '10', color: '#F99600', bgColor: '#FFF2DA' },
    { label: 'selfService.taxRegisters.statusSelect.confirmed', value: '11', color: '#06A561', bgColor: '#DAF9EC' }
  ];

  public static readonly ADD_NEW_OPTIONS = [
    'selfService.taxRegisters.listAddNewOption.registerNew',
    'selfService.taxRegisters.listAddNewOption.alterTaxInfo',
    'selfService.taxRegisters.listAddNewOption.registerNewDP',
    'selfService.taxRegisters.listAddNewOption.registerLowDP',
    'selfService.taxRegisters.listAddNewOption.registerTaxSettlement',
    'selfService.taxRegisters.listAddNewOption.registerReceiveReceipt',
    'selfService.taxRegisters.listAddNewOption.confirmAuthorized'
  ];

  public static readonly ACTION_PER_RECORD = {
    SEND_BROWSE: 'SEND_BROWSE',
    CANCEL_REQUEST: 'CANCEL_REQUEST',
    EDIT: 'EDIT',
    DELETE: 'DELETE',
    VIEW_HISTORY: 'VIEW_HISTORY'
  };

  public static readonly STATUS_OBJ = {
    IS_DECLARING: 1,
    WAIT_APPROVE: 2,
    FINANCIAL_ACCOUNTANT_RECEIVED: 3,
    FINANCIAL_ACCOUNTANT_IS_PROCESSING: 4,
    FINANCIAL_ACCOUNTANT_RETURN: 5,
    TAX_AUTHORITIES_AGREE: 6,
    TAX_AUTHORITIES_RETURN: 7
  };

  public static readonly REV_INVOICE_STATUS = {
    IS_REV_INVOICE: 1,
    IS_NOT_REV_INVOICE: 0
  };

  public static readonly REG_TYPE = {
    REGISTER_NEW: '1',
    ALTER_TAX_INFO: '2',
    REGISTER_NEW_DP: '3',
    REGISTER_LOW_DP: '4',
    REGISTER_TAX_SETTLEMENT: '5',
    REGISTER_RECEIVE_RECEIPT: '6',
    CONFIRM_AUTHORIZED: '7'
  };

  public static readonly REGISTRATION_TYPE = {
    DECLARATION_REGISTER: 'DECLARATION_REGISTER'
  };

  public static readonly CATALOGS = {
    GIOI_TINH: 'GIOI_TINH',
    DAN_TOC: 'DAN_TOC',
    TON_GIAO: 'TON_GIAO',
    TINH_TRANG_HON_NHAN: 'TINH_TRANG_HON_NHAN',
    TINH: 'TINH',
    HUYEN: 'HUYEN',
    XA: 'XA',
    LOAI_TAI_KHOAN: 'LOAI_TAI_KHOAN',
    LOAI_GIAY_TO: 'LOAI_GIAY_TO',
    MA_VUNG_DIEN_THOAI: 'MA_VUNG_DIEN_THOAI',
    QUOC_GIA: 'QUOC_GIA',
    DOI_TUONG_CV: 'DOI_TUONG_CV',
    CAP_BAC_QUAN_HAM: 'CAP_BAC_QUAN_HAM',
    LEVEL_NV: 'LEVEL_NV',
    LOAI_HOP_DONG: 'LOAI_HOP_DONG',
    LOAI_HINH_DT: 'LOAI_HINH_DT',
    HE_DT: 'HE_DT',
    HINHTHUC_DAOTAO: 'HINHTHUC_DAOTAO',
    XEP_LOAI_DT: 'XEP_LOAI_DT',
    MOI_QUAN_HE_NT: 'MOI_QUAN_HE_NT',
    TINH_TRANG_NT: 'TINH_TRANG_NT',
    DOITUONG_CHINHSACH: 'DOITUONG_CHINHSACH',
    CAP_QD_KHENTHUONG: 'CAP_QD_KHENTHUONG',
    HINHTHUC_KHENTHUONG: 'HINHTHUC_KHENTHUONG',
    LOAI_KHENTHUONG: 'LOAI_KHENTHUONG',
    HINHTHUC_KYLUAT: 'HINHTHUC_KYLUAT',
    LOAI_HO_SO: 'LOAI_HO_SO',
    LOAI_PHU_CAP: 'LOAI_PHU_CAP',
    CAP_QD_KYLUAT: 'CAP_QD_KYLUAT',
    LY_DO_NGHI: 'LY_DO_NGHI',
    XEP_LOAI_TOT_NGHIEP: 'XEP_LOAI_TOT_NGHIEP',
    NOI_CAP_CCCD: 'NOI_CAP_CCCD',
    NOI_CAP_CMND: 'NOI_CAP_CMND',
    TEN_CQT: 'TEN_COQUANTHUE'
  };

  public static readonly REG_TYPE_CODE = {
    TAX_CREATE: 'CREATE',
    TAX_CHANGE: 'CHANGE',
    REGISTER_CHANGE_TAX: 'CHANGE',
    REGISTER_NEW_DP: 'CREATE',
    REGISTER_LOW_DP: 'CANCEL',
    REGISTER_TAX_SETTLEMENT: 'DECLARE',
    REGISTER_RECEIVE_RECEIPT: 'REV_INVOICE'
  };

  public static readonly LOG_OBJECT_TYPE = {
    TAX_NUMBER: 'PTX_TAX_NUMBER_REGISTERS',
    DEPENDENT: 'PTX_DEPENDENT_REGISTERS',
    DECLARATION: 'PTX_DECLARATION_REGISTERS',
    CONFIRM: 'PTX_CONFIRM_REGISTERS'
  };

  public static readonly CONTRACT_TYPE_CODE = {
    HOP_DONG: 'HD',
    PHU_HOP_DONG: 'PHD',
    PHU_LUC_TAM_HOAN: 'PLTH'
  };

}
