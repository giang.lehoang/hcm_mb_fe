import { LEAVE_TYPE, PERSONAL_TYPE, REQUEST_STATUS } from "./enums";

export const PERSONAL_TYPE_DATASOURCE = [
  { label: 'Bản thân', value: PERSONAL_TYPE.PERSONAL },
  { label: 'Khác', value: PERSONAL_TYPE.OTHER }
]

export const REQUEST_STATUS_DATASOURCE = [
  { label: 'Dự thảo', value: REQUEST_STATUS.DRAFT },
  { label: 'Chờ phê duyệt', value: REQUEST_STATUS.WAIT_APPROVE },
  { label: 'Đã phê duyệt', value: REQUEST_STATUS.APPROVED },
  { label: 'Từ chối phê duyệt', value: REQUEST_STATUS.REJECT },
  { label: 'Đề nghị hủy', value: REQUEST_STATUS.REQUEST_CANCEL },
  { label: 'Đã hủy', value: REQUEST_STATUS.CANCEL },
  { label: 'Đề nghị điều chỉnh', value: REQUEST_STATUS.AJUST },
  { label: 'Chờ xét duyệt backdate', value: REQUEST_STATUS.BACK_DATE },
  { label: 'Chờ XD hủy  backdate', value: REQUEST_STATUS.CANCEL_BACKDATE }
]

export const REQUEST_APPROVE_STATUS_DATASOURCE = [
  { label: 'Đề nghị hủy', value: REQUEST_STATUS.REQUEST_CANCEL },
  { label: 'Chờ xét duyệt backdate', value: REQUEST_STATUS.BACK_DATE },
  { label: 'Chờ XD hủy backdate', value: REQUEST_STATUS.CANCEL_BACKDATE },
  { label: 'Chờ phê duyệt', value: REQUEST_STATUS.WAIT_APPROVE },
  { label: 'Đã phê duyệt', value: REQUEST_STATUS.APPROVED },
  { label: 'Từ chối phê duyệt', value: REQUEST_STATUS.REJECT }
]

export const TAG = {
    STATUS_DRAFT: '#2db7f5',
    STATUS_WAIT_APPROVE: '#36f5e8',
    STATUS_APPROVED: '#87d068',
    STATUS_REJECT: '#f50',
    STATUS_REQUEST_CANCEL: '#f2bb74',
    STATUS_CANCEL: '#d4d3cf',
    STATUS_AJUST: '#f5e942',
    WAIT_APPROVE_BACKDATE: '#f5582d',
};

export const LEAVE_TYPE_DATASOURCE = [
  { label: 'Đề nghị nghỉ', value: LEAVE_TYPE.LEAVE },
  { label: 'Đăng ký đi làm sớm', value: LEAVE_TYPE.WORK_EARLY },
  { label: 'Công tác', value: LEAVE_TYPE.MISSION },
  { label: 'Chấm công OT', value: LEAVE_TYPE.OT },
  { label: 'Chấm công thứ 7', value: LEAVE_TYPE.SATURDAY },
  { label: 'Tham gia đào tạo', value: LEAVE_TYPE.TRAINING },
]
