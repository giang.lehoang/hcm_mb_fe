import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";
import {HttpParams} from "@angular/common/http";
import * as moment from "moment";
import { DeclarationService, DependentService, TaxCommonService } from '@hcm-mfe/self-service/data-access/services';
import { StorageService } from '@hcm-mfe/shared/common/store';
import { HTTP_STATUS_CODE, STORAGE_NAME } from '@hcm-mfe/shared/common/constants';
import { SessionKey } from '@hcm-mfe/shared/common/enums';
import { BaseResponse, User, UserLogin } from '@hcm-mfe/shared/data-access/models';
import { PersonalInfo } from '@hcm-mfe/self-service/data-access/models';
import {Constant, UrlConstant} from "@hcm-mfe/self-service/data-access/common";

@Injectable({
  providedIn: 'root'
})
export class CheckDependentRegisterGuard implements CanActivate {
  employeeId: number;
  userLogin: UserLogin = new UserLogin();
  currentUser?: User;
  personalInfo?: PersonalInfo;

  constructor(
    private dependentRegistersService: DependentService,
    private dependentService: DependentService,
    private toastrService: ToastrService,
    private declarationService: DeclarationService,
    private taxCommonService: TaxCommonService,
    private translateService: TranslateService,
    private router: Router
  ) {
    this.userLogin = StorageService.get(STORAGE_NAME.USER_LOGIN);
    if (localStorage.getItem(SessionKey.CURRENCY_USER)) {
      this.currentUser = JSON.parse(localStorage.getItem(SessionKey.CURRENCY_USER) ?? '');
    }
    this.employeeId = this.userLogin.employeeId ?? this.currentUser?.empId ?? 1;
  }

    canActivate(route: ActivatedRouteSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const params = new HttpParams();
    const regType = route?.data['regType']?.value;
    const registerId = route.queryParams['registerId'];
    const listRelatives = this.dependentRegistersService.getListRelatives(this.employeeId, params);
    const listDependents = this.dependentService.getListDependents(this.employeeId, params);
    switch (regType) {
      case Constant.REQUEST_TYPES[2].value:
        return this.checkList(listRelatives, this.translateService.instant('common.notification.relativesNotExist'));
      case Constant.REQUEST_TYPES[3].value:
        return this.checkList(listDependents, this.translateService.instant('common.notification.dependentNotExist'));
      case Constant.REQUEST_TYPES[4].value:
        if (registerId) {
          return this.checkYearDeclarationRegister(registerId);
        }
        break
    }
    return true;
  }

  async checkList(listData: Observable<BaseResponse>, messageError: string): Promise<boolean> {
    const res = await listData.toPromise();
    if (res && res.code === HTTP_STATUS_CODE.OK && res.data.listData.length === 0) {
      this.toastrService.warning(messageError);
      this.router.navigateByUrl(UrlConstant.URL.TAX_REGISTER_PAGE).then();
      return false;
    }
    return true;
  }

  async checkYearDeclarationRegister(registerId: number): Promise<boolean> {
    const declarations = await this.taxCommonService.getTaxRegisterById(registerId, Constant.REG_TYPE.REGISTER_TAX_SETTLEMENT).toPromise();
    if (declarations.code === HTTP_STATUS_CODE.OK) {
      const year = declarations.data?.year;
      const res = await this.declarationService.getListYear(Constant.REGISTRATION_TYPE.DECLARATION_REGISTER, year).toPromise();
      if (res.code === HTTP_STATUS_CODE.OK && res.data.length > 0) {
        const dataDate = res.data[0];
        const fromDate = dataDate.fromDate ? moment(dataDate.fromDate, 'DD/MM/YYYY').toDate() : null;
        const toDate = dataDate.toDate ? moment(dataDate.toDate, 'DD/MM/YYYY').toDate() : null;
        const currentDate = moment(moment(new Date()).format('DD/MM/YYYY'), 'DD/MM/YYYY').toDate();
        if ((fromDate && currentDate < fromDate) || (toDate && currentDate > toDate)) {
          this.toastrService.warning(this.translateService.instant('common.notification.registrationOver'));
          this.router.navigateByUrl(UrlConstant.URL.TAX_REGISTER_PAGE).then();
          return false;
        }
      }
    }
    return true;
  }
}
