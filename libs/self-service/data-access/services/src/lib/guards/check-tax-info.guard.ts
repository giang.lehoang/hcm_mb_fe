import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Injectable, OnDestroy} from '@angular/core';
import {Observable, of} from 'rxjs';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {catchError, map} from 'rxjs/operators';
import {NzModalRef, NzModalService} from 'ng-zorro-antd/modal';
import { User, UserLogin } from '@hcm-mfe/shared/data-access/models';
import { PersonalInfo } from '@hcm-mfe/self-service/data-access/models';
import { PersonalInfoService } from '@hcm-mfe/self-service/data-access/services';
import { StorageService } from '@hcm-mfe/shared/common/store';
import { HTTP_STATUS_CODE, STORAGE_NAME } from '@hcm-mfe/shared/common/constants';
import { SessionKey } from '@hcm-mfe/shared/common/enums';
import { StringUtils } from '@hcm-mfe/shared/common/utils';
import {Constant, UrlConstant} from "@hcm-mfe/self-service/data-access/common";
import {PopupConfirmComponent} from "@hcm-mfe/self-service/ui/popup-confirm";

@Injectable({
  providedIn: 'root'
})
export class CheckTaxInfoGuard implements CanActivate, OnDestroy {
    employeeId: number | null = null;
    userLogin: UserLogin = new UserLogin();
    currentUser?: User;
    personalInfo?: PersonalInfo;
    modalRef!: NzModalRef;
    constructor(
        private personalInfoService: PersonalInfoService,
        private toastService: ToastrService,
        private translateService: TranslateService,
        private router: Router,
        private modal: NzModalService
    ) {}

  getEmpLogin() {
    this.userLogin = StorageService.get(STORAGE_NAME.USER_LOGIN);
    if (localStorage.getItem(SessionKey.CURRENCY_USER)) {
      this.currentUser = JSON.parse(localStorage.getItem(SessionKey.CURRENCY_USER) ?? '');
    }
    this.employeeId = this.userLogin.employeeId ?? this.currentUser?.empId ?? 1;
  }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        this.getEmpLogin();
        const regType = route?.data['regType']?.value;
        const personalInfoApi = this.personalInfoService.getPersonalInfo(this.employeeId ?? 1);

        return personalInfoApi.pipe(
            map((res) => {
                if (res.code === HTTP_STATUS_CODE.OK) {
                    this.personalInfo = res?.data;
                    const taxNo = this.personalInfo?.taxNo;

                    switch (regType) {
                        case Constant.REQUEST_TYPES[0].value: // dk cap moi mst
                            if (taxNo) {
                                this.toastService.warning(this.translateService.instant('common.notification.employeeTaxNoExist', {taxNo: taxNo}));
                                this.router.navigateByUrl(UrlConstant.URL.TAX_REGISTER_PAGE).then();
                                return false;
                            }
                            break;
                        case Constant.REQUEST_TYPES[1].value: // dk thay doi thong tin mst
                            if (StringUtils.isNullOrEmpty(taxNo ?? '')) {
                                this.modalRef = this.modal.create({
                                    nzContent: PopupConfirmComponent,
                                    nzWidth: 414,
                                    nzFooter: null,
                                    nzClosable: false
                                });
                                return false;
                            }
                            break;
                        default:
                            return false;
                    }

                    return true;
                } else {
                    return false;
                }
            }),
            catchError(() => of(false))
        );
    }

    ngOnDestroy(): void {
        this.modalRef?.destroy();
    }

}
