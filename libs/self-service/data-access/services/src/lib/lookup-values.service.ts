import { Injectable } from '@angular/core';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import {UrlConstant} from "@hcm-mfe/self-service/data-access/common";
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";

@Injectable({
  providedIn: 'root'
})
export class LookupValuesService extends BaseService {

  public getCatalog(typeCode: string, parentCode?: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.CATALOGS.PREFIX;
    return this.get(url, { params: parentCode ? { typeCode, parentCode } : { typeCode } });
  }

  getCatalogModuleTax(typeCode: string, isExistsAttr?: string) {
    const url = UrlConstant.API_VERSION + UrlConstant.CATALOGS.PREFIX;
    return this.get(url, { params: isExistsAttr ? { typeCode, isExistsAttr } : { typeCode } }, MICRO_SERVICE.TAX_SERVICE);
  }

}
