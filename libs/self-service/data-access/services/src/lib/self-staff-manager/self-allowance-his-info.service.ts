import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from '@hcm-mfe/self-service/data-access/common';
import { SelfAllowanceHistory } from '@hcm-mfe/self-service/data-access/models';
import {BaseResponse, CurrentPage} from "@hcm-mfe/shared/data-access/models";

@Injectable({
    providedIn: 'root'
})
export class SelfAllowanceHisInfoService extends BaseService{
  readonly baseUrl = UrlConstant.API_VERSION + UrlConstant.ALLOWANCE_HISTORY_INFO.PREFIX;

  // Lấy danh sách
  public getList(employeeId: number, param: CurrentPage): Observable<BaseResponse> {
    const url = this.baseUrl + UrlConstant.ALLOWANCE_HISTORY_INFO.LIST.replace('{employeeId}', employeeId.toString())
    return this.get(url, {params: param});
  }

// Lưu bản ghi
  public saveRecord(request: SelfAllowanceHistory) {
    const url = this.baseUrl + UrlConstant.ALLOWANCE_HISTORY_INFO.SAVE;
    return this.post(url, request);
  }


// Xóa bản ghi
  public deleteRecord(allowanceProcessId: number) {
    const url = this.baseUrl + UrlConstant.ALLOWANCE_HISTORY_INFO.DELETE.replace('{allowanceProcessId}', allowanceProcessId.toString())
    return this.delete(url);
  }
}
