import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from '@hcm-mfe/self-service/data-access/common';
import {BaseResponse, CurrentPage} from "@hcm-mfe/shared/data-access/models";

@Injectable({
    providedIn: 'root'
})
export class SelfProjectHisInfoService extends BaseService {
  readonly baseUrl = UrlConstant.API_VERSION + UrlConstant.PROJECT_HISTORY_INFO.PREFIX;

  // Lấy danh sách
  public getList(employeeId: number, param: CurrentPage): Observable<BaseResponse> {
    const url = this.baseUrl + UrlConstant.PROJECT_HISTORY_INFO.LIST.replace('{employeeId}', employeeId.toString())
    return this.get(url, {params: param});
  }

// Lưu bản ghi
  public saveRecord(request: FormData) {
    // const httpOptions = {
    //   headers: new HttpHeaders({ "Accept": "application/json" }),
    // }
    const url = this.baseUrl + UrlConstant.PROJECT_HISTORY_INFO.SAVE;
    return this.post(url, request);
  }


// Xóa bản ghi
  public deleteRecord(projectHisId: number) {
    const url = this.baseUrl + UrlConstant.PROJECT_HISTORY_INFO.DELETE.replace('{projectMemberId}', projectHisId.toString())
    return this.delete(url);
  }

  // Lấy thông tin dự án
  public getProjectList(): Observable<BaseResponse>{
    const url = UrlConstant.API_VERSION + UrlConstant.CATALOGS.PROJECTS;
    return this.get(url, {params: {jobType: 'PROJECT'}});
  }

  // Lấy thông tin vai trò dự án
  public getJobsList(): Observable<BaseResponse> {
    const url = UrlConstant.API_VERSION + UrlConstant.CATALOGS.JOBS;
    return this.get(url, {params:{jobType: 'PROJECT'}})
  }

  // Lấy thông tin tham gia dự án
  public getProjectMemberDetail(projectMemberId: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.PROJECT_HISTORY_INFO.PREFIX  + '/' + projectMemberId;
    return this.get(url);
  }

  // Lấy thông tin người quản lý
  public getManagerInfo(managerId:number) {
    const url = UrlConstant.API_VERSION + UrlConstant.EMPLOYEES.PREFIX + "/" + managerId + UrlConstant.EMPLOYEES.PERSONAL_INFO;
    return this.get(url);
  }

  public async getProjectMemberDetailWithAsync(projectMemberId: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.PROJECT_HISTORY_INFO.PREFIX  + '/' + projectMemberId;
    return await this.getWithAsync(url);
  }
}
