import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from '@hcm-mfe/self-service/data-access/common';
import { SelfWorkBeforeInfo } from '@hcm-mfe/self-service/data-access/models';
import {BaseResponse, CurrentPage} from "@hcm-mfe/shared/data-access/models";

@Injectable({
    providedIn: 'root'
})
export class SelfWorkBeforeHisService extends BaseService{
  readonly baseUrl = UrlConstant.API_VERSION + UrlConstant.WORK_BEFORE_HISTORY.PREFIX;

  // Lấy danh sách
  public getList(employeeId: number, param: CurrentPage): Observable<BaseResponse> {
    const url = this.baseUrl + UrlConstant.WORK_BEFORE_HISTORY.LIST.replace('{employeeId}', employeeId.toString())
    return this.get(url, {params: param});
  }

// Lưu bản ghi
  public saveRecord(request: SelfWorkBeforeInfo) {
    const url = this.baseUrl + UrlConstant.WORK_BEFORE_HISTORY.SAVE;
    return this.post(url, request);
  }


// Xóa bản ghi
  public deleteRecord(workOutsideId: number) {
    const url = this.baseUrl + UrlConstant.WORK_BEFORE_HISTORY.DELETE.replace('{workOutsideId}', workOutsideId.toString())
    return this.delete(url);
  }
}
