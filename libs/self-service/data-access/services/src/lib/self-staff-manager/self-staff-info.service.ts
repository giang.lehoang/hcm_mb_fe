import {Injectable} from "@angular/core";
import {BaseService} from '@hcm-mfe/shared/common/base-service';
import {UrlConstant} from '@hcm-mfe/self-service/data-access/common';

@Injectable({
    providedIn: 'root'
})
export class SelfStaffInfoService extends BaseService {

    public getCatalog(typeCode: string, parentCode?: number) {
        const url = UrlConstant.API_VERSION + UrlConstant.CATALOGS.PREFIX;
        return this.get(url, {params: parentCode ? {typeCode, parentCode} : {typeCode}});
    }

    public getListSchool() {
        const url = UrlConstant.API_VERSION + UrlConstant.SCHOOLS;
        return this.get(url);
    }

    public getListFaculty() {
        const url = UrlConstant.API_VERSION + UrlConstant.FACULTIES;
        return this.get(url);
    }

    public getListMajorLevel() {
        const url = UrlConstant.API_VERSION + UrlConstant.MAJOR_LEVELS;
        return this.get(url);
    }
}
