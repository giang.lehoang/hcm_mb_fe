import {Injectable} from "@angular/core";
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from '@hcm-mfe/self-service/data-access/common';
import {CurrentPage} from "@hcm-mfe/shared/data-access/models";

@Injectable({
    providedIn: 'root'
})
export class SelfDocumentAttachmentService extends BaseService {

  public getDocumentAttachment(employeeId: number, param: CurrentPage) {
    const url = UrlConstant.API_VERSION + UrlConstant.DOCUMENT_ATTACHMENT.PREFIX + UrlConstant.DOCUMENT_ATTACHMENT.LIST + employeeId;
    return this.get(url, {params: param});
  }

}
