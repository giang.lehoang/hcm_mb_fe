import {Injectable} from '@angular/core';
import {BehaviorSubject, Subject} from 'rxjs';
import { SelfBankInfo, SelfIdentityInfo, SelfPersonalInfo } from '@hcm-mfe/self-service/data-access/models';

@Injectable({
    providedIn: 'root'
})
export class SelfShareDataService {

  private personalInfoSource = new Subject<SelfPersonalInfo>();
  personalInfo$ = this.personalInfoSource.asObservable();
  changePersonalInfo(personalInfo: SelfPersonalInfo ) {
    this.personalInfoSource.next(personalInfo);
  }

  private employee = new BehaviorSubject<{employeeId: number | null, employeeCode: string | null}>({employeeId: null, employeeCode: null});
  public employee$ = this.employee.asObservable();
  changeEmployee(employee: {employeeId: number, employeeCode: string}) {
    this.employee.next(employee);
  }
  getEmployee() { return this.employee.getValue() }

  private identityInfoSource = new Subject<SelfIdentityInfo>();
  identityInfo$ = this.identityInfoSource.asObservable();
  changeIdentityInfo(identityInfo: SelfIdentityInfo) {
    this.identityInfoSource.next(identityInfo)
  }

  private bankInfoSource = new Subject<SelfBankInfo>();
  bankInfo$ = this.bankInfoSource.asObservable();
  changeBankInfo(bankInfo: SelfBankInfo) {
    this.bankInfoSource.next(bankInfo)
  }

}
