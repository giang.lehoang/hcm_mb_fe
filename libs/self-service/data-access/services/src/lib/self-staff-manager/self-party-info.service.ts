import {Injectable} from "@angular/core";
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from '@hcm-mfe/self-service/data-access/common';
import {SelfPartyInfo} from "@hcm-mfe/self-service/data-access/models";

@Injectable({
    providedIn: 'root'
})
export class SelfPartyInfoService extends BaseService {

  public getPartyInfo(employeeId: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.EMPLOYEES.PREFIX + '/' + employeeId + UrlConstant.EMPLOYEES.PARTY;
    return this.get(url);
  }

  public savePartyInfo(employeeId: number, partyInfo: SelfPartyInfo) {
    const url = UrlConstant.API_VERSION + UrlConstant.EMPLOYEES.PREFIX + '/' + employeeId + UrlConstant.EMPLOYEES.PARTY;
    return this.post(url, partyInfo);
  }
}
