import {Injectable} from '@angular/core';
import {BaseService} from '@hcm-mfe/shared/common/base-service';
import {UrlConstant} from '@hcm-mfe/self-service/data-access/common';

@Injectable({
    providedIn: 'root'
})
export class SelfIdentityInfoService extends BaseService {

    public getIdentityInfoV2(employeeId: number) {
        const url = UrlConstant.API_VERSION + UrlConstant.EMPLOYEES.PREFIX + UrlConstant.EMPLOYEES.IDENTITY + '/' + employeeId;
        return this.get(url);
    }

    public saveIdentityInfo(formData: FormData) {
        const url = UrlConstant.API_VERSION + UrlConstant.EMPLOYEES.PREFIX + UrlConstant.EMPLOYEES.IDENTITY;
        return this.post(url, formData);
    }

    public deleteIdentityInfo(identityInfoId: number) {
        const url = UrlConstant.API_VERSION + UrlConstant.EMPLOYEES.PREFIX + UrlConstant.EMPLOYEES.IDENTITY + UrlConstant.EMPLOYEES.DELETE + '/' + identityInfoId;
        return this.delete(url);
    }

    public rollBack(draftId: number) {
        const url = UrlConstant.API_VERSION + UrlConstant.EMPLOYEES.PREFIX + UrlConstant.EMPLOYEES.IDENTITY + '/rollback' + '/' + draftId;
        return this.get(url);
    }

}
