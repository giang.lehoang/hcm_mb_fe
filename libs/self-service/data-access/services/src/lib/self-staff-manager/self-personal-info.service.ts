import {Injectable} from "@angular/core";
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from '@hcm-mfe/self-service/data-access/common';
import { SelfPersonalInfo } from '@hcm-mfe/self-service/data-access/models';

@Injectable({
    providedIn: 'root'
})
export class SelfPersonalInfoService extends BaseService {

  public getPersonalInfo(employeeId: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.EMPLOYEES.PREFIX + '/' + employeeId + UrlConstant.EMPLOYEES.PERSONAL_INFO;
    return this.get(url);
  }

  public savePersonalInfo(employeeId: number, personalInfo: SelfPersonalInfo) {
    const url = UrlConstant.API_VERSION + UrlConstant.EMPLOYEES.PREFIX + '/' + employeeId + UrlConstant.EMPLOYEES.PERSONAL_INFO;
    return this.post(url, personalInfo);
  }

  public getAvatar(employeeId: number) {
    const url = UrlConstant.API_VERSION + '/employees/' + employeeId + UrlConstant.EMPLOYEES.AVATAR;
    return this.get(url);
  }

  public uploadAvatar(employeeId: number, formData: FormData) {
    const url = UrlConstant.API_VERSION + UrlConstant.EMPLOYEES.PREFIX + '/' + employeeId + UrlConstant.EMPLOYEES.AVATAR;
    return this.post(url, formData);
  }

  public deleteAvatar(employeeId: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.EMPLOYEES.PREFIX + '/' + employeeId + UrlConstant.EMPLOYEES.AVATAR;
    return this.delete(url);
  }
}
