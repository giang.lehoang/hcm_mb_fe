import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from '@hcm-mfe/self-service/data-access/common';
import { SelfRelativeInfo } from '@hcm-mfe/self-service/data-access/models';
import {BaseResponse, CurrentPage} from "@hcm-mfe/shared/data-access/models";

@Injectable({
    providedIn: 'root'
})
export class SelfRelativesInfoService extends BaseService{
  readonly baseUrl = UrlConstant.API_VERSION + UrlConstant.RELATIVES_INFO.PREFIX;

 // Lấy danh sách thân nhân
 public getList(employeeId: number, param: CurrentPage): Observable<BaseResponse> {
  const url = this.baseUrl + UrlConstant.RELATIVES_INFO.LIST.replace('{employeeId}', employeeId.toString())
  return this.get(url, {params: param});
}

// Lưu bản ghi
  public saveRecord(request: SelfRelativeInfo) {
    const url = this.baseUrl + UrlConstant.RELATIVES_INFO.SAVE;
    return this.post(url, request);
  }


// Xóa bản ghi
  public deleteRecord(familyRelationshipId: number) {
    const url = this.baseUrl + UrlConstant.RELATIVES_INFO.DELETE.replace('{familyRelationshipId}', familyRelationshipId.toString())
    return this.delete(url);
  }

  // Lấy bản ghi
  public getRecord(familyRelationshipId: number) {
    const url = this.baseUrl + UrlConstant.RELATIVES_INFO.DETAIL.replace('{familyRelationshipId}', familyRelationshipId.toString())
    return this.get(url);
  }


}
