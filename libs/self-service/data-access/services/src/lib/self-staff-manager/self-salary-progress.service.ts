import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from '@hcm-mfe/self-service/data-access/common';
import { SelfSalary } from '@hcm-mfe/self-service/data-access/models';
import {BaseResponse, CurrentPage} from "@hcm-mfe/shared/data-access/models";

@Injectable({
    providedIn: 'root'
})
export class SelfSalaryProgressService extends BaseService {
  readonly baseUrl = UrlConstant.API_VERSION + UrlConstant.SALARY_PROGRESS.PREFIX;

  // Lấy danh sách
  public getList(employeeId: number, param: CurrentPage): Observable<BaseResponse> {
    const url = this.baseUrl + UrlConstant.SALARY_PROGRESS.LIST.replace('{employeeId}', employeeId.toString())
    return this.get(url, {params: param});
  }

// Lưu bản ghi
  public saveRecord(request: SelfSalary) {
    const url = this.baseUrl + UrlConstant.SALARY_PROGRESS.SAVE;
    return this.post(url, request);
  }


// Xóa bản ghi
  public deleteRecord(salaryProcessId: number) {
    const url = this.baseUrl + UrlConstant.SALARY_PROGRESS.DELETE.replace('{salaryProcessId}', salaryProcessId.toString())
    return this.delete(url);
  }

  // Lấy danh sách dải lương
  public getSalaryGradeList(): Observable<BaseResponse> {
    const url = UrlConstant.API_VERSION + UrlConstant.CATALOGS.SALARY_GRADES;
    return this.get(url)
  }

  // Lấy danh sách bậc lương
  public getSalaryRankList(): Observable<BaseResponse> {
    const url = UrlConstant.API_VERSION + UrlConstant.CATALOGS.SALARY_RANKS;
    return this.get(url)
  }
}
