import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from '@hcm-mfe/self-service/data-access/common';
import {BaseResponse, CurrentPage} from "@hcm-mfe/shared/data-access/models";

@Injectable({
    providedIn: 'root'
})
export class SelfContractHisInfoService extends BaseService {
  readonly baseUrl = UrlConstant.API_VERSION + UrlConstant.CONTRACT_HISTORY_INFO.PREFIX;

  // Lấy danh sách
    public getList(employeeId: number, param: CurrentPage): Observable<BaseResponse> {
        const url = this.baseUrl + UrlConstant.CONTRACT_HISTORY_INFO.LIST.replace('{employeeId}', employeeId.toString());
    return this.get(url, {params: param});
  }

    // Xóa bản ghi
    public deleteRecord(contractProcessId: number) {
        const url = this.baseUrl + UrlConstant.CONTRACT_HISTORY_INFO.DELETE.replace('{contractProcessId}', contractProcessId.toString())
        return this.delete(url);
    }

}
