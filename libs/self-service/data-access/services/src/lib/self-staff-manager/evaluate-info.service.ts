import { Injectable } from '@angular/core';
import {BaseService} from "@hcm-mfe/shared/common/base-service";
import {UrlConstant} from "@hcm-mfe/self-service/data-access/common";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {MICRO_SERVICE} from "@hcm-mfe/shared/common/constants";

@Injectable({
  providedIn: 'root'
})
export class EvaluateInfoService extends BaseService{
  readonly baseUrl = 'goa' + UrlConstant.API_VERSION;

  getEvaluateInfo(param: NzSafeAny) {
    const url = this.baseUrl + UrlConstant.EVALUATE_INFO;
    return this.get(url, {params: param}, MICRO_SERVICE.TARGET_ENDPOIN_ASSESSMENT);
  }
}
