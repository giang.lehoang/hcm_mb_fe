import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {BaseService} from '@hcm-mfe/shared/common/base-service';
import {UrlConstant} from '@hcm-mfe/self-service/data-access/common';
import {BaseResponse, CurrentPage} from "@hcm-mfe/shared/data-access/models";

@Injectable({
    providedIn: 'root'
})
export class SelfWorkInfoService extends BaseService {
    readonly baseUrl = UrlConstant.API_VERSION + UrlConstant.WORK_HISTORY.PREFIX;

    // Lấy danh sách
    public getList(employeeId: number, param: CurrentPage): Observable<BaseResponse> {
        const url = this.baseUrl + UrlConstant.WORK_HISTORY.LIST.replace('{employeeId}', employeeId.toString());
        return this.get(url, {params: param});
    }

    // Xóa bản ghi
    public deleteRecord(workProcessId: number) {
        const url = this.baseUrl + UrlConstant.WORK_HISTORY.DELETE.replace('{workProcessId}', workProcessId.toString())
        return this.delete(url);
    }

    // Lấy thông tin bản ghi
    public getRecord(workProcessId: number) {
        const url = this.baseUrl + UrlConstant.WORK_HISTORY.DETAIL.replace('{workProcessId}', workProcessId.toString())
        return this.get(url);
    }

}
