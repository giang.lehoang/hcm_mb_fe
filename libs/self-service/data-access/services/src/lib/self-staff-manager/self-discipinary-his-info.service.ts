import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from '@hcm-mfe/self-service/data-access/common';
import { SelfDisciplinaryHistory } from '@hcm-mfe/self-service/data-access/models';
import {BaseResponse, CurrentPage} from "@hcm-mfe/shared/data-access/models";

@Injectable({
    providedIn: 'root'
})
export class SelfDiscipinaryHisInfoService extends BaseService{

  readonly baseUrl = UrlConstant.API_VERSION + UrlConstant.DISCIPINARY_HISTORY_INFO.PREFIX;

  // Lấy danh sách
  public getList(employeeId: number, param: CurrentPage): Observable<BaseResponse> {
    const url = this.baseUrl + UrlConstant.DISCIPINARY_HISTORY_INFO.LIST.replace('{employeeId}', employeeId.toString())
    return this.get(url, {params: param});
  }

// Lưu bản ghi
  public saveRecord(request: SelfDisciplinaryHistory) {
    const url = this.baseUrl + UrlConstant.DISCIPINARY_HISTORY_INFO.SAVE;
    return this.post(url, request);
  }


// Xóa bản ghi
  public deleteRecord(decplineRecordId: number) {
    const url = this.baseUrl + UrlConstant.DISCIPINARY_HISTORY_INFO.DELETE.replace('{decplineRecordId}', decplineRecordId.toString())
    return this.delete(url);
  }
}
