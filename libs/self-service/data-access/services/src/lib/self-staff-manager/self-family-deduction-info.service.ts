import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from '@hcm-mfe/self-service/data-access/common';
import { SelfFamilyDeductionInfo } from '@hcm-mfe/self-service/data-access/models';
import {BaseResponse, CurrentPage} from "@hcm-mfe/shared/data-access/models";

@Injectable({
    providedIn: 'root'
})
export class SelfFamilyDeductionInfoService extends BaseService {
  readonly baseUrl = UrlConstant.API_VERSION + UrlConstant.FAMILY_DEDUCTION.PREFIX;

  // Lấy danh sách giảm trừ
  public getList(employeeId: number, param: CurrentPage): Observable<BaseResponse> {
    const url = this.baseUrl + UrlConstant.FAMILY_DEDUCTION.LIST.replace('{employeeId}', employeeId.toString())
    return this.get(url, {params: param});
  }

    // Lưu bản ghi
    public saveRecord(request: SelfFamilyDeductionInfo) {
        const url = this.baseUrl + UrlConstant.FAMILY_DEDUCTION.SAVE;
        return this.post(url, request);
    }

// Xóa bản ghi
  public deleteRecord(dependentPersonId: number) {
    const url = this.baseUrl + UrlConstant.FAMILY_DEDUCTION.DELETE.replace('{dependentPersonId}', dependentPersonId.toString())
    return this.delete(url);
  }

  // Xóa bản ghi
  public getRecord(dependentPersonId: number) {
    const url = this.baseUrl + UrlConstant.FAMILY_DEDUCTION.DETAIL.replace('{dependentPersonId}', dependentPersonId.toString())
    return this.get(url);
  }
}
