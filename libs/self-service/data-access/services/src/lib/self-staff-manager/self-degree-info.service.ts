import { Injectable } from '@angular/core';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from '@hcm-mfe/self-service/data-access/common';
import { CurrentPage } from '@hcm-mfe/shared/data-access/models';

@Injectable({
  providedIn: 'root'
})
export class SelfDegreeInfoService extends BaseService {

  public getDegreeInfosV2(employeeId: number, param: CurrentPage) {
    const url = UrlConstant.API_VERSION + UrlConstant.DEGREE_INFOS.DRAFT_ACTION + UrlConstant.DEGREE_INFOS.LIST + '/' + employeeId;
    return this.get(url, { params: param });
  }

  public getDegreeInfoDetail(educationDegreeId: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.DEGREE_INFOS.PREFIX + '/' + educationDegreeId;
    return this.get(url);
  }

  public saveDegreeInfoDraft(formData: FormData) {
    const url = UrlConstant.API_VERSION + UrlConstant.DEGREE_INFOS.DRAFT_ACTION;
    return this.post(url, formData);
  }

  public deleteDegreeDraft(educationDegreeId: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.DEGREE_INFOS.DRAFT_DELETE + '/' + educationDegreeId;
    return this.delete(url);
  }

  public rollBack(draftId: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.DEGREE_INFOS.DRAFT_ACTION + '/rollback' + '/' + draftId;
    return this.get(url);
  }
}
