import {Injectable} from "@angular/core";
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from '@hcm-mfe/self-service/data-access/common';
import {HrEmployeeInfo} from "@hcm-mfe/staff-manager/data-access/models/info";

@Injectable({
    providedIn: 'root'
})
export class SelfContactInfoService extends BaseService {

  public getContactInfo(employeeId: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.EMPLOYEES.PREFIX + '/' + employeeId + UrlConstant.EMPLOYEES.CONTACT;
    return this.get(url);
  }

    public saveContactInfo(employeeId: number, contactInfo: HrEmployeeInfo) {
    const url = UrlConstant.API_VERSION + UrlConstant.EMPLOYEES.PREFIX + '/' + employeeId + UrlConstant.EMPLOYEES.CONTACT;
    return this.post(url, contactInfo);
  }
}
