import {Injectable} from "@angular/core";
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from '@hcm-mfe/self-service/data-access/common';
import {CurrentPage} from "@hcm-mfe/shared/data-access/models";

@Injectable({
    providedIn: 'root'
})
export class SelfEduHisInfoService extends BaseService {

  public getEduHisInfo(employeeId: number, param: CurrentPage) {
    const url = UrlConstant.API_VERSION + UrlConstant.EDU_HIS_INFO.PREFIX + UrlConstant.EDU_HIS_INFO.LIST + '/' + employeeId;
    return this.get(url, {params: param});
  }

}
