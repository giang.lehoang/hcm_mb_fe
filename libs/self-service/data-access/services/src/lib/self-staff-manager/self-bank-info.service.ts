import {Injectable} from "@angular/core";
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from '@hcm-mfe/self-service/data-access/common';

@Injectable({
    providedIn: 'root'
})
export class SelfBankInfoService extends BaseService {

  public getBankInfo(employeeId: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.EMPLOYEES.PREFIX + '/' + employeeId + UrlConstant.EMPLOYEES.BANK;
    return this.get(url);
  }

}
