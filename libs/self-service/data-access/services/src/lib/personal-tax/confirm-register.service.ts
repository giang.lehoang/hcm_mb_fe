import { Injectable } from '@angular/core';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { ConfirmRegister } from '@hcm-mfe/self-service/data-access/models';
import { UrlConstant } from '@hcm-mfe/self-service/data-access/common';
import { MICRO_SERVICE } from '@hcm-mfe/shared/common/constants';


@Injectable({
    providedIn: 'root'
})
export class ConfirmRegisterService extends BaseService{

    saveData(data: ConfirmRegister) {
        const url = UrlConstant.API_VERSION + UrlConstant.CONFIRM_REGISTERS.SAVE;
        return this.post(url, data, {}, MICRO_SERVICE.TAX_SERVICE);
    }

}
