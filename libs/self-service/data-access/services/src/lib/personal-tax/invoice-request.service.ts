import {Injectable} from '@angular/core';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { DeclarationRegister } from '@hcm-mfe/self-service/data-access/models';
import { UrlConstant } from '@hcm-mfe/self-service/data-access/common';
import { MICRO_SERVICE } from '@hcm-mfe/shared/common/constants';

@Injectable({
    providedIn: 'root'
})
export class InvoiceRequestService extends BaseService {

    saveData(data: DeclarationRegister) {
        const url = UrlConstant.API_VERSION + UrlConstant.INVOICE_REQUEST.SAVE;
        return this.post(url, data, {}, MICRO_SERVICE.TAX_SERVICE);
    }

    deleteData(id: number) {
        const url = UrlConstant.API_VERSION + UrlConstant.INVOICE_REQUEST.GET_BY_ID.replace('{id}', id.toString());
        return this.delete(url, {}, MICRO_SERVICE.TAX_SERVICE);
    }

}
