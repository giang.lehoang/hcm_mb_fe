import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpParams} from "@angular/common/http";
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from '@hcm-mfe/self-service/data-access/common';
import { MICRO_SERVICE } from '@hcm-mfe/shared/common/constants';
import {BaseResponse} from "@hcm-mfe/shared/data-access/models";

@Injectable({
  providedIn: 'root'
})
export class DependentService extends BaseService {

  public getListRelatives(employeeId: number, param: HttpParams): Observable<BaseResponse> {
    const url = UrlConstant.API_VERSION + UrlConstant.RELATIVES_INFO.LIST_BY_EMP_ID.replace('{employeeId}', employeeId.toString())
    return this.get(url, {params: param});
  }

  public getCatalog(typeCode: string, parentCode?: string){
    const url = UrlConstant.API_VERSION + UrlConstant.CATALOGS.PREFIX;
    return this.get(url, {params: parentCode ? {typeCode, parentCode} : {typeCode}});
  }

  public saveDependentRegister(data: FormData) {
    const url = UrlConstant.API_VERSION + UrlConstant.DEPENDENT_REGISTERS.SAVE_DEPENDENT;
    return this.post(url, data,{}, MICRO_SERVICE.TAX_SERVICE);
  }

  public getListDependents(employeeId: number, param: HttpParams): Observable<BaseResponse> {
    const url = UrlConstant.API_VERSION + UrlConstant.DEPENDENT_INFO.LIST.replace('{employeeId}', employeeId.toString())
    return this.get(url, {params: param});
  }

}
