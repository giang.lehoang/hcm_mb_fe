import { Injectable } from '@angular/core';
import {HttpParams} from "@angular/common/http";
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { DeclarationRegister } from '@hcm-mfe/self-service/data-access/models';
import { UrlConstant } from '@hcm-mfe/self-service/data-access/common';
import { MICRO_SERVICE } from '@hcm-mfe/shared/common/constants';

@Injectable({
  providedIn: 'root'
})
export class DeclarationService extends BaseService {
  saveData(data: DeclarationRegister) {
    const url = UrlConstant.API_VERSION + UrlConstant.DECLARATION_REGISTERS.SAVE;
    return this.post(url, data, {}, MICRO_SERVICE.TAX_SERVICE);
  }

  deleteData(id: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.DECLARATION_REGISTERS.GET_BY_ID.replace('{id}', id.toString());
    return this.delete(url, {}, MICRO_SERVICE.TAX_SERVICE);
  }
  public getListYear(registrationType: string, year?: number) {
    let params = new HttpParams().set('registrationType', registrationType);
    if (year) {
      params = params.set('year', year);
    }
    const url = UrlConstant.API_VERSION + UrlConstant.DECLARATION_REGISTERS.GET_LIST_YEAR;
    return this.get(url, {params: params}, MICRO_SERVICE.TAX_SERVICE);
  }

  public getDataByProperties(params: HttpParams) {
    const url = UrlConstant.API_VERSION + UrlConstant.DECLARATION_REGISTERS.CHECK;
    return this.get(url, {params: params}, MICRO_SERVICE.TAX_SERVICE);
  }

}
