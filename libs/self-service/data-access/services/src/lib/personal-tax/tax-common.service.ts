import {Injectable} from '@angular/core';
import {BaseService} from '@hcm-mfe/shared/common/base-service';
import {Constant, UrlConstant} from '@hcm-mfe/self-service/data-access/common';
import {MICRO_SERVICE} from '@hcm-mfe/shared/common/constants';

@Injectable({
    providedIn: 'root'
})
export class TaxCommonService extends BaseService {

    getTaxRegisterById(id: number, regType: string) {
        let url = '';
        switch (regType) {
            case  Constant.REG_TYPE.ALTER_TAX_INFO:
            case  Constant.REG_TYPE.REGISTER_NEW:
                url = UrlConstant.API_VERSION + UrlConstant.TAX_REGISTERS.GET_BY_ID.replace('{id}', id.toString());
                break;
            case  Constant.REG_TYPE.REGISTER_NEW_DP:
            case  Constant.REG_TYPE.REGISTER_LOW_DP:
                url = UrlConstant.API_VERSION + UrlConstant.DEPENDENT_REGISTERS.GET_BY_ID.replace('{id}', id.toString());
                break;
            case Constant.REG_TYPE.REGISTER_TAX_SETTLEMENT:
                url = UrlConstant.API_VERSION + UrlConstant.DECLARATION_REGISTERS.GET_BY_ID.replace('{id}', id.toString());
                break;
            case Constant.REG_TYPE.REGISTER_RECEIVE_RECEIPT:
                url = UrlConstant.API_VERSION + UrlConstant.INVOICE_REQUEST.GET_BY_ID.replace('{id}', id.toString());
                break;
            case Constant.REG_TYPE.CONFIRM_AUTHORIZED:
                url = UrlConstant.API_VERSION + UrlConstant.CONFIRM_REGISTERS.GET_BY_ID.replace('{id}', id.toString());
                break;
        }
        return this.get(url, {}, MICRO_SERVICE.TAX_SERVICE);
    }
}
