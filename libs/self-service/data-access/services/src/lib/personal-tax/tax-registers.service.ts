import {Injectable} from '@angular/core';
import {HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import { BaseResponse } from '@hcm-mfe/shared/data-access/models';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { Constant, UrlConstant } from '@hcm-mfe/self-service/data-access/common';
import { MICRO_SERVICE } from '@hcm-mfe/shared/common/constants';

@Injectable({
    providedIn: 'root'
})
export class TaxRegistersService extends BaseService {

    public searchData(urlEndpoint: string, searchParam: HttpParams, pagination: { startRecord: number, pageSize: number }): Observable<BaseResponse> {
        if (pagination) searchParam = searchParam.appendAll(pagination);
        const url = UrlConstant.API_VERSION + urlEndpoint;
        return this.get(url, {params: searchParam}, MICRO_SERVICE.TAX_SERVICE);
    }

    public updateWorkFlow(id: number, regType: string, status: number) {
      let url = '';
      switch (regType) {
        case Constant.REG_TYPE.REGISTER_NEW:
        case Constant.REG_TYPE.ALTER_TAX_INFO:
          url = UrlConstant.API_VERSION + UrlConstant.TAX_REGISTERS.UPDATE_WORK_FLOW;
          break;
        case Constant.REG_TYPE.REGISTER_NEW_DP:
        case Constant.REG_TYPE.REGISTER_LOW_DP:
          url = UrlConstant.API_VERSION + UrlConstant.DEPENDENT_REGISTERS.UPDATE_WORK_FLOW;
          break;
      }
      url = url.replace('{id}', id.toString());
      url = url.replace('{status}', status.toString());
      return this.post(url, {}, {}, MICRO_SERVICE.TAX_SERVICE);
    }

    public deleteData(id: number, regType: string): Observable<BaseResponse> {
      let url = '';
      switch (regType) {
        case Constant.REG_TYPE.REGISTER_NEW:
        case Constant.REG_TYPE.ALTER_TAX_INFO:
          url = UrlConstant.API_VERSION + UrlConstant.TAX_REGISTERS.DELETE_DATA;
          break;
        case Constant.REG_TYPE.REGISTER_NEW_DP:
        case Constant.REG_TYPE.REGISTER_LOW_DP:
          url = UrlConstant.API_VERSION + UrlConstant.DEPENDENT_REGISTERS.DELETE_DATA;
          break;
      }
      url = url.replace('{id}', id.toString());
      return this.delete(url, {}, MICRO_SERVICE.TAX_SERVICE);
    }

    public saveRecord(request: FormData) {
        const url = UrlConstant.API_VERSION + UrlConstant.TAX_REGISTERS.SAVE;
        return this.post(url, request, {}, MICRO_SERVICE.TAX_SERVICE);
    }

    public getRecentRegister() {
        const url = UrlConstant.API_VERSION + UrlConstant.TAX_REGISTERS.GET_RECENT_REGISTER;
        return this.get(url, {}, MICRO_SERVICE.TAX_SERVICE);
    }

}
