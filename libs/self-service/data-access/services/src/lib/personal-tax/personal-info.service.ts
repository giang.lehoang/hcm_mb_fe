import { Injectable } from '@angular/core';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from '@hcm-mfe/self-service/data-access/common';

@Injectable({
  providedIn: 'root'
})
export class PersonalInfoService extends BaseService{

  public getPersonalInfo(employeeId: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.EMPLOYEES.PREFIX + '/' + employeeId + UrlConstant.EMPLOYEES.PERSONAL_INFO;
    return this.get(url);
  }

  public getIdentities(employeeId: number) {
      const url = UrlConstant.API_VERSION + UrlConstant.EMPLOYEES.PREFIX + '/' + employeeId + UrlConstant.EMPLOYEES.IDENTITIES;
      return this.get(url);
  }
}
