import { HttpClient, HttpParams } from '@angular/common/http';
import {Injectable} from '@angular/core';
import { Observable } from 'rxjs';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from '@hcm-mfe/self-service/data-access/common';
import { MICRO_SERVICE } from '@hcm-mfe/shared/common/constants';
import { RequestApproversSearchForm } from '@hcm-mfe/self-service/data-access/models';
import { BaseResponse } from '@hcm-mfe/shared/data-access/models';


@Injectable({
  providedIn: 'root'
})
export class RequestsApproversService extends BaseService {

  constructor(private _httpClient: HttpClient) {
    super(_httpClient);
  }

  getRequestsApprovers(params: RequestApproversSearchForm): Observable<BaseResponse> {
    const url = UrlConstant.API_VERSION + UrlConstant.REQUESTS.APPROVERS;
    return this.get(url, { params: this._parseOptions(params) }, MICRO_SERVICE.EMPLOYEE_MANAGEMENT);
  }
    _parseOptions(formData: RequestApproversSearchForm) {
    let params = new HttpParams();
    if (!formData) {
        return params;
    }
    for (const [key, value] of Object.entries(formData)) {
        if(value) {
            params = params.set(key, value + '');
        }
    }
    return params;
  }
}
