import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { LEAVE_TYPE, UrlConstant } from '@hcm-mfe/self-service/data-access/common';
import { MICRO_SERVICE } from '@hcm-mfe/shared/common/constants';
import { BaseResponse } from '@hcm-mfe/shared/data-access/models';
import {AbsRequestDTO, AbsRequests} from '@hcm-mfe/self-service/data-access/models';
import {NzSafeAny} from "ng-zorro-antd/core/types";

@Injectable({
  providedIn: 'root'
})
export class RequestsService extends BaseService {
    saveOrUpdate(form: AbsRequestDTO): Observable<BaseResponse> {
    const fileList = form?.fileList || [];
    delete form?.fileList;
        const formData = new FormData();
    formData.append("data", new Blob([JSON.stringify(form)], {
      type: 'application/json',
    }));
        fileList.forEach((nzFile: NzSafeAny) => {
      formData.append('files', nzFile)
    });
    const url = UrlConstant.API_VERSION + UrlConstant.REQUESTS.SAVE;
    return this.post(url, formData, {}, MICRO_SERVICE.ABS_MANAGEMENT);
  }

  findOne(requestId: number): Observable<BaseResponse> {
    const url = UrlConstant.API_VERSION + UrlConstant.REQUESTS.PREFIX;
    return this.get(`${url}/${requestId}`, {}, MICRO_SERVICE.ABS_MANAGEMENT);
  }

  getUserLogin(): Observable<BaseResponse> {
    const url = UrlConstant.API_VERSION + UrlConstant.REQUESTS.USER_LOGIN;
    return this.get(`${url}`, {}, MICRO_SERVICE.ABS_MANAGEMENT);
  }

    search(params: AbsRequestDTO): Observable<BaseResponse> {
    const url = UrlConstant.API_VERSION + UrlConstant.REQUESTS.SEARCH;
    return this.get(`${url}/${params.leaveType || LEAVE_TYPE.LEAVE}`, { params: this._parseOptions(params) }, MICRO_SERVICE.ABS_MANAGEMENT);
  }

  deleteRecord(requestId: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.REQUESTS.DELETE;
    return this.delete(url.replace('{requestId}', requestId + ''), {}, MICRO_SERVICE.ABS_MANAGEMENT);
  }

  submitRequest(requestId: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.REQUESTS.SUBMIT;
    return this.get(url.replace('{requestId}', requestId + ''), {}, MICRO_SERVICE.ABS_MANAGEMENT);
  }

  cancel(requestId: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.REQUESTS.CANCEL;
    return this.post(url, { requestId: requestId }, {}, MICRO_SERVICE.ABS_MANAGEMENT);
  }

  approve(form: AbsRequests): Observable<BaseResponse> {
    const url = UrlConstant.API_VERSION + UrlConstant.REQUESTS.APPROVE;
    return this.post(url, form, {}, MICRO_SERVICE.ABS_MANAGEMENT);
  }

  reject(form: AbsRequests): Observable<BaseResponse> {
    const url = UrlConstant.API_VERSION + UrlConstant.REQUESTS.REJECT;
    return this.post(url, form, {}, MICRO_SERVICE.ABS_MANAGEMENT);
  }

    _parseOptions(formData: AbsRequestDTO) {
    let params = new HttpParams();
    if (!formData) {
        return params;
    }
    for (const [key, value] of Object.entries(formData)) {
        if(value) {
            params = params.set(key, value + '');
        }
    }
    return params;
  }

  rejectBackdate(form: AbsRequests): Observable<BaseResponse> {
    const url = UrlConstant.API_VERSION + UrlConstant.REQUESTS.REJECT_BACKDATE;
    return this.post(url, form, {}, MICRO_SERVICE.ABS_MANAGEMENT);
  }
}
