import { Injectable } from '@angular/core';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from '@hcm-mfe/self-service/data-access/common';
import { MICRO_SERVICE } from '@hcm-mfe/shared/common/constants';
import {AbsRequestLeavesDTO} from "@hcm-mfe/self-service/data-access/models";

@Injectable({
  providedIn: 'root'
})
export class RequestLeavesService extends BaseService {

    public calculateLeaves(params: AbsRequestLeavesDTO) {
    const url = UrlConstant.API_VERSION + UrlConstant.REQUEST_LEAVES.CACULATE_LEAVES;
    return this.get(url, { params: params }, MICRO_SERVICE.ABS_MANAGEMENT);
  }
}
