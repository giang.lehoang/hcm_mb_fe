import { Injectable } from '@angular/core';
import { BaseService } from '@hcm-mfe/shared/common/base-service';
import { UrlConstant } from '@hcm-mfe/self-service/data-access/common';
import { MICRO_SERVICE } from '@hcm-mfe/shared/common/constants';

@Injectable({
    providedIn: 'root'
})
export class ReasonLeavesService extends BaseService {

    public getAllReasonLeaves(groupCode: string,) {
        const url = UrlConstant.API_VERSION + UrlConstant.REASON_LEAVES.PREFIX;
        return this.get(`${url}/${groupCode}/all`, { }, MICRO_SERVICE.ABS_MANAGEMENT);
    }

    public getLeaveInfo(employeeId: number, year: number) {
        const url = UrlConstant.API_VERSION + UrlConstant.REASON_LEAVES.LEAVE_INFO;
        return this.get(`${url}/${employeeId}/${year}`, { }, MICRO_SERVICE.ABS_MANAGEMENT);
    }

}
