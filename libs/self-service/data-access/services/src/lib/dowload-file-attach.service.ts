import {Injectable} from '@angular/core';
import {BaseService} from '@hcm-mfe/shared/common/base-service';
import {UrlConstant} from '@hcm-mfe/self-service/data-access/common';
import {environment} from '@hcm-mfe/shared/environment';

@Injectable({
    providedIn: 'root'
})
export class DownloadFileAttachService extends BaseService {
    readonly baseUrl = UrlConstant.API_VERSION + UrlConstant.DOWNLOAD_FILE_ATTACH;

    doDownloadAttachFile(docId: number) {
        const url = this.baseUrl + `?docId=${docId}`;
        return this.getRequestFile(url);
    }

    doAbsDownloadAttachFile(docId: number, security: string | '') {
        const url = this.baseUrl + `?docId=${docId}&security=${security}`;
        return this.getRequestAbsFile(url);
    }

    doTaxDownloadAttachFile(docId: number, security: string | '') {
        const url = this.baseUrl + `?docId=${docId}&security=${security}`;
        return this.getRequestAbsFile(url, {}, environment.backend.taxServiceBackend + url);
    }

}
