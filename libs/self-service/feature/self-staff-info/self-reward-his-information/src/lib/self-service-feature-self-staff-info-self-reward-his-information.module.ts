import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelfRewardHisInformationComponent } from './self-reward-his-information/self-reward-his-information.component';
import { NzTableModule } from 'ng-zorro-antd/table';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';

@NgModule({
  imports: [CommonModule, NzTableModule, TranslateModule, SharedUiMbButtonModule],
  declarations: [SelfRewardHisInformationComponent],
  exports: [SelfRewardHisInformationComponent],
})
export class SelfServiceFeatureSelfStaffInfoSelfRewardHisInformationModule {}
