import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  SelfContractHisInformationComponent
} from './self-contract-his-information/self-contract-his-information.component';
import { NzTableModule } from 'ng-zorro-antd/table';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';

@NgModule({
  imports: [CommonModule, NzTableModule, TranslateModule, SharedUiMbButtonModule],
  declarations: [SelfContractHisInformationComponent],
  exports: [SelfContractHisInformationComponent],
})
export class SelfServiceFeatureSelfStaffInfoSelfContractHisInformationModule {}
