import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelfRelativesInformationComponent } from './self-relatives-information/self-relatives-information.component';
import { NzTableModule } from 'ng-zorro-antd/table';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';

@NgModule({
  imports: [CommonModule, NzTableModule, TranslateModule, SharedUiMbButtonModule, NzPopconfirmModule],
  declarations: [SelfRelativesInformationComponent],
  exports: [SelfRelativesInformationComponent],
})
export class SelfServiceFeatureSelfStaffInfoSelfRelativesInformationModule {}
