import {Component, OnInit, TemplateRef} from '@angular/core';
import {NzModalRef, NzModalService} from 'ng-zorro-antd/modal';
import { TranslateService } from '@ngx-translate/core';
import {ToastrService} from "ngx-toastr";
import { SelfRelativeInfo } from '@hcm-mfe/self-service/data-access/models';
import { BaseResponse, Pagination } from '@hcm-mfe/shared/data-access/models';
import { ValidateService } from '@hcm-mfe/shared/core';
import { SelfRelativesInfoService, SelfShareDataService } from '@hcm-mfe/self-service/data-access/services';
import { HTTP_STATUS_CODE, Mode } from '@hcm-mfe/shared/common/constants';
import { SelfEditRelativesInfoComponent } from '@hcm-mfe/self-service/feature/self-staff-info/self-edit-relatives-info';
import {Subscription} from "rxjs";
import {NzSafeAny} from "ng-zorro-antd/core/types";


/**
 * Thông tin thân nhân
 */
@Component({
  selector: 'app-relatives-information',
  templateUrl: './self-relatives-information.component.html',
  styleUrls: ['./self-relatives-information.component.scss'],
})
export class SelfRelativesInformationComponent implements OnInit {
  items: SelfRelativeInfo[] = [];
  loading = false;
  employeeId = 1;
  modal!: NzModalRef;
  count = 0;
  pagination = new Pagination();
  subs: Subscription[] = [];

  constructor(
    public validateService: ValidateService,
    private toastrService: ToastrService,
    private modalService: NzModalService,
    private translate: TranslateService,
    private shareDataService: SelfShareDataService,
    private relativesInfoService: SelfRelativesInfoService,
  ) {}

  ngOnInit(): void {
    this.shareDataService.employee$.subscribe(employee => {
      if (employee.employeeId)
      this.employeeId = employee.employeeId;
      this.pagination.pageNumber = 1;
      this.getInfo()
    })
  }

  getInfo() {
    this.loading = true;
    let param = this.pagination.getCurrentPage();

    this.subs.push(
      this.relativesInfoService.getList(this.employeeId, param).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.items = res.data.listData;
          this.count = res.data.count;
        }
        this.loading = false
      }, error => {
        this.toastrService.error(error.message)
        this.loading = false
        this.items = []
      })
    );
  }

  resetContextMenu() {

  }

  rightClickTable($event: MouseEvent, data: SelfRelativeInfo) {

  }

  showModalUpdate(data: SelfRelativeInfo, footerTmpl: TemplateRef<NzSafeAny>) {
    this.modal = this.modalService.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
      nzTitle: this.translate.instant("selfService.relativesInformation.modal.update"),
      nzContent: SelfEditRelativesInfoComponent,
      nzComponentParams: {
        mode: Mode.EDIT,
        data: data
      },
      nzFooter: footerTmpl
    });
    // this.modal.afterClose.subscribe(result => result?.refresh ? this.getInfo(): '')

  }

  deleteItem(data: SelfRelativeInfo) {
    this.subs.push(
      this.relativesInfoService.deleteRecord(data.familyRelationshipId).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.pagination.pageNumber = 1;
          this.getInfo();
          this.toastrService.success(this.translate.instant('common.notification.deleteSuccess'));
        }
      }, error => {
        this.toastrService.error(this.translate.instant('common.notification.deleteError'));
      })
    );
  }
  refresh() { this.getInfo() }

  ngOnDestroy() {
    this.modal?.destroy();
    this.subs?.forEach(sub => sub.unsubscribe());
  }

}
