import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {BaseResponse, SelfPartyInfo, SelfPartyInfoGroup} from '@hcm-mfe/self-service/data-access/models';
import { SelfPartyInfoService, SelfShareDataService } from '@hcm-mfe/self-service/data-access/services';


@Component({
  selector: 'app-party-information',
  templateUrl: './self-party-information.component.html',
  styleUrls: ['./self-party-information.component.scss']
})
export class SelfPartyInformationComponent implements OnInit, OnDestroy {
  items!: SelfPartyInfoGroup;
  subs: Subscription[] = [];
  response: BaseResponse = new BaseResponse();
  partyInfo!: SelfPartyInfo;
  employeeId = 1;

  constructor(
    private shareDataService: SelfShareDataService,
    private partyInfoService: SelfPartyInfoService
  ) { }

  ngOnInit(): void {
    this.subs.push(
      this.shareDataService.employee$.subscribe(employee => {
        if (employee.employeeId)
        this.employeeId = employee.employeeId;
        this.getPartyInfo(this.employeeId);
      })
    );
  }

  ngOnDestroy() {
    this.subs?.forEach(sub => sub.unsubscribe());
  }

  getPartyInfo(employeeId: number) {
    this.subs.push(
      this.partyInfoService.getPartyInfo(employeeId).subscribe(res => {
        this.response = res;
        this.partyInfo = this.response?.data;
        this.items = {employeeId: employeeId, data: this.response?.data};
      })
    );
  }

  refresh() {
    this.getPartyInfo(this.employeeId);
  }

}
