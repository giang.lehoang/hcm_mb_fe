import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  SelfProjectHisInformationComponent
} from './self-project-his-information/self-project-his-information.component';
import { NzTableModule } from 'ng-zorro-antd/table';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';

@NgModule({
  imports: [CommonModule, NzTableModule, TranslateModule, SharedUiMbButtonModule],
  declarations: [SelfProjectHisInformationComponent],
  exports: [SelfProjectHisInformationComponent],
})
export class SelfServiceFeatureSelfStaffInfoSelfProjectHisInformationModule {}
