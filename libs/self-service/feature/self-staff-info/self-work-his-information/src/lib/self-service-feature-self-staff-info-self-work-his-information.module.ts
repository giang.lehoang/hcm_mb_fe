import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelfWorkHisInformationComponent } from './self-work-his-information/self-work-his-information.component';
import { NzTableModule } from 'ng-zorro-antd/table';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';

@NgModule({
  imports: [CommonModule, NzTableModule, TranslateModule, SharedUiMbButtonModule],
  declarations: [SelfWorkHisInformationComponent],
  exports: [SelfWorkHisInformationComponent],
})
export class SelfServiceFeatureSelfStaffInfoSelfWorkHisInformationModule {}
