import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {NzModalRef, NzModalService} from "ng-zorro-antd/modal";
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";
import {saveAs} from 'file-saver';
import {ListFileAttach, SelfWorkHistory} from '@hcm-mfe/self-service/data-access/models';
import { BaseResponse, Pagination } from '@hcm-mfe/shared/data-access/models';
import { ValidateService } from '@hcm-mfe/shared/core';
import {
  DownloadFileAttachService,
  SelfShareDataService,
  SelfWorkInfoService
} from '@hcm-mfe/self-service/data-access/services';
import { HTTP_STATUS_CODE } from '@hcm-mfe/shared/common/constants';
import { getTypeExport } from '@hcm-mfe/shared/common/utils';
import {Subscription} from "rxjs";

@Component({
  selector: 'app-work-his-information',
  templateUrl: './self-work-his-information.component.html',
  styleUrls: ['./self-work-his-information.component.scss'],
})
export class SelfWorkHisInformationComponent implements OnInit {
  items: SelfWorkHistory[] = [];
  loading = false;
  employeeId = 1;
  modal!: NzModalRef;
  count = 0;
  pagination = new Pagination();
  subs: Subscription[] = [];

  constructor(
    public validateService: ValidateService,
    private http: HttpClient,
    private toastrService: ToastrService,
    private modalService: NzModalService,
    private translate: TranslateService,
    private shareDataService: SelfShareDataService,
    private workInfoService: SelfWorkInfoService,
    private downloadFileAttachService: DownloadFileAttachService
  ) { }

  ngOnInit(): void {
    this.subs.push(
      this.shareDataService.employee$.subscribe(employee => {
        if (employee.employeeId)
        this.employeeId = employee.employeeId;
        this.pagination.pageNumber = 1;
        this.getInfo()
      })
    );
  }
  getInfo() {
    this.loading = true;
    const param = this.pagination.getCurrentPage();
    this.subs.push(
      this.workInfoService.getList(this.employeeId, param).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.items = res.data.listData;
          this.count = res.data.count;
        }
        this.loading = false
      }, error => {
        this.toastrService.error(error.message)
        this.loading = false
        this.items = []
      })
    );
  }

  resetContextMenu() {

  }

  rightClickTable($event: MouseEvent, data: SelfWorkHistory) {

  }

  refresh() {this.getInfo();}

  doDownloadAttach(file: ListFileAttach) {
    this.subs.push(
      this.downloadFileAttachService.doDownloadAttachFile(~~file.docId).subscribe(res => {
        const reportFile = new Blob([res.body], {type: getTypeExport(file.fileName.split(".").pop())});
        saveAs(reportFile, file.fileName);
      }, error => this.toastrService.error(error.message))
    );
  }

  ngOnDestroy(): void {
    this.modal?.destroy();
    this.subs?.forEach(sub => sub.unsubscribe());
  }
}
