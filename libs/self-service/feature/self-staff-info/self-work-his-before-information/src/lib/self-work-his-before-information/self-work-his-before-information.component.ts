import {Component, OnInit, TemplateRef} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ToastrService} from "ngx-toastr";
import {NzModalRef, NzModalService} from "ng-zorro-antd/modal";
import {TranslateService} from "@ngx-translate/core";
import { SelfWorkBeforeInfo } from '@hcm-mfe/self-service/data-access/models';
import { BaseResponse, Pagination } from '@hcm-mfe/shared/data-access/models';
import { ValidateService } from '@hcm-mfe/shared/core';
import { SelfShareDataService, SelfWorkBeforeHisService } from '@hcm-mfe/self-service/data-access/services';
import { HTTP_STATUS_CODE, Mode } from '@hcm-mfe/shared/common/constants';
import { SelfAddWorkHisBeforeComponent } from '@hcm-mfe/self-service/feature/self-staff-info/self-add-work-his-before';
import {Subscription} from "rxjs";

@Component({
  selector: 'app-work-his-before-information',
  templateUrl: './self-work-his-before-information.component.html',
  styleUrls: ['./self-work-his-before-information.component.scss'],
})
export class SelfWorkHisBeforeInformationComponent implements OnInit {
  items: SelfWorkBeforeInfo[] = [];
  loading = false;
  employeeId = 1;
  modal!: NzModalRef;
  count = 0;
  pagination = new Pagination();
  subs: Subscription[] = [];

  constructor(
    public validateService: ValidateService,
    private http: HttpClient,
    private toastrService: ToastrService,
    private modalService: NzModalService,
    private translate: TranslateService,
    private shareDataService: SelfShareDataService,
    private workBeforeHisService: SelfWorkBeforeHisService
  ) { }

  ngOnInit(): void {
    this.subs.push(
      this.shareDataService.employee$.subscribe(employee => {
        if (employee.employeeId)
        this.employeeId = employee.employeeId;
        this.pagination.pageNumber = 1;
        this.getInfo()
      })
    );
  }

  getInfo() {
    this.loading = true;
    let param = this.pagination.getCurrentPage();
    this.subs.push(
      this.workBeforeHisService.getList(this.employeeId, param).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.items = res.data.listData;
          this.count = res.data.count;
        }
        this.loading = false
      }, error => {
        this.toastrService.error(error.message)
        this.loading = false
        this.items = []
      })
    );
  }

  resetContextMenu() {

  }

  rightClickTable($event: MouseEvent, data: SelfWorkBeforeInfo) {

  }

  showModalUpdate(data: SelfWorkBeforeInfo, footerTmpl: TemplateRef<{}>) {
    this.modal = this.modalService.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
      nzTitle: this.translate.instant('selfService.workHisBeforeInformation.modal.update'),
      nzContent: SelfAddWorkHisBeforeComponent,
      nzComponentParams: {
        mode: Mode.EDIT,
        data: data
      },
      nzFooter: footerTmpl
    });
    this.modal.afterClose.subscribe(result => result?.refresh ? this.getInfo(): '')
  }

  deleteItem(data: SelfWorkBeforeInfo) {
    this.subs.push(
      this.workBeforeHisService.deleteRecord(data.workedOutsideId).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.pagination.pageNumber = 1;
          this.getInfo();
          this.toastrService.success(this.translate.instant('common.notification.deleteSuccess'));
        }
      }, error => {
        this.toastrService.error(this.translate.instant('common.notification.deleteError'));
      })
    );
  }

  refresh() {this.getInfo();}

  ngOnDestroy(): void {
    this.modal?.destroy();
    this.subs?.forEach(sub => sub.unsubscribe());
  }

}
