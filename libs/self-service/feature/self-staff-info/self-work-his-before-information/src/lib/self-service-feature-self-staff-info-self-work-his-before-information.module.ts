import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  SelfWorkHisBeforeInformationComponent
} from './self-work-his-before-information/self-work-his-before-information.component';
import { TranslateModule } from '@ngx-translate/core';
import { NzTableModule } from 'ng-zorro-antd/table';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';

@NgModule({
  imports: [CommonModule, TranslateModule, NzTableModule, SharedUiMbButtonModule, NzPopconfirmModule],
  declarations: [SelfWorkHisBeforeInformationComponent],
  exports: [SelfWorkHisBeforeInformationComponent],
})
export class SelfServiceFeatureSelfStaffInfoSelfWorkHisBeforeInformationModule {}
