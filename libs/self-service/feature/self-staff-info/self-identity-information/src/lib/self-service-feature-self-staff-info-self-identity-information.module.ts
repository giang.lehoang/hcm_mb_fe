import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelfIdentityInformationComponent } from './self-identity-information/self-identity-information.component';
import { NzFormModule } from 'ng-zorro-antd/form';
import { SharedUiMbTextLabelModule } from '@hcm-mfe/shared/ui/mb-text-label';
import { TranslateModule } from '@ngx-translate/core';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { SharedPipesStatusModule } from '@hcm-mfe/shared/pipes/status';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';

@NgModule({
  imports: [CommonModule, NzFormModule, SharedUiMbTextLabelModule, TranslateModule, NzTagModule, SharedUiMbButtonModule, SharedPipesStatusModule, NzPopconfirmModule],
  declarations: [SelfIdentityInformationComponent],
  exports: [SelfIdentityInformationComponent],
})
export class SelfServiceFeatureSelfStaffInfoSelfIdentityInformationModule {}
