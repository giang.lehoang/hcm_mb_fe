import { Component, OnDestroy, OnInit, TemplateRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import {BaseResponse, SelfIdentityInfo, SelfIdentityInfoGroup} from '@hcm-mfe/self-service/data-access/models';
import { SelfIdentityInfoService, SelfShareDataService } from '@hcm-mfe/self-service/data-access/services';
import { HTTP_STATUS_CODE } from '@hcm-mfe/shared/common/constants';
import { SelfAddIdentityInfoComponent } from '@hcm-mfe/self-service/feature/self-staff-info/self-add-identity-info';
import {NzSafeAny} from "ng-zorro-antd/core/types";

@Component({
  selector: 'app-identity-information',
  templateUrl: './self-identity-information.component.html',
  styleUrls: ['./self-identity-information.component.scss']
})
export class SelfIdentityInformationComponent implements OnInit, OnDestroy {
  items!: SelfIdentityInfoGroup;
  subs: Subscription[] = [];
  response: BaseResponse = new BaseResponse();
  identities: SelfIdentityInfo[] = [];
  identityMains: SelfIdentityInfo[] = [];
  identityNotMains: SelfIdentityInfo[] = [];
  employeeId = 1;
  textButton = 'Xem thêm';
  isExtend = false;
  modal!: NzModalRef;
  listStatus = [
    {key: "INSERT", value: this.translate.instant('selfService.label.suggestAdd'), color: 'purple'},
    {key: "DELETE", value: this.translate.instant('selfService.label.suggestDelete'), color: 'red'},
    {key: "UPDATE", value: this.translate.instant('selfService.label.suggestEdit'), color: 'blue'},
    {key: "APPROVED", value: this.translate.instant('selfService.label.approved'), color: 'green'},
  ];

  constructor(
    private shareDataService: SelfShareDataService,
    private identityInfoService: SelfIdentityInfoService,
    private modalService: NzModalService,
    private translate: TranslateService,
    private toastrService: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.subs.push(
      this.shareDataService.employee$.subscribe(employee => {
        if (employee.employeeId)
        this.employeeId = employee.employeeId;
        this.getIdentityInfo(this.employeeId);
      })
    );
  }

  ngOnDestroy() {
    this.modal?.destroy();
    this.subs.forEach(sub => sub.unsubscribe());
  }

  getIdentityInfo(employeeId: number) {
    this.subs.push(
      this.identityInfoService.getIdentityInfoV2(employeeId).subscribe(res => {
        this.response = res;
        this.identities = this.response?.data;
        this.identityMains = this.identities?.filter(item => item.isMain === 1);
        this.identityNotMains = this.identities?.filter(item => item.isMain !== 1);
        this.items = { employeeId: employeeId, data: this.response?.data };
        this.shareDataService.changeIdentityInfo(this.response?.data);
      })
    );
  }

  refresh() {
    this.getIdentityInfo(this.employeeId);
  }

  deleteItem(data: SelfIdentityInfo) {
    data.employeeId = this.shareDataService.getEmployee().employeeId;
    this.subs.push(this.identityInfoService.deleteIdentityInfo(data.personalIdentityId).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.refresh();
          this.toastrService.success(this.translate.instant('common.notification.deleteSuccess'));
        } else {
          this.toastrService.error(this.translate.instant('common.notification.deleteError') + ': ' + res.message);
        }
      }, error => {
      this.toastrService.error(this.translate.instant('common.notification.deleteError') + ': ' + error.error);
      })
    );
  }

  extendInfo() {
    this.isExtend = !this.isExtend;
    this.textButton = this.isExtend ? 'Thu gọn' : 'Xem thêm';
  }

  showModalUpdate(data: SelfIdentityInfo, footerTmpl: TemplateRef<NzSafeAny>) {
    const valueData = { employeeId: this.employeeId, data: data };
    this.modal = this.modalService.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
      nzTitle: this.translate.instant('selfService.panel.identityInformation'),
      nzContent: SelfAddIdentityInfoComponent,
      nzComponentParams: {
        data: data
      },
      nzFooter: footerTmpl
    });
    this.modal.afterClose.subscribe(result => result?.refresh ? this.refresh() : '');
  }

  rollBack(draftPersonalIdentityId: number) {
    this.subs.push(
      this.identityInfoService.rollBack(draftPersonalIdentityId).subscribe(res => {
        if (res) {
          this.getIdentityInfo(this.employeeId);
        }
      })
    );
  }
}
