import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelfAddIdentityInfoComponent } from './self-add-identity-info/self-add-identity-info.component';
import { NzFormModule } from 'ng-zorro-antd/form';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { SharedDirectivesNumberInputModule } from '@hcm-mfe/shared/directives/number-input';
import { SharedUiMbDatePickerModule } from '@hcm-mfe/shared/ui/mb-date-picker';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { NzIconModule } from 'ng-zorro-antd/icon';
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
    imports: [CommonModule, NzFormModule, ReactiveFormsModule, SharedUiMbSelectModule, TranslateModule, SharedUiMbInputTextModule, SharedDirectivesNumberInputModule, SharedUiMbDatePickerModule, NzUploadModule, NzIconModule, SharedUiLoadingModule],
  declarations: [SelfAddIdentityInfoComponent],
  exports: [SelfAddIdentityInfoComponent],
})
export class SelfServiceFeatureSelfStaffInfoSelfAddIdentityInfoModule {}
