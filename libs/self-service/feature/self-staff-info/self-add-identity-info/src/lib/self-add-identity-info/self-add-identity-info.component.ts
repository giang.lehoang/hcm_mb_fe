import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import * as moment from 'moment';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {NzModalRef} from 'ng-zorro-antd/modal';
import {NzUploadFile} from 'ng-zorro-antd/upload';
import { saveAs } from 'file-saver';
import {
  DownloadFileAttachService, SelfIdentityInfoService,
  SelfShareDataService,
  SelfStaffInfoService
} from '@hcm-mfe/self-service/data-access/services';
import {Constant} from '@hcm-mfe/self-service/data-access/common';
import {BaseResponse, CatalogModel} from '@hcm-mfe/shared/data-access/models';
import {beforeUploadFile, getTypeExport} from '@hcm-mfe/shared/common/utils';
import {DateValidator} from '@hcm-mfe/shared/common/validators';
import {SelfIdentityInfo, SelfLookupValues} from '@hcm-mfe/self-service/data-access/models';
import {HTTP_STATUS_CODE} from '@hcm-mfe/shared/common/constants';
import {NzSafeAny} from "ng-zorro-antd/core/types";

@Component({
  selector: 'app-add-identity-info',
  templateUrl: './self-add-identity-info.component.html',
  styleUrls: ['./self-add-identity-info.component.scss']
})
export class SelfAddIdentityInfoComponent implements OnInit, OnDestroy {
  @Input() data!: SelfIdentityInfo;
  form: FormGroup;
  isSubmitted = false;
  personalIdentityId = 'personalIdentityId';
  identityType = 'idTypeCode';
  identityNumber = 'idNo';
  dateOfIdentity = 'idIssueDate';
  placeOfIdentity = 'idIssuePlace';
  documentMain = 'isMain';
  identityExpireDate = 'toDate';
  fromDate = 'fromDate';
  listIdentityType: SelfLookupValues[] = [];
  subs: Subscription[] = [];
  fileList: NzUploadFile[] = [];

  isFileEmpty = false;
  docIdsDelete: number[] = [];
  isLoadingPage = false;
  constant = Constant;
  listPlace: CatalogModel[] = [];

  constructor(
    private fb: FormBuilder,
    private staffInfoService: SelfStaffInfoService,
    private shareDataService: SelfShareDataService,
    private identityService: SelfIdentityInfoService,
    private toastrService: ToastrService,
    private translate: TranslateService,
    private modalRef: NzModalRef,
    private downloadFileAttachService: DownloadFileAttachService
  ) {
    this.form = this.fb.group({});
    this.form.addControl(this.identityType, new FormControl(null, Validators.required));
    this.form.addControl(this.identityNumber, new FormControl(null, [Validators.required, Validators.minLength(6), Validators.maxLength(12)]));
    this.form.addControl(this.dateOfIdentity, new FormControl(null));
    this.form.addControl(this.placeOfIdentity, new FormControl(null, Validators.maxLength(199)));
    this.form.addControl(this.documentMain, new FormControl(null));
    this.form.addControl(this.identityExpireDate, new FormControl(null));
    this.form.addControl(this.personalIdentityId, new FormControl(null));
    this.form.addControl(this.fromDate, new FormControl(null));
    this.form.addValidators(DateValidator.validateRangeDate(this.dateOfIdentity, this.identityExpireDate, 'rangeDateError'));
  }

  ngOnInit(): void {
    this.getIdentityType();
    this.pathValue();
  }

  ngOnDestroy() {
    this.subs?.forEach(sub => sub.unsubscribe());
  }

  getIdentityType() {
    this.subs.push(
      this.staffInfoService.getCatalog(Constant.CATALOGS.LOAI_GIAY_TO).subscribe(res => {
        const response: BaseResponse = res;
        this.listIdentityType = response.data;
      })
    );
  }

  beforeUpload = (file: NzUploadFile): boolean => {
    this.fileList = beforeUploadFile(file, this.fileList, 3000000, this.toastrService, this.translate, 'common.notification.errorFile', '.PDF', '.TIF', '.JPG', '.GIF', '.PNG');
    this.isFileEmpty = false;
    return false;
  };

  removeFile = (file: NzUploadFile) => {
    this.docIdsDelete.push(Number(file.uid));
    const index = this.fileList.indexOf(file);
    this.fileList.splice(index, 1);
    if (this.fileList.length === 0) {
      this.isFileEmpty = true;
    }
    return false;
  }

  save() {
    this.isSubmitted = true;
    if (this.fileList.length === 0) this.isFileEmpty = true;

    if (this.form.valid && this.fileList.length > 0) {
      this.isLoadingPage = true;
      const value: SelfIdentityInfo = this.form.value;
      value.employeeId = this.shareDataService.getEmployee().employeeId;
      value.idIssueDate = value.idIssueDate ? moment(value.idIssueDate).format('DD/MM/YYYY') : '';
      value.toDate = value.toDate ? moment(value.toDate).format('DD/MM/YYYY') : '';
      value.docIdsDelete = this.docIdsDelete;
      if (this.data?.personalIdentityId) {
        value.isCloneFile = 1;
      }

      const formData = new FormData();
      formData.append('data', new Blob([JSON.stringify(value)], {
        type: 'application/json'
      }));
      this.fileList.forEach((nzFile: NzUploadFile) => {
        formData.append('files', nzFile as NzSafeAny);
      });

      this.subs.push(this.identityService.saveIdentityInfo(formData).subscribe(res => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.toastrService.success(this.translate.instant(this.data ? 'common.notification.updateSuccess' : 'common.notification.addSuccess'));
            this.modalRef.close({refresh: true})
          } else {
            this.toastrService.error(this.translate.instant(this.data ? 'common.notification.updateError' : 'common.notification.addError') + ':' + res.message);
          }
          this.isLoadingPage = false;
        }, error => {
          this.toastrService.error(this.translate.instant(this.data ? 'common.notification.updateError' : 'common.notification.addError') + ': ' + error?.message);
          this.isLoadingPage = false;
        })
      );
    }
  }

  pathValue() {
    if (this.data) {
      this.form.get(this.identityType)?.setValue(this.data.idTypeCode);
      this.form.get(this.identityNumber)?.setValue(this.data.idNo);
      this.form.get(this.dateOfIdentity)?.setValue(this.data.idIssueDate ? moment(this.data.idIssueDate, 'DD/MM/YYYY').toDate() : null);
      this.form.get(this.placeOfIdentity)?.setValue(this.data.idIssuePlace);
      this.form.get(this.documentMain)?.setValue(this.data.isMain);
      this.form.get(this.identityExpireDate)?.setValue(this.data.toDate ? moment(this.data.toDate, 'DD/MM/YYYY').toDate() : null);
      this.form.get(this.personalIdentityId)?.setValue(this.data.personalIdentityId);
      this.form.get(this.fromDate)?.setValue(this.data.fromDate);

      // file
      if (this.data?.attachFileList && this.data?.attachFileList.length > 0) {
        this.data?.attachFileList.forEach((item) => {
          this.fileList.push({
            uid: item.docId?.toString(),
            name: item.fileName,
            url: item.security,
            status: 'done'
          });
        });
        this.fileList = [...this.fileList];
      }

    }
  }

  downloadFile = (file: NzUploadFile) => {
    if (!file.url) {
      return;
    }
    this.subs.push(
      this.downloadFileAttachService.doAbsDownloadAttachFile(Number(file.uid), file.url).subscribe({
        next: (res) => {
          const reportFile = new Blob([res], {type: getTypeExport(file.name.split(".").pop())});
          saveAs(reportFile, file.name);
        }, error: (err) => {
          this.toastrService.error(err.message);
        }
      })
    );
  }

  selectIdentityType(event: string) {
    let typeCode;
    if (event === Constant.PAPERS_CODE.ID_NO) {
      typeCode = Constant.CATALOGS.NOI_CAP_CMND;
    } else if (event === Constant.PAPERS_CODE.CITIZEN_ID) {
      typeCode = Constant.CATALOGS.NOI_CAP_CCCD;
    }
    if (typeCode) {
      this.subs.push(
        this.staffInfoService.getCatalog(typeCode).subscribe(res => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.listPlace = res.data.map((item: NzSafeAny) => {
              item.value = item.label;
              return item;
            })
          } else {
            this.toastrService.error(res?.message);
          }
        }, error => this.toastrService.error(error.message))
      );
    }

  }

}
