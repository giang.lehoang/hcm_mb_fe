import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelfDegreeInformationComponent } from './self-degree-information/self-degree-information.component';
import { NzTableModule } from 'ng-zorro-antd/table';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { SharedPipesStatusModule } from '@hcm-mfe/shared/pipes/status';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';
import { NzTagModule } from 'ng-zorro-antd/tag';

@NgModule({
  imports: [CommonModule, NzTableModule, TranslateModule, SharedUiMbButtonModule, SharedPipesStatusModule, NzPopconfirmModule, NzTagModule],
  declarations: [SelfDegreeInformationComponent],
  exports: [SelfDegreeInformationComponent],
})
export class SelfServiceFeatureSelfStaffInfoSelfDegreeInformationModule {}
