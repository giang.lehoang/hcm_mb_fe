import {Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {Subscription} from "rxjs";
import {NzModalRef, NzModalService} from "ng-zorro-antd/modal";
import {TranslateService} from "@ngx-translate/core";
import {ToastrService} from "ngx-toastr";
import {Pagination} from '@hcm-mfe/shared/data-access/models';
import {ValidateService} from '@hcm-mfe/shared/core';
import {SelfDegreeInfoService, SelfShareDataService} from '@hcm-mfe/self-service/data-access/services';
import {BaseResponse, SelfDegreeInfo, SelfDegreeInfoGroup} from '@hcm-mfe/self-service/data-access/models';
import {SelfAddDegreeInfoComponent} from '@hcm-mfe/self-service/feature/self-staff-info/self-add-degree-info';
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";

@Component({
  selector: 'app-degree-infomation',
  templateUrl: './self-degree-information.component.html',
  styleUrls: ['./self-degree-information.component.scss']
})
export class SelfDegreeInformationComponent implements OnInit, OnDestroy {
  items!: SelfDegreeInfoGroup;
  loading = false;
  degreeInfos: SelfDegreeInfo[] = [];
  subs: Subscription[] = [];
  response: BaseResponse = new BaseResponse();
  employeeId = 1;
  modal!: NzModalRef;
  count = 0;
  pagination = new Pagination();
  listStatus = [
    {key: "INSERT", value: this.translate.instant('selfService.label.suggestAdd'), color: 'purple'},
    {key: "DELETE", value: this.translate.instant('selfService.label.suggestDelete'), color: 'red'},
    {key: "UPDATE", value: this.translate.instant('selfService.label.suggestEdit'), color: 'blue'},
    {key: "APPROVED", value: this.translate.instant('selfService.label.approved'), color: 'green'},
  ];

  constructor(
    public validateService: ValidateService,
    private degreeInfoService: SelfDegreeInfoService,
    private modalService: NzModalService,
    private translate: TranslateService,
    private toastrService: ToastrService,
    private shareDataService: SelfShareDataService,
  ) {
  }

  ngOnInit(): void {
    this.subs.push(
      this.shareDataService.employee$.subscribe(employee => {
        if (employee.employeeId)
        this.employeeId = employee.employeeId;
        this.pagination.pageNumber = 1;
        this.getDegreeInfos()
      })
    );
  }

  ngOnDestroy() {
    this.modal?.destroy();
    this.subs.forEach(sub => sub.unsubscribe());
  }

  getDegreeInfos() {
    this.loading = true;
    const param = this.pagination.getCurrentPage();
    this.subs.push(
      this.degreeInfoService.getDegreeInfosV2(this.employeeId, param).subscribe(res => {
        this.response = res;
        this.loading = false
        this.count = this.response?.data?.count;
        this.degreeInfos = this.response?.data?.listData;
        this.items = {employeeId: this.employeeId, data: this.response?.data?.listData};
      }, error => {
        this.loading = false;
      })
    );
  }

  refresh() {
    this.getDegreeInfos();
  }

  resetContextMenu() {

  }

  rightClickTable($event: MouseEvent, data: SelfDegreeInfo) {

  }

  showModalUpdate(data: SelfDegreeInfo, footerTmpl: TemplateRef<NzSafeAny>) {
    const valueData = {employeeId: this.employeeId, data: data};
    this.modal = this.modalService.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 2 : window.innerWidth,
      nzTitle: this.translate.instant('selfService.degreeInformation.modal.update'),
      nzContent: SelfAddDegreeInfoComponent,
      nzComponentParams: {
        data: data
      },
      nzFooter: footerTmpl
    });
    this.subs.push(this.modal.afterClose.subscribe(result => result?.refresh ? this.refresh() : ''));
  }

  deleteItem(data: SelfDegreeInfo) {
    this.subs.push(
      this.degreeInfoService.deleteDegreeDraft(data.educationDegreeId).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.pagination.pageNumber = 1;
          this.getDegreeInfos();
          this.toastrService.success(this.translate.instant('common.notification.deleteSuccess'));
        }
      }, error => this.toastrService.error(error.message))
    );
  }

  rollBack(draftEducationDegreeId: number) {
    this.subs.push(
      this.degreeInfoService.rollBack(draftEducationDegreeId).subscribe(res => {
        if (res) {
          this.getDegreeInfos();
        }
      })
    );
  }
}
