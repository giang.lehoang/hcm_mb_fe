import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelfPersonalInformationsComponent } from './self-personal-informations/self-personal-informations.component';
import { SharedDirectivesScrollSpyModule } from '@hcm-mfe/shared/directives/scroll-spy';
import { NzFormModule } from 'ng-zorro-antd/form';
import {
  SelfServiceFeatureSelfStaffInfoSelfBriefDescriptionModule
} from '@hcm-mfe/self-service/feature/self-staff-info/self-brief-description';
import { SharedUiMbScrollTabsModule } from '@hcm-mfe/shared/ui/mb-scroll-tabs';
import { SharedUiMbCollapseModule } from '@hcm-mfe/shared/ui/mb-collapse';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [CommonModule, SharedDirectivesScrollSpyModule, NzFormModule, SelfServiceFeatureSelfStaffInfoSelfBriefDescriptionModule,
    SharedUiMbScrollTabsModule, SharedUiMbCollapseModule, SharedUiMbInputTextModule, SharedUiMbButtonModule, TranslateModule, FormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: SelfPersonalInformationsComponent
      }
    ])
  ],
  declarations: [SelfPersonalInformationsComponent],
  exports: [SelfPersonalInformationsComponent],
})
export class SelfServiceFeatureSelfStaffInfoSelfPersonalInformationsModule {}
