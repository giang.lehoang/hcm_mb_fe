import { PanelOption } from '@hcm-mfe/shared/data-access/models';
import {
  SelfPersonalInformationComponent
} from '@hcm-mfe/self-service/feature/self-staff-info/self-personal-information';
import { SelfEditPersonalInfoComponent } from '@hcm-mfe/self-service/feature/self-staff-info/self-edit-personal-info';
import {
  SelfIdentityInformationComponent
} from '@hcm-mfe/self-service/feature/self-staff-info/self-identity-information';
import { SelfAddIdentityInfoComponent } from '@hcm-mfe/self-service/feature/self-staff-info/self-add-identity-info';
import {
  SelfContactInformationComponent
} from '@hcm-mfe/self-service/feature/self-staff-info/self-contact-information';
import { SelfEditContactInfoComponent } from '@hcm-mfe/self-service/feature/self-staff-info/self-edit-contact-info';
import { SelfBankInformationComponent } from '@hcm-mfe/self-service/feature/self-staff-info/self-bank-information';
import { SelfPartyInformationComponent } from '@hcm-mfe/self-service/feature/self-staff-info/self-party-information';
import { SelfEditPartyInfoComponent } from '@hcm-mfe/self-service/feature/self-staff-info/self-edit-party-info';
import {
  SelfRelativesInformationComponent
} from '@hcm-mfe/self-service/feature/self-staff-info/self-relatives-information';
import { SelfEditRelativesInfoComponent } from '@hcm-mfe/self-service/feature/self-staff-info/self-edit-relatives-info';
import {
  SelfFamilyDeductionsInformationComponent
} from '@hcm-mfe/self-service/feature/self-staff-info/self-family-deductions-information';
import {
  SelfWorkHisBeforeInformationComponent
} from '@hcm-mfe/self-service/feature/self-staff-info/self-work-his-before-information';
import {
  SelfWorkHisInformationComponent
} from '@hcm-mfe/self-service/feature/self-staff-info/self-work-his-information';
import {
  SelfProjectHisInformationComponent
} from '@hcm-mfe/self-service/feature/self-staff-info/self-project-his-information';
import {
  SelfContractHisInformationComponent
} from '@hcm-mfe/self-service/feature/self-staff-info/self-contract-his-information';
import { SelfDegreeInformationComponent } from '@hcm-mfe/self-service/feature/self-staff-info/self-degree-information';
import { SelfAddDegreeInfoComponent } from '@hcm-mfe/self-service/feature/self-staff-info/self-add-degree-info';
import { SelfEduHisInfomationComponent } from '@hcm-mfe/self-service/feature/self-staff-info/self-edu-his-infomation';
import {
  SelfRewardHisInformationComponent
} from '@hcm-mfe/self-service/feature/self-staff-info/self-reward-his-information';
import {
  SelfDisciplinaryHisInformationComponent
} from '@hcm-mfe/self-service/feature/self-staff-info/self-disciplinary-his-information';
import {
  SelfDocumentAttachedInformationComponent
} from '@hcm-mfe/self-service/feature/self-staff-info/self-document-attached-information';
import { SelfAddWorkHisBeforeComponent } from '@hcm-mfe/self-service/feature/self-staff-info/self-add-work-his-before';
import {SelfEvaluateInfoComponent} from "@hcm-mfe/self-service/feature/self-staff-info/self-evaluate-info";


export const PANELS_INFORMATION: PanelOption[] = [
  {
    id: 'personal_information',
    active: true,
    disabled: false,
    icon: 'assets/icon/self-staff-manager/personal-info.svg',
    name: 'selfService.panel.personalInformation',
    panelComponent: SelfPersonalInformationComponent,
    extraMode: [{
      type: 'EDIT_MODAL',
      contentHeader: 'selfService.panel.personalInformation',
      content: SelfEditPersonalInfoComponent
    }]
  },
  {
    id: 'identity_information',
    active: true,
    disabled: false,
    icon: 'assets/icon/self-staff-manager/identification.svg',
    name: 'selfService.panel.identityInformation',
    panelComponent: SelfIdentityInformationComponent,
    extraMode: [{
      type: 'ADD_MODAL',
      contentHeader: 'selfService.panel.identityInformation',
      content: SelfAddIdentityInfoComponent
    }]
  },
  {
    id: 'contact_information',
    active: true,
    disabled: false,
    icon: 'assets/icon/self-staff-manager/contact-info.svg',
    name: 'selfService.panel.contactInformation',
    panelComponent: SelfContactInformationComponent,
    extraMode: [{
      type: 'EDIT_MODAL',
      contentHeader: 'selfService.panel.contactInformation',
      content: SelfEditContactInfoComponent
    }]
  },
  {
    id: 'bank_information',
    active: true,
    disabled: false,
    icon: 'assets/icon/self-staff-manager/credit-card.svg',
    name: 'selfService.panel.bankInformation',
    panelComponent: SelfBankInformationComponent
  },
  {
    id: 'party_information',
    active: true,
    disabled: false,
    icon: 'assets/icon/self-staff-manager/collection-bookmark.svg',
    name: 'selfService.panel.partyInformation',
    panelComponent: SelfPartyInformationComponent,
    extraMode: [{
      type: 'EDIT_MODAL',
      contentHeader: 'selfService.panel.partyInformation',
      content: SelfEditPartyInfoComponent
    }]
  },
  {
    id: 'relatives_information',
    active: true,
    disabled: false,
    icon: 'assets/icon/self-staff-manager/collection-bookmark.svg',
    name: 'selfService.panel.relativesInformation',
    panelComponent: SelfRelativesInformationComponent,
    extraMode: [{
      type: 'ADD_MODAL',
      content: SelfEditRelativesInfoComponent,
      contentHeader: 'selfService.relativesInformation.modal.add'
    }]
  },
  {
    id: 'family_deductions_information',
    active: true,
    disabled: false,
    icon: 'assets/icon/self-staff-manager/list-alt.svg',
    name: 'selfService.panel.familyDeductionsInformation',
    panelComponent: SelfFamilyDeductionsInformationComponent
  },
  {
    id: 'work_his_before_information',
    active: true,
    disabled: false,
    icon: 'assets/icon/self-staff-manager/history.svg',
    name: 'selfService.panel.workHisBeforeInformation',
    panelComponent: SelfWorkHisBeforeInformationComponent,
    extraMode: [{
      type: 'ADD_MODAL',
      content: SelfAddWorkHisBeforeComponent,
      contentHeader: 'selfService.workHisBeforeInformation.modal.add'
    }]
  },
  {
    id: 'work_his_information',
    active: true,
    disabled: false,
    icon: 'assets/icon/self-staff-manager/history.svg',
    name: 'selfService.panel.workHisInformation',
    panelComponent: SelfWorkHisInformationComponent
  },
  {
    id: 'project_his_information',
    active: true,
    disabled: false,
    icon: 'assets/icon/self-staff-manager/history.svg',
    name: 'selfService.panel.projectHisInformation',
    panelComponent: SelfProjectHisInformationComponent
  },
  {
    id: 'contract_his_information',
    active: true,
    disabled: false,
    icon: 'assets/icon/self-staff-manager/history.svg',
    name: 'selfService.panel.contractHisInformation',
    panelComponent: SelfContractHisInformationComponent
  },
  {
    id: 'evaluate_information',
    active: true,
    disabled: false,
    icon: 'assets/icon/self-staff-manager/history.svg',
    name: 'selfService.panel.evaluateInformation',
    panelComponent: SelfEvaluateInfoComponent
  },
  // {
  //   id: 'salary_his_information',
  //   active: true,
  //   disabled: false,
  //   icon: 'assets/icon/self-staff-manager/history.svg',
  //   name: 'selfService.panel.salaryHisInformation',
  //   panelComponent: SelfSalaryHisInformationComponent
  // },
  // {
  //   id: 'allowance_his_information',
  //   active: true,
  //   disabled: false,
  //   icon: 'assets/icon/self-staff-manager/history.svg',
  //   name: 'selfService.panel.allowanceHisInformation',
  //   panelComponent: SelfAllowanceHisInformationComponent
  // },
  {
    id: 'degree_information',
    active: true,
    disabled: false,
    icon: 'assets/icon/self-staff-manager/collection-bookmark.svg',
    name: 'selfService.panel.degreeInformation',
    panelComponent: SelfDegreeInformationComponent,
    extraMode: [{
      type: 'ADD_MODAL',
      content: SelfAddDegreeInfoComponent,
      contentHeader: 'selfService.degreeInformation.modal.add'
    }]
  },
  {
    id: 'edu_his_information',
    active: true,
    disabled: false,
    icon: 'assets/icon/self-staff-manager/list-alt.svg',
    name: 'selfService.panel.eduHisInformation',
    panelComponent: SelfEduHisInfomationComponent
  },
  {
    id: 'reward_his_information',
    active: true,
    disabled: false,
    icon: 'assets/icon/self-staff-manager/list-alt.svg',
    name: 'selfService.panel.rewardHisInformation',
    panelComponent: SelfRewardHisInformationComponent
  },
  {
    id: 'disciplinary_his_information',
    active: true,
    disabled: false,
    icon: 'assets/icon/self-staff-manager/list-alt.svg',
    name: 'selfService.panel.disciplinaryHisInformation',
    panelComponent: SelfDisciplinaryHisInformationComponent
  },
  {
    id: 'document_attached_information',
    active: true,
    disabled: false,
    icon: 'assets/icon/self-staff-manager/list-alt.svg',
    name: 'selfService.panel.documentAttachedInformation',
    panelComponent: SelfDocumentAttachedInformationComponent
  }
];
