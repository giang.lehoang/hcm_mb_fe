export const SCROLL_TABS_DATA = [
  {
    title:'selfService.tab.personalInformation',
    scrollTo: 'personal_information'
  },
  {
    title:'selfService.tab.workHisBeforeInformation',
    scrollTo: 'work_his_before_information'
  },
  {
    title:'selfService.tab.workHisInformation',
    scrollTo: 'work_his_information'
  },
  {
    title:'selfService.tab.eduHisInformation',
    scrollTo: 'edu_his_information'
  },
  {
    title:'selfService.tab.rewardHisInformation',
    scrollTo: 'reward_his_information'
  },
  {
    title:'selfService.tab.documentAttachedInformation',
    scrollTo: 'document_attached_information',
  },

]
