import {Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {NzModalRef, NzModalService} from 'ng-zorro-antd/modal';
import {PANELS_INFORMATION} from './self-personal-infomations.config';
import {TranslateService} from '@ngx-translate/core';
import {SCROLL_TABS_DATA} from './self-scroll-tab.config';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {MbCollapseComponent} from '@hcm-mfe/shared/ui/mb-collapse';
import {SelfPersonalInfoService, SelfShareDataService} from '@hcm-mfe/self-service/data-access/services';
import {StorageService} from '@hcm-mfe/shared/common/store';
import {Mode, STORAGE_NAME} from '@hcm-mfe/shared/common/constants';
import {SessionKey} from '@hcm-mfe/shared/common/enums';
import {SelfPersonalInfo} from '@hcm-mfe/self-service/data-access/models';
import {PanelOption, ScrollTabOption, User} from '@hcm-mfe/shared/data-access/models';
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-personal-infomations',
  templateUrl: './self-personal-infofmations.component.html',
  styleUrls: ['./self-personal-informations.component.scss']
})
export class SelfPersonalInformationsComponent implements OnInit, OnDestroy {
  public panels = PANELS_INFORMATION;
  public scrollTabs: ScrollTabOption[] = SCROLL_TABS_DATA;
  formSearch: FormGroup;
  modal!: NzModalRef;
  employeeId!: number;
  employeeCode!: string;
  subs: Subscription[] = [];
  pageName: string;
  option: NzSafeAny;

  @ViewChild('collapse') collapse!: MbCollapseComponent;
  @ViewChild('footerTmpl') footerTmpl!: TemplateRef<NzSafeAny>;

  constructor(private modalService: NzModalService,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private translate: TranslateService,
              private staffInfoService: SelfPersonalInfoService,
              private shareService: SelfShareDataService,
              private fb: FormBuilder
  ) {
    this.pageName = this.translate.instant(this.activatedRoute.snapshot.data['pageName']);
    const userLogin = StorageService.get(STORAGE_NAME.USER_LOGIN);
    let currentUser: User;
    if (localStorage.getItem(SessionKey.CURRENCY_USER)) {
      currentUser = JSON.parse(localStorage.getItem(SessionKey.CURRENCY_USER) ?? '');
    }
    this.subs.push(this.activatedRoute.queryParams.subscribe((params) => {
      this.employeeId = params['employeeId'] ?? userLogin.employeeId ?? currentUser?.empId ?? 1;
      this.employeeCode = params['employeeCode'] ?? userLogin.employeeCode ?? currentUser?.empCode ?? 'MB001';
    }));
    this.formSearch = this.fb.group({
      formStaff: null,
      employeeCode: null
    });
  }

  ngOnInit(): void {
    this.shareService.changeEmployee({ employeeId: this.employeeId, employeeCode: this.employeeCode });
  }

  openEditModal(panel: PanelOption, type: string) {
    const extraOption = panel.extraMode?.find(mode => mode.type === type);
    const instance = this.collapse.getInstance(panel);
    this.modal = this.modalService.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
      nzTitle: this.translate.instant(extraOption?.contentHeader ?? ''),
      nzContent: extraOption?.content,
      nzComponentParams: {
        mode: Mode.EDIT,
        data: instance.items
      },
      nzFooter: this.footerTmpl
    });

    this.subs.push(
      this.modal.afterClose.subscribe(res => {
        if (res && res.refresh) {
          instance.refresh();
        }
      })
    )
  }

  openAddModal(panel: PanelOption, type: string) {
    const extraOption = panel.extraMode?.find(mode => mode.type === type);
    const instance = this.collapse.getInstance(panel);

    this.modal = this.modalService.create({
      nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
      nzTitle: this.translate.instant(extraOption?.contentHeader ?? ''),
      nzContent: extraOption?.content,
      nzComponentParams: {
        mode: Mode.ADD
      },
      nzFooter: this.footerTmpl
    });
    this.subs.push(this.modal.afterClose.subscribe(res => {
      if (res && res.refresh) {
        instance.refresh();
      }
    }));
  }

  onSearchChange(panel: PanelOption, value: string) {
    const instance = this.collapse.getInstance(panel);
    instance.searchData(value);
  }

  checkExtraMode = (panel: PanelOption, type: string) => panel.extraMode?.find(mode => mode.type === type);

  cancel() {
    this.modal.destroy();
  }

  save() {
    this.modal.getContentComponent().save();
  }

  ngOnDestroy() {
    this.modal?.destroy();
    this.subs?.forEach(sub => sub.unsubscribe());
  }

  onEmpInfoReady(event: SelfPersonalInfo) {
    this.option = {
      pageName: this.pageName,
      empCode: event.employeeCode,
      empName: event.fullName,
      isSelfService: true
    }
  }

}
