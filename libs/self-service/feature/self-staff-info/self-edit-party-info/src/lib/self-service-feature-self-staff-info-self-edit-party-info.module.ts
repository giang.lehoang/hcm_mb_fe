import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelfEditPartyInfoComponent } from './self-edit-party-info/self-edit-party-info.component';
import { NzFormModule } from 'ng-zorro-antd/form';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedUiMbDatePickerModule } from '@hcm-mfe/shared/ui/mb-date-picker';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
    imports: [CommonModule, NzFormModule, ReactiveFormsModule, SharedUiMbDatePickerModule, TranslateModule, SharedUiMbInputTextModule, SharedUiMbSelectModule, SharedUiLoadingModule],
  declarations: [SelfEditPartyInfoComponent],
  exports: [SelfEditPartyInfoComponent]
})
export class SelfServiceFeatureSelfStaffInfoSelfEditPartyInfoModule {}
