import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Subscription} from "rxjs";
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";
import {NzModalRef} from "ng-zorro-antd/modal";
import * as moment from 'moment';
import {
  BaseResponse,
  SelfLookupValues,
  SelfPartyInfo,
  SelfPartyInfoGroup
} from '@hcm-mfe/self-service/data-access/models';
import {SelfPartyInfoService, SelfStaffInfoService} from '@hcm-mfe/self-service/data-access/services';
import {DateValidator} from '@hcm-mfe/shared/common/validators';
import {Constant} from '@hcm-mfe/self-service/data-access/common';
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";

@Component({
  selector: 'app-edit-party-info',
  templateUrl: './self-edit-party-info.component.html',
  styleUrls: ['./self-edit-party-info.component.scss']
})
export class SelfEditPartyInfoComponent implements OnInit, OnDestroy {
  @Input() data!: SelfPartyInfoGroup;
  form: FormGroup;
  armyLevels: SelfLookupValues[] = [];
  subs: Subscription[] = [];
  partyInfo!: SelfPartyInfo;
  isSubmitted = false;
  isLoadingPage = false;


  constructor(
    private fb: FormBuilder,
    private staffInfoService: SelfStaffInfoService,
    private partyInfoService: SelfPartyInfoService,
    private toastService: ToastrService,
    private translate: TranslateService,
    private modalRef: NzModalRef
  ) {
    this.form = this.fb.group({
      partyDate: null,
      partyPlace: [null, Validators.maxLength(200)],
      partyOfficialDate: null,
      armyJoinDate: null,
      armyLevelCode: null,
    },{
      validators: [
        DateValidator.validateRangeDate('partyDate', 'partyOfficialDate', 'rangeDateError')
      ]
    });
  }

  ngOnInit(): void {
    this.partyInfo = this.data?.data;
    this.getCatalogs();
    this.pathValue();
  }

  ngOnDestroy() {
    this.modalRef?.destroy();
    this.subs?.forEach(sub => sub.unsubscribe());
  }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      this.isLoadingPage = true;
      const value: SelfPartyInfo = this.form.value;
      value.partyDate = value.partyDate ? moment(value.partyDate).format('DD/MM/YYYY') : '';
      value.partyOfficialDate = value.partyOfficialDate ? moment(value.partyOfficialDate).format('DD/MM/YYYY') : '';
      value.armyJoinDate = value.armyJoinDate ? moment(value.armyJoinDate).format('DD/MM/YYYY') : '';
      this.subs.push(
        this.partyInfoService.savePartyInfo(this.data?.employeeId, value).subscribe(res => {
          if (res && res.code === HTTP_STATUS_CODE.OK) {
            this.toastService.success(this.translate.instant('common.notification.updateSuccess'));
            this.modalRef.close({refresh: true});
          } else {
            this.toastService.error(this.translate.instant('common.notification.updateError') + ': ' + res?.message);
            this.modalRef.close({refresh: false});
          }
          this.isLoadingPage = false;
        }, error => {
          this.toastService.error(this.translate.instant('common.notification.updateError') + ': ' + error?.message);
          this.modalRef.close({refresh: false});
          this.isLoadingPage = false;
        })
      );
    }
  }

  pathValue() {
    if (this.partyInfo) {
      this.form.patchValue({
        partyDate: this.partyInfo.partyDate ? moment(this.partyInfo.partyDate, 'DD/MM/YYYY').toDate() : null,
        partyPlace: this.partyInfo.partyPlace,
        partyOfficialDate: this.partyInfo.partyOfficialDate ? moment(this.partyInfo.partyOfficialDate, 'DD/MM/YYYY').toDate() : null,
        armyJoinDate: this.partyInfo.armyJoinDate ? moment(this.partyInfo.armyJoinDate, 'DD/MM/YYYY').toDate() : null,
        armyLevelCode: this.partyInfo.armyLevelCode,
      });
    }
  }

  getCatalogs() {
    this.getArmyLevels();
  }

  getArmyLevels() {
    this.subs.push(
      this.staffInfoService.getCatalog(Constant.CATALOGS.CAP_BAC_QUAN_HAM).subscribe(res => {
        const response: BaseResponse = res;
        this.armyLevels = response.data;
      })
    );
  }

}
