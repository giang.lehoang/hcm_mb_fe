import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ToastrService} from "ngx-toastr";
import {NzModalRef, NzModalService} from "ng-zorro-antd/modal";
import {TranslateService} from "@ngx-translate/core";
import { SelfAllowanceHistory } from '@hcm-mfe/self-service/data-access/models';
import { BaseResponse, Pagination } from '@hcm-mfe/shared/data-access/models';
import { ValidateService } from '@hcm-mfe/shared/core';
import { SelfAllowanceHisInfoService, SelfShareDataService } from '@hcm-mfe/self-service/data-access/services';
import { HTTP_STATUS_CODE } from '@hcm-mfe/shared/common/constants';
import {Subscription} from "rxjs";


@Component({
  selector: 'app-allowance-his-information',
  templateUrl: './self-allowance-his-information.component.html',
  styleUrls: ['./self-allowance-his-information.component.scss'],
})
export class SelfAllowanceHisInformationComponent implements OnInit {
  items: SelfAllowanceHistory[] = [];
  loading = false;
  employeeId = 1;
  modal!: NzModalRef;
  count = 0;
  pagination = new Pagination();
  subs: Subscription[] = [];

  constructor(
    public validateService: ValidateService,
    private http: HttpClient,
    private toastrService: ToastrService,
    private modalService: NzModalService,
    private translate: TranslateService,
    private shareDataService: SelfShareDataService,
    private allowanceHisInfoService: SelfAllowanceHisInfoService
  ) { }

  ngOnInit(): void {
    this.subs.push(
      this.shareDataService.employee$.subscribe(employee => {
        if (employee.employeeId)
        this.employeeId = employee.employeeId;
        this.pagination.pageNumber = 1;
        this.getInfo()
      })
    );
  }

  getInfo() {
    this.loading = true;
    let param = this.pagination.getCurrentPage();

    this.subs.push(
      this.allowanceHisInfoService.getList(this.employeeId, param).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.items = res.data.listData;
          this.count = res.data.count;
        } else {
          this.toastrService.error(res?.message);
        }
        this.loading = false
      }, error => {
        this.toastrService.error(error.message)
        this.loading = false
        this.items = []
      })
    );
  }

  resetContextMenu() {

  }

  rightClickTable($event: MouseEvent, data: SelfAllowanceHistory) {

  }

  refresh() { this.getInfo() }

  ngOnDestroy() {
    this.subs?.forEach(sub => sub.unsubscribe());
  }
}
