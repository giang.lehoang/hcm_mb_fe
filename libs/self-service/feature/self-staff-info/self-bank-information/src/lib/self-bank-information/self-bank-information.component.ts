import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {BaseResponse, SelfBankInfo, SelfBankInfoGroup} from '@hcm-mfe/self-service/data-access/models';
import {SelfBankInfoService, SelfShareDataService} from '@hcm-mfe/self-service/data-access/services';
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {ToastrService} from "ngx-toastr";


@Component({
  selector: 'app-bank-information',
  templateUrl: './self-bank-information.component.html',
  styleUrls: ['./self-bank-information.component.scss']
})
export class SelfBankInformationComponent implements OnInit, OnDestroy {
  items?: SelfBankInfoGroup;
  subs: Subscription[] = [];
  response: BaseResponse = new BaseResponse();
  bankInfos: SelfBankInfo[] = [];
  bankInfoMains: SelfBankInfo[] = [];
  employeeId = 1;

  constructor(
    private shareDataService: SelfShareDataService,
    private bankInfoService: SelfBankInfoService,
    private toastrService: ToastrService,
  ) {
  }

  ngOnInit(): void {
    this.subs.push(
      this.shareDataService.employee$.subscribe(employee => {
        if (employee.employeeId) {
          this.employeeId = employee.employeeId;
        }
        this.getBankInfo(this.employeeId);
      })
    );
  }

  ngOnDestroy() {
    this.subs?.forEach(sub => sub.unsubscribe());
  }

  getBankInfo(employeeId: number) {
    this.subs.push(
      this.bankInfoService.getBankInfo(employeeId).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.response = res;
          this.bankInfos = this.response?.data;
          this.bankInfoMains = this.bankInfos?.filter(item => item.isPaymentAccount);
          this.items = {employeeId: employeeId, data: this.response?.data};
        } else {
          this.toastrService.error(res?.message);
        }
      }, error => this.toastrService.error(error.message))
    );
  }

  refresh() {
    this.getBankInfo(this.employeeId);
  }
}
