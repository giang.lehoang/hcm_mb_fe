import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelfBankInformationComponent } from './self-bank-information/self-bank-information.component';
import { NzFormModule } from 'ng-zorro-antd/form';
import { SharedUiMbTextLabelModule } from '@hcm-mfe/shared/ui/mb-text-label';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [CommonModule, NzFormModule, SharedUiMbTextLabelModule, TranslateModule],
  declarations: [SelfBankInformationComponent],
  exports: [SelfBankInformationComponent],
})
export class SelfServiceFeatureSelfStaffInfoSelfBankInformationModule {}
