import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NzModalRef, NzModalService} from "ng-zorro-antd/modal";
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";
import * as moment from 'moment';
import {SelfEmployeeDetail, SelfRelativeInfo} from '@hcm-mfe/self-service/data-access/models';
import {HTTP_STATUS_CODE, Mode} from '@hcm-mfe/shared/common/constants';
import {BaseResponse, Category} from '@hcm-mfe/shared/data-access/models';
import {
  SelfContactInfoService,
  SelfIdentityInfoService,
  SelfPersonalInfoService,
  SelfRelativesInfoService,
  SelfShareDataService,
  SelfStaffInfoService
} from '@hcm-mfe/self-service/data-access/services';
import {Constant} from '@hcm-mfe/self-service/data-access/common';
import {Subscription} from 'rxjs';


@Component({
  selector: 'app-edit-relatives-info',
  templateUrl: './self-edit-relatives-info.component.html',
  styleUrls: ['./self-edit-relatives-info.component.scss'],
})
export class SelfEditRelativesInfoComponent implements OnInit, OnDestroy {
  data = new SelfRelativeInfo();
  mode = Mode.ADD;

  isSubmitted = false;
  form: FormGroup;

  listFamilyRelationship: Category[] = [];

  listRelationStatus: Category[] = [];

  listPolicyType: Category[] = [];
  subs: Subscription[] = [];
  isLoadingPage = false;

  constructor(
    private fb: FormBuilder,
    private modalRef: NzModalRef,
    private toastService: ToastrService,
    private modalService: NzModalService,
    private translate: TranslateService,
    private shareDataService: SelfShareDataService,
    private staffInfoService: SelfStaffInfoService,
    private personalInfoService: SelfPersonalInfoService,
    private identityInfoService: SelfIdentityInfoService,
    private contactInfoService: SelfContactInfoService,
    private relativesInfoService: SelfRelativesInfoService
  ) {
    this.form = fb.group({
      familyRelationshipId: [null],
      employeeId: [null],
      // Mã mối quan hệ
      relationTypeCode: [null, [Validators.required]], //Loại mối quan hệ
      relationStatusCode: [null, [Validators.required]], //Tình trạng thân nhân
      policyTypeCode: [null], // Đối tượng chính sách

      isInCompany: [false], //Có phải nhân viên trong công ty. truyền vào giá trị 1 nếu người dùng chọn checked
      referenceEmployeeId: [{value: null, disabled: true}], //ID nhân viên của thân nhân

      // Chưa có GTTT/MST
      fullName: [null, [Validators.required]], //Họ tên thân nhâ

      personalIdNumber: [null, [Validators.minLength(6), Validators.maxLength(12)]], // CMT / CCCD
      dateOfBirth: [null], // Ngày sinh
      workOrganization: [null, [Validators.maxLength(200)]], // Đơn vị công tác
      job: [null, [Validators.maxLength(200)]], // Nghề nghiệp
      currentAddress: [null, Validators.maxLength(200)], // Chỗ ở hiện tại
      note: [null, [Validators.maxLength(500)]], // Ghi chú
      isHouseholdOwner: [false],
      phoneNumber: [null]
    });
    this.getFamilyRelationshipList();
    this.getRelationStatusList();
    this.getPolicyTypeList();
  }

  get f() {
    return this.form.controls;
  }

  ngOnInit(): void {
    this.onChange();
    if (this.mode === Mode.EDIT)
      this.patchValueInfo();

    this.f['employeeId'].setValue(this.shareDataService.getEmployee().employeeId)
  }

  onChange() {
    this.f['isInCompany'].valueChanges.subscribe(checked => {
      if(checked) {
        this.f['referenceEmployeeId'].disable();
        this.f['fullName'].disable();
        this.f['dateOfBirth'].disable();
        this.f['personalIdNumber'].disable();
        this.f['workOrganization'].disable();
        this.f['job'].disable();
        this.f['currentAddress'].disable();
      } else {
        this.f['referenceEmployeeId'].enable();
        this.f['fullName'].enable();
        this.f['dateOfBirth'].enable();
        this.f['personalIdNumber'].enable();
        this.f['workOrganization'].enable();
        this.f['job'].enable();
        this.f['currentAddress'].enable();
      }
    });
  }



  patchValueInfo() {
    this.subs.push(
      this.relativesInfoService.getRecord(this.data.familyRelationshipId).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.form.patchValue(res.data);
          let isInCompany = false;
          if (res.data.isInCompany && res.data.isInCompany !== 0) {
            isInCompany = true;
          }
          let isHouseholdOwner = false;
          if (res.data.isHouseholdOwner && res.data.isHouseholdOwner !== 0) {
            isHouseholdOwner = true;
          }
          this.f['isInCompany'].patchValue(isInCompany);
          this.f['isHouseholdOwner'].patchValue(isHouseholdOwner);
          this.f['personalIdNumber'].patchValue(res.data.personalIdNumber?.trim());
          if (res.data.dateOfBirth) {
            this.f['dateOfBirth'].setValue(moment(res.data.dateOfBirth, 'DD/MM/YYYY').toDate());
          }
        } else {
          this.toastService.error(res?.message);
        }
      }, () => this.modalRef.close())
    );

  }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      this.isLoadingPage = true;
      const request: SelfRelativeInfo = this.form.getRawValue();
      request.dateOfBirth = this.f['dateOfBirth'].value ? moment(this.f['dateOfBirth'].value).format('DD/MM/YYYY') : null;
      request.isInCompany = this.f['isInCompany'].value ? 1 : 0;
      request.referenceEmployeeId = this.f['isInCompany'].value ? this.f['referenceEmployeeId'].value : null;
      request.isHouseholdOwner = this.f['isHouseholdOwner'].value ? 1 : 0;

      this.subs.push(
        this.relativesInfoService.saveRecord(request).subscribe((res: BaseResponse) => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.toastService.success(this.translate.instant('common.notification.updateSuccess'));
            this.modalRef.close({refresh: true})
          } else if(res.code === HTTP_STATUS_CODE.ACCEPTED){
            this.toastService.warning(this.translate.instant('common.notification.identificationCardWarning'));
            this.toastService.success(this.translate.instant('common.notification.updateSuccess'));
            this.modalRef.close({ refresh: true });
          } else {
            this.toastService.error(this.translate.instant('common.notification.updateError') + ':' + res.message);
          }
          this.isLoadingPage = false;
        }, error => {
          this.toastService.error(this.translate.instant('common.notification.updateError') + ':' + error?.message);
          this.isLoadingPage = false;
        })
      );
    }
  }

  getFamilyRelationshipList() {
    this.subs.push(
      this.staffInfoService.getCatalog(Constant.CATALOGS.MOI_QUAN_HE_NT).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) this.listFamilyRelationship = res.data;
      }, error => this.toastService.error(error.message))
    );
  }

  getRelationStatusList() {
    this.subs.push(
      this.staffInfoService.getCatalog(Constant.CATALOGS.TINH_TRANG_NT).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) this.listRelationStatus = res.data;
      }, error => this.toastService.error(error.message))
    );
  }

  getPolicyTypeList() {
    this.subs.push(
      this.staffInfoService.getCatalog(Constant.CATALOGS.DOITUONG_CHINHSACH).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) this.listPolicyType = res.data;
      }, error => this.toastService.error(error.message))
    )
  }

  searchReferenceEmployee(ref: SelfEmployeeDetail) {
    this.form.patchValue({
      referenceEmployeeId: ref.employeeId,
      fullName: ref.fullName,
      dateOfBirth: ref.dateOfBirth ? moment(ref.dateOfBirth, 'DD/MM/YYYY').toDate() : null,
      personalIdNumber: ref.personalId,
      workOrganization: ref.orgName,
      job: ref.jobName,
      currentAddress: ref.currentAddress
    })
  }

  ngOnDestroy(): void {
    this.modalRef?.destroy();
    this.subs?.forEach(sub => sub.unsubscribe());
  }
}
