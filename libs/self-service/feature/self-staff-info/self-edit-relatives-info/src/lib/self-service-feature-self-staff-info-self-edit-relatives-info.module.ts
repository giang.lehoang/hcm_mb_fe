import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelfEditRelativesInfoComponent } from './self-edit-relatives-info/self-edit-relatives-info.component';
import { NzFormModule } from 'ng-zorro-antd/form';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { TranslateModule } from '@ngx-translate/core';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { SharedUiEmployeeDataPickerModule } from '@hcm-mfe/shared/ui/employee-data-picker';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { SharedDirectivesNumberInputModule } from '@hcm-mfe/shared/directives/number-input';
import { SharedUiMbDatePickerModule } from '@hcm-mfe/shared/ui/mb-date-picker';
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
    imports: [CommonModule, NzFormModule, ReactiveFormsModule, SharedUiMbSelectModule, TranslateModule, NzCheckboxModule, SharedUiEmployeeDataPickerModule, SharedUiMbInputTextModule, FormsModule, SharedDirectivesNumberInputModule, SharedUiMbDatePickerModule, SharedUiLoadingModule],
  declarations: [SelfEditRelativesInfoComponent],
  exports: [SelfEditRelativesInfoComponent],
})
export class SelfServiceFeatureSelfStaffInfoSelfEditRelativesInfoModule {}
