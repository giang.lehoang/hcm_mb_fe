import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelfEduHisInfomationComponent } from './self-edu-his-infomation/self-edu-his-infomation.component';
import { NzTableModule } from 'ng-zorro-antd/table';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';

@NgModule({
  imports: [CommonModule, NzTableModule, TranslateModule, SharedUiMbButtonModule],
  declarations: [SelfEduHisInfomationComponent],
  exports: [SelfEduHisInfomationComponent],
})
export class SelfServiceFeatureSelfStaffInfoSelfEduHisInfomationModule {}
