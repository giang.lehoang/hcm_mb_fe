import {Component, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {NzModalRef, NzModalService} from "ng-zorro-antd/modal";
import {TranslateService} from "@ngx-translate/core";
import {ToastrService} from "ngx-toastr";
import { BaseResponse, SelfEduHisInfo, SelfEduHisInfoGroup } from '@hcm-mfe/self-service/data-access/models';
import { Pagination } from '@hcm-mfe/shared/data-access/models';
import { ValidateService } from '@hcm-mfe/shared/core';
import { SelfEduHisInfoService, SelfShareDataService } from '@hcm-mfe/self-service/data-access/services';

@Component({
  selector: 'app-edu-his-infomation',
  templateUrl: './self-edu-his-infomation.component.html',
  styleUrls: ['./self-edu-his-infomation.component.scss']
})
export class SelfEduHisInfomationComponent implements OnInit {
  items?: SelfEduHisInfoGroup;
  loading = false;
  eduHisInfos: SelfEduHisInfo[] = [];
  subs: Subscription[] = [];
  response: BaseResponse = new BaseResponse();
  employeeId = 1;
  modal!: NzModalRef;
  count = 0;
  pagination = new Pagination();

  constructor(
    public validateService: ValidateService,
    private eduHisInfoService: SelfEduHisInfoService,
    private modalService: NzModalService,
    private translate: TranslateService,
    private toastrService: ToastrService,
    private shareDataService: SelfShareDataService,
  ) { }

  ngOnInit(): void {
    this.subs.push(
      this.shareDataService.employee$.subscribe(employee => {
        if (employee.employeeId)
        this.employeeId = employee.employeeId;
        this.pagination.pageNumber = 1;
        this.getEduHisInfos();
      })
    );
  }

  ngOnDestroy() {
    this.modal?.destroy();
    this.subs.forEach(sub => sub.unsubscribe());
  }

  getEduHisInfos() {
    this.loading = true;
    const param = this.pagination.getCurrentPage();

    this.subs.push(
      this.eduHisInfoService.getEduHisInfo(this.employeeId, param).subscribe(res => {
        this.response = res;
        this.loading = false
        this.eduHisInfos = this.response?.data?.listData;
        this.count = this.response?.data?.count;
        this.items = {employeeId: this.employeeId, data: this.response?.data?.listData};
      }, () =>this.loading = false)
    );
  }

  refresh() {
    this.getEduHisInfos();
  }

  resetContextMenu() {

  }

  rightClickTable($event: MouseEvent, data: SelfEduHisInfo) {

  }
}
