import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import { BaseResponse, SelfPersonalInfo } from '@hcm-mfe/self-service/data-access/models';
import { SelfPersonalInfoService, SelfShareDataService } from '@hcm-mfe/self-service/data-access/services';


@Component({
  selector: 'app-personal-information',
  templateUrl: './self-personal-information.component.html',
  styleUrls: ['./self-personal-information.component.scss']
})
export class SelfPersonalInformationComponent implements OnInit, OnDestroy {
  items!: SelfPersonalInfo;
  showEdit = false;
  personalInfo!: SelfPersonalInfo;
  subs: Subscription[] = [];
  response: BaseResponse = new BaseResponse();
  employeeId = 1;

  constructor(
    private shareDataService: SelfShareDataService,
    private personalInfoService: SelfPersonalInfoService
  ) { }

  ngOnInit(): void {
    this.subs.push(
      this.shareDataService.employee$.subscribe(employee => {
        if (employee.employeeId)
        this.employeeId = employee.employeeId;
        this.getPersonalInfo();
      })
    );
  }

  ngOnDestroy() {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  getPersonalInfo() {
    this.subs.push(
      this.personalInfoService.getPersonalInfo(this.employeeId).subscribe(res => {
        this.response = res;
        this.personalInfo = this.response?.data;
        this.items = this.response?.data;
        this.shareDataService.changePersonalInfo(this.response?.data);
      })
    );
  }

  refresh() {
    this.getPersonalInfo();
  }

}
