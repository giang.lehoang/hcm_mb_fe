import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelfPersonalInformationComponent } from './self-personal-information/self-personal-information.component';
import { NzFormModule } from 'ng-zorro-antd/form';
import { SharedUiMbTextLabelModule } from '@hcm-mfe/shared/ui/mb-text-label';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [CommonModule, NzFormModule, SharedUiMbTextLabelModule, TranslateModule],
  declarations: [SelfPersonalInformationComponent],
  exports: [SelfPersonalInformationComponent],
})
export class SelfServiceFeatureSelfStaffInfoSelfPersonalInformationModule {}
