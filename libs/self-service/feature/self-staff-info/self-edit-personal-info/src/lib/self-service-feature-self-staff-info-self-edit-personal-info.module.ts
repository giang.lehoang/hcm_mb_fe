import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelfEditPersonalInfoComponent } from './self-edit-personal-info/self-edit-personal-info.component';
import { NzFormModule } from 'ng-zorro-antd/form';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { SharedUiMbDatePickerModule } from '@hcm-mfe/shared/ui/mb-date-picker';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { SharedDirectivesNumberInputModule } from '@hcm-mfe/shared/directives/number-input';
import { SharedUiMbInputCompactModule } from '@hcm-mfe/shared/ui/mb-input-compact';
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
    imports: [CommonModule, NzFormModule, ReactiveFormsModule, SharedUiMbInputTextModule, SharedUiMbDatePickerModule, TranslateModule, SharedUiMbSelectModule, SharedDirectivesNumberInputModule, SharedUiMbInputCompactModule, SharedUiLoadingModule],
  declarations: [SelfEditPersonalInfoComponent],
  exports: [SelfEditPersonalInfoComponent],
})
export class SelfServiceFeatureSelfStaffInfoSelfEditPersonalInfoModule {}
