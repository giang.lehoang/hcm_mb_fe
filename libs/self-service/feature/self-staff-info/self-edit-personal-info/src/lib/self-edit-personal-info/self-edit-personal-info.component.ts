import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subscription} from "rxjs";
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";
import {NzModalRef} from "ng-zorro-antd/modal";
import * as moment from 'moment';
import {SelfListMobileNumber, SelfLookupValues, SelfPersonalInfo} from '@hcm-mfe/self-service/data-access/models';
import {
  SelfPersonalInfoService,
  SelfShareDataService,
  SelfStaffInfoService
} from '@hcm-mfe/self-service/data-access/services';
import { Constant } from '@hcm-mfe/self-service/data-access/common';
import { BaseResponse } from '@hcm-mfe/shared/data-access/models';
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";

@Component({
  selector: 'app-edit-personal-info',
  templateUrl: './self-edit-personal-info.component.html',
  styleUrls: ['./self-edit-personal-info.component.scss']
})
export class SelfEditPersonalInfoComponent implements OnInit, OnDestroy {
  @Input() data!: SelfPersonalInfo;
  isSubmitted = false;
  form: FormGroup;
  listSex: SelfLookupValues[] = [];
  listMarital: SelfLookupValues[] = [];
  listNational: SelfLookupValues[] = [];
  listFolk: SelfLookupValues[] = [];
  listReligion: SelfLookupValues[] = [];
  validaInput = [Validators.required, Validators.pattern('^[0-9]{0,10}$')];
  personalInfo!: SelfPersonalInfo;
  subs: Subscription[] = [];
  listPhoneNumbers: SelfListMobileNumber[] = [];
  phoneAreas: SelfLookupValues[] = [];
  isLoadingPage = false;


  constructor(
    private fb: FormBuilder,
    private shareDataService: SelfShareDataService,
    private personalInfoService: SelfPersonalInfoService,
    private staffInfoService: SelfStaffInfoService,
    private toastService: ToastrService,
    private translate: TranslateService,
    private modalRef: NzModalRef
  ) {
    this.form = fb.group({
      fullName: [null, [Validators.required, Validators.maxLength(200)]],
      dateOfBirth: [{value: null, disabled: true}, Validators.required],
      genderCode: [{value: null, disabled: true}, Validators.required],
      maritalStatusCode: [null, Validators.required],
      nationCode: [null, Validators.required],
      ethnicCode: [null, Validators.required],
      religionCode: [null, Validators.required],
      personalEmail: [null, Validators.email],
      companyPhone: [null, Validators.pattern('^[0-9]{0,50}$')],
      listMobileNumber: [null],
      isInsuranceMb: [null, Validators.required],
      insuranceNo: [null, Validators.maxLength(10)],
      taxNo: [null, [Validators.minLength(10), Validators.maxLength(10)]],
      empTypeName: [null],
      positionName: [null],
      orgName: [null],
      empTypeCode: [null],
      positionCode: [null],
      orgCode: [null]
    })
  }

  ngOnInit(): void {
   this.personalInfo = this.data;
   this.getCatalogs();
   this.pathValueInfo();
  }

  ngOnDestroy() {
    this.modalRef?.destroy();
    this.subs.forEach(sub => sub.unsubscribe());
  }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      this.isLoadingPage = true;
      const value: SelfPersonalInfo = Object.assign({}, this.personalInfo, this.form.value);
      value.dateOfBirth = moment(this.form.value.dateOfBirth).format('DD/MM/YYYY');
      value.mobileNumber = value.listMobileNumber ? value.listMobileNumber[value.listMobileNumber.findIndex(item => item.isMain === 1)]?.phoneNumber : value.mobileNumber;
      if (this.personalInfo.employeeId) {
        this.subs.push(this.personalInfoService.savePersonalInfo(this.personalInfo.employeeId, value).subscribe(res => {
          if (res && res.code === HTTP_STATUS_CODE.OK) {
            this.toastService.success(this.translate.instant('common.notification.updateSuccess'));
            this.modalRef.close({refresh: true});
          } else {
            this.toastService.error(this.translate.instant('common.notification.updateError') + ': ' + res?.message);
            this.modalRef.close({refresh: false});
          }
          this.isLoadingPage = false;
        }, error =>  {
          this.toastService.error(this.translate.instant('common.notification.updateError') + ': ' + error?.message);
          this.modalRef.close({refresh: false});
          this.isLoadingPage = false;
        }));
      }
    }
  }

  pathValueInfo() {
    if (this.personalInfo?.listMobileNumber)
    this.listPhoneNumbers = this.personalInfo?.listMobileNumber;
    this.form.patchValue({
      fullName: this.personalInfo?.fullName,
      dateOfBirth: this.personalInfo?.dateOfBirth ? moment(this.personalInfo?.dateOfBirth, 'DD/MM/YYYY').toDate() : null,
      genderCode: this.personalInfo?.genderCode,
      maritalStatusCode: this.personalInfo?.maritalStatusCode,
      nationCode: this.personalInfo?.nationCode,
      ethnicCode: this.personalInfo?.ethnicCode,
      religionCode: this.personalInfo?.religionCode,
      personalEmail: this.personalInfo?.personalEmail,
      companyPhone: this.personalInfo?.companyPhone,
      listMobileNumber: this.personalInfo?.listMobileNumber,
      isInsuranceMb: this.personalInfo?.isInsuranceMb,
      insuranceNo: this.personalInfo?.insuranceNo,
      taxNo: this.personalInfo?.taxNo,
      positionName: this.personalInfo?.positionName,
      empTypeName: this.personalInfo?.empTypeName,
      orgName: this.personalInfo?.orgName,
      empTypeCode: this.personalInfo?.empTypeCode,
      positionCode: this.personalInfo?.positionCode,
      orgCode: this.personalInfo?.orgCode,
    });
  }

  getCatalogs() {
    this.getListGender();
    this.getListMarital();
    this.getNational();
    this.getEthnic();
    this.getReligion();
    this.getPhoneArea();
  }

  getListGender() {
    this.subs.push(
      this.staffInfoService.getCatalog(Constant.CATALOGS.GIOI_TINH).subscribe(res => {
        const response: BaseResponse = res;
        this.listSex = response.data;
      })
    );
  }

  getListMarital() {
    this.subs.push(
      this.staffInfoService.getCatalog(Constant.CATALOGS.TINH_TRANG_HON_NHAN).subscribe(res => {
        const response: BaseResponse = res;
        this.listMarital = response.data;
      })
    );
  }

  getNational() {
    this.subs.push(
      this.staffInfoService.getCatalog(Constant.CATALOGS.QUOC_GIA).subscribe(res => {
        const response: BaseResponse = res;
        this.listNational = response.data;
      })
    );
  }

  getEthnic() {
    this.subs.push(
      this.staffInfoService.getCatalog(Constant.CATALOGS.DAN_TOC).subscribe(res => {
        const response: BaseResponse = res;
        this.listFolk = response.data;
      })
    );
  }

  getReligion() {
    this.subs.push(
      this.staffInfoService.getCatalog(Constant.CATALOGS.TON_GIAO).subscribe(res => {
        const response: BaseResponse = res;
        this.listReligion = response.data;
      })
    );
  }

  getPhoneArea() {
    this.subs.push(
      this.staffInfoService.getCatalog(Constant.CATALOGS.MA_VUNG_DIEN_THOAI).subscribe(res => {
        const response: BaseResponse = res;
        this.phoneAreas = response.data;
      })
    );
  }
}
