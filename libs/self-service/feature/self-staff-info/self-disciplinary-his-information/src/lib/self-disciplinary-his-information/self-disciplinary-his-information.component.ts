import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {TranslateService} from '@ngx-translate/core';
import {NzModalRef, NzModalService} from 'ng-zorro-antd/modal';
import {ToastrService} from 'ngx-toastr';
import { BaseResponse, Pagination } from '@hcm-mfe/shared/data-access/models';
import { ValidateService } from '@hcm-mfe/shared/core';
import {
  SelfDiscipinaryHisInfoService,
  SelfShareDataService
} from '@hcm-mfe/self-service/data-access/services';
import { SelfDisciplinaryHistory } from '@hcm-mfe/self-service/data-access/models';
import { HTTP_STATUS_CODE } from '@hcm-mfe/shared/common/constants';
import {Subscription} from "rxjs";

;

@Component({
  selector: 'app-disciplinary-his-information',
  templateUrl: './self-disciplinary-his-information.component.html',
  styleUrls: ['./self-disciplinary-his-information.component.scss'],
})
export class SelfDisciplinaryHisInformationComponent implements OnInit {
  items: SelfDisciplinaryHistory[] = [];
  loading = false;
  employeeId =  1;
  modal!: NzModalRef;
  count = 0;
  pagination = new Pagination();
  subs: Subscription[] = [];

  constructor(
    public validateService: ValidateService,
    private http: HttpClient,
    private toastrService: ToastrService,
    private modalService: NzModalService,
    private translate: TranslateService,
    private shareDataService: SelfShareDataService,
    private discipinaryHisInfoService: SelfDiscipinaryHisInfoService
  ) { }

  ngOnInit(): void {
    this.subs.push(
      this.shareDataService.employee$.subscribe(employee => {
        if (employee.employeeId)
        this.employeeId = employee.employeeId;
        this.pagination.pageNumber = 1;
        this.getInfo()
      })
    );
  }

  getInfo() {
    this.loading = true;
    let param = this.pagination.getCurrentPage();
    this.subs.push(
      this.discipinaryHisInfoService.getList(this.employeeId, param).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.items = res.data.listData;
          this.count = res.data.count;
        }
        this.loading = false
      }, error => {
        this.toastrService.error(error.message)
        this.loading = false
        this.items = []
      })
    );
  }

  resetContextMenu() {

  }

  rightClickTable($event: MouseEvent, data: SelfDisciplinaryHistory) {

  }
  refresh() { this.getInfo() }

  ngOnDestroy(): void {
    this.modal?.destroy();
    this.subs?.forEach(sub => sub.unsubscribe());
  }

}
