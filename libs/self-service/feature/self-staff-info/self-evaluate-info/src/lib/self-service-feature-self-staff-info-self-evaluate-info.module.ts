import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SelfEvaluateInfoComponent} from './self-evaluate-info/self-evaluate-info.component';
import {NzTableModule} from "ng-zorro-antd/table";
import {TranslateModule} from "@ngx-translate/core";
import {SharedUiMbButtonModule} from "@hcm-mfe/shared/ui/mb-button";

@NgModule({
  imports: [CommonModule, NzTableModule, TranslateModule, SharedUiMbButtonModule],
  declarations: [SelfEvaluateInfoComponent],
  exports: [SelfEvaluateInfoComponent],
})
export class SelfServiceFeatureSelfStaffInfoSelfEvaluateInfoModule {}
