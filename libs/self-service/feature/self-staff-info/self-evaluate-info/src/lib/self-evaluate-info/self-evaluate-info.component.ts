import {Component, OnInit} from '@angular/core';
import {NzModalRef, NzModalService} from "ng-zorro-antd/modal";
import {BaseResponse, Pagination} from "@hcm-mfe/shared/data-access/models";
import {Subscription} from "rxjs";
import {ValidateService} from "@hcm-mfe/shared/core";
import {HttpClient} from "@angular/common/http";
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";
import {EvaluateInfoService, SelfShareDataService} from "@hcm-mfe/self-service/data-access/services";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {NzSafeAny} from "ng-zorro-antd/core/types";

@Component({
  selector: 'hcm-mfe-self-evaluate-info',
  templateUrl: './self-evaluate-info.component.html',
  styleUrls: ['./self-evaluate-info.component.scss']
})
export class SelfEvaluateInfoComponent implements OnInit {

  items: NzSafeAny[] = [];
  loading = false;
  employeeId: number | null = null;
  modal!: NzModalRef;
  count = 0;
  pagination = new Pagination();
  subs: Subscription[] = [];

  constructor(
    public validateService: ValidateService,
    private http: HttpClient,
    private toastrService: ToastrService,
    private modalService: NzModalService,
    private translate: TranslateService,
    private shareDataService: SelfShareDataService,
    private evaluateInfoService: EvaluateInfoService,
  ) { }

  ngOnInit(): void {
    this.getInfo();
  }
  getInfo() {
    this.loading = true;
    const param = {
      page: this.pagination.pageNumber - 1,
      size: this.pagination.pageSize
    }
    this.subs.push(
      this.evaluateInfoService.getEvaluateInfo(param).subscribe((res: BaseResponse) => {
        if (res.status === HTTP_STATUS_CODE.OK) {
          this.items = res.data.content;
          this.count = res.data.totalElements;
        }
        this.loading = false
      }, error => {
        this.toastrService.error(error.message);
        this.loading = false;
        this.items = [];
      })
    );
  }

  refresh() { this.getInfo() }

  ngOnDestroy() {
    this.modal?.destroy();
    this.subs.forEach(sub => sub.unsubscribe());
  }

}
