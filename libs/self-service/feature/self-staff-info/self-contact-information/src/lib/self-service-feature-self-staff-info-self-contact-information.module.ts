import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelfContactInformationComponent } from './self-contact-information/self-contact-information.component';
import { NzFormModule } from 'ng-zorro-antd/form';
import { SharedUiMbTextLabelModule } from '@hcm-mfe/shared/ui/mb-text-label';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [CommonModule, NzFormModule, SharedUiMbTextLabelModule, TranslateModule],
  declarations: [SelfContactInformationComponent],
  exports: [SelfContactInformationComponent],
})
export class SelfServiceFeatureSelfStaffInfoSelfContactInformationModule {}
