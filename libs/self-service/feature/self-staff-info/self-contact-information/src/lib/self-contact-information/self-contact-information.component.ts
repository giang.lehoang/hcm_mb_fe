import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {BaseResponse, SelfContactInfo, SelfContactInfoGroup} from '@hcm-mfe/self-service/data-access/models';
import { SelfContactInfoService, SelfShareDataService } from '@hcm-mfe/self-service/data-access/services';


@Component({
  selector: 'app-contact-information',
  templateUrl: './self-contact-information.component.html',
  styleUrls: ['./self-contact-information.component.scss']
})
export class SelfContactInformationComponent implements OnInit, OnDestroy {
  items!: SelfContactInfoGroup;
  subs: Subscription[] = [];
  response: BaseResponse = new BaseResponse();
  contactInfo?: SelfContactInfo;
  employeeId = 1;

  constructor(
    private shareDataService: SelfShareDataService,
    private contactInfoServices: SelfContactInfoService
  ) { }

  ngOnInit(): void {
    this.subs.push(
      this.shareDataService.employee$.subscribe(employee => {
        if (employee.employeeId)
        this.employeeId = employee.employeeId;
        this.getContactInfo(this.employeeId);
      })
    );
  }

  ngOnDestroy() {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  getContactInfo(employeeId: number) {
    this.subs.push(
      this.contactInfoServices.getContactInfo(employeeId).subscribe(res => {
        this.response = res;
        this.contactInfo = this.response?.data;
        this.items = {employeeId: employeeId, data: this.response?.data};
      })
    );
  }

  refresh() {
    this.getContactInfo(this.employeeId);
  }

}
