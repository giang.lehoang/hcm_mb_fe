import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelfSalaryHisInformationComponent } from './self-salary-his-information/self-salary-his-information.component';
import { NzTableModule } from 'ng-zorro-antd/table';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { SharedPipesFormatCurrencyModule } from '@hcm-mfe/shared/pipes/format-currency';

@NgModule({
  imports: [CommonModule, NzTableModule, TranslateModule, SharedUiMbButtonModule, SharedPipesFormatCurrencyModule],
  declarations: [SelfSalaryHisInformationComponent],
  exports: [SelfSalaryHisInformationComponent],
})
export class SelfServiceFeatureSelfStaffInfoSelfSalaryHisInformationModule {}
