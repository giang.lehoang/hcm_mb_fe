import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {NzModalRef, NzModalService} from 'ng-zorro-antd/modal';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import { SelfSalary } from '@hcm-mfe/self-service/data-access/models';
import { BaseResponse, Pagination } from '@hcm-mfe/shared/data-access/models';
import { ValidateService } from '@hcm-mfe/shared/core';
import { SelfSalaryProgressService, SelfShareDataService } from '@hcm-mfe/self-service/data-access/services';
import { HTTP_STATUS_CODE } from '@hcm-mfe/shared/common/constants';
import {Subscription} from "rxjs";



@Component({
  selector: 'app-salary-his-information',
  templateUrl: './self-salary-his-information.component.html',
  styleUrls: ['./self-salary-his-information.component.scss'],
})
export class SelfSalaryHisInformationComponent implements OnInit {
  items: SelfSalary[] = [];
  loading = false;
  employeeId = 1;
  modal!: NzModalRef;
  count = 0;
  pagination = new Pagination();
  subs: Subscription[] = [];

  constructor(
    public validateService: ValidateService,
    private http: HttpClient,
    private toastrService: ToastrService,
    private modalService: NzModalService,
    private translate: TranslateService,
    private shareDataService: SelfShareDataService,
    private salaryProgressService: SelfSalaryProgressService,
  ) { }

  ngOnInit(): void {
    this.subs.push(
      this.shareDataService.employee$.subscribe(employee => {
        if (employee.employeeId)
        this.employeeId = employee.employeeId;
        this.pagination.pageNumber = 1;
        this.getInfo()
      })
    );
  }

  getInfo() {
    this.loading = true;
    let param = this.pagination.getCurrentPage();
    this.subs.push(
      this.salaryProgressService.getList(this.employeeId, param).subscribe((res: BaseResponse) => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.items = res.data.listData;
            this.count = res.data.count;

          }
          this.loading = false
        }, error => {
          this.toastrService.error(error.message)
          this.loading = false
          this.items = []
      })
    );
  }

  resetContextMenu() {

  }

  rightClickTable($event: MouseEvent, data: SelfSalary) {

  }

  refresh() {this.getInfo();}

  ngOnDestroy(): void {
    this.modal?.destroy();
    this.subs?.forEach(sub => sub.unsubscribe());
  }
}
