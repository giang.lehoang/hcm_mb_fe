import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelfAddDegreeInfoComponent } from './self-add-degree-info/self-add-degree-info.component';
import { NzFormModule } from 'ng-zorro-antd/form';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbDatePickerModule } from '@hcm-mfe/shared/ui/mb-date-picker';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzIconModule } from 'ng-zorro-antd/icon';
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
  imports: [CommonModule, NzFormModule, SharedUiMbSelectModule, TranslateModule, SharedUiMbDatePickerModule, ReactiveFormsModule, SharedUiMbInputTextModule, NzUploadModule, NzCheckboxModule, NzIconModule, SharedUiLoadingModule],
  declarations: [SelfAddDegreeInfoComponent],
  exports: [SelfAddDegreeInfoComponent],
})
export class SelfServiceFeatureSelfStaffInfoSelfAddDegreeInfoModule {}
