import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import {NzUploadFile} from 'ng-zorro-antd/upload';
import { saveAs } from 'file-saver';
import {
  DownloadFileAttachService,
  SelfDegreeInfoService,
  SelfShareDataService,
  SelfStaffInfoService
} from '@hcm-mfe/self-service/data-access/services';
import {Constant} from '@hcm-mfe/self-service/data-access/common';
import {BaseResponse} from '@hcm-mfe/shared/data-access/models';
import {beforeUploadFile, getTypeExport} from '@hcm-mfe/shared/common/utils';
import {
  SelfDegreeInfo,
  SelfFacultyInfo,
  SelfLookupValues,
  SelfSchoolInfo
} from '@hcm-mfe/self-service/data-access/models';
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";


@Component({
  selector: 'app-add-degree-info',
  templateUrl: './self-add-degree-info.component.html',
  styleUrls: ['./self-add-degree-info.component.scss']
})
export class SelfAddDegreeInfoComponent implements OnInit, OnDestroy {
  @Input() data!: SelfDegreeInfo;
  isSubmitted = false;
  form: FormGroup;
  subs: Subscription[] = [];
  degreeTypes: SelfLookupValues[] = [];
  eduRankCodes: SelfLookupValues[] = [];
  faculty!: SelfFacultyInfo;
  school!: SelfSchoolInfo;
  listSchool: SelfSchoolInfo[] = [];
  listFaculty: SelfFacultyInfo[] = [];
  listMajorLevel: SelfLookupValues[] = [];
  fileList: NzUploadFile[] = [];

  isFileEmpty = false;
  docIdsDelete: number[] = [];
  isLoadingPage = false;

  constructor(
    private fb: FormBuilder,
    private degreeInfoService: SelfDegreeInfoService,
    private staffInfoService: SelfStaffInfoService,
    private toastService: ToastrService,
    private translate: TranslateService,
    private shareDataService: SelfShareDataService,
    private modalRef: NzModalRef,
    private downloadFileAttachService: DownloadFileAttachService
  ) {
    this.form = this.fb.group({
      educationDegreeId: null,
      degreeTypeCode: [null, Validators.required],
      issueYear: null,
      schoolId: [null, Validators.required],
      schoolName: [null],
      facultyId: [null, Validators.required],
      facultyName: [null],
      majorLevelId: [null, Validators.required],
      eduRankCode: null,
      isHighest: null,
      note: null
    });
  }

  ngOnInit(): void {
    this.getEduRank();
    this.getDegreeType();
    this.getListSchool();
    this.getListFaculty();
    this.getListMajorLevel();
    this.pathValue();
  }

  getDegreeType() {
    this.subs.push(
      this.staffInfoService.getCatalog(Constant.CATALOGS.LOAI_HINH_DT).subscribe(res => {
        const response: BaseResponse = res;
        this.degreeTypes = response?.data;
      })
    );
  }

  beforeUpload = (file: NzUploadFile): boolean => {
    this.fileList = beforeUploadFile(file, this.fileList, 3000000, this.toastService, this.translate, 'common.notification.errorFile', '.PDF');
    this.isFileEmpty = false;
    return false;
  };

  removeFile = (file: NzUploadFile) => {
    this.docIdsDelete.push(Number(file.uid));
    const index = this.fileList.indexOf(file);
    this.fileList.splice(index, 1);
    if (this.fileList.length === 0) {
      this.isFileEmpty = true;
    }
    return false;
  };

  downloadFile = (file: NzUploadFile) => {
    if (!file.url) {
      return;
    }
    this.subs.push(
      this.downloadFileAttachService.doAbsDownloadAttachFile(Number(file.uid), file.url).subscribe({
        next: (res) => {
          const reportFile = new Blob([res], {type: getTypeExport(file.name.split(".").pop())});
          saveAs(reportFile, file.name);
        }, error: (err) => {
          this.toastService.error(err.message);
        }
      })
    );
  }

  getEduRank() {
    this.subs.push(
      this.staffInfoService.getCatalog(Constant.CATALOGS.XEP_LOAI_TOT_NGHIEP).subscribe(res => {
        const response: BaseResponse = res;
        this.eduRankCodes = response?.data;
      })
    );
  }

  getListSchool() {
    this.subs.push(
      this.staffInfoService.getListSchool().subscribe(res => {
        const response: BaseResponse = res;
        this.listSchool = response?.data;
      })
    );
  }

  getListFaculty() {
    this.subs.push(
      this.staffInfoService.getListFaculty().subscribe(res => {
        const response: BaseResponse = res;
        this.listFaculty = response?.data;
      })
    );
  }

  getListMajorLevel() {
    this.subs.push(
      this.staffInfoService.getListMajorLevel().subscribe(res => {
        const response: BaseResponse = res;
        this.listMajorLevel = response?.data;
      })
    );
  }


  pathValue() {
    if (this.data?.educationDegreeId) {
      this.getListSchool();
      this.getListFaculty();
      this.subs.push(
        this.degreeInfoService.getDegreeInfoDetail(this.data.educationDegreeId).subscribe(res => {
          const response: BaseResponse = res;
          const degreeInfo: SelfDegreeInfo = response?.data;
          this.subs.push(
            this.staffInfoService.getListFaculty().subscribe(res => {
              if (res.code === HTTP_STATUS_CODE.OK) {
                this.listFaculty = res?.data;
                this.listFaculty.forEach((data) => {
                  if (data.facultyId === degreeInfo?.facultyId) {
                    this.faculty = data;
                    this.form.controls['facultyName'].setValidators(data.isTextManual == 1 ? [Validators.required, Validators.maxLength(200)] : []);
                  }
                })
              } else {
                this.toastService.error(res?.message);
              }
            }, error => this.toastService.error(error.message))
          );

          this.subs.push(
            this.staffInfoService.getListSchool().subscribe(res => {
              if (res.code === HTTP_STATUS_CODE.OK) {
                this.listSchool = res?.data;
                this.listSchool.forEach((data) => {
                  if (data.schoolId === degreeInfo?.schoolId) {
                    this.school = data;
                    this.form.controls['schoolName'].setValidators(data.isTextManual == 1 ? [Validators.required, Validators.maxLength(200)] : []);
                  }
                })
              } else {
                this.toastService.error(res?.message);
              }
            }, error => this.toastService.error(error.message))
          );
          this.form.patchValue({
            educationDegreeId: degreeInfo?.educationDegreeId,
            issueYear: degreeInfo?.issueYear ? moment(degreeInfo?.issueYear, 'YYYY').toDate() : null,
            degreeName: degreeInfo?.degreeName,
            schoolId: degreeInfo?.schoolId,
            facultyId: degreeInfo?.facultyId,
            degreeTypeCode: degreeInfo?.degreeTypeCode,
            majorLevelId: degreeInfo?.majorLevelId,
            eduRankCode: degreeInfo?.eduRankCode,
            isRelatedJob: degreeInfo?.isRelatedJob == 1,
            isHighest: degreeInfo?.isHighest == 1,
            note: degreeInfo?.note,
            schoolName:degreeInfo?.schoolName,
            facultyName:degreeInfo?.facultyName,
          });

          // file
          if (this.data?.attachFileList && this.data?.attachFileList.length > 0) {
            this.data?.attachFileList.forEach(item => {
              this.fileList.push({
                uid: item.docId?.toString(),
                name: item.fileName,
                url: item.security,
                status: 'done'
              });
            });
            this.fileList = [...this.fileList];
          }

        })
    );
    }
  }

  save() {
    this.isSubmitted = true;
    if (this.fileList.length === 0) this.isFileEmpty = true;

    if (this.form.valid && this.fileList.length > 0) {
      this.isLoadingPage = true;
      const value = this.form.value;
      value.issueYear = value.issueYear ? +moment(value.issueYear).format('YYYY') : null;
      value.isHighest = value.isHighest ? 1 : 0;
      value.employeeId = this.shareDataService.getEmployee().employeeId;
      value.docIdsDelete = this.docIdsDelete;
      if (this.data?.educationDegreeId) {
        value.isCloneFile = 1;
      }

      const formData = new FormData();
      formData.append('data', new Blob([JSON.stringify(value)], {
        type: 'application/json'
      }));
      this.fileList.forEach((nzFile: NzUploadFile) => {
        formData.append('files', nzFile as NzSafeAny);
      });

      this.subs.push(
        this.degreeInfoService.saveDegreeInfoDraft(formData).subscribe(res => {
          if (res && res.code === 200) {
            this.toastService.success(this.translate.instant('common.notification.updateSuccess'));
            this.modalRef.close({refresh: true});
          } else {
            this.toastService.error(this.translate.instant('common.notification.updateError') + ': ' + res?.message);
            this.modalRef.close({refresh: false});
          }
          this.isLoadingPage = false;
        }, () => {
          this.toastService.error(this.translate.instant('common.notification.updateError'));
          this.modalRef.close({refresh: false});
          this.isLoadingPage = false;
        })
      );
    }
  }

  changeFaculty(event:number) {
    this.listFaculty.forEach((data) => {
      if(data.facultyId === event) {
        this.faculty = data;
        this.form.controls['facultyName'].setValue(null);
        this.form.controls['facultyName'].setValidators(data.isTextManual == 1 ? [Validators.required, Validators.maxLength(200)]: []);
      }
    })
  }

  changeSchool(event:number) {
    this.listSchool.forEach((data) => {
      if(data.schoolId === event) {
        this.school = data;
        this.form.controls['schoolName'].setValue(null);
        this.form.controls['schoolName'].setErrors(null);
        this.form.controls['schoolName'].setValidators(data.isTextManual == 1 ? [Validators.required, Validators.maxLength(200)]: []);
      }
    })
  }

  ngOnDestroy(): void {
    this.subs?.forEach(sub => sub.unsubscribe());
  }
}
