import {Component, OnDestroy, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { SelfShareDataService, SelfWorkBeforeHisService } from '@hcm-mfe/self-service/data-access/services';
import { BaseResponse } from '@hcm-mfe/shared/data-access/models';
import { HTTP_STATUS_CODE, Mode } from '@hcm-mfe/shared/common/constants';
import { SelfWorkBeforeInfo } from '@hcm-mfe/self-service/data-access/models';
import { DateValidator } from '@hcm-mfe/shared/common/validators';
import {Subscription} from "rxjs";

@Component({
  selector: 'app-add-work-his-before',
  templateUrl: './self-add-work-his-before.component.html',
  styleUrls: ['./self-add-work-his-before.component.scss'],
})
export class SelfAddWorkHisBeforeComponent implements OnInit, OnDestroy {
  mode = Mode.ADD;
  data: SelfWorkBeforeInfo = new SelfWorkBeforeInfo();

  isSubmitted = false;
  form: FormGroup;
  subs: Subscription[] = [];
  isLoadingPage = false;

  constructor(
    private fb: FormBuilder,
    private modalRef: NzModalRef,
    private toastService: ToastrService,
    private modalService: NzModalService,
    private translate: TranslateService,
    private shareDataService: SelfShareDataService,
    private workBeforeHisService: SelfWorkBeforeHisService
  ) {
    this.form = this.fb.group({
      workedOutsideId: [null],
      employeeId: [null],
      fromDate: [null, Validators.required],
      toDate: [null, Validators.required],
      organizationName: [null, [Validators.required, Validators.maxLength(200)]],
      positionName: [null, [Validators.required, Validators.maxLength(200)]],
      referPerson: [null, Validators.maxLength(200)],
      referPersonPosition: [null, Validators.maxLength(200)],
      mission: [null, Validators.maxLength(1000)],
      reasonLeave: [null, Validators.maxLength(500)],
      rewardInfo: [null, Validators.maxLength(1000)],
      decplineInfo: [null, Validators.maxLength(1000)]
    }, {
      validators: [
        DateValidator.validateRangeDate('fromDate', 'toDate', 'rangeDateError')
      ]
    });
  }

  get f() {
    return this.form.controls;
  }

  ngOnInit(): void {
    if (this.mode === Mode.EDIT)
      this.patchValueInfo();
    this.f['employeeId'].setValue(this.shareDataService.getEmployee().employeeId);
  }

  patchValueInfo() {
    this.form.patchValue(this.data);
    this.f['fromDate'].setValue(this.data.fromDate ? moment(this.data.fromDate, 'MM/YYYY').toDate() : null);
    this.f['toDate'].setValue(this.data.toDate ? moment(this.data.toDate, 'MM/YYYY').toDate() : null);
  }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      this.isLoadingPage = true;
      const request: SelfWorkBeforeInfo = this.form.value;
      request.fromDate = this.f['fromDate'].value ? moment(this.f['fromDate'].value).format('MM/YYYY') : null;
      request.toDate = this.f['toDate'].value ? moment(this.f['toDate'].value).format('MM/YYYY') : null;
      this.subs.push(
        this.workBeforeHisService.saveRecord(request).subscribe((res: BaseResponse) => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.toastService.success(this.translate.instant('common.notification.updateSuccess'));
            this.modalRef.close({ refresh: true });
          } else {
            this.toastService.error(this.translate.instant('common.notification.updateError') + ':' + res.message);
          }
          this.isLoadingPage = false;
        }, error => {
          this.toastService.error(this.translate.instant('common.notification.updateError') + ':' + error?.message);
          this.isLoadingPage = false;
        })
      )
    }
  }

  ngOnDestroy() {
    this.subs?.forEach(sub => sub.unsubscribe());
  }
}
