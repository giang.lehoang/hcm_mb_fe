import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelfAddWorkHisBeforeComponent } from './self-add-work-his-before/self-add-work-his-before.component';
import { NzFormModule } from 'ng-zorro-antd/form';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedUiMbDatePickerModule } from '@hcm-mfe/shared/ui/mb-date-picker';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
    imports: [CommonModule, NzFormModule, ReactiveFormsModule, SharedUiMbDatePickerModule, TranslateModule, SharedUiMbInputTextModule, SharedUiLoadingModule],
  declarations: [SelfAddWorkHisBeforeComponent],
  exports: [SelfAddWorkHisBeforeComponent],
})
export class SelfServiceFeatureSelfStaffInfoSelfAddWorkHisBeforeModule {}
