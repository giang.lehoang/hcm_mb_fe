import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  SelfDocumentAttachedInformationComponent
} from './self-document-attached-information/self-document-attached-information.component';
import { NzTableModule } from 'ng-zorro-antd/table';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';

@NgModule({
  imports: [CommonModule, NzTableModule, TranslateModule, SharedUiMbButtonModule],
  declarations: [SelfDocumentAttachedInformationComponent],
  exports: [SelfDocumentAttachedInformationComponent],
})
export class SelfServiceFeatureSelfStaffInfoSelfDocumentAttachedInformationModule {}
