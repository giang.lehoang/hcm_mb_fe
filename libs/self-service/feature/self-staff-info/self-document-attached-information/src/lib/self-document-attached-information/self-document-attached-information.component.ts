import {Component, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {NzModalRef, NzModalService} from 'ng-zorro-antd/modal';
import {TranslateService} from '@ngx-translate/core';
import {saveAs} from 'file-saver';
import {Pagination} from '@hcm-mfe/shared/data-access/models';
import {ValidateService} from '@hcm-mfe/shared/core';
import {
  DownloadFileAttachService,
  SelfDocumentAttachmentService,
  SelfShareDataService
} from '@hcm-mfe/self-service/data-access/services';
import {ListFileAttach, SelfEmployeeProfile} from '@hcm-mfe/self-service/data-access/models';
import {HTTP_STATUS_CODE} from '@hcm-mfe/shared/common/constants';
import {getTypeExport, StringUtils} from '@hcm-mfe/shared/common/utils';
import {ToastrService} from "ngx-toastr";

@Component({
    selector: 'app-document-attached-information',
    templateUrl: './self-document-attached-information.component.html',
    styleUrls: ['./self-document-attached-information.component.scss']
})
export class SelfDocumentAttachedInformationComponent implements OnInit {
  items: SelfEmployeeProfile[] = [];
  currentItems: SelfEmployeeProfile[] = [];
  employeeId = 1;
  loading = false;
  subs: Subscription[] = [];
  modal!: NzModalRef;
  count = 0;
  pagination = new Pagination();

  constructor(
    public validateService: ValidateService,
    private translate: TranslateService,
    private shareDataService: SelfShareDataService,
    private modalService: NzModalService,
    public documentAttachmentService: SelfDocumentAttachmentService,
    private downloadFileAttachService: DownloadFileAttachService,
    private toastrService: ToastrService,
  ) {
  }

  ngOnInit(): void {
    this.subs.push(
      this.shareDataService.employee$.subscribe(employee => {
        if (employee.employeeId)
          this.employeeId = employee.employeeId;
        this.pagination.pageNumber = 1;
        this.getInfo();
      })
    );
  }

  getInfo() {
    this.loading = true;
    let param = this.pagination.getCurrentPage();
    this.subs.push(
      this.documentAttachmentService.getDocumentAttachment(this.employeeId, param).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.currentItems = res.data.listData;
          this.items = res.data.listData;
          this.count = res.data.count;
        }
        this.loading = false;

      }, (error) => {
        this.loading = false;
        this.toastrService.error(error.message)
      })
    );
  }

    resetContextMenu() {

    }

  rightClickTable($event: MouseEvent, data: SelfEmployeeProfile) {
  }

    searchData(value: string) {
        this.currentItems = StringUtils.isNullOrEmpty(value) ? [...this.items] :
            this.items.filter(item => item.profileTypeName.startsWith(value));
    }

    refresh() {
        this.getInfo();
    }

  doDownloadAttach(file: ListFileAttach) {
    this.subs.push(
      this.downloadFileAttachService.doDownloadAttachFile(~~file.docId).subscribe(res => {
        const reportFile = new Blob([res.body], {type: getTypeExport(file.fileName.split('.').pop())});
        saveAs(reportFile, file.fileName);
      }, error => this.toastrService.error(error.message))
    );
  }

  ngOnDestroy(): void {
    this.modal?.destroy();
    this.subs?.forEach(sub => sub.unsubscribe());
  }
}
