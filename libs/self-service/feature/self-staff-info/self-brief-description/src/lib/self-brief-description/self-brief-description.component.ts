import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {Observable, Observer, Subscription} from "rxjs";
import {NzUploadFile, NzUploadXHRArgs} from 'ng-zorro-antd/upload';
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";
import {SelfPersonalInfo} from '@hcm-mfe/self-service/data-access/models';
import {SelfPersonalInfoService, SelfShareDataService} from '@hcm-mfe/self-service/data-access/services';
import {HTTP_STATUS_CODE} from "@hcm-mfe/shared/common/constants";
import {NzSafeAny} from "ng-zorro-antd/core/types";

@Component({
  selector: 'app-brief-description',
  templateUrl: './self-brief-description.component.html',
  styleUrls: ['./self-brief-description.component.scss']
})
export class SelfBriefDescriptionComponent implements OnInit, OnDestroy {
  personalInfo?: SelfPersonalInfo;
  subs: Subscription[] = [];
  employeeId = 1;
  avtBase64: string | ArrayBuffer | null = 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';
  avtIsLoading = true;
  fileAvt?: NzUploadXHRArgs | null;
  formDataAvt?: FormData | null;
  showTmp = false;

  @Output() isEmpInfoReady: EventEmitter<SelfPersonalInfo> = new EventEmitter<SelfPersonalInfo>();

  constructor(
    private shareDataService: SelfShareDataService,
    private personalInfoService: SelfPersonalInfoService,
    private toastService: ToastrService,
    private translate: TranslateService,
  ) { }

  ngOnInit(): void {
    this.subs.push(
      this.shareDataService.employee$.subscribe(employee => {
        if (employee.employeeId) {
          this.employeeId = employee.employeeId;
          this.getAvatar(this.employeeId);
        }
        this.getPersonalInfo();
      })
    );
  }

  ngOnDestroy(): void {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  getPersonalInfo() {
    this.subs.push(
      this.shareDataService.personalInfo$.subscribe(value => {
        this.personalInfo = value;
        this.isEmpInfoReady.emit(value);
      })
    );
  }

  getAvatar(employeeId: number) {
    this.avtIsLoading = true;
    this.subs.push(
      this.personalInfoService.getAvatar(employeeId).subscribe(res => {
       this.avtBase64 = res?.data ? 'data:image/jpg;base64,' + res?.data : 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';
       this.avtIsLoading = false;
      })
    );
  }

  beforeUpload = (file: NzUploadFile) => {
    return new Observable((observer: Observer<boolean>) => {
      const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
      if (!isJpgOrPng) {
        this.toastService.error(this.translate.instant('selfService.validate.fileType', {fileType: 'png, jpg'}));
        observer.complete();
        return;
      }
      observer.next(isJpgOrPng);
      observer.complete();
    });
  };

  handleUpload = (item: NzUploadXHRArgs): NzSafeAny => {
    this.avtIsLoading = true;
    const formData = new FormData();
    formData.append('fileAvatar', item.file as NzSafeAny);
    this.formDataAvt = formData;
    this.fileAvt = item;
    const reader = new FileReader();
    reader.readAsDataURL(item.file as NzSafeAny);
    reader.onload = () => {
      this.avtBase64 = reader.result;
      this.avtIsLoading = false;
      this.showTmp = true;
    };
  }

  deleteAvt() {
    this.avtIsLoading = true;
    this.subs.push(
      this.personalInfoService.deleteAvatar(this.employeeId).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.avtBase64 = 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';
          this.avtIsLoading = false;
          this.toastService.success(this.translate.instant('common.notification.updateSuccess'));
        } else {
          this.toastService.error(res?.message);
        }
      }, error => {
        this.toastService.error(error.message)
        this.avtIsLoading = false;
      })
    );
  }

  saveAvt() {
    if (this.fileAvt && this.formDataAvt) {
      this.avtIsLoading = true;
      this.subs.push(
        this.personalInfoService.uploadAvatar(this.employeeId, this.formDataAvt).subscribe(() => {
          this.getAvatar(this.employeeId);
          this.avtIsLoading = false;
          this.showTmp = false;
          this.fileAvt = null;
          this.formDataAvt = null;
        })
      );
    }
  }
}
