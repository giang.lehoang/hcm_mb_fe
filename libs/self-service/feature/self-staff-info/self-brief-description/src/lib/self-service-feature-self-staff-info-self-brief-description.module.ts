import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelfBriefDescriptionComponent } from './self-brief-description/self-brief-description.component';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { TranslateModule } from '@ngx-translate/core';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';
import { SharedUiMbTextLabelModule } from '@hcm-mfe/shared/ui/mb-text-label';
import { NzMenuModule } from 'ng-zorro-antd/menu';

@NgModule({
  imports: [CommonModule, NzFormModule, NzSpinModule, NzIconModule, SharedUiMbButtonModule, NzUploadModule, TranslateModule, NzPopconfirmModule, SharedUiMbTextLabelModule, NzMenuModule],
  declarations: [SelfBriefDescriptionComponent],
  exports: [SelfBriefDescriptionComponent],
})
export class SelfServiceFeatureSelfStaffInfoSelfBriefDescriptionModule {}
