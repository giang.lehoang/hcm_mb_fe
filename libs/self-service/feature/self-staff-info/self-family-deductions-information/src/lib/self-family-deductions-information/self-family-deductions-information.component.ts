import {NzModalRef, NzModalService} from 'ng-zorro-antd/modal';
import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";
import {BaseResponse, SelfFamilyDeductionInfo} from '@hcm-mfe/self-service/data-access/models';
import {Pagination} from '@hcm-mfe/shared/data-access/models';
import {ValidateService} from '@hcm-mfe/shared/core';
import {SelfFamilyDeductionInfoService, SelfShareDataService} from '@hcm-mfe/self-service/data-access/services';
import {HTTP_STATUS_CODE} from '@hcm-mfe/shared/common/constants';
import {Subscription} from "rxjs";

@Component({
  selector: 'app-family-deductions-information',
  templateUrl: './self-family-deductions-information.component.html',
  styleUrls: ['./self-family-deductions-information.component.scss'],
})
export class SelfFamilyDeductionsInformationComponent implements OnInit {
  items: SelfFamilyDeductionInfo[] = [];
  loading = false;
  employeeId: number | null = null;
  modal!: NzModalRef;
  count = 0;
  pagination = new Pagination();
  subs: Subscription[] = [];

  constructor(
    public validateService: ValidateService,
    private http: HttpClient,
    private toastrService: ToastrService,
    private modalService: NzModalService,
    private translate: TranslateService,
    private shareDataService: SelfShareDataService,
    private familyDeductionInfoService: SelfFamilyDeductionInfoService
  ) {
  }

  ngOnInit(): void {
    this.subs.push(
      this.shareDataService.employee$.subscribe(employee => {
        this.employeeId = employee.employeeId;
        this.pagination.pageNumber = 1;
        this.getInfo()
      })
    );
  }

  getInfo() {
    this.loading = true;
    const param = this.pagination.getCurrentPage();
    if (!this.employeeId) {
      return;
    }
    this.subs.push(
      this.familyDeductionInfoService.getList(this.employeeId, param).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.items = res.data.listData;
          this.count = res.data.count;
        }
        this.loading = false
      }, () => {
        this.loading = false
        this.items = []
      })
    );
  }

  resetContextMenu() {

  }

  rightClickTable($event: MouseEvent, data: SelfFamilyDeductionInfo) {

  }

  refresh() {
    this.getInfo()
  }

  ngOnDestroy(): void {
    this.modal?.destroy();
    this.subs?.forEach(sub => sub.unsubscribe());
  }

}
