import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  SelfFamilyDeductionsInformationComponent
} from './self-family-deductions-information/self-family-deductions-information.component';
import { TranslateModule } from '@ngx-translate/core';
import { NzTableModule } from 'ng-zorro-antd/table';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';

@NgModule({
  imports: [CommonModule, NzTableModule, TranslateModule, SharedUiMbButtonModule],
  declarations: [SelfFamilyDeductionsInformationComponent],
  exports: [SelfFamilyDeductionsInformationComponent],
})
export class SelfServiceFeatureSelfStaffInfoSelfFamilyDeductionsInformationModule {}
