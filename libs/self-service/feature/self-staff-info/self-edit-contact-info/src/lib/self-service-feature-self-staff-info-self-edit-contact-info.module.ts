import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelfEditContactInfoComponent } from './self-edit-contact-info/self-edit-contact-info.component';
import { NzFormModule } from 'ng-zorro-antd/form';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { TranslateModule } from '@ngx-translate/core';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
    imports: [CommonModule, NzFormModule, ReactiveFormsModule, SharedUiMbInputTextModule, TranslateModule, NzMenuModule, SharedUiMbSelectModule, SharedUiLoadingModule],
  declarations: [SelfEditContactInfoComponent],
  exports: [SelfEditContactInfoComponent],
})
export class SelfServiceFeatureSelfStaffInfoSelfEditContactInfoModule {}
