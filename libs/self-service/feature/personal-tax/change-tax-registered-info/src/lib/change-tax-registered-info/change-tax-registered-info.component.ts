import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NzUploadFile} from 'ng-zorro-antd/upload';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {saveAs} from 'file-saver';
import * as moment from 'moment';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {
    ObjectCategory,
    PersonalIdentity,
    PersonalInfo,
    TaxNumberRegister,
    TaxNumberRegistersResponse
} from '@hcm-mfe/self-service/data-access/models';
import {Constant, SCREEN_MODE, UrlConstant} from '@hcm-mfe/self-service/data-access/common';
import {
    DownloadFileAttachService, LookupValuesService,
    PersonalInfoService,
    TaxCommonService,
    TaxRegistersService
} from '@hcm-mfe/self-service/data-access/services';
import {HTTP_STATUS_CODE, STORAGE_NAME} from '@hcm-mfe/shared/common/constants';
import {BaseResponse, LookupValues, SelectModal, User, UserLogin} from '@hcm-mfe/shared/data-access/models';
import {StorageService} from '@hcm-mfe/shared/common/store';
import {SessionKey} from '@hcm-mfe/shared/common/enums';
import {beforeUploadFile, getTypeExport, Utils} from '@hcm-mfe/shared/common/utils';
import {NzSafeAny} from "ng-zorro-antd/core/types";


@Component({
    selector: 'app-change-tax-registered-info',
    templateUrl: './change-tax-registered-info.component.html',
    styleUrls: ['./change-tax-registered-info.component.scss']
})
export class ChangeTaxRegisteredInfoComponent implements OnInit, OnDestroy {
    form: FormGroup = this.fb.group([]);
    listPaperType: ObjectCategory[] = [];
    listNational: LookupValues[] = [];

    provinces: LookupValues[] = [];
    districts: LookupValues[] = [];
    wards: LookupValues[] = [];
    listCitizenPlace: ObjectCategory[] = [];
    listOldCitizenPlace: ObjectCategory[] = [];

    districtsCurrent: LookupValues[] = [];
    wardsCurrent: LookupValues[] = [];

    isLoadingProvinces = false;
    isLoadingDistricts = false;
    isLoadingWards = false;
    isLoadingProvincesCurrent = false;
    isLoadingDistrictsCurrent = false;
    isLoadingWardsCurrent = false;

    isSubmitted = false;
    fileList: NzUploadFile[] = [];
    docIdsDelete: number[] = [];

    employeeId: number;
    userLogin: UserLogin = new UserLogin();
    currentUser?: User;

    registerId?: number;
    citizenPaperTypeValue = Constant.PAPERS_CODE.CITIZEN_ID;
    changeTaxRegisterInfoEdit?: TaxNumberRegistersResponse;
    employeeIdentities: PersonalIdentity[] | undefined;
    subs: Subscription[] = [];
    isEdit = false;
    isLoadingPage = false;
    constant = Constant;
    isDetail = false;

    constructor(
        private fb: FormBuilder,
        private lookupValuesService: LookupValuesService,
        private toastService: ToastrService,
        private translate: TranslateService,
        private downloadFileAttachService: DownloadFileAttachService,
        private taxRegisterService: TaxRegistersService,
        private personalInfoService: PersonalInfoService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private taxCommonService: TaxCommonService,
    ) {
        this.userLogin = StorageService.get(STORAGE_NAME.USER_LOGIN);
        const currentUser = localStorage.getItem(SessionKey.CURRENCY_USER);
        if (currentUser) {
            this.currentUser = JSON.parse(currentUser ?? '');
        }
        this.employeeId = this.userLogin.employeeId ?? this.currentUser?.empId ?? 1;

        this.registerId = this.activatedRoute?.snapshot?.queryParams['registerId'];
    }

    ngOnInit(): void {
        this.getPaperTypes();
        this.getCatalogs();
        this.initForm();
        this.getIdentities();
        this.isDetail = this?.activatedRoute?.snapshot?.data['screenMode'] === SCREEN_MODE.VIEW;
        // Mode = EDIT
        if (this.registerId) {
            this.getRegisterEdit();
        }
    }

    getCatalogs() {
        this.getNational();
        this.getProvinces();
    }

    getCitizenPlace(key: string) {
        let typeCode = '';
        if (key === 'oldIdTypeCode') {
            typeCode = this.form.controls['oldIdTypeCode']?.value === this.citizenPaperTypeValue ? Constant.CATALOGS.NOI_CAP_CCCD : Constant.CATALOGS.NOI_CAP_CMND;
        } else {
            typeCode = this.form.controls['idTypeCode']?.value === this.citizenPaperTypeValue ? Constant.CATALOGS.NOI_CAP_CCCD : Constant.CATALOGS.NOI_CAP_CMND;
        }
        this.subs.push(
            this.lookupValuesService.getCatalog(typeCode).subscribe({
                next: (res: BaseResponse) => {
                    if (res.code === HTTP_STATUS_CODE.OK) {
                        if (key === 'oldIdTypeCode') {
                            this.listOldCitizenPlace = res?.data;
                        } else {
                            this.listCitizenPlace = res?.data;
                        }
                    } else {
                        this.toastService.error(res?.message);
                    }
                },
                error: (err) => {
                    this.toastService.error(err?.message);
                }
            })
        );
    }

    getNational() {
        this.subs.push(
            this.lookupValuesService.getCatalog(Constant.CATALOGS.QUOC_GIA).subscribe({
                next: (res: BaseResponse) => {
                    if (res.code === HTTP_STATUS_CODE.OK) {
                        this.listNational = res.data;
                        this.setDefaultNation(this.listNational);
                    } else {
                        this.toastService.error(res?.data);
                    }
                },
                error: (err) => {
                    this.toastService.error(err?.message);
                }
            })
        );
    }

    setDefaultNation(nations: LookupValues[]) {
        const vnModel = nations?.find(nation => nation.label === Constant.VN_NATION);
        if (vnModel) {
            this.form.controls['permanentNationCode'].setValue(vnModel?.value);
            this.form.controls['currentNationCode'].setValue(vnModel?.value);
        }
    }

    private getRegisterEdit() {
        if (this.registerId) {
            const commonTaxApi = this.taxCommonService.getTaxRegisterById(this.registerId, Constant.REG_TYPE.ALTER_TAX_INFO);
            this.isLoadingPage = true;
            this.subs.push(
                commonTaxApi.subscribe({
                    next: (res) => {
                        if (res.code === HTTP_STATUS_CODE.OK) {
                            this.changeTaxRegisterInfoEdit = res?.data;
                            this.patchValueToForm();
                        } else {
                            this.toastService.error(res?.data);
                        }
                        this.isLoadingPage = false;
                    },
                    error: (err) => {
                        this.isLoadingPage = false;
                        this.toastService.error(err?.message);
                    }
                })
            );
        }
    }

    private patchValueToForm() {
        this.form.controls['oldIdTypeCode'].setValue(this?.changeTaxRegisterInfoEdit?.oldIdTypeCode);

        this.form.controls['idTypeCode'].setValue(this?.changeTaxRegisterInfoEdit?.idTypeCode);
        this.form.controls['idNo'].setValue(this?.changeTaxRegisterInfoEdit?.idNo);
        this.form.controls['idDate'].setValue(Utils.convertDateToFillForm(this.changeTaxRegisterInfoEdit?.idDate));
        this.form.controls['idPlace'].setValue(this?.changeTaxRegisterInfoEdit?.idPlace);

        this.form.controls['permanentProvinceCode'].setValue(this?.changeTaxRegisterInfoEdit?.permanentProvinceCode);
        this.form.controls['permanentDistrictCode'].setValue(this?.changeTaxRegisterInfoEdit?.permanentDistrictCode);
        this.form.controls['permanentWardCode'].setValue(this?.changeTaxRegisterInfoEdit?.permanentWardCode);
        this.form.controls['permanentDetail'].setValue(this?.changeTaxRegisterInfoEdit?.permanentDetail);
        this.form.controls['currentProvinceCode'].setValue(this?.changeTaxRegisterInfoEdit?.currentProvinceCode);
        this.form.controls['currentDistrictCode'].setValue(this?.changeTaxRegisterInfoEdit?.currentDistrictCode);
        this.form.controls['currentWardCode'].setValue(this?.changeTaxRegisterInfoEdit?.currentWardCode);
        this.form.controls['currentDetail'].setValue(this?.changeTaxRegisterInfoEdit?.currentDetail);

        this.form.controls['note'].setValue(this?.changeTaxRegisterInfoEdit?.note ? this?.changeTaxRegisterInfoEdit?.note : null);
        this.form.controls['mobileNumber'].setValue(this?.changeTaxRegisterInfoEdit?.mobileNumber);
        this.form.controls['email'].setValue(this?.changeTaxRegisterInfoEdit?.email);
        this.form.controls['rejectReason'].setValue(this?.changeTaxRegisterInfoEdit?.rejectReason);
        this.form.controls['taxPlace'].setValue(this?.changeTaxRegisterInfoEdit?.taxPlace);

        // file
        if (this?.changeTaxRegisterInfoEdit?.attachFileList && this.changeTaxRegisterInfoEdit?.attachFileList.length > 0) {
            this?.changeTaxRegisterInfoEdit?.attachFileList.forEach((item) => {
                this.fileList.push({
                    uid: item.docId.toString(),
                    name: item.fileName,
                    thumbUrl: item.security,
                    status: 'done'
                });
            });
            this.fileList = [...this.fileList];
        }
        this.isEdit = true;
        if (this.isDetail) {
          this.form.disable();
        }
    }

    getEmployeeInfo(event: PersonalInfo) {
        this.setTaxInfo(event);
        this.setPhoneAndEmailInfo(event);
    }

    setPhoneAndEmailInfo(employeeInfo: PersonalInfo) {
        if (!this.isEdit) {
          this.form.controls['mobileNumber'].setValue(employeeInfo?.mobileNumber);
          this.form.controls['email'].setValue(employeeInfo?.email);
        }
        this.isEdit = false;
    }

    setTaxInfo(employeeInfo: PersonalInfo) {
        this.form.controls['taxNo'].setValue(employeeInfo?.taxNo);
        if (!this.registerId) {
            this.form.controls['taxPlace'].setValue(employeeInfo?.taxPlace);
        }
    }

    getPaperTypes() {
        this.subs.push(
            this.lookupValuesService.getCatalogModuleTax(Constant.CATALOGS.LOAI_GIAY_TO, Constant.CATALOGS.TEN_CQT).subscribe(res => {
                if (res.code === HTTP_STATUS_CODE.OK) {
                  this.listPaperType = res.data;
                }
            }, error => this.toastService.error(error.message))
        );
    }

    getIdentities() {
        this.subs.push(
            this.personalInfoService.getIdentities(this.employeeId).subscribe({
                next: (res: BaseResponse) => {
                    if (res.code === HTTP_STATUS_CODE.OK) {
                        this.employeeIdentities = res?.data;
                    } else {
                        this.toastService.error(res?.message);
                    }
                },
                error: (err) => {
                    this.toastService.error(err?.message);
                }
            })
        );
    }

    initForm() {
        this.form = this.fb.group({
            taxNo: [{value: '', disabled: true}],
            taxPlace: [null],

            oldIdTypeCode: ['', [Validators.required]],
            oldIdNo: [null, [Validators.required]],
            oldIdDate: [null, [Validators.required]],
            oldIdPlace: [null],
            oldIdPlaceCode: [null],

            idTypeCode: ['', [Validators.required]],
            idNo: [null, [Validators.required]],
            idDate: [null],
            idPlaceCode: [null],
            idPlace: [null],

            permanentNationCode: [{value: null}, [Validators.required]],
            permanentProvinceCode: [null, [Validators.required]],
            permanentDistrictCode: [null, [Validators.required]],
            permanentWardCode: [null, [Validators.required]],
            permanentDetail: [null, [Validators.required]],
            currentNationCode: [{value: null}, [Validators.required]],
            currentProvinceCode: [null, [Validators.required]],
            currentDistrictCode: [null, [Validators.required]],
            currentWardCode: [null, [Validators.required]],
            currentDetail: [null, [Validators.required]],

            note: [null],
            rejectReason: [null],
            mobileNumber: [null, [Validators.required, Validators.pattern('^[0-9]{10}$')]],
            email: [null, [Validators.required, Validators.email]],
        });
    }

    onOldPaperTypeChange(event: SelectModal, key: string) {
        this.getCitizenPlace(key);
        const paperType = event?.itemSelected?.value;
        this.addOldIdNoValidator(paperType);
        let identity;
        if (paperType === Constant.PAPERS_CODE.ID_NO) {
            identity = this.employeeIdentities?.find(item => item.idTypeCode ? item.idTypeCode === paperType : item.idNo?.length === 9);
        } else if (paperType === Constant.PAPERS_CODE.CITIZEN_ID) {
            identity = this.employeeIdentities?.find(item => item.idTypeCode ? item.idTypeCode === paperType : item.idNo?.length === 12);
        } else if(paperType) {
            identity = this.employeeIdentities?.find(item => item.idTypeCode ? item.idTypeCode === paperType : (item.idNo?.length !== 12 && item.idNo?.length !== 9));
        }
        if (!this.registerId) { // MODE = ADD
            if (identity) {
                this.form.controls['oldIdNo'].setValue(identity?.idNo);
                this.form.controls['oldIdDate'].setValue(Utils.convertDateToFillForm(identity?.idIssueDate));
                this.form.controls['oldIdPlaceCode'].setValue(identity?.idIssuePlace);
                this.form.controls['oldIdPlace'].setValue(identity?.idIssuePlace);
            } else {
                this.form.controls['oldIdNo'].reset();
                this.form.controls['oldIdDate'].reset();
                this.form.controls['oldIdPlaceCode'].reset();
                this.form.controls['oldIdPlace'].reset();
            }
        } else { // MODE = EDIT
            if (paperType === this.changeTaxRegisterInfoEdit?.oldIdTypeCode) {
                this.form.controls['oldIdNo'].setValue(this?.changeTaxRegisterInfoEdit?.oldIdNo);
                this.form.controls['oldIdDate'].setValue(Utils.convertDateToFillForm(this?.changeTaxRegisterInfoEdit?.oldIdDate));
                this.form.controls['oldIdPlaceCode'].setValue(this?.changeTaxRegisterInfoEdit?.oldIdPlaceCode);
                this.form.controls['oldIdPlace'].setValue(this?.changeTaxRegisterInfoEdit?.oldIdPlace);
            } else if (identity) {
                this.form.controls['oldIdNo'].setValue(identity?.idNo);
                this.form.controls['oldIdDate'].setValue(Utils.convertDateToFillForm(identity?.idIssueDate));
                this.form.controls['oldIdPlace'].setValue(identity?.idIssuePlace);
                this.form.controls['oldIdPlaceCode'].setValue(identity?.idIssuePlace);
            } else {
                this.form.controls['oldIdNo'].reset();
                this.form.controls['oldIdDate'].reset();
                this.form.controls['oldIdPlace'].reset();
                this.form.controls['oldIdPlaceCode'].reset();
            }
        }

    }

    onPaperTypeChange(event: SelectModal, key: string) {
        this.getCitizenPlace(key);
        const paperType = event?.itemSelected?.value;
        this.addIdNoValidator(paperType);

        if (this.registerId) { // MODE = EDIT
            if (paperType === this.changeTaxRegisterInfoEdit?.idTypeCode) {
                this.form.controls['idNo'].setValue(this.changeTaxRegisterInfoEdit?.idNo);
                this.form.controls['idDate'].setValue(Utils.convertDateToFillForm(this?.changeTaxRegisterInfoEdit?.idDate));
                this.form.controls['idPlace'].setValue(this.changeTaxRegisterInfoEdit?.idPlace);
                this.form.controls['idPlaceCode'].setValue(this.changeTaxRegisterInfoEdit?.idPlaceCode);
            } else {
                this.form.controls['idNo'].reset();
                this.form.controls['idDate'].reset();
                this.form.controls['idPlace'].reset();
                this.form.controls['idPlaceCode'].reset();
            }
        }

    }

    beforeUpload = (file: NzUploadFile): boolean => {
        const maxSize: number = 5*1024*1024;
        if (this.fileList.length < 4) {
          this.fileList = beforeUploadFile(file, this.fileList, maxSize, this.toastService, this.translate, 'common.notification.fileExtensionInvalidTaxRegister', '.TIF', '.JPG', '.GIF', '.PNG', '.PDF', '.DOC', '.DOCX', '.XLS', '.XLSX')
        } else {
          this.toastService.error(this.translate.instant('selfService.notification.maxFile'));
        }
        return false;
    }

    downloadFile = (file: NzUploadFile) => {
        if (file.thumbUrl) {
            this.isLoadingPage = true;
            const sub = this.downloadFileAttachService.doTaxDownloadAttachFile(Number(file.uid), file.thumbUrl).subscribe({
                next: (res) => {
                    this.isLoadingPage = false;
                    const reportFile = new Blob([res], {type: getTypeExport(file.name.split('.').pop())});
                    saveAs(reportFile, file.name);
                }, error: () => {
                    this.isLoadingPage = false;
                    this.toastService.error(this.translate.instant('common.notification.downloadFilePermissionDeny'));
                }
            });
            this.subs.push(sub);
        }
    }

    removeFile = (file: NzUploadFile): boolean => {
        this.docIdsDelete.push(Number(file.uid));
        const index = this.fileList.indexOf(file);
        this.fileList.splice(index, 1);
        return false;
    };

    getProvinces() {
        this.subs.push(
            this.lookupValuesService.getCatalog(Constant.CATALOGS.TINH).subscribe({
                next: (res: BaseResponse) => {
                    if (res.code === HTTP_STATUS_CODE.OK) {
                        this.provinces = res.data;
                        this.isLoadingProvinces = false;
                        this.isLoadingProvincesCurrent = false;
                    } else {
                        this.toastService.error(res?.data);
                    }
                },
                error: (err) => {
                    this.toastService.error(err?.message);
                }
            })
        );
    }

    changeProvince($event: SelectModal, type: 'HK' | 'HT') {
        if ($event?.itemSelected) {
            if (type === 'HK' && $event?.itemSelected?.value !== this?.changeTaxRegisterInfoEdit?.permanentProvinceCode) {
                this.form.controls['permanentDistrictCode'].reset();
                this.form.controls['permanentWardCode'].reset();
            }
            if (type === 'HT' && $event?.itemSelected?.value !== this?.changeTaxRegisterInfoEdit?.currentProvinceCode) {
                this.form.controls['currentDistrictCode'].reset();
                this.form.controls['currentWardCode'].reset();
            }
            this.getDistricts($event?.itemSelected?.value, type);
        }
    }

    changeDistrict($event: SelectModal, type: 'HK' | 'HT') {
        if ($event?.itemSelected) {
            if (type === 'HK' && $event?.itemSelected?.value !== this?.changeTaxRegisterInfoEdit?.permanentDistrictCode) {
                this.form.controls['permanentWardCode'].reset();
            }
            if (type === 'HT' && $event?.itemSelected?.value !== this?.changeTaxRegisterInfoEdit?.currentDistrictCode) {
                this.form.controls['currentWardCode'].reset();
            }
            this.getWards($event?.itemSelected?.value, type);
        }
    }

    getDistricts(parentCode?: number, type: 'HK' | 'HT' = 'HK') {
        if (parentCode) {
            if (type === 'HK') {
                this.isLoadingDistricts = true;
            } else {
                this.isLoadingDistrictsCurrent = true;
            }
            this.subs.push(
                this.lookupValuesService.getCatalog(Constant.CATALOGS.HUYEN, parentCode).subscribe({
                    next: (res: BaseResponse) => {
                        if (res.code === HTTP_STATUS_CODE.OK) {
                            if (type === 'HK') {
                                this.districts = res.data;
                                this.isLoadingDistricts = false;
                            } else {
                                this.districtsCurrent = res.data;
                                this.isLoadingDistrictsCurrent = false;
                            }
                        } else {
                            this.toastService.error(res?.data);
                        }
                    },
                    error: (err) => {
                        this.toastService.error(err?.message);
                    }
                })
            );
        }
    }

    getWards(parentCode?: number, type: 'HK' | 'HT' = 'HK') {
        if (parentCode) {
            if (type === 'HK') {
                this.isLoadingWards = true;
            } else {
                this.isLoadingWardsCurrent = true;
            }
            this.subs.push(
                this.lookupValuesService.getCatalog(Constant.CATALOGS.XA, parentCode).subscribe({
                    next: (res: BaseResponse) => {
                        if (res.code === HTTP_STATUS_CODE.OK) {
                            if (type === 'HK') {
                                this.wards = res.data;
                                this.isLoadingWards = false;
                            } else {
                                this.wardsCurrent = res.data;
                                this.isLoadingWardsCurrent = false;
                            }
                        } else {
                            this.toastService.error(res?.data);
                        }
                    },
                    error: (err) => {
                        this.toastService.error(err?.message);
                    }
                })
            );
        }
    }

    validateIdNo() {
        const oldIdTypeCode = this.form.controls['oldIdTypeCode'].value;
        const oldIdNo = this.form.controls['oldIdNo'].value;

        const idTypeCode = this.form.controls['idTypeCode'].value;
        const idNo = this.form.controls['idNo'].value;

        // so giay to cu
        if (oldIdTypeCode && oldIdTypeCode === Constant.PAPERS_CODE.ID_NO) { // cmt
            if (oldIdNo && oldIdNo.length !== 9) {
                this.form.controls['oldIdNo'].setErrors({idNoOnlyAccept9Number : true});
            } else {
                this.form.controls['oldIdNo'].setErrors(null);
                this.form.controls['oldIdNo'].setValidators(Validators.required);
            }
        } else if (oldIdTypeCode && oldIdTypeCode === Constant.PAPERS_CODE.CITIZEN_ID) { // cccd
            if (oldIdNo && oldIdNo.length !== 12) {
                this.form.controls['oldIdNo'].setErrors({citizenOnlyAccept12Number : true});
            } else {
                this.form.controls['oldIdNo'].setErrors(null);
                this.form.controls['oldIdNo'].setValidators(Validators.required);
            }
        }

        // so giay to moi
        if (idTypeCode && idTypeCode === Constant.PAPERS_CODE.ID_NO) { // cmt
            if (idNo && idNo.length !== 9) {
                this.form.controls['idNo'].setErrors({idNoOnlyAccept9Number : true});
            } else {
                this.form.controls['idNo'].setErrors(null);
                this.form.controls['idNo'].setValidators(Validators.required);
            }
        } else if (idTypeCode && idTypeCode === Constant.PAPERS_CODE.CITIZEN_ID) { // cccd
            if (idNo && idNo.length !== 12) {
                this.form.controls['idNo'].setErrors({citizenOnlyAccept12Number : true});
            } else {
                this.form.controls['idNo'].setErrors(null);
                this.form.controls['idNo'].setValidators(Validators.required);
            }
        }

    }

    onSave(actionType: 'SAVE' | 'SEND_BROWSE') {
        this.isSubmitted = true;
        this.processFormValue();
        this.validateIdNo();
        if (this.form.valid && this.fileList?.length > 0) {
            const request: TaxNumberRegister = this.form.value;

            request.employeeId = this.employeeId;
            request.regType = Constant.REQUEST_TYPES[1].value;
            request.oldIdDate = moment(this.form.controls['oldIdDate'].value).format('DD/MM/YYYY');
            request.idDate = moment(this.form.controls['idDate'].value).format('DD/MM/YYYY');
            request.taxNumberRegisterId = this.registerId ? this.registerId : null;

            request.docIdsDelete = this.docIdsDelete;

            if (actionType === 'SAVE') {
                request.status = Constant.SAVE_DRAFT;
            } else {
                request.status = Constant.SEND_BROWSE;
            }

            const formData = new FormData();
            formData.append('data', new Blob([JSON.stringify(request)], {
                type: 'application/json'
            }));

            this.fileList.forEach((nzFile: NzSafeAny) => {
                formData.append('files', nzFile);
            });
            this.isLoadingPage = true;
            const sub = this.taxRegisterService.saveRecord(formData).subscribe((res: BaseResponse) => {
                this.isLoadingPage = false;
                if (res.code === HTTP_STATUS_CODE.OK) {
                    if (actionType === 'SEND_BROWSE') {
                        this.toastService.success(this.translate.instant('common.notification.sendBrowseSuccess2'));
                    } else {
                        const notification = this.registerId ? 'common.notification.editSuccess' : 'common.notification.addSuccess'
                        this.toastService.success(this.translate.instant(notification));
                    }
                    this.router.navigateByUrl(UrlConstant.URL.TAX_REGISTER_PAGE).then();
                } else if (res.code === HTTP_STATUS_CODE.CREATED) {
                    this.toastService.error(res.message);
                }
            }, error => {
                this.isLoadingPage = false;
                this.toastService.error(error?.message);
            });
            this.subs.push(sub);
        }
    }

    onBack() {
        this.router.navigateByUrl(UrlConstant.URL.TAX_REGISTER_PAGE).then();
    }

    private processFormValue() {
        this.form.controls['oldIdNo'].setValue(this.form.controls['oldIdNo'].value?.trim());
        this.form.controls['oldIdPlace'].setValue(this.form.controls['oldIdPlace'].value?.trim());
        this.form.controls['idNo'].setValue(this.form.controls['idNo'].value?.trim());
        this.form.controls['idPlace'].setValue(this.form.controls['idPlace'].value?.trim());
        this.form.controls['note'].setValue(this.form.controls['note'].value?.trim());
        this.form.controls['email'].setValue(this.form.controls['email'].value?.trim());
    }

    onChangeIssueDate(event: Date, key: string) {
      if (event) {
        const nowDate = new Date();
        if (event >= nowDate) {
          this.form.controls[key].setErrors({errorDate: true});
        } else {
          this.form.controls[key].setErrors(null);
        }
      } else {
        this.form.controls[key].setErrors(null);
      }
    }

    addOldIdNoValidator(paperType: string) {
        switch (paperType) {
            case Constant.PAPERS_CODE.ID_NO:
            case Constant.PAPERS_CODE.CITIZEN_ID:
                this.form.controls['oldIdPlaceCode'].setValidators(Validators.required);
                this.form.controls['oldIdPlace'].setValidators(null);
                break;
            default :
                this.form.controls['oldIdPlaceCode'].setValidators(null);
                this.form.controls['oldIdPlace'].setValidators(Validators.required);
                break;
        }
    }

    addIdNoValidator(paperType: string) {
        switch (paperType) {
            case Constant.PAPERS_CODE.ID_NO:
            case Constant.PAPERS_CODE.CITIZEN_ID:
                this.form.controls['idPlaceCode'].setValidators(Validators.required);
                this.form.controls['idPlace'].setValidators(null);
                break;
            default :
                this.form.controls['idPlaceCode'].setValidators(null);
                this.form.controls['idPlace'].setValidators(Validators.required);
                break;
        }
    }

    selectOldIdPlace(event: SelectModal) {
        this.form.controls['oldIdPlace'].setValue(event?.itemSelected?.label);
    }

    selectIdPlace(event: SelectModal) {
        this.form.controls['idPlace'].setValue(event?.itemSelected?.label);
    }

    ngOnDestroy(): void {
        this.subs?.forEach(sub => sub.unsubscribe());
    }

}
