import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChangeTaxRegisteredInfoComponent } from './change-tax-registered-info/change-tax-registered-info.component';
import { NzFormModule } from 'ng-zorro-antd/form';
import { ReactiveFormsModule } from '@angular/forms';
import { SelfServiceUiPersonalInformationModule } from '@hcm-mfe/self-service/ui/personal-information';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { SharedUiMbDatePickerModule } from '@hcm-mfe/shared/ui/mb-date-picker';
import { SharedDirectivesNumberInputModule } from '@hcm-mfe/shared/directives/number-input';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { RouterModule } from '@angular/router';
import {NzIconModule} from "ng-zorro-antd/icon";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
    imports: [CommonModule, NzFormModule, ReactiveFormsModule, SelfServiceUiPersonalInformationModule, TranslateModule,
        SharedUiMbInputTextModule, NzDividerModule, SharedUiMbSelectModule, SharedUiMbDatePickerModule, SharedDirectivesNumberInputModule,
        NzUploadModule, SharedUiMbButtonModule, NzButtonModule,
        RouterModule.forChild([
            {
                path: '',
                component: ChangeTaxRegisteredInfoComponent
            }
        ]), NzIconModule, SharedUiLoadingModule
    ],
  declarations: [ChangeTaxRegisteredInfoComponent],
  exports: [ChangeTaxRegisteredInfoComponent],
})
export class SelfServiceFeaturePersonalTaxChangeTaxRegisteredInfoModule {}
