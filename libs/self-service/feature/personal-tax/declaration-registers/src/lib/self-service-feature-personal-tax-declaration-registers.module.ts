import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeclarationRegistersComponent } from './declaration-registers/declaration-registers.component';
import { SelfServiceUiPersonalInformationModule } from '@hcm-mfe/self-service/ui/personal-information';
import { NzFormModule } from 'ng-zorro-antd/form';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { TranslateModule } from '@ngx-translate/core';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { RouterModule } from '@angular/router';
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
    imports: [CommonModule, SelfServiceUiPersonalInformationModule, NzFormModule, ReactiveFormsModule, SharedUiMbSelectModule,
        TranslateModule, NzRadioModule, NzSwitchModule, SharedUiMbInputTextModule, SharedUiMbButtonModule, NzButtonModule,
        RouterModule.forChild([
            {
                path: '',
                component: DeclarationRegistersComponent
            }
        ]), SharedUiLoadingModule
    ],
  declarations: [DeclarationRegistersComponent],
  exports: [DeclarationRegistersComponent],
})
export class SelfServiceFeaturePersonalTaxDeclarationRegistersModule {}
