import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";
import {ActivatedRoute, Router} from "@angular/router";
import * as moment from "moment";
import {Subscription} from "rxjs";
import {Constant, SCREEN_MODE, UrlConstant} from '@hcm-mfe/self-service/data-access/common';
import {SelectModal, User, UserLogin} from '@hcm-mfe/shared/data-access/models';
import {
  DeclarationRegister,
  DeclarationYear,
  LockRegistrationResponse, PersonalInfo
} from '@hcm-mfe/self-service/data-access/models';
import { DeclarationService, TaxCommonService } from '@hcm-mfe/self-service/data-access/services';
import { HTTP_STATUS_CODE, STORAGE_NAME } from '@hcm-mfe/shared/common/constants';
import { StorageService } from '@hcm-mfe/shared/common/store';
import { SessionKey } from '@hcm-mfe/shared/common/enums';
import {NzSafeAny} from "ng-zorro-antd/core/types";

@Component({
  selector: 'app-declaration-registers',
  templateUrl: './declaration-registers.component.html',
  styleUrls: ['./declaration-registers.component.scss']
})
export class DeclarationRegistersComponent implements OnInit, OnDestroy {
  form: FormGroup = this.fb.group([]);
  isSubmitted = false;
  isLoadingPage = false;
  isDetail = false;
  constant = Constant;
  employeeId?: number;
  userLogin: UserLogin = new UserLogin();
  currentUser?: User;
  registerId?: number;
  listYear: DeclarationYear[] = [];
  listAllYear: DeclarationYear[] = [];
  currentYear = new Date().getFullYear() - 1;
  employeeLoginInfo?: PersonalInfo;
  subs: Subscription[] = [];
  options = [
    { label: this.translate.instant('selfService.declarationRegisters.authorization'), value: Constant.IS_AUTHORIZATION },
    { label: this.translate.instant('selfService.declarationRegisters.selfSettlement'), value: Constant.IS_NOT_AUTHORIZATION },
  ]

  constructor(
    private fb: FormBuilder,
    private toastService: ToastrService,
    private translate: TranslateService,
    private declarationService: DeclarationService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private taxCommonService: TaxCommonService
  ) {}

  ngOnInit(): void {
    this.initForm();
    this.getEmployeeId();
    this.getListYear();
    this.registerId = this.activatedRoute.snapshot.queryParams['registerId'];
    this.isDetail = this?.activatedRoute?.snapshot?.data['screenMode'] === SCREEN_MODE.VIEW;
    if(this.registerId) {
      this.pathValueForm();
    }
  }

  initForm() {
    this.form = this.fb.group({
      year: [null, Validators.required],
      isCommit: true,
      note: null,
      methodCode: Constant.IS_AUTHORIZATION,
      isRegisterReceive: false,
      declarationRegisterId: null
    })
  }

  pathValueForm() {
    if (this.registerId) {
      this.isLoadingPage = true;
      this.subs.push(
        this.taxCommonService.getTaxRegisterById(this.registerId, Constant.REG_TYPE.REGISTER_TAX_SETTLEMENT).subscribe(res => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            const yearInfo: DeclarationYear | undefined = this.listAllYear.find(item => item.year === res.data.year);
            if (yearInfo && !this.listYear.map(item => item.year)?.includes(yearInfo.year)) {
              yearInfo.value = yearInfo.year;
              yearInfo.label = yearInfo.year?.toString();
              this.listYear.push(yearInfo)
            }
            const data: DeclarationRegister = res.data;
            this.form.patchValue(data);
            this.form.controls['isRegisterReceive'].setValue(data.revInvoice === Constant.REV_INVOICE_STATUS.IS_REV_INVOICE);
            if (this.isDetail) {
              this.form.disable();
            }
          }
          this.isLoadingPage = false;
        }, error => {
          this.isLoadingPage = false;
          this.toastService.error(error?.message)
        })
      );
    }
  }

  getListYear() {
    this.subs.push(
      this.declarationService.getListYear(Constant.REGISTRATION_TYPE.DECLARATION_REGISTER).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listAllYear = res.data;
          this.listYear = res.data.reduce((accumulator: NzSafeAny[],item: NzSafeAny) => {
            const currentDate = moment(moment(new Date()).format('DD/MM/YYYY'), 'DD/MM/YYYY').toDate();
            const fromDate = moment(item.fromDate, 'DD/MM/YYYY').toDate();
            const toDate = moment(item.toDate, 'DD/MM/YYYY').toDate();
            if (currentDate >= fromDate && currentDate <= toDate) {
              item.value = item.year;
              item.label = item.year.toString();
              accumulator.push(item)
            }
            return accumulator;
          },[]);
        }
      }, error => {
        this.toastService.error(error?.message);
      })
    );
  }

  changeDeclarationYear(event: SelectModal) {
    if (event.itemSelected) {
      const currentDate = moment(moment(new Date()).format('DD/MM/YYYY'), 'DD/MM/YYYY').toDate();
      const fromDate = moment(event.itemSelected.fromDate, 'DD/MM/YYYY').toDate();
      const toDate = moment(event.itemSelected.toDate, 'DD/MM/YYYY').toDate();
      if (currentDate < fromDate || currentDate > toDate) {
        this.form.setErrors({errorDate: true})
      } else {
        this.form.setErrors(null);
      }
    }
  }

  getEmployeeId() {
    this.userLogin = StorageService.get(STORAGE_NAME.USER_LOGIN);
    const currentUser = localStorage.getItem(SessionKey.CURRENCY_USER);
    if (currentUser) {
        this.currentUser = JSON.parse(currentUser ?? '');
    }
    this.employeeId = this.userLogin.employeeId ?? this.currentUser?.empId ?? 1;
  }


  onSave() {
    if (this.employeeLoginInfo) {
      if (!this.employeeLoginInfo.taxNo) {
        this.toastService.error(this.translate.instant("selfService.validate.userLoginNotHaveTaxNo"));
        return;
      }
    }
    this.isSubmitted = true;
    if (this.form.errors && this.form.errors['errorDate']) {
      this.toastService.error(this.translate.instant('selfService.notification.errorDeclarationDate'));
      return;
    }
    if (this.form.valid) {
      const data: DeclarationRegister = this.form.value;
      data.employeeId = this.employeeId;
      data.status = Constant.STATUS_OBJ.IS_DECLARING;
      data.regType = Constant.REG_TYPE.REGISTER_TAX_SETTLEMENT;
      data.revInvoice = this.form.controls['isRegisterReceive'].value ? Constant.REV_INVOICE_STATUS.IS_REV_INVOICE : Constant.REV_INVOICE_STATUS.IS_NOT_REV_INVOICE;
      this.isLoadingPage = true;
      this.subs.push(
        this.declarationService.saveData(data).subscribe(res => {
          this.isLoadingPage = false;
          if (res && res.code === HTTP_STATUS_CODE.OK) {
            const notification = this.registerId ? 'common.notification.editSuccess' : 'common.notification.addSuccess'
            this.toastService.success(this.translate.instant(notification));
            this.router.navigateByUrl(UrlConstant.URL.TAX_REGISTER_PAGE).then();
          } else {
            this.toastService.error(this.translate.instant('common.notification.updateError') + ': ' + res.message);
          }
        }, error => {
          this.isLoadingPage = false;
          this.toastService.error(error?.message);
        })
      );
    }
  }

  changeMethodCode(event: string) {
    if (event === Constant.IS_AUTHORIZATION) {
      this.form.controls['isRegisterReceive'].setValue(false);
    }
    if (event === Constant.IS_NOT_AUTHORIZATION) {
      this.form.controls['isRegisterReceive'].setValue(true);
    }
  }

  getEmployeeInfo(event: PersonalInfo) {
    this.employeeLoginInfo = event;
  }

  ngOnDestroy(): void {
    this.subs?.forEach(sub => sub.unsubscribe());
  }

    onBack() {
        this.router.navigateByUrl(UrlConstant.URL.TAX_REGISTER_PAGE).then();
    }

}
