import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {HttpParams} from '@angular/common/http';
import {Subscription} from 'rxjs';
import {Constant, SCREEN_MODE, UrlConstant} from '@hcm-mfe/self-service/data-access/common';
import {PersonalInfo, User, UserLogin} from '@hcm-mfe/shared/data-access/models';
import {
  DeclarationService,
  InvoiceRequestService,
  TaxCommonService
} from '@hcm-mfe/self-service/data-access/services';
import {HTTP_STATUS_CODE, STORAGE_NAME} from '@hcm-mfe/shared/common/constants';
import {StorageService} from '@hcm-mfe/shared/common/store';
import {SessionKey} from '@hcm-mfe/shared/common/enums';
import {
  DeclarationRegister,
  DeclarationYear,
  LockRegistrationResponse
} from '@hcm-mfe/self-service/data-access/models';

@Component({
  selector: 'app-receive-invoices',
  templateUrl: './receive-invoices.component.html',
  styleUrls: ['./receive-invoices.component.scss']
})
export class ReceiveInvoicesComponent implements OnInit, OnDestroy {
  form: FormGroup = this.fb.group([]);
  constant = Constant;
  userLogin: UserLogin = new UserLogin();
  currentUser?: User;
  employeeId?: number;
  employeeCode?: string;
  registerId?: number;
  email?: string;
  listYear: DeclarationYear[] = [];
  isSubmitted = false;
  disableBtnSave = false;
  employeeLoginInfo?: PersonalInfo;
  subscriptions: Subscription[] = [];
  isLoadingPage = false;
  isDetail = false;
  isEdit = false;

  constructor(
    private fb: FormBuilder,
    private declarationService: DeclarationService,
    private invoiceRequestService: InvoiceRequestService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private taxCommonService: TaxCommonService,
    private toastService: ToastrService,
    private translate: TranslateService
  ) {
  }

  ngOnInit(): void {
    this.initForm();
    this.getEmployeeId();
    this.getListYear();
    this.isDetail = this?.activatedRoute?.snapshot?.data['screenMode'] === SCREEN_MODE.VIEW;
    this.registerId = this.activatedRoute.snapshot.queryParams['registerId'];
    if (this.registerId) {
      this.pathValueForm();
    }
  }

  initForm() {
    this.form = this.fb.group({
      year: [null, Validators.required],
      note: null,
      declarationRegisterId: null,
      email: null
    });
  }

  pathValueForm() {
    if (this.registerId) {
      this.isLoadingPage = true;
      this.subscriptions.push(
        this.taxCommonService.getTaxRegisterById(this.registerId, Constant.REG_TYPE.REGISTER_RECEIVE_RECEIPT).subscribe(res => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            const data: DeclarationRegister = res.data;
            this.form.patchValue(data);
            if (this.isDetail) {
              this.form.disable();
            }
          }
          this.isLoadingPage = false;
          this.isEdit = true;
        }, error => {
          this.isLoadingPage = false;
          this.toastService.error(error?.message);
        })
      );
    }
  }

  getListYear() {
    this.subscriptions.push(
      this.declarationService.getListYear(Constant.REGISTRATION_TYPE.DECLARATION_REGISTER).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.listYear = res.data.map((item: LockRegistrationResponse) => {
            item.value = item.year;
            item.label = item.year?.toString();
            return item;
          });
        }
      }, error => {
        this.toastService.error(error?.message);
      })
    );
  }

  getEmployeeId() {
    this.userLogin = StorageService.get(STORAGE_NAME.USER_LOGIN);
    const currentUser = localStorage.getItem(SessionKey.CURRENCY_USER);
    if (currentUser) {
      this.currentUser = JSON.parse(currentUser ?? '');
    }
    this.employeeId = this.userLogin.employeeId ?? this.currentUser?.empId ?? 1;
  }

  selectYear(year: number) {
    if (year) {
      const params = new HttpParams().set('employeeId', this.employeeId ?? '').set('year', year).set('methodCode', Constant.IS_AUTHORIZATION).set('regType', Constant.REG_TYPE_CODE.REGISTER_TAX_SETTLEMENT);
      this.subscriptions.push(
        this.declarationService.getDataByProperties(params).subscribe(res => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            if (res.data.length > 0) {
              this.toastService.error(this.translate.instant('selfService.notification.errorRegister'));
              this.disableBtnSave = true;
            } else {
              this.disableBtnSave = false;
            }
          }
        }, error => this.toastService.error(error?.message))
      );
    }
  }

  getEmailEmployee(event: PersonalInfo) {
    if (!this.isEdit) {
      this.form.controls['email'].setValue(event?.email);
    }
    this.employeeCode = event.employeeCode;
    this.isEdit = false;
  }

  onSave(status: number) {
    if (this.employeeLoginInfo) {
      if (!this.employeeLoginInfo.taxNo) {
        this.toastService.error(this.translate.instant("selfService.validate.userLoginNotHaveTaxNo"));
        return;
      }
    }
    this.isSubmitted = true;
    if (this.form.valid) {
      const data: DeclarationRegister = this.form.value;
      data.employeeId = this.employeeId;
      data.employeeCode = this.employeeCode;
      data.invoiceRequestId = this.registerId;
      data.status = status;
      this.isLoadingPage = true;
      this.subscriptions.push(this.invoiceRequestService.saveData(data).subscribe(res => {
        this.isLoadingPage = false;
        if (res && res.code === HTTP_STATUS_CODE.OK) {
          const notification = this.registerId ? 'common.notification.editSuccess' : 'common.notification.addSuccess'
          this.toastService.success(this.translate.instant(notification));
          this.router.navigateByUrl(UrlConstant.URL.TAX_REGISTER_PAGE).then();
        } else {
          this.toastService.error(this.translate.instant('common.notification.updateError') + ': ' + res.message);
        }
      }, error => {
        this.isLoadingPage = false;
        this.toastService.error(error?.message);
      }));
    }
  }

  onCancelRegister() {
    if (this.registerId) {
      this.isLoadingPage = true;
      this.subscriptions.push(
        this.invoiceRequestService.deleteData(this.registerId).subscribe(res => {
          this.isLoadingPage = false;
          if (res && res.code === HTTP_STATUS_CODE.OK) {
            this.toastService.success(this.translate.instant('common.notification.cancelSuccess'));
            this.router.navigateByUrl(UrlConstant.URL.TAX_REGISTER_PAGE).then();
          } else {
            this.toastService.error(this.translate.instant('common.notification.updateError') + ': ' + res.message);
          }
        }, error => {
          this.isLoadingPage = false;
          this.toastService.error(error?.message);
        })
      );
    }
  }

  ngOnDestroy(): void {
    this.subscriptions?.forEach(sub => sub.unsubscribe());
  }

  getEmployeeInfo(event: PersonalInfo) {
    this.employeeLoginInfo = event;
  }

  onBack() {
    this.router.navigateByUrl(UrlConstant.URL.TAX_REGISTER_PAGE).then();
  }

}
