import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReceiveInvoicesComponent } from './receive-invoices/receive-invoices.component';
import { SelfServiceUiPersonalInformationModule } from '@hcm-mfe/self-service/ui/personal-information';
import { NzFormModule } from 'ng-zorro-antd/form';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbTextLabelModule } from '@hcm-mfe/shared/ui/mb-text-label';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { RouterModule } from '@angular/router';
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
    imports: [CommonModule, SelfServiceUiPersonalInformationModule, NzFormModule, ReactiveFormsModule, SharedUiMbSelectModule,
        TranslateModule, SharedUiMbTextLabelModule, SharedUiMbInputTextModule, SharedUiMbButtonModule, NzButtonModule,
        RouterModule.forChild([
            {
                path: '',
                component: ReceiveInvoicesComponent
            }
        ]), SharedUiLoadingModule
    ],
  declarations: [ReceiveInvoicesComponent],
  exports: [ReceiveInvoicesComponent],
})
export class SelfServiceFeaturePersonalTaxReceiveInvoicesModule {}
