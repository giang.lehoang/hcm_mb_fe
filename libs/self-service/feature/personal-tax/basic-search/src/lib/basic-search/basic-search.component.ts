import {Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {HttpParams} from '@angular/common/http';
import * as moment from 'moment';
import {NzSafeAny} from 'ng-zorro-antd/core/types';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {NzModalRef, NzModalService} from 'ng-zorro-antd/modal';
import {MBTableConfig, Pagination} from '@hcm-mfe/shared/data-access/models';
import {ObjectCategory, TaxRegisterInfo} from '@hcm-mfe/self-service/data-access/models';
import {Constant, UrlConstant} from '@hcm-mfe/self-service/data-access/common';
import {TaxRegistersService} from '@hcm-mfe/self-service/data-access/services';
import {DateValidator} from '@hcm-mfe/shared/common/validators';
import {StringUtils} from '@hcm-mfe/shared/common/utils';
import {HTTP_STATUS_CODE} from '@hcm-mfe/shared/common/constants';
import {ChangeHistoryModalComponent} from '@hcm-mfe/self-service/feature/personal-tax/change-history-modal';
import {Subscription} from "rxjs";
import {PopupService} from "@hcm-mfe/shared/ui/popup";

@Component({
    selector: 'app-basic-search',
    templateUrl: './basic-search.component.html',
    styleUrls: ['./basic-search.component.scss']
})
export class BasicSearchComponent implements OnInit, OnDestroy {
    form: FormGroup = this.fb.group([]);
    requestTypes: ObjectCategory[] = [];
    statusList: ObjectCategory[] = [];
    tableConfig!: MBTableConfig;

    pagination = new Pagination();
    searchResult: TaxRegisterInfo[] = [];

    @ViewChild('actionTmpl', {static: true}) actionTmpl!: TemplateRef<NzSafeAny>;
    @ViewChild('statusTmpl', {static: true}) statusTmpl!: TemplateRef<NzSafeAny>;
    @ViewChild('registerInfoTmpl', {static: true}) registerInfoTmpl!: TemplateRef<NzSafeAny>;
    @ViewChild('footerTmpl') footerTmpl!: TemplateRef<Record<string, never>>;

    SEND_BROWSE_ACTION = Constant.ACTION_PER_RECORD.SEND_BROWSE;
    CANCEL_ACTION = Constant.ACTION_PER_RECORD.CANCEL_REQUEST;
    EDIT_ACTION = Constant.ACTION_PER_RECORD.EDIT;
    DELETE_ACTION = Constant.ACTION_PER_RECORD.DELETE;

    listAddNewOption = Constant.ADD_NEW_OPTIONS;
    isShowPopOver = false;
    isLoadingPage = false;
    constant = Constant;

    changeHistoryModal!: NzModalRef;
    subs: Subscription[] = [];

    constructor(
        private taxRegisterService: TaxRegistersService,
        private fb: FormBuilder,
        private translate: TranslateService,
        private toastService: ToastrService,
        private modalService: NzModalService,
        private deletePopup: PopupService,
        private router: Router
    ) {
    }


    ngOnInit(): void {
        this.initDataSelect();
        this.initForm();
        this.initTable();
        this.search(1);
    }

    initDataSelect() {
        this.requestTypes = Constant.REQUEST_TYPES.map(item => {
            item.label = this.translate.instant(item.label);
            return item;
        });

        this.statusList = Constant.STATUSES.map(item => {
            item.label = this.translate.instant(item.label);
            return item;
        });
    }

    initForm() {
        this.form = this.fb.group({
            requestTypeId: [null],
            status: [null],
            fromDate: [null],
            toDate: [null],
        }, {
            validators: [
                DateValidator.validateRangeDate('fromDate', 'toDate', 'rangeDateError')
            ]
        });
    }

    search(pageNumber: number) {
        this.pagination.pageNumber = pageNumber;
        this.doSearch();
    }

    processDataBeforeSend() {
        let params = new HttpParams();

        let regType = null;
        let status = null;
        let fromDate = null;
        let toDate = null;

        if (!StringUtils.isNullOrEmpty(this?.form?.value?.requestTypeId)) {
            regType = this?.form?.value?.requestTypeId;
        }
        if (!StringUtils.isNullOrEmpty(this?.form?.value?.status)) {
            status = ~~this?.form?.value?.status;
        }
        if (!StringUtils.isNullOrEmpty(this?.form?.value?.fromDate)) {
            fromDate = moment(this?.form?.value?.fromDate).format('DD/MM/YYYY');
        }
        if (!StringUtils.isNullOrEmpty(this?.form?.value?.toDate)) {
            toDate = moment(this?.form?.value?.toDate).format('DD/MM/YYYY');
        }

        if (regType) {
            params = params.append('regType', regType);
        }

        if (status) {
            params = params.append('status', status);
        }

        if (fromDate) {
            params = params.append('fromDate', fromDate);
        }

        if (toDate) {
            params = params.append('toDate', toDate);
        }

        return params;
    }

    processResponse(listData: TaxRegisterInfo[]): TaxRegisterInfo[] {
        return listData.map(item => {

            Constant.REQUEST_TYPES.forEach((item1) => {
                if (item1.value === item.regType) {
                    item.regTypeCode = item1.value;
                    item.regType = this.translate.instant(item1.label);
                }
            });

            return item;
        });
    }

    doSearch() {
        if (this.form?.valid) {
            const params = this.processDataBeforeSend();
            const taxRegisterApi = this.taxRegisterService.searchData(UrlConstant.SEARCH_FORM.TAX_REGISTERS, params, this.pagination.getCurrentPage());
            this.isLoadingPage = true;
            const sub = taxRegisterApi.subscribe({
                next: (res) => {
                    if (res.code == HTTP_STATUS_CODE.OK) {
                        this.searchResult = this.processResponse(res.data.listData);
                        this.tableConfig.total = res.data.count;
                    }
                    this.isLoadingPage = false;
                },
                error: (err) => {
                    this.isLoadingPage = false;
                    this.toastService.error(err.message);
                }
            });
            this.subs.push(sub)
        }
    }

    private initTable(): void {
        this.tableConfig = {
            headers: [
                {
                    title: 'selfService.taxRegisters.table.requestType',
                    field: 'regType',
                },
                {
                    title: 'selfService.taxRegisters.table.registerInfo',
                    field: 'registerInformation',
                    tdTemplate: this.registerInfoTmpl
                },
                {
                    title: 'selfService.taxRegisters.table.registerDate',
                    field: 'createDate',
                    tdClassList: ['text-center'],
                    thClassList: ['text-center'],
                    width: 160,
                },
                {
                    title: 'selfService.taxRegisters.table.status',
                    field: 'status',
                    width: 160,
                    tdTemplate: this.statusTmpl,
                    thClassList: ['text-center'],
                    tdClassList: ['text-center']
                },
                {
                    title: 'selfService.taxRegisters.table.action',
                    field: 'action',
                    tdClassList: ['text-center'],
                    tdTemplate: this.actionTmpl,
                    fixedDir: 'right',
                    width: 80
                },
            ],
            total: 0,
            loading: false,
            size: 'small',
            pageSize: this.pagination.pageSize,
            pageIndex: 1
        };
    }

    onSendBrowseClick(registerId: number, regType: string) {
        this.isLoadingPage = true;
        const sub = this.taxRegisterService.updateWorkFlow(registerId, regType, Constant.SEND_BROWSE).subscribe({
            next: (res) => {
                if (res.code == HTTP_STATUS_CODE.OK) {
                    this.doSearch();
                    this.toastService.success(this.translate.instant('common.notification.sendBrowseSuccess'));
                } else {
                    this.toastService.error(res.message);
                }
                this.isLoadingPage = false;
            },
            error: (err) => {
                this.isLoadingPage = false;
                this.toastService.error(err.message);
            }
        });
        this.subs.push(sub)
    }

    onCancelRequestClick(registerId: number, regType: string) {
        this.isLoadingPage = true;
        const sub = this.taxRegisterService.updateWorkFlow(registerId, regType, Constant.CANCEL_REQUEST).subscribe({
            next: (res) => {
                if (res.code == HTTP_STATUS_CODE.OK) {
                    this.doSearch();
                    this.toastService.success(this.translate.instant('common.notification.cancelRequestSuccess'));
                } else {
                    this.toastService.error(res.message);
                }
                this.isLoadingPage = false;
            },
            error: (err) => {
                this.isLoadingPage = false;
                this.toastService.error(err.message);
            }
        });
        this.subs.push(sub)
    }

    onDeleteClick(registerId: number, regType: string) {
        this.deletePopup.showModal(() => {
            this.isLoadingPage = true;
            const sub = this.taxRegisterService.deleteData(registerId, regType).subscribe({
                next: (res) => {
                    if (res.code == HTTP_STATUS_CODE.OK) {
                        this.doSearch();
                        this.toastService.success(this.translate.instant('common.notification.deleteSuccess'));
                    } else {
                        this.toastService.error(res.message);
                    }
                    this.isLoadingPage = false;
                },
                error: (err) => {
                    this.isLoadingPage = false;
                    this.toastService.error(err.message);
                }
            });
            this.subs.push(sub);
        });
    }

    onViewHistoryClick(registerId: number, regType: string) {
        let objectType = '';
        switch (regType) {
            case Constant.REG_TYPE.REGISTER_NEW:
            case Constant.REG_TYPE.ALTER_TAX_INFO:
                objectType = Constant.LOG_OBJECT_TYPE.TAX_NUMBER;
                break;
            case Constant.REG_TYPE.REGISTER_NEW_DP:
            case Constant.REG_TYPE.REGISTER_LOW_DP:
                objectType = Constant.LOG_OBJECT_TYPE.DEPENDENT;
                break;
            case Constant.REG_TYPE.REGISTER_TAX_SETTLEMENT:
            case Constant.REG_TYPE.REGISTER_RECEIVE_RECEIPT:
                objectType = Constant.LOG_OBJECT_TYPE.DECLARATION;
                break;
            case Constant.REG_TYPE.CONFIRM_AUTHORIZED:
                objectType = Constant.LOG_OBJECT_TYPE.CONFIRM;
                break;
        }
        this.changeHistoryModal = this.modalService.create({
            nzWidth: window.innerWidth > 767 ? window.innerWidth / 1.5 > 1100 ? 1100 : window.innerWidth / 1.5 : window.innerWidth,
            nzContent: ChangeHistoryModalComponent,
            nzComponentParams: {
                objectId: registerId,
                objectType: objectType
            },
            nzTitle: this.translate.instant('selfService.taxRegisters.table.changeHistory.titleHeader'),
            nzFooter: this.footerTmpl
        });
    }

    cancel() {
        this.changeHistoryModal.destroy();
    }

    onDblClickNode(event: NzSafeAny) {
      this.onEditClick(event?.registerId, event?.regTypeCode, true);
    }

    onEditClick(registerId: number, regType: string, isDetail = false) {
        let url = '';
        switch (regType) {
            case Constant.REG_TYPE.REGISTER_NEW:
                url = isDetail ? UrlConstant.URL.REGISTER_NEW_DETAIL : UrlConstant.URL.REGISTER_NEW_EDIT;
                break;
            case Constant.REG_TYPE.ALTER_TAX_INFO:
                url = isDetail ? UrlConstant.URL.ALTER_TAX_INFO_DETAIL : UrlConstant.URL.ALTER_TAX_INFO_EDIT;
                break;
            case Constant.REG_TYPE.REGISTER_NEW_DP:
                url = isDetail ? UrlConstant.URL.REGISTER_NEW_DP_DETAIL : UrlConstant.URL.REGISTER_NEW_DP_EDIT;
                break;
            case Constant.REG_TYPE.REGISTER_LOW_DP:
                url = isDetail ? UrlConstant.URL.REGISTER_LOW_DP_DETAIL : UrlConstant.URL.REGISTER_LOW_DP_EDIT;
                break;
            case Constant.REG_TYPE.REGISTER_TAX_SETTLEMENT:
                url = isDetail ? UrlConstant.URL.REGISTER_TAX_SETTLEMENT_DETAIL : UrlConstant.URL.REGISTER_TAX_SETTLEMENT_EDIT;
                break;
            case Constant.REG_TYPE.REGISTER_RECEIVE_RECEIPT:
                url = isDetail ? UrlConstant.URL.REGISTER_RECEIVE_RECEIPT_DETAIL : UrlConstant.URL.REGISTER_RECEIVE_RECEIPT_EDIT;
                break;
            case Constant.REG_TYPE.CONFIRM_AUTHORIZED:
                url = isDetail ? UrlConstant.URL.CONFIRM_AUTHORIZED_DETAIL : UrlConstant.URL.CONFIRM_AUTHORIZED_EDIT;
                break;
        }
        this.router.navigateByUrl(url + registerId).then();
    }

    onClosePopover() {
        this.isShowPopOver = false;
    }

    onItemClick(item: string) {
        switch (item) {
            case Constant.ADD_NEW_OPTIONS[0]: // Đăng ký mã số thuế
                this.router.navigateByUrl(UrlConstant.URL.REGISTER_NEW_CREATE).then();
                break;
            case Constant.ADD_NEW_OPTIONS[1]: // Đăng ký thay đổi thông tin đk mã số thuế
                this.router.navigateByUrl(UrlConstant.URL.ALTER_TAX_INFO_CREATE).then();
                break;
            case Constant.ADD_NEW_OPTIONS[2]:  // Đăng ký người phụ thuộc
                this.router.navigateByUrl(UrlConstant.URL.REGISTER_NEW_DP_CREATE).then();
                break;
            case Constant.ADD_NEW_OPTIONS[3]:  // Đăng ký giảm người phụ thuộc
                this.router.navigateByUrl(UrlConstant.URL.REGISTER_LOW_DP_CREATE).then();
                break;
            case Constant.ADD_NEW_OPTIONS[4]:  // Đăng ký quản trị thuế
                this.router.navigateByUrl(UrlConstant.URL.REGISTER_TAX_SETTLEMENT_CREATE).then();
                break;
            case Constant.ADD_NEW_OPTIONS[5]:  // Đăng ký nhận chứng từ thuế
                this.router.navigateByUrl(UrlConstant.URL.REGISTER_RECEIVE_RECEIPT_CREATE).then();
                break;
            case Constant.ADD_NEW_OPTIONS[6]:  // ĐK cung cấp TT cho công ty thành viên
                this.router.navigateByUrl(UrlConstant.URL.CONFIRM_AUTHORIZED_CREATE).then();
                break;
        }
        this.isShowPopOver = false;
    }

    ngOnDestroy(): void {
        this.changeHistoryModal?.destroy();
        this.subs.forEach(sub => sub.unsubscribe());
    }
}
