import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BasicSearchComponent} from './basic-search/basic-search.component';
import {NzFormModule} from 'ng-zorro-antd/form';
import {ReactiveFormsModule} from '@angular/forms';
import {SharedUiMbSelectModule} from '@hcm-mfe/shared/ui/mb-select';
import {TranslateModule} from '@ngx-translate/core';
import {SharedUiMbDatePickerModule} from '@hcm-mfe/shared/ui/mb-date-picker';
import {SharedUiMbButtonModule} from '@hcm-mfe/shared/ui/mb-button';
import {SharedUiMbTableWrapModule} from '@hcm-mfe/shared/ui/mb-table-wrap';
import {SharedUiMbTableModule} from '@hcm-mfe/shared/ui/mb-table';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzDropDownModule} from 'ng-zorro-antd/dropdown';
import {NzPopconfirmModule} from 'ng-zorro-antd/popconfirm';
import {NzTagModule} from 'ng-zorro-antd/tag';
import {RouterModule} from '@angular/router';
import {SelfServiceDataAccessPipesModule} from "@hcm-mfe/self-service/data-access/pipes";
import {NzPopoverModule} from "ng-zorro-antd/popover";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiPopupModule} from "@hcm-mfe/shared/ui/popup";
import {NzModalModule} from "ng-zorro-antd/modal";

@NgModule({
  imports: [CommonModule, NzFormModule, ReactiveFormsModule, SharedUiMbSelectModule, TranslateModule, SharedUiMbDatePickerModule,
    SharedUiMbButtonModule, SharedUiMbTableWrapModule, SharedUiMbTableModule, NzIconModule, NzButtonModule, NzDropDownModule,
    NzPopconfirmModule, NzTagModule, SharedUiLoadingModule, NzModalModule,
    RouterModule.forChild([
      {
        path: '',
        component: BasicSearchComponent
      }
    ]), SelfServiceDataAccessPipesModule, NzPopoverModule, SharedUiPopupModule
  ],
  declarations: [BasicSearchComponent],
  exports: [BasicSearchComponent]
})
export class SelfServiceFeaturePersonalTaxBasicSearchModule {}
