import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmProvideInfoComponent } from './confirm-provide-info/confirm-provide-info.component';
import { SelfServiceUiPersonalInformationModule } from '@hcm-mfe/self-service/ui/personal-information';
import { NzFormModule } from 'ng-zorro-antd/form';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { RouterModule } from '@angular/router';
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";
import {SharedUiMbDatePickerModule} from "@hcm-mfe/shared/ui/mb-date-picker";
import { NzRadioModule } from 'ng-zorro-antd/radio';

@NgModule({
    imports: [CommonModule, SelfServiceUiPersonalInformationModule, NzFormModule, ReactiveFormsModule, SharedUiMbInputTextModule,
        TranslateModule, SharedUiMbButtonModule, NzButtonModule,
        RouterModule.forChild([
            {
                path: '',
                component: ConfirmProvideInfoComponent
            }
        ]), SharedUiLoadingModule, SharedUiMbDatePickerModule, NzRadioModule
    ],
  declarations: [ConfirmProvideInfoComponent],
  exports: [ConfirmProvideInfoComponent],
})
export class SelfServiceFeaturePersonalTaxConfirmProvideInfoModule {}
