import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute, Router} from '@angular/router';
import {User, UserLogin} from '@hcm-mfe/shared/data-access/models';
import {ConfirmRegisterService, TaxCommonService} from '@hcm-mfe/self-service/data-access/services';
import {StorageService} from '@hcm-mfe/shared/common/store';
import {HTTP_STATUS_CODE, STORAGE_NAME} from '@hcm-mfe/shared/common/constants';
import {SessionKey} from '@hcm-mfe/shared/common/enums';
import {Constant, SCREEN_MODE, UrlConstant} from '@hcm-mfe/self-service/data-access/common';
import {ConfirmRegister} from '@hcm-mfe/self-service/data-access/models';
import {Subscription} from "rxjs";
import {NzSafeAny} from "ng-zorro-antd/core/types";
import {Utils} from "@hcm-mfe/shared/common/utils";

@Component({
    selector: 'app-confirm-provide-info',
    templateUrl: './confirm-provide-info.component.html',
    styleUrls: ['./confirm-provide-info.component.scss']
})
export class ConfirmProvideInfoComponent implements OnInit, OnDestroy {
    subs: Subscription[] = []
    userLogin: UserLogin = new UserLogin();
    currentUser?: User;
    employeeId?: number;

    form: FormGroup = this.fb.group([]);
    isSubmitted = false;
    isLoadingPage = false;
    isDetail = false;
    registerId?: number = undefined;
    isAccepted?: number;
    isSetAccountNo = true;
    options = [
        { label: this.translate.instant('selfService.provideInfo.agree'), value: Constant.ACCEPT_PROVIDE_INFO },
        { label: this.translate.instant('selfService.provideInfo.notAgree'), value: Constant.NOT_ACCEPT_PROVIDE_INFO },
    ]

    constructor(
        private fb: FormBuilder,
        private toastService: ToastrService,
        private translate: TranslateService,
        private activatedRoute: ActivatedRoute,
        private taxCommonService: TaxCommonService,
        private confirmRegisterService: ConfirmRegisterService,
        private router: Router,
    ) {
        this.registerId = this.activatedRoute.snapshot.queryParams['registerId'];
        this.isDetail = this?.activatedRoute?.snapshot?.data['screenMode'] === SCREEN_MODE.VIEW;
    }

    ngOnInit(): void {
        this.getEmployeeId();
        this.setFormGroup();
        if (this.registerId) {
            this.pathValueForm();
        }
    }

    getEmployeeId() {
        this.userLogin = StorageService.get(STORAGE_NAME.USER_LOGIN);
        const currentUser = localStorage.getItem(SessionKey.CURRENCY_USER);
        if (currentUser) {
            this.currentUser = JSON.parse(currentUser ?? '');
        }
        this.employeeId = this.userLogin.employeeId ?? this.currentUser?.empId ?? 1;
    }

    pathValueForm() {
        if (this.registerId) {
            this.isLoadingPage = true;
            const sub = this.taxCommonService.getTaxRegisterById(this.registerId, Constant.REG_TYPE.CONFIRM_AUTHORIZED).subscribe(res => {
                if (res.code === HTTP_STATUS_CODE.OK) {
                    const data = res.data;
                    this.isAccepted = data.isAccepted;
                    this.form.patchValue(data);
                    this.form.controls['createDate'].setValue(Utils.convertDateToFillForm(res.data.createDate));
                    if (this.isDetail) {
                      this.form.disable();
                    }

                } else {
                    this.toastService.error(res.message);
                }
                this.isLoadingPage = false;
                this.isSetAccountNo = false;
            }, error => {
                this.isLoadingPage = false;
                this.toastService.error(error?.message)
            });
            this.subs.push(sub);
        }
    }

    setFormGroup() {
        this.form = this.fb.group({
            note: [null],
            accountNo: [null],
            isAccepted: [Constant.ACCEPT_PROVIDE_INFO],
            createDate: [new Date()],
        });
    }

    onSave() {
        this.isSubmitted = true;
        if (this.form.valid) {
            const confirmRegisterApi = this.confirmRegisterService;
            const confirmRegister: ConfirmRegister = this.form.value;
            confirmRegister.employeeId = this.employeeId;

            this.isLoadingPage = true;
            const sub = confirmRegisterApi.saveData(confirmRegister).subscribe({
                next: (res) => {
                    if (res && res.code === HTTP_STATUS_CODE.OK) {
                        const notification = 'common.notification.saveSuccess';
                        this.toastService.success(this.translate.instant(notification));
                        this.router.navigateByUrl(UrlConstant.URL.TAX_REGISTER_PAGE).then();
                    } else {
                        this.toastService.error(this.translate.instant('common.notification.provideInfoFail') + ': ' + res.message);
                    }
                    this.isLoadingPage = false;
                },
                error: (err) => {
                    this.toastService.error(err.message);
                    this.isLoadingPage = false;
                }
            });
            this.subs.push(sub);
        }
    }

    getEmployeeInfo(event: NzSafeAny) {
        if (event && this.isSetAccountNo) {
            this.form.controls['accountNo'].setValue(event.accountNo);
        }
    }

    onBack() {
        this.router.navigateByUrl(UrlConstant.URL.TAX_REGISTER_PAGE).then();
    }

    ngOnDestroy(): void {
        this.subs.forEach(sub => sub.unsubscribe());
    }

}
