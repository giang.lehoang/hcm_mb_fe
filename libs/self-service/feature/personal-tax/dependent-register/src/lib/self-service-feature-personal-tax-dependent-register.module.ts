import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DependentRegisterComponent } from './dependent-register/dependent-register.component';
import { SelfServiceUiPersonalInformationModule } from '@hcm-mfe/self-service/ui/personal-information';
import { NzFormModule } from 'ng-zorro-antd/form';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbDatePickerModule } from '@hcm-mfe/shared/ui/mb-date-picker';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { SharedDirectivesNumberInputModule } from '@hcm-mfe/shared/directives/number-input';
import { RouterModule } from '@angular/router';
import {NzIconModule} from "ng-zorro-antd/icon";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
    imports: [CommonModule, SelfServiceUiPersonalInformationModule, NzFormModule, ReactiveFormsModule, SharedUiMbSelectModule,
        TranslateModule, SharedUiMbDatePickerModule, NzSwitchModule, SharedUiMbInputTextModule, NzUploadModule, SharedUiMbButtonModule,
        NzButtonModule, SharedDirectivesNumberInputModule,
        RouterModule.forChild([
            {
                path: '',
                component: DependentRegisterComponent
            }
        ]), NzIconModule, SharedUiLoadingModule
    ],
  declarations: [DependentRegisterComponent],
  exports: [DependentRegisterComponent],
})
export class SelfServiceFeaturePersonalTaxDependentRegisterModule {}
