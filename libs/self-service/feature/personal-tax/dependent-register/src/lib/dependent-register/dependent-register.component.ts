import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {HttpParams} from "@angular/common/http";
import {NzUploadFile} from "ng-zorro-antd/upload";
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core"
import * as moment from 'moment';
import {ActivatedRoute, Router} from "@angular/router";
import {saveAs} from 'file-saver';
import {Subscription} from "rxjs";
import {Constant, SCREEN_MODE, UrlConstant} from '@hcm-mfe/self-service/data-access/common';
import {BaseResponse, SelectModal, User, UserLogin} from '@hcm-mfe/shared/data-access/models';
import {
  DependentService,
  DownloadFileAttachService,
  TaxCommonService
} from '@hcm-mfe/self-service/data-access/services';
import {HTTP_STATUS_CODE, STORAGE_NAME} from '@hcm-mfe/shared/common/constants';
import {StorageService} from '@hcm-mfe/shared/common/store';
import {SessionKey} from '@hcm-mfe/shared/common/enums';
import {CatalogModel, DependentRegister, PersonalInfo} from '@hcm-mfe/self-service/data-access/models';
import {beforeUploadFile, getTypeExport, StringUtils} from '@hcm-mfe/shared/common/utils';
import {CustomValidators, DateValidator} from '@hcm-mfe/shared/common/validators';
import {NzSafeAny} from "ng-zorro-antd/core/types";

@Component({
  selector: 'app-dependent-register',
  templateUrl: './dependent-register.component.html',
  styleUrls: ['./dependent-register.component.scss']
})
export class DependentRegisterComponent implements OnInit, OnDestroy {
  form: FormGroup = this.fb.group([]);
  listRelatives: CatalogModel[] = [];
  employeeId?: number;
  userLogin: UserLogin = new UserLogin();
  currentUser?: User;
  provinces: CatalogModel[] = [];
  nations: CatalogModel[] = [];
  districts: CatalogModel[] = [];
  wards: CatalogModel[] = [];
  isSubmitted = false;
  fileList: NzUploadFile[] = [];
  data?: DependentRegister;
  constant = Constant;
  registerId?: number;
  docIdsDelete: number[] = [];
  employeeLoginInfo?: PersonalInfo;
  subs: Subscription[] = [];
  isLoadingPage = false;
  isDetail = false;
  status?: number;
  isDisabledCheckIdNo = false;

  constructor(
    private dependentRegistersService: DependentService,
    private fb: FormBuilder,
    private toastService: ToastrService,
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private taxCommonService: TaxCommonService,
    private downloadFileAttachService: DownloadFileAttachService
  ) { }

  ngOnInit(): void {
    this.getEmployeeId();
    this.getRelatives();
    this.initForm();
    this.getProvinces();
    this.getNations();
    this.isDetail = this?.activatedRoute?.snapshot?.data['screenMode'] === SCREEN_MODE.VIEW;
    this.registerId = this.activatedRoute.snapshot.queryParams['registerId'];
    if(this.registerId) {
      this.pathValueForm().then();
    }
  }

  initForm() {
    this.form = this.fb.group({
      familyRelationshipId: [null, [Validators.required]],
      dateOfBirth: [null],
      isHaveIdNo: [true],
      taxNo: [null, [Validators.minLength(10), Validators.maxLength(10)]],
      idNo: [null],
      note: [null],
      fromDate: [null, [Validators.required]],
      toDate: [null],
      codeNo: null,
      bookNo:null,
      nationCode: null,
      provinceCode: null,
      districtCode: null,
      wardCode: null,
      rejectReason: null,
      dependentAge: null
    },{
      validators: [
        DateValidator.validateRangeDate('fromDate', 'toDate', 'rangeDateError'),
        CustomValidators.validateOnly9Or12Digit('idNo')
      ]
    });
  }

    async pathValueForm() {
        if (this.registerId) {
            this.isLoadingPage = true;
            const res = await this.taxCommonService.getTaxRegisterById(this.registerId, Constant.REG_TYPE.REGISTER_NEW_DP).toPromise();
            if (res.code === HTTP_STATUS_CODE.OK) {
                this.isLoadingPage = false;
                const data: DependentRegister = res.data;
                this.status = data.status;
                if (data.provinceCode) {
                    this.getDistricts(data.provinceCode);
                }
                if (data.districtCode) {
                    this.getWards(data.districtCode);
                }
                this.form.patchValue(data);
                if (data.codeNo) {
                    this.form.controls['isHaveIdNo'].setValue(false);
                }

                this.form.controls['dateOfBirth'].setValue(data.dateOfBirth ? moment(data.dateOfBirth, 'DD/MM/YYYY').toDate() : null);
                this.form.controls['fromDate'].setValue(data.fromDate ? moment(data.fromDate, 'DD/MM/YYYY').toDate() : null);
                this.form.controls['toDate'].setValue(data.toDate ? moment(data.toDate, 'DD/MM/YYYY').toDate() : null);
                this.form.controls['idNo'].setValue(data?.idNo?.toString().trim());
                this. form.controls['taxNo'].setValue(data?.taxNo?.toString().trim());

                if (this.form.controls['dateOfBirth'].value) {
                  this.form.controls['dependentAge'].setValue(this.getDependentAge(this.form.controls['dateOfBirth'].value)?.toString());
                }


                if (data?.attachFileList && data?.attachFileList.length > 0) {
                    data?.attachFileList.forEach((item) => {
                        this.fileList.push({
                            uid: item.docId.toString(),
                            name: item.fileName,
                            thumbUrl: item.security,
                            status: 'done'
                        });
                    });
                    this.fileList = [...this.fileList];
                }
                if (this.isDetail) {
                  this.form.disable();
                }

            } else {
                this.isLoadingPage = false;
                this.toastService.error(res.message);
            }
        }
    }

  getEmployeeId() {
    this.userLogin = StorageService.get(STORAGE_NAME.USER_LOGIN);
    const currentUser = localStorage.getItem(SessionKey.CURRENCY_USER);
    if (currentUser) {
        this.currentUser = JSON.parse(currentUser ?? '');
    }
    this.employeeId = this.userLogin.employeeId ?? this.currentUser?.empId ?? 1;
  }

    getRelatives() {
        const param = new HttpParams().set("pageSize", 1000);
        if (this.employeeId) {
            this.subs.push(
                this.dependentRegistersService.getListRelatives(this.employeeId, param).subscribe((res: BaseResponse) => {
                    if (res.code === HTTP_STATUS_CODE.OK) {
                        this.listRelatives = res.data.listData.map((item: NzSafeAny) => {
                            item.label = item?.relationTypeName + " " + item?.fullName;
                            item.value = item.familyRelationshipId;
                            item.taxNo = item.taxNumber;
                            item.idNo =  item.personalIdNumber;
                            return item;
                        })
                    }
                }, error => {
                    this.toastService.error(error.message);
                })
            );
        }
    }

  changeRelatives(event: SelectModal) {
    if (event?.itemSelected) {
      this.form.controls['dateOfBirth']?.setValue(event.itemSelected?.dateOfBirth ? moment(event?.itemSelected?.dateOfBirth,'DD/MM/YYYY').toDate() : null);
      this.form.controls['idNo']?.setValue(event?.itemSelected.personalIdNumber?.toString().trim());
      this.form.controls['taxNo']?.setValue(event?.itemSelected.taxNumber?.toString().trim());
    }
  }

  getNations() {
    this.subs.push(
      this.dependentRegistersService.getCatalog(Constant.CATALOGS.QUOC_GIA).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.nations = res.data;
        } else {
          this.nations = [];
        }
      }, error => this.toastService.error(error?.message))
    );
  }

  getProvinces() {
    this.subs.push(
      this.dependentRegistersService.getCatalog(Constant.CATALOGS.TINH).subscribe(res => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          this.provinces = res.data;
        } else {
          this.provinces = [];
        }
      }, error => this.toastService.error(error?.message))
    );
  }

  getDistricts(parentCode?: string) {
    if (parentCode) {
      this.subs.push(
        this.dependentRegistersService.getCatalog(Constant.CATALOGS.HUYEN, parentCode).subscribe(res => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.districts = res.data;
          } else {
            this.districts = [];
          }
        }, error => this.toastService.error(error?.message))
      );
    }
  }

  getWards(parentCode?: string) {
    if (parentCode) {
      this.subs.push(
        this.dependentRegistersService.getCatalog(Constant.CATALOGS.XA, parentCode).subscribe(res => {
          if (res.code === HTTP_STATUS_CODE.OK) {
            this.wards = res.data;
          } else {
            this.wards = [];
          }
        }, error => this.toastService.error(error?.message))
      );
    }
  }

  changeProvince(event: SelectModal) {
    this.getDistricts(event?.itemSelected?.value);
  }

  changeDistrict(event: SelectModal) {
    this.getWards(event?.itemSelected?.value);
  }

  checkIsIdNo(event: boolean) {
    this.setValidateForm(!event, "codeNo", "bookNo", "nationCode", "provinceCode", "districtCode", "wardCode");
  }

  setValidateForm(isError: boolean, ...properties: string[]) {
    if(isError) {
      properties.forEach((item: string) => {
        this.form.controls[item].setValidators(Validators.required);
      });
    } else {
      properties.forEach((item: string) => {
        this.form.controls[item].setErrors(null);
      });
    }
  }

  save(status:number) {
    this.validateTaxNo()

    if (this.employeeLoginInfo) {
        if (!this.employeeLoginInfo.taxNo) {
            this.toastService.error(this.translate.instant("selfService.validate.userLoginNotHaveTaxNo"));
            return;
        }
    }

    this.isSubmitted = true;
    if (this.form.valid && this.fileList?.length > 0) {
      const value: DependentRegister = this.form.value;
      value.status = status;
      value.regType = Constant.REG_TYPE.REGISTER_NEW_DP;
      value.employeeId = this.employeeId;
      value.dependentRegisterId = this.registerId ? this.registerId : null;
      value.docIdsDelete = this.docIdsDelete;

      if (this.form.controls['isHaveIdNo'].value) {
        value.codeNo = null;
      }

      const formData = new FormData();
      formData.append('data', new Blob([JSON.stringify(value)], {
        type: 'application/json'
      }));

      this.fileList.forEach((nzFile: NzSafeAny) => {
        formData.append('files', nzFile);
      });
      this.isLoadingPage = true;
      this.subs.push(
        this.dependentRegistersService.saveDependentRegister(formData).subscribe(res => {
          this.isLoadingPage = false;
          if (res && res.code === HTTP_STATUS_CODE.OK) {
            const notification = this.registerId ? (status === Constant.STATUS_OBJ.IS_DECLARING ? 'common.notification.editSuccess' : 'common.notification.sendBrowseSuccess') : 'common.notification.addSuccess'
            this.toastService.success(this.translate.instant(notification));
            this.router.navigateByUrl(UrlConstant.URL.TAX_REGISTER_PAGE).then();
          } else {
            this.toastService.error(this.translate.instant('common.notification.updateError') + ': ' + res.message);
          }
        }, (error) => {
          this.isLoadingPage = false;
          this.toastService.error(error?.message);
        })
      );
    }
  }

  validateTaxNo() {
    if (this.form.value['isHaveIdNo']) {
      this.form.controls['taxNo'].removeValidators(Validators.required);
      if (StringUtils.isNullOrEmpty(this.form.value['idNo']) && StringUtils.isNullOrEmpty(this.form.value['taxNo'])) {
        this.form.controls['taxNo'].addValidators([Validators.required]);
      }
    } else {
      this.form.controls['idNo'].reset();
      this.form.controls['taxNo'].reset();
    }
    this.form.controls['taxNo'].updateValueAndValidity();
  }

  beforeUpload = (file: NzUploadFile): boolean => {
    const maxSize: number = 5*1024*1024;
    if (this.fileList.length < 4) {
      this.fileList = beforeUploadFile(file, this.fileList, maxSize, this.toastService, this.translate, 'common.notification.fileExtensionInvalidTaxRegister', ".TIF", ".JPG", ".GIF", ".PNG", ".PDF", ".DOC", ".DOCX", ".XLS", ".XLSX")
    } else {
      this.toastService.error(this.translate.instant('selfService.notification.maxFile'));
    }
    return false;
  }

  changeDateOfBirth(event: Date) {
    if (!this.form.controls['toDate'].value) {
      const toDate = new Date(event);
      const dateNow = new Date();
      if (event && dateNow.getFullYear() - toDate.getFullYear() < Constant.MAX_AGE_REDUCE) {
        toDate.setFullYear(toDate.getFullYear() + Constant.MAX_AGE_REDUCE);
        this.form.controls['toDate'].setValue(toDate);
      } else {
        this.form.controls['toDate'].setValue(null);
      }
    }

    //tinh tuoi nguoi phu thuoc
    if (event && this.getDependentAge(event) >= 14) {
        this.form.controls['isHaveIdNo'].setValue(true);
        this.isDisabledCheckIdNo = true;
        this.form.controls['idNo'].addValidators(Validators.required);
    } else {
      this.isDisabledCheckIdNo = false;
      this.form.controls['idNo'].removeValidators(Validators.required);
    }
    this.form.controls['idNo'].updateValueAndValidity();
  }

  getDependentAge(dateOfBirth: Date): number {
    const nowDate = new Date();
    let age = 0;
    try {
      age = nowDate.getFullYear() - dateOfBirth?.getFullYear();
    } catch (error) {}
    return age > 0 ? age : 0;
  }

  downloadFile = (file: NzUploadFile) => {
      if (file.thumbUrl) {
          this.isLoadingPage = true;
          this.subs.push(
              this.downloadFileAttachService.doTaxDownloadAttachFile(Number(file.uid), file.thumbUrl).subscribe({
                  next: (res) => {
                      this.isLoadingPage = false;
                      const reportFile = new Blob([res], {type: getTypeExport(file.name.split(".").pop())});
                      saveAs(reportFile, file.name);
                  }, error: () => {
                      this.isLoadingPage = false;
                      this.toastService.error(this.translate.instant("common.notification.downloadFilePermissionDeny"));
                  }
              })
          );
      }
  }

  removeFile = (file: NzUploadFile): boolean => {
    if(file.uid) {
      this.docIdsDelete.push(Number(file.uid));
    }
    const index = this.fileList.indexOf(file);
    this.fileList.splice(index, 1);
    return false;
  }

  ngOnDestroy(): void {
    this.subs?.forEach(sub => sub.unsubscribe());
  }

  onBack() {
    this.router.navigateByUrl(UrlConstant.URL.TAX_REGISTER_PAGE).then();
  }

    getEmployeeInfo(event: PersonalInfo) {
        this.employeeLoginInfo = event;
    }

}
