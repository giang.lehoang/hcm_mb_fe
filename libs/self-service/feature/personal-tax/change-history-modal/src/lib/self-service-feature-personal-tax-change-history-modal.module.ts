import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChangeHistoryModalComponent } from './change-history-modal/change-history-modal.component';
import { SharedUiMbTableWrapModule } from '@hcm-mfe/shared/ui/mb-table-wrap';
import { SharedUiMbTableModule } from '@hcm-mfe/shared/ui/mb-table';
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
  imports: [CommonModule, SharedUiMbTableWrapModule, SharedUiMbTableModule, SharedUiLoadingModule],
  declarations: [ChangeHistoryModalComponent],
  exports: [ChangeHistoryModalComponent],
})
export class SelfServiceFeaturePersonalTaxChangeHistoryModalModule {}
