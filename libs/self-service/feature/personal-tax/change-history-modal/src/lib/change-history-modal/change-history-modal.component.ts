import {Component, OnDestroy, OnInit} from '@angular/core';
import {HttpParams} from '@angular/common/http';
import { MBTableConfig, Pagination } from '@hcm-mfe/shared/data-access/models';
import { UrlConstant } from '@hcm-mfe/self-service/data-access/common';
import { HTTP_STATUS_CODE } from '@hcm-mfe/shared/common/constants';
import { ChangeHistory } from '@hcm-mfe/self-service/data-access/models';
import { LogActionService } from '@hcm-mfe/self-service/data-access/services';
import {Subscription} from "rxjs";

@Component({
    selector: 'app-change-history-modal',
    templateUrl: './change-history-modal.component.html',
    styleUrls: ['./change-history-modal.component.scss']
})
export class ChangeHistoryModalComponent implements OnInit, OnDestroy {

    subs: Subscription[] = [];
    changeHistoryData: ChangeHistory[] = [];
    tableConfig!: MBTableConfig;
    pagination = new Pagination();

    objectId: number = 0;
    objectType: string = '';
    isLoadingPage = false;

    constructor(
        private logActionService: LogActionService
    ) {
    }

    ngOnInit(): void {
        this.initTable();
        this.doSearch(1);
    }

    doSearch(pageIndex: number) {
        this.pagination.pageNumber = pageIndex;
        this.tableConfig.loading = true;

        const logActionApi = this.logActionService;
        let params = new HttpParams();
        params = params.append('objectId', this.objectId);
        params = params.append('objectType', this.objectType);
        this.isLoadingPage = true;
        const sub = logActionApi.searchData(UrlConstant.SEARCH_FORM.LOG_ACTIONS, params, this.pagination.getCurrentPage()).subscribe({
            next: (res) => {
                if (res.code == HTTP_STATUS_CODE.OK) {
                    this.changeHistoryData = res?.data?.listData;
                    this.tableConfig.pageIndex = pageIndex;
                    this.tableConfig.total = res.data.count;
                }
                this.tableConfig.loading = false;
                this.isLoadingPage = false;
            },
            error: () => {
              this.isLoadingPage = false;
              this.tableConfig.loading = true
            }
        });
        this.subs.push(sub)
    }

    private initTable(): void {
        this.tableConfig = {
            headers: [
                {
                    title: 'selfService.taxRegisters.table.changeHistory.createDate',
                    field: 'createDate',
                    tdClassList: ['text-center'], thClassList: ['text-center'],
                    width: 200
                },
                {
                    title: 'selfService.taxRegisters.table.changeHistory.createdBy',
                    field: 'createdBy',
                    width: 200
                },
                {
                    title: 'selfService.taxRegisters.table.changeHistory.content',
                    field: 'content',
                    width: 300
                }
            ],
            total: 0,
            loading: false,
            size: 'small',
            pageSize: this.pagination.pageSize,
            pageIndex: 1,
        };
    }

  ngOnDestroy(): void {
    this.subs.forEach(sub => sub.unsubscribe());
  }
}
