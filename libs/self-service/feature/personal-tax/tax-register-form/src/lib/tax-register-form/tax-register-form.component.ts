import {Component, OnInit} from '@angular/core';
import {NzUploadFile} from 'ng-zorro-antd/upload';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {forkJoin, of, Subscription} from 'rxjs';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import * as moment from 'moment';
import {switchMap} from 'rxjs/operators';
import {saveAs} from 'file-saver';
import { Constant, SCREEN_MODE, UrlConstant } from '@hcm-mfe/self-service/data-access/common';
import { BaseResponse, SelectModal, User, UserLogin } from '@hcm-mfe/shared/data-access/models';
import {
  DownloadFileAttachService, PersonalInfoService,
  TaxCommonService, TaxRegistersService
} from '@hcm-mfe/self-service/data-access/services';
import { HTTP_STATUS_CODE, STORAGE_NAME } from '@hcm-mfe/shared/common/constants';
import { StorageService } from '@hcm-mfe/shared/common/store';
import { SessionKey } from '@hcm-mfe/shared/common/enums';
import { beforeUploadFile, getTypeExport } from '@hcm-mfe/shared/common/utils';
import {
    ListFileAttach,
    LookupValues,
    ObjectCategory,
    PersonalIdentity, PersonalInfo, TaxNumberRegister,
    TaxNumberRegistersResponse
} from '@hcm-mfe/self-service/data-access/models';
import { LookupValuesService } from '@hcm-mfe/personal-tax/data-access/services';
import { ActivatedRoute, Router } from '@angular/router';
import { ValidationService } from '@hcm-mfe/shared/core';
import {NzSafeAny} from "ng-zorro-antd/core/types";

@Component({
    selector: 'app-tax-register-form',
    templateUrl: './tax-register-form.component.html',
    styleUrls: ['./tax-register-form.component.scss']
})
export class TaxRegisterFormComponent implements OnInit {
    employeeId: number;
    userLogin: UserLogin = new UserLogin();
    currentUser?: User;

    regType: string;
    screenMode: string;

    fileList: NzUploadFile[] = [];
    form: FormGroup = this.fb.group([]);
    isSubmitted = false;

    listPaperType: ObjectCategory[] = [];
    listCitizenPlace: ObjectCategory[] = [];
    listNational: LookupValues[] = [];

    provinces: LookupValues[] = [];
    districts: LookupValues[] = [];
    wards: LookupValues[] = [];

    districtsCurrent: LookupValues[] = [];
    wardsCurrent: LookupValues[] = [];

    isLoadingProvinces = false;
    isLoadingDistricts = false;
    isLoadingWards = false;
    isLoadingProvincesCurrent = false;
    isLoadingDistrictsCurrent = false;
    isLoadingWardsCurrent = false;
    isLoadingPage = false;

    personalIdentities: PersonalIdentity[] = [];

    subs: Subscription[] = [];

    registerId?: number;
    taxNumberRegisterEdit?: TaxNumberRegistersResponse;

    citizenPaperTypeValue = Constant.PAPERS_CODE.CITIZEN_ID;
    docIdsDelete: number[] = [];
    isEdit = false;
    isDetail = false;
    constant = Constant;


    constructor(
        private fb: FormBuilder,
        private lookupValuesService: LookupValuesService,
        private personalInfoService: PersonalInfoService,
        private toastService: ToastrService,
        private translate: TranslateService,
        private taxRegisterService: TaxRegistersService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private taxCommonService: TaxCommonService,
        private downloadFileAttachService: DownloadFileAttachService,
    ) {
        this.userLogin = StorageService.get(STORAGE_NAME.USER_LOGIN);
        const currentUser = localStorage.getItem(SessionKey.CURRENCY_USER);
        if (currentUser) {
            this.currentUser = JSON.parse(currentUser ?? '');
        }
        this.employeeId = this.userLogin.employeeId ?? this.currentUser?.empId ?? 1;

        this.screenMode = this?.activatedRoute?.snapshot?.data['screenMode'];

        // lấy loại đăng ký
        this.regType = this?.activatedRoute?.snapshot?.data['regType']?.value;
    }

    ngOnInit(): void {
        this.getIdentities();
        this.getPaperTypes();
        this.getCatalogs();
        this.initForm();
        this.isDetail = this.screenMode === SCREEN_MODE.VIEW;
        // Mode = EDIT
        if (this.screenMode === SCREEN_MODE.AJUST || this.screenMode === SCREEN_MODE.VIEW) {
            this.patchValueToForm();
        }
    }

    getPaperTypes() {
        this.subs.push(
            this.lookupValuesService.getCatalogModuleTax(Constant.CATALOGS.LOAI_GIAY_TO, Constant.CATALOGS.TEN_CQT).subscribe(res => {
                if (res.code === HTTP_STATUS_CODE.OK) {
                    this.listPaperType = res.data;
                }
            }, error => this.toastService.error(error.message))
        );
    }

    getIdentities() {
        this.subs.push(this.personalInfoService.getIdentities(this.employeeId).subscribe({
            next: (res) => {
                if (res.code == HTTP_STATUS_CODE.OK) {
                    this.personalIdentities = res.data;
                } else {
                    this.toastService.error(res?.data);
                }
            },
            error: (err) => {
                this.toastService.error(err.message);
            }
        }));
    }

    fillDataFollowSelectChange(paperType: string) {

        // Nếu là màn hình edit và (editData === paperType) -> fill giấy tờ -> return
        if ( !!(this.taxNumberRegisterEdit) && (paperType === this.taxNumberRegisterEdit?.idTypeCode) ) {
            this.form.controls['idNo'].setValue(this.taxNumberRegisterEdit?.idNo);
            this.form.controls['idDate'].setValue(moment(this.taxNumberRegisterEdit?.idDate, 'DD/MM/YYYY').toDate());
            this.form.controls['idPlace'].setValue(this.taxNumberRegisterEdit?.idPlace);
            this.form.controls['idPlaceCode'].setValue(this.taxNumberRegisterEdit?.idPlaceCode);
            return;
        }

        let identity;
        if (paperType === Constant.PAPERS_CODE.ID_NO) {
            identity = this.personalIdentities?.find(item => item.idTypeCode ? item.idTypeCode === paperType : item.idNo?.length === 9);
        } else if (paperType === Constant.PAPERS_CODE.CITIZEN_ID) {
            identity = this.personalIdentities?.find(item => item.idTypeCode ? item.idTypeCode === paperType : item.idNo?.length === 12);
        } else if(paperType) {
            identity = this.personalIdentities?.find(item => item.idTypeCode ? item.idTypeCode === paperType : (item.idNo?.length !== 12 && item.idNo?.length !== 9));
        }
        if (identity) {
            switch (paperType) {
                case Constant.PAPERS_CODE.ID_NO:
                case Constant.PAPERS_CODE.CITIZEN_ID:
                    this.form.controls['idNo'].setValue(identity.idNo);
                    this.form.controls['idDate'].setValue(identity.idIssueDate ? moment(identity.idIssueDate, 'DD/MM/YYYY').toDate() : null);
                    this.form.controls['idPlaceCode'].setValue(identity.idIssuePlace);
                    break;
                default :
                    this.form.controls['idNo'].setValue(identity.idNo);
                    this.form.controls['idDate'].setValue(identity.idIssueDate ? moment(identity.idIssueDate, 'DD/MM/YYYY').toDate() : null);
                    this.form.controls['idPlace'].setValue(identity.idIssuePlace);
                    break;
            }
        } else {
            this.form.controls['idNo'].setValue(null);
            this.form.controls['idDate'].setValue(null);
            this.form.controls['idPlace'].setValue(null);
            this.form.controls['idPlaceCode'].setValue(null);
        }
    }

    initForm() {
        this.form = this.fb.group({
            idNo: [null, [Validators.required]],
            idDate: [null, [Validators.required]],
            idPlace: [null],
            idPlaceCode: [null],
            idTypeCode: ['', [Validators.required]],
            permanentNationCode: [{ value: null }, [Validators.required]],
            permanentProvinceCode: [null, [Validators.required]],
            permanentDistrictCode: [null, [Validators.required]],
            permanentWardCode: [null, [Validators.required]],
            permanentDetail: [null, [Validators.required]],
            currentNationCode: [{ value: null }, [Validators.required]],
            currentProvinceCode: [null, [Validators.required]],
            currentDistrictCode: [null, [Validators.required]],
            currentWardCode: [null, [Validators.required]],
            currentDetail: [null, [Validators.required]],
            mobileNumber: [null, [Validators.required, Validators.pattern('^[0-9]{10}$')]],
            email: [null, [Validators.required, Validators.email]],
            rejectReason: [null],
        });
    }

    getCatalogs() {
        this.getNational();
        this.getProvinces();
        this.getCitizenPlace();
    }

    getCitizenPlace() {
        const typeCode = this.form.controls['idTypeCode']?.value === this.citizenPaperTypeValue ? Constant.CATALOGS.NOI_CAP_CCCD : Constant.CATALOGS.NOI_CAP_CMND;
        this.subs.push(
            this.lookupValuesService.getCatalog(typeCode).subscribe({
                next: (res: BaseResponse) => {
                    if (res.code === HTTP_STATUS_CODE.OK) {
                        this.listCitizenPlace = res?.data;
                    } else {
                        this.toastService.error(res?.message);
                    }
                },
                error: (err) => {
                    this.toastService.error(err?.message);
                }
            })
        );
    }

    getNational() {
        this.subs.push(
            this.lookupValuesService.getCatalog(Constant.CATALOGS.QUOC_GIA).subscribe({
                next: (res: BaseResponse) => {
                    if (res.code === HTTP_STATUS_CODE.OK) {
                        this.listNational = res.data;
                        this.setDefaultNation(this.listNational);
                    } else {
                        this.toastService.error(res?.data);
                    }
                },
                error: (err) => {
                    this.toastService.error(err?.message);
                }
            })
        );
    }

    setDefaultNation(nations: LookupValues[]) {
        const vnModel = nations?.find(nation => nation.label === Constant.VN_NATION);
        if (vnModel) {
            this.form.controls['permanentNationCode'].setValue(vnModel?.value);
            this.form.controls['currentNationCode'].setValue(vnModel?.value);
        }
    }

    getProvinces() {
        this.subs.push(
            this.lookupValuesService.getCatalog(Constant.CATALOGS.TINH).subscribe({
                next: (res: BaseResponse) => {
                    if (res.code === HTTP_STATUS_CODE.OK) {
                        this.provinces = res.data;
                        this.isLoadingProvinces = false;
                        this.isLoadingProvincesCurrent = false;
                    } else {
                        this.toastService.error(res?.data);
                    }
                },
                error: (err) => {
                    this.toastService.error(err?.message);
                }
            })
        );
    }

    changeProvince($event: SelectModal, type: 'HK' | 'HT') {
        if (type === 'HK' && $event?.itemSelected?.value !== this?.taxNumberRegisterEdit?.permanentProvinceCode) {
            this.form.controls['permanentDistrictCode'].reset();
            this.form.controls['permanentWardCode'].reset();
        }
        if (type === 'HT' && $event?.itemSelected?.value !== this?.taxNumberRegisterEdit?.currentProvinceCode) {
            this.form.controls['currentDistrictCode'].reset();
            this.form.controls['currentWardCode'].reset();
        }
        this.getDistricts($event?.itemSelected?.value, type);
    }

    changeDistrict($event: SelectModal, type: 'HK' | 'HT') {
        if (type === 'HK' && $event?.itemSelected?.value !== this?.taxNumberRegisterEdit?.permanentDistrictCode) {
            this.form.controls['permanentWardCode'].reset();
        }
        if (type === 'HT' && $event?.itemSelected?.value !== this?.taxNumberRegisterEdit?.currentDistrictCode) {
            this.form.controls['currentWardCode'].reset();
        }
        this.getWards($event?.itemSelected?.value, type);
    }

    getDistricts(parentCode?: number, type: 'HK' | 'HT' = 'HK') {
        if (parentCode) {
            if (type === 'HK') {
                this.isLoadingDistricts = true;
            } else {
                this.isLoadingDistrictsCurrent = true;
            }
            this.subs.push(
                this.lookupValuesService.getCatalog(Constant.CATALOGS.HUYEN, parentCode).subscribe({
                    next: (res: BaseResponse) => {
                        if (res.code === HTTP_STATUS_CODE.OK) {
                            if (type === 'HK') {
                                this.districts = res.data;
                                this.isLoadingDistricts = false;
                            } else {
                                this.districtsCurrent = res.data;
                                this.isLoadingDistrictsCurrent = false;
                            }
                        } else {
                            this.toastService.error(res?.data);
                        }
                    },
                    error: (err) => {
                        this.toastService.error(err?.message);
                    }
                })
            );
        }
    }

    getWards(parentCode?: number, type: 'HK' | 'HT' = 'HK') {
        if (parentCode) {
            if (type === 'HK') {
                this.isLoadingWards = true;
            } else {
                this.isLoadingWardsCurrent = true;
            }
            this.subs.push(
                this.lookupValuesService.getCatalog(Constant.CATALOGS.XA, parentCode).subscribe({
                    next: (res: BaseResponse) => {
                        if (res.code === HTTP_STATUS_CODE.OK) {
                            if (type === 'HK') {
                                this.wards = res.data;
                                this.isLoadingWards = false;
                            } else {
                                this.wardsCurrent = res.data;
                                this.isLoadingWardsCurrent = false;
                            }
                        } else {
                            this.toastService.error(res?.data);
                        }
                    },
                    error: (err) => {
                        this.toastService.error(err?.message);
                    }
                })
            );
        }
    }

    beforeUpload = (file: NzUploadFile): boolean => {
        const maxSize: number = 5*1024*1024;
        if (this.fileList.length < 4) {
          this.fileList = beforeUploadFile(file, this.fileList, maxSize, this.toastService, this.translate, 'common.notification.fileExtensionInvalidTaxRegister', ".TIF", ".JPG", ".GIF", ".PNG", ".PDF", ".DOC", ".DOCX", ".XLS", ".XLSX")
        } else {
          this.toastService.error(this.translate.instant('selfService.notification.maxFile'));
        }
        return false;
    }

    downloadFile = (file: NzUploadFile) => {
        this.isLoadingPage = true;
        this.subs.push(this.downloadFileAttachService.doTaxDownloadAttachFile(Number(file.uid), file.thumbUrl ?? '').subscribe({
            next: (res) => {
                this.isLoadingPage = false;
                const reportFile = new Blob([res], {type: getTypeExport(file.name.split(".").pop())});
                saveAs(reportFile, file.name);
            }, error : () => {
                this.isLoadingPage = false;
                this.toastService.error(this.translate.instant("common.notification.downloadFilePermissionDeny"));
            }
        }));
    }

    removeFile = (file: NzUploadFile): boolean => {
        this.docIdsDelete.push(Number(file.uid));
        const index = this.fileList.indexOf(file);
        this.fileList.splice(index, 1);
        return false;
    }

    validateIdNo() {
        const idTypeCode = this.form.controls['idTypeCode'].value;
        const idNo = this.form.controls['idNo'].value;

        if (idTypeCode && idTypeCode === Constant.PAPERS_CODE.ID_NO) { // cmt
            if (idNo && idNo.length !== 9) {
                this.form.controls['idNo'].setErrors({idNoOnlyAccept9Number : true});
            } else {
                this.form.controls['idNo'].setErrors(null);
                this.form.controls['idNo'].setValidators(Validators.required);
            }
        } else if (idTypeCode && idTypeCode === Constant.PAPERS_CODE.CITIZEN_ID) { // cccd
            if (idNo && idNo.length !== 12) {
                this.form.controls['idNo'].setErrors({citizenOnlyAccept12Number : true});
            } else {
                this.form.controls['idNo'].setErrors(null);
                this.form.controls['idNo'].setValidators(Validators.required);
            }
        }

    }

    // Save click
    onSave(actionType: 'SAVE' | 'SEND_BROWSE') {
        this.isSubmitted = true;
        this.processFormValue();
        this.validateIdNo();
        if (this.form.valid && this.fileList?.length > 0) {
            const request: TaxNumberRegister = this.form.value;

            request.employeeId = this.employeeId;
            request.regType = Constant.REQUEST_TYPES[0].value;
            request.idDate = moment(this.form.controls['idDate'].value).format("DD/MM/YYYY");
            request.taxNumberRegisterId = this.registerId ? this.registerId : null;

            request.docIdsDelete = this.docIdsDelete;

            if (actionType === 'SAVE') {
                request.status = Constant.SAVE_DRAFT;
            } else {
                request.status = Constant.SEND_BROWSE;
            }

            // console.log(request);

            const formData = new FormData();
            formData.append('data', new Blob([JSON.stringify(request)], {
                type: 'application/json'
            }));

            this.fileList.forEach((nzFile: NzSafeAny) => {
                formData.append('files', nzFile);
            });
            this.isLoadingPage = true;
            this.subs.push(this.taxRegisterService.saveRecord(formData).subscribe((res: BaseResponse) => {
                this.isLoadingPage = false;
                if (res.code === HTTP_STATUS_CODE.OK) {
                    if (actionType === 'SEND_BROWSE') {
                        this.toastService.success(this.translate.instant('common.notification.sendBrowseSuccess1'));
                    } else {
                        const notification = this.registerId ? 'common.notification.editSuccess' : 'common.notification.addSuccess'
                        this.toastService.success(this.translate.instant(notification));
                    }
                    this.router.navigateByUrl(UrlConstant.URL.TAX_REGISTER_PAGE).then();
                } else if (res.code === HTTP_STATUS_CODE.CREATED) {
                    this.toastService.error(res.message);
                }
            }, error => {
                this.isLoadingPage = false;
                this.toastService.error(error?.message);
            }));
        }
    }

    onPaperTypeChange(event: SelectModal) {
        const paperType = event.itemSelected.value;
        this.addValidator(paperType);
        this.fillDataFollowSelectChange(paperType);
    }

    addValidator(paperType: string) {
        switch (paperType) {
            case Constant.PAPERS_CODE.ID_NO:
            case Constant.PAPERS_CODE.CITIZEN_ID:
                this.form.controls['idNo'].addValidators(ValidationService.number);
                this.form.controls['idPlaceCode'].setValidators(Validators.required);
                this.form.controls['idPlace'].setValidators(null);
                break;
            default :
                this.form.controls['idPlaceCode'].setValidators(null);
                this.form.controls['idPlace'].setValidators(Validators.required);
                break;
        }
    }

    selectIdPlace(event: SelectModal) {
        this.form.controls['idPlace'].setValue(event?.itemSelected?.label);
    }

    patchValueToForm() {
        this.registerId = this?.activatedRoute?.snapshot?.queryParams['registerId'];
        if (this.registerId) {
            const commonTaxApi = this.taxCommonService.getTaxRegisterById(this.registerId, Constant.REG_TYPE.REGISTER_NEW);
            this.isLoadingPage = true;
            this.subs.push(commonTaxApi.pipe(
                switchMap( (res) => {
                    const data = res.data;
                    this.taxNumberRegisterEdit = data;
                    return of(data);
                })
            ).subscribe({
                next: (data) => {
                    forkJoin([
                        this.lookupValuesService.getCatalog(Constant.CATALOGS.TINH),
                        this.lookupValuesService.getCatalog(Constant.CATALOGS.HUYEN, data.permanentProvinceCode),
                        this.lookupValuesService.getCatalog(Constant.CATALOGS.XA, data.permanentDistrictCode),

                        this.lookupValuesService.getCatalog(Constant.CATALOGS.HUYEN, data.currentProvinceCode),
                        this.lookupValuesService.getCatalog(Constant.CATALOGS.XA, data.currentDistrictCode),
                    ]).subscribe({
                        next: ([provinces, permanentDistricts, permanentWards, currentDistricts, currentWards]) => {
                            this.isLoadingPage = false;
                            this.provinces = provinces.data;
                            this.districts = permanentDistricts.data;
                            this.wards = permanentWards.data;
                            this.districtsCurrent = currentDistricts.data;
                            this.wardsCurrent = currentWards.data;

                            this.form.patchValue(data);

                            // file
                            if (data?.attachFileList && data?.attachFileList.length > 0) {
                                data?.attachFileList.forEach((item: ListFileAttach) => {
                                    this.fileList.push({
                                        uid: item.docId,
                                        name: item.fileName,
                                        thumbUrl: item.security,
                                        status: 'done'
                                    });
                                });
                                this.fileList = [...this.fileList];
                            }
                        },
                        error: (err) => {
                            this.isLoadingPage = false;
                            this.toastService.error(err.message);
                        }
                    });
                },
            }));
            this.isEdit = true;
            if (this.isDetail) {
              this.form.disable();
            }
        }
    }

    onBack() {
        this.router.navigateByUrl(UrlConstant.URL.TAX_REGISTER_PAGE).then();
    }

    processFormValue() {
        this.form.controls['idNo'].setValue(this.form.controls['idNo']?.value?.trim());
        this.form.controls['idPlace'].setValue(this.form.controls['idPlace']?.value?.trim());
        this.form.controls['permanentDetail'].setValue(this.form.controls['permanentDetail']?.value?.trim());
        this.form.controls['currentDetail'].setValue(this.form.controls['currentDetail']?.value?.trim());
        this.form.controls['email'].setValue(this.form.controls['email']?.value?.trim());
    }

    getEmployeeInfo(event: PersonalInfo) {
        this.setPhoneAndEmailInfo(event);
    }

    setPhoneAndEmailInfo(employeeInfo: PersonalInfo) {
        if (!this.isEdit) {
            this.form.controls['mobileNumber'].setValue(employeeInfo?.mobileNumber);
            this.form.controls['email'].setValue(employeeInfo?.email);
        }
        this.isEdit = false;
    }

}
