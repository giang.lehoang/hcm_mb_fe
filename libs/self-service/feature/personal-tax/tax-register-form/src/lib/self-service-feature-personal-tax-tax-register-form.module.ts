import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaxRegisterFormComponent } from './tax-register-form/tax-register-form.component';
import { NzFormModule } from 'ng-zorro-antd/form';
import { ReactiveFormsModule } from '@angular/forms';
import { SelfServiceUiPersonalInformationModule } from '@hcm-mfe/self-service/ui/personal-information';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { SharedUiMbDatePickerModule } from '@hcm-mfe/shared/ui/mb-date-picker';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { SharedDirectivesNumberInputModule } from '@hcm-mfe/shared/directives/number-input';
import { RouterModule } from '@angular/router';
import {NzIconModule} from "ng-zorro-antd/icon";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
    imports: [CommonModule, NzFormModule, ReactiveFormsModule, SelfServiceUiPersonalInformationModule, SharedUiMbSelectModule,
        TranslateModule, SharedUiMbInputTextModule, SharedUiMbDatePickerModule, NzDividerModule, NzUploadModule, SharedUiMbButtonModule,
        NzButtonModule, SharedDirectivesNumberInputModule,
        RouterModule.forChild([
            {
                path: '',
                component: TaxRegisterFormComponent
            }
        ]), NzIconModule, SharedUiLoadingModule
    ],
  declarations: [TaxRegisterFormComponent],
  exports: [TaxRegisterFormComponent],
})
export class SelfServiceFeaturePersonalTaxTaxRegisterFormModule {}
