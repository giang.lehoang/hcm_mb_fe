import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NzUploadFile} from "ng-zorro-antd/upload";
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";
import {HttpParams} from "@angular/common/http";
import * as moment from 'moment';
import {ActivatedRoute, Router} from "@angular/router";
import {saveAs} from 'file-saver';
import {Subscription} from "rxjs";
import {Constant, SCREEN_MODE, UrlConstant} from '@hcm-mfe/self-service/data-access/common';
import {SelectModal, User, UserLogin} from '@hcm-mfe/shared/data-access/models';
import {
    DependentService,
    DownloadFileAttachService,
    TaxCommonService
} from '@hcm-mfe/self-service/data-access/services';
import {HTTP_STATUS_CODE, STORAGE_NAME} from '@hcm-mfe/shared/common/constants';
import {StorageService} from '@hcm-mfe/shared/common/store';
import {SessionKey} from '@hcm-mfe/shared/common/enums';
import {DependentInfo, DependentRegister, ListFileAttach} from '@hcm-mfe/self-service/data-access/models';
import {beforeUploadFile, getTypeExport} from '@hcm-mfe/shared/common/utils';
import {NzSafeAny} from "ng-zorro-antd/core/types";

@Component({
    selector: 'app-dependent-deduction',
    templateUrl: './dependent-deduction.component.html',
    styleUrls: ['./dependent-deduction.component.scss']
})
export class DependentDeductionComponent implements OnInit, OnDestroy {
    form: FormGroup = this.fb.group([]);
    listDependents: DependentInfo[] = [];
    isSubmitted = false;
    fileList: NzUploadFile[] = [];
    employeeId?: number;
    userLogin: UserLogin = new UserLogin();
    currentUser?: User;
    constant = Constant;
    data?: DependentRegister;
    registerId?: number;
    docIdsDelete: number[] = [];
    subs: Subscription[] = [];
    isLoadingPage = false;
    isDetail = false;
    status!: number;

    constructor(
        private toastrService: ToastrService,
        private translate: TranslateService,
        private fb: FormBuilder,
        private dependentService: DependentService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private taxCommonService: TaxCommonService,
        private downloadFileAttachService: DownloadFileAttachService
    ) {
    }

    ngOnInit(): void {
        this.initForm();
        this.getEmployeeId();
        this.getListDependents();
        this.registerId = this.activatedRoute.snapshot.queryParams['registerId'];
        this.isDetail = this?.activatedRoute?.snapshot?.data['screenMode'] === SCREEN_MODE.VIEW;
        if (this.registerId) {
            this.pathValueForm();
        }
    }

    initForm() {
        this.form = this.fb.group({
            familyRelationshipId: [null, Validators.required],
            dateOfBirth: null,
            taxNumber: [null, [Validators.minLength(10), Validators.maxLength(10)]],
            note: null,
            fromDate: null,
            toDate: [null, [Validators.required]],
	    rejectReason: [null],
        });
    }

    pathValueForm() {
        if (!this.registerId) {
            return;
        }
        this.isLoadingPage = true;
        this.subs.push(
            this.taxCommonService.getTaxRegisterById(this.registerId, Constant.REG_TYPE.REGISTER_LOW_DP).subscribe(res => {
                if (res.code === HTTP_STATUS_CODE.OK) {
                    this.isLoadingPage = false;
                    const data = res.data;
                    this.status = data.status;
                    this.form.patchValue(data);
                    this.form.controls['taxNumber'].setValue(data.taxNo);
                    this.form.controls['dateOfBirth'].setValue(data.dateOfBirth ? moment(data.dateOfBirth, 'DD/MM/YYYY').toDate() : null);
                    this.form.controls['toDate'].setValue(data.toDate ? moment(data.toDate, 'DD/MM/YYYY').toDate() : null);

                    if (data?.attachFileList && data?.attachFileList.length > 0) {
                        data?.attachFileList.forEach((item: ListFileAttach) => {
                            this.fileList.push({
                                uid: item.docId,
                                name: item.fileName,
                                thumbUrl: item.security,
                                status: 'done'
                            });
                        });
                        this.fileList = [...this.fileList];
                    }
                    if (this.isDetail) {
                      this.form.disable();
                    }
                }
            }, error => {
                this.isLoadingPage = false;
                this.toastrService.error(error?.message)
            })
        );
    }

    getEmployeeId() {
        this.userLogin = StorageService.get(STORAGE_NAME.USER_LOGIN);
        const currentUser = localStorage.getItem(SessionKey.CURRENCY_USER);
        if (currentUser) {
            this.currentUser = JSON.parse(currentUser ?? '');
        }
        this.employeeId = this.userLogin.employeeId ?? this.currentUser?.empId ?? 1;
    }

    getListDependents() {
        const param = new HttpParams().set("pageSize", 1000);
        if (!this.employeeId) {
            return;
        }
        this.subs.push(
            this.dependentService.getListDependents(this.employeeId, param).subscribe(res => {
                if (res.code === HTTP_STATUS_CODE.OK) {
                    const data: DependentInfo[] = res.data.listData;
                    this.listDependents = data?.map(item => {
                        item.value = item.familyRelationshipId;
                        item.label = item.relationTypeName + " " + item.fullName;
                        return item;
                    })
                }
            }, () => {
                this.listDependents = [];
            })
        );
    }

    changeDependents(event: SelectModal) {
      if (event.itemSelected) {
        this.form.controls['dateOfBirth'].setValue(event.itemSelected.dateOfBirth ? moment(event.itemSelected.dateOfBirth,'DD/MM/YYYY').toDate() : null);
        this.form.controls['taxNumber'].setValue(event.itemSelected.taxNumber);
        this.form.controls['toDate'].setValue(event.itemSelected.toDate ? moment(event.itemSelected.toDate,'MM/YYYY').toDate() : null);
      }
    }

    setFromDateToForm(event: SelectModal) {
      this.form.controls['fromDate'].setValue(event?.itemSelected?.fromDate ? moment(event.itemSelected.fromDate,'MM/YYYY').toDate() : null);
    }

    beforeUpload = (file: NzUploadFile): boolean => {
        const maxSize: number = 5*1024*1024;
        if (this.fileList.length < 4) {
          this.fileList = beforeUploadFile(file, this.fileList, maxSize, this.toastrService, this.translate, 'common.notification.fileExtensionInvalidTaxRegister', ".TIF", ".JPG", ".GIF", ".PNG", ".PDF", ".DOC", ".DOCX", ".XLS", ".XLSX")
        } else {
          this.toastrService.error(this.translate.instant('selfService.notification.maxFile'));
        }
        return false;
    }

    downloadFile = (file: NzUploadFile) => {
        if (!file.thumbUrl) {
            return;
        }
        this.isLoadingPage = true;
        this.subs.push(
            this.downloadFileAttachService.doTaxDownloadAttachFile(Number(file.uid), file.thumbUrl).subscribe({
                next: (res) => {
                    this.isLoadingPage = false;
                    const reportFile = new Blob([res], {type: getTypeExport(file.name.split(".").pop())});
                    saveAs(reportFile, file.name);
                }, error: () => {
                    this.isLoadingPage = false;
                    this.toastrService.error(this.translate.instant("common.notification.downloadFilePermissionDeny"));
                }
            })
        );
    }

    removeFile = (file: NzUploadFile): boolean => {
        if (file.uid) {
            this.docIdsDelete.push(Number(file.uid));
        }
        const index = this.fileList.indexOf(file);
        this.fileList.splice(index, 1);
        return false;
    }

    onSave(status: number) {
        this.isSubmitted = true;
        const toDate = this.form.controls['toDate'].value;
        const fromDate = this.form.controls['fromDate'].value;
        if (toDate && toDate <= fromDate) {
            this.toastrService.error(this.translate.instant('common.notification.errorDate'));
            return;
        }
        if (this.form.valid) {
            const value: DependentRegister = this.form.value;
            value.status = status;
            value.regType = Constant.REG_TYPE.REGISTER_LOW_DP;
            value.employeeId = this.employeeId;
            value.taxNo = this.form.value['taxNumber'];
            value.docIdsDelete = this.docIdsDelete;
            value.dependentRegisterId = this.registerId;
            value.fromDate = fromDate ? moment(fromDate, 'dd/MM/yyyy').toDate() : null;
            value.toDate = toDate ? moment(toDate, 'dd/MM/yyyy').toDate() : null;
            const formData = new FormData();

            formData.append('data', new Blob([JSON.stringify(value)], {
                type: 'application/json'
            }));

            this.fileList.forEach((nzFile: NzSafeAny) => {
                formData.append('files', nzFile);
            });
            this.isLoadingPage = true;
            this.subs.push(
                this.dependentService.saveDependentRegister(formData).subscribe(res => {
                    this.isLoadingPage = false;
                    if (res && res.code === HTTP_STATUS_CODE.OK) {
                      const notification = this.registerId ? (status === Constant.STATUS_OBJ.IS_DECLARING ? 'common.notification.editSuccess' : 'common.notification.sendBrowseSuccess') : 'common.notification.addSuccess'
                      this.toastrService.success(this.translate.instant(notification));
                        this.router.navigateByUrl(UrlConstant.URL.TAX_REGISTER_PAGE).then();
                    } else {
                        this.toastrService.error(this.translate.instant('common.notification.updateError') + ': ' + res.message);
                    }
                }, (error) => {
                    this.isLoadingPage = false;
                    this.toastrService.error(error?.message);
                })
            );
        }
    }

    ngOnDestroy(): void {
        this.subs?.forEach(sub => sub.unsubscribe());
    }

    onBack() {
        this.router.navigateByUrl(UrlConstant.URL.TAX_REGISTER_PAGE).then();
    }


}
