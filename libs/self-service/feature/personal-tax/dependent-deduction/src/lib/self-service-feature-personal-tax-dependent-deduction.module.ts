import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DependentDeductionComponent } from './dependent-deduction/dependent-deduction.component';
import { NzFormModule } from 'ng-zorro-antd/form';
import { SelfServiceUiPersonalInformationModule } from '@hcm-mfe/self-service/ui/personal-information';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbDatePickerModule } from '@hcm-mfe/shared/ui/mb-date-picker';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { RouterModule } from '@angular/router';
import {SharedDirectivesNumberInputModule} from "@hcm-mfe/shared/directives/number-input";
import {SharedUiLoadingModule} from "@hcm-mfe/shared/ui/loading";

@NgModule({
    imports: [CommonModule, NzFormModule, SelfServiceUiPersonalInformationModule, ReactiveFormsModule, SharedUiMbSelectModule,
        TranslateModule, SharedUiMbDatePickerModule, SharedUiMbInputTextModule, NzUploadModule, SharedUiMbButtonModule, NzButtonModule,
        RouterModule.forChild([
            {
                path: '',
                component: DependentDeductionComponent
            }
        ]), SharedDirectivesNumberInputModule, SharedUiLoadingModule
    ],
  declarations: [DependentDeductionComponent],
  exports: [DependentDeductionComponent],
})
export class SelfServiceFeaturePersonalTaxDependentDeductionModule {}
