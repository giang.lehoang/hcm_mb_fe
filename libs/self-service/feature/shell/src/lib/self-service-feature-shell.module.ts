import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormatCurrencyPipe } from '@hcm-mfe/shared/pipes/format-currency';
import { SelfServiceFeatureShellRoutingModule } from './self-service-feature-shell.routing.module';
import {CheckDependentRegisterGuard, CheckTaxInfoGuard} from "@hcm-mfe/self-service/data-access/services";
import {SharedUiPopupModule} from "@hcm-mfe/shared/ui/popup";
import {NzModalModule} from "ng-zorro-antd/modal";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
  imports: [CommonModule,
    SelfServiceFeatureShellRoutingModule,
    SharedUiPopupModule,
    NzModalModule, FormsModule, ReactiveFormsModule
  ],
  providers: [DatePipe, FormatCurrencyPipe, CheckDependentRegisterGuard, CheckTaxInfoGuard]
})
export class SelfServiceFeatureShellModule {}
