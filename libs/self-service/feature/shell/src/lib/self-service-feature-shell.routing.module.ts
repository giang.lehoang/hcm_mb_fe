import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {Constant, SCREEN_MODE} from '@hcm-mfe/self-service/data-access/common';
import {CheckDependentRegisterGuard, CheckTaxInfoGuard} from "@hcm-mfe/self-service/data-access/services";

const router: Routes = [
  {
    path: 'staff',
    data: {
      pageName: 'selfService.pageName.staffManager',
      breadcrumb: 'selfService.breadcrumb.staffManager',
      disabled: true
    },
    children: [
      {
        path: '',
        data: {
          pageName: 'selfService.pageName.staffInfo',
          breadcrumb: 'selfService.breadcrumb.selfStaffInfo',
          disabled: true
        },
        children: [
          {
            path: 'personal-info',
            data: {
              pageName: 'selfService.pageName.personalInfo',
              breadcrumb: 'selfService.breadcrumb.personalInfo'
            },
            loadChildren: () => import('@hcm-mfe/self-service/feature/self-staff-info/self-personal-informations').then(m => m.SelfServiceFeatureSelfStaffInfoSelfPersonalInformationsModule)
          }
        ]
      }
    ]
  },
  {
    path: 'requests-manager',
    data: {
      pageName: 'selfService.pageName.requestsManager',
      breadcrumb: 'selfService.breadcrumb.requestsManager',
      disabled: true
    },
    children: [
      {
        path: 'my-request',
        loadChildren: () => import('@hcm-mfe/self-service/feature/requests-manager/request-list').then(m => m.SelfServiceFeatureRequestsManagerRequestListModule),
        data: {
          pageName: 'selfService.pageName.myRequest',
          breadcrumb: 'selfService.breadcrumb.myRequest'
        },
      },
      {
        path: 'request-approve',
        loadChildren: () => import('@hcm-mfe/self-service/feature/requests-manager/request-approve').then(m => m.SelfServiceFeatureRequestsManagerRequestApproveModule),
        data: {
          pageName: 'selfService.pageName.requestApprove',
          breadcrumb: 'selfService.breadcrumb.requestApprove'
        }
      },
      {
        path: 'create',
        loadChildren: () => import('@hcm-mfe/self-service/feature/requests-manager/request-form').then(m => m.SelfServiceFeatureRequestsManagerRequestFormModule),
        data: {
          pageName: 'selfService.pageName.requestCreate',
          breadcrumb: 'selfService.breadcrumb.requestCreate',
          screenMode: SCREEN_MODE.CREATE
        }
      },
      {
        path: 'update',
        loadChildren: () => import('@hcm-mfe/self-service/feature/requests-manager/request-form').then(m => m.SelfServiceFeatureRequestsManagerRequestFormModule),
        data: {
          pageName: 'selfService.pageName.requestUpdate',
          breadcrumb: 'selfService.breadcrumb.requestUpdate',
          screenMode: SCREEN_MODE.UPDATE
        }
      },
      {
        path: 'ajust',
        loadChildren: () => import('@hcm-mfe/self-service/feature/requests-manager/request-form').then(m => m.SelfServiceFeatureRequestsManagerRequestFormModule),
        data: {
          pageName: 'selfService.pageName.requestAjust',
          breadcrumb: 'selfService.breadcrumb.requestAjust',
          screenMode: SCREEN_MODE.AJUST
        }
      },
      {
        path: 'detail',
        loadChildren: () => import('@hcm-mfe/self-service/feature/requests-manager/request-form').then(m => m.SelfServiceFeatureRequestsManagerRequestFormModule),
        data: {
          pageName: 'selfService.pageName.requestDetail',
          breadcrumb: 'selfService.breadcrumb.requestDetail',
          screenMode: SCREEN_MODE.VIEW
        }
      }
    ]
  },
  {
    path: 'personal-tax',
    data: {
      pageName: 'selfService.pageName.personalTax',
      breadcrumb: 'selfService.breadcrumb.personalTax',
      disabled: true
    },
    children: [
      {
        path: 'tax-registers',
        data: {
          pageName: 'selfService.pageName.selfTaxRegisters',
          breadcrumb: 'selfService.breadcrumb.selfTaxRegisters'
        },
        children: [
          {
            path: '',
            loadChildren: () => import('@hcm-mfe/self-service/feature/personal-tax/basic-search').then(m => m.SelfServiceFeaturePersonalTaxBasicSearchModule),
          },
          {
            path: 'create',
            loadChildren: () => import('@hcm-mfe/self-service/feature/personal-tax/tax-register-form').then(m => m.SelfServiceFeaturePersonalTaxTaxRegisterFormModule),
            data: {
              pageName: 'selfService.pageName.registerNew',
              breadcrumb: 'selfService.breadcrumb.registerNew',
              regType: Constant.REQUEST_TYPES[0],
            },
            canActivate: [CheckTaxInfoGuard]
          },
          {
            path: 'edit',
            loadChildren: () => import('@hcm-mfe/self-service/feature/personal-tax/tax-register-form').then(m => m.SelfServiceFeaturePersonalTaxTaxRegisterFormModule),
            data: {
              pageName: 'selfService.pageName.editRegisterNew',
              breadcrumb: 'selfService.breadcrumb.editRegisterNew',
              screenMode: SCREEN_MODE.AJUST
            },
          },
          {
            path: 'detail',
            loadChildren: () => import('@hcm-mfe/self-service/feature/personal-tax/tax-register-form').then(m => m.SelfServiceFeaturePersonalTaxTaxRegisterFormModule),
            data: {
              pageName: 'selfService.pageName.detailRegisterNew',
              breadcrumb: 'selfService.breadcrumb.detailRegisterNew',
              screenMode: SCREEN_MODE.VIEW
            },
          },
          {
            path: 'register-change',
            loadChildren: () => import('@hcm-mfe/self-service/feature/personal-tax/change-tax-registered-info').then(m => m.SelfServiceFeaturePersonalTaxChangeTaxRegisteredInfoModule),
            data: {
              pageName: 'selfService.pageName.selfRegisterChange',
              breadcrumb: 'selfService.breadcrumb.selfRegisterChange',
              regType: Constant.REQUEST_TYPES[1]
            },
            canActivate: [CheckTaxInfoGuard]
          },
          {
            path: 'register-change-edit',
            loadChildren: () => import('@hcm-mfe/self-service/feature/personal-tax/change-tax-registered-info').then(m => m.SelfServiceFeaturePersonalTaxChangeTaxRegisteredInfoModule),
            data: {
              pageName: 'selfService.pageName.registerChangeEdit',
              breadcrumb: 'selfService.breadcrumb.registerChangeEdit',
            }
          },
          {
            path: 'register-change-detail',
            loadChildren: () => import('@hcm-mfe/self-service/feature/personal-tax/change-tax-registered-info').then(m => m.SelfServiceFeaturePersonalTaxChangeTaxRegisteredInfoModule),
            data: {
              pageName: 'selfService.pageName.registerChangeDetail',
              breadcrumb: 'selfService.breadcrumb.registerChangeDetail',
              screenMode: SCREEN_MODE.VIEW
            }
          },
          {
            path: 'provide-info',
            loadChildren: () => import('@hcm-mfe/self-service/feature/personal-tax/confirm-provide-info').then(m => m.SelfServiceFeaturePersonalTaxConfirmProvideInfoModule),
            data: {
              pageName: 'selfService.pageName.provideInfo',
              breadcrumb: 'selfService.breadcrumb.provideInfo',
            },
          },
          {
            path: 'provide-info-edit',
            loadChildren: () => import('@hcm-mfe/self-service/feature/personal-tax/confirm-provide-info').then(m => m.SelfServiceFeaturePersonalTaxConfirmProvideInfoModule),
            data: {
              pageName: 'selfService.pageName.provideInfoEdit',
              breadcrumb: 'selfService.breadcrumb.provideInfoEdit',
            },
          },
          {
            path: 'provide-info-detail',
            loadChildren: () => import('@hcm-mfe/self-service/feature/personal-tax/confirm-provide-info').then(m => m.SelfServiceFeaturePersonalTaxConfirmProvideInfoModule),
            data: {
              pageName: 'selfService.pageName.provideInfoDetail',
              breadcrumb: 'selfService.breadcrumb.provideInfoDetail',
              screenMode: SCREEN_MODE.VIEW
            },
          },
          {
            path: 'dependent-registers',
            loadChildren: () => import('@hcm-mfe/self-service/feature/personal-tax/dependent-register').then(m => m.SelfServiceFeaturePersonalTaxDependentRegisterModule),
            data: {
              pageName: 'selfService.pageName.dependentRegister',
              breadcrumb: 'selfService.breadcrumb.dependentRegister',
              regType: Constant.REQUEST_TYPES[2]
            },
            canActivate: [CheckDependentRegisterGuard]
          },
          {
            path: 'dependent-edit',
            loadChildren: () => import('@hcm-mfe/self-service/feature/personal-tax/dependent-register').then(m => m.SelfServiceFeaturePersonalTaxDependentRegisterModule),
            data: {
              pageName: 'selfService.pageName.dependentRegisterEdit',
              breadcrumb: 'selfService.breadcrumb.dependentRegisterEdit'
            }
          },
          {
            path: 'dependent-detail',
            loadChildren: () => import('@hcm-mfe/self-service/feature/personal-tax/dependent-register').then(m => m.SelfServiceFeaturePersonalTaxDependentRegisterModule),
            data: {
              pageName: 'selfService.pageName.dependentRegisterDetail',
              breadcrumb: 'selfService.breadcrumb.dependentRegisterDetail',
              screenMode: SCREEN_MODE.VIEW
            }
          },
          {
            path: 'dependent-deduction',
            loadChildren: () => import('@hcm-mfe/self-service/feature/personal-tax/dependent-deduction').then(m => m.SelfServiceFeaturePersonalTaxDependentDeductionModule),
            data: {
              pageName: 'selfService.pageName.selfDependentDeduction',
              breadcrumb: 'selfService.breadcrumb.selfDependentDeduction',
              regType: Constant.REQUEST_TYPES[3]
            },
            canActivate: [CheckDependentRegisterGuard]
          },
          {
            path: 'dependent-deduction-edit',
            loadChildren: () => import('@hcm-mfe/self-service/feature/personal-tax/dependent-deduction').then(m => m.SelfServiceFeaturePersonalTaxDependentDeductionModule),
            data: {
              pageName: 'selfService.pageName.dependentDeductionEdit',
              breadcrumb: 'selfService.breadcrumb.dependentDeductionEdit'
            }
          },
          {
            path: 'dependent-deduction-detail',
            loadChildren: () => import('@hcm-mfe/self-service/feature/personal-tax/dependent-deduction').then(m => m.SelfServiceFeaturePersonalTaxDependentDeductionModule),
            data: {
              pageName: 'selfService.pageName.dependentDeductionDetail',
              breadcrumb: 'selfService.breadcrumb.dependentDeductionDetail',
              screenMode: SCREEN_MODE.VIEW
            }
          },
          {
            path: 'declaration-register',
            loadChildren: () => import('@hcm-mfe/self-service/feature/personal-tax/declaration-registers').then(m => m.SelfServiceFeaturePersonalTaxDeclarationRegistersModule),
            data: {
              pageName: 'selfService.pageName.selfDeclarationRegisters',
              breadcrumb: 'selfService.breadcrumb.selfDeclarationRegisters'
            }
          },
          {
            path: 'declaration-edit',
            loadChildren: () => import('@hcm-mfe/self-service/feature/personal-tax/declaration-registers').then(m => m.SelfServiceFeaturePersonalTaxDeclarationRegistersModule),
            data: {
              pageName: 'selfService.pageName.declarationRegistersEdit',
              breadcrumb: 'selfService.breadcrumb.declarationRegistersEdit',
              regType: Constant.REQUEST_TYPES[4]
            },
            canActivate: [CheckDependentRegisterGuard]
          },
          {
            path: 'declaration-detail',
            loadChildren: () => import('@hcm-mfe/self-service/feature/personal-tax/declaration-registers').then(m => m.SelfServiceFeaturePersonalTaxDeclarationRegistersModule),
            data: {
              pageName: 'selfService.pageName.declarationRegistersDetail',
              breadcrumb: 'selfService.breadcrumb.declarationRegistersDetail',
              regType: Constant.REQUEST_TYPES[4],
              screenMode: SCREEN_MODE.VIEW
            },
          },
          {
            path: 'receive-invoices-register',
            loadChildren: () => import('@hcm-mfe/self-service/feature/personal-tax/receive-invoices').then(m => m.SelfServiceFeaturePersonalTaxReceiveInvoicesModule),
            data: {
              pageName: 'selfService.pageName.receiveInvoicesRegister',
              breadcrumb: 'selfService.breadcrumb.receiveInvoicesRegister'
            }
          },
          {
            path: 'receive-invoices-edit',
            loadChildren: () => import('@hcm-mfe/self-service/feature/personal-tax/receive-invoices').then(m => m.SelfServiceFeaturePersonalTaxReceiveInvoicesModule),
            data: {
              pageName: 'selfService.pageName.receiveInvoicesEdit',
              breadcrumb: 'selfService.breadcrumb.receiveInvoicesEdit'
            }
          },
          {
            path: 'receive-invoices-detail',
            loadChildren: () => import('@hcm-mfe/self-service/feature/personal-tax/receive-invoices').then(m => m.SelfServiceFeaturePersonalTaxReceiveInvoicesModule),
            data: {
              pageName: 'selfService.pageName.receiveInvoicesDetail',
              breadcrumb: 'selfService.breadcrumb.receiveInvoicesDetail',
              screenMode: SCREEN_MODE.VIEW
            }
          },
        ]
      },
    ]
  },
]

@NgModule({
  imports: [RouterModule.forChild(router)],
  exports: [RouterModule],
})
export class SelfServiceFeatureShellRoutingModule {}
