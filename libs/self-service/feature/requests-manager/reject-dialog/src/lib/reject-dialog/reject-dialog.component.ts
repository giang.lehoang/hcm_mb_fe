import {Component, OnDestroy, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { ToastrService } from 'ngx-toastr';
import { AbsRequests } from '@hcm-mfe/self-service/data-access/models';
import { RequestsService } from '@hcm-mfe/self-service/data-access/services';
import {Subscription} from "rxjs";

@Component({
  selector: 'app-reject-dialog',
  templateUrl: './reject-dialog.component.html',
  styleUrls: ['./reject-dialog.component.scss']
})
export class RejectDialogComponent implements OnInit, OnDestroy {
  form: FormGroup;
  isSubmitted: boolean = false;
  data?: AbsRequests;
  subs: Subscription[] = [];
  constructor(
    private fb: FormBuilder,
    private requestsService: RequestsService,
    private modalRef: NzModalRef,
    private toastrService: ToastrService,
    private translate: TranslateService,
  ) {
    this.form = this.fb.group({
      rejectReason: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
  }

  get f() { return this.form.controls; }

  save() {
    this.isSubmitted = true;
    if (this.form.valid) {
      const formData = {
        ...this.data,
        ...this.form.value
      }
      this.subs.push(this.requestsService.reject(formData).subscribe(res => {
        const msg = this.data?.allowApprove ? this.translate.instant('selfService.table.action.reject') : this.translate.instant('selfService.table.action.unreview');
        if (res && res.code === 200) {
          this.toastrService.success(this.translate.instant('selfService.table.action.rejectSuccess', {msg: msg}));
          this.modalRef.close({ refresh: true });
        } else {
          this.toastrService.error(this.translate.instant('selfService.table.action.rejectError', {msg: msg}));
          this.modalRef.close({ refresh: false });
        }
      }));
    }
  }

  ngOnDestroy(): void {
    this.subs.forEach(sub => sub.unsubscribe());
  }
}
