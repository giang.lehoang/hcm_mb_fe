import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RejectDialogComponent } from './reject-dialog/reject-dialog.component';
import { NzFormModule } from 'ng-zorro-antd/form';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { TranslateModule } from '@ngx-translate/core';
import { SharedDirectivesScrollSpyModule } from '@hcm-mfe/shared/directives/scroll-spy';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [CommonModule, NzFormModule, SharedUiMbInputTextModule, TranslateModule, SharedDirectivesScrollSpyModule, FormsModule, ReactiveFormsModule],
  declarations: [RejectDialogComponent],
  exports: [RejectDialogComponent],
})
export class SelfServiceFeatureRequestsManagerRejectDialogModule {}
