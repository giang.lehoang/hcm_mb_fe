import {AfterViewInit, Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {AbstractControl, FormArray, FormBuilder, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {BehaviorSubject, forkJoin, Subscription} from 'rxjs';
import * as moment from 'moment';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NzUploadFile} from 'ng-zorro-antd/upload';
import * as _ from 'lodash';
import {saveAs} from 'file-saver';
import {
  LEAVE_TYPE,
  PAGE_NAME,
  PERSONAL_TYPE,
  PERSONAL_TYPE_DATASOURCE,
  REQUEST_APPROVER_STATUS,
  REQUEST_STATUS,
  SCREEN_MODE
} from '@hcm-mfe/self-service/data-access/common';
import {AppFunction, BaseResponse, UserLogin} from '@hcm-mfe/shared/data-access/models';
import {
  AbsReasonLeaves,
  AbsRequestApprovers,
  AbsRequestLeaves,
  AbsRequestSupporters,
  Approver,
  CaculateLeavesResponse,
  CalLeavesApprover,
  ComputeListConfigApprover,
  ListFileAttach,
  PageInfo,
  RequestApproverItem,
  SelfEmployeeDetail
} from '@hcm-mfe/self-service/data-access/models';
import {LookupValuesService} from '@hcm-mfe/personal-tax/data-access/services';
import {
  DownloadFileAttachService,
  ReasonLeavesService,
  RequestLeavesService,
  RequestsApproversService,
  RequestsService
} from '@hcm-mfe/self-service/data-access/services';
import {SessionService, StorageService} from '@hcm-mfe/shared/common/store';
import {HTTP_STATUS_CODE, STORAGE_NAME, SYSTEM_FORMAT_DATA} from '@hcm-mfe/shared/common/constants';
import {checkFileExtension, getTypeExport} from '@hcm-mfe/shared/common/utils';
import {DateValidator} from '@hcm-mfe/shared/common/validators';
import {NzSafeAny} from "ng-zorro-antd/core/types";
import { FunctionCode } from '@hcm-mfe/shared/common/enums';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { RejectDialogComponent } from '@hcm-mfe/self-service/feature/requests-manager/reject-dialog';

@Component({
  selector: 'app-request-form',
  templateUrl: './request-form.component.html',
  styleUrls: ['./request-form.component.scss']
})
export class RequestFormComponent implements OnInit, AfterViewInit, OnDestroy {
  userLogin: UserLogin = new UserLogin();
  employeeId: number | null = null; // id user login
  isLoading = false;
  objFunction: AppFunction;

  formId = 'request-form';
  form: FormGroup;

  listReasonLeave: AbsReasonLeaves[] = []; // Loại đơn
  subjectCalculateLeaves = new BehaviorSubject<CaculateLeavesResponse[]>([]);
  requestId: number | null = null;

  isChanged = false;
  isLeave = false;
  isSubmitted = false;

  screenMode = '';
  SCREEN_MODE = SCREEN_MODE;
  PERSONAL_TYPE_DATASOURCE = PERSONAL_TYPE_DATASOURCE;
  PERSONAL_TYPE = PERSONAL_TYPE;
  REQUEST_STATUS = REQUEST_STATUS;

  fileList: NzUploadFile[] = [];
  docIdsDelete: number[] = [];
  subs: Subscription[] = [];

  hrLevel: number | null = null;
  approvalLevelId: number | null = null;
  employeeIdApproval: number | null = null;
  viewActionApproval = false;
  allowApprove = false;
  modal!: NzModalRef;

  constructor(private fb: FormBuilder,
    private lookupValuesService: LookupValuesService,
    private requestsService: RequestsService,
    private reasonLeavesService: ReasonLeavesService,
    private toastService: ToastrService,
    private translate: TranslateService,
    private router: Router,
    private route: ActivatedRoute,
    private downloadFileAttachService: DownloadFileAttachService,
    private requestLeavesService: RequestLeavesService,
    public sessionService: SessionService,
    private requestsApproversService: RequestsApproversService,
    private modalService: NzModalService
  ) {
    this.handleGetCatalog();
    const { requestId } = this.route.snapshot.queryParams;
    this.requestId = requestId;
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ABS_TIMEKEEPINGS}`);
    this.form = this.fb.group({
      requestId: [this.requestId],
      leaveType: [LEAVE_TYPE.LEAVE],
      personalType: [PERSONAL_TYPE.PERSONAL, [Validators.required]],
      employeeId: [null],
      employeeCode: [''],
      fullName: [''],
      reasonDetail: ['', Validators.maxLength(2000)],
      isCommitted: [true],
      status: [null],
      allDays: [0], // số ngày nghỉ
      totalDays: [0], // số ngày làm việc nghỉ
      listAbsRequestLeaves: this.fb.array([
        this._createReason(null)
      ], {
        validators: [this.validatorFormReason()]
      }),
      listAbsRequestSupporters: this.fb.array([
        this._createSupporter(null)
      ], {
        validators: [this.validatorFormSupporter(), this.validateSupporterDuplicateEmployee()]
      }),
      listAbsRequestApprovers: this.fb.array([])
    });
  }

  ngOnInit(): void {
    this.userLogin = StorageService.get(STORAGE_NAME.USER_LOGIN);
    if (this.requestId) {
      this.subs.push(this.requestsService.findOne(this.requestId).subscribe((res: BaseResponse) => {
        if (res.code === HTTP_STATUS_CODE.OK) {
          const { listAbsRequestApprovers, listAbsRequestLeaves, listAbsRequestSupporters, status } = res.data;
          this.form.patchValue(res.data);
          if (listAbsRequestLeaves) {
            this.reasons.clear();
            listAbsRequestLeaves.forEach((item: AbsRequestLeaves) => {
              this.reasons.push(this._createReason(item));
            });
          }

          if (res.data?.files && res.data?.files.length > 0) {
            res.data.files.forEach((file: ListFileAttach) => {
              this.fileList.push({
                uid: file.docId,
                name: file.fileName,
                url: file.security,
                status: 'done',
              })
            });
          }

          this.fileList = _.cloneDeep(this.fileList);
          if (listAbsRequestSupporters) {
            this.supporters.clear();
            listAbsRequestSupporters.forEach((item: AbsRequestSupporters) => {
              const empl = new SelfEmployeeDetail();
              empl.employeeId = item.employeeId;
              empl.fullName = item.fullName;
              empl.employeeCode = item.employeeCode;
              this.supporters.push(this._createSupporter({
                ...item,
                employeeId: empl
              }));
            });
          }
          // Khoi tao danh sach nguoi phe duyet/xet duyet
          if (listAbsRequestApprovers) {
            this.approvers.clear();
            const {hrLevel, approvalLevelId} = this._computedLevel(listAbsRequestApprovers);
            this.hrLevel = hrLevel || null;
            this.approvalLevelId = approvalLevelId || null;
            if (approvalLevelId) {
              this.subs.push(this.requestsApproversService.getRequestsApprovers({
                employeeId: res.data?.employeeId,
                approverLevelId: approvalLevelId,
                hrLevel: hrLevel,
              }).subscribe(res => {
                const requestApprovers = res?.data || [];

                requestApprovers.forEach((item: RequestApproverItem) => {
                  item.approvers = item.approvers || [];
                  item.approvalLevel = item.approvers?.length > 0 ? item.approvers[0].groupLevelId : 0;
                  item.approvers = item.approvers.map((e: Approver) => ({ ...e, nameCode: `${e.employeeCode} - ${e.fullName} (${e.positionName})` }));
                });

                listAbsRequestApprovers.forEach((item: AbsRequestApprovers, idx: number) => {
                  const isApprover = idx == listAbsRequestApprovers.length - 1;
                  item.title = isApprover ? this.translate.instant('selfService.label.approver') : `${this.translate.instant('selfService.label.reviewer')} ${idx + 1}`;

                  const requestApprover = requestApprovers.find((x: RequestApproverItem) => x.approvalLevel == item.approvalLevel) || {};

                  if (!item?.isSuggest || item?.isSuggest === -1) {
                    const empl = {
                      employeeId: item?.employeeId,
                      fullName: item?.fullName,
                      employeeCode: item?.employeeCode,
                    }
                    if (item) {
                      item.employeeId = empl as SelfEmployeeDetail;
                    }
                  }
                  item.disableSuggest = item?.isSuggest === -1 ? 1 : 0;
                  item.isSuggest = item.isSuggest === -1 ? 0 : item.isSuggest;
                  this.approvers.push(this._createApprover({ ...item, ...requestApprover, isApprover: isApprover }));
                });
              }));
            }
          }
          const idx = listAbsRequestApprovers.findIndex((e: AbsRequestApprovers) => e.employeeId == this.employeeId);
          if (status == REQUEST_STATUS.WAIT_APPROVE && idx > -1) {
            const approver = listAbsRequestApprovers[idx];
            this.viewActionApproval = approver && approver.status == REQUEST_APPROVER_STATUS.WAIT_APPROVE && approver.isAllowView;
            this.allowApprove = idx === listAbsRequestApprovers.length - 1;
          }
          
        } else {
          this.toastService.error(res.message);
        }
      }));
    }
    this.subs.push(this.route.data.subscribe((data: PageInfo) => {
      this.screenMode = data?.screenMode || SCREEN_MODE.CREATE;
    }));
  }

  checkLeave(): void {
    this.isLeave = false;
    if(this.reasons?.value && this.reasons?.value?.length > 0) {
      for (let i = 0 ; i < this.reasons.value.length ; i++) {
        const _reason = this.reasons.value[i];
        const reasonLeave = this.listReasonLeave.find((el: AbsReasonLeaves) => el.reasonLeaveId === _reason.reasonLeaveId);
        if (reasonLeave && reasonLeave.isLeave === 1) {
          this.isLeave = true;
          break;
        }
      }
    }
  }

  downloadFile = (file: NzUploadFile) => {
    if (!file.url) {
      return;
    }
    this.subs.push(this.downloadFileAttachService.doAbsDownloadAttachFile(Number(file.uid), file.url).subscribe({
      next: (res) => {
        const reportFile = new Blob([res], {type: getTypeExport(file.name.split(".").pop())});
        saveAs(reportFile, file.name);
      }, error: () => {
        this.toastService.error(this.translate.instant("common.notification.downloadFilePermissionDeny"));
      }
    }));
  }



  removeFile = (file: NzUploadFile) => {
    this.docIdsDelete.push(Number(file.uid));
    const index = this.fileList.indexOf(file);
    this.fileList.splice(index, 1);
    return true;
  }

  _computedLevel(listAbsRequestApproversConfig: CalLeavesApprover[]) {
    if (!listAbsRequestApproversConfig || listAbsRequestApproversConfig.length == 0) {
      return {};
    }
    let approvalLevelId = null;
    let seq = 0;
    let hrLevel = 0;
    listAbsRequestApproversConfig.forEach((approver: CalLeavesApprover) => {
      if (approver.isHr && approver.approvalLevel > hrLevel) {
        hrLevel = approver.approvalLevel;
      }
      if (!approver.isHr) {
        if (seq < approver.approvalOrder) {
          approvalLevelId = approver.approvalLevel;
          seq = approver.approvalOrder;
        }
      }
    });
    return {
      approvalLevelId,
      hrLevel
    }
  }

  ngAfterViewInit() {
    const formInput = document.getElementById(this.formId);
    if (formInput) {
      formInput.addEventListener('input', this._detectClickItemOrChangeInput);
      formInput.addEventListener('mousedown', this._detectClickItemOrChangeInput);
    }
    this.subs.push(this.subjectCalculateLeaves.subscribe((calLeaves) => {
      this._calculateLeaves(calLeaves);
    }));
  }

  ngOnDestroy() {
    this.subs.forEach(sub => sub.unsubscribe());
    const formInput = document.getElementById(this.formId);
    if (formInput) {
      formInput.removeEventListener('input', this._detectClickItemOrChangeInput);
      formInput.removeEventListener('mousedown', this._detectClickItemOrChangeInput);
    }
  }

  validatorFormSupporter(): ValidatorFn {
    return (formArray: AbstractControl) => {
      const arrayDataValid: number[] = [];
      if (formArray instanceof FormArray) {
        formArray.controls.forEach((item: FormGroup | AbstractControl, i) => {
          if (item.value.employeeId && item.value.employeeId['employeeId']) {
            const value = item.value.employeeId['employeeId'];
            const indexOf = arrayDataValid.indexOf(value);
            if (indexOf === -1) {
              arrayDataValid.push(value);
              const error: NzSafeAny = formArray.controls[i].get('employeeId')?.errors;
              formArray.controls[i].get('employeeId')?.setErrors(null);
              if (error?.duplicate) {
                formArray.controls[i].get('employeeId')?.setErrors({'duplicateEmployee': true});
              }
              if (error?.required) {
                formArray.controls[i].get('employeeId')?.setErrors({'required': true});
              }
            } else {
              formArray.controls[i].get('employeeId')?.setErrors({'duplicate': true});
              formArray.controls[indexOf].get('employeeId')?.setErrors({'duplicate': true});
            }
          }
        });
      }

      return null;
    }
  }

  validateEmployee(): ValidatorFn {
    return (_control: AbstractControl): { [key: string]: string } | null => {
      if (!this.form) {
        return null;
      }
      this.supporters.controls.forEach((item: FormGroup | AbstractControl, i) => {
        if (item.value.employeeId && item.value.employeeId['employeeId'] && _control.value) {
          const value = item.value.employeeId['employeeId'];
          const valueEmployeeId = _control.value.employeeId ?? 0;
          if (value === valueEmployeeId) {
            this.supporters.controls[i].get('employeeId')?.setErrors({'duplicateEmployee': true});
          } else {
            this.supporters.controls[i].get('employeeId')?.setErrors(null);
          }
        }
      })
      return null;
    }
  }

  validateSupporterDuplicateEmployee(): ValidatorFn {
    return (formArray: AbstractControl) => {
      if (!this.form) {
        return null;
      }
      const _employeeId = this.f['employeeId'] ? this.f['employeeId'].value : null;

      if (formArray instanceof FormArray) {
        formArray.controls.forEach((item: FormGroup | AbstractControl, i) => {
          if (item.value.employeeId && item.value.employeeId['employeeId'] && _employeeId) {
            const value = item.value.employeeId['employeeId'];
            const valueEmployee = this.requestId ? _employeeId : _employeeId['employeeId'] ?? 0;
            if (value === valueEmployee) {
              formArray.controls[i].get('employeeId')?.setErrors({'duplicateEmployee': true});
            } else {
              const error: NzSafeAny = formArray.controls[i].get('employeeId')?.errors;
              formArray.controls[i].get('employeeId')?.setErrors(null);
              if (error?.duplicate) {
                formArray.controls[i].get('employeeId')?.setErrors({'duplicate': true});
              }
              if (error?.required) {
                formArray.controls[i].get('employeeId')?.setErrors({'required': true});
              }
            }
          }
        });
      }

      return null;
    }
  }

  get f() {
    return this.form.controls;
  }

  get reasons() {
    return this.f['listAbsRequestLeaves'] as FormArray;
  }

  get supporters() {
    return this.f['listAbsRequestSupporters'] as FormArray;
  }

  get approvers() {
    return this.f['listAbsRequestApprovers'] as FormArray;
  }

  beforeUpload = (file: NzUploadFile): boolean => {
    // Chấp nhận: PDF, XLS, XLSX, file định dạng ảnh
    if (!checkFileExtension(file?.name, ".XLS", ".XLSX", ".PDF", ".TIF", ".JPG", ".GIF", ".PNG")) {
      this.toastService.error(this.translate.instant('common.notification.fileExtensionInvalidXlsPdf'));
      return false;
    }

    if (file.size && file.size >= 3000000) {
      this.toastService.error(this.translate.instant('common.notification.fileSizeOver3MB'));
      return false;
    }

    this.fileList = this.fileList.concat(file);
    return false;
  }

  /**
   * Thêm Loại đơn
   */
  addReason() {
    this.reasons.push(this._createReason(null))
  }

  /**
   * Xóa loại đơn
   * @param index
   */
  removeReason(index: number) {
    if (this.reasons.length > 1)
      this.reasons.removeAt(index);
    else {
      this.reasons.removeAt(index);
      this.addReason();
    }
    const currentCaculateLeaves = this.subjectCalculateLeaves.value;
    if (currentCaculateLeaves[index]) {
      currentCaculateLeaves.splice(index, 1);
      this.subjectCalculateLeaves.next(currentCaculateLeaves);
    }
    this.checkLeave();
  }

  /**
   * Thêm người nhận bàn giao
   */
  addSupporter() {
    this.supporters.push(this._createSupporter(null));
  }

  /**
   * Xóa người nhận bàn giao
   * @param index
   */
  removeSupporter(index: number) {
    if (this.supporters.length > 1)
      this.supporters.removeAt(index);
    else {
      this.supporters.removeAt(index);
      this.addSupporter();
    }
  }

  /**
   * Lấy danh mục Loại đơn
   */
  getListReasonLeave() {
    this.subs.push(
      this.reasonLeavesService.getAllReasonLeaves(LEAVE_TYPE.LEAVE).subscribe(res => {
        const response: BaseResponse = res;
        this.listReasonLeave = response.data;
      })
    );
  }

  /**
   * Lấy userlogin
   */
  getUserLogin() {
    this.subs.push(
      this.requestsService.getUserLogin().subscribe(res => {
        this.employeeId = res.data;
      })
    );
  }

  /**
   * Xử lý lấy các datasource
   */
  handleGetCatalog() {
    this.getListReasonLeave();
    this.getUserLogin();
  }


  /**
   * validateReasonDuplication
   */
  validatorFormReason(): ValidatorFn {
    return (formArray: AbstractControl) => {
      if (formArray instanceof FormArray) {
        const rowNum = formArray.controls.length;
        const check = [];
        const isFail = [];
        for (let index = 0; index < rowNum; index++) {
          check[index] = false;
          isFail[index] = false;
        }
        for (let i = 0; i < rowNum - 1; i++) {
          if (!check[i]) {
            check[i] = true;
            for (let j = i + 1; j < rowNum; j++) {
              if (this._isDuplicate(formArray.controls[i].value, formArray.controls[j].value)) {
                check[j] = true;
                isFail[i] = true;
                isFail[j] = true;
                formArray.controls[i].get('fromTime')?.setErrors({'dublicateTime': true});
                formArray.controls[i].get('toTime')?.setErrors({'dublicateTime': true});
                formArray.controls[j].get('fromTime')?.setErrors({'dublicateTime': true});
                formArray.controls[j].get('toTime')?.setErrors({'dublicateTime': true});
              } else {
                if (!isFail[i]) {
                  formArray.controls[i].get('fromTime')?.setErrors(null);
                  formArray.controls[i].get('toTime')?.setErrors(null);
                  if (!formArray.controls[i].get('fromTime')?.value) {
                    formArray.controls[i].get('fromTime')?.setErrors({'required': true});
                  }
                  if (!formArray.controls[i].get('toTime')?.value) {
                    formArray.controls[i].get('toTime')?.setErrors({'required': true});
                  }
                }
                if (!isFail[j]) {
                  formArray.controls[j].get('fromTime')?.setErrors(null);
                  formArray.controls[j].get('toTime')?.setErrors(null);
                  if (!formArray.controls[j].get('fromTime')?.value) {
                    formArray.controls[j].get('fromTime')?.setErrors({'required': true});
                  }
                  if (!formArray.controls[j].get('toTime')?.value) {
                    formArray.controls[j].get('toTime')?.setErrors({'required': true});
                  }
                }
              }
            }
          }
        }
      }

      return null;
    }
  }

  _isDuplicate(value1: AbsReasonLeaves, value2: AbsReasonLeaves) {
    if (value1.fromTime && value1.toTime && value2.fromTime && value2.toTime) {
      const from1 = moment(value1.fromTime, SYSTEM_FORMAT_DATA.DATE_TIME_FORMAT).toDate().getTime();
      const to1 = moment(value1.toTime, SYSTEM_FORMAT_DATA.DATE_TIME_FORMAT).toDate().getTime();
      const from2 = moment(value2.fromTime, SYSTEM_FORMAT_DATA.DATE_TIME_FORMAT).toDate().getTime();
      const to2 = moment(value2.toTime, SYSTEM_FORMAT_DATA.DATE_TIME_FORMAT).toDate().getTime();
      return !(from2 >= to1 || to2 <= from1);
    }
    return false;
  }

  /**
   * Lưu đơn
   */
  onSave(status?: number) {
    this.isSubmitted = true;
    if (this.form.valid) {
      const request = _.cloneDeep(this.form.value);
      if (request.employeeId && request.employeeId['employeeId']) {
        request.employeeId = request.employeeId['employeeId'];
      }

      request.listAbsRequestSupporters = request.listAbsRequestSupporters.map((e: NzSafeAny) => ({
        employeeId: e.employeeId ? e.employeeId['employeeId'] : null,
        taskContent: e.taskContent
      }));

      request.listAbsRequestApprovers = request.listAbsRequestApprovers.map((e: NzSafeAny, idx: number) => ({
        employeeId: e.isSuggest ? e.employeeId : (e.employeeId ? e.employeeId['employeeId'] : null),
        approvalLevel: e.approvalLevel,
        hrLevel: e.hrLevel,
        isSuggest: e.disableSuggest ? -1 : (e.isSuggest ? 1 : 0),
        approvalOrder: idx + 1
      }));

      if (status) {
        request.status = status;
      }
      request.docIdsDelete = this.docIdsDelete;
      request.fileList = this.fileList;
      this.isLoading = true;
      this.subs.push(this.requestsService.saveOrUpdate(request).subscribe((res: BaseResponse) => {
        this.isLoading = false;
        if (res.code === HTTP_STATUS_CODE.OK) {
          const msg = !this.requestId ? 'common.notification.addSuccess' : 'common.notification.updateSuccess';
          this.toastService.success(this.translate.instant(msg));
          this.router.navigateByUrl(`/ss/requests-manager/my-request`).then();
        } else {
          this.toastService.error(this.translate.instant('common.notification.updateError') + ":" + res.message);
        }
      }));
    }
  }

  /**
   * Hủy
   */
  onCancel() {
    const {pageName} = this.route.snapshot.queryParams;
    if (pageName && pageName == PAGE_NAME.APPROVE) {
      this.router.navigateByUrl(`/ss/requests-manager/request-approve`).then();
    } else {
      this.router.navigateByUrl(`/ss/requests-manager/my-request`).then();
    }
  }


  _computeListConfigApprovers(calLeaves: CaculateLeavesResponse[]) {
    let allDays = 0;
    let totalDays = 0;
    let listConfigApprovers: CalLeavesApprover[] = []; // danh sách cấu hình
    calLeaves.forEach((item: CaculateLeavesResponse) => {
      if (item) {
        allDays += item.allDays ?? 0;
        totalDays += item.totalDays ?? 0;
        if (item.listApprovers) {
          item.listApprovers.forEach(approvers => {
            if (!listConfigApprovers.find(e => e.approvalOrder == approvers.approvalOrder)) {
              listConfigApprovers.push(approvers)
            }
          });
        }
      }
    });
    listConfigApprovers = _.orderBy(listConfigApprovers, ['approvalOrder'], ['asc']);
    return {
      allDays,
      totalDays,
      listConfigApprovers,
    }
  }

  /**
   * Tính Số ngày nghỉ + Tổng số ngày nghỉ
   * Người xét duyệt + Người phê duyệt
   * @param calLeaves
   */
  _calculateLeaves(calLeaves: CaculateLeavesResponse[]) {
    if (!this.isChanged) {
      return;
    }
    if (calLeaves && calLeaves.length > 0) {
      const calculateLeavesResponse: ComputeListConfigApprover = this._computeListConfigApprovers(calLeaves);
      const {allDays, totalDays} = calculateLeavesResponse;
      let {listConfigApprovers} = calculateLeavesResponse;
      const {hrLevel, approvalLevelId} = this._computedLevel(listConfigApprovers ?? []);
      let _employeeId = null;
      if (this.f['personalType'].value == PERSONAL_TYPE.OTHER) {
        const request = _.cloneDeep(this.form.value);
        if (request.employeeId && request.employeeId['employeeId']) {
          _employeeId = request.employeeId['employeeId'];
        }
      } else {
        _employeeId = this.employeeId;
      }
      const isReloadApproval = this.hrLevel != hrLevel || this.approvalLevelId != approvalLevelId || this.employeeIdApproval != _employeeId;
      if (isReloadApproval) {
        if (approvalLevelId && _employeeId && isReloadApproval) {
          this.hrLevel = hrLevel;
          this.approvalLevelId = approvalLevelId;
          this.employeeIdApproval = _employeeId;
          this.subs.push(this.requestsApproversService.getRequestsApprovers({
            employeeId: _employeeId,
            approverLevelId: approvalLevelId,
            hrLevel: hrLevel,
          }).subscribe(res => {
            const requestApprovers = res?.data || [];
            listConfigApprovers = [];
            requestApprovers.forEach((item: RequestApproverItem) => {
              item.approvers = item.approvers || [];
              item.approvalLevel = item.approvers.length > 0 ? item.approvers[0].groupLevelId : 0;
              item.approvers = item.approvers.map((e: Approver) => ({
                ...e,
                nameCode: `${e.employeeCode} - ${e.fullName} (${e.positionName})`
              }));
              listConfigApprovers?.push(<CalLeavesApprover>item);
            });
            this._buildAbsRequestApprovers(listConfigApprovers);
          }));
        } else {
          this.hrLevel = null;
          this.approvalLevelId = null;
          this.employeeIdApproval = null;
          this._buildAbsRequestApprovers([]);
        }
      }
      this.f['allDays'].setValue(allDays);
      this.f['totalDays'].setValue(totalDays);
    } else {
      this.f['allDays'].setValue(0);
      this.f['totalDays'].setValue(0);
      this.hrLevel = null;
      this.approvalLevelId = null;
      this.employeeIdApproval = null;
      this._buildAbsRequestApprovers([]);
    }
  }

  _buildAbsRequestApprovers(listApprovers: RequestApproverItem[]) {
    this.approvers.clear();
    if (listApprovers && listApprovers.length > 0) {
      let stt = 1;
      for (let idx = 0; idx < listApprovers.length; idx++) {
        const approver = listApprovers[idx];
        const isApprover = idx == listApprovers.length - 1 ? 1 : 0; // 1 là người phê duyệt, 0 - là người xét duyệt
        if (approver.isHr && (!approver.approvalLevel || approver.approvalLevel == 0)) {
          continue;
        }
        this.approvers.push(this._createApprover({
          isSuggest: 1,
          hrLevel: approver.isHr ? approver.approvalLevel : 0,
          approvers: approver.approvers,
          approvalOrder: [idx + 1],
          approvalLevel: approver.approvalLevel,
          title: isApprover ? this.translate.instant('selfService.label.approver') : `${this.translate.instant('selfService.label.reviewer')} ${stt++}`,
          isApprover: isApprover
        }));
      }
    }
  }

  onChangeReason(idx: number) {
    const _reason = this.reasons.value[idx];
    const reasonLeave = this.listReasonLeave.find((el: AbsReasonLeaves) => el.reasonLeaveId === _reason.reasonLeaveId);
    let defaultTimeOff;
    if (reasonLeave) {
      defaultTimeOff = reasonLeave.defaultTimeOff;
    }
    let _employeeId = null;
    if (this.f['personalType'].value == PERSONAL_TYPE.OTHER) {
      const request = _.cloneDeep(this.form.value);
      if (request.employeeId && request.employeeId['employeeId']) {
        _employeeId = request.employeeId['employeeId'];
      }
    } else {
      _employeeId = this.employeeId;
    }
    if (_reason && _reason.reasonLeaveId && _reason.fromTime && _reason.toTime && _employeeId) {
      _reason.employeeId = _employeeId;
      this.subs.push(this.requestLeavesService.calculateLeaves(_reason).subscribe(res => {
        const currentCaculateLeaves = this.subjectCalculateLeaves.value as Array<CaculateLeavesResponse>;
        currentCaculateLeaves[idx] = res.data;
        this.subjectCalculateLeaves.next(currentCaculateLeaves);
      }));
    } else if (_reason && _reason.reasonLeaveId && _reason.fromTime && !_reason.toTime && defaultTimeOff) {
      const item = this.reasons.at(idx);
      const endDate = moment(moment(_reason.fromTime, 'DD/MM/YYYY HH:mm:ss').add(defaultTimeOff, 'days').add(4, 'hours')).format('DD/MM/YYYY HH:mm:ss');
      item.get('toTime')?.setValue(endDate)
    } else {
      const currentCaculateLeaves: NzSafeAny = this.subjectCalculateLeaves.value as Array<CaculateLeavesResponse>;
      currentCaculateLeaves[idx] = null;
      this.subjectCalculateLeaves.next(currentCaculateLeaves);
    }
    this.checkLeave();
  }

  /**
   * Tạo Loại đơn
   * @param data
   * @returns
   */
  _createReason(data: AbsRequestLeaves | NzSafeAny) {
    const reason = this.fb.group({
      reasonLeaveId: [null, [Validators.required]],
      fromTime: [null, [Validators.required]],
      toTime: [null, [Validators.required]],
    }, {
      validators: [DateValidator.validateTwoDateTime('fromTime', 'toTime')]
    });
    if (data) {
      reason.patchValue(data);
    }
    return reason;
  }

  /**
   * Tạo người tiếp nhận
   * @param data
   * @returns
   */
  _createSupporter(data: NzSafeAny) {
    const supporter = this.fb.group({
      employeeId: [null, [Validators.required]],
      taskContent: [null, [Validators.required, Validators.maxLength(2000)]],
    })
    if (data) {
      supporter.patchValue(data);
    }
    return supporter;
  }

  /**
   * Người xét duyệt + phê duyệt
   * @param data
   * @returns
   */
  _createApprover(data: RequestApproverItem) {
    const validateCondition = data?.isApprover ? [Validators.required] : [];
    const supporter = this.fb.group({
      employeeId: [null, validateCondition],
      approvalOrder: [null],
      approvalLevel: [null],
      title: [null],
      approvers: [null],
      hrLevel: [null],
      isSuggest: [0],
      disableSuggest: [0],
      isApprover: [null],
    })
    if (data) {
      supporter.patchValue(data);
    }
    return supporter;
  }

  _detectClickItemOrChangeInput = () => {
    this.isChanged = true;
  }

  onChangePersonalType() {
    if (this.f['personalType'].value == PERSONAL_TYPE.OTHER) {
      this.f['employeeId'].clearValidators();
      this.f['employeeId'].setValidators([Validators.required, this.validatePersonalTypeOther(), this.validateEmployee()]);
    } else {
      this.f['employeeId'].clearValidators();
    }
    this.form.updateValueAndValidity();
  }

  validatePersonalTypeOther(): ValidatorFn {
    return (_control: AbstractControl): { [key: string]: boolean } | null => {
      if (!this.form) {
        return null;
      }
      const valueEmployeeId = _control.value?.employeeId ?? 0;
      if (this.f['personalType'].value == PERSONAL_TYPE.OTHER && valueEmployeeId == this.employeeId && this.f['employeeId']) {
        return {personalTypeOther: true};
      }
      return null;
    }
  }

  changeEmployee(evt: SelfEmployeeDetail) {
    let _employeeId = null;
    if (evt && this.reasons?.length > 0) {
      _employeeId = evt.employeeId;
      const listPromise = [];
      for (let idx = 0; idx < this.reasons.value.length; idx++) {
        const _reason = this.reasons.value[idx];
        if (_reason && _reason.reasonLeaveId && _reason.fromTime && _reason.toTime && _employeeId) {
          _reason.employeeId = _employeeId;
          listPromise.push(this.requestLeavesService.calculateLeaves(_reason));
        }
      }
      this.subs.push(forkJoin(listPromise).subscribe((res: BaseResponse[]) => {
        if (res?.length > 0) {
          const _reasons = this.reasons.value;
          const currentCaculateLeaves = this.subjectCalculateLeaves.value as Array<CaculateLeavesResponse>;
          for (let i = 0; i < res.length; i++) {
            const cal = res[i].data;
            const idx = _reasons.findIndex((e: AbsReasonLeaves) =>
              e.reasonLeaveId == cal.reasonLeaveId
              && e.fromTime == cal.fromTime
              && e.toTime == cal.toTime);
            if (idx >= 0) {
              currentCaculateLeaves[idx] = res[i].data;
            }
          }
          this.subjectCalculateLeaves.next(currentCaculateLeaves);
        }
      }));
    }
  }

  switchApproverSelector(approverFormGroup: AbstractControl) {
    if (approverFormGroup instanceof FormGroup) {
      approverFormGroup.controls['employeeId'].setValue(null);
    }
  }

  /**
   * Them nguoi Xet duyet/Phe duyet
   * @param idx number
   */
  addApprover(idx: number) {
    const approvers = this.approvers.value;
    const newApprover = _.cloneDeep(approvers[idx]);
    newApprover.employeeId = null;
    newApprover.isSuggest = 0;
    newApprover.disableSuggest = 1;
    approvers.splice(idx + 1, 0, newApprover);
    this.approvers.clear();
    approvers.forEach((approver: AbsRequestApprovers, idx : number) => {
      const isApprover = idx == approvers.length - 1;
      approver.isApprover = isApprover;
      approver.title = isApprover ? this.translate.instant('selfService.label.approver') : `${this.translate.instant('selfService.label.reviewer')} ${idx + 1}`
      this.approvers.push(this._createApprover(approver));
    });
  }
  
  /**
   * Xoa nguoi Xet duyet/Phe duyet
   * @param index number
   */
  removeApprover(index: number) {
    if (this.approvers.length > 1)
      this.approvers.removeAt(index);
    else {
      const row = this.approvers.controls[0].value;
      row.employeeId = null;
      this.approvers.removeAt(index);
      this.approvers.push(this._createApprover(row));
    }
    this.rebuildTitleApprover();
  }

  rebuildTitleApprover() {
    const approvers = this.approvers.value;
    this.approvers.clear();
    approvers.forEach((approver: AbsRequestApprovers, idx : number) => {
      const isApprover = idx == approvers.length - 1;
      approver.isApprover = isApprover;
      approver.title = isApprover ? this.translate.instant('selfService.label.approver') : `${this.translate.instant('selfService.label.reviewer')} ${idx + 1}`
      this.approvers.push(this._createApprover(approver));
    });
  }

  /**
   * Từ chối phê duyệt
   * @param allowApprove
   * @param footerTmpl
   */
   actionReject(allowApprove: boolean, footerTmpl: TemplateRef<NzSafeAny>) {
    this.modal = this.modalService.create(
      {
        nzWidth: 500,
        nzTitle: allowApprove ? this.translate.instant('selfService.table.action.reject') : this.translate.instant('selfService.table.action.unreview'),
        nzContent: RejectDialogComponent,
        nzComponentParams: {
          data: {
            requestId: this.form?.value?.requestId,
            allowApprove: allowApprove
          }
        },
        nzFooter: footerTmpl
      }
    );
    this.subs.push(this.modal.afterClose.subscribe(result => {
      if (result?.refresh) {
        this.toastService.success(this.translate.instant('selfService.table.action.rejectedSuccess'));
        this.router.navigateByUrl(`/ss/requests-manager/request-approve`).then();
      }
    }));
  }

  /**
   * Phê duyệt
   */
   actionApprove() {
    this.isLoading = true;
    this.subs.push(this.requestsService.approve({ requestId: this.form?.value?.requestId }).subscribe((res) => {
      this.isLoading = false;
      const { code, message } = res || {};
      if (code == 400) {
        this.toastService.error(message);
      } else {
        this.toastService.success(this.translate.instant('selfService.table.action.approvedSuccess'));
        this.router.navigateByUrl(`/ss/requests-manager/request-approve`).then();
      }
    }));
  }

}
