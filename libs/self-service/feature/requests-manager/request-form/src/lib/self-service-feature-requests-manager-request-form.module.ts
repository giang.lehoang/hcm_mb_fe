import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequestFormComponent } from './request-form/request-form.component';
import { NzFormModule } from 'ng-zorro-antd/form';
import { SharedDirectivesScrollSpyModule } from '@hcm-mfe/shared/directives/scroll-spy';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { SharedUiEmployeeDataPickerModule } from '@hcm-mfe/shared/ui/employee-data-picker';
import { SharedUiMbTextLabelModule } from '@hcm-mfe/shared/ui/mb-text-label';
import { SharedUiMbDateTimeWorkModule } from '@hcm-mfe/shared/ui/mb-date-time-work';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, NzFormModule, SharedDirectivesScrollSpyModule, TranslateModule, SharedUiMbSelectModule,
    SharedUiEmployeeDataPickerModule, SharedUiMbTextLabelModule, SharedUiMbDateTimeWorkModule, SharedUiMbButtonModule,
    SharedUiMbInputTextModule, NzSwitchModule, NzUploadModule, NzIconModule, NzCheckboxModule, FormsModule, ReactiveFormsModule, NzPopconfirmModule,
    RouterModule.forChild([
      {
        path: '',
        component: RequestFormComponent
      }
    ])
  ],
  declarations: [RequestFormComponent],
  exports: [RequestFormComponent],
})
export class SelfServiceFeatureRequestsManagerRequestFormModule {}
