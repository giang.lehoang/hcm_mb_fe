import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequestListComponent } from './request-list/request-list.component';
import { NzFormModule } from 'ng-zorro-antd/form';
import { SharedDirectivesScrollSpyModule } from '@hcm-mfe/shared/directives/scroll-spy';
import { SharedUiMbDatePickerModule } from '@hcm-mfe/shared/ui/mb-date-picker';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { SharedUiMbTableWrapModule } from '@hcm-mfe/shared/ui/mb-table-wrap';
import { SharedUiMbTableModule } from '@hcm-mfe/shared/ui/mb-table';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzTableModule } from 'ng-zorro-antd/table';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { SharedUiMbSelectCheckAbleModule } from '@hcm-mfe/shared/ui/mb-select-check-able';

@NgModule({
  imports: [CommonModule, SharedUiLoadingModule, NzFormModule, SharedDirectivesScrollSpyModule, SharedUiMbDatePickerModule, TranslateModule, SharedUiMbSelectModule,
    SharedUiMbButtonModule, SharedUiMbTableWrapModule, SharedUiMbTableModule, NzTagModule, NzButtonModule, NzDropDownModule,
    NzIconModule, NzPopconfirmModule, NzModalModule, NzDatePickerModule, NzTableModule, FormsModule, ReactiveFormsModule, NzToolTipModule,
    SharedUiMbSelectCheckAbleModule,
    RouterModule.forChild([
      {
        path: '',
        component: RequestListComponent
      }
    ])
  ],
  declarations: [RequestListComponent],
  exports: [RequestListComponent],
})
export class SelfServiceFeatureRequestsManagerRequestListModule {}
