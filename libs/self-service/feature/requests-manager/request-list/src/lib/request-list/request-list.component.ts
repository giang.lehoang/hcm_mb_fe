import {Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { NzModalService } from 'ng-zorro-antd/modal';
import { ToastrService } from 'ngx-toastr';
import { forkJoin, Subscription } from 'rxjs';
import * as _ from 'lodash';
import {
  LEAVE_TYPE,
  PAGE_NAME,
  REQUEST_STATUS,
  REQUEST_STATUS_DATASOURCE,
  TAG
} from '@hcm-mfe/self-service/data-access/common';
import { BaseResponse, MBTableConfig, Pagination, User, UserLogin } from '@hcm-mfe/shared/data-access/models';
import { ReasonLeavesService, RequestsService } from '@hcm-mfe/self-service/data-access/services';
import { LookupValuesService } from '@hcm-mfe/personal-tax/data-access/services';
import { DateValidator } from '@hcm-mfe/shared/common/validators';
import { StorageService } from '@hcm-mfe/shared/common/store';
import { HTTP_STATUS_CODE, STORAGE_NAME, SYSTEM_FORMAT_DATA } from '@hcm-mfe/shared/common/constants';
import { SessionKey } from '@hcm-mfe/shared/common/enums';
import {
  AbsDayLeaveYear,
  AbsRequestApprovers,
  AbsRequestLeaves,
  AbsRequests
} from '@hcm-mfe/self-service/data-access/models';


@Component({
  selector: 'app-request-list',
  templateUrl: './request-list.component.html',
  styleUrls: ['./request-list.component.scss']
})
export class RequestListComponent implements OnInit, OnDestroy {
  REQUEST_STATUS_DATASOURCE = REQUEST_STATUS_DATASOURCE;
  REQUEST_STATUS = REQUEST_STATUS;
  tableConfig!: MBTableConfig;
  pagination = new Pagination();
  searchResult: AbsRequests[] = [];
  form: FormGroup;
  listReasonLeave: AbsRequestLeaves[] = []; // Loại đơn
  subs: Subscription[] = [];
  isSubmitted = false;
  TAG = TAG;
  @ViewChild('statusTmpl', { static: true }) status!: TemplateRef<NzSafeAny>;
  @ViewChild('actionTmpl', { static: true }) action!: TemplateRef<NzSafeAny>;
  @ViewChild('timeLeaveTmpl', { static: true }) timeLeave!: TemplateRef<NzSafeAny>;
  @ViewChild('reasonTmpl', { static: true }) reason!: TemplateRef<NzSafeAny>;
  @ViewChild('reviewersTmpl', { static: true }) reviewers!: TemplateRef<NzSafeAny>;
  @ViewChild('approversTmpl', { static: true }) approvers!: TemplateRef<NzSafeAny>;

  isModalLeaveShow = false;
  isLoading = false;
  yearSelect = new Date();
  listDayLeaveInYear: AbsDayLeaveYear[] = [];

  employeeId: number;
  userLogin: UserLogin = new UserLogin();
  currentUser?: User;

  constructor(
    private route: Router,
    private fb: FormBuilder,
    private requestsService: RequestsService,
    private lookupValuesService: LookupValuesService,
    private reasonLeavesService: ReasonLeavesService,
    private translate: TranslateService,
    private modal: NzModalService,
    private toastrService: ToastrService
  ) {
    this.form = this.fb.group({
      leaveType: [LEAVE_TYPE.LEAVE],
      fromTime: [null],
      toTime: [null],
      listStatus: [[]],
      reasonLeaveId: [[]],
      startRecord: [0],
      pageSize: [10],
      pageName: [PAGE_NAME.EMPLOYEE]
    }, {
      validators: DateValidator.validateTwoDate('fromTime', 'toTime', 'greaterAndEqual')
    });

    this.userLogin = StorageService.get(STORAGE_NAME.USER_LOGIN);
    if (localStorage.getItem(SessionKey.CURRENCY_USER)) {
      this.currentUser = JSON.parse(localStorage.getItem(SessionKey.CURRENCY_USER) ?? '');
    }
    this.employeeId = this.userLogin.employeeId ?? this.currentUser?.empId ?? 1;
  }

  ngOnInit(): void {
    this.initTable();

    this.subs.push(forkJoin([
      this.reasonLeavesService.getAllReasonLeaves(LEAVE_TYPE.LEAVE)
    ]).subscribe(
      ([listReasonLeave]) => {
        this.getListReasonLeave(listReasonLeave);
        this.doSearch(1);
      }, () => {
        this.toastrService.error(this.translate.instant('common.notification.error'));
      }
    ));
  }

  /**
   * Thêm mới
   */
  createRequest() {
    this.route.navigateByUrl(`/ss/requests-manager/create`);
  }

  /**
   * Tìm kiếm
   * @param pageIndex
   */
  doSearch(pageIndex: number) {
    this.isSubmitted = true;
    this.isLoading = true;
    if (this.form.valid) {
      this.pagination.pageNumber = pageIndex ?? 1;
      this.tableConfig.pageIndex = pageIndex ? this.tableConfig.pageIndex : 1;
      this.tableConfig.loading = true;
      const formData = _.cloneDeep(this.form.value);
      formData.fromTime = formData.fromTime ? moment(formData.fromTime).format(SYSTEM_FORMAT_DATA.DATE_TIME_FORMAT) : null;
      formData.toTime = formData.toTime ? moment(formData.toTime).format(SYSTEM_FORMAT_DATA.DATE_TIME_FORMAT) : null;
      if(formData.reasonLeaveId){
        formData.reasonLeaveId = formData.reasonLeaveId.join(',');
      }
      if(formData.listStatus){
        formData.listStatus = formData.listStatus.join(',');
      }
      this.subs.push(this.requestsService.search({ ...formData, ...this.pagination.getCurrentPage() }).subscribe((res: BaseResponse) => {
        this.isLoading = false;
        this.searchResult = this.beforeRender(res.data.listData);
        this.tableConfig.pageIndex = pageIndex;
        this.tableConfig.total = res.data.count;
        this.tableConfig.loading = false;
      }));
    }
  }

  beforeRender(listData: AbsRequests[]) {
    if (!listData) {
      return listData;
    }
    return listData.map((item: AbsRequests) => {
      if (item.listAbsRequestLeaves) {
        item.listAbsRequestLeavesText = item.listAbsRequestLeaves.map((leave: AbsRequestLeaves) => {
          const { fromTime, toTime, reasonLeaveId } = leave;
          const leaveTypeName = this.listReasonLeave.find((e: AbsRequestLeaves) => e.reasonLeaveId === reasonLeaveId)?.name;
          return this.translate.instant('selfService.table.requestLeavesText', {
            leaveTypeName: leaveTypeName,
            form: this._formatDateTime(fromTime),
            to: this._formatDateTime(toTime)
          });
        });
        item.allDays = this.translate.instant('selfService.table.numberDate', { param: item.allDays ?? 0 });
        item.totalDays = this.translate.instant('selfService.table.numberDate', { param: item.totalDays ?? 0 });
        if (item.listAbsRequestApprovers) {
          item.listAbsRequestApprovers = item.listAbsRequestApprovers.filter((e: AbsRequestApprovers) => e.employeeId);
          item.listAbsRequestApprovers = _.orderBy(item.listAbsRequestApprovers, ['approvalOrder'], ['asc']);
          const numApprover = item.listAbsRequestApprovers.length;
          item.listReviewer = item.listAbsRequestApprovers.slice(0, numApprover - 1).map((e: AbsRequestApprovers) => ({ ...e, fullName: `${e.fullName}-${e.employeeCode}`}));
          item.listApprover = item.listAbsRequestApprovers.slice(numApprover - 1, numApprover).map((e: AbsRequestApprovers) => ({ ...e, fullName: `${e.fullName}-${e.employeeCode}`}));
        }
      }
      return item;
    });
  }

  _formatDateTime(dateInput: string | undefined) {
    const time = moment(dateInput, SYSTEM_FORMAT_DATA.DATE_TIME_FORMAT).format('HH:mm');
    const date = moment(dateInput, SYSTEM_FORMAT_DATA.DATE_TIME_FORMAT).format(SYSTEM_FORMAT_DATA.DATE_FORMAT);
    return this.translate.instant('selfService.table.dateTimeLabel', { time, date });
  }

  private initTable(): void {
    this.pagination.pageNumber = 1;
    this.tableConfig = {
      headers: [
        // {
        //   title: 'selfService.table.requestLeave',
        //   field: 'requestLeave',
        //   tdTemplate: this.reason,
        //   width: 180
        // },
        {
          title: 'selfService.label.requestsPerson',
          field: 'fullName',
          width: 120
        },
        {
          title: 'selfService.table.timeLeave',
          field: 'requestLeave',
          tdTemplate: this.timeLeave,
          width: 230
        },
        {
          title: 'selfService.table.numberDayLeave',
          field: 'allDays',
          width: 60,
          show: false
        },
        {
          title: 'selfService.table.numberDayWorkLeave',
          field: 'totalDays',
          width: 60
        },
        {
          title: 'selfService.table.reviewer',
          field: 'reviewer',
          tdTemplate: this.reviewers,
          width: 150
        },
        {
          title: 'selfService.table.approver',
          field: 'approver',
          tdTemplate: this.approvers,
          width: 150
        },
        {
          title: 'selfService.label.reasonType',
          field: 'reasonDetail',
          width: 150,
          show: false
        },
        {
          title: 'selfService.table.status',
          field: 'status',
          tdTemplate: this.status,
          tdClassList: ['text-nowrap', 'text-center'],
          width: 100,
          fixed: window.innerWidth > 1024,
          fixedDir: 'right'
        },
        {
          title: '',
          field: 'action',
          tdClassList: ['text-nowrap', 'text-center'],
          width: 30,
          show: true,
          tdTemplate: this.action,
          fixed: window.innerWidth > 1024,
          fixedDir: 'right'
        }
      ],
      total: 0,
      needScroll: false,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  /**
   * Xử lý lấy các datasource
   */
  // handleGetCatalog() {
  //   this.getListReasonType();
  //   this.getListReasonLeave();
  // }

  /**
   * Lấy danh mục Lý do
   */
  // getListReasonType(res) {
  //   this.subs.push(
  //     this.listReasonType = res.data
  //   );
  // }

  /**
   * Lấy danh mục Loại đơn
   */
  getListReasonLeave(res: BaseResponse) {
      this.listReasonLeave = res.data
  }

  getStatus(value: number) {
    return this.REQUEST_STATUS_DATASOURCE.find(status => status.value === value)?.label;
  }

  /**
   * Xóa
   * @param requestId
   */
  deleteItem(requestId: number) {
    this.isLoading = true;
    this.subs.push(this.requestsService.deleteRecord(requestId).subscribe((res: BaseResponse) => {
      this.isLoading = false;
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.doSearch(1);
        this.toastrService.success(this.translate.instant('common.notification.deleteSuccess'));
      }
    }, () => {
      this.toastrService.error(this.translate.instant('common.notification.deleteError'));
    }));
  }

  submitRequest(requestId: number) {
    this.isLoading = true;
    this.requestsService.submitRequest(requestId).subscribe((res: BaseResponse) => {
      this.isLoading = false;
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.doSearch(1);
        this.toastrService.success(this.translate.instant('common.notification.sendBrowseSuccess'));
      }
    }, () => {
      this.toastrService.error(this.translate.instant('common.notification.updateError'));
    })
  }
  /**
   * Cập nhật
   * @param requestId
   */
  onUpdate(requestId: number) {
    this.route.navigateByUrl(`/ss/requests-manager/update?requestId=${requestId}`);
  }

  /**
   * Điều chỉnh đơn
   * @param requestId
   */
  actionAjust(requestId: number) {
    this.route.navigateByUrl(`/ss/requests-manager/ajust?requestId=${requestId}`);
  }

  /**
   * Hủy đơn
   * @param requestId
   */
  actionCancel(requestId: number) {
    this.isLoading = true;
    this.subs.push(this.requestsService.cancel(requestId).subscribe((res: BaseResponse) => {
      this.isLoading = false;
      if (res.code === HTTP_STATUS_CODE.OK) {
        this.doSearch(1);
        this.toastrService.success(this.translate.instant('selfService.table.action.cancelSuccess'));
      }
    }, () => {
      this.toastrService.error(this.translate.instant('selfService.table.action.cancelError'));
    }));
  }

  /**
   * Check điều kiện show action hủy
   * @param status
   */
  showActionCancel(status: number) {
    return [REQUEST_STATUS.WAIT_APPROVE, REQUEST_STATUS.APPROVED].includes(status);
  }


  /**
   * Xem chi tiết
   * @param requestId
   */
  actionViewDetail(requestId: number) {
    this.route.navigateByUrl(`/ss/requests-manager/detail?requestId=${requestId}`);
  }

  onLeaveDetailModalClick() {
    this.isModalLeaveShow = true;
    this.subs.push(this.reasonLeavesService.getLeaveInfo(this.employeeId, new Date().getFullYear()).subscribe({
      next: (res) => {
        this.listDayLeaveInYear = res.data;
      }
    }));
  }

  onCancelLeaveDetailModal() {
    this.isModalLeaveShow = false;
    this.yearSelect = new Date();
  }

  onYearChange(date: Date) {
    const year = date.getFullYear();
    this.subs.push(this.reasonLeavesService.getLeaveInfo(this.employeeId, year).subscribe({
      next: (res) => {
        this.listDayLeaveInYear = res.data;
      }
    }));
  }

  ngOnDestroy(): void {
    this.subs.forEach(sub => sub.unsubscribe());
  }
}
