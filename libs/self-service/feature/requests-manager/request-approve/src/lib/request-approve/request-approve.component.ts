import { AfterViewInit, Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import * as _ from 'lodash';
import {
  Constant,
  LEAVE_TYPE,
  PAGE_NAME,
  REQUEST_APPROVER_STATUS,
  REQUEST_STATUS,
  REQUEST_APPROVE_STATUS_DATASOURCE,
  TAG
} from '@hcm-mfe/self-service/data-access/common';
import { BaseResponse, CatalogModel, MBTableConfig, Pagination } from '@hcm-mfe/shared/data-access/models';
import { ReasonLeavesService, RequestsService } from '@hcm-mfe/self-service/data-access/services';
import { LookupValuesService } from '@hcm-mfe/personal-tax/data-access/services';
import { DateValidator } from '@hcm-mfe/shared/common/validators';
import { SYSTEM_FORMAT_DATA } from '@hcm-mfe/shared/common/constants';
import { RejectDialogComponent } from '@hcm-mfe/self-service/feature/requests-manager/reject-dialog';
import {
  AbsReasonLeaves,
  AbsRequestApproversDTO,
  AbsRequestDTO,
  AbsRequestLeavesDTO
} from '@hcm-mfe/self-service/data-access/models';

@Component({
  selector: 'app-request-approve',
  templateUrl: './request-approve.component.html',
  styleUrls: ['./request-approve.component.scss']
})
export class RequestApproveComponent implements OnInit, AfterViewInit, OnDestroy {
  form: FormGroup;

  tableConfig!: MBTableConfig;
  pagination = new Pagination();
  isLoading=true;

  searchResult: AbsRequestDTO[] = [];
  listReasonType: CatalogModel[] = []; // Lý do
  listReasonLeave: AbsReasonLeaves[] = []; // Loại đơn
  listRequestId: number[] = [];

  modal!: NzModalRef;
  isSubmitted = false;

  TAG = TAG;
  REQUEST_APPROVE_STATUS_DATASOURCE = REQUEST_APPROVE_STATUS_DATASOURCE;
  REQUEST_STATUS = REQUEST_STATUS;

  @ViewChild('statusTmpl', { static: true }) status!: TemplateRef<NzSafeAny>;
  @ViewChild('actionTmpl', { static: true }) action!: TemplateRef<NzSafeAny>;
  @ViewChild('timeLeaveTmpl', { static: true }) timeLeave!: TemplateRef<NzSafeAny>;
  @ViewChild('reasonTmpl', { static: true }) reason!: TemplateRef<NzSafeAny>;
  @ViewChild('reviewersTmpl', { static: true }) reviewers!: TemplateRef<NzSafeAny>;
  @ViewChild('approversTmpl', { static: true }) approvers!: TemplateRef<NzSafeAny>;
  @ViewChild('selectTmpl', { static: true }) select!: TemplateRef<NzSafeAny>;
  @ViewChild('footerRejectTmpl', { static: true }) footerReject!: TemplateRef<NzSafeAny>;

  subs: Subscription[] = [];

  constructor(
    private route: Router,
    private fb: FormBuilder,
    private requestsService: RequestsService,
    private lookupValuesService: LookupValuesService,
    private reasonLeavesService: ReasonLeavesService,
    private translate: TranslateService,
    private toastService: ToastrService,
    private modalService: NzModalService
  ) {
    this.form = this.fb.group({
      leaveType: [LEAVE_TYPE.LEAVE],
      fromTime: [null],
      toTime: [null],
      listStatus: [[REQUEST_STATUS.REQUEST_CANCEL, REQUEST_STATUS.WAIT_APPROVE]],
      keySearch: [''],
      startRecord: [0],
      pageSize: [10],
      isApprovePage: [1],
      pageName: [PAGE_NAME.APPROVE]
    }, {
      validators: DateValidator.validateTwoDate('fromTime', 'toTime', 'greaterAndEqual')
    });
    this.handleGetCatalog();
  }

  ngOnInit(): void {
    this.initTable();
  }

  ngAfterViewInit(): void {
    this.doSearch(1);
  }

  ngOnDestroy() {
    this.modal?.destroy();
    this.subs.forEach(sub => sub.unsubscribe());
  }

  /**
   * Tìm kiếm
   * @param pageIndex
   */
  doSearch(pageIndex: number | any) {
    this.isSubmitted = true;
    if (this.form.valid) {
      this.pagination.pageNumber = pageIndex ?? 1;
      this.tableConfig.pageIndex = pageIndex ? this.tableConfig.pageIndex : 1;
      this.tableConfig.loading = true;
      const formData = _.cloneDeep(this.form.value);
      formData.fromTime = formData.fromTime ? moment(formData.fromTime).format(SYSTEM_FORMAT_DATA.DATE_TIME_FORMAT) : null;
      formData.toTime = formData.toTime ? moment(formData.toTime).format(SYSTEM_FORMAT_DATA.DATE_TIME_FORMAT) : null;
      this.isLoading = true;
      this.requestsService.search({ ...formData, ...this.pagination.getCurrentPage() }).subscribe((res: BaseResponse) => {
        this.isLoading = false;
        this.searchResult = this.beforeRender(res.data.listData);
        this.tableConfig.pageIndex = pageIndex;
        this.tableConfig.total = res.data.count;
        this.tableConfig.loading = false;
      });
    }
  }

  beforeRender(listData: AbsRequestDTO[]) {
    if (!listData) {
      return listData;
    }
    return listData.map((item: AbsRequestDTO) => {
      if (item.listAbsRequestLeaves) {
        item.listAbsRequestLeavesText = item.listAbsRequestLeaves.map((leave: AbsRequestLeavesDTO) => {
          const { fromTime, toTime, reasonLeaveId } = leave;
          const leaveTypeName = this.listReasonLeave.find((e: AbsReasonLeaves) => e.reasonLeaveId === reasonLeaveId)?.name;
          return this.translate.instant('selfService.table.requestLeavesText', {
            leaveTypeName: leaveTypeName,
            form: this._formatDateTime(fromTime),
            to: this._formatDateTime(toTime)
          });
        });
        item.allDays = this.translate.instant('selfService.table.numberDate', { param: item.allDays ?? 0 });
        item.totalDays = this.translate.instant('selfService.table.numberDate', { param: item.totalDays ?? 0 });
        if (item.listAbsRequestApprovers) {
          item.listAbsRequestApprovers = item.listAbsRequestApprovers.filter((e: AbsRequestApproversDTO) => e.employeeId);
          item.listAbsRequestApprovers = _.orderBy(item.listAbsRequestApprovers, ['approvalOrder'], ['asc']);
          const numApprover = item.listAbsRequestApprovers.length;
          const lstReviewer = item.listAbsRequestApprovers.slice(0, numApprover - 1);
          const lstApprover = item.listAbsRequestApprovers.slice(numApprover - 1, numApprover);
          item.listReviewer = lstReviewer.map((e: AbsRequestApproversDTO) => ({ ...e, fullName: `${e.fullName}-${e.employeeCode}`}));
          item.listApprover = lstApprover.map((e: AbsRequestApproversDTO) => ({ ...e, fullName: `${e.fullName}-${e.employeeCode}`}));
          if (lstApprover.find((e: AbsRequestApproversDTO) => e.employeeId == item.viewEmployeeId)) {
            item.allowApprove = true; // xét duyệt hoặc phê duyệt
          }
          const approver = item.listAbsRequestApprovers.find((e: AbsRequestApproversDTO) => e.employeeId == item.viewEmployeeId);
          item.viewButtonAction = !!item.status && approver && approver.status == REQUEST_APPROVER_STATUS.WAIT_APPROVE
            && this.showActionApproveReject(item.status); // điều kiện hiển thị action
        }
      }
      return item;
    });
  }

  _formatDateTime(dateInput: string | undefined) {
    if (!dateInput) {
      return '';
    }
    const time = moment(dateInput, SYSTEM_FORMAT_DATA.DATE_TIME_FORMAT).format('HH:mm');
    const date = moment(dateInput, SYSTEM_FORMAT_DATA.DATE_TIME_FORMAT).format(SYSTEM_FORMAT_DATA.DATE_FORMAT);
    return this.translate.instant('selfService.table.dateTimeLabel', { time, date });
  }

  private initTable(): void {
    this.pagination.pageNumber = 1;
    this.tableConfig = {
      headers: [
        {
          title: 'selfService.table.select',
          field: 'select',
          width: 35,
          tdTemplate: this.select,
          thClassList: ['text-nowrap', 'text-center'],
          tdClassList: ['text-nowrap', 'text-center'],
          show: true,
          fixed: window.innerWidth > 1024,
          fixedDir: 'left'
        },
        {
          title: 'selfService.label.requestsPerson',
          field: 'fullName',
          width: 120
        },
        {
          title: 'selfService.table.timeLeave',
          field: 'requestLeave',
          tdTemplate: this.timeLeave,
          width: 230
        },
        {
          title: 'selfService.table.numberDayLeave',
          field: 'allDays',
          width: 60,
          show: false
        },
        {
          title: 'selfService.table.numberDayWorkLeave',
          field: 'totalDays',
          width: 60
        },
        {
          title: 'selfService.table.reviewer',
          field: 'reviewer',
          tdTemplate: this.reviewers,
          width: 150,
          show: false
        },
        {
          title: 'selfService.table.approver',
          field: 'approver',
          tdTemplate: this.approvers,
          width: 150,
          show: false
        },
        {
          title: 'selfService.label.reasonType',
          field: 'reasonDetail',
          width: 150
        },
        {
          title: 'selfService.table.status',
          field: 'status',
          tdTemplate: this.status,
          tdClassList: ['text-nowrap', 'text-center'],
          width: 90,
          fixed: window.innerWidth > 1024,
          fixedDir: 'right'
        },
        {
          title: '',
          field: 'action',
          tdClassList: ['text-nowrap', 'text-center'],
          width: 30,
          show: true,
          tdTemplate: this.action,
          fixed: window.innerWidth > 1024,
          fixedDir: 'right'
        }
      ],
      total: 0,
      needScroll: false,
      loading: false,
      size: 'small',
      pageSize: this.pagination.pageSize,
      pageIndex: 1
    };
  }

  /**
   * Xử lý lấy các datasource
   */
  handleGetCatalog() {
    this.getListReasonType();
    this.getListReasonLeave();
  }

  /**
   * Lấy danh mục Lý do
   */
  getListReasonType() {
    this.subs.push(
      this.lookupValuesService.getCatalog(Constant.CATALOGS.LY_DO_NGHI).subscribe(res => {
        const response: BaseResponse = res;
        this.listReasonType = response.data;
      })
    );
  }

  /**
   * Lấy danh mục Loại đơn
   */
  getListReasonLeave() {
    this.subs.push(
      this.reasonLeavesService.getAllReasonLeaves(LEAVE_TYPE.LEAVE).subscribe(res => {
        const response: BaseResponse = res;
        this.listReasonLeave = response.data;
      })
    );
  }

  getStatus(value: number) {
    return this.REQUEST_APPROVE_STATUS_DATASOURCE.find(status => status.value === value)?.label;
  }

  getReasonLabel(reasonCode: string) {
    return this.listReasonType.find(status => status.value === reasonCode)?.label;
  }

  /**
   * Phê duyệt
   * @param requestId
   */
  actionApprove(requestId: number) {
    this.isLoading = true;
    this.subs.push(this.requestsService.approve({ requestId: requestId }).subscribe((res) => {
      this.isLoading = false;
      const { code, message } = res || {};
      if (code == 400) {
        this.toastService.error(message);
      } else {
        this.toastService.success(this.translate.instant('selfService.table.action.approvedSuccess'));
        this.doSearch(null);
      }
    }));
  }

  /**
   * Từ chối phê duyệt
   * @param requestId
   * @param allowApprove
   * @param footerTmpl
   */
  actionReject(requestId: number, allowApprove: boolean, footerTmpl: TemplateRef<NzSafeAny>) {
    this.modal = this.modalService.create(
      {
        nzWidth: 500,
        nzTitle: allowApprove ? this.translate.instant('selfService.table.action.reject') : this.translate.instant('selfService.table.action.unreview'),
        nzContent: RejectDialogComponent,
        nzComponentParams: {
          data: {
            requestId: requestId,
            allowApprove: allowApprove
          }
        },
        nzFooter: footerTmpl
      }
    );
    this.subs.push(this.modal.afterClose.subscribe(result => {
      if (result?.refresh) {
        this.toastService.success(this.translate.instant('selfService.table.action.rejectedSuccess'));
        this.doSearch(null);
      }
    }));
  }

  /**
   * Check điều kiện show action Phê duyệt/Từ chối
   * @param status
   */
  showActionApproveReject(status: number) {
    return [REQUEST_STATUS.WAIT_APPROVE, REQUEST_STATUS.REQUEST_CANCEL].includes(status);
  }

  /**
   * Xem chi tiết
   * @param requestId
   */
  actionViewDetail(requestId: number) {
    this.route.navigateByUrl(`/ss/requests-manager/detail?requestId=${requestId}&pageName=${PAGE_NAME.APPROVE}`).then();
  }

  onCheckChange(event: Event) {
    const target = event.target as HTMLInputElement;
    const id = parseInt(target.value);
    if (target.checked) {
      this.listRequestId.push(id);
    } else {
      this.listRequestId.splice(this.listRequestId.indexOf(id), 1);
    }
  }

  /**
   * Phê duyệt theo lô
   */
  approveByList() {
    this.isLoading = true;
    this.subs.push(this.requestsService.approve({ requestIds: this.listRequestId }).subscribe((res) => {
      this.isLoading = false;
      const { code, message } = res || {};
      if (code == 400) {
        this.toastService.error(message);
      } else {
        this.toastService.success(this.translate.instant('selfService.table.action.approvedSuccess'));
        this.doSearch(null);
        this.listRequestId = [];
      }
    }));
  }

  /**
   * Từ chối phê duyệt theo lô
   */
  openRejectByList() {
    this.modal = this.modalService.create(
      {
        nzWidth: 500,
        nzTitle: this.translate.instant('selfService.table.action.reject'),
        nzContent: RejectDialogComponent,
        nzComponentParams: {
          data: {
            requestIds: this.listRequestId
          }
        },
        nzFooter: this.footerReject
      }
    );
    this.subs.push(this.modal.afterClose.subscribe(result => {
      if (result?.refresh) {
        this.toastService.success(this.translate.instant('selfService.table.action.rejectedSuccess'));
        this.doSearch(null);
        this.listRequestId = [];
      }
    }));
  }

}
