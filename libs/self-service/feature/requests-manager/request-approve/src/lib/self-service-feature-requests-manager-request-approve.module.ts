import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequestApproveComponent } from './request-approve/request-approve.component';
import { NzFormModule } from 'ng-zorro-antd/form';
import { SharedDirectivesScrollSpyModule } from '@hcm-mfe/shared/directives/scroll-spy';
import { SharedUiMbDatePickerModule } from '@hcm-mfe/shared/ui/mb-date-picker';
import { TranslateModule } from '@ngx-translate/core';
import { SharedUiMbSelectModule } from '@hcm-mfe/shared/ui/mb-select';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { SharedUiMbTableWrapModule } from '@hcm-mfe/shared/ui/mb-table-wrap';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';
import { SharedUiMbTableModule } from '@hcm-mfe/shared/ui/mb-table';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedUiLoadingModule } from '@hcm-mfe/shared/ui/loading';
import {NzModalModule} from "ng-zorro-antd/modal";
import { SharedUiMbSelectCheckAbleModule } from '@hcm-mfe/shared/ui/mb-select-check-able';
import { SharedUiMbInputTextModule } from '@hcm-mfe/shared/ui/mb-input-text';
import {NzToolTipModule} from "ng-zorro-antd/tooltip";

@NgModule({
    imports: [CommonModule, SharedUiLoadingModule, NzFormModule, SharedDirectivesScrollSpyModule, SharedUiMbDatePickerModule, TranslateModule,
        SharedUiMbSelectModule, SharedUiMbButtonModule, SharedUiMbTableWrapModule, NzPopconfirmModule, SharedUiMbTableModule,
        NzTagModule, NzButtonModule, NzDropDownModule, NzIconModule, FormsModule, ReactiveFormsModule, NzToolTipModule, NzModalModule,
        SharedUiMbSelectCheckAbleModule, SharedUiMbInputTextModule,
        RouterModule.forChild([
            {
                path: '',
                component: RequestApproveComponent
            }
        ])
    ],
  declarations: [RequestApproveComponent],
  exports: [RequestApproveComponent],
})
export class SelfServiceFeatureRequestsManagerRequestApproveModule {}
