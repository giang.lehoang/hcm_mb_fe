import { Injectable } from '@angular/core';
import { UrlConstant } from '@hcm-mfe/shared/common/constants';
import { BaseService } from '@hcm-mfe/shared/common/base-service';

@Injectable({
  providedIn: 'root' // just before your class
})
export class DashboardInfoService extends BaseService {

  public getBanner(type: string) {
    const url = UrlConstant.API_VERSION + UrlConstant.DASHBOARD.BANNER + '/' + type;
    return this.get(url);
  }

  public getArticle() {
    const url = UrlConstant.API_VERSION + UrlConstant.DASHBOARD.ARTICLE;
    return this.get(url);
  }

}
