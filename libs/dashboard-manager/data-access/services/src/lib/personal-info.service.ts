import {Injectable} from "@angular/core";
import { PersonalInfo } from '@hcm-mfe/shared/data-access/models';
import { UrlConstant } from '@hcm-mfe/shared/common/constants';
import { BaseService } from '@hcm-mfe/shared/common/base-service';

@Injectable({
  providedIn: 'root' // just before your class
})
export class PersonalInfoService extends BaseService {

  public getPersonalInfo(employeeId: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.EMPLOYEES.PREFIX + '/' + employeeId + UrlConstant.EMPLOYEES.PERSONAL_INFO;
    return this.get(url);
  }

  public savePersonalInfo(employeeId: number, personalInfo: PersonalInfo) {
    const url = UrlConstant.API_VERSION + UrlConstant.EMPLOYEES.PREFIX + '/' + employeeId + UrlConstant.EMPLOYEES.PERSONAL_INFO;
    return this.post(url, personalInfo);
  }

  public getAvatar(employeeId: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.EMPLOYEES.PREFIX + '/' + employeeId + UrlConstant.EMPLOYEES.AVATAR;
    return this.get(url);
  }

  public uploadAvatar(employeeId: number, formData: FormData) {
    const url = UrlConstant.API_VERSION + UrlConstant.EMPLOYEES.PREFIX + '/' + employeeId + UrlConstant.EMPLOYEES.AVATAR;
    return this.post(url, formData);
  }

  public deleteAvatar(employeeId: number) {
    const url = UrlConstant.API_VERSION + UrlConstant.EMPLOYEES.PREFIX + '/' + employeeId + UrlConstant.EMPLOYEES.AVATAR;
    return this.delete(url);
  }
}
