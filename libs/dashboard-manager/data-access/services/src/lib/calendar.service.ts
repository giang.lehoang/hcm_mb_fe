import { Injectable } from '@angular/core';
import { MICRO_SERVICE, UrlConstant } from '@hcm-mfe/shared/common/constants';
import { BaseService } from '@hcm-mfe/shared/common/base-service';

@Injectable({
  providedIn: 'root' // just before your class
})
export class CalendarService extends BaseService {

  public getListTimeKeeping(params: any) {
    const url = UrlConstant.API_VERSION + UrlConstant.CALENDAR.PREFIX;
    return this.get(url, { params }, MICRO_SERVICE.ABS_MANAGEMENT);
  }

}
