import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseService } from '@hcm-mfe/shared/common/base-service';

@NgModule({
  imports: [CommonModule],
  providers: [BaseService]
})
export class DashboardManagerDataAccessServicesModule {}
