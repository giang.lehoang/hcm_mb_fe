export class Constant {
  public static readonly LIST_FILE = [
    {url: "assets/upload/BM.QT.CNTT.MB.52.08- Tai lieu huong dan su dung_QL TTCN.pdf", fileName:"Tài liệu hướng dẫn sử dụng QL TTCN"},
    {url: "assets/upload/BM.QT.CNTT.MB.52.08_ Tai lieu HDSD HR_Mô hình và kế hoạch.pdf", fileName:"Tài liệu hướng dẫn sử dụng Mô hình và kế hoạch"},
    {url: "assets/upload/BM.QT.CNTT.MB.52.08- Tai lieu huong dan su dung_Qly Vang mat_nghi phep.pdf", fileName:"Tài liệu hướng dẫn sử dụng QL Vắng mặt nghỉ phép"},
    {url: "assets/upload/BM.QT.CNTT.MB 52 08- Tai lieu huong dan su dung_HCM 2_QLMT - CBNV - Xac nhan Bo chi tieu.pdf", fileName:"Tài liệu hướng dẫn sử dụng xác nhận bộ chỉ tiêu(CBNV)"},
    {url: "assets/upload/BM.QT.CNTT.MB 52 08- Tai lieu huong dan su dung_HCM 2_QLMT - CBQL - Giao Bo chi tieu.pdf", fileName:"Tài liệu hướng dẫn sử dụng giao bộ chỉ tiêu(CBQL)"}
  ];
}
