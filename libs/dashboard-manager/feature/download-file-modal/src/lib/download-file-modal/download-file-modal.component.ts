import {Component, Input, OnInit} from '@angular/core';
import {Pagination} from "@hcm-mfe/shared/data-access/models";

@Component({
  selector: 'app-download-file-modal',
  templateUrl: './download-file-modal.component.html',
  styleUrls: ['./download-file-modal.component.scss']
})
export class DownloadFileModalComponent implements OnInit {
  listFile = [];
  pagination = new Pagination();

  constructor() { }

  ngOnInit(): void {
  }

  downloadFile(url: string, fileName: string) {
    const a = document.createElement("a");
    a.href = url;
    a.download = fileName;
    a.click();
  }
}
