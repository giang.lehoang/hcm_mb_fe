import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DownloadFileModalComponent} from "./download-file-modal/download-file-modal.component";
import {NzTableModule} from "ng-zorro-antd/table";
import {TranslateModule} from "@ngx-translate/core";
import {NzIconModule} from "ng-zorro-antd/icon";

@NgModule({
  imports: [CommonModule, NzTableModule, TranslateModule, NzIconModule],
  declarations: [DownloadFileModalComponent],
  exports: [DownloadFileModalComponent],
})
export class DashboardManagerFeatureDownloadFileModalModule {}
