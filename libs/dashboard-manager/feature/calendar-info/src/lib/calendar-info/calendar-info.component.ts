import { AfterViewInit, Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { environment } from '@hcm-mfe/shared/environment';
import { CalendarService } from '@hcm-mfe/dashboard-manager/data-access/services';
import { HttpParams } from '@angular/common/http';
import { StorageService } from '@hcm-mfe/shared/common/store';
import { STORAGE_NAME } from '@hcm-mfe/shared/common/constants';
import { SessionKey } from '@hcm-mfe/shared/common/enums';
import { User, UserLogin } from '@hcm-mfe/shared/data-access/models';
import { UserService } from '@hcm-mfe/shared/core';
import { Utils } from '@hcm-mfe/shared/common/utils';

export interface SelectDate {
  date: string;
  type: number;
  title: string;
  today: boolean;
  backgroundColor: string;
  borderColor: string;
}

@Component({
  selector: 'app-calendar-info',
  templateUrl: './calendar-info.component.html',
  styleUrls: ['./calendar-info.component.scss']
})
export class CalendarInfoComponent implements OnInit, AfterViewInit {
  date: any;
  listSelectedDate: SelectDate[] = [];
  color = {
    today: '#141ED2',
    goToWork: '#66CAFF',
    halfOff: '#F4B740',
    fullOff: '#FF4D4F'
  }

  listNoteType = [
    {type: 4, borderColor: this.color.today, title: "dashboard.label.today"},
    {type: 1, borderColor: this.color.goToWork, title: "dashboard.label.goToWork"},
    {type: 2, borderColor: this.color.halfOff, title: "dashboard.label.halfOff"},
    {type: 3, borderColor: this.color.fullOff, title: "dashboard.label.fullOff"}
  ];

  @ViewChild("elementCalBox") elementCalBox: ElementRef;
  @ViewChild("elementCusMonth") elementCusMonth: ElementRef;
  calValue: any;
  $window = window;
  userLogin: UserLogin = new UserLogin();
  currentUser: User;
  employeeId = 1;

  constructor(
    private renderer: Renderer2,
    private router: Router,
    private translate: TranslateService,
    private calendarService: CalendarService,
    private userService: UserService,
  ) {
    this.userLogin = StorageService.get(STORAGE_NAME.USER_LOGIN);
    if(!localStorage.getItem(SessionKey.CURRENCY_USER)) {
      this.userService.getCurrentUser().toPromise().then(res => {
        if(res && res.data) {
          localStorage.setItem(SessionKey.CURRENCY_USER, JSON.stringify(res.data));
          this.currentUser = res.data;
        }
      });
    } else {
      if (localStorage.getItem(SessionKey.CURRENCY_USER)) {
        this.currentUser = JSON.parse(localStorage.getItem(SessionKey.CURRENCY_USER) ?? '');
      }
      this.employeeId = this.userLogin?.employeeId ?? this.currentUser?.empId;
    }
  }

  ngOnInit(): void {
    this.translate.onLangChange.subscribe(res => {
       setTimeout(() => {
         this.changeDataContent();
       }, 0);
    });
  }

  getListTimeKeeping(fromDate: any, toDate: any) {
    let params = new HttpParams();

    params = params.append('fromDate', fromDate);
    params = params.append('toDate', toDate);
    params = params.append('employeeId', this.employeeId);
    this.calendarService.getListTimeKeeping(params).subscribe( res => {
      if (res && res.data) {
        res.data.forEach(el => {
          el.backgroundColor = 'transparent';
          if(el.today) {
            el.backgroundColor = this.color.today;
          }
          switch (el.type) {
            case 2:
              el.borderColor = this.color.halfOff;
              break;
            case 3:
              el.borderColor = this.color.fullOff;
              break;
            default:
              el.borderColor = this.color.goToWork;
              break;
          }
        })
      }
      this.listSelectedDate = res.data;
    })
  }

  ngAfterViewInit() {
    this.changeDataContent();
    let elementsHeader = this.elementCalBox.nativeElement.querySelector('.ant-picker-header');
    let elementsPicker = this.elementCalBox.nativeElement.querySelectorAll('.ant-picker-cell');
    let elementsButton = elementsHeader.querySelectorAll('button');
    elementsButton.forEach(item => {
      this.renderer.listen(item, 'click', () => {
       this.changeDataContent();
      });
    });
    elementsPicker.forEach(item => {
      this.renderer.listen(item, 'click', () => {
        this.changeDataContent();
      });
    });
  }

  changeDataContent() {
    this.listSelectedDate = [];
    let elementsHeader = this.elementCalBox.nativeElement.querySelector('.ant-picker-header');
    let elementsContent = this.elementCalBox.nativeElement.querySelector('.ant-picker-content');
    let elements = elementsContent.querySelectorAll('th');
    let elementsMonth = elementsHeader.querySelector('.ant-picker-header-month-btn');
    elements.forEach(item => {
      if (this.translate.currentLang == 'vn') {
        item.innerHTML = item.textContent.replace('Th ', 'T');
      }
    });
    let dateInner = moment(elementsMonth.textContent, 'MM/YYYY').toDate();
    if (this.translate.currentLang == 'vn') {
      this.calValue = 'Tháng ' + (dateInner.getMonth() + 1) + ', ' + dateInner.getFullYear();
    } else {
      this.calValue = moment(dateInner).format('MM/YYYY');
    }
    this.elementCusMonth.nativeElement.innerHTML = this.calValue;
    if(this.employeeId) {
      this.getListTimeKeeping(Utils.getFirstDayOrLastDayOfCurrentMonthString(true, dateInner.getMonth(), dateInner.getFullYear()),
        Utils.getFirstDayOrLastDayOfCurrentMonthString(false, dateInner.getMonth() + 1, dateInner.getFullYear()));
    }
  }

  getStyle(current: any): string {
    let style: string = '';
    let textColor = '#000';
    let dateStr = moment(current).format('DD/MM/YYYY');
    this.listSelectedDate.forEach(item => {
      if (item.date == dateStr) {
        if (item.backgroundColor != 'transparent')
          textColor = '#fff';
        style = `border: 1px solid ${item.borderColor}!important; background:${item.backgroundColor}; color:${textColor}`;
      }
    });
    return style;
  }

  goToRegisterPage() {
    this.router.navigateByUrl(environment.url.urlRequestsManager);
  }
}
