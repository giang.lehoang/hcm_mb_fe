import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalendarInfoComponent } from './calendar-info/calendar-info.component';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { FormsModule } from '@angular/forms';
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
    imports: [CommonModule, NzDatePickerModule, SharedUiMbButtonModule, FormsModule, TranslateModule],
  declarations: [
    CalendarInfoComponent
  ],
  exports: [CalendarInfoComponent]
})
export class DashboardManagerFeatureCalendarInfoModule {}
