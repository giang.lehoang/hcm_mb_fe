import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GeneralInfoComponent } from './general-info/general-info.component';
import { DashboardManagerFeatureApplicationInfoModule } from '@hcm-mfe/dashboard-manager/feature/application-info';
import { DashboardManagerFeatureArticleInfoModule } from '@hcm-mfe/dashboard-manager/feature/article-info';
import { DashboardManagerFeatureMaximInfoModule } from '@hcm-mfe/dashboard-manager/feature/maxim-info';
import { DashboardManagerFeatureBannerInfoModule } from '@hcm-mfe/dashboard-manager/feature/banner-info';
import { DashboardManagerFeatureCalendarInfoModule } from '@hcm-mfe/dashboard-manager/feature/calendar-info';
import { DashboardManagerFeaturePersonInfoModule } from '@hcm-mfe/dashboard-manager/feature/person-info';
import {RouterModule} from "@angular/router";
import {NzModalModule} from "ng-zorro-antd/modal";

@NgModule({
  imports: [CommonModule,
    DashboardManagerFeatureApplicationInfoModule,
    DashboardManagerFeatureArticleInfoModule,
    DashboardManagerFeatureMaximInfoModule,
    DashboardManagerFeatureBannerInfoModule,
    DashboardManagerFeatureCalendarInfoModule,
    DashboardManagerFeaturePersonInfoModule,
    RouterModule.forChild([
      {
        path: '',
        component: GeneralInfoComponent
      }
    ]),
    NzModalModule
  ],
  declarations: [GeneralInfoComponent],
  exports: [GeneralInfoComponent]
})
export class DashboardManagerFeatureGeneralInfoModule {}
