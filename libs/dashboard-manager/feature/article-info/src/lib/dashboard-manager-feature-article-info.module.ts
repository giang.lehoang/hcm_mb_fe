import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticleInfoComponent } from './article-info/article-info.component';
import { SharedPipesDateFormatModule } from '@hcm-mfe/shared/pipes/date-format';

@NgModule({
  imports: [CommonModule,
    SharedPipesDateFormatModule],
  declarations: [
    ArticleInfoComponent
  ],
  exports: [ArticleInfoComponent]
})
export class DashboardManagerFeatureArticleInfoModule {}
