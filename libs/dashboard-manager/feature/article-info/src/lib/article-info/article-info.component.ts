import {
  AfterViewChecked,
  AfterViewInit,
  Component,
  ElementRef,
  HostListener,
  OnInit,
  QueryList,
  Renderer2,
  ViewChild,
  ViewChildren
} from '@angular/core';
import { auto } from '@popperjs/core';
import { DashboardInfoService } from '@hcm-mfe/dashboard-manager/data-access/services';

@Component({
  selector: 'app-article-info',
  templateUrl: './article-info.component.html',
  styleUrls: ['./article-info.component.scss']
})
export class ArticleInfoComponent implements OnInit, AfterViewInit, AfterViewChecked {
  currentWidth = window.innerWidth;
  arts = [];
  checkFirst = false;

  @ViewChild('elementHeight', {static: false}) elementHeight: ElementRef;
  @ViewChild('elementArtItems', {static: false}) elementArtItems: ElementRef;
  @ViewChildren('elementSubHeight') elementSubHeight: QueryList<ElementRef>;
  treemapObserver = new ResizeObserver(() => this.render());

  constructor(
    private dashboardService: DashboardInfoService
  ) {
  }

  ngOnInit(): void {
    this.getArticle();
  }

  getArticle() {
    this.dashboardService.getArticle().subscribe(res => {
      if (res.code == '200' && res.data != undefined && res.data.listData != undefined) {
        res.data.listData.forEach(item => {
          if (item.imgLargeData != undefined)
            item.imgLargeData = item.imgLargeData != undefined ? 'data:image/jpg;base64,' + item.imgLargeData : 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';
        });
        this.arts = res.data.listData;
      }
    });
  }

  private render() {
    this.currentWidth = window.innerWidth;
    let heightItems = 0;
    this.elementSubHeight?.toArray().forEach((item: ElementRef, i: number) => {
        if (item.nativeElement.offsetHeight > 0 && i < 4) {
          heightItems += item.nativeElement.offsetHeight;
        }
      }
    );
    if (this.currentWidth > 1100) {
      let gap = 0;
      let heightArts = 0;
      if (this.elementHeight?.nativeElement?.offsetHeight > 0) {
        heightArts = this.elementHeight.nativeElement.offsetHeight;
        gap = Math.round((heightArts - heightItems) / 3);
        if (gap < 0) {
          gap = 20;
        }
        if (gap > 20) {
          let _n = 4;
          heightItems = 0;
          for (let i = 0; i < this.elementSubHeight.toArray().length; i++) {
            heightItems += this.elementSubHeight.toArray()[i].nativeElement.offsetHeight;
            if ((heightItems + (i * 20)) >= heightArts) {
              _n = i + 1;
              break;
            }
          }
          gap = Math.round((heightArts - heightItems) / (_n - 1));
        }
        this.elementArtItems?.nativeElement.setAttribute('style', 'grid-gap: ' + gap + 'px; height: ' + heightArts + 'px');
      }
    } else {
      this.elementArtItems?.nativeElement.setAttribute('style', 'grid-gap: 20px; height: ' + (heightItems + 60) + 'px');
    }
  }

  ngAfterViewInit() {
    this.checkFirst = false;
  }

  ngAfterViewChecked() {
    if (this.elementArtItems && this.checkFirst == false) {
      this.checkFirst = true;
      this.treemapObserver.observe(this.elementArtItems.nativeElement);
    }
  }

  @HostListener('window:resize', ['$event'])
  onWindowResize(event) {
    this.currentWidth = event.target.innerWidth;
    this.elementArtItems?.nativeElement?.removeAttribute('style');
    this.render();
  }

  openImg(data) {
    const contentType = data?.split(';')[0]?.split(':')[1];
    const byteCharacters = atob(data.substr(`data:${contentType};base64,`.length));
    const byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += 1024) {
      const slice = byteCharacters.slice(offset, offset + 1024);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);

      byteArrays.push(byteArray);
    }
    const blob = new Blob(byteArrays, {type: contentType});
    const blobUrl = URL.createObjectURL(blob);

    window.open(blobUrl, '_blank');
  }
}
