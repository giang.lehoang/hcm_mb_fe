import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersonalInfoComponent } from './personal-info/personal-info.component';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { ToastrModule } from 'ngx-toastr';
import {NzIconModule} from "ng-zorro-antd/icon";

@NgModule({
    imports: [CommonModule, NzSpinModule, NzUploadModule, SharedUiMbButtonModule, ToastrModule, NzIconModule],
  declarations: [
    PersonalInfoComponent
  ],
  exports: [PersonalInfoComponent],
})
export class DashboardManagerFeaturePersonInfoModule {}
