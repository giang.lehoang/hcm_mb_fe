import { Component, OnDestroy, OnInit } from '@angular/core';
import { NzUploadXHRArgs } from 'ng-zorro-antd/upload';
import { Observable, Observer, Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { StorageService } from "@hcm-mfe/shared/common/store";
import { STORAGE_NAME } from '@hcm-mfe/shared/common/constants';
import { User, UserLogin } from '@hcm-mfe/shared/data-access/models';
import { environment } from '@hcm-mfe/shared/environment';
import { SessionKey } from '@hcm-mfe/shared/common/enums';
import { PersonalInfoService } from '@hcm-mfe/dashboard-manager/data-access/services';
import { CustomToastrService, UserService } from '@hcm-mfe/shared/core';

@Component({
  selector: 'app-personal-info',
  templateUrl: './personal-info.component.html',
  styleUrls: ['./personal-info.component.scss'],
})
export class PersonalInfoComponent implements OnInit, OnDestroy {
  avtBase64: string | ArrayBuffer = '';
  avtIsLoading = false;
  fileAvt: NzUploadXHRArgs;
  formDataAvt: FormData;
  showTmp = false;
  subs: Subscription[] = [];
  employeeId: number;
  userLogin: UserLogin = new UserLogin();
  currentUser: User;

  constructor(
    private toastrService: CustomToastrService,
    private translate: TranslateService,
    private router: Router,
    private userService: UserService,
    private personalInfoService: PersonalInfoService,
  ) {
    this.userLogin = StorageService.get(STORAGE_NAME.USER_LOGIN);
    if(!localStorage.getItem(SessionKey.CURRENCY_USER)) {
      this.userService.getCurrentUser().toPromise().then(res => {
        if(res && res.data) {
          localStorage.setItem(SessionKey.CURRENCY_USER, JSON.stringify(res.data));
          this.currentUser = res.data;
          this.getAvatar(this.currentUser.empId);
        }
      });
    } else {
      if (localStorage.getItem(SessionKey.CURRENCY_USER)) {
        this.currentUser = JSON.parse(localStorage.getItem(SessionKey.CURRENCY_USER) ?? '');
      }
      this.employeeId = this.userLogin?.employeeId ?? this.currentUser?.empId;
    }
  }

  ngOnInit(): void {
    if(this.employeeId) {
      this.getAvatar(this.employeeId);
    }
  }

  ngOnDestroy() {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  beforeUpload = (file: File) => {
    return new Observable((observer: Observer<boolean>) => {
      const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
      if (!isJpgOrPng) {
        // this.toastrService.error(this.translate.instant('staffManager.validate.fileType', { fileType: 'png, jpg' }));
        observer.complete();
        return;
      }
      observer.next(isJpgOrPng);
      observer.complete();
    });
  };

  handleUpload = (item: NzUploadXHRArgs) => {
    this.avtIsLoading = true;
    const formData = new FormData();
    formData.append('fileAvatar', item.file as any);
    this.formDataAvt = formData;
    this.fileAvt = item;
    const reader = new FileReader();
    reader.readAsDataURL(item.file as any);
    reader.onload = () => {
      this.avtBase64 = reader.result;
      this.avtIsLoading = false;
      this.showTmp = true;
    };
  };

  getAvatar(employeeId: number) {
    this.avtIsLoading = true;
    this.subs.push(
      this.personalInfoService.getAvatar(employeeId).subscribe(res => {
        this.avtBase64 = res?.data ? 'data:image/jpg;base64,' + res?.data : 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';
        this.avtIsLoading = false;
      })
    );
  }

  deleteAvt() {
    this.avtIsLoading = true;
    this.subs.push(
      this.personalInfoService.deleteAvatar(this.employeeId).subscribe(res => {
        this.avtBase64 = 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';
        this.avtIsLoading = false;
        this.toastrService.success(this.translate.instant('common.notification.updateSuccess'));
      }, error => {
        this.avtIsLoading = false;
      })
    );
  }

  saveAvt() {
    if (this.fileAvt && this.formDataAvt) {
      this.avtIsLoading = true;
      this.subs.push(
        this.personalInfoService.uploadAvatar(this.employeeId, this.formDataAvt).subscribe(res => {
          this.getAvatar(this.employeeId);
          this.avtIsLoading = false;
          this.showTmp = false;
          this.fileAvt = null;
          this.formDataAvt = null;
        })
      );
    }
  }

  viewInfo() {
    this.router.navigateByUrl(environment.url.urlPersonalInfo);
  }
}
