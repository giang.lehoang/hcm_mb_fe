import { Component, OnInit } from '@angular/core';
import SwiperCore, { Autoplay, Navigation, Pagination, Virtual } from 'swiper';
import { DashboardInfoService } from '@hcm-mfe/dashboard-manager/data-access/services';

SwiperCore.use([
  Autoplay,
  Navigation,
  Pagination,
  Virtual
]);

@Component({
  selector: 'app-maxim-info',
  templateUrl: './maxim-info.component.html',
  styleUrls: ['./maxim-info.component.scss']
})
export class MaximInfoComponent implements OnInit {
  maxims = [];

  constructor(private dashboardService: DashboardInfoService) {
  }

  ngOnInit(): void {
    this.getMaxim();
  }

  getMaxim() {
    this.dashboardService.getBanner('DN').subscribe(res => {
      if (res.code == '200' && res.data != undefined && res.data.listData != undefined) {
        res.data.listData.forEach(item => {
          item.iconPath = '/assets/img/maxim/maxim.png';
        });
        this.maxims = res.data.listData;
      }
    });
  }

}
