import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaximInfoComponent } from './maxim-info/maxim-info.component';
import { SwiperModule } from 'swiper/angular';

@NgModule({
  imports: [CommonModule, SwiperModule],
  declarations: [
    MaximInfoComponent
  ],
  exports: [MaximInfoComponent]
})
export class DashboardManagerFeatureMaximInfoModule {}
