import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { SwiperComponent } from 'swiper/angular';
import SwiperCore, { Autoplay, Navigation, Virtual } from 'swiper';
import { environment } from '@hcm-mfe/shared/environment';
import {NzModalRef, NzModalService} from "ng-zorro-antd/modal";
import {Constant} from "@hcm-mfe/dashboard-manager/data-access/models";
import {TranslateService} from "@ngx-translate/core";
import {DownloadFileModalComponent} from "@hcm-mfe/dashboard-manager/feature/download-file-modal";
import { StorageService } from '@hcm-mfe/shared/common/store';
import { STORAGE_NAME } from '@hcm-mfe/shared/common/constants';

SwiperCore.use([
  Autoplay,
  Navigation,
  Virtual
]);

@Component({
  selector: 'app-application-info',
  templateUrl: './application-info.component.html',
  styleUrls: ['./application-info.component.scss']
})
export class ApplicationInfoComponent implements OnInit {

  @ViewChild('swiperComponent') swiper: SwiperComponent;
  isDisable = StorageService.get(STORAGE_NAME.APP) === environment.app.HCM_ESS_WEB;
  activeIndex = 0;
  slidesPerView = 6;
  wSpace = 0;
  offsetWidth = 480;
  currentWidth = window.innerWidth;
  apps = [
    { url: '#', title: this.translateService.instant("dashboard.title.forms"), iconPath: '/assets/img/icon_card/mau_bieu.png', disabled: false, openNewTab: false, downloadFile: true },
    { url: environment.url.urlRequestsManager, title: this.translateService.instant("dashboard.title.requestsManager"), iconPath: '/assets/img/icon_card/nghi.png', disabled: false, openNewTab: false },
    { url: environment.url.urlPayroll, title: this.translateService.instant("dashboard.title.salaryInfo"), iconPath: '/assets/img/icon_card/payroll.png', disabled: this.isDisable, openNewTab: true },
    { url: '#', title: this.translateService.instant("dashboard.title.todoList"), iconPath: '/assets/img/icon_card/todo.png', disabled: true, openNewTab: false },
    { url: environment.url.urlTraining, title: this.translateService.instant("dashboard.title.training"), iconPath: '/assets/img/icon_card/dao_tao.png', disabled: this.isDisable, openNewTab: true },
    { url: environment.url.urlRecruitment, title: this.translateService.instant("dashboard.title.recruitment"), iconPath: '/assets/img/icon_card/tuyen_dung.png', disabled: this.isDisable, openNewTab: true },
    { url: '#', title: this.translateService.instant("dashboard.title.familyAllowances"), iconPath: '/assets/img/icon_card/giam_tru.png', disabled: true, openNewTab: false },
    { url: environment.url.urlCareerPath, title: this.translateService.instant("dashboard.title.careerPath"), iconPath: '/assets/img/icon_card/career_path.png', disabled: this.isDisable, openNewTab: true },
    { url: environment.url.urlHRInsider, title: this.translateService.instant("dashboard.title.HRInsider"), iconPath: '/assets/img/icon_card/hr_insider.png', disabled: this.isDisable, openNewTab: true }
  ];

  modal: NzModalRef;
  listFiles = Constant.LIST_FILE;

  swiperConfig: any = {
    loop: false,
    spaceBetween: this.wSpace,
    virtual: true,
    breakpoints: {
      200: {
        slidesPerView: 1,
        spaceBetween: 0
      },
      360: {
        slidesPerView: 2
      },
      500: {
        slidesPerView: 3
      },
      767: {
        slidesPerView: 4
      },
      992: {
        slidesPerView: this.slidesPerView
      },
      993: {
        slidesPerView: 3
      },
      1100: {
        slidesPerView: 4
      },
      1300: {
        slidesPerView: 6
      },
      1500: {
        slidesPerView: this.slidesPerView
      }
    }
  };


  constructor(private modalService: NzModalService,
              private translateService: TranslateService) {
    if (this.currentWidth > 992) {
      this.offsetWidth = this.currentWidth - 160 - 450;
    } else {
      this.offsetWidth = this.currentWidth - 48;
    }
  }

  ngOnInit(): void {
    this.wSpace = (this.offsetWidth - this.slidesPerView * 198) / (this.slidesPerView - 1);
    this.resizeConfig();
  }

  swipeNext() {
    this.swiper.swiperRef.slideNext();
  }

  swipePrev() {
    this.swiper.swiperRef.slidePrev();
  }

  slideChangeEvent() {
    this.activeIndex = this.swiper.swiperRef.activeIndex;
  }

  @HostListener('window:resize', ['$event'])
  onWindowResize(event) {
    this.currentWidth = event.target.innerWidth;
    this.resizeConfig();
  }

  resizeConfig() {
    this.offsetWidth = this.currentWidth - 160 - 450;
    this.wSpace = (this.offsetWidth - 6 * 198) / 5;
    if (this.currentWidth < 1441) {
      this.offsetWidth = this.currentWidth - 160 - 315;
      this.wSpace = (this.offsetWidth - 6 * 129) / 5;
    }
    if (this.currentWidth < 1100) {
      this.offsetWidth = this.currentWidth - 160 - 315;
      this.wSpace = (this.offsetWidth - 3 * 198) / 2;
    }
    if (this.currentWidth < 993) {
      this.offsetWidth = this.currentWidth - 48;
      this.wSpace = (this.offsetWidth - 6 * 198) / 5;
    }
    if (this.currentWidth < 992) {
      this.offsetWidth = this.currentWidth - 48;
      this.wSpace = (this.offsetWidth - 4 * 198) / 3;
    }
    if (this.currentWidth < 767) {
      this.offsetWidth = this.currentWidth - 48;
      this.wSpace = (this.offsetWidth - 3 * 198) / 2;
    }
    if (this.currentWidth < 500) {
      this.offsetWidth = this.currentWidth - 48;
      this.wSpace = (this.offsetWidth - 2 * 129) / 1;
    }
  }

  downloadFile(downloadFile) {
    if (downloadFile) {
      this.modal = this.modalService.create({
        nzWidth: window.innerWidth > 767 ? window.innerWidth / 2 > 1100 ? 1100 : window.innerWidth / 2 : window.innerWidth,
        nzTitle: this.translateService.instant('dashboard.label.title'),
        nzContent: DownloadFileModalComponent,
        nzComponentParams: {
          listFile: this.listFiles
        },
        nzFooter: null
      });
    }
  }
}
