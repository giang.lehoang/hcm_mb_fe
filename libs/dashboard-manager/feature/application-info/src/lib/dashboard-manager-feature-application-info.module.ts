import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApplicationInfoComponent } from './application-info/application-info.component';
import { SwiperModule } from 'swiper/angular';
import { SharedUiMbButtonModule } from '@hcm-mfe/shared/ui/mb-button';
import { SharedUiMbCardItemModule } from '@hcm-mfe/shared/ui/mb-card-item';

@NgModule({
  imports: [CommonModule, SwiperModule, SharedUiMbButtonModule, SharedUiMbCardItemModule],
  declarations: [
    ApplicationInfoComponent
  ],
  exports: [ApplicationInfoComponent]
})
export class DashboardManagerFeatureApplicationInfoModule {}
