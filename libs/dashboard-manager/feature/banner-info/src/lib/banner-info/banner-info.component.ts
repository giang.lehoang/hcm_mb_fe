import { Component, OnInit, ViewChild } from '@angular/core';
import SwiperCore, { Autoplay, Navigation, Virtual } from 'swiper';
import { SwiperComponent } from 'swiper/angular';
import { DashboardInfoService } from '@hcm-mfe/dashboard-manager/data-access/services';
import { NzSafeAny } from 'ng-zorro-antd/core/types';

SwiperCore.use([
  Autoplay,
  Navigation,
  Virtual
]);

@Component({
  selector: 'app-banner-info',
  templateUrl: './banner-info.component.html',
  styleUrls: ['./banner-info.component.scss']
})
export class BannerInfoComponent implements OnInit {

  @ViewChild('swiperComponent') swiper: SwiperComponent;
  activeIndex = 0;

  slides2 = [];

  constructor(
    private dashboardService: DashboardInfoService
  ) {
  }

  ngOnInit(): void {
    this.getBanner();
  }

  getBanner() {
    this.dashboardService.getBanner('BN').subscribe(res => {
      if (res.code == '200' && res.data != undefined && res.data.listData != undefined) {
        res.data.listData.forEach(item => {
          if (item.imgData != undefined)
            item.imgData = item.imgData != undefined ? 'data:image/jpg;base64,' + item.imgData : 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';
        });
        this.slides2 = res.data.listData;
      }
    });
  }

  swipeNext() {
    this.swiper.swiperRef.slideNext();
  }

  swipePrev() {
    this.swiper.swiperRef.slidePrev();
  }

  slideChangeEvent() {
    this.activeIndex = this.swiper.swiperRef.activeIndex;
  }

  clickBanner(slide: NzSafeAny) {
    if (slide.refLink) {
      window.open(slide.refLink);
    }
  }
}
