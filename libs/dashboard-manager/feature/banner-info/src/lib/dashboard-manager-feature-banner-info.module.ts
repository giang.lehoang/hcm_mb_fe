import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BannerInfoComponent } from './banner-info/banner-info.component';
import { SwiperModule } from 'swiper/angular';

@NgModule({
  imports: [CommonModule, SwiperModule],
  declarations: [
    BannerInfoComponent
  ],
  exports: [BannerInfoComponent]
})
export class DashboardManagerFeatureBannerInfoModule {}
