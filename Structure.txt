root
│
├───apps
│   ├───shell
│   ├───system
│   ├───dashboard-manager
│   ├───staff-manager
│   ├───staff-abs
│   ├───goal-management
│   ├───model-organization
│   └───learn-development
│
├───dist
├───libs
    ├───shared
    │   ├───assets // chứa các file ảnh, i18n, css, font
    │   ├───common // chứa các component, services, module dùng chung cho cả hệ thống
    │   │   ├───base-component
    │   │   ├───enums
    │   │   ├───keycloak
    │   │   ├───utils
    │   │   ├───validators
    │   │   ├───init
    │   │   ├───base-service
    │   │   ├───translate
    │   │   └───constants
    │   ├───core // chứa các service dùng chung
    │   ├───data-access
    │   │   ├───models // chứa các model dùng chung cho share
    │   │   ├───services // chứa các service dùng cho share
    │   │   └───store
    │   ├───directives // chứa các directives
    │   ├───environment // cấu hình url môi trường
    │   ├───pipes // chứa các pipes
    │   ├───feature // chứa các feature dùng cho các mfe
    │   └───ui // chứa các component common
    │       ├───mb-button
    │       ├───mb-date-time-work
    │       ├───mb-data-picker
    │       ├───mb-card-item
    │       ├───mb-button-icon
    │       ├───mb-collapse
    │       ├───mb-date-picker
    │       ├───mb-import
    │       ├───core
    │       ├───loading
    │
    ├───shell
    │   ├───data-access
    │   │   	├───common // chứa các constant, enum dùng trong mfe
    │   │   	├───models // chứa các model dùng trong mfe
    │   │   	└───services // // chứa các service dùng trong mfe
    │   └───feature
    │     ├───header
    │     ├───notification
    │     ├───menu-search
    │     └───layout
    │           └───src
    │               └───lib
    │                   └───layout
    │
    ├───system
    │   ├───data-access
    │   ├───feature // chứa các lib feature. mỗi 1 component tương ứng với 1 lib
    │   │   ├───applications-config-form
    │   │   ├───permission-category-form
    │   │   ├───manipulation-form
    │   │   ├───applications-config-list
    │   │   ├───manipulation-list
    │   │   └───shell-system // chứa routing forward đến các lib(component) cụ thể
    │   └───ui // chứa các component dùng chung trong mfe
    │       └───choose-function
    ├───dashboard-manager
        ├───data-access
        ├───feature
        └───ui
